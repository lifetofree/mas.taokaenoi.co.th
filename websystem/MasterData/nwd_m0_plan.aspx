﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="nwd_m0_plan.aspx.cs" Inherits="websystem_MasterData_nwd_m0_plan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <%--  <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadImages").MultiFile();
            }
        })
    </script>


    <script type="text/javascript" src='<%=ResolveUrl("~/Scripts/jquery.MultiFile.js")%>'></script>--%>



    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <div class="col-md-12">
                <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary" runat="server" CommandName="btnToInsert" OnCommand="btnCommand"><i class="fa fa-plus-square"></i> สร้างแผนผังวางอุปกรณ์ </asp:LinkButton>

                <asp:GridView ID="GvMaster"
                    runat="server"
                    AutoGenerateColumns="false"
                    DataKeyNames="imgidx"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowEditing="Master_RowEditing"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">No result</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="imgidx" runat="server" CssClass="form-control"
                                    Visible="False" Text='<%# Eval("imgidx")%>' />


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">สถานที่ตั้งอุปกรณ์</label>
                                            <asp:Label ID="lblLocIDX" runat="server" Text='<%# Bind("LocIDX") %>' Visible="false" />
                                            <asp:DropDownList ID="ddlLocUpdate" AutoPostBack="true" runat="server"
                                                CssClass="form-control">
                                            </asp:DropDownList>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">อาคารที่ตั้งอุปกรณ์</label>
                                            <asp:Label ID="lblBUIDX" runat="server" Text='<%# Bind("BUIDX") %>' Visible="false" />
                                            <asp:DropDownList ID="ddlBuildUpdate" AutoPostBack="true" runat="server"
                                                CssClass="form-control">
                                            </asp:DropDownList>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">ชั้นที่ตั้งอุปกรณ์</label>
                                            <asp:Label ID="lblFLIDX" runat="server" Text='<%# Bind("FLIDX") %>' Visible="false" />
                                            <asp:DropDownList ID="ddlFloorUpdate" AutoPostBack="true" runat="server"
                                                CssClass="form-control">
                                            </asp:DropDownList>
                                        </small>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">ชื่อแผนผัง</label>
                                            <asp:TextBox ID="txtimage_update" CssClass="form-control" runat="server" Text='<%# Bind("img_name") %>' />

                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">สถานะ</label>
                                            <asp:DropDownList ID="ddlStatusUpdate" AutoPostBack="false" runat="server"
                                                CssClass="form-control" SelectedValue='<%# Eval("img_status") %>'>
                                                <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                            </asp:DropDownList>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                            ValidationGroup="saveTypeNameUpdate" CommandName="Update"
                                            OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                         บันทึก
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                            CommandName="Cancel">ยกเลิก</asp:LinkButton>
                                    </div>
                                </div>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานที่ตั้งอุปกรณ์" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Locname" runat="server" Text='<%# Eval("place_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="อาคารที่ตั้งอุปกรณ์" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Buildname" runat="server" Text='<%# Eval("room_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชั้นที่ตั้งอุปกรณ์" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Floorname" runat="server" Text='<%# Eval("Floor_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="File Document"  ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" >
                            <ItemTemplate>
                                 <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>
                                 <asp:HiddenField runat="server" ID="hidFile11" Value='<%#  Eval("img_name") %>' />

                            </ItemTemplate>

                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>



                        <%--<asp:TemplateField HeaderText="ชื่อแผนผัง" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Regisname" runat="server" Text='<%# Eval("img_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>--%>



                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Statuscategory" runat="server" Text='<%# getStatus((int)Eval("img_status")) %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("imgidx") %>'
                                    OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>


        <asp:View ID="ViewInsert" runat="server">
            <div class="row col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <asp:LinkButton CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                                CommandName="btnCancel" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label>สถานที่ตั้งอุปกรณ์</label>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>

                                    <asp:DropDownList ID="ddlLoc" runat="server" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="requiredddltype"
                                        ValidationGroup="save" runat="server"
                                        Display="Dynamic"
                                        SetFocusOnError="true"
                                        ControlToValidate="ddlLoc"
                                        Font-Size="1em" ForeColor="Red"
                                        ErrorMessage="กรุณาเลือกสถานที่ตั้งอุปกรณ์" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>อาคารที่ตั้งอุปกรณ์</label>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>

                                    <asp:DropDownList ID="ddlBuild" runat="server" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                        ValidationGroup="save" runat="server"
                                        Display="Dynamic"
                                        SetFocusOnError="true"
                                        ControlToValidate="ddlBuild"
                                        Font-Size="1em" ForeColor="Red"
                                        ErrorMessage="กรุณาเลือกอาคารที่ตั้งอุปกรณ์" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>ชั้นที่ตั้งอุปกรณ์</label>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>

                                    <asp:DropDownList ID="ddlfloor" runat="server" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                        ValidationGroup="save" runat="server"
                                        Display="Dynamic"
                                        SetFocusOnError="true"
                                        ControlToValidate="ddlfloor"
                                        Font-Size="1em" ForeColor="Red"
                                        ErrorMessage="กรุณาเลือกชั้นที่ตั้งอุปกรณ์" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>ชื่อแผนผังอุปกรณ์</label>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtimage" runat="server" CssClass="form-control" PlaceHolder="Ex. สถานที่_อาคาร_ชั้น = RJN_01_01"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="save" runat="server" Display="None" ControlToValidate="txtimage" Font-Size="11"
                                        ErrorMessage="กรุณากรอกชื่อแผนผังอุปกรณ์"
                                        ValidationExpression="กรุณากรอกชื่อแผนผังอุปกรณ์"
                                        SetFocusOnError="true" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                    <asp:RegularExpressionValidator ID="RegularExpressiondValidator1" runat="server"
                                        ValidationGroup="Save" Display="None"
                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                        ControlToValidate="txtimage"
                                        ValidationExpression="^[\s\S]{0,1000}$"
                                        SetFocusOnError="true" />

                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressiondValidator1" Width="160" />

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>สถานะ</label>
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                                <asp:ListItem Value="1" Text="Online" />
                                <asp:ListItem Value="0" Text="Offline" />
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Upload File</label>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>

                                    <asp:FileUpload ID="UploadImages" Font-Size="small" CssClass="btn btn-warning btn-sm " AutoPostBack="true" ViewStateMode="Enabled" runat="server" accept="gif|jpg|png" />
                                    </div>
                                   
                                    <asp:RequiredFieldValidator ID="RequiredFielduploadfile_memo" runat="server"
                                        ControlToValidate="UploadImages" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณาเลือกไฟล์ jpg หรือ pdf*" ValidationGroup="Saveinsert" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalluploadfile_memo" runat="Server"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFielduploadfile_memo" />

                                    <asp:RegularExpressionValidator ID="RegularExpresuploadfile_memo"
                                        runat="server" ErrorMessage="*กรุณาเลือกเฉพาะไฟล์ jpg หรือ pdf*" SetFocusOnError="true"
                                        Display="None" ControlToValidate="UploadImages" ValidationExpression="[a-zA-Z\\].*(.jpg|.JPG|.pdf|.PDF)$"
                                        ValidationGroup="Saveinsert" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validatoruploadfile_memo" runat="Server"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpresuploadfile_memo" />
                                    <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|png</font></p>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="form-group">
                            <asp:LinkButton id="lbsave" CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="Save" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>

    </asp:MultiView>
</asp:Content>

