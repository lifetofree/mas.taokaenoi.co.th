using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_hr_payroll")]
public class data_hr_payroll
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("slip_mode")]
    public string slip_mode { get; set; }

    [XmlElement("slip_data_list_u0")]
    public slip_data_detail_u0[] slip_data_list_u0 { get; set; }
    [XmlElement("en_slip_data_list_u0")]
    public en_slip_data_detail_u0[] en_slip_data_list_u0 { get; set; }
    [XmlElement("search_slip_data_list")]
    public search_slip_data_detail[] search_slip_data_list { get; set; }

    [XmlElement("es_paytype_m0_list")]
    public es_paytype_m0_detail[] es_paytype_m0_list { get; set; }
    [XmlElement("es_paycde_m0_list")]
    public es_paycde_m0_detail[] es_paycde_m0_list { get; set; }
}

[Serializable]
public class slip_data_detail_u0
{
    [XmlElement("u0_idx")]
    public int u0_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("personal_email")]
    public string personal_email { get; set; }
    [XmlElement("account_no")]
    public string account_no { get; set; }
    [XmlElement("start_date")]
    public string start_date { get; set; }
    
    [XmlElement("salary_rate")]
    public decimal salary_rate { get; set; } //อัตรา
    [XmlElement("slip_days")]
    public decimal slip_days { get; set; } //วัน
    [XmlElement("slip_e_salary")]
    public decimal slip_e_salary { get; set; } //เงินเดือน
    [XmlElement("slip_e_position")]
    public decimal slip_e_position { get; set; } //ตำแหน่ง
    [XmlElement("slip_e_ot10")]
    public decimal slip_e_ot10 { get; set; } //OTx1
    [XmlElement("slip_e_ot15")]
    public decimal slip_e_ot15 { get; set; } //OTx1.5
    [XmlElement("slip_e_ot20")]
    public decimal slip_e_ot20 { get; set; } //OTx2
    [XmlElement("slip_e_ot30")]
    public decimal slip_e_ot30 { get; set; } //OTx3
    [XmlElement("slip_e_living")]
    public decimal slip_e_living { get; set; } //ค่าครองชีพ
    [XmlElement("slip_e_telephone")]
    public decimal slip_e_telephone { get; set; } //ค่าโทรศัพท์
    [XmlElement("slip_e_allowance")]
    public decimal slip_e_allowance { get; set; } //เบี้ยขยัน
    [XmlElement("slip_e_operating")]
    public decimal slip_e_operating { get; set; } //ค่าปฏิบัติงาน
    [XmlElement("slip_e_travel")]
    public decimal slip_e_travel { get; set; } //ค่าเดินทาง
    [XmlElement("slip_e_commission")]
    public decimal slip_e_commission { get; set; } //ค่าคอมมิชชั่น
    [XmlElement("slip_e_incentive")]
    public decimal slip_e_incentive { get; set; } //Incentive
    [XmlElement("slip_e_guarantee")]
    public decimal slip_e_guarantee { get; set; } //คืนค้ำประกัน
    [XmlElement("slip_e_bonus")]
    public decimal slip_e_bonus { get; set; } //โบนัส
    [XmlElement("slip_e_other")]
    public decimal slip_e_other { get; set; } //เงินได้อื่น
    [XmlElement("slip_earnings")]
    public decimal slip_earnings { get; set; } //รวมรับ

    [XmlElement("slip_d_absent")]
    public decimal slip_d_absent { get; set; } //ขาดงาน
    [XmlElement("slip_d_late")]
    public decimal slip_d_late { get; set; } //มาสาย
    [XmlElement("slip_d_goback")]
    public decimal slip_d_goback { get; set; } //หักกลับก่อน
    [XmlElement("slip_d_over")]
    public decimal slip_d_over { get; set; } //หักลาเกินสิทธิ์
    [XmlElement("slip_d_broken")]
    public decimal slip_d_broken { get; set; } //หักบกพร่อง
    [XmlElement("slip_d_aia")]
    public decimal slip_d_aia { get; set; } //หักAIA
    [XmlElement("slip_d_uniform")]
    public decimal slip_d_uniform { get; set; } //หักเครื่องแบบ
    [XmlElement("slip_d_welfare")]
    public decimal slip_d_welfare { get; set; } //หักสวัสดิการ
    [XmlElement("slip_d_damage")]
    public decimal slip_d_damage { get; set; } //ของเสียหาย
    [XmlElement("slip_d_other")]
    public decimal slip_d_other { get; set; } //หักอื่นๆ
    [XmlElement("slip_d_tax")]
    public decimal slip_d_tax { get; set; } //หักภาษี
    [XmlElement("slip_d_fund")]
    public decimal slip_d_fund { get; set; } //หักกองทุน
    [XmlElement("slip_d_social")]
    public decimal slip_d_social { get; set; } //หักประกันสังคม
    [XmlElement("slip_d_guarantee")]
    public decimal slip_d_guarantee { get; set; } //ค้ำประกัน
    [XmlElement("slip_d_loan")]
    public decimal slip_d_loan { get; set; } //หักเงินกู้
    [XmlElement("slip_d_legal")]
    public decimal slip_d_legal { get; set; } //หักเงินกรมบังคับคดี
    [XmlElement("slip_deduction")]
    public decimal slip_deduction { get; set; } //รวมจ่าย
    [XmlElement("slip_net")]
    public decimal slip_net { get; set; } //สุทธิ

    [XmlElement("payroll_date")]
    public string payroll_date { get; set; } //วันที่จ่าย
    [XmlElement("slip_password")]
    public string slip_password { get; set; }
    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement("u0_status")]
    public int u0_status { get; set; }
}

[Serializable]
public class en_slip_data_detail_u0 : slip_data_detail_u0
{
    [XmlElement("en_salary_rate")]
    public string en_salary_rate { get; set; } //อัตรา
    [XmlElement("en_slip_days")]
    public string en_slip_days { get; set; } //วัน
    [XmlElement("en_slip_e_salary")]
    public string en_slip_e_salary { get; set; } //เงินเดือน
    [XmlElement("en_slip_e_position")]
    public string en_slip_e_position { get; set; } //ตำแหน่ง
    [XmlElement("en_slip_e_ot10")]
    public string en_slip_e_ot10 { get; set; } //OTx1
    [XmlElement("en_slip_e_ot15")]
    public string en_slip_e_ot15 { get; set; } //OTx1.5
    [XmlElement("en_slip_e_ot20")]
    public string en_slip_e_ot20 { get; set; } //OTx2
    [XmlElement("en_slip_e_ot30")]
    public string en_slip_e_ot30 { get; set; } //OTx3
    [XmlElement("en_slip_e_living")]
    public string en_slip_e_living { get; set; } //ค่าครองชีพ
    [XmlElement("en_slip_e_telephone")]
    public string en_slip_e_telephone { get; set; } //ค่าโทรศัพท์
    [XmlElement("en_slip_e_allowance")]
    public string en_slip_e_allowance { get; set; } //เบี้ยขยัน
    [XmlElement("en_slip_e_operating")]
    public string en_slip_e_operating { get; set; } //ค่าปฏิบัติงาน
    [XmlElement("en_slip_e_travel")]
    public string en_slip_e_travel { get; set; } //ค่าเดินทาง
    [XmlElement("en_slip_e_commission")]
    public string en_slip_e_commission { get; set; } //ค่าคอมมิชชั่น
    [XmlElement("en_slip_e_incentive")]
    public string en_slip_e_incentive { get; set; } //Incentive
    [XmlElement("en_slip_e_guarantee")]
    public string en_slip_e_guarantee { get; set; } //คืนค้ำประกัน
    [XmlElement("en_slip_e_bonus")]
    public string en_slip_e_bonus { get; set; } //โบนัส
    [XmlElement("en_slip_e_other")]
    public string en_slip_e_other { get; set; } //เงินได้อื่น
    [XmlElement("en_slip_earnings")]
    public string en_slip_earnings { get; set; } //รวมรับ

    [XmlElement("en_slip_d_absent")]
    public string en_slip_d_absent { get; set; } //ขาดงาน
    [XmlElement("en_slip_d_late")]
    public string en_slip_d_late { get; set; } //มาสาย
    [XmlElement("en_slip_d_goback")]
    public string en_slip_d_goback { get; set; } //หักกลับก่อน
    [XmlElement("en_slip_d_over")]
    public string en_slip_d_over { get; set; } //หักลาเกินสิทธิ์
    [XmlElement("en_slip_d_broken")]
    public string en_slip_d_broken { get; set; } //หักบกพร่อง
    [XmlElement("en_slip_d_aia")]
    public string en_slip_d_aia { get; set; } //หักAIA
    [XmlElement("en_slip_d_uniform")]
    public string en_slip_d_uniform { get; set; } //หักเครื่องแบบ
    [XmlElement("en_slip_d_welfare")]
    public string en_slip_d_welfare { get; set; } //หักสวัสดิการ
    [XmlElement("en_slip_d_damage")]
    public string en_slip_d_damage { get; set; } //ของเสียหาย
    [XmlElement("en_slip_d_other")]
    public string en_slip_d_other { get; set; } //หักอื่นๆ
    [XmlElement("en_slip_d_tax")]
    public string en_slip_d_tax { get; set; } //หักภาษี
    [XmlElement("en_slip_d_fund")]
    public string en_slip_d_fund { get; set; } //หักกองทุน
    [XmlElement("en_slip_d_social")]
    public string en_slip_d_social { get; set; } //หักประกันสังคม
    [XmlElement("en_slip_d_guarantee")]
    public string en_slip_d_guarantee { get; set; } //ค้ำประกัน
    [XmlElement("en_slip_d_loan")]
    public string en_slip_d_loan { get; set; } //หักเงินกู้
    [XmlElement("en_slip_d_legal")]
    public string en_slip_d_legal { get; set; } //หักเงินกรมบังคับคดี
    [XmlElement("en_slip_deduction")]
    public string en_slip_deduction { get; set; } //รวมจ่าย
    [XmlElement("en_slip_net")]
    public string en_slip_net { get; set; } //สุทธิ
}

[Serializable]
public class search_slip_data_detail
{
    [XmlElement("s_u0_idx")]
    public string s_u0_idx { get; set; }

    [XmlElement("s_emp_idx")]
    public string s_emp_idx { get; set; }
    [XmlElement("s_org_idx")]
    public string s_org_idx { get; set; }
    [XmlElement("s_rdept_idx")]
    public string s_rdept_idx { get; set; }
    [XmlElement("s_rsec_idx")]
    public string s_rsec_idx { get; set; }
    [XmlElement("s_rpos_idx")]
    public string s_rpos_idx { get; set; }

    [XmlElement("s_slip_year")]
    public string s_slip_year { get; set; }
    [XmlElement("s_slip_mode")]
    public string s_slip_mode { get; set; }
}

[Serializable]
public class es_paytype_m0_detail
{
    [XmlElement("m0_idx")]
    public int m0_idx { get; set; }
    [XmlElement("type_name")]
    public string type_name { get; set; }
    [XmlElement("m0_status")]
    public int m0_status { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class es_paycde_m0_detail
{
    [XmlElement("m0_idx")]
    public int m0_idx { get; set; }
    [XmlElement("code_no")]
    public string code_no { get; set; }
    [XmlElement("code_name")]
    public string code_name { get; set; }
    [XmlElement("code_length")]
    public int code_length { get; set; }
    [XmlElement("display_length")]
    public int display_length { get; set; }
    [XmlElement("paytype_idx")]
    public int paytype_idx { get; set; }
    [XmlElement("number_flag")]
    public int number_flag { get; set; }
    [XmlElement("number_dec")]
    public int number_dec { get; set; }
    [XmlElement("balance_flag")]
    public int balance_flag { get; set; }
    [XmlElement("balance_dec")]
    public int balance_dec { get; set; }
    [XmlElement("paycode_comment")]
    public string paycode_comment { get; set; }
    [XmlElement("m0_status")]
    public int m0_status { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("paytype_name")]
    public string paytype_name { get; set; }
}