﻿<%@ WebHandler Language="C#" Class="JsonResponseHoliday" %>

using System;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Web.SessionState;

public class JsonResponseHoliday : IHttpHandler, IRequiresSessionState
{

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/json";

        DateTime start = Convert.ToDateTime(context.Request.QueryString["start"]);
        //try { start = Convert.ToDateTime(context.Request.QueryString["start"]); } catch (Exception e) { start = DateTime.Now; }
        DateTime end = Convert.ToDateTime(context.Request.QueryString["end"]);

        int org_idx = int.Parse(context.Session["org_idx"].ToString());

        List<int> idList = new List<int>();
        List<ImproperHolidayCalendarEvent> tasksList = new List<ImproperHolidayCalendarEvent>();

        //Generate JSON serializable events
        foreach (CalendarHolidayEvent cevent in EventHolidayDAO.getEvents(start, end, org_idx))
        {
            tasksList.Add(new ImproperHolidayCalendarEvent
            {
                id = cevent.id,
                title = cevent.title,
                start = String.Format("{0:s}", cevent.start),
                end = String.Format("{0:s}", cevent.start),
                holiday_name = cevent.holiday_name,
                description = cevent.description,
                allDay = cevent.allDay,


            });
            idList.Add(cevent.id);
        }

        context.Session["idList"] = idList;

        //Serialize events to string
        System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        string sJSON = oSerializer.Serialize(tasksList);

        //Write JSON to response object
        context.Response.Write(sJSON);
    }

    public bool IsReusable
    {
        get { return false; }
    }
}