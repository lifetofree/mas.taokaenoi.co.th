﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;

public partial class websystem_en_rpt_plan : System.Web.UI.Page
{
    #region Init
    function_tool _funcTool = new function_tool();
    data_en_planning _data_en_planning = new data_en_planning();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGeten_report = _serviceUrl + ConfigurationManager.AppSettings["urlGeten_report"];
    static string _urlGeten_lookup = _serviceUrl + ConfigurationManager.AppSettings["urlGeten_lookup"];

    string _localJson = "";
    int _tempInt = 0;

    int emp_idx = 0;


    #endregion Init

    #region Constant
    public static class Constants
    {
        public const int SELECT_ALL = 20;
        public const int SELECT_WHERE = 21;
        public const int NUMBER_NULL = -999;
    }
    #endregion Constant

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
        
        if (!IsPostBack)
        {
           
            hddf_empty.Value = "full";
            actionIndex();
            hddf_empty.Value = "";

        }
        getTitle();
    }
    #endregion Page Load
    public void getTitle()
    {
        string sName = "";
        data_en_planning dataenplanning = new data_en_planning();
        //dataenplanning.en_lookup_action = new en_lookup[1];
        //en_lookup obj_lookup = new en_lookup();
        //obj_lookup.operation_status_id = "org_en_plan";
        //dataenplanning.en_lookup_action[0] = obj_lookup;
        //dataenplanning = _func_dmu.zCallServicePostENPlanning(_urlGeten_lookup, dataenplanning);
        dataenplanning = _func_dmu.zENLookupDataList(dataenplanning, "org_en_plan");
        litTitle1.Text = "";
        if (dataenplanning.en_lookup_action != null)
        {
            litTitle1.Text = dataenplanning.en_lookup_action[0].zName;
        }
        litTitle2.Text = "แบบฟอร์มใบบันทึก แผนการทำ PM เครื่องจักรประจำปี " + (_func_dmu.zStringToInt(ddlYearSearch_plan_pm.SelectedValue) + 543).ToString();
        if(ddlbuilding.SelectedValue != "0")
        {
            sName = ddlbuilding.Items[ddlbuilding.SelectedIndex].Text;
        }
        litTitle3.Text = "(Preventive Maintenance Program) เครื่องจักรในกระบวนการผลิต" + sName;

        string sLine = "...............................";
        string sDine_date = "วันที่...../...../.....";

        litapp_t1_1.Text = "ผู้จัดทำ" + sLine;
        litapp_t1_2.Text = "ผู้จัดการแผนกซ่อมบำรุง";
        litapp_t1_3.Text = sDine_date;

        litapp_t2_1.Text = "ผู้ตรวจสอบ" + sLine;
        litapp_t2_2.Text = "ผู้จัดการฝ่ายวิศวกรรม";
        litapp_t2_3.Text = sDine_date;

        litapp_t3_1.Text = "ผู้ตรวจสอบ" + sLine;
        litapp_t3_2.Text = "ผู้อำนวยการฝ่ายผลิต";
        litapp_t3_3.Text = sDine_date;

        litapp_t4_1.Text = "ผู้อนุมัติ" + sLine;
        litapp_t4_2.Text = "ประธานเจ้าหน้าที่ฝ่ายปฎิบัติการ";
        litapp_t4_3.Text = sDine_date;

        dataenplanning = new data_en_planning();
        //dataenplanning.en_lookup_action = new en_lookup[1];
        //obj_lookup = new en_lookup();
        //obj_lookup.operation_status_id = "iso_plan_year";
        //dataenplanning.en_lookup_action[0] = obj_lookup;
        //dataenplanning = _func_dmu.zCallServicePostENPlanning(_urlGeten_lookup, dataenplanning);
        dataenplanning = _func_dmu.zENLookupDataList(dataenplanning, "iso_plan_year");
        litiso.Text = "";
        if (dataenplanning.en_lookup_action != null)
        {
            litiso.Text = dataenplanning.en_lookup_action[0].zName;
        }


    }

    #region Action
    protected void actionIndex()
    {
        zSetMode(0);
    }

    #endregion Action

    #region Check Session Redirect
    protected void checkSessionRedirect()
    {
        //if (Session["sesLoginIdxBackEnd"] == null)
        //{
        //   Response.Redirect(ResolveUrl("~/login"));
        //}
    }
    #endregion Check Session Redirect

    private void Page_Init(object sender, EventArgs e)
    {
        ClearApplicationCache();
    }
    #region reuse
    public void ClearApplicationCache()
    {
        List<string> keys = new List<string>();
        // retrieve application Cache enumerator
        IDictionaryEnumerator enumerator = Cache.GetEnumerator();
        // copy all keys that currently exist in Cache
        while (enumerator.MoveNext())
        {
            keys.Add(enumerator.Key.ToString());
        }

        // delete every key from cache
        for (int i = 0; i < keys.Count; i++)
        {
            Cache.Remove(keys[i]);
        }
    }
    #endregion

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "btnplan_pm":
                zSetMode(0);
                break;
            case "btnreport":
                zSetMode(1);
                break;
            case "btnFilter_plan_pm":
                hddf_empty.Value = "full";
                getHtml_RptPlanPM();
                hddf_empty.Value = "";
                break;
        }
    }
    #endregion btnCommand

    #region zSetMode
    public void zSetMode(int AiMode)
    {

        switch (AiMode)
        {
            case 0:  //แผนการทำ PM เครื่องจักร
                {
                    setActiveTab("plan_pm");
                    _func_dmu.zENDropDownList(ddlYearSearch_plan_pm,
                                "",
                                "zyear",
                                "zyear",
                                "0",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "plan_year"
                                );
                    _func_dmu.zENLocation(ddllocate);
                    _func_dmu.zENtypemachine(ddltypemachine);
                    _func_dmu.zClearDataDropDownList(ddltypecode, "เลือก...");
                    _func_dmu.zClearDataDropDownList(ddlbuilding, "เลือก...");
                    getHtml_RptPlanPM();
                }
                break;
            case 1:  //Report
                {
                    setActiveTab("report");
                }
                break;
            case 2://preview mode
                {


                }
                break;
            case 3://Detail mode
                {


                }
                break;
            case 4:  //insert detail mode
                {


                }
                break;
        }
    }
    #endregion zSetMode

    #region setActiveTab
    protected void setActiveTab(string activeTab)
    {
        liplan_pm.Attributes.Add("class", "");
        lireport.Attributes.Add("class", "");
        switch (activeTab)
        {
            case "plan_pm":
                liplan_pm.Attributes.Add("class", "active");
                break;
            case "report":
                lireport.Attributes.Add("class", "active");
                break;

        }
    }
    #endregion setActiveTab

    public string zMonthEN(int AMonth)
    {

        return _func_dmu.zMonthEN(AMonth);
    }
    public string getHtml_RptPlanPM(string sEmpty = "")
    {
        getTitle();
        /*
        data_en_planning dataenplanning = new data_en_planning();
        dataenplanning.en_report_action = new en_report[1];
        en_report obj_rpt = new en_report();
        obj_rpt.zyear = _func_dmu.zStringToInt(ddlYearSearch_plan_pm.SelectedValue);
        obj_rpt.LocIDX = _func_dmu.zStringToInt(ddllocate.SelectedValue);
        obj_rpt.TmcIDX = _func_dmu.zStringToInt(ddltypemachine.SelectedValue);
        obj_rpt.TCIDX = _func_dmu.zStringToInt(ddltypecode.SelectedValue);
        obj_rpt.BuildingIDX = _func_dmu.zStringToInt(ddlbuilding.SelectedValue);

        obj_rpt.operation_status_id = "plan_year";
        dataenplanning.en_report_action[0] = obj_rpt;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataenplanning));
        dataenplanning = _func_dmu.zCallServicePostENPlanning(_urlGeten_report, dataenplanning);
        string sName = "", sHtml = "", sHtml1 = "", sHtml2 = "", sRow;
        int ic = 0,iMCCode=0;
        string sClone = "", sMCCode="";

        if (dataenplanning.en_report_action != null)
        {
            foreach (var item in dataenplanning.en_report_action)
            {
                sName = item.MCNameTH;
                if (item.zMCCode == sClone)
                {
                    sName = "";
                    sHtml1 = "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" ><center> " + sName + " </center></td>";
                    sHtml1 = sHtml1 + "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" > " + sName + " </td>";
                }
                else
                {
                    ic++;
                    sName = ic.ToString();
                    sHtml1 = "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" ><center> " + sName + " </center></td>";
                    sName = item.MCNameTH;
                    sHtml1 = sHtml1 + "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" > " + sName + " </td>";
                }

                sName = item.MCCode;
                sHtml1 = sHtml1 + "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" > " + sName + " </td>";

                sHtml2 = "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" ><center>  </center></td>";
                sHtml2 = sHtml2 + "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" >  </td>";
                sHtml2 = sHtml2 + "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" >  </td>";
                
                sRow = getu0idx(item.MCIDX, sHtml1, sHtml2);
                sHtml = sHtml + sRow;
                sClone = item.zMCCode;
                iMCCode = item.MCIDX;

            }
            sName = "";
            sHtml1 = "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" ><center> " + sName + " </center></td>";
            sHtml1 = sHtml1 + "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" > " + sName + " </td>";

            sRow = getu0idx(iMCCode, sHtml1, sHtml2);
            sHtml = sHtml + sRow;
        }
        */
        string sHtml = getzMC();
        return sHtml;
    }

    public string getzMC()
    {
       // getTitle();
        data_en_planning dataenplanning = new data_en_planning();
        dataenplanning.en_report_action = new en_report[1];
        en_report obj_rpt = new en_report();
        //obj_trn_plan.filter_keyword = txtFilterKeyword_rpt_SCHED.Text;

        obj_rpt.zyear = _func_dmu.zStringToInt(ddlYearSearch_plan_pm.SelectedValue);
        obj_rpt.LocIDX = _func_dmu.zStringToInt(ddllocate.SelectedValue);
        obj_rpt.TmcIDX = _func_dmu.zStringToInt(ddltypemachine.SelectedValue);
        obj_rpt.TCIDX = _func_dmu.zStringToInt(ddltypecode.SelectedValue);
        obj_rpt.BuildingIDX = _func_dmu.zStringToInt(ddlbuilding.SelectedValue);

        obj_rpt.operation_status_id = "plan_year_mc";
        dataenplanning.en_report_action[0] = obj_rpt;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataenplanning));
        dataenplanning = _func_dmu.zCallServicePostENPlanning(_urlGeten_report, dataenplanning);
        string sMCCode = "";
        string sHtml = "", sRow = "";
        int ic = 0;
        if (dataenplanning.en_report_action != null)
        {
            pnl_alert.Visible = false;
            pnl_print_plan.Visible = true;
            foreach (var item in dataenplanning.en_report_action)
            {
                ic++;
                sRow = getHtml(item.zMCCode, ic, item.zCount);
                sHtml = sHtml + sRow;
            }
        }
        else
        {
            pnl_alert.Visible = true;
            pnl_print_plan.Visible = false;
        }

        return sHtml;
    }
    public string getHtml(string zMCCode,int _icount, int _irowspan)
    {
        data_en_planning dataenplanning = new data_en_planning();
        dataenplanning.en_report_action = new en_report[1];
        en_report obj_rpt = new en_report();
        //obj_trn_plan.filter_keyword = txtFilterKeyword_rpt_SCHED.Text;

        obj_rpt.zyear = _func_dmu.zStringToInt(ddlYearSearch_plan_pm.SelectedValue);
        obj_rpt.LocIDX = _func_dmu.zStringToInt(ddllocate.SelectedValue);
        obj_rpt.TmcIDX = _func_dmu.zStringToInt(ddltypemachine.SelectedValue);
        obj_rpt.TCIDX = _func_dmu.zStringToInt(ddltypecode.SelectedValue);
        obj_rpt.BuildingIDX = _func_dmu.zStringToInt(ddlbuilding.SelectedValue);
        obj_rpt.zMCCode = zMCCode;
        obj_rpt.operation_status_id = "plan_year";
        dataenplanning.en_report_action[0] = obj_rpt;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataenplanning));
        dataenplanning = _func_dmu.zCallServicePostENPlanning(_urlGeten_report, dataenplanning);
        string sName = "", sHtml = "", sHtml1 = "", sHtml2 = "", sRow;
        int ic = 0, iMCCode = 0;
        string sClone = "", sMCCode = "", sMCNameTH="";

        if (dataenplanning.en_report_action != null)
        {
            foreach (var item in dataenplanning.en_report_action)
            {
                ic++;
                sMCNameTH = item.MCNameTH;
                if (ic == 1)
                {
                    sMCCode = item.MCCode;
                }
                else
                {
                    sMCCode = sMCCode + " , "+item.MCCode;
                }
                
                /*
                if (item.zMCCode == sClone)
                {
                    sName = "";
                    sHtml1 = "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" ><center> " + sName + " </center></td>";
                    sHtml1 = sHtml1 + "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" > " + sName + " </td>";
                }
                else
                {
                    ic++;
                    sName = ic.ToString();
                    sHtml1 = "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" ><center> " + sName + " </center></td>";
                    sName = item.MCNameTH;
                    sHtml1 = sHtml1 + "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" > " + sName + " </td>";
                }

                sName = item.MCCode;
                sHtml1 = sHtml1 + "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" > " + sName + " </td>";

                sHtml2 = "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" ><center>  </center></td>";
                sHtml2 = sHtml2 + "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" >  </td>";
                sHtml2 = sHtml2 + "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;\" >  </td>";

                sRow = getu0idx(item.MCIDX, sHtml1, sHtml2);
                sHtml = sHtml + sRow;
                sClone = item.zMCCode;
                */
                iMCCode = item.MCIDX;

            }
            if(_irowspan > 0)
            {
                sHtml1 = "<td rowspan = " + _irowspan.ToString() + " style=\"font-size: 12px;font-family:serif;padding: 1px 0px;border: 1px solid #ddd;\" ><center> " + _icount.ToString() + " </center></td>";
                sHtml1 = sHtml1 + "<td rowspan = " + _irowspan.ToString() + " style=\"font-size: 12px;font-family:serif;padding: 1px 0px;text-align: left;border: 1px solid #ddd;\" > " + sMCNameTH + " </td>";
                sHtml1 = sHtml1 + "<td rowspan = " + _irowspan.ToString() + " style=\"font-size: 11px;font-family:serif;padding: 1px 0px;text-align: left;border: 1px solid #ddd;\" > " + sMCCode + " </td>";

            }
            else
            {
                sHtml1 = "<td style=\"font-size: 12px;font-family:serif;padding: 1px 0px;border: 1px solid #ddd;\" ><center> " + _icount.ToString() + " </center></td>";
                sHtml1 = sHtml1 + "<td style=\"font-size: 12px;font-family:serif;padding: 1px 0px;text-align: left;border: 1px solid #ddd;\" > " + sMCNameTH + " </td>";
                sHtml1 = sHtml1 + "<td style=\"font-size: 11px;font-family:serif;padding: 1px 0px;text-align: left;border: 1px solid #ddd;\" > " + sMCCode + " </td>";

            }
            sHtml2 = "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;border: 1px solid #ddd;\" ><center>  </center></td>";
            sHtml2 = sHtml2 + "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;border: 1px solid #ddd;\" >  </td>";
            sHtml2 = sHtml2 + "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;border: 1px solid #ddd;\" >  </td>";

            sRow = getu0idx(iMCCode, sHtml1, sHtml2, _irowspan);
            sHtml = sHtml + sRow;
        }

        return sHtml;
    }
    public string getCollum(int id, int iMCIDX, string sName)
    {
        StringBuilder _StringBuilder = new StringBuilder();
        _StringBuilder.Clear();

        data_en_planning dataenplanning = new data_en_planning();
        dataenplanning.en_report_action = new en_report[1];
        en_report obj_rpt = new en_report();
        obj_rpt.zyear = _func_dmu.zStringToInt(ddlYearSearch_plan_pm.SelectedValue);
        obj_rpt.LocIDX = _func_dmu.zStringToInt(ddllocate.SelectedValue);
        obj_rpt.TmcIDX = _func_dmu.zStringToInt(ddltypemachine.SelectedValue);
        obj_rpt.TCIDX = _func_dmu.zStringToInt(ddltypecode.SelectedValue);
        obj_rpt.BuildingIDX = _func_dmu.zStringToInt(ddlbuilding.SelectedValue);
        obj_rpt.idx = id;
        obj_rpt.MCIDX = iMCIDX;
        obj_rpt.operation_status_id = "plan_year_detail";
        dataenplanning.en_report_action[0] = obj_rpt;
       // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataenplanning));
        dataenplanning = _func_dmu.zCallServicePostENPlanning(_urlGeten_report, dataenplanning);
        //dataenplanning.en_report_action != null
        if (dataenplanning.en_report_action != null)
        {
            /*
            int i1 = 0, i2 = 0, i3 = 0, i4 = 0, i5 = 0
                , i6 = 0, i7 = 0, i8 = 0, i9 = 0, i10 = 0
                , i11 = 0, i12 = 0, i13 = 0, i14 = 0, i15 = 0
                , i16 = 0, i17 = 0, i18 = 0, i19 = 0, i20 = 0
                , i21 = 0, i22 = 0, i23 = 0, i24 = 0, i25 = 0
                , i26 = 0, i27 = 0, i28 = 0, i29 = 0, i30 = 0
                , i31 = 0, i32 = 0, i33 = 0, i34 = 0, i35 = 0
                , i36 = 0, i37 = 0, i38 = 0, i39 = 0, i40 = 0
                , i41 = 0, i42 = 0, i43 = 0, i44 = 0, i45 = 0
                , i46 = 0, i47 = 0, i48 = 0;
            */
            string January_1 = "", January_2 = "", January_3 = "", January_4 = ""; //1
            string February_1 = "", February_2 = "", February_3 = "", February_4 = "";//2
            string March_1 = "", March_2 = "", March_3 = "", March_4 = "";//3
            string April_1 = "", April_2 = "", April_3 = "", April_4 = "";//4
            string May_1 = "", May_2 = "", May_3 = "", May_4 = "";//5
            string June_1 = "", June_2 = "", June_3 = "", June_4 = "";//6
            string July_1 = "", July_2 = "", July_3 = "", July_4 = "";//7
            string August_1 = "", August_2 = "", August_3 = "", August_4 = "";//8
            string September_1 = "", September_2 = "", September_3 = "", September_4 = "";//9
            string October_1 = "", October_2 = "", October_3 = "", October_4 = "";//10
            string November_1 = "", November_2 = "", November_3 = "", November_4 = "";//11
            string December_1 = "", December_2 = "", December_3 = "", December_4 = "";//12

            foreach (var item in dataenplanning.en_report_action)
            {
                string sField = item.zmonth.ToString() + "_" + item.zweek.ToString();
                string sData = sName;// item.zName;
                if (sField == "1_1")//1
                {
                    January_1 = sData;
                }
                else if (sField == "1_2")
                {
                    January_2 = sData;
                }
                else if (sField == "1_3")
                {
                    January_3 = sData;
                }
                else if (sField == "1_4")
                {
                    January_4 = sData;
                }//2
                else if (sField == "2_1")
                {
                    February_1 = sData;
                }
                else if (sField == "2_2")
                {
                    February_2 = sData;
                }
                else if (sField == "2_3")
                {
                    February_3 = sData;
                }
                else if (sField == "2_4")
                {
                    February_4 = sData;
                }//3
                else if (sField == "3_1")
                {
                    March_1 = sData;
                }
                else if (sField == "3_2")
                {
                    March_2 = sData;
                }
                else if (sField == "3_3")
                {
                    March_3 = sData;
                }
                else if (sField == "3_4")
                {
                    March_4 = sData;
                }//4
                else if (sField == "4_1")
                {
                    April_1 = sData;
                }
                else if (sField == "4_2")
                {
                    April_2 = sData;
                }
                else if (sField == "4_3")
                {
                    April_3 = sData;
                }
                else if (sField == "4_4")
                {
                    April_4 = sData;
                }//5
                else if (sField == "5_1")
                {
                    May_1 = sData;
                }
                else if (sField == "5_2")
                {
                    May_2 = sData;
                }
                else if (sField == "5_3")
                {
                    May_3 = sData;
                }
                else if (sField == "5_4")
                {
                    May_4 = sData;
                }//6
                else if (sField == "6_1")
                {
                    June_1 = sData;
                }
                else if (sField == "6_2")
                {
                    June_2 = sData;
                }
                else if (sField == "6_3")
                {
                    June_3 = sData;
                }
                else if (sField == "6_4")
                {
                    June_4 = sData;
                }//7
                else if (sField == "7_1")
                {
                    July_1 = sData;
                }
                else if (sField == "7_2")
                {
                    July_2 = sData;
                }
                else if (sField == "7_3")
                {
                    July_3 = sData;
                }
                else if (sField == "7_4")
                {
                    July_4 = sData;
                }//8
                else if (sField == "8_1")
                {
                    August_1 = sData;
                }
                else if (sField == "8_2")
                {
                    August_2 = sData;
                }
                else if (sField == "8_3")
                {
                    August_3 = sData;
                }
                else if (sField == "8_4")
                {
                    August_4 = sData;
                }//9
                else if (sField == "9_1")
                {
                    September_1 = sData;
                }
                else if (sField == "9_2")
                {
                    September_2 = sData;
                }
                else if (sField == "9_3")
                {
                    September_3 = sData;
                }
                else if (sField == "9_4")
                {
                    September_4 = sData;
                }//10
                else if (sField == "10_1")
                {
                    October_1 = sData;
                }
                else if (sField == "10_2")
                {
                    October_2 = sData;
                }
                else if (sField == "10_3")
                {
                    October_3 = sData;
                }
                else if (sField == "10_4")
                {
                    October_4 = sData;
                }//11
                else if (sField == "11_1")
                {
                    November_1 = sData;
                }
                else if (sField == "11_2")
                {
                    November_2 = sData;
                }
                else if (sField == "11_3")
                {
                    November_3 = sData;
                }
                else if (sField == "11_4")
                {
                    November_4 = sData;
                }//12
                else if (sField == "12_1")
                {
                    December_1 = sData;
                }
                else if (sField == "12_2")
                {
                    December_2 = sData;
                }
                else if (sField == "12_3")
                {
                    December_3 = sData;
                }
                else if (sField == "12_4")
                {
                    December_4 = sData;
                }
            }
            int imonth = 0, icount = 0;
            string sDataWeek = "", sDataWeekAll = "";
            for (int i = 1; i <= 48; i++)
            {
                icount++;
                if (icount == 1)
                {
                    imonth++;
                }
                if (imonth == 1)
                {
                    sDataWeek = getWeek(icount, January_1, January_2, January_3, January_4);
                }
                else if (imonth == 2)
                {
                    sDataWeek = getWeek(icount, February_1, February_2, February_3, February_4);
                }
                else if (imonth == 3)
                {
                    sDataWeek = getWeek(icount, March_1, March_2, March_3, March_4);
                }
                else if (imonth == 4)
                {
                    sDataWeek = getWeek(icount, April_1, April_2, April_3, April_4);
                }
                else if (imonth == 5)
                {
                    sDataWeek = getWeek(icount, May_1, May_2, May_3, May_4);
                }
                else if (imonth == 6)
                {
                    sDataWeek = getWeek(icount, June_1, June_2, June_3, June_4);
                }
                else if (imonth == 7)
                {
                    sDataWeek = getWeek(icount, July_1, July_2, July_3, July_4);
                }
                else if (imonth == 8)
                {
                    sDataWeek = getWeek(icount, August_1, August_2, August_3, August_4);
                }
                else if (imonth == 9)
                {
                    sDataWeek = getWeek(icount, September_1, September_2, September_3, September_4);
                }
                else if (imonth == 10)
                {
                    sDataWeek = getWeek(icount, October_1, October_2, October_3, October_4);
                }
                else if (imonth == 11)
                {
                    sDataWeek = getWeek(icount, November_1, November_2, November_3, November_4);
                }
                else if (imonth == 12)
                {
                    sDataWeek = getWeek(icount, December_1, December_2, December_3, December_4);
                }
                _StringBuilder.AppendLine(sDataWeek);
                if (icount == 4)
                {
                    icount = 0;
                }
            }



        }
        else
        {
            for (int i = 1; i <= 48; i++)
            {
                _StringBuilder.AppendLine("<td style=\"font-family:serif;padding: 0px 0px;border: 1px solid #ddd;\" > ");
                _StringBuilder.AppendLine("<center>x </center>");
                _StringBuilder.AppendLine("</td>");
            }
        }

        return _StringBuilder.ToString();
    }

    public string getWeek(int iWeek, string Week_1, string Week_2, string Week_3, string Week_4)
    {
        string sData = "";
        if (iWeek == 1)
        {
            sData = Week_1;
        }
        else if (iWeek == 2)
        {
            sData = Week_2;
        }
        else if (iWeek == 3)
        {
            sData = Week_3;
        }
        else if (iWeek == 4)
        {
            sData = Week_4;
        }

        StringBuilder _StringBuilder = new StringBuilder();
        _StringBuilder.Clear();
        _StringBuilder.AppendLine("<td style=\"font-size: 11px;font-family:serif;padding: 5px 0px;border: 1px solid #ddd;\" > ");
        _StringBuilder.AppendLine("<center> " + sData + " </center>");
        _StringBuilder.AppendLine("</td>");

        return _StringBuilder.ToString();
    }
    public string getu0idx(int iMCIDX, string sHTML1, string sHTML2,int _irowspan)
    {
        StringBuilder _StringBuilder = new StringBuilder();
        _StringBuilder.Clear();

        data_en_planning dataenplanning = new data_en_planning();
        dataenplanning.en_report_action = new en_report[1];
        en_report obj_rpt = new en_report();
        obj_rpt.zyear = _func_dmu.zStringToInt(ddlYearSearch_plan_pm.SelectedValue);
        obj_rpt.LocIDX = _func_dmu.zStringToInt(ddllocate.SelectedValue);
        obj_rpt.TmcIDX = _func_dmu.zStringToInt(ddltypemachine.SelectedValue);
        obj_rpt.TCIDX = _func_dmu.zStringToInt(ddltypecode.SelectedValue);
        obj_rpt.BuildingIDX = _func_dmu.zStringToInt(ddlbuilding.SelectedValue);
        obj_rpt.MCIDX = iMCIDX;
        obj_rpt.operation_status_id = "plan_year_u0idx";
        dataenplanning.en_report_action[0] = obj_rpt;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataenplanning));
        dataenplanning = _func_dmu.zCallServicePostENPlanning(_urlGeten_report, dataenplanning);
        int ic = 0;
        if (dataenplanning.en_report_action != null)
        {
            foreach (var item in dataenplanning.en_report_action)
            {
                string _sql;
                _sql = getCollum(item.idx, iMCIDX, item.zName);
                ic++;
                if (ic == 1)
                {
                    _StringBuilder.AppendLine("<tr>" + sHTML1 + _sql + "</tr>");
                }
                else
                {
                    if (_irowspan > 0)
                    {
                        _StringBuilder.AppendLine("<tr>"  + _sql + "</tr>");
                    }
                    else
                    {
                        _StringBuilder.AppendLine("<tr>" + sHTML2 + _sql + "</tr>");
                    }
                   // _StringBuilder.AppendLine("<tr>" + sHTML2 + _sql + "</tr>");
                }
            }
        }
        else
        {
            _StringBuilder.AppendLine("<tr>");
            _StringBuilder.AppendLine(sHTML1);
            for (int i = 1; i <= 48; i++)
            {

                _StringBuilder.AppendLine("<td style=\"font-family:serif;padding: 0px 0px;border: 1px solid #ddd;\" > ");
                _StringBuilder.AppendLine("<center> </center>");
                _StringBuilder.AppendLine("</td>");
            }
            _StringBuilder.AppendLine("</tr>");
        }

        return _StringBuilder.ToString();
    }
    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        hddf_empty.Value = "";
        var ddName = (DropDownList)sender;
        switch (ddName.ID)
        {
            case "ddltypemachine":

                _func_dmu.zENtypecodemachine(ddltypecode, int.Parse(ddltypemachine.SelectedValue));

                break;
            case "ddllocate":
                _func_dmu.zENBuilding(ddlbuilding, int.Parse(ddllocate.SelectedValue));

                break;

        }

    }

    #endregion
}