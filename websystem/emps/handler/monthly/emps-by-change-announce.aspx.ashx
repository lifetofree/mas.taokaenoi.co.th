﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

public class Handler : IHttpHandler
{

   public void ProcessRequest(HttpContext context)
   {
      string callback = context.Request.QueryString["callback"];
      int changeAnnounceIdx = 0;
      bool subString = true;
      int.TryParse(context.Request.QueryString["changeAnnounceIdx"], out changeAnnounceIdx);
      bool.TryParse(context.Request.QueryString["subString"], out subString);
      string json = this.getEvents(changeAnnounceIdx, subString);
      if (!string.IsNullOrEmpty(callback))
      {
         json = string.Format("{0}({1});", callback, json);
      }
      context.Response.ContentType = "text/json";
      context.Response.Write(json);
   }

   private string getEvents(int changeAnnounceIdx, bool subString = false)
   {
      List<object> emps = new List<object>();
      using (SqlConnection conn = new SqlConnection())
      {
         conn.ConnectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;
         using (SqlCommand cmd = new SqlCommand())
         {
            cmd.CommandText = "SELECT * FROM view_emps_u0_change_announce_parttime WHERE u0_change_announce_idx = @changeAnnounceIdx";
            cmd.Parameters.AddWithValue("@changeAnnounceIdx", changeAnnounceIdx);
            cmd.Connection = conn;
            conn.Open();
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
               var textHexColor = String.Empty;
               var parttimeCode = String.Empty;
               while (sdr.Read())
               {
                  System.Drawing.Color col = System.Drawing.ColorTranslator.FromHtml(sdr["parttime_bg_color"].ToString());
                  if (((col.R * 0.2126) + (col.G * 0.7152) + (col.B * 0.0722)) > (255 / 2))
                  {
                     textHexColor = "#000000";
                  }
                  else
                  {
                     textHexColor = "#ffffff";
                  }
                  if (sdr["parttime_code"].ToString().Length > 7 && subString == true)
                  {
                     parttimeCode = sdr["parttime_code"].ToString().Substring(0, 7) + "..";
                  }
                  else
                  {
                     parttimeCode = sdr["parttime_code"].ToString();
                  }
                  emps.Add(new
                  {
                     parttimeIdx = sdr["m0_parttime_idx"],
                     title = parttimeCode,
                     start = String.Format("{0:yyyy-MM-dd}", sdr["u1_change_announce_date"]),
                     color = sdr["parttime_bg_color"],
                     textColor = textHexColor,
                     allDay = true,
                     changeAnnounceStartDate = String.Format("{0:yyyy-MM-dd}", sdr["change_announce_date_start"]),
                     changeAnnounceEndDate = String.Format("{0:yyyy-MM-dd}", sdr["change_announce_date_end"])
                  });
               }
            }
            conn.Close();
         }
         return (new JavaScriptSerializer().Serialize(emps));
      }
   }

   public bool IsReusable
   {
      get
      {
         return false;
      }
   }
}


