﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_rcm_question")]
public class data_rcm_question
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }
    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }
    [XmlElement("ReturnEmpIDX")]
    public string ReturnEmpIDX { get; set; }
    [XmlElement("ReturnCount")]
    public string ReturnCount { get; set; }

    [XmlElement("Boxm0_typequest")]
    public m0_typequest[] Boxm0_typequest { get; set; }

    [XmlElement("Boxm0_topicquest")]
    public m0_topicquest[] Boxm0_topicquest { get; set; }

    [XmlElement("Boxm0_typeanswer")]
    public m0_typeanswer[] Boxm0_typeanswer { get; set; }

    [XmlElement("Boxm0_question")]
    public m0_question[] Boxm0_question { get; set; }

    [XmlElement("Boxu0_answer")]
    public u0_answer[] Boxu0_answer { get; set; }

    [XmlElement("Boxu1_answer")]
    public u1_answer[] Boxu1_answer { get; set; }

    [XmlElement("BoxSendEmailQuiz")]
    public sendemailquiz[] BoxSendEmailQuiz { get; set; }

    [XmlElement("Boxl0_logquestion")]
    public l0_answer[] Boxl0_logquestion { get; set; }

    [XmlElement("Boxm1_personlist")]
    public m1_persondetail[] Boxm1_personlist { get; set; }

}


[Serializable]
public class m0_typequest
{
    [XmlElement("m0_tqidx")]
    public int m0_tqidx { get; set; }

    [XmlElement("type_quest")]
    public string type_quest { get; set; }

}

[Serializable]
public class m0_typeanswer
{
    [XmlElement("m0_taidx")]
    public int m0_taidx { get; set; }

    [XmlElement("type_asn")]
    public string type_asn { get; set; }

    [XmlElement("m0_toidx")]
    public int m0_toidx { get; set; }

    [XmlElement("topic_name")]
    public string topic_name { get; set; }


}

[Serializable]
public class m0_topicquest
{
    [XmlElement("m0_toidx")]
    public int m0_toidx { get; set; }

    [XmlElement("m0_tqidx")]
    public int m0_tqidx { get; set; }

    [XmlElement("orgidx")]
    public int orgidx { get; set; }

    [XmlElement("rdeptidx")]
    public int rdeptidx { get; set; }

    [XmlElement("rsecidx")]
    public int rsecidx { get; set; }

    [XmlElement("rposidx")]
    public int rposidx { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("topic_name")]
    public string topic_name { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("PosNameTH")]
    public string PosNameTH { get; set; }

    [XmlElement("m0_taidx")]
    public int m0_taidx { get; set; }

    [XmlElement("no_choice")]
    public int no_choice { get; set; }

    [XmlElement("type_quest")]
    public string type_quest { get; set; }

    [XmlElement("total_score")]
    public string total_score { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("P_Name")]
    public string P_Name { get; set; }

    [XmlElement("P_Description")]
    public string P_Description { get; set; }

    [XmlElement("P_Location")]
    public string P_Location { get; set; }

    [XmlElement("score_choice")]
    public int score_choice { get; set; }

    [XmlElement("score_comment")]
    public string score_comment { get; set; }

    [XmlElement("PIDX")]
    public int PIDX { get; set; }

    [XmlElement("StatusDoc")]
    public string StatusDoc { get; set; }

    [XmlElement("datedoing")]
    public string datedoing { get; set; }

    [XmlElement("u0_anidx")]
    public int u0_anidx { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("P_m0_toidx")]
    public string P_m0_toidx { get; set; }

    [XmlElement("identity_card")]
    public string identity_card { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("DateStart")]
    public string DateStart { get; set; }

    [XmlElement("DateEnd")]
    public string DateEnd { get; set; }

    [XmlElement("IFSearchbetween")]
    public int IFSearchbetween { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("node_decision")]
    public int node_decision { get; set; }

    [XmlElement("determine")]
    public string determine { get; set; }

    [XmlElement("statusinterview")]
    public string statusinterview { get; set; }


}

[Serializable]
public class m0_question
{
    [XmlElement("m0_quidx")]
    public int m0_quidx { get; set; }

    [XmlElement("m0_toidx")]
    public int m0_toidx { get; set; }

    [XmlElement("m0_taidx")]
    public int m0_taidx { get; set; }

    [XmlElement("m0_tqidx")]
    public int m0_tqidx { get; set; }

    [XmlElement("no_choice")]
    public int no_choice { get; set; }

    [XmlElement("quest_name")]
    public string quest_name { get; set; }

    [XmlElement("pic_quest")]
    public int pic_quest { get; set; }

    [XmlElement("choice_a")]
    public string choice_a { get; set; }

    [XmlElement("pic_a")]
    public int pic_a { get; set; }

    [XmlElement("choice_b")]
    public string choice_b { get; set; }

    [XmlElement("pic_b")]
    public int pic_b { get; set; }

    [XmlElement("choice_c")]
    public string choice_c { get; set; }

    [XmlElement("pic_c")]
    public int pic_c { get; set; }

    [XmlElement("choice_d")]
    public string choice_d { get; set; }

    [XmlElement("pic_d")]
    public int pic_d { get; set; }

    [XmlElement("choice_ans")]
    public int choice_ans { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("answer")]
    public string answer { get; set; }

    [XmlElement("u0_anidx")]
    public int u0_anidx { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("u1_anidx")]
    public int u1_anidx { get; set; }

    [XmlElement("score")]
    public string score { get; set; }

    [XmlElement("node_decision")]
    public int node_decision { get; set; }

    [XmlElement("determine")]
    public string determine { get; set; }

}

[Serializable]
public class u0_answer
{
    [XmlElement("m0_toidx")]
    public int m0_toidx { get; set; }

    [XmlElement("m0_taidx")]
    public int m0_taidx { get; set; }

    [XmlElement("m0_tqidx")]
    public int m0_tqidx { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("P_m0_toidx")]
    public string P_m0_toidx { get; set; }

    [XmlElement("PIDX")]
    public int PIDX { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("u0_anidx")]
    public int u0_anidx { get; set; }

    [XmlElement("determine")]
    public string determine { get; set; }

}

[Serializable]
public class u1_answer
{
    [XmlElement("m0_quidx")]
    public int m0_quidx { get; set; }

    [XmlElement("answer")]
    public string answer { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("m0_toidx")]
    public int m0_toidx { get; set; }

    [XmlElement("score")]
    public string score { get; set; }

    [XmlElement("u1_anidx")]
    public int u1_anidx { get; set; }

}

[Serializable]
public class sendemailquiz
{
    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("P_Name")]
    public string P_Name { get; set; }

    [XmlElement("datedoing")]
    public string datedoing { get; set; }

    [XmlElement("JName")]
    public string JName { get; set; }

    [XmlElement("P_Location")]
    public string P_Location { get; set; }

    [XmlElement("P_Description")]
    public string P_Description { get; set; }

    [XmlElement("StatusDoc")]
    public string StatusDoc { get; set; }

    [XmlElement("total_score")]
    public string total_score { get; set; }

    [XmlElement("score_choice")]
    public string score_choice { get; set; }

    [XmlElement("score_comment")]
    public string score_comment { get; set; }

    [XmlElement("topic_name")]
    public string topic_name { get; set; }


}

[Serializable]
public class l0_answer
{
    [XmlElement("u0_anidx")]
    public int u0_anidx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

}

[Serializable]
public class m1_persondetail
{
    [XmlElement("topic_name_person")]
    public string topic_name_person { get; set; }

    [XmlElement("P_Name")]
    public string P_Name { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("PIDX")]
    public int PIDX { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("m0_toidx")]
    public int m0_toidx { get; set; }

    [XmlElement("P_EmpIDX")]
    public int P_EmpIDX { get; set; }

    [XmlElement("TPIDX")]
    public int TPIDX { get; set; }

    [XmlElement("P_Description")]
    public string P_Description { get; set; }

    [XmlElement("P_Location")]
    public string P_Location { get; set; }

    [XmlElement("topic_name")]
    public string topic_name { get; set; } 
}