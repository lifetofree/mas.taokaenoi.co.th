﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="el_Trn_plan_course_qrcode.aspx.cs" Inherits="websystem_el_Trn_plan_course_qrcode" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>


    <link href="../../../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/emps-contents/bootstrap-datetimepicker.min.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/custom.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/emps-contents/fa/css/font-awesome.min.css" runat="server" rel="stylesheet" />
    <link href="./../Content/emps-contents/empshift-style-general.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/emps-contents/bootstrap-clockpicker.min.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/emps-contents/colorpicker/css/bootstrap-colorpicker.min.css" runat="server" rel="stylesheet" />

    <link href="../../../Content/jquery-ui-1.12.1/jquery-ui.min.css" runat="server" rel="stylesheet" />
    <link href="../../../Content/fullcalendar.css" rel="stylesheet" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/qtip2/3.0.3/jquery.qtip.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous" />


</head>
<body>
    <form id="form1" runat="server">
        <asp:Literal ID="Litdebug" runat="server" Text=""></asp:Literal>
        <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>

        <div style="height: auto; width: 100%; margin: 0 0; display: block; border: 3px solid #D9B4B4; font-family: 'MS Reference Sans Serif';">
            
            <header style="height:270px;">
                <div runat="server" id="dv_header">
                    <%-- start header --%>
                </div>
                
            </header>

            
            <div class="row">
                <asp:Panel ID="Panel_course" runat="server">
                    <%--<div class="alert-message alert-info" runat="server" visible="true">
                        <blockquote class="danger" style="background-color: darkturquoise; color: black;">
                    --%>
                    <div class="row h3" style="margin-top: 0px;">
                        <div class="form-group">
                            <asp:Label ID="lbcourse" class="col-md-offset-2 col-md-2 control-labelnotop text_right" runat="server" Text="คอร์สอบรม :" />
                            <div class="col-md-8">
                                <asp:Literal ID="litcourse" runat="server"></asp:Literal>
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="lblecturer_type" class="col-md-offset-2 col-md-2 control-labelnotop text_right" runat="server" Text="วิทยากร :" />
                            <div class="col-md-8">
                                <asp:GridView ID="Gvinstitution"
                                    CssClass="col-md-12 word-wrap"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    HeaderStyle-CssClass="default"
                                    ShowFooter="false"
                                    AllowPaging="false"
                                    ShowHeader="false"
                                    GridLines="None">

                                    <Columns>

                                        <asp:TemplateField
                                            HeaderText="ชื่อ - สกุล"
                                            ItemStyle-HorizontalAlign="Left"
                                            HeaderStyle-CssClass="text-center">
                                            <EditItemTemplate>
                                                <asp:TextBox ID="txtinstitution_name_edit_L" runat="server" Text='<%# Bind("zName") %>'></asp:TextBox>
                                            </EditItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="txtinstitution_name_item_L" runat="server" Text='<%# Eval("zName") %>'
                                                    Width="100%"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                    <%-- </blockquote>
                    </div>--%>
                </asp:Panel>
            </div>


            <div class="col-md-12">

                <asp:Panel ID="Panel1" runat="server" Height="550px">

                    <asp:GridView ID="GvListData"
                        CssClass="table col-md-12 word-wrap"
                        runat="server"
                        AutoGenerateColumns="false"
                        HeaderStyle-CssClass="default"
                        ShowFooter="false"
                        AllowPaging="false"
                        ShowHeader="false"
                        GridLines="None">

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText=""
                                ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center">
                                <ItemTemplate>

                                    <div class="col-md-6" runat="server" visible='<%# getImgVisible((string)Eval("qrcode_evaluationform"),1) %>' id="pnlevaluationform">
                                        <div class="panel panel-primary center-block" style="height: auto; width: 70%;">
                                            <div class="panel-body">
                                                <div class="row h4" style="text-align: center;">
                                                    แบบฟอร์มประเมินผลความพึงพอใจหลังการฝึกอบรม
                                                </div>
                                                <div class="row" style="text-align: center;">
                                                    <img src='<%# ResolveUrl(getImgUrl((string)Eval("qrcode_evaluationform"),(string)Eval("training_course_no"))) %>'
                                                        style="width: 70%; height: 70%" />
                                                </div>
                                                <div class="row" style="text-align: center;">
                                                    <asp:HyperLink ID="btnqrcode1" runat="server"
                                                        CssClass="container" data-original-title="Linke Url"
                                                        data-toggle="tooltip" Text="Linke Url"
                                                        Target="_blank"
                                                        NavigateUrl='<%# getUrlevaluationform((int)Eval("u0_training_course_idx")) %>'
                                                        Style="background-color: darkgreen; border: none; color: white; text-align: center; text-decoration: none; display: inline-block; padding: 5px 5px; font-size: 14px; width: 70%; cursor: pointer;"><i class="fa fa-link"></i>&nbsp;Linke Url</asp:HyperLink>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6" runat="server" visible='<%# getImgVisible((string)Eval("qrcode_test"),(int)Eval("open_flag")) %>' id="pnltest">
                                        <div class="panel panel-primary center-block" style="height: auto; width: 70%;">
                                            <div class="panel-body">
                                                <div class="row h4" style="text-align: center;">
                                                    แบบฟอร์มทดสอบหลังการฝึกอบรม
                                                </div>
                                                <div class="row" style="text-align: center;">
                                                    <img src='<%# ResolveUrl(getImgUrl((string)Eval("qrcode_test"),(string)Eval("training_course_no"))) %>'
                                                        style="width: 70%; height: 70%" />
                                                </div>
                                                <div class="row" style="text-align: center;">
                                                    <asp:HyperLink ID="LinkButton1" runat="server"
                                                        CssClass="container" data-original-title="Linke Url"
                                                        data-toggle="tooltip" Text="Linke Url"
                                                        Target="_blank"
                                                        NavigateUrl='<%# getUrlTest((int)Eval("u0_training_course_idx")) %>'
                                                        Style="background-color: darkgreen; border: none; color: white; text-align: center; text-decoration: none; display: inline-block; padding: 5px 5px; font-size: 14px; width: 70%; cursor: pointer;"><i class="fa fa-link"></i>&nbsp;Linke Url</asp:HyperLink>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </asp:Panel>
            </div>

            <footer>
                <div runat="server" id="dv_footer">
                    <div class="row">
                        <%-- <asp:Panel ID="Panel2" runat="server" Height="350px"></asp:Panel>--%>
                    </div>
                </div>
            </footer>
        </div>

    </form>
</body>
</html>
