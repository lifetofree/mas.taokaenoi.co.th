﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_hr_permission : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    service_execute _serviceExecute = new service_execute();
    data_permission_set _dataPermission = new data_permission_set();

    string _localXml = String.Empty;
    string _localString = String.Empty;
    string _localJson = String.Empty;
    int _actionType = 0;
    int _emp_idx = 0;
    int _default_int = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetCostcenterOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetCostcenterOld"];
    static string _urlGetInsertCostcenter = _serviceUrl + ConfigurationManager.AppSettings["urlGetInsertCostcenter"];
    static string _urlGetUpdateCostcenter = _serviceUrl + ConfigurationManager.AppSettings["urlGetUpdateCostcenter"];
    static string _urlGetDeleteCostcenter = _serviceUrl + ConfigurationManager.AppSettings["urlGetDeleteCostcenter"];

    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];

    static string _urlGetDetail_Permssion = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetail_Permssion"];
    static string _urlSetDetail_Permssion_Insert = _serviceUrl + ConfigurationManager.AppSettings["urlSetDetail_Permssion_Insert"];
    static string _urlGetDetail_Permssion_delete = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetail_Permssion_delete"];
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = Session["emp_idx"].ToString();
            MvMaster.SetActiveView(ViewIndex);

            select_org(ddlorg_search);
            SelectMasterList(int.Parse(ddlorg_search.SelectedValue),int.Parse(ddldep_search.SelectedValue),int.Parse(ddlsec_search.SelectedValue),int.Parse(ddlpos_search.SelectedValue),int.Parse(ddlsystem_search.SelectedValue)
                ,int.Parse(ddlpermission_search.SelectedValue));
            defult_Master_List();
        }

    }
    #endregion

    #region INSERT&SELECT&UPDATE

    protected void defult_Master_List()
    {
        //var chkposition_edit = (CheckBox)FvEdit_Detail.FindControl("chkposition_edit");
        //var GvPosAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvPosAdd_Edit");
        //var BoxPosition_edit = (Panel)FvEdit_Detail.FindControl("BoxPosition_edit");

        ViewState["vsTemp_PosAdd"] = null;

        var dsPosadd = new DataSet();
        dsPosadd.Tables.Add("PosAdd_Add");

        dsPosadd.Tables[0].Columns.Add("OrgIDX_S", typeof(int));
        dsPosadd.Tables[0].Columns.Add("OrgNameTH_S", typeof(String));
        dsPosadd.Tables[0].Columns.Add("RDeptIDX_S", typeof(int));
        dsPosadd.Tables[0].Columns.Add("DeptNameTH_S", typeof(String));
        dsPosadd.Tables[0].Columns.Add("RSecIDX_S", typeof(int));
        dsPosadd.Tables[0].Columns.Add("SecNameTH_S", typeof(String));
        dsPosadd.Tables[0].Columns.Add("RPosIDX_S", typeof(int));
        dsPosadd.Tables[0].Columns.Add("PosNameTH_S", typeof(String));

        dsPosadd.Tables[0].Columns.Add("p_system_s", typeof(int));
        dsPosadd.Tables[0].Columns.Add("p_system_name_s", typeof(String));
        dsPosadd.Tables[0].Columns.Add("p_permission_type_s", typeof(int));
        dsPosadd.Tables[0].Columns.Add("p_permission_type_name_s", typeof(String));

        ViewState["vsTemp_PosAdd"] = dsPosadd;
        GvPosAdd_Add.DataSource = dsPosadd.Tables[0];
        GvPosAdd_Add.DataBind();

        //BoxPosition_add.Visible = true;
        /* ---------------------------------------------------------------------------- */

        ViewState["vsTemp_SecAdd"] = null;

        var dsSecadd = new DataSet();
        dsSecadd.Tables.Add("SecAdd_Add");

        dsSecadd.Tables[0].Columns.Add("OrgIDX_Se", typeof(int));
        dsSecadd.Tables[0].Columns.Add("OrgNameTH_Se", typeof(String));
        dsSecadd.Tables[0].Columns.Add("RDeptIDX_Se", typeof(int));
        dsSecadd.Tables[0].Columns.Add("DeptNameTH_Se", typeof(String));
        dsSecadd.Tables[0].Columns.Add("RSecIDX_Se", typeof(int));
        dsSecadd.Tables[0].Columns.Add("SecNameTH_Se", typeof(String));
        //dsSecadd.Tables[0].Columns.Add("RPosIDX_S", typeof(int));
        //dsSecadd.Tables[0].Columns.Add("PosNameTH_S", typeof(String));

        ViewState["vsTemp_SecAdd"] = dsSecadd;
        GvSecAdd_Permis.DataSource = dsSecadd.Tables[0];
        GvSecAdd_Permis.DataBind();

    }

    protected void Insert_Status()
    {
        int e = 0;
        int a = 0;
        if (ViewState["vsTemp_PosAdd"] != null)
        {
            var dsPos = (DataSet)ViewState["vsTemp_PosAdd"];
            var AddPos = new permission_set_detail[dsPos.Tables[0].Rows.Count];
            foreach (DataRow dr in dsPos.Tables[0].Rows)
            {
                AddPos[e] = new permission_set_detail();
                //AddPos[e].emp_idx_set = dr["OrgIDX_S"].ToString();
                AddPos[e].rpos_idx_set = dr["RPosIDX_S"].ToString();
                AddPos[e].p_system = int.Parse(dr["p_system_s"].ToString());
                AddPos[e].p_permission_type = int.Parse(dr["p_permission_type_s"].ToString());
                AddPos[e].p_emp_create = int.Parse(ViewState["EmpIDX"].ToString());
                //AddPos[e].type_action = 1; //int.Parse(ViewState["EmpIDX"].ToString());

                e++;
            }

            if (dsPos.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dataPermission.permission_list = AddPos;
            }
        }

        if (ViewState["vsTemp_SecAdd"] != null)
        {
            var dsSecadd = (DataSet)ViewState["vsTemp_SecAdd"];
            var AddSec = new permission_use_detail[dsSecadd.Tables[0].Rows.Count];
            foreach (DataRow dr in dsSecadd.Tables[0].Rows)
            {
                AddSec[a] = new permission_use_detail();
                AddSec[a].rsec_idx_use = dr["RSecIDX_Se"].ToString();

                e++;
            }

            if (dsSecadd.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dataPermission.permission_use_list = AddSec;
            }
        }

        /*_dataPermission.permission_list = new permission_set_detail[1];
        permission_set_detail dtpermission = new permission_set_detail();

        dtpermission.peridx = 0;
        dtpermission.emp_idx_set = ViewState["EmpIDX"].ToString();
        dtpermission.rpos_idx_set = ViewState["lblrpos_idx_insert"].ToString(); //Gridview
        dtpermission.p_system = int.Parse(ddlsystem.SelectedValue); //System
        dtpermission.p_permission_type = int.Parse(ddlpermission_type.SelectedValue); //permission type / create - approve
        //dtpermission.p_status = int.Parse(ddl_status.SelectedValue);
        dtpermission.p_emp_create = int.Parse(ViewState["EmpIDX"].ToString());
        dtpermission.rsec_idx_use = ViewState["lblsec_idx_insert"].ToString(); //Gridview
        dtpermission.type_action = 1;*/

        //_dataPermission.permission_list[0] = dtpermission;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataPermission));
        _dataPermission = callService_permission(_urlSetDetail_Permssion_Insert, _dataPermission);
    }

    protected void SelectMasterList(int orgidx,int rdeptidx,int rsecidx,int rposidx,int system,int permission)
    {
        _dataPermission.permission_list = new permission_set_detail[1];
        permission_set_detail dtpermission = new permission_set_detail();

        dtpermission.peridx = 0;
        dtpermission.type_action = 1;
        dtpermission.p_permission_type = permission;
        dtpermission.orgidx_search = orgidx;
        dtpermission.rdeptidx_search = rdeptidx;
        dtpermission.rsecidx_search = rsecidx;
        dtpermission.rposidx_search = rposidx;
        dtpermission.system_search = system;

        _dataPermission.permission_list[0] = dtpermission;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataPermission));
        _dataPermission = callService_permission(_urlGetDetail_Permssion, _dataPermission);
        setGridData(GvMaster, _dataPermission.permission_list);
    }

    protected data_employee callService(string _cmdUrl, data_employee _dtmaster)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmaster);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtmaster = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtmaster;
    }

    protected data_permission_set callService_permission(string _cmdUrl, data_permission_set _dtmaster)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmaster);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtmaster = (data_permission_set)_funcTool.convertJsonToObject(typeof(data_permission_set), _localJson);

        return _dtmaster;
    }

    protected void Update_Master_List()
    {
        _dataEmployee.CostCenterDetail = new CostCenter[1];
        CostCenter dtemployee = new CostCenter();

        dtemployee.CostIDX = int.Parse(ViewState["CostIDX_update"].ToString());
        dtemployee.CostName = ViewState["txtcusname_update"].ToString();
        dtemployee.CostNo = ViewState["txtcusno_update"].ToString();
        dtemployee.LocIDX = int.Parse(ViewState["ddlorg_update"].ToString());
        dtemployee.CostStatus = int.Parse(ViewState["ddStatus_update"].ToString());

        _dataEmployee.CostCenterDetail[0] = dtemployee;
        //text.Text = dtemployee.ToString();
        _dataEmployee = callService(_urlGetUpdateCostcenter, _dataEmployee);
    }

    protected void Delete_Master_List()
    {
        _dataPermission.permission_list = new permission_set_detail[1];
        permission_set_detail dtpermission = new permission_set_detail();

        dtpermission.peridx = int.Parse(ViewState["Peridx_delete"].ToString());
        dtpermission.type_action = 1;
        dtpermission.p_status = 9;

        _dataPermission.permission_list[0] = dtpermission;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataPermission));
        _dataPermission = callService_permission(_urlGetDetail_Permssion_delete, _dataPermission);
    }

    protected void select_dep(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServiceEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกฝ่าย....", "0"));
    }

    protected void select_sec(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServiceEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกหน่วยงาน....", "0"));
    }

    protected void select_pos(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dataEmployee.position_list = new position_details[1];
        position_details _posList = new position_details();
        _posList.org_idx = _org_idx;
        _posList.rdept_idx = _rdept_idx;
        _posList.rsec_idx = _rsec_idx;
        _dataEmployee.position_list[0] = _posList;

        _dataEmployee = callServiceEmployee(_urlGetPositionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.position_list, "pos_name_th", "rpos_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกตำแหน่ง....", "0"));
    }

    protected void select_org(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServiceEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกองค์กร....", "0"));
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);
        return _dataEmployee;
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList(int.Parse(ddlorg_search.SelectedValue), int.Parse(ddldep_search.SelectedValue), int.Parse(ddlsec_search.SelectedValue), int.Parse(ddlpos_search.SelectedValue), int.Parse(ddlsystem_search.SelectedValue)
                    , int.Parse(ddlpermission_search.SelectedValue));
                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList(int.Parse(ddlorg_search.SelectedValue), int.Parse(ddldep_search.SelectedValue), int.Parse(ddlsec_search.SelectedValue), int.Parse(ddlpos_search.SelectedValue), int.Parse(ddlsystem_search.SelectedValue)
                    , int.Parse(ddlpermission_search.SelectedValue));

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList(int.Parse(ddlorg_search.SelectedValue), int.Parse(ddldep_search.SelectedValue), int.Parse(ddlsec_search.SelectedValue), int.Parse(ddlpos_search.SelectedValue), int.Parse(ddlsystem_search.SelectedValue)
     , int.Parse(ddlpermission_search.SelectedValue));
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                /*int CostIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtcusno_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtcusno_update");
                var txtcusname_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtcusname_update");
                var ddStatus_update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatus_update");
                var ddlorg_update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlorg_edit");

                GvMaster.EditIndex = -1;

                ViewState["CostIDX_update"] = CostIDX;
                ViewState["txtcusno_update"] = txtcusno_update.Text;
                ViewState["txtcusname_update"] = txtcusname_update.Text;
                ViewState["ddStatus_update"] = ddStatus_update.SelectedValue;
                ViewState["ddlorg_update"] = ddlorg_update.SelectedValue;

                Update_Master_List();*/
                SelectMasterList(int.Parse(ddlorg_search.SelectedValue), int.Parse(ddldep_search.SelectedValue), int.Parse(ddlsec_search.SelectedValue), int.Parse(ddlpos_search.SelectedValue), int.Parse(ddlsystem_search.SelectedValue)
                    , int.Parse(ddlpermission_search.SelectedValue));
                break;
        }
    }

    #endregion

    #region GvRowDeleting
    protected void gvRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvPosAdd_Add":

                //var GvReference = (GridView)FvInsertEmp.FindControl("GvReference");
                var dsvsDelete = (DataSet)ViewState["vsTemp_PosAdd"];
                var drRefer = dsvsDelete.Tables[0].Rows;

                drRefer.RemoveAt(e.RowIndex);

                ViewState["vsTemp_PosAdd"] = dsvsDelete;
                GvPosAdd_Add.EditIndex = -1;
                GvPosAdd_Add.DataSource = ViewState["vsTemp_PosAdd"];
                GvPosAdd_Add.DataBind();

                break;

            case "GvSecAdd_Permis":

                //var GvReference = (GridView)FvInsertEmp.FindControl("GvReference");
                var dsvsDelete_per = (DataSet)ViewState["vsTemp_SecAdd"];
                var drPermis = dsvsDelete_per.Tables[0].Rows;

                drPermis.RemoveAt(e.RowIndex);

                ViewState["vsTemp_SecAdd"] = dsvsDelete_per;
                GvSecAdd_Permis.EditIndex = -1;
                GvSecAdd_Permis.DataSource = ViewState["vsTemp_SecAdd"];
                GvSecAdd_Permis.DataBind();

                break;

        }

    }
    #endregion


    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        //DropDownList ddlorg_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlorg_add_edit");
        //DropDownList ddlshift_add_group = (DropDownList)FvEdit_Detail.FindControl("ddlshift_add_group");

        switch (ddName.ID)
        {
            case "ddlorg_add":
                select_dep(ddldep_add, int.Parse(ddlorg_add.SelectedValue));
                break;
            case "ddldep_add":
                select_sec(ddlsec_add, int.Parse(ddlorg_add.SelectedValue), int.Parse(ddldep_add.SelectedValue));
                break;
            case "ddlsec_add":
                select_pos(ddlpos_add, int.Parse(ddlorg_add.SelectedValue), int.Parse(ddldep_add.SelectedValue), int.Parse(ddlsec_add.SelectedValue));
                break;
            case "ddlpos_add":
                //select_approve_add(ddlapprove1, int.Parse(ddlorg.SelectedValue), int.Parse(ddldep.SelectedValue), int.Parse(ddlsec.SelectedValue), int.Parse(ddlpos.SelectedValue));
                //select_approve_add(ddlapprove2, int.Parse(ddlorg.SelectedValue), int.Parse(ddldep.SelectedValue), int.Parse(ddlsec.SelectedValue), int.Parse(ddlpos.SelectedValue));
                break;
            case "ddlorg_add_permis":
                select_dep(ddldep_add_permis, int.Parse(ddlorg_add_permis.SelectedValue));
                break;
            case "ddldep_add_permis":
                select_sec(ddlsec_add_permis, int.Parse(ddlorg_add_permis.SelectedValue), int.Parse(ddldep_add_permis.SelectedValue));
                break;

            case "ddlorg_search":
                select_dep(ddldep_search, int.Parse(ddlorg_search.SelectedValue));
                break;

            case "ddldep_search":
                select_sec(ddlsec_search, int.Parse(ddlorg_search.SelectedValue), int.Parse(ddldep_search.SelectedValue));

                break;

            case "ddlsec_search":
                select_pos(ddlpos_search, int.Parse(ddlorg_search.SelectedValue), int.Parse(ddldep_search.SelectedValue), int.Parse(ddlsec_search.SelectedValue));
                break;
        }
    }

    #endregion


    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAddHolder":
                btnaddholder.Visible = false;
                div_search.Visible = false;
                Panel_Add.Visible = true;
                select_org(ddlorg_add);
                select_org(ddlorg_add_permis);
                break;

            case "btnCancel":
                btnaddholder.Visible = true;
                Panel_Add.Visible = false;
                div_search.Visible = true ;

                break;

            case "btnAdd":
                Insert_Status();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "CmdDel":
                int Peridx = int.Parse(cmdArg);
                ViewState["Peridx_delete"] = Peridx;
                Delete_Master_List();
                SelectMasterList(int.Parse(ddlorg_search.SelectedValue), int.Parse(ddldep_search.SelectedValue), int.Parse(ddlsec_search.SelectedValue), int.Parse(ddlpos_search.SelectedValue), int.Parse(ddlsystem_search.SelectedValue)
                    , int.Parse(ddlpermission_search.SelectedValue));

                break;

            case "AddPosAdd_Add":
                var dsPosAdd = (DataSet)ViewState["vsTemp_PosAdd"];

                var drPosAdd = dsPosAdd.Tables[0].NewRow();
                drPosAdd["OrgIDX_S"] = int.Parse(ddlorg_add.SelectedValue);
                drPosAdd["OrgNameTH_S"] = ddlorg_add.SelectedItem.Text;
                drPosAdd["RDeptIDX_S"] = int.Parse(ddldep_add.SelectedValue);
                drPosAdd["DeptNameTH_S"] = ddldep_add.SelectedItem.Text;
                drPosAdd["RSecIDX_S"] = int.Parse(ddlsec_add.SelectedValue);
                drPosAdd["SecNameTH_S"] = ddlsec_add.SelectedItem.Text;
                drPosAdd["RPosIDX_S"] = int.Parse(ddlpos_add.SelectedValue);
                drPosAdd["PosNameTH_S"] = ddlpos_add.SelectedItem.Text;

                drPosAdd["p_system_s"] = int.Parse(ddlsystem.SelectedValue);
                drPosAdd["p_system_name_s"] = ddlsystem.SelectedItem.Text;
                drPosAdd["p_permission_type_s"] = int.Parse(ddlpermission_type.SelectedValue);
                drPosAdd["p_permission_type_name_s"] = ddlpermission_type.SelectedItem.Text;

                dsPosAdd.Tables[0].Rows.Add(drPosAdd);

                ViewState["vsTemp_PosAdd"] = dsPosAdd;

                GvPosAdd_Add.DataSource = dsPosAdd.Tables[0];
                GvPosAdd_Add.DataBind();

                ddlsystem.SelectedValue = null;
                ddlpermission_type.SelectedValue = null;
                ddlorg_add.SelectedValue = null;
                ddldep_add.SelectedValue = null;
                ddlsec_add.SelectedValue = null;
                ddlpos_add.SelectedValue = null;

                break;

            case "AddSecAdd_Permis":

                //var GvPosAdd_Edit = (GridView)FvEdit_Detail.FindControl("GvPosAdd_Edit");
                //var ddlorg_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlorg_add_edit");
                //var ddldep_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddldep_add_edit");
                //var ddlsec_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlsec_add_edit");
                //var ddlpos_add_edit = (DropDownList)FvEdit_Detail.FindControl("ddlpos_add_edit");

                var dsSecAdd_permis = (DataSet)ViewState["vsTemp_SecAdd"];

                var drSecAdd_permis = dsSecAdd_permis.Tables[0].NewRow();
                drSecAdd_permis["OrgIDX_Se"] = int.Parse(ddlorg_add_permis.SelectedValue);
                drSecAdd_permis["OrgNameTH_Se"] = ddlorg_add_permis.SelectedItem.Text;
                drSecAdd_permis["RDeptIDX_Se"] = int.Parse(ddldep_add_permis.SelectedValue);
                drSecAdd_permis["DeptNameTH_Se"] = ddldep_add_permis.SelectedItem.Text;
                drSecAdd_permis["RSecIDX_Se"] = int.Parse(ddlsec_add_permis.SelectedValue);
                drSecAdd_permis["SecNameTH_Se"] = ddlsec_add_permis.SelectedItem.Text;
                //drSecAdd_permis["RPosIDX_S"] = int.Parse(ddlpos_add.SelectedValue);
                //drSecAdd_permis["PosNameTH_S"] = ddlpos_add.SelectedItem.Text;

                dsSecAdd_permis.Tables[0].Rows.Add(drSecAdd_permis);

                ViewState["vsTemp_SecAdd"] = dsSecAdd_permis;

                GvSecAdd_Permis.DataSource = dsSecAdd_permis.Tables[0];
                GvSecAdd_Permis.DataBind();

                ddlorg_add_permis.SelectedValue = null;
                ddldep_add_permis.SelectedValue = null;
                ddlsec_add_permis.SelectedValue = null;

                break;

            case "CmdSearch":
                SelectMasterList(int.Parse(ddlorg_search.SelectedValue), int.Parse(ddldep_search.SelectedValue), int.Parse(ddlsec_search.SelectedValue), int.Parse(ddlpos_search.SelectedValue), int.Parse(ddlsystem_search.SelectedValue)
                , int.Parse(ddlpermission_search.SelectedValue));
                break;

            case "btnRefresh":
                ddlorg_search.SelectedValue = "0";
                ddldep_search.SelectedValue = "0";
                ddlsec_search.SelectedValue = "0";
                ddlpos_search.SelectedValue = "0";
                ddlsystem_search.SelectedValue = "0";
                ddlpermission_search.SelectedValue = "0";

                SelectMasterList(int.Parse(ddlorg_search.SelectedValue), int.Parse(ddldep_search.SelectedValue), int.Parse(ddlsec_search.SelectedValue), int.Parse(ddlpos_search.SelectedValue), int.Parse(ddlsystem_search.SelectedValue)
                , int.Parse(ddlpermission_search.SelectedValue));

                break;
        }



    }
    #endregion

}
