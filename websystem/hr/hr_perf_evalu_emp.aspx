﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_perf_evalu_emp.aspx.cs" Inherits="websystem_hr_perf_evalu_emp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <!--*** START Menu ***-->
    <div class="row">

        <div id="divMenu" runat="server" class="col-md-12">

            <nav class="navbar navbar-default">

                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">Menu</a>

                </div>

                <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;"
                    id="bs-example-navbar-collapse-1">

                    <ul class="nav navbar-nav navbar-left" runat="server">

                        <li id="_divMenuLiToIndex" runat="server">
                            <asp:LinkButton ID="_divMenuBtnToIndex" runat="server" CommandName="_divMenuBtnToIndex"
                                OnCommand="btnCommand" Text="รายการทั่วไป" />
                        </li>

                        <li id="_divMenuLiToDivInsert" runat="server">
                            <asp:LinkButton ID="_divMenuBtnToInsert" runat="server" CommandName="_divMenuLiToInsert"
                                OnCommand="btnCommand" Text="ประเมินผลการปฏิบัติงานระดับพนักงานรายวัน" />
                        </li>

                        <li id="_divMenuLiToDivGarp" runat="server">

                            <asp:LinkButton ID="_divMenuBtnToGarp" runat="server" CommandName="_divMenuBtnToGarp"
                                OnCommand="btnCommand" Text="รายงาน" />

                        </li>


                    </ul>

                    <ul class="nav navbar-nav navbar-right" runat="server" visible="false">

                        <li id="_divMenuLiToDocument" runat="server">

                            <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                                NavigateUrl="https://docs.google.com/document/d/1KUftIg57xPtx-v0uOx419WOBH0ZRsoba8X2--ook_fw/edit"
                                Target="_blank" Text="คู่มือการใช้งาน" />

                        </li>

                    </ul>

                </div>

            </nav>

        </div>

    </div>
    <!--*** END Menu ***-->


    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">


            <div class="row">

                <div class="col-md-12">

                    <asp:GridView ID="GvIndexlist" Visible="true" runat="server" AutoGenerateColumns="false"
                        DataKeyNames="u0idx"
                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12 word-wrap"
                        HeaderStyle-CssClass="info" HeaderStyle-Height="30px" AllowPaging="true" PageSize="10"
                        OnPageIndexChanging="gvPageIndexChanging" OnRowDataBound="gvRowDataBound">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก"
                            LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" ControlStyle-Font-Size="10"
                                HeaderStyle-Font-Size="9">
                                <ItemTemplate>

                                    <asp:Label ID="lb_ItemIndex" runat="server" Visible="true"
                                        Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>

                                    <asp:Label ID="lb_u0idx" runat="server" Visible="false"
                                        Text='<%# Eval("u0idx") %>' />

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" ControlStyle-Font-Size="10"
                                HeaderStyle-Font-Size="9">
                                <ItemTemplate>

                                    <asp:Label ID="lb_doccode" runat="server" CssClass="col-sm-12">
                                        <p><%# Eval("doccode") %></p>
                                    </asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่เอกสาร" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" ControlStyle-Font-Size="10"
                                HeaderStyle-Font-Size="9">
                                <ItemTemplate>

                                    <asp:Label ID="lb_zdocdate" runat="server" CssClass="col-sm-12">
                                        <p><%# Eval("zdocdate") %></p>
                                    </asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียดพนักงาน" HeaderStyle-Width="25%"
                                HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left"
                                ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                                <ItemTemplate>
                                    <asp:Label ID="lb_emp_code" runat="server" CssClass="col-sm-12">
                                        <p><b>รหัสพนักงาน:</b> &nbsp;<%# Eval("u0_emp_code") %></p>
                                    </asp:Label>
                                    <asp:Label ID="lb_emp_name_th" runat="server" CssClass="col-sm-12">
                                        <p><b>ชื่อ - นามสกุล:</b> &nbsp;<%# Eval("u0_emp_name_th") %></p>
                                    </asp:Label>
                                    <asp:Label ID="lb_org_name_th" runat="server" CssClass="col-sm-12">
                                        <p><b>องค์กร:</b> &nbsp;<%# Eval("u0_org_name_th") %></p>
                                    </asp:Label>
                                    <asp:Label ID="lb_dept_name_th" runat="server" CssClass="col-sm-12">
                                        <p><b>ฝ่าย:</b> &nbsp;<%# Eval("u0_dept_name_th") %></p>
                                    </asp:Label>
                                    <asp:Label ID="lb_sec_name_th" runat="server" CssClass="col-sm-12">
                                        <p><b>แผนก:</b> &nbsp;<%# Eval("u0_sec_name_th") %></p>
                                    </asp:Label>
                                    <asp:Label ID="lb_pos_name_th" runat="server" CssClass="col-sm-12">
                                        <p><b>ตำแหน่ง:</b> &nbsp;<%# Eval("u0_pos_name_th") %></p>
                                    </asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-CssClass="text-center"
                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%" ControlStyle-Font-Size="10"
                                HeaderStyle-Font-Size="9">
                                <ItemTemplate>

                                    <asp:Label ID="lb_remark" runat="server" CssClass="col-sm-12">
                                        <p><%# Eval("remark") %></p>
                                    </asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                                HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# getStatus( (int)Eval("u0_status")) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-Width="10%"
                                HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center"
                                ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                                <ItemTemplate>

                                    <asp:Label ID="lb_u0_emp_idx" runat="server" Visible="false"
                                        Text='<%# Eval("u0_emp_idx") %>' />
                                    <asp:Label ID="lb_supervisor1_idx" runat="server" Visible="false"
                                        Text='<%# Eval("supervisor1_idx") %>' />
                                    <asp:Label ID="lb_supervisor2_idx" runat="server" Visible="false"
                                        Text='<%# Eval("supervisor2_idx") %>' />
                                    <asp:Label ID="lb_u0_status" runat="server" Visible="false"
                                        Text='<%# Eval("u0_status") %>' />

                                    <asp:UpdatePanel ID="updatebtnsaveEdit" runat="server">
                                        <ContentTemplate>

                                            <asp:LinkButton ID="btnmanage_its" CssClass="btn btn-info btn-sm"
                                                runat="server" data-toggle="tooltip" title="รายละเอียด"
                                                OnCommand="btnCommand" CommandArgument='<%#
                                            Eval("u0idx")
                                            %>' CommandName="btnmanage_its">
                                                <i class="fa fa-file"></i>
                                            </asp:LinkButton>

                                            <asp:LinkButton ID="btnexport" CssClass="btn btn-primary btn-sm"
                                                runat="server" data-toggle="tooltip"
                                                Text="<i class='fa fa-file-excel'></i>" CommandName="btnexport"
                                                OnCommand="btnCommand" CommandArgument='<%# Eval("u0idx") %>'
                                                title="Export"></asp:LinkButton>

                                            <asp:LinkButton CssClass="btn btn-default btn-sm" runat="server"
                                                data-original-title="ยกเลิกส่งข้อมูลให้ HR" data-toggle="tooltip"
                                                Text="" ID="btnunconfirm" CommandName="btnunconfirm"
                                                OnCommand="btnCommand" CommandArgument='<%# Eval("u0idx") %>'
                                                OnClientClick="return confirm('คุณต้องการยกเลิกส่งข้อมูลให้ HR ใช่หรือไม่')">
                                                <i class="fa fa-undo"></i>
                                            </asp:LinkButton>

                                            <asp:LinkButton CssClass="btn btn-danger btn-sm" runat="server"
                                                data-original-title="ลบ" data-toggle="tooltip" Text="ลบ" ID="btnDelete"
                                                CommandName="btnDelete" OnCommand="btnCommand"
                                                CommandArgument='<%# Eval("u0idx") %>'
                                                OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                                                <i class="fa fa-trash"></i>
                                            </asp:LinkButton>



                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnmanage_its" />
                                            <asp:PostBackTrigger ControlID="btnexport" />
                                            <asp:PostBackTrigger ControlID="btnDelete" />
                                            <asp:PostBackTrigger ControlID="btnDelete" />

                                        </Triggers>
                                    </asp:UpdatePanel>

                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                </div>

            </div>

        </asp:View>
        <%--  --%>
        <asp:View ID="ViewMain" runat="server">


            <div class="col-lg-12" id="detailMain" runat="server" visible="true">
                <div class="panel panel-info">

                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp;
                                รายละเอียดข้อมูลผู้ทำรายการ</strong></h3>
                    </div>

                    <asp:FormView ID="FvDetailUser_Main" runat="server" Width="100%" DefaultMode="Edit">
                        <EditItemTemplate>

                            <div class="panel-body">

                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="lb_emps_code_purchase" Text='<%# Eval("emp_code") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>

                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล :</label>
                                        <div class="col-sm-4 control-label textleft">

                                            <asp:TextBox ID="fullname_purchase_approve"
                                                Text='<%# Eval("emp_name_th") %>' runat="server" CssClass="form-control"
                                                Enabled="false" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="Label7" Text='<%# Eval("org_name_th") %>' runat="server"
                                                CssClass="form-control" Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย :</label>

                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="lbdreptnamepurchase" Text='<%# Eval("dept_name_th") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="Label9" Text='<%# Eval("sec_name_th") %>' runat="server"
                                                CssClass="form-control" Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง :</label>

                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="Label10" Text='<%# Eval("pos_name_th") %>' runat="server"
                                                CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เบอร์ติดต่อ :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="Label11" Text='<%# Eval("emp_mobile_no") %>' runat="server"
                                                CssClass="form-control" Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">e-mail :</label>

                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="lbemail" Text='<%# Eval("emp_email") %>' runat="server"
                                                CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </EditItemTemplate>
                    </asp:FormView>

                </div>

                <div class="panel panel-info" runat="server" visible="true">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-file"></i><strong>&nbsp;
                                รายละเอียด</strong></h3>
                    </div>
                    <div class="panel-body" style="height: 600px; overflow-y: scroll; overflow-x: scroll;">
                        <div class="col-md-12">

                            <asp:FormView ID="FV_docno" runat="server" Width="100%" OnDataBound="FvDetail_DataBound"
                                DefaultMode="Edit">
                                <EditItemTemplate>

                                    <div class="form-horizontal" runat="server" visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label49" class="col-md-2 control-label" runat="server"
                                                Text="รหัสเอกสาร : " />
                                            <div class="col-sm-4 control-label textleft">
                                                <asp:TextBox Enabled="false" ID="lbdocument_code"
                                                    Text='<%# Eval("doccode") %>' runat="server"
                                                    CssClass="form-control fa-align-left"></asp:TextBox>

                                            </div>
                                            <asp:Label ID="Label50" class="col-md-2 control-label" runat="server"
                                                Text="วันที่เอกสาร : " />
                                            <div class="col-sm-4 control-label textleft">
                                                <asp:TextBox Enabled="false" ID="Labellld"
                                                    Text='<%# Eval("zdocdate") %>' runat="server"
                                                    CssClass="form-control fa-align-left"></asp:TextBox>
                                            </div>
                                        </div>

                                    </div>
                                </EditItemTemplate>
                            </asp:FormView>

                            <asp:FormView ID="FvInsert" runat="server" Width="100%" OnDataBound="FvDetail_DataBound"
                                DefaultMode="Edit">
                                <EditItemTemplate>



                                    <%-- <div class="form-horizontal" role="form"> --%>
                                    <asp:TextBox ID="lbdocument_code" Text='<%# Eval("doccode") %>' runat="server"
                                        Visible="false" CssClass="form-control"></asp:TextBox>

                                    <%-- OnRowCreated="grvMergeHeader_RowCreated" --%>

                                    <asp:GridView ID="gvItemsu1PerfEvaluEmpDaily" runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12 word-wrap"
                                        HeaderStyle-CssClass="info" GridLines="None" OnRowDataBound="gvRowDataBound">

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="9"
                                                ControlStyle-Font-Size="10" HeaderStyle-Width="3%">
                                                <ItemTemplate>

                                                    <%# (Container.DataItemIndex +1) %>
                                                    <%--<br />--%>
                                                    <asp:Label ID="lb_total_score_before" runat="server" Visible="false"
                                                        Text='<%# Eval("total_score_before") %>' />
                                                    <%--<br />--%>
                                                    <asp:Label ID="lb_total_deduction_before" runat="server"
                                                        Visible="false" Text='<%# Eval("total_deduction_before") %>' />
                                                    <%--<br />--%>
                                                    <asp:Label ID="lb_score_balance_before" runat="server"
                                                        Visible="false" Text='<%# Eval("score_balance_before") %>' />
                                                    <%--<br />--%>
                                                    <asp:Label ID="lb_score_assessment_qty_before" runat="server"
                                                        Visible="false"
                                                        Text='<%# Eval("score_assessment_qty_before") %>' />

                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="รหัส" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9"
                                                ControlStyle-Font-Size="10" HeaderStyle-Width="5%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_u0_emp_idx" runat="server" Visible="false"
                                                        Text='<%# Eval("u0_emp_idx") %>' />
                                                    <asp:Label ID="lb_emp_idx" runat="server" Visible="false"
                                                        Text='<%# Eval("emp_idx") %>' />
                                                    <asp:Label ID="lb_u0idx" runat="server" Visible="false"
                                                        Text='<%# Eval("u0idx") %>' />
                                                    <asp:Label ID="lb_u1idx" runat="server" Visible="false"
                                                        Text='<%# Eval("u1idx") %>' />
                                                    <asp:Label ID="lb_supervisor1_idx" runat="server" Visible="false"
                                                        Text='<%# Eval("supervisor1_idx") %>' />
                                                    <asp:Label ID="lb_supervisor2_idx" runat="server" Visible="false"
                                                        Text='<%# Eval("supervisor2_idx") %>' />
                                                    <asp:Label ID="lb_u0_status" runat="server" Visible="false"
                                                        Text='<%# Eval("u0_status") %>' />

                                                    <asp:Label ID="lb_emp_code" runat="server" CssClass="col-sm-12"
                                                        Text='<%# Eval("emp_code") %>'>

                                                    </asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ชื่อ - สกุล"
                                                HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left"
                                                HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                                                HeaderStyle-Width="15%">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_emp_name_th" runat="server" CssClass="col-sm-12"
                                                        Width="130px" Text='<%# Eval("emp_name_th") %>'>

                                                    </asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ตำแหน่ง" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9"
                                                ControlStyle-Font-Size="10" HeaderStyle-Width="10%">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_pos_name_th" runat="server" CssClass="col-sm-12"
                                                        Text='<%# Eval("pos_name_th") %>'>

                                                    </asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="เริ่มงาน" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9"
                                                ControlStyle-Font-Size="10" HeaderStyle-Width="5%">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_emp_probation_date" runat="server"
                                                        CssClass="col-sm-12" Text='<%# Eval("emp_probation_date") %>'>

                                                    </asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="CC" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9"
                                                ControlStyle-Font-Size="10" HeaderStyle-Width="5%">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_costcenter_no" runat="server" CssClass="col-sm-12"
                                                        Text='<%# Eval("costcenter_no") %>'>

                                                    </asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="CC Name" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9"
                                                ControlStyle-Font-Size="10" HeaderStyle-Width="5%">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_costcenter_name" runat="server"
                                                        CssClass="col-sm-12" Text='<%# Eval("costcenter_name") %>'>

                                                    </asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="อายุงาน" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9"
                                                ControlStyle-Font-Size="10" HeaderStyle-Width="10%">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_work_experience_qty" runat="server"
                                                        CssClass="col-sm-12" Width="100px"
                                                        Text='<%# Eval("work_experience_qty") %>'>

                                                    </asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="การทำงานเป็นทีม"
                                                HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left"
                                                HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                                                HeaderStyle-Width="10%">
                                                <ItemTemplate>

                                                    <asp:TextBox ID="txt_ownership_qty" runat="server" Font-Size="Small"
                                                        Visible="true" TextMode="Number" Width="70px"
                                                        CssClass="form-control" Text='<%# Eval("ownership_qty") %>' />

                                                    <asp:RangeValidator ID="Range_ownership_qty"
                                                        ControlToValidate="txt_ownership_qty" MinimumValue="0"
                                                        MaximumValue="5" Display="Dynamic" ForeColor="Red"
                                                        Type="Integer" Text="กรุณากรอกข้อมูล 0 ถึง 5 !"
                                                        runat="server" />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="มุ่งมั่นสู่ความเป็นเสิศ"
                                                HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left"
                                                HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                                                HeaderStyle-Width="10%">
                                                <ItemTemplate>

                                                    <asp:TextBox ID="txt_improvement_continuously_qty" runat="server"
                                                        Font-Size="Small" Visible="true" TextMode="Number" Width="70px"
                                                        CssClass="form-control"
                                                        Text='<%# Eval("improvement_continuously_qty") %>' />

                                                    <asp:RangeValidator ID="Range_improvement_continuously_qty"
                                                        ControlToValidate="txt_improvement_continuously_qty"
                                                        MinimumValue="0" MaximumValue="5" Display="Dynamic"
                                                        ForeColor="Red" Type="Integer" Text="กรุณากรอกข้อมูล 0 ถึง 5 !"
                                                        runat="server" />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สร้างสรรค์สิ่งใหม่"
                                                HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left"
                                                HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10">
                                                <ItemTemplate>

                                                    <asp:TextBox ID="txt_commitment_qty" runat="server"
                                                        Font-Size="Small" Visible="true" TextMode="Number" Width="70px"
                                                        CssClass="form-control" Text='<%# Eval("commitment_qty") %>' />

                                                    <asp:RangeValidator ID="Range_commitment_qty"
                                                        ControlToValidate="txt_commitment_qty" MinimumValue="0"
                                                        MaximumValue="5" Display="Dynamic" ForeColor="Red"
                                                        Type="Integer" Text="กรุณากรอกข้อมูล 0 ถึง 5 !"
                                                        runat="server" />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="รวมคะแนน" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9"
                                                ControlStyle-Font-Size="10">
                                                <ItemTemplate>

                                                    <asp:Label ID="txt_total_score" runat="server" Font-Size="Small"
                                                        TextMode="Number" Text='<%# Eval("total_score") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ขาด" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9"
                                                ControlStyle-Font-Size="10" Visible="false">
                                                <ItemTemplate>

                                                    <asp:Label ID="txt_missing_qty" runat="server" Font-Size="Small"
                                                        Text='<%# Eval("missing_qty") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ลาป่วย" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9"
                                                ControlStyle-Font-Size="10" Visible="false">
                                                <ItemTemplate>

                                                    <asp:Label ID="txt_sick_Leave_qty" runat="server" Font-Size="Small"
                                                        Text='<%# Eval("sick_Leave_qty") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สายกลับก่อน (นาที)"
                                                HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Right"
                                                HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" Visible="false">
                                                <ItemTemplate>

                                                    <asp:Label ID="txt_call_back_qty" runat="server" Font-Size="Small"
                                                        Text='<%# Eval("call_back_qty") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ตักเตือนด้วยวาจา (ครั้ง)"
                                                HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Right"
                                                HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" Visible="false">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_admonish_speech" runat="server" Font-Size="Small"
                                                        Text='<%# Eval("admonish_speech") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ใบเตือน (ครั้ง)"
                                                HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Right"
                                                HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" Visible="false">
                                                <ItemTemplate>

                                                    <asp:Label ID="txt_warning_notice_qty" runat="server"
                                                        Font-Size="Small" Text='<%# Eval("warning_notice_qty") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="พักงาน (ครั้ง)"
                                                HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Right"
                                                HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" Visible="false">
                                                <ItemTemplate>

                                                    <asp:Label ID="txt_work_break_qty" runat="server" Font-Size="Small"
                                                        Text='<%# Eval("work_break_qty") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="รวมหัก (ทั้งหมด)"
                                                HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Right"
                                                HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" Visible="false">
                                                <ItemTemplate>

                                                    <asp:Label ID="txt_total_deduction" runat="server" Font-Size="Small"
                                                        Text='<%# Eval("total_deduction") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="รวมหัก" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9"
                                                ControlStyle-Font-Size="10" Visible="false">
                                                <ItemTemplate>

                                                    <asp:Label ID="txt_deduction_qty" runat="server" Font-Size="Small"
                                                        Text='<%# Eval("deduction_qty") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="คงเหลือคะแนนสถิติ"
                                                HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Right"
                                                HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" Visible="false">
                                                <ItemTemplate>

                                                    <asp:Label ID="txt_score_balance" runat="server" Font-Size="Small"
                                                        Text='<%# Eval("score_balance") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สรุปคะแนนประเมิน"
                                                HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Right"
                                                HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" Visible="false">
                                                <ItemTemplate>

                                                    <asp:Label ID="txt_score_assessment_qty" runat="server"
                                                        Font-Size="Small" Text='<%# Eval("score_assessment_qty") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="เกรด" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Size="9"
                                                ControlStyle-Font-Size="10" Visible="false">
                                                <ItemTemplate>

                                                    <asp:Label ID="txt_grade" runat="server" Font-Size="Small"
                                                        Text='<%# Eval("grade") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="เกรดสุทธิ" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Size="9"
                                                ControlStyle-Font-Size="10" Visible="false">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_grade_criteria" runat="server" Font-Size="Small"
                                                        Text='<%# Eval("grade_criteria") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ค่าแรง" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9"
                                                ControlStyle-Font-Size="10" Visible="false">
                                                <ItemTemplate>

                                                    <asp:TextBox ID="txt_labor_cost" runat="server" Font-Size="Small"
                                                        TextMode="Number" Width="70px" CssClass="form-control"
                                                        Text='<%# Eval("labor_cost") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="โบนัส" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9"
                                                ControlStyle-Font-Size="10" Visible="false">
                                                <ItemTemplate>

                                                    <asp:Label ID="txt_bonus" runat="server" Font-Size="Small"
                                                        Text='<%# Eval("bonus") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ค่าแรงใหม่"
                                                HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Right"
                                                HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" Visible="false">
                                                <ItemTemplate>

                                                    <asp:TextBox ID="txt_labor_cost_new" runat="server"
                                                        Font-Size="Small" TextMode="Number" Width="70px"
                                                        CssClass="form-control" Text='<%# Eval("labor_cost_new") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="โบนัส (วัน)"
                                                HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Right"
                                                HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" Visible="false">
                                                <ItemTemplate>

                                                    <asp:TextBox ID="txt_bonus_day" runat="server" Font-Size="Small"
                                                        TextMode="Number" Width="70px" CssClass="form-control"
                                                        Text='<%# Eval("bonus_day") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-CssClass="text-center"
                                                ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9"
                                                ControlStyle-Font-Size="10" Visible="false">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_description" runat="server" Font-Size="Small"
                                                        Width="100px" ForeColor="Red"
                                                        Text='<%# Eval("description") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </EditItemTemplate>
                            </asp:FormView>

                        </div>
                    </div>
                </div>

                <asp:FormView ID="FvInsertH" runat="server" Width="100%" OnDataBound="FvDetail_DataBound"
                    DefaultMode="Edit">
                    <EditItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-body">
                                <div class="form-horizontal">

                                    <asp:TextBox ID="txt_hidden_u0idx" runat="server" Visible="false"
                                        Text='<%# Eval("u0idx") %>'></asp:TextBox>
                                    <div class="form-group">
                                        <asp:Label ID="Labedle1" class="col-md-2 control-label" runat="server"
                                            Text="หมายเหตุ : " />
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtremark" TextMode="multiline" Rows="5"
                                                CssClass="form-control" runat="server" Text='<%# Eval("remark") %>'>
                                            </asp:TextBox>
                                        </div>
                                    </div>
                                    <asp:Panel ID="Panel_ImportAdd" runat="server" Visible="false">
                                        <asp:UpdatePanel ID="upActor1Node1Files11" runat="server"
                                            UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">แนบไฟล์ Excel</label>
                                                    <div class="col-md-10">
                                                        <asp:FileUpload ID="upload" ViewStateMode="Enabled"
                                                            AutoPostBack="true" Font-Size="small" ClientIDMode="Static"
                                                            runat="server"
                                                            CssClass="btn btn-primary btn-sm multi max-1 accept-xlsx" />
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnImport" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <asp:UpdatePanel ID="upActor1Node1Files1" runat="server">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <div class="col-md-2  col-md-offset-2">
                                                        <asp:LinkButton ID="btnImport" CssClass="btn btn-success btn-sm"
                                                            data-toggle="tooltip" title="Import" runat="server"
                                                            CommandName="btnImport" OnCommand="btnCommand"
                                                            CommandArgument='<%# Eval("u0idx") %>'
                                                            OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')">
                                                            <i class="fa fa-database"></i> Import</asp:LinkButton>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnImport" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </asp:Panel>
                                    <div class="form-group">
                                        <asp:Label ID="Label17" CssClass="col-md-2 control-labelnotop text_right"
                                            runat="server" Text="สถานะ :" />
                                        <div class="col-md-4">
                                            <asp:Label ID="lb_u0_status" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="ddlu0_status" runat="server" CssClass="form-control"
                                                Visible="false" Text='<%# Eval("u0_status") %>'>
                                            </asp:Label>

                                            <%--<asp:DropDownList ID="ddlu0_status" runat="server" CssClass="form-control" Visible="false"
                                                SelectedValue='<%# Eval("u0_status") == null ? "1" : Eval("u0_status") %>'>
                                            <asp:ListItem Value="1" Text="ดำเนินการสร้าง / แก้ไข"></asp:ListItem>
                                            <asp:ListItem Value="2" Text="จบการดำเนินการและส่งข้อมูลให้ HR">
                                            </asp:ListItem>
                                            </asp:DropDownList>--%>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-3 col-md-offset-8">
                                            <asp:ValidationSummary ID="Summary" runat="server" ShowSummary="false"
                                                ShowMessageBox="true"
                                                HeaderText="เกิดข้อผิดพลาดค่ะ กรุณาตรวจสอบอีกครั้งค่ะ" />
                                            <asp:LinkButton ID="btnExportInput" CssClass="btn btn-info btn-sm"
                                                runat="server" CommandName="btnExportInput" OnCommand="btnCommand"
                                                title="Export" CommandArgument='<%# Eval("u0idx") %>'><i
                                                    class="fa fa-file-excel"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btncalculator" CssClass="btn btn-primary btn-sm"
                                                runat="server" CommandName="btncalculator" OnCommand="btnCommand"
                                                title="คำนวน" CommandArgument='<%# Eval("u0idx") %>'><i
                                                    class="fa fa-calculator"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnSave" CssClass="btn btn-success btn-sm"
                                                runat="server" CommandName="btnSave" OnCommand="btnCommand"
                                                OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')"
                                                title="Save" CommandArgument='<%# Eval("u0idx") %>'><i
                                                    class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnConfirm" CssClass="btn btn-info btn-sm"
                                                runat="server" CommandName="btnConfirm" OnCommand="btnCommand"
                                                OnClientClick="return confirm('คุณต้องการจบการดำเนินการและส่งข้อมูลให้ HR ใช่หรือไม่ ?')"
                                                title="จบการดำเนินการและส่งข้อมูลให้ HR"
                                                CommandArgument='<%# Eval("u0idx") %>'><i
                                                    class="fa fa-check-square"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnCancel" CssClass="btn btn-default  btn-sm"
                                                runat="server" ValidationGroup="Cancel" CommandName="btnCancel"
                                                OnCommand="btnCommand" title="Cancel"
                                                OnClientClick="return confirm('คุณต้องการออกจากรายการนี้ใช่หรือไม่ ?')">
                                                <i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>
            </div>


            <br />

        </asp:View>

        <asp:View ID="View_report" runat="server">


            <div class="row">

                <div class="col-md-12">

                    <asp:UpdatePanel ID="Update_PnSearchReport" runat="server">
                        <ContentTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">ค้นหารายงาน </h3>
                                </div>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <%--<div class="form-group">
                                                <label class="col-sm-2 control-label">ประเภทรายงาน</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlTypeReport" runat="server"  CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกประเภทรายงาน ---</asp:ListItem>
                                                        <asp:ListItem Value="1">ตาราง</asp:ListItem>
                                                        <asp:ListItem Value="2">กราฟ</asp:ListItem>
                                                    </asp:DropDownList>
                                                   
                                                </div>
                                                <label class="col-sm-6 control-label"></label>
                                            </div>--%>

                                        <!-- START Report Graph -->
                                        <asp:UpdatePanel ID="Update_PnGraphReportDetail" runat="server"
                                            UpdateMode="Conditional">
                                            <ContentTemplate>


                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">ประเภทรายงาน</label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlTypeReport" runat="server"
                                                            OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                            AutoPostBack="true" CssClass="form-control">
                                                            <%-- <asp:ListItem Value="0">--- เลือกประเภทรายงาน ---</asp:ListItem> --%>
                                                            <asp:ListItem Value="1">ตาราง</asp:ListItem>
                                                            <%-- <asp:ListItem Value="2">กราฟ</asp:ListItem> --%>
                                                        </asp:DropDownList>

                                                    </div>
                                                    <label class="col-sm-6 control-label"></label>
                                                </div>

                                                <div class="form-group" runat="server" visible="false" id="pln_grade">
                                                    <label class="col-sm-2 control-label">เกรด</label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlgrade" runat="server"
                                                            CssClass="form-control">
                                                            <%-- <asp:ListItem Value="0">--- เลือกเกรด ---</asp:ListItem>--%>
                                                            <asp:ListItem Value="1">เกรด</asp:ListItem>
                                                            <asp:ListItem Value="2">เกรดสุทธิ</asp:ListItem>
                                                        </asp:DropDownList>

                                                    </div>
                                                    <label class="col-sm-6 control-label"></label>
                                                </div>

                                                <div class="form-group">

                                                    <label class="col-sm-2 control-label">ปี</label>
                                                    <div class="col-sm-3">

                                                        <asp:DropDownList ID="ddlyear" CssClass="form-control"
                                                            runat="server">
                                                            <%-- <asp:ListItem Value="0">--- เลือก ---</asp:ListItem>
                                                            <asp:ListItem Value="2018">2018</asp:ListItem>--%>
                                                            <asp:ListItem Value="2019">2019</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-4">
                                                        <asp:LinkButton ID="btnSearchGraph" runat="server"
                                                            CssClass="btn btn-info" OnCommand="btnCommand"
                                                            CommandName="cmdSearchReportGraph"
                                                            data-original-title="ค้นหา" data-toggle="tooltip"
                                                            ValidationGroup="SearchReportDetailGraph"><span
                                                                class="glyphicon glyphicon-search"></span> ค้นหา
                                                        </asp:LinkButton>
                                                        <asp:LinkButton ID="btnexport_report" CssClass="btn btn-primary"
                                                            runat="server" data-toggle="tooltip"
                                                            Text="<i class='fa fa-file-excel'></i>"
                                                            CommandName="btnexport_report" OnCommand="btnCommand"
                                                            title="Export"></asp:LinkButton>

                                                    </div>
                                                    <label class="col-sm-6 control-label"></label>
                                                </div>


                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnSearchGraph" />
                                                <asp:PostBackTrigger ControlID="btnexport_report" />
                                            </Triggers>

                                        </asp:UpdatePanel>
                                        <!-- START Report Graph -->

                                    </div>
                                </div>

                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <!-- Panel Search Report -->


                    <asp:UpdatePanel ID="Update_PanelGraph" runat="server">
                        <ContentTemplate>
                            <%--<div class="panel panel-success ">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>&nbsp; ข้อมูลรายงาน</strong></h3>
                                </div>
                                <div class="panel-body">--%>

                            <asp:Literal ID="litReportChart" runat="server" />
                            <asp:Literal ID="litReportChart1" runat="server" />
                            <%--</div>
                            </div>--%>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                    <%--                    <div class="panel panel-success ">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>&nbsp; ข้อมูลรายงาน</strong></h3>
                        </div>
                        <div class="panel-body">--%>

                    <asp:UpdatePanel ID="Update_Panelreport" runat="server">
                        <ContentTemplate>


                            <asp:GridView ID="GVreport" runat="server" AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12 word-wrap"
                                HeaderStyle-CssClass="info" GridLines="None" OnRowDataBound="gvRowDataBound">

                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" HeaderStyle-Width="3%">
                                        <ItemTemplate>

                                            <asp:Label ID="lb_DataItemIndex" runat="server" CssClass="col-sm-12"
                                                Text='<%# (Container.DataItemIndex +1) %>'>
                                            </asp:Label>

                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รหัส" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" HeaderStyle-Width="5%">
                                        <ItemTemplate>

                                            <asp:Label ID="lb_emp_code" runat="server" CssClass="col-sm-12"
                                                Text='<%# Eval("emp_code") %>'>

                                            </asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชื่อ - สกุล" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" HeaderStyle-Width="15%">
                                        <ItemTemplate>

                                            <asp:Label ID="lb_emp_name_th" runat="server" CssClass="col-sm-12"
                                                Width="130px" Text='<%# Eval("emp_name_th") %>'>

                                            </asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ตำแหน่ง" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" HeaderStyle-Width="10%">
                                        <ItemTemplate>

                                            <asp:Label ID="lb_pos_name_th" runat="server" CssClass="col-sm-12"
                                                Text='<%# Eval("pos_name_th") %>'>

                                            </asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เริ่มงาน" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" HeaderStyle-Width="5%">
                                        <ItemTemplate>

                                            <asp:Label ID="lb_emp_probation_date" runat="server" CssClass="col-sm-12"
                                                Text='<%# Eval("emp_probation_date") %>'>

                                            </asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="CC" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" HeaderStyle-Width="5%">
                                        <ItemTemplate>

                                            <asp:Label ID="lb_costcenter_no" runat="server" CssClass="col-sm-12"
                                                Text='<%# Eval("costcenter_no") %>'>

                                            </asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="CC Name" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" HeaderStyle-Width="5%">
                                        <ItemTemplate>

                                            <asp:Label ID="lb_costcenter_name" runat="server" CssClass="col-sm-12"
                                                Text='<%# Eval("costcenter_name") %>'>

                                            </asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="อายุงาน" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" HeaderStyle-Width="10%">
                                        <ItemTemplate>

                                            <asp:Label ID="lb_work_experience" runat="server" CssClass="col-sm-12"
                                                Width="100px" Text='<%# Eval("work_experience") %>'>

                                            </asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="การทำงานเป็นทีม" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" HeaderStyle-Width="10%">
                                        <ItemTemplate>

                                            <asp:Label ID="txt_ownership_qty" runat="server" Font-Size="Small"
                                                Visible="true" TextMode="Number" Width="70px"
                                                Text='<%# Eval("ownership_qty") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="มุ่งมั่นสู่ความเป็นเสิศ"
                                        HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" HeaderStyle-Width="10%">
                                        <ItemTemplate>


                                            <asp:Label ID="txt_improvement_continuously_qty" runat="server"
                                                Font-Size="Small" Visible="true" TextMode="Number" Width="70px"
                                                Text='<%# Eval("improvement_continuously_qty") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สร้างสรรค์สิ่งใหม่"
                                        HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10">
                                        <ItemTemplate>

                                            <asp:Label ID="txt_commitment_qty" runat="server" Font-Size="Small"
                                                Visible="true" TextMode="Number" Width="70px"
                                                Text='<%# Eval("commitment_qty") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รวมคะแนน" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10">
                                        <ItemTemplate>

                                            <asp:Label ID="txt_total_score" runat="server" Font-Size="Small"
                                                TextMode="Number" Text='<%# Eval("total_score") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ขาด" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" Visible="false">
                                        <ItemTemplate>

                                            <asp:Label ID="txt_missing_qty" runat="server" Font-Size="Small"
                                                Text='<%# Eval("missing_qty") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ลาป่วย" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" Visible="false">
                                        <ItemTemplate>

                                            <asp:Label ID="txt_sick_Leave_qty" runat="server" Font-Size="Small"
                                                Text='<%# Eval("sick_Leave_qty") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สายกลับก่อน (นาที)"
                                        HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Right"
                                        HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" Visible="false">
                                        <ItemTemplate>

                                            <asp:Label ID="txt_call_back_qty" runat="server" Font-Size="Small"
                                                Text='<%# Eval("call_back_qty") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="ตักเตือนด้วยวาจา (ครั้ง)"
                                        HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Right"
                                        HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" Visible="false">
                                        <ItemTemplate>

                                            <asp:Label ID="lb_admonish_speech" runat="server" Font-Size="Small"
                                                Text='<%# Eval("admonish_speech") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="ใบเตือน (ครั้ง)" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" Visible="false">
                                        <ItemTemplate>

                                            <asp:Label ID="txt_warning_notice_qty" runat="server" Font-Size="Small"
                                                TextMode="Number" Width="70px"
                                                Text='<%# Eval("warning_notice_qty") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="พักงาน (ครั้ง)" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" Visible="false">
                                        <ItemTemplate>

                                            <asp:Label ID="txt_work_break_qty" runat="server" Font-Size="Small"
                                                TextMode="Number" Width="70px" Text='<%# Eval("work_break_qty") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รวมหัก (ทั้งหมด)" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" Visible="false">
                                        <ItemTemplate>

                                            <asp:Label ID="txt_total_deduction" runat="server" Font-Size="Small"
                                                Text='<%# Eval("total_deduction") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รวมหัก" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" Visible="false">
                                        <ItemTemplate>

                                            <asp:Label ID="txt_deduction_qty" runat="server" Font-Size="Small"
                                                Text='<%# Eval("deduction_qty") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="คงเหลือคะแนนสถิติ" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" Visible="false">
                                        <ItemTemplate>

                                            <asp:Label ID="txt_score_balance" runat="server" Font-Size="Small"
                                                Text='<%# Eval("score_balance") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สรุปคะแนนประเมิน" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" Visible="false">
                                        <ItemTemplate>

                                            <asp:Label ID="txt_score_assessment_qty" runat="server" Font-Size="Small"
                                                Text='<%# Eval("score_assessment_qty") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เกรด" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" Visible="false">
                                        <ItemTemplate>

                                            <asp:Label ID="txt_grade" runat="server" Font-Size="Small"
                                                Text='<%# Eval("grade") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เกรดสุทธิ" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" Visible="false">
                                        <ItemTemplate>

                                            <asp:Label ID="txt_grade_criteria" runat="server" Font-Size="Small"
                                                Text='<%# Eval("grade_criteria") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-CssClass="text-center"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9"
                                        ControlStyle-Font-Size="10" Visible="false">
                                        <ItemTemplate>

                                            <asp:Label ID="lb_description" runat="server" Font-Size="Small"
                                                Width="100px" ForeColor="Red" Text='<%# Eval("description") %>' />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>



                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
            </div>
            <%--</div>
            </div>--%>
        </asp:View>

    </asp:MultiView>


    <asp:Panel ID="pnl_ExportExcel" runat="server" Visible="false">

        <asp:GridView ID="GvExportExcel" runat="server" AutoGenerateColumns="false"
            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12 word-wrap"
            HeaderStyle-CssClass="info" GridLines="None" OnRowDataBound="gvRowDataBound">

            <EmptyDataTemplate>
                <div style="text-align: center">ไม่พบข้อมูล</div>
            </EmptyDataTemplate>
            <Columns>

                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="3%">
                    <ItemTemplate>

                        <asp:Label ID="lb_DataItemIndex" runat="server" CssClass="col-sm-12"
                            Text='<%# (Container.DataItemIndex +1) %>'>
                        </asp:Label>

                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="รหัส" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left"
                    HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" HeaderStyle-Width="5%">
                    <ItemTemplate>

                        <asp:Label ID="lb_emp_code" runat="server" CssClass="col-sm-12" Text='<%# Eval("emp_code") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="ชื่อ - สกุล" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="15%">
                    <ItemTemplate>

                        <asp:Label ID="lb_emp_name_th" runat="server" CssClass="col-sm-12" Width="130px"
                            Text='<%# Eval("emp_name_th") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="ตำแหน่ง" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="10%">
                    <ItemTemplate>

                        <asp:Label ID="lb_pos_name_th" runat="server" CssClass="col-sm-12"
                            Text='<%# Eval("pos_name_th") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="เริ่มงาน" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="5%">
                    <ItemTemplate>

                        <asp:Label ID="lb_emp_probation_date" runat="server" CssClass="col-sm-12"
                            Text='<%# Eval("emp_probation_date") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="CC" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left"
                    HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" HeaderStyle-Width="5%">
                    <ItemTemplate>

                        <asp:Label ID="lb_costcenter_no" runat="server" CssClass="col-sm-12"
                            Text='<%# Eval("costcenter_no") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="CC Name" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="5%">
                    <ItemTemplate>

                        <asp:Label ID="lb_costcenter_name" runat="server" CssClass="col-sm-12"
                            Text='<%# Eval("costcenter_name") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="อายุงาน" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="10%">
                    <ItemTemplate>

                        <asp:Label ID="lb_work_experience_qty" runat="server" CssClass="col-sm-12" Width="100px"
                            Text='<%# Eval("work_experience") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="การทำงานเป็นทีม" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="10%">
                    <ItemTemplate>

                        <asp:Label ID="txt_ownership_qty" runat="server" Font-Size="Small" Visible="true"
                            TextMode="Number" Width="70px" CssClass="form-control"
                            Text='<%# Eval("ownership_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="มุ่งมั่นสู่ความเป็นเสิศ" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="10%">
                    <ItemTemplate>


                        <asp:Label ID="txt_improvement_continuously_qty" runat="server" Font-Size="Small" Visible="true"
                            TextMode="Number" Width="70px" CssClass="form-control"
                            Text='<%# Eval("improvement_continuously_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="สร้างสรรค์สิ่งใหม่" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10">
                    <ItemTemplate>

                        <asp:Label ID="txt_commitment_qty" runat="server" Font-Size="Small" Visible="true"
                            TextMode="Number" Width="70px" CssClass="form-control"
                            Text='<%# Eval("commitment_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="รวมคะแนน" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10">
                    <ItemTemplate>

                        <asp:Label ID="txt_total_score" runat="server" Font-Size="Small" TextMode="Number"
                            Text='<%# Eval("total_score") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <%-- <asp:TemplateField HeaderText="ขาด" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Right"
                    HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="txt_missing_qty" runat="server" Font-Size="Small"
                            Text='<%# Eval("missing_qty") %>' />

                </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="ลาป่วย" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="txt_sick_Leave_qty" runat="server" Font-Size="Small"
                            Text='<%# Eval("sick_Leave_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="สายกลับก่อน (นาที)" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="txt_call_back_qty" runat="server" Font-Size="Small"
                            Text='<%# Eval("call_back_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="ตักเตือนด้วยวาจา (ครั้ง)" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="lb_admonish_speech" runat="server" Font-Size="Small"
                            Text='<%# Eval("admonish_speech") %>' />

                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="ใบเตือน (ครั้ง)" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="txt_warning_notice_qty" runat="server" Font-Size="Small" TextMode="Number"
                            Width="70px" CssClass="form-control" Text='<%# Eval("warning_notice_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="พักงาน (ครั้ง)" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="txt_work_break_qty" runat="server" Font-Size="Small" TextMode="Number"
                            Width="70px" CssClass="form-control" Text='<%# Eval("work_break_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="รวมหัก (ทั้งหมด)" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="txt_total_deduction" runat="server" Font-Size="Small"
                            Text='<%# Eval("total_deduction") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="คงเหลือคะแนนสถิติ" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="txt_score_balance" runat="server" Font-Size="Small"
                            Text='<%# Eval("score_balance") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="สรุปคะแนนประเมิน" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="txt_score_assessment_qty" runat="server" Font-Size="Small"
                            Text='<%# Eval("score_assessment_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField> --%>

                <%-- <asp:TemplateField HeaderText="เกรด" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Center"
                    HeaderStyle-Font-Size="9"
                    ControlStyle-Font-Size="10" Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="txt_grade" runat="server"
                            Font-Size="Small"
                            Text='<%# Eval("grade") %>' />

                </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="เกรดสุทธิ" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="txt_grade_criteria" runat="server" Font-Size="Small"
                            Text='<%# Eval("grade_criteria") %>' />

                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="true">
                    <ItemTemplate>

                        <asp:Label ID="lb_description" runat="server" Font-Size="Small" Width="100px" ForeColor="Red"
                            Text='<%# Eval("description") %>' />

                    </ItemTemplate>
                </asp:TemplateField> --%>



                <%--
                <asp:TemplateField HeaderText="รวมคะแนน(ทศนิยม)" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right"
                    HeaderStyle-Font-Size="9"
                    ControlStyle-Font-Size="10">
                    <ItemTemplate>

                        <asp:Label ID="txt_total_score_before" runat="server"
                            Font-Size="Small"
                            TextMode="Number"
                            Text='<%# Eval("total_score_before") %>' />

                </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="คงเหลือคะแนนสถิติ(ทศนิยม)" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="true">
                    <ItemTemplate>

                        <asp:Label ID="txt_score_balance_before" runat="server" Font-Size="Small"
                            Text='<%# Eval("score_balance_before") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="สรุปคะแนนประเมิน(ทศนิยม)" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10">
                    <ItemTemplate>

                        <asp:Label ID="txt_score_assessment_qty_before" runat="server" Font-Size="Small"
                            Text='<%# Eval("score_assessment_qty_before") %>' />

                    </ItemTemplate>
                </asp:TemplateField>--%>


            </Columns>
        </asp:GridView>

        <asp:GridView ID="GvExportExcel_1" runat="server" AutoGenerateColumns="false"
            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12 word-wrap"
            HeaderStyle-CssClass="info" GridLines="None" OnRowDataBound="gvRowDataBound">

            <EmptyDataTemplate>
                <div style="text-align: center">ไม่พบข้อมูล</div>
            </EmptyDataTemplate>
            <Columns>

                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="3%">
                    <ItemTemplate>

                        <asp:Label ID="lb_DataItemIndex" runat="server" CssClass="col-sm-12"
                            Text='<%# (Container.DataItemIndex +1) %>'>
                        </asp:Label>

                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="รหัส" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left"
                    HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" HeaderStyle-Width="5%">
                    <ItemTemplate>

                        <asp:Label ID="lb_emp_code" runat="server" CssClass="col-sm-12" Text='<%# Eval("emp_code") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="ชื่อ - สกุล" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="15%">
                    <ItemTemplate>

                        <asp:Label ID="lb_emp_name_th" runat="server" CssClass="col-sm-12" Width="130px"
                            Text='<%# Eval("emp_name_th") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="ตำแหน่ง" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="10%">
                    <ItemTemplate>

                        <asp:Label ID="lb_pos_name_th" runat="server" CssClass="col-sm-12"
                            Text='<%# Eval("pos_name_th") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="เริ่มงาน" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="5%">
                    <ItemTemplate>

                        <asp:Label ID="lb_emp_probation_date" runat="server" CssClass="col-sm-12"
                            Text='<%# Eval("emp_probation_date") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="CC" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left"
                    HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" HeaderStyle-Width="5%">
                    <ItemTemplate>

                        <asp:Label ID="lb_costcenter_no" runat="server" CssClass="col-sm-12"
                            Text='<%# Eval("costcenter_no") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="CC Name" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="5%">
                    <ItemTemplate>

                        <asp:Label ID="lb_costcenter_name" runat="server" CssClass="col-sm-12"
                            Text='<%# Eval("costcenter_name") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="อายุงาน" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="10%">
                    <ItemTemplate>

                        <asp:Label ID="lb_work_experience_qty" runat="server" CssClass="col-sm-12" Width="100px"
                            Text='<%# Eval("work_experience") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="การทำงานเป็นทีม" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="10%">
                    <ItemTemplate>

                        <asp:Label ID="txt_ownership_qty" runat="server" Font-Size="Small" Visible="true"
                            TextMode="Number" Width="70px" CssClass="form-control"
                            Text='<%# Eval("ownership_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="มุ่งมั่นสู่ความเป็นเสิศ" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="10%">
                    <ItemTemplate>


                        <asp:Label ID="txt_improvement_continuously_qty" runat="server" Font-Size="Small" Visible="true"
                            TextMode="Number" Width="70px" CssClass="form-control"
                            Text='<%# Eval("improvement_continuously_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="สร้างสรรค์สิ่งใหม่" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10">
                    <ItemTemplate>

                        <asp:Label ID="txt_commitment_qty" runat="server" Font-Size="Small" Visible="true"
                            TextMode="Number" Width="70px" CssClass="form-control"
                            Text='<%# Eval("commitment_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="รวมคะแนน" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10">
                    <ItemTemplate>

                        <asp:Label ID="txt_total_score" runat="server" Font-Size="Small" TextMode="Number"
                            Text='<%# Eval("total_score") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <%-- <asp:TemplateField HeaderText="ขาด" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Right"
                    HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="txt_missing_qty" runat="server" Font-Size="Small"
                            Text='<%# Eval("missing_qty") %>' />

                </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="ลาป่วย" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="txt_sick_Leave_qty" runat="server" Font-Size="Small"
                            Text='<%# Eval("sick_Leave_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="สายกลับก่อน (นาที)" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="txt_call_back_qty" runat="server" Font-Size="Small"
                            Text='<%# Eval("call_back_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="ตักเตือนด้วยวาจา (ครั้ง)" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="lb_admonish_speech" runat="server" Font-Size="Small"
                            Text='<%# Eval("admonish_speech") %>' />

                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="ใบเตือน (ครั้ง)" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="txt_warning_notice_qty" runat="server" Font-Size="Small" TextMode="Number"
                            Width="70px" CssClass="form-control" Text='<%# Eval("warning_notice_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="พักงาน (ครั้ง)" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="txt_work_break_qty" runat="server" Font-Size="Small" TextMode="Number"
                            Width="70px" CssClass="form-control" Text='<%# Eval("work_break_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="รวมหัก (ทั้งหมด)" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="txt_total_deduction" runat="server" Font-Size="Small"
                            Text='<%# Eval("total_deduction") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="สรุปคะแนนประเมิน" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="false">
                    <ItemTemplate>

                        <asp:Label ID="txt_score_assessment_qty" runat="server" Font-Size="Small"
                            Text='<%# Eval("score_assessment_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="true">
                    <ItemTemplate>

                        <asp:Label ID="lb_description" runat="server" Font-Size="Small" Width="100px" ForeColor="Red"
                            Text='<%# Eval("description") %>' />

                    </ItemTemplate>
                </asp:TemplateField> --%>


                <%--                <asp:TemplateField HeaderText="รวมคะแนน(ทศนิยม)" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right"
                    HeaderStyle-Font-Size="9"
                    ControlStyle-Font-Size="10">
                    <ItemTemplate>

                        <asp:Label ID="txt_total_score_before" runat="server"
                            Font-Size="Small"
                            TextMode="Number"
                            Text='<%# Eval("total_score_before") %>' />

                </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="คงเหลือคะแนนสถิติ(ทศนิยม)" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    Visible="true">
                    <ItemTemplate>

                        <asp:Label ID="txt_score_balance_before" runat="server" Font-Size="Small"
                            Text='<%# Eval("score_balance_before") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="สรุปคะแนนประเมิน(ทศนิยม)" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Right" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10">
                    <ItemTemplate>

                        <asp:Label ID="txt_score_assessment_qty_before" runat="server" Font-Size="Small"
                            Text='<%# Eval("score_assessment_qty_before") %>' />

                    </ItemTemplate>
                </asp:TemplateField>--%>


            </Columns>
        </asp:GridView>

        <asp:GridView ID="GvExportExcel_2" runat="server" AutoGenerateColumns="false"
            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12 word-wrap"
            HeaderStyle-CssClass="info" GridLines="None" OnRowDataBound="gvRowDataBound">

            <EmptyDataTemplate>
                <div style="text-align: center">ไม่พบข้อมูล</div>
            </EmptyDataTemplate>
            <Columns>

                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="3%">
                    <ItemTemplate>

                        <asp:Label ID="lb_DataItemIndex" runat="server" CssClass="col-sm-12"
                            Text='<%# (Container.DataItemIndex +1) %>'>
                        </asp:Label>

                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="รหัส" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left"
                    HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" HeaderStyle-Width="5%">
                    <ItemTemplate>

                        <asp:Label ID="lb_emp_code" runat="server" CssClass="col-sm-12" Text='<%# Eval("emp_code") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="ชื่อ - สกุล" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="15%">
                    <ItemTemplate>

                        <asp:Label ID="lb_emp_name_th" runat="server" CssClass="col-sm-12" Width="130px"
                            Text='<%# Eval("emp_name_th") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="ฝ่าย" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left"
                    HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" HeaderStyle-Width="10%">
                    <ItemTemplate>

                        <asp:Label ID="lb_dept_name_th" runat="server" CssClass="col-sm-12"
                            Text='<%# Eval("dept_name_th") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="แผนก" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left"
                    HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" HeaderStyle-Width="10%">
                    <ItemTemplate>

                        <asp:Label ID="lb_sec_name_th" runat="server" CssClass="col-sm-12"
                            Text='<%# Eval("sec_name_th") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="ตำแหน่ง" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="10%">
                    <ItemTemplate>

                        <asp:Label ID="lb_pos_name_th" runat="server" CssClass="col-sm-12"
                            Text='<%# Eval("pos_name_th") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="เริ่มงาน" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="5%">
                    <ItemTemplate>

                        <asp:Label ID="lb_emp_probation_date" runat="server" CssClass="col-sm-12"
                            Text='<%# Eval("emp_probation_date") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="CC" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left"
                    HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10" HeaderStyle-Width="5%">
                    <ItemTemplate>

                        <asp:Label ID="lb_costcenter_no" runat="server" CssClass="col-sm-12"
                            Text='<%# Eval("costcenter_no") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="CC Name" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="5%">
                    <ItemTemplate>

                        <asp:Label ID="lb_costcenter_name" runat="server" CssClass="col-sm-12"
                            Text='<%# Eval("costcenter_name") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="อายุงาน" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="10%">
                    <ItemTemplate>

                        <asp:Label ID="lb_work_experience_qty" runat="server" CssClass="col-sm-12" Width="100px"
                            Text='<%# Eval("work_experience_qty") %>'>

                        </asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="การทำงานเป็นทีม" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="10%">
                    <ItemTemplate>

                        <asp:Label ID="txt_ownership_qty" runat="server" Font-Size="Small" Visible="true"
                            TextMode="Number" Width="70px" CssClass="form-control"
                            Text='<%# Eval("ownership_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="มุ่งมั่นสู่ความเป็นเสิศ" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10"
                    HeaderStyle-Width="10%">
                    <ItemTemplate>


                        <asp:Label ID="txt_improvement_continuously_qty" runat="server" Font-Size="Small" Visible="true"
                            TextMode="Number" Width="70px" CssClass="form-control"
                            Text='<%# Eval("improvement_continuously_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="สร้างสรรค์สิ่งใหม่" HeaderStyle-CssClass="text-center"
                    ItemStyle-HorizontalAlign="Left" HeaderStyle-Font-Size="9" ControlStyle-Font-Size="10">
                    <ItemTemplate>

                        <asp:Label ID="txt_commitment_qty" runat="server" Font-Size="Small" Visible="true"
                            TextMode="Number" Width="70px" CssClass="form-control"
                            Text='<%# Eval("commitment_qty") %>' />

                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>



    <script type="text/javascript">
        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            //var dateToday = new Date();
            //currentTime = new Date();
            $('.datetimepicker-from').datetimepicker({

                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'day'),
                maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true

            });
            $('.datetimepicker-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                //}

            });
            $('.show-from-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'day'),
                maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true
            });
            $('.show-to-onclick').click(function () {
                $('.datetimepicker-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));

                //}
                //$('.txtSearchToHidden').val($('.datetimepicker-to').val());
                // <%-- $('#<%= txtSearchToHidden.ClientID %>').val($('.datetimepicker-to').val()); --%>
            });

            /* START Filter DatetimePicker Permission Temporary */
            $('.datetimepicker-filter-from').datetimepicker({
                format: 'DD/MM/YYYY',
                //ignoreReadonly: true
            });
            $('.datetimepicker-filter-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //    <%-- $('#<%= txtFilterIndexPermTempFromHidden.ClientID %>').val($('.datetimepicker-filter-from').val()); --%>
            });
            $('.show-filter-from-onclick').click(function () {
                $('.datetimepicker-filter-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-to').datetimepicker({
                format: 'DD/MM/YYYY',
                //ignoreReadonly: true
            });
            $('.show-filter-to-onclick').click(function () {
                $('.datetimepicker-filter-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                // <%-- $('#<%= txtFilterIndexPermTempToHidden.ClientID %>').val($('.datetimepicker-filter-to').val()); --%>
            });
            $('.datetimepicker-filter-only').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-only-onclick').click(function () {
                $('.datetimepicker-filter-only').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-only').on('dp.change', function (e) {
                // <%-- $('#<%= txtFilterIndexPermTempOnlyHidden.ClientID %>').val($('.datetimepicker-filter-only').val()); --%>
            });
            /* END Filter Datetimepicker Permission Temporary */
            /* START Filter Datetimepicker Permission Permanent */
            $('.datetimepicker-filter-perm-from').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.datetimepicker-filter-perm-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-perm-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-perm-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                // <%-- $('#<%= txtFilterIndexPermPermFromHidden.ClientID %>').val($('.datetimepicker-filter-perm-from').val()); --%>
            });
            $('.show-filter-perm-from-onclick').click(function () {
                $('.datetimepicker-filter-perm-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-to-onclick').click(function () {
                $('.datetimepicker-filter-perm-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-perm-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-perm-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                // <%-- $('#<%= txtFilterIndexPermPermToHidden.ClientID %>').val($('.datetimepicker-filter-perm-to').val()); --%>
            });
            $('.datetimepicker-filter-perm-only').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-only-onclick').click(function () {
                $('.datetimepicker-filter-perm-only').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-only').on('dp.change', function (e) {
                // <%-- $('#<%= txtFilterIndexPermPermOnlyHidden.ClientID %>').val($('.datetimepicker-filter-perm-only').val()); --%>
            });
            /* END Filter Datetimepicker Permission Permanent */
        });
    </script>

    <script type="text/javascript">
        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            encoding: "xml",
            theme: "modern",
            menubar: false,
            resize: false,
            statusbar: false,
            plugins: ["advlist autolink lists charmap preview hr anchor",
                "pagebreak code nonbreaking table contextmenu directionality paste"],
            toolbar1: "styleselect | bold italic underline | undo redo",
            toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            setup: function (editor) {
                editor.on('change', function () { tinymce.triggerSave(); });
            }
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            tinymce.remove(".tinymce");

            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "tinymce",
                encoding: "xml",
                theme: "modern",
                menubar: false,
                resize: false,
                statusbar: false,
                plugins: ["advlist autolink lists charmap preview hr anchor",
                    "pagebreak code nonbreaking table contextmenu directionality paste"],
                toolbar1: "styleselect | bold italic underline | undo redo",
                toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                setup: function (editor) {
                    editor.on('change', function () { tinymce.triggerSave(); });
                }
            });

            $(".multi").MultiFile();
        })
    </script>
</asp:Content>