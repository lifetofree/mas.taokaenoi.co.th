﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_qa_cims_m0_investigate : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    data_qa_cims _data_qa_cims = new data_qa_cims();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlCimsSetInvestigate = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetInvestigate"];
    static string _urlCimsGetInvestigate = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetInvestigate"];
    static string _urlCimsDeleteInvestigate = _serviceUrl + ConfigurationManager.AppSettings["urlCimsDeleteInvestigate"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ViewState["empIDX"] = Session["emp_idx"];
            Select_Investigate();


        }
    }
    #endregion Page Load

    #region initPage
    protected void initPage()
    {
        MultiView1.SetActiveView(View1);
        Select_Investigate();
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdAddInvestigate":

                Gv_select_investigate.EditIndex = -1;
                Select_Investigate();
                btn_addinvestigate.Visible = false;
                setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                Fv_Insert_Result.Visible = true;
                //Gv_select_place.Visible = false;


                break;

            case "Lbtn_submit_investigate":
                Insert_Investigate();
                Select_Investigate();
                btn_addinvestigate.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;

            case "Lbtn_cancel_investigate":
                btn_addinvestigate.Visible = true;
                Fv_Insert_Result.Visible = false;
                //  setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                //Gv_select_unit.Visible = false;
                // Gv_select_place.Visible = true;
                break;

            case "btnTodelete_investigate":
                // ViewState["DELETE_PLACE"] = int.Parse(cmdArg);

                int a = int.Parse(cmdArg);
                data_qa_cims Investigate_de = new data_qa_cims();
                qa_cims_m0_investigate_detail Investigate_sde = new qa_cims_m0_investigate_detail();

                Investigate_de.qa_cims_m0_investigate_list = new qa_cims_m0_investigate_detail[1];

                Investigate_sde.investigate_idx = int.Parse(a.ToString());

                Investigate_de.qa_cims_m0_investigate_list[0] = Investigate_sde;
                //  test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_de));
                Investigate_de = callServicePostMasterQACIMS(_urlCimsDeleteInvestigate, Investigate_de);

                Select_Investigate();
                //Gv_select_unit.Visible = false;
                btn_addinvestigate.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;
        }
    }
    #endregion btnCommand

    #region Custom Functions
    protected void Insert_Investigate()
    {
        FormView MvDocDetail_insert_qa_cims = (FormView)View1.FindControl("Fv_Insert_Result");
        TextBox tex_investigate_detail = (TextBox)MvDocDetail_insert_qa_cims.FindControl("txtinvestigate_detail");
        DropDownList dropD_status_investigate = (DropDownList)MvDocDetail_insert_qa_cims.FindControl("ddInvestigate");

        data_qa_cims Investigate_b = new data_qa_cims();
        qa_cims_m0_investigate_detail Investigate_s = new qa_cims_m0_investigate_detail();
        Investigate_b.qa_cims_m0_investigate_list = new qa_cims_m0_investigate_detail[1];
        Investigate_s.investigate_detail = tex_investigate_detail.Text;
        Investigate_s.cemp_idx = _emp_idx;
        Investigate_s.investigate_status = int.Parse(dropD_status_investigate.SelectedValue);
        Investigate_b.qa_cims_m0_investigate_list[0] = Investigate_s;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        Investigate_b = callServicePostMasterQACIMS(_urlCimsSetInvestigate, Investigate_b);

        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Unit_b.return_code));
        if (Investigate_b.return_code == 101)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            tex_investigate_detail.Text = String.Empty;
        }
    }

    protected void Select_Investigate()
    {
        data_qa_cims Investigate_b = new data_qa_cims();
        qa_cims_m0_investigate_detail Investigate_s = new qa_cims_m0_investigate_detail();

        Investigate_b.qa_cims_m0_investigate_list = new qa_cims_m0_investigate_detail[1];
        Investigate_b.qa_cims_m0_investigate_list[0] = Investigate_s;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
        Investigate_b = callServicePostMasterQACIMS(_urlCimsGetInvestigate, Investigate_b);
        setGridData(Gv_select_investigate, Investigate_b.qa_cims_m0_investigate_list);
        Gv_select_investigate.DataSource = Investigate_b.qa_cims_m0_investigate_list;
        Gv_select_investigate.DataBind();
    }
    #endregion Custom Functions

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "Gv_select_investigate":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbinvestigate_status = (Label)e.Row.Cells[3].FindControl("lbinvestigate_status");
                    Label investigate_statusOnline = (Label)e.Row.Cells[3].FindControl("investigate_statusOnline");
                    Label investigate_statusOffline = (Label)e.Row.Cells[3].FindControl("investigate_statusOffline");

                    ViewState["_investigate_status"] = lbinvestigate_status.Text;


                    if (ViewState["_investigate_status"].ToString() == "1")
                    {
                        investigate_statusOnline.Visible = true;
                    }
                    else if (ViewState["_investigate_status"].ToString() == "0")
                    {
                        investigate_statusOffline.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                    //btn_addplace.Visible = true;
                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);

                    //MultiView1.Visible = true;
                    //Gv_select_unit.Visible = false;
                    btn_addinvestigate.Visible = true;
                    Fv_Insert_Result.Visible = false;

                }



                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_investigate":
                Gv_select_investigate.EditIndex = e.NewEditIndex;
                Select_Investigate();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "Gv_select_investigate":

                var investigate_idx = (TextBox)Gv_select_investigate.Rows[e.RowIndex].FindControl("ID_investigate");
                var investigate_detail = (TextBox)Gv_select_investigate.Rows[e.RowIndex].FindControl("Name_investigate");
                var investigate_status = (DropDownList)Gv_select_investigate.Rows[e.RowIndex].FindControl("ddEdit_investigate");


                Gv_select_investigate.EditIndex = -1;

                data_qa_cims investigate_Update = new data_qa_cims();
                investigate_Update.qa_cims_m0_investigate_list = new qa_cims_m0_investigate_detail[1];
                qa_cims_m0_investigate_detail investigate_s = new qa_cims_m0_investigate_detail();
                investigate_s.investigate_idx = int.Parse(investigate_idx.Text);
                investigate_s.investigate_detail = investigate_detail.Text;
                investigate_s.investigate_status = int.Parse(investigate_status.SelectedValue);

                investigate_Update.qa_cims_m0_investigate_list[0] = investigate_s;

                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(unit_Update));

                investigate_Update = callServicePostMasterQACIMS(_urlCimsSetInvestigate, investigate_Update);

                Select_Investigate();

                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "Gv_select_investigate":
                Gv_select_investigate.EditIndex = -1;
                Select_Investigate();
                //btn_addplace.Visible = true;
                //SETFOCUS_ONTOP.Focus();
                //Gv_select_unit.Visible = false;
                btn_addinvestigate.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_investigate":
                Gv_select_investigate.PageIndex = e.NewPageIndex;
                Gv_select_investigate.DataBind();
                Select_Investigate();
                break;

        }
    }
    #endregion

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
    #endregion callService Functions
}