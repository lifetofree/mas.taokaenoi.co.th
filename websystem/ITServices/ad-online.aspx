﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="ad-online.aspx.cs" Inherits="websystem_ITServices_ad_online" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <!--*** START Menu ***-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left" runat="server">
                    <li id="_divMenuLiToDivIndex" runat="server" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">รายการทั่วไป <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <asp:LinkButton ID="_divMenuBtnToDivIndexPermPerm" runat="server"
                                    CommandName="_divMenuBtnToDivIndexPermPerm"
                                    OnCommand="btnCommand" Text="สิทธิ์ทั่วไป" />
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <asp:LinkButton ID="_divMenuBtnToDivIndexPermTemp" runat="server"
                                    CommandName="_divMenuBtnToDivIndexPermTemp"
                                    OnCommand="btnCommand" Text="สิทธิ์ชั่วคราว" />
                            </li>
                        </ul>
                    </li>
                    <li id="_divMenuLiToDivCreate" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivCreate" runat="server"
                            CommandName="_divMenuBtnToDivCreate"
                            OnCommand="btnCommand" Text="สร้างรายการขอใช้สิทธิ์" />
                    </li>
                    <li id="_divMenuLiToDivAddMore" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivAddmore" runat="server"
                            CommandName="_divMenuBtnToDivAddmore"
                            OnCommand="btnCommand" Text="ขอใช้สิทธิ์เพิ่มเติม" />
                    </li>
                    <li id="_divMenuLiToDivWaiting" runat="server" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <asp:Label ID="nav_approve" Font-Bold="true" runat="server"></asp:Label>
                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li>
                                <asp:LinkButton ID="_divMenBtnToDivWaitingPermPerm" runat="server"
                                    CommandName="_divMenuBtnToDivWaitingPermPerm"
                                    OnCommand="btnCommand" Text="สิทธิ์ทั่วไป" />
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <asp:LinkButton ID="_divMenuBtnToDivWaitingPermTemp" runat="server"
                                    CommandName="_divMenuBtnToDivWaitingPermTemp"
                                    OnCommand="btnCommand" Text="สิทธิ์ชั่วคราว" />
                            </li>
                        </ul>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right" runat="server">
                    <li id="_divMenuLiToImageFlow" runat="server" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Flow <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <asp:HyperLink ID="_divMenuBtnToFlowPermanent" runat="server"
                                    NavigateUrl="https://drive.google.com/file/d/1FIxUC3g_xnFU5Bn247Mr1gKIPCS7OOzd/view?usp=sharing"
                                    Target="_blank" Text="สิทธิ์ทั่วไป" />
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <asp:HyperLink ID="_divMenuBtnToFlowTemporary" runat="server"
                                    NavigateUrl="https://drive.google.com/file/d/0By7bA2Q3i-t2VTExSTdHYUF1LW8/view"
                                    Target="_blank" Text="สิทธิ์ชั่วคราว" />
                            </li>
                        </ul>
                    </li>
                    <li id="_divMenuLiToDocument" runat="server">
                        <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                            NavigateUrl="https://docs.google.com/document/d/16Lz397buV068dvzTJK9vhJpodD_keV7WRGhJhg_ZUyU"
                            Target="_blank" Text="คู่มือการใช้งาน" />
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!--*** END Menu ***-->

    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    </div>


    <!--*** Start DIV Index ***-->
    <div id="divIndex" runat="server" class="col-md-12" visible="false">
        <!-- Start Permission permanent area -->
        <div id="_divIndexPermPerm" runat="server" visible="false">
            <div class="alert alert-info f-s-14" role="alert">รายการทั่วไป : สิทธิ์ทั่วไป</div>
            <asp:LinkButton ID="btnIndexPermPermShowFilter" runat="server" CssClass="btn btn-primary m-b-10 f-s-14"
                OnCommand="btnCommand" CommandName="btnIndexPermPermShowFilter" Text="แสดงเครื่องมือค้นหา" Visible="true" />
            <asp:LinkButton ID="btnIndexPermPermHideFilter" runat="server" CssClass="btn btn-danger m-b-10 f-s-14"
                OnCommand="btnCommand" CommandName="btnIndexPermPermHideFilter" Text="ซ่อนเครื่องมือค้นหา" Visible="false" />
            <div id="_divIndexPermPerm_FilterTool" runat="server" visible="false">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="f-s-13">บริษัทผู้ขอใช้สิทธิ์</label>
                                <asp:DropDownList ID="ddlFilterIndexPermPermOrg" runat="server" CssClass="form-control" AutoPostBack="true"
                                    OnSelectedIndexChanged="onSelectedIndexChanged" />
                            </div>
                            <div class="form-group">
                                <label class="f-s-13">ฝ่ายผู้ขอใช้สิทธิ์</label>
                                <asp:DropDownList ID="ddlFilterIndexPermPermDept" runat="server" CssClass="form-control" AutoPostBack="true"
                                    OnSelectedIndexChanged="onSelectedIndexChanged" />
                            </div>
                            <div class="form-group">
                                <label class="f-s-13">แผนกผู้ขอใช้สิทธิ์</label>
                                <asp:DropDownList ID="ddlFilterIndexPermPermSec" runat="server" CssClass="form-control" AutoPostBack="true"
                                    OnSelectedIndexChanged="onSelectedIndexChanged" />
                            </div>
                            <div class="form-group">
                                <label class="f-s-13">ตำแหน่งผู้ขอใช้สิทธิ์</label>
                                <asp:DropDownList ID="ddlFilterIndexPermPermPos" runat="server" CssClass="form-control" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="f-s-13">รหัสเอกสาร</label>
                                <asp:TextBox ID="txtFilterIndexPermPermCodeId" runat="server" CssClass="form-control" placeholder="รหัสเอกสาร..." />
                            </div>
                            <div class="form-group">
                                <label class="f-s-13">รหัสพนักงาน/ชื่อ - สกุลผู้ขอใช้สิทธิ์</label>
                                <asp:TextBox ID="txtFilterIndexPermPermEmpCodeFullNameTHApplicant" runat="server" CssClass="form-control" placeholder="รหัสพนักงาน/ชื่อ - สกุลผู้ขอใช้สิทธิ์..." />
                            </div>
                            <div class="form-group">
                                <label class="f-s-13">สถานะรายการ</label>
                                <asp:DropDownList ID="ddlFilterIndexPermPermNode" runat="server" CssClass="form-control" />
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 p-l-0">
                                    <label class="f-s-13">วันที่ทำรายการ</label>
                                    <asp:DropDownList ID="ddlFilterIndexPermPermConditionDate" runat="server" CssClass="form-control" AutoPostBack="true"
                                        OnSelectedIndexChanged="onSelectedIndexChanged">
                                        <asp:ListItem Value="">-- เลือกเงื่อนไข --</asp:ListItem>
                                        <asp:ListItem Value="max">ตั้งแต่</asp:ListItem>
                                        <asp:ListItem Value="min">ไม่เกิน</asp:ListItem>
                                        <asp:ListItem Value="bet">ระหว่าง</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div id="divFilterIndexPermPermOnly" runat="server" class="col-md-8 p-l-0 p-r-0" visible="false">
                                    <label class="f-s-13">&nbsp;</label>
                                    <div class='input-group date'>
                                        <asp:HiddenField ID="txtFilterIndexPermPermOnlyHidden" runat="server" />
                                        <asp:TextBox ID="txtFilterIndexPermPermOnly" runat="server" placeholder="เลือกวันที่..."
                                            CssClass="form-control datetimepicker-filter-perm-only cursor-pointer" ReadOnly="true" />
                                        <span class="input-group-addon show-filter-perm-only-onclick">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div id="divFilterIndexPermPermFrom" runat="server" class="col-md-4 p-l-0" visible="true">
                                    <label class="f-s-13">&nbsp;</label>
                                    <div class='input-group date'>
                                        <asp:HiddenField ID="txtFilterIndexPermPermFromHidden" runat="server" />
                                        <asp:TextBox ID="txtFilterIndexPermPermFrom" runat="server" placeholder="ตั้งแต่วันที่..."
                                            CssClass="form-control datetimepicker-filter-perm-from cursor-pointer" ReadOnly="true" />
                                        <span class="input-group-addon show-filter-perm-from-onclick">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div id="divFilterIndexPermPermTo" runat="server" class="col-md-4 p-r-0 p-l-0" visible="true">
                                    <label class="f-s-13">&nbsp;</label>
                                    <div class='input-group date'>
                                        <asp:HiddenField ID="txtFilterIndexPermPermToHidden" runat="server" />
                                        <asp:TextBox ID="txtFilterIndexPermPermTo" runat="server" placeholder="ถึงวันที่..."
                                            CssClass="form-control datetimepicker-filter-perm-to cursor-pointer" ReadOnly="true" />
                                        <span class="input-group-addon show-filter-perm-to-onclick">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="pull-left">
                                <asp:LinkButton ID="btnFilterIndexPermPerm" OnCommand="btnCommand" CommandName="btnFilterIndexPermPerm" runat="server" CssClass="btn btn-primary" Text="ค้นหา" />
                                <asp:LinkButton ID="btnResetFilterIndexPermPerm" OnCommand="btnCommand" CommandName="btnResetFilterIndexPermPerm" runat="server" CssClass="btn btn-warning f-s-14" Text="ล้างค่า" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:GridView ID="gvIndexPermPerm"
                runat="server"
                AutoGenerateColumns="false"
                DataKeyNames="u0_perm_perm_idx"
                CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                HeaderStyle-CssClass="info"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="onRowDataBound"
                OnPageIndexChanging="onPageIndexChanging"
                AutoPostBack="False">
                <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div class="text-center">ไม่พบข้อมูล</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="รหัสเอกสาร" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="12%">
                        <ItemTemplate>
                            <asp:Label ID="u0PermPermCodeId" runat="server" Text='<%# Eval("u0_perm_perm_code_id") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียดผู้ขอใช้สิทธิ์" ItemStyle-CssClass="f-s-13 text-left" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="30%">
                        <ItemTemplate>
                            <p>
                                <b>รหัสพนักงาน:</b>
                                <asp:Label ID="u0EmpCode" runat="server" Text='<%# Eval("emp_code") %>' />
                            </p>
                            <p>
                                <b>ชื่อ - สกุล:</b>
                                <asp:Label ID="u0EmpNameTH" runat="server" Text='<%# Eval("emp_name_th") %>' />
                            </p>
                            <p>
                                <b>บริษัท:</b>
                                <asp:Label ID="u0EmpOrgTH" runat="server" Text='<%# Eval("org_name_th") %>' />
                            </p>
                            <p>
                                <b>ฝ่าย:</b>
                                <asp:Label ID="u0EmpDeptTH" runat="server" Text='<%# Eval("dept_name_th") %>' />
                            </p>
                            <p>
                                <b>แผนก:</b>
                                <asp:Label ID="u0EmpSecTH" runat="server" Text='<%# Eval("sec_name_th") %>' />
                            </p>
                            <b>ตำแหน่ง:</b>
                            <asp:Label ID="u0EmpPosTH" runat="server" Text='<%# Eval("pos_name_th") %>' />

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="วันที่ทำรายการ" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="12%">
                        <ItemTemplate>
                            <asp:Label ID="u0PermPermCreatedAt" runat="server" Text='<%# Eval("u0_perm_perm_created_at") %>' />
                            <asp:Label ID="u0node_perm" runat="server" Visible="false" Text='<%# Eval("u0_node_idx_ref") %>' />
                            <asp:Label ID="u0perm_rsecidx" runat="server" Visible="false" Text='<%# Eval("rsec_idx") %>' />
                            <asp:Label ID="u0perm_jobgradeidx" runat="server" Visible="false" Text='<%# Eval("jobgrade_idx") %>' />
                            <asp:Label ID="u0Createdpermby" runat="server" Visible="false" Text='<%# Eval("u0_perm_perm_created_by") %>' />


                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <asp:Label ID="u0PermPermNodeTo1" runat="server" Text='<%# Eval("status_to_1") %>' />
                            <asp:Label ID="connectText" runat="server"><br />โดย<br /></asp:Label>
                            <asp:Label ID="u0PermPermNodeTo2" runat="server" Text='<%# Eval("status_to_2") %>' />
                            <br />
                            <%--<asp:Label ID="lblwhoapprove" runat="server" CssClass="btn btn-xs btn-info"><i class="fa fa-user"></i></asp:Label>--%>
                            <asp:Label ID="lblwhoapprove" runat="server" CssClass="btn btn-xs btn-primary"><i class="glyphicon glyphicon-user"></i></asp:Label>


                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="9%">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnIndexPermPerm_ViewDetail" runat="server" CssClass="btn btn-info"
                                OnCommand="btnCommand" CommandName="btnIndexPermPerm_ViewDetail" CommandArgument='<%# Eval("u0_perm_perm_idx") %>'><i class="fa fa-file"></i>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="_divIndexPermPerm_Detail" runat="server" visible="false">
            <asp:LinkButton ID="btnBack_ToDivIndexPermPerm" runat="server" CssClass="btn btn-danger m-b-10 f-s-14" OnCommand="btnCommand"
                CommandName="btnBack_ToDivIndexPermPerm"><i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ</asp:LinkButton>
            <div class="panel panel-info">
                <div class="panel-heading">รายละเอียด</div>
                <div class="panel-body">
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleU0PermPermCode" runat="server" CssClass="f-s-13 f-bold" Text="รหัสเอกสาร :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueU0PermPermCode" runat="server" CssClass="f-s-13" />
                        <asp:Label ID="lblValueU0PermPermIDX_Perm" Visible="false" runat="server" CssClass="f-s-13" />

                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleU0PermPermCreatorEmpCode" runat="server" CssClass="f-s-13 f-bold" Text="รหัสพนักงานผู้ทำรายการ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueU0PermPermCreatorEmpCode" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleU0PermPermCreatorFullNameTH" runat="server" CssClass="f-s-13 f-bold" Text="ชื่อ - สกุลผู้ทำรายการ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueU0PermPermCreatorFullNameTH" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleU0PermPermCreatedAt" runat="server" CssClass="f-s-13 f-bold" Text="วันที่ทำรายการ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueU0PermPermCreatedAt" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleU0PermPermEmpCode" runat="server" CssClass="f-s-13 f-bold" Text="รหัสพนักงานผู้ขอใช้สิทธิ์ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueU0PermPermEmpCode" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleU0PermPermEmpFullNameTH" runat="server" CssClass="f-s-13 f-bold" Text="ชื่อ - สกุลผู้ขอใช้สิทธิ์ (TH) :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueU0PermPermEmpFullNameTH" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleU0PermPermEmpFullNameEN" runat="server" CssClass="f-s-13 f-bold" Text="ชื่อ - สกุลผู้ขอใช้สิทธิ์ (EN) :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueU0PermPermEmpFullNameEN" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleU0PermPermOrgNameTH" runat="server" CssClass="f-s-13 f-bold" Text="บริษัทผู้ขอใช้สิทธิ์ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueU0PermPermOrgNameTH" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleU0PermPermDeptNameTH" runat="server" CssClass="f-s-13 f-bold" Text="ฝ่ายผู้ขอใช้สิทธิ์ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueU0PermPermDeptNameTH" runat="server" CssClass="f-s-13" />
                        
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleU0PermPermSecNameTH" runat="server" CssClass="f-s-13 f-bold" Text="แผนกผู้ขอใช้สิทธิ์ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueU0PermPermSecNameTH" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleU0PermPermPosNameTH" runat="server" CssClass="f-s-13 f-bold" Text="ตำแหน่งผู้ขอใช้สิทธิ์ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueU0PermPermPosNameTH" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleU0PermPermCostCenter" runat="server" CssClass="f-s-13 f-bold" Text="Cost Center ผู้ขอใช้สิทธิ์ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueU0PermPermCostCenter" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleU0PermPermNode" runat="server" CssClass="f-s-13 f-bold" Text="สถานะรายการ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueU0PermPermNode" runat="server" CssClass="f-s-13" />
                        <asp:Label ID="lblvpnidx" runat="server" Visible="false" CssClass="f-s-13" />

                    </div>

                    <div id="divIndexU0PermPermDate" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="Label4" runat="server" CssClass="f-s-13 f-bold text-danger" Text="วันที่เริ่มใช้งาน :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueIndexU0PermPermDate" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>
                    <div id="divIndexU0PermPermPol" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleIndexU0PermPermPol" runat="server" CssClass="f-s-13 f-bold text-danger" Text="ระดับสิทธิ์ AD :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueIndexU0PermPermPol" runat="server" CssClass="f-s-13 text-danger" />
                            <asp:Label ID="lblValueIndexU0PermPermPolEx" runat="server" CssClass="btn btn-xs btn-info"><i class="fa fa-question"></i></asp:Label>
                        </div>
                    </div>

                    <div id="divIndexU0PermPermReason" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleIndexU0PermPermReason" runat="server" CssClass="f-s-13 f-bold text-danger" Text="เหตุผลในการขอสิทธิ์ AD(High) :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueIndexU0PermPermReason" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>


                    <div id="divIndexU0PermPermSap" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleIndexU0PermPermSap" runat="server" CssClass="f-s-13 f-bold text-danger" Text="User SAP ที่ใช้ :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueIndexU0PermPermSap" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>
                    <div id="divIndexU0PermPermEmail" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleIndexU0PermPermEmail" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Email ที่ใช้ :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueIndexU0PermPermEmailType" runat="server" CssClass="f-s-13 text-danger" />
                            <asp:Label ID="lblValueIndexU0PermPermEmailText" runat="server" CssClass="f-s-13 text-danger" />
                            <asp:Label ID="lblValueIndexU0PermPermTypeEmail" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>

                    <div id="divIndexU0PermPermEmail_Replace" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="Label3" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Email ทดแทน :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueIndexU0PermPermEmailReplace" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>

                    <%--  <div id="divIndexU0PermPermVpnSAP" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="Label8" runat="server" CssClass="f-s-13 f-bold text-danger" Text="เหตุผลในการขอสิทธิ์ VPN(SAP) :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueIndexU0PermPermVPNSap" runat="server" CssClass="f-s-13 text-danger" />

                        </div>
                    </div>

                    <div id="divIndexU0PermPermVpnBI" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="Label10" runat="server" CssClass="f-s-13 f-bold text-danger" Text="เหตุผลในการขอสิทธิ์ VPN(BI) :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueIndexU0PermPermVPNBI" runat="server" CssClass="f-s-13 text-danger" />

                        </div>
                    </div>


                    <div id="divIndexU0PermPermVpnExpress" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="Label12" runat="server" CssClass="f-s-13 f-bold text-danger" Text="เหตุผลในการขอสิทธิ์ VPN(Express) :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueIndexU0PermPermVPNExpress" runat="server" CssClass="f-s-13 text-danger" />

                        </div>
                    </div>--%>

                    <div id="divIndexU0PermPermad" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="Label11" runat="server" CssClass="f-s-13 f-bold text-danger" Text="สิทธิ์การใช้งาน AD :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueIndexU0PermPermad" runat="server" CssClass="f-s-13 text-danger" />

                        </div>
                    </div>

                    <div id="divIndexU0PermPermCommentHeaderEmpNew" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleIndexU0PermPermCommentHeaderEmpNew" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Comment ผู้อนุมัติ :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueIndexU0PermPermCommentHeaderEmpNew" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>
                    <div id="divIndexU0PermPermCommentDirectorEmpNew" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleIndexU0PermPermCommentDirectorEmpNew" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Comment ผู้อนุมัติ :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueIndexU0PermPermCommentDirectorEmpNew" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>
                    <div id="divIndexU0PermPermCommentDirectorHR" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleIndexU0PermPermCommentDirectorHR" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Comment ผู้อนุมัติ :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueIndexU0PermPermCommentDirectorHR" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>
                    <div id="divIndexU0PermPermCommentIT" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="Label13" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Comment IT :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueIndexU0PermPermCommentIT" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>
                    <div id="divIndexU0PermPermCommentHeaderIT" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleIndexU0PermPermCommentHeaderIT" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Comment หัวหน้า IT :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueIndexU0PermPermCommentHeaderIT" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>
                    <div id="divIndexU0PermPermCommentDirectorIT" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleIndexU0PermPermCommentDirectorIT" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Comment Director IT :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueIndexU0PermPermCommentDirectorIT" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>
                    <div id="divIndexU0PermPermCommentITClose" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleIndexU0PermPermCommentITClose" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Comment IT ปิดงาน:" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueIndexU0PermPermCommentITClose" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>
                    <div id="divIndexU1PermPermSubCommentIT" runat="server" visible="true" class="col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">รายการบันทึก Comment IT</div>
                            <table class="table table-striped f-s-12 table-empshift-responsive">
                                <asp:Repeater ID="rptIndexPermPermSubCmIT" runat="server">
                                    <HeaderTemplate>
                                        <tr>
                                            <th>วัน / เวลา</th>
                                            <th>ผู้บันทึก</th>
                                            <th>Comment</th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="วัน / เวลา"><%# Eval("u1_perm_perm_created_at_date") %> <%# Eval("u1_perm_perm_created_at_time") %></td>
                                            <td data-th="ผู้บันทึก"><%# Eval("emp_name_th") %></td>
                                            <td data-th="Comment"><%# Eval("u1_perm_perm_cm_it") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </div>
                    </div>

                    <!-- Start Permission VPN-->
                    <div class="col-lg-12">
                        <div class="col-lg-2">
                        </div>
                        <div class="col-lg-8">
                            <asp:GridView ID="GvShowVPNPerm"
                                runat="server"
                                AutoGenerateColumns="false"
                                Enabled="false"
                                DataKeyNames="vpnidx"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="info"
                                HeaderStyle-Height="40px"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center;">Data Cannot Be Found</div>
                                </EmptyDataTemplate>


                                <Columns>
                                    <asp:TemplateField HeaderText="เลือกรายการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:CheckBox ID="chkvpn" runat="server" />&nbsp;&nbsp;
                                        <asp:Literal ID="lblvpn_edit" runat="server" Text='<%# Eval("vpn_name") %>' /></small>
                                            <asp:Literal ID="litvpnidx" Visible="false" runat="server" Text='<%# Eval("vpnidx") %>' /></small>

                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="ระดับสิทธิ์ VPN" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:CheckBoxList ID="chksubvpn" runat="server" />
                                                <%--<asp:RadioButtonList ID="rdovpn_edit" runat="server"></asp:RadioButtonList>--%>
                                                <%--OnDataBound="onSelectedIndexChanged"--%>
                                                <asp:Literal ID="litvpn1idx" Visible="false" runat="server" /></small>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="litremark" runat="server" /></small>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%--                            <asp:TemplateField HeaderText="เอกสารแนบ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                <ItemTemplate>
                                    <small>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                                </Columns>

                            </asp:GridView>

                            <asp:GridView ID="gvFile" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="info"
                                OnRowDataBound="Master_RowDataBound"
                                BorderStyle="None"
                                CellSpacing="2"
                                Font-Size="Small">

                                <Columns>
                                    <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                        <ItemTemplate>
                                            <div class="col-lg-10">
                                                <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                            </div>
                                            <div class="col-lg-2">
                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                            </div>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>
                        <div class="col-lg-2">
                        </div>
                    </div>
                    <!-- End Permission VPN-->
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">ประวัติการดำเนินการ</div>
                <table class="table table-striped f-s-12 table-empshift-responsive">
                    <asp:Repeater ID="rptPermPermLog" runat="server">
                        <HeaderTemplate>
                            <tr>
                                <th>วัน / เวลา</th>
                                <th>ผู้ดำเนินการ</th>
                                <th>ดำเนินการ</th>
                                <th>ผลการดำเนินการ</th>
                            </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td data-th="วัน / เวลา"><%# Eval("perm_log_created_at_date") %> <%# Eval("perm_log_created_at_time") %></td>
                                <td data-th="ผู้ดำเนินการ"><%# Eval("emp_name_th") %></td>
                                <td data-th="ดำเนินการ"><%# Eval("node_name_1") %></td>
                                <td data-th="ผลการดำเนินการ"><%# Eval("status_from_2") %></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
                <div class="m-t-10 m-b-10"></div>
            </div>
        </div>
        <!-- End Permission permanent area -->
        <!-- Start Permission temporary area -->
        <div id="_divIndexPermTemp" runat="server" visible="false">
            <div class="alert alert-info f-s-14" role="alert">รายการทั่วไป : สิทธิ์ชั่วคราว</div>
            <asp:LinkButton ID="btnIndexPermTempShowFilter" runat="server" CssClass="btn btn-primary m-b-10 f-s-14"
                OnCommand="btnCommand" CommandName="btnIndexPermTempShowFilter" Text="แสดงเครื่องมือค้นหา" Visible="true" />
            <asp:LinkButton ID="btnIndexPermTempHideFilter" runat="server" CssClass="btn btn-danger m-b-10 f-s-14"
                OnCommand="btnCommand" CommandName="btnIndexPermTempHideFilter" Text="ซ่อนเครื่องมือค้นหา" Visible="false" />
            <div id="_divIndexPermTemp_FilterTool" runat="server" visible="false">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="f-s-13">บริษัท</label>
                                <asp:DropDownList ID="ddlFilterIndexPermTempOrg" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged" />
                            </div>
                            <div class="form-group">
                                <label class="f-s-13">ฝ่าย</label>
                                <asp:DropDownList ID="ddlFilterIndexPermTempDept" runat="server" CssClass="form-control" />
                            </div>
                            <div class="form-group">
                                <label class="f-s-13">สถานะรายการ</label>
                                <asp:DropDownList ID="ddlFilterIndexPermTempNode" runat="server" CssClass="form-control" />
                            </div>
                            <div class="form-group">
                                <div class="col-md-4 p-l-0">
                                    <label class="f-s-13">วันที่ทำรายการ</label>
                                    <asp:DropDownList ID="ddlFilterIndexPermTempConditionDate" runat="server" CssClass="form-control" AutoPostBack="true"
                                        OnSelectedIndexChanged="onSelectedIndexChanged">
                                        <asp:ListItem Value="">-- เลือกเงื่อนไข --</asp:ListItem>
                                        <asp:ListItem Value="max">ตั้งแต่</asp:ListItem>
                                        <asp:ListItem Value="min">ไม่เกิน</asp:ListItem>
                                        <asp:ListItem Value="bet">ระหว่าง</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div id="divFilterIndexPermTempOnly" runat="server" class="col-md-8 p-l-0 p-r-0" visible="false">
                                    <label class="f-s-13">&nbsp;</label>
                                    <div class='input-group date'>
                                        <asp:HiddenField ID="txtFilterIndexPermTempOnlyHidden" runat="server" />
                                        <asp:TextBox ID="txtFilterIndexPermTempOnly" runat="server" placeholder="เลือกวันที่..."
                                            CssClass="form-control datetimepicker-filter-only cursor-pointer" ReadOnly="true" />
                                        <span class="input-group-addon show-filter-only-onclick">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div id="divFilterIndexPermTempFrom" runat="server" class="col-md-4 p-l-0" visible="true">
                                    <label class="f-s-13">&nbsp;</label>
                                    <div class='input-group date'>
                                        <asp:HiddenField ID="txtFilterIndexPermTempFromHidden" runat="server" />
                                        <asp:TextBox ID="txtFilterIndexPermTempFrom" runat="server" placeholder="ตั้งแต่วันที่..."
                                            CssClass="form-control datetimepicker-filter-from cursor-pointer" ReadOnly="true" />
                                        <span class="input-group-addon show-filter-from-onclick">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <div id="divFilterIndexPermTempTo" runat="server" class="col-md-4 p-r-0 p-l-0" visible="true">
                                    <label class="f-s-13">&nbsp;</label>
                                    <div class='input-group date'>
                                        <asp:HiddenField ID="txtFilterIndexPermTempToHidden" runat="server" />
                                        <asp:TextBox ID="txtFilterIndexPermTempTo" runat="server" placeholder="ถึงวันที่..."
                                            CssClass="form-control datetimepicker-filter-to cursor-pointer" ReadOnly="true" />
                                        <span class="input-group-addon show-filter-to-onclick">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="f-s-13">รหัสเอกสาร</label>
                                <asp:TextBox ID="txtFilterIndexPermTempCodeId" runat="server" CssClass="form-control" placeholder="รหัสเอกสาร..." />
                            </div>
                            <div class="form-group">
                                <label class="f-s-13">ชื่อผู้ทำรายการ</label>
                                <asp:TextBox ID="txtFilterIndexPermTempCreatorName" runat="server" CssClass="form-control" placeholder="ชื่อผู้ทำรายการ..." />
                            </div>
                            <div class="form-group">
                                <label class="f-s-13">บริษัทที่มาติดต่อ</label>
                                <asp:TextBox ID="txtFilterIndexPermTempCompName" runat="server" CssClass="form-control" placeholder="บริษัทที่มาติดต่อ..." />
                            </div>
                            <div class="form-group">
                                <label class="f-s-13">รหัสบัตรประชาชน/ชื่อ ผู้มาติดต่อ</label>
                                <asp:TextBox ID="txtFilterIndexPermTempContact" runat="server" CssClass="form-control" placeholder="รหัสบัตรประชาชน/ชื่อ ผู้มาติดต่อ..." />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="pull-left">
                                <asp:LinkButton ID="btnFilterIndexPermTemp" OnCommand="btnCommand" CommandName="btnFilterIndexPermTemp" runat="server" CssClass="btn btn-primary" Text="ค้นหา" />
                                <asp:LinkButton ID="btnResetFilterIndexPermTemp" OnCommand="btnCommand" CommandName="btnResetFilterIndexPermTemp" runat="server" CssClass="btn btn-warning f-s-14" Text="ล้างค่า" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:GridView ID="gvIndexPermTemp"
                runat="server"
                AutoGenerateColumns="false"
                DataKeyNames="u0_perm_temp_idx"
                CssClass="table table-bordered table-striped table-responsive col-md-12 footable"
                HeaderStyle-CssClass="info"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="onRowDataBound"
                OnPageIndexChanging="onPageIndexChanging"
                AutoPostBack="False">
                <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div class="text-center">ไม่พบข้อมูล</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="รหัสเอกสาร" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="12%">
                        <ItemTemplate>
                            <asp:Label ID="u0PermTempCodeId" runat="server" Text='<%# Eval("u0_perm_temp_code_id") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียดผู้ทำรายการ" ItemStyle-CssClass="f-s-13 text-left" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="30%">
                        <ItemTemplate>
                            <p>
                                <b>รหัสพนักงาน:</b>
                                <asp:Label ID="u0EmpCode" runat="server" Text='<%# Eval("emp_code") %>' />
                            </p>
                            <p>
                                <b>ชื่อ - สกุล:</b>
                                <asp:Label ID="u0EmpNameTH" runat="server" Text='<%# Eval("emp_name_th") %>' />
                            </p>
                            <p>
                                <b>บริษัท:</b>
                                <asp:Label ID="u0EmpOrgTH" runat="server" Text='<%# Eval("org_name_th") %>' />
                            </p>
                            <p>
                                <b>ฝ่าย:</b>
                                <asp:Label ID="u0EmpDeptTH" runat="server" Text='<%# Eval("dept_name_th") %>' />
                            </p>
                            <b>วันที่ทำรายการ:</b>
                            <asp:Label ID="u0CreatedAt" runat="server" Text='<%# Eval("u0_perm_temp_created_at") %>' />
                            <asp:Label ID="u0node_temp" runat="server" Visible="false" Text='<%# Eval("u0_node_idx_ref") %>' />
                            <asp:Label ID="u0temp_rsecidx" runat="server" Visible="false" Text='<%# Eval("rsec_idx") %>' />
                            <asp:Label ID="u0temp_jobgradeidx" runat="server" Visible="false" Text='<%# Eval("jobgrade_idx") %>' />
                            <asp:Label ID="u0Createdtempby" runat="server" Visible="false" Text='<%# Eval("u0_perm_temp_created_by") %>' />



                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียดการติดต่อ" ItemStyle-CssClass="f-s-13 text-left" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="24%">
                        <ItemTemplate>
                            <p>
                                <b>ชื่อบริษัท/หน่วยงาน:</b>
                                <asp:Label ID="ulblU0PermTempCompName" runat="server" Text='<%# Eval("u0_perm_temp_comp_name") %>' />
                            </p>
                            <p>
                                <b>เหตุผลที่ขอ:</b>
                                <asp:Label ID="lblU0PermTempReason" runat="server" Text='<%# Eval("u0_perm_temp_detail") %>' />
                            </p>
                            <p>
                                <b>จำนวน:</b>
                                <asp:Label ID="lblU1PermTempAmountContact" runat="server" Text='<%# Eval("u1_perm_temp_contact_amount") %>' />
                                คน
                            </p>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <asp:Label ID="u0PermTempNodeTo1" runat="server" Text='<%# Eval("status_to_1") %>' />
                            <asp:Label ID="connectText" runat="server"><br />โดย<br /></asp:Label>
                            <asp:Label ID="u0PermTempNodeTo2" runat="server" Text='<%# Eval("status_to_2") %>' />

                            <br />
                            <asp:Label ID="lblwhoapprove" runat="server" CssClass="btn btn-xs btn-primary"><i class="glyphicon glyphicon-user"></i></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="9%">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnIndexPermTemp_ViewDetail" runat="server" CssClass="btn btn-info"
                                OnCommand="btnCommand" CommandName="btnIndexPermTemp_ViewDetail" CommandArgument='<%# Eval("u0_perm_temp_idx") %>'>
                        <i class="fa fa-file"></i>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="_divIndexPermTemp_Detail" runat="server" visible="false">
            <asp:LinkButton ID="btnBack_ToDivIndexPermTemp" runat="server" CssClass="btn btn-danger m-b-10 f-s-14" OnCommand="btnCommand"
                CommandName="btnBack_ToDivIndexPermTemp"><i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ</asp:LinkButton>
            <div class="panel panel-info">
                <div class="panel-heading">รายละเอียด</div>
                <div class="panel-body">
                    <div class="col-md-6 word-wrap">
                        <div class="col-md-5 m-b-5">
                            <asp:Label ID="lblTitleU0PermTempCode" runat="server" CssClass="f-s-13 f-bold" Text="รหัสเอกสาร" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueU0PermTempCode" runat="server" CssClass="f-s-13" />
                            <asp:Label ID="lblValueU0PermTempIDX_Temp" Visible="false" runat="server" CssClass="f-s-13" />

                        </div>
                        <div class="col-md-5 m-b-5">
                            <asp:Label ID="lblTitleU0PermTempCreatorName" runat="server" CssClass="f-s-13 f-bold" Text="ผู้ทำรายการ" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueU0PermTempCreatorName" runat="server" CssClass="f-s-13" />
                        </div>
                        <div class="col-md-5 m-b-5">
                            <asp:Label ID="lblTitleU0PermTempCreatorOrg" runat="server" CssClass="f-s-13 f-bold" Text="บริษัท" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueU0PermTempCreatorOrg" runat="server" CssClass="f-s-13" />
                        </div>
                        <div class="col-md-5 m-b-5">
                            <asp:Label ID="lblTitleU0PermTempCreatorDept" runat="server" CssClass="f-s-13 f-bold" Text="ฝ่าย" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueU0PermTempCreatorDept" runat="server" CssClass="f-s-13" />
                        </div>
                        <div class="col-md-5 m-b-5">
                            <asp:Label ID="lblTitleU0PermTempCreatorSec" runat="server" CssClass="f-s-13 f-bold" Text="แผนก" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueU0PermTempCreatorSec" runat="server" CssClass="f-s-13" />
                        </div>
                        <div class="col-md-5 m-b-5">
                            <asp:Label ID="lblTitleU0PermTempCreatorPos" runat="server" CssClass="f-s-13 f-bold" Text="ตำแหน่ง" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueU0PermTempCreatorPos" runat="server" CssClass="f-s-13" />
                        </div>
                        <div class="col-md-5 m-b-5">
                            <asp:Label ID="lblTitleU0PermTempCreatedAt" runat="server" CssClass="f-s-13 f-bold" Text="วันที่ทำรายการ" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueU0PermTempCreatedAt" runat="server" CssClass="f-s-13" />
                        </div>
                        <div class="col-md-5 m-b-5">
                            <asp:Label ID="lblTitleU0PermTempCompName" runat="server" CssClass="f-s-13 f-bold" Text="บริษัทที่มาติดต่อ" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueU0PermTempCompName" runat="server" CssClass="f-s-13" />
                        </div>
                        <div class="col-md-5 m-b-5">
                            <asp:Label ID="lblTitleU0PermTempReason" runat="server" CssClass="f-s-13 f-bold" Text="เหตุผลการขอใช้สิทธิ์" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueU0PermTempReason" runat="server" CssClass="f-s-13" />
                        </div>
                        <div id="groupDate" runat="server" visible="false">
                            <div class="col-md-5 m-b-5">
                                <asp:Label ID="lblTitleU0DateStart" runat="server" CssClass="f-s-13 f-bold" Text="วันที่เริ่มใช้สิทธิ์" />
                            </div>
                            <div class="col-md-7 m-b-5">
                                <asp:Label ID="lblValueU0DateStart" runat="server" CssClass="f-s-13" />
                            </div>
                            <div class="col-md-5 m-b-5">
                                <asp:Label ID="lblTitleU0DateEnd" runat="server" CssClass="f-s-13 f-bold" Text="วันที่สิ้นสุดใช้สิทธิ์" />
                            </div>
                            <div class="col-md-7 m-b-5">
                                <asp:Label ID="lblValueU0DateEnd" runat="server" CssClass="f-s-13" />
                            </div>
                        </div>
                        <div class="col-md-5 m-b-5">
                            <asp:Label ID="lblTitleAmountContact" runat="server" CssClass="f-s-13 f-bold" Text="จำนวน" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueAmountContact" runat="server" CssClass="f-s-13" />
                        </div>
                        <div class="col-md-5 m-b-5">
                            <asp:Label ID="lblTitleU0PermTempNode" runat="server" CssClass="f-s-13 f-bold" Text="สถานะรายการ" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueU0PermTempNode" runat="server" CssClass="f-s-13" />
                            <asp:Label ID="lblvpnidxtemp" Visible="false" runat="server" CssClass="f-s-13" />


                        </div>
                        <div id="_divIndexDetailCmHeaderIT" runat="server">
                            <div class="col-md-5 m-b-5">
                                <asp:Label ID="lblTitleU0PermTempCommentHeaderIT" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Comment หัวหน้า IT" />
                            </div>
                            <div class="col-md-7 m-b-5">
                                <asp:Label ID="lblValueU0PermTempCommentHeaderIT" runat="server" CssClass="f-s-13 text-danger" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 word-wrap">
                        <div class="panel panel-info">
                            <div class="panel-heading">รายชื่อผู้มาติดต่อ</div>
                            <table class="table table-striped f-s-12 table-empshift-responsive">
                                <asp:Repeater ID="rptU1PermTempContactsListCompareDate" runat="server">
                                    <HeaderTemplate>
                                        <tr>
                                            <th>รหัสบัตรประชาชน/ บัตรประจำตัวคนต่างชาติ</th>
                                            <th>ชื่อ - สกุล</th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="รหัสบัตรประชาชน"><%# idCardPattern((string)Eval("u1_perm_temp_idcard"), true) %></td>
                                            <td data-th="ชื่อ - สกุล"><%# Eval("u1_perm_temp_fullname") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <asp:Repeater ID="rptU1PermTempContactsList" runat="server">
                                    <HeaderTemplate>
                                        <tr>
                                            <th>รหัสบัตรประชาชน</th>
                                            <th>ชื่อ - สกุล</th>
                                            <th>วันที่เริ่มใช้สิทธิ์</th>
                                            <th>วันที่สิ้นสุดใช้สิทธิ์</th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="รหัสบัตรประชาชน"><%# idCardPattern((string)Eval("u1_perm_temp_idcard"), true) %></td>
                                            <td data-th="ชื่อ - สกุล"><%# Eval("u1_perm_temp_fullname") %></td>
                                            <td data-th="วันที่เริ่มใช้สิทธิ์"><%# Eval("u1_perm_temp_start") %></td>
                                            <td data-th="วันที่สิ้นสุดใช้สิทธิ์"><%#Eval("u1_perm_temp_end") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                            <div class="m-t-10 m-b-10"></div>

                        </div>
                    </div>
                    <!-- End Permission VPN-->
                    <!-- Start Permission TEMP VPN-->

                    <div class="col-md-6 word-wrap" id="divshowvpntemp" runat="server">
                        <div class="panel panel-info">
                            <div class="panel-heading">รายการขอใข้สิทธิ์ VPN</div>
                            <div class="panel-body">
                                <asp:GridView ID="GvShowVPNTemp"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    Enabled="false"
                                    DataKeyNames="vpnidx"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="info"
                                    HeaderStyle-Height="40px"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center;">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>


                                    <Columns>
                                        <asp:TemplateField HeaderText="เลือกรายการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:CheckBox ID="chkvpn_temp" runat="server" />&nbsp;&nbsp;
                                        <asp:Literal ID="lblvpntemp_temp" runat="server" Text='<%# Eval("vpn_name") %>' /></small>
                                                <asp:Literal ID="litvpnidx_temp" Visible="false" runat="server" Text='<%# Eval("vpnidx") %>' /></small>

                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ระดับสิทธิ์ VPN" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:CheckBoxList ID="chksubvpn_temp" runat="server" />
                                                    <%--<asp:RadioButtonList ID="rdovpn_edit" runat="server"></asp:RadioButtonList>--%>
                                                    <asp:Literal ID="litvpn1idx_temp" Visible="false" runat="server" /></small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="litremark_temp" runat="server" /></small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>

                                </asp:GridView>



                            </div>
                        </div>
                    </div>

                    <div class="col-md-6 word-wrap"></div>

                    <div class="col-md-6 word-wrap">
                        <asp:GridView ID="gvFileTemp" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive"
                            HeaderStyle-CssClass="info"
                            OnRowDataBound="Master_RowDataBound"
                            BorderStyle="None"
                            CellSpacing="2"
                            Font-Size="Small">

                            <Columns>
                                <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                    <ItemTemplate>
                                        <div class="col-lg-10">
                                            <asp:Literal ID="ltFileName11_temp" runat="server" Text='<%# Eval("FileName") %>' />
                                        </div>
                                        <div class="col-lg-2">
                                            <asp:HyperLink runat="server" ID="btnDL11_temp" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                            <asp:HiddenField runat="server" ID="hidFile11_temp" Value='<%# Eval("Download") %>' />
                                        </div>

                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>
                    </div>

                    <!-- End Permission TEMP VPN-->
                </div>
            </div>
            <div class="panel panel-info">
                <div class="panel-heading">ประวัติการดำเนินการ</div>
                <table class="table table-striped f-s-12 table-empshift-responsive">
                    <asp:Repeater ID="rptPermTempLog" runat="server">
                        <HeaderTemplate>
                            <tr>
                                <th>วัน / เวลา</th>
                                <th>ผู้ดำเนินการ</th>
                                <th>ดำเนินการ</th>
                                <th>ผลการดำเนินการ</th>
                            </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td data-th="วัน / เวลา"><%# Eval("perm_log_created_at_date") %> <%# Eval("perm_log_created_at_time") %></td>
                                <td data-th="ผู้ดำเนินการ"><%# Eval("emp_name_th") %></td>
                                <td data-th="ดำเนินการ"><%# Eval("node_name_1") %></td>
                                <td data-th="ผลการดำเนินการ"><%# Eval("status_from_2") %></td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
                <div class="m-t-10 m-b-10"></div>
            </div>
        </div>
        <!-- End Permission temporary area -->
    </div>
    <!--*** End DIV Index ***-->
    <!--*** Start DIV Create ***-->

    <asp:UpdatePanel ID="divCreate" runat="server" class="col-md-12" Visible="false">
        <ContentTemplate>

            <%--<div id="divCreate" runat="server" class="col-md-12" visible="false">--%>
            <div class="panel panel-info">
                <div class="panel-heading">สร้างรายการขอใช้สิทธิ์</div>
                <div class="panel-body col-md-12">
                    <div class="col-md-12">
                        <asp:TextBox ID="txtCreatorOrgNameTH" runat="server" Visible="false" />
                        <asp:TextBox ID="txtCreatorDeptNameTH" runat="server" Visible="false" />

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="f-s-13">รหัสพนักงานผู้ทำรายการ</label>
                                <asp:TextBox ID="txtCreatorEmpCode" runat="server" CssClass="form-control" Enabled="false" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="f-s-13">ชื่อ - นามสกุลผู้ทำรายการ</label>
                                <asp:TextBox ID="txtCreatorFullName" runat="server" CssClass="form-control" Enabled="false" />
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="f-s-13">รูปแบบสิทธิ์</label>
                        </div>
                        <div class="col-md-10 m-b-10">
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" Visible="true">
                                <ContentTemplate>
                                    <asp:RadioButtonList ID="rdoPermissionType" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged">
                                    </asp:RadioButtonList>


                                    <div class="col-md-12" id="div_showfile" runat="server" style="color: transparent;">
                                        <div class="form-group">
                                            <label class="f-s-13"></label>
                                            <asp:FileUpload ID="FileUpload11" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|png|pdf" />


                                        </div>
                                    </div>

                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="rdoPermissionType" EventName="SelectedIndexChanged" />
                                    <asp:PostBackTrigger ControlID="btnInsertPermPerm" />
                                </Triggers>
                            </asp:UpdatePanel>


                        </div>

                    </div>
                    <!-- Start DIV Permission Permanent -->


                    <div id="_divCreate_PermPerm" runat="server">


                        <div class="col-md-12">
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server" Visible="true">
                                <ContentTemplate>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">รหัสพนักงาน</label>
                                            <asp:TextBox ID="txtEmpNewEmpIdxHidden" runat="server" Visible="false" />
                                            <asp:TextBox ID="txtEmpNewJobGradeLevelHidden" runat="server" Visible="false" />
                                            <asp:TextBox ID="txtEmpNewEmpCode" runat="server" CssClass="form-control" placeholder="รหัสพนักงาน..."
                                                OnTextChanged="onTextChanged" AutoPostBack="true" MaxLength="8" autocomplete="off" />
                                        </div>
                                    </div>

                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="txtEmpNewEmpCode" EventName="TextChanged" />
                                </Triggers>
                            </asp:UpdatePanel>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="f-s-13">บริษัท</label>
                                    <asp:TextBox ID="txtEmpNewOrgNameTHHidden" runat="server" Visible="false" />
                                    <asp:TextBox ID="txtEmpNewOrgNameTH" runat="server" CssClass="form-control" placeholder="บริษัท..." Enabled="false" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="f-s-13">ชื่อ - สกุล (TH)</label>
                                    <asp:TextBox ID="txtEmpNewFullNameTHHidden" runat="server" Visible="false" />
                                    <asp:TextBox ID="txtEmpNewFullNameTH" runat="server" CssClass="form-control" placeholder="ชื่อ - สกุล (TH)..." Enabled="false" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="f-s-13">ฝ่าย</label>
                                    <asp:TextBox ID="txtEmpNewDeptNameTHHidden" runat="server" Visible="false" />
                                    <asp:TextBox ID="txtEmpNewDeptNameTH" runat="server" CssClass="form-control" placeholder="ฝ่าย..." Enabled="false" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="f-s-13">ชื่อ - สกุล (EN)</label>
                                    <asp:TextBox ID="txtEmpNewFullNameENHidden" runat="server" Visible="false" />
                                    <asp:TextBox ID="txtEmpNewFullNameEN" runat="server" CssClass="form-control" placeholder="ชื่อ - สกุล (EN)..." Enabled="false" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="f-s-13">แผนก</label>
                                    <asp:TextBox ID="txtEmpNewSecNameTHHidden" runat="server" Visible="false" />
                                    <asp:TextBox ID="txtEmpNewSecNameTH" runat="server" CssClass="form-control" placeholder="แผนก..." Enabled="false" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="f-s-13">Cost Center</label>
                                    <asp:TextBox ID="txtEmpNewCostCenterNoHidden" runat="server" Visible="false" />
                                    <asp:TextBox ID="txtEmpNewCostCenterNo" runat="server" CssClass="form-control" placeholder="Cost Center..." Enabled="false" />
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="f-s-13">ตำแหน่ง</label>
                                    <asp:TextBox ID="txtEmpNewPosNameTHHidden" runat="server" Visible="false" />
                                    <asp:TextBox ID="txtEmpNewPosNameTH" runat="server" CssClass="form-control" placeholder="ตำแหน่ง..." Enabled="false" />
                                </div>
                            </div>

                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="f-s-13">วันที่เริ่มใช้</label>
                                    <div class='input-group date'>
                                        <asp:TextBox ID="AddStartdate_add" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10"
                                        ValidationGroup="saveInsertPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกวันที่เริ่มใช้"
                                        Display="None" SetFocusOnError="true" ControlToValidate="AddStartdate_add" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160"
                                        PopupPosition="BottomLeft" />
                                </div>
                            </div>
                        </div>

                        <asp:UpdatePanel ID="UpdatePanel6" runat="server" Visible="true">
                            <ContentTemplate>
                                <div class="col-md-12">
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label class="f-s-13">ระดับสิทธิ์ AD</label>
                                            <asp:DropDownList ID="ddladonline" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged" runat="server" CssClass="form-control" Width="100%" />

                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label>&nbsp;</label>
                                            <div class="clearfix"></div>

                                            <asp:Label ID="lblshowad" runat="server" CssClass="btn btn-info col-md-12"><i class="fa fa-question fa-lg"></i></asp:Label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                            </ContentTemplate>
                            <Triggers>
                                <%-- <asp:AsyncPostBackTrigger ControlID="rdoPermissionType" EventName="SelectedIndexChanged" />
                                <asp:PostBackTrigger ControlID="btnInsertPermPerm" />--%>
                            </Triggers>
                        </asp:UpdatePanel>


                        <%--<div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="f-s-13">ระดับสิทธิ์ AD</label>
                                    <div class="form-inline">
                                        <div class="form-group col-xs-11 row">
                                            
                                        </div>

                                        <div class="form-group col-xs-1 row">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>--%>

                        <div class="col-md-12" runat="server" id="div_ad" visible="false">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%--<label class="f-s-13">เหตุผลในการขอสิทธิ์ AD(</label> --%>
                                    <asp:Label runat="server" ID="showreasonad" CssClass="f-s-13"></asp:Label>
                                    <asp:TextBox ID="txtremark" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7"
                                        ValidationGroup="saveInsertPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณากรอกเหตุผลในการขอสิทธิ์ AD(High)"
                                        Display="None" SetFocusOnError="true" ControlToValidate="txtremark" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160"
                                        PopupPosition="BottomLeft" />
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="f-s-13">User SAP ที่ใช้</label>
                                    <asp:TextBox ID="txtsap_add" runat="server" CssClass="form-control" placeholder="User SAP ที่ใช้..." MaxLength="50" />
                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                                    ValidationGroup="saveInsertPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณากรอก User SAP ที่ใช้"
                                    Display="None" SetFocusOnError="true" ControlToValidate="txtsap_add" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160"
                                    PopupPosition="BottomLeft" />--%>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="f-s-13">Email ที่ขอใช้</label>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <asp:RadioButtonList ID="rdomail" runat="server" CssClass="radio-list-inline-emps"
                                                AutoPostBack="true" RepeatDirection="Vertical" OnSelectedIndexChanged="onSelectedIndexChanged">
                                                <asp:ListItem Value="center">Email กลาง</asp:ListItem>
                                                <asp:ListItem Value="private">Email ส่วนตัว</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                    ValidationGroup="saveInsertPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกสิทธิ์การใช้ Email"
                                    Display="None" SetFocusOnError="true" ControlToValidate="rdomail" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160"
                                    PopupPosition="BottomLeft" />--%>
                                </div>
                            </div>
                        </div>

                        <div runat="server" id="div_chooseemail" visible="false">
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="f-s-13">ประเภทอีเมล์</label>
                                        <div class="row">
                                            <div class="col-md-7 p-l-0 p-r-0">
                                                <asp:DropDownList ID="ddlemail" runat="server" CssClass="form-control" ValidationGroup="saveInsertPermPerm" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged">
                                                    <asp:ListItem Value="0" Text="กรุณาเลือกประเภทอีเมล์"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidators7"
                                                    ValidationGroup="saveInsertPermPerm" InitialValue="0" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกประเภทอีเมล์"
                                                    Display="None" SetFocusOnError="true" ControlToValidate="ddlemail" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidators7" Width="160"
                                                    PopupPosition="BottomLeft" />
                                            </div>
                                            <%--   <div class="col-md-5 p-r-0" runat="server" id="divshowmail" visible="false">
                                            <asp:TextBox ID="txtemailreplace" runat="server" CssClass="form-control" placeholder="Email ทดแทน..." MaxLength="50" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatoer8"
                                                ValidationGroup="saveInsertPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณากรอก Email ทดแทน"
                                                Display="None" SetFocusOnError="true" ControlToValidate="txtemailreplace" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatoer8" Width="
                                            160"
                                                PopupPosition="BottomLeft" />
                                        </div>--%>

                                            <div class="col-md-5 p-r-0" runat="server" id="divemail1or3" visible="false">
                                                <asp:DropDownList ID="ddl1or3" runat="server" ValidationGroup="saveInsertPermPerm" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="กรุณาเลือกอีเมล์"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11"
                                                    ValidationGroup="saveInsertPermPerm" InitialValue="0" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกอีเมล์"
                                                    Display="None" SetFocusOnError="true" ControlToValidate="ddl1or3" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160"
                                                    PopupPosition="BottomLeft" />
                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="col-md-12" id="divemailad" runat="server" visible="false">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <%-- <div class="row">--%>
                                        <div class="col-md-7 p-l-0 p-r-0">
                                            <asp:TextBox ID="txtemailad" runat="server" CssClass="form-control"
                                                placeholder="E-mail..." MaxLength="100" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                ValidationGroup="saveInsertPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณากรอก Email"
                                                Display="None" SetFocusOnError="true" ControlToValidate="txtemailad" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160"
                                                PopupPosition="BottomLeft" />
                                        </div>
                                        <div class="col-md-5 p-r-0">
                                            <asp:DropDownList ID="ddldomain" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="@taokaenoi.co.th">@taokaenoi.co.th</asp:ListItem>
                                                <asp:ListItem Value="@drtobi.co.th">@drtobi.co.th</asp:ListItem>
                                                <asp:ListItem Value="@taokaenoiland.com">@taokaenoiland.com</asp:ListItem>
                                                <asp:ListItem Value="@genc.co.th">@genc.co.th</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9"
                                            ValidationGroup="saveInsertPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณากรอก Email"
                                            Display="None" SetFocusOnError="true" ControlToValidate="txtWaitingPermPermEmailText" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="None"
                                            ControlToValidate="txtemailad"
                                            ValidationGroup="saveInsertPermPerm"
                                            SetFocusOnError="true" Font-Size="13px" ForeColor="Red"
                                            ErrorMessage="ใช้ได้เฉพาะภาษาอังกฤษ ตัวเลข _ . - เท่านั้น"
                                            ValidationExpression="^[^<>฿+*()[\]\\,;:\%#^\s@\'$&!@]+[a-zA-Z0-9]" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160"
                                            PopupPosition="BottomLeft" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160"
                                            PopupPosition="BottomLeft" />
                                        <%-- </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <asp:UpdatePanel ID="Div_Usevpn" runat="server" Visible="true">
                            <ContentTemplate>

                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="f-s-13">ขอใช้สิทธิ์ VPN</label>
                                            <div class="row">
                                                <div id="divGvUseVPN_scroll" style="overflow-x: scroll; width: 100%" runat="server">
                                                    <asp:GridView ID="GvUseVPN" runat="server"
                                                        AutoGenerateColumns="False"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                                        OnPageIndexChanging="onPageIndexChanging"
                                                        OnRowDataBound="onRowDataBound">
                                                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                                        <RowStyle Font-Size="Small" />
                                                        <PagerStyle CssClass="pageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="ประเภทสิทธิ์" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">
                                                                <ItemTemplate>

                                                                    <asp:UpdatePanel ID="Div_rdotype_vpn" runat="server">
                                                                        <ContentTemplate>

                                                                            <asp:Label ID="lbl_vpnidx" runat="server" Visible="false" Text='<%# Eval("vpnidx") %>'></asp:Label>
                                                                            <asp:Label ID="lbl_vpn_name" runat="server" Visible="false" Text='<%# Eval("vpn_name") %>'></asp:Label>
                                                                            <asp:CheckBox ID="chxtype_vpn" runat="server" Value='<%# Eval("vpnidx") %>' Text='<%# Bind("vpn_name") %>'
                                                                                Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"
                                                                                OnCheckedChanged="chkSelectedIndexChanged" AutoPostBack="true" />


                                                                            <%--<asp:RadioButton ID="rdotype_vpn" runat="server" Value='<%# Eval("vpnidx") %>' Text='<%# Bind("vpn_name") %>' 
                                                                            Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" 
                                                                            OnCheckedChanged="rdoSelectedIndexChanged" AutoPostBack="true" />--%>
                                                                            <%-- <asp:Label ID="lbl_vpn_name" runat="server" Visible="true" Text='<%# Eval("vpn_name") %>'></asp:Label>--%>
                                                                        </ContentTemplate>
                                                                        <%--<Triggers>
                                                                            <asp:AsyncPostBackTrigger ControlID="rdoPermissionType" EventName="SelectedIndexChanged" />
                                                                        </Triggers>--%>
                                                                    </asp:UpdatePanel>


                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ระดับสิทธิ์" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">
                                                                <ItemTemplate>
                                                                    <asp:UpdatePanel ID="Update_rdoStatusUseViewDetail" runat="server">
                                                                        <ContentTemplate>
                                                                            <%--<asp:Label ID="Check_Count_List" Visible="false" runat="server"></asp:Label>--%>

                                                                            <asp:Label ID="lbl_vpnidx_check" runat="server" Visible="false" Text='<%# Eval("vpnidx") %>'></asp:Label>
                                                                            <asp:CheckBoxList ID="chklevel_vpn" runat="server" Visible="false">
                                                                                <%--<asp:ListItem Value="1">&nbsp;ใช้รถ</asp:ListItem>
                                                                            <asp:ListItem Value="2">&nbsp;ไม่ใช้รถ</asp:ListItem>--%>
                                                                            </asp:CheckBoxList>

                                                                            <%-- <asp:RequiredFieldValidator ID="Re_rdoStatusUse"
                                                            runat="server"
                                                            ControlToValidate="rdoStatusUse" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาเลือกการใช้งานรถ"
                                                            ValidationGroup="SaveCarUseHr" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_rdoStatusUse" Width="200" PopupPosition="BottomLeft" />--%>
                                                                        </ContentTemplate>
                                                                    </asp:UpdatePanel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txt_remark" runat="server" Enabled="false" CssClass="form-control" TextMode="multiline" Rows="2" placeholder="หมายเหตุ ..." />

                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>
                                                    </asp:GridView>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12" id="Divusevpn" runat="server" visible="false">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">ขอใช้สิทธิ์ VPN</label>
                                            <div class="row">

                                                <div class="col-md-4 p-l-0 p-r-0">
                                                    <asp:CheckBox ID="chkvpnsap" runat="server" AutoPostBack="true" OnCheckedChanged="onSelectedIndexChanged" CssClass="checkbox-inline f-s-13" Text="ขอใช้สิทธิ์ VPN SAP" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12" runat="server" id="divreason_vnpsap" visible="false">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">เหตุผลในการขอสิทธิ์ VPN SAP</label>
                                            <asp:TextBox ID="txtreasonsap" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8"
                                                ValidationGroup="saveInsertPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="เหตุผลในการขอสิทธิ์ VPN SAP"
                                                Display="None" SetFocusOnError="true" ControlToValidate="txtreasonsap" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160"
                                                PopupPosition="BottomLeft" />
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12" id="Divusevpnbi" runat="server" visible="false">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4 p-l-0 p-r-0">
                                                    <asp:CheckBox ID="chkvpnbi" runat="server" AutoPostBack="true" OnCheckedChanged="onSelectedIndexChanged" CssClass="checkbox-inline f-s-13" Text="ขอใช้สิทธิ์ VPN BI" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12" runat="server" id="divreason_vnpbi" visible="false">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">เหตุผลในการขอสิทธิ์ VPN BI</label>
                                            <asp:TextBox ID="txtreasonbi" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4"
                                                ValidationGroup="saveInsertPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="เหตุผลในการขอสิทธิ์ VPN BI"
                                                Display="None" SetFocusOnError="true" ControlToValidate="txtreasonbi" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160"
                                                PopupPosition="BottomLeft" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12" id="Divusevpnexpress" runat="server" visible="false">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4 p-l-0 p-r-0">
                                                    <asp:CheckBox ID="chkvpnexpress" runat="server" AutoPostBack="true" OnCheckedChanged="onSelectedIndexChanged" CssClass="checkbox-inline f-s-13" Text="ขอใช้สิทธิ์ VPN Express" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12" runat="server" id="divreason_vnpexpress" visible="false">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="f-s-13">เหตุผลในการขอสิทธิ์ VPN Express</label>
                                            <asp:TextBox ID="txtreasonexpress" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                                                ValidationGroup="saveInsertPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="เหตุผลในการขอสิทธิ์ VPN Express"
                                                Display="None" SetFocusOnError="true" ControlToValidate="txtreasonexpress" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160"
                                                PopupPosition="BottomLeft" />
                                        </div>
                                    </div>
                                </div>

                                <%-- <div class="col-xs-12">
                            <div class="col-md-8 p-r-0" runat="server" id="divreason_vnpexpress" visible="false">
                            <asp:TextBox ID="txtreasonexpress" TextMode="MultiLine" Rows="5" runat="server" CssClass="form-control" placeholder="เหตุผลในการขอสิทธิ์ VPN Express" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                ValidationGroup="saveInsertPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="เหตุผลในการขอสิทธิ์ VPN Express"
                                Display="None" SetFocusOnError="true" ControlToValidate="txtemailreplace" />
                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatoer8" Width="160"
                                PopupPosition="BottomLeft" />
                        </div>
                    </div>--%>


                                <%--<div class="clearfix"></div>--%>
                            </ContentTemplate>
                            <%--<Triggers>
                                    <asp:PostBackTrigger ControlID="btnInsertPermPerm" />
                                </Triggers>--%>
                        </asp:UpdatePanel>

                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>

                                <div class="col-md-12">

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="f-s-13">แนบไฟล์ขอใช้สิทธิ์</label>
                                            <asp:FileUpload ID="UploadFile_vpn" ClientIDMode="Static" ViewStateMode="Enabled" AutoPostBack="true" CssClass="control-label multi max-2 accept-png|jpg|pdf maxsize-1024  with-preview"
                                                ValidationGroup="saveInsertPermPerm"
                                                runat="server" />
                                            <%--<asp:FileUpload ID="myFileUpload" onchange="if (confirm('Upload ' + this.value + '?')) this.form.submit();" runat="server" />--%>
                                            <p class="help-block"><font color="red">**เฉพาะนามสกุล jpg, png, pdf</font></p>


                                            <%-- CssClass="btn btn-warning"  <asp:RequiredFieldValidator ID="required_UploadFile_UseCarOut"
                                            runat="server" ValidationGroup="SaveCarUseHrOut"
                                            Display="None"
                                            SetFocusOnError="true"
                                            ControlToValidate="UploadFile_UseCarOut"
                                            Font-Size="13px" ForeColor="Red"
                                            ErrorMessage="กรุณาเลือกไฟล์" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                            TargetControlID="required_UploadFile_UseCarOut" Width="200" PopupPosition="BottomRight" />--%>
                                        </div>
                                    </div>

                                    <%-- <script>
                                        function openfileDialog() {
                                            $("#FileUploadControl").click();
                                        }
                                    </script>--%>


                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="row">

                                                <asp:LinkButton ID="btnInsertPermPerm" runat="server" OnCommand="btnCommand" CommandName="btnInsertPermPerm" Visible="true"
                                                    Text="บันทึก" CssClass="btn btn-success pull-right f-s-14" ValidationGroup="saveInsertPermPerm" />

                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnInsertPermPerm" />
                            </Triggers>
                        </asp:UpdatePanel>

                    </div>

                    <!-- End DIV Permission Permanent -->


                    <!-- Start DIV Permission Temporary -->
                    <div id="_divCreate_PermTemp" runat="server">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="f-s-13">ชื่อบริษัท/หน่วยงาน</label>
                                <asp:TextBox ID="txtEmpVisitorCompany" runat="server" CssClass="form-control" placeholder="ชื่อบริษัท/หน่วยงาน..." MaxLength="50" />
                                <asp:RequiredFieldValidator ID="requiredTxtEmpVisitorCompany"
                                    runat="server" ValidationGroup="saveInsertPermTemp"
                                    Display="None"
                                    SetFocusOnError="true"
                                    ControlToValidate="txtEmpVisitorCompany"
                                    Font-Size="13px" ForeColor="Red"
                                    ErrorMessage="กรุณากรอกชื่อบริษัท/หน่วยงาน" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRequiredTxtEmpVisitorCompany" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                    TargetControlID="requiredTxtEmpVisitorCompany" Width="160" PopupPosition="BottomRight" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="f-s-13">เหตุผลของการขอใช้</label>
                                <asp:TextBox ID="txtEmpVisitorReason" runat="server" CssClass="form-control" placeholder="เหตุผลของการขอใช้..." MaxLength="50" />
                                <asp:RequiredFieldValidator ID="requiredTxtEmpVisitorReason"
                                    runat="server" ValidationGroup="saveInsertPermTemp"
                                    Display="None"
                                    SetFocusOnError="true"
                                    ControlToValidate="txtEmpVisitorReason"
                                    Font-Size="13px" ForeColor="Red"
                                    ErrorMessage="กรุณากรอกเหตุลผลของการขอใช้" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRequiredTxtEmpVisitorReason" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                    TargetControlID="requiredTxtEmpVisitorReason" Width="160" PopupPosition="BottomRight" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">เพิ่มข้อมูลผู้มาติดต่อ</div>
                                <div class="panel-body">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="f-s-13">เลือกประเภทบัตร</label>
                                            <asp:RadioButtonList ID="rdotype_visitor" runat="server" CssClass="radio-list-inline-emps" AutoPostBack="true" RepeatDirection="Vertical" OnSelectedIndexChanged="onSelectedIndexChanged">
                                                <asp:ListItem Value="1"> บัตรประจำตัวประชาชน</asp:ListItem>
                                                <asp:ListItem Value="2"> บัตรประจำตัวคนต่างชาติ</asp:ListItem>
                                            </asp:RadioButtonList>

                                        </div>

                                        <div class="form-group" id="_DivVisitorIDCard" runat="server" visible="false">
                                            <label class="f-s-13">เลขบัตรประชาชน</label>
                                            <asp:TextBox ID="txtEmpVisitorIDCard" runat="server" CssClass="form-control" placeholder="เลขบัตรประชาชน..." MaxLength="13"
                                                autocomplete="off" />
                                            <asp:RequiredFieldValidator ID="requiredTxtEmpVisitorIDCard"
                                                ValidationGroup="addContactToList" runat="server"
                                                Display="None"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtEmpVisitorIDCard"
                                                Font-Size="13px" ForeColor="Red"
                                                ErrorMessage="กรุณากรอกเลขบัตรประชาชน" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRequiredTxtEmpVisitorIDCard" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="requiredTxtEmpVisitorIDCard" Width="160" PopupPosition="BottomRight" />
                                            <asp:RegularExpressionValidator ID="regExTxtEmpVisitorIDCard" runat="server"
                                                ValidationGroup="addContactToList"
                                                Display="None"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtEmpVisitorIDCard"
                                                Font-Size="13px" ForeColor="Red"
                                                ValidationExpression="^[0-9]{13}$"
                                                ErrorMessage="เลขบัตรประชาชนไม่ถูกต้อง" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRegExTxtEmpVisitorIDCard" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="regExTxtEmpVisitorIDCard" Width="160" PopupPosition="BottomRight" />
                                        </div>

                                        <div class="form-group" id="_DivVisitorIDCardForeign" runat="server" visible="false">
                                            <label class="f-s-13">เลขบัตรประจำตัวคนต่างชาติ</label>
                                            <asp:TextBox ID="txtEmpVisitorIDCardForeign" runat="server" CssClass="form-control" MaxLength="13" placeholder="เลขบัตรประจำตัวคนต่างชาติ..." autocomplete="off" />
                                            <asp:RequiredFieldValidator ID="requiredtxtEmpVisitorIDCardForeign"
                                                ValidationGroup="addContactToList" runat="server"
                                                Display="None"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtEmpVisitorIDCardForeign"
                                                Font-Size="13px" ForeColor="Red"
                                                ErrorMessage="กรุณากรอกเลขบัตรประจำตัวคนต่างชาติ" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="requiredtxtEmpVisitorIDCardForeign" Width="160" PopupPosition="BottomRight" />
                                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="addContactToList"
                                                Display="None"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtEmpVisitorIDCard"
                                                Font-Size="13px" ForeColor="Red"
                                                ValidationExpression="^[0-9]{13}$"
                                                ErrorMessage="เลขบัตรประชาชนไม่ถูกต้อง" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="regExTxtEmpVisitorIDCard" Width="160" PopupPosition="BottomRight" />--%>
                                        </div>


                                        <div class="form-group">
                                            <label class="f-s-13">ชื่อ - นามสกุล (เฉพาะภาษาอังกฤษ)</label>
                                            <asp:TextBox ID="txtEmpVisitorFullName" runat="server" CssClass="form-control" placeholder="ชื่อ - นามสกุล..." MaxLength="50" />
                                            <asp:RequiredFieldValidator ID="requiredEmpVisitorTxtFirstName"
                                                ValidationGroup="addContactToList" runat="server"
                                                Display="None"
                                                SetFocusOnError="true"
                                                ControlToValidate="txtEmpVisitorFullName"
                                                Font-Size="13px" ForeColor="Red"
                                                ErrorMessage="กรุณากรอกชื่อ - นามสกุล" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRequiredEmpVisitorTxtFirstName" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="requiredEmpVisitorTxtFirstName" Width="160" PopupPosition="BottomRight" />
                                            <asp:RegularExpressionValidator ID="regExTxtEmpVisitorFullName" runat="server"
                                                ValidationGroup="addContactToList"
                                                ControlToValidate="txtEmpVisitorFullName"
                                                Display="None"
                                                SetFocusOnError="true"
                                                Font-Size="13px" ForeColor="Red"
                                                ValidationExpression="^[a-zA-Z ]+$"
                                                ErrorMessage="ชื่อ - นามสกุลต้องเป็นภาษาอังกฤษเท่านั้น" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRegExTxtEmpVisitorFullName" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="regExTxtEmpVisitorFullName" Width="160" PopupPosition="BottomRight" />
                                        </div>
                                        <div class="form-group">
                                            <label class="f-s-13">วันที่เริ่มใช้สิทธิ์</label>
                                            <div class="input-group date">
                                                <asp:HiddenField ID="txtSearchFromHidden" runat="server" />
                                                <asp:TextBox ID="txtSearchFrom" runat="server" placeholder="วันที่เริ่มต้นใช้สิทธิ์..."
                                                    CssClass="form-control datetimepicker-from cursor-pointer" ReadOnly="true" />
                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="f-s-13">วันที่สิ้นสุดใช้สิทธิ์</label>
                                            <div class="input-group date">
                                                <asp:HiddenField ID="txtSearchToHidden" runat="server" />
                                                <asp:TextBox ID="txtSearchTo" runat="server" placeholder="วันที่สิ้นสุดใช้สิทธิ์..."
                                                    CssClass="form-control datetimepicker-to cursor-pointer" ReadOnly="true" />
                                                <span class="input-group-addon show-to-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <asp:LinkButton ID="btnAddContactToList" runat="server" OnCommand="btnCommand" CommandName="btnAddContactToList"
                                            ValidationGroup="addContactToList" CssClass="btn btn-primary pull-right" Text="เพิ่ม" />
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="col-md-8 m-t-10">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                รายชื่อผู้มาติดต่อ
                                 <asp:Label ID="lblAmountContact" runat="server" CssClass="f-s-13 text-danger pull-right" />
                                            </div>
                                            <asp:Label ID="litNoresultGvEmpVisitorContact" runat="server" CssClass="col-md-12 text-center emps-p-all-10" Text="No result" />
                                            <asp:GridView ID="gvEmpVisitorContact" runat="server"
                                                CssClass="table table-striped table-responsive"
                                                GridLines="None" OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <%# (Container.DataItemIndex + 1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="drContactPermTempIDCard" HeaderText="เลขบัตรประชาชน/บัตรประจำตัวคนต่างชาติ"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-Width="20%" />
                                                   <%-- <asp:BoundField DataField="drContactPermTempIDCardForeign" HeaderText="เลขบัตรประจำตัวคนต่างชาติ"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-Width="20%" />--%>
                                                    <asp:BoundField DataField="drContactPermTempFullNameTH" HeaderText="ชื่อ - นามสกุล"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-Width="30%" />
                                                    <asp:BoundField DataField="drContactPermTempStart" HeaderText="วันที่เริ่มต้นใช้สิทธิ์"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-Width="20%" />
                                                    <asp:BoundField DataField="drContactPermTempEnd" HeaderText="วันที่สิ้นสุดใช้สิทธิ์"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-Width="20%" />
                                                    <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnRemoveEmpVisitor" runat="server" CssClass="btn btn-danger btn-xs"
                                                                OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')" CommandName="btnRemoveEmpVisitor">
                                             <i class="fa fa-times"></i>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <asp:UpdatePanel ID="Div_UsevpnTemporary" runat="server" Visible="false">
                            <ContentTemplate>

                                <div class="col-md-12">
                                    <%-- <div class="col-md-12">--%>
                                    <div class="form-group">
                                        <label class="f-s-13">ขอใช้สิทธิ์ VPN</label>
                                        <div class="row">
                                            <div id="divGvUseVPNTemporary_scroll" style="overflow-x: scroll; width: 100%" runat="server">
                                                <asp:GridView ID="GvUseVPNTemporary" runat="server"
                                                    AutoGenerateColumns="False"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                                    OnPageIndexChanging="onPageIndexChanging"
                                                    OnRowDataBound="onRowDataBound">
                                                    <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                                    <RowStyle Font-Size="Small" />
                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="ประเภทสิทธิ์" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">
                                                            <ItemTemplate>

                                                                <asp:UpdatePanel ID="Div_rdotype_vpn_temporary" runat="server">
                                                                    <ContentTemplate>

                                                                        <asp:Label ID="lbl_vpnidx_temporary" runat="server" Visible="false" Text='<%# Eval("vpnidx") %>'></asp:Label>
                                                                        <asp:Label ID="lbl_vpn_name_temporary" runat="server" Visible="false" Text='<%# Eval("vpn_name") %>'></asp:Label>
                                                                        <asp:CheckBox ID="chxtype_vpn_temporary" runat="server" Value='<%# Eval("vpnidx") %>' Text='<%# Bind("vpn_name") %>'
                                                                            Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right"
                                                                            OnCheckedChanged="chkSelectedIndexChanged" AutoPostBack="true" />


                                                                        <%--<asp:RadioButton ID="rdotype_vpn" runat="server" Value='<%# Eval("vpnidx") %>' Text='<%# Bind("vpn_name") %>' 
                                                                            Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" 
                                                                            OnCheckedChanged="rdoSelectedIndexChanged" AutoPostBack="true" />--%>
                                                                        <%-- <asp:Label ID="lbl_vpn_name" runat="server" Visible="true" Text='<%# Eval("vpn_name") %>'></asp:Label>--%>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>


                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ระดับสิทธิ์" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">
                                                            <ItemTemplate>
                                                                <asp:UpdatePanel ID="Update_rdoStatusUseViewDetail_temporary" runat="server">
                                                                    <ContentTemplate>
                                                                        <%--<asp:Label ID="Check_Count_List" Visible="false" runat="server"></asp:Label>--%>

                                                                        <asp:Label ID="lbl_vpnidx_check_temporary" runat="server" Visible="false" Text='<%# Eval("vpnidx") %>'></asp:Label>
                                                                        <asp:CheckBoxList ID="chklevel_vpn_temporary" runat="server" Visible="false">
                                                                            <%--<asp:ListItem Value="1">&nbsp;ใช้รถ</asp:ListItem>
                                                                            <asp:ListItem Value="2">&nbsp;ไม่ใช้รถ</asp:ListItem>--%>
                                                                        </asp:CheckBoxList>

                                                                        <%-- <asp:RequiredFieldValidator ID="Re_rdoStatusUse"
                                                            runat="server"
                                                            ControlToValidate="rdoStatusUse" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาเลือกการใช้งานรถ"
                                                            ValidationGroup="SaveCarUseHr" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_rdoStatusUse" Width="200" PopupPosition="BottomLeft" />--%>
                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="left">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="txt_remark_temporary" runat="server" Enabled="false" CssClass="form-control" TextMode="multiline" Rows="2" placeholder="หมายเหตุ ..." />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </div>

                                        </div>
                                    </div>
                                    <%--   </div>--%>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="f-s-13">แนบไฟล์ขอใช้สิทธิ์</label>
                                        <asp:FileUpload ID="UploadFile_vpnTemporary" ClientIDMode="Static" ViewStateMode="Enabled" AutoPostBack="true" CssClass="control-label multi max-2 accept-png|jpg|pdf maxsize-1024  with-preview" ValidationGroup="saveInsertPermTemp" runat="server" />
                                        <%--<asp:FileUpload ID="myFileUpload" onchange="if (confirm('Upload ' + this.value + '?')) this.form.submit();" runat="server" />--%>
                                        <p class="help-block"><font color="red">**เฉพาะนามสกุล jpg, png, pdf</font></p>



                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <asp:LinkButton ID="btnInsertPermTemp" runat="server" OnCommand="btnCommand" CommandName="btnInsertPermTemp"
                                        Text="บันทึก" CssClass="btn btn-success pull-right f-s-14" ValidationGroup="saveInsertPermTemp" Visible="false"
                                        OnClientClick="return confirmWithOutValidated('คุณต้องการบันทึกใช่หรือไม่ ?','saveInsertPermTemp')" />
                                </div>


                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnInsertPermTemp" />
                            </Triggers>


                        </asp:UpdatePanel>
                    </div>
                    <!-- End DIV Permission Temporary -->
                </div>

                <div class="clearfix"></div>
            </div>
            </div>

        </ContentTemplate>

    </asp:UpdatePanel>
    <!--*** End DIV Create ***-->
    <div class="clearfix"></div>
    <!--*** Start DIV Create ***-->
    <div id="divAddmore" runat="server" class="col-md-12" visible="false">
        <div class="panel panel-info">
            <div class="panel-heading">สร้างรายการขอใช้สิทธิ์เพิ่มเติม</div>
            <div class="panel-body">
                <div class="col-md-12">
                    <asp:TextBox ID="txtCreatorOrgNameTH_Addmore" runat="server" Visible="false" />
                    <asp:TextBox ID="txtCreatorDeptNameTH_Addmore" runat="server" Visible="false" />

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="f-s-13">รหัสพนักงานผู้ทำรายการ</label>
                            <asp:TextBox ID="txtCreatorEmpCode_Addmore" runat="server" CssClass="form-control" Enabled="false" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="f-s-13">ชื่อ - นามสกุลผู้ทำรายการ</label>
                            <asp:TextBox ID="txtCreatorFullName_Addmore" runat="server" CssClass="form-control" Enabled="false" />
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="f-s-13">รหัสพนักงาน</label>
                            <asp:TextBox ID="txtEmpNewEmpIdxHidden_Addmore" runat="server" Visible="false" />
                            <asp:TextBox ID="txtEmpNewJobGradeLevelHidden_Addmore" runat="server" Visible="false" />
                            <asp:TextBox ID="txtEmpNewEmpCode_Addmore" runat="server" CssClass="form-control" placeholder="รหัสพนักงาน..."
                                OnTextChanged="onTextChanged" AutoPostBack="true" MaxLength="8" autocomplete="off" />
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="f-s-13">บริษัท</label>
                            <asp:TextBox ID="txtEmpNewOrgNameTHHidden_Addmore" runat="server" Visible="false" />
                            <asp:TextBox ID="txtEmpNewOrgNameTH_Addmore" runat="server" CssClass="form-control" placeholder="บริษัท..." Enabled="false" />
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="f-s-13">ชื่อ - สกุล (TH)</label>
                            <asp:TextBox ID="txtEmpNewFullNameTHHidden_Addmore" runat="server" Visible="false" />
                            <asp:TextBox ID="txtEmpNewFullNameTH_Addmore" runat="server" CssClass="form-control" placeholder="ชื่อ - สกุล (TH)..." Enabled="false" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="f-s-13">ฝ่าย</label>
                            <asp:TextBox ID="txtEmpNewDeptNameTHHidden_Addmore" runat="server" Visible="false" />
                            <asp:TextBox ID="txtEmpNewDeptNameTH_Addmore" runat="server" CssClass="form-control" placeholder="ฝ่าย..." Enabled="false" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="f-s-13">ชื่อ - สกุล (EN)</label>
                            <asp:TextBox ID="txtEmpNewFullNameENHidden_Addmore" runat="server" Visible="false" />
                            <asp:TextBox ID="txtEmpNewFullNameEN_Addmore" runat="server" CssClass="form-control" placeholder="ชื่อ - สกุล (EN)..." Enabled="false" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="f-s-13">แผนก</label>
                            <asp:TextBox ID="txtEmpNewSecNameTHHidden_Addmore" runat="server" Visible="false" />
                            <asp:TextBox ID="txtEmpNewSecNameTH_Addmore" runat="server" CssClass="form-control" placeholder="แผนก..." Enabled="false" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="f-s-13">Cost Center</label>
                            <asp:TextBox ID="txtEmpNewCostCenterNoHidden_Addmore" runat="server" Visible="false" />
                            <asp:TextBox ID="txtEmpNewCostCenterNo_Addmore" runat="server" CssClass="form-control" placeholder="Cost Center..." Enabled="false" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="f-s-13">ตำแหน่ง</label>
                            <asp:TextBox ID="txtEmpNewPosNameTHHidden_Addmore" runat="server" Visible="false" />
                            <asp:TextBox ID="txtEmpNewPosNameTH_Addmore" runat="server" CssClass="form-control" placeholder="ตำแหน่ง..." Enabled="false" />
                            <asp:TextBox ID="txtvpnidx_addmore" runat="server" CssClass="form-control" Visible="false" />
                        </div>
                    </div>

                </div>

                <asp:UpdatePanel ID="UpdatePanel7" runat="server" Visible="true">
                    <ContentTemplate>
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="f-s-13">วันที่เริ่มใช้งาน</label>

                                    <div class='input-group date'>
                                        <asp:TextBox ID="AddStartdate_more" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16"
                                        ValidationGroup="saveInsertPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกวันที่เริ่มใช้"
                                        Display="None" SetFocusOnError="true" ControlToValidate="AddStartdate_more" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator16" Width="160"
                                        PopupPosition="BottomLeft" />

                                    <div class="col-md-1" id="div_showfiletestaddmore" runat="server" style="color: transparent;">

                                        <asp:FileUpload ID="FileUpload1_testaddmore" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|png|pdf" />
                                    </div>

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="f-s-13">รายการขอใช้สิทธิ์เพิ่มเติม</label>
                                    <div class="row">

                                        <div class="col-md-3 p-l-0 p-r-0">
                                            <asp:CheckBox ID="chkmoread" runat="server" AutoPostBack="true" OnCheckedChanged="onSelectedIndexChanged" CssClass="checkbox-inline f-s-13" Text="ขอใช้สิทธิ์ AD" />
                                        </div>
                                        <div class="col-md-3 p-l-0 p-r-0">
                                            <asp:CheckBox ID="chkmoreusersap" runat="server" AutoPostBack="true" OnCheckedChanged="onSelectedIndexChanged" CssClass="checkbox-inline f-s-13" Text="ขอใช้สิทธิ์ User SAP" />
                                        </div>
                                        <div class="col-md-3 p-l-0 p-r-0">
                                            <asp:CheckBox ID="chkmoreemail" runat="server" AutoPostBack="true" OnCheckedChanged="onSelectedIndexChanged" CssClass="checkbox-inline f-s-13" Text="ขอใช้สิทธิ์ Email" />
                                        </div>
                                        <%--  <div class="col-md-3 p-l-0 p-r-0">
                                    <asp:CheckBox ID="chkmorevpn" runat="server" AutoPostBack="true" OnCheckedChanged="onSelectedIndexChanged" CssClass="checkbox-inline f-s-13" Text="ขอใช้สิทธิ์ VPN" />
                                </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                    <Triggers>
                        <%--<asp:AsyncPostBackTrigger ControlID="rdoPermissionType" EventName="SelectedIndexChanged" />--%>
                        <asp:PostBackTrigger ControlID="btnInsertAddmore" />
                    </Triggers>
                </asp:UpdatePanel>



                <div class="col-md-12" id="divmore_adonline" runat="server" visible="false">
                    <%--<div class="col-md-6">--%>
                    <div class="form-group">

                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="f-s-13">ระดับสิทธิ์ AD</label>
                                <asp:DropDownList ID="ddladonline_more" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged" runat="server" CssClass="form-control" Width="100%" />

                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group">
                                <label>&nbsp;</label>
                                <div class="clearfix"></div>
                                <asp:Label ID="lblshowad_more" runat="server" CssClass="btn btn-info col-md-12"><i class="fa fa-question fa-lg"></i></asp:Label>
                            </div>
                        </div>
                        <%--<label class="f-s-13">ระดับสิทธิ์ AD</label>
                            <div class="form-inline">
                                <div class="form-group col-xs-11 row">
                                    <asp:DropDownList ID="ddladonline_more" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged" runat="server" CssClass="form-control" Width="100%" />
                                </div>
                               
                                <div class="form-group col-xs-1 row">
                                    <asp:Label ID="lblshowad_more" runat="server" CssClass="btn btn-info m-l-5 permpol"><i class="fa fa-question fa-lg"></i></asp:Label>
                                </div>
                            </div>--%>
                    </div>

                    <div class="clearfix"></div>
                    <%--  </div>--%>
                    <%--<div class="clearfix"></div>--%>
                </div>

                <div class="clearfix"></div>
                <div class="col-md-12" id="divshowreasonmore" runat="server" visible="false">
                    <div class="col-md-6">
                        <div class="form-group">
                            <%--<label class="f-s-13">เหตุผลการขอใช้สิทธิ์</label>--%>
                            <asp:Label runat="server" ID="showreasonad_more" CssClass="f-s-13"></asp:Label>

                            <asp:TextBox ID="txtreason_more" TextMode="MultiLine" Rows="5" runat="server" CssClass="form-control" placeholder="เหตุผลการขอใช้สิทธิ์..." />


                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-12" id="divmore_usersap" runat="server" visible="false">
                    <div class="col-md-6">
                        <div class="form-group">

                            <label class="f-s-13">User SAP ที่ใช้</label>
                            <asp:UpdatePanel ID="UpdatePanel_txtsapmore" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtsapmore" runat="server" CssClass="form-control" placeholder="User SAP ที่ใช้..." MaxLength="50" />
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>
                    </div>

                </div>

                <div id="divmore_email" runat="server" visible="false">
                    <div class="col-md-12" id="divmore_chooseemail" runat="server">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="f-s-13">Email ที่ขอใช้</label>
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:RadioButtonList ID="rdomail_more" runat="server" CssClass="radio-list-inline-emps"
                                            AutoPostBack="true" RepeatDirection="Vertical" OnSelectedIndexChanged="onSelectedIndexChanged">
                                            <asp:ListItem Value="1">Email กลาง</asp:ListItem>
                                            <asp:ListItem Value="2">Email ส่วนตัว</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </div>

                    <div runat="server" id="divmore_emaildetail" visible="false">
                        <div class="col-md-12">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="f-s-13">ประเภทอีเมล์</label>
                                    <div class="row">
                                        <div class="col-md-7 p-l-0 p-r-0">
                                            <asp:DropDownList ID="ddlemailmore" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกประเภทอีเมล์"></asp:ListItem>
                                            </asp:DropDownList>
                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator12"
                                                ValidationGroup="saveInsertPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกประเภทอีเมล์"
                                                Display="None" SetFocusOnError="true" ControlToValidate="ddlemail" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidators7" Width="160"
                                                PopupPosition="BottomLeft" />--%>
                                        </div>


                                        <div class="col-md-5 p-r-0" runat="server" id="divemail1or3_more" visible="true">
                                            <asp:DropDownList ID="ddlemail1or3_more" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกอีเมล์"></asp:ListItem>
                                            </asp:DropDownList>
                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator13"
                                                ValidationGroup="saveInsertPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกอีเมล์"
                                                Display="None" SetFocusOnError="true" ControlToValidate="ddl1or3" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160"
                                                PopupPosition="BottomLeft" />--%>
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="col-md-12" id="divemailad_more" runat="server" visible="true">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <%-- <div class="row">--%>
                                    <div class="col-md-7 p-l-0 p-r-0">
                                        <asp:TextBox ID="txtemilmore" runat="server" CssClass="form-control"
                                            placeholder="E-mail..." MaxLength="100" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14"
                                            ValidationGroup="saveInsertPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณากรอก Email"
                                            Display="None" SetFocusOnError="true" ControlToValidate="txtemilmore" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator14" Width="160"
                                            PopupPosition="BottomLeft" />
                                    </div>
                                    <div class="col-md-5 p-r-0">
                                        <asp:DropDownList ID="ddldomainmore" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="@taokaenoi.co.th">@taokaenoi.co.th</asp:ListItem>
                                            <asp:ListItem Value="@drtobi.co.th">@drtobi.co.th</asp:ListItem>
                                            <asp:ListItem Value="@taokaenoiland.com">@taokaenoiland.com</asp:ListItem>
                                            <asp:ListItem Value="@genc.co.th">@genc.co.th</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator15"
                                        ValidationGroup="saveInsertPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณากรอก Email"
                                        Display="None" SetFocusOnError="true" ControlToValidate="txtemilmore" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" Display="None"
                                        ControlToValidate="txtemilmore"
                                        ValidationGroup="saveInsertPermPerm"
                                        SetFocusOnError="true" Font-Size="13px" ForeColor="Red"
                                        ErrorMessage="ใช้ได้เฉพาะภาษาอังกฤษ ตัวเลข _ . - เท่านั้น"
                                        ValidationExpression="^[^<>฿+*()[\]\\,;:\%#^\s@\'$&!@]+[a-zA-Z0-9]" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="160"
                                        PopupPosition="BottomLeft" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160"
                                        PopupPosition="BottomLeft" />
                                    <%-- </div>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
                <br />
                <div class="col-md-12" id="divmore_vpn" runat="server">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">รายการขอใข้สิทธิ์ VPN</div>
                            <div class="panel-body">
                                <asp:GridView ID="GvAddmore"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    DataKeyNames="vpnidx"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="info"
                                    HeaderStyle-Height="40px"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center;">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>


                                    <Columns>
                                        <asp:TemplateField HeaderText="เลือกรายการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:CheckBox ID="chkvpn_addmore" runat="server" OnCheckedChanged="chkSelectedIndexChanged" AutoPostBack="true" />&nbsp;&nbsp;
                                                <asp:Literal ID="lblvpn_addmore" runat="server" Text='<%# Eval("vpn_name") %>' /></small>
                                                <asp:Literal ID="litvpnidx_addmore" Visible="false" runat="server" Text='<%# Eval("vpnidx") %>' /></small>

                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ระดับสิทธิ์ VPN" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:CheckBoxList ID="chksubvpn_addmore" runat="server" />
                                                    <%--<asp:RadioButtonList ID="rdovpn_edit" runat="server"></asp:RadioButtonList>--%>
                                                    <asp:Literal ID="litvpn1idx_addmore" Visible="false" runat="server" /></small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="litremark_addmore" runat="server" Visible="false"></asp:Literal>
                                                    <asp:TextBox ID="txtremark_addmore" CssClass="form-control" TextMode="MultiLine" Rows="3" runat="server" /></small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>

                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>


                <%--<div id="divmore_vpn" runat="server" visible="false">
                    <div class="col-md-12" id="divmore_vpnsap" runat="server">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="f-s-13">เหตุผลในการขอสิทธิ์ VPN SAP</label>
                                <asp:TextBox ID="txtmore_vpnsap" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control" placeholder="เหตุผลในการขอสิทธิ์ VPN SAP..."></asp:TextBox>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12" id="divmore_vpnbi" runat="server">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="f-s-13">เหตุผลในการขอสิทธิ์ VPN BI</label>
                                <asp:TextBox ID="txtmore_vpnbi" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control" placeholder="เหตุผลในการขอสิทธิ์ VPN BI..."></asp:TextBox>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-12" id="divmore_vpnexpress" runat="server">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="f-s-13">เหตุผลในการขอสิทธิ์ VPN Express</label>
                                <asp:TextBox ID="txtmore_vpnexpress" runat="server" TextMode="MultiLine" Rows="5" CssClass="form-control" placeholder="เหตุผลในการขอสิทธิ์ VPN Express..."></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>--%>

                <div class="clearfix"></div>
                <div class="col-md-12">
                    <asp:UpdatePanel ID="div_Addmore" runat="server" Visible="false">
                        <ContentTemplate>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="f-s-13">แนบไฟล์ขอใช้สิทธิ์</label>
                                    <asp:FileUpload ID="UploadFile_vpnaddmore" ClientIDMode="Static" ViewStateMode="Enabled" AutoPostBack="true"
                                        CssClass="control-label multi max-2 accept-png|jpg|pdf maxsize-1024  with-preview"
                                        ValidationGroup="saveInsertPermPerm"
                                        runat="server" />
                                    <%--<asp:FileUpload ID="myFileUpload" onchange="if (confirm('Upload ' + this.value + '?')) this.form.submit();" runat="server" />--%>
                                    <%-- <p class="help-block"><font color="red">**เฉพาะนามสกุล jpg, png</font></p>--%>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <asp:LinkButton ID="btnInsertAddmore" runat="server" OnCommand="btnCommand" CommandName="btnInsertPermPerm_More" Visible="false"
                                    Text="บันทึก" CssClass="btn btn-success pull-right f-s-14" ValidationGroup="saveInsertPermPerm"
                                    OnClientClick="return confirmWithOutValidated('คุณต้องการบันทึกใช่หรือไม่ ?','')" />

                            </div>

                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnInsertAddmore" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>



    <!--*** Start DIV Waiting ***-->
    <div id="divWaiting" runat="server" class="col-md-12" visible="false">
        <!-- Start Permission Permanent Area -->
        <div id="_divWaitingPermPerm" runat="server" visible="false">
            <div class="alert alert-info f-s-14" role="alert">รายการที่รออนุมัติ : สิทธิ์ทั่วไป</div>
            <asp:GridView ID="gvWaitingPermPerm"
                runat="server"
                AutoGenerateColumns="false"
                DataKeyNames="u0_perm_perm_idx"
                CssClass="table table-bordered table-striped table-responsive col-md-12"
                HeaderStyle-CssClass="info"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="onRowDataBound"
                OnPageIndexChanging="onPageIndexChanging"
                AutoPostBack="False">
                <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div class="text-center">ไม่พบข้อมูล</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="รหัสเอกสาร" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="12%">
                        <ItemTemplate>
                            <asp:Label ID="u0PermPermCodeId" runat="server" Text='<%# Eval("u0_perm_perm_code_id") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="รายละเอียดผู้ขอใช้สิทธิ์" ItemStyle-CssClass="f-s-13 text-left" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="30%">
                        <ItemTemplate>
                            <p>
                                <b>ชื่อ - สกุล:</b>
                                <asp:Label ID="u0EmpNameTH" runat="server" Text='<%# Eval("emp_name_th") %>' />
                            </p>
                            <p>
                                <b>บริษัท:</b>
                                <asp:Label ID="u0EmpOrgTH" runat="server" Text='<%# Eval("org_name_th") %>' />
                            </p>
                            <p>
                                <b>ฝ่าย:</b>
                                <asp:Label ID="u0EmpDeptTH" runat="server" Text='<%# Eval("dept_name_th") %>' />
                            </p>
                            <p>
                                <b>แผนก:</b>
                                <asp:Label ID="u0EmpSecTH" runat="server" Text='<%# Eval("sec_name_th") %>' />
                            </p>
                            <b>ตำแหน่ง:</b>
                            <asp:Label ID="u0EmpPosTH" runat="server" Text='<%# Eval("pos_name_th") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="วันที่ทำรายการ" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="12%">
                        <ItemTemplate>
                            <asp:Label ID="u0PermPermCreatedAt" runat="server" Text='<%# Eval("u0_perm_perm_created_at") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <asp:Label ID="u0PermTempNodeTo1" runat="server" Text='<%# Eval("status_to_1") %>' />
                            <asp:Label ID="connectText" runat="server"><br />โดย<br /></asp:Label>
                            <asp:Label ID="u0PermTempNodeTo2" runat="server" Text='<%# Eval("status_to_2") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="9%">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnIndexPermPerm_ViewDetail" runat="server" CssClass="btn btn-info"
                                OnCommand="btnCommand" CommandName="btnWaitingPermPerm_ViewDetail" CommandArgument='<%# Eval("u0_perm_perm_idx") %>'>
                        <i class="fa fa-file"></i>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="_divWaitingPermPerm_Detail" runat="server" visible="false">
            <asp:LinkButton ID="btnBack_ToDivWaitingPermPerm" runat="server" CssClass="btn btn-danger m-b-10 f-s-14" OnCommand="btnCommand"
                CommandName="btnBack_ToDivWaitingPermPerm"><i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ</asp:LinkButton>
            <div class="panel panel-info m-b-10">
                <div class="panel-heading">รายละเอียด</div>
                <div class="panel-body">
                    <asp:Label ID="lblValueU0PermPermEmpIdxCreator" runat="server" Visible="false" />
                    <asp:Label ID="lblValueU0PermPermEmpIdx" runat="server" Visible="false" />
                    <asp:Label ID="lblValueU0PermPermRSecIDX_EmpIdx" runat="server" Visible="false" />
                    <asp:Label ID="lblValueU0PermPermIdx" runat="server" Visible="false" />
                    <asp:Label ID="lblValueU0PermPermJobGradeLevel" runat="server" Visible="false" />
                    <asp:Label ID="lblValueU0PermPermFnameEN" runat="server" Visible="false" />
                    <asp:Label ID="lblValueU0PermPermLnameEN" runat="server" Visible="false" />

                    <asp:Label ID="lblValueU0PermPermNodeCurrent" runat="server" Visible="false" />
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleWaitingPermPermCode" runat="server" CssClass="f-s-13 f-bold" Text="รหัสเอกสาร :" />
                        <asp:Label ID="lblvpnidxedit" runat="server" Visible="false" CssClass="f-s-13" />

                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueWaitingPermPermCode" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleWaitingPermPermCreatorEmpCode" runat="server" CssClass="f-s-13 f-bold" Text="รหัสพนักงานผู้ทำรายการ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueWaitingPermPermCreatorEmpCode" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleWaitingPermPermCreatorFullNameTH" runat="server" CssClass="f-s-13 f-bold" Text="ชื่อ - สกุลผู้ทำรายการ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueWaitingPermPermCreatorFullNameTH" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleWaitingPermPermCreatedAt" runat="server" CssClass="f-s-13 f-bold" Text="วันที่ทำรายการ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueWaitingPermPermCreatedAt" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleWaitingPermPermEmpCode" runat="server" CssClass="f-s-13 f-bold" Text="รหัสพนักงานผู้ขอใช้สิทธิ์ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueWaitingPermPermEmpCode" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleWaitingPermPermEmpNameTH" runat="server" CssClass="f-s-13 f-bold" Text="ชื่อ - สกุลผู้ขอใช้สิทธิ์ (TH) :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueWaitingPermPermEmpNameTH" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleWaitingPermPermEmpNameEN" runat="server" CssClass="f-s-13 f-bold" Text="ชื่อ - สกุลผู้ขอใช้สิทธิ์ (EN) :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueWaitingPermPermEmpNameEN" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleWaitingPermPermOrgNameTH" runat="server" CssClass="f-s-13 f-bold" Text="บริษัทผู้ขอใช้สิทธิ์ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueWaitingPermPermOrgNameTH" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleWaitingPermPermDeptNameTH" runat="server" CssClass="f-s-13 f-bold" Text="ฝ่ายผู้ขอใช้สิทธิ์ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueWaitingPermPermDeptNameTH" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleWaitingPermPermSecNameTH" runat="server" CssClass="f-s-13 f-bold" Text="แผนกผู้ขอใช้สิทธิ์ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueWaitingPermPermSecNameTH" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleWaitingPermPermPosNameTH" runat="server" CssClass="f-s-13 f-bold" Text="ตำแหน่งผู้ขอใช้สิทธิ์ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueWaitingPermPermPosNameTH" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleWaitingPermPermCostCenter" runat="server" CssClass="f-s-13 f-bold" Text="Cost Center ผู้ขอใช้สิทธิ์ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueWaitingPermPermCostCenter" runat="server" CssClass="f-s-13" />
                    </div>
                    <div class="col-md-3 m-b-5 col-md-offset-2">
                        <asp:Label ID="lblTitleWaitingPermPermNode" runat="server" CssClass="f-s-13 f-bold" Text="สถานะรายการ :" />
                    </div>
                    <div class="col-md-7 m-b-5">
                        <asp:Label ID="lblValueWaitingPermPermNode" runat="server" CssClass="f-s-13" />
                    </div>

                    <div id="divWaitingPermPermDetailDate" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="Label1" runat="server" CssClass="f-s-13 f-bold text-danger" Text="วันที่เริ่มใช้งาน :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="Label2" runat="server" CssClass="f-s-13 text-danger" />
                            <asp:Label ID="lblValueWaitingPermPermDate" runat="server" CssClass="f-s-13 text-danger"><i class="fa fa-question"></i></asp:Label>
                        </div>
                    </div>

                    <div id="divWaitingPermPermDetailPermPol" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleWaitingPermPermPol" runat="server" CssClass="f-s-13 f-bold text-danger" Text="ระดับสิทธิ์ AD :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueWaitingPermPermPol" runat="server" CssClass="f-s-13 text-danger" />
                            <asp:Label ID="lblValueWaitingPermPermPolEx" runat="server" CssClass="btn btn-xs btn-info"><i class="fa fa-question"></i></asp:Label>
                        </div>
                    </div>
                    <div id="divWaitingPermPermDetailReason" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="LalblTitleWaitingPermPermReason" runat="server" CssClass="f-s-13 f-bold text-danger" Text="เหตุผลในการขอสิทธิ์ AD(High) :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueWaitingPermReason" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>

                    <div id="divWaitingPermPermDetailSap" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleWaitingPermPermSap" runat="server" CssClass="f-s-13 f-bold text-danger" Text="User SAP ที่ใช้ :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueWaitingPermPermSap" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>
                    <div id="divWaitingPermPermDetailEmail" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleWaitingPermPermEmail" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Email ที่ขอใช้ :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueWaitingPermPermEmailType" runat="server" CssClass="f-s-13 text-danger" />
                            (<asp:Label ID="lblValueWaitingPermPermEmailText" runat="server" CssClass="f-s-13 text-danger" />)
                            <asp:Label ID="lblValueWaitingPermPermTypeEmail" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="col-lg-2">
                        </div>
                        <div class="col-lg-8">
                            <asp:GridView ID="gvFilePerm_Wait" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="info"
                                OnRowDataBound="Master_RowDataBound"
                                BorderStyle="None"
                                CellSpacing="2"
                                Font-Size="Small">

                                <Columns>
                                    <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                        <ItemTemplate>
                                            <div class="col-lg-10">
                                                <asp:Literal ID="ltFileName11_permwait" runat="server" Text='<%# Eval("FileName") %>' />
                                            </div>
                                            <div class="col-lg-2">
                                                <asp:HyperLink runat="server" ID="btnDL11_permwait" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                <asp:HiddenField runat="server" ID="hidFile11_permwait" Value='<%# Eval("Download") %>' />
                                            </div>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                        <div class="col-lg-2">
                        </div>
                    </div>

                    <div id="divWaitingPermPermad" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="Label9" runat="server" CssClass="f-s-13 f-bold text-danger" Text="สิทธิ์การใช้งาน AD :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueWaitingPermPermad" runat="server" CssClass="f-s-13 text-danger" />

                        </div>
                    </div>
                    <div id="divWaitingU0PermPermCommentHeaderEmpNew" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleWaitingU0PermPermCommentHeaderEmpNew" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Comment ผู้อนุมัติ :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueWaitingU0PermPermCommentHeaderEmpNew" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>
                    <div id="divWaitingU0PermPermCommentDirectorEmpNew" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleWaitingU0PermPermCommentDirectorEmpNew" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Comment ผู้อนุมัติ :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueWaitingU0PermPermCommentDirectorEmpNew" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>
                    <div id="divWaitingU0PermPermCommentDirectorHR" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleWaitingU0PermPermCommentDirectorHR" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Comment ผู้อนุมัติ :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueWaitingU0PermPermCommentDirectorHR" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>

                    <div id="divWaitingU0PermPermCommentIT" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="Label14" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Comment IT :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueWaitingU0PermPermCommentIT" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>

                    <div id="divWaitingU0PermPermCommentHeaderIT" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleWaitingU0PermPermCommentHeaderIT" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Comment หัวหน้า IT" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueWaitingU0PermPermCommentHeaderIT" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>
                    <div id="divWaitingU0PermPermCommentDirectorIT" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleWaitingU0PermPermCommentDirectorIT" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Comment Director IT :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueWaitingU0PermPermCommentDirectorIT" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>
                    <div id="divWaitingU0PermPermCommentITClose" runat="server" visible="false">
                        <div class="col-md-3 m-b-5 col-md-offset-2">
                            <asp:Label ID="lblTitleWaitingU0PermPermCommentITClose" runat="server" CssClass="f-s-13 f-bold text-danger" Text="Comment IT ปิดงาน :" />
                        </div>
                        <div class="col-md-7 m-b-5">
                            <asp:Label ID="lblValueWaitingU0PermPermCommentITClose" runat="server" CssClass="f-s-13 text-danger" />
                        </div>
                    </div>
                    <div id="divWaitingU1PermPermSubCommentIT" runat="server" visible="true" class="col-md-8 col-md-offset-2">
                        <div class="panel panel-default">
                            <div class="panel-heading">รายการบันทึก Comment IT</div>
                            <table class="table table-striped f-s-12 table-empshift-responsive">
                                <asp:Repeater ID="rptWaitingPermPermSubCmIT" runat="server">
                                    <HeaderTemplate>
                                        <tr>
                                            <th>วัน / เวลา</th>
                                            <th>ผู้บันทึก</th>
                                            <th>Comment</th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td data-th="วัน / เวลา"><%# Eval("u1_perm_perm_created_at_date") %> <%# Eval("u1_perm_perm_created_at_time") %></td>
                                            <td data-th="ผู้บันทึก"><%# Eval("emp_name_th") %></td>
                                            <td data-th="Comment"><%# Eval("u1_perm_perm_cm_it") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="divWaitingPermPermApproveAndConfig" runat="server" class="panel panel-info m-b-10">
                <div class="panel-heading">ส่วนของ<asp:Label ID="lblTitleWaitingPermPermApproverAndConfig" runat="server" /></div>
                <div class="panel-body">
                    <!-- START Waiting Config -->
                    <div id="_divWaitingPermPermConfig" runat="server">

                        <div class="col-md-6 col-md-offset-3 m-t-10">
                            <div class="form-group">
                                <label class="f-s-14">วันที่เริ่มใช้</label>
                                <div class='input-group date'>
                                    <asp:TextBox ID="AddStartdate" runat="server" CssClass="form-control  from-date-datepicker" Text='<%# formatDateTime((String)Eval("u0_perm_perm_date_use"))%>'></asp:TextBox>
                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                </div>
                                <asp:RequiredFieldValidator ID="RequiredFieldVsalidator1"
                                    ValidationGroup="approveWaitingPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณากรอก User SAP ที่ใช้"
                                    Display="None" SetFocusOnError="true" ControlToValidate="AddStartdate" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtendsser2" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldVsalidator1" Width="160"
                                    PopupPosition="BottomLeft" />
                            </div>
                        </div>

                        <div class="col-md-6 col-md-offset-3 m-b-10">
                            <div class="form-group">
                                <label class="f-s-14">ระดับสิทธิ์ AD</label>
                                <div class="form-inline">
                                    <div class="form-group col-xs-11 row">
                                        <asp:DropDownList ID="ddlWaitingPermPermPol" runat="server" CssClass="form-control" Width="100%">
                                            <%--   <asp:ListItem Value="0">กรุณาเลือกระดับสิทธิ์ AD...</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </div>
                                    <%--  <asp:RequiredFieldValidator ID="requiredDDLWaitingPermPermPol"
                                        ValidationGroup="approveWaitingPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกระดับสิทธิ์ AD"
                                        Display="None" SetFocusOnError="true" ControlToValidate="ddlWaitingPermPermPol" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="extenderRequiredDDLWaitingPermPermPol" runat="Server"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="requiredDDLWaitingPermPermPol" Width="160"
                                        PopupPosition="BottomLeft" />--%>
                                    <div class="form-group col-xs-1 row">
                                        <asp:Label ID="lblWaitingPermPermToolTipPol" runat="server" CssClass="btn btn-info m-l-5 permpol"><i class="fa fa-question fa-lg"></i></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="clearfix"></div>
                        <div class="col-md-6 col-md-offset-3 m-t-10">
                            <div class="form-group">
                                <label class="f-s-14">User SAP ที่ใช้</label>
                                <asp:TextBox ID="txtWaitingPermPermSap" runat="server" CssClass="form-control" placeholder="User SAP ที่ใช้..." MaxLength="50" />
                            </div>
                        </div>

                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group">
                                <label class="f-s-14">Email ที่ขอใช้</label>
                                <asp:UpdatePanel ID="panelRadio" runat="server">
                                    <ContentTemplate>

                                        <asp:RadioButtonList ID="rdoWaitingPermPermEmailType" runat="server" CssClass="radio-list-inline-emps"
                                            AutoPostBack="true" RepeatDirection="Vertical" OnSelectedIndexChanged="onSelectedIndexChanged">
                                            <asp:ListItem Value="center">Email กลาง</asp:ListItem>
                                            <asp:ListItem Value="private">Email ส่วนตัว</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>




                        <div class="col-md-6 col-md-offset-3" id="divemail_edit" runat="server" visible="false">
                            <div class="form-group">
                                <label class="f-s-14">ประเภทอีเมล์</label>
                                <div class="row">
                                    <div class="col-md-7 p-l-0 p-r-2">
                                        <asp:DropDownList ID="ddlWaitingPermPermEmailType" AutoPostBack="true" OnSelectedIndexChanged="onSelectedIndexChanged" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0" Text="กรุณาเลือกประเภทอีเมล์"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                    <div class="col-md-5 p-l-0 p-r-0" id="divemail" visible="false" runat="server">
                                        <asp:DropDownList ID="ddl1or3_edit" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0" Text="กรุณาเลือกอีเมล์ทดแทน..."></asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-md-offset-3" id="divnewmail" runat="server" visible="false">
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-7 p-l-0 p-r-0">
                                        <asp:TextBox ID="txtWaitingPermPermEmailText" runat="server" CssClass="form-control"
                                            placeholder="E-mail..." MaxLength="100" />
                                    </div>
                                    <div class="col-md-5 p-r-0">
                                        <asp:DropDownList ID="ddlWaitingPermPermEmailDomain" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="@taokaenoi.co.th">@taokaenoi.co.th</asp:ListItem>
                                            <asp:ListItem Value="@drtobi.co.th">@drtobi.co.th</asp:ListItem>
                                            <asp:ListItem Value="@taokaenoiland.com">@taokaenoiland.com</asp:ListItem>
                                            <asp:ListItem Value="@genc.co.th">@genc.co.th</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="requiredTxtWaitingPermPermEmailText"
                                        ValidationGroup="approveWaitingPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณากรอก Email"
                                        Display="None" SetFocusOnError="true" ControlToValidate="txtWaitingPermPermEmailText" />
                                    <asp:RegularExpressionValidator ID="regularTxtWaitingPermPermEmailText" runat="server" Display="None"
                                        ControlToValidate="txtWaitingPermPermEmailText"
                                        ValidationGroup="approveWaitingPermPerm"
                                        SetFocusOnError="true" Font-Size="13px" ForeColor="Red"
                                        ErrorMessage="ใช้ได้เฉพาะภาษาอังกฤษ ตัวเลข _ . - เท่านั้น"
                                        ValidationExpression="^[^<>฿+*()[\]\\,;:\%#^\s@\'$&!@]+[a-zA-Z0-9]" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="extenderRequiredTxtWaitingPermPermEmailText" runat="Server"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="requiredTxtWaitingPermPermEmailText" Width="160"
                                        PopupPosition="BottomLeft" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="extenderRegularTxtWaitingPermPermEmailText" runat="Server"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="regularTxtWaitingPermPermEmailText" Width="160"
                                        PopupPosition="BottomLeft" />
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-8">
                                <asp:GridView ID="GvEditVPN"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    DataKeyNames="vpnidx"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="info"
                                    HeaderStyle-Height="40px"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center;">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>


                                    <Columns>
                                        <asp:TemplateField HeaderText="เลือกรายการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:CheckBox ID="chkvpnedit" runat="server" OnCheckedChanged="chkSelectedIndexChanged" AutoPostBack="true" />&nbsp;&nbsp;
                                        <asp:Literal ID="lblvpnedit" runat="server" Text='<%# Eval("vpn_name") %>' /></small>
                                                <asp:Literal ID="litvpnidxedit" Visible="false" runat="server" Text='<%# Eval("vpnidx") %>' /></small>

                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ระดับสิทธิ์ VPN" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:CheckBoxList ID="chksubvpnedit" runat="server" />
                                                    <asp:Literal ID="litvpn1idxedit" Visible="false" runat="server" /></small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="litremarkedit" Visible="false" runat="server" /></small>
                                                <asp:TextBox ID="txtremarkedit" CssClass="form-control" TextMode="MultiLine" Rows="3" runat="server" /></small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>

                                </asp:GridView>

                            </div>
                            <div class="col-lg-2">
                            </div>
                        </div>

                        <%-- 
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group">
                                <asp:CheckBox ID="chkvpnsap_edit" OnCheckedChanged="onSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="checkbox-inline-emps f-s-13" Text="ขอใช้สิทธิ์ VPN SAP" />

                            </div>
                        </div>




                      <div class="col-md-6 col-md-offset-3" id="divsapedit" runat="server">
                            <div class="form-group">
                                <label class="f-s-13">เหตุผลในการขอสิทธิ์ VPN SAP</label>
                                <asp:TextBox ID="txtreasonsap_edit" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                    ValidationGroup="approveWaitingPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="เหตุผลในการขอสิทธิ์ VPN SAP"
                                    Display="None" SetFocusOnError="true" ControlToValidate="txtreasonsap_edit" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160"
                                    PopupPosition="BottomLeft" />
                            </div>
                        </div>

                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group">
                                <asp:CheckBox ID="chkvpnbi_edit" OnCheckedChanged="onSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="checkbox-inline-emps f-s-13" Text="ขอใช้สิทธิ์ VPN BI" />

                            </div>
                        </div>


                        <div class="col-md-6 col-md-offset-3" id="divbiedit" runat="server">
                            <div class="form-group">
                                <label class="f-s-13">เหตุผลในการขอสิทธิ์ VPN BI</label>
                                <asp:TextBox ID="txtreasonbi_edit" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                    ValidationGroup="approveWaitingPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="เหตุผลในการขอสิทธิ์ VPN BI"
                                    Display="None" SetFocusOnError="true" ControlToValidate="txtreasonbi_edit" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160"
                                    PopupPosition="BottomLeft" />
                            </div>
                        </div>

                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group">
                                <asp:CheckBox ID="chkvpnexpress_edit" OnCheckedChanged="onSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="checkbox-inline-emps f-s-13" Text="ขอใช้สิทธิ์ VPN Express" />

                            </div>
                        </div>

                        <div class="col-md-6 col-md-offset-3" id="divexpressedit" runat="server">
                            <div class="form-group">
                                <label class="f-s-13">เหตุผลในการขอสิทธิ์ VPN Express</label>
                                <asp:TextBox ID="txtreasonexpress_edit" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                    ValidationGroup="approveWaitingPermPerm" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="เหตุผลในการขอสิทธิ์ VPN Express"
                                    Display="None" SetFocusOnError="true" ControlToValidate="txtreasonexpress_edit" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160"
                                    PopupPosition="BottomLeft" />
                            </div>
                        </div>--%>
                    </div>
                    <!-- END Waiting Config -->

                    <!-- START Header Emp New -->
                    <div id="_divWaitingPermPermHeaderEmpNew" runat="server" visible="false">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group">
                                <label class="f-s-14">Comment ผู้มีสิทธิ์อนุมัติ</label>
                                <asp:TextBox ID="txtWaitingPermPermHeaderEmpNewCommentText" runat="server" CssClass="form-control multiline-no-resize"
                                    placeholder="Comment ผู้มีสิทธิ์อนุมัติ..." TextMode="MultiLine" Rows="7" MaxLength="500" />
                                <asp:RequiredFieldValidator ID="requiredTxtWaitingPermPermHeaderEmpNewCommentTextApprove"
                                    ValidationGroup="approveWaitingPermPerm" runat="server" Font-Size="13px" ForeColor="Red"
                                    ErrorMessage="กรุณากรอก Comment" ControlToValidate="txtWaitingPermPermHeaderEmpNewCommentText"
                                    Display="None" SetFocusOnError="true" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="requiredTxtWaitingPermPermHeaderEmpNewCommentTextApprove" Width="160"
                                    PopupPosition="BottomLeft" />

                                <asp:RequiredFieldValidator ID="requiredTxtWaitingPermPermHeaderEmpNewCommentText"
                                    ValidationGroup="notApproveWaitingPermPermHeaderEmpNew" runat="server" Font-Size="13px" ForeColor="Red"
                                    ErrorMessage="กรุณากรอก Comment" ControlToValidate="txtWaitingPermPermHeaderEmpNewCommentText"
                                    Display="None" SetFocusOnError="true" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="extenderRequiredTxtWaitingPermPermHeaderEmpNewCommentText" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="requiredTxtWaitingPermPermHeaderEmpNewCommentText" Width="160"
                                    PopupPosition="BottomLeft" />
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <asp:LinkButton ID="btnWaitingPermPermApproveHeaderEmpNew" runat="server" CssClass="btn btn-success f-s-14" Width="100" Text="อนุมัติ"
                                OnClientClick="return confirmWithOutValidated('คุณต้องการอนุมัติใช่หรือไม่ ?', 'approveWaitingPermPerm')"
                                ValidationGroup="approveWaitingPermPerm" OnCommand="btnCommand" CommandName="btnWaitingPermPermApproveHeaderEmpNew" />
                            <asp:LinkButton ID="btnWaitingPermPermNotApproveHeaderEmpNew" runat="server" CssClass="btn btn-danger f-s-14" Width="100" Text="ไม่อนุมัติ"
                                OnClientClick="return confirmWithOutValidated('คุณต้องการไม่อนุมัติใช่หรือไม่ ?', 'notApproveWaitingPermPermHeaderEmpNew')"
                                ValidationGroup="notApproveWaitingPermPermHeaderEmpNew" OnCommand="btnCommand" CommandName="btnWaitingPermPermNotApproveHeaderEmpNew" />
                            <asp:LinkButton ID="btnWaitingPermPermEditHeaderEmpNew" runat="server" CssClass="btn btn-info f-s-14" Text="ส่งแก้ไขรายการ"
                                OnClientClick="return confirmWithOutValidated('คุณต้องการส่งแก้ไขรายการใช่หรือไม่ ?', 'EditWaitingPermPermHeaderEmpNew')"
                                ValidationGroup="notApproveWaitingPermPermHeaderEmpNew" OnCommand="btnCommand" CommandName="btnWaitingPermPermEditUser_EmpNew" />
                        </div>
                    </div>
                    <!-- END Header Emp New -->

                    <!-- START Director Emp New -->
                    <div id="_divWaitingPermPermDirectorEmpNew" runat="server" visible="false">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group">
                                <label class="f-s-14">Comment ผู้มีสิทธิ์อนุมัติ</label>
                                <asp:TextBox ID="txtWaitingPermPermDirectorEmpNewCommentText" runat="server" CssClass="form-control multiline-no-resize" placeholder="Comment ผู้มีสิทธิ์อนุมัติ..."
                                    TextMode="MultiLine" Rows="7" />
                                <asp:RequiredFieldValidator ID="requiredTxtWaitingPermPermDirectorEmpNewCommentText"
                                    ValidationGroup="notApproveWaitingPermPermDirectorEmpNew"
                                    runat="server" Font-Size="13px" ForeColor="Red" Display="None" SetFocusOnError="true"
                                    ErrorMessage="กรุณากรอก Comment"
                                    ControlToValidate="txtWaitingPermPermDirectorEmpNewCommentText" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="extenderRequiredTxtWaitingPermPermDirectorEmpNewCommentText" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" Width="160"
                                    TargetControlID="requiredTxtWaitingPermPermDirectorEmpNewCommentText"
                                    PopupPosition="BottomLeft" />
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <asp:LinkButton ID="btnWaitingPermPermApproveDirectorEmpNew" runat="server" CssClass="btn btn-success f-s-14" Width="100" Text="อนุมัติ"
                                OnClientClick="return confirmWithOutValidated('คุณต้องการอนุมัติใช่หรือไม่ ?', 'approveWaitingPermPerm')"
                                ValidationGroup="approveWaitingPermPerm" OnCommand="btnCommand" CommandName="btnWaitingPermPermApproveDirectorEmpNew" />
                            <asp:LinkButton ID="btnWaitingPermPermNotApproveDirectorEmpNew" runat="server" CssClass="btn btn-danger f-s-14" Width="100" Text="ไม่อนุมัติ"
                                OnClientClick="return confirmWithOutValidated('คุณต้องการไม่อนุมัติใช่หรือไม่ ?', 'notApproveWaitingPermPermDirectorEmpNew')"
                                ValidationGroup="notApproveWaitingPermPermDirectorEmpNew" OnCommand="btnCommand" CommandName="btnWaitingPermPermNotApproveDirectorEmpNew" />
                            <asp:LinkButton ID="btnWaitingPermPermEditDirectorEmpNew" runat="server" CssClass="btn btn-info f-s-14" Text="ส่งแก้ไขรายการ"
                                OnClientClick="return confirmWithOutValidated('คุณต้องการส่งแก้ไขรายการใช่หรือไม่ ?', 'EditWaitingPermPermDirectorEmpNew')"
                                ValidationGroup="EditWaitingPermPermDirectorEmpNew" OnCommand="btnCommand" CommandName="btnWaitingPermPermEditUser_DirectorEmpNew" />
                        </div>
                    </div>
                    <!-- END Header Emp New -->

                    <!-- START Director HR -->
                    <div id="_divWaitingPermPermDirectorHR" runat="server" visible="false">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group">
                                <label class="f-s-14">Comment ผู้มีสิทธิ์อนุมัติ</label>
                                <asp:TextBox ID="txtWaitingPermPermDirectorHRCommentText" runat="server" CssClass="form-control multiline-no-resize"
                                    placeholder="Comment ผู้มีสิทธิ์อนุมัติ..." TextMode="MultiLine" Rows="7" />
                                <asp:RequiredFieldValidator ID="requiredTxtWaitingPermPermDirectorHRCommentText"
                                    ValidationGroup="notApproveWaitingPermPermDirectorHR"
                                    runat="server" Font-Size="13px" ForeColor="Red" Display="None" SetFocusOnError="true"
                                    ErrorMessage="กรุณากรอก Comment"
                                    ControlToValidate="txtWaitingPermPermDirectorHRCommentText" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="extenderRequiredTxtWaitingPermPermDirectorHRCommentText" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" Width="160"
                                    TargetControlID="requiredTxtWaitingPermPermDirectorHRCommentText"
                                    PopupPosition="BottomLeft" />
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <asp:LinkButton ID="btnWaitingPermPermApproveDirectorHR" runat="server" CssClass="btn btn-success f-s-14" Width="100" Text="อนุมัติ"
                                OnClientClick="return confirmWithOutValidated('คุณต้องการอนุมัติใช่หรือไม่ ?', 'approveWaitingPermPerm')"
                                ValidationGroup="approveWaitingPermPerm" OnCommand="btnCommand" CommandName="btnWaitingPermPermApproveDirectorHR" />
                            <asp:LinkButton ID="btnWaitingPermPermNotApproveDirectorHR" runat="server" CssClass="btn btn-danger f-s-14" Width="100" Text="ไม่อนุมัติ"
                                OnClientClick="return confirmWithOutValidated('คุณต้องการไม่อนุมัติใช่หรือไม่ ?', 'notApproveWaitingPermPermDirectorHR')"
                                ValidationGroup="notApproveWaitingPermPermDirectorHR" OnCommand="btnCommand" CommandName="btnWaitingPermPermNotApproveDirectorHR" />
                            <asp:LinkButton ID="btnWaitingPermPermEditDirectorHR" runat="server" CssClass="btn btn-info f-s-14" Text="ส่งแก้ไขรายการ"
                                OnClientClick="return confirmWithOutValidated('คุณต้องการส่งแก้ไขรายการใช่หรือไม่ ?', 'EditWaitingPermPermDirectorHR')"
                                ValidationGroup="EditWaitingPermPermDirectorHR" OnCommand="btnCommand" CommandName="btnWaitingPermPermEditUser_DirectorHR" />
                        </div>
                    </div>
                    <!-- END Header HR -->

                    <!-- START Header IT -->
                    <div id="_divWaitingPermPermHeaderIT" runat="server" visible="false">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group">
                                <label class="f-s-14">Comment หัวหน้า IT</label>
                                <asp:TextBox ID="txtWaitingPermPermHeaderITCommentText" runat="server" CssClass="form-control multiline-no-resize"
                                    placeholder="Comment หัวหน้า IT..." TextMode="MultiLine" Rows="7" />
                                <asp:RequiredFieldValidator ID="requiredTxtWaitingPermPermHeaderITCommentText"
                                    ValidationGroup="notApproveWaitingPermPermHeaderIT"
                                    runat="server" Font-Size="13px" ForeColor="Red" Display="None" SetFocusOnError="true"
                                    ErrorMessage="กรุณากรอก Comment"
                                    ControlToValidate="txtWaitingPermPermHeaderITCommentText" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="extenderRequiredTxtWaitingPermPermHeaderITCommentText" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" Width="160"
                                    TargetControlID="requiredTxtWaitingPermPermHeaderITCommentText"
                                    PopupPosition="BottomLeft" />
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <asp:LinkButton ID="btnWaitingPermPermApproveHeaderIT" runat="server" CssClass="btn btn-success f-s-14" Width="100" Text="อนุมัติ"
                                OnClientClick="return confirmWithOutValidated('คุณต้องการอนุมัติใช่หรือไม่ ?', 'approveWaitingPermPerm')"
                                ValidationGroup="approveWaitingPermPerm" OnCommand="btnCommand" CommandName="btnWaitingPermPermApproveHeaderIT" />
                            <asp:LinkButton ID="btnWaitingPermPermNotApproveHeaderIT" runat="server" CssClass="btn btn-danger f-s-14" Width="100" Text="ไม่อนุมัติ"
                                OnClientClick="return confirmWithOutValidated('คุณต้องการไม่อนุมัติใช่หรือไม่ ?', 'notApproveWaitingPermPermHeaderIT')"
                                ValidationGroup="notApproveWaitingPermPermHeaderIT" OnCommand="btnCommand" CommandName="btnWaitingPermPermNotApproveHeaderIT" />
                            <asp:LinkButton ID="btnWaitingPermPermEditHeaderIT" runat="server" CssClass="btn btn-info f-s-14" Text="ส่งแก้ไขรายการ"
                                OnClientClick="return confirmWithOutValidated('คุณต้องการส่งแก้ไขรายการใช่หรือไม่ ?', 'EditWaitingPermPermDirectorHR')"
                                ValidationGroup="EditWaitingPermPermDirectorHR" OnCommand="btnCommand" CommandName="btnWaitingPermPermEditUser_HeaderIT" />
                        </div>
                    </div>
                    <!-- END Header IT -->

                    <!-- START Director IT -->
                    <div id="_divWaitingPermPermDirectorIT" runat="server" visible="false">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group">
                                <label class="f-s-14">Comment Director IT</label>
                                <asp:TextBox ID="txtWaitingPermPermDirectorITCommentText" runat="server" CssClass="form-control multiline-no-resize"
                                    placeholder="Comment Director IT..." TextMode="MultiLine" Rows="7" />
                                <asp:RequiredFieldValidator ID="requiredTxtWaitingPermPermDirectorITCommentText"
                                    ValidationGroup="notApproveWaitingPermPermDirectorIT"
                                    runat="server" Font-Size="13px" ForeColor="Red" Display="None" SetFocusOnError="true"
                                    ErrorMessage="กรุณากรอก Comment"
                                    ControlToValidate="txtWaitingPermPermDirectorITCommentText" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="extenderRequiredTxtWaitingPermPermDirectorITCommentText" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" Width="160"
                                    TargetControlID="requiredTxtWaitingPermPermDirectorITCommentText"
                                    PopupPosition="BottomLeft" />
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-3 text-center">
                            <asp:LinkButton ID="btnWaitingPermPermApproveDirectorIT" runat="server" CssClass="btn btn-success f-s-14" Width="100" Text="อนุมัติ"
                                OnClientClick="return confirmWithOutValidated('คุณต้องการอนุมัติใช่หรือไม่ ?', 'approveWaitingPermPerm')"
                                ValidationGroup="approveWaitingPermPerm" OnCommand="btnCommand" CommandName="btnWaitingPermPermApproveDirectorIT" />
                            <asp:LinkButton ID="btnWaitingPermPermNotApproveDirectorIT" runat="server" CssClass="btn btn-danger f-s-14" Width="100" Text="ไม่อนุมัติ"
                                OnClientClick="return confirmWithOutValidated('คุณต้องการไม่อนุมัติใช่หรือไม่ ?', 'notApproveWaitingPermPermDirectorIT')"
                                ValidationGroup="notApproveWaitingPermPermDirectorIT" OnCommand="btnCommand" CommandName="btnWaitingPermPermNotApproveDirectorIT" />
                            <asp:LinkButton ID="btnWaitingPermPermEditDirectorIT" runat="server" CssClass="btn btn-info f-s-14" Text="ส่งแก้ไขรายการ"
                                OnClientClick="return confirmWithOutValidated('คุณต้องการส่งแก้ไขรายการใช่หรือไม่ ?', 'EditWaitingPermPermDirectorHR')"
                                ValidationGroup="EditWaitingPermPermDirectorIT" OnCommand="btnCommand" CommandName="btnWaitingPermPermEditUser_DirectorIT" />
                        </div>
                    </div>
                    <!-- END Header IT -->

                    <!-- START IT -->
                    <div id="_divWaitingPermPermIT" runat="server" visible="false">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group" id="divaditclosejob" runat="server">
                                <label class="f-s-14">สิทธิ์การใช้งาน AD</label>
                                <asp:TextBox ID="txtWaitingPermPermITAD" runat="server" CssClass="form-control"
                                    placeholder="สิทธิ์การใช้งาน AD..." MaxLength="100" />
                                <asp:RequiredFieldValidator ID="requiredTxtWaitingPermPermITAD"
                                    ValidationGroup="approveWaitingPermPerm"
                                    runat="server" Font-Size="13px" ForeColor="Red" Display="None" SetFocusOnError="true"
                                    ErrorMessage="กรุณากรอกสิทธิ์การใช้งาน AD"
                                    ControlToValidate="txtWaitingPermPermITAD" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="extenderRequiredTxtWaitingPermPermITAD" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" Width="160"
                                    TargetControlID="requiredTxtWaitingPermPermITAD"
                                    PopupPosition="BottomLeft" />
                            </div>
                            <div class="form-group">
                                <label class="f-s-14">Comment IT</label>
                                <asp:TextBox ID="txtWaitingPermPermITCommentText" runat="server" CssClass="form-control multiline-no-resize"
                                    placeholder="Comment IT..." TextMode="MultiLine" Rows="7" />
                                <asp:RequiredFieldValidator ID="requiredTxtWaitingPermPermITCommentText"
                                    ValidationGroup="notApproveWaitingPermPermIT"
                                    runat="server" Font-Size="13px" ForeColor="Red" Display="None" SetFocusOnError="true"
                                    ErrorMessage="กรุณากรอก Comment"
                                    ControlToValidate="txtWaitingPermPermITCommentText" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="extenderRequiredTxtWaitingPermPermITCommentText" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" Width="160"
                                    TargetControlID="requiredTxtWaitingPermPermITCommentText"
                                    PopupPosition="BottomLeft" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-0 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 p-l-0 p-r-0 text-center">
                            <div class="col-xs-12 col-sm-4 col-md-12 col-lg-3 m-b-3">
                                <asp:LinkButton ID="btnWaitingPermPermApproveIT" runat="server"
                                    CssClass="btn btn-success f-s-14" Width="100%" Text="บันทึก"
                                    OnClientClick="return confirmWithOutValidated('คุณต้องการอนุมัติใช่หรือไม่ ?', 'approveWaitingPermPerm')"
                                    ValidationGroup="approveWaitingPermPerm" OnCommand="btnCommand" CommandName="btnWaitingPermPermApproveIT" />
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-12 col-lg-3 m-b-3">
                                <asp:LinkButton ID="btnWaitingPermPermSaveCommentIT" runat="server"
                                    CssClass="btn btn-primary f-s-14" Width="100%" Text="บันทึก Comment"
                                    OnClientClick="return confirmWithOutValidated('คุณต้องการส่ง Comment ใช่หรือไม่ ?', 'notApproveWaitingPermPermIT')"
                                    ValidationGroup="notApproveWaitingPermPermIT" OnCommand="btnCommand" CommandName="btnWaitingPermPermSaveCommentIT" />
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-12 col-lg-3 m-b-3">
                                <asp:LinkButton ID="btnWaitingPermPermNotApproveIT" runat="server"
                                    CssClass="btn btn-danger f-s-14" Width="100%" Text="ไม่เห็นควร"
                                    OnClientClick="return confirmWithOutValidated('คุณต้องการไม่อนุมัติใช่หรือไม่ ?', 'notApproveWaitingPermPermIT')"
                                    ValidationGroup="notApproveWaitingPermPermIT" OnCommand="btnCommand" CommandName="btnWaitingPermPermNotApproveIT" />

                                <asp:LinkButton ID="btnWaitingPermPermBackIT" runat="server"
                                    CssClass="btn btn-default f-s-14" Width="100%" Text="กลับสู่หน้าหลัก"
                                    OnCommand="btnCommand" CommandName="btnWaitingPermPermBackIT" />
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-12 col-lg-3 m-b-3">

                                <asp:LinkButton ID="btnWaitingPermPermEditIT" Width="100%" runat="server" CssClass="btn btn-info f-s-14" Text="ส่งแก้ไขรายการ"
                                    OnClientClick="return confirmWithOutValidated('คุณต้องการส่งแก้ไขรายการใช่หรือไม่ ?', 'EditWaitingPermPermDirectorHR')"
                                    ValidationGroup="EditWaitingPermPermDirectorIT" OnCommand="btnCommand" CommandName="btnWaitingPermPermEditUser_IT" />
                            </div>

                        </div>
                    </div>
                    <!-- END IT -->

                    <!-- START Edit User -->
                    <div id="_divWaitingPermPermUser" runat="server" visible="false">

                        <div class="col-md-6 col-md-offset-3 text-center">
                            <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-success f-s-14" Width="150" Text="บันทึก"
                                OnClientClick="return confirmWithOutValidated('คุณต้องการบันทึกแก้ไขรายการใช่หรือไม่ ?', 'approveWaitingPermPerm')"
                                ValidationGroup="approveWaitingPermPerm" OnCommand="btnCommand" CommandName="btnWaitingPermPermEditUser" />
                            <asp:LinkButton ID="LinkButton2" runat="server" CssClass="btn btn-default f-s-14" Width="150" Text="กลับสู่หน้าหลัก"
                                OnCommand="btnCommand" CommandName="btnWaitingPermPermBackIT" />

                        </div>
                    </div>
                    <!-- END Header Emp New -->
                </div>
            </div>
        </div>
        <!-- End Permission Permanent Area -->
        <!-- Start Permission Temporary Area -->
        <div id="_divWaitingPermTemp" runat="server" visible="false">
            <div class="alert alert-info f-s-14" role="alert">รายการที่รออนุมัติ : สิทธิ์ชั่วคราว</div>
            <asp:GridView ID="gvWaitingPermTemp"
                runat="server"
                AutoGenerateColumns="false"
                DataKeyNames="u0_perm_temp_idx"
                CssClass="table table-striped table-bordered table-responsive col-md-12"
                HeaderStyle-CssClass="info"
                AllowPaging="true"
                PageSize="10"
                OnPageIndexChanging="onPageIndexChanging"
                AutoPostBack="False">
                <PagerStyle CssClass="pageCustom" HorizontalAlign="Center" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div class="text-center">ไม่พบข้อมูล</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="รหัสเอกสาร" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="12%">
                        <ItemTemplate>
                            <asp:Label ID="u0PermTempCodeId" runat="server" Text='<%# Eval("u0_perm_temp_code_id") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียดผู้ทำรายการ" ItemStyle-CssClass="f-s-13 text-left" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="30%">
                        <ItemTemplate>
                            <p>
                                <b>ชื่อ - สกุล:</b>
                                <asp:Label ID="u0EmpNameTH" runat="server" Text='<%# Eval("emp_name_th") %>' />
                            </p>
                            <p>
                                <b>บริษัท:</b>
                                <asp:Label ID="u0EmpOrgTH" runat="server" Text='<%# Eval("org_name_th") %>' />
                            </p>
                            <p>
                                <b>ฝ่าย:</b>
                                <asp:Label ID="u0EmpDeptTH" runat="server" Text='<%# Eval("dept_name_th") %>' />
                            </p>
                            <b>วันที่ทำรายการ:</b>
                            <asp:Label ID="u0CreatedAt" runat="server" Text='<%# Eval("u0_perm_temp_created_at") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียดการติดต่อ" ItemStyle-CssClass="f-s-13 text-left" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="24%">
                        <ItemTemplate>
                            <p>
                                <b>ชื่อบริษัท/หน่วยงาน:</b>
                                <asp:Label ID="u0PermTempCompName" runat="server" Text='<%# Eval("u0_perm_temp_comp_name") %>' />
                            </p>
                            <p>
                                <b>เหตุผลที่ขอ:</b>
                                <asp:Label ID="u0PermTempReason" runat="server" Text='<%# Eval("u0_perm_temp_detail") %>' />
                            </p>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <asp:Label ID="u0PermTempNodeTo1" runat="server" Text='<%# Eval("status_to_1") %>' />
                            <br />
                            โดย<br />
                            <asp:Label ID="u0PermTempNodeTo2" runat="server" Text='<%# Eval("status_to_2") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="9%">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnWaitingPermTemp_ViewDetail" runat="server" CssClass="btn btn-info"
                                OnCommand="btnCommand" CommandName="btnWaitingPermTemp_ViewDetail" CommandArgument='<%# Eval("u0_perm_temp_idx") %>'>
                        <i class="fa fa-file"></i>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div id="_divWaitingPermTemp_Detail" runat="server" visible="false">
            <asp:LinkButton ID="btnBack_ToDivWaitingPermTemp" runat="server" CssClass="btn btn-danger m-b-10 f-s-14" OnCommand="btnCommand"
                CommandName="btnBack_ToDivWaitingPermTemp"><i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ</asp:LinkButton>
            <div class="panel panel-info m-b-10">
                <div class="panel-heading">รายละเอียด</div>
                <div class="panel-body">
                    <div class="col-md-8 col-md-offset-1 word-wrap">
                        <asp:Label ID="lblValueU0PermTempIdx" runat="server" Visible="false" />
                        <asp:Label ID="lblValueU0PermTempNodeId" runat="server" Visible="false" />
                        <div class="col-md-5 m-b-5 p-l-0">
                            <asp:Label ID="lblTitleWaitingPermTempCode" runat="server" CssClass="f-s-13 f-bold" Text="รหัสเอกสาร :" />
                        </div>
                        <div class="col-md-7 m-b-5 p-l-0">
                            <asp:Label ID="lblValueWaitingPermTempCode" runat="server" CssClass="f-s-13" />
                            <asp:Label ID="lblvpnidxedittemp" runat="server" Visible="false" CssClass="f-s-13" />

                        </div>
                        <div class="col-md-5 m-b-5 p-l-0">
                            <asp:Label ID="lblTitleWaitingPermTempCreator" runat="server" CssClass="f-s-13 f-bold" Text="ผู้ทำรายการ :" />
                        </div>
                        <div class="col-md-7 m-b-5 p-l-0">
                            <asp:Label ID="lblValueWaitingPermTempCreator" runat="server" CssClass="f-s-13" />
                        </div>
                        <div class="col-md-5 m-b-5 p-l-0">
                            <asp:Label ID="lblTitleWaitingPermTempOrgName" runat="server" CssClass="f-s-13 f-bold" Text="บริษัท :" />
                        </div>
                        <div class="col-md-7 m-b-5 p-l-0">
                            <asp:Label ID="lblValueWaitingPermTempOrgName" runat="server" CssClass="f-s-13" />
                        </div>
                        <div class="col-md-5 m-b-5 p-l-0">
                            <asp:Label ID="lblTitleWaitingPermTempDeptName" runat="server" CssClass="f-s-13 f-bold" Text="ฝ่าย :" />
                        </div>
                        <div class="col-md-7 m-b-5 p-l-0">
                            <asp:Label ID="lblValueWaitingPermTempDeptName" runat="server" CssClass="f-s-13" />
                        </div>
                        <div class="col-md-5 m-b-5 p-l-0">
                            <asp:Label ID="lblTitleWaitingPermTempSecName" runat="server" CssClass="f-s-13 f-bold" Text="แผนก :" />
                        </div>
                        <div class="col-md-7 m-b-5 p-l-0">
                            <asp:Label ID="lblValueWaitingPermTempSecName" runat="server" CssClass="f-s-13" />
                        </div>
                        <div class="col-md-5 m-b-5 p-l-0">
                            <asp:Label ID="lblTitleWaitingPermTempPosName" runat="server" CssClass="f-s-13 f-bold" Text="ตำแหน่ง :" />
                        </div>
                        <div class="col-md-7 m-b-5 p-l-0">
                            <asp:Label ID="lblValueWaitingPermTempPosName" runat="server" CssClass="f-s-13" />
                        </div>
                        <div class="col-md-5 m-b-5 p-l-0">
                            <asp:Label ID="lblTitleWaitingPermTempCreatedAt" runat="server" CssClass="f-s-13 f-bold" Text="วันที่ทำรายการ :" />
                        </div>
                        <div class="col-md-7 m-b-5 p-l-0">
                            <asp:Label ID="lblValueWaitingPermTempCreatedAt" runat="server" CssClass="f-s-13" />
                        </div>
                        <div class="col-md-5 m-b-5 p-l-0">
                            <asp:Label ID="lblTitleWaitingPermTempCompName" runat="server" CssClass="f-s-13 f-bold" Text="บริษัทที่มาติดต่อ :" />
                        </div>
                        <div class="col-md-7 m-b-5 p-l-0">
                            <asp:Label ID="lblValueWaitingPermTempCompName" runat="server" CssClass="f-s-13" />
                        </div>
                        <div class="col-md-5 m-b-5 p-l-0">
                            <asp:Label ID="lblTitleWaitingPermTempReason" runat="server" CssClass="f-s-13 f-bold" Text="เหตุผลการขอใช้สิทธิ์ :" />
                        </div>
                        <div class="col-md-7 m-b-5 p-l-0">
                            <asp:Label ID="lblValueWaitingPermTempReason" runat="server" CssClass="f-s-13" />
                        </div>
                        <div id="groupDateWaiting" runat="server" visible="false">
                            <div class="col-md-5 m-b-5 p-l-0">
                                <asp:Label ID="lblTitleWaitingU0DateStart" runat="server" CssClass="f-s-13 f-bold" Text="วันที่เริ่มใช้สิทธิ์ :" />
                            </div>
                            <div class="col-md-7 m-b-5 p-l-0">
                                <asp:Label ID="lblValueWaitingU0DateStart" runat="server" CssClass="f-s-13" />
                            </div>
                            <div class="col-md-5 m-b-5 p-l-0">
                                <asp:Label ID="lblTitleWaitingU0DateEnd" runat="server" CssClass="f-s-13 f-bold" Text="วันที่สิ้นสุดใช้สิทธิ์ :" />
                            </div>
                            <div class="col-md-7 m-b-5 p-l-0">
                                <asp:Label ID="lblValueWaitingU0DateEnd" runat="server" CssClass="f-s-13" />
                            </div>
                        </div>
                        <div class="col-md-5 m-b-5 p-l-0">
                            <asp:Label ID="lblTitleWaitingAmountContact" runat="server" CssClass="f-s-13 f-bold" Text="จำนวน :" />
                        </div>
                        <div class="col-md-7 m-b-5 p-l-0">
                            <asp:Label ID="lblValueWaitingAmountContact" runat="server" CssClass="f-s-13" />
                        </div>
                        <div class="col-md-5 m-b-5 p-l-0">
                            <asp:Label ID="lblTitleWaitingPermTempNode" runat="server" CssClass="f-s-13 f-bold" Text="สถานะรายการ :" />
                        </div>
                        <div class="col-md-7 m-b-5 p-l-0">
                            <asp:Label ID="lblValueWaitingPermTempNode" runat="server" CssClass="f-s-13" />
                        </div>
                        <div id="_divWaitingPermTempCmDirectorCreator" runat="server" visible="false" class="m-t-10 text-danger">
                            <div class="col-md-5 m-b-5 p-l-0">
                                <asp:Label ID="lblTitleWaitingPermTempCmDirectorCreator" runat="server" CssClass="f-s-13 f-bold"
                                    Text="Comment Director ผู้ทำรายการ :" />
                            </div>
                            <div class="col-md-7 m-b-5 p-l-0">
                                <asp:Label ID="lblValueWaitingPermTempCmDirectorCreator" runat="server" CssClass="f-s-13" />
                            </div>
                        </div>
                        <div id="_divWaitingPermTempCmHeaderIT" runat="server" visible="false" class="text-danger">
                            <div class="col-md-5 m-b-5 p-l-0">
                                <asp:Label ID="lblTitleWaitingPermTempCmHeaderIT" runat="server" CssClass="f-s-13 f-bold" Text="Comment หัวหน้า IT :" />
                            </div>
                            <div class="col-md-7 m-b-5 p-l-0">
                                <asp:Label ID="lblValueWaitingPermTempCmHeaderIT" runat="server" CssClass="f-s-13" />
                            </div>
                        </div>
                        <div id="_divWaitingPermTempCmDirectorIT" runat="server" visible="false" class="m-t-10 text-danger">
                            <div class="col-md-5 m-b-5 p-l-0">
                                <asp:Label ID="lblTitleWaitingPermTempCmDirectorIT" runat="server" CssClass="f-s-13 f-bold" Text="Comment Director IT :" />
                            </div>
                            <div class="col-md-7 m-b-5 p-l-0">
                                <asp:Label ID="lblValueWaitingPermTempCmDirectorIT" runat="server" CssClass="f-s-13" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 m-t-10">
                        <div class="panel panel-info">
                            <div class="panel-heading">รายชื่อผู้มาติดต่อ</div>
                            <div class="panel-body">
                                <asp:GridView ID="gvWaitingU1PermTempContactsList" runat="server"
                                    AutoGenerateColumns="false"
                                    DataKeyNames="u1_perm_temp_idx"
                                    CssClass="table table-striped table-bordered table-responsive col-md-12"
                                    HeaderStyle-CssClass="default"
                                    OnRowDataBound="onRowDataBound"
                                    AutoPostBack="False">
                                    <PagerStyle CssClass="pageCustom" HorizontalAlign="Center" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                    <EmptyDataTemplate>
                                        <div class="text-center">ไม่พบข้อมูล</div>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField HeaderText="การอนุมัติ" ItemStyle-CssClass="f-s-13 text-center cs-pointer" HeaderStyle-CssClass="f-s-13 text-center"
                                            HeaderStyle-Width="9%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblU1PermTempIdx" runat="server" Text='<%# Eval("u1_perm_temp_idx") %>' Visible="false" />
                                                <asp:Label ID="lblU1PermTempStatus" runat="server" Text='<%# Eval("u1_perm_temp_status") %>' Visible="false" />
                                                <asp:CheckBox ID="chkWaitingU1PermTempContactList" runat="server" Checked="true" CssClass="chk-f-s-16" />
                                                <asp:Label ID="lblAppLastNode" runat="server" Visible="false" CssClass="text-success">
                                       <i class="fa fa-check"></i>
                                                </asp:Label>
                                                <asp:Label ID="lblU1PermTempNotApp" runat="server" Text="ไม่อนุมัติ" CssClass="text-danger" Visible="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="รหัสบัตรประชาชน" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblU1PermTempIDCard" runat="server"
                                                    Text='<%# idCardPattern((string)Eval("u1_perm_temp_idcard"), true) %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ชื่อ - สกุล" ItemStyle-CssClass="f-s-13 text-left" HeaderStyle-CssClass="f-s-13 text-center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblU1PermTempFullNameTH" runat="server" Text='<%# Eval("u1_perm_temp_fullname") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="วันที่เริ่มใช้สิทธิ์" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblU1PermTempStart" runat="server" Text='<%# Eval("u1_perm_temp_start") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="วันที่สิ้นสุดใช้สิทธิ์" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblU1PermTempEnd" runat="server" Text='<%# Eval("u1_perm_temp_end") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Username" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblU1PermTempNotAppUsername" runat="server" Text="--" Visible="false" />
                                                <asp:TextBox ID="txtU1PermTempUsername" runat="server" CssClass="form-control" MaxLength="50"
                                                    placeholder="Username..." />
                                                <asp:RequiredFieldValidator ID="requiredTxtU1PermTempUsername"
                                                    ValidationGroup="savePermTempIT" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณากรอก Username"
                                                    Display="None" SetFocusOnError="true" ControlToValidate="txtU1PermTempUsername" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="extenderRequiredTxtU1PermTempUsername" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="requiredTxtU1PermTempUsername" Width="160"
                                                    PopupPosition="BottomLeft" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Password" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center">
                                            <ItemTemplate>
                                                <asp:Label ID="lblU1PermTempNotAppPassword" runat="server" Text="--" Visible="false" />
                                                <asp:TextBox ID="txtU1PermTempPassword" runat="server" CssClass="form-control" MaxLength="50"
                                                    placeholder="Password..." />
                                                <asp:RequiredFieldValidator ID="requiredTxtU1PermTempPassword" runat="server"
                                                    ValidationGroup="savePermTempIT" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณากรอก Password"
                                                    Display="None" SetFocusOnError="true" ControlToValidate="txtU1PermTempPassword" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="extenderRequiredTxtU1PermTempPassword" runat="server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="requiredTxtU1PermTempPassword" Width="160"
                                                    PopupPosition="BottomLeft" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 m-t-10" id="diveditvpntemp" runat="server">
                        <div class="panel panel-info">
                            <div class="panel-heading">รายการขอใข้สิทธิ์ VPN</div>
                            <div class="panel-body">
                                <asp:GridView ID="GvEditVPNTemp"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    DataKeyNames="vpnidx"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="info"
                                    HeaderStyle-Height="40px"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center;">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>


                                    <Columns>
                                        <asp:TemplateField HeaderText="เลือกรายการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:CheckBox ID="chkvpnedittemp" runat="server" OnCheckedChanged="chkSelectedIndexChanged" AutoPostBack="true" />&nbsp;&nbsp;
                                        <asp:Literal ID="lblvpnedittemp" runat="server" Text='<%# Eval("vpn_name") %>' /></small>
                                                <asp:Literal ID="litvpnidxedittemp" Visible="false" runat="server" Text='<%# Eval("vpnidx") %>' /></small>

                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ระดับสิทธิ์ VPN" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:CheckBoxList ID="chksubvpnedittemp" runat="server" />
                                                    <asp:Literal ID="litvpn1idxedittemp" Visible="false" runat="server" /></small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="litremarkedittemp" Visible="false" runat="server" /></small>
                                                <asp:TextBox ID="txtremarkedittemp" CssClass="form-control" TextMode="MultiLine" Rows="3" runat="server" /></small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>

                                </asp:GridView>

                                <asp:GridView ID="gvFileTemp_Wait" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                    HeaderStyle-CssClass="info"
                                    OnRowDataBound="Master_RowDataBound"
                                    BorderStyle="None"
                                    CellSpacing="2"
                                    Font-Size="Small">

                                    <Columns>
                                        <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                            <ItemTemplate>
                                                <div class="col-lg-10">
                                                    <asp:Literal ID="ltFileName11_tempwait" runat="server" Text='<%# Eval("FileName") %>' />
                                                </div>
                                                <div class="col-lg-2">
                                                    <asp:HyperLink runat="server" ID="btnDL11_tempwait" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                    <asp:HiddenField runat="server" ID="hidFile11_tempwait" Value='<%# Eval("Download") %>' />
                                                </div>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>

                            </div>
                        </div>
                    </div>



                    <div class="col-md-12 m-t-10">
                        <!-- Start Node Director of Creator -->
                        <div id="_divWaitingPermTempDirectorOfCreator" runat="server" visible="false">
                            <div class="form-group">
                                <label class="f-s-14">Comment Director ผู้ทำรายการ</label>
                                <asp:TextBox ID="txtWaitingPermTempCommentDirectorOfCreator" runat="server" CssClass="form-control multiline-no-resize"
                                    TextMode="MultiLine" Rows="5" placeholder="Comment Director ผู้ทำรายการ..." />
                                <asp:RequiredFieldValidator ID="requiredTxtWaitingPermTempCommentDirectorOfCreator"
                                    ValidationGroup="commentPermTempDirectorOfCreator" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณากรอก Comment"
                                    Display="None" SetFocusOnError="true" ControlToValidate="txtWaitingPermTempCommentDirectorOfCreator" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="extenderRequiredTxtWaitingPermTempCommentDirectorOfCreator" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="requiredTxtWaitingPermTempCommentDirectorOfCreator" Width="160"
                                    PopupPosition="BottomLeft" />
                                <div class="text-center m-t-10 m-b-10">
                                    <asp:LinkButton ID="btnWaitingPermTempAppDirectorOfCreator" runat="server" CssClass="btn btn-success f-s-14 m-r-10" Width="90"
                                        OnClientClick="return confirmWithOutValidated('คุณต้องการอนุมัติใช่หรือไม่ ?', 'commentPermTempDirectorOfCreator')"
                                        ValidationGroup="commentPermTempDirectorOfCreator" Text="อนุมัติ" OnCommand="btnCommand"
                                        CommandName="btnWaitingPermTempAppDirectorOfCreator" />
                                    <asp:LinkButton ID="btnWaitingPermTempNotAppDirectorOfCreator" runat="server" CssClass="btn btn-danger f-s-14" Width="90"
                                        OnClientClick="return confirmWithOutValidated('คุณต้องการไม่อนุมัติใช่หรือไม่ ?', 'commentPermTempDirectorOfCreator')"
                                        ValidationGroup="commentPermTempDirectorOfCreator" Text="ไม่อนุมัติ" OnCommand="btnCommand"
                                        CommandName="btnWaitingPermTempNotAppDirectorOfCreator" />
                                </div>
                            </div>
                        </div>
                        <!-- End Node Director of Creator -->

                        <!-- Start Node Header of IT -->
                        <div id="_divWaitingPermTempHeaderOfIT" runat="server" visible="false">
                            <div class="form-group">
                                <label class="f-s-14">Comment หัวหน้า IT</label>
                                <asp:TextBox ID="txtWaitingPermTempCommentHeaderIT" runat="server" CssClass="form-control multiline-no-resize" TextMode="MultiLine"
                                    Rows="5" placeholder="Comment หัวหน้า IT..." />
                                <asp:RequiredFieldValidator ID="requiredTxtWaitingPermTempCommentHeaderIT"
                                    ValidationGroup="commentPermTempHeaderIT" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณากรอก Comment"
                                    Display="None" SetFocusOnError="true" ControlToValidate="txtWaitingPermTempCommentHeaderIT" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="extenderRequiredTxtWaitingPermTempCommentHeaderIT" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="requiredTxtWaitingPermTempCommentHeaderIT" Width="160"
                                    PopupPosition="BottomLeft" />
                                <div class="text-center m-t-10 m-b-10">
                                    <asp:LinkButton ID="btnWaitingPermTempAppHeaderIT" runat="server" CssClass="btn btn-success f-s-14 m-r-10" Width="90"
                                        OnClientClick="return confirmWithOutValidated('คุณต้องการอนุมัติใช่หรือไม่ ?', 'commentPermTempHeaderIT')"
                                        ValidationGroup="commentPermTempHeaderIT" Text="อนุมัติ" OnCommand="btnCommand"
                                        CommandName="btnWaitingPermTempAppHeaderIT" />
                                    <asp:LinkButton ID="btnWaitingPermTempNotAppHeaderIT" runat="server" CssClass="btn btn-danger f-s-14" Width="90"
                                        OnClientClick="return confirmWithOutValidated('คุณต้องการไม่อนุมัติใช่หรือไม่ ?', 'commentPermTempHeaderIT')"
                                        ValidationGroup="commentPermTempHeaderIT" Text="ไม่อนุมัติ" OnCommand="btnCommand"
                                        CommandName="btnWaitingPermTempNotAppHeaderIT" />
                                </div>
                            </div>
                        </div>
                        <!-- End Node Header of IT -->

                        <!-- Start Node Director of IT -->
                        <div id="_divWaitingPermTempDirectorOfIT" runat="server" visible="false">
                            <div class="form-group">
                                <label class="f-s-14">Comment Director IT</label>
                                <asp:TextBox ID="txtWaitingPermTempCommentDirectorIT" runat="server" CssClass="form-control multiline-no-resize" TextMode="MultiLine"
                                    Rows="5" placeholder="Comment Director IT..." />
                                <asp:RequiredFieldValidator ID="requiredTxtWaitingPermTempCommentDirectorIT"
                                    ValidationGroup="commentPermTempDirectorIT" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณากรอก Comment"
                                    Display="None" SetFocusOnError="true" ControlToValidate="txtWaitingPermTempCommentDirectorIT" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="extenderRequiredTxtWaitingPermTempCommentDirectorIT" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="requiredTxtWaitingPermTempCommentDirectorIT" Width="160"
                                    PopupPosition="BottomLeft" />
                                <div class="text-center m-t-10 m-b-10">
                                    <asp:LinkButton ID="btnWaitingPermTempAppDirectorIT" runat="server" CssClass="btn btn-success f-s-14 m-r-10" Width="90"
                                        OnClientClick="return confirmWithOutValidated('คุณต้องการอนุมัติใช่หรือไม่ ?', 'commentPermTempDirectorIT')"
                                        ValidationGroup="commentPermTempDirectorIT" Text="อนุมัติ" OnCommand="btnCommand" CommandName="btnWaitingPermTempAppDirectorIT" />
                                    <asp:LinkButton ID="btnWaitingPermTempNotAppDirectorIT" runat="server" CssClass="btn btn-danger f-s-14" Width="90"
                                        OnClientClick="return confirmWithOutValidated('คุณต้องการไม่อนุมัติใช่หรือไม่ ?', 'commentPermTempDirectorIT')"
                                        ValidationGroup="commentPermTempDirectorIT" Text="ไม่อนุมัติ" OnCommand="btnCommand"
                                        CommandName="btnWaitingPermTempNotAppDirectorIT" />
                                </div>
                            </div>
                        </div>
                        <!-- End Node Director of IT -->

                        <!-- Start Node IT -->
                        <div id="_divWaitingPermTempIT" runat="server" visible="false">
                            <div class="text-center m-b-10">
                                <asp:LinkButton ID="btnWaitingPermTempSaveIT" runat="server" CssClass="btn btn-success f-s-14" Width="90"
                                    OnClientClick="return confirmWithOutValidated('คุณต้องการบันทึกใช่หรือไม่ ?', 'savePermTempIT')"
                                    ValidationGroup="savePermTempIT" Text="บันทึก" OnCommand="btnCommand" CommandName="btnWaitingPermTempSaveIT" />
                            </div>
                        </div>
                        <!-- End Node IT -->
                    </div>
                </div>
            </div>
        </div>
        <!-- End Permission Temporary Area -->
    </div>
    <!--*** End DIV Waiting ***-->
    <!--*** Start JS Area ***-->
    <script type="text/javascript">
        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.datetimepicker-from').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'months'),
                ignoreReadonly: true
            });
            $('.datetimepicker-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                $('#<%= txtSearchFromHidden.ClientID %>').val($('.datetimepicker-from').val());
            });
            $('.show-from-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: moment().add(-1, 'months'),
                ignoreReadonly: true
            });
            $('.show-to-onclick').click(function () {
                $('.datetimepicker-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                $('#<%= txtSearchToHidden.ClientID %>').val($('.datetimepicker-to').val());
            });
            /* START Filter DatetimePicker Permission Temporary */
            $('.datetimepicker-filter-from').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.datetimepicker-filter-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                $('#<%= txtFilterIndexPermTempFromHidden.ClientID %>').val($('.datetimepicker-filter-from').val());
            });
            $('.show-filter-from-onclick').click(function () {
                $('.datetimepicker-filter-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-to').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-to-onclick').click(function () {
                $('.datetimepicker-filter-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                $('#<%= txtFilterIndexPermTempToHidden.ClientID %>').val($('.datetimepicker-filter-to').val());
            });
            $('.datetimepicker-filter-only').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-only-onclick').click(function () {
                $('.datetimepicker-filter-only').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-only').on('dp.change', function (e) {
                $('#<%= txtFilterIndexPermTempOnlyHidden.ClientID %>').val($('.datetimepicker-filter-only').val());
            });
            /* END Filter Datetimepicker Permission Temporary */
            /* START Filter Datetimepicker Permission Permanent */
            $('.datetimepicker-filter-perm-from').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.datetimepicker-filter-perm-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-perm-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-perm-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                $('#<%= txtFilterIndexPermPermFromHidden.ClientID %>').val($('.datetimepicker-filter-perm-from').val());
            });
            $('.show-filter-perm-from-onclick').click(function () {
                $('.datetimepicker-filter-perm-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-to-onclick').click(function () {
                $('.datetimepicker-filter-perm-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-perm-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-perm-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                $('#<%= txtFilterIndexPermPermToHidden.ClientID %>').val($('.datetimepicker-filter-perm-to').val());
            });
            $('.datetimepicker-filter-perm-only').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-only-onclick').click(function () {
                $('.datetimepicker-filter-perm-only').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-only').on('dp.change', function (e) {
                $('#<%= txtFilterIndexPermPermOnlyHidden.ClientID %>').val($('.datetimepicker-filter-perm-only').val());
            });
            /* END Filter Datetimepicker Permission Permanent */
        });
    </script>

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script src='<%=ResolveUrl("~/Scripts/jquery.MultiFileNew.js")%>'></script>
    <script type="text/javascript">
        $(".multi").MultiFile();

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {

            $(".multi").MultiFile();
        })
    </script>

    <!--*** End JS Area ***-->
</asp:Content>
