﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="m0_sap_module_lv5.aspx.cs" Inherits="websystem_MasterData_m0_sap_module_lv5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litdebug" runat="server"></asp:Literal>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-gears"></i><strong>&nbsp; ประเภท Adjust  (แก้ไข - LV 5)</strong></h3>
                        </div>

                        <div class="panel-body">

                            <div class="form-group">

                                <asp:LinkButton ID="btnaddmodule" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="AddModule Lv5" runat="server" CommandName="CmdAddModule"
                                    OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>

                            <%------------------------ Div ADD  ------------------------%>

                            <asp:Panel ID="Panel_AddModule" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Add Module(LV 5)</strong></h4>
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="lbl_system" CssClass="col-sm-3 control-label" runat="server" Text="System" />
                                                <div class="col-sm-8">
                                                    <asp:DropDownList ID="ddl_System" AutoPostBack="true" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="0">กรุณาเลือกระบบ....</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="Re_ddl_System" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddl_System" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกระบบ"
                                                    ValidationExpression="กรุณาเลือกระบบ"
                                                    InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddl_System" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="lbl_code" runat="server" Text="Code" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtcode" runat="server" CssClass="form-control" PlaceHolder="Code..." />
                                                </div>

                                                <asp:RequiredFieldValidator ID="Re_txtcode" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="txtcode" Font-Size="11"
                                                    ErrorMessage="Please enter code"
                                                    ValidationExpression="Please enter code"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtcode" Width="160" />

                                                <asp:RegularExpressionValidator ID="Re_txtcode1" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="กรุณาตรวจสอบข้อมูลที่กรอก(?/)" Font-Size="11"
                                                    ControlToValidate="txtcode"
                                                    ValidationExpression="^[ก-๙0-9a-zA-Z-]{1,10}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtcode1" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="lbl_name" runat="server" Text="Name" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtname" runat="server" CssClass="form-control" PlaceHolder="Name..." />
                                                </div>

                                                <asp:RequiredFieldValidator ID="Re_txtname" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="txtname" Font-Size="11"
                                                    ErrorMessage="Please enter name"
                                                    ValidationExpression="Please enter name"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtname" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                <div class="col-sm-8">
                                                    <asp:DropDownList ID="ddStatusadd" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="lbladd" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />

                                </div>
                            </asp:Panel>

                            <div id="div_selectGvmaster" runat="server" visible="false">
                                <asp:GridView ID="GvMaster" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="primary"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    DataKeyNames="MS5IDX"
                                    PageSize="10"
                                    OnRowDataBound="Master_RowDataBound"
                                    OnRowEditing="Master_RowEditing"
                                    OnRowCancelingEdit="Master_RowCancelingEdit"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowUpdating="Master_RowUpdating">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="#">

                                            <ItemTemplate>
                                                <asp:Label ID="lb_MS4IDX" runat="server" Visible="false" Text='<%# Eval("MS5IDX") %>' />
                                                <%# (Container.DataItemIndex +1) %>
                                            </ItemTemplate>
                                            <EditItemTemplate>
                                                <div class="form-horizontal" role="form">
                                                    <div class="panel-heading">
                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtMS5IDX_edit" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("MS5IDX")%>' />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="System" />
                                                            <div class="col-sm-8">
                                                                <asp:Label ID="lbl_SysIDX_edit" runat="server" Text='<%# Bind("SysIDX") %>' Visible="false" />
                                                                <asp:DropDownList ID="ddl_SysIDX_edit" AutoPostBack="true" Enabled="false" CssClass="form-control" runat="server">
                                                                </asp:DropDownList>
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="Re_ddl_SysIDX_edit" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                ControlToValidate="ddl_SysIDX_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือก System"
                                                                ValidationExpression="กรุณาเลือก System" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddl_SysIDX_edit" Width="160" />


                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label26" runat="server" Text="Code" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txtMS5_Code_edit" runat="server" CssClass="form-control" Text='<%# Eval("MS5_Code")%>' />

                                                            </div>

                                                            <asp:RequiredFieldValidator ID="Re_txtMS5_Code_edit" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                ControlToValidate="txtMS5_Code_edit" Font-Size="11"
                                                                ErrorMessage="กรุณากรอก Code"
                                                                ValidationExpression="กรุณากรอก Code"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtMS5_Code_edit" Width="160" />

                                                            <asp:RegularExpressionValidator ID="Re_txtMS5_Code_edit1" runat="server"
                                                                ValidationGroup="Save_edit" Display="None"
                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                ControlToValidate="txtMS5_Code_edit"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtMS5_Code_edit1" Width="160" />

                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label3" runat="server" Text="Name" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                            <div class="col-sm-8">
                                                                <asp:TextBox ID="txt_MS5_Name_edit" runat="server" CssClass="form-control" Text='<%# Eval("MS5_Name")%>' />

                                                            </div>

                                                            <asp:RequiredFieldValidator ID="Re_txt_MS5_Name_edit" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                ControlToValidate="txt_MS5_Name_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาใส่สถานะรายการ"
                                                                ValidationExpression="กรุณาใส่สถานะรายการ"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_MS5_Name_edit" Width="160" />
                                                            <asp:RegularExpressionValidator ID="Re_txt_MS5_Name_edit1" runat="server"
                                                                ValidationGroup="Save_edit" Display="None"
                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                ControlToValidate="txt_MS5_Name_edit"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_MS5_Name_edit1" Width="160" />

                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                            <div class="col-sm-8">
                                                                <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("MS4Status") %>'>
                                                                    <asp:ListItem Value="1" Text="Online" />
                                                                    <asp:ListItem Value="0" Text="Offline" />
                                                                </asp:DropDownList>

                                                            </div>
                                                        </div>


                                                        <div class="form-group">
                                                            <div class="col-sm-2 col-sm-offset-10">
                                                                <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update"><i class="fa fa-check"></i></asp:LinkButton>
                                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>


                                            </EditItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="System" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_SysNameTH" runat="server" Text='<%# Eval("SysNameTH") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>

                                                <asp:Label ID="lb_MS5_Name" runat="server" Text='<%# Eval("MS5_Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Code" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_MS5_Code" runat="server" Text='<%# Eval("MS5_Code") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbMS5StatusDetail" runat="server" Text='<%# Eval("MS5StatusDetail") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                                <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("MS5IDX") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>