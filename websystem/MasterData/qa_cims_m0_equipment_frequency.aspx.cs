﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_qa_cims_m0_equipment_frequency : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    data_qa_cims _data_qa_cims = new data_qa_cims();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //master
    static string _urlCimsGetUnit = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetUnit"];

    static string _urlCimsSetM0_Frequency = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetM0_Frequency"];
    static string _urlCimsGetM0_Frequency = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetM0_Frequency"];
    static string _urlCimsDelM0_Frequency = _serviceUrl + ConfigurationManager.AppSettings["urlCimsDelM0_Frequency"];

  
    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        ////getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Select_Frequency();

            //ViewState["empIDX"] = Session["emp_idx"];
            ////Select_Equipment_result();
            


        }
    }
    #endregion Page Load

    protected void getM0Unit(DropDownList ddlName, string _unit_idx)
    {
        data_qa_cims _dataqacims = new data_qa_cims();
        _dataqacims.qa_cims_m0_unit_list = new qa_cims_m0_unit_detail[1];
        qa_cims_m0_unit_detail _m0Set = new qa_cims_m0_unit_detail();
        _dataqacims.qa_cims_m0_unit_list[0] = _m0Set;

        _dataqacims = callServicePostMasterQACIMS(_urlCimsGetUnit, _dataqacims);

        setDdlData(ddlName, _dataqacims.qa_cims_m0_unit_list, "unit_symbol_en", "unit_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกหน่วยวัด --", "0"));
        ddlName.SelectedValue = _unit_idx;
    }

    protected void Select_Frequency()
    {
        data_qa_cims data_m0_frequency = new data_qa_cims();
        qa_cims_m0_frequency_detail m0_frequency = new qa_cims_m0_frequency_detail();

        data_m0_frequency.qa_cims_m0_frequency_list = new qa_cims_m0_frequency_detail[1];

        data_m0_frequency.qa_cims_m0_frequency_list[0] = m0_frequency;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
        data_m0_frequency = callServicePostMasterQACIMS(_urlCimsGetM0_Frequency, data_m0_frequency);
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_frequency));
        setGridData(GvFrequency, data_m0_frequency.qa_cims_m0_frequency_list);

    }

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {

            case "cmdInsertFrequency":

                MvMaster.SetActiveView(ViewInsert);
                //getM0Unit((DropDownList)ViewInsert.FindControl("ddl_unit_idx"), "0");

                break;

            case "cmdSaveFrequency":
                //litdebug.Text = _emp_idx.ToString();

                data_qa_cims data_frequency = new data_qa_cims();
                qa_cims_m0_frequency_detail m0_frequency_detail = new qa_cims_m0_frequency_detail();
                data_frequency.qa_cims_m0_frequency_list = new qa_cims_m0_frequency_detail[1];

                m0_frequency_detail.frequency_count = txt_frequency_count.Text;
                m0_frequency_detail.cemp_idx = _emp_idx;
                m0_frequency_detail.frequency_status = int.Parse(ddl_frequency_status.SelectedValue);
                m0_frequency_detail.frequency_unit = int.Parse(ddl_frequency_unit.SelectedValue);

                data_frequency.qa_cims_m0_frequency_list[0] = m0_frequency_detail;
                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        

                data_frequency = callServicePostMasterQACIMS(_urlCimsSetM0_Frequency, data_frequency);

                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_resulution));
                if (data_frequency.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                }
                else
                {
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    //tex_equipment_result.Text = String.Empty;
                }



                break;

            case "cmdDelFrequency":

                int frequency_idx_del = int.Parse(cmdArg);

                data_qa_cims data_frequency_del = new data_qa_cims();
                qa_cims_m0_frequency_detail m0_frequency_detail_del = new qa_cims_m0_frequency_detail();
                data_frequency_del.qa_cims_m0_frequency_list = new qa_cims_m0_frequency_detail[1];

                m0_frequency_detail_del.equipment_frequency_idx = frequency_idx_del;
                m0_frequency_detail_del.cemp_idx = _emp_idx;

                data_frequency_del.qa_cims_m0_frequency_list[0] = m0_frequency_detail_del;
                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        

                data_frequency_del = callServicePostMasterQACIMS(_urlCimsDelM0_Frequency, data_frequency_del);

                Select_Frequency();

                break;

            case "cmdCancel":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;




        }
    }
    #endregion btnCommand

    #region bind data
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }
    #endregion bind data

    #region Gridview
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvFrequency":


                if (e.Row.RowType == DataControlRowType.DataRow)
                {


                    Label lbl_frequency_status = (Label)e.Row.FindControl("lbl_frequency_status");
                    Label lbl_frequency_statusOnline = (Label)e.Row.FindControl("lbl_frequency_statusOnline");
                    Label lbl_frequency_statusOffline = (Label)e.Row.FindControl("lbl_frequency_statusOffline");

                    ViewState["vs_frequency_status"] = lbl_frequency_status.Text;


                    if (ViewState["vs_frequency_status"].ToString() == "1")
                    {
                        lbl_frequency_statusOnline.Visible = true;
                    }
                    else if (ViewState["vs_frequency_status"].ToString() == "0")
                    {
                        lbl_frequency_statusOffline.Visible = true;
                    }
                    else
                    {

                    }

                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";


                    //Label _lbl_frequency_unit_edit = (Label)e.Row.FindControl("lbl_frequency_unit_edit");
                    //getM0CaltypeList((DropDownList)e.Row.Cells[0].FindControl("DD_caltype"), txt_caltype.Text);

                    //getM0Unit((DropDownList)e.Row.FindControl("ddlunit_idx_edit"), _lbl_unit_idx_edit.Text);


                    //btn_addplace.Visible = true;
                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);

                    //MultiView1.Visible = true;
                    //Gv_select_unit.Visible = false;
                    //btn_addequipment_result.Visible = true;
                    //Fv_Insert_Result.Visible = false;

                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvFrequency":
                GvFrequency.EditIndex = e.NewEditIndex;
                Select_Frequency();

                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvFrequency":

                var txt_equipment_frequency_idx_edit = (TextBox)GvFrequency.Rows[e.RowIndex].FindControl("txt_equipment_frequency_idx_edit");
                var txt_frequency_count_edit = (TextBox)GvFrequency.Rows[e.RowIndex].FindControl("txt_frequency_count_edit");
                var ddlunit_frequency_edit = (DropDownList)GvFrequency.Rows[e.RowIndex].FindControl("ddlunit_frequency_edit");
                var ddl_frequency_status_edit = (DropDownList)GvFrequency.Rows[e.RowIndex].FindControl("ddl_frequency_status_edit");


                GvFrequency.EditIndex = -1;

                data_qa_cims data_frequency_edit = new data_qa_cims();
                data_frequency_edit.qa_cims_m0_frequency_list = new qa_cims_m0_frequency_detail[1];
                qa_cims_m0_frequency_detail m0_frequency_edit = new qa_cims_m0_frequency_detail();

                m0_frequency_edit.equipment_frequency_idx = int.Parse(txt_equipment_frequency_idx_edit.Text);
                m0_frequency_edit.frequency_count = txt_frequency_count_edit.Text;
                m0_frequency_edit.frequency_unit = int.Parse(ddlunit_frequency_edit.SelectedValue);
                m0_frequency_edit.frequency_status = int.Parse(ddl_frequency_status_edit.SelectedValue);

                data_frequency_edit.qa_cims_m0_frequency_list[0] = m0_frequency_edit;

                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_resolution_edit));

                data_frequency_edit = callServicePostMasterQACIMS(_urlCimsSetM0_Frequency, data_frequency_edit);

                if (data_frequency_edit.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                    //Select_Frequency();
                }
                else
                {
                    Select_Frequency();
                }
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvFrequency":
                GvFrequency.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvFrequency":
                GvFrequency.PageIndex = e.NewPageIndex;
                GvFrequency.DataBind();
                Select_Frequency();
                break;
        }
    }

    #endregion Gridview

    #region event dropdownlist

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion dropdownlist

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }
    #endregion callService
}