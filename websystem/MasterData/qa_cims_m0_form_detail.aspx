﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_cims_m0_form_detail.aspx.cs" Inherits="websystem_MasterData_qa_cims_m0_form_detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <div class="col-sm-12">
        <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    </div>
    <!--multiview-->
    <asp:MultiView ID="mvMaster" runat="server">
        <asp:View ID="pageGenaral" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnAddFormDetail" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มชื่อหัวข้อ" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdAddFromDetail" CommandArgument="0" title="เพิ่มชื่อหัวข้อ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มชื่อหัวข้อ</asp:LinkButton>

            </div>
            <!--formview insert-->
            <asp:FormView ID="fvformInsert" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i>&nbsp;เพิ่มชื่อหัวข้อ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:RequiredFieldValidator ID="ReqtbFormName" runat="server"
                                        ControlToValidate="tbFormName" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกชื่อหัวข้อ" ValidationGroup="saveFormName" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValtbFormName" runat="Server" PopupPosition="TopLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbFormName" Width="180" />

                                    <label class="col-sm-2 control-label">ชื่อหัวข้อ</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbFormName" runat="server" CssClass="form-control" placeholder="กรอกชื่อหัวข้อ ..." Enabled="true" />
                                    </div>


                                    <div class="col-sm-1"></div>
                                </div>
                                <asp:RequiredFieldValidator ID="ReqtbFormDetail" runat="server"
                                    ControlToValidate="tbFormDetail" Display="None" SetFocusOnError="true"
                                    ErrorMessage="*กรุณากรอกรายละเอียดชื่อหัวข้อ" ValidationGroup="saveFormName" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValtbFormDetail" runat="Server" PopupPosition="BottomRight"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbFormDetail" Width="180" />
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">รายละเอียดชื่อหัวข้อ</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbFormDetail" runat="server" CssClass="form-control" Rows="3" TextMode="MultiLine"
                                            placeholder="กรอกรายละเอียดชื่อหัวข้อ ..." Enabled="true" />
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อชุดข้อมูล</label>
                                    <div class="col-sm-4">
                                        <asp:RequiredFieldValidator ID="ReddlSetNameCreate" runat="server" InitialValue="0"
                                            ControlToValidate="ddlSetNameCreate" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือกชื่อชุดข้อมูล" ValidationGroup="saveFormName" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValddlSetNameCreate" runat="Server" PopupPosition="BottomRight"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlSetNameCreate" Width="180" />
                                        <asp:DropDownList ID="ddlSetNameCreate" runat="server" CssClass="form-control" Enabled="true">
                                        </asp:DropDownList>
                                    </div>

                                    <label class="col-sm-1 control-label">Option</label>
                                    <div class="col-sm-4">
                                        <asp:RequiredFieldValidator ID="ReddlOptionCreate" runat="server" InitialValue="0"
                                            ControlToValidate="ddlOptionCreate" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือก Option" ValidationGroup="saveFormName" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValddlOptionCreate" runat="Server" PopupPosition="BottomRight"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlOptionCreate" Width="180" />
                                        <asp:DropDownList ID="ddlOptionCreate" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">สถานะ</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="ddlFormStatus" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" ValidationGroup="saveFormName" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvMaster"
                runat="server"
                DataKeyNames="form_detail_idx"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="10"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="lbFormidx" runat="server" CssClass=" font_text text_center" Visible="false" Text='<%# Eval("form_detail_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="_updateFormidx" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("form_detail_idx")%>'></asp:TextBox>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อหัวข้อ" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdateFormName" runat="server" CssClass="form-control" Text='<%# Eval("form_detail_name")%>' />
                                            <asp:RequiredFieldValidator ID="RetbupdateFormName" runat="server"
                                                ControlToValidate="tbupdateFormName" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อหัวข้อ" ValidationGroup="updateFormName" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValtbupdateFormName" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetbupdateFormName" Width="180" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="tbUpdateSetNameIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("set_idx")%>'></asp:TextBox>
                                        <asp:Label ID="lblUpdateSetname" CssClass="col-sm-3 control-label" runat="server" Text="ชุดข้อมูล" />
                                        <asp:RequiredFieldValidator ID="ReqddlUpdateSetName" runat="server"
                                            ControlToValidate="ddlUpdateSetName" Display="None" InitialValue="0" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือกชุดข้อมูล" ValidationGroup="updateFormName" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValddlUpdateSetName" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqddlUpdateSetName" Width="180" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlUpdateSetName"
                                                CssClass="form-control fa-align-left" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="tbUpdateOptionIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("option_idx")%>'></asp:TextBox>
                                        <asp:Label ID="lblUpdateOption" CssClass="col-sm-3 control-label" runat="server" Text="Option" />
                                        <asp:RequiredFieldValidator ID="ReddlUpdateOption" runat="server"
                                            ControlToValidate="ddlUpdateOption" Display="None" InitialValue="0" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือก Option" ValidationGroup="updateFormName" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValddlUpdateOption" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlUpdateOption" Width="180" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlUpdateOption"
                                                CssClass="form-control fa-align-left" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="รายละเอียดหัวข้อ" />
                                        <div class="col-sm-7">
                                            <asp:RequiredFieldValidator ID="RetbupdateFormDetail" runat="server"
                                                ControlToValidate="tbupdateFormDetail" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกรายละเอียดหัวข้อ" ValidationGroup="updateFormName" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValtbupdateFormDetail" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetbupdateFormDetail" Width="200" />
                                            <asp:TextBox ID="tbupdateFormDetail" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("form_detail")%>' />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_setname_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlupdateFormStatus" Text='<%# Eval("form_detail_status") %>'
                                                CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">online</asp:ListItem>
                                                <asp:ListItem Value="0">offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" ValidationGroup="updateFormName"
                                                Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อหัวข้อ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblFormName" runat="server" Text='<%# Eval("form_detail_name")%>'></asp:Label>
                                <%--<asp:TextBox ID="tbFormIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("form_detail_root_idx")%>' />--%>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียดหัวข้อ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbFormDetail" runat="server" Text='<%# Eval("form_detail")%>'></asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อชุดข้อมูล" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <%--<asp:Label ID="lbl_SetIdx" runat="server" Visible="true" Text='<%# Eval("set_idx")%>'></asp:Label>--%>
                                <asp:Label ID="lbl_SetName" runat="server" Text='<%# Eval("set_name")%>'></asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Option" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <%--<asp:Label ID="lbl_OptionIdx" runat="server" Visible="true" Text='<%# Eval("option_idx")%>'></asp:Label>--%>
                                <asp:Label ID="lbl_Option" runat="server" Text='<%# Eval("option_name")%>'></asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbstatusForm" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("form_detail_status") %>'></asp:Label>
                                <asp:Label ID="status_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="status_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>

                            <%--<asp:LinkButton ID="btnviewset" CssClass="text-read" runat="server" CommandName="cmdView"
                                data-toggle="tooltip" OnCommand="btnCommand" Visible="false"
                                CommandArgument='<%#Eval("form_detail_idx") + "," + "0" %>' title="view">
                                        <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i></asp:LinkButton>--%>
                            <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelettypeNews" CssClass="text-trash" runat="server" CommandName="cmdDelete"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("form_detail_idx") +"," + "0" %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
        <%--<asp:View ID="pageCreateRoot" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnBack" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
                <asp:LinkButton ID="btnAddRoot" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มหัวข้อย่อย" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdAddFromDetail" CommandArgument="1" title=""><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มหัวข้อย่อย</asp:LinkButton>
            </div>
            <!--formview insert Level 2-->
            <asp:FormView ID="fvformInsertRoot" DefaultMode="Insert" runat="server" Width="100%">
                <InsertItemTemplate>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i>&nbsp;เพิ่มชื่อหัวข้อ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ชื่อหัวข้อ</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbFormNameLevel1" runat="server" CssClass="form-control" placeholder="กรอกชื่อหัวข้อ ..." Enabled="false" />
                                    </div>

                                    <label class="col-sm-1 control-label">สถานะ</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlFormStatusLevel2" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อหัวข้อย่อย</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbFormNameLevel2" runat="server" CssClass="form-control" placeholder="กรอกชื่อหัวข้อย่อย ..." Enabled="true" />
                                    </div>
                                    <asp:RequiredFieldValidator ID="ReqtbFormNameLevel2" runat="server"
                                        ControlToValidate="tbFormNameLevel2" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกรายละเอียดชื่อหัวข้อ" ValidationGroup="saveFormNameLevel2" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValtbFormNameLevel2" runat="Server" PopupPosition="BottomRight"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbFormNameLevel2" Width="180" />
                                    <div class="col-sm-1"></div>
                                </div>

                                <asp:RequiredFieldValidator ID="ReqtbFormDetailLevel2" runat="server"
                                    ControlToValidate="tbFormDetailLevel2" Display="None" SetFocusOnError="true"
                                    ErrorMessage="*กรุณากรอกรายละเอียดชื่อหัวข้อ" ValidationGroup="saveFormNameLevel2" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValtbFormDetailLevel2" runat="Server" PopupPosition="BottomRight"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbFormDetailLevel2" Width="180" />

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อชุดข้อมูล</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlSetName" runat="server" CssClass="form-control" Enabled="true">
                                        </asp:DropDownList>
                                    </div>

                                    <label class="col-sm-1 control-label">Option</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlOption" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">รายละเอียดชื่อหัวข้อย่อย</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbFormDetailLevel2" runat="server" CssClass="form-control" Rows="3" TextMode="MultiLine"
                                            placeholder="กรอกรายละเอียดชื่อหัวข้อย่อย ..." Enabled="true" />
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" ValidationGroup="saveFormNameLevel2" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="1"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="1"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>
            <asp:GridView ID="gvMasterRoot"
                runat="server"
                DataKeyNames="form_detail_idx"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="5"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="lbFormidx" runat="server" CssClass=" font_text text_center" Visible="false" Text='<%# Eval("form_detail_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="_updateFormRootidx" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("form_detail_idx")%>'></asp:TextBox>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อหัวข้อ" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdateFormRootName" runat="server" CssClass="form-control" Text='<%# Eval("form_detail_name")%>' />

                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="รายละเอียดหัวข้อ" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdateFormRootDetail" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("form_detail")%>' />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="tbSetNameIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("set_idx")%>'></asp:TextBox>
                                        <asp:Label ID="lblSetnameUpdate" CssClass="col-sm-3 control-label" runat="server" Text="ชุดข้อมูล" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlSetNameUpdate"
                                                CssClass="form-control fa-align-left" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="tbOptionIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("option_idx")%>'></asp:TextBox>
                                        <asp:Label ID="lblOptionUpdate" CssClass="col-sm-3 control-label" runat="server" Text="Option" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlOptionUpdate"
                                                CssClass="form-control fa-align-left" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_setname_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlupdateFormRootStatus" Text='<%# Eval("form_datail_status") %>'
                                                CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">online</asp:ListItem>
                                                <asp:ListItem Value="0">offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อหัวข้อ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblFormNameLevel2" runat="server" Text='<%# Eval("form_detail_name")%>'></asp:Label>
                                <asp:TextBox ID="tbFormIDXLevel2" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("form_detail_root_idx")%>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียดหัวข้อ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbFormDetailLevel2" runat="server" Text='<%# Eval("form_detail")%>'></asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อชุดข้อมูล" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblSetName" runat="server" Text='<%# Eval("set_name")%>'></asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Option" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblOption" runat="server" Text='<%# Eval("option_name")%>'></asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbstatusFormLevel2" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("form_datail_status") %>'></asp:Label>
                                <asp:Label ID="status_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="status_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>

                            <asp:LinkButton ID="btnviewset" CssClass="text-read small" runat="server" CommandName="cmdView"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("form_detail_idx") + "," + "1" %>' title="view">
                                        <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i></asp:LinkButton>
                            <asp:LinkButton ID="Edit" CssClass="text-edit small" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelettypeNews" CssClass="text-trash small" runat="server" CommandName="cmdDelete"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("form_detail_idx") +"," + "1" %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>

        <asp:View ID="pageRootSubSet" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="lnkbtnBack" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="1" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
                <asp:LinkButton ID="lnkbtnRootSet" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มหัวข้อย่อย" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdAddFromDetail" CommandArgument="2" title=""><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มหัวข้อย่อย</asp:LinkButton>
            </div>
            <!--formview insert Level 3-->
            <asp:FormView ID="fvformInsertSubFormDetail" runat="server" DefaultMode="Insert" Width="100%">
                <InsertItemTemplate>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i>&nbsp;เพิ่มชื่อหัวข้อย่อย</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ชื่อหัวข้อ</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbFormNameRootLevel2" runat="server" CssClass="form-control" placeholder="กรอกชื่อหัวข้อ ..." Enabled="false" />
                                    </div>

                                    <label class="col-sm-1 control-label">สถานะ</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlFormStatusLevel3" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อหัวข้อย่อย</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbFormNameLevel3" runat="server" CssClass="form-control" placeholder="กรอกชื่อหัวข้อย่อย ..." Enabled="true" />
                                    </div>
                                    <asp:RequiredFieldValidator ID="ReqtbFormNameLevel3" runat="server"
                                        ControlToValidate="tbFormNameLevel3" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกรายละเอียดชื่อหัวข้อ" ValidationGroup="saveFormNameLevel3" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValtbFormNameLevel3" runat="Server" PopupPosition="BottomRight"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbFormNameLevel3" Width="180" />
                                    <div class="col-sm-1"></div>
                                </div>
                                <asp:RequiredFieldValidator ID="ReqtbFormDetailLevel3" runat="server"
                                    ControlToValidate="tbFormDetailLevel3" Display="None" SetFocusOnError="true"
                                    ErrorMessage="*กรุณากรอกรายละเอียดชื่อหัวข้อ" ValidationGroup="saveFormNameLevel3" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValtbFormDetailLevel2" runat="Server" PopupPosition="BottomRight"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbFormDetailLevel3" Width="180" />
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อชุดข้อมูล</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlSubSetName" runat="server" CssClass="form-control" Enabled="true">
                                        </asp:DropDownList>
                                    </div>

                                    <label class="col-sm-1 control-label">Option</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlSubOption" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">รายละเอียดชื่อหัวข้อย่อย</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbFormDetailLevel3" runat="server" CssClass="form-control" Rows="3" TextMode="MultiLine"
                                            placeholder="กรอกรายละเอียดชื่อหัวข้อย่อย ..." Enabled="true" />
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" ValidationGroup="saveFormNameLevel3" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="2"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="2"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvMasterSubForm"
                runat="server"
                DataKeyNames="form_detail_idx"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="5"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="lbFormidx" runat="server" CssClass=" font_text text_center" Visible="false" Text='<%# Eval("form_detail_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="_updateFormSubidx" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("form_detail_idx")%>'></asp:TextBox>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อหัวข้อ" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdateFormSubName" runat="server" CssClass="form-control" Text='<%# Eval("form_detail_name")%>' />

                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="รายละเอียดหัวข้อ" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdateFormSubDetail" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("form_detail")%>' />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_setname_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlupdateFormSubStatus" Text='<%# Eval("form_datail_status") %>'
                                                CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">online</asp:ListItem>
                                                <asp:ListItem Value="0">offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อหัวข้อ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblFormNameLevel2" runat="server" Text='<%# Eval("form_detail_name")%>'></asp:Label>
                                <asp:TextBox ID="tbFormIDXLevel2" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("form_detail_root_idx")%>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียดหัวข้อ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbFormDetailLevel2" runat="server" Text='<%# Eval("form_detail")%>'></asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbstatusFormSub" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("form_datail_status") %>'></asp:Label>
                                <asp:Label ID="status_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="status_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:LinkButton ID="Edit" CssClass="text-edit small" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelettypeNews" CssClass="text-trash small" runat="server" CommandName="cmdDelete"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("form_detail_idx") +"," + "2" %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>--%>
    </asp:MultiView>


</asp:Content>

