﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="enp_planning.aspx.cs" Inherits="websystem_EN_enp_planning" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadImages").MultiFile();
            }
        })
    </script>

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>
    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <!--*** START Menu ***-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left" runat="server">
                    <li id="_divMenuLiToViewIndex" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivIndex" runat="server"
                            CommandName="_divMenuBtnToDivIndex"
                            OnCommand="btnCommand" Text="กำหนดการ PM รายปี" />
                    </li>

                    <li id="_divMenuLiToViewManage" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivMange" runat="server"
                            CommandName="_divMenuBtnToDivMange"
                            OnCommand="btnCommand" Text="จัดการแพลน" />
                    </li>
                    <li id="_divMenuLiToViewAdd" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivAdd" runat="server"
                            CommandName="_divMenuBtnToDivAdd"
                            OnCommand="btnCommand" Text="เพิ่มรายการ" />
                    </li>

                    <li id="_divMenuLiToViewAddResult" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivAddResult" runat="server"
                            CommandName="_divMenuBtnToDivAddResult"
                            OnCommand="btnCommand" Text="กรอกผล PM" />
                    </li>

                    <li id="_divMenuLiToViewApprove" visible="false" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivApprove" runat="server"
                            CommandName="_divMenuBtnToDivApprove"
                            OnCommand="btnCommand" Text="รายการรออนุมัติ" />
                    </li>

                    <li id="_divMenuLiToDocument" runat="server">
                        <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                            NavigateUrl="https://docs.google.com/document/d/1HLd4Dn65bT1-OHSWpx5x9GSW8s4RdsU2v5yqfRs4c2o/edit?usp=sharing"
                            Target="_blank" Text="คู่มือการใช้งาน" />
                    </li>
                </ul>
            </div>
        </nav>
    </div>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="col-lg-12">
                        <asp:Panel ID="pnlsearch_plan_pm" runat="server">
                            <div class="panel panel-primary m-t-10">
                                <div class="panel-heading f-bold">ค้นหา</div>
                                <div class="panel-body">

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>ปี</label>
                                            <asp:DropDownList ID="ddlYearSearch_plan_pm" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>โรงงาน</label>
                                            <asp:DropDownList ID="ddllocate_search" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0" Text="เลือก..."></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>


                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>ประเภทเครื่องจักร</label>
                                            <asp:DropDownList ID="ddltypemachine_search" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" class="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="เลือก..."></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>รหัสกลุ่มเครื่องจักร</label>
                                            <asp:DropDownList ID="ddltypecode_search" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" class="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="เลือก..."></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>


                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>อาคาร</label>
                                            <asp:DropDownList ID="ddlbuilding_search" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0" Text="เลือก..."></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <label>
                                            &nbsp;
                                        </label>
                                        <div class="form-group">
                                            <asp:LinkButton ID="btnFilter_plan_pm"
                                                runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                                OnCommand="btnCommand" CommandName="btnFilter_plan_pm" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </asp:Panel>

                        <div class="col-md-11 col-md-offset-1">
                            <asp:Panel ID="panel2" runat="server" CssClass="text-right m-b-10 hidden-print">

                                <button
                                    onclick="tableToExcel('Tableprint_plan', 'แบบฟอร์มใบบันทึกแผนการทำ PM เครื่องจักร')"
                                    class="btn btn-primary">
                                    <i class='fa fa-file-excel'></i>
                                    Export Excel
                                </button>
                                <asp:LinkButton ID="btnprint_plan" CssClass="btn btn-primary" runat="server"
                                    Text="<i class='fa fa-print'></i> พิมพ์" OnClientClick="return printDiv('print_plan');" />
                            </asp:Panel>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div class="col-lg-12">
                <asp:Panel ID="pnl_alert" runat="server">
                    <div class="alert alert-danger col-md-12" style="text-align: center" id="dv_alert" runat="server">
                        ไม่พบข้อมูล
                    </div>
                </asp:Panel>
            </div>


            <asp:Panel ID="pnl_print_plan" runat="server">
                <div class="panel-body" style="height: 600px; overflow-y: scroll; overflow-x: scroll;">
                    <div id="print_plan" class="col-lg-12">
                        <table id="Tableprint_plan"
                            class="table table-bordered word-wrap"
                            style="font-family: Trebuchet MS, Arial, Helvetica, sans-serif; border-collapse: collapse;">
                            <thead style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px;">

                                <tr>
                                    <th colspan="51" style="border-style: hidden; color: black; font-size: 14px; font-family: serif; background-color: white; width: 45%; padding: 0px 0px;">
                                        <center>
                                     <asp:Literal ID="litTitle1" runat="server"></asp:Literal>
                                    </center>
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="51" style="border-style: hidden; color: black; font-size: 14px; font-family: serif; background-color: white; width: 45%; padding: 0px 0px;">
                                        <center>
                                    
                                    <asp:Literal ID="litTitle2" runat="server"></asp:Literal>
                                </center>
                                    </th>
                                </tr>
                                <tr>
                                    <th colspan="51" style="border-style: hidden; color: black; font-size: 14px; font-family: serif; background-color: white; width: 45%; padding: 0px 0px;">
                                        <center>
                                    <asp:Literal ID="litTitle3" runat="server"></asp:Literal>
                                </center>
                                        <br />
                                    </th>
                                </tr>

                                <tr></tr>
                                <tr style="width: 100%;">
                                    <th rowspan="4" style="background-color: #4CAF50; border: 1px solid #ddd; width: 10px; padding: 0px 0px;">No.
                                    <br />
                                        <br />
                                        <br />
                                        <br />
                                    </th>
                                    <th rowspan="4" style="background-color: #4CAF50; border: 1px solid #ddd; padding: 0px 0px; width: 100px; position: center;">
                                        <%-- <center>--%>
                                 Machine&nbsp;Name&nbsp;/&nbsp;Equipment
                                    <br />
                                        <br />
                                        <br />
                                        <br />
                                        <%-- </center>--%>
                                    </th>
                                    <th style="background-color: #4CAF50; border: 1px solid #ddd; width: 200px; padding: 0px 0px;">
                                        <asp:Panel runat="server" Width="100px"></asp:Panel>
                                        <center>
                                         
                                        สัปดาห์ที่
                                    </center>

                                    </th>
                                    <% for (int iWeek = 1; iWeek <= 48; iWeek++)
                                        {


                                    %>
                                    <th style="background-color: #4CAF50; border: 1px solid #ddd; width: 20px; padding: 0px 0px;">
                                        <center>
                                        <%= iWeek.ToString()  %>
                                    </center>
                                    </th>
                                    <%
                                        } %>
                                </tr>
                                <tr>
                                    <th style="background-color: #4CAF50; border: 1px solid #ddd;">
                                        <center>
                                       เดือน
                                    </center>
                                    </th>
                                    <% for (int iWeek = 1; iWeek <= 12; iWeek++)
                                        {


                                    %>
                                    <th colspan="4" style="background-color: #4CAF50; border: 1px solid #ddd;">
                                        <center>
                                        <%= zMonth(iWeek)  %>
                                    </center>
                                    </th>
                                    <%
                                        } %>
                                </tr>
                                <tr>
                                    <th style="background-color: #4CAF50; border: 1px solid #ddd;">
                                        <center>
                                       สัปดาห์
                                    </center>
                                    </th>
                                    <%  int item = 0;
                                        for (int iWeek = 1; iWeek <= 48; iWeek++)
                                        {

                                            if (item == 4)
                                            {
                                                item = 0;
                                            }
                                            item++;
                                    %>
                                    <th style="background-color: #4CAF50; border: 1px solid #ddd;">
                                        <center>
                                        <%= item.ToString()  %>
                                    </center>
                                    </th>
                                    <%

                                        } %>
                                </tr>
                                <tr>
                                    <th style="background-color: #4CAF50; border: 1px solid #ddd;">
                                        <center>
                                       CODE
                                    </center>
                                    </th>
                                    <%  for (int iWeek = 1; iWeek <= 48; iWeek++)
                                        {
                                    %>
                                    <th style="background-color: #4CAF50; border: 1px solid #ddd;">
                                        <center>
                                       
                                    </center>
                                    </th>
                                    <%

                                        } %>
                                </tr>
                            </thead>
                            <tbody>
                                <%= getHtml_RptPlanPM() %>
                                <tr>
                                </tr>
                                <tr>
                                    <td style="border-style: hidden;">
                                        <br />
                                    </td>
                                    <td colspan="6" style="border-style: hidden; font-size: 13px; font-family: serif; padding: 0px 0px;">
                                        <br />
                                        <center> 
                                        <asp:Literal ID="litapp_t1_1" runat="server"></asp:Literal>
                                        <br />
                                        <asp:Literal ID="litapp_t1_2" runat="server"></asp:Literal>
                                        <br />
                                        <asp:Literal ID="litapp_t1_3" runat="server"></asp:Literal>
                                    </center>
                                    </td>
                                    <td colspan="14" style="border-style: hidden; font-size: 13px; font-family: serif; padding: 0px 0px;">
                                        <br />
                                        <center> 
                                        <asp:Literal ID="litapp_t2_1" runat="server"></asp:Literal>
                                        <br />
                                        <asp:Literal ID="litapp_t2_2" runat="server"></asp:Literal>
                                        <br />
                                        <asp:Literal ID="litapp_t2_3" runat="server"></asp:Literal>
                                    </center>
                                    </td>
                                    <td colspan="14" style="border-style: hidden; font-size: 13px; font-family: serif; padding: 0px 0px;">
                                        <br />
                                        <center> 
                                        <asp:Literal ID="litapp_t3_1" runat="server"></asp:Literal>
                                        <br />
                                        <asp:Literal ID="litapp_t3_2" runat="server"></asp:Literal>
                                        <br />
                                        <asp:Literal ID="litapp_t3_3" runat="server"></asp:Literal>
                                    </center>
                                    </td>
                                    <td colspan="16" style="border-style: hidden; font-size: 13px; font-family: serif; padding: 0px 0px;">
                                        <br />
                                        <center> 
                                        <asp:Literal ID="litapp_t4_1" runat="server"></asp:Literal>
                                        <br />
                                        <asp:Literal ID="litapp_t4_2" runat="server"></asp:Literal>
                                        <br />
                                        <asp:Literal ID="litapp_t4_3" runat="server"></asp:Literal>
                                    </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="51" style="border-style: hidden; font-size: 13px; font-family: serif; text-align: right; padding: 0px 0px;">

                                        <asp:Literal ID="litiso" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </tbody>

                        </table>

                    </div>
                </div>

            </asp:Panel>
        </asp:View>

        <asp:View ID="ViewManage" runat="server">
            <div class="col-lg-12">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; Search Plan</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div id="SETBoxAllSearch" runat="server">
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label16" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ :" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddllocatemanage_search" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidacctor5" ValidationGroup="btnsearch" runat="server" Display="None"
                                                ControlToValidate="ddllocatemanage_search" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกสถานที่"
                                                ValidationExpression="กรุณาเลือกสถานที่" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidadtorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidacctor5" Width="160" />

                                        </div>
                                        <asp:Label ID="Label13" class="col-sm-2 control-label" runat="server" Text="อาคาร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlbuildingmanage_search" ValidationGroup="Save" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกอาคาร...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label14" class="col-sm-2 control-label" runat="server" Text="ห้อง : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlroommanage_search" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกห้อง...</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                        <asp:Label ID="Label67" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทเครื่องจักร :" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypemanage_search" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกประเภทเครื่องจักร...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>


                                    <div class="form-group">

                                        <asp:Label ID="Label12" CssClass="col-sm-2 control-label" runat="server" Text="รหัสกลุ่มเครื่องจักร :" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypecodemanage_search" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="กรุณาเลือกรหัสกลุ่มเครื่องจักร..." Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <asp:Label ID="Label3" class="col-sm-2 control-label" runat="server" Text="รหัสกลุ่ม : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlgroupmachinemanage_search" ValidationGroup="Save" class="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกรหัสกลุ่ม..."></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label31" CssClass="col-sm-2 control-label" runat="server" Text="ความถี่ในการดำเนินการ :" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlplanmange_search" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="กรุณาเลือกความถี่ในการดำเนินการ..." Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <asp:Label ID="Label32" class="col-sm-2 control-label" runat="server" Text="ปี : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlyearmanage_plan" ValidationGroup="Save" class="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกปี..."></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <div class="col-sm-5 col-sm-offset-2">
                                            <asp:LinkButton ID="LinkButton3" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch" ValidationGroup="btnsearch" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton5" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Refresh" runat="server" CommandName="_divMenuBtnToDivMange" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="col-lg-12">

                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="GvManagePlan"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    DataKeyNames="u0idx"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="success"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    PageSize="10"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center;">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>

                                    <Columns>
                                        <asp:TemplateField HeaderText="ปี" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>

                                                    <asp:Literal ID="ltyear" runat="server" Text='<%# Eval("yearplan") %>' /></small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รายละเอียดสถานที่" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" ItemStyle-CssClass="text-left">
                                            <ItemTemplate>
                                                <%-- <small>
                                                    <asp:Literal ID="ltlocname" runat="server" Text='<%# Eval("LocName") %>' /></small>--%>

                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label20" runat="server">สถานที่: </asp:Label></strong>
                                                    <asp:Literal ID="ltlocname" runat="server" Text='<%# Eval("LocName") %>' />
                                                    </p>

                                                            <strong>
                                                                <asp:Label ID="Label21" runat="server">อาคาร: </asp:Label></strong>
                                                    <asp:Literal ID="ltbuilding" runat="server" Text='<%# Eval("BuildingName") %>' />
                                                    </p>
                                                            <strong>
                                                                <asp:Label ID="Label22" runat="server">ห้อง: </asp:Label></strong>
                                                    <asp:Literal ID="ltroom" runat="server" Text='<%# Eval("RoomName") %>' />

                                                </small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ประเภทเครื่องจักร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="8%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="lttype" runat="server" Text='<%# Eval("TypeNameTH") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="รหัสกลุ่มเครื่องจักร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" ItemStyle-CssClass="text-left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="lttypecode" runat="server" Text='<%# Eval("NameTypecode") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="รหัสย่อย" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" ItemStyle-CssClass="text-left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="ltgroupcode" runat="server" Text='<%# Eval("NameGroupCode") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ความถี่ในการดำเนินการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" ItemStyle-CssClass="text-left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="Literal1" Visible="false" runat="server" Text='<%# Eval("u0idx") %>' /></small>
                                                <asp:Literal ID="lisystem" runat="server" Text='<%# Eval("time_desc") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="เครื่องจักร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" ItemStyle-CssClass="text-left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="liMCCode" runat="server" Text='<%# Eval("MCCode") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="8%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbViewEN" runat="server" CssClass="btn btn-warning btn-xs" Text="" OnCommand="btnCommand" CommandName="ViewEditPlan" title="ViewDetail" CommandArgument='<%# Eval("u0idx")%>'><i class="fa fa-edit" ></i> </asp:LinkButton>
                                                <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-xs" runat="server" CommandName="CmdDeletePlan" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("u0idx") %>'><i class="fa fa-trash"></i></asp:LinkButton>

                                                </p>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>

            </div>
        </asp:View>

        <asp:View ID="ViewEditPlan" runat="server">
            <div class="col-lg-12">
                <div class="form-horizontal" role="form">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                        </div>

                        <div class="panel-body">


                            <asp:FormView ID="FvDetailEmpPan" DefaultMode="ReadOnly" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <ItemTemplate>
                                    <%-- <div class="form-horizontal" role="form">--%>

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสผู้ทำรายการ : " />
                                            <div class="col-xs-3">
                                                <asp:Label ID="LbEmpCode" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("EmpCode") %>' />

                                            </div>

                                            <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                            <div class="col-xs-4">
                                                <asp:Label ID="LbEmpName" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("FullNameTH") %>' />
                                            </div>
                                        </div>
                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัท : " />
                                            <div class="col-xs-3">
                                                <asp:Label ID="LbOrgNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("OrgNameTH")%>' />
                                            </div>

                                            <asp:Label ID="Label34" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ฝ่าย : " />
                                            <div class="col-xs-4">
                                                <asp:Label ID="LbDeptNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("DeptNameTH")%>' />
                                            </div>
                                        </div>
                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Labedl1" class="col-xs-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                            <div class="col-xs-3">
                                                <asp:Label ID="Label23" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("SecNameTH") %>' />

                                            </div>

                                            <asp:Label ID="Label8" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ตำแหน่ง : " />
                                            <div class="col-xs-4">
                                                <asp:Label ID="LbPosNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("PosNameTH")%>' />
                                            </div>
                                        </div>
                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">
                                        <div class="col-md-12">

                                            <asp:Label ID="Label5" class="col-xs-2 control-labelnotop text_right" runat="server" Text="เบอร์ติดต่อ : " />
                                            <div class="col-xs-3">
                                                <asp:Label ID="Label25" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("MobileNo") %>' />
                                            </div>

                                            <asp:Label ID="Label6" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="E-mail : " />
                                            <div class="col-xs-4">
                                                <asp:Label ID="lblemail" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("Email") %>' />

                                            </div>
                                        </div>
                                    </div>

                                </ItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>

                </div>

            </div>

            <div class="col-lg-12">
                <div class="form-horizontal" role="form">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-calendar"></i><strong>&nbsp; กำหนดการ PM</strong></h3>
                        </div>

                        <div class="panel-body">

                            <asp:FormView ID="FvDetailEditPlan" DefaultMode="ReadOnly" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">

                                <ItemTemplate>

                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label39" class="col-xs-2 control-labelnotop text_right" runat="server" Text="สถานที่ : " />
                                            <div class="col-xs-3">
                                                <asp:Label ID="Label40" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("LocName") %>' />

                                            </div>

                                            <asp:Label ID="Label43" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="อาคาร : " />
                                            <div class="col-xs-4">
                                                <asp:Label ID="Label44" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("BuildingName")%>' />
                                            </div>
                                        </div>
                                    </div>
                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label45" class="col-xs-2 control-labelnotop text_right" runat="server" Text="ห้อง : " />
                                            <div class="col-xs-3">
                                                <asp:Label ID="Label46" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("RoomName") %>' />

                                            </div>


                                        </div>
                                    </div>



                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Labedl1" class="col-xs-2 control-labelnotop text_right" runat="server" Text="ปีที่ดำเนินการ : " />
                                            <div class="col-xs-3">
                                                <asp:Label ID="Label23" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("yearplan") %>' />

                                            </div>

                                            <asp:Label ID="Label8" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ช่วงเวลาดำเนินการ : " />
                                            <div class="col-xs-4">
                                                <asp:Label ID="LbPosNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("time_desc")%>' />
                                            </div>
                                        </div>
                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">
                                        <div class="col-md-12">

                                            <asp:Label ID="Label5" class="col-xs-2 control-labelnotop text_right" runat="server" Text="ระยะวันที่ดำเนินการ : " />
                                            <div class="col-xs-3">
                                                <asp:Label ID="Label25" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("countplan") %>' />
                                            </div>

                                            <asp:Label ID="Label6" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทเครื่องจักร	 : " />
                                            <div class="col-xs-4">
                                                <asp:Label ID="lblemail" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("TypeNameTH") %>' />

                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-md-12">

                                            <asp:Label ID="Label33" class="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสกลุ่มเครื่องจักร : " />
                                            <div class="col-xs-3">
                                                <asp:Label ID="Label36" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("NameTypecode") %>' />
                                            </div>

                                            <asp:Label ID="Label37" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสย่อย : " />
                                            <div class="col-xs-4">
                                                <asp:Label ID="Label38" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("NameGroupcode") %>' />

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label41" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสเครื่องจักร : " />
                                            <div class="col-xs-4">
                                                <asp:Label ID="Label42" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("MCCode") %>' />

                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label17" CssClass="col-sm-1 control-label" runat="server" />

                                        <div class="col-lg-8">
                                            <asp:GridView ID="gvFile" Visible="true" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                                HeaderStyle-CssClass="primary"
                                                OnRowDataBound="Master_RowDataBound"
                                                BorderStyle="None"
                                                CellSpacing="2"
                                                Font-Size="Small">

                                                <Columns>
                                                    <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                                        <ItemTemplate>
                                                            <div class="col-lg-10">
                                                                <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                            </div>
                                                            <div class="col-lg-2">
                                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>
                                                                <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-1 col-sm-offset-11">
                                            <asp:LinkButton ID="LinkButton7" CssClass="btn btn-primary btn-sm" runat="server" OnCommand="btnCommand" CommandName="btneditplan"><i class="	fa fa-edit"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </ItemTemplate>

                                <EditItemTemplate>
                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label39" class="col-xs-2 control-labelnotop text_right" runat="server" Text="สถานที่ : " />
                                            <div class="col-xs-3">
                                                <asp:TextBox ID="Label40" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("LocName") %>' />
                                                <asp:TextBox ID="txtLocIDX" Visible="false" CssClass="form-control" runat="server" Text='<%# Eval("LocIDX") %>' />

                                            </div>

                                            <asp:Label ID="Label43" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="อาคาร : " />
                                            <div class="col-xs-4">
                                                <asp:TextBox ID="Label44" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("BuildingName")%>' />
                                                <asp:TextBox ID="txtBuildingIDX" Visible="false" CssClass="form-control" runat="server" Text='<%# Eval("BuildingIDX") %>' />

                                            </div>
                                        </div>
                                    </div>
                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label45" class="col-xs-2 control-labelnotop text_right" runat="server" Text="ห้อง : " />
                                            <div class="col-xs-3">
                                                <asp:TextBox ID="Label46" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("RoomName") %>' />
                                                <asp:TextBox ID="txtRoomIDX" Visible="false" CssClass="form-control" runat="server" Text='<%# Eval("RoomIDX") %>' />


                                            </div>


                                        </div>
                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Labedl1" class="col-xs-2 control-labelnotop text_right" runat="server" Text="ปีที่ดำเนินการ : " />
                                            <div class="col-xs-3">
                                                <asp:TextBox ID="Label23" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("yearplan") %>' />

                                            </div>

                                            <asp:Label ID="Label8" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ช่วงเวลาดำเนินการ : " />
                                            <div class="col-xs-4">
                                                <asp:TextBox ID="Lbplan_edit" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("m0tidx")%>' />
                                                <asp:DropDownList ID="ddlplan_edit" class="form-control" runat="server" ValidationGroup="SaveEdit">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiddredFieldValidator7" ValidationGroup="SaveEdit" runat="server" Display="None"
                                                    ControlToValidate="ddlplan_edit" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกความถี่ในการดำเนินการ"
                                                    ValidationExpression="กรุณาเลือกความถี่ในการดำเนินการ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenders7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiddredFieldValidator7" Width="160" />

                                            </div>
                                        </div>
                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">
                                        <div class="col-md-12">

                                            <asp:Label ID="Label5" class="col-xs-2 control-labelnotop text_right" runat="server" Text="ระยะวันที่ดำเนินการ : " />
                                            <div class="col-xs-3">
                                                <asp:TextBox ID="txtcount_edit" Enabled="false" ValidationGroup="SaveEdit" CssClass="form-control" runat="server" Text='<%# Eval("countplan") %>' />

                                                <asp:RequiredFieldValidator ID="RqResstxtprddice22" ValidationGroup="SaveEdit" runat="server" Display="None"
                                                    ControlToValidate="txtcount_edit" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกจำนวน" />
                                                <asp:RegularExpressionValidator ID="Retxtpaassrice22" runat="server" ValidationGroup="SaveEdit" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                    ControlToValidate="txtcount_edit"
                                                    ValidationExpression="^[0-9]{1,10}$" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqResstxtprddice22" Width="160" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtpaassrice22" Width="160" />

                                            </div>

                                            <asp:Label ID="Label6" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ประเภทเครื่องจักร	 : " />
                                            <div class="col-xs-4">
                                                <asp:TextBox ID="txttypename" CssClass="form-control" Enabled="false" runat="server" Text='<%# Eval("TypeNameTH") %>' />
                                                <asp:TextBox ID="txtTmcIDX" CssClass="form-control" Visible="false" runat="server" Text='<%# Eval("TmcIDX") %>' />

                                            </div>

                                            <%-- <asp:Label ID="Label6" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสเครื่องจักร : " />
                                            <div class="col-xs-4">
                                                <asp:TextBox ID="lblemail" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("MCCode") %>' />

                                            </div>--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12">

                                            <asp:Label ID="Label33" class="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสกลุ่มเครื่องจักร : " />
                                            <div class="col-xs-3">
                                                <asp:TextBox ID="txttypecode" CssClass="form-control" Enabled="false" runat="server" Text='<%# Eval("NameTypecode") %>' />
                                                <asp:TextBox ID="txtTCIDX" CssClass="form-control" Visible="false" runat="server" Text='<%# Eval("TCIDX") %>' />

                                            </div>

                                            <asp:Label ID="Label37" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสย่อย : " />
                                            <div class="col-xs-4">
                                                <asp:TextBox ID="txtgroupcode" CssClass="form-control" Enabled="false" runat="server" Text='<%# Eval("NameGroupcode") %>' />
                                                <asp:TextBox ID="txtGCIDX" CssClass="form-control" Visible="false" runat="server" Text='<%# Eval("GCIDX") %>' />


                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group" style="overflow-y: scroll; width: auto; height: 100px" id="idchecklist" runat="server">
                                        <asp:Label ID="Label15" class="col-sm-2 control-label" runat="server" Text="เครื่องจักร : " />
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtchk_mcidx" Visible="false" CssClass="form-control" runat="server" Text='<%# Eval("MCIDX_Comma") %>'></asp:TextBox>
                                            <asp:CheckBoxList ID="chkmachine_edit"
                                                runat="server"
                                                CellPadding="5"
                                                CellSpacing="5"
                                                RepeatColumns="4"
                                                RepeatDirection="Vertical"
                                                RepeatLayout="Table"
                                                TextAlign="Right"
                                                Width="100%">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-10">
                                            <asp:LinkButton ID="LinkButton6" CssClass="btn btn-success btn-sm" ValidationGroup="SaveEdit" runat="server" OnCommand="btnCommand" CommandName="btnsaveedit" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check-circle"></i></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton7" CssClass="btn btn-danger btn-sm" runat="server" OnCommand="btnCommand" CommandName="btncanceledit" OnClientClick="return confirm('คุณต้องการยกเลิกแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-remove-sign"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </EditItemTemplate>
                            </asp:FormView>

                            <hr />
                            <br />

                            <asp:GridView ID="GvShowPlan" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="success"
                                HeaderStyle-Height="40px"
                                DataKeyNames="u1idx_planning"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>


                                    <asp:TemplateField HeaderText="วันที่เริ่มต้นดำเนินการ" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldateplanstart" runat="server" Text='<%# Eval("dateplanstart") %>'></asp:Label>
                                            <asp:Label ID="Label35" Visible="false" runat="server" Text='<%# Eval("u1idx_planning") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="วันที่สิ้นสุดดำเนินการ" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbldateplanend" runat="server" Text='<%# Eval("dateplanend") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="วันที่ดำเนินการ" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lbdatemanage" runat="server" Text='<%# Eval("DateManage") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="รหัสรายการ" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRecode" runat="server" Text='<%# Eval("RECode") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-HorizontalAlign="Left" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:Label ID="ltstatus" Visible="false" runat="server" Text='<%# Eval("STAppIDX") %>'></asp:Label>
                                            <asp:Label ID="listate" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center" HeaderStyle-Width="8%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbViewEN" runat="server" CssClass="btn btn-warning btn-xs" Text="" OnCommand="btnCommand" CommandName="ViewFormResult" title="ViewDetail" CommandArgument='<%# Eval("RPIDX") +  ";" + Eval("nodeidx")+ ";" + Eval("actoridx")+ ";" +  Eval("STAppIDX")%>'><i class="fa fa-eye" ></i> </asp:LinkButton>
                                            </p>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                            <div class="form-group">
                                <div class="col-sm-1 col-sm-offset-11">
                                    <asp:LinkButton ID="LinkButton7" CssClass="btn btn-danger btn-sm" runat="server" OnCommand="btnCommand" CommandName="_divMenuBtnToDivMange" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-remove-sign"></i></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </asp:View>

        <asp:View ID="ViewAdd" runat="server">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-calendar-check-o"></i><strong>&nbsp; กำหนดวันที่นัดหมาย</strong></h3>
                    </div>
                    <asp:FormView ID="FvInsert" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">

                                        <asp:Label ID="Label17" class="col-sm-2 control-label" runat="server" Text="กำหนดแผนการใช้ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlcustom" ValidationGroup="Save" class="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกกำหนดแผนการใช้..."></asp:ListItem>
                                                <asp:ListItem Value="1" Text="ต่อปี"></asp:ListItem>
                                                <asp:ListItem Value="2" Text="ทุกปี"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlcustom" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกกำหนดแผนการใช้"
                                                ValidationExpression="กรุณาเลือกกำหนดแผนการใช้" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />

                                        </div>
                                        <asp:Label ID="Label2" class="col-sm-3 control-label" runat="server" Text="สถานที่ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddllocate" ValidationGroup="Save" class="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกสถานที่..."></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddllocate" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกสถานที่"
                                                ValidationExpression="กรุณาเลือกสถานที่" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label1" class="col-sm-2 control-label" runat="server" Text="ประเภทเครื่องจักร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypemachine" ValidationGroup="Save" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" class="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกประเภทเครื่องจักร..."></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddltypemachine" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทเครื่องจักร"
                                                ValidationExpression="กรุณาเลือกประเภทเครื่องจักร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                        </div>

                                        <asp:Label ID="Label18" class="col-sm-3 control-label" runat="server" Text="รหัสกลุ่มเครื่องจักร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypecode" ValidationGroup="Save" class="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกรหัสกลุ่มเครื่องจักร..."></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddltypecode" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกรหัสกลุ่มเครื่องจักร"
                                                ValidationExpression="กรุณาเลือกรหัสกลุ่มเครื่องจักร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label3" class="col-sm-2 control-label" runat="server" Text="รหัสกลุ่ม : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlgroupmachine" ValidationGroup="Save" class="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกรหัสกลุ่ม..."></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlgroupmachine" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกรหัสกลุ่ม"
                                                ValidationExpression="กรุณาเลือกรหัสกลุ่ม" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label13" class="col-sm-2 control-label" runat="server" Text="อาคาร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlbuilding" ValidationGroup="Save" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกอาคาร...</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlbuilding" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกอาคาร"
                                                ValidationExpression="กรุณาเลือกอาคาร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                        </div>

                                        <asp:Label ID="Label14" class="col-sm-3 control-label" runat="server" Text="ห้อง : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlroom" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกห้อง...</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                    </div>

                                    <div class="form-group" style="overflow-y: scroll; width: auto; height: 100px" id="idchecklist" visible="false" runat="server">
                                        <asp:Label ID="Label15" class="col-sm-2 control-label" runat="server" Text="เครื่องจักร : " />
                                        <div class="col-sm-10">

                                            <asp:CheckBoxList ID="chkmachine"
                                                runat="server"
                                                CellPadding="5"
                                                CellSpacing="5"
                                                RepeatColumns="4"
                                                RepeatDirection="Vertical"
                                                RepeatLayout="Table"
                                                TextAlign="Right"
                                                Width="100%">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>

                                    <hr />

                                    <div class="form-group">

                                        <asp:Label ID="Label11" class="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddldep" class="form-control" ValidationGroup="Save" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกฝ่าย..."></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddldep" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกฝ่าย"
                                                ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />

                                        </div>

                                        <asp:Label ID="Label12" class="col-sm-3 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsec" runat="server" CssClass="form-control" ValidationGroup="Save">
                                                <asp:ListItem Value="0">กรุณาเลือกแผนก...</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlsec" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกแผนก"
                                                ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label10" class="col-sm-2 control-label" runat="server" Text="ความถี่ในการดำเนินการ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlplan" class="form-control" runat="server" ValidationGroup="Save">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกความถี่ในการดำเนินการ..."></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlplan" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกความถี่ในการดำเนินการ"
                                                ValidationExpression="กรุณาเลือกความถี่ในการดำเนินการ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />

                                        </div>

                                        <asp:Label ID="Label16" class="col-sm-3 control-label" runat="server" Text="วันที่เริ่มดำเนินการ : " />
                                        <div class="col-sm-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="AddStartdate" runat="server" ValidationGroup="Save" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="AddStartdate" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                    ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label19" class="col-sm-2 control-label" runat="server" Text="ระยะความถี่ในการดำเนินการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtcount" CssClass="form-control" runat="server" ValidationGroup="Save"> </asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RqRetxtprddice22" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="txtcount" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวน" />
                                            <asp:RegularExpressionValidator ID="Retxtpssrice22" runat="server" ValidationGroup="Save" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtcount"
                                                ValidationExpression="^[0-9]{1,10}$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprddice22" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtpssrice22" Width="160" />

                                        </div>
                                        <div class="col-sm-2">
                                            <asp:Label ID="Labeh" CssClass="col-sm-2 control-label" runat="server" Text="วัน" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-2">
                                            <asp:LinkButton ID="btnAddPlan" CssClass="btn btn-warning btn-sm" runat="server" Text="ADD +" ValidationGroup="Save" OnCommand="btnCommand" CommandName="CmdAddPlan"></asp:LinkButton>
                                        </div>

                                    </div>

                                    <br />
                                    <div class="form-group">
                                        <asp:GridView ID="GvReportAdd"
                                            runat="server"
                                            CssClass="table table-striped table-responsive"
                                            GridLines="None"
                                            OnRowCommand="onRowCommand"
                                            Visible="false"
                                            AutoGenerateColumns="false">


                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>

                                            <Columns>


                                                <asp:BoundField DataField="custom" HeaderText="กำหนดแผนการใช้" ItemStyle-CssClass="text-center" ItemStyle-Width="5%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="customidx" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />




                                                <asp:BoundField DataField="LocName" HeaderText="สถานที่" ItemStyle-CssClass="text-center" ItemStyle-Width="5%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="LocIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />




                                                <asp:BoundField DataField="NameEN" HeaderText="ประเภทเครื่องจักร" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="TmcIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />



                                                <asp:BoundField DataField="NameTypecode" HeaderText="รหัสกลุ่มเครื่องจักร" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="TCIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />


                                                <asp:BoundField DataField="NameGroupcode" HeaderText="รหัสกลุ่ม" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="GCIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />


                                                <asp:BoundField DataField="BuildingName" HeaderText="อาคาร" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="BuildingIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />


                                                <asp:BoundField DataField="RoomName" HeaderText="ห้อง" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="RoomIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="RDeptName" HeaderText="ฝ่าย" ItemStyle-CssClass="text-center" ItemStyle-Width="5%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="RDeptIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />


                                                <asp:BoundField DataField="RSecName" HeaderText="แผนก" ItemStyle-CssClass="text-center" ItemStyle-Width="5%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="RSecIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />



                                                <asp:BoundField DataField="MCCode" HeaderText="เครื่องจักร" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="MCIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />




                                                <asp:BoundField DataField="time_desc" HeaderText="ความถี่ในการดำเนินการ" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="m0tidx" Visible="false" ItemStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="datestart" HeaderText="วันที่เริ่มดำเนินการ" ItemStyle-CssClass="text-center" ItemStyle-Width="17%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                <asp:BoundField DataField="countdate" HeaderText="ระยะความถี่ในการดำเนินการ" ItemStyle-CssClass="text-center" ItemStyle-Width="20%"
                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />



                                                <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteList"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                    </ItemTemplate>

                                                    <EditItemTemplate />
                                                    <FooterTemplate />
                                                </asp:TemplateField>

                                            </Columns>

                                        </asp:GridView>
                                    </div>


                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>

                                            <%-------------- Upload File --------------%>

                                            <div class="form-group">

                                                <asp:Label ID="Label54" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                                <div class="col-sm-7">
                                                    <asp:FileUpload ID="UploadImages" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-primary btn-sm multi  max-1 " accept="gif|jpg|pdf|png|pdf" />
                                                    <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png</font></p>

                                                </div>
                                            </div>


                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>


                                    <div class="form-group" id="divsave" runat="server" visible="false">
                                        <div class="col-sm-2 col-sm-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" CommandName="CmdInsertForm"><i class="glyphicon glyphicon-floppy-disk"></i></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" CssClass="btn btn-danger btn-sm" runat="server" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-floppy-remove"></i></asp:LinkButton>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewAddResult" runat="server">

            <div class="col-lg-12" id="div_searchresult" runat="server" visible="false">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; Search Data</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div id="Div1" runat="server">
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label47" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ :" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddllocate_result" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>

                                        </div>
                                        <asp:Label ID="Label48" class="col-sm-2 control-label" runat="server" Text="อาคาร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlbuilding_result" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกอาคาร...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label49" class="col-sm-2 control-label" runat="server" Text="ห้อง : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlroom_result" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกห้อง...</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                        <asp:Label ID="Label50" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทเครื่องจักร :" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltype_result" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกประเภทเครื่องจักร...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>


                                    <div class="form-group">

                                        <asp:Label ID="Label51" CssClass="col-sm-2 control-label" runat="server" Text="รหัสกลุ่มเครื่องจักร :" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypecode_result" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="กรุณาเลือกรหัสกลุ่มเครื่องจักร..." Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <asp:Label ID="Label52" class="col-sm-2 control-label" runat="server" Text="รหัสกลุ่ม : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlgroupcode_result" ValidationGroup="Save" class="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกรหัสกลุ่ม..."></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label55" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย :" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddldept_result" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                        <asp:Label ID="Label56" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsec_result" ValidationGroup="Save" class="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกแผนก..."></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label53" CssClass="col-sm-2 control-label" runat="server" Text="ความถี่ในการดำเนินการ :" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlplan_result" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="กรุณาเลือกความถี่ในการดำเนินการ..." Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <%--                                        <asp:Label ID="Label54" class="col-sm-2 control-label" runat="server" Text="ปี : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlyear_result" ValidationGroup="Save" class="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกปี..."></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>--%>
                                    </div>



                                    <div class="form-group">

                                        <div class="col-sm-5 col-sm-offset-2">
                                            <asp:LinkButton ID="LinkButton8" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch_result" ValidationGroup="btnsearch" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton9" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Refresh" runat="server" CommandName="_divMenuBtnToDivAddResult" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-12">

                <div id="gridviewindex" runat="server">
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <asp:UpdatePanel ID="upActor1Node1Files" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="GvViewENRepair"
                                        runat="server"
                                        AutoGenerateColumns="false"
                                        DataKeyNames="RPIDX"
                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                        HeaderStyle-CssClass="info"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="true"
                                        PageSize="10"
                                        OnPageIndexChanging="Master_PageIndexChanging"
                                        OnRowDataBound="Master_RowDataBound">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center;">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>

                                        <Columns>

                                            <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("RECode") %>' /></small>
                                                    <asp:Literal ID="Literal5" Visible="false" runat="server" Text='<%# Eval("RPIDX") %>' />
                                                    <asp:Literal ID="ltstatus" Visible="false" runat="server" Text='<%# Eval("STAppIDX") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ระบบ" Visible="false" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Literal ID="lisystem" runat="server" Text='<%# Eval("SysNameTH") %>' /></small>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ข้อมูลเครื่องจักร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" ItemStyle-CssClass="text-left">
                                                <ItemTemplate>
                                                    <small>
                                                        <strong>
                                                            <asp:Label ID="Label20" runat="server">ประเภทเครื่องจักร: </asp:Label></strong>
                                                        <asp:Literal ID="Literal210" runat="server" Text='<%# Eval("TypeNameTH") %>' />
                                                        </p>

                                                            <strong>
                                                                <asp:Label ID="Label21" runat="server">รหัสกลุ่มเครื่องจักร: </asp:Label></strong>
                                                        <asp:Literal ID="Lietedral11" runat="server" Text='<%# Eval("NameTypecode") %>' />
                                                        </p>
                                                            <strong>
                                                                <asp:Label ID="Label22" runat="server">รหัสกลุ่มย่อย: </asp:Label></strong>
                                                        <asp:Literal ID="Literfal13" runat="server" Text='<%# Eval("NameGroupcode") %>' />

                                                        </p>
                                                    <strong>
                                                        <asp:Label ID="Label1036" runat="server">รหัสใหม่: </asp:Label></strong>
                                                        <asp:Literal ID="Literal41" runat="server" Text='<%# Eval("MCCode") %>' />
                                                        </p>
                                                    <strong>
                                                        <asp:Label ID="Label132" runat="server">รหัสเก่า: </asp:Label></strong>
                                                        <asp:Literal ID="Literal12" runat="server" Text='<%# Eval("Abbreviation") %>' />

                                                    </small>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ข้อมูลกำหนดการ PM" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" ItemStyle-CssClass="text-left">
                                                <ItemTemplate>

                                                    <small>
                                                        <strong>
                                                            <asp:Label ID="Label210" runat="server">ฝ่าย: </asp:Label></strong>
                                                        <asp:Literal ID="Liter2al10" runat="server" Text='<%# Eval("UserDeptNameTH") %>' />
                                                        </p>

                                                            <strong>
                                                                <asp:Label ID="Label121" runat="server">แผนก: </asp:Label></strong>
                                                        <asp:Literal ID="Lieteral11" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                                        </p>
                                                            <strong>
                                                                <asp:Label ID="Lab1el22" runat="server">ช่วงเวลา: </asp:Label></strong>
                                                        <asp:Literal ID="Literal13" runat="server" Text='<%# Eval("datestart") %>' />

                                                        </p>
                                                    <strong>
                                                        <asp:Label ID="Label11036" runat="server">วันที่ดำเนินการ: </asp:Label></strong>
                                                        <asp:Literal ID="Lidteral41" runat="server" Text='<%# Eval("DateManage") %>' />
                                                        </p>
                                                    <strong>
                                                        <asp:Label ID="Label1132" runat="server">ผู้สร้าง: </asp:Label></strong>
                                                        <asp:Literal ID="Literasl12" runat="server" Text='<%# Eval("NameUser") %>' />

                                                        </p>
                                                          <strong>
                                                              <asp:Label ID="Label23" runat="server">วันที่สร้าง: </asp:Label></strong>
                                                        <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("DateAlertJob") %>' />

                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ข้อมูลเพิ่มเติม" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="14%">
                                                <ItemTemplate>
                                                    <small>
                                                        <strong>
                                                            <asp:Label ID="Label17" runat="server"> รายละเอียด: </asp:Label></strong>
                                                        <asp:Literal ID="DetailProblem" runat="server" Text='<%# Eval("CommentUser")%>' />
                                                        </p>
                                                    <strong>
                                                        <asp:Label ID="Label1037" runat="server"> สถานที่: </asp:Label></strong>
                                                        <asp:Literal ID="Literal1230" runat="server" Text='<%# Eval("LocName") %>' />
                                                        </p>
                                                      <strong>
                                                          <asp:Label ID="Label16" runat="server"> อาคาร: </asp:Label></strong>
                                                        <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("BuildingName") %>' />
                                                        </p>
                                                      <strong>
                                                          <asp:Label ID="Label18" runat="server"> ห้อง: </asp:Label></strong>
                                                        <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("Roomname") %>' />
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="สถานะรายการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="7%" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="listate" runat="server" /></small>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lbViewEN" runat="server" CssClass="btn btn-warning btn-xs" Text="" OnCommand="btnCommand" CommandName="ViewFormResult" title="ViewDetail" CommandArgument='<%# Eval("RPIDX") +  ";" + Eval("nodeidx")+ ";" + Eval("actoridx")+ ";" +  Eval("STAppIDX")%>'><i class="fa fa-edit" ></i> </asp:LinkButton>
                                                    </p>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>

            </div>
        </asp:View>

        <asp:View ID="ViewDetailResult" runat="server">
            <div class="col-lg-12">


                <div class="form-horizontal" role="form">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <div class="alert-message alert-message-success">
                                <blockquote class="danger" style="background-color: powderblue;">
                                    <h4 style="text-align: left"><b>
                                        <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="แบบฟอร์มใบบันทึก การตรวจเช็คเครื่องจักรสำคัญ"></asp:Label>
                                    </b></h4>
                                </blockquote>
                            </div>
                            <div class="panel-body">

                                <asp:FormView ID="FvBindMachine" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                    <EditItemTemplate>
                                        <div class="panel-body">
                                            <div class="form-horizontal" role="form">

                                                <div class="form-group">
                                                    <asp:Label ID="Label25" class="col-sm-2 control-label" runat="server" Text="สถานที่ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtlocname" Enabled="false" Text='<%# Eval("LocName") %>' runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                    <asp:Label ID="Label26" class="col-sm-2 control-label" runat="server" Text="อาคาร : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtbuilding" Enabled="false" Text='<%# Eval("BuildingName") %>' runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label27" class="col-sm-2 control-label" runat="server" Text="ห้อง : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtroom" Enabled="false" Text='<%# Eval("Roomname") %>' runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                    <asp:Label ID="Label17" class="col-sm-2 control-label" runat="server" Text="Machine Name : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtmacinename" Enabled="false" Text='<%# Eval("MCNameTH") %>' runat="server" CssClass="form-control"></asp:TextBox>
                                                        <asp:TextBox ID="txtu1idx_planning" Visible="false" Text='<%# Eval("u1idx_planning") %>' runat="server" CssClass="form-control"></asp:TextBox>
                                                        <asp:TextBox ID="txtu0idx" Visible="false" Text='<%# Eval("u0idx") %>' runat="server" CssClass="form-control"></asp:TextBox>
                                                        <asp:TextBox ID="txtRPIDX" Visible="false" Text='<%# Eval("RPIDX") %>' runat="server" CssClass="form-control"></asp:TextBox>

                                                    </div>


                                                </div>


                                                <div class="form-group">
                                                    <asp:Label ID="Label24" class="col-sm-2 control-label" runat="server" Text="Machine No : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtmccode" Enabled="false" Text='<%# Eval("MCCode") %>' runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                    <asp:Label ID="Label28" class="col-sm-2 control-label" runat="server" Text="ความถี่ดำเนินการ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txttime" Enabled="false" Text='<%# Eval("time_desc") %>' runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <asp:Label ID="Label29" class="col-sm-2 control-label" runat="server" Text="ช่วงกำหนดการ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtdatestart" Enabled="false" Text='<%# Eval("datestart") %>' runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>

                                                    <asp:Label ID="Label30" class="col-sm-2 control-label" runat="server" Text="วันที่ดำเนินการ : " />
                                                    <div class="col-sm-3">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="txtdatemanage" Text='<%# Eval("DateManage") %>' runat="server" ValidationGroup="Save" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่"></asp:TextBox>
                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="txtdatemanage" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                                ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </EditItemTemplate>
                                </asp:FormView>


                                <asp:GridView ID="GvItem" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="success small"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="false"
                                    DataKeyNames="m1idx"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="POINT จุดที่ตรวจ" ItemStyle-CssClass="text-center" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Literal ID="lbtype" runat="server" Text='<%# Eval("type_name_th") %>'></asp:Literal>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="CONTENT รายละเอียดที่ตรวจ" ItemStyle-CssClass="text-center" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Literal Visible="false" ID="lbm2idx" runat="server" Text='<%# Eval("m2idx") %>'></asp:Literal>
                                                <asp:Label ID="lbcontent" runat="server" Text='<%# Eval("contentth") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="STANDARD มาตรฐาน" ItemStyle-CssClass="text-center" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbstandard" runat="server" Text='<%# Eval("standard_name_th") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="METHOD วิธีที่ตรวจ" ItemStyle-CssClass="text-center" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbmethod" runat="server" Text='<%# Eval("method_name_th") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="เวลาในการปฏิบัติการ" ItemStyle-CssClass="text-center" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbtime" runat="server" Text='<%# Eval("time") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="อะไหล่" ItemStyle-CssClass="text-center" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbtool" runat="server" Text='<%# Eval("tooling_name_th") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="สถานะดำเนินการ" ItemStyle-CssClass="text-center" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <div id="div_addstatus" runat="server">
                                                    <asp:DropDownList ID="ddlstatus" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0" Text="กรุณาเลือกสถานะดำเนินการ..."></asp:ListItem>
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="saveresult" runat="server" Display="None"
                                                        ControlToValidate="ddlstatus" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกสถานะดำเนินการ"
                                                        ValidationExpression="กรุณาเลือกสถานะดำเนินการ" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />

                                                </div>

                                                <div id="div_bindstatus" runat="server">

                                                    <asp:Label ID="lbstatus_name" runat="server" Text='<%# Eval("status_name") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ความคิดเห็นเพิ่มเติม" ItemStyle-CssClass="text-center" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <div id="div_addcomment" runat="server">
                                                    <asp:TextBox ID="txtremark" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                                </div>

                                                <div id="div_bindcomment" runat="server">

                                                    <asp:Label ID="lbcomment" runat="server" Text='<%# Eval("result_comment") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>



                                <div class="form-group" runat="server" id="div_save">
                                    <div class="col-sm-1 col-sm-offset-11">
                                        <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success btn-sm" ValidationGroup="saveresult" runat="server" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" CommandName="CmdAddResult"><i class="glyphicon glyphicon-ok-sign"></i></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton2" CssClass="btn btn-danger btn-sm" runat="server" OnCommand="btnCommand" CommandName="btnbackresult" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-remove-sign"></i></asp:LinkButton>
                                    </div>
                                </div>


                                <div class="form-group" runat="server" id="div_cancel">
                                    <div class="col-sm-1 col-sm-offset-11">
                                        <asp:LinkButton ID="btnsaveuser" CssClass="btn btn-success btn-sm" runat="server" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" CommandName="CmdAddResult"><i class="glyphicon glyphicon-ok-sign"></i></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton4" CssClass="btn btn-danger btn-sm" runat="server" OnCommand="btnCommand" CommandName="btnbackresult" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="glyphicon glyphicon-remove-sign"></i></asp:LinkButton>
                                    </div>
                                </div>

                            </div>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>




        </asp:View>

        <asp:View ID="ViewApprove" runat="server">
            <div class="col-lg-12">
            </div>
        </asp:View>


    </asp:MultiView>



    <script type="text/javascript">
        var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
    </script>

    <script type="text/javascript">
        function fnExcelReport() {
            var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange; var j = 0;
            tab = document.getElementById('Tableprint_sched'); // id of table

            for (j = 0; j < tab.rows.length; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }
    </script>

    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);
            var printWindow = window.open('', '', 'height=800,width=1000');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
                //printWindow.resizeTo(100,100);
                //printWindow.focus();
                //printwindow.window.close();
            }, 500);
            return false;
        }

        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }

        function dtPickerInput(inputClassFrom, inputClassTo) {
            $(inputClassFrom).datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $(inputClassTo).datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }

        function dtPickerBtn(inputClassFrom, inputClassTo, btnClassFrom, btnClassTo) {
            $(btnClassFrom).click(function () {
                $(inputClassFrom).data("DateTimePicker").show();
            });
            $(btnClassTo).click(function () {
                $(inputClassTo).data("DateTimePicker").show();
            });
        }

        $(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
    </script>

    <asp:HiddenField ID="hddf_empty" runat="server" />
</asp:Content>

