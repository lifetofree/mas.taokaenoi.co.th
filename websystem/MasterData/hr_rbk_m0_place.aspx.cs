﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_hr_rbk_m0_place : System.Web.UI.Page
{

    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_roombooking _data_roombooking = new data_roombooking();

    data_qa_cims _data_qa_cims = new data_qa_cims();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- roombooking --//
    static string _urlSetRbkm0Place = _serviceUrl + ConfigurationManager.AppSettings["urlSetRbkm0Place"];
    static string _urlGetRbkm0Place = _serviceUrl + ConfigurationManager.AppSettings["urlGetRbkm0Place"];
    static string _urlSetRbkm0PlaceDel = _serviceUrl + ConfigurationManager.AppSettings["urlSetRbkm0PlaceDel"];


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ViewState["empIDX"] = Session["emp_idx"];
            Select_Place();


        }
    }
    #endregion Page Load

    #region initPage
    protected void initPage()
    {
        MvSystem.SetActiveView(ViewIndex);
        Select_Place();
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdAddPlace":

                Gv_select_place.EditIndex = -1;
                Select_Place();
                btn_addplace.Visible = false;
                setFormData(FvInsert, FormViewMode.Insert, null);
                FvInsert.Visible = true;
                //Gv_select_place.Visible = false;


                break;

            case "CmdSave":
                Insert_Place();
                Select_Place();
                btn_addplace.Visible = true;
                FvInsert.Visible = false;
                break;

            case "Lbtn_cancel_place":
                btn_addplace.Visible = true;
                FvInsert.Visible = false;

                break;

            case "btnTodelete_place":

                int place_idx_del = int.Parse(cmdArg);

                data_roombooking data_m0_place_del = new data_roombooking();
                rbk_m0_place_detail m0_place_del = new rbk_m0_place_detail();
                data_m0_place_del.rbk_m0_place_list = new rbk_m0_place_detail[1];

                m0_place_del.place_idx = place_idx_del;
                m0_place_del.cemp_idx = _emp_idx;

                data_m0_place_del.rbk_m0_place_list[0] = m0_place_del;
                data_m0_place_del = callServicePostRoomBooking(_urlSetRbkm0PlaceDel, data_m0_place_del);

                Select_Place();
                //Gv_select_unit.Visible = false;
                btn_addplace.Visible = true;
                FvInsert.Visible = false;
                break;
        }
    }
    #endregion btnCommand

    #region Custom Functions
    protected void Insert_Place()
    {
        
        TextBox tex_place_name = (TextBox)FvInsert.FindControl("txtplace_name");
        DropDownList dropD_status_place = (DropDownList)FvInsert.FindControl("ddPlace");

        data_roombooking data_m0_place = new data_roombooking();
        rbk_m0_place_detail m0_place_insert = new rbk_m0_place_detail();
        data_m0_place.rbk_m0_place_list = new rbk_m0_place_detail[1];

        m0_place_insert.place_idx = 0;
        m0_place_insert.place_name = tex_place_name.Text;
        m0_place_insert.cemp_idx = _emp_idx;
        m0_place_insert.place_status = int.Parse(dropD_status_place.SelectedValue);

        data_m0_place.rbk_m0_place_list[0] = m0_place_insert;       
        data_m0_place = callServicePostRoomBooking(_urlSetRbkm0Place, data_m0_place);

        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Unit_b.return_code));
        if (data_m0_place.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            tex_place_name.Text = String.Empty;
            //tex_place_code.Text = String.Empty;
        }
    }

    protected void Select_Place()
    {

        data_roombooking data_m0_place_detail = new data_roombooking();
        rbk_m0_place_detail m0_place_detail = new rbk_m0_place_detail();
        data_m0_place_detail.rbk_m0_place_list = new rbk_m0_place_detail[1];

        m0_place_detail.place_idx = 0;
       
        data_m0_place_detail.rbk_m0_place_list[0] = m0_place_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_place_detail = callServicePostRoomBooking(_urlGetRbkm0Place, data_m0_place_detail);
       
        setGridData(Gv_select_place, data_m0_place_detail.rbk_m0_place_list);
        //Gv_select_place.DataSource = m0_place_detail.qa_cims_m0_place_list;
        //Gv_select_place.DataBind();
    }
    #endregion Custom Functions

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "Gv_select_place":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbpplace_status = (Label)e.Row.Cells[3].FindControl("lbpplace_status");
                    Label place_statusOnline = (Label)e.Row.Cells[3].FindControl("place_statusOnline");
                    Label place_statusOffline = (Label)e.Row.Cells[3].FindControl("place_statusOffline");

                    ViewState["_place_status"] = lbpplace_status.Text;


                    if (ViewState["_place_status"].ToString() == "1")
                    {
                        place_statusOnline.Visible = true;
                    }
                    else if (ViewState["_place_status"].ToString() == "0")
                    {
                        place_statusOffline.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                    //btn_addplace.Visible = true;
                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);

                    //MultiView1.Visible = true;
                    //Gv_select_unit.Visible = false;
                    btn_addplace.Visible = true;
                    FvInsert.Visible = false;

                }



                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_place":
                Gv_select_place.EditIndex = e.NewEditIndex;
                Select_Place();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "Gv_select_place":

                var txt_place_idx_edit = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("txt_place_idx_edit");
                var place_name = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("Name_place");
                var place_status = (DropDownList)Gv_select_place.Rows[e.RowIndex].FindControl("ddEdit_place");


                Gv_select_place.EditIndex = -1;


                data_roombooking data_m0_place = new data_roombooking();
                rbk_m0_place_detail m0_place_insert = new rbk_m0_place_detail();
                data_m0_place.rbk_m0_place_list = new rbk_m0_place_detail[1];

                m0_place_insert.place_idx = int.Parse(txt_place_idx_edit.Text);
                m0_place_insert.place_name = place_name.Text;
                m0_place_insert.cemp_idx = _emp_idx;
                m0_place_insert.place_status = int.Parse(place_status.SelectedValue);

                data_m0_place.rbk_m0_place_list[0] = m0_place_insert;
                data_m0_place = callServicePostRoomBooking(_urlSetRbkm0Place, data_m0_place);


                if (data_m0_place.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);
                    Select_Place();
                }
                else
                {
                    Select_Place();
                }

                

                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "Gv_select_place":
                Gv_select_place.EditIndex = -1;
                Select_Place();
                //btn_addplace.Visible = true;
                //SETFOCUS_ONTOP.Focus();
                //Gv_select_unit.Visible = false;
                btn_addplace.Visible = true;
                FvInsert.Visible = false;
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_place":
                Gv_select_place.PageIndex = e.NewPageIndex;
                Gv_select_place.DataBind();
                Select_Place();
                break;

        }
    }
    #endregion

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_roombooking callServicePostRoomBooking(string _cmdUrl, data_roombooking _data_roombooking)
    {
        _localJson = _funcTool.convertObjectToJson(_data_roombooking);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_roombooking = (data_roombooking)_funcTool.convertJsonToObject(typeof(data_roombooking), _localJson);


        return _data_roombooking;
    }
    #endregion callService Functions
}
