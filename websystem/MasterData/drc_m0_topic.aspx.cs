﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_drc_m0_topic : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_drc _data_drc = new data_drc();

    data_qa_cims _data_qa_cims = new data_qa_cims();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];


    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//


    //
    static string _urlGetM0SetnameDRC = _serviceUrl + ConfigurationManager.AppSettings["urlGetM0SetnameDRC"];
    static string _urlSetM0SetnameDRC = _serviceUrl + ConfigurationManager.AppSettings["urlSetM0SetnameDRC"];
    static string _urlSetDelM0SetnameDRC = _serviceUrl + ConfigurationManager.AppSettings["urlSetDelM0SetnameDRC"];
    static string _urlSetUpdateRootM0SetName = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateRootM0SetName"];

    //m0_topic
    static string _urlGetM0TopicDRC = _serviceUrl + ConfigurationManager.AppSettings["urlGetM0TopicDRC"];
    static string _urlSetM0TopicDRC = _serviceUrl + ConfigurationManager.AppSettings["urlSetM0TopicDRC"];
    static string _urlSetUpdateM0TopicDRC = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateM0TopicDRC"];
    static string _urlSetDelM0TopicDRC = _serviceUrl + ConfigurationManager.AppSettings["urlSetDelM0TopicDRC"];
    static string _urlGetM0SetnameDropDrowDRC = _serviceUrl + ConfigurationManager.AppSettings["urlGetM0SetnameDropDrowDRC"];
    static string _urlGetM0OptionnameDropDrowDRC = _serviceUrl + ConfigurationManager.AppSettings["urlGetM0OptionnameDropDrowDRC"];



    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {

            initPage();
            //MvMaster_lab.SetActiveView(view_Genaral);
            //Select_lab();
        }

    }
    #endregion

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region initPage
    protected void initPage()
    {

        clearSession();
        clearViewState();
        setActiveView("view_Genaral", 0);
        //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);
        SelectTopicName();
    }
    #endregion

    #region Select Bind Data
    protected void SelectTopicName()
    {

        data_drc _data_drc_topicdetail = new data_drc();
        SetTopicNameDetail m0_topicdetail = new SetTopicNameDetail();
        _data_drc_topicdetail.BoxTopicNameList = new SetTopicNameDetail[1];
        m0_topicdetail.root_idx = 0;//int.Parse(ViewState["vs_setname_sidx"].ToString());

        _data_drc_topicdetail.BoxTopicNameList[0] = m0_topicdetail;

        _data_drc_topicdetail = callServicePostDRC(_urlGetM0TopicDRC, _data_drc_topicdetail);
        setGridData(GvTopic, _data_drc_topicdetail.BoxTopicNameList);

       // test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_drc_topicdetail));

    }

    protected void SelectRootSetnane()
    {

        //test_lab.Text = ViewState["vs_setname_sidx"].ToString();

        data_drc _data_drc_topicdetail_root = new data_drc();
        SetTopicNameDetail m0_topicdetail_root = new SetTopicNameDetail();
        _data_drc_topicdetail_root.BoxTopicNameList = new SetTopicNameDetail[1];

        m0_topicdetail_root.root_idx = int.Parse(ViewState["vs_setname_sidx"].ToString());

        _data_drc_topicdetail_root.BoxTopicNameList[0] = m0_topicdetail_root;

        _data_drc_topicdetail_root = callServicePostDRC(_urlGetM0TopicDRC, _data_drc_topicdetail_root);
        setGridData(GvRootSetTopicName, _data_drc_topicdetail_root.BoxTopicNameList);

        //test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_drc));

    }

    protected void SelectRootSetnane_Sub()
    {

        //test_lab.Text = ViewState["vs_setname_sidx"].ToString();

        data_drc _data_drc_topicdetail_sub = new data_drc();
        SetTopicNameDetail m0_topicdetail_root = new SetTopicNameDetail();
        _data_drc_topicdetail_sub.BoxTopicNameList = new SetTopicNameDetail[1];

        m0_topicdetail_root.root_idx = int.Parse(ViewState["vs_setname_sidx_sub"].ToString());

        _data_drc_topicdetail_sub.BoxTopicNameList[0] = m0_topicdetail_root;

        _data_drc_topicdetail_sub = callServicePostDRC(_urlGetM0TopicDRC, _data_drc_topicdetail_sub);
        setGridData(GvRootLevel, _data_drc_topicdetail_sub.BoxTopicNameList);

        //test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_drc));

    }

    protected void ddlSelect_settopic()
    {

        DropDownList ddlsettopic = (DropDownList)fvform_Insert_Root.FindControl("ddlsettopic");
        ddlsettopic.AppendDataBoundItems = true;
        ddlsettopic.Items.Add(new ListItem("เลือกชุดข้อมูล", "00"));

        data_drc _data_drc_settopic = new data_drc();
        SetNameDetail m0_set_topic = new SetNameDetail();
        _data_drc_settopic.BoxM0SetNameList = new SetNameDetail[1];


        _data_drc_settopic.BoxM0SetNameList[0] = m0_set_topic;

        _data_drc_settopic = callServicePostDRC(_urlGetM0SetnameDropDrowDRC, _data_drc_settopic);

        ddlsettopic.DataSource = _data_drc_settopic.BoxM0SetNameList;
        ddlsettopic.DataTextField = "set_name";
        ddlsettopic.DataValueField = "sidx";
        ddlsettopic.DataBind();

    }

    protected void ddlSelect_option()
    {

        DropDownList ddloption = (DropDownList)fvform_Insert_Root.FindControl("ddloption");
        ddloption.AppendDataBoundItems = true;
        ddloption.Items.Add(new ListItem("เลือกการกรอกข้อมูล", "00"));

        data_drc _data_drc_option = new data_drc();
        SetOptionDetail m0_option = new SetOptionDetail();
        _data_drc_option.BoxOptionList = new SetOptionDetail[1];

        _data_drc_option.BoxOptionList[0] = m0_option;

        _data_drc_option = callServicePostDRC(_urlGetM0OptionnameDropDrowDRC, _data_drc_option);

        ddloption.DataSource = _data_drc_option.BoxOptionList;
        ddloption.DataTextField = "Option_name";
        ddloption.DataValueField = "OPIDX";
        ddloption.DataBind();

    }

    protected void ddlSelect_settopic_sub()
    {

        DropDownList ddlsettopic_lv = (DropDownList)fvform_root.FindControl("ddlsettopic_lv");
        ddlsettopic_lv.AppendDataBoundItems = true;
        ddlsettopic_lv.Items.Add(new ListItem("เลือกชุดข้อมูล", "00"));

        data_drc _data_drc_settopic = new data_drc();
        SetNameDetail m0_set_topic = new SetNameDetail();
        _data_drc_settopic.BoxM0SetNameList = new SetNameDetail[1];


        _data_drc_settopic.BoxM0SetNameList[0] = m0_set_topic;

        _data_drc_settopic = callServicePostDRC(_urlGetM0SetnameDropDrowDRC, _data_drc_settopic);

        ddlsettopic_lv.DataSource = _data_drc_settopic.BoxM0SetNameList;
        ddlsettopic_lv.DataTextField = "set_name";
        ddlsettopic_lv.DataValueField = "sidx";
        ddlsettopic_lv.DataBind();

    }

    protected void ddlSelect_option_sub()
    {

        DropDownList ddloption_lv = (DropDownList)fvform_root.FindControl("ddloption_lv");
        ddloption_lv.AppendDataBoundItems = true;
        ddloption_lv.Items.Add(new ListItem("เลือกการกรอกข้อมูล", "00"));

        data_drc _data_drc_option = new data_drc();
        SetOptionDetail m0_option = new SetOptionDetail();
        _data_drc_option.BoxOptionList = new SetOptionDetail[1];

        _data_drc_option.BoxOptionList[0] = m0_option;

        _data_drc_option = callServicePostDRC(_urlGetM0OptionnameDropDrowDRC, _data_drc_option);

        ddloption_lv.DataSource = _data_drc_option.BoxOptionList;
        ddloption_lv.DataTextField = "Option_name";
        ddloption_lv.DataValueField = "OPIDX";
        ddloption_lv.DataBind();

    }


    #endregion

    #region setDdlData
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion

    #region clear
    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    #endregion

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }
    #endregion

    #region callService
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_drc callServicePostDRC(string _cmdUrl, data_drc _data_drc)
    {
        _localJson = _funcTool.convertObjectToJson(_data_drc);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_drc = (data_drc)_funcTool.convertJsonToObject(typeof(data_drc), _localJson);


        return _data_drc;
    }

    #endregion

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {

            case "cmdAddSetTopicname":
                switch (cmdArg)
                {
                    case "0":
                        GvTopic.EditIndex = -1;
                        // btn_addlab.Visible = false;
                        setFormData(FvInsertTopicName, FormViewMode.Insert, null);
                        FvInsertTopicName.Visible = true;

                        SelectTopicName();
                        break;
                    case "1":
                        GvRootSetTopicName.EditIndex = -1;

                        btnAddRootSet.Visible = false;
                        setFormData(fvform_Insert_Root, FormViewMode.Insert, null);
                        //getM1Set_labName();
                        //FormView fvDocDetail_insert_Root_qa = (FormView)view_Genaral.FindControl("fvform_Insert_Root");
                        //getM1equipment_nameList((DropDownList)fvDocDetail_insert_Root_qa.FindControl("DD_EquipmentName"), "0");
                        fvform_Insert_Root.Visible = true;
                        TextBox txt_tidx_root = (TextBox)fvform_Insert_Root.FindControl("txt_tidx_root");
                        TextBox txt_tidx_root_list = (TextBox)fvform_Insert_Root.FindControl("txt_tidx_root_list");

                        txt_tidx_root.Text = ViewState["vs_setname_sidx"].ToString();
                        txt_tidx_root_list.Text = ViewState["vs_setname"].ToString();

                        SelectRootSetnane();
                        ddlSelect_settopic(); //ddl topic name
                        ddlSelect_option();   //ddl option name

                        break;

                    case "2":

                        btnAddSetRootTopicname.Visible = false;
                        setFormData(fvform_root, FormViewMode.Insert, null);
                        fvform_root.Visible = true;

                        TextBox txt_tidx_root_lv = (TextBox)fvform_root.FindControl("txt_tidx_root_lv");
                        TextBox txt_tidx_root_list_lv = (TextBox)fvform_root.FindControl("txt_tidx_root_list_lv");

                        txt_tidx_root_lv.Text = ViewState["vs_setname_sidx_sub"].ToString();
                        txt_tidx_root_list_lv.Text = ViewState["vs_setname_sub"].ToString();


                        SelectRootSetnane_Sub();
                        ddlSelect_settopic_sub(); //ddl topic name
                        ddlSelect_option_sub();   //ddl option name


                        break;
                }
                break;
            case "cmdCancel":
                switch (cmdArg)
                {
                    case "0":
                        GvTopic.EditIndex = -1;
                        btn_addsetname.Visible = true;
                        setActiveView("view_Genaral", 0);
                        FvInsertTopicName.Visible = false;
                        setFormData(FvInsertTopicName, FormViewMode.Insert, null);

                        SETFOCUS.Focus();
                        break;
                    case "1":
                        GvRootSetTopicName.EditIndex = -1;

                        btnAddRootSet.Visible = true;
                        setActiveView("view_CreateRoot", 0);
                        fvform_Insert_Root.Visible = false;
                        setFormData(fvform_Insert_Root, FormViewMode.Insert, null);

                        SETFOCUS.Focus();
                        break;
                }

                break;

            case "cmdbackTosetRoot":
                //ViewState["m0_lab_idx"] = 0;
                btnAddRootSet.Visible = true;
                setActiveView("view_CreateRoot", 0);
                setFormData(fvform_Insert_Root, FormViewMode.ReadOnly, null);
                SETFOCUS.Focus();
                break;

            case "cmdback":
                //ViewState["m0_lab_idx"] = 0;
                btn_addsetname.Visible = true;
                setActiveView("view_Genaral", 0);
                setFormData(FvInsertTopicName, FormViewMode.Insert, null);
                SETFOCUS.Focus();
                break;

            case "Lbtn_cancel_lab":
                btn_addsetname.Visible = true;
                setFormData(FvInsertTopicName, FormViewMode.ReadOnly, null);
                FvInsertTopicName.Visible = false;
                break;

            case "cmdSave":
                switch (cmdArg)
                {
                    case "0": //insert setname

                        TextBox txt_topic_name = (TextBox)FvInsertTopicName.FindControl("txt_topic_name");
                        DropDownList ddl_topic_status = (DropDownList)FvInsertTopicName.FindControl("ddl_topic_status");

                        data_drc _data_drc_insert_topic = new data_drc();
                        SetTopicNameDetail m0_topic_insert = new SetTopicNameDetail();
                        _data_drc_insert_topic.BoxTopicNameList = new SetTopicNameDetail[1];

                        m0_topic_insert.tidx = 0;
                        m0_topic_insert.topic_name = txt_topic_name.Text;
                        m0_topic_insert.topic_status = int.Parse(ddl_topic_status.SelectedValue);
                        m0_topic_insert.cempidx = _emp_idx;

                        _data_drc_insert_topic.BoxTopicNameList[0] = m0_topic_insert;

                        _data_drc_insert_topic = callServicePostDRC(_urlSetM0TopicDRC, _data_drc_insert_topic);
                        //test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(lab_bIN));

                        setActiveView("view_Genaral", 0);

                        btn_addsetname.Visible = true;
                        SelectTopicName();

                        SETFOCUS.Focus();
                        break;

                    case "1":

                        TextBox txt_tidx_root = (TextBox)fvform_Insert_Root.FindControl("txt_tidx_root");
                        TextBox txt_topic_name_set = (TextBox)fvform_Insert_Root.FindControl("txt_topic_name_set");
                        DropDownList ddlroot_topicstatus = (DropDownList)fvform_Insert_Root.FindControl("ddlroot_topicstatus");
                        DropDownList ddlsettopic = (DropDownList)fvform_Insert_Root.FindControl("ddlsettopic");
                        DropDownList ddloption = (DropDownList)fvform_Insert_Root.FindControl("ddloption");


                        data_drc _data_drc_topicroot = new data_drc();
                        SetTopicNameDetail m0_settopicname_root = new SetTopicNameDetail();
                        _data_drc_topicroot.BoxTopicNameList = new SetTopicNameDetail[1];

                        m0_settopicname_root.root_idx = int.Parse(txt_tidx_root.Text);
                        m0_settopicname_root.topic_name = txt_topic_name_set.Text;
                        //m0_settopicname_root.set_name = txt_statusname_set.Text;
                        m0_settopicname_root.topic_status = int.Parse(ddlroot_topicstatus.SelectedValue);
                        m0_settopicname_root.sidx = int.Parse(ddlsettopic.SelectedValue);
                        m0_settopicname_root.OPIDX = int.Parse(ddloption.SelectedValue);
                        m0_settopicname_root.cempidx = _emp_idx;

                        _data_drc_topicroot.BoxTopicNameList[0] = m0_settopicname_root;

                        _data_drc_topicroot = callServicePostDRC(_urlSetM0TopicDRC, _data_drc_topicroot);

                        setActiveView("view_CreateRoot", 0);
                        setFormData(fvform_Insert_Root, FormViewMode.ReadOnly, null);
                        //fvform_Insert_Root.Visible = true;
                        btnAddRootSet.Visible = true;

                        SelectRootSetnane();
                        SETFOCUS.Focus();
                        break;

                    case "2":

                        TextBox txt_tidx_root_lv = (TextBox)fvform_root.FindControl("txt_tidx_root_lv");
                        TextBox txt_topic_name_set_lv = (TextBox)fvform_root.FindControl("txt_topic_name_set_lv");
                        DropDownList ddlroot_topicstatus_lv = (DropDownList)fvform_root.FindControl("ddlroot_topicstatus_lv");
                        DropDownList ddlsettopic_lv = (DropDownList)fvform_root.FindControl("ddlsettopic_lv");
                        DropDownList ddloption_lv = (DropDownList)fvform_root.FindControl("ddloption_lv");

                        data_drc _data_drc_topic_sub = new data_drc();
                        SetTopicNameDetail m0_settopicname_sub = new SetTopicNameDetail();
                        _data_drc_topic_sub.BoxTopicNameList = new SetTopicNameDetail[1];

                        m0_settopicname_sub.root_idx = int.Parse(txt_tidx_root_lv.Text);
                        m0_settopicname_sub.topic_name = txt_topic_name_set_lv.Text;
                        //m0_settopicname_root.set_name = txt_statusname_set.Text;
                        m0_settopicname_sub.topic_status = int.Parse(ddlroot_topicstatus_lv.SelectedValue);
                        m0_settopicname_sub.sidx = int.Parse(ddlsettopic_lv.SelectedValue);
                        m0_settopicname_sub.OPIDX = int.Parse(ddloption_lv.SelectedValue);
                        m0_settopicname_sub.cempidx = _emp_idx;

                        _data_drc_topic_sub.BoxTopicNameList[0] = m0_settopicname_sub;

                        _data_drc_topic_sub = callServicePostDRC(_urlSetM0TopicDRC, _data_drc_topic_sub);

                        setActiveView("view_CreateSetRoot", 0);
                        setFormData(fvform_root, FormViewMode.ReadOnly, null);

                        btnAddSetRootTopicname.Visible = true;

                        SelectRootSetnane_Sub();
                        SETFOCUS.Focus();


                        break;

                }
                break;

            case "cmdDeleteSetTopicname":

                int tidx_del = int.Parse(cmdArg);

                data_drc _data_drc_topic_del = new data_drc();
                SetTopicNameDetail m0_topic_del = new SetTopicNameDetail();
                _data_drc_topic_del.BoxTopicNameList = new SetTopicNameDetail[1];

                m0_topic_del.tidx = tidx_del;
                m0_topic_del.cempidx = _emp_idx;

                _data_drc_topic_del.BoxTopicNameList[0] = m0_topic_del;
                _data_drc_topic_del = callServicePostDRC(_urlSetDelM0TopicDRC, _data_drc_topic_del);

                FvInsertTopicName.Visible = false;
                btn_addsetname.Visible = true;
                SelectTopicName();
                SETFOCUS.Focus();

                break;

            case "btnTodeleteTopicRoot":
                int sidx_root_del = int.Parse(cmdArg);

                data_drc _data_drc_topicroot_del = new data_drc();
                SetTopicNameDetail m0_topicroot_del = new SetTopicNameDetail();
                _data_drc_topicroot_del.BoxTopicNameList = new SetTopicNameDetail[1];

                m0_topicroot_del.tidx = sidx_root_del;
                m0_topicroot_del.cempidx = _emp_idx;

                _data_drc_topicroot_del.BoxTopicNameList[0] = m0_topicroot_del;
                _data_drc_topicroot_del = callServicePostDRC(_urlSetDelM0TopicDRC, _data_drc_topicroot_del);

                fvform_Insert_Root.Visible = false;
                btnAddRootSet.Visible = true;
                SelectRootSetnane();
                SETFOCUS.Focus();
                break;
            case "btnTodeleteSubTopicRoot":
                int sidx_rootsub_del = int.Parse(cmdArg);

                data_drc _data_drc_topicroot_subdel = new data_drc();
                SetTopicNameDetail m0_topicroot_subdel = new SetTopicNameDetail();
                _data_drc_topicroot_subdel.BoxTopicNameList = new SetTopicNameDetail[1];

                m0_topicroot_subdel.tidx = sidx_rootsub_del;
                m0_topicroot_subdel.cempidx = _emp_idx;

                _data_drc_topicroot_subdel.BoxTopicNameList[0] = m0_topicroot_subdel;
                _data_drc_topicroot_subdel = callServicePostDRC(_urlSetDelM0TopicDRC, _data_drc_topicroot_subdel);

                fvform_root.Visible = false;
                btnAddSetRootTopicname.Visible = true;
                SelectRootSetnane_Sub();

                SETFOCUS.Focus();
                break;

            case "cmdViewSetTopicname":

                GvRootSetTopicName.EditIndex = -1;
                btnAddRootSet.Visible = true;

                string[] arg2 = new string[2];
                arg2 = e.CommandArgument.ToString().Split(';');
                int setname_sidx = int.Parse(arg2[0]);
                string setname_ = arg2[1];

                ViewState["vs_setname_sidx"] = setname_sidx;
                ViewState["vs_setname"] = setname_;

                setActiveView("view_CreateRoot", setname_sidx);
                setFormData(fvform_Insert_Root, FormViewMode.ReadOnly, null);
                SelectRootSetnane();
                SETFOCUS.Focus();
                break;

            case "cmdViewSetTopicnameRoot":

                GvRootLevel.EditIndex = -1;

                btnAddSetRootTopicname.Visible = true;

                string[] arg3 = new string[2];
                arg3 = e.CommandArgument.ToString().Split(';');
                int setname_sidx_sub = int.Parse(arg3[0]);
                string setname_sup_ = arg3[1];

                ViewState["vs_setname_sidx_sub"] = setname_sidx_sub;
                ViewState["vs_setname_sub"] = setname_sup_;

                setActiveView("view_CreateSetRoot", setname_sidx_sub);
                setFormData(fvform_root, FormViewMode.ReadOnly, null);
                SelectRootSetnane_Sub();
                SETFOCUS.Focus();
                break;
        }

    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvTopic":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_topic_status = (Label)e.Row.FindControl("lbl_topic_status");
                    Label lbl_topic_statussOpen = (Label)e.Row.FindControl("lbl_topic_statussOpen");
                    Label lbl_topic_statusClose = (Label)e.Row.FindControl("lbl_topic_statusClose");

                    ViewState["vs_Topicname_status"] = lbl_topic_status.Text;


                    if (ViewState["vs_Topicname_status"].ToString() == "1")
                    {
                        lbl_topic_statussOpen.Visible = true;
                    }
                    else if (ViewState["vs_setname_status"].ToString() == "0")
                    {
                        lbl_topic_statusClose.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    btn_addsetname.Visible = true;
                    FvInsertTopicName.Visible = false;

                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);
                }
                break;

            case "GvRootSetTopicName":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_topic_status_root = (Label)e.Row.FindControl("lbl_topic_status_root");
                    Label lbl_topic_status_root_Online = (Label)e.Row.FindControl("lbl_topic_status_root_Online");
                    Label lbl_topic_status_root_Offline = (Label)e.Row.FindControl("lbl_topic_status_root_Offline");



                    ViewState["_lbtestDetail_status_roottopic"] = lbl_topic_status_root.Text;


                    if (ViewState["_lbtestDetail_status_roottopic"].ToString() == "1")
                    {
                        lbl_topic_status_root_Online.Visible = true;
                    }
                    else if (ViewState["_lbtestDetail_status_roottopic"].ToString() == "0")
                    {
                        lbl_topic_status_root_Offline.Visible = true;
                    }

                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";


                    var txt_sidx_edit_root = (TextBox)e.Row.FindControl("txt_sidx_edit_root");
                    var ddlsettopicsidxedit = (DropDownList)e.Row.FindControl("ddlsettopicsidxedit");
                    var txt_OPIDX_edit = (TextBox)e.Row.FindControl("txt_OPIDX_edit");
                    var ddloptionedit = (DropDownList)e.Row.FindControl("ddloptionedit");

                    ddlsettopicsidxedit.AppendDataBoundItems = true;

                    data_drc _data_drc_settopic = new data_drc();
                    SetNameDetail m0_set_topic = new SetNameDetail();
                    _data_drc_settopic.BoxM0SetNameList = new SetNameDetail[1];


                    _data_drc_settopic.BoxM0SetNameList[0] = m0_set_topic;

                    _data_drc_settopic = callServicePostDRC(_urlGetM0SetnameDropDrowDRC, _data_drc_settopic);

                    ddlsettopicsidxedit.DataSource = _data_drc_settopic.BoxM0SetNameList;
                    ddlsettopicsidxedit.DataTextField = "set_name";
                    ddlsettopicsidxedit.DataValueField = "sidx";
                    ddlsettopicsidxedit.DataBind();
                    ddlsettopicsidxedit.Items.Insert(0, new ListItem("เลือกชุดข้อมูล ....", "00"));
                    ddlsettopicsidxedit.SelectedValue = txt_sidx_edit_root.Text;


                    //
                    ddloptionedit.AppendDataBoundItems = true;
                   

                    data_drc _data_drc_option = new data_drc();
                    SetOptionDetail m0_option = new SetOptionDetail();
                    _data_drc_option.BoxOptionList = new SetOptionDetail[1];

                    _data_drc_option.BoxOptionList[0] = m0_option;

                    _data_drc_option = callServicePostDRC(_urlGetM0OptionnameDropDrowDRC, _data_drc_option);

                    ddloptionedit.DataSource = _data_drc_option.BoxOptionList;
                    ddloptionedit.DataTextField = "Option_name";
                    ddloptionedit.DataValueField = "OPIDX";
                    ddloptionedit.DataBind();
                    ddloptionedit.Items.Insert(0, new ListItem("เลือกการกรอกข้อมูล ....", "00"));
                    //ddloptionedit.Items.Add(0,new ListItem("เลือกการกรอกข้อมูล", "00"));
                    ddloptionedit.SelectedValue = txt_OPIDX_edit.Text;


                    btnAddRootSet.Visible = true;
                    fvform_Insert_Root.Visible = false;
                    //setFormData(fvform_Insert_Root, FormViewMode.ReadOnly, null);
                }

                break;
            case "GvRootLevel":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_topic_status_sub = (Label)e.Row.FindControl("lbl_topic_status_sub");
                    Label lbl_topic_status_sub_Online = (Label)e.Row.FindControl("lbl_topic_status_sub_Online");
                    Label lbl_topic_status_sub_Offline = (Label)e.Row.FindControl("lbl_topic_status_sub_Offline");

                    ViewState["_lbtestDetail_status_subtopic"] = lbl_topic_status_sub.Text;


                    if (ViewState["_lbtestDetail_status_subtopic"].ToString() == "1")
                    {
                        lbl_topic_status_sub_Online.Visible = true;
                    }
                    else if (ViewState["_lbtestDetail_status_subtopic"].ToString() == "0")
                    {
                        lbl_topic_status_sub_Offline.Visible = true;
                    }

                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";


                    var txt_sidx_edit_root_sub = (TextBox)e.Row.FindControl("txt_sidx_edit_root_sub");
                    var ddlsettopicsidxedit_sub = (DropDownList)e.Row.FindControl("ddlsettopicsidxedit_sub");
                    var txt_OPIDX_edit_sub = (TextBox)e.Row.FindControl("txt_OPIDX_edit_sub");
                    var ddloptionedit_sub = (DropDownList)e.Row.FindControl("ddloptionedit_sub");

                    ddlsettopicsidxedit_sub.AppendDataBoundItems = true;

                    data_drc _data_drc_settopic = new data_drc();
                    SetNameDetail m0_set_topic = new SetNameDetail();
                    _data_drc_settopic.BoxM0SetNameList = new SetNameDetail[1];


                    _data_drc_settopic.BoxM0SetNameList[0] = m0_set_topic;

                    _data_drc_settopic = callServicePostDRC(_urlGetM0SetnameDropDrowDRC, _data_drc_settopic);

                    ddlsettopicsidxedit_sub.DataSource = _data_drc_settopic.BoxM0SetNameList;
                    ddlsettopicsidxedit_sub.DataTextField = "set_name";
                    ddlsettopicsidxedit_sub.DataValueField = "sidx";
                    ddlsettopicsidxedit_sub.DataBind();
                    ddlsettopicsidxedit_sub.Items.Insert(0, new ListItem("เลือกชุดข้อมูล ....", "00"));
                    ddlsettopicsidxedit_sub.SelectedValue = txt_sidx_edit_root_sub.Text;


                    //
                    ddloptionedit_sub.AppendDataBoundItems = true;


                    data_drc _data_drc_option = new data_drc();
                    SetOptionDetail m0_option = new SetOptionDetail();
                    _data_drc_option.BoxOptionList = new SetOptionDetail[1];

                    _data_drc_option.BoxOptionList[0] = m0_option;

                    _data_drc_option = callServicePostDRC(_urlGetM0OptionnameDropDrowDRC, _data_drc_option);

                    ddloptionedit_sub.DataSource = _data_drc_option.BoxOptionList;
                    ddloptionedit_sub.DataTextField = "Option_name";
                    ddloptionedit_sub.DataValueField = "OPIDX";
                    ddloptionedit_sub.DataBind();
                    ddloptionedit_sub.Items.Insert(0, new ListItem("เลือกการกรอกข้อมูล ....", "00"));
                    //ddloptionedit.Items.Add(0,new ListItem("เลือกการกรอกข้อมูล", "00"));
                    ddloptionedit_sub.SelectedValue = txt_OPIDX_edit_sub.Text;


                    btnAddSetRootTopicname.Visible = true;
                    fvform_root.Visible = false;
                    //setFormData(fvform_Insert_Root, FormViewMode.ReadOnly, null);
                }

                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvTopic":
                GvTopic.EditIndex = e.NewEditIndex;
                SelectTopicName();
                break;
            case "GvRootSetTopicName":
                GvRootSetTopicName.EditIndex = e.NewEditIndex;
                SelectRootSetnane();
                break;
            case "GvRootLevel":
                GvRootLevel.EditIndex = e.NewEditIndex;
                SelectRootSetnane_Sub();
                break;
        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvTopic":

                var txt_tidx_edit = (TextBox)GvTopic.Rows[e.RowIndex].FindControl("txt_tidx_edit");
                var txt_topic_name_edit = (TextBox)GvTopic.Rows[e.RowIndex].FindControl("txt_topic_name_edit");
                var dd_topic_status_edit = (DropDownList)GvTopic.Rows[e.RowIndex].FindControl("dd_topic_status_edit");


                GvTopic.EditIndex = -1;

                data_drc _data_drc_edit = new data_drc();
                SetTopicNameDetail m0_topic_edit = new SetTopicNameDetail();
                _data_drc_edit.BoxTopicNameList = new SetTopicNameDetail[1];

                m0_topic_edit.tidx = int.Parse(txt_tidx_edit.Text);
                m0_topic_edit.topic_name = txt_topic_name_edit.Text;
                m0_topic_edit.topic_status = int.Parse(dd_topic_status_edit.SelectedValue);
                m0_topic_edit.cempidx = _emp_idx;

                _data_drc_edit.BoxTopicNameList[0] = m0_topic_edit;

                _data_drc_edit = callServicePostDRC(_urlSetUpdateM0TopicDRC, _data_drc_edit);


                SelectTopicName();

                break;

            case "GvRootSetTopicName":

                var lblUIDX_edit = (Label)GvRootSetTopicName.Rows[e.RowIndex].FindControl("lblUIDX_edit");
                var txt_topic_name_edit_root = (TextBox)GvRootSetTopicName.Rows[e.RowIndex].FindControl("txt_topic_name_edit");
                var ddltopic_status_edit_root = (DropDownList)GvRootSetTopicName.Rows[e.RowIndex].FindControl("ddltopic_status_edit");
                var ddlsettopicsidxedit = (DropDownList)GvRootSetTopicName.Rows[e.RowIndex].FindControl("ddlsettopicsidxedit");
                var ddloptionedit = (DropDownList)GvRootSetTopicName.Rows[e.RowIndex].FindControl("ddloptionedit");

                GvRootSetTopicName.EditIndex = -1;


                data_drc _data_drc_edit_root = new data_drc();
                SetTopicNameDetail m0_topic_edit_root = new SetTopicNameDetail();
                _data_drc_edit_root.BoxTopicNameList = new SetTopicNameDetail[1];

                m0_topic_edit_root.tidx = int.Parse(lblUIDX_edit.Text);
                m0_topic_edit_root.topic_name = txt_topic_name_edit_root.Text;
                m0_topic_edit_root.topic_status = int.Parse(ddltopic_status_edit_root.SelectedValue);
                m0_topic_edit_root.cempidx = _emp_idx;
                m0_topic_edit_root.sidx = int.Parse(ddlsettopicsidxedit.SelectedValue);
                m0_topic_edit_root.OPIDX = int.Parse(ddloptionedit.SelectedValue);


                _data_drc_edit_root.BoxTopicNameList[0] = m0_topic_edit_root;


                //test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_drc_edit_root));
                _data_drc_edit_root = callServicePostDRC(_urlSetUpdateM0TopicDRC, _data_drc_edit_root);

                SelectRootSetnane();


                break;
            case "GvRootLevel":

                var lblUIDX_edit_root = (Label)GvRootLevel.Rows[e.RowIndex].FindControl("lblUIDX_edit_root");
                var txt_topic_name_edit_root_sub = (TextBox)GvRootLevel.Rows[e.RowIndex].FindControl("txt_topic_name_edit_root");
                var ddltopic_status_edit_root_sub = (DropDownList)GvRootLevel.Rows[e.RowIndex].FindControl("ddltopic_status_edit_root");
                var ddlsettopicsidxedit_sub = (DropDownList)GvRootLevel.Rows[e.RowIndex].FindControl("ddlsettopicsidxedit_sub");
                var ddloptionedit_sub = (DropDownList)GvRootLevel.Rows[e.RowIndex].FindControl("ddloptionedit_sub");

                GvRootLevel.EditIndex = -1;


                data_drc _data_drc_edit_root_sub = new data_drc();
                SetTopicNameDetail m0_topic_edit_root_sub = new SetTopicNameDetail();
                _data_drc_edit_root_sub.BoxTopicNameList = new SetTopicNameDetail[1];

                m0_topic_edit_root_sub.tidx = int.Parse(lblUIDX_edit_root.Text);
                m0_topic_edit_root_sub.topic_name = txt_topic_name_edit_root_sub.Text;
                m0_topic_edit_root_sub.topic_status = int.Parse(ddltopic_status_edit_root_sub.SelectedValue);
                m0_topic_edit_root_sub.cempidx = _emp_idx;
                m0_topic_edit_root_sub.sidx = int.Parse(ddlsettopicsidxedit_sub.SelectedValue);
                m0_topic_edit_root_sub.OPIDX = int.Parse(ddloptionedit_sub.SelectedValue);


                _data_drc_edit_root_sub.BoxTopicNameList[0] = m0_topic_edit_root_sub;


                //test_lab.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_drc_edit_root));
                _data_drc_edit_root_sub = callServicePostDRC(_urlSetUpdateM0TopicDRC, _data_drc_edit_root_sub);

                SelectRootSetnane_Sub();


                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvTopic":
                GvTopic.EditIndex = -1;
                SelectTopicName();
                btn_addsetname.Visible = true;
                break;
            case "GvRootSetTopicName":
                GvRootSetTopicName.EditIndex = -1;
                SelectRootSetnane();
                btnAddRootSet.Visible = true;
                break;
            case "GvRootLevel":
                GvRootLevel.EditIndex = -1;
                SelectRootSetnane_Sub();
                btnAddSetRootTopicname.Visible = true;
                break;
        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvTopic":
                GvTopic.PageIndex = e.NewPageIndex;
                GvTopic.DataBind();
                SelectTopicName();
                break;

            case "GvRootSetTopicName":
                GvRootSetTopicName.PageIndex = e.NewPageIndex;
                GvRootSetTopicName.DataBind();
                SelectRootSetnane();
                break;
            case "GvRootLevel":
                GvRootLevel.PageIndex = e.NewPageIndex;
                GvRootLevel.DataBind();
                SelectRootSetnane_Sub();
                break;
        }
    }
    #endregion

    #region setActiveView

    protected void setActiveView(string activeTab, int uidx)
    {
        MvMaster_lab.SetActiveView((View)MvMaster_lab.FindControl(activeTab));
        FvInsertTopicName.Visible = false;
        //fvform_Insert_Root.Visible = false;

        switch (activeTab)
        {
            case "view_Genaral":

                break;

            case "view_CreateRoot":
                break;

            case "view_CreateSetRoot":

                break;
        }
    }

    #endregion
}