﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="hr_perf_evalu_emp_profile_print.aspx.cs" Inherits="websystem_hr_perf_evalu_emp_profile_print" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <%-- <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />--%>
    <link href="../../Content/bootstrap.css" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />

    <title>Print</title>

    <%--<style type="text/css" media="print,screen">
        @page {
            size: A4 landscape;
            margin: 25px;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
            }
        }

        .formPrint {
            margin: 10pt; /*from 5 to 10*/
            padding: 10pt; /*add*/
            width: 842pt;
        }
    </style>--%>
</head>
<%-- <body onload="window.print()">--%>
<body onload="window.print()">

    <form id="form1" runat="server" width="100%">
        <script src='<%=ResolveUrl("~/Scripts/jquery-3.1.1.min.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/bootstrap.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/custom.js") %>'></script>
        <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <div class="formPrint">



            <asp:FormView ID="FvProfile" DefaultMode="ReadOnly" runat="server" Width="100%">
                <ItemTemplate>

                    <div class="col-lg-12">

                        <div class="form-horizontal" role="form">

                            <div class="col-lg-12">
                                <br />
                            </div>

                            <div class="row">

                                <div class="col-md-12">

                                    <div style="border: none;">


                                        <table style="width: 100%; border: none;">
                                            <tr>
                                                <td align="left" valign="top" style="border: none;">
                                                    <asp:Image ID="images" runat="server" Width="100px" Height="100px" ImageUrl='<%# ResolveUrl("~/images/hr_perf_evalu_emp_profile/logo_tkn.png") %>' />
                                                </td>
                                                <td align="right" valign="top">
                                                    <div style="font-size: medium;" class="h1">
                                                        <b>บริษัท เถ้าแก่น้อย ฟู๊ดแอนด์มาร์เก็ตติ้ง จำกัด (มหาชน)
                                                        </b>
                                                    </div>
                                                    <div style="font-size: medium;" class="h3">
                                                        <asp:Label ID="Label3" CssClass="control-labelnotop text_right"
                                                            runat="server" Text="แบบประเมินผลการปฏิบัติงานประจำปี : " />
                                                        <asp:Label ID="lb_year" CssClass="control-labelnotop text_left" runat="server" Text='' />
                                                        <asp:TextBox ID="txt_year"  runat="server" Visible="false" Text='<%# Eval("zyear") %>'>
                                                        </asp:TextBox>
                                                    </div>
                                                    <div style="font-size: medium;" class="form-inline h3">
                                                        <asp:Label ID="Label2" CssClass="control-labelnotop text_right"
                                                            runat="server" Text="เกณฑ์การประเมิน : " />

                                                        <asp:TextBox ID="lb_pms_name" CssClass="text-left" Width="150px" runat="server" Text='' ReadOnly="true" />
                                                        <asp:TextBox ID="lb_performane_name" CssClass="text-left" Width="150px" runat="server" Text='' ReadOnly="true"  />

                                                    </div>
                                                </td>
                                            </tr>

                                        </table>

                                    </div>

                                </div>

                            </div>


                            <div class="row">
                                <div class="col-lg-12">

                                    <table style="width: 100%; background-color: white; padding: 0 0;">



                                        <tr>
                                            <td style="text-align: right; border: none; width: 100px;">
                                                <asp:Label ID="Label29" runat="server">
                                                    รหัสพนักงาน
                                                </asp:Label>
                                            </td>
                                            <td style="text-align: left; width: 30px; border: none;">
                                                <asp:TextBox ID="TextBox3" Width="100" runat="server" Text='<%# Eval("emp_code") %>'  ReadOnly="true" >
                                                </asp:TextBox>
                                            </td>
                                            <td style="text-align: left; border: none; width: 20px;">
                                                <asp:Label ID="Label30" runat="server">
                                
                                                </asp:Label>
                                            </td>
                                            <td style="text-align: right; border: none; width: 100px;">
                                                <asp:Label ID="Label45" runat="server">
                                                      ชื่อ-นามสกุล
                                                </asp:Label>
                                            </td>
                                            <td style="text-align: left; width: 50px; border: none;">
                                                <asp:TextBox ID="TextBox4" runat="server" Width="200" Text='<%# Eval("emp_name_th") %>'  ReadOnly="true" >
                                
                                                </asp:TextBox>
                                            </td>
                                            <td style="text-align: left; border: none; width: 20px;">
                                                <asp:Label ID="Label46" runat="server">
                                
                                                </asp:Label>
                                            </td>
                                            <td style="text-align: right; border: none; width: 70px;">
                                                <asp:Label ID="Label55" runat="server">
                                                     ตำแหน่ง
                                                </asp:Label>
                                            </td>
                                            <td style="text-align: left; width: 50px; border: none;">
                                                <asp:TextBox ID="TextBox8" runat="server" Width="200" Text='<%# Eval("pos_name_th") %>'  ReadOnly="true" >
                                
                                                </asp:TextBox>
                                            </td>
                                            <td style="text-align: left; border: none;">
                                                <asp:Label ID="Label56" runat="server">
                                
                                                </asp:Label>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: left; border: none; height: 7px;" colspan="9">
                                                <asp:Label ID="Label51" runat="server">
                                 
                                                </asp:Label>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: right; border: none;">
                                                <asp:Label ID="Label47" runat="server">
                                                           วันเริ่มงาน
                                                </asp:Label>
                                            </td>
                                            <td style="border: none;">
                                                <asp:TextBox ID="TextBox5" Width="100" runat="server" Text='<%# Eval("emp_probation_date") %>'  ReadOnly="true" >
                                                </asp:TextBox>
                                            </td>
                                            <td style="border: none;">
                                                <asp:Label ID="Label48" runat="server">
                                
                                                </asp:Label>
                                            </td>
                                            <td style="text-align: right; border: none;">
                                                <asp:Label ID="Label49" runat="server">
                                                    หน่วยงาน
                                                </asp:Label>
                                            </td>
                                            <td style="border: none;">
                                                <asp:TextBox ID="TextBox6" Width="200" runat="server" Text='<%# Eval("dept_name_th") %>'  ReadOnly="true" >
                                
                                                </asp:TextBox>
                                            </td>
                                            <td style="border: none;">
                                                <asp:Label ID="Label50" runat="server">
                               
                                                </asp:Label>
                                            </td>

                                            <td style="text-align: right; border: none;">
                                                <asp:Label ID="Label53" runat="server">
                                                     อายุงาน
                                                </asp:Label>
                                            </td>
                                            <td style="border: none;">
                                                <asp:TextBox ID="TextBox7" Width="200" runat="server" Text='<%# Eval("work_experience") %>'  ReadOnly="true" >
                                
                                                </asp:TextBox>
                                            </td>
                                            <td style="border: none;">
                                                <asp:Label ID="Label54" runat="server">
                                
                                                </asp:Label>
                                            </td>


                                        </tr>
                                        <tr>
                                            <td style="text-align: left; border: none; height: 7px;" colspan="9">
                                                <asp:Label ID="Label52" runat="server">
                                 
                                                </asp:Label>
                                            </td>
                                        </tr>

                                    </table>


                                    <asp:Label ID="LbEmpidx" Visible="false" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("emp_idx") %>' />

                                </div>
                            </div>



                        </div>
                    </div>
                </ItemTemplate>

            </asp:FormView>


            <div class="col-lg-12 word-wrap">
                <br />
                <div style="background-color: white; text-align: left;">
                    <h3 class="panel-title ">ผลการปฏิบัติงานตาม KPI (Performance Appraisal)</h3>
                </div>
                <br />
                <%-- <div class="col-lg-12">--%>
                <asp:GridView ID="gvprofileKPI"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-bordered table-hover table-responsive col-lg-12 word-wrap"
                    HeaderStyle-CssClass="info"
                    RowStyle-BackColor="White"
                    GridLines="None"
                    OnRowDataBound="Master_RowDataBound"
                    OnRowCreated="grvMergeHeader_RowCreated">

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Font-Size="9"
                            ControlStyle-Font-Size="10"
                            HeaderStyle-Width="13%">
                            <ItemTemplate>

                                <asp:Label ID="lb_ItemIndex" runat="server" Visible="false"
                                    Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>


                                <asp:Label ID="lb_data_name" runat="server" CssClass="col-sm-12"
                                    Text='<%# Eval("data_name") %>'>
                                                                  
                                </asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Corporate KPI" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Font-Size="9"
                            ControlStyle-Font-Size="10"
                            HeaderStyle-Width="5%">
                            <ItemTemplate>

                                <asp:Label ID="lb_corp_kpi" runat="server" CssClass="col-sm-12"
                                    Text='<%# Eval("corp_kpi") %>'>
                                                                  
                                </asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="TAOKAE-D" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Font-Size="9"
                            ControlStyle-Font-Size="10"
                            HeaderStyle-Width="5%">
                            <ItemTemplate>

                                <asp:Label ID="lb_taokae_d" runat="server" CssClass="col-sm-12"
                                    Text='<%# Eval("taokae_d") %>'>
                                </asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="TKN Value" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Font-Size="9"
                            ControlStyle-Font-Size="10"
                            HeaderStyle-Width="5%">
                            <ItemTemplate>

                                <asp:Label ID="lb_tkn_value" runat="server" CssClass="col-sm-12"
                                    Text='<%# Eval("tkn_value") %>'>
                                                                  
                                </asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สาย ขาดงาน" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Font-Size="9"
                            ControlStyle-Font-Size="10"
                            HeaderStyle-Width="5%">
                            <ItemTemplate>

                                <asp:Label ID="lb_absence" runat="server" CssClass="col-sm-12"
                                    Text='<%# Eval("absence") %>'>
                                                                  
                                </asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="TOTAL" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Font-Size="9"
                            ControlStyle-Font-Size="10">
                            <ItemTemplate>

                                <asp:Label ID="lb_total_deduction" runat="server"
                                    Text='<%# Eval("total_deduction") %>' />

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Font-Size="9"
                            ControlStyle-Font-Size="10"
                            HeaderStyle-Width="10%">
                            <ItemTemplate>

                                <div style="text-align: center;">
                                    <asp:Label ID="lb_punishment1" runat="server"
                                        CssClass="col-sm-12"
                                        Text='<%# Eval("punishment1") %>'>
                                                                  
                                    </asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Font-Size="9"
                            ControlStyle-Font-Size="10"
                            HeaderStyle-Width="5%">
                            <ItemTemplate>

                                <div style="text-align: center;">
                                    <asp:Label ID="lb_punishment2"
                                        runat="server" CssClass="col-sm-12"
                                        Text='<%# Eval("punishment2") %>'>
                                                                  
                                    </asp:Label>
                                </div>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-Font-Size="9"
                            ControlStyle-Font-Size="10">
                            <ItemTemplate>
                                <b>
                                    <asp:Label ID="lb_grade" runat="server"
                                        Font-Bold="true"
                                        Font-Size="Medium"
                                        Text='<%# Eval("grade") %>' />
                                </b>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-Font-Size="9"
                            ControlStyle-Font-Size="10">
                            <ItemTemplate>

                                <asp:Label ID="lb_grade_name" runat="server"
                                    Text='<%# Eval("grade_name") %>' />

                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

            </div>

            <div class="col-lg-12">

                <table style="width: 100%; padding: 0 0;">

                    <tr>
                        <td style="text-align: left; border: none;">
                            <asp:Label ID="Label18" runat="server"
                                Text="A - OUTSTANDING มีผลการปฏิบัติงานโดดเด่นอย่างชัดเจน 96-100 %"
                                ForeColor="Green"
                                Font-Size="Small">
                            </asp:Label>
                        </td>
                        <td style="text-align: left; border: none;">
                            <asp:Label ID="Label13" runat="server"
                                Text="B - EXCEEDS EXPECTATIONS เกินกว่าที่คาดหวัง 81-95%"
                                ForeColor="Green"
                                Font-Size="Small">
                            </asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td style="text-align: left; border: none;">
                            <asp:Label ID="Label14" runat="server"
                                Text="C - MEET EXPECTATIONS เป็นไปตามความคาดหวัง 61-80 %"
                                ForeColor="Green"
                                Font-Size="Small">
                            </asp:Label>
                        </td>
                        <td style="text-align: left; border: none;">
                            <asp:Label ID="Label15" runat="server"
                                Text="D - IMPROVEMENT NEEDED ต้องปรับปรุง 50-60%"
                                ForeColor="#cc9900"
                                Font-Size="Small">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; border: none;" colspan="2">
                            <asp:Label ID="Label16" runat="server"
                                Text="E - CONSISTENLY BELOW EXPECTATIONS ไม่เป็นไปตามที่คาดหวังอย่างต่อเนื่อง ต่ำกว่า 49%"
                                ForeColor="Red"
                                Font-Size="Small">
                            </asp:Label>
                        </td>
                    </tr>

                </table>

            </div>

            <div class="col-lg-12">
                <br />
            </div>


            <div class="col-lg-12">
                <table style="width: 100%; border: 1px solid #ddd !important;">

                    <tr>
                        <td style="text-align: left; border: 1px solid #ddd !important;">
                            <asp:Label ID="Label23" runat="server">
                                 <p><b>ข้อคิดเห็น</b></p>  
                            </asp:Label>
                        </td>


                    </tr>

                    <tr>
                        <td style="text-align: center; border: 1px solid #ddd !important;">
                            <asp:Label ID="Label26" runat="server">
                                <br />
                            </asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td style="text-align: center; border: 1px solid #ddd !important;">
                            <asp:Label ID="Label24" runat="server">
                                <br />
                            </asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td style="text-align: center; border: 1px solid #ddd !important;">
                            <asp:Label ID="Label25" runat="server">
                                <br />
                            </asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td style="text-align: center; border: 1px solid #ddd !important;">
                            <asp:Label ID="Label27" runat="server">
                                <br />
                            </asp:Label>
                        </td>
                    </tr>

                </table>
            </div>
            <div class="col-lg-12">
                <br />
            </div>



            <div class="col-lg-12">

                <div style="border: 1px solid #ddd !important;">
                    <table style="width: 100%; background-color: white; padding: 0 0;">

                        <tr>
                            <td style="text-align: left; border: none;" colspan="6">
                                <asp:Label ID="Label31" runat="server">
                                 <p><b>โทษทางวินัย</b></p>  
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align: right; border: none; width: 100px;">
                                <asp:Label ID="Label32" runat="server">
                                หนังสือเตือน
                                </asp:Label>
                            </td>
                            <td style="text-align: left; width: 30px; border: none;">
                                <asp:TextBox ID="lb_punishment1_1" Width="100" runat="server" CssClass="text-center" Text="-"  ReadOnly="true" >
                                </asp:TextBox>
                            </td>
                            <td style="text-align: left; border: none; width: 100px;">
                                <asp:Label ID="Label37" runat="server">
                                เรื่อง
                                </asp:Label>
                            </td>
                            <td style="text-align: right; border: none; width: 70px;">
                                <asp:Label ID="Label38" runat="server">
                                <br />
                                </asp:Label>
                            </td>
                            <td style="text-align: left; width: 50px; border: none;">
                                <asp:TextBox ID="txt_punishment1" runat="server"  Width="100" Text="-"   CssClass="text-center"  ReadOnly="true"  >
                                
                                </asp:TextBox>
                            </td>
                            <td style="text-align: left; border: none;">
                                <asp:Label ID="Label40" runat="server">
                                ครั้ง (ครั้งละ 10 คะแนน)*เกิน 3 ครั้งขึ้นไปไม่ปรับเงินประจำปี
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align: left; border: none; height: 7px;" colspan="6">
                                <asp:Label ID="Label20" runat="server">
                                 
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align: right; border: none;">
                                <asp:Label ID="Label41" runat="server">
                                พักงาน
                                </asp:Label>
                            </td>
                            <td style="border: none;">
                                <asp:TextBox ID="txt_punishment2" Width="100" runat="server" Text="-"  CssClass="text-center"  ReadOnly="true"  >
                                </asp:TextBox>
                            </td>
                            <td style="border: none;" colspan="3">
                                <asp:Label ID="Label42" runat="server">
                                ครั้งไม่ปรับเงินประจำปี
                                </asp:Label>
                            </td>
                           <%-- <td style="text-align: right; border: none;">
                                <asp:Label ID="Label43" runat="server">
                               พักงาน
                                </asp:Label>
                            </td>
                            <td style="border: none;">
                                <asp:TextBox ID="TextBox2" runat="server">
                                
                                </asp:TextBox>
                            </td>
                            <td style="border: none;">
                                <asp:Label ID="Label44" runat="server">
                                ครั้งไม่ปรับเงินประจำปี
                                </asp:Label>
                            </td>--%>
                        </tr>
                        <tr>
                            <td style="text-align: left; border: none; height: 7px;" colspan="6">
                                <asp:Label ID="Label12" runat="server">
                                 
                                </asp:Label>
                            </td>
                        </tr>

                    </table>
                </div>

            </div>

            <div class="col-lg-12">
                <br />
            </div>

            <div class="col-lg-12">

                <div style="border: 1px solid #ddd !important;">
                    <table style="width: 100%; background-color: white; padding: 0 0;">

                        <tr>
                            <td style="text-align: left; border: none;" colspan="6">
                                <asp:Label ID="Label28" runat="server">
                                 <p><b>สถิติมาทำงาน</b></p>  
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align: right; border: none; width: 100px;">
                                <asp:Label ID="Label29" runat="server">
                                สาย / กลับก่อน
                                </asp:Label>
                            </td>
                            <td style="text-align: left; width: 30px; border: none;">
                                <asp:TextBox ID="txt_late_mm_1" Width="100" runat="server" Text="-"  CssClass="text-center"  ReadOnly="true"  >
                                </asp:TextBox>
                            </td>
                            <td style="text-align: left; border: none; width: 100px;">
                                <asp:Label ID="Label30" runat="server">
                                ครั้ง
                                </asp:Label>
                            </td>
                            <td style="text-align: right; border: none; width: 70px;">
                                <asp:Label ID="Label45" runat="server">
                                <br />
                                </asp:Label>
                            </td>
                            <td style="text-align: left; width: 50px; border: none;">
                                <asp:TextBox ID="txt_late_mm" runat="server"  Width="100" Text="-"  CssClass="text-center"  ReadOnly="true"  >
                                
                                </asp:TextBox>
                            </td>
                            <td style="text-align: left; border: none;">
                                <asp:Label ID="Label46" runat="server">
                                นาที (30 นาที หัก 1 คะแนน)
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align: left; border: none; height: 7px;" colspan="6">
                                <asp:Label ID="Label51" runat="server">
                                 
                                </asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td style="text-align: right; border: none;">
                                <asp:Label ID="Label47" runat="server">
                                ขาดงาน
                                </asp:Label>
                            </td>
                            <td style="border: none;">
                                <asp:TextBox ID="TextBox5" Width="100" runat="server" Text="-"  CssClass="text-center"  ReadOnly="true"  >
                                </asp:TextBox>
                            </td>
                            <td style="border: none;">
                                <asp:Label ID="Label48" runat="server">
                                ครั้ง
                                </asp:Label>
                            </td>
                            <td style="text-align: right; border: none;">
                                <asp:Label ID="Label49" runat="server">
                               
                                </asp:Label>
                            </td>
                            <td style="border: none;">
                                <asp:TextBox ID="txt_missing_qty"  Width="100" runat="server" Text="-"  CssClass="text-center"  ReadOnly="true"  >
                                
                                </asp:TextBox>
                            </td>
                            <td style="border: none;">
                                <asp:Label ID="Label50" runat="server">
                                วัน (วันละ 5 คะแนน)
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; border: none; height: 7px;" colspan="6">
                                <asp:Label ID="Label52" runat="server">
                                 
                                </asp:Label>
                            </td>
                        </tr>

                    </table>
                </div>

            </div>

            <div class="col-lg-12">
                <br />
                <br />
                <br />
                <br />
                <br />
                <br />
            </div>

            <div class="col-lg-12">
                <table style="width: 100%;"
                    class="table table-bordered word-wrap">
                    <tr style="text-align: center;">
                        <td colspan="2" style="width: 70%;">ปัจจัยการประเมิน</td>
                        <td>พนักงานประเมินตนเอง</td>
                        <td>ผู้บังคับบัญชาประเมินผ่านระบบ</td>
                    </tr>
                    <tr>
                        <td colspan="4"><b>TKN VALUE ค่านิยมองค์กร</b></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lb_1" runat="server">
                                                 <p><b style="color:blue;">O</b>WNERSHIP</p>  
                            </asp:Label>
                        </td>
                        <td>มุ่งมั่น บรรลุผล จนสำเร็จ</td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_emp_ownership" runat="server">
                            </asp:Label>
                        </td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_leader_ownership" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label3" runat="server">
                                                 <p><b style="color:blue;">I</b>MPROVEMENT CONTINUEOUSLY</p>  
                            </asp:Label>
                        </td>
                        <td>เชี่ยวชาญ ต่อยอด ส่งมอบความพึงพอใจ</td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_emp_improvement_continueously" runat="server">
                            </asp:Label>
                        </td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_leader_improvement_continueously" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label4" runat="server">
                                                 <p><b style="color:blue;">C</b>OMMITMENT</p>  
                            </asp:Label>
                        </td>
                        <td>ตั้งใจ รับผิดชอบ ทุ่มเท</td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_emp_commitment" runat="server">
                            </asp:Label>
                        </td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_leader_commitment" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr style="text-align: right;">
                        <td colspan="2"><b>คะแนนรวม</b></td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_emp_tkn_value" runat="server">
                            </asp:Label>
                        </td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_leader_tkn_value" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4"><b>LEADERSHIP CAPABILITIES ความเป็นผู้นำ</b></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label5" runat="server">
                                                 <p><b style="color:red;">T</b>   INNOVA<b style="color:red;"><u>T</u></b>ION</p>  
                            </asp:Label>
                        </td>
                        <td>บทบาทผู้นำในการสร้างนวัตกรรม</td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_emp_innovation" runat="server">
                            </asp:Label>
                        </td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_leader_innovation" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label6" runat="server">
                                                 <p><b style="color:red;">A</b>   <b style="color:red;"><u>A</u></b>CHIEVING  RESULT</p>  
                            </asp:Label>
                        </td>
                        <td>การมุ่งเน้นผลสำเร็จของงาน</td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_emp_achieving_result" runat="server">
                            </asp:Label>
                        </td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_leader_achieving_result" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label7" runat="server">
                                                 <p><b style="color:red;">O</b>   RELATI<b style="color:red;"><u>O</u></b>NS</p>  
                            </asp:Label>
                        </td>
                        <td>การสร้างความสัมพันธ์ในการทำงาน</td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_emp_relations" runat="server">
                            </asp:Label>
                        </td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_leader_relations" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label8" runat="server">
                                                 <p><b style="color:red;">K</b>   JOB  <b style="color:red;"><u>K</u></b>NOWLEDGE</p>  
                            </asp:Label>
                        </td>
                        <td>ความรู้ความสามารถในงาน</td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_emp_job_knowledge" runat="server">
                            </asp:Label>
                        </td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_leader_job_knowledge" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label9" runat="server">
                                                 <p><b style="color:red;">A</b>   <b style="color:red;"><u>A</u></b>RT OF COMMUNICATION</p>  
                            </asp:Label>
                        </td>
                        <td>สื่อสารอย่างมีศิลปะและจูงใจ</td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_emp_art_communication" runat="server">
                            </asp:Label>
                        </td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_leader_art_communication" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label10" runat="server">
                                                 <p><b style="color:red;">E</b>   L<b style="color:red;"><u>E</u></b>ADERSHIP</p>  
                            </asp:Label>
                        </td>
                        <td>ความเป็นผู้นำ</td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_emp_leadership" runat="server">
                            </asp:Label>
                        </td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_leader_leadership" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label11" runat="server">
                                                 <p><b style="color:green;">D</b>   <b style="color:green;"><u>D</u></b>ECISION MAKING & PLANNING</p>  
                            </asp:Label>
                        </td>
                        <td>การแก้ปัญหา การตัดสินใจ และการวางแผน</td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_emp_decision_making_planning" runat="server">
                            </asp:Label>
                        </td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_leader_decision_making_planning" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr style="text-align: right;">
                        <td colspan="2"><b>คะแนนรวม</b></td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_emp_leadership_capabilities" runat="server">
                            </asp:Label>
                        </td>
                        <td style="text-align: right;">
                            <asp:Label ID="lb_leader_leadership_capabilities" runat="server">
                            </asp:Label>
                        </td>
                    </tr>
                </table>
            </div>

            <div class="col-lg-12">
                <table style="width: 100%; background-color: white"
                    class="table table-bordered table-hover table-responsive col-lg-12 word-wrap">

                    <tr>
                        <td style="text-align: center;">
                            <asp:Label ID="Label17" runat="server">
                                 <p><b>สำหรับผู้ถูกประเมิน</b></p>  
                            </asp:Label>
                        </td>

                        <td style="text-align: center;">
                            <asp:Label ID="Label19" runat="server">
                                 <p><b>สำหรับผู้ประเมิน</b></p>  
                            </asp:Label>
                        </td>

                        <td style="text-align: center;">
                            <asp:Label ID="Label21" runat="server">
                                 <p><b>สำหรับผู้บริหารตามสายงานฯ</b></p>  
                            </asp:Label>
                        </td>

                    </tr>

                    <tr>
                        <td style="text-align: center;">
                            <asp:Label ID="Label22" runat="server">
                                 <br /> 
                                <br /> 
                                <br /> 
                            </asp:Label>
                        </td>

                        <td style="text-align: center;"></td>

                        <td style="text-align: center;"></td>

                    </tr>

                </table>
            </div>


        </div>

    </form>
</body>
</html>
