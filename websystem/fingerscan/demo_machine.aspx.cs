using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_fingerscan_demo_machine : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    int emp_idx = 0;
    int _masterType; //1:machine; 2:place; 3:location; 4:zone;
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        // emp_idx = _funcTool.convertToInt(Session["emp_idx"].ToString());

        if(!IsPostBack)
        {
            _masterType = 1;
            rblMasterType2.SelectedValue = _masterType.ToString();
            initPage();
        }
    }

    #region event command
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch(cmdName)
        {
            case "cmdCreate":
                _masterType = _funcTool.convertToInt(rblMasterType2.SelectedValue);
                setActiveView("viewCreate", _masterType);

                div_heading.Attributes.Add("class", "panel panel-info");
                litHeadingTitle.Text = "เพิ่มข้อมูล";
                rblMasterType.Enabled = false;
                break;
            case "cmdEdit":
                _masterType = _funcTool.convertToInt(rblMasterType2.SelectedValue);
                setActiveView("viewCreate", _masterType);

                div_heading.Attributes.Add("class", "panel panel-warning");
                litHeadingTitle.Text = "แก้ไขข้อมูล";
                rblMasterType.Enabled = false;
                switch(_masterType)
                {
                    case 1:
                        LinkButton lbReset = (LinkButton)fvDetail.FindControl("lbReset");
                        lbReset.Visible = false;
                        break;
                    case 2:
                        LinkButton lbReset2 = (LinkButton)fvDetail2.FindControl("lbReset");
                        lbReset2.Visible = false;
                        break;
                    case 3:
                        LinkButton lbReset3 = (LinkButton)fvDetail3.FindControl("lbReset");
                        lbReset3.Visible = false;
                        break;
                    case 4:
                        LinkButton lbReset4 = (LinkButton)fvDetail4.FindControl("lbReset");
                        lbReset4.Visible = false;
                        break;
                }
                break;
            case "cmdSave": case "cmdCancel":
                _masterType = _funcTool.convertToInt(rblMasterType2.SelectedValue);
                setActiveView("viewList", _masterType);
                break;
            case "cmdReset":
                _masterType = _funcTool.convertToInt(rblMasterType2.SelectedValue);
                switch(_masterType)
                {
                    case 1:
                        setFormData(fvDetail, FormViewMode.Insert, null);
                        break;
                    case 2:
                        setFormData(fvDetail2, FormViewMode.Insert, null);
                        break;
                    case 3:
                        setFormData(fvDetail3, FormViewMode.Insert, null);
                        break;
                    case 4:
                        setFormData(fvDetail4, FormViewMode.Insert, null);
                        break;
                }
                break;
        }
    }

    protected void rblSelectedIndexChanged(object sender, EventArgs e)
    {
        RadioButtonList rblName = (RadioButtonList)sender;
        _masterType = _funcTool.convertToInt(rblName.SelectedValue);

        switch (rblName.ID)
        {
            // case "rblMasterType":
            //     setActiveView("viewCreate", _masterType);
            //     break;
            case "rblMasterType2":
                setActiveView("viewList", _masterType);
                break;
        }
    }
    #endregion event command

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveView("viewList", _masterType);
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["state"] = null;
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setActiveView(string activeTab, int masterType)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        switch(activeTab)
        {
            case "viewList":
                // setGridData(gvList, null);
                // setGridData(gvList2, null);
                // setGridData(gvList3, null);

                table1.Visible = false;
                table2.Visible = false;
                table3.Visible = false;
                table4.Visible = false;

                switch(masterType)
                {
                    case 1:
                        table1.Visible = true;
                        // search_unit_detail _search_units = new search_unit_detail();
                        // _search_units.s_m0idx = "0";
                        // _data_services.search_unit_list = new search_unit_detail[1];
                        // _data_services.search_unit_list[0] = _search_units;
                        // _data_services = getM0UnitsList(_data_services);
                        // setGridData(gvList, _data_services.m0_units_list);
                        break;
                    case 2:
                        table2.Visible = true;
                        // search_brand_detail _search_brands = new search_brand_detail();
                        // _search_brands.s_m0idx = "0";
                        // _data_services.search_brand_list = new search_brand_detail[1];
                        // _data_services.search_brand_list[0] = _search_brands;
                        // _data_services = getM0BrandsList(_data_services);
                        // setGridData(gvList2, _data_services.m0_brands_list);
                        break;
                    case 3:
                        table3.Visible = true;
                        // search_detail_prefix_detail _search_prefix = new search_detail_prefix_detail();
                        // _search_prefix.s_m0idx = "0";
                        // _data_services.search_detail_prefix_list = new search_detail_prefix_detail[1];
                        // _data_services.search_detail_prefix_list[0] = _search_prefix;
                        // _data_services = getM0DetailPrefixList(_data_services);
                        // setGridData(gvList3, _data_services.m0_detail_prefix_list);
                        break;
                    case 4:
                        table4.Visible = true;
                        break;
                }
                break;
            case "viewCreate":
                rblMasterType.SelectedValue = rblMasterType2.SelectedValue;
                setFormData(fvDetail, FormViewMode.ReadOnly, null);
                setFormData(fvDetail2, FormViewMode.ReadOnly, null);
                setFormData(fvDetail3, FormViewMode.ReadOnly, null);
                setFormData(fvDetail4, FormViewMode.ReadOnly, null);
                // litDebug.Text = masterType.ToString();
                switch(masterType)
                {
                    case 1:
                        setFormData(fvDetail, FormViewMode.Insert, null);
                        break;
                    case 2:
                        setFormData(fvDetail2, FormViewMode.Insert, null);
                        break;
                    case 3:
                        setFormData(fvDetail3, FormViewMode.Insert, null);
                        break;
                    case 4:
                        setFormData(fvDetail4, FormViewMode.Insert, null);
                        break;
                }
                break;
        }
    }

    // protected data_services getExecuteData(string serviceName, data_services dataIn, int actionType)
    // {
    //     return (data_services)_funcTool.convertXmlToObject(typeof(data_services), _serviceExecute.actionExec(_connString, "data_services", serviceName, dataIn, actionType));
    // }

    // protected void linkBtnTrigger(LinkButton linkBtnID)
    // {
    //     UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
    //     UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
    //     triggerLinkBtn.ControlID = linkBtnID.UniqueID;
    //     updatePanel.Triggers.Add(triggerLinkBtn);
    // }

    // protected data_services getM0UnitsList(data_services _data_services)
    // {
    //     return getExecuteData(_serviceUnit, _data_services, _funcTool.convertToInt(_actionSelect + "0"));
    // }

    // protected data_services getM0BrandsList(data_services _data_services)
    // {
    //     return getExecuteData(_serviceBrand, _data_services, _funcTool.convertToInt(_actionSelect + "0"));
    // }

    // protected data_services getM0DetailPrefixList(data_services _data_services)
    // {
    //     return getExecuteData(_serviceDetailPrefix, _data_services, _funcTool.convertToInt(_actionSelect + "0"));
    // }

    public string getStatusIcon(Int32 inStatus)
    {
        return (inStatus == 1 ? "<i class=\"fas fa-check text-success\"></i>" : "<i class=\"fas fa-times text-danger\"></i>");
    }
    #endregion reuse
}