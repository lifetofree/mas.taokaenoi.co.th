﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CalendarEvent
/// </summary>
public class CalendarEvent
{
    public int id { get; set; }
    public int place_idx { get; set; }
    public int m0_room_idx { get; set; }
    public string title { get; set; }
    public string description { get; set; }
    public string place_name { get; set; }
    public string room_name_th { get; set; }
    public DateTime start { get; set; }
    public DateTime end { get; set; }
    public bool allDay { get; set; }
    public string topic_booking { get; set; }
    public string emp_name_th { get; set; }
}