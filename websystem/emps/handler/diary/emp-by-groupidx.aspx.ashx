﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

public class Handler : IHttpHandler
{

   public void ProcessRequest(HttpContext context)
   {
      string callback = context.Request.QueryString["callback"];
      int groupDiaryIDX = 0;
      int.TryParse(context.Request.QueryString["groupDiaryIDX"], out groupDiaryIDX);
      string json = this.getEmployeeDiaryByGroupDiaryIDX(groupDiaryIDX);
      if (!string.IsNullOrEmpty(callback))
      {
         json = string.Format("{0}({1});", callback, json);
      }
      context.Response.ContentType = "text/json";
      context.Response.Write(json);
   }

   private string getEmployeeDiaryByGroupDiaryIDX(int groupDiaryIDX)
   {
      List<object> emps = new List<object>();
      using (SqlConnection conn = new SqlConnection())
      {
         conn.ConnectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;
         using (SqlCommand cmd = new SqlCommand())
         {
            var employeeDiary = 1;
            cmd.CommandText = "SELECT m0gd.m0_group_diary_idx, view_emp.EmpIDX FROM emps_m0_group_diary m0gd INNER JOIN [Centralized].[dbo].[ViewEmployee] view_emp ON m0gd.rsec_idx_ref = view_emp.RSecID WHERE m0gd.m0_group_diary_idx = @groupDiaryIDX AND m0gd.group_diary_status <> 9 AND view_emp.EmpStatus = 1 AND view_emp.EmpType =" + employeeDiary;
            cmd.Parameters.AddWithValue("@groupDiaryIDX", groupDiaryIDX);
            cmd.Connection = conn;
            conn.Open();
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            using (SqlDataReader sdr = cmd.ExecuteReader())
            {
               while (sdr.Read())
               {
                  emps.Add(new
                  {
                     emp_idx = sdr["EmpIDX"]
                  });
               }
            }
            conn.Close();
         }
         return (new JavaScriptSerializer().Serialize(emps));
      }
   }

   public bool IsReusable
   {
      get
      {
         return false;
      }
   }
}


