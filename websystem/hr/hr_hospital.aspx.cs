﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_hr_hospital : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    service_execute _serviceExecute = new service_execute();

    string _localXml = String.Empty;
    string _localString = String.Empty;
    string _localJson = String.Empty;
    int _actionType = 0;
    int _emp_idx = 0;
    int _default_int = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetHospitalOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetHospitalOld"];
    static string _urlGetInsertHospital = _serviceUrl + ConfigurationManager.AppSettings["urlGetInsertHospital"];
    static string _urlGetUpdateHospital = _serviceUrl + ConfigurationManager.AppSettings["urlGetUpdateHospital"];
    static string _urlGetDeleteHospital = _serviceUrl + ConfigurationManager.AppSettings["urlGetDeleteHospital"];


    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();
        }

    }
    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Insert_Status()
    {
        _dataEmployee.hospital_list = new Hospital[1];
        Hospital dtemployee = new Hospital();

        dtemployee.Name = txtname.Text;
        dtemployee.HStatus = int.Parse(ddl_status.SelectedValue);
        
        _dataEmployee.hospital_list[0] = dtemployee;
        //text.Text = dtemployee.ToString();
        _dataEmployee = callService(_urlGetInsertHospital, _dataEmployee);
    }

    protected void SelectMasterList()
    {
        _dataEmployee.hospital_list = new Hospital[1];
        Hospital dtemployee = new Hospital();

        _dataEmployee.hospital_list[0] = dtemployee;

        _dataEmployee = callService(_urlGetHospitalOld, _dataEmployee);
        //text.Text = _dataEmployee.ToString();
        setGridData(GvMaster, _dataEmployee.hospital_list);
    }

    protected data_employee callService(string _cmdUrl, data_employee _dtmaster)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmaster);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtmaster = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);
        
        return _dtmaster;
    }

    protected void Update_Master_List()
    {
        _dataEmployee.hospital_list = new Hospital[1];
        Hospital dtemployee = new Hospital();

        dtemployee.HosIDX = int.Parse(ViewState["HosIDX_update"].ToString());
        dtemployee.Name = ViewState["txtname_update"].ToString();
        dtemployee.HStatus = int.Parse(ViewState["ddStatus_update"].ToString());

        _dataEmployee.hospital_list[0] = dtemployee;
        //text.Text = dtemployee.ToString();
        _dataEmployee = callService(_urlGetUpdateHospital, _dataEmployee);
    }

    protected void Delete_Master_List()
    {
        _dataEmployee.hospital_list = new Hospital[1];
        Hospital dtemployee = new Hospital();

        dtemployee.HosIDX = int.Parse(ViewState["HosIDX_delete"].ToString());

        _dataEmployee.hospital_list[0] = dtemployee;
        //text.Text = dtemployee.ToString();
        _dataEmployee = callService(_urlGetDeleteHospital, _dataEmployee);
    }



    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        /*var txtstatus_edit = (TextBox)e.Row.FindControl("txtstatus_edit");
                        var ddl_downtime_edit = (DropDownList)e.Row.FindControl("ddl_downtime_edit");
                        var lbDTIDX = (Label)e.Row.FindControl("lbDTIDX");

                        ddl_downtime_edit.AppendDataBoundItems = true;

                        _dtsupport.BoxStatusSAP = new StatusSAP[1];
                        StatusSAP dtsupport = new StatusSAP();

                        _dtsupport.BoxStatusSAP[0] = dtsupport;

                        _dtsupport = callServiceDevices(_urlGetDowntime, _dtsupport);


                        ddl_downtime_edit.DataSource = _dtsupport.BoxStatusSAP;
                        ddl_downtime_edit.DataTextField = "downtime_name";
                        ddl_downtime_edit.DataValueField = "dtidx";
                        ddl_downtime_edit.DataBind();
                        ddl_downtime_edit.SelectedValue = lbDTIDX.Text;*/


                    }

                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int HosIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtname_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtname_update");
                var ddStatus_update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatus_update");

                GvMaster.EditIndex = -1;

                ViewState["HosIDX_update"] = HosIDX;
                ViewState["txtname_update"] = txtname_update.Text;
                ViewState["ddStatus_update"] = ddStatus_update.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion


    #endregion


    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAddHolder":

                btnaddholder.Visible = false;
                Panel_Add.Visible = true;
                break;

            case "btnCancel":
                btnaddholder.Visible = true;
                Panel_Add.Visible = false;

                break;

            case "btnAdd":
                Insert_Status();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "CmdDel":

                int HosIDX = int.Parse(cmdArg);
                ViewState["HosIDX_delete"] = HosIDX;
                Delete_Master_List();
                SelectMasterList();

                break;
        }



    }
    #endregion

}
