﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_qa_cims_m0_location : System.Web.UI.Page
{

    #region Connect
    function_tool _funcTool = new function_tool();
    data_employee _dataEmployee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    ////--master-- //
    static string _urlCimsSetLocation = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetLocation"];
    static string _urlCimsGetLocationName = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetLocationName"];
    static string _urlCimsSetLocationZone = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetLocationZone"];
    static string _urlCimsGetLocationZoneName = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetLocationZoneName"];



    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            setActiveView("ViewInsertLocation", 0, "");
        }

    }

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        //default
        btnAddLocation.Visible = true;
        btnAddZone.Visible = true;

        switch (cmdName)
        {
            case "cmdAddLocation":

                setFormData(fvInsertLocation, FormViewMode.Insert, null);
                btnAddLocation.Visible = false;

                //gvFormList.EditIndex = -1;
                //getFormList();
                break;

            case "cmdAddZone":
                setFormData(fvInsertZone, FormViewMode.Insert, null);
                btnAddZone.Visible = false;

                var txt_Location_view = (TextBox)fvInsertZone.FindControl("txt_Location_view");
                txt_Location_view.Text = txt_Location_name.Text;

                break;

            case "cmdCancel":
                setActiveView("ViewInsertLocation", 0, "");
                break;

            case "cmdCancelZone":
                setActiveView("ViewInsertZone", int.Parse(ViewState["vs_m0_location_idx"].ToString()), txt_Location_name.Text);
                break;

            case "cmdView":

                string[] arg1 = new string[2];
                arg1 = e.CommandArgument.ToString().Split(',');
                int _m0_location_idx = int.Parse((arg1[0].ToString()));
                string _location_name = (arg1[1]);

                ViewState["vs_m0_location_idx"] = _m0_location_idx.ToString();

                setActiveView("ViewInsertZone", int.Parse(ViewState["vs_m0_location_idx"].ToString()), _location_name);
                txt_Location_name.Text = _location_name.ToString();
                break;

            case "cmdBack":
                setActiveView("ViewInsertLocation", 0, "");
                break;

            case "cmdDelete":
                int _m0_m0_location_idx = int.Parse(cmdArg);

                data_qa_cims _data_m0_location_del = new data_qa_cims();
                _data_m0_location_del.qa_cims_m0_location_list = new qa_cims_m0_location_detail[1];
                qa_cims_m0_location_detail _m0_location_del = new qa_cims_m0_location_detail();

                _m0_location_del.m0_location_idx = (_m0_m0_location_idx);
                _m0_location_del.location_status = 9;

                _data_m0_location_del.qa_cims_m0_location_list[0] = _m0_location_del;

                _data_m0_location_del = callServicePostMasterQACIMS(_urlCimsSetLocation, _data_m0_location_del);

                if (_data_m0_location_del.return_code == 0)
                {
                    getLocationList();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินรายการไม่สำเร็จ กรุณาลองใหม่อีกครั้ง');", true);
                }

                break;

            case "cmdSave":

                var txt_Locationname = (TextBox)fvInsertLocation.FindControl("txt_Locationname");
                var ddl_status_location = (DropDownList)fvInsertLocation.FindControl("ddl_status_location");
                
                data_qa_cims _data_cims_m0_location = new data_qa_cims();
                _data_cims_m0_location.qa_cims_m0_location_list = new qa_cims_m0_location_detail[1];
                qa_cims_m0_location_detail _m0_location = new qa_cims_m0_location_detail();

                _m0_location.cemp_idx = _emp_idx;
                _m0_location.location_name = txt_Locationname.Text;              
                _m0_location.location_status = int.Parse(ddl_status_location.SelectedValue);

                _data_cims_m0_location.qa_cims_m0_location_list[0] = _m0_location;

                _data_cims_m0_location = callServicePostMasterQACIMS(_urlCimsSetLocation, _data_cims_m0_location);

                btnAddLocation.Visible = false;

                if (_data_cims_m0_location.return_code == 0)
                {
                    //getFormList();
                    getLocationList();
                    setFormData(fvInsertLocation, FormViewMode.Insert, null);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                }

                break;

            case "cmdSaveZone":

                var txt_Zone = (TextBox)fvInsertZone.FindControl("txt_Zone");               
                var ddl_status_zone = (DropDownList)fvInsertZone.FindControl("ddl_status_zone");

                data_qa_cims _data_cims_zone = new data_qa_cims();
                _data_cims_zone.qa_cims_m0_location_list = new qa_cims_m0_location_detail[1];
                qa_cims_m0_location_detail _m0_zone = new qa_cims_m0_location_detail();

                _m0_zone.cemp_idx = _emp_idx;
                _m0_zone.location_zone = txt_Zone.Text;              
                _m0_zone.zone_status = int.Parse(ddl_status_zone.SelectedValue);
                _m0_zone.m0_location_idx = int.Parse(ViewState["vs_m0_location_idx"].ToString());

                _data_cims_zone.qa_cims_m0_location_list[0] = _m0_zone;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_cims_zone));

                _data_cims_zone = callServicePostMasterQACIMS(_urlCimsSetLocationZone, _data_cims_zone);

                if (_data_cims_zone.return_code == 0)
                {
                    getLocationZone(int.Parse(ViewState["vs_m0_location_idx"].ToString()), "");
                    txt_Zone.Text = "";
                    //txt_form_table_name.Text = "";
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                }

                break;

            case "cmdDeleteZone":

                int _m1_location_idx_zone = int.Parse(cmdArg);

                data_qa_cims _data_cims_zone_del = new data_qa_cims();
                _data_cims_zone_del.qa_cims_m0_location_list = new qa_cims_m0_location_detail[1];
                qa_cims_m0_location_detail _m0_zone_del = new qa_cims_m0_location_detail();

                _m0_zone_del.m1_location_idx = (_m1_location_idx_zone);
                _m0_zone_del.zone_status = 9;

                _data_cims_zone_del.qa_cims_m0_location_list[0] = _m0_zone_del;

                _data_cims_zone_del = callServicePostMasterQACIMS(_urlCimsSetLocationZone, _data_cims_zone_del);

                if (_data_cims_zone_del.return_code == 0)
                {
                    getLocationZone(int.Parse(ViewState["vs_m0_location_idx"].ToString()), "");
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินรายการไม่สำเร็จ กรุณาลองใหม่อีกครั้ง');", true);
                }

                break;
        }
    }

    #endregion

    #region setgriddata

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void gvRowDeleted(object sender, GridViewDeleteEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvTableResultList":

                //GridView _DelgvTableResultList = (GridView)fvFormTableResult.FindControl("gvTableResultList");
                //var _pnbtnSave = (Panel)fvFormTableResult.FindControl("pnbtnSave");

                //var DeleteFormList = (DataSet)ViewState["vsDataTableFormList"];
                //var drFormList = DeleteFormList.Tables[0].Rows;

                //drFormList.RemoveAt(e.RowIndex);

                //ViewState["vsDataTableFormList"] = DeleteFormList;
                //_DelgvTableResultList.EditIndex = -1;

                //setGridData(_DelgvTableResultList, ViewState["vsDataTableFormList"]);

                //if (_DelgvTableResultList.Rows.Count == 0)
                //{
                //    _pnbtnSave.Visible = false;
                //}
                //else
                //{
                //    _pnbtnSave.Visible = true;
                //}
                break;
        }
    }

    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "gvLocation":
                gvLocation.PageIndex = e.NewPageIndex;
                getLocationList();
                break;

            case "gvZone":
                gvZone.PageIndex = e.NewPageIndex;
                getLocationZone(int.Parse(ViewState["vs_m0_location_idx"].ToString()), "");
                break;
        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvLocation":


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label var_lbl_location_status = (Label)e.Row.FindControl("lbl_location_status");
                    Label var_lbllocation_statusOnline = (Label)e.Row.FindControl("lbllocation_statusOnline");
                    Label var_lbllocation_statusOffline = (Label)e.Row.FindControl("lbllocation_statusOffline");
                                
                    ViewState["_vs_location_status"] = var_lbl_location_status.Text;

                    if (ViewState["_vs_location_status"].ToString() == "1")
                    {
                        var_lbllocation_statusOnline.Visible = true;
                    }
                    else if (ViewState["_vs_location_status"].ToString() == "0")
                    {
                        var_lbllocation_statusOffline.Visible = true;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    //Label notedetails = (Label)e.Row.FindControl("notedetails");
                    //TextBox txt_update_note_detail = (TextBox)e.Row.FindControl("txt_update_note_detail");

                    //txt_update_note_detail.Text = HttpUtility.HtmlDecode(notedetails.Text.ToString());

                    setFormData(fvInsertLocation, FormViewMode.ReadOnly, null);
                }
                break;

            case "gvZone":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_lbzone_status = (Label)e.Row.FindControl("lbzone_status");
                    Label lbl_lbzone_statusOnline = (Label)e.Row.FindControl("lbzone_statusOnline");
                    Label lbl_lbzone_statusOffline = (Label)e.Row.FindControl("lbzone_statusOffline");

                    ViewState["_vs_lbzone_status"] = lbl_lbzone_status.Text;

                    if (ViewState["_vs_lbzone_status"].ToString() == "1")
                    {
                        lbl_lbzone_statusOnline.Visible = true;
                    }
                    else if (ViewState["_vs_lbzone_status"].ToString() == "0")
                    {
                        lbl_lbzone_statusOffline.Visible = true;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    setFormData(fvInsertZone, FormViewMode.ReadOnly, null);

                    TextBox txt_m0_location_name_update = (TextBox)e.Row.FindControl("txt_m0_location_name_update");
                    txt_m0_location_name_update.Text = txt_Location_name.Text;
                }
                break;
        }
    }

    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvLocation":

                var _txt_m0_location_idx_edit = (TextBox)gvLocation.Rows[e.RowIndex].FindControl("txt_m0_location_idx_edit");
                var _txt_location_name_edit = (TextBox)gvLocation.Rows[e.RowIndex].FindControl("txt_location_name_edit");               
                var _ddllocation_status = (DropDownList)gvLocation.Rows[e.RowIndex].FindControl("ddllocation_status");

                gvLocation.EditIndex = -1;

                data_qa_cims _data_m0_location = new data_qa_cims();
                _data_m0_location.qa_cims_m0_location_list = new qa_cims_m0_location_detail[1];
                qa_cims_m0_location_detail _update_m0_location = new qa_cims_m0_location_detail();

                _update_m0_location.m0_location_idx = int.Parse(_txt_m0_location_idx_edit.Text);
                _update_m0_location.location_name = _txt_location_name_edit.Text;             
                _update_m0_location.location_status = int.Parse(_ddllocation_status.SelectedValue);
                _update_m0_location.cemp_idx = _emp_idx;

                _data_m0_location.qa_cims_m0_location_list[0] = _update_m0_location;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_m0_location));
                _data_m0_location = callServicePostMasterQACIMS(_urlCimsSetLocation, _data_m0_location);

                if (_data_m0_location.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                    gvLocation.EditIndex = -1;
                    getLocationList();
                }
                else
                {
                    getLocationList();
                    setFocus.Focus();
                }

                break;

            case "gvZone":

                var _update_txt_m1_location_idx = (TextBox)gvZone.Rows[e.RowIndex].FindControl("txt_m1_location_idx");
                var _update_txt_update_location_zone = (TextBox)gvZone.Rows[e.RowIndex].FindControl("txt_update_location_zone");
                var _update_ddlupdatezone_status = (DropDownList)gvZone.Rows[e.RowIndex].FindControl("ddlupdatezone_status");
               
                gvZone.EditIndex = -1;

                data_qa_cims _data_update_zone = new data_qa_cims();
                _data_update_zone.qa_cims_m0_location_list = new qa_cims_m0_location_detail[1];
                qa_cims_m0_location_detail _m0_updatezone = new qa_cims_m0_location_detail();

                _m0_updatezone.m1_location_idx = int.Parse(_update_txt_m1_location_idx.Text);
                _m0_updatezone.location_zone = _update_txt_update_location_zone.Text;
                _m0_updatezone.zone_status = int.Parse(_update_ddlupdatezone_status.SelectedValue);
                _m0_updatezone.cemp_idx = _emp_idx;

                _data_update_zone.qa_cims_m0_location_list[0] = _m0_updatezone;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_update_zone));
                _data_update_zone = callServicePostMasterQACIMS(_urlCimsSetLocationZone, _data_update_zone);

                if (_data_update_zone.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                    getLocationZone(int.Parse(ViewState["vs_m0_location_idx"].ToString()), "");
                }
                else
                {
                    getLocationZone(int.Parse(ViewState["vs_m0_location_idx"].ToString()), "");
                    setFocus.Focus();
                }
                break;
        }
    }

    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvLocation":

                gvLocation.EditIndex = -1;
                getLocationList();
                setFocus.Focus();
                break;

            case "gvZone":
                gvZone.EditIndex = -1;
                getLocationZone(int.Parse(ViewState["vs_m0_location_idx"].ToString()), "");

                setFocus.Focus();
                break;
        }
    }

    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvLocation":
                gvLocation.EditIndex = e.NewEditIndex;
                getLocationList();

                break;

            case "gvZone":
                gvZone.EditIndex = e.NewEditIndex;
                getLocationZone(int.Parse(ViewState["vs_m0_location_idx"].ToString()), "");
                break;
        }
    }

    #endregion setgriddata

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region getdata

    protected void getLocationList()
    {
        data_qa_cims _data_cims_location_list = new data_qa_cims();
        _data_cims_location_list.qa_cims_m0_location_list = new qa_cims_m0_location_detail[1];
        qa_cims_m0_location_detail _m0_locationlist = new qa_cims_m0_location_detail();

        _data_cims_location_list.qa_cims_m0_location_list[0] = _m0_locationlist;

        _data_cims_location_list = callServicePostMasterQACIMS(_urlCimsGetLocationName, _data_cims_location_list);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_cims_location_list));

        setGridData(gvLocation, _data_cims_location_list.qa_cims_m0_location_list);
    }

    protected void getLocationZone(int _m0_location_idx, string location_name)
    {
        data_qa_cims _data_m0_zone = new data_qa_cims();
        _data_m0_zone.qa_cims_m0_location_list = new qa_cims_m0_location_detail[1];
        qa_cims_m0_location_detail _m0_location_zone = new qa_cims_m0_location_detail();

        _m0_location_zone.m0_location_idx = int.Parse(_m0_location_idx.ToString());

        _data_m0_zone.qa_cims_m0_location_list[0] = _m0_location_zone;

        _data_m0_zone = callServicePostMasterQACIMS(_urlCimsGetLocationZoneName, _data_m0_zone);

        setGridData(gvZone, _data_m0_zone.qa_cims_m0_location_list);
    }
    #endregion getdata

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);

        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
    #endregion callService Functions

    #region setActiveView

    protected void setActiveView(string activeTab, int idx, string locationname)
    {
        mvMultiview.SetActiveView((View)mvMultiview.FindControl(activeTab));

        setFormData(fvInsertLocation, FormViewMode.ReadOnly, null);
        setFormData(fvInsertZone, FormViewMode.ReadOnly, null);


        switch (activeTab)
        {
            case "ViewInsertLocation":
                getLocationList();
                break;

            case "ViewInsertZone":

                getLocationZone(idx, "");

                lbl_show_namezone.Text = "Location : " + locationname.ToString();
                btnAddZone.Visible = true;
                break;

        }
    }

    #endregion setActiveView
}