﻿using System;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_adonline_permission_type : System.Web.UI.Page
{
   #region Init
   data_adonline dataADOnline = new data_adonline();
   service_execute servExec = new service_execute();
   function_tool _funcTool = new function_tool();
   string misConn = "conn_mis";
   string misConnReal = "conn_mis_real";
   string adPermissionPolicyService = "adPermissionPolicyService";
   static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
   static string _urlCreateM0PermPol = _serviceUrl + ConfigurationManager.AppSettings["urlADCreateM0PermPol"];
   static string _urlReadM0PermPol = _serviceUrl + ConfigurationManager.AppSettings["urlADReadPermPol"];
   static string _urlUpdateM0PermPol = _serviceUrl + ConfigurationManager.AppSettings["urlADUpdateM0PermPol"];
   static string _urlDeleteM0PermPol = _serviceUrl + ConfigurationManager.AppSettings["urlADDeleteM0PermPol"];
   string _local_xml = string.Empty;
   string _local_json = string.Empty;
   int[] empIDX = { 172, 173, 1374, 1413, 3760, 4839 }; //P'Phon, P'Mai, Bonus, P'Toei, Mod, Jame
   #endregion Init

   #region Constant
   public static class Constants
   {
      public const int SELECT_ALL = 20;
      public const int CREATE = 10;
      public const int UPDATE = 11;
      public const int SELECT_WHERE = 21;
      public const int SELECT_WHERE_SEARCH = 22;
      public const int BAN = 30;
      public const int UNBAN = 31;
      public const int SELECT_WHERE_EXISTS_NAME = 28;
      public const int SELECT_WHERE_EXISTS_NAME_UPDATE = 29;
      public const string VALID_FALSE = "101";
   }
   #endregion Constant

   #region Page Load
   protected void Page_Load(object sender, EventArgs e)
   {
      if (!IsPostBack)
      {
         divIndex.Visible = true;
         divInsert.Visible = false;
         ViewState["empIDX"] = Session["emp_idx"];
         actionIndex();
      }
   }
   #endregion Page Load

   #region Action
   protected void actionIndex()
   {
      permission_policy objADOnline = new permission_policy();
      dataADOnline.ad_permission_policy_action = new permission_policy[1];
      dataADOnline.ad_permission_policy_action[0] = objADOnline;
      dataADOnline = callServiceADOnline(_urlReadM0PermPol, dataADOnline);
      setGridData(gvPermPol, dataADOnline.ad_permission_policy_action);
   }

   protected void actionCreate()
   {
      Page.Validate();
      if (Page.IsValid == true)
      {
         permission_policy objADOnline = new permission_policy();
         dataADOnline.ad_permission_policy_action = new permission_policy[1];
         objADOnline.perm_pol_name = txtPermPolName.Text.Trim();
         objADOnline.perm_pol_desc = txtPermPolDesc.Text.Trim();
         objADOnline.perm_pol_status = int.Parse(ddlPermPolStatus.SelectedValue);
         dataADOnline.ad_permission_policy_action[0] = objADOnline;
         dataADOnline = callServiceADOnline(_urlCreateM0PermPol, dataADOnline);
         if (dataADOnline.return_code == 101)
         {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ชื่อระดับสิทธิ์นี้ มีในระบบแล้ว');", true);
         }
         else
         {
            divIndex.Visible = true;
            divInsert.Visible = false;
            actionIndex();
         }
      }
   }

   protected void actionUpdate(int idx, string name, string desc, int status)
   {
      permission_policy objADOnline = new permission_policy();
      dataADOnline.ad_permission_policy_action = new permission_policy[1];
      objADOnline.m0_perm_pol_idx = idx;
      objADOnline.perm_pol_name = name;
      objADOnline.perm_pol_desc = desc;
      objADOnline.perm_pol_status = status;
      dataADOnline.ad_permission_policy_action[0] = objADOnline;
      callServiceADOnline(_urlUpdateM0PermPol, dataADOnline);
   }

   protected void actionDelete(int id)
   {
      permission_policy objADOnline = new permission_policy();
      dataADOnline.ad_permission_policy_action = new permission_policy[1];
      objADOnline.m0_perm_pol_idx = id;
      dataADOnline.ad_permission_policy_action[0] = objADOnline;
      callServiceADOnline(_urlDeleteM0PermPol, dataADOnline);
   }
   #endregion Action

   #region btnCommand
   protected void btnCommand(object sender, CommandEventArgs e)
   {
      string cmdName = e.CommandName.ToString();
      string cmdArg = e.CommandArgument.ToString();
      switch (cmdName)
      {
         case "btnToInsert":
            divIndex.Visible = false;
            divInsert.Visible = true;
            txtPermPolName.Text = string.Empty;
            txtPermPolDesc.Text = string.Empty;
            break;

         case "btnInsert":
            actionCreate();
            break;

         case "btnBan":
            actionDelete(int.Parse(cmdArg));
            actionIndex();
            break;

         case "btnCancel":
            divIndex.Visible = !divIndex.Visible;
            divInsert.Visible = !divInsert.Visible;
            actionIndex();
            break;
      }
   }
   #endregion btnCommand

   #region Custom Functions
   protected void setGridData(GridView gvName, Object obj)
   {
      gvName.DataSource = obj;
      gvName.DataBind();
   }

   protected string getStatus(int status)
   {
      if (status == 1)
      {
         return "<span class='status-online f-bold'>Online</span>";
      }
      else if (status == 0)
      {
         return "<span class='status-offline f-bold'>Offline</span>";
      }
      else
      {
         return "<span class='status-offline f-bold'>Delete</span>";
      }
   }

   protected string nl2br(string text)
   {
      if (text != string.Empty)
      {
         return text.Replace("\n", "<br />");
      }
      return string.Empty;
   }

   protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPermPol":
            gvName.PageIndex = e.NewPageIndex;
            gvName.DataBind();
            actionIndex();
            break;
      }
   }

   protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPermPol":
            if (e.Row.RowState.ToString().Contains("Edit"))
            {
               GridView editGrid = sender as GridView;
               int colSpan = editGrid.Columns.Count;
               for (int i = 1; i < colSpan; i++)
               {
                  e.Row.Cells[i].Visible = false;
                  e.Row.Cells[i].Controls.Clear();
               }
               e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
               e.Row.Cells[0].CssClass = "";
            }
            break;
      }
   }

   protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPermPol":
            gvName.EditIndex = -1;
            actionIndex();
            break;
      }
   }

   protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPermPol":
            int dataKeyId = Convert.ToInt32(gvName.DataKeys[e.RowIndex].Values[0].ToString());
            TextBox txtPermPolNameUpdate = (TextBox)gvName.Rows[e.RowIndex].FindControl("txtPermPolNameUpdate");
            TextBox txtPermPolDescUpdate = (TextBox)gvName.Rows[e.RowIndex].FindControl("txtPermPolDescUpdate");
            DropDownList ddlPermPolStatusUpdate = (DropDownList)gvName.Rows[e.RowIndex].FindControl("ddlPermPolStatusUpdate");
            gvName.EditIndex = -1;
            actionUpdate(dataKeyId, txtPermPolNameUpdate.Text.Trim(), txtPermPolDescUpdate.Text.Trim(), int.Parse(ddlPermPolStatusUpdate.SelectedValue));
            actionIndex();
            break;
      }
   }

   protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPermPol":
            gvName.EditIndex = e.NewEditIndex;
            actionIndex();
            break;
      }
   }

   protected data_adonline callServiceADOnline(string _cmdUrl, data_adonline _data_adonline)
   {
      _local_json = _funcTool.convertObjectToJson(_data_adonline);
      _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);
      _data_adonline = (data_adonline)_funcTool.convertJsonToObject(typeof(data_adonline), _local_json);
      return _data_adonline;
   }
   #endregion Custom Functions
}
