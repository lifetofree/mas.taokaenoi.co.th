﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_probation_m0_topics_evaluated : System.Web.UI.Page
{

    #region initial function/data

    function_tool _funcTool = new function_tool();
    data_probation _data_probation = new data_probation();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetTypevaluation = _serviceUrl + ConfigurationManager.AppSettings["urlGetTypevaluation"];
    static string _urlSetNamevaluation = _serviceUrl + ConfigurationManager.AppSettings["urlSetNamevaluation"];
    static string _urlGetTopicsList = _serviceUrl + ConfigurationManager.AppSettings["urlGetTopicsList"];
    static string _urlGetsubTopicsList = _serviceUrl + ConfigurationManager.AppSettings["urlGetsubTopicsList"];

    
    string _localJson = "";
    int _tempInt = 0;

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        set_defult();

        if (!IsPostBack)
        {
            actionSelect_type_evaluation();

        }

    }

    #region setDefult
    protected void set_defult()
    {
        // listMenu0.BackColor = System.Drawing.Color.LightGray;

    }
    #endregion

    #region select data
    protected void actionSelect_type_evaluation()
    {
        data_probation _data_probation = new data_probation();
        _data_probation.m0_type_evaluation_list = new m0_type_evaluation_detail[1];
        m0_type_evaluation_detail _m0_type_evaluation = new m0_type_evaluation_detail();

        _data_probation.m0_type_evaluation_list[0] = _m0_type_evaluation;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_probation));
        _data_probation = callServiceProbation(_urlGetTypevaluation, _data_probation);
        setGridViewDataBind(gvEvaluationType, _data_probation.m0_type_evaluation_list);

    }

    protected void actionSelect_list_topics()
    {

        
        // data_probation _data_probation = new data_probation();
        _data_probation.m0_name_evaluation_list = new m0_name_evaluation_detail[1];
        m0_name_evaluation_detail _m0_list_topic = new m0_name_evaluation_detail();

        _m0_list_topic.emps_type_probation = int.Parse(ViewState["_m0_type_idx"].ToString());
        //_m0_list_topic.root_m0_topics_idx = int.Parse(root_m0_Topics.Text);

        _data_probation.m0_name_evaluation_list[0] = _m0_list_topic;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_probation));
        _data_probation = callServiceProbation(_urlGetTopicsList, _data_probation);

        listTypeTopic_firstLevel.DataSource = _data_probation.m0_name_evaluation_list;
        listTypeTopic_firstLevel.DataBind();

        var root_m0_Topics = (Label)listTypeTopic_firstLevel.Items[1].FindControl("root_m0_Topics");
        var listsubTopics = (Repeater)listTypeTopic_firstLevel.Items[1].FindControl("listsubTopics");
        litDebug.Text = root_m0_Topics.Text;

        if ((root_m0_Topics.Text) != "0")
        {
            _data_probation.m0_name_evaluation_list = new m0_name_evaluation_detail[1];
            m0_name_evaluation_detail _m0_sub_topic = new m0_name_evaluation_detail();

          //  _m0_sub_topic.emps_type_probation = int.Parse(ViewState["_m0_type_idx"].ToString());
            _m0_sub_topic.root_m0_topics_idx = int.Parse(root_m0_Topics.Text);

            _data_probation.m0_name_evaluation_list[0] = _m0_sub_topic;
            _data_probation = callServiceProbation(_urlGetsubTopicsList, _data_probation);

            listsubTopics.DataSource = _data_probation.m0_name_evaluation_list;
            listsubTopics.DataBind();
        }
    }

    protected void actionSelect_suuTopics()
    {
        // data_probation _data_probation = new data_probation();
        _data_probation.m0_name_evaluation_list = new m0_name_evaluation_detail[1];
        m0_name_evaluation_detail _m0_list_topic = new m0_name_evaluation_detail();

        _m0_list_topic.root_m0_topics_idx = int.Parse(ViewState["_m0_type_idx"].ToString());

        _data_probation.m0_name_evaluation_list[0] = _m0_list_topic;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_probation));
        _data_probation = callServiceProbation(_urlGetTopicsList, _data_probation);
        listTypeTopic_firstLevel.DataSource = _data_probation.m0_name_evaluation_list;
        listTypeTopic_firstLevel.DataBind();

    }

    #endregion

    #region setGridViewDataBind
    protected void setGridViewDataBind(GridView gridViewName, Object obj)
    {
        gridViewName.DataSource = obj;
        gridViewName.DataBind();
    }
    #endregion

    #region gridView

    #region gvPageIndexChanging

    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvEvaluationType":

                break;
        }

    }

    #endregion

    #region gvRowDataBound

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gridViewName = (GridView)sender;

        switch (gridViewName.ID)
        {
            case "gvEvaluationType":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label label_status_evaluation = (Label)e.Row.Cells[2].FindControl("label_status_evaluation");
                    Label status_offline = (Label)e.Row.Cells[2].FindControl("status_offline");
                    Label status_online = (Label)e.Row.Cells[2].FindControl("status_online");

                    ViewState["status_evaluation"] = label_status_evaluation.Text;

                    if (ViewState["status_evaluation"].ToString() == "9")
                    {
                        status_offline.Visible = true;
                    }
                    else if (ViewState["status_evaluation"].ToString() == "1")
                    {
                        status_online.Visible = true;
                    }
                }
                    break;
        }
    }

    #endregion

    #endregion

    #region btn_Command

    protected void btn_command(object sender, CommandEventArgs e)
    {
        string cmd_name = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmd_name)
        {
            case "editform_evaluation":
                int _type_idx;
                _type_idx = int.Parse(cmdArg);
                ViewState["_m0_type_idx"] = _type_idx;
                actionSelect_list_topics();
                mvMultiview.SetActiveView(viewInsertForm);
                break;

            case "BackHome":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "insertList":
               
                m0_name_evaluation_detail m0_name_evaluation = new m0_name_evaluation_detail();
                _data_probation.m0_name_evaluation_list = new m0_name_evaluation_detail[1];

                m0_name_evaluation.topics_name = txttopicsName.Text;
                m0_name_evaluation.root_m0_topics_idx = 0;
                m0_name_evaluation.emps_type_probation = int.Parse(ViewState["_m0_type_idx"].ToString());

               

                _data_probation.m0_name_evaluation_list[0] = m0_name_evaluation;

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_probation));
                //litDebug.Text = (ViewState["m0_type_idx"].ToString());//HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_probation));

                _data_probation = callServiceProbation(_urlSetNamevaluation, _data_probation);

                if (_data_probation.return_code == "0")
                {
                    actionSelect_list_topics();

                }
                else
                {
                    setError(_data_probation.return_code.ToString() + " - " + _data_probation.return_msg);
                }

                txttopicsName.Text = String.Empty;

                break;

            case "insertlistlevel2":
                int m0_topics_idx;
                m0_topics_idx = int.Parse(cmdArg);
                ViewState["m0_topics_idx"] = m0_topics_idx;

                panel_subTopics.Visible = true;

                break;

            case "insertsubTopics":

                m0_name_evaluation_detail m0_name_subTpics = new m0_name_evaluation_detail();
                _data_probation.m0_name_evaluation_list = new m0_name_evaluation_detail[1];

                m0_name_subTpics.topics_name = txtSubtopicsName.Text;
                m0_name_subTpics.root_m0_topics_idx = int.Parse(ViewState["m0_topics_idx"].ToString());
                m0_name_subTpics.emps_type_probation = int.Parse(ViewState["_m0_type_idx"].ToString());

                _data_probation.m0_name_evaluation_list[0] = m0_name_subTpics;

                _data_probation = callServiceProbation(_urlSetNamevaluation, _data_probation);


                break;


        }

    }

    #endregion
    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }


    #region call method
    protected data_probation callServiceProbation(string _cmdUrl, data_probation _data_probation)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_probation);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_probation = (data_probation)_funcTool.convertJsonToObject(typeof(data_probation), _localJson);

        return _data_probation;
    }
    #endregion

 

}