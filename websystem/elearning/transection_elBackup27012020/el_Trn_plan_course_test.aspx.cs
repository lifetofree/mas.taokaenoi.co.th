﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_el_Trn_plan_course_test : System.Web.UI.Page
{
    #region Init
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_employee = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_employee"];

    static string _urlGetel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u_plan_course"];
    static string _urlSetInsel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u_plan_course"];
    static string _urlDelel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_u_plan_course"];
    static string _urlSetUpdel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_u_plan_course"];

    static string _urlsendEmail_plan_course_hrtohrd = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_plan_course_hrtohrd"];
    static string _urlsendEmail_plan_course_hrdtomd = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_plan_course_hrdtomd"];
    static string _urlsendEmail_plan_course_hrdtohr_all = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_plan_course_hrdtohr_all"];
    static string _urlsendEmail_plan_course_mdtohrd_all = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_plan_course_mdtohrd_all"];
    static string _urlsendEmail_outplan_course_usertoleader = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_outplan_course_usertoleader"];
    static string _urlsendemail_outplan_leadertohr = _serviceUrl + ConfigurationManager.AppSettings["urlsendemail_outplan_leadertohr"];
    static string _urlsendemail_outplan_leadertouser_all = _serviceUrl + ConfigurationManager.AppSettings["urlsendemail_outplan_leadertouser_all"];


    static string _urlGetel_Report_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_Report_plan_course"];

    //หลักสูตร 
    static string _urlSetInsel_u_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u_course"];

    string _gPathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];

    string _localJson = "";
    int _tempInt = 0;
    int emp_idx = 0;

    string _Folder_coursetestimg = "course_test_img";
    string _Folder_courseBin = "course_test_img_bin";

    #endregion Init

    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
        if (!IsPostBack)
        {
            int id = 0;
            ViewState["_id"] = "0";
            ViewState["_type"] = "";
            try
            {
                if (Page.RouteData.Values["id"] != null)
                {
                    string sid = Page.RouteData.Values["id"].ToString().ToLower();
                    string[] urldecry = (_funcTool.getDecryptRC4(sid, "id")).Split('|');
                    sid = urldecry[0];
                    id = _func_dmu.zStringToInt(sid);
                    ViewState["_id"] = sid;
                    sid = Page.RouteData.Values["type"].ToString().ToLower();
                    urldecry = (_funcTool.getDecryptRC4(sid, "type")).Split('|');
                    ViewState["_type"] = urldecry[0];
                }
                //string sid = Request.QueryString["id"].ToString().Trim();

            }
            catch
            {

            }
             setTitle(id);
            //checkcourse_test_permission_result();
        }

    }
    private void setTitle(int id)
    {
         dv_header.Attributes.Add("style", "background-image: url("+ResolveUrl("~/images/elearning/testform/test_header.png") + "); height: 100%; background-position: top; background-repeat: no-repeat; background-size: 100% 100%;");
         dv_footer.Attributes.Add("style", "background-image: url(" + ResolveUrl("~/images/elearning/testform/test_footer.png") + "); height: 100%; background-position: top; background-repeat: no-repeat; background-size: 100% 100%;");
        
        lit_result.Text = "ทดสอบหลังการฝึกอบรมเสร็จแล้ว";
        litcourse.Text = "";
        data_elearning dataelearning_detail;
        training_course obj_detail;
        //el_u1_training_course_lecturer
        dataelearning_detail = new data_elearning();
        dataelearning_detail.el_training_course_action = new training_course[1];
        obj_detail = new training_course();
        obj_detail.u0_training_course_idx = id;
        obj_detail.operation_status_id = "U1-FULL";
        dataelearning_detail.el_training_course_action[0] = obj_detail;
        dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
        CreateDs_el_u1_training_course_lecturer();
        CreateDs_el_tc_test();
        pnldata.Visible = false;
        pnl_alert.Visible = false;
       
        if (dataelearning_detail.el_training_course_action != null)
        {
            pnldata.Visible = true;
            DataSet ds = (DataSet)ViewState["vsel_u1_training_course_lecturer"];
            DataRow dr;
            foreach (var v_item in dataelearning_detail.el_training_course_action)
            {
                dr = ds.Tables["dsel_u1_training_course_lecturer"].NewRow();
                dr["lecturer_type"] = v_item.lecturer_type.ToString();
                dr["m0_institution_idx_ref"] = v_item.m0_institution_idx_ref.ToString();
                dr["zName"] = v_item.institution_name;
                ds.Tables["dsel_u1_training_course_lecturer"].Rows.Add(dr);
            }
            ViewState["vsel_u1_training_course_lecturer"] = ds;
        }
        

        pnl_permission.Visible = false;
        dataelearning_detail = new data_elearning();
        dataelearning_detail.el_training_course_action = new training_course[1];
        obj_detail = new training_course();
        obj_detail.u0_training_course_idx = id;
        obj_detail.emp_idx_ref = emp_idx;
        obj_detail.operation_status_id = "U3-FULL-PERMISSION";
        dataelearning_detail.el_training_course_action[0] = obj_detail;
        dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
        // Litdebug.Text = dataelearning_detail.el_training_course_action.Count().ToString();

        int iError = 0;
        if (dataelearning_detail.el_training_course_action != null)
        {
            pnl_permission.Visible = false;
            pnl_gvdata.Visible = true;
            dv_course.Visible = true;
        }
        else
        {
            pnl_permission.Visible = true;
            pnl_gvdata.Visible = false;
            dv_course.Visible = false;
            iError++;
        }
        //Litdebug.Text = iError.ToString();
        if (iError == 0)
        {
            dataelearning_detail = new data_elearning();
            dataelearning_detail.el_training_course_action = new training_course[1];
            obj_detail = new training_course();
            obj_detail.u0_training_course_idx = id;
            obj_detail.emp_idx_ref = emp_idx;
            obj_detail.operation_status_id = "course_test";
            obj_detail.zstatus = ViewState["_type"].ToString();
            dataelearning_detail.el_training_course_action[0] = obj_detail;
            // Litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning_detail));
            dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);

            if (dataelearning_detail.el_training_course_action != null)
            {

                obj_detail = dataelearning_detail.el_training_course_action[0];
                litcourse.Text = obj_detail.training_course_no + " - " + obj_detail.zcourse_name;
                _func_dmu.zSetGridData(Gvinstitution, ViewState["vsel_u1_training_course_lecturer"]);
                //if (pnl_permission.Visible == false)
                //{
                    DataSet ds = (DataSet)ViewState["vsel_tc_test"];
                    DataRow dr;
                    int i = 0;
                    foreach (var v_item in dataelearning_detail.el_training_course_action)
                    {
                        i++;
                        dr = ds.Tables["dsel_tc_test"].NewRow();
                        dr["id"] = i.ToString();
                        dr["u0_training_course_idx"] = v_item.u0_training_course_idx.ToString();
                        dr["u2_course_idx"] = v_item.u2_course_idx.ToString();
                        dr["course_item"] = v_item.course_item.ToString();
                        dr["training_course_item"] = i.ToString();
                        dr["course_proposition"] = v_item.course_proposition;
                        dr["course_propos_a"] = v_item.course_propos_a;
                        dr["course_propos_b"] = v_item.course_propos_b;
                        dr["course_propos_c"] = v_item.course_propos_c;
                        dr["course_propos_d"] = v_item.course_propos_d;
                        dr["course_proposition_img"] = v_item.course_proposition_img;
                        dr["course_propos_a_img"] = v_item.course_propos_a_img;
                        dr["course_propos_b_img"] = v_item.course_propos_b_img;
                        dr["course_propos_c_img"] = v_item.course_propos_c_img;
                        dr["course_propos_d_img"] = v_item.course_propos_d_img;
                        dr["course_propos_answer"] = v_item.course_propos_answer;
                        dr["course_no"] = v_item.course_no;

                        ds.Tables["dsel_tc_test"].Rows.Add(dr);
                    }
                    ViewState["vsel_tc_test"] = ds;
                    _func_dmu.zSetGridData(GvListData, ViewState["vsel_tc_test"]);

                //}
                //else
                //{
                //   // pnl_gvdata.Visible = false;
                //}
               
            }
            else
            {
                // iError = iError + checkcourse_test_permission(1);
                checkcourse_test_permission_result();
            }
            if (iError == 0)
            {
                
            }
        }
        
        

        SETFOCUS.Focus();

    }
    private Boolean CheckErrorPermission()
    {
        int _id = _func_dmu.zStringToInt(ViewState["_id"].ToString());
        Boolean _Boolean = false;
        data_elearning dataelearning_detail;
        training_course obj_detail;
        dataelearning_detail = new data_elearning();
        dataelearning_detail.el_training_course_action = new training_course[1];
        obj_detail = new training_course();
        obj_detail.u0_training_course_idx = _id;
        obj_detail.emp_idx_ref = emp_idx;
        obj_detail.operation_status_id = "U3-FULL-PERMISSION";
        dataelearning_detail.el_training_course_action[0] = obj_detail;
        dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
        if (dataelearning_detail.el_training_course_action == null)
        {
            _Boolean = true;
        }
        dataelearning_detail = new data_elearning();
        dataelearning_detail.el_training_course_action = new training_course[1];
        obj_detail = new training_course();
        obj_detail.u0_training_course_idx = _id;
        obj_detail.emp_idx_ref = emp_idx;
        obj_detail.operation_status_id = "course_test";
        obj_detail.zstatus = ViewState["_type"].ToString();
        dataelearning_detail.el_training_course_action[0] = obj_detail;
        dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
        if (dataelearning_detail.el_training_course_action == null)
        {
            _Boolean = true;
        }

        return _Boolean;
    }
    protected void rb_Yes_Click(object sender, EventArgs e)
    {
        RadioButton rb_Yes = (RadioButton)sender;
        GridViewRow grid_row = (GridViewRow)rb_Yes.NamingContainer;
        if (((RadioButton)grid_row.FindControl("rb_Yes")).Checked == true)
        {
            //Action that you want to implement
        }
    }
    protected void rb_No_Click(object sender, EventArgs e)
    {
        RadioButton rb_Yes = (RadioButton)sender;
        GridViewRow grid_row = (GridViewRow)rb_Yes.NamingContainer;
        if (((RadioButton)grid_row.FindControl("rb_No")).Checked == true)
        {
            //Action that you want to implement
        }
    }
    private void CreateDs_el_tc_test()
    {
        string sDs = "dsel_tc_test";
        string sVs = "vsel_tc_test";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_no", typeof(String));
        ds.Tables[sDs].Columns.Add("zcourse_name", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u2_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("course_item", typeof(String));
        ds.Tables[sDs].Columns.Add("course_proposition", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_a", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_b", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_c", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_d", typeof(String));
        ds.Tables[sDs].Columns.Add("course_proposition_img", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_a_img", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_b_img", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_c_img", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_d_img", typeof(String));
        ds.Tables[sDs].Columns.Add("course_propos_answer", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_item", typeof(String));
        ds.Tables[sDs].Columns.Add("doc_no", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_propos_answer", typeof(String));
        ds.Tables[sDs].Columns.Add("course_no", typeof(String));
        ViewState[sVs] = ds;

    }
    protected void onRowDataBound(Object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        string sGvName = GvName.ID;
        if (sGvName == "GvListData")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbcourse_propos_a = (Label)e.Row.Cells[0].FindControl("lbcourse_propos_a");
                Label lbcourse_propos_b = (Label)e.Row.Cells[0].FindControl("lbcourse_propos_b");
                Label lbcourse_propos_c = (Label)e.Row.Cells[0].FindControl("lbcourse_propos_c");
                Label lbcourse_propos_d = (Label)e.Row.Cells[0].FindControl("lbcourse_propos_d");
                Label lbcourse_propos_a_img = (Label)e.Row.Cells[0].FindControl("lbcourse_propos_a_img");
                Label lbcourse_propos_b_img = (Label)e.Row.Cells[0].FindControl("lbcourse_propos_b_img");
                Label lbcourse_propos_c_img = (Label)e.Row.Cells[0].FindControl("lbcourse_propos_c_img");
                Label lbcourse_propos_d_img = (Label)e.Row.Cells[0].FindControl("lbcourse_propos_d_img");
                Label lbcourse_no = (Label)e.Row.Cells[0].FindControl("lbcourse_no");
                Label lbcourse_item = (Label)e.Row.Cells[0].FindControl("lbcourse_item");
                Label lbcourse_proposition_img = (Label)e.Row.Cells[0].FindControl("lbcourse_proposition_img");
                Label lbtraining_course_item = (Label)e.Row.Cells[0].FindControl("lbtraining_course_item");

                RadioButtonList rdoItem = (RadioButtonList)e.Row.Cells[0].FindControl("rdoItem");
                Image img_proposition = (Image)e.Row.Cells[0].FindControl("img_proposition");

                int ic = 0;
                string _img = "", _label = "";
                ListItem item;


                // โจทย์
                _img = getImgUrl(lbcourse_no.Text, lbcourse_item.Text, lbcourse_proposition_img.Text);
                img_proposition.ImageUrl = ResolveUrl(_img);
                img_proposition.Visible = false;
                if (_img != "")
                {
                    img_proposition.Visible = true;
                }
                //1
                ic++;
                _img = getImgUrl(lbcourse_no.Text, lbcourse_item.Text, lbcourse_propos_a_img.Text);
                _label = "ก)&nbsp; " + lbcourse_propos_a.Text;
                item = getListItem(_label, _img, ic.ToString());
                rdoItem.Items.Add(item);

                //2
                ic++;
                _img = getImgUrl(lbcourse_no.Text, lbcourse_item.Text, lbcourse_propos_b_img.Text);
                _label = "ข)&nbsp; " + lbcourse_propos_b.Text;
                item = getListItem(_label, _img, ic.ToString());
                rdoItem.Items.Add(item);

                //3
                ic++;
                _img = getImgUrl(lbcourse_no.Text, lbcourse_item.Text, lbcourse_propos_c_img.Text);
                _label = "ค)&nbsp; " + lbcourse_propos_c.Text;
                item = getListItem(_label, _img, ic.ToString());
                rdoItem.Items.Add(item);

                //4
                ic++;
                _img = getImgUrl(lbcourse_no.Text, lbcourse_item.Text, lbcourse_propos_d_img.Text);
                _label = "ง)&nbsp; " + lbcourse_propos_d.Text;
                item = getListItem(_label, _img, ic.ToString());
                rdoItem.Items.Add(item);


            }


        }
        else if (sGvName == "GvListResult")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbgrade_status = (Label)e.Row.Cells[0].FindControl("lbresult_flag");
                Panel pnlicon_yes = (Panel)e.Row.Cells[0].FindControl("pnlicon_yes");
                Panel pnlicon_no = (Panel)e.Row.Cells[0].FindControl("pnlicon_no");
                pnlicon_yes.Visible = false;
                pnlicon_no.Visible = false;
                //if (_func_dmu.zStringToInt(lbgrade_status.Text) == 1)
                //{
                //    pnlicon_yes.Visible = true;
                //    pnlicon_no.Visible = false;
                //}
                //else 
                //{
                //    pnlicon_yes.Visible = false;
                //    pnlicon_no.Visible = true;
                //}
            }
        }

    }
    private ListItem getListItem(string _course_propos, string _img, string course_item)
    {
        ListItem item;
        if (_img == "")
        {
            item = new ListItem(_course_propos + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", course_item);
        }
        else
        {
            item = new ListItem(_course_propos + "&nbsp;&nbsp;&nbsp;<img src='" + ResolveUrl(_img) + "' alt='" + "' title='" + "' width = '50' height = '50' />  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;", course_item);
        }
        return item;
    }
    private void CreateDs_el_u1_training_course_lecturer()
    {
        string sDs = "dsel_u1_training_course_lecturer";
        string sVs = "vsel_u1_training_course_lecturer";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u1_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("lecturer_type", typeof(String));
        ds.Tables[sDs].Columns.Add("m0_institution_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ViewState[sVs] = ds;

    }
    public string getImgUrl(string ADocno = "", string AItem = "", string AImage = "")
    {
        string sPathImage = "";

        if (AImage == "")
        {
            sPathImage = "";
        }
        else
        {
            sPathImage = _gPathFile + _Folder_coursetestimg + "/" + ADocno + "/" + AItem + "/" + AImage;
        }
        sPathImage = ResolveUrl(sPathImage);

        return sPathImage;
    }
    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        int _idx = 0;
        string[] _calc = new string[4];
        switch (cmdName)
        {
            case "btnSaveInsert":
                
                if (checkError() == false)
                {
                    if(CheckErrorPermission() == true)
                    {
                        int _id = _func_dmu.zStringToInt(ViewState["_id"].ToString());
                        setTitle(_id);
                    }
                    else
                    {
                        zSave();
                        
                    }
                    
                    SETFOCUS.Focus();
                }
                break;

            case "btnrefresh":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnhome":
                string sUrl = _func_dmu._LikeMAS  + "trainingcourse-register";
                Session["_session_elearning_test"] = "elearning";
                Page.Response.Redirect(sUrl, true);
                break;
        }

    }
    
    #endregion btnCommand
    private Boolean checkError()
    {
        Boolean _Boolean = false;
        foreach (GridViewRow gr_item in GvListData.Rows)
        {
            RadioButtonList rdoItem = (RadioButtonList)gr_item.FindControl("rdoItem");
            Label lbzName = (Label)gr_item.FindControl("lbzName");
            if (_func_dmu.zStringToInt(rdoItem.SelectedValue) == 0)
            {
                showAlert("กรุณาเลือกคำตอบข้อ : " + lbzName.Text);
                _Boolean = true;
                break;
            }

        }
        return _Boolean;
    }
    private void showAlert(string Str)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + Str + "');", true);
    }
    private void zSave()
    {
        string _fromrunno = "trn_p_c_evaluform";
        string sDocno;
        string m0_prefix = "TRQT";
        sDocno = _func_dmu.zRun_Number(_fromrunno, m0_prefix, "YYMM", "N", "0000");
        int itemObj = 0;
        data_elearning dataelearning = new data_elearning();
        itemObj = 0;
        training_course[] objcourse1 = new training_course[GvListData.Rows.Count];
        foreach (GridViewRow gr_item in GvListData.Rows)
        {
            RadioButtonList rdoItem = (RadioButtonList)gr_item.FindControl("rdoItem");
            Label lbu0_training_course_idx = (Label)gr_item.FindControl("lbu0_training_course_idx");
            Label lbu2_course_idx = (Label)gr_item.FindControl("lbu2_course_idx");
            Label lbcourse_item = (Label)gr_item.FindControl("lbcourse_item");
            Label lbcourse_proposition = (Label)gr_item.FindControl("lbcourse_proposition");
            Label lbcourse_propos_a = (Label)gr_item.FindControl("lbcourse_propos_a");
            Label lbcourse_propos_b = (Label)gr_item.FindControl("lbcourse_propos_b");
            Label lbcourse_propos_c = (Label)gr_item.FindControl("lbcourse_propos_c");
            Label lbcourse_propos_d = (Label)gr_item.FindControl("lbcourse_propos_d");
            Label lbcourse_proposition_img = (Label)gr_item.FindControl("lbcourse_proposition_img");
            Label lbcourse_propos_a_img = (Label)gr_item.FindControl("lbcourse_propos_a_img");
            Label lbcourse_propos_b_img = (Label)gr_item.FindControl("lbcourse_propos_b_img");
            Label lbcourse_propos_c_img = (Label)gr_item.FindControl("lbcourse_propos_c_img");
            Label lbcourse_propos_d_img = (Label)gr_item.FindControl("lbcourse_propos_d_img");
            Label lbcourse_propos_answer = (Label)gr_item.FindControl("lbcourse_propos_answer");
            Label lbcourse_no = (Label)gr_item.FindControl("lbcourse_no");
            Label lbtraining_course_item = (Label)gr_item.FindControl("lbtraining_course_item");

            //if (_func_dmu.zStringToInt(lbcourse_item.Text) > 0)
            //{
            objcourse1[itemObj] = new training_course();

            objcourse1[itemObj].u0_training_course_idx_ref = _func_dmu.zStringToInt(lbu0_training_course_idx.Text);
            objcourse1[itemObj].doc_no = sDocno;
            objcourse1[itemObj].u0_training_course_idx = _func_dmu.zStringToInt(lbu0_training_course_idx.Text);
            objcourse1[itemObj].u2_course_idx = _func_dmu.zStringToInt(lbu2_course_idx.Text);
            objcourse1[itemObj].course_item = _func_dmu.zStringToInt(lbcourse_item.Text);
            objcourse1[itemObj].training_course_item = _func_dmu.zStringToInt(lbtraining_course_item.Text);

            objcourse1[itemObj].course_proposition = lbcourse_proposition.Text;
            objcourse1[itemObj].course_proposition_img = lbcourse_proposition_img.Text;

            objcourse1[itemObj].course_propos_a = lbcourse_propos_a.Text;
            objcourse1[itemObj].course_propos_a_img = lbcourse_propos_a_img.Text;

            objcourse1[itemObj].course_propos_b = lbcourse_propos_b.Text;
            objcourse1[itemObj].course_propos_b_img = lbcourse_propos_b_img.Text;

            objcourse1[itemObj].course_propos_c = lbcourse_propos_c.Text;
            objcourse1[itemObj].course_propos_c_img = lbcourse_propos_c_img.Text;

            objcourse1[itemObj].course_propos_d = lbcourse_propos_d.Text;
            objcourse1[itemObj].course_propos_d_img = lbcourse_propos_d_img.Text;

            objcourse1[itemObj].course_propos_answer = lbcourse_propos_answer.Text;
            objcourse1[itemObj].training_course_propos_answer = rdoItem.SelectedValue;

            if (_func_dmu.zStringToInt(lbcourse_propos_answer.Text) == _func_dmu.zStringToInt(rdoItem.SelectedValue))
            {
                objcourse1[itemObj].result_flag = 1;
            }
            else
            {
                objcourse1[itemObj].result_flag = 0;
            }
            objcourse1[itemObj].emp_idx_ref = emp_idx;
            objcourse1[itemObj].training_course_created_by = emp_idx;
            objcourse1[itemObj].training_course_status = 1;
            objcourse1[itemObj].training_course_updated_by = emp_idx;
            if(ViewState["_type"].ToString() == "elearning")
            {
                objcourse1[itemObj].el_flag = 1;
            }
            else
            {
                objcourse1[itemObj].el_flag = 0;
            }
            objcourse1[itemObj].operation_status_id = "U11";
            itemObj++;
            //}

        }
        dataelearning.el_training_course_action = objcourse1;
        // Litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_plan_course, dataelearning);
        pnl_alert.Visible = false;
        pnldata.Visible = false;
        pnl_complete.Visible = true;
        //checkcourse_test_permission(2);
        // _func_dmu.zSetGridData(GvListResult, dataelearning.el_training_course_action);
        checkcourse_test_permission_result();

    }

    public string getResultScoreTotal(decimal _course_score_avg, int _score_qty)
    {
        decimal _decimal = 0;
        _decimal = _course_score_avg * _score_qty;

        return _func_dmu.zFormatfloat(_decimal.ToString(), 2);
    }

    protected string getStatusLevel(int status)
    {

        if (status == 1)
        {

            return "<span class='statusmaster-online' >ผ่าน</span>";
        }
        else
        {
            return "<span class='statusmaster-offline' >ไม่ผ่าน</span>";
        }
    }

    protected string getCourseAssessmentName(int status)
    {

        if (status == 1)
        {

            return "<span class='' >มีการประเมิน</span>";
        }
        else
        {
            return "<span class='' >ไม่มีการประเมิน</span>";
        }
    }
    private int checkcourse_test_permission(int _istatus)
    {
        int iError = 0;
        int _id = _func_dmu.zStringToInt(ViewState["_id"].ToString());
        data_elearning dataelearning_detail;
        training_course obj_detail;
        lit_alert.Text = "ไม่พบข้อมูล";
        if(_istatus == 1) //select
        {
            pnl_alert.Visible = true;
        }
        else
        {
            pnl_alert.Visible = false;
        }

        pnl_permission.Visible = false;
        pnl_gvdata.Visible = false;
        pnldata.Visible = false;

        dataelearning_detail = new data_elearning();
        dataelearning_detail.el_training_course_action = new training_course[1];
        obj_detail = new training_course();
        obj_detail.u0_training_course_idx = _id;
        obj_detail.emp_idx_ref = emp_idx;
        obj_detail.operation_status_id = "course_test_permission";
        if (ViewState["_type"].ToString() == "elearning")
        {
            obj_detail.el_flag = 1;
        }
        else
        {
            obj_detail.el_flag = 0;
        }
        dataelearning_detail.el_training_course_action[0] = obj_detail;
      //  Litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning_detail));
        dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
        if (dataelearning_detail.el_training_course_action != null)
        {
            pnl_alert.Visible = false;
            pnl_complete.Visible = true;
            
            string sYes, sNo;
            sYes = "คุณได้ผ่านการทดสอบหลังการฝึกอบรมในหลักสูตรนี้แล้ว";
            sNo = "คุณไม่ผ่านการทดสอบหลังการฝึกอบรมในหลักสูตรนี้ครบ 2 ครั้งแล้วกรุณากลับไปเรียนใหม่.";

            foreach (var item in dataelearning_detail.el_training_course_action)
            {
                // Litdebug.Text = item.item_count.ToString();
                if (item.item_count == 2)
                {
                    if (item.result_flag == 1)
                    {
                        lit_result.Text = sYes;
                    }
                    else
                    {
                        lit_result.Text = sNo;

                    }
                    iError++;
                }
                else
                {
                    if (item.result_flag == 1)
                    {
                        lit_result.Text = sYes;
                        iError++;
                    }
                    else
                    {
                        lit_result.Text = "ทดสอบหลังการฝึกอบรมเสร็จแล้ว";

                    }
                }
            }
            if (iError > 0)
            {
                btnrefresh.Visible = false;
               
            }
            else
            {
                btnrefresh.Visible = true;
            }
            _func_dmu.zSetGridData(GvListResult, dataelearning_detail.el_training_course_action);
            
        }

        return iError;

    }

    private void checkcourse_test_permission_result()
    {
        MvMaster.SetActiveView(ViewResult);
        int iError = 0;
        int _id = _func_dmu.zStringToInt(ViewState["_id"].ToString());
        data_elearning dataelearning_detail;
        training_course obj_detail;
        dataelearning_detail = new data_elearning();
        dataelearning_detail.el_training_course_action = new training_course[1];
        obj_detail = new training_course();
        obj_detail.u0_training_course_idx = _id;
        obj_detail.emp_idx_ref = emp_idx;
        obj_detail.operation_status_id = "course_test_permission";
        if (ViewState["_type"].ToString() == "elearning")
        {
            obj_detail.el_flag = 1;
        }
        else
        {
            obj_detail.el_flag = 0;
        }
        dataelearning_detail.el_training_course_action[0] = obj_detail;
        //  Litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning_detail));
        dataelearning_detail = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_detail);
        if (dataelearning_detail.el_training_course_action != null)
        {
            pnl_alert.Visible = false;
            pnl_complete.Visible = true;

            string sYes, sNo;
            sYes = "คุณได้ผ่านการทดสอบหลังการฝึกอบรมในหลักสูตรนี้แล้ว";
            sNo = "คุณไม่ผ่านการทดสอบหลังการฝึกอบรมในหลักสูตรนี้ครบ 2 ครั้งแล้วกรุณากลับไปเรียนใหม่.";

            foreach (var item in dataelearning_detail.el_training_course_action)
            {
                if (item.item_count == 2)
                {
                    if (item.result_flag == 1)
                    {
                        lit_result.Text = sYes;
                    }
                    else
                    {
                        lit_result.Text = sNo;

                    }
                    iError++;
                }
                else
                {
                    if (item.result_flag == 1)
                    {
                        lit_result.Text = sYes;
                        iError++;
                    }
                    else
                    {
                        lit_result.Text = "ทดสอบหลังการฝึกอบรมเสร็จแล้ว";

                    }
                }
            }
            if (iError > 0)
            {
                btnrefresh.Visible = false;

            }
            else
            {
                btnrefresh.Visible = true;
            }
            _func_dmu.zSetGridData(GvListResult, dataelearning_detail.el_training_course_action);

        }

        
    }

}