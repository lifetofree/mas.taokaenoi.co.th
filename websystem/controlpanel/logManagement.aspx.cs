﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

public partial class websystem_controlpanel_logManagement : System.Web.UI.Page
{

    #region Connect
    //DBConn DBConn = new DBConn();
    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    data_log _dtlog = new data_log();
    function_scan _funcScan = new function_scan();
    function_db _funcdb = new function_db();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlSetLog_11 = _serviceUrl + ConfigurationManager.AppSettings["urlSetLog"];
    static string _urlGetLog_20 = _serviceUrl + ConfigurationManager.AppSettings["urlGetLog"];
    static string _urlGetLogDetail_21 = _serviceUrl + ConfigurationManager.AppSettings["urlGetLogDetail"];
    static string _urlurlGetEventTypes_90 = _serviceUrl + ConfigurationManager.AppSettings["urlGetEventTypes"];

    int emp_idx = 0;
    int defaultInt = 0;

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            Defult();
        }
    }

    #region SELECT - INSERT - VIEW - SEARCH

    protected void Defult()
    {
        ViewState["search_logdetail"] = "0";
        ViewState["search_Eventtype"] = "0";
        ViewState["search_empcode"] = "0";
        ViewState["search_ipaddress"] = "0";
        ViewState["search_empname"] = "0";
        ViewState["search_SearchDate"] = "0";

        Menu_Color(1);
        MvMaster.SetActiveView(ViewIndex);
        Select_Gridview_Log();

        Fv_Search_log_Index.ChangeMode(FormViewMode.Insert);
        Fv_Search_log_Index.DataBind();

        Select_Checkbox_event_type();
    }

    protected void Select_Gridview_Log()
    {
        _dtlog.search_key_list = new search_key[1];
        search_key _data_s_log_detail = new search_key();

        _data_s_log_detail.s_log_detail = ViewState["search_logdetail"].ToString();
        _data_s_log_detail.s_emp_code = ViewState["search_empcode"].ToString();
        _data_s_log_detail.s_ip_address = ViewState["search_ipaddress"].ToString();
        _data_s_log_detail.s_emp_name_th = ViewState["search_empname"].ToString();
        _data_s_log_detail.s_m0_event_type = ViewState["search_Eventtype"].ToString();
        _data_s_log_detail.s_create_date = ViewState["search_SearchDate"].ToString();

        _dtlog.search_key_list[0] = _data_s_log_detail;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtlog)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        _dtlog = callServicePostLogs(_urlGetLog_20, _dtlog);
        setGridData(Gv_Log, _dtlog.u0_log_list);
    }

    protected void Select_Fromview_Log(int uidx)
    {
        _dtlog.search_key_list = new search_key[1];
        search_key _data_s_log_detail = new search_key();

        _data_s_log_detail.s_uidx = uidx.ToString();

        _dtlog.search_key_list[0] = _data_s_log_detail;
        _dtlog = callServicePostLogs(_urlGetLogDetail_21, _dtlog);
        setFormData(FvViewDetaillog, _dtlog.u0_log_list);

        //fs.Text = uidx.ToString();//HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtlog)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        //string xml_log_detail = "<DataOrganization><RPosition1><RPosIDX>4700</RPosIDX><PosIDX>4884</PosIDX></RPosition1></DataOrganization>";//_funcTool.convertObjectToXml(_dtlog.u0_log_list[0].log_detail); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML

        //string xml_ = HttpUtility.HtmlDecode(_funcTool.convertObjectToXml(_dtlog.u0_log_list[0].log_detail));
        //string xml_Replace_1 = xml_.Replace("<string>", "");
        //gs.Text = PrintXML(xml_log_detail);

        TextBox txt_view_log_detail = (TextBox)FvViewDetaillog.FindControl("txt_view_log_detail");
        data_log dl = (data_log)_funcTool.convertXmlToObject(typeof(data_log), _dtlog.u0_log_list[0].log_detail);
        txt_view_log_detail.Text = string.Format("{0}", _funcTool.prettyPrint(dl.u0_log_list[0].log_detail));
    }

    protected void Select_Fromview_Search()
    {
        _dtlog.u0_log_list = new u0_log_detail[1];
        u0_log_detail _data_log_detail = new u0_log_detail();

        //_data_log_detail.uidx = uidx;

        _dtlog.u0_log_list[0] = _data_log_detail;
        _dtlog = callServicePostLogs(_urlGetLogDetail_21, _dtlog);
        setFormData(FvViewDetaillog, _dtlog.u0_log_list);
    }

    protected void Select_Checkbox_event_type()
    {
        CheckBoxList YrChkBox = (CheckBoxList)Fv_Search_log_Index.FindControl("YrChkBox");

        YrChkBox.Items.Clear();
        YrChkBox.AppendDataBoundItems = true;
        _dtlog = new data_log();

        _dtlog.m0_event_list = new m0_event_type[1];
        m0_event_type _data_event_detail = new m0_event_type();

        _data_event_detail.midx = 0;

        _dtlog.m0_event_list[0] = _data_event_detail;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtlog)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML
        _dtlog = callServicePostLogs(_urlurlGetEventTypes_90, _dtlog);

        YrChkBox.DataSource = _dtlog.m0_event_list;
        YrChkBox.DataTextField = "event_type";
        YrChkBox.DataValueField = "midx";
        YrChkBox.DataBind();
    }
    #endregion

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }
    #endregion

    #region callService

    protected data_log callServiceLogs(string _cmdUrl, data_log _data_device)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_device);
        //fss.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        //fss.Text = _cmdUrl + _localJson;

        // convert json to object
        _data_device = (data_log)_funcTool.convertJsonToObject(typeof(data_log), _localJson);

        return _data_device;
    }

    protected data_log callServicePostLogs(string _cmdUrl, data_log _data_device)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_device);
        //text.Text =  _cmdUrl + _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fs.Text = _cmdUrl + _localJson;

        // convert json to object
        _data_device = (data_log)_funcTool.convertJsonToObject(typeof(data_log), _localJson);

        return _data_device;
    }

    protected data_log callServicePostLogs_Post(string _cmdUrl, data_log _data_device)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_device);
        fs.Text =  _cmdUrl + _localJson;

        // call services
        //_localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fs.Text =  _cmdUrl + _localJson;
        //_data_device = _funcTool.convertJsonToXml(_localJson);
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertJsonToXml(_localJson)); //เพิ่มบรรทัดนี้ลงไปเพื่อดู XML

        // convert json to object
        _data_device = (data_log)_funcTool.convertJsonToObject(typeof(data_log), _localJson);

        return _data_device;
    }


    #endregion

    #region FvDetail_DataBound
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {
            case "FvViewDetaillog":
                if (FvViewDetaillog.CurrentMode == FormViewMode.Edit)
                {

                }
            break;
        }
    }
    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_Log":
                Gv_Log.PageIndex = e.NewPageIndex;
                Gv_Log.DataBind();
                Select_Gridview_Log();
                break;
        }
    }
    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        switch (ddName.ID)
        {
            case "ddlSearchDate":
                DropDownList ddlSearchDate_1 = (DropDownList)Fv_Search_log_Index.FindControl("ddlSearchDate");
                TextBox AddEndDate_1 = (TextBox)Fv_Search_log_Index.FindControl("AddEndDate");
                if (int.Parse(ddlSearchDate_1.SelectedValue) == 3)
                {
                    AddEndDate_1.Enabled = true;
                }
                else
                {
                    AddEndDate_1.Enabled = false;
                    AddEndDate_1.Text = string.Empty;
                }
                break;
        }
    }
    #endregion

    #region Switch-Munu Color
    protected void Menu_Color(int choice)
    {
        switch (choice)
        {
            case 1: //Index
                lbindex.BackColor = System.Drawing.Color.LightGray;
                boxsearch.Visible = true;
                boxindex.Visible = true;
                boxview.Visible = false;
                break;
            case 2: //View
                lbindex.BackColor = System.Drawing.Color.LightGray;
                boxsearch.Visible = false;
                boxindex.Visible = false;
                boxview.Visible = true;
                break;
        }
    }
    #endregion

    #region BTN
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "btnIndex":
                Menu_Color(1);
                MvMaster.SetActiveView(ViewIndex);
                break;
            case "btnView_log":
                Menu_Color(2);
                MvMaster.SetActiveView(ViewIndex);
                FvViewDetaillog.ChangeMode(FormViewMode.Edit);
                
                int tmep_uidx = int.Parse(cmdArg.ToString());
                //fs.Text = tmep_uidx.ToString();
                Select_Fromview_Log(tmep_uidx);
                txtfocus.Focus();
                break;
            case "btn_search_index":

                TextBox txt_search_logdetail = (TextBox)Fv_Search_log_Index.FindControl("txt_search_logdetail");
                TextBox txt_search_empcode = (TextBox)Fv_Search_log_Index.FindControl("txt_search_empcode");
                TextBox txt_search_ipaddress = (TextBox)Fv_Search_log_Index.FindControl("txt_search_ipaddress");
                TextBox txt_search_empname = (TextBox)Fv_Search_log_Index.FindControl("txt_search_empname");
                DropDownList ddlSearchDate = (DropDownList)Fv_Search_log_Index.FindControl("ddlSearchDate");
                TextBox AddStartdate = (TextBox)Fv_Search_log_Index.FindControl("AddStartdate");
                TextBox AddEndDate = (TextBox)Fv_Search_log_Index.FindControl("AddEndDate");
                CheckBoxList chk_search_Eventtype = (CheckBoxList)Fv_Search_log_Index.FindControl("YrChkBox");

                List<String> YrStrList = new List<string>();
                foreach (ListItem item in chk_search_Eventtype.Items)
                {
                    if (item.Selected)
                    {
                        YrStrList.Add(item.Value);
                    }
                }
                String YrStr = String.Join(",", YrStrList.ToArray());
                
                ViewState["search_logdetail"] = txt_search_logdetail.Text;
                ViewState["search_empcode"] = txt_search_empcode.Text;
                ViewState["search_ipaddress"] = txt_search_ipaddress.Text;
                ViewState["search_empname"] = txt_search_empname.Text;
                ViewState["search_Eventtype"] = YrStr;

                switch (ddlSearchDate.SelectedValue)
                {
                    case "1": // > || &gt; ''dd/MM/YYYY HH:ss''
                        // ViewState["search_SearchDate"] = "&gt; '" + AddStartdate.Text + "'";
                        ViewState["search_SearchDate"]  = HttpUtility.UrlEncode(" > '" + AddStartdate.Text + "'");
                        break;
                    case "2": // < || &lt; ''dd/MM/YYYY HH:ss''
                        // ViewState["search_SearchDate"] = "&lt; '" + AddStartdate.Text + "'";
                        ViewState["search_SearchDate"]  = HttpUtility.UrlEncode(" < '" + AddStartdate.Text + "'");
                        break;
                    case "3": // BETWEEN || BETWEEN ''24/05/2017 00:00'' AND ''24/05/2017 23:59''
                        // ViewState["search_SearchDate"] = "BETWEEN '" + AddStartdate.Text + "' AND '" + AddEndDate.Text + "'";
                        ViewState["search_SearchDate"]  = HttpUtility.UrlEncode("BETWEEN '" + AddStartdate.Text + "' AND '" + AddEndDate.Text + "'");
                        break;
                }

                //fs.Text = HttpUtility.HtmlEncode(AddStartdate.Text);//ViewState["search_SearchDate"].ToString();
                // litDebug.Text = ('a,<d>').Replace("&lt;", "<").Replace("&gt;", ">");//HttpUtility.UrlEncode(">'" + AddStartdate.Text + "'");
                Select_Gridview_Log();
                break;
            case "btn_search_clear":
                Fv_Search_log_Index.ChangeMode(FormViewMode.Insert);
                Fv_Search_log_Index.DataBind();

                Select_Checkbox_event_type();
                break;
            case "btn_view_back":
                Menu_Color(1);
                MvMaster.SetActiveView(ViewIndex);
                break;
        }
    }
    #endregion
}