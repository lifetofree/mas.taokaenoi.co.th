﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="status-alert.aspx.cs" Inherits="websystem_statal_ipaddress" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
   <asp:Literal ID="litDebug" runat="server" />
   <!-- Start Search Pingable Report -->
   <div id="divSearchPingableReport" runat="server" visible="true">
      <div class="col-md-12">
         <asp:UpdatePanel ID="panelSearchPingableReport" runat="server">
            <ContentTemplate>
               <div class="panel panel-info">
                  <div class="panel-heading f-bold f-s-12">Search</div>
                  <div class="panel-body">
                     <div class="form-inline text-center">
                        <div class="form-group">
                           <label for="txtSearchFrom">From :</label>
                           <div class='input-group date'>
                              <asp:HiddenField ID="txtSearchFromHidden" runat="server" />
                              <asp:TextBox ID="txtSearchFrom" runat="server" placeholder="From date time..."
                                 CssClass="form-control datetimepicker-from cursor-pointer" ReadOnly="true" />
                              <span class="input-group-addon show-from-onclick">
                                 <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                              <span class="input-group-addon clear-from-onclick">
                                 <span class="fa fa-times"></span>
                              </span>
                           </div>
                           <label for="txtSearchTo">To :</label>
                           <div class='input-group date'>
                              <asp:HiddenField ID="txtSearchToHidden" runat="server" />
                              <asp:TextBox ID="txtSearchTo" runat="server" placeholder="To date time..."
                                 CssClass="form-control datetimepicker-to cursor-pointer" ReadOnly="true" />
                              <span class="input-group-addon show-to-onclick">
                                 <span class="glyphicon glyphicon-calendar"></span>
                              </span>
                              <span class="input-group-addon clear-to-onclick">
                                 <span class="fa fa-times"></span>
                              </span>
                           </div>
                        </div>
                     </div>
                     <br />
                     <div class="form-inline text-center">
                        <div class="form-group">
                           <label for="keywordSearch">Keyword :</label>
                           <asp:TextBox ID="keywordSearch" runat="server" CssClass="form-control" placeholder="IP Address, Status..." />
                           <asp:LinkButton ID="btnSearchReport" runat="server" OnCommand="btnCommand" CommandName="btnSearchReport" CssClass="btn btn-primary"><i class="fa fa-search"></i></asp:LinkButton>
                        </div>
                     </div>
                  </div>
               </div>
            </ContentTemplate>
            <Triggers>
               <asp:PostBackTrigger ControlID="btnSearchReport" />
            </Triggers>
         </asp:UpdatePanel>
      </div>
   </div>
   <!-- End Search Pingable Report -->

   <!-- Start Button To Show Report -->
   <div class="col-md-12">
      <asp:UpdatePanel ID="panelChangeView" runat="server">
         <ContentTemplate>
            <asp:LinkButton ID="btnToReportChart" runat="server" OnCommand="btnCommand" CommandName="btnToReportChart" CssClass="pull-right btn btn-info"><i class="fa fa-bar-chart"></i> Chart</asp:LinkButton>
            <asp:LinkButton ID="btnToReportTable" runat="server" OnCommand="btnCommand" CommandName="btnToReportTable" CssClass="pull-right btn btn-info m-r-10"><i class="fa fa-table" aria-hidden="true"></i> Table</asp:LinkButton>
         </ContentTemplate>
         <Triggers>
            <asp:PostBackTrigger ControlID="btnToReportTable" />
            <asp:PostBackTrigger ControlID="btnToReportChart" />
         </Triggers>
      </asp:UpdatePanel>
   </div>
   <!-- End Button To Show Report -->

   <!-- Start Gridview Pingable Report -->
   <div id="divReportTable" runat="server">
      <div class="col-md-12">
         <asp:UpdatePanel ID="panelGvPingableReport" runat="server">
            <ContentTemplate>
               <asp:GridView ID="gvPingableReport"
                  runat="server"
                  AutoGenerateColumns="false"
                  DataKeyNames="l0_ipaddress_idx"
                  CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                  HeaderStyle-CssClass="info"
                  AllowPaging="true"
                  PageSize="10"
                  AutoPostBack="False"
                  OnPageIndexChanging="OnPageIndexChanging">
                  <PagerStyle CssClass="pageCustom" />
                  <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                  <EmptyDataTemplate>
                     <div style="text-align: center">No result</div>
                  </EmptyDataTemplate>
                  <Columns>
                     <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                        <ItemTemplate>
                           <small>
                              <%# (Container.DataItemIndex + 1) %>
                           </small>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Created at" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                        <ItemTemplate>
                           <small>
                              <asp:Label ID="ipaddressCreatedAt" runat="server" Text='<%# Eval("ipaddress_created_at") %>' />
                           </small>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="IP Address" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="35%">
                        <ItemTemplate>
                           <small>
                              <asp:Label ID="ipaddressName" runat="server" Text='<%# Eval("ipaddress_name") %>' />
                           </small>
                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                        <ItemTemplate>
                           <small>
                              <asp:Label ID="ipaddressStatus" runat="server" Text='<%# Eval("ipaddress_status") %>' />
                           </small>
                        </ItemTemplate>
                     </asp:TemplateField>
                  </Columns>
               </asp:GridView>
            </ContentTemplate>
            <Triggers>
               <asp:AsyncPostBackTrigger ControlID="gvPingableReport" EventName="PageIndexChanging" />
            </Triggers>
         </asp:UpdatePanel>
      </div>
   </div>
   <!-- End Gridview Pingable Report -->

   <!-- Start Chart Pingable Report -->
   <div id="divReportChart" runat="server">
      <div class="col-md-12">
         <div class="panel panel-info m-t-10">
            <div class="panel-heading f-bold f-s-12">Chart</div>
            <div class="panel-body">
               <asp:Literal ID="litReportChart" runat="server" />
            </div>
         </div>
      </div>
   </div>
   <!-- End Chart Pingable Report -->

   <!-- Start JS Area -->
   <script type="text/javascript">
      $('.clear-from-onclick').click(function () {
         $('.datetimepicker-from').val('');
         $('#<%= txtSearchFromHidden.ClientID %>').val('');
      });
      $('.clear-to-onclick').click(function () {
         $('.datetimepicker-to').val('');
         $('#<%= txtSearchToHidden.ClientID %>').val('');
      });
      $('.datetimepicker-from').datetimepicker({
         format: 'DD/MM/YYYY HH:mm',
         ignoreReadonly: true
      });
      $('.datetimepicker-from').on('dp.change', function (e) {
         if ($(this).val() != "") {
            var dateChangeAnnounceVal = $(this).val();
            var splitDateChangeAnnounce = dateChangeAnnounceVal.split("/");
            var interDateChangeAnnounce = splitDateChangeAnnounce[2] + '-' + splitDateChangeAnnounce[1] + '-' + splitDateChangeAnnounce[0];
            $('.datetimepicker-to').data("DateTimePicker").minDate(e.date);
            $('#<%= txtSearchFromHidden.ClientID %>').val($('.datetimepicker-from').val());
         }
      });
      $('.show-from-onclick').click(function () {
         $('.datetimepicker-from').data("DateTimePicker").show();
      });

      $('.datetimepicker-to').datetimepicker({
         format: 'DD/MM/YYYY HH:mm',
         ignoreReadonly: true
      });
      $('.show-to-onclick').click(function () {
         $('.datetimepicker-to').data("DateTimePicker").show();
      });
      $('.datetimepicker-to').on('dp.change', function (e) {
         if ($(this).val() != "") {
            var dateChangeAnnounceVal = $(this).val();
            var splitDateChangeAnnounce = dateChangeAnnounceVal.split("/");
            var interDateChangeAnnounce = splitDateChangeAnnounce[2] + '-' + splitDateChangeAnnounce[1] + '-' + splitDateChangeAnnounce[0];
            $('#<%= txtSearchToHidden.ClientID %>').val($('.datetimepicker-to').val());
         }
      });

      var prm = Sys.WebForms.PageRequestManager.getInstance();
      prm.add_endRequest(function () {
         $('.clear-from-onclick').click(function () {
            $('.datetimepicker-from').val('');
            $('#<%=txtSearchFromHidden.ClientID%>').val('');
         });
         $('.clear-to-onclick').click(function () {
            $('.datetimepicker-to').val('');
            $('<%=txtSearchToHidden.ClientID%>').val('');
         });
         $('.datetimepicker-from').datetimepicker({
            format: 'DD/MM/YYYY HH:mm',
            ignoreReadonly: true
         });
         $('.datetimepicker-from').on('dp.change', function (e) {
            if ($(this).val() != "") {
               var dateChangeAnnounceVal = $(this).val();
               var splitDateChangeAnnounce = dateChangeAnnounceVal.split("/");
               var interDateChangeAnnounce = splitDateChangeAnnounce[2] + '-' + splitDateChangeAnnounce[1] + '-' + splitDateChangeAnnounce[0];
               $('.datetimepicker-to').data("DateTimePicker").minDate(e.date);
               $('#<%= txtSearchFromHidden.ClientID %>').val($('.datetimepicker-from').val());
            }
         });
         $('.show-from-onclick').click(function () {
            $('.datetimepicker-from').data("DateTimePicker").show();
         });

         $('.datetimepicker-to').datetimepicker({
            format: 'DD/MM/YYYY HH:mm',
            ignoreReadonly: true
         });
         $('.show-to-onclick').click(function () {
            $('.datetimepicker-to').data("DateTimePicker").show();
         });
         $('.datetimepicker-to').on('dp.change', function (e) {
            if ($(this).val() != "") {
               var dateChangeAnnounceVal = $(this).val();
               var splitDateChangeAnnounce = dateChangeAnnounceVal.split("/");
               var interDateChangeAnnounce = splitDateChangeAnnounce[2] + '-' + splitDateChangeAnnounce[1] + '-' + splitDateChangeAnnounce[0];
               $('#<%= txtSearchToHidden.ClientID %>').val($('.datetimepicker-to').val())
            }
         });
      });
   </script>
   <!-- End JS Area -->
</asp:Content>
