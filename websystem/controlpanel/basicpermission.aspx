﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="basicpermission.aspx.cs" Inherits="websystem_controlpanel_basicpermission" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <div class="col-md-12" role="main">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>

        <!--*** START Topic Permission By Department ***-->

        <div class="col-md-12">
            <div id="fvPermissionDep" runat="server" class="panel panel-info">

                <div class="panel-heading">Permission By Section</div>


            </div>
        </div>

        <!--*** END Topic Permission By Department ***-->

        <!--*** START Search Index ***-->
        <div class="form-group">
            <div class="col-md-12">

                <div class="form-group">
                    <asp:LinkButton ID="btnCheckPermission" CssClass="btn btn-success" runat="server" Visible="false" data-original-title="เช็คสิทธิ์" data-toggle="tooltip" CommandName="cmdCheckPermission" OnCommand="btnCommand">
                     <span class="glyphicon glyphicon-user"></span> เช็คสิทธิ์</asp:LinkButton>

                    <asp:LinkButton ID="BackbtnCheckPermission" CssClass="btn btn-default" runat="server" data-original-title="กลับ" data-toggle="tooltip" CommandName="cmdBackPermission" OnCommand="btnCommand" Visible="false">
                     <span class="glyphicon glyphicon-arrow-left"></span> กลับ</asp:LinkButton>
                </div>

                <asp:Panel ID="PanelPermission" runat="server" Visible="false">
                    <%-- <div id="showsearch" runat="server" visible="true">--%>
                    <div class="panel panel-default">
                        <div class="panel-heading f-bold">ค้นหาระบบตามสิทธื์</div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">

                                    <%-- <asp:UpdatePanel ID="update_test11" runat="server" UpdateMode="Always">
                                            <ContentTemplate>--%>
                                    <label class="col-sm-2 control-label">องค์กร</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlOrg_checkpermission" runat="server" ValidationGroup="SearchPermission" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="Re_ddlOrg_checkpermission" 
                                            ValidationGroup="SearchPermission" 
                                            runat="server" Display="None"
                                            ControlToValidate="ddlOrg_checkpermission" Font-Size="11"
                                            ErrorMessage="เลือกองค์กร"
                                            ValidationExpression="เลือกองค์กร" InitialValue="-1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlOrg_checkpermission" Width="160" />
                                    </div>
                                    <%--    </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlOrg_excel" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>--%>
                                    <label class="col-sm-2 control-label">ฝ่าย</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlDept_checkpermission" runat="server" ValidationGroup="SearchPermission" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="Re_ddlDept_checkpermission" 
                                            ValidationGroup="SearchPermission" runat="server" Display="None"
                                            ControlToValidate="ddlDept_checkpermission" Font-Size="11"
                                            ErrorMessage="เลือกฝ่าย"
                                            ValidationExpression="เลือกฝ่าย" InitialValue="-1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlDept_checkpermission" Width="160" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แผนก</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlSec_checkpermission" runat="server" ValidationGroup="SearchPermission" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="Re_ddlSec_checkpermission" 
                                            ValidationGroup="SearchPermission" runat="server" Display="None"
                                            ControlToValidate="ddlSec_checkpermission" Font-Size="11"
                                            ErrorMessage="เลือกแผนก"
                                            ValidationExpression="เลือกแผนก" InitialValue="-1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlSec_checkpermission" Width="160" />
                                    </div>

                                    <%-- <div class="col-sm-2">--%>
                                    <%-- <asp:LinkButton ID="btnInsertDeptExcel" CssClass="btn btn-default" runat="server" CommandName="btnInsertDeptExcel" OnCommand="btnCommand" ValidationGroup="saveInsertDeptexcel" title="เพิ่มแผนก"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>--%>
                                    <%--</div>--%>

                                    <label class="col-sm-6 control-label"></label>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ระบบ</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlsystem_checkpermission" runat="server" ValidationGroup="SearchPermission" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                        <asp:RequiredFieldValidator ID="Re_ddlsystem_checkpermission" 
                                            ValidationGroup="SearchPermission" runat="server" Display="None"
                                            ControlToValidate="ddlsystem_checkpermission" Font-Size="11"
                                            ErrorMessage="เลือกระบบ"
                                            ValidationExpression="เลือกระบบ" InitialValue="-1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlsystem_checkpermission" Width="160" />
                                    </div>

                                   

                                    <label class="col-sm-6 control-label"></label>
                                </div>


                                <%-------------- btnsearch --------------%>
                                <div class="form-group">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-4">
                                        <div class="pull-left">
                                            <asp:LinkButton ID="btnSearch_CheckPermission" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchPermission" OnCommand="btnCommand" CommandName="cmdSearchPermission" data-toggle="tooltip" title="เช็คสิทธิ์"><span class="glyphicon glyphicon-search"></span>&nbsp;เช็คสิทธิ์</asp:LinkButton>

                                            <asp:LinkButton ID="btnReset_CheckPermission" CssClass="btn btn-default" Visible="false" runat="server" OnCommand="btnCommand" CommandName="cmdresetSearchPermission" data-toggle="tooltip" title="ล้างค่า"><i class="fa fa-refresh"></i>&nbsp;ล้างค่า</asp:LinkButton>
                                        </div>

                                    </div>
                                </div>
                                <%-------------- btnsearch --------------%>
                            </div>
                        </div>

                    </div>
                    <%-- </div>--%>
                </asp:Panel>


                <asp:Panel ID="Dept_Excel" runat="server" Visible="true">
                    <%-- <div id="showsearch" runat="server" visible="true">--%>
                    <div class="panel panel-default">
                        <div class="panel-heading f-bold">ค้นหา</div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">

                                    <%-- <asp:UpdatePanel ID="update_test11" runat="server" UpdateMode="Always">
                                            <ContentTemplate>--%>
                                    <label class="col-sm-2 control-label">องค์กร</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlOrg_search" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                        <%-- <asp:RequiredFieldValidator ID="RequiredddlOrg_excel" ValidationGroup="saveInsertDeptexcel" runat="server" Display="None"
                                            ControlToValidate="ddlOrg_excel" Font-Size="11" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือกองค์กร"
                                            ValidationExpression="*กรุณาเลือกองค์กร" InitialValue="-1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlOrg_excel" Width="160" />--%>
                                    </div>
                                    <%--    </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlOrg_excel" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>--%>
                                    <label class="col-sm-2 control-label">ฝ่าย</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlDept_search" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                        <%--<asp:RequiredFieldValidator ID="RequiredddlDept_excel" ValidationGroup="saveInsertDeptexcel" runat="server" Display="None"
                                            ControlToValidate="ddlDept_excel" Font-Size="11"
                                            ErrorMessage="ฝ่าย"
                                            ValidationExpression="ฝ่าย" InitialValue="-1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlDept_excel" Width="160" />--%>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แผนก</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlSec_search" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                        <%--<asp:RequiredFieldValidator ID="RequiredddlSec_excel" ValidationGroup="saveInsertDeptexcel" runat="server" Display="None"
                                            ControlToValidate="ddlSec_excel" Font-Size="11"
                                            ErrorMessage="แผนก"
                                            ValidationExpression="แผนก" InitialValue="-1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlSec_excel" Width="160" />--%>
                                    </div>

                                    <%-- <div class="col-sm-2">--%>
                                    <%-- <asp:LinkButton ID="btnInsertDeptExcel" CssClass="btn btn-default" runat="server" CommandName="btnInsertDeptExcel" OnCommand="btnCommand" ValidationGroup="saveInsertDeptexcel" title="เพิ่มแผนก"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>--%>
                                    <%--</div>--%>

                                    <label class="col-sm-6 control-label"></label>
                                </div>


                                <%-------------- btnsearch --------------%>
                                <div class="form-group">
                                    <div class="col-sm-2"></div>
                                    <div class="col-sm-4">
                                        <div class="pull-left">
                                            <asp:LinkButton ID="btnSearchIndexDetail" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchIndex" OnCommand="btnCommand" CommandName="cmdSearchIndexDetail" data-toggle="tooltip" title="ค้นหา"><span class="glyphicon glyphicon-search"></span>&nbsp;ค้นหา</asp:LinkButton>

                                            <asp:LinkButton ID="btnResetSearch" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="cmdresetSearch" data-toggle="tooltip" title="ล้างค่า"><i class="fa fa-refresh"></i>&nbsp;ล้างค่า</asp:LinkButton>
                                        </div>

                                    </div>
                                </div>
                                <%-------------- btnsearch --------------%>
                            </div>
                        </div>

                    </div>
                    <%-- </div>--%>
                </asp:Panel>
            </div>
        </div>
        <!--*** END Search Index ***-->

        <!--*** START Check Selected All ***-->

        <div class="form-group">

            <div class="col-md-12" runat="server" id="chk_permission">
                <%-- <hr />--%>
                <asp:CheckBox ID="chkAll" runat="server" Text="Select all" OnCheckedChanged="Checkbox" AutoPostBack="true" RepeatDirection="Vertical" />

            </div>
        </div>
        <!--*** END Check Selected All ***-->



        <!--*** START Permission By Department ***    -->
        <div class="col-md-12">
            <asp:GridView ID="GvMaster"
                runat="server"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive col-md-12"
                HeaderStyle-CssClass="info"
                HeaderStyle-ForeColor="#31708f"
                AllowPaging="false"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnPageIndexChanging="Master_PageIndexChanging">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div class="text-center">No result</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="No." HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <%# (Container.DataItemIndex +1) %></small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="No.Org" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbOrgIDX" runat="server" Text='<%# Eval("OrgIDX") %>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Organization" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbOrgIDXTH" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="RDepIDX" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbDepIDX" runat="server" Text='<%# Eval("RDeptIDX") %>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Department" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbDepTH" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="RSecIDX" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbRSecIDX" runat="server" Text='<%# Eval("RSecIDX") %>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="SecNameTH" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbSecNameTH" runat="server" Text='<%# Eval("SecNameTH") %>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>



                    <asp:TemplateField HeaderText="Selected" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:CheckBox ID="cbrecipients" runat="server" AutoPostBack="true" OnCheckedChanged="Checkbox" Text='<%# Container.DataItemIndex %>' Style="color: transparent;"></asp:CheckBox>
                                <asp:HiddenField ID="hfSelected" runat="server" Value='<%# Eval("selected") %>' />

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Menagement" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" Visible="false">
                        <ItemTemplate>
                            <asp:LinkButton ID="lbinsertpermission" CssClass="" runat="server" data-original-title="InsertPermission" data-toggle="tooltip" Text="" OnCommand="btnCommand" CommandName="cmdInsertPerDep" CommandArgument='<%# Eval("OrgIDX")+ ";" + Eval("RDeptIDX")+ ";" + Eval("RSecIDX")%>'><span class="glyphicon glyphicon-plus text-success" aria-hidden="true"></span></asp:LinkButton>

                        </ItemTemplate>
                    </asp:TemplateField>



                </Columns>

            </asp:GridView>

        </div>
        <!--*** END Permission By Department ***-->


        <!--*** START Back To System ***-->
        <div class="form-group">
            <div id="fvBacktoBasicPermission" class="row" runat="server" visible="false">
                <div class="col-md-12">
                    <asp:LinkButton ID="lbBlack" CssClass="btn btn-info" runat="server" data-original-title="Back to System" data-toggle="tooltip" Text="Back" OnCommand="btnCommand" CommandName="cmdBacktoSysPer"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span></asp:LinkButton>
                </div>
            </div>
        </div>

        <!--*** END Back To System ***-->


        <!--*** START SystemList ***-->
        <div id="fvViewSystemListPerDep" class="col-md-3" runat="server" visible="false">
            <div id="fvSysPerDepList" runat="server" class="panel panel-info">
                <div class="panel-heading">System list</div>
                <div class="panel-body">
                    <asp:DataList ID="dtlSystemPerDep" runat="server" RepeatDirection="Vertical"
                        RepeatColumns="1">
                        <ItemTemplate>

                            <div class="form-group">
                                <asp:Label ID="lborgidxsys" runat="server" Text='<%# Eval("OrgIDX")%>' Visible="false"></asp:Label>
                                <asp:Label ID="lbdepidxsys" runat="server" Text='<%# Eval("RDeptIDX")%>' Visible="false"></asp:Label>
                                <asp:Label ID="lbrsecidxsys" runat="server" Text='<%# Eval("RSecIDX")%>' Visible="false"></asp:Label>

                                <asp:Label ID="lblSysPerIDX" runat="server" Text='<%# Eval("system_idx")%>' Visible="false"></asp:Label>
                                <asp:LinkButton ID="lbsystemperdep" CssClass="" runat="server" data-original-title="ClickSystem" data-toggle="tooltip" Text='<%# Eval("system_name_th")%>' OnCommand="btnCommand" CommandName="cmdClickSystem" CommandArgument='<%# Eval("system_idx")+ ";" + Eval("OrgIDX")+ ";" + Eval("RDeptIDX")+ ";" + Eval("RSecIDX")%>'>
                                </asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>

            </div>

        </div>

        <!--*** END SystemList ***-->

        <!--*** START ADD System With Permission By Dept ***-->
        <div class="form-group ">
            <div class="col-md-6">
                <div id="fvClickSystemPerDep" runat="server" class="" visible="false">
                    <%--<div class="panel-heading">Select Role with System</div>--%>
                    <div class="panel-body">
                        <asp:FormView ID="fvClickSystem" runat="server" DefaultMode="Insert" Width="100%">
                            <InsertItemTemplate>
                                <asp:HiddenField ID="hfSysPerDep" runat="server" Value="0"></asp:HiddenField>
                                <asp:Label ID="lbOrgIDXRoleoer" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lbRdepIDXRolePer" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lbRSecIDXRolePer" runat="server" Visible="false"></asp:Label>
                                <div class="form-horizontal" role="form">

                                    <asp:Label ID="lbrolesysperidx" runat="server" Visible="false"></asp:Label></label>
                                        <asp:Label ID="lbsysidx" runat="server" Visible="false"></asp:Label></label>
                                        <div class="form-group">
                                            <asp:Label ID="lbroleidxdepinsert" runat="server" Text="ADDRoleDepinsert" Visible="false"></asp:Label></label>                                   
									        <label class="col-sm-4 control-label">
                                                <asp:Label ID="Label1" runat="server"></asp:Label></label>
                                            <div class="col-sm-4">
                                                <asp:Label ID="lbrdorolepermissiondep" runat="server" Text='<%# Eval("role_idx")%>' Visible="true"></asp:Label>
                                                <asp:CheckBoxList ID="rdorolepermissiondep" runat="server"
                                                    CellPadding="1"
                                                    CellSpacing="5"
                                                    RepeatColumns="1"
                                                    Width="100%"
                                                    RepeatDirection="Vertical"
                                                    RepeatLayout="Table"
                                                    TextAlign="Right"
                                                    ValidationGroup="formInsert">
                                                </asp:CheckBoxList>


                                            </div>
                                        </div>
                                    <br />



                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </div>

                </div>

                <div id="fvbtnroleperdep" runat="server" class="form-group " visible="false">

                    <div class="col-sm-offset-4 col-sm-8">
                        <asp:LinkButton ID="lbRoleDepInsert" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdSaveRoleDep" CommandArgument="0" ValidationGroup="formInsert"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></asp:LinkButton>

                        <asp:LinkButton ID="lbClearDep" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Clear" OnCommand="btnCommand" CommandName="cmdClearRoleDep"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></asp:LinkButton>
                    </div>

                </div>

            </div>
        </div>

        <!--*** END ADD System With Permission By Dept ***-->

        <!--*** START SystemList ***-->
        <div id="fvall" runat="server" class="col-md-12">
            <div id="fvViewSystemallList" class="col-md-3" runat="server" visible="false">
                <div id="fvallSysPerDepList" runat="server" class="panel panel-info">
                    <div class="panel-heading">System some list</div>
                    <div class="panel-body">
                        <asp:DataList ID="dtlallSystemPerDep" runat="server" RepeatDirection="Vertical"
                            RepeatColumns="1">
                            <ItemTemplate>

                                <div class="form-group">
                                    <%--<asp:Label ID="lborgidxsys" runat="server" Text='<%# Eval("OrgIDX")%>' Visible="false" ></asp:Label>
                                <asp:Label ID="lbdepidxsys" runat="server" Text='<%# Eval("RDeptIDX")%>' Visible="false" ></asp:Label>
                                 <asp:Label ID="lbrsecidxsys" runat="server" Text='<%# Eval("RSecIDX")%>' Visible="false" ></asp:Label>--%>

                                    <asp:Label ID="lblallSysPerIDX" runat="server" Text='<%# Eval("system_idx")%>' Visible="false"></asp:Label>
                                    <asp:LinkButton ID="lbsystemallperdep" CssClass="" runat="server" data-original-title="ClickSystem" data-toggle="tooltip" Text='<%# Eval("system_name_th")%>' OnCommand="btnCommand" CommandName="cmdClickallSystem" CommandArgument='<%# Eval("system_idx")%>'>
                                    </asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                    </div>

                </div>

            </div>

            <!--*** START ADD all System With Permission By Dept ***-->
            <div class="form-group ">
                <div class="col-md-6">
                    <div id="fvClickallSystemPerDep" runat="server" class="" visible="false">
                        <%--<div class="panel-heading">Select Role with System</div>--%>
                        <div class="panel-body">
                            <asp:FormView ID="fvClickallSystem" runat="server" DefaultMode="Insert" Width="100%">
                                <InsertItemTemplate>
                                    <asp:HiddenField ID="hfallSysPerDep" runat="server" Value="0"></asp:HiddenField>
                                    <asp:Label ID="lballOrgIDXRoleoer" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lballRdepIDXRolePer" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lballRSecIDXRolePer" runat="server" Visible="false"></asp:Label>
                                    <div class="form-horizontal" role="form">

                                        <asp:Label ID="lballrolesysperidx" runat="server" Visible="false"></asp:Label></label>
                                    <asp:Label ID="lballsysidx" runat="server" Visible="false"></asp:Label></label>
                                    <div class="form-group">
                                        <asp:Label ID="lballroleidxdepinsert" runat="server" Text="ADDRoleDepinsert" Visible="false"></asp:Label></label>                                   
									   <%-- <label class="col-sm-12 control-label">
									    <asp:Label ID="Label1" runat="server" ></asp:Label></label>--%>
                                        <div class=" col-sm-offset-3 col-sm-9">
                                            <asp:Label ID="lballrdorolepermissiondep" runat="server" Text='<%# Eval("role_idx")%>' Visible="true"></asp:Label>
                                            <asp:CheckBoxList ID="rdoallrolepermissiondep" runat="server"
                                                CellPadding="1"
                                                CellSpacing="5"
                                                RepeatColumns="1"
                                                Width="100%"
                                                RepeatDirection="Vertical"
                                                RepeatLayout="Table"
                                                TextAlign="Right"
                                                ValidationGroup="formInsert">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                        <%--<br />--%>
                                    </div>
                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>

                    </div>

                    <div id="fvallbtnroleperdep" runat="server" class="form-group " visible="false">

                        <div class="col-sm-offset-3 col-sm-8">
                            <asp:LinkButton ID="lballRoleDepInsert" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdallSaveRoleDep" CommandArgument="0" ValidationGroup="formInsert"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></asp:LinkButton>

                            <asp:LinkButton ID="lballClearDep" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Clear" OnCommand="btnCommand" CommandName="cmdClearallRoleDep"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></asp:LinkButton>
                        </div>

                    </div>

                </div>
            </div>

            <!--*** END ADD all System With Permission By Dept ***-->

        </div>
        <!--*** END SystemList ***-->




        <!--*** START SystemallList ***-->
        <div id="fvallsystem" runat="server" class="col-md-12">
            <div id="fvViewallSystem" class="col-md-3" runat="server" visible="false">
                <div id="fvallSystemPerDepList" runat="server" class="panel panel-info">
                    <div class="panel-heading">System All list</div>
                    <div class="panel-body">
                        <asp:DataList ID="dtlallSystem" runat="server" RepeatDirection="Vertical"
                            RepeatColumns="1">
                            <ItemTemplate>

                                <div class="form-group">

                                    <asp:Label ID="lblallSystemPerIDX" runat="server" Text='<%# Eval("system_idx")%>' Visible="false"></asp:Label>
                                    <asp:LinkButton ID="lballsystemperdep" CssClass="" runat="server" data-original-title="ClickSystem" data-toggle="tooltip" Text='<%# Eval("system_name_th")%>' OnCommand="btnCommand" CommandName="cmdallSystem" CommandArgument='<%# Eval("system_idx")%>'>
                                    </asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                    </div>

                </div>

            </div>



            <!--*** START Some System With Permission By Dept ***-->
            <div class="form-group ">
                <div class="col-md-6">
                    <div id="fvClicksomeSystemPerDep" runat="server" class="" visible="false">
                        <%--<div class="panel-heading">Select Role with System</div>--%>
                        <div class="panel-body">
                            <asp:FormView ID="fvClicksomeSystem" runat="server" DefaultMode="Insert" Width="100%">
                                <InsertItemTemplate>
                                    <asp:HiddenField ID="hfsomeSysPerDep" runat="server" Value="0"></asp:HiddenField>
                                    <asp:Label ID="lbsomeOrgIDXRoleoer" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lbsomeRdepIDXRolePer" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lbsomeRSecIDXRolePer" runat="server" Visible="false"></asp:Label>
                                    <div class="form-horizontal" role="form">

                                        <asp:Label ID="lbsomerolesysperidx" runat="server" Visible="false"></asp:Label></label>
                                    <asp:Label ID="lbsomesysidx" runat="server" Visible="false"></asp:Label></label>
                                    <div class="form-group">
                                        <asp:Label ID="lbsomeroleidxdepinsert" runat="server" Text="ADDRoleDepinsert" Visible="false"></asp:Label></label>                                   
									    <%--<label class="col-sm-12 control-label">
									    <asp:Label ID="Label1" runat="server" ></asp:Label></label>--%>
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <asp:Label ID="lbsomerdorolepermissiondep" runat="server" Text='<%# Eval("role_idx")%>' Visible="true"></asp:Label>
                                            <asp:CheckBoxList ID="rdosomerolepermissiondep" runat="server"
                                                CellPadding="1"
                                                CellSpacing="5"
                                                RepeatColumns="1"
                                                Width="100%"
                                                RepeatDirection="Vertical"
                                                RepeatLayout="Table"
                                                TextAlign="Right"
                                                ValidationGroup="formInsert">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>
                                        <%-- <br /> --%>
                                    </div>
                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>

                    </div>

                    <div id="fvsomebtnroleperdep" runat="server" class="form-group" visible="false">

                        <div class="col-sm-offset-3 col-sm-9">
                            <asp:LinkButton ID="lbsomeRoleDepInsert" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdsomeSaveRoleDep" CommandArgument="0" ValidationGroup="formInsert"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></asp:LinkButton>

                            <asp:LinkButton ID="lbsomeClearDep" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Clear" OnCommand="btnCommand" CommandName="cmdClearsomeRoleDep"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></asp:LinkButton>
                        </div>

                    </div>

                </div>
            </div>
            <!--*** END Some all System With Permission By Dept ***-->



        </div>
        <!--*** END SystemallList ***-->

        <!--*** START ADD all System With Permission By Dept ***-->
        <div class="form-group ">
            <div class="col-md-12">
                <div id="Div_showPerSection" runat="server" class="" visible="false">
                    <%--<div class="panel-heading">Select Role with System</div>
                    <div class="panel-body">--%>
                    <asp:FormView ID="FvChecPermissionInSec" runat="server" Width="100%">
                        <EditItemTemplate>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">รายการสิทธิ์</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-8 col-md-offset-4">
                                        <%--<div class="form-horizontal" role="form">--%>

                                        <asp:Label ID="lballrolesysperidxsec" runat="server" Visible="false"></asp:Label></label>
                                    <asp:Label ID="lballsysidx_sec" runat="server" Visible="false"></asp:Label></label>

                                    <div class="form-group">
                                        <asp:Label ID="lballroleidxdepinsert2222" runat="server" Text="ADDRoleDepinsert" Visible="false"></asp:Label></label>                                   
									   <%-- <label class="col-sm-12 control-label">
									    <asp:Label ID="Label1" runat="server" ></asp:Label></label>--%>
                                        <%--<div class=" col-sm-offset-3 col-sm-9">--%>
                                        <div class="col-md-12">
                                            <asp:Label ID="lballCheck_perSec" runat="server" Text='<%# Eval("role_idx")%>' Visible="false"></asp:Label>
                                            <asp:CheckBoxList ID="check_persection_detail" runat="server"
                                                CellPadding="1"
                                                CellSpacing="5"
                                                RepeatColumns="1"
                                                Width="100%"
                                                RepeatDirection="Vertical"
                                                RepeatLayout="Table"
                                                TextAlign="Right"
                                                ValidationGroup="formInsert">
                                            </asp:CheckBoxList>
                                        </div>
                                    </div>

                                        <div class="form-group">
                                            <div class="form-group pull-right">
                                                <asp:LinkButton ID="lbChangeRoleDep" Text="เปลี่ยนแปลงสิทธิ์" CssClass="btn btn-success" runat="server"
                                                    data-original-title="Change" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdSaveChangeRoleDep" CommandArgument="0" ValidationGroup="SaveChangePermission"></asp:LinkButton>

                                                <%-- <asp:LinkButton ID="btnSaveApprove" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandName="cmdSaveApprove" ValidationGroup="SaveApproveHeaduser"></asp:LinkButton>--%>

                                                <asp:LinkButton ID="lballClearDep" CssClass="btn btn-danger" runat="server" Visible="false" data-original-title="Cancel" data-toggle="tooltip" Text="Clear" OnCommand="btnCommand" CommandName="cmdClearallRoleDep"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></asp:LinkButton>
                                            </div>


                                        </div>


                                    </div>
                                </div>
                                <%--<br />--%>
                                <%--</div>--%>
                            </div>

                        </EditItemTemplate>
                    </asp:FormView>
                    <%-- </div>--%>
                </div>

                <%--<div id="Div2" runat="server" class="form-group " visible="false">

                    <div class="col-sm-offset-3 col-sm-8">
                        <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdallSaveRoleDep" CommandArgument="0" ValidationGroup="formInsert"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></asp:LinkButton>

                        <asp:LinkButton ID="LinkButton2" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Clear" OnCommand="btnCommand" CommandName="cmdClearallRoleDep"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></asp:LinkButton>
                    </div>

                </div>--%>
            </div>
        </div>

        <!--*** END ADD all System With Permission By Dept ***-->



    </div>

</asp:Content>
