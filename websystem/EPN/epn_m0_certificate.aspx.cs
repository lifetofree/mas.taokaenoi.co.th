﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_EPN_epn_m0_certificate : System.Web.UI.Page
{
    #region service
    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    // ชื่อกล่อง  ตัวแปร = new ชื่อกล่อง
    data_certificate_doc _dtcer = new data_certificate_doc();
    data_employee _dtEmployee = new data_employee();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    // เรียก service มาใช้ จาก web config
    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
   // static string urlgetInsertCertificate = _serviceUrl + ConfigurationManager.AppSettings["urlget_InsertCertificate"];

    int _emp_idx = 0;

    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #region BTN
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();
        int m0_ceridx = 0;


        //GvMaster.Visible = false;
        switch (cmdName)
        {

            case "btnCmn":
                Panel_Add.Visible = true;
                btnshow.Visible = false;
                break;

        }




    }


    #endregion
    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                litDebug.Text = e.Row.RowState.ToString();
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                //  SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                // SelectMasterList();
                btnshow.Visible = false;
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                // SelectMasterList();
                btnshow.Visible = true;
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int m0_ceridx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtname_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtname_edit");
                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");


                GvMaster.EditIndex = -1;

                //ViewState["m0_ceridx"] = m0_ceridx;
                //ViewState["cer_name"] = txtname_edit.Text;
                //ViewState["cer_stataus"] = StatusUpdate.SelectedValue;
                btnshow.Visible = true;
                //     UpdateMaster();
                // SelectMasterList();

                break;
        }
    }

    #endregion

    #region CallService

    protected data_certificate_doc callServicePost(string _cmdUrl, data_certificate_doc _dtcer)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtcer);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtcer = (data_certificate_doc)_funcTool.convertJsonToObject(typeof(data_certificate_doc), _localJson);


        return _dtcer;

    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }
    #endregion
}