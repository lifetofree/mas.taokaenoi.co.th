﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Xml;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_Region
/// </summary>
[Serializable]
[XmlRoot("data_Region")]
public class data_Region
{
    public string ReturnCode { get; set; }
    public int ReturnCount { get; set; }
    [XmlElement("LanguageList")]
    public DetailLanguage[] LanguageList { get; set; }
    [XmlElement("ReligionList")]
    public DetailReligion[] ReligionList { get; set; }
    [XmlElement("RaceList")]
    public DetailRace[] RaceList { get; set; }
    [XmlElement("NationalityList")]
    public DetailNationality[] NationalityList { get; set; }
    [XmlElement("CountryList")]
    public DetailCountry[] CountryList { get; set; }
    [XmlElement("GeoList")]
    public DetailGeo[] GeoList { get; set; }
    [XmlElement("PADList")]
    public DetailPAD[] PADList { get; set; }
    [XmlElement("ProvList")]
    public DetailProv[] ProvList { get; set; }
    [XmlElement("AmpList")]
    public DetailAmp[] AmpList { get; set; }
    [XmlElement("DistList")]
    public DetailDist[] DistList { get; set; }


}
[Serializable]
public class DetailLanguage
{
    [XmlElement("LanIDX")]
    public int LanIDX { get; set; }
    [XmlElement("NameTH")]
    public string NameTH { get; set; }
    [XmlElement("NameEN")]
    public string NameEN { get; set; }
    [XmlElement("LStatus")]
    public int LStatus { get; set; }
    [XmlElement("LStatusDetail")]
    private string _LStatusDetail;
    public string LStatusDetail { get; set; }
}
[Serializable]
public class DetailReligion
{
    [XmlElement("RelIDX")]
    public int RelIDX { get; set; }
    [XmlElement("RelNameTH")]
    public string RelNameTH { get; set; }
    [XmlElement("RelNameEN")]
    public string RelNameEN { get; set; }
    [XmlElement("RelStatus")]
    public int RelStatus { get; set; }
    [XmlElement("RelStatusDetail")]
    private string _RelStatusDetail;
    public string RelStatusDetail { get; set; }
}
[Serializable]
public class DetailRace
{
    [XmlElement("RaceIDX")]
    public int RaceIDX { get; set; }
    [XmlElement("RaceName")]
    public string RaceName { get; set; }
    [XmlElement("RaceStatus")]
    public int RaceStatus { get; set; }
    [XmlElement("RaceStatusDetail")]
    private string _RaceStatusDetail;
    public string RaceStatusDetail { get; set; }
}
[Serializable]
public class DetailNationality
{
    [XmlElement("NatIDX")]
    public int NatIDX { get; set; }
    [XmlElement("NatName")]
    public string NatName { get; set; }
    [XmlElement("NatStatus")]
    public int NatStatus { get; set; }
    [XmlElement("NatStatusDetail")]
    private string _NatStatusDetail;
    public string NatStatusDetail { get; set; }
}
[Serializable]
public class DetailCountry
{
    [XmlElement("CountryIDX")]
    public int CountryIDX { get; set; }
    [XmlElement("CountryName")]
    public string CountryName { get; set; }
    [XmlElement("CountryStatus")]
    public int CountryStatus { get; set; }
    [XmlElement("CountryStatusDetail")]
    private string _CountryStatusDetail;
    public string CountryStatusDetail { get; set; }
}
[Serializable]
public class DetailGeo
{
    [XmlElement("GeoIDX")]
    public int GeoIDX { get; set; }
    [XmlElement("GeoName")]
    public string GeoName { get; set; }
    [XmlElement("GeoStatus")]
    public int GeoStatus { get; set; }
    [XmlElement("GeoStatusDetail")]
    private string _GeoStatusDetail;
    public string GeoStatusDetail { get; set; }
}
[Serializable]
public class DetailPAD
{
    public int DataID { get; set; }
    public string DataName { get; set; }
    public string DataPosition { get; set; }
    public int DataStatus { get; set; }
}
[Serializable]
public class DetailProv
{
    [XmlElement("ProvIDX")]
    public int ProvIDX { get; set; }
    [XmlElement("ProvCode")]
    public string ProvCode { get; set; }
    [XmlElement("ProvName")]
    public string ProvName { get; set; }
    [XmlElement("GeoIDX")]
    public int GeoIDX { get; set; }
    [XmlElement("GeoName")]
    public string GeoName { get; set; }
    [XmlElement("ProvStatus")]
    public int ProvStatus { get; set; }
}
[Serializable]
public class DetailAmp
{
    [XmlElement("AmpIDX")]
    public int AmpIDX { get; set; }
    [XmlElement("AmpCode")]
    public string AmpCode { get; set; }
    [XmlElement("AmpName")]
    public string AmpName { get; set; }
    [XmlElement("ProvIDX")]
    public int ProvIDX { get; set; }
    [XmlElement("ProvName")]
    public string ProvName { get; set; }
    [XmlElement("GeoIDX")]
    public int GeoIDX { get; set; }
    [XmlElement("AmpStatus")]
    public int AmpStatus { get; set; }
}
[Serializable]
public class DetailDist
{
    [XmlElement("DistIDX")]
    public int DistIDX { get; set; }
    [XmlElement("DistCode")]
    public string DistCode { get; set; }
    [XmlElement("DistName")]
    public string DistName { get; set; }
    [XmlElement("AmpIDX")]
    public int AmpIDX { get; set; }
    [XmlElement("ProvIDX")]
    public int ProvIDX { get; set; }
    [XmlElement("GeoIDX")]
    public int GeoIDX { get; set; }
    [XmlElement("DistStatus")]
    public int DistStatus { get; set; }
}
