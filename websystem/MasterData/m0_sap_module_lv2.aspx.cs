﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_m0_sap_module_lv2 : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();


    private string ODSP_Reletion = "ODSP_Reletion";
    data_employee dtEmployee = new data_employee();

    dataODSP_Relation dtODSP = new dataODSP_Relation();
    DataReservation datareservation = new DataReservation();
    DataSupportIT _dtsupport = new DataSupportIT();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetM0SapModuleLv1 = _serviceUrl + ConfigurationManager.AppSettings["urlGetM0SapModuleLv1"];
   
    //lv2
    static string _urlGetddlM1CodeSap = _serviceUrl + ConfigurationManager.AppSettings["urlGetddlM1CodeSap"];
    static string _urlGetSearchddlCodeLv1 = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchddlCodeLv1"];
    static string _urlGetMasterModuleSapLv2 = _serviceUrl + ConfigurationManager.AppSettings["urlGetMasterModuleSapLv2"];
    static string _urlSetModuleSapLv2 = _serviceUrl + ConfigurationManager.AppSettings["urlSetModuleSapLv2"];
    static string _urlSetModuleSapLv2Update = _serviceUrl + ConfigurationManager.AppSettings["urlSetModuleSapLv2Update"];
    static string _urlSetDelModuleSapLv2 = _serviceUrl + ConfigurationManager.AppSettings["urlSetDelModuleSapLv2"];
    static string _urlGetSearchDetailLv2 = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchDetailLv2"];



    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            //SelectSystem();

            SelectMasterList();
            Select_ddlSearchCodelv1();

            Panel_Search.Visible = true;
            div_selectGvmaster.Visible = true;
        }

        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

    }
    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Insert_Status()
    {
        DataSupportIT _dtsupport_insert = new DataSupportIT();
        _dtsupport_insert.BoxMSAPlist = new MSAPlist[1];
        MSAPlist dtsupport_insert = new MSAPlist();

        dtsupport_insert.SysIDX = int.Parse(ddl_System.SelectedValue);
        dtsupport_insert.MS1IDX = int.Parse(ddlModule.SelectedValue);
        dtsupport_insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        dtsupport_insert.MS2_Name = txtname.Text;
        dtsupport_insert.MS2_Code = txtcode.Text;
        dtsupport_insert.MS2Status = int.Parse(ddStatusadd.SelectedValue);

        _dtsupport_insert.BoxMSAPlist[0] = dtsupport_insert;

        _dtsupport_insert = callServiceDevices(_urlSetModuleSapLv2, _dtsupport_insert);
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport_insert));
        if (_dtsupport_insert.ReturnCode == "2")
        {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ผิดพลาด! มีข้อมูลนี้อยู่ในระบบแล้ว');", true);
        }

    }

    protected void Select_ddlModule()
    {
        ddlModule.AppendDataBoundItems = true;
        ddlModule.Items.Clear();
        ddlModule.Items.Add(new ListItem("Select Module ....", "0"));

        DataSupportIT _dtsupport_ddl = new DataSupportIT();
        _dtsupport_ddl.BoxMSAPlist = new MSAPlist[1];
        MSAPlist dtsupport_ddl_list = new MSAPlist();

        dtsupport_ddl_list.SysIDX = int.Parse(ddl_System.SelectedValue);

        _dtsupport_ddl.BoxMSAPlist[0] = dtsupport_ddl_list;
       
        _dtsupport_ddl = callServiceDevices(_urlGetddlM1CodeSap, _dtsupport_ddl);

        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport_ddl));
        ddlModule.DataSource = _dtsupport_ddl.BoxMSAPlist;
        ddlModule.DataTextField = "MS1_Code";
        ddlModule.DataValueField = "MS1IDX";
        ddlModule.DataBind();
    }

    protected void Select_ddlSearchCodelv1()
    {
        ddlSearchCodelv1.AppendDataBoundItems = true;
        ddlSearchCodelv1.Items.Clear();
        ddlSearchCodelv1.Items.Add(new ListItem("Select Module ....", "0"));

        DataSupportIT _dtsupport_search = new DataSupportIT();
        _dtsupport_search.BoxMSAPlist = new MSAPlist[1];
        MSAPlist dtsupport_ddl_list = new MSAPlist();

        _dtsupport_search.BoxMSAPlist[0] = dtsupport_ddl_list;

        _dtsupport_search = callServiceDevices(_urlGetSearchddlCodeLv1, _dtsupport_search);

        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport_ddl));
        ddlSearchCodelv1.DataSource = _dtsupport_search.BoxMSAPlist;
        ddlSearchCodelv1.DataTextField = "MS1_Code";
        ddlSearchCodelv1.DataValueField = "MS1IDX";
        ddlSearchCodelv1.DataBind();
    }

    protected void SelectMasterList()
    {

        DataSupportIT _dtsupport_select = new DataSupportIT();
        _dtsupport_select.BoxMSAPlist = new MSAPlist[1];
        MSAPlist dtsupport_list = new MSAPlist();

        _dtsupport_select.BoxMSAPlist[0] = dtsupport_list;

        _dtsupport_select = callServiceDevices(_urlGetMasterModuleSapLv2, _dtsupport_select);

        ViewState["vs_Index"] = _dtsupport_select.BoxMSAPlist;
       
        setGridData(GvMaster, ViewState["vs_Index"]);
    }

    protected DataSupportIT callServiceDevices(string _cmdUrl, DataSupportIT _dtsupport)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtsupport = (DataSupportIT)_funcTool.convertJsonToObject(typeof(DataSupportIT), _localJson);

        return _dtsupport;
    }

    protected void SelectSystem()
    {

        ddl_System.Items.Clear();
        ddl_System.AppendDataBoundItems = true;
        ddl_System.Items.Add(new ListItem("กรุณาเลือกระบบ...", "0"));

        DataSupportIT _dtsupport = new DataSupportIT();
        _dtsupport.BoxMSAPlist = new MSAPlist[1];
        MSAPlist m0_sap = new MSAPlist();

        _dtsupport.BoxMSAPlist[0] = m0_sap;
        _dtsupport = callServiceDevices(_urlGetM0SapModuleLv1, _dtsupport);

        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));
        ddl_System.DataSource = _dtsupport.BoxMSAPlist;
        ddl_System.DataTextField = "SysNameTH";
        ddl_System.DataValueField = "SysIDX";
        ddl_System.DataBind();

    }

    protected void Update_Master_List()
    {


        DataSupportIT _dtsupport_update = new DataSupportIT();
        _dtsupport_update.BoxMSAPlist = new MSAPlist[1];
        MSAPlist dtsupport = new MSAPlist();

        dtsupport.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        dtsupport.SysIDX = int.Parse(ViewState["SysIDX_Update"].ToString());
        dtsupport.MS2IDX = int.Parse(ViewState["MS2IDX_Update"].ToString());
        dtsupport.MS2_Name = ViewState["MS2_Name_Update"].ToString();
        dtsupport.MS2_Code = ViewState["MS2_Code_Update"].ToString();
        dtsupport.MS2Status = int.Parse(ViewState["MS2Status_Update"].ToString());
        dtsupport.MS1IDX = int.Parse(ViewState["MS1IDX_Update"].ToString());

        _dtsupport_update.BoxMSAPlist[0] = dtsupport;
        _dtsupport_update = callServiceDevices(_urlSetModuleSapLv2Update, _dtsupport_update);

        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport_update));
        if(_dtsupport_update.ReturnCode == "2")
        {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            SelectMasterList();

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้อยู่ในระบบแล้ว ไม่สามารถอัพเดทข้อมูลได้');", true);

        }

    }

    protected void Delete_Master_List()
    {
        DataSupportIT _dtsupport_delete = new DataSupportIT();
        _dtsupport_delete.BoxMSAPlist = new MSAPlist[1];
        MSAPlist dtsupport_delete = new MSAPlist();

        dtsupport_delete.MS2IDX = int.Parse(ViewState["MS2IDX_Delete_Update"].ToString());
        dtsupport_delete.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtsupport_delete.BoxMSAPlist[0] = dtsupport_delete;

        _dtsupport_delete = callServiceDevices(_urlSetDelModuleSapLv2, _dtsupport_delete);
    }

    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        //var txtstatus_edit = (TextBox)e.Row.FindControl("txtstatus_edit");
                        var ddl_SysIDX_edit = (DropDownList)e.Row.FindControl("ddl_SysIDX_edit");
                        var lbl_SysIDX_edit = (Label)e.Row.FindControl("lbl_SysIDX_edit");
                        var ddlMS1IDX_edit = (DropDownList)e.Row.FindControl("ddlMS1IDX_edit");
                        var lbl_MS1IDX_edit = (Label)e.Row.FindControl("lbl_MS1IDX_edit");

                        ddl_SysIDX_edit.AppendDataBoundItems = true;
                        ddl_SysIDX_edit.Items.Add(new ListItem("กรุณาเลือกระบบ...", "0"));
                        DataSupportIT _dtsupport = new DataSupportIT();
                        _dtsupport.BoxMSAPlist = new MSAPlist[1];
                        MSAPlist m0_sap = new MSAPlist();

                        _dtsupport.BoxMSAPlist[0] = m0_sap;
                        _dtsupport = callServiceDevices(_urlGetM0SapModuleLv1, _dtsupport);

                        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport));
                        ddl_SysIDX_edit.DataSource = _dtsupport.BoxMSAPlist;
                        ddl_SysIDX_edit.DataTextField = "SysNameTH";
                        ddl_SysIDX_edit.DataValueField = "SysIDX";
                        ddl_SysIDX_edit.DataBind();
                        ddl_SysIDX_edit.SelectedValue = lbl_SysIDX_edit.Text;


                        ddlMS1IDX_edit.AppendDataBoundItems = true;
                        ddlMS1IDX_edit.Items.Add(new ListItem("กรุณาเลือกประเภท Module...", "0"));

                        DataSupportIT _dtsupport_ddl = new DataSupportIT();
                        _dtsupport_ddl.BoxMSAPlist = new MSAPlist[1];
                        MSAPlist dtsupport_ddl_list = new MSAPlist();

                        dtsupport_ddl_list.SysIDX = int.Parse(ddl_SysIDX_edit.SelectedValue);

                        _dtsupport_ddl.BoxMSAPlist[0] = dtsupport_ddl_list;

                        _dtsupport_ddl = callServiceDevices(_urlGetddlM1CodeSap, _dtsupport_ddl);

                        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtsupport_ddl));
                        ddlMS1IDX_edit.DataSource = _dtsupport_ddl.BoxMSAPlist;
                        ddlMS1IDX_edit.DataTextField = "MS1_Code";
                        ddlMS1IDX_edit.DataValueField = "MS1IDX";
                        ddlMS1IDX_edit.DataBind();
                        ddlMS1IDX_edit.SelectedValue = lbl_MS1IDX_edit.Text;

                    }

                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                //SelectMasterList();
                setGridData(GvMaster, ViewState["vs_Index"]);

                setOntop.Focus();
                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":


                int MS2IDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtMS2_Code_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtMS2_Code_edit");
                var txt_MS2_Name_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txt_MS2_Name_edit");
                var ddStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");
                var ddlMS1IDX_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlMS1IDX_edit");
                var ddl_SysIDX_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddl_SysIDX_edit");

                GvMaster.EditIndex = -1;

                ViewState["MS2IDX_Update"] = MS2IDX;
                ViewState["MS2_Name_Update"] = txt_MS2_Name_edit.Text;
                ViewState["MS2_Code_Update"] = txtMS2_Code_edit.Text;
                ViewState["MS2Status_Update"] = ddStatusUpdate.SelectedValue;
                ViewState["MS1IDX_Update"] = ddlMS1IDX_edit.SelectedValue;
                ViewState["SysIDX_Update"] = ddl_SysIDX_edit.SelectedValue;


                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion


    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        switch (ddName.ID)
        {

            case "ddl_System":
                Select_ddlModule();

                //litdebug.Text = "33333";

                break;
        }
    }
    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAddModule":

                btnaddmodule.Visible = false;
                Panel_Search.Visible = false;
                div_selectGvmaster.Visible = false;

                Panel_AddModule.Visible = true;
                SelectSystem();
                Select_ddlModule();

                setOntop.Focus();
                break;

            case "Cmdcancel":

                btnaddmodule.Visible = true;
                Panel_Search.Visible = true;
                div_selectGvmaster.Visible = true;

                Panel_AddModule.Visible = false;
                
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                Select_ddlSearchCodelv1();
                setOntop.Focus();
                break;

            case "btnAdd":
                Insert_Status();
                setOntop.Focus();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "CmdDel":

                int MS2IDX_Delete = int.Parse(cmdArg);
                ViewState["MS2IDX_Delete_Update"] = MS2IDX_Delete;

                Delete_Master_List();
                SelectMasterList();
                setOntop.Focus();

                break;
            case "CmdSearch":

                var SearchCodelv1 = ddlSearchCodelv1.SelectedValue;


                DataSupportIT _dtsupport_searchdetail2 = new DataSupportIT();
                _dtsupport_searchdetail2.BoxMSAPlist = new MSAPlist[1];
                MSAPlist dtsupport_list_search = new MSAPlist();
                dtsupport_list_search.MS1IDX = int.Parse(SearchCodelv1);
                _dtsupport_searchdetail2.BoxMSAPlist[0] = dtsupport_list_search;

                _dtsupport_searchdetail2 = callServiceDevices(_urlGetSearchDetailLv2, _dtsupport_searchdetail2);

                ViewState["vs_Index"] = _dtsupport_searchdetail2.BoxMSAPlist;
                setGridData(GvMaster, ViewState["vs_Index"]);
                setOntop.Focus();
                break;
        }



    }
    #endregion
}