﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_chr_m0_iso : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();
    data_employee _dataEmployee = new data_employee();
    data_chr _dtchr = new data_chr();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlInsert_Master_ISO = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Master_ISO"];
    static string urlSelect_Master_ISO = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_ISO"];
    static string urlUpdate_Master_ISO = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_Master_ISO"];
    static string urlDelete_Master_ISO = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_Master_ISO"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string urlSelect_System = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_SystemList"];



    #endregion

    #region SQL
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();

        }

        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

    }

    protected void SelectMasterList()
    {

        _dtchr.BoxMaster_ISOList = new MasterISOList[1];
        MasterISOList dataselect = new MasterISOList();

        _dtchr.BoxMaster_ISOList[0] = dataselect;

        _dtchr = callServicePostCHR(urlSelect_Master_ISO, _dtchr);
        setGridData(GvMaster, _dtchr.BoxMaster_ISOList);

    }

    protected void Update_Master_List()
    {
        _dtchr.BoxMaster_ISOList = new MasterISOList[1];
        MasterISOList dataupdate = new MasterISOList();

        dataupdate.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        dataupdate.ISOIDX = int.Parse(ViewState["ISOIDX"].ToString());
        dataupdate.Sysidx = int.Parse(ViewState["Sysidx"].ToString());
        dataupdate.OrgIDX = int.Parse(ViewState["OrgIDX"].ToString());
        dataupdate.Doccode = ViewState["txtDoccodeUpdate"].ToString();
        dataupdate.Docname = ViewState["txtDocnameUpdate"].ToString();
        dataupdate.VersionIso = ViewState["txtVersionUpdate"].ToString();
        dataupdate.Result = ViewState["txtResultUpdate"].ToString();
        dataupdate.ISO_Status = int.Parse(ViewState["ddStatusUpdate"].ToString());

        _dtchr.BoxMaster_ISOList[0] = dataupdate;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtchr));
        _dtchr = callServicePostCHR(urlUpdate_Master_ISO, _dtchr);

    }

    protected void Delete_Master_List()
    {

        _dtchr.BoxMaster_ISOList = new MasterISOList[1];
        MasterISOList dataupdate = new MasterISOList();

        dataupdate.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        dataupdate.ISOIDX = int.Parse(ViewState["ISOIDX"].ToString());
        _dtchr.BoxMaster_ISOList[0] = dataupdate;

        _dtchr = callServicePostCHR(urlDelete_Master_ISO, _dtchr);

    }

    protected void Insert_MasterData()
    {

        _dtchr.BoxMaster_ISOList = new MasterISOList[1];
        MasterISOList datainsert = new MasterISOList();

        datainsert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        datainsert.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
        datainsert.Sysidx = int.Parse(ddlsystem.SelectedValue);
        datainsert.ISO_Status = int.Parse(ddStatusadd.SelectedValue);
        datainsert.Doccode = txtcode.Text;
        datainsert.Docname = txtname.Text;
        datainsert.VersionIso = txtver.Text;
        datainsert.Result = txtresult.Text;

        _dtchr.BoxMaster_ISOList[0] = datainsert;

        _dtchr = callServicePostCHR(urlInsert_Master_ISO, _dtchr);

    }

    protected void getSystemList(DropDownList ddlName)
    {
        _dtchr = new data_chr();
        _dtchr.BoxMaster_SystemList = new MasterSystemList[1];
        MasterSystemList dataselect = new MasterSystemList();

        _dtchr.BoxMaster_SystemList[0] = dataselect;

        _dtchr = callServicePostCHR(urlSelect_System, _dtchr);
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtchr));
        setDdlData(ddlName, _dtchr.BoxMaster_SystemList, "System_name", "Sysidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกระบบ...", "0"));

    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร...", "0"));
    }

    #endregion

    #region Call Service
    protected data_chr callServicePostCHR(string _cmdUrl, data_chr _dtchr)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtchr);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtchr = (data_chr)_funcTool.convertJsonToObject(typeof(data_chr), _localJson);

        return _dtchr;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //  convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
    #endregion

    #region Reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void SetDefaultAdd()
    {
        getSystemList(ddlsystem);
        getOrganizationList(ddlorgidx);
        txtname.Text = String.Empty;
        ddStatusadd.SelectedValue = "1";
    }
    #endregion


    #region GridView
    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        var ddlSysSapIDX = (DropDownList)e.Row.FindControl("ddlSysSapIDX");
                        var lbSysSapIDX = (Label)e.Row.FindControl("lbSysSapIDX");

                        var ddlorgidx_edit = (DropDownList)e.Row.FindControl("ddlorgidx_edit");
                        var lbOrganization = (Label)e.Row.FindControl("lbOrganization");



                        getSystemList(ddlSysSapIDX);
                        ddlSysSapIDX.SelectedValue = lbSysSapIDX.Text;

                        getOrganizationList(ddlorgidx_edit);
                        ddlorgidx_edit.SelectedValue = lbOrganization.Text;

                    }

                }


                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();
                btnshow.Visible = false;
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                btnshow.Visible = true;
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int ISOIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var ddlSysSapIDX = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlSysSapIDX");
                var ddlorgidx_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlorgidx_edit");
                var txtDoccodeUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtDoccodeUpdate");
                var txtDocnameUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtDocnameUpdate");
                var txtVersionUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtVersionUpdate");
                var txtResultUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtResultUpdate");
                var txtLastChange = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtLastChange");
                var ddStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster.EditIndex = -1;

                ViewState["ISOIDX"] = ISOIDX;
                ViewState["Sysidx"] = ddlSysSapIDX.SelectedValue;
                ViewState["OrgIDX"] = ddlorgidx_edit.SelectedValue;
                ViewState["txtDoccodeUpdate"] = txtDoccodeUpdate.Text;
                ViewState["txtDocnameUpdate"] = txtDocnameUpdate.Text;
                ViewState["txtVersionUpdate"] = txtVersionUpdate.Text;
                ViewState["txtResultUpdate"] = txtResultUpdate.Text;
                ViewState["txtLastChange"] = txtLastChange.Text;
                ViewState["ddStatusUpdate"] = ddStatusUpdate.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion

    #endregion


    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":
                btnshow.Visible = false;
                Panel_Add.Visible = true;
                SetDefaultAdd();
                break;

            case "btnCancel":
                btnshow.Visible = true;
                Panel_Add.Visible = false;
                break;

            case "btnAdd":
                Insert_MasterData();
                Panel_Add.Visible = false;
                btnshow.Visible = true;
                SelectMasterList();
                break;
            case "CmdDel":
                int ISOIDX = int.Parse(cmdArg);
                ViewState["ISOIDX"] = ISOIDX;
                Delete_Master_List();
                SelectMasterList();

                break;
        }



    }
    #endregion
}