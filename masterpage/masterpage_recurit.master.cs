﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

using System.Collections;
using System.Data;


public partial class masterpage_masterpage : System.Web.UI.MasterPage
{
    #region initial function/data
    
    function_tool _funcTool = new function_tool();
    data_system _data_system = new data_system();
    data_employee _data_employee = new data_employee();


    menusystem_detail showmenu = new menusystem_detail();


    //data_employee _data_emp = new data_employee();



    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _key_linkconvert = ConfigurationManager.AppSettings["key_linkconvert"];
    static string _urlGetSystemMenu = _serviceUrl + ConfigurationManager.AppSettings["urlGetSystemMenu"];
    static string _urlGetEmpMenu = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmpMenu"];

    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];

    static string _baseUrl = "http://172.16.11.5/taokaenoi.co.th/MAS/MainPage";

    int emp_idx = 0;
    string _localJson = "";
    
    //int _menu_idx = 0;
    string _defaultDdlText;
    string _defaultDdlValue;

    int _tempInt = 0;


    #endregion initial function/data
    private void Page_Init(object sender, EventArgs e)
    {

        /*if (Request.Form["emp_idx"] != null)
        {
            bool _result = Int32.TryParse(Request.Form["emp_idx"], out _tempInt);
            if (_result && _tempInt > 0)
            {
                Session["emp_idx"] = Request.Form["emp_idx"];

                ViewState["rdept_idx"] = Request.Form["rdept_idx"];
                ViewState["rsec_idx"] = Request.Form["rsec_idx"];


            }
        }
        else if (Session["emp_idx"] == null || Session["reset_type"] != null)
        {
            string path = HttpContext.Current.Request.Url.AbsolutePath;
            Response.Redirect(ResolveUrl("~/warning") + "?url=" + path);
        }*/
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        setPanelTitle();

        /*
        if (!IsPostBack)
        {

            ViewState["emp_idx_check"] = int.Parse(Session["emp_idx"].ToString());

            actionIndex();

            _data_system.menusystem_list = new menusystem_detail[1];
            menusystem_detail _menusystemDetail = new menusystem_detail();

            _menusystemDetail.rdept_idx = int.Parse(ViewState["rdept_idx"].ToString());
            _menusystemDetail.rsec_idx = int.Parse(ViewState["rsec_idx"].ToString());


            _data_system.menusystem_list[0] = _menusystemDetail;

            // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_system));
            _data_system = callServiceMenuSystem(_urlGetSystemMenu, _data_system);

            if (_data_system.menusystem_list == null)
            {
                ViewState["Menulist"] = null;
                Session["Per_System"] = null;

                Repeater rep = new Repeater();
                rep.ID = "RepeaterMenu";
                rep.DataSource = null;
                rep.DataBind();

                Repeater1.DataSource = null;
                Repeater1.DataBind();

            }
            else
            {
                ViewState["Menulist"] = _data_system;
                Session["Per_System"] = _data_system;
                var linqmenu = from a in _data_system.menusystem_list where a.menu_level == 1 select a;

                Repeater rep = new Repeater();
                rep.ID = "RepeaterMenu";
                rep.DataSource = linqmenu.ToList();
                rep.DataBind();

                Repeater1.DataSource = linqmenu.ToList();
                Repeater1.DataBind();

                //Response.Write("Check Emp");

                //Response.Write(Session["emp_idx"].ToString());
            }

            var system = (data_system)Session["Per_System"];
            int SystemIDX = 0;

            try
            {
                SystemIDX = (int)Session["System"];

            }
            catch
            {

            }

            if (SystemIDX != 0)
            {

                if (system == null)
                {
                    Response.Redirect(ResolveUrl("~/warning"));
                }
                else
                {

                    var linqsystem = from a in system.menusystem_list where a.menu_idx == SystemIDX select a;

                    if (linqsystem.Count() < 1)
                    {
                        Response.Redirect(ResolveUrl("~/warning"));
                    }

                }


            }

        }
        */

    }


    #region selected    
    protected void actionIndex()
    {
        /*
        data_employee _data_employee = new data_employee();

        _data_employee.employee_list = new employee_detail[1];
        employee_detail _employee_detail = new employee_detail();

        _employee_detail.emp_idx = int.Parse(ViewState["emp_idx_check"].ToString());

        _data_employee.employee_list[0] = _employee_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        //_data_employee = callServiceEmpProfile(_urlGetEmpMenu, _data_employee);

        _data_employee = callServiceEmpProfile(_urlGetMyProfile + ViewState["emp_idx_check"].ToString());


        //ViewState["rdept_idx"] = _data_employee.empmenusystem_list[0].rdept_idx;
        //ViewState["rsec_idx"] = _data_employee.empmenusystem_list[0].rsec_idx;

        ViewState["rdept_idx"] = _data_employee.employee_list[0].rdept_idx;
        ViewState["rsec_idx"] = _data_employee.employee_list[0].rsec_idx;
        */

    }

    #endregion selected


    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "ClickMenu":

                break;
            //case "ClickToTM1":

            //    //string url_tm1 = "http://172.16.11.53:9510/pmpsvc/";
            //    ScriptManager.RegisterClientScriptBlock(this, GetType(), "", "window.open('http://172.16.11.53:9510/pmpsvc/', '', '');", true);

            //    break;



        }

   }

    #endregion btnCommand

    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

       
        var Menu = (data_system)ViewState["Menulist"];
        data_system datasystemoffice2 = new data_system();
     

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            var spanMenu = (HtmlGenericControl)e.Item.FindControl("spanMenu");

            if (Menu != null)
            {


                int l = 0;
                int lvl = 0;
               
                var list = (menusystem_detail)e.Item.DataItem;
                int menu_idx = list.menu_idx;


                HttpResponse response = HttpContext.Current.Response;
                response.Clear();

                StringBuilder sb = new System.Text.StringBuilder();

                var boxmenuplus = new menusystem_detail[(from a in Menu.menusystem_list where a.MenuIDX1 == list.MenuIDX1 && a.menu_level != 1 select a).Count()];
                foreach (menusystem_detail menu in (from a in Menu.menusystem_list where a.MenuIDX1 == list.MenuIDX1 && a.menu_level != 1 select a))
                {
                    boxmenuplus[l] = new menusystem_detail();
                    boxmenuplus[l].menu_idx = menu.menu_idx;
                    boxmenuplus[l].MenuIDX1 = menu.MenuIDX1;
                    boxmenuplus[l].menu_level = menu.menu_level;
                    datasystemoffice2.menusystem_list = boxmenuplus;
                    l++;
                }


                l = 0;
                foreach (menusystem_detail menu in (from a in Menu.menusystem_list where a.MenuIDX1 == list.MenuIDX1 && a.menu_level != 1 select a))
                {

                    spanMenu.Attributes.Add("class", "fa arrow");

                    try
                    {
                        lvl = datasystemoffice2.menusystem_list[l + 1].menu_level;
                    }
                    catch
                    {
                        lvl = 0;
                    }

                    //Response.Write(lvl.ToString() + "___" + menu.menu_level.ToString() + "__" + l.ToString() + "//" );



                    //***** Open Tag Menu *****/

                    if (lvl > menu.menu_level)
                    {

                        if (l == 0)
                        {

                            sb.Append("<ul class='nav' style='color:#777; padding-left:10px'>");
                            sb.Append("<li>");
                        }
                        else
                        {

                            sb.Append("<li>");
                        }

                    }
                    else if (lvl == menu.menu_level)
                    {

                        if (l == 0)
                        {

                            sb.Append("<ul class='nav' style='color:#777;padding-left:10px'>");
                            sb.Append("<li>");
                        }
                        else
                        {

                            sb.Append("<li>");
                        }

                    }
                    else if (lvl < menu.menu_level && lvl != 0)
                    {
                        sb.Append("<li>");
                    }
                    else if (lvl == 0)
                    {

                        if (l == 0)
                        {

                            sb.Append("<ul class='nav' style='color:#777;padding-left:10px'>");
                            sb.Append("<li>");
                        }
                        else
                        {

                            sb.Append("<li>");
                        }



                    }


                    int width = 0;

                    //***** Tag String *****//

                    ////sb.Append("<a href='" + ResolveUrl("~/" + (menu.menu_url.ToString())) + "'" + "style=margin-left:" + width * menu.menu_level + ";color:#777; ><i class='fa fa-file fa-fw'></i>" + menu.menu_name_th.ToString());

                    string input1 = menu.menu_url.ToString();
                    string[] result1 = input1.Split(new string[] { "MAS/" }, StringSplitOptions.None);
                    string[] result2 = input1.Split(new string[] { "taokaenoi.co.th/" }, StringSplitOptions.None);

                    string input2 = "/taokaenoi.co.th";

                    string[] result3 = input2.Split(new string[] { "/" }, StringSplitOptions.None);
                    ////Response.Write("<br />");
                    ////Response.Write(result3[1] + result1[0]); //+  result2[0]

                    if (result1.Length > 1)
                    {
                        //litDebug.Text += result1[1] + "<br>";
                    }
                    else
                    {
                        //litDebug.Text += result1[0] + "<br>";
                    }


                    if (result1[0] != "/" && result1[0] != "")
                    {
                   
                       sb.Append("<a href='" + ResolveUrl("~/" + (menu.menu_url.ToString())) + "'" + "style=margin-left:" + width * menu.menu_level + ";color:#777; ><i class='fa fa-file fa-fw'></i>" + menu.menu_name_th.ToString());
                        
                    }

                    if (result1[0] == "")
                    {
                  
                        sb.Append("<a href='" + ResolveUrl("~/" + (menu.menu_url.ToString())) + "'" + "style=margin-left:" + width * menu.menu_level + ";color:#777; ><i class='fa fa-file fa-fw'></i>" + menu.menu_name_th.ToString());
                        
                    }

                    if (result1.Length > 1) //lv 3
                    {
                        string emp = Session["emp_idx"].ToString();//Session["emp_idx"].ToString();//Session["EmpIDX"].ToString();
                        string rdept = ViewState["rdept_idx"].ToString();

                        string url = _baseUrl + "?emp_idx=" + emp + "&rdept_idx=" + rdept + "&url=" + menu.menu_url.ToString();

                        //litDebug.Text += menu.menu_name_th.ToString() + menu.menu_idx;
                        sb.Append("<li>");

                        if (menu.menu_idx == 1069)
                        {
                            //sb.Append("<a class='backchange-request' href='" + ResolveUrl("~/" + (menu.menu_url.ToString())) + "'" + "style=margin-left:" + width * menu.menu_level + ";><i class='fa fa-file fa-fw'></i>" + menu.menu_name_th.ToString());

                            //sb.Append("<a class='backchange-request' href='" + ResolveUrl("~/" + (menu.menu_url.ToString())) + "'" + "style=margin-left:" + width * menu.menu_level + ";color:#777; ><i class='fa fa-file fa-fw'></i>" + menu.menu_name_th.ToString());
                            sb.AppendFormat("<a class='backchange-request' style='color:#777;text-decoration:none;' href='{1}' class='sub-menu-inform'><i class=\'fa fa-file fa-fw\'></i> {0}", menu.menu_name_th.ToString(), url);

                        }
                        else
                        {
                            sb.AppendFormat("<a style='color:#777;text-decoration:none;' href='{1}' class='sub-menu-inform'><i class=\'fa fa-file fa-fw\'></i> {0}", menu.menu_name_th.ToString(), url);
                        }
                        
                        sb.Append("</li>");

                    }


                    //*****  Show Icon  *****//
                    if (lvl > menu.menu_level)
                    {
                        sb.Append("<span class='fa arrow'></span></a>");
                    }
                    else
                    {
                        sb.Append("</a>");

                    }


                    //***** Clsoe Tag Menu *****//
                    if (lvl > menu.menu_level)
                    {
                        sb.Append("<ul class='nav' style='color:#777;padding-left:10px'>");
                    }
                    else if (lvl == menu.menu_level)
                    {
                        sb.Append("</li>");
                    }
                    else if (lvl < menu.menu_level && lvl != 0)
                    {

                        sb.Append("</li>");

                        int i2 = 1;
                        while (i2 <= (menu.menu_level - lvl))
                        {

                            sb.Append("</ul>");
                            sb.Append("</li>");

                            i2++;
                        }

                    }
                    else if (lvl == 0)
                    {

                        if (l == 0)
                        {
                            sb.Append("</li>");
                            sb.Append("</ul>");
                            sb.Append("</li>");
                        }
                        else
                        {

                            int i2 = 1;
                            while (i2 <= (menu.menu_level - 1))
                            {

                                sb.Append("</li>");
                                sb.Append("</ul>");

                                i2++;
                            }


                        }


                    }



                    l++;


                }



                (e.Item.FindControl("littest") as Literal).Text = sb.ToString();


            }




        }
    }


    #region set panel title
    protected void setPanelTitle()
    {
        string path = Request.Url.AbsolutePath;
        string module = path.Substring(path.LastIndexOf('/') + 1).ToLower();
        string moduleName;
       
        //test Title Program
        string module_Value = path.Substring(path.IndexOf('/') + 1).ToLower();
       
        switch (module)
        {
            case "dashboard":
                moduleName = "Dashboard";
                break;
            case "training":
                moduleName = "Training";
                break;
            default:
                moduleName = convertTitle(module);
                break;
        }
        
    }
    #endregion set panel title

    protected string convertTitle(string dataIn)
    {
        string dataOut = "";
        string[] _tempData = dataIn.Split('-');

        foreach (string item in _tempData)
        {
            dataOut += item.FirstOrDefault().ToString().ToUpper() + String.Join("", item.Skip(1)) + " ";
        }

        return dataOut;
    }

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        ////setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();

    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected data_system callServiceMenuSystem(string _cmdUrl, data_system _data_system)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_system);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_system = (data_system)_funcTool.convertJsonToObject(typeof(data_system), _localJson);

        return _data_system;
    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)

    {
        //// convert to json
        // _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _data_employee;
    }

    #endregion reuse
}
