﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_its_m0_specit : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_itasset _data_itasset = new data_itasset();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetits_masterit = _serviceUrl + ConfigurationManager.AppSettings["urlGetits_masterit"];
    static string _urlSetInsits_masterit = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsits_masterit"];
    static string _urlSetUpdits_masterit = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdits_masterit"];
    static string _urlDelits_masterit = _serviceUrl + ConfigurationManager.AppSettings["urlDelits_masterit"];
    static string _urlGetErrorits_masterit = _serviceUrl + ConfigurationManager.AppSettings["urlGetErrorits_masterit"];


    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;
    string _operation_status_id = "its_m0_specit";

    int emp_idx = 0;
    #endregion initial function/data


    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
            actionIndex();

        }
    }

    #region selected   
    protected void actionIndex()
    {
        pnlsearch.Visible = true;
        data_itasset data_itasset = new data_itasset();
        data_itasset.its_masterit_action = new its_masterit[1];

        its_masterit obj = new its_masterit();

        obj.m0_spec_idx = 0;
        obj.filter_keyword = txtFilterKeyword.Text;
        obj.operation_status_id = _operation_status_id;
        data_itasset.its_masterit_action[0] = obj;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_itasset));
        data_itasset = _func_dmu.zCallServicePostITAsset(_urlGetits_masterit, data_itasset);

        setGridData(GvMaster, data_itasset.its_masterit_action);

    }
    #endregion selected 
    

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _m0_spec_idx;
        string _txtspec_name, _txtspec_remark;
        int _cemp_idx;

        its_masterit objM0_ProductType = new its_masterit();

        switch (cmdName)
        {

            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);
                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnInsert":
                _txtspec_name = ((TextBox)ViewInsert.FindControl("txtspec_name")).Text.Trim();
                _txtspec_remark = ((TextBox)ViewInsert.FindControl("txtspec_remark")).Text.Trim();
                DropDownList _ddlspec_status = (DropDownList)ViewInsert.FindControl("ddlspec_status");
                _cemp_idx = emp_idx;
                if (_txtspec_name == "")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกspec');", true);
                    return;
                }
                if(checkCodeError(0, _txtspec_name) == true)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('spec นี้มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
                    return;
                }
                its_masterit obj = new its_masterit();
                _data_itasset.its_masterit_action = new its_masterit[1];
                obj.operation_status_id = _operation_status_id;
                obj.m0_spec_idx = 0;//_type_idx; 
                obj.spec_name = _txtspec_name;
                obj.spec_remark = _txtspec_remark;
                obj.spec_status = int.Parse(_ddlspec_status.SelectedValue);
                obj.spec_created_by = _cemp_idx;

                _data_itasset.its_masterit_action[0] = obj;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_itasset));
                
                _data_itasset = _func_dmu.zCallServicePostITAsset(_urlSetInsits_masterit, _data_itasset);

                if (_data_itasset.ReturnCode == "0")
                {
                    actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    setError(_data_itasset.ReturnCode.ToString() + " - " + _data_itasset.ReturnMsg);
                }

                //actionCreate();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnDelete":

                _m0_spec_idx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;


                _data_itasset.its_masterit_action = new its_masterit[1];
                objM0_ProductType.m0_spec_idx = _m0_spec_idx;
                // objM0_ProductType.CEmpIDX = _cemp_idx;
                objM0_ProductType.operation_status_id = _operation_status_id;
                _data_itasset.its_masterit_action[0] = objM0_ProductType;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_itasset));

                _data_itasset = _func_dmu.zCallServicePostITAsset(_urlDelits_masterit, _data_itasset);


                if (_data_itasset.ReturnCode == "0")
                {
                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    
                }
                else
                {
                    setError(_data_itasset.ReturnCode + " - " + _data_itasset.ReturnMsg);
                }

                break;
            case "btnFilter":
                actionIndex();
                break;



        }
    }
    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                actionIndex();
                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    pnlsearch.Visible = false;
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {
                        
                    }
                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                actionIndex();
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                int m0_spec_idx_update = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtspec_name = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtspec_nameUpdate");
                var txtspec_remark = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtspec_remarkUpdate");
                var ddlspec_status = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlspec_statusUpdate");
                int icode = 0;
                
                if (txtspec_name.Text == "")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกspec');", true);
                    return;
                }
                if (checkCodeError(m0_spec_idx_update, txtspec_name.Text.Trim()) == true)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('spec นี้มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
                    return;
                }

                GvMaster.EditIndex = -1;

                _data_itasset.its_masterit_action = new its_masterit[1];
                its_masterit obj = new its_masterit();

                obj.operation_status_id = _operation_status_id;
                obj.m0_spec_idx = m0_spec_idx_update;
                obj.spec_name = txtspec_name.Text;
                obj.spec_remark = txtspec_remark.Text;
                obj.spec_status = int.Parse(ddlspec_status.SelectedValue);
                obj.spec_updated_by = emp_idx;

                _data_itasset.its_masterit_action[0] = obj;

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_itasset));
                _data_itasset = _func_dmu.zCallServicePostITAsset(_urlSetUpdits_masterit, _data_itasset);

                if (_data_itasset.ReturnCode == "0")
                {
                    //initPage();
                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);  
                    actionIndex();


                }
                else
                {
                    setError(_data_itasset.ReturnCode + " - " + _data_itasset.ReturnMsg);
                }


                break;
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj, string sysidx_menu, string menuidx_per, string role_per)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        //switch (fvName.ID)
        //{



        //}
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();
        

    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();
        

    }

    protected void setVisible()
    {
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }
    
    public Boolean checkCodeError(int _m0_spec_idx, string _spec_code)
    {
        Boolean error = false;
        data_itasset data_itasset = new data_itasset();
        data_itasset.its_masterit_action = new its_masterit[1];

        its_masterit obj = new its_masterit();

        obj.m0_spec_idx = _m0_spec_idx;
        obj.spec_name = _spec_code;
        obj.operation_status_id = _operation_status_id;
        data_itasset.its_masterit_action[0] = obj;
        data_itasset = _func_dmu.zCallServicePostITAsset(_urlGetErrorits_masterit, data_itasset);
        if(data_itasset.its_masterit_action != null)
        {
            //obj = data_itasset.its_masterit_action[0];
            //if (obj.spec_code.ToString().Trim()== _spec_code.Trim())
            //{

            //}
            error = true;

        }
        return error;
    }

    #endregion reuse

}