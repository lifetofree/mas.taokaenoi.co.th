﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_report_dashboard.aspx.cs" Inherits="websystem_hr_hr_report_dashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">


    <asp:Literal ID="txt" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">
        <asp:View ID="ViewIndex" runat="server">
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <div class="col-lg-12 ">
                        <div class="bg-system">
                            <asp:Image ID="Image4" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/hr-dashboard/header4.png" Style="height: 100%; width: 100%;" />
                        </div>
                    </div>


                    <div class="col-lg-12">

                        <div class="panel panel-warning" style="border-width: 2pt">
                            <div class="panel-heading" style="background-color: #feb83d; color: black;">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; ค้นหาข้อมูล</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label2" runat="server" Text="ประเภทรายการ" CssClass="col-sm-2 control-label"></asp:Label>
                                        <div class="col-sm-3">

                                            <asp:DropDownList ID="ddltype_report" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="1">รายงานภาพรวม</asp:ListItem>
                                                <asp:ListItem Value="2">รายงานขาดงาน</asp:ListItem>
                                                <asp:ListItem Value="3">รายงานคนลาออก</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Search_Report" runat="server" Display="None"
                                                ControlToValidate="ddltype_report" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทรายการ"
                                                ValidationExpression="กรุณาเลือกประเภทรายการ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group" id="div_searchdate" visible="true" runat="server">
                                        <asp:Label ID="Label55" runat="server" Text="ระบุวันค้นหา" CssClass="col-sm-2 control-label"></asp:Label>
                                        <div class="col-sm-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="AddStartdate_report" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator37" ValidationGroup="Search_Report" runat="server" Display="None"
                                                    ControlToValidate="AddStartdate_report" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                    ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender55" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator37" Width="160" />
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group" id="div_searchmonth" visible="false" runat="server">

                                        <asp:Label ID="Label814" runat="server" Text="เดือน" CssClass="col-sm-2 control-label"></asp:Label>
                                        <div class="col-sm-3" runat="server">
                                            <asp:DropDownList ID="ddlmonth" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">Select Month....</asp:ListItem>
                                                <asp:ListItem Value="1">มกราคม</asp:ListItem>
                                                <asp:ListItem Value="2">กุมภาพันธ์</asp:ListItem>
                                                <asp:ListItem Value="3">มีนาคม</asp:ListItem>
                                                <asp:ListItem Value="4">เมษายน</asp:ListItem>
                                                <asp:ListItem Value="5">พฤกษภาคม</asp:ListItem>
                                                <asp:ListItem Value="6">มิถุนายน</asp:ListItem>
                                                <asp:ListItem Value="7">กรกฎาคม</asp:ListItem>
                                                <asp:ListItem Value="8">สิงหาคม</asp:ListItem>
                                                <asp:ListItem Value="9">กันยายน</asp:ListItem>
                                                <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                                <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                                <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <asp:Label ID="Label165" runat="server" Text="ปี" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlyear" CssClass="form-control" runat="server">
                                            </asp:DropDownList>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label1" runat="server" Text="องค์กร" CssClass="col-sm-2 control-label"></asp:Label>
                                        <div class="col-sm-3">

                                            <asp:DropDownList ID="ddlOrg_report" runat="server" CssClass="form-control">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Search_Report" runat="server" Display="None"
                                                ControlToValidate="ddlOrg_report" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกองค์กร"
                                                ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-3 col-sm-offset-2">
                                            <asp:LinkButton ID="btnsearch_report" CssClass="btn btn-info btn-sm" runat="server" CommandName="CmdSearch" OnCommand="btnCommand" ValidationGroup="Search_Report" title="Search"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnrefresh_report" CssClass="btn btn-default  btn-sm" runat="server" CommandName="btnrefresh" OnCommand="btnCommand" title="Refresh"><i class="
glyphicon glyphicon-refresh"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 " id="div_chart_dashboard" runat="server" visible="false">

                        <div class="panel panel-warning" style="border-width: 2pt">
                            <div class="panel-heading" style="background-color: #feb83d; color: black;">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; รายงาน</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="col-lg-12 ">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="alert-message alert-message-success">
                                                    <blockquote class="danger" style="font-size: small; background-color: #ff9999; text-align: center">
                                                        <h4><b>จำนวนพนักงานแยกตามระดับ</b></h4>
                                                    </blockquote>
                                                </div>
                                                <asp:Literal ID="litReportChart_groupemp" runat="server" />
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="alert-message alert-message-success">
                                                    <blockquote class="danger" style="font-size: small; background-color: #ff9999; text-align: center">
                                                        <h4><b>จำนวนพนักงานแยกตาม Job Grade</b></h4>
                                                    </blockquote>
                                                </div>
                                                <asp:Literal ID="litReportChart_jobgrade" runat="server" />
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="alert-message alert-message-success">
                                                    <blockquote class="danger" style="font-size: small; background-color: #ff9999; text-align: center">
                                                        <h4><b>จำนวนพนักงานแยกตาม Plant</b></h4>
                                                    </blockquote>
                                                </div>
                                                <asp:Literal ID="litReportChart_qtyplant" runat="server" />
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="alert-message alert-message-success">
                                                        <blockquote class="danger" style="font-size: small; background-color: #ff9999; text-align: center">
                                                            <h4><b>จำนวนพนักงานแยกตามระดับและเพศ</b></h4>
                                                        </blockquote>
                                                    </div>
                                                    <asp:Literal ID="litReportChart_sexemp" runat="server" />
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="alert-message alert-message-success">
                                                        <blockquote class="danger" style="font-size: small; background-color: #ff9999; text-align: center">
                                                            <h4><b>จำนวนพนักงานแยกตามฝ่ายและ Plant</b></h4>
                                                        </blockquote>
                                                    </div>
                                                    <div style="overflow-x: scroll; overflow-y: scroll; width: 100%; max-height: 400px;" id="divtablegenerate" runat="server">


                                                        <asp:GridView ID="GvPlantByDept" runat="server" HeaderStyle-CssClass="info"
                                                            AllowPaging="false" AllowSorting="True"
                                                            CssClass="table table-striped table-bordered"
                                                            CellPadding="4" ForeColor="Black" GridLines="Vertical" BorderStyle="Solid"
                                                            HeaderStyle-BorderWidth="2pt"
                                                            ShowFooter="true"
                                                            OnRowDataBound="Master_RowDataBound">

                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center; background-color: #f2dede;">No result.</div>
                                                            </EmptyDataTemplate>
                                                        </asp:GridView>


                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="col-lg-12 " id="div_Table_Appsent" runat="server" visible="false">

                        <div class="panel panel-warning" style="border-width: 2pt">
                            <div class="panel-heading" style="background-color: #feb83d; color: black;">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; รายงาน</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="col-lg-12 ">


                                        <div style="overflow-x: scroll; overflow-y: scroll; width: 100%; max-height: 600px;">


                                            <asp:GridView ID="GvAppsent" runat="server" HeaderStyle-CssClass="info"
                                                AllowPaging="false" AllowSorting="True"
                                                CssClass="table table-striped table-bordered"
                                                CellPadding="4" ForeColor="Black" GridLines="Vertical" BorderStyle="Solid"
                                                HeaderStyle-BorderWidth="2pt"
                                                ShowFooter="true"
                                                OnRowDataBound="Master_RowDataBound">

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center; background-color: #f2dede;">No result.</div>
                                                </EmptyDataTemplate>
                                            </asp:GridView>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-12 " id="div_Table_Resign" runat="server" visible="false">

                        <div class="panel panel-warning" style="border-width: 2pt">
                            <div class="panel-heading" style="background-color: #feb83d; color: black;">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; รายงาน</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="col-lg-12 ">


                                        <div style="overflow-x: scroll; overflow-y: scroll; width: 100%; max-height: 650px;">


                                            <asp:GridView ID="GvResign" runat="server" HeaderStyle-CssClass="info"
                                                AllowPaging="false" AllowSorting="True"
                                                CssClass="table table-striped table-bordered"
                                                CellPadding="4" ForeColor="Black" GridLines="Vertical" BorderStyle="Solid"
                                                HeaderStyle-BorderWidth="2pt"
                                                ShowFooter="true"
                                                OnRowDataBound="Master_RowDataBound">

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center; background-color: #f2dede;">No result.</div>
                                                </EmptyDataTemplate>
                                            </asp:GridView>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnsearch_report" />
                </Triggers>
            </asp:UpdatePanel>


        </asp:View>
    </asp:MultiView>

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });


    </script>


</asp:Content>

