﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="private_sarary.aspx.cs" Inherits="websystem_ManageEmployee_private_sarary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <div class="mainbody container-fluid">
        <div class="row">
            <div class="navbar-wrapper">
                <div class="container-fluid">
                    <div class="navbar navbar-light navbar-static-top" style="background-color: #1199c2;" role="navigation">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span
                                        class="icon-bar"></span><span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand">
                                    <small>
                                        <font color="#daebe8"><i class="glyphicon glyphicon-usd"></i>&nbsp; Sarary &nbsp;(ข้อมูลเงินเดือน)</font>
                                    </small>
                                    <%-- #87b308--%>
                                </a>
                            </div>
                            <div class="navbar-collapse collapse">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- menu -->
        <div class="col-sm-12">
            <div class="alert alert-warning" role="alert">
                <strong>** ข้อมูลเงินเดือน ถือเป็นความลับ โปรดระมัดระวังในการดูข้อมูลของท่าน</strong>
            </div>
            <h4><span class="label label-default"><i class="glyphicon glyphicon-usd"></i>ข้อมูลเงินเดือน</span></h4>
            <div class="panel panel-primary">
                <asp:Repeater ID="repeater_sarary" runat="server">
                    <ItemTemplate>
                        <div class="panel-body">
                            <div class="pull-left">
                                <a href="#">
                                    <img class="media-object img-thumbnail"
                                        src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/icon-transfer.png")%>'
                                        width="100px" height="100px" style="margin-right: 8px; margin-top: 7px;">
                                </a>
                            </div>
                            <div class="post-content">
                                <br />
                                <div class="col-sm-2">
                                    <font color="#1A5276"><small class="list-group-item-heading text_left"><b>เงินเดือนของท่านคือ</b></small></font>
                                </div>
                                <div class="col-sm-10">
                                    <div class="alert alert-success" role="alert">
                                        <span><i class="glyphicon glyphicon-usd"></i>&nbsp;<%# Eval("sarary") %></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>

                <asp:Panel ID="repeater_sarary_empty" runat="server" Visible="false">
                    <div class="panel-body">
                        <div class="pull-left">
                            <a href="#">
                                <img class="media-object img-thumbnail"
                                    src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/icon-transfer.png")%>'
                                    width="100px" height="100px" style="margin-right: 8px; margin-top: 7px;">
                            </a>
                        </div>
                        <div class="post-content">
                            <br />

                            <div class="col-sm-2">
                                <font color="#1A5276"><small class="list-group-item-heading text_left"><b>เงินเดือนของท่านคือ</b></small></font>
                            </div>
                            <div class="col-sm-10">
                                <div class="alert alert-danger" role="alert">
                                    <strong>ไม่มีข้อมูลเงินเดือนของท่าน</strong>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>
        </div>
</asp:Content>

