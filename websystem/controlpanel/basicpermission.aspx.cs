﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;



using System.Net;
using System.Drawing;

using System.IO;

using System.Data.SqlClient;

using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Net.Mail;
using System.Collections;

public partial class websystem_controlpanel_basicpermission : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();

    //data_menu _dataMenu = new data_menu();
    data_permission _data_permission = new data_permission();
    data_employee _data_employee = new data_employee();
    data_employee _dataEmployee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //org
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];

    //
    static string _urlGetOrganization = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganization"];
    static string _urlGetSystemDept = _serviceUrl + ConfigurationManager.AppSettings["urlGetSystemDept"];
    static string _urlGetRoleDept = _serviceUrl + ConfigurationManager.AppSettings["urlGetRoleDept"];
    static string _urlSetRoleDept = _serviceUrl + ConfigurationManager.AppSettings["urlSetRoleDept"];
    static string _urlGetcheckall = _serviceUrl + ConfigurationManager.AppSettings["urlGetcheckall"];
    static string _urlGetEmpMenu = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmpMenu"];
    static string _urlGetDetailSearch = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailSearch"];
    static string _urlGetRoleDeptSectionDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetRoleDeptSectionDetail"];
    static string _urlUpdateSetRoleDept = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateSetRoleDept"];

    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];

    int emp_idx = 0;
    string _localJson = "";
    int _tempInt = 0;
    int _menu_idx = 0;
    string _defaultDdlText;
    string _defaultDdlValue;

    int _OrgIDX = 0;
    int _RDepIDX = 0;
    int _RSecIDX = 0;
    int _system_idx = 0;
    int _ods_idx = 0;

    int _allsystem_idx = 0;
    int _systemallperdep_idx = 0;

    #endregion initial function/data

    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            //actionIndex(0);
            //actionSystem(0, 0, 0, 0);

            actionIndexEmp();

            if (ViewState["rsec_idx"].ToString() == "210")
            {
                emp_idx = int.Parse(Session["emp_idx"].ToString());

                actionIndex(0);
                actionSystem(0, 0, 0, 0);


                ////fvDeptSelect
                DropDownList ddlOrg_search = (DropDownList)Dept_Excel.FindControl("ddlOrg_search");
                DropDownList ddlDept_search = (DropDownList)Dept_Excel.FindControl("ddlDept_search");
                DropDownList ddlSec_search = (DropDownList)Dept_Excel.FindControl("ddlSec_search");
                //litDebug.Text = ViewState["vsMaterial"].ToString(); 1202884

                getOrganizationList((DropDownList)Dept_Excel.FindControl("ddlOrg_search"));
                getDepartmentList(ddlDept_search, int.Parse(ddlOrg_search.SelectedValue));
                getSectionList(ddlSec_search, int.Parse(ddlOrg_search.SelectedValue), int.Parse(ddlDept_search.SelectedValue));

                GvMaster.Visible = true;
                Dept_Excel.Visible = true;
                btnCheckPermission.Visible = true;
                chk_permission.Visible = true;

                BackbtnCheckPermission.Visible = false;
                PanelPermission.Visible = false;


            }
            else
            {
                Response.Redirect("http://mas.taokaenoi.co.th");
            }

        }

    }


    ////#region selected    
    ////protected void actionIndexEmp() //เอาไว้ใช้กัน Link ตรง
    ////{
    ////    data_system _data_system112 = new data_system();

    ////    _data_system112.empmenusystem_list = new empmenusystem_detail[1];
    ////    empmenusystem_detail _empDetail12 = new empmenusystem_detail();

    ////    _empDetail12.emp_idx = emp_idx;

    ////    _data_system112.empmenusystem_list[0] = _empDetail12;

    ////    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
    ////    _data_system112 = callServiceMenuSystem(_urlGetEmpMenu, _data_system112);

    ////    ViewState["rdept_idx"] = _data_system112.empmenusystem_list[0].rdept_idx;
    ////    ViewState["rsec_idx"] = _data_system112.empmenusystem_list[0].rsec_idx;


    ////}

    ////#endregion selected

    #region selected    
    protected void actionIndexEmp() //เอาไว้ใช้กัน Link ตรง
    {

        data_employee _data_employee = new data_employee();

        _data_employee.employee_list = new employee_detail[1];
        employee_detail _employee_detail = new employee_detail();

        _employee_detail.emp_idx = emp_idx;//int.Parse(Session["emp_idx"].ToString());

        _data_employee.employee_list[0] = _employee_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        //_data_employee = callServiceEmpProfile(_urlGetEmpMenu, _data_employee);

        _data_employee = callServiceEmpProfile(_urlGetMyProfile + emp_idx.ToString());

        ViewState["rdept_idx"] = _data_employee.employee_list[0].rdept_idx;
        ViewState["rsec_idx"] = _data_employee.employee_list[0].rsec_idx;


    }

    #endregion selected

    #region Action

    protected void actionIndex(int _OrgIDX)
    {
        //data_permission _data_permission = new data_permission();
        _data_permission.permission_list = new permission_detail[1];
        permission_detail _permissionDetail = new permission_detail();
        _permissionDetail.OrgIDX = _OrgIDX;

        _data_permission.permission_list[0] = _permissionDetail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission));
        _data_permission = callServicePermission(_urlGetOrganization, _data_permission);



        // check return_code
        if (_data_permission.return_code == 0)
        {
            if (ViewState["permission_list"] == null)
            {
                ViewState["permission_list"] = _data_permission.permission_list;

            }
            setGridData(GvMaster, ViewState["permission_list"]);

            //GvMaster.Columns[8].Visible = true;

        }
        else
        {
            setError(_data_permission.return_code.ToString() + " - " + _data_permission.return_msg);
        }
    }

    protected void actionSystem(int _system_idx, int _OrgIDXSys, int _RDepIDXSys, int _RSecIDXSys)
    {


        data_permission _data_permission1 = new data_permission();
        _data_permission1.system_list = new system_detail[1];
        system_detail _permissionDetail = new system_detail();
        _permissionDetail.OrgIDX = _OrgIDXSys;
        _permissionDetail.RDeptIDX = _RDepIDXSys;
        _permissionDetail.RSecIDX = _RSecIDXSys;

        _data_permission1.system_list[0] = _permissionDetail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission1));
        _data_permission1 = callServicePermission(_urlGetSystemDept, _data_permission1);
        //var rtorgidx = _data_permission1.return_orgidx;
        //var rtrdepidx = _data_permission1.return_rdepidx;

        // check return_code
        if (_data_permission1.return_code == 0)
        {
            setDataList(dtlSystemPerDep, _data_permission1.system_list);

            //setGridData(GvMaster, _data_permission1.system_list);
        }
        else
        {
            setError(_data_permission1.return_code.ToString() + " - " + _data_permission1.return_msg);
        }


    }

    protected void actionSystemall()
    {


        data_permission _data_permission2 = new data_permission();
        _data_permission2.systemall_list = new systemall_detail[1];
        systemall_detail _checkallDetail = new systemall_detail();
        //_permissionDetail.OrgIDX = _OrgIDXSys;
        //_permissionDetail.RDeptIDX = _RDepIDXSys;
        //_permissionDetail.RSecIDX = _RSecIDXSys;

        _data_permission2.systemall_list[0] = _checkallDetail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission1));
        _data_permission2 = callServicePermission(_urlGetcheckall, _data_permission2);
        //var rtorgidx = _data_permission1.return_orgidx;
        //var rtrdepidx = _data_permission1.return_rdepidx;

        // check return_code
        if (_data_permission2.return_code == 0)
        {
            setDataList(dtlallSystemPerDep, _data_permission2.systemall_list);

            //setGridData(GvMaster, _data_permission1.system_list);
        }
        else
        {
            setError(_data_permission2.return_code.ToString() + " - " + _data_permission2.return_msg);
        }
    }

    protected void actionSystemallperdep()
    {

        data_permission _data_permission3 = new data_permission();
        _data_permission3.systemall_list = new systemall_detail[1];
        systemall_detail _checkallDetail = new systemall_detail();
        //_permissionDetail.OrgIDX = _OrgIDXSys;
        //_permissionDetail.RDeptIDX = _RDepIDXSys;
        //_permissionDetail.RSecIDX = _RSecIDXSys;

        _data_permission3.systemall_list[0] = _checkallDetail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission1));
        _data_permission3 = callServicePermission(_urlGetcheckall, _data_permission3);
        //var rtorgidx = _data_permission1.return_orgidx;
        //var rtrdepidx = _data_permission1.return_rdepidx;

        // check return_code
        if (_data_permission3.return_code == 0)
        {
            setDataList(dtlallSystem, _data_permission3.systemall_list);

            //setGridData(GvMaster, _data_permission1.system_list);
        }
        else
        {
            setError(_data_permission3.return_code.ToString() + " - " + _data_permission3.return_msg);
        }
    }


    #endregion Action

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        //*** value ***//
        int _cemp_idx;

        role_detail _roledeptDetail = new role_detail();

        switch (cmdName)
        {
            case "cmdInsertPerDep":

                GvMaster.Visible = false;
                fvPermissionDep.Visible = false;
                chkAll.Visible = false;

                fvViewallSystem.Visible = false;
                fvClicksomeSystemPerDep.Visible = false;
                fvsomebtnroleperdep.Visible = false;

                btnCheckPermission.Visible = false;


                string[] arg = new string[3];
                arg = e.CommandArgument.ToString().Split(';');
                int _orgidx_arg = int.Parse(arg[0]);
                int _depidx_arg = int.Parse(arg[1]);
                int _rsecidx_arg = int.Parse(arg[2]);

                fvBacktoBasicPermission.Visible = true;
                fvViewSystemListPerDep.Visible = true;

                actionSystem(0, _orgidx_arg, _depidx_arg, _rsecidx_arg);


                break;

            case "cmdBacktoSysPer": // ปุ่ม Back กลับไปหน้าหลัก
                chkAll.Visible = true;
                fvBacktoBasicPermission.Visible = false;
                fvViewSystemListPerDep.Visible = false;
                fvClickSystemPerDep.Visible = false;
                fvbtnroleperdep.Visible = false;


                GvMaster.Visible = true;
                fvPermissionDep.Visible = true;


                break;

            case "cmdClickSystem": // Click ระบบแสดง Role

                fvClickSystemPerDep.Visible = true;
                fvbtnroleperdep.Visible = true;
                //var fvbtnroleperdep = (FormView)fvClickSystem.FindControl("fvbtnroleperdep");

                //fvbtnroleperdep.Visible = true;


                string[] arg1 = new string[4];
                arg1 = e.CommandArgument.ToString().Split(';');
                int _system_arg1 = int.Parse(arg1[0]);
                int _orgidx_arg1 = int.Parse(arg1[1]);
                int _depidx_arg1 = int.Parse(arg1[2]);
                int _rsecidx_arg1 = int.Parse(arg1[3]);

                setFormData(fvClickSystem, FormViewMode.Insert, _data_permission.role_list, _system_arg1.ToString(), _orgidx_arg1.ToString(), _depidx_arg1.ToString(), _rsecidx_arg1.ToString());


                break;

            case "cmdSaveRoleDep": // ปุ่ม save

                CheckBoxList rdorolepermissiondep = (CheckBoxList)fvClickSystem.FindControl("rdorolepermissiondep");
                Label lbOrgIDXRoleoer = (Label)fvClickSystem.FindControl("lbOrgIDXRoleoer");
                Label lbRdepIDXRolePer = (Label)fvClickSystem.FindControl("lbRdepIDXRolePer");
                Label lbRSecIDXRolePer = (Label)fvClickSystem.FindControl("lbRSecIDXRolePer");
                Label lbsysidx = (Label)fvClickSystem.FindControl("lbsysidx");

                _ods_idx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;

                _data_permission.role_list = new role_detail[1];

                _roledeptDetail.ods_idx = _ods_idx;
                _roledeptDetail.cemp_idx = _cemp_idx;
                _roledeptDetail.OrgIDX = int.Parse(lbOrgIDXRoleoer.Text);
                _roledeptDetail.RDeptIDX = int.Parse(lbRdepIDXRolePer.Text);
                _roledeptDetail.RSecIDX = int.Parse(lbRSecIDXRolePer.Text);
                _roledeptDetail.system_idx = int.Parse(lbsysidx.Text);

                // **** วนลูป CheckBox in Permission ****//
                string sumcheck_sysdept = "";

                foreach (ListItem rdopermissiontypes in rdorolepermissiondep.Items)
                {
                    if (rdopermissiontypes.Selected)
                    {
                        //_roleDetail.permission_idx = int.Parse(rdopermissiontypes.Value);

                        sumcheck_sysdept += rdopermissiontypes.Value + ",";
                    }
                }

                _roledeptDetail.rolepermission_idxstring = sumcheck_sysdept;

                _data_permission.role_list[0] = _roledeptDetail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_permission));

                _data_permission = callServicePermission(_urlSetRoleDept, _data_permission);

                //check return_code
                if (_data_permission.return_code == 0)
                {
                    //initPage();
                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);  

                    //actionSystem(0, _menuidxrole.ToString(), _systidxrole.ToString());
                    actionSystem(int.Parse(lbsysidx.Text), int.Parse(lbOrgIDXRoleoer.Text), int.Parse(lbRdepIDXRolePer.Text), int.Parse(lbRSecIDXRolePer.Text));
                    setFormData(fvClickSystem, FormViewMode.Insert, _data_permission.role_list, lbsysidx.Text, lbOrgIDXRoleoer.Text, lbRdepIDXRolePer.Text, lbRSecIDXRolePer.Text);


                }
                else
                {
                    setError(_data_permission.return_code.ToString() + " - " + _data_permission.return_msg);
                }



                break;

            case "cmdClearRoleDep": // ปุ่ม ยกเลิกกลับไปหน้าหลัก

                fvClickSystemPerDep.Visible = false;
                fvbtnroleperdep.Visible = false;

                break;

            case "chkAll":

                break;


            case "cmdClickallSystem":

                fvClickallSystemPerDep.Visible = true;
                fvallbtnroleperdep.Visible = true;
                //var fvbtnroleperdep = (FormView)fvClickSystem.FindControl("fvbtnroleperdep");

                //fvbtnroleperdep.Visible = true;
                _allsystem_idx = int.Parse(cmdArg);

                setFormData(fvClickallSystem, FormViewMode.Insert, _data_permission.role_list, _allsystem_idx.ToString(), "0", "0", "0");


                break;

            case "cmdSearchPermission":


                DropDownList ddlOrg_checkpermission1 = (DropDownList)PanelPermission.FindControl("ddlOrg_checkpermission");
                DropDownList ddlDept_checkpermission1 = (DropDownList)PanelPermission.FindControl("ddlDept_checkpermission");
                DropDownList ddlSec_checkpermission1 = (DropDownList)PanelPermission.FindControl("ddlSec_checkpermission");
                DropDownList ddlsystem_checkpermission1 = (DropDownList)PanelPermission.FindControl("ddlsystem_checkpermission");
               // FormView FvChecPermissionInSec = (FormView)Div_showPerSection.FindControl("FvChecPermissionInSec");


                _data_permission.role_list = new role_detail[1];
                role_detail _allroleDetail_check = new role_detail();

                //_roleDetailsys.role_idx = int.Parse(role_per);
                _allroleDetail_check.system_idx = int.Parse(ddlsystem_checkpermission1.SelectedValue);
                _allroleDetail_check.OrgIDX = int.Parse(ddlOrg_checkpermission1.SelectedValue);
                _allroleDetail_check.RDeptIDX = int.Parse(ddlDept_checkpermission1.SelectedValue);
                _allroleDetail_check.RSecIDX = int.Parse(ddlSec_checkpermission1.SelectedValue);


                _data_permission.role_list[0] = _allroleDetail_check;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_permission));
                _data_permission = callServicePermission(_urlGetRoleDeptSectionDetail, _data_permission);
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_permission));
                ////var rtu0idx = _dataMenu9.return_code;

                if(_data_permission.return_code == 0)
                {
                    Div_showPerSection.Visible = true;

                    FormView FvChecPermissionInSec = (FormView)Div_showPerSection.FindControl("FvChecPermissionInSec");
                    FvChecPermissionInSec.ChangeMode(FormViewMode.Edit);
                    FvChecPermissionInSec.Visible = true;

                    setFormData(FvChecPermissionInSec, FormViewMode.Edit, _data_permission.role_list, ddlsystem_checkpermission1.SelectedValue.ToString(), ddlOrg_checkpermission1.SelectedValue.ToString(), ddlDept_checkpermission1.SelectedValue.ToString(), ddlSec_checkpermission1.SelectedValue.ToString());

                    // setFormData(FvChecPermissionInSec, FormViewMode.Insert, _data_permission.role_list, _allsystem_idx.ToString(), "0", "0", "0");


                    CheckBoxList check_persection_detail = (CheckBoxList)FvChecPermissionInSec.FindControl("check_persection_detail");

                    check_persection_detail.AppendDataBoundItems = true;
                    check_persection_detail.DataSource = _data_permission.role_list;
                    check_persection_detail.DataTextField = "role_name_th";
                    check_persection_detail.DataValueField = "role_idx";
                    check_persection_detail.DataBind();

                    for (int i = 0; i < check_persection_detail.Items.Count; i++)
                    {
                        check_persection_detail.Items[i].Selected = true;
                    }
                }
                else
                {
                    Div_showPerSection.Visible = false;
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พอข้อมูล ---');", true);
                    break;
                }

                break;

            case "cmdSaveChangeRoleDep":


                DropDownList ddlOrg_checkpermission_change = (DropDownList)PanelPermission.FindControl("ddlOrg_checkpermission");
                DropDownList ddlDept_checkpermission_change = (DropDownList)PanelPermission.FindControl("ddlDept_checkpermission");
                DropDownList ddlSec_checkpermission_change = (DropDownList)PanelPermission.FindControl("ddlSec_checkpermission");
                DropDownList ddlsystem_checkpermission_change = (DropDownList)PanelPermission.FindControl("ddlsystem_checkpermission");


                FormView FvChecPermissionInSec_change = (FormView)Div_showPerSection.FindControl("FvChecPermissionInSec");
                FvChecPermissionInSec_change.ChangeMode(FormViewMode.Edit);
                FvChecPermissionInSec_change.Visible = true;

                CheckBoxList check_persection_detail_change = (CheckBoxList)FvChecPermissionInSec_change.FindControl("check_persection_detail");

                _ods_idx = int.Parse(cmdArg);

                _data_permission.role_list = new role_detail[1];

                _roledeptDetail.ods_idx = _ods_idx;
                _roledeptDetail.cemp_idx = emp_idx;
                _roledeptDetail.OrgIDX = int.Parse(ddlOrg_checkpermission_change.SelectedValue);
                _roledeptDetail.RDeptIDX = int.Parse(ddlDept_checkpermission_change.SelectedValue);
                _roledeptDetail.RSecIDX = int.Parse(ddlSec_checkpermission_change.SelectedValue);
                _roledeptDetail.system_idx = int.Parse(ddlsystem_checkpermission_change.SelectedValue);

                // **** วนลูป CheckBox in Permission ****//
                string sumcheck_sysdept_change = "";

                foreach (ListItem rdopermission_change in check_persection_detail_change.Items)
                {
                    if (rdopermission_change.Selected == false)
                    {
                        //_roleDetail.permission_idx = int.Parse(rdopermissiontypes.Value);

                        sumcheck_sysdept_change += rdopermission_change.Value + ",";
                    }
                }

                _roledeptDetail.rolepermission_idxstring = sumcheck_sysdept_change;

                _data_permission.role_list[0] = _roledeptDetail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_permission));

                _data_permission = callServicePermission(_urlUpdateSetRoleDept, _data_permission);

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "cmdallSaveRoleDep": // ปุ่ม save all permission

                permission_detail[] _tempList1 = (permission_detail[])ViewState["permission_list"];


                //for (int i = 0; i <= GvMaster.Rows.Count - 1; i++)
                for (int i = 0; i <= _tempList1.Count() - 1; i++)
                {

                    if (_tempList1[i].selected)
                    {


                        CheckBoxList rdoallrolepermissiondep = (CheckBoxList)fvClickallSystem.FindControl("rdoallrolepermissiondep");
                        Label lballsysidx = (Label)fvClickallSystem.FindControl("lballsysidx");
                        _ods_idx = int.Parse(cmdArg);
                        _cemp_idx = emp_idx;

                        //var cbrecipients = (GvMaster.Rows[i].Cells[8].FindControl("cbrecipients") as CheckBox).Checked;
                        int lbOrgIDXall = _tempList1[i].OrgIDX;//(GvMaster.Rows[i].Cells[1].FindControl("lbOrgIDX") as Label).Text;
                        int lbDepIDXall = _tempList1[i].RDeptIDX;//(GvMaster.Rows[i].Cells[3].FindControl("lbDepIDX") as Label).Text;
                        int lbRSecIDXall = _tempList1[i].RSecIDX;//(GvMaster.Rows[i].Cells[5].FindControl("lbRSecIDX") as Label).Text;


                        _data_permission.role_list = new role_detail[1];

                        _roledeptDetail.ods_idx = _ods_idx;
                        _roledeptDetail.cemp_idx = _cemp_idx;
                        _roledeptDetail.OrgIDX = lbOrgIDXall;//int.Parse(lbOrgIDXall);
                        _roledeptDetail.RDeptIDX = lbDepIDXall;//int.Parse(lbDepIDXall);
                        _roledeptDetail.RSecIDX = lbRSecIDXall;// int.Parse(lbRSecIDXall);
                        _roledeptDetail.system_idx = int.Parse(lballsysidx.Text);

                        //// // **** วนลูป CheckBox in Permission ****//
                        string allsumcheck_sysdept = "";

                        foreach (ListItem allrdopermissiontypes in rdoallrolepermissiondep.Items)
                        {
                            if (allrdopermissiontypes.Selected)
                            {
                                //_roleDetail.permission_idx = int.Parse(rdopermissiontypes.Value);

                                allsumcheck_sysdept += allrdopermissiontypes.Value + ",";
                            }
                        }


                        _roledeptDetail.rolepermission_idxstring = allsumcheck_sysdept;

                        _data_permission.role_list[0] = _roledeptDetail;

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_permission));

                        _data_permission = callServicePermission(_urlSetRoleDept, _data_permission);

                    }


                }

                Page.Response.Redirect(Page.Request.Url.ToString(), true);


                break;

            case "cmdallSystem":

                fvClicksomeSystemPerDep.Visible = true;
                fvsomebtnroleperdep.Visible = true;
                //var fvbtnroleperdep = (FormView)fvClickSystem.FindControl("fvbtnroleperdep");

                //fvbtnroleperdep.Visible = true;
                _systemallperdep_idx = int.Parse(cmdArg);


                setFormData(fvClicksomeSystem, FormViewMode.Insert, _data_permission.role_list, _systemallperdep_idx.ToString(), "0", "0", "0");


                break;

            //case "cmdsomeSaveRoleDep": // ปุ่ม save all sys permission

            //    permission_detail[] _tempList3 = (permission_detail[])ViewState["permission_list"];

            //    _data_permission.role_list = new role_detail[1];
            //    //for (int i = 0; i <= GvMaster.Rows.Count - 1; i++)

            //    var _u2doc_node4 = new role_detail[_tempList3.Count()];
            //    int count_u2doc_n4 = 0;
            //    string allsumcheck_sysdept1 = "";

            //    for (int i = 0; i <= _tempList3.Count() - 1; i++)
            //    {

            //        if (_tempList3[i].selected == false)
            //        {
            //            //Response.Write("22222");

            //            CheckBoxList rdosomerolepermissiondep = (CheckBoxList)fvClicksomeSystem.FindControl("rdosomerolepermissiondep");
            //            Label lbsomesysidx = (Label)fvClicksomeSystem.FindControl("lbsomesysidx");
            //            _ods_idx = int.Parse(cmdArg);
            //            _cemp_idx = emp_idx;

            //            //var cbrecipients = (GvMaster.Rows[i].Cells[8].FindControl("cbrecipients") as CheckBox).Checked;
            //            int lbOrgIDXall = _tempList3[i].OrgIDX;//(GvMaster.Rows[i].Cells[1].FindControl("lbOrgIDX") as Label).Text;
            //            int lbDepIDXall = _tempList3[i].RDeptIDX;//(GvMaster.Rows[i].Cells[3].FindControl("lbDepIDX") as Label).Text;
            //            int lbRSecIDXall = _tempList3[i].RSecIDX;//(GvMaster.Rows[i].Cells[5].FindControl("lbRSecIDX") as Label).Text;

            //            //if (cbrecipients == true)
            //            //{
            //            ////_data_permission.role_list = new role_detail[1];

            //            _u2doc_node4[count_u2doc_n4] = new role_detail();
            //            _u2doc_node4[count_u2doc_n4].ods_idx = _ods_idx;
            //            _u2doc_node4[count_u2doc_n4].cemp_idx = _cemp_idx;
            //            _u2doc_node4[count_u2doc_n4].OrgIDX = lbOrgIDXall;
            //            _u2doc_node4[count_u2doc_n4].RDeptIDX = lbDepIDXall;
            //            _u2doc_node4[count_u2doc_n4].RSecIDX = lbRSecIDXall;
            //            _u2doc_node4[count_u2doc_n4].system_idx = int.Parse(lbsomesysidx.Text);

            //            ////_roledeptDetail.ods_idx = _ods_idx;
            //            ////_roledeptDetail.cemp_idx = _cemp_idx;
            //            ////_roledeptDetail.OrgIDX = lbOrgIDXall;//int.Parse(lbOrgIDXall);
            //            ////_roledeptDetail.RDeptIDX = lbDepIDXall;//int.Parse(lbDepIDXall);
            //            ////_roledeptDetail.RSecIDX = lbRSecIDXall;// int.Parse(lbRSecIDXall);
            //            ////_roledeptDetail.system_idx = int.Parse(lbsomesysidx.Text);

            //            //// // **** วนลูป CheckBox in Permission ****//


            //            foreach (ListItem allrdopermissiontypes in rdosomerolepermissiondep.Items)
            //            {
            //                if (allrdopermissiontypes.Selected)
            //                {
            //                    //_roleDetail.permission_idx = int.Parse(rdopermissiontypes.Value);

            //                    allsumcheck_sysdept1 += allrdopermissiontypes.Value + ",";
            //                }
            //            }

            //            _u2doc_node4[0].rolepermission_idxstring = allsumcheck_sysdept1;
            //            //// _roledeptDetail.rolepermission_idxstring = allsumcheck_sysdept;

            //            ////_data_permission.role_list[0] = _roledeptDetail;

            //            ////litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_permission));

            //            //_data_permission = callServicePermission(_urlSetRoleDept, _data_permission);
            //            count_u2doc_n4++;
            //        }

            //    }

            //    _data_permission.role_list = _u2doc_node4;
            //    litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_permission));

            //    //Page.Response.Redirect(Page.Request.Url.ToString(), true);

            //    break;

            //case "cmdClearallRoleDep": // Clear ของ Selected

            //    Page.Response.Redirect(Page.Request.Url.ToString(), true);

            //    break;

            case "cmdsomeSaveRoleDep": // ปุ่ม save all sys permission

                permission_detail[] _tempList3 = (permission_detail[])ViewState["permission_list"];

                //for (int i = 0; i <= GvMaster.Rows.Count - 1; i++)
                for (int i = 0; i <= _tempList3.Count() - 1; i++)
                {

                    if (_tempList3[i].selected == false)
                    {
                        //Response.Write("22222");

                        CheckBoxList rdosomerolepermissiondep = (CheckBoxList)fvClicksomeSystem.FindControl("rdosomerolepermissiondep");
                        Label lbsomesysidx = (Label)fvClicksomeSystem.FindControl("lbsomesysidx");
                        _ods_idx = int.Parse(cmdArg);
                        _cemp_idx = emp_idx;

                        //var cbrecipients = (GvMaster.Rows[i].Cells[8].FindControl("cbrecipients") as CheckBox).Checked;
                        int lbOrgIDXall = _tempList3[i].OrgIDX;//(GvMaster.Rows[i].Cells[1].FindControl("lbOrgIDX") as Label).Text;
                        int lbDepIDXall = _tempList3[i].RDeptIDX;//(GvMaster.Rows[i].Cells[3].FindControl("lbDepIDX") as Label).Text;
                        int lbRSecIDXall = _tempList3[i].RSecIDX;//(GvMaster.Rows[i].Cells[5].FindControl("lbRSecIDX") as Label).Text;

                        //if (cbrecipients == true)
                        //{
                        _data_permission.role_list = new role_detail[1];

                        _roledeptDetail.ods_idx = _ods_idx;
                        _roledeptDetail.cemp_idx = _cemp_idx;
                        _roledeptDetail.OrgIDX = lbOrgIDXall;//int.Parse(lbOrgIDXall);
                        _roledeptDetail.RDeptIDX = lbDepIDXall;//int.Parse(lbDepIDXall);
                        _roledeptDetail.RSecIDX = lbRSecIDXall;// int.Parse(lbRSecIDXall);
                        _roledeptDetail.system_idx = int.Parse(lbsomesysidx.Text);

                        //// // **** วนลูป CheckBox in Permission ****//
                        string allsumcheck_sysdept = "";

                        foreach (ListItem allrdopermissiontypes in rdosomerolepermissiondep.Items)
                        {
                            if (allrdopermissiontypes.Selected)
                            {
                                //_roleDetail.permission_idx = int.Parse(rdopermissiontypes.Value);

                                allsumcheck_sysdept += allrdopermissiontypes.Value + ",";
                            }
                        }


                        _roledeptDetail.rolepermission_idxstring = allsumcheck_sysdept;

                        _data_permission.role_list[0] = _roledeptDetail;

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_permission));

                        _data_permission = callServicePermission(_urlSetRoleDept, _data_permission);

                    }

                }

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;


            case "cmdClearsomeRoleDep": // Clear ของ Selected all

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "cmdSearchIndexDetail":

                GvMaster.PageIndex = 0;

                DropDownList ddlOrg_search = (DropDownList)Dept_Excel.FindControl("ddlOrg_search");
                DropDownList ddlDept_search = (DropDownList)Dept_Excel.FindControl("ddlDept_search");
                DropDownList ddlSec_search = (DropDownList)Dept_Excel.FindControl("ddlSec_search");

                data_permission _data_searchdetail = new data_permission();
                _data_searchdetail.permission_list = new permission_detail[1];
                permission_detail _searchindex = new permission_detail();

                _searchindex.OrgIDX = int.Parse(ddlOrg_search.SelectedValue);
                _searchindex.RDeptIDX = int.Parse(ddlDept_search.SelectedValue);
                _searchindex.RSecIDX = int.Parse(ddlSec_search.SelectedValue);

                _data_searchdetail.permission_list[0] = _searchindex;

                _data_searchdetail = callServicePermission(_urlGetDetailSearch, _data_searchdetail);

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_searchdetail));

                ViewState["permission_list"] = _data_searchdetail.permission_list;

                setGridData(GvMaster, ViewState["permission_list"]);


                break;

            case "cmdresetSearch":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "cmdCheckPermission":

                GvMaster.Visible = false;
                Dept_Excel.Visible = false;
                btnCheckPermission.Visible = false;
                chk_permission.Visible = false;
                fvViewSystemallList.Visible = false;
                fvClickallSystemPerDep.Visible = false;
                fvallbtnroleperdep.Visible = false;

                fvViewallSystem.Visible = false;
                fvClicksomeSystemPerDep.Visible = false;
                fvsomebtnroleperdep.Visible = false;

                BackbtnCheckPermission.Visible = true;
                PanelPermission.Visible = true;

                ////fvDeptSelect
                DropDownList ddlOrg_checkpermission = (DropDownList)PanelPermission.FindControl("ddlOrg_checkpermission");
                DropDownList ddlDept_checkpermission = (DropDownList)PanelPermission.FindControl("ddlDept_checkpermission");
                DropDownList ddlSec_checkpermission = (DropDownList)PanelPermission.FindControl("ddlSec_checkpermission");
                DropDownList ddlsystem_checkpermission = (DropDownList)PanelPermission.FindControl("ddlsystem_checkpermission");
                //litDebug.Text = ViewState["vsMaterial"].ToString(); 1202884

                getOrganizationList((DropDownList)Dept_Excel.FindControl("ddlOrg_checkpermission"));
                getDepartmentList(ddlDept_checkpermission, int.Parse(ddlOrg_checkpermission.SelectedValue));
                getSectionList(ddlSec_checkpermission, int.Parse(ddlOrg_checkpermission.SelectedValue), int.Parse(ddlDept_checkpermission.SelectedValue));

                getSystemList((DropDownList)PanelPermission.FindControl("ddlsystem_checkpermission"));


                break;

            case "cmdBackPermission":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                //GvMaster.Visible = true;
                //Dept_Excel.Visible = true;
                //btnCheckPermission.Visible = true;

                //BackbtnCheckPermission.Visible = false;
                //PanelPermission.Visible = false;

                break;

        }
    }

    #endregion btnCommand

    #region Checkbox เชคแสดง
    protected void Checkbox(object sender, EventArgs e)
    {
        var cb = (CheckBox)sender;
        switch (cb.ID)
        {

            case "chkAll":

                // actionSystemall();
                if (chkAll.Checked)

                {

                    //actionSystemall();
                    fvallbtnroleperdep.Visible = false;
                    fvViewSystemallList.Visible = false;
                    fvClickallSystemPerDep.Visible = false;

                    fvViewallSystem.Visible = true;
                    actionSystemallperdep();

                    foreach (GridViewRow GVR in GvMaster.Rows)
                    {
                        CheckBox chk = (CheckBox)GVR.Cells[7].FindControl("cbrecipients");

                        if (ViewState["permission_list"] != null)
                        {
                            permission_detail[] _tempList = (permission_detail[])ViewState["permission_list"];

                            chk.Checked = true;
                            chk.Enabled = false;


                        }
                    }

                }
                else
                {

                    fvViewallSystem.Visible = false;
                    fvClicksomeSystemPerDep.Visible = false;
                    fvsomebtnroleperdep.Visible = false;


                    foreach (GridViewRow GVR in GvMaster.Rows)
                    {
                        CheckBox chk = (CheckBox)GVR.Cells[7].FindControl("cbrecipients");

                        chk.Checked = false;
                        chk.Enabled = true;
                    }
                }
                break;

            case "cbrecipients":


                fvViewallSystem.Visible = false;

                fvViewSystemallList.Visible = true;
                actionSystemall();


                if (ViewState["permission_list"] != null)
                {

                    permission_detail[] _tempList = (permission_detail[])ViewState["permission_list"];
                    int _checked = int.Parse(cb.Text);
                    _tempList[_checked].selected = cb.Checked;

                    //cb.Enabled = true;

                }

                break;

        }
    }
    #endregion

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "-1"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        data_employee _dataEmployee_rdept = new data_employee();
        _dataEmployee_rdept.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee_rdept.department_list[0] = _deptList;

        _dataEmployee_rdept = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee_rdept);
        setDdlData(ddlName, _dataEmployee_rdept.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "-1"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "-1"));
    }

    protected void getSystemList(DropDownList ddlName)
    {
        data_permission _data_permission_check = new data_permission();
        _data_permission_check.systemall_list = new systemall_detail[1];
        systemall_detail _checkpermissionl = new systemall_detail();

        _data_permission_check.systemall_list[0] = _checkpermissionl;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_permission1));
        _data_permission_check = callServicePermission(_urlGetcheckall, _data_permission_check);
        //var rtorgidx = _data_permission1.return_orgidx;
        //var rtrdepidx = _data_permission1.return_rdepidx;

        // check return_code
        if (_data_permission_check.return_code == 0)
        {

            setDdlData(ddlName, _data_permission_check.systemall_list, "system_name_th", "system_idx");
            ddlName.Items.Insert(0, new ListItem("--- เลือกระบบ ---", "-1"));
            ////ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "-1"));
            //setDataList(dtlallSystemPerDep, _data_permission_check.systemall_list);

            //setGridData(GvMaster, _data_permission1.system_list);
        }
        else
        {
            setError(_data_permission_check.return_code.ToString() + " - " + _data_permission_check.return_msg);
        }



        ////_dataEmployee.organization_list = new organization_details[1];
        ////organization_details _orgList = new organization_details();
        ////_orgList.org_idx = 0;
        ////_dataEmployee.organization_list[0] = _orgList;

        ////_dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        //////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        ////setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ////ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "-1"));
    }



    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        //ddl
        DropDownList ddlOrg_search = (DropDownList)Dept_Excel.FindControl("ddlOrg_search");
        DropDownList ddlDept_search = (DropDownList)Dept_Excel.FindControl("ddlDept_search");
        DropDownList ddlSec_search = (DropDownList)Dept_Excel.FindControl("ddlSec_search");

        //ddl check permission    
        DropDownList ddlOrg_checkpermission = (DropDownList)PanelPermission.FindControl("ddlOrg_checkpermission");
        DropDownList ddlDept_checkpermission = (DropDownList)PanelPermission.FindControl("ddlDept_checkpermission");
        DropDownList ddlSec_checkpermission = (DropDownList)PanelPermission.FindControl("ddlSec_checkpermission");


        switch (ddlName.ID)
        {
            case "ddlOrg_search":
                // ddlDocType.Focus();
                getDepartmentList(ddlDept_search, int.Parse(ddlOrg_search.SelectedItem.Value));
                ddlSec_search.Items.Clear();
                ddlSec_search.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "-1"));
                // ddlSec_search.Items.Clear();
                break;
            case "ddlDept_search":
                //ddlDocType.Focus();
                getSectionList(ddlSec_search, int.Parse(ddlOrg_search.SelectedItem.Value), int.Parse(ddlDept_search.SelectedItem.Value));

                break;
            case "ddlOrg_checkpermission":
                // ddlDocType.Focus();
                getDepartmentList(ddlDept_checkpermission, int.Parse(ddlOrg_checkpermission.SelectedItem.Value));
                ddlSec_checkpermission.Items.Clear();
                ddlSec_checkpermission.Items.Insert(0, new ListItem("--- เลือกแผนก ---", "-1"));
                // ddlSec_search.Items.Clear();
                break;
            case "ddlDept_checkpermission":
                //ddlDocType.Focus();
                getSectionList(ddlSec_checkpermission, int.Parse(ddlOrg_checkpermission.SelectedItem.Value), int.Parse(ddlDept_checkpermission.SelectedItem.Value));

                break;

        }
    }

    #region bind data
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj, string sysidxdep, string orgidxdep, string rdeptdep, string rsecdep)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        switch (fvName.ID)
        {
            case "fvClickSystem":

                HiddenField hfSysPerDep = (HiddenField)fvClickSystem.FindControl("hfSysPerDep");
                Label lbOrgIDXRoleoer = (Label)fvClickSystem.FindControl("lbOrgIDXRoleoer");
                Label lbRdepIDXRolePer = (Label)fvClickSystem.FindControl("lbRdepIDXRolePer");
                Label lbRSecIDXRolePer = (Label)fvClickSystem.FindControl("lbRSecIDXRolePer");

                Label lbrolesysperidx = (Label)fvClickSystem.FindControl("lbrolesysperidx");
                Label lbsysidx = (Label)fvClickSystem.FindControl("lbsysidx");
                CheckBoxList rdorolepermissiondep = (CheckBoxList)fvClickSystem.FindControl("rdorolepermissiondep");

                lbsysidx.Text = sysidxdep;
                lbOrgIDXRoleoer.Text = orgidxdep;
                lbRdepIDXRolePer.Text = rdeptdep;
                lbRSecIDXRolePer.Text = rsecdep;

                _data_permission.role_list = new role_detail[1];
                role_detail _roleDetail = new role_detail();

                //_roleDetailsys.role_idx = int.Parse(role_per);
                _roleDetail.system_idx = int.Parse(lbsysidx.Text);
                // _roleDetailsys.menu_idx = int.Parse(menuidx_per);

                _data_permission.role_list[0] = _roleDetail;

                _data_permission = callServicePermission(_urlGetRoleDept, _data_permission);
                //var rtu0idx = _dataMenu9.return_code;

                rdorolepermissiondep.AppendDataBoundItems = true;
                rdorolepermissiondep.DataSource = _data_permission.role_list;
                rdorolepermissiondep.DataTextField = "role_name_th";
                rdorolepermissiondep.DataValueField = "role_idx";
                rdorolepermissiondep.DataBind();

                

                break;

            case "fvClickallSystem":

                HiddenField hfallSysPerDep = (HiddenField)fvClickallSystem.FindControl("hfallSysPerDep");
                Label lballOrgIDXRoleoer = (Label)fvClickallSystem.FindControl("lballOrgIDXRoleoer");
                Label lballRdepIDXRolePer = (Label)fvClickallSystem.FindControl("lballRdepIDXRolePer");
                Label lballRSecIDXRolePer = (Label)fvClickallSystem.FindControl("lballRSecIDXRolePer");

                //Label lbrolesysperidx = (Label)fvClickSystem.FindControl("lbrolesysperidx");
                Label lballsysidx = (Label)fvClickallSystem.FindControl("lballsysidx");
                CheckBoxList rdoallrolepermissiondep = (CheckBoxList)fvClickallSystem.FindControl("rdoallrolepermissiondep");

                lballsysidx.Text = sysidxdep;
                //lbOrgIDXRoleoer.Text = orgidxdep;
                //lbRdepIDXRolePer.Text = rdeptdep;
                //lbRSecIDXRolePer.Text = rsecdep;

                _data_permission.role_list = new role_detail[1];
                role_detail _allroleDetail = new role_detail();

                //_roleDetailsys.role_idx = int.Parse(role_per);
                _allroleDetail.system_idx = int.Parse(lballsysidx.Text);
                // _roleDetailsys.menu_idx = int.Parse(menuidx_per);

                _data_permission.role_list[0] = _allroleDetail;

                _data_permission = callServicePermission(_urlGetRoleDept, _data_permission);
                ////var rtu0idx = _dataMenu9.return_code;

                rdoallrolepermissiondep.AppendDataBoundItems = true;
                rdoallrolepermissiondep.DataSource = _data_permission.role_list;
                rdoallrolepermissiondep.DataTextField = "role_name_th";
                rdoallrolepermissiondep.DataValueField = "role_idx";
                rdoallrolepermissiondep.DataBind();

                

                break;

            
            case "fvClicksomeSystem":

                HiddenField hfsomeSysPerDep = (HiddenField)fvClicksomeSystem.FindControl("hfsomeSysPerDep");
                Label lbsomeOrgIDXRoleoer = (Label)fvClicksomeSystem.FindControl("lbsomeOrgIDXRoleoer");
                Label lbsomeRdepIDXRolePer = (Label)fvClicksomeSystem.FindControl("lbsomeRdepIDXRolePer");
                Label lbsomeRSecIDXRolePer = (Label)fvClicksomeSystem.FindControl("lbsomeRSecIDXRolePer");

                //Label lbrolesysperidx = (Label)fvClickSystem.FindControl("lbrolesysperidx");
                Label lbsomesysidx = (Label)fvClicksomeSystem.FindControl("lbsomesysidx");
                CheckBoxList rdosomerolepermissiondep = (CheckBoxList)fvClicksomeSystem.FindControl("rdosomerolepermissiondep");

                lbsomesysidx.Text = sysidxdep;
                //lbOrgIDXRoleoer.Text = orgidxdep;
                //lbRdepIDXRolePer.Text = rdeptdep;
                //lbRSecIDXRolePer.Text = rsecdep;

                _data_permission.role_list = new role_detail[1];
                role_detail _allsysroleDetail = new role_detail();

                //_roleDetailsys.role_idx = int.Parse(role_per);
                _allsysroleDetail.system_idx = int.Parse(lbsomesysidx.Text);
                // _roleDetailsys.menu_idx = int.Parse(menuidx_per);

                _data_permission.role_list[0] = _allsysroleDetail;

                _data_permission = callServicePermission(_urlGetRoleDept, _data_permission);
                ////var rtu0idx = _dataMenu9.return_code;

                rdosomerolepermissiondep.AppendDataBoundItems = true;
                rdosomerolepermissiondep.DataSource = _data_permission.role_list;
                rdosomerolepermissiondep.DataTextField = "role_name_th";
                rdosomerolepermissiondep.DataValueField = "role_idx";
                rdosomerolepermissiondep.DataBind();


               

                break;

            //case "FvChecPermissionInSec":

            //    HiddenField hfallSysPerDepSec = (HiddenField)FvChecPermissionInSec.FindControl("hfallSysPerDepSec");
            //    //Label lballOrgIDXRoleoer = (Label)fvClickallSystem.FindControl("lballOrgIDXRoleoer");
            //    //Label lballRdepIDXRolePer = (Label)fvClickallSystem.FindControl("lballRdepIDXRolePer");
            //    //Label lballRSecIDXRolePer = (Label)fvClickallSystem.FindControl("lballRSecIDXRolePer");

            //    //Label lbrolesysperidx = (Label)fvClickSystem.FindControl("lbrolesysperidx");
            //    //Label lballsysidx = (Label)fvClickallSystem.FindControl("lballsysidx");

            //    CheckBoxList check_persection_detail = (CheckBoxList)FvChecPermissionInSec.FindControl("check_persection_detail");

            //    //lballsysidx.Text = sysidxdep;
            //    //lbOrgIDXRoleoer.Text = orgidxdep;
            //    //lbRdepIDXRolePer.Text = rdeptdep;
            //    //lbRSecIDXRolePer.Text = rsecdep;

            //    _data_permission.role_list = new role_detail[1];
            //    role_detail _allroleDetail_check = new role_detail();

            //    //_roleDetailsys.role_idx = int.Parse(role_per);
            //    _allroleDetail_check.system_idx = int.Parse(sysidxdep);
            //    _allroleDetail_check.OrgIDX = int.Parse(orgidxdep);
            //    _allroleDetail_check.RDeptIDX = int.Parse(rdeptdep);
            //    _allroleDetail_check.RSecIDX = int.Parse(rsecdep);

              
            //    _data_permission.role_list[0] = _allroleDetail_check;

            //    litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_permission));
            //   // _data_permission = callServicePermission(_urlGetRoleDeptSectionDetail, _data_permission);
            //    ////var rtu0idx = _dataMenu9.return_code;

            //    check_persection_detail.AppendDataBoundItems = true;
            //    check_persection_detail.DataSource = _data_permission.role_list;
            //    check_persection_detail.DataTextField = "role_name_th";
            //    check_persection_detail.DataValueField = "role_idx";
            //    check_persection_detail.DataBind();

            //    for (int i = 0; i < check_persection_detail.Items.Count; i++)
            //    {
            //        check_persection_detail.Items[i].Selected = true;
            //    }

            //    break;




        }
    }

    #endregion bind data


    #region Paging
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                actionIndex(0);



                break;

        }
    }

    #endregion

    #region DataBound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {


        //role_detail _roledeptDetail = new role_detail();
        //Checking the RowType of the Row  
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int index = e.Row.RowIndex;
            //  CheckBox chk = (CheckBox)GvMaster.Rows[i].Cells[7].FindControl("cbrecipients");
            CheckBox cbrecipients = (CheckBox)e.Row.FindControl("cbrecipients");

            //test
            HiddenField hfSelected = (HiddenField)e.Row.FindControl("hfSelected");
            //test

            if (chkAll.Checked || bool.Parse(hfSelected.Value) == true)
            {
                cbrecipients.Checked = true;

                if (chkAll.Checked && cbrecipients.Checked)
                {
                    cbrecipients.Enabled = false;
                }


            }

            else
            {
                cbrecipients.Checked = false;
                cbrecipients.Enabled = true;
            }

        }
    }

    #endregion

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        ////setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();

    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_permission callServicePermission(string _cmdUrl, data_permission _data_permission)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_permission);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_permission = (data_permission)_funcTool.convertJsonToObject(typeof(data_permission), _localJson);

        return _data_permission;
    }

    protected data_system callServiceMenuSystem(string _cmdUrl, data_system _data_system)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_system);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_system = (data_system)_funcTool.convertJsonToObject(typeof(data_system), _localJson);

        return _data_system;
    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)

    {
        //// convert to json
        // _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _data_employee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    #endregion reuse 
}