﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for EventCarDAO
/// </summary>
public class EventCarDAO
{
    //change the connection string as per your database connection.
    private static string connectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;

    //this method retrieves all events within range start-end
    //public static List<CalendarEvent> getEvents(DateTime start, DateTime end, int place_idx)
    public static List<CalendarCarEvent> getEvents(DateTime start, DateTime end, int detailtype_car_idx, int m0_car_idx, int car_use_idx)
    {
        List<CalendarCarEvent> events = new List<CalendarCarEvent>();
        SqlConnection con = new SqlConnection(connectionString);
        //SqlCommand cmd = new SqlCommand("SELECT event_id, description, title, event_start, event_end, all_day FROM rbk_test_event", con);
        SqlCommand cmd = new SqlCommand("SELECT u0_document_idx, objective_booking, place_name_other, date_start_checkdate, date_end_checkdate, place_idx, place_name, emp_name_th, car_register, m0_car_idx, detailtype_car_idx, detailtype_car_name, car_use_idx, car_use_name, type_booking_name FROM view_cbk_detailcalendar where (staidx = 4 OR staidx = 3) AND (date_start_checkdate>=@start OR date_end_checkdate<=@end) AND (@detailtype_car_idx = 0 OR detailtype_car_idx = @detailtype_car_idx) AND (@m0_car_idx = 0 OR m0_car_idx = @m0_car_idx) AND (@car_use_idx = 0 OR car_use_idx = @car_use_idx)"
            //AND(@detailtype_car_idx = 0 OR detailtype_car_idx = @detailtype_car_idx) AND(@m0_car_idx = 0 OR m0_car_idx = @m0_car_idx)
            //"left join cbk_u2_car_document u2 on u0.u0_document_idx = u2.u0_document_idx" 
            //"inner join [mas].[dbo].[cbk_m0_car] m0_car on u2.m0_car_idx = m0_car.m0_car_idx"
            //"left join cbk_u2_car_document u2 on u0.u0_document_idx = u2.u0_document_idx " +
            //"left join cbk_m0_car] m0_car m0_car on u2.m0_car_idx = m0_car.m0_car_idx " +
            //"left join M0_Province m0_province on m0_car.car_province_idx = m0_province.ProvIDX" +
            //"where (staidx = 4) " +
            //"AND date_start_checkdate>=@start " +
            //"AND date_end_checkdate<=@end " +
            //"AND (@place_idx = 0 OR place_idx = @place_idx) " +
            //"AND (@m0_car_idx = 0 OR m0_car_idx = @m0_car_idx)"
            , con);// AND date_start_checkdate>=@start AND date_end_checkdate<=@end
        cmd.Parameters.Add("@start", SqlDbType.DateTime).Value = start;
        cmd.Parameters.Add("@end", SqlDbType.DateTime).Value = end;
        cmd.Parameters.Add("@detailtype_car_idx", SqlDbType.Int).Value = detailtype_car_idx;
        cmd.Parameters.Add("@m0_car_idx", SqlDbType.Int).Value = m0_car_idx;
        cmd.Parameters.Add("@car_use_idx", SqlDbType.Int).Value = car_use_idx;

        using (con)
        {
            con.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                events.Add(new CalendarCarEvent()
                {
                    id = Convert.ToInt32(reader["u0_document_idx"]),
                    //title = Convert.ToString("เลขทะเบียน :" + " " + reader["car_register"]) + " " + Convert.ToString("สถานที่ :" + " " + reader["place_name"]) + " " + Convert.ToString("จองโดย :" + " " + reader["emp_name_th"]),
                    title = Convert.ToString("เลขทะเบียน :" + " " + reader["car_register"]) + " " + Convert.ToString("จองโดย :" + " " + reader["emp_name_th"]),
                    description = Convert.ToString(reader["objective_booking"]),
                    start = Convert.ToDateTime(reader["date_start_checkdate"]),
                    end = Convert.ToDateTime(reader["date_end_checkdate"]),
                    place_idx = Convert.ToInt32(reader["place_idx"]),
                    place_name = Convert.ToString(reader["place_name"]),
                    //m0_room_idx = Convert.ToInt32(reader["m0_room_idx"]),
                    car_register = Convert.ToString(reader["car_register"]),
                    topic_booking = Convert.ToString(reader["place_name_other"]),
                    emp_name_th = Convert.ToString(reader["emp_name_th"]),
                    detailtype_car_name = Convert.ToString(reader["detailtype_car_name"]),
                    car_use_name = Convert.ToString(reader["car_use_name"]),
                    type_booking_name = Convert.ToString(reader["type_booking_name"])
                    //allDay = Convert.ToBoolean(reader["all_day"])
                });
            }
        }
        return events;


        //side note: if you want to show events only related to particular users,
        //if user id of that user is stored in session as Session["userid"]
        //the event table also contains an extra field named 'user_id' to mark the event for that particular user
        //then you can modify the SQL as:
        //SELECT event_id, description, title, event_start, event_end FROM event where user_id=@user_id AND event_start>=@start AND event_end<=@end
        //then add paramter as:cmd.Parameters.AddWithValue("@user_id", HttpContext.Current.Session["userid"]);
    }

    //this method updates the event title and description
    public static void updateEvent(int id, String title, String description)
    {
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("UPDATE Event SET title=@title, description=@description WHERE event_id=@event_id", con);
        cmd.Parameters.Add("@title", SqlDbType.VarChar).Value = title;
        cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = description;
        cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = id;

        using (con)
        {
            con.Open();
            cmd.ExecuteNonQuery();
        }
    }

    //this method updates the event start and end time ... allDay parameter added for FullCalendar 2.x
    public static void updateEventTime(int id, DateTime start, DateTime end, bool allDay)
    {
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("UPDATE Event SET event_start=@event_start, event_end=@event_end, all_day=@all_day WHERE event_id=@event_id", con);
        cmd.Parameters.Add("@event_start", SqlDbType.DateTime).Value = start;
        cmd.Parameters.Add("@event_end", SqlDbType.DateTime).Value = end;
        cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = id;
        cmd.Parameters.Add("@all_day", SqlDbType.Bit).Value = allDay;

        using (con)
        {
            con.Open();
            cmd.ExecuteNonQuery();
        }
    }

    //this mehtod deletes event with the id passed in.
    public static void deleteEvent(int id)
    {
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("DELETE FROM Event WHERE (event_id = @event_id)", con);
        cmd.Parameters.Add("@event_id", SqlDbType.Int).Value = id;

        using (con)
        {
            con.Open();
            cmd.ExecuteNonQuery();
        }
    }

    //this method adds events to the database
    public static int addEvent(CalendarCarEvent cevent)
    {
        //add event to the database and return the primary key of the added event row

        //insert
        SqlConnection con = new SqlConnection(connectionString);
        SqlCommand cmd = new SqlCommand("INSERT INTO Event(title, description, event_start, event_end, all_day) VALUES(@title, @description, @event_start, @event_end, @all_day)", con);
        cmd.Parameters.Add("@title", SqlDbType.VarChar).Value = cevent.title;
        cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = cevent.description;
        cmd.Parameters.Add("@event_start", SqlDbType.DateTime).Value = cevent.start;
        cmd.Parameters.Add("@event_end", SqlDbType.DateTime).Value = cevent.end;
        cmd.Parameters.Add("@all_day", SqlDbType.Bit).Value = cevent.allDay;

        int key = 0;
        using (con)
        {
            con.Open();
            cmd.ExecuteNonQuery();

            //get primary key of inserted row
            cmd = new SqlCommand("SELECT max(event_id) FROM Event where title=@title AND description=@description AND event_start=@event_start AND event_end=@event_end AND all_day=@all_day", con);
            cmd.Parameters.Add("@title", SqlDbType.VarChar).Value = cevent.title;
            cmd.Parameters.Add("@description", SqlDbType.VarChar).Value = cevent.description;
            cmd.Parameters.Add("@event_start", SqlDbType.DateTime).Value = cevent.start;
            cmd.Parameters.Add("@event_end", SqlDbType.DateTime).Value = cevent.end;
            cmd.Parameters.Add("@all_day", SqlDbType.Bit).Value = cevent.allDay;

            key = (int)cmd.ExecuteScalar();
        }

        return key;
    }
}
