﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_qa_cims_m0_unit : System.Web.UI.Page
{
    

    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    data_qa_cims _data_qa_cims = new data_qa_cims();


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlCimsSetUnit = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetUnit"];
    static string _urlCimsGetUnit = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetUnit"];
    static string _urlCimsDeleteUnit = _serviceUrl + ConfigurationManager.AppSettings["urlCimsDeleteUnit"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //ViewState["empIDX"] = Session["emp_idx"];
            Select_Unit();


        }
    }
    #endregion Page Load

    #region initPage
    protected void initPage()
    {
        MultiView1.SetActiveView(View1);
        Select_Unit();
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdAddUnit":

                Gv_select_unit.EditIndex = -1;
                Select_Unit();
                btn_addunit.Visible = false;
                setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                Fv_Insert_Result.Visible = true;
                //Gv_select_place.Visible = false;


                break;

            case "Lbtn_submit_unit":
                Insert_Unit();
                Select_Unit();
                btn_addunit.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;

            case "Lbtn_cancel_unit":
                btn_addunit.Visible = true;
                Fv_Insert_Result.Visible = false;
                //  setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                //Gv_select_unit.Visible = false;
                // Gv_select_place.Visible = true;
                break;

            case "btnTodelete_unit":
                // ViewState["DELETE_PLACE"] = int.Parse(cmdArg);

                int a = int.Parse(cmdArg);
                data_qa_cims Unit_de = new data_qa_cims();
                qa_cims_m0_unit_detail Unit_sde = new qa_cims_m0_unit_detail();

                Unit_de.qa_cims_m0_unit_list = new qa_cims_m0_unit_detail[1];

                Unit_sde.unit_idx = int.Parse(a.ToString());

                Unit_de.qa_cims_m0_unit_list[0] = Unit_sde;
                //  test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_de));
                Unit_de = callServicePostMasterQACIMS(_urlCimsDeleteUnit, Unit_de);

                //if (Unit_de.return_code == 0)
                //{

                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการสำเร็จ');", true);

                //}

                //else

                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ');", true);


                //}

                Select_Unit();
                //Gv_select_unit.Visible = false;
                btn_addunit.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;
        }
    }
    #endregion btnCommand

    #region Custom Functions
    protected void Insert_Unit()
    {
        FormView MvDocDetail_insert_qa_cims = (FormView)View1.FindControl("Fv_Insert_Result");
        TextBox tex_unit_name_th = (TextBox)MvDocDetail_insert_qa_cims.FindControl("txtunit_name_th");
        TextBox tex_unit_name_en = (TextBox)MvDocDetail_insert_qa_cims.FindControl("txtunit_name_en");
        TextBox tex_unit_symbol_th = (TextBox)MvDocDetail_insert_qa_cims.FindControl("txtunit_symbol_th");
        TextBox tex_unit_symbol_en = (TextBox)MvDocDetail_insert_qa_cims.FindControl("txtunit_symbol_en");
        DropDownList dropD_status_unit = (DropDownList)MvDocDetail_insert_qa_cims.FindControl("ddUnit");

        data_qa_cims Unit_b = new data_qa_cims();
        qa_cims_m0_unit_detail Unit_s = new qa_cims_m0_unit_detail();
        Unit_b.qa_cims_m0_unit_list = new qa_cims_m0_unit_detail[1];
        Unit_s.unit_name_th = tex_unit_name_th.Text;
        Unit_s.unit_name_en = tex_unit_name_en.Text;
        Unit_s.unit_symbol_th = tex_unit_symbol_th.Text;
        Unit_s.unit_symbol_en = tex_unit_symbol_en.Text;
        Unit_s.cemp_idx = _emp_idx;
        Unit_s.unit_status = int.Parse(dropD_status_unit.SelectedValue);
        Unit_b.qa_cims_m0_unit_list[0] = Unit_s;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        Unit_b = callServicePostMasterQACIMS(_urlCimsSetUnit, Unit_b);

        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Unit_b.return_code));
        if (Unit_b.return_code == 101)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            tex_unit_name_th.Text = String.Empty;
            tex_unit_name_en.Text = String.Empty;
            tex_unit_symbol_th.Text = String.Empty;
            tex_unit_symbol_en.Text = String.Empty;
        }
    }

    protected void Select_Unit()
    {
        data_qa_cims Unit_b = new data_qa_cims();
        qa_cims_m0_unit_detail Unit_s = new qa_cims_m0_unit_detail();
        Unit_b.qa_cims_m0_unit_list = new qa_cims_m0_unit_detail[1];

        Unit_s.condition = 1;

        Unit_b.qa_cims_m0_unit_list[0] = Unit_s;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));
        Unit_b = callServicePostMasterQACIMS(_urlCimsGetUnit, Unit_b);
        setGridData(Gv_select_unit, Unit_b.qa_cims_m0_unit_list);
        //Gv_select_unit.DataSource = Unit_b.qa_cims_m0_unit_list;
        //Gv_select_unit.DataBind();
    }
    #endregion Custom Functions

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "Gv_select_unit":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbpunit_status = (Label)e.Row.Cells[3].FindControl("lbpunit_status");
                    Label unit_statusOnline = (Label)e.Row.Cells[3].FindControl("unit_statusOnline");
                    Label unit_statusOffline = (Label)e.Row.Cells[3].FindControl("unit_statusOffline");
                    Label lbunit_symbol_en = (Label)e.Row.Cells[4].FindControl("lbunit_symbol_en");
                    LinkButton btnEdit = (LinkButton)e.Row.Cells[6].FindControl("btnEdit");
                    LinkButton btnTodelete = (LinkButton)e.Row.Cells[6].FindControl("btnTodelete");

                    ViewState["_unit_status"] = lbpunit_status.Text;


                    if (ViewState["_unit_status"].ToString() == "1")
                    {
                        unit_statusOnline.Visible = true;
                    }
                    else if (ViewState["_unit_status"].ToString() == "0")
                    {
                        unit_statusOffline.Visible = true;
                    }

                    if(lbunit_symbol_en.Text == "microlite")
                    {
                        lbunit_symbol_en.Text = "µl";
                        btnEdit.Visible = false;
                        btnTodelete.Visible = false;
                    }
                    else if(lbunit_symbol_en.Text == "micrometer")
                    {
                        lbunit_symbol_en.Text = "µm";
                        btnEdit.Visible = false;
                        btnTodelete.Visible = false;
                        //lbunit_symbol_en.Text = "µl";
                    }
                    else if (lbunit_symbol_en.Text == "degree Cel")
                    {
                        lbunit_symbol_en.Text = "°C";
                        btnEdit.Visible = false;
                        btnTodelete.Visible = false;
                        //lbunit_symbol_en.Text = "µl";
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                    //btn_addplace.Visible = true;
                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);

                    //MultiView1.Visible = true;
                    //Gv_select_unit.Visible = false;
                    btn_addunit.Visible = true;
                    Fv_Insert_Result.Visible = false;

                }

                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_unit":
                Gv_select_unit.EditIndex = e.NewEditIndex;
                Select_Unit();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "Gv_select_unit":
                
                var unit_idx = (TextBox)Gv_select_unit.Rows[e.RowIndex].FindControl("ID_unit");
                var unit_name_th = (TextBox)Gv_select_unit.Rows[e.RowIndex].FindControl("Name_unit_th");
                var unit_name_en = (TextBox)Gv_select_unit.Rows[e.RowIndex].FindControl("Name_unit_en");
                var unit_symbol_th = (TextBox)Gv_select_unit.Rows[e.RowIndex].FindControl("Symbol_unit_th");
                var unit_symbol_en = (TextBox)Gv_select_unit.Rows[e.RowIndex].FindControl("Symbol_unit_en");
                var status_unit = (DropDownList)Gv_select_unit.Rows[e.RowIndex].FindControl("ddEdit_unit");


                Gv_select_unit.EditIndex = -1;

                data_qa_cims unit_Update = new data_qa_cims();
                unit_Update.qa_cims_m0_unit_list = new qa_cims_m0_unit_detail[1];
                qa_cims_m0_unit_detail unit_s = new qa_cims_m0_unit_detail();
                unit_s.unit_idx = int.Parse(unit_idx.Text);
                unit_s.unit_name_th = unit_name_th.Text;
                unit_s.unit_name_en = unit_name_en.Text;
                unit_s.unit_symbol_th = unit_symbol_th.Text;
                unit_s.unit_symbol_en = unit_symbol_en.Text;
                unit_s.unit_status = int.Parse(status_unit.SelectedValue);

                unit_Update.qa_cims_m0_unit_list[0] = unit_s;

                //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(unit_Update));

                unit_Update = callServicePostMasterQACIMS(_urlCimsSetUnit, unit_Update);

                Select_Unit();
                
                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "Gv_select_unit":
                Gv_select_unit.EditIndex = -1;
                Select_Unit();
                //btn_addplace.Visible = true;
                //SETFOCUS_ONTOP.Focus();
                //Gv_select_unit.Visible = false;
                btn_addunit.Visible = true;
                Fv_Insert_Result.Visible = false;
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_unit":
                Gv_select_unit.PageIndex = e.NewPageIndex;
                Gv_select_unit.DataBind();
                Select_Unit();
                break;

        }
    }
    #endregion

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);


        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
    #endregion callService Functions
}
