﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class websystem_MasterData_qa_lab_setname : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_qa _data_qa = new data_qa();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- master data setname --//
    static string _urlQaSetNameList = _serviceUrl + ConfigurationManager.AppSettings["urlQaSetNameList"];
    static string _urlQaGetNameList = _serviceUrl + ConfigurationManager.AppSettings["urlQaGetNameList"];
    static string _urlQaDelSetName = _serviceUrl + ConfigurationManager.AppSettings["urlQaDelSetName"];
    
    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;

    // rdept permission
    // int[] rdept_qmr = { 20 }; //QMR:26
    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        ////foreach (int item in rdept_qmr)
        ////{
        ////    if (_dataEmployee.employee_list[0].rdept_idx == item)
        ////    {
        ////        _flag_qmr = true;
        ////        break;
        ////    }
        ////}
    }


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
        }

      //  linkBtnTrigger(lnkbtn_addset);
    }
    #region event command
  
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdAddSet":
                switch (cmdArg)
                {
                    case "0":
                        setFormData(fvformInsert, FormViewMode.Insert, null);
                        break;

                    case "1":
                        setFormData(fvformInsertRoot, FormViewMode.Insert, null);
                     
                         getM0SetNameList();
                        break;
                    case "2":
                        
                        setFormData(fvformInsertSubSet, FormViewMode.Insert, null);
                        getM0RootSetList();

                        break;

                }
               
                break;

            case "cmdCancel":
                switch (cmdArg)
                {
                    case "0":
                        setActiveView("pageGenaral", 0);
                        setFormData(fvformInsert, FormViewMode.ReadOnly, null);
                        SETFOCUS.Focus();
                        break;

                    case "1":
                        setActiveView("pageCreateRoot", 0);
                        if ((ViewState["ROOTIDX"].ToString()) != null)
                        {
                            ViewState["set_idx"] = ViewState["ROOTIDX"].ToString();
                            getM0SetNameList();
                            setFormData(fvformInsertRoot, FormViewMode.ReadOnly, null);
                        }
                        else
                        {
                            ViewState["set_idx"] = 0;
                            setFormData(fvformInsertRoot, FormViewMode.Insert, null);
                        }

                        SETFOCUS.Focus();
                        break;

                    case "2":
                        setActiveView("pageRootSubSet", 0);
                        setFormData(fvformInsertSubSet, FormViewMode.ReadOnly, null);
                        SETFOCUS.Focus();
                        break;
                }
               
                break;

            case "cmdSave":
                switch (cmdArg)
                {
                    case "0":
                        TextBox tbSetName = (TextBox)fvformInsert.FindControl("tbSetName");
                        TextBox tbDetailSet = (TextBox)fvformInsert.FindControl("tbDetailSet");
                        DropDownList ddl_setname = (DropDownList)fvformInsert.FindControl("ddl_setname");

                        _data_qa.qa_m0_setname_list = new qa_m0_setname_detail[1];
                        qa_m0_setname_detail _m0setname = new qa_m0_setname_detail();
                        _m0setname.cemp_idx = _emp_idx;
                        _m0setname.set_name = tbSetName.Text;
                        _m0setname.set_name_status = int.Parse(ddl_setname.SelectedValue);
                        _m0setname.set_detail = tbDetailSet.Text;

                        _data_qa.qa_m0_setname_list[0] = _m0setname;
                    
                        _data_qa = callServicePostMasterQA(_urlQaSetNameList, _data_qa);
                        //-- check data duplicate in database
                        if (_data_qa.return_code == 101)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                          
                        }
                        else
                        {
                            tbSetName.Text = String.Empty;
                            tbDetailSet.Text = String.Empty;
                        }

                      
                        select_setname();
                        setFormData(fvformInsert, FormViewMode.Insert, null);
                        break;

                    case "1":
                        TextBox tbRootSetName = (TextBox)fvformInsertRoot.FindControl("tbRootSetName");
                        TextBox tbDetailRootSet = (TextBox)fvformInsertRoot.FindControl("tbDetailRootSet");
                        DropDownList ddlSetName = (DropDownList)fvformInsertRoot.FindControl("ddlSetName");
                        DropDownList ddlRootSetStatus = (DropDownList)fvformInsertRoot.FindControl("ddlRootSetStatus");

                        _data_qa.qa_m0_setname_list = new qa_m0_setname_detail[1];
                        qa_m0_setname_detail _m0root = new qa_m0_setname_detail();
                        _m0root.cemp_idx = _emp_idx;
                        _m0root.set_name = tbRootSetName.Text;
                        _m0root.setroot_idx = int.Parse(ViewState["set_idx"].ToString());
                        _m0root.set_name_status = int.Parse(ddlRootSetStatus.SelectedValue);
                        _m0root.set_detail = tbDetailRootSet.Text;

                        _data_qa.qa_m0_setname_list[0] = _m0root;
                        _data_qa = callServicePostMasterQA(_urlQaSetNameList, _data_qa);
                        //-- check data duplicate in database
                        if (_data_qa.return_code == 101)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                        }
                        else
                        {
                            tbRootSetName.Text = String.Empty;
                            tbDetailRootSet.Text = String.Empty;
                        }

                        select_RootSetname();

                        break;

                    case "2":
                        TextBox tbSubSetName = (TextBox)fvformInsertSubSet.FindControl("tbSubSetName");
                        TextBox tbDetailSubSet = (TextBox)fvformInsertSubSet.FindControl("tbDetailSubSet");
                        DropDownList ddlSubSetStatus = (DropDownList)fvformInsertSubSet.FindControl("ddlSubSetStatus");

                        _data_qa.qa_m0_setname_list = new qa_m0_setname_detail[1];
                        qa_m0_setname_detail _m0Subset = new qa_m0_setname_detail();
                        _m0Subset.cemp_idx = _emp_idx;
                        _m0Subset.set_name = tbSubSetName.Text;
                        _m0Subset.setroot_idx = int.Parse(ViewState["set_idx"].ToString());
                        _m0Subset.set_name_status = int.Parse(ddlSubSetStatus.SelectedValue);
                        _m0Subset.set_detail = tbDetailSubSet.Text;

                        _data_qa.qa_m0_setname_list[0] = _m0Subset;
                        _data_qa = callServicePostMasterQA(_urlQaSetNameList, _data_qa);
                        //-- check data duplicate in database
                        if (_data_qa.return_code == 101)
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

                        }
                        else
                        {
                            tbDetailSubSet.Text = String.Empty;
                            tbSubSetName.Text = String.Empty;
                        }
                      
                        select_RootSubSet();
                        break;
                }
              
                break;

            case "cmdDeleteSet":

                string[] cmdArgDelete = cmdArg.Split(',');
                int set_idx = int.TryParse(cmdArgDelete[0].ToString(), out _default_int) ? int.Parse(cmdArgDelete[0].ToString()) : _default_int;
                int condition = int.TryParse(cmdArgDelete[1].ToString(), out _default_int) ? int.Parse(cmdArgDelete[1].ToString()) : _default_int;

                _data_qa.qa_m0_setname_list = new qa_m0_setname_detail[1];
                qa_m0_setname_detail _delset = new qa_m0_setname_detail();
                _delset.set_idx = set_idx;
                _data_qa.qa_m0_setname_list[0] = _delset;
                _data_qa = callServicePostMasterQA(_urlQaDelSetName, _data_qa);
                if (_data_qa.return_code == 0)
                {
                    //litDebug.Text = "success";
                }
                else
                {
                   // litDebug.Text = "not success";
                }

                    switch (condition)
                    {
                        // select set name level 1 (DELETE)
                        case 0:
                            select_setname();
                            break;
                       // select set name level 2 (DELETE)
                        case 1:
                            select_RootSetname();
                        break;
                       // select set name level 3 (DELETE)
                        case 2:
                            select_RootSubSet();
                        break;
                    }
                break;

            case "cmdViewSet":
                string[] cmdArgView = cmdArg.Split(',');
                ViewState["set_idx"] = int.TryParse(cmdArgView[0].ToString(), out _default_int) ? int.Parse(cmdArgView[0].ToString()) : _default_int;
                int conditionView = int.TryParse(cmdArgView[1].ToString(), out _default_int) ? int.Parse(cmdArgView[1].ToString()) : _default_int;

                switch (conditionView)
                {
                    case 0:
                        setActiveView("pageCreateRoot", int.Parse(ViewState["set_idx"].ToString()));
                        setFormData(fvformInsertRoot, FormViewMode.ReadOnly, null);
                        select_RootSetname();
                       
                       // litDebug.Text = ViewState["set_idx"].ToString();
                        break;

                    case 1:
                        setActiveView("pageRootSubSet", int.Parse(ViewState["set_idx"].ToString()));
                        setFormData(fvformInsertSubSet, FormViewMode.ReadOnly, null);
                        select_RootSubSet();
                        break;

                }

                
                break;

        }
    }
    #endregion event command

    #region class select
    protected void select_setname()
    {
        _data_qa.qa_m0_setname_list = new qa_m0_setname_detail[1];
        qa_m0_setname_detail _selectset = new qa_m0_setname_detail();
        _data_qa.qa_m0_setname_list[0] = _selectset;
        _data_qa = callServicePostMasterQA(_urlQaGetNameList, _data_qa);
        var _linqSet = from data_qa in _data_qa.qa_m0_setname_list
                       where data_qa.setroot_idx == (0)
                       select data_qa;
        setGridData(gvMaster, _linqSet.ToList());

    }

    protected void select_RootSetname()
    {
        _data_qa.qa_m0_setname_list = new qa_m0_setname_detail[1];
        qa_m0_setname_detail _selectset = new qa_m0_setname_detail();
        _data_qa.qa_m0_setname_list[0] = _selectset;
        _data_qa = callServicePostMasterQA(_urlQaGetNameList, _data_qa);
        var _linqRootSet = from data_qa in _data_qa.qa_m0_setname_list
                           where data_qa.setroot_idx == int.Parse(ViewState["set_idx"].ToString())
                           select data_qa;
        setGridData(gvMasterRoot, _linqRootSet.ToList());

    }

    protected void select_RootSubSet()
    {
        _data_qa.qa_m0_setname_list = new qa_m0_setname_detail[1];
        qa_m0_setname_detail _selectset = new qa_m0_setname_detail();
        _data_qa.qa_m0_setname_list[0] = _selectset;
        _data_qa = callServicePostMasterQA(_urlQaGetNameList, _data_qa);
        var _linqSubSet = from data_qa in _data_qa.qa_m0_setname_list
                       where data_qa.setroot_idx == int.Parse(ViewState["set_idx"].ToString())
                       select data_qa;
        setGridData(gvMasterSubSet, _linqSubSet.ToList());

    }
    #endregion

    #region reuse
    protected void initPage()
    {

        clearSession();
        clearViewState();

        setActiveView("pageGenaral", 0);
        setFormData(fvformInsert, FormViewMode.ReadOnly, null);
        select_setname();
    }

    #region dropdown list
    protected void getM0RootSetList()
    {
        setFormData(fvformInsertSubSet, FormViewMode.Insert, null);
        TextBox txtSubSetName = (TextBox)fvformInsertSubSet.FindControl("txtSubSetName");

        data_qa _dataqa = new data_qa();
        _dataqa.qa_m0_setname_list = new qa_m0_setname_detail[1];
        qa_m0_setname_detail _m0SubSet = new qa_m0_setname_detail();
        _dataqa.qa_m0_setname_list[0] = _m0SubSet;
        _dataqa = callServicePostMasterQA(_urlQaGetNameList, _dataqa);
        var _linqSet = (from data_qa in _dataqa.qa_m0_setname_list
                        where data_qa.set_idx == int.Parse(ViewState["set_idx"].ToString())
                        select new
                        {
                            data_qa.set_name
                        }).ToList();
        foreach (var item in _linqSet)
        {
            txtSubSetName.Text = item.set_name;

        }
    }

    protected void getM0SetNameList()
    {
        setFormData(fvformInsertRoot, FormViewMode.Insert, null);
        TextBox tbSetNameList = (TextBox)fvformInsertRoot.FindControl("tbSetNameList");
      
        data_qa _dataqa = new data_qa();
        _dataqa.qa_m0_setname_list = new qa_m0_setname_detail[1];
        qa_m0_setname_detail _m0SubSet = new qa_m0_setname_detail();
        _dataqa.qa_m0_setname_list[0] = _m0SubSet;
        _dataqa = callServicePostMasterQA(_urlQaGetNameList, _dataqa);
        var _linqSet = (from data_qa in _dataqa.qa_m0_setname_list
                        where data_qa.set_idx == int.Parse(ViewState["set_idx"].ToString())
                        select new
                        {
                            data_qa.set_name
                        }).ToList();
        foreach (var item in _linqSet)
        {
            tbSetNameList.Text = item.set_name;
           
        }

    }
    #endregion

    //#region label
    //protected void getM0RootNameSetList(Label label)
    //{

    //   // setLabelData(label, _linqSet.ToString());
    //}
    //#endregion

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setLabelData(Label lbName, string obj)
    {
        lbName.Text = obj;

    }
    #region PageIndexChanging

    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvMaster":
                gvMaster.PageIndex = e.NewPageIndex;
                gvMaster.DataBind();
                select_setname();
                break;

            case "gvMasterRoot":
                gvMasterRoot.PageIndex = e.NewPageIndex;
                gvMasterRoot.DataBind();
                select_RootSetname();
                break;

            case "gvMasterSubSet":
                gvMasterSubSet.PageIndex = e.NewPageIndex;
                gvMasterSubSet.DataBind();
                select_RootSubSet();
                break;
        }

    }

    #endregion

    #region gvRowDataBound

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvMaster":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbstatus_setname = (Label)e.Row.Cells[3].FindControl("lbstatus_setname");
                    Label status_online = (Label)e.Row.Cells[3].FindControl("status_online");
                    Label status_offline = (Label)e.Row.Cells[3].FindControl("status_offline");

                    ViewState["status_setname"] = lbstatus_setname.Text;

                    if (ViewState["status_setname"].ToString() == "0")
                    {
                        status_offline.Visible = true;
                    }
                    else if (ViewState["status_setname"].ToString() == "1")
                    {
                        status_online.Visible = true;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }



                break;

            case "gvMasterRoot":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbRootStatus = (Label)e.Row.Cells[4].FindControl("lbRootStatus");
                    Label status_online = (Label)e.Row.Cells[4].FindControl("status_online");
                    Label status_offline = (Label)e.Row.Cells[4].FindControl("status_offline");
                    TextBox tbSetRootIDX = (TextBox)e.Row.Cells[1].FindControl("tbSetRootIDX");

                    //   getM0RootNameSetList(lblRootLevelSet);
                    ViewState["ROOTIDX"] = tbSetRootIDX.Text;

                    ViewState["status_rootidx"] = lbRootStatus.Text;

                    if (ViewState["status_rootidx"].ToString() == "0")
                    {
                        status_offline.Visible = true;
                    }
                    else if (ViewState["status_rootidx"].ToString() == "1")
                    {
                        status_online.Visible = true;
                    }

               
                }
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }

                break;

            case "gvMasterSubSet":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbRootSubStatus = (Label)e.Row.Cells[4].FindControl("lbRootSubStatus");
                    Label status_online = (Label)e.Row.Cells[4].FindControl("status_online");
                    Label status_offline = (Label)e.Row.Cells[4].FindControl("status_offline");

                    ViewState["status_subset"] = lbRootSubStatus.Text;

                    if (ViewState["status_subset"].ToString() == "0")
                    {
                        status_offline.Visible = true;
                    }
                    else if (ViewState["status_subset"].ToString() == "1")
                    {
                        status_online.Visible = true;
                    }


                }
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }

                break;
        }

    }

    #endregion

    #region gvRowUpdating

    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvMaster":

                var _update_setidx = (TextBox)gvMaster.Rows[e.RowIndex].FindControl("_update_setidx");
                var _update_setname = (TextBox)gvMaster.Rows[e.RowIndex].FindControl("tbupdate_setname");
                var _update_set_detail = (TextBox)gvMaster.Rows[e.RowIndex].FindControl("tbupdate_set_detail");
                var _update_set_status = (DropDownList)gvMaster.Rows[e.RowIndex].FindControl("ddlupdate_set_status");

                gvMaster.EditIndex = -1;

                _data_qa.qa_m0_setname_list = new qa_m0_setname_detail[1];
                qa_m0_setname_detail _upset = new qa_m0_setname_detail();
                _upset.set_idx = int.Parse(_update_setidx.Text);
                _upset.set_name = _update_setname.Text;
                _upset.set_detail = _update_set_detail.Text;
                _upset.set_name_status = int.Parse(_update_set_status.SelectedValue);
                _data_qa.qa_m0_setname_list[0] = _upset;

                _data_qa = callServicePostMasterQA(_urlQaSetNameList, _data_qa);
                if (_data_qa.return_code == 102)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                }

                else
                {
                  
                }

                select_setname();
                SETFOCUS.Focus();
                break;

            case "gvMasterRoot":

                var _updateRootSetidx = (TextBox)gvMasterRoot.Rows[e.RowIndex].FindControl("_updateRootSetidx");
                var _updateRootSetName = (TextBox)gvMasterRoot.Rows[e.RowIndex].FindControl("tbupdateRootSetName");
                var _updateRootSetDetail = (TextBox)gvMasterRoot.Rows[e.RowIndex].FindControl("tbupdateRootSetDetail");
                var _updateRootSetLevel1 = (DropDownList)gvMasterRoot.Rows[e.RowIndex].FindControl("ddlRootSetLevel1");
                var _updateRootSetStatus = (DropDownList)gvMasterRoot.Rows[e.RowIndex].FindControl("ddlupdateRootSetStatus");

                gvMasterRoot.EditIndex = -1;

                _data_qa.qa_m0_setname_list = new qa_m0_setname_detail[1];
                qa_m0_setname_detail _upRootset = new qa_m0_setname_detail();
                _upRootset.set_idx = int.Parse(_updateRootSetidx.Text);
                _upRootset.set_name = _updateRootSetName.Text;
                _upRootset.set_detail = _updateRootSetDetail.Text;
                _upRootset.set_name_status = int.Parse(_updateRootSetStatus.SelectedValue);
                _data_qa.qa_m0_setname_list[0] = _upRootset;

                _data_qa = callServicePostMasterQA(_urlQaSetNameList, _data_qa);
                if (_data_qa.return_code == 102)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                }

                else
                {

                }
                select_RootSetname();
                SETFOCUS.Focus();
                break;

            case "gvMasterSubSet":

                var _updateSubSetidx = (TextBox)gvMasterSubSet.Rows[e.RowIndex].FindControl("_updateSubSetidx");
                var _updateSubSetName = (TextBox)gvMasterSubSet.Rows[e.RowIndex].FindControl("tbupdateSubSetName");
                var _updateSubSetDetail = (TextBox)gvMasterSubSet.Rows[e.RowIndex].FindControl("tbupdateSubSetDetail");
                var _updateSubSetStatus = (DropDownList)gvMasterSubSet.Rows[e.RowIndex].FindControl("ddlupdateSubSetStatus");

                gvMasterSubSet.EditIndex = -1;

                _data_qa.qa_m0_setname_list = new qa_m0_setname_detail[1];
                qa_m0_setname_detail _upSubset = new qa_m0_setname_detail();
                _upSubset.set_idx = int.Parse(_updateSubSetidx.Text);
                _upSubset.set_name = _updateSubSetName.Text;
                _upSubset.set_detail = _updateSubSetDetail.Text;
                _upSubset.set_name_status = int.Parse(_updateSubSetStatus.SelectedValue);
                _data_qa.qa_m0_setname_list[0] = _upSubset;

                _data_qa = callServicePostMasterQA(_urlQaSetNameList, _data_qa);
                if (_data_qa.return_code == 102)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถแก้ไขข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                }
                else
                {

                }
                select_RootSubSet();
                SETFOCUS.Focus();
                break;
        }
        

    }

    #endregion

    #region gvRowCancelingEdit

    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvMaster":

                gvMaster.EditIndex = -1;
                select_setname();
                SETFOCUS.Focus();
                break;

            case "gvMasterRoot":
                gvMasterRoot.EditIndex = -1;
                select_RootSetname();
                SETFOCUS.Focus();
                break;

            case "gvMasterSubSet":
                gvMasterSubSet.EditIndex = -1;
                select_RootSubSet();
                SETFOCUS.Focus();
                break;

        }
    }

    #endregion

    #region GvRowEditing

    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvMaster":
                gvMaster.EditIndex = e.NewEditIndex;
                select_setname();
                break;

            case "gvMasterRoot":
                gvMasterRoot.EditIndex = e.NewEditIndex;
                select_RootSetname();
                break;

            case "gvMasterSubSet":
                gvMasterSubSet.EditIndex = e.NewEditIndex;
                select_RootSubSet();
                break;
        }
    }

    #endregion

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }


    protected void setActiveView(string activeTab, int uidx)
    {
        mvMaster.SetActiveView((View)mvMaster.FindControl(activeTab));

        switch (activeTab)
        {
            case "pageGenaral":

                // fvEmpDetail
                // setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);

                break;

            case "pageCreateRoot":
                getM0SetNameList();
                break;

            case "pageRootSubSet":

                getM0RootSetList();

                break;


            //case "1":
            //    setFormData(fvformInsertRoot, FormViewMode.Insert, null);

            //    getM0SetNameList();
            //    break;
            //case "2":

            //    setFormData(fvformInsertSubSet, FormViewMode.Insert, null);
               

            //    break;

        }
    }




    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected data_qa callServicePostMasterQA(string _cmdUrl, data_qa _data_qa)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_qa);
        // litDebug.Text = _localJson;

        // call services
        // _localJson = _funcTool.callServicePost(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);

        return _data_qa;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    #endregion

}