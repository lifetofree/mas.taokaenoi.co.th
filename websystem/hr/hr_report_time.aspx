﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_report_time.aspx.cs" Inherits="websystem_hr_report_time" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadImage").MultiFile();
            }
        })
    </script>

    <%----------- Menu Tab Start---------------%>
    <div id="BoxTabMenuIndex" runat="server">
        <div class="form-group">
            <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Menu1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="sub-navbar">
                            <a class="navbar-brand " href="#"><b>Menu</b></a>
                        </div>
                    </div>

                    <div class="collapse navbar-collapse" id="Menu1">
                        <%--<asp:LinkButton ID="lbindex" CssClass="btn_menulist" runat="server" CommandName="btnIndex" OnCommand="btnCommand" CommandArgument="1">รายการทั่วไป</asp:LinkButton>
                        <asp:LinkButton ID="lbadd" CssClass="btn_menulist" runat="server" CommandName="btnadd" OnCommand="btnCommand" CommandArgument="2">เพิ่มพนักงาน</asp:LinkButton>
                        <asp:LinkButton ID="lbsetapprove" CssClass="btn_menulist" runat="server" CommandName="btnsetapprove" OnCommand="btnCommand" CommandArgument="3">จัดการข้อมูลผู้อนุมัติ</asp:LinkButton>--%>
                        <asp:LinkButton ID="lblreport" CssClass="btn_menulist" runat="server" CommandName="btnreport" OnCommand="btnCommand" CommandArgument="4">รายงาน</asp:LinkButton>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </nav>
        </div>
    </div>
    <%--<asp:Label ID="lbl_txt" runat="server"></asp:Label>--%>
    <%-------------- Menu Tab End--------------%>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewReport" runat="server">

            <asp:Panel ID="box_index" runat="server" Visible="true">
                <div class="col-lg-12">
                    <asp:Label ID="adf" runat="server"></asp:Label>
                    <%-------------- BoxSearch Start--------------%>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ค้นหาข้อมูล Report</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" id="Div1" runat="server">
                                <asp:FormView ID="Fv_Search_Emp_Report" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                    <InsertItemTemplate>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label38" runat="server" Text="Employee Code" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label39" runat="server" Text="รหัสพนักงาน" /></b></small>
                                                </h2>
                                            </label>

                                            <div class="col-sm-2">
                                                <asp:TextBox ID="txtempcode_s" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                            </div>

                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label121" runat="server" Text="Employee Name" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label122" runat="server" Text="ชื่อพนักงาน" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="txtempname_s" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                            </div>
                                        </div>

                                        <asp:Panel ID="BoxSearch_Date" runat="server" Visible="true">
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">
                                                    <h2 class="panel-title">
                                                        <asp:Label ID="Label269" runat="server" Text="Start Date" /><br />
                                                        <small><b>
                                                            <asp:Label ID="Label270" runat="server" Text="วันที่" /></b></small>
                                                    </h2>
                                                </label>
                                                <div class="col-sm-2">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="txtdatestart" runat="server" CssClass="form-control from-date-datepicker" AutoComplete="off" MaxLengh="100%" ValidationGroup="Search"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>

                                                        <asp:RequiredFieldValidator ID="ReadquiredFieldValidator1" ValidationGroup="Search" runat="server" Display="None"
                                                            ControlToValidate="txtdatestart" Font-Size="11"
                                                            ErrorMessage="วันที่เริ่ม ...." />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouootExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReadquiredFieldValidator1" Width="160" />
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <asp:Label ID="Label223" runat="server" ForeColor="Red" Text="***" />
                                                </div>

                                                <label class="col-sm-2 control-label">
                                                    <h2 class="panel-title">
                                                        <asp:Label ID="Label3" runat="server" Text="Location" /><br />
                                                        <small><b>
                                                            <asp:Label ID="Label4" runat="server" Text="ประจำสำนักงาน" /></b></small>
                                                    </h2>
                                                </label>

                                                <div class="col-sm-2">
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                                        <ContentTemplate>
                                                            <asp:DropDownList ID="ddllocation" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="0">เลือกสำนักงาน....</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="ddllocation" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </div>

                                            </div>
                                        </asp:Panel>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label101" runat="server" Text="Organization" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label102" runat="server" Text="บริษัท" /></b></small>
                                                </h2>
                                            </label>

                                            <div class="col-sm-2">
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlorg_rp" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="ddlorg_rp" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>

                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label103" runat="server" Text="Department" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label104" runat="server" Text="ฝ่าย" /></b></small>
                                                </h2>
                                            </label>

                                            <div class="col-sm-2">
                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddldep_rp" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">เลือกฝ่าย....</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="ddldep_rp" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label105" runat="server" Text="Section" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label106" runat="server" Text="หน่วยงาน" /></b></small>
                                                </h2>
                                            </label>

                                            <div class="col-sm-2">
                                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlsec_rp" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">เลือกหน่วยงาน....</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="ddlsec_rp" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>

                                            <label class="col-sm-2 control-label col-sm-offset-1">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label220" runat="server" Text="EmpType" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label225" runat="server" Text="ประเภทพนักงาน" /></b></small>
                                                </h2>
                                            </label>

                                            <div class="col-sm-2">
                                                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                    <ContentTemplate>
                                                        <asp:DropDownList ID="ddlemptype_s" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0">ประเภทพนักงาน....</asp:ListItem>
                                                            <asp:ListItem Value="1">รายวัน</asp:ListItem>
                                                            <asp:ListItem Value="2">รายเดือน</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="ddlemptype_s" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label1" runat="server" Text="Shifttime" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label2" runat="server" Text="กะทำงาน" /></b></small>
                                                </h2>
                                            </label>

                                            <div class="col-sm-2">
                                                <asp:DropDownList ID="ddlparttime" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Req32uiredF3ieldValidator1" ValidationGroup="Search" runat="server" Display="None"
                                                    ControlToValidate="ddlparttime" Font-Size="11"
                                                    ErrorMessage="กะทำงาน ...."
                                                    ValidationExpression="กะทำงาน ...." InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req32uiredF3ieldValidator1" Width="160" />
                                            </div>
                                            <div class="col-sm-1">
                                                <asp:Label ID="Label5" runat="server" ForeColor="Red" Text="***" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2">
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:LinkButton ID="btnsearch_report" class="btn btn-success" ValidationGroup="Search" runat="server" Text="Search" data-original-title="search" data-toggle="tooltip" CommandName="btn_search_report" OnCommand="btnCommand" Visible="true" />
                                                        <asp:Button ID="btnclearsearch_index" class="btn btn-primary" runat="server" Text="Clear" data-original-title="search" data-toggle="tooltip" CommandName="Cmdsearch_clear" OnCommand="btnCommand" />
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnsearch_report" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>

                                    </InsertItemTemplate>
                                </asp:FormView>
                            </div>
                        </div>
                    </div>
                    <%-------------- BoxSearch End--------------%>

                    <div class="panel-heading">
                        <h5 class="panel-title"><font color="blue"><strong><asp:Label ID="lblsum_list" runat="server" /></strong></font></h5>
                    </div>

                    <asp:GridView ID="GvEmployee_Report"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="20"
                        DataKeyNames="emp_idx"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        OnRowDataBound="Master_RowDataBound"
                        ShowFooter="true">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="สำนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lblLocName" runat="server" Text='<%# Eval("LocName") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <div style="text-align: right;">
                                        <asp:Label ID="lit_sum" runat="server" Text="รวมทั้งหมด :"></asp:Label>
                                    </div>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="พนักงานทั้งหมด(คน)" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                <ItemTemplate>
                                    <small>
                                        <center><asp:Label ID="lbltime_all_sum" runat="server" Text='<%# Eval("time_all_sum") %>'></asp:Label><asp:Label runat="server" Text=""/></center>
                                    </small>
                                </ItemTemplate>

                                <FooterTemplate>
                                    <div style="text-align: right; background-color: chartreuse;">
                                        <asp:Label ID="lit_time_all_sum" runat="server" Font-Bold="true"></asp:Label>

                                    </div>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="พนักงานสแกนนิ้วเข้าทั้งหมด(คน)" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                <ItemTemplate>
                                    <small>
                                        <center><asp:Label ID="lbltime_in" runat="server" Text='<%# Eval("time_in") %>'></asp:Label><asp:Label runat="server" Text=""/></center>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <div style="text-align: right; background-color: chartreuse;">
                                        <asp:Label ID="lit_time_in" runat="server" Font-Bold="true"></asp:Label>

                                    </div>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="พนักงานสแกนนิ้วออกทั้งหมด(คน)" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                <ItemTemplate>
                                    <small>
                                        <center><asp:Label ID="lbltime_out" runat="server" Text='<%# Eval("time_out") %>'></asp:Label><asp:Label runat="server" Text=""/></center>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <div style="text-align: right; background-color: chartreuse;">
                                        <asp:Label ID="lit_time_out" runat="server" Font-Bold="true"></asp:Label>

                                    </div>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="พนักงานเข้างานไม่ตรงเวลา(คน)" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                <ItemTemplate>
                                    <small>
                                        <center><asp:Label ID="lbltime_in_rate" runat="server" Text='<%# Eval("time_in_rate") %>'></asp:Label><asp:Label runat="server" Text=""/></center>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <div style="text-align: right; background-color: chartreuse;">
                                        <asp:Label ID="lit_time_in_rate" runat="server" Font-Bold="true"></asp:Label>

                                    </div>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="พนักงานลาทั้งหมด(คน)" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                <ItemTemplate>
                                    <small>
                                        <center><asp:Label ID="lbltime_leave" runat="server" Text='<%# Eval("time_leave") %>'></asp:Label><asp:Label runat="server" Text=""/></center>
                                    </small>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <div style="text-align: right; background-color: chartreuse;">
                                        <asp:Label ID="lit_time_leave" runat="server" Font-Bold="true"></asp:Label>

                                    </div>
                                </FooterTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จัดการ" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%">
                                <ItemTemplate>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="btnview_detail" CssClass="btn btn-primary" runat="server" CommandName="btnview_detail" OnCommand="btnCommand" data-toggle="tooltip" title="รายละเอียด" CommandArgument='<%# Eval("LocName") %>'><i class="fa fa-edit"></i></asp:LinkButton>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnview_detail" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </ItemTemplate>

                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <%--<asp:TemplateField HeaderText="กะทำงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="3%">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="lblempshift_name" runat="server" Text='<%# Eval("empshift_name") %>'></asp:Label>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        </Columns>
                    </asp:GridView>

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Panel ID="boxchart" runat="server" Visible="false">
                                <div class="panel panel-info" id="grant" runat="server">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>&nbsp; ข้อมูลรายงาน</strong></h3>
                                    </div>
                                    <div class="panel-body">
                                        <asp:Literal ID="litReportChart" runat="server" />
                                    </div>
                                </div>
                            </asp:Panel>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </asp:Panel>

            <asp:Panel ID="box_detail" runat="server" Visible="false">
                <!-- Back To Detail To Day -->
                <div class="form-group">
                    <label class="col-sm-12">
                        <div class="form-group">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:LinkButton ID="btnBackToDetailOTDay" CssClass="btn btn-success" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                        CommandName="btnreport_back" OnCommand="btnCommand" CommandArgument="4">< ย้อนกลับ</asp:LinkButton>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnBackToDetailOTDay" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </label>
                </div>
                <!-- Back To Detail To Day -->

                <div class="form-group">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <asp:UpdatePanel ID="UpdatePanel21" runat="server">
                                <ContentTemplate>
                                    <asp:Button ID="btnexport1" CssClass="btn btn-primary" runat="server" data-toggle="tooltip" Text="Export Excel (พนักงานทั้งหมด)" CommandName="btnExport" OnCommand="btnCommand" CommandArgument="1" title="Export"></asp:Button>
                                    <asp:Button ID="btnexport2" CssClass="btn btn-primary" runat="server" data-toggle="tooltip" Text="Export Excel (สแกนนิ้วเข้าทั้งหมด)" CommandName="btnExport" OnCommand="btnCommand" CommandArgument="2" title="Export"></asp:Button>
                                    <asp:Button ID="btnexport3" CssClass="btn btn-primary" runat="server" data-toggle="tooltip" Text="Export Excel (สแกนนิ้วออกทั้งหมด)" CommandName="btnExport" OnCommand="btnCommand" CommandArgument="3" title="Export"></asp:Button>
                                    <asp:Button ID="btnexport4" CssClass="btn btn-primary" runat="server" data-toggle="tooltip" Text="Export Excel (เข้างานไม่ตรงเวลา)" CommandName="btnExport" OnCommand="btnCommand" CommandArgument="4" title="Export"></asp:Button>
                                    <asp:Button ID="btnexport5" CssClass="btn btn-primary" runat="server" data-toggle="tooltip" Text="Export Excel (พนักงานลาทั้งหมด)" CommandName="btnExport" OnCommand="btnCommand" CommandArgument="5" title="Export"></asp:Button>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnexport1" />
                                    <asp:PostBackTrigger ControlID="btnexport2" />
                                    <asp:PostBackTrigger ControlID="btnexport3" />
                                    <asp:PostBackTrigger ControlID="btnexport4" />
                                    <asp:PostBackTrigger ControlID="btnexport5" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="form-group" runat="server">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; พนักงานทั้งหมด</strong></h3>
                                </div>
                                <div class="panel-heading">
                                    <h3 class="panel-title"><font color="blue"><strong><asp:Label ID="lbltime_all_sum" runat="server" /></strong></font></h3>
                                </div>
                                <asp:GridView ID="GvEmployee_time_all_sum"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                    HeaderStyle-CssClass="table_headCenter"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="True"
                                    PageSize="10"
                                    DataKeyNames="emp_idx"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                    </EmptyDataTemplate>

                                    <Columns>

                                        <asp:TemplateField HeaderText="พนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblemp_code" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbldep" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblsec" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblpos" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="form-group" runat="server">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; สแกนนิ้วเข้าทั้งหมด</strong></h3>
                                </div>
                                <div class="panel-heading">
                                    <h3 class="panel-title"><font color="blue"><strong><asp:Label ID="lbltime_in" runat="server" /></strong></font></h3>
                                </div>
                                <asp:GridView ID="GvEmployee_time_in"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                    HeaderStyle-CssClass="table_headCenter"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="True"
                                    PageSize="10"
                                    DataKeyNames="emp_idx"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                    </EmptyDataTemplate>

                                    <Columns>

                                        <asp:TemplateField HeaderText="พนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblemp_code" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbldep" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblsec" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblpos" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="form-group" runat="server">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; สแกนนิ้วออกทั้งหมด</strong></h3>
                                </div>
                                <div class="panel-heading">
                                    <h3 class="panel-title"><font color="blue"><strong><asp:Label ID="lbltime_out" runat="server" /></strong></font></h3>
                                </div>
                                <asp:GridView ID="GvEmployee_time_out"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                    HeaderStyle-CssClass="table_headCenter"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="True"
                                    PageSize="10"
                                    DataKeyNames="emp_idx"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                    </EmptyDataTemplate>

                                    <Columns>

                                        <asp:TemplateField HeaderText="พนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblemp_code" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbldep" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblsec" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblpos" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="form-group" runat="server">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; เข้างานไม่ตรงเวลา</strong></h3>
                                </div>
                                <div class="panel-heading">
                                    <h3 class="panel-title"><font color="blue"><strong><asp:Label ID="lbltime_in_rate" runat="server" /></strong></font></h3>
                                </div>
                                <asp:GridView ID="GvEmployee_time_in_rate"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                    HeaderStyle-CssClass="table_headCenter"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="True"
                                    PageSize="10"
                                    DataKeyNames="emp_idx"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                    </EmptyDataTemplate>

                                    <Columns>

                                        <asp:TemplateField HeaderText="พนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblemp_code" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbldep" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblsec" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblpos" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-12">
                        <div class="form-group" runat="server">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; พนักงานลาทั้งหมด</strong></h3>
                                </div>
                                <div class="panel-heading">
                                    <h3 class="panel-title"><font color="blue"><strong><asp:Label ID="lbltime_leave" runat="server" /></strong></font></h3>
                                </div>
                                <asp:GridView ID="GvEmployee_time_leave"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                    HeaderStyle-CssClass="table_headCenter"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="True"
                                    PageSize="10"
                                    DataKeyNames="emp_idx"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                    </EmptyDataTemplate>

                                    <Columns>

                                        <asp:TemplateField HeaderText="พนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblemp_code" runat="server" Text='<%# Eval("emp_code") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชื่อ-นามสกุล" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblname" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbldep" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblsec" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblpos" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>

            </asp:Panel>
        </asp:View>



    </asp:MultiView>

</asp:Content>

