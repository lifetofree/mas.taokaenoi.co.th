﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="tpm_form_result.aspx.cs" Inherits="websystem_hr_tpm_form_result" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden; background-color: #e6ccff;" id="bs-example-navbar-collapse-1">
                <%--#ffcccc--%>
                <ul class="nav navbar-nav navbar-left" runat="server">
                    <li id="_divMenuLiToViewIndex" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivIndex" runat="server"
                            CommandName="_divMenuBtnToDivIndex"
                            OnCommand="btnCommand" Text="รายการประเมิน" />
                    </li>


                    <li id="_divMenuLiToViewAdd" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivAdd" runat="server"
                            CommandName="_divMenuBtnToDivAdd"
                            OnCommand="btnCommand" Text="ทำการประเมิน" />
                    </li>

                    <li id="_divMenuLiToViewApprove" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivApprove" runat="server"
                            CommandName="_divMenuBtnToDivApprove"
                            OnCommand="btnCommand" Text="รายการรออนุมัติ">
                            <asp:Label ID="nav_approve" Font-Bold="true" runat="server"></asp:Label>
                        </asp:LinkButton>

                    </li>

                    <li id="_divMenuLiToViewReport" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivReport" runat="server"
                            CommandName="_divMenuBtnToDivReport"
                            OnCommand="btnCommand" Text="รายงาน">
                        </asp:LinkButton>

                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right" runat="server">

                    <li id="_divMenuLiToViewManual" runat="server">
                        <asp:HyperLink ID="_divMenuBtnToDivManual" runat="server"
                            NavigateUrl="https://docs.google.com/document/d/18VKWH8jnN4i-KJLloZ7YLKVBNzrPanSXkTDIAdcBFMY/edit?usp=sharing" Target="_blank"
                            CommandName="_divMenuBtnToDivManual"
                            OnCommand="btnCommand" Text="คู่มือการใช้งาน" />
                    </li>

                    <li id="_divMenuLiToViewFlow" runat="server">
                        <asp:HyperLink ID="_divMenuBtnToDivFlow" runat="server"
                            NavigateUrl="https://drive.google.com/file/d/1vFkON0MNMnm-cPJbNRdjP-qmq-SKOpxq/view?usp=sharing" Target="_blank"
                            CommandName="_divMenuBtnToDivFlow"
                            OnCommand="btnCommand" Text="Flow Chart" />
                    </li>
                </ul>
            </div>
        </nav>
    </div>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">

            <div class="col-lg-12">
                <asp:Image ID="imghead" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/tpm-formresult/Cover_Head_ฟอร์มประเมิน_GIF_Test-03_0.gif" Style="height: 100%; width: 100%;" />
            </div>
            <br />
            <div class="col-lg-12">

                <div class="alert-message alert-message-success">
                    <blockquote class="danger" style="font-size: small; background-color: #A7DB42; text-align: left">
                        <h5><b>
                            <asp:Label ID="lblheadbanner" runat="server">รายการประเมิน</asp:Label></b></h5>
                    </blockquote>
                </div>


                <asp:GridView ID="GvIndex" runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                    HeaderStyle-CssClass="info small"
                    HeaderStyle-Height="40px"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowDataBound="Master_RowDataBound"
                     OnPageIndexChanging="Master_PageIndexChanging"
                    DataKeyNames="u0_docidx">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="ข้อมูลผู้ประเมิน" ItemStyle-Width="45%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:Label ID="lblu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                <asp:Label ID="lblunidx" runat="server" Visible="false" Text='<%# Eval("unidx") %>'></asp:Label>
                                <asp:Label ID="lblacidx" runat="server" Visible="false" Text='<%# Eval("acidx") %>'></asp:Label>
                                <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                <small>
                                    <strong>
                                        <asp:Label ID="Labesl15" runat="server">องค์กร: </asp:Label></strong>
                                    <asp:Literal ID="litorg_idx" Visible="false" runat="server" Text='<%# Eval("org_idx") %>' />
                                    <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("org_name_th") %>' />
                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labeld24" runat="server">ฝ่าย: </asp:Label></strong>
                                    <asp:Literal ID="litrdept_idx" Visible="false" runat="server" Text='<%# Eval("rdept_idx") %>' />
                                    <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label25" runat="server">แผนก: </asp:Label></strong>
                                    <asp:Literal ID="litrsec_idx" Visible="false" runat="server" Text='<%# Eval("rsec_idx") %>' />
                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                    </p>
                                                           <strong>
                                                               <asp:Label ID="Label19" runat="server">ตำแหน่ง: </asp:Label></strong>
                                    <asp:Literal ID="litrpos_idx" Visible="false" runat="server" Text='<%# Eval("rpos_idx") %>' />
                                    <asp:Literal ID="Literal9" runat="server" Text='<%# Eval("pos_name_th") %>' />
                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label26" runat="server">ชื่อ-สกุล: </asp:Label></strong>
                                    <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                    <asp:Literal ID="litcemp_idx" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                    </p>
                                </small>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="คะแนนประเมินตนเอง" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <strong>
                                        <asp:Label ID="Labtel15" runat="server">Core Value : </asp:Label></strong>
                                    <asp:Literal ID="litsumminecore" runat="server" Text='<%# Eval("sum_mine_core_value") %>' />
                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labefl24" runat="server">Competencies : </asp:Label></strong>
                                    <asp:Literal ID="litsumminecom" runat="server" Text='<%# Eval("sum_mine_competency") %>' />
                                    </p>
                                  
                                </small>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="คะแนนผู้มีสิทธิ์ประเมิน" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <strong>
                                        <asp:Label ID="Label15" runat="server">Core Value : </asp:Label></strong>
                                    <asp:Literal ID="litsumheadcore" runat="server" Text='<%# Eval("sum_head_core_value") %>' />
                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label24" runat="server">Competencies : </asp:Label></strong>
                                    <asp:Literal ID="litsumheadcom" runat="server" Text='<%# Eval("sum_head_competency") %>' />
                                    </p>
                                  
                                </small>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                            <ItemTemplate>
                                <b>
                                    <asp:Label ID="lblStatusDoc" runat="server" Text='<%# Eval("StatusDoc") %>' />
                                </b>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnviewdetail" CssClass="btn btn-warning btn-sm" runat="server" OnCommand="btnCommand" ToolTip="View" CommandName="CmdDetail" CommandArgument='<%#  Eval("u0_docidx") + ";" + Eval("cemp_idx") + ";" +"1"+  ";" +  Eval("unidx")+  ";" +  Eval("acidx")+  ";" +  Eval("staidx") + ";" + Eval("form_name") + ";" + Eval("sum_head_core_value") + ";" + Eval("sum_head_competency") + ";" + Eval("u0_typeidx") + ";" + "1"%>'><i class="	fa fa-book"></i></asp:LinkButton>

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </div>




        </asp:View>

        <asp:View ID="ViewInsert" runat="server">
            <div class="col-lg-12">

                <div class="alert-message alert-message-success">
                    <blockquote class="danger" style="font-size: small; background-color: lightsalmon; text-align: left">
                        <h5 style="text-align: center"><b>
                            <asp:Label ID="lblHeadForm" runat="server"></asp:Label></b></h5>
                    </blockquote>
                </div>

                <div class="alert-message alert-message-success">
                    <blockquote class="danger" style="font-size: small; background-color: gainsboro; text-align: left">
                        <h5><b>ส่วนที่ 1: ข้อมูลทั่วไปเกี่ยวกับพนักงาน</b></h5>
                    </blockquote>
                </div>


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>


                    <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label38" runat="server" Text="รหัสผู้ทำรายการ" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label39" runat="server" Text="Employee ID" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label121" runat="server" Text="ชื่อ - นามสกุล" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label122" runat="server" Text="Name - Lastname" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label2" runat="server" Text="บริษัท" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label20" runat="server" Text="Organization" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label21" runat="server" Text="ฝ่าย" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label22" runat="server" Text="Division" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label23" runat="server" Text="แผนก / ส่วน" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label27" runat="server" Text="Department/Section" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label28" runat="server" Text="ตำแหน่ง" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label29" runat="server" Text="Position" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label30" runat="server" Text="วันที่เริ่มงาน" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label31" runat="server" Text="Start date" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtstartdate" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label32" runat="server" Text="อีเมล์" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label33" runat="server" Text="E-mail" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                </div>

                <div class="alert-message alert-message-success">
                    <blockquote class="danger" style="font-size: small; background-color: gainsboro; text-align: left">
                        <h5><b>ส่วนที่ 2: การประเมินผลการปฏิบัติงานและการให้ข้อมูลป้อนกลับ</b></h5>
                    </blockquote>

                    <h5 style="font-size: small;"><b>คำชี้แจง: ในการประเมินผลปลายปี เราจะใช้เกณฑ์คะแนนประเมินพฤติกรรม 1 - 5 โดยแต่ละคะแนนจะมีความหมายและแนวทางของเกณฑ์ประเมินพฤติกรรมเพื่อการพิจารณาตามตารางด้านล่างนี้</b></h5>

                </div>


                <asp:GridView ID="Gvmaster_point" runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-lg-12"
                    HeaderStyle-CssClass="danger"
                    HeaderStyle-Height="40px"
                    AllowPaging="false"
                    OnRowDataBound="Master_RowDataBound"
                    DataKeyNames="m0_point">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <b>
                                    <asp:Label ID="lblScore" runat="server" Text='<%# Eval("point_name") %>'></asp:Label>
                                </b>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="30%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <b>
                                    <p>
                                        <asp:Label ID="lblRatth" runat="server" Text='<%# Eval("rat_nameth") %>'></asp:Label>
                                    </p>
                                    <p>
                                        <asp:Label ID="lblRaten" runat="server" Text='<%# Eval("rat_nameen") %>'></asp:Label>
                                    </p>

                                </b>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="60%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left">
                            <ItemTemplate>
                                <p>
                                    <asp:Label ID="lblDefinitionth" runat="server" Text='<%# Eval("definition_th") %>'></asp:Label>
                                </p>
                                <p>
                                    <asp:Label ID="lblDefinitionen" runat="server" Text='<%# Eval("definition_en") %>'></asp:Label>
                                </p>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>



                <asp:GridView ID="GvCoreValue" runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered  table-responsive col-lg-12"
                    HeaderStyle-CssClass="danger"
                    HeaderStyle-Height="40px"
                    AllowPaging="false"
                    DataKeyNames="m1_coreidx"
                    OnRowDataBound="Master_RowDataBound">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <b>
                                    <asp:Label ID="lblCorevalue" runat="server" Text="Core Values"></asp:Label>
                                    <asp:Label ID="lblm1_coreidx" Visible="false" runat="server" Text='<%# Eval("m1_coreidx") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>

                                <%--<b><%# (Container.DataItemIndex +1) %></b>--%>
                                <b>
                                    <asp:Label ID="lblno" runat="server" Text='<%# (Container.DataItemIndex +1) %>'></asp:Label>
                                </b>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <b>
                                    <asp:Label ID="lblcore_name" runat="server" Text='<%# Eval("core_name") %>'></asp:Label>

                                    <br />
                                    <asp:Label ID="lbltype_core" runat="server" Text='<%# Eval("type_core") %>'></asp:Label>
                                </b>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <asp:GridView ID="GvSub_CoreValue" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-responsive col-lg-12"
                                    HeaderStyle-CssClass="small"
                                    GridLines="None"
                                    ShowHeader="false"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="false"
                                    DataKeyNames="m2_coreidx">


                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField ItemStyle-CssClass="text-left" ItemStyle-Width="100%" Visible="true" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <asp:Label ID="lblm2_coreidx" Visible="false" runat="server" Text='<%# Eval("m2_coreidx") %>' />

                                                <small>
                                                    <p>
                                                        <asp:Label ID="lblcore_nameen" runat="server" Text='<%# Eval("core_nameen") %>'></asp:Label>
                                                    </p>
                                                    <p>
                                                        <asp:Label ID="lblcore_nameth" runat="server" Text='<%# Eval("core_nameth") %>'></asp:Label>
                                                    </p>
                                                </small>

                                            </ItemTemplate>

                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <asp:UpdatePanel ID="update_insert" runat="server">
                                    <ContentTemplate>
                                        <asp:Label ID="lblcore_reason" runat="server"></asp:Label>
                                        <asp:RadioButtonList ID="rdochoice_mine" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                        </asp:RadioButtonList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                            ValidationGroup="SaveAdd" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                            Display="None" SetFocusOnError="true" ControlToValidate="rdochoice_mine" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCaslloutExtenxder7" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160"
                                            PopupPosition="BottomLeft" />



                                        <asp:Panel ID="panel_add_comment_core" runat="server" Visible="false">
                                            <%--   <hr />--%>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <asp:Label ID="Ladbeel7" CssClass="control-label" runat="server" Text="กรุณาระบุเหตุผล : " />
                                                </div>
                                            </div>
                                            <div class="form-group">

                                                <div class="col-sm-12">
                                                    <asp:TextBox ID="txtcore_insert" TextMode="MultiLine" Rows="5" Columns="3" CssClass="form-control" runat="server"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtcore_insert" Font-Size="11"
                                                        ErrorMessage="กรุณาระบุเหตุผล"
                                                        ValidationExpression="กรุณาระบุเหตุผล"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatoarCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                        ValidationGroup="SaveAdd" Display="None"
                                                        ErrorMessage="กรุณาระบุเหตุผลอย่างน้อย 50 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtcore_insert"
                                                        ValidationExpression="^[\s\S]{50,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                                </div>
                                            </div>

                                        </asp:Panel>
                                    </ContentTemplate>

                                </asp:UpdatePanel>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>


                <asp:GridView ID="GvCompetencies" runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered  table-responsive col-lg-12"
                    HeaderStyle-CssClass="danger"
                    HeaderStyle-Height="40px"
                    AllowPaging="false"
                    DataKeyNames="m1_typeidx"
                    OnRowDataBound="Master_RowDataBound">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="ปัจจัยประเมิน Competency Factor" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <b>
                                    <asp:Label ID="lblCompetencies" runat="server" Text="Competencies"></asp:Label>
                                    <asp:Label ID="lblu0_typeidx" Visible="false" runat="server" Text='<%# Eval("u0_typeidx") %>'></asp:Label></b>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ปัจจัยประเมิน Competency Factor" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>

                                <b>
                                    <asp:Label ID="lblno" runat="server"></asp:Label></b>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ปัจจัยประเมิน Competency Factor" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <b>
                                    <asp:Label ID="lblm1_typeidx" Visible="false" runat="server" Text='<%# Eval("m1_typeidx") %>'></asp:Label>
                                    <asp:Label ID="lblm1_type_name" runat="server" Text='<%# Eval("m1_type_name") %>'></asp:Label>
                                    <asp:Label ID="lblform_name" runat="server" Visible="false" Text='<%# Eval("form_name") %>'></asp:Label></b>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="พฤติกรรมการทำงานที่บริษัทคาดหวัง Behavioral Indicators" ItemStyle-Width="35%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="smaller">
                            <ItemTemplate>
                                <asp:GridView ID="GvSub_Competencies" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-responsive col-lg-12"
                                    HeaderStyle-CssClass="small"
                                    GridLines="None"
                                    ShowHeader="false"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="false"
                                    DataKeyNames="m2_typeidx">


                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField ItemStyle-CssClass="text-left" ItemStyle-Width="100%" Visible="true" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <asp:Label ID="lblm2_coreidx" Visible="false" runat="server" Text='<%# Eval("m2_typeidx") %>' />
                                                <small>
                                                    <asp:Label ID="lblm2_type_name" runat="server" Text='<%# Eval("m2_type_name") %>'></asp:Label>
                                                </small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ประเมินตนเอง (1-5) Self Assessment" ItemStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>

                                <asp:RadioButtonList ID="rdochoicecom_mine" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                    ValidationGroup="SaveAdd" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                    Display="None" SetFocusOnError="true" ControlToValidate="rdochoicecom_mine" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenvxder7" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160"
                                    PopupPosition="BottomLeft" />


                                <asp:Panel ID="panel_add_comment_competencies" runat="server" Visible="false">
                                    <%-- <hr />--%>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:Label ID="Lacdbel7" CssClass="control-label" runat="server" Text="กรุณาระบุเหตุผล : " />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtcom_insert" TextMode="MultiLine" Rows="5" Columns="3" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtcom_insert" Font-Size="11"
                                                ErrorMessage="กรุณาระบุเหตุผล"
                                                ValidationExpression="กรุณาระบุเหตุผล"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtegnder3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="กรุณาระบุเหตุผลอย่างน้อย 50 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtcom_insert"
                                                ValidationExpression="^[\s\S]{50,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                        </div>
                                    </div>

                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>


                <h6 style="color: #0070c0;">Remark: Superior's Comments & Concrete Evidences (If Performance Rating of 1 or 4 and 5 is given)
                </h6>


                <div class="alert-message alert-message-success">
                    <blockquote class="danger" style="font-size: small; background-color: gainsboro; text-align: left">
                        <h5><b>ส่วนที่ 3: ความสามารถที่โดดเด่นและด้านที่ต้องพัฒนาเพิ่มเติม</b></h5>
                    </blockquote>
                    <h5 style="font-size: small;"><b>คำชี้แจง: โปรดเลือกหัวข้อปัจจัยพฤติกรรมที่พนักงานต้องพัฒนาเพิ่มเติม 1 ข้อ และปัจจัยพฤติกรรมความสามารถที่โดดเด่น 1 ข้อ โดยพนักงานและผู้บังคับบัญชาสามารถรับผิดชอบในการพัฒนาได้ด้วยตนเอง</b></h5>
                    <h5 style="font-size: medium;"><b>3.1 ด้านที่ต้องพัฒนาเพิ่มเติม</b></h5>

                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; ด้านที่ต้องพัฒนาเพิ่มเติม</strong></h3>
                    </div>
                    <asp:FormView ID="fvcomment" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">

                                        <label class="col-sm-3 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label23" runat="server" Text="ชื่อพฤติกรรม/ปัจจัย" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label27" runat="server" Text="(Behavior/Factor Name)" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-9">
                                            <asp:DropDownList ID="ddlimprove_insert" CssClass="form-control" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveDev_DataSet" runat="server" Display="None"
                                                ControlToValidate="ddlimprove_insert" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกด้านที่ต้องพัฒนา"
                                                ValidationExpression="กรุณาเลือกหัวข้อหลัก" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidadtorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                        </div>


                                        <%-- <asp:TextBox ID="txtbehavior" TextMode="MultiLine" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveDev_DataSet" runat="server" Display="None" ControlToValidate="txtbehavior" Font-Size="11"
                                                ErrorMessage="กรุณากรอกพฤติกรรม"
                                                ValidationExpression="กรุณากรอกพฤติกรรม"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="SaveDev_DataSet" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtbehavior"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                             </div>
                                        --%>
                                    </div>
                                    <div class="form-group">

                                        <label class="col-sm-3 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label4" runat="server" Text="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label5" runat="server" Text="Comment & Development Actions Plan (please identify action, timeline and person in charge)" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtcomment" TextMode="MultiLine" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="SaveDev_DataSet" runat="server" Display="None" ControlToValidate="txtcomment" Font-Size="11"
                                                ErrorMessage="กรุณากรอกคอมเม้นท์"
                                                ValidationExpression="กรุณากรอกคอมเม้นท์"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server"
                                                ValidationGroup="SaveDev_DataSet" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtcomment"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator6" Width="160" />

                                        </div>



                                    </div>

                                    <div class="form-group" runat="server">
                                        <div class="col-sm-2 col-sm-offset-3">
                                            <asp:LinkButton ID="btnAdddataset_dev" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdInsert_DatasetDev" OnCommand="btnCommand" ValidationGroup="SaveDev_DataSet" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save">ADD +</asp:LinkButton>
                                        </div>
                                    </div>


                                    <div class="form-group">


                                        <div class="col-lg-12">
                                            <asp:GridView ID="GvDevAdd" Visible="false"
                                                runat="server"
                                                HeaderStyle-CssClass="danger"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                GridLines="None"
                                                OnRowCommand="onRowCommand"
                                                OnRowDataBound="Master_RowDataBound"
                                                AutoGenerateColumns="false">


                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>

                                                    <asp:TemplateField HeaderText="No." ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>

                                                            <b><%# (Container.DataItemIndex +1) %></b>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <%-- <asp:Literal ID="lbm1_typeidx" runat="server" Text='<%# Eval("typeidx") %>' />--%>
                                                            <asp:Literal ID="lbm1_type_name" runat="server" Text='<%# Eval("behavior") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Literal ID="lbstatus_name" runat="server" Text='<%# Eval("comment") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Delete" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btndeletedev" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="btndeletedev"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                </div>

                <h5 style="font-size: medium;"><b>3.2 ความสามารถที่โดดเด่น</b></h5>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-star"></i><strong>&nbsp; ความสามารถที่โดดเด่น</strong></h3>
                    </div>

                    <asp:FormView ID="fvstar" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">

                                        <label class="col-sm-3 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label23" runat="server" Text="ชื่อพฤติกรรม/ปัจจัย" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label27" runat="server" Text="(Behavior/Factor Name)" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-9">
                                            <asp:DropDownList ID="ddlstar_insert" CssClass="form-control" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveStar_DataSet" runat="server" Display="None"
                                                ControlToValidate="ddlstar_insert" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกด้านที่ต้องพัฒนา"
                                                ValidationExpression="กรุณาเลือกหัวข้อหลัก" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidadtorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                        </div>

                                    </div>


                                    <div class="form-group">

                                        <label class="col-sm-3 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label4" runat="server" Text="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label5" runat="server" Text="Comment & Development Actions Plan (please identify action, timeline and person in charge)" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtcomment_star" TextMode="MultiLine" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="SaveStar_DataSet" runat="server" Display="None" ControlToValidate="txtcomment_star" Font-Size="11"
                                                ErrorMessage="กรุณากรอกคอมเม้นท์"
                                                ValidationExpression="กรุณากรอกคอมเม้นท์"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server"
                                                ValidationGroup="SaveStar_DataSet" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtcomment_star"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator7" Width="160" />

                                        </div>

                                    </div>


                                    <div class="form-group" runat="server">
                                        <div class="col-sm-2 col-sm-offset-3">
                                            <asp:LinkButton ID="btnAdddataset_star" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdInsert_Datasetstar" OnCommand="btnCommand" ValidationGroup="SaveStar_DataSet" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save">ADD +</asp:LinkButton>
                                        </div>
                                    </div>


                                    <div class="form-group">


                                        <div class="col-lg-12">
                                            <asp:GridView ID="GvStarAdd" Visible="false"
                                                runat="server"
                                                HeaderStyle-CssClass="danger"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                GridLines="None"
                                                OnRowDataBound="Master_RowDataBound"
                                                OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">


                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>

                                                    <asp:TemplateField HeaderText="No." ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>

                                                            <b><%# (Container.DataItemIndex +1) %></b>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Literal ID="lbbehavior_star" runat="server" Text='<%# Eval("behavior_star") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Literal ID="lbcomment_star" runat="server" Text='<%# Eval("comment_star") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Delete" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btndeletestar" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="btndeletestar"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                </div>


                <div id="div_saveinsert" runat="server">

                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <%-- <div class="form-group">
                                <asp:Label ID="Label7" CssClass="col-sm-4 control-label text_right" runat="server" Text="สถานะการใช้งาน : " />
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlstatus_insert" CssClass="form-control" runat="server">
                                      </asp:DropDownList>
                                </div>

                            </div>--%>
                            <div class="form-group">
                                <div class="col-sm-4 col-sm-offset-8">
                                    <asp:LinkButton ID="LinkButton5" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdInsert" CommandArgument="2" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i> บันทึกข้อมูล</asp:LinkButton>
                                    <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" CommandArgument="1" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="glyphicon glyphicon-envelope"></i> ส่งประเมินข้อมูล</asp:LinkButton>
                                    <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="btnCancel" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i> ยกเลิก</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </asp:View>

        <asp:View ID="ViewApprove" runat="server">
            <div class="col-lg-12">
                <asp:Image ID="Image1" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/tpm-formresult/Cover_Head_ฟอร์มประเมิน_GIF_Test-03_0.gif" Style="height: 100%; width: 100%;" />
            </div>

            <br />
            <div class="col-lg-12">

                <div class="alert-message alert-message-success">
                    <blockquote class="danger" style="font-size: small; background-color: #A7DB42; text-align: left">
                        <h5><b>
                            <asp:Label ID="Label1" runat="server">รายการรออนุมัติ</asp:Label></b></h5>
                    </blockquote>
                </div>

                <asp:GridView ID="Gvapprove_index" runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                    HeaderStyle-CssClass="info small"
                    HeaderStyle-Height="40px"
                    AllowPaging="true"
                    PageSize="10"
                     OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound"
                    DataKeyNames="u0_docidx">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="ข้อมูลผู้ประเมิน" ItemStyle-Width="45%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:Label ID="lblu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                <asp:Label ID="lblunidx" runat="server" Visible="false" Text='<%# Eval("unidx") %>'></asp:Label>
                                <asp:Label ID="lblacidx" runat="server" Visible="false" Text='<%# Eval("acidx") %>'></asp:Label>
                                <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                <small>
                                    <strong>
                                        <asp:Label ID="Labesl15" runat="server">องค์กร: </asp:Label></strong>
                                    <asp:Literal ID="litorg_idx" Visible="false" runat="server" Text='<%# Eval("org_idx") %>' />
                                    <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("org_name_th") %>' />
                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labeld24" runat="server">ฝ่าย: </asp:Label></strong>
                                    <asp:Literal ID="litrdept_idx" Visible="false" runat="server" Text='<%# Eval("rdept_idx") %>' />
                                    <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label25" runat="server">แผนก: </asp:Label></strong>
                                    <asp:Literal ID="litrsec_idx" Visible="false" runat="server" Text='<%# Eval("rsec_idx") %>' />
                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                    </p>
                                                           <strong>
                                                               <asp:Label ID="Label19" runat="server">ตำแหน่ง: </asp:Label></strong>
                                    <asp:Literal ID="litrpos_idx" Visible="false" runat="server" Text='<%# Eval("rpos_idx") %>' />
                                    <asp:Literal ID="Literal9" runat="server" Text='<%# Eval("pos_name_th") %>' />
                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label26" runat="server">ชื่อ-สกุล: </asp:Label></strong>
                                    <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                    <asp:Literal ID="litcemp_idx" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                    </p>
                                </small>
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="คะแนนประเมินตนเอง" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <strong>
                                        <asp:Label ID="Labtel15" runat="server">Core Value : </asp:Label></strong>
                                    <asp:Literal ID="litsumminecore" runat="server" Text='<%# Eval("sum_mine_core_value") %>' />
                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labefl24" runat="server">Competencies : </asp:Label></strong>
                                    <asp:Literal ID="litsumminecom" runat="server" Text='<%# Eval("sum_mine_competency") %>' />
                                    </p>
                                  
                                </small>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="คะแนนผู้มีสิทธิ์ประเมิน" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <strong>
                                        <asp:Label ID="Label15" runat="server">Core Value : </asp:Label></strong>
                                    <asp:Literal ID="litsumheadcore" runat="server" Text='<%# Eval("sum_head_core_value") %>' />
                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label24" runat="server">Competencies : </asp:Label></strong>
                                    <asp:Literal ID="litsumheadcom" runat="server" Text='<%# Eval("sum_head_competency") %>' />
                                    </p>
                                  
                                </small>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                            <ItemTemplate>
                                <b>
                                    <asp:Label ID="lblStatusDoc" runat="server" Text='<%# Eval("StatusDoc") %>' />
                                </b>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnviewdetail" CssClass="btn btn-warning btn-sm" runat="server" OnCommand="btnCommand" ToolTip="View" CommandName="CmdDetail" CommandArgument='<%#  Eval("u0_docidx") + ";" + Eval("cemp_idx") + ";" +"2"+  ";" +  Eval("unidx")+  ";" +  Eval("acidx")+  ";" +  Eval("staidx") + ";" + Eval("form_name") + ";" + Eval("sum_head_core_value") + ";" + Eval("sum_head_competency") + ";" + Eval("u0_typeidx") + ";" + "2"%>'><i class="	fa fa-book"></i></asp:LinkButton>

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>

        <asp:View ID="ViewDetail" runat="server">
            <div class="col-lg-12">

                <div class="alert-message alert-message-success">
                    <blockquote class="danger" style="font-size: small; background-color: lightsalmon; text-align: left">
                        <h5 style="text-align: center"><b>
                            <asp:Label ID="lblHeadForm_detail" runat="server"></asp:Label></b></h5>
                    </blockquote>
                </div>

                <div class="alert-message alert-message-success">
                    <blockquote class="danger" style="font-size: small; background-color: gainsboro; text-align: left">
                        <h5><b>ส่วนที่ 1: ข้อมูลทั่วไปเกี่ยวกับพนักงาน</b></h5>
                    </blockquote>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="Fvdetailusercreate" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <%--  <div class="form-horizontal" role="form">

                                      <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ Employee ID : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ Name - Lastname : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                     <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท Organization : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="txtorgidx" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย Division : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก / ส่วนDepartment/Section : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="txtsecidx" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง Position : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                     <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="วันที่เริ่มงาน Start date : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtstartdate" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                                --%>

                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label38" runat="server" Text="รหัสผู้ทำรายการ" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label39" runat="server" Text="Employee ID" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label121" runat="server" Text="ชื่อ - นามสกุล" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label122" runat="server" Text="Name - Lastname" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label2" runat="server" Text="บริษัท" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label20" runat="server" Text="Organization" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="txtorgidx" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label21" runat="server" Text="ฝ่าย" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label22" runat="server" Text="Division" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label23" runat="server" Text="แผนก / ส่วน" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label27" runat="server" Text="Department/Section" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:TextBox ID="txtsecidx" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label28" runat="server" Text="ตำแหน่ง" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label29" runat="server" Text="Position" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label30" runat="server" Text="วันที่เริ่มงาน" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label31" runat="server" Text="Start date" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtstartdate" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <label class="col-sm-2 control-label col-sm-offset-1">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label32" runat="server" Text="อีเมล์" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label33" runat="server" Text="E-mail" /></b></small>
                                            </h2>
                                        </label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>

                <div class="alert-message alert-message-success">
                    <blockquote class="danger" style="font-size: small; background-color: gainsboro; text-align: left">
                        <h5><b>ส่วนที่ 2: การประเมินผลการปฏิบัติงานและการให้ข้อมูลป้อนกลับ</b></h5>
                    </blockquote>

                    <h5 style="font-size: small;"><b>คำชี้แจง: ในการประเมินผลปลายปี เราจะใช้เกณฑ์คะแนนประเมินพฤติกรรม 1 - 5 โดยแต่ละคะแนนจะมีความหมายและแนวทางของเกณฑ์ประเมินพฤติกรรมเพื่อการพิจารณาตามตารางด้านล่างนี้</b></h5>

                </div>


                <asp:GridView ID="gvpoint_view" runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-lg-12"
                    HeaderStyle-CssClass="danger"
                    HeaderStyle-Height="40px"
                    AllowPaging="false"
                    OnRowDataBound="Master_RowDataBound"
                    DataKeyNames="m0_point">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <asp:Label ID="lblScore" runat="server" Text='<%# Eval("point_name") %>'></asp:Label>
                                    </b>

                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="30%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <small>
                                    <b>
                                        <p>
                                            <asp:Label ID="lblRatth" runat="server" Text='<%# Eval("rat_nameth") %>'></asp:Label>
                                        </p>
                                        <p>
                                            <asp:Label ID="lblRaten" runat="server" Text='<%# Eval("rat_nameen") %>'></asp:Label>
                                        </p>
                                    </b>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="60%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <small>
                                    <p>
                                        <asp:Label ID="lblDefinitionth" runat="server" Text='<%# Eval("definition_th") %>'></asp:Label>
                                    </p>
                                    <p>
                                        <asp:Label ID="lblDefinitionen" runat="server" Text='<%# Eval("definition_en") %>'></asp:Label>
                                    </p>

                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                <asp:GridView ID="gvcorevalue_view" runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-lg-12"
                    HeaderStyle-CssClass="danger"
                    HeaderStyle-Height="40px"
                    AllowPaging="false"
                    DataKeyNames="m1_coreidx"
                    OnRowDataBound="Master_RowDataBound">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="2%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <b>
                                    <asp:Label ID="lblCorevalue" runat="server" Text="Core Values"></asp:Label>

                                    <asp:Label ID="lblm1_coreidx" Visible="false" runat="server" Text='<%# Eval("m1_coreidx") %>'></asp:Label>
                                    <asp:Label ID="lblu1_docidx" Visible="false" runat="server" Text='<%# Eval("u1_docidx") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>

                                <b><%# (Container.DataItemIndex +1) %></b>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <%--<asp:Label ID="lbltype_core" runat="server" Text='<%# Eval("type_core") %>'></asp:Label></b>
                                <asp:Label ID="lblcore_name" runat="server" Text='<%# Eval("core_name") %>'></asp:Label>--%>
                                <%--  <b>
                                    <asp:Label ID="lbltype_core" runat="server" Text='<%# Eval("type_core") %>'></asp:Label>
                                    <br />
                                    <asp:Label ID="lblcore_name" runat="server" Text='<%# Eval("core_name") %>'></asp:Label></b>--%>
                                <b>
                                    <asp:Label ID="lblcore_name" runat="server" Text='<%# Eval("core_name") %>'></asp:Label>

                                    <br />
                                    <asp:Label ID="lbltype_core" runat="server" Text='<%# Eval("type_core") %>'></asp:Label>
                                </b>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="26%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="smaller">
                            <ItemTemplate>
                                <asp:GridView ID="gvsubcorevalue_view" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-borderedtable-responsive col-lg-12"
                                    HeaderStyle-CssClass="Medium"
                                    GridLines="None"
                                    ShowHeader="false"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="false"
                                    DataKeyNames="m2_coreidx">


                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField ItemStyle-CssClass="text-left" ItemStyle-Width="100%" Visible="true" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <asp:Label ID="lblm2_coreidx" Visible="false" runat="server" Text='<%# Eval("m2_coreidx") %>' />

                                                <small>
                                                    <p>
                                                        <asp:Label ID="lblcore_nameen" runat="server" Text='<%# Eval("core_nameen") %>'></asp:Label>
                                                    </p>
                                                    <p>
                                                        <asp:Label ID="lblcore_nameth" runat="server" Text='<%# Eval("core_nameth") %>'></asp:Label>
                                                    </p>
                                                </small>

                                            </ItemTemplate>

                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-Width="32%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>

                                <asp:RadioButtonList ID="rdochoice_mine_detail" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="RequiredFiesldValidator6"
                                    ValidationGroup="SaveAdd" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                    Display="None" SetFocusOnError="true" ControlToValidate="rdochoice_mine_detail" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExxtenxder7" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFiesldValidator6" Width="160"
                                    PopupPosition="BottomLeft" />

                                <asp:Label ID="lblanswer" runat="server" Visible="false" Text='<%# Eval("m0_point_mine") %>' />

                                <asp:Panel ID="panel_update_comment_coremine" runat="server" Visible="false">
                                    <div class="form-group">
                                        <asp:Label ID="Ladabel7" CssClass="control-label" runat="server" Text="กรุณาระบุเหตุผล : " />
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="txtremarkmine_core" Text='<%# Eval("remark_mine") %>' TextMode="MultiLine" Rows="5" Columns="3" CssClass="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtremarkmine_core" Font-Size="11"
                                            ErrorMessage="กรุณาระบุเหตุผล"
                                            ValidationExpression="กรุณาระบุเหตุผล"
                                            SetFocusOnError="true" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValiddatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                            ValidationGroup="SaveAdd" Display="None"
                                            ErrorMessage="กรุณาระบุเหตุผลอย่างน้อย 50 ตัวอักษร" Font-Size="11"
                                            ControlToValidate="txtremarkmine_core"
                                            ValidationExpression="^[\s\S]{50,1000}$"
                                            SetFocusOnError="true" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                        <asp:Label ID="lblremark_coremine" Visible="false" Text='<%# Eval("remark_mine") %>' CssClass="control-label" runat="server" />
                                    </div>

                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField Visible="false" ItemStyle-Width="32%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <asp:RadioButtonList ID="rdochoice_head_detail" runat="server" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                </asp:RadioButtonList>

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                    ValidationGroup="SaveAdd" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                    Display="None" SetFocusOnError="true" ControlToValidate="rdochoice_head_detail" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExetenxder7" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160"
                                    PopupPosition="BottomLeft" />

                                <asp:Panel ID="panel_update_comment_corehead" runat="server" Visible="false">
                                    <div class="form-group">
                                        <asp:Label ID="Ladbeyl7" CssClass="control-label" runat="server" Text="กรุณาระบุเหตุผล : " />
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="txtremarkhead_core" Text='<%# Eval("remark_head") %>' TextMode="MultiLine" Rows="5" Columns="3" CssClass="form-control" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatodr2" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtremarkhead_core" Font-Size="11"
                                            ErrorMessage="กรุณาระบุเหตุผล"
                                            ValidationExpression="กรุณาระบุเหตุผล"
                                            SetFocusOnError="true" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatodrCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatodr2" Width="160" />
                                        <asp:RegularExpressionValidator ID="RegularExpressisonValidator3" runat="server"
                                            ValidationGroup="SaveAdd" Display="None"
                                            ErrorMessage="กรุณาระบุเหตุผลอย่างน้อย 50 ตัวอักษร" Font-Size="11"
                                            ControlToValidate="txtremarkhead_core"
                                            ValidationExpression="^[\s\S]{50,1000}$"
                                            SetFocusOnError="true" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCdalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressisonValidator3" Width="160" />

                                    </div>

                                </asp:Panel>
                                <asp:Label ID="lblanswer_head" runat="server" Visible="false" Text='<%# Eval("m0_point_head") %>' />
                                <asp:Label ID="lblremark_corehead" Visible="false" Text='<%# Eval("remark_head") %>' CssClass="control-label" runat="server" />


                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                <asp:GridView ID="gvcoreompetencies_view" runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-lg-12"
                    HeaderStyle-CssClass="danger"
                    HeaderStyle-Height="40px"
                    AllowPaging="false"
                    DataKeyNames="m1_typeidx"
                    OnRowDataBound="Master_RowDataBound">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>
                    <Columns>

                        <asp:TemplateField HeaderText="ปัจจัยประเมิน Competency Factor" ItemStyle-Width="2%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <b>
                                    <asp:Label ID="lblCompetencies" runat="server" Text="Competencies"></asp:Label>
                                    <asp:Label ID="lblu0_typeidx" Visible="false" runat="server" Text='<%# Eval("u0_typeidx") %>'></asp:Label></b>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ปัจจัยประเมิน Competency Factor" ItemStyle-Width="2%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>

                                <b>
                                    <asp:Label ID="lblno" runat="server"></asp:Label></b>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ปัจจัยประเมิน Competency Factor" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <b>
                                    <asp:Label ID="lblm1_typeidx" Visible="false" runat="server" Text='<%# Eval("m1_typeidx") %>'></asp:Label>
                                    <asp:Label ID="lblm1_type_name" runat="server" Text='<%# Eval("m1_type_name") %>'></asp:Label>
                                    <asp:Label ID="lblu1_docidx" Visible="false" runat="server" Text='<%# Eval("u1_docidx") %>'></asp:Label></b>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="พฤติกรรมการทำงานที่บริษัทคาดหวัง Behavioral Indicators" ItemStyle-Width="26%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="smaller">
                            <ItemTemplate>
                                <asp:GridView ID="gvsubcompetencies_view" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-borderedtable-responsive col-lg-12"
                                    HeaderStyle-CssClass="Medium"
                                    GridLines="None"
                                    ShowHeader="false"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="false"
                                    DataKeyNames="m2_typeidx">


                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField ItemStyle-CssClass="text-left" ItemStyle-Width="100%" Visible="true" ItemStyle-Font-Size="Medium">
                                            <ItemTemplate>
                                                <asp:Label ID="lblm2_coreidx" Visible="false" runat="server" Text='<%# Eval("m2_typeidx") %>' />
                                                <small>
                                                    <asp:Label ID="lblm2_type_name" runat="server" Text='<%# Eval("m2_type_name") %>'></asp:Label>
                                                </small>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ประเมินตนเอง Self Assessment (1-5)" ItemStyle-Width="32%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>

                                <asp:RadioButtonList ID="rdochoicecom_mine_detail" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="RequiredFiesladValidator6"
                                    ValidationGroup="SaveAdd" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                    Display="None" SetFocusOnError="true" ControlToValidate="rdochoicecom_mine_detail" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalsloutExtenxder7" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFiesladValidator6" Width="160"
                                    PopupPosition="BottomLeft" />


                                <asp:Panel ID="panel_update_comment_competenciesmine" runat="server" Visible="false">
                                    <%--  <hr />--%>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:Label ID="Ladbsel7" CssClass="control-label" runat="server" Text="กรุณาระบุเหตุผล : " />
                                        </div>
                                        <div class="col-sm-8">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="lblremark" Text='<%# Eval("remark_mine") %>' TextMode="MultiLine" Rows="5" Columns="3" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="lblremark" Font-Size="11"
                                                ErrorMessage="กรุณาระบุเหตุผล"
                                                ValidationExpression="กรุณาระบุเหตุผล"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="กรุณาระบุเหตุผลอย่างน้อย 50 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="lblremark"
                                                ValidationExpression="^[\s\S]{50,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatworCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                        </div>
                                    </div>

                                </asp:Panel>
                                <asp:Label ID="lblanswer" runat="server" Visible="false" Text='<%# Eval("m0_point_mine") %>' />
                                <asp:Label ID="lblremark_commine" Visible="false" Text='<%# Eval("remark_mine") %>' CssClass="control-label" runat="server" />


                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="หัวหน้าประเมิน Superior's Rating (1-5)" Visible="false" ItemStyle-Width="32%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>
                                <asp:RadioButtonList ID="rdochoicecom_head_detail" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                </asp:RadioButtonList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                    ValidationGroup="SaveAdd" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                    Display="None" SetFocusOnError="true" ControlToValidate="rdochoicecom_head_detail" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatofrCalloutExtenxder7" runat="Server"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160"
                                    PopupPosition="BottomLeft" />


                                <asp:Panel ID="panel_update_comment_competencieshead" runat="server" Visible="false">
                                    <%-- <hr />--%>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:Label ID="Ladabel7" CssClass="control-label" runat="server" Text="กรุณาระบุเหตุผล : " />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txtremark_head" Text='<%# Eval("remark_head") %>' TextMode="MultiLine" Rows="5" Columns="3" CssClass="form-control" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidaator2" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtremark_head" Font-Size="11"
                                                ErrorMessage="กรุณาระบุเหตุผล"
                                                ValidationExpression="กรุณาระบุเหตุผล"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatosrCallosutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidaator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressiwonValidator3" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="กรุณาระบุเหตุผลอย่างน้อย 50 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark_head"
                                                ValidationExpression="^[\s\S]{50,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtendger4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressiwonValidator3" Width="160" />

                                        </div>
                                    </div>

                                </asp:Panel>
                                <asp:Label ID="lblanswer_head" runat="server" Visible="false" Text='<%# Eval("m0_point_head") %>' />
                                <asp:Label ID="lblremark_comhead" Visible="false" Text='<%# Eval("remark_head") %>' CssClass="control-label" runat="server" />

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                <h6 style="color: #0070c0;">Remark: Superior's Comments & Concrete Evidences (If Performance Rating of 1 or 4 and 5 is given)
                </h6>


                <div class="alert-message alert-message-success">
                    <blockquote class="danger" style="font-size: Medium; background-color: gainsboro; text-align: left">
                        <h5><b>ส่วนที่ 3: ความสามารถที่โดดเด่นและด้านที่ต้องพัฒนาเพิ่มเติม</b></h5>
                    </blockquote>
                    <h5 style="font-size: small;"><b>คำชี้แจง: โปรดเลือกหัวข้อปัจจัยพฤติกรรมที่พนักงานต้องพัฒนาเพิ่มเติม 1 ข้อ และปัจจัยพฤติกรรมความสามารถที่โดดเด่น 1 ข้อ โดยพนักงานและผู้บังคับบัญชาสามารถรับผิดชอบในการพัฒนาได้ด้วยตนเอง</b></h5>
                    <h5 style="font-size: Medium;"><b>3.1 ด้านที่ต้องพัฒนาเพิ่มเติม</b></h5>

                </div>


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; ด้านที่ต้องพัฒนาเพิ่มเติม</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <asp:GridView ID="gvimprove_detail" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="danger"
                                HeaderStyle-Height="40px"
                                AllowPaging="false"
                                DataKeyNames="u2_docidx"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField ItemStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="Label11" runat="server" Text='<%# Eval("FullNameTH") %>'></asp:Label>
                                            <br />
                                            <asp:Label ID="lblu2_docidx" Visible="false" runat="server" Text='<%# Eval("u2_docidx") %>'></asp:Label>
                                            <asp:Label ID="lblcemp_idx" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>'></asp:Label>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("typename") %>'></asp:Label>
                                            <asp:Label ID="lblm0_typeidx_choose" Visible="false" runat="server" Text='<%# Eval("m0_typeidx_choose") %>'></asp:Label>

                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtu2_docidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("u2_docidx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">

                                                        <label class="col-sm-3 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label23" runat="server" Text="ชื่อพฤติกรรม/ปัจจัย" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label27" runat="server" Text="(Behavior/Factor Name)" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-9">
                                                            <asp:TextBox ID="lblm0_typeidx_choose_edit" Visible="false" runat="server" Text='<%# Eval("m0_typeidx_choose") %>'></asp:TextBox>

                                                            <asp:TextBox ID="txtbehavior_edit" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("behavior_name")%>' />
                                                            <asp:DropDownList ID="ddlimprove_edit" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                ControlToValidate="ddlimprove_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกด้านที่ต้องพัฒนา"
                                                                ValidationExpression="กรุณาเลือกด้านที่ต้องพัฒนา" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidadtorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />

                                                        </div>
                                                    </div>

                                                    <%--<div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ชื่อพฤติกรรม/ปัจจัย(Behavior/Factor Name)" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="lblm0_typeidx_choose_edit" Visible="false" runat="server" Text='<%# Eval("m0_typeidx_choose") %>'></asp:TextBox>

                                                            <asp:TextBox ID="txtbehavior_edit" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("behavior_name")%>' />
                                                            <asp:DropDownList ID="ddlimprove_edit" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Save_edit" runat="server" Display="None"
                                                                ControlToValidate="ddlimprove_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกด้านที่ต้องพัฒนา"
                                                                ValidationExpression="กรุณาเลือกด้านที่ต้องพัฒนา" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidadtorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />

                                                        </div>
                                                    </div>--%>


                                                    <div class="form-group">

                                                        <label class="col-sm-3 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label4" runat="server" Text="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label5" runat="server" Text="Comment & Development Actions Plan (please identify action, timeline and person in charge)" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-9">
                                                            <asp:TextBox ID="txtcomment_edit" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("comment_name")%>' />

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtcomment_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาใส่สถานะรายการ"
                                                                ValidationExpression="กรุณาใส่สถานะรายการ"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                                ValidationGroup="Save_edit" Display="None"
                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                ControlToValidate="txtcomment_edit"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                                        </div>
                                                    </div>

                                                    <%--  <div class="form-group">
                                                        <asp:Label ID="Label10" runat="server" Text="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)Comment & Development Actions Plan (please identify action, timeline and person in charge)" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtcomment_edit" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("comment_name")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtcomment_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาใส่สถานะรายการ"
                                                            ValidationExpression="กรุณาใส่สถานะรายการ"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                            ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtcomment_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                                    </div>--%>

                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-Width="50%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>

                                            <asp:Label ID="Ldabel3" runat="server" Text='<%# Eval("comment_name") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="จัดการข้อมูล" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit_improve" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete_improve" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel_comment" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("u2_docidx") + ";" + 3 %>'><i class="fa fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>




                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>

                    <asp:FormView ID="fvimprove_edit" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">

                                        <label class="col-sm-3 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label23" runat="server" Text="ชื่อพฤติกรรม/ปัจจัย" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label27" runat="server" Text="(Behavior/Factor Name)" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-9">
                                            <asp:DropDownList ID="ddlimprove_update" CssClass="form-control" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveEdit_Improve" runat="server" Display="None"
                                                ControlToValidate="ddlimprove_update" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกด้านที่ต้องพัฒนา"
                                                ValidationExpression="กรุณาเลือกด้านที่ต้องพัฒนา" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidadtorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                        </div>
                                    </div>


                                    <div class="form-group">

                                        <label class="col-sm-3 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label4" runat="server" Text="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label5" runat="server" Text="Comment & Development Actions Plan (please identify action, timeline and person in charge)" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtcomment_edit" TextMode="MultiLine" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveEdit_Improve" runat="server" Display="None" ControlToValidate="txtcomment_edit" Font-Size="11"
                                                ErrorMessage="กรุณากรอกคอมเม้นท์"
                                                ValidationExpression="กรุณากรอกคอมเม้นท์"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatosrwCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="SaveEdit_Improve" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtcomment_edit"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCa3lloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                        </div>
                                    </div>


                                    <div class="form-group" runat="server">
                                        <div class="col-sm-2 col-sm-offset-3">
                                            <asp:LinkButton ID="btnAdddataset_dev" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdUpdate_DatasetDev" OnCommand="btnCommand" ValidationGroup="SaveEdit_Improve" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save">ADD +</asp:LinkButton>
                                        </div>
                                    </div>


                                    <div class="form-group">


                                        <div class="col-lg-12">
                                            <asp:GridView ID="GvDevEdit" Visible="false"
                                                runat="server"
                                                HeaderStyle-CssClass="danger"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                GridLines="None"
                                                OnRowDataBound="Master_RowDataBound"
                                                OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">


                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>

                                                    <asp:TemplateField HeaderText="No." ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>

                                                            <b><%# (Container.DataItemIndex +1) %></b>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Literal ID="litm1_typeidx_comma" Visible="false" runat="server" Text='<%# Eval("m1_typeidx_comma") %>' />
                                                            <asp:Literal ID="lbm1_type_name" runat="server" Text='<%# Eval("behavior") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Literal ID="lbstatus_name" runat="server" Text='<%# Eval("comment") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Delete" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btndeletedev_edit" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="btndeletedev_edit"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                </div>


                <h5 style="font-size: Medium;"><b>3.2 ความสามารถที่โดดเด่น</b></h5>


                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-star"></i><strong>&nbsp; ความสามารถที่โดดเด่น</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <asp:GridView ID="gvstar_detail" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="danger"
                                HeaderStyle-Height="40px"
                                AllowPaging="false"
                                DataKeyNames="u2_docidx"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="ชื่อพฤติกรรม/ปัจจัย(Behavior/Factor Name)" ItemStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="Label11" runat="server" Text='<%# Eval("FullNameTH") %>'></asp:Label>
                                            <br />
                                            <asp:Label ID="lblu2_docidx" Visible="false" runat="server" Text='<%# Eval("u2_docidx") %>'></asp:Label>
                                            <asp:Label ID="lblcemp_idx" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>'></asp:Label>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("typename") %>'></asp:Label>
                                            <asp:Label ID="lblm0_typeidx_choose" Visible="false" runat="server" Text='<%# Eval("m0_typeidx_choose") %>'></asp:Label>

                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtu2_docidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("u2_docidx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">

                                                        <label class="col-sm-3 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label23" runat="server" Text="ชื่อพฤติกรรม/ปัจจัย" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label27" runat="server" Text="(Behavior/Factor Name)" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-9">
                                                            <asp:TextBox ID="lblm0_typeidx_choose_edit" Visible="false" runat="server" Text='<%# Eval("m0_typeidx_choose") %>'></asp:TextBox>
                                                            <asp:TextBox ID="txtbehavior_edit" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("behavior_name")%>' />

                                                            <asp:DropDownList ID="ddlstar_edit" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="SaveEdit_Star" runat="server" Display="None"
                                                                ControlToValidate="ddlstar_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกความสามารถที่โดดเด่น"
                                                                ValidationExpression="กรุณาเลือกความสามารถที่โดดเด่น" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidadtorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />

                                                        </div>
                                                    </div>

                                                    <%-- <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ชื่อพฤติกรรม/ปัจจัย(Behavior/Factor Name)" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="lblm0_typeidx_choose_edit" Visible="false" runat="server" Text='<%# Eval("m0_typeidx_choose") %>'></asp:TextBox>
                                                            <asp:TextBox ID="txtbehavior_edit" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("behavior_name")%>' />

                                                            <asp:DropDownList ID="ddlstar_edit" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="SaveEdit_Star" runat="server" Display="None"
                                                                ControlToValidate="ddlstar_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกความสามารถที่โดดเด่น"
                                                                ValidationExpression="กรุณาเลือกความสามารถที่โดดเด่น" InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidadtorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />


                                                        </div>

                                                    </div>--%>

                                                    <div class="form-group">

                                                        <label class="col-sm-3 control-label">
                                                            <h2 class="panel-title">
                                                                <asp:Label ID="Label4" runat="server" Text="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" /><br />
                                                                <small><b>
                                                                    <asp:Label ID="Label5" runat="server" Text="Comment & Development Actions Plan (please identify action, timeline and person in charge)" /></b></small>
                                                            </h2>
                                                        </label>

                                                        <div class="col-sm-9">
                                                            <asp:TextBox ID="txtcomment_edit" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("comment_name")%>' />

                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtcomment_edit" Font-Size="11"
                                                                ErrorMessage="กรุณาใส่สถานะรายการ"
                                                                ValidationExpression="กรุณาใส่สถานะรายการ"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                                ValidationGroup="Save_edit" Display="None"
                                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                ControlToValidate="txtcomment_edit"
                                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                                SetFocusOnError="true" />

                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                                        </div>
                                                    </div>

                                                    <%--      <div class="form-group">
                                                        <asp:Label ID="Label10" runat="server" Text="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)Comment & Development Actions Plan (please identify action, timeline and person in charge)" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtcomment_edit" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("comment_name")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtcomment_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาใส่สถานะรายการ"
                                                            ValidationExpression="กรุณาใส่สถานะรายการ"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                            ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtcomment_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />

                                                    </div>--%>

                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)Comment & Development Actions Plan (please identify action, timeline and person in charge)" ItemStyle-Width="50%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>

                                            <asp:Label ID="Labesl3" runat="server" Text='<%# Eval("comment_name") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="จัดการข้อมูล" Visible="false" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit_star" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete_star" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel_comment" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("u2_docidx") + ";" + 4 %>'><i class="fa fa-trash"></i></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>




                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>


                    <asp:FormView ID="fvstart_edit" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">

                                        <label class="col-sm-3 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label23" runat="server" Text="ชื่อพฤติกรรม/ปัจจัย" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label27" runat="server" Text="(Behavior/Factor Name)" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-9">
                                            <asp:DropDownList ID="ddlstar_update" CssClass="form-control" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveEdit_Star" runat="server" Display="None"
                                                ControlToValidate="ddlstar_update" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกความสามารถที่โดดเด่น"
                                                ValidationExpression="กรุณาเลือกความสามารถที่โดดเด่น" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidadtorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                        </div>

                                    </div>




                                    <div class="form-group">

                                        <label class="col-sm-3 control-label">
                                            <h2 class="panel-title">
                                                <asp:Label ID="Label4" runat="server" Text="ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" /><br />
                                                <small><b>
                                                    <asp:Label ID="Label5" runat="server" Text="Comment & Development Actions Plan (please identify action, timeline and person in charge)" /></b></small>
                                            </h2>
                                        </label>

                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtcomment_star_edit" TextMode="MultiLine" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SaveEdit_Star" runat="server" Display="None" ControlToValidate="txtcomment_star_edit" Font-Size="11"
                                                ErrorMessage="กรุณากรอกคอมเม้นท์"
                                                ValidationExpression="กรุณากรอกคอมเม้นท์"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                                                ValidationGroup="SaveEdit_Star" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtcomment_star_edit"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator5" Width="160" />

                                        </div>

                                    </div>



                                    <div class="form-group" runat="server">
                                        <div class="col-sm-2 col-sm-offset-3">
                                            <asp:LinkButton ID="btnAdddataset_star" CssClass="btn btn-warning btn-sm" runat="server" ValidationGroup="SaveEdit_Star" CommandName="CmdUpdate_Datasetstar" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save">ADD +</asp:LinkButton>
                                        </div>
                                    </div>


                                    <div class="form-group">


                                        <div class="col-lg-12">
                                            <asp:GridView ID="GvStarEdit" Visible="false"
                                                runat="server"
                                                HeaderStyle-CssClass="danger"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                GridLines="None"
                                                OnRowDataBound="Master_RowDataBound"
                                                OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">


                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>

                                                    <asp:TemplateField HeaderText="No." ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>

                                                            <b><%# (Container.DataItemIndex +1) %></b>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Behavior/Factor Name" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Literal ID="litm1_typeidx_comma" Visible="false" runat="server" Text='<%# Eval("m1_typeidx_comma") %>' />
                                                            <asp:Literal ID="lbbehavior_star" runat="server" Text='<%# Eval("behavior_star") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Comment & Development Actions Plan (please identify action, timeline and person in charge)" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Literal ID="lbcomment_star" runat="server" Text='<%# Eval("comment_star") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Delete" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btndeletestar_edit" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="btndeletestar_edit"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                </div>

                <div id="divupdatebutton_createandapprove" runat="server">

                    <%-- <div class="panel-body">--%>
                    <div class="form-horizontal" role="form">

                        <%-- <div class="form-group">
                                <asp:Label ID="Label2" CssClass="col-sm-4 control-label text_right" runat="server" Text="สถานะการใช้งาน : " />
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlstatus_update" CssClass="form-control" runat="server">
                                    </asp:DropDownList>
                                </div>

                            </div>--%>
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-8">
                                <asp:LinkButton ID="btnunsuccess" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdUpdate" CommandArgument="2" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i> บันทึกข้อมูล</asp:LinkButton>
                                <asp:LinkButton ID="btnsuccess" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdUpdate" CommandArgument="1" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="glyphicon glyphicon-envelope"></i> ส่งประเมินข้อมูล</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton2" CssClass="btn btn-default  btn-sm" runat="server" CommandName="btnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i> ยกเลิก</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <%-- </div>--%>
                </div>

                <div id="divuserapprove" runat="server">

                    <%--  <div class="panel-body">--%>
                    <div class="form-horizontal" role="form">

                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-8">
                                <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdUpdate" CommandArgument="5" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการยอมรับการประเมินนี้ใช่หรือไม่ ?')" title="Save"><i class="glyphicon glyphicon-ok-circle"></i> ยอมรับการประเมิน</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton6" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdUpdate" CommandArgument="6" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณไม่ต้องการยอมรับการประเมินนี้ใช่หรือไม่ ?')" title="Save"><i class="glyphicon glyphicon-ban-circle"></i> ไม่ยอมรับการประเมิน</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton7" CssClass="btn btn-default  btn-sm" runat="server" CommandName="btnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i> ยกเลิก</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <%--</div>--%>
                </div>

                <div id="divapprove2" runat="server">

                    <%--  <div class="panel-body">--%>
                    <div class="form-horizontal" role="form">

                        <div class="form-group">
                            <div class="col-sm-3 col-sm-offset-9">
                                <asp:LinkButton ID="LinkButton8" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdUpdate" CommandArgument="4" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการยืนยันการรับทราบใช่หรือไม่ ?')" title="Save"><i class="glyphicon glyphicon-ok-circle"></i> รับทราบ</asp:LinkButton>
                                <asp:LinkButton ID="LinkButton10" CssClass="btn btn-default  btn-sm" runat="server" CommandName="btnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i> ยกเลิก</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                    <%--</div>--%>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-eye-open"></i><strong>&nbsp; ประวัติการอนุมัติ</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <asp:Repeater ID="rpLog" runat="server">
                                <HeaderTemplate>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"><small>วัน / เวลา</small></label>
                                        <label class="col-sm-3 control-label"><small>ผู้ดำเนินการ</small></label>
                                        <label class="col-sm-3 control-label"><small>ดำเนินการ</small></label>
                                        <label class="col-sm-3 control-label"><small>ผลการดำเนินการ</small></label>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="col-sm-2">
                                            </div>
                                            <div class="col-sm-10">
                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small><%#Eval("create_date")%></small></span>&nbsp;/&nbsp;
                                                    <small><%#Eval("time_create")%></small>

                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <span>&nbsp;&nbsp;&nbsp;<small><%# Eval("FullNameTH") %>&nbsp;&nbsp;&nbsp;(<%# Eval("actor_name") %>)</small></span>
                                        </div>
                                        <div class="col-sm-3">
                                            <span><small><%# Eval("node_name") %></small></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><small><%# Eval("status_name") %></small></span>
                                        </div>

                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>


                <div class="form-group" id="divcancel" runat="server" visible="false">
                    <div class="col-sm-1 col-sm-offset-11">
                        <asp:LinkButton ID="btnCancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                    </div>
                </div>

            </div>

        </asp:View>

        <asp:View ID="ViewReport" runat="server">
            <asp:UpdatePanel ID="update_report" runat="server">
                <ContentTemplate>
                    <div class="col-lg-12">
                        <asp:Image ID="Image2" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/tpm-formresult/Cover_Head_ฟอร์มประเมิน_GIF_Test-03_0.gif" Style="height: 100%; width: 100%;" />
                    </div>
                    <br />
                    <div class="col-lg-12">

                        <div class="alert-message alert-message-success">
                            <blockquote class="danger" style="font-size: small; background-color: #A7DB42; text-align: left">
                                <h5><b>
                                    <asp:Label ID="Label12" runat="server">รายงาน</asp:Label></b></h5>
                            </blockquote>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" style="background-color: darkkhaki; text-align: left">
                                <h2 class="panel-title"><i class="fa fa-folder-open"></i><strong>&nbsp; ค้นหาผลการประเมิน</strong></h2>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <asp:Label ID="Label59" runat="server" Text="ประเภทรายงาน" CssClass="col-sm-2 control-label"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypereport" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกประเภทรายงาน...</asp:ListItem>
                                                <asp:ListItem Value="1">รายงานรายละเอียดรายการ</asp:ListItem>
                                                <asp:ListItem Value="2">รายงานภาพรวมการประเมิน</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorw2" ValidationGroup="Search_Report" runat="server" Display="None"
                                                ControlToValidate="ddltypereport" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทรายงาน"
                                                ValidationExpression="กรุณาเลือกประเภทรายงาน" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorw2" Width="160" />

                                        </div>

                                        <asp:Label ID="Label13" runat="server" Text="องค์กร" CssClass="col-sm-2 control-label"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlOrg_report" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="SelectedIndexChanged">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Search_Report" runat="server" Display="None"
                                                ControlToValidate="ddlOrg_report" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกองค์กร"
                                                ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label7" runat="server" Text="ข้อมูลฝ่าย" CssClass="col-sm-2 control-label"></asp:Label>
                                        <div class="col-sm-10">
                                            <div class="checkbox checkbox-primary">
                                                <asp:RadioButtonList ID="rdochoose_dept" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" RepeatColumns="2" runat="server">
                                                    <asp:ListItem Value="1">ทั้งหมด</asp:ListItem>
                                                    <asp:ListItem Value="2">เฉพาะฝ่าย</asp:ListItem>
                                                </asp:RadioButtonList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidastor6"
                                                    ValidationGroup="Search_Report" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกข้อมูลฝ่าย"
                                                    Display="None" SetFocusOnError="true" ControlToValidate="rdochoose_dept" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="VaslidatorCaslloutExtenxder7" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidastor6" Width="160"
                                                    PopupPosition="BottomLeft" />
                                            </div>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div id="divdept" runat="server" visible="false" style="overflow-y: scroll; width: 100%;">
                                            <asp:Label ID="Label6" runat="server" Text="เลือกฝ่าย" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-10">
                                                <div class="checkbox checkbox-primary">
                                                    <asp:CheckBoxList ID="chkdept" RepeatColumns="2" runat="server" RepeatDirection="Vertical"
                                                        RepeatLayout="Table"
                                                        Width="100%"
                                                        TextAlign="Right">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_detail_table" runat="server" visible="false">

                                        <div class="form-group">
                                            <asp:Label ID="Label17" runat="server" Text="ชื่อพนักงาน" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtempname_report" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label18" runat="server" Text="รหัสพนักงาน" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtempcode_report" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label14" runat="server" Text="สถานะดำเนินการ" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlnode_report" runat="server" CssClass="form-control">
                                                    <%--   <asp:ListItem Value="5">กรุณาเลือกสถานะดำเนินการ....</asp:ListItem>--%>
                                                </asp:DropDownList>
                                            </div>
                                            <asp:Label ID="Label81" CssClass="col-sm-2 control-label" runat="server" Text="ปี : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlyear" CssClass="form-control" runat="server">
                                                </asp:DropDownList>

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div id="divcolumn" runat="server" visible="true" style="overflow-y: auto; width: 100%">

                                                <asp:Label ID="Label20" runat="server" Text="เลือกColumns" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-10">
                                                    <div class="checkbox checkbox-primary">
                                                        <asp:CheckBoxList ID="YrChkBoxColumns"
                                                            runat="server"
                                                            RepeatColumns="2"
                                                            RepeatDirection="Vertical"
                                                            RepeatLayout="Table"
                                                            Width="100%"
                                                            TextAlign="Right">
                                                        </asp:CheckBoxList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div id="div_all_table" runat="server" visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label8" CssClass="col-sm-2 control-label" runat="server" Text="ปี : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlyear_all" CssClass="form-control" runat="server">
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-3 col-sm-offset-2">
                                            <asp:LinkButton ID="btnsearch_report" CssClass="btn btn-info btn-sm" runat="server" CommandName="CmdSearch" OnCommand="btnCommand" ValidationGroup="Search_Report" title="Search"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnexport" CssClass="btn btn-warning btn-sm" runat="server" Visible="false" CommandName="CmdSearch_Export" OnCommand="btnCommand" ValidationGroup="Search_Report" title="Export Excel"><i class="glyphicon glyphicon-export"></i></asp:LinkButton>

                                            <asp:LinkButton ID="LinkButton4" CssClass="btn btn-default  btn-sm" runat="server" CommandName="btnrefresh" OnCommand="btnCommand" title="Refresh"><i class="
glyphicon glyphicon-refresh"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <asp:GridView ID="GvReport" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                            HeaderStyle-CssClass="info"
                            HeaderStyle-Height="40px"
                            AllowPaging="true"
                            PageSize="10"
                            OnPageIndexChanging="Master_PageIndexChanging"
                            OnRowDataBound="Master_RowDataBound"
                            DataKeyNames="u0_docidx">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="องค์กร" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <asp:Label ID="lblu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                        <asp:Label ID="lblunidx" runat="server" Visible="false" Text='<%# Eval("unidx") %>'></asp:Label>
                                        <asp:Label ID="lblacidx" runat="server" Visible="false" Text='<%# Eval("acidx") %>'></asp:Label>
                                        <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                        <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("org_name_th") %>' />

                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>

                                        <asp:Literal ID="litrdept_idx" Visible="false" runat="server" Text='<%# Eval("rdept_idx") %>' />
                                        <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("dept_name_th") %>' />

                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="แผนก" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>

                                        <asp:Literal ID="litrsec_idx" Visible="false" runat="server" Text='<%# Eval("rsec_idx") %>' />
                                        <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("sec_name_th") %>' />

                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>

                                        <asp:Literal ID="litrpos_idx" Visible="false" runat="server" Text='<%# Eval("rpos_idx") %>' />
                                        <asp:Literal ID="Literal9" runat="server" Text='<%# Eval("pos_name_th") %>' />

                                    </ItemTemplate>

                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Cost Center" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>

                                        <asp:Literal ID="litcostcenter_no" runat="server" Text='<%# Eval("costcenter_no") %>' />
                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>

                                        <asp:Literal ID="litempcode" runat="server" Text='<%# Eval("emp_code") %>' />

                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="ชื่อ-สกุล (ผู้ถูกประเมิน)" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                        <asp:Literal ID="litcemp_idx" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                    </ItemTemplate>

                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="คะแนนประเมินตนเอง Core Value" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <asp:Literal ID="litsumminecore" runat="server" Text='<%# Eval("sum_mine_core_value") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="คะแนนประเมินตนเอง Competency" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <asp:Literal ID="litsumminecom" runat="server" Text='<%# Eval("sum_mine_competency") %>' />

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ชื่อ-สกุล (ผู้มีสิทธิ์ประเมิน)" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("approve1") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="วันที่ดำเนินการเสร็จสิ้น" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <asp:Literal ID="litupdate_date" runat="server" Text='<%# Eval("update_date") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="คะแนนผู้มีสิทธิ์ประเมิน Core Value" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>

                                        <asp:Literal ID="litsumheadcore" runat="server" Text='<%# Eval("sum_head_core_value") %>' />

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="คะแนนผู้มีสิทธิ์ประเมิน Competency" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>

                                        <asp:Literal ID="litsumheadcom" runat="server" Text='<%# Eval("sum_head_competency") %>' />

                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatusDoc" runat="server" Text='<%# Eval("StatusDoc") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnviewdetail" CssClass="btn btn-warning btn-sm" runat="server" OnCommand="btnCommand" ToolTip="View" CommandName="CmdDetail" CommandArgument='<%#  Eval("u0_docidx") + ";" + Eval("cemp_idx") + ";" +"3"+  ";" +  Eval("unidx")+  ";" +  Eval("acidx")+  ";" +  Eval("staidx") + ";" + Eval("form_name") + ";" + Eval("sum_head_core_value") + ";" + Eval("sum_head_competency") + ";" + Eval("u0_typeidx")  + ";" + "3"%>'><i class="	fa fa-book"></i></asp:LinkButton>

                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>


                        <asp:GridView ID="GvExport_Score" runat="server"
                            AutoGenerateColumns="false"
                            AllowPaging="false"
                            OnRowDataBound="Master_RowDataBound"
                            CssClass="table table-striped table-bordered table-hover table-responsive">
                            <PagerStyle CssClass="PageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="องค์กร" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <asp:Label ID="lblu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                        <asp:Label ID="lblunidx" runat="server" Visible="false" Text='<%# Eval("unidx") %>'></asp:Label>
                                        <asp:Label ID="lblacidx" runat="server" Visible="false" Text='<%# Eval("acidx") %>'></asp:Label>
                                        <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                        <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("org_name_th") %>' />

                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>

                                        <asp:Literal ID="litrdept_idx" Visible="false" runat="server" Text='<%# Eval("rdept_idx") %>' />
                                        <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("dept_name_th") %>' />

                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="แผนก" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>

                                        <asp:Literal ID="litrsec_idx" Visible="false" runat="server" Text='<%# Eval("rsec_idx") %>' />
                                        <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("sec_name_th") %>' />

                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>

                                        <asp:Literal ID="litrpos_idx" Visible="false" runat="server" Text='<%# Eval("rpos_idx") %>' />
                                        <asp:Literal ID="Literal9" runat="server" Text='<%# Eval("pos_name_th") %>' />

                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Cost Center" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>

                                        <asp:Literal ID="litcostcenter_no" runat="server" Text='<%# Eval("costcenter_no") %>' />
                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>

                                        <asp:Literal ID="litempcode" runat="server" Text='<%# Eval("emp_code") %>' />

                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="ชื่อ-สกุล (ผู้ถูกประเมิน)" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                        <asp:Literal ID="litcemp_idx" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                    </ItemTemplate>

                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="คะแนนประเมินตนเอง Core Value" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <asp:Literal ID="litsumminecore" runat="server" Text='<%# Eval("sum_mine_core_value") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="คะแนนประเมินตนเอง Competency" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <asp:Literal ID="litsumminecom" runat="server" Text='<%# Eval("sum_mine_competency") %>' />

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ชื่อ-สกุล (ผู้มีสิทธิ์ประเมิน)" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("approve1") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="วันที่ดำเนินการเสร็จสิ้น" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <asp:Literal ID="litupdate_date" runat="server" Text='<%# Eval("update_date") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="คะแนนผู้มีสิทธิ์ประเมิน Core Value" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>

                                        <asp:Literal ID="litsumheadcore" runat="server" Text='<%# Eval("sum_head_core_value") %>' />

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="คะแนนผู้มีสิทธิ์ประเมิน Competency" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>

                                        <asp:Literal ID="litsumheadcom" runat="server" Text='<%# Eval("sum_head_competency") %>' />

                                    </ItemTemplate>
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-Width="8%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatusDoc" runat="server" Text='<%# Eval("StatusDoc") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <asp:GridView ID="GvReport_All" runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                            HeaderStyle-CssClass="info"
                            HeaderStyle-Height="40px"
                            AllowPaging="true"
                            PageSize="10"
                            OnPageIndexChanging="Master_PageIndexChanging"
                            OnRowDataBound="Master_RowDataBound"
                            DataKeyNames="rdept_idx">

                            <PagerStyle CssClass="pageCustom" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>

                                <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>
                                        <asp:Label ID="lblrdept_idx" runat="server" Visible="false" Text='<%# Eval("rdept_idx") %>'></asp:Label>
                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                        </p>
                                    </ItemTemplate>

                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="" ItemStyle-Width="80%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Medium">
                                    <ItemTemplate>

                                        <asp:GridView ID="GvDetailSec_Report" runat="server"
                                            AutoGenerateColumns="false"
                                            CssClass="table table-striped table-bordered  table-responsive col-lg-12"
                                            HeaderStyle-CssClass="default"
                                            HeaderStyle-Height="40px"
                                            AllowPaging="false"
                                            ShowFooter="true"
                                            GridLines="Both"
                                            OnRowDataBound="Master_RowDataBound"
                                            DataKeyNames="rsec_idx">

                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>


                                                <asp:TemplateField HeaderText="แผนก" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="left" ItemStyle-Font-Size="Medium">
                                                    <ItemTemplate>
                                                        <%-- <strong>
                                                    <asp:Label ID="Labesl15" runat="server">แผนก: </asp:Label></strong>--%>
                                                        <asp:Label ID="lblrsec_idx" runat="server" Visible="false" Text='<%# Eval("rsec_idx") %>'></asp:Label>
                                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                                        <%-- </p>--%>
                                                        <%--  <strong>&nbsp;&nbsp;
                                                     <asp:Label ID="Label9" runat="server">จำนวน: </asp:Label></strong>
                                                <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("qty") %>' /> &nbsp;&nbsp;
                                                   <strong>
                                                     <asp:Label ID="Label10" runat="server">คน </asp:Label></strong>--%>
                                                        <%--</p>--%>
                                                    </ItemTemplate>

                                                    <FooterTemplate>
                                                        <div style="text-align: right;">
                                                            <asp:Literal ID="litqty" runat="server" Text="รวม :" />

                                                        </div>
                                                    </FooterTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="จำนวนคนทั้งหมด (คน)" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("qty") %>' />
                                                        &nbsp;&nbsp;
                                                    </ItemTemplate>

                                                    <FooterTemplate>
                                                        <div style="text-align: center; background-color: gold;">
                                                            <asp:Label ID="lit_total_qty" runat="server" Font-Bold="true"></asp:Label>
                                                            <%--<p>
                                                        <asp:Label ID="lit_total_waitpower" runat="server" Font-Bold="true"></asp:Label>
                                                    </p>
                                                    <p>
                                                        <asp:Label ID="lit_total_uncomplete" runat="server" Font-Bold="true"></asp:Label>
                                                    </p>
                                                    <p>
                                                        <asp:Label ID="lit_total_complete" runat="server" Font-Bold="true"></asp:Label>
                                                    </p>--%>
                                                        </div>
                                                    </FooterTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="จบการดำเนินการ (คน)" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="litcomplete" runat="server" Text='<%# Eval("complete") %>' />
                                                        &nbsp;&nbsp;
                                                    </ItemTemplate>

                                                    <FooterTemplate>
                                                        <div style="text-align: center; background-color: gold;">
                                                            <asp:Label ID="lit_total_complete" runat="server" Font-Bold="true"></asp:Label>

                                                        </div>
                                                    </FooterTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="กำลังดำเนินการ (คน)" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                    <ItemTemplate>
                                                        <asp:Literal ID="lituncomplete" runat="server" Text='<%# Eval("uncomplete") %>' />
                                                        &nbsp;&nbsp;
                                                    </ItemTemplate>

                                                    <FooterTemplate>
                                                        <div style="text-align: center; background-color: gold;">
                                                            <asp:Label ID="lit_total_uncomplete" runat="server" Font-Bold="true"></asp:Label>

                                                        </div>
                                                    </FooterTemplate>
                                                </asp:TemplateField>


                                            </Columns>
                                        </asp:GridView>
                                    </ItemTemplate>
                                </asp:TemplateField>



                            </Columns>
                        </asp:GridView>

                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnsearch_report" />
                    <asp:PostBackTrigger ControlID="btnexport" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:View>
    </asp:MultiView>


</asp:Content>

