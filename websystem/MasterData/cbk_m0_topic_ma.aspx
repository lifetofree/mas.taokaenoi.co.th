﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="cbk_m0_topic_ma.aspx.cs" Inherits="websystem_MasterData_cbk_m0_topic_ma" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litdebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvSystem" runat="server" ActiveViewIndex="0">
        <asp:View ID="ViewIndex" runat="server">

            <div class="form-group">
                <asp:LinkButton ID="btn_AddTopicMA" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่มหัวข้อ MA" data-toggle="tooltip" title="เพิ่มหัวข้อ MA" runat="server"
                    CommandName="cmdAddTopicMA" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มหัวข้อ MA</asp:LinkButton>
            </div>

            <asp:FormView ID="FvInsert" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มรายละเอียดหัวข้อ MA</h3>
                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">

                                <div class="form-horizontal" role="form">

                                    <div class="panel-heading">

                                        <div class="form-group">
                                            <asp:Label ID="lbltopic_name" runat="server" Text="ชื่อหัวข้อ MA" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_topic_name" runat="server" CssClass="form-control" placeholder="กรอกชื่อหัวข้อ MA ..." Enabled="true" />

                                                <asp:RequiredFieldValidator ID="Re_txt_topic_name"
                                                    runat="server"
                                                    ControlToValidate="txt_topic_name"
                                                    Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อหัวข้อ MA"
                                                    ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_pname" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_topic_name" Width="220" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <%--<div class="form-group">
                                            <asp:Label ID="lbl_detailtopic_name" runat="server" Text="รายละเอียดเพิ่มเติม" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_detailtopic_name" runat="server" CssClass="form-control" TextMode="multiline" Rows="3" placeholder="รายละเอียดเพิ่มเติม ..." Enabled="true" />

                                               
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>--%>

                                        <%-- <div class="form-group">
                                            <asp:Label ID="lblplace_code" runat="server" Text="ชื่อสถานที่ย่อ(en)" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtplace_code" runat="server" CssClass="form-control" placeholder="กรอกชื่อสถานที่ย่อ(en) ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="Re_place_code" runat="server"
                                                    ControlToValidate="txtplace_code" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อสถานที่ย่อ(en)" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_pcode" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_place_code" Width="220" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>--%>

                                        <div class="form-group">
                                            <asp:Label ID="lbl_status_topic" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddlStatusTopic" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="btnSave" ValidationGroup="Save" CssClass="btn btn-success" Text="Save" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="CmdSave" OnCommand="btnCommand"></asp:LinkButton>
                                                <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <%--select Place--%>
            <asp:GridView ID="GvDetail" runat="server" Visible="true"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="lbl_topic_ma_idx" runat="server" Visible="false" Text='<%# Eval("topic_ma_idx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>

                        </ItemTemplate>
                        <EditItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_topic_ma_idx_edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("topic_ma_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbltopic_ma_name_edit" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อหัวข้อ MA" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_topic_ma_name_edit" runat="server" CssClass="form-control " Text='<%# Eval("topic_ma_name") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Re_txt_topic_ma_name_edit" runat="server"
                                                ControlToValidate="txt_topic_ma_name_edit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อหัวข้อ MA" ValidationGroup="Editpname" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_pname" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_topic_ma_name_edit" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>

                                    <%--<div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อหัวข้อ MA" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_detail_ma_name_edit" runat="server" CssClass="form-control " TextMode="multiline" Rows="3" placeholder="กรอกรายละเอียดเพิ่มเติม ..." Text='<%# Eval("detail_ma_name") %>'></asp:TextBox>
                                            
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>--%>

                                    <div class="form-group">
                                        <asp:Label ID="lbl_topic_ma_status_edit" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddltopic_ma_status_edit" Text='<%# Eval("topic_ma_status") %>'
                                                CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editpname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อหัวข้อ MA" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_topic_ma_name_detail" runat="server"
                                        Text='<%# Eval("topic_ma_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--<asp:TemplateField HeaderText="รายละเอียดเพิ่มเติม" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_detail_ma_name_detail" runat="server"
                                        Text='<%# Eval("detail_ma_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>--%>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">

                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_topic_ma_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("topic_ma_status") %>'></asp:Label>
                                    <asp:Label ID="topic_ma_statusOnline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="topic_ma_statusOffline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="cmdDelete"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบสถานที่นี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("topic_ma_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
    </asp:MultiView>


</asp:Content>
