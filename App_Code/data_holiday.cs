﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_roombooking
/// </summary>
/// 
[Serializable]
[XmlRoot("data_holiday")]
public class data_holiday
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("emps_u0_holiday_list")]
    public emps_u0_holiday_detail[] emps_u0_holiday_list { get; set; }

    [XmlElement("emps_u0_holiday_manage_list")]
    public emps_u0_holiday_manage_detail[] emps_u0_holiday_manage_list { get; set; }

    [XmlElement("emps_u1_holiday_manage_list")]
    public emps_u1_holiday_manage_detail[] emps_u1_holiday_manage_list { get; set; }
}


[Serializable]
public class emps_u0_holiday_detail
{
    [XmlElement("holiday_idx")]
    public int holiday_idx { get; set; }

    [XmlElement("date_idx")]
    public int date_idx { get; set; }

    [XmlElement("holiday_name")]
    public string holiday_name { get; set; }

    [XmlElement("holiday_date")]
    public string holiday_date { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("holiday_status")]
    public int holiday_status { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("year_idx")]
    public int year_idx { get; set; }

    [XmlElement("year_holiday")]
    public string year_holiday { get; set; }

}

[Serializable]
public class emps_u0_holiday_manage_detail
{
    [XmlElement("u0_manage_idx")]
    public int u0_manage_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("u0_manage_status")]
    public int u0_manage_status { get; set; }

    [XmlElement("cemp_idx_update")]
    public int cemp_idx_update { get; set; }

    [XmlElement("holiday_idx")]
    public int holiday_idx { get; set; }

    [XmlElement("date_idx")]
    public int date_idx { get; set; }

    [XmlElement("holiday_name")]
    public string holiday_name { get; set; }

    [XmlElement("holiday_date")]
    public string holiday_date { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("holiday_status")]
    public int holiday_status { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("year_idx")]
    public int year_idx { get; set; }

    [XmlElement("year_holiday")]
    public string year_holiday { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

}

[Serializable]
public class emps_u1_holiday_manage_detail
{
    [XmlElement("u0_manage_idx")]
    public int u0_manage_idx { get; set; }

    [XmlElement("u1_manage_idx")]
    public int u1_manage_idx { get; set; }

    [XmlElement("u1_manage_status")]
    public int u1_manage_status { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("cemp_idx_update")]
    public int cemp_idx_update { get; set; }

    [XmlElement("u0_manage_status")]
    public int u0_manage_status { get; set; }

    [XmlElement("holiday_idx")]
    public int holiday_idx { get; set; }

    [XmlElement("date_idx")]
    public int date_idx { get; set; }

    [XmlElement("holiday_name")]
    public string holiday_name { get; set; }

    [XmlElement("holiday_date")]
    public string holiday_date { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("holiday_status")]
    public int holiday_status { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("year_idx")]
    public int year_idx { get; set; }

    [XmlElement("year_holiday")]
    public string year_holiday { get; set; }

}



