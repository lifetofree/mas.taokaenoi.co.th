﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Data.SqlClient;

using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Helpers;

using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data;
using System.Text.RegularExpressions;
using DotNet.Highcharts.Enums;

public partial class websystem_ITServices_ReportCHR : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();
    DataSupportIT _dtsupport = new DataSupportIT();
    data_chr _dtchr = new data_chr();
    data_employee _dtEmployee = new data_employee();
    data_employee _dataEmployee = new data_employee();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _ret_val = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlSelect_chrddlLV1 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_chrddlLV1"];
    static string urlGetAdminSAP = _serviceUrl + ConfigurationManager.AppSettings["urlGetAdminSAP"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];

    static string urlSelect_StatusList = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_StatusList"];
    static string urlSelect_TypeClose1 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeClose1"];
    static string urlSelect_TypeClose2 = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeClose2"];
    static string urlSelect_TypeChr = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeChr"];
    static string urlSelect_System = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_System"];
    static string urlSelect_List = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_List_CHR"];
    static string urlSelect_ExportCHR = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ExportCHR"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    string url = "http://www.taokaenoi.co.th/MAS/CHR";



    #endregion

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            getOrganizationList(ddlorgidx);
            Select_ddlLV1();
            Select_AdminAccept();

            Select_Status();
            Select_ddlLV2();
            Select_ddlLV3();
            Select_TypeCHR();
            select_empIdx_present();
            GenerateddlYear(ddlyear);
        }
        linkBtnTrigger(lbindex);
        linkBtnTrigger(lbladd);
        linkBtnTrigger(lblcancel);
        linkBtnTrigger(lblapprove);
        linkBtnTrigger(lblmandays);
        linkBtnTrigger(lbladmin);

        lblreport.BackColor = System.Drawing.Color.LightGray;
        ////Select_System();
    }

    #endregion

    #region CallService

    protected DataSupportIT callServicePostITRepair(string _cmdUrl, DataSupportIT _dtsupport)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtsupport = (DataSupportIT)_funcTool.convertJsonToObject(typeof(DataSupportIT), _localJson);




        return _dtsupport;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }

    protected data_chr callServicePostCHR(string _cmdUrl, data_chr _dtchr)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtchr);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtchr = (data_chr)_funcTool.convertJsonToObject(typeof(data_chr), _localJson);




        return _dtchr;
    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }


    #endregion

    #region Select


    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

    }

    protected void Select_Status()
    {

        ddlSearchStatus.Items.Clear();
        ddlSearchStatus.AppendDataBoundItems = true;
        ddlSearchStatus.Items.Add(new ListItem("เลือกขั้นตอนดำเนินการ....", "0"));

        _dtchr = new data_chr();

        _dtchr.BoxCHRList = new CHRList[1];
        CHRList system = new CHRList();

        _dtchr.BoxCHRList[0] = system;

        _dtchr = callServicePostCHR(urlSelect_StatusList, _dtchr);

        ddlSearchStatus.DataSource = _dtchr.BoxCHRList;
        ddlSearchStatus.DataTextField = "node_desc";
        ddlSearchStatus.DataValueField = "noidx";
        ddlSearchStatus.DataBind();

    }

    protected void Select_ddlLV2()
    {

        ddlSAPLV2.Items.Clear();
        ddlSAPLV2.AppendDataBoundItems = true;
        ddlSAPLV2.Items.Add(new ListItem("เลือกเคสปิดงาน LV2....", "0"));

        _dtchr = new data_chr();

        _dtchr.BoxCHRList = new CHRList[1];
        CHRList system = new CHRList();

        _dtchr.BoxCHRList[0] = system;

        _dtchr = callServicePostCHR(urlSelect_TypeClose1, _dtchr);

        ddlSAPLV2.DataSource = _dtchr.BoxCHRList;
        ddlSAPLV2.DataTextField = "TypeClose1_name";
        ddlSAPLV2.DataValueField = "TC1IDX";
        ddlSAPLV2.DataBind();

    }

    protected void Select_TypeCHR()
    {

        ddlchr.Items.Clear();
        ddlchr.AppendDataBoundItems = true;
        ddlchr.Items.Add(new ListItem("เลือกประเภทรายการ....", "0"));

        _dtchr = new data_chr();

        _dtchr.BoxCHRList = new CHRList[1];
        CHRList system = new CHRList();

        _dtchr.BoxCHRList[0] = system;

        _dtchr = callServicePostCHR(urlSelect_TypeChr, _dtchr);

        ddlchr.DataSource = _dtchr.BoxCHRList;
        ddlchr.DataTextField = "Typename";
        ddlchr.DataValueField = "MTIDX";
        ddlchr.DataBind();

    }

    protected void Select_ddlLV3()
    {

        ddlSAPLV3.Items.Clear();
        ddlSAPLV3.AppendDataBoundItems = true;
        ddlSAPLV3.Items.Add(new ListItem("เลือกเคสปิดงาน LV3....", "0"));

        _dtchr = new data_chr();

        _dtchr.BoxCHRList = new CHRList[1];
        CHRList system = new CHRList();

        _dtchr.BoxCHRList[0] = system;

        _dtchr = callServicePostCHR(urlSelect_TypeClose2, _dtchr);

        ddlSAPLV3.DataSource = _dtchr.BoxCHRList;
        ddlSAPLV3.DataTextField = "TypeClose2_name";
        ddlSAPLV3.DataValueField = "TC2IDX";
        ddlSAPLV3.DataBind();

    }

    protected void Select_ddlLV1()
    {
        var ddlSAPLV1 = (DropDownList)ViewReport.FindControl("ddlSAPLV1");

        ddlSAPLV1.Items.Clear();
        ddlSAPLV1.AppendDataBoundItems = true;
        ddlSAPLV1.Items.Add(new ListItem("เลือกเคสปิดงาน LV1 ....", "0"));

        //_dtsupport = new DataSupportIT();
        //_dtsupport.BoxUserRequest = new UserRequestList[1];
        //UserRequestList ddllv1 = new UserRequestList();

        //_dtsupport.BoxUserRequest[0] = ddllv1;

        //_dtsupport = callServicePostITRepair(urlSelect_chrddlLV1, _dtsupport);


        //ddlSAPLV1.DataSource = _dtsupport.BoxUserRequest;
        //ddlSAPLV1.DataTextField = "Name_Code1";
        //ddlSAPLV1.DataValueField = "MS1IDX";
        //ddlSAPLV1.DataBind();

        _dtchr = new data_chr();
        _dtchr.BoxCHRList = new CHRList[1];
        CHRList ddllv1 = new CHRList();

        _dtchr.BoxCHRList[0] = ddllv1;

        _dtchr = callServicePostCHR(urlSelect_chrddlLV1, _dtchr);


        ddlSAPLV1.DataSource = _dtchr.BoxCHRList;
        ddlSAPLV1.DataTextField = "Name_Code1";
        ddlSAPLV1.DataValueField = "MS1IDX";
        ddlSAPLV1.DataBind();


    }

    protected void Select_AdminAccept()
    {
        ddlaccept.Items.Clear();
        ddlaccept.AppendDataBoundItems = true;
        ddlaccept.Items.Add(new ListItem("เลือกเจ้าหน้าที่...", "0"));


        ddlacceptsap.Items.Clear();
        ddlacceptsap.AppendDataBoundItems = true;
        ddlacceptsap.Items.Add(new ListItem("เลือกเจ้าหน้าที่...", "0"));

        ddlacceptsap1.Items.Clear();
        ddlacceptsap1.AppendDataBoundItems = true;
        ddlacceptsap1.Items.Add(new ListItem("เลือกเจ้าหน้าที่...", "0"));


        ddladminaccept.Items.Clear();
        ddladminaccept.AppendDataBoundItems = true;
        ddladminaccept.Items.Add(new ListItem("เลือกเจ้าหน้าที่...", "0"));

        ddladminaccept1.Items.Clear();
        ddladminaccept1.AppendDataBoundItems = true;
        ddladminaccept1.Items.Add(new ListItem("เลือกเจ้าหน้าที่...", "0"));

        _dtEmployee = new data_employee();

        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail organite = new employee_detail();

        _dtEmployee.employee_list[0] = organite;

        _dtEmployee = callServicePostEmp(urlGetAdminSAP, _dtEmployee);

        ddlaccept.DataSource = _dtEmployee.employee_list;
        ddlaccept.DataTextField = "emp_fullname_th";
        ddlaccept.DataValueField = "emp_idx";
        ddlaccept.DataBind();

        ddlacceptsap.DataSource = _dtEmployee.employee_list;
        ddlacceptsap.DataTextField = "emp_fullname_th";
        ddlacceptsap.DataValueField = "emp_idx";
        ddlacceptsap.DataBind();

        ddlacceptsap1.DataSource = _dtEmployee.employee_list;
        ddlacceptsap1.DataTextField = "emp_fullname_th";
        ddlacceptsap1.DataValueField = "emp_idx";
        ddlacceptsap1.DataBind();

        ddladminaccept.DataSource = _dtEmployee.employee_list;
        ddladminaccept.DataTextField = "emp_fullname_th";
        ddladminaccept.DataValueField = "emp_idx";
        ddladminaccept.DataBind();

        ddladminaccept1.DataSource = _dtEmployee.employee_list;
        ddladminaccept1.DataTextField = "emp_fullname_th";
        ddladminaccept1.DataValueField = "emp_idx";
        ddladminaccept1.DataBind();

    }

    protected void SelectMaster_List()
    {
        _dtchr = new data_chr();

        _dtchr.BoxCHRList = new CHRList[1];
        CHRList system = new CHRList();

        //system.Sysidx = int.Parse(ddlsystem.SelectedValue);
        system.MTIDX = int.Parse(ddlchr.SelectedValue);
        system.noidx = int.Parse(ddlSearchStatus.SelectedValue);
        system.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
        system.RDeptIDX = int.Parse(ddlrdeptidx.SelectedValue);
        system.MS1IDX = int.Parse(ddlSAPLV1.SelectedValue);
        system.TC1IDX = int.Parse(ddlSAPLV2.SelectedValue);
        system.TC2IDX = int.Parse(ddlSAPLV3.SelectedValue);
        system.AEmpIDX = int.Parse(ddlaccept.SelectedValue);
        system.AEmpIDX1 = int.Parse(ddlacceptsap.SelectedValue);
        system.AEmpIDX2 = int.Parse(ddlacceptsap1.SelectedValue);

        system.IFSearch = int.Parse(ddlTypeSearchDate.SelectedValue);
        system.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
        system.DategetJob = AddStartdate.Text;
        system.DateCloseJob = AddEndDate.Text;

        _dtchr.BoxCHRList[0] = system;


        _dtchr = callServicePostCHR(urlSelect_List, _dtchr);
        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtchr));
        setGridData(GvCHR, _dtchr.BoxCHRList);
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    public void Select_ChartLV1()
    {
        data_chr _dtchr = new data_chr();
        CHRList searchlv1 = new CHRList();
        _dtchr.BoxCHRList = new CHRList[1];


        searchlv1.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
        searchlv1.RDeptIDX = int.Parse(ddlrdeptidx.SelectedValue);
        searchlv1.IFSearch = int.Parse(ddlTypeSearchDate.SelectedValue);
        searchlv1.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
        searchlv1.DategetJob = AddStartdate.Text;
        searchlv1.DateCloseJob = AddEndDate.Text;
        searchlv1.AEmpIDX = int.Parse(ddladminaccept.SelectedValue);
        searchlv1.AEmpIDX1 = int.Parse(ddladminaccept1.SelectedValue);

        _dtchr.BoxCHRList[0] = searchlv1;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtchr));
        _local_xml = serviceexcute.actionExec("conn_mis", "data_chr", "service_CHR", _dtchr, 225);
        _dtchr = (data_chr)_funcTool.convertXmlToObject(typeof(data_chr), _local_xml);

        if (_dtchr.ReturnCode == "0")
        {
            var caseclose = new List<object>();

            foreach (var data in _dtchr.BoxCHRList)
            {
                caseclose.Add(new object[] { data.Name.ToString(), data.countmodule1.ToString() });
            }

            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new Title { Text = "Case LV1 / Times" });
            chart.InitChart(new Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart.SetSeries(new Series[]
                 {
                        new Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });


            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }

    }

    public void Select_ChartLV2()
    {
        data_chr _dtchr = new data_chr();
        CHRList searchlv1 = new CHRList();
        _dtchr.BoxCHRList = new CHRList[1];


        searchlv1.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
        searchlv1.RDeptIDX = int.Parse(ddlrdeptidx.SelectedValue);
        searchlv1.IFSearch = int.Parse(ddlTypeSearchDate.SelectedValue);
        searchlv1.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
        searchlv1.DategetJob = AddStartdate.Text;
        searchlv1.DateCloseJob = AddEndDate.Text;
        searchlv1.AEmpIDX = int.Parse(ddladminaccept.SelectedValue);
        searchlv1.AEmpIDX1 = int.Parse(ddladminaccept1.SelectedValue);

        _dtchr.BoxCHRList[0] = searchlv1;


        _local_xml = serviceexcute.actionExec("conn_mis", "data_chr", "service_CHR", _dtchr, 220);
        _dtchr = (data_chr)_funcTool.convertXmlToObject(typeof(data_chr), _local_xml);

        if (_dtchr.ReturnCode == "0")
        {
            var caseclose = new List<object>();

            foreach (var data in _dtchr.BoxCHRList)
            {
                caseclose.Add(new object[] { data.TypeClose1_name.ToString(), data.countmodule1.ToString() });
            }

            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new Title { Text = "Case LV2 / Times" });
            chart.InitChart(new Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart.SetSeries(new Series[]
                 {
                        new Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });


            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }

    }

    public void Select_ChartSAPLV3()
    {
        data_chr _dtchr = new data_chr();
        CHRList searchlv1 = new CHRList();
        _dtchr.BoxCHRList = new CHRList[1];


        searchlv1.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
        searchlv1.RDeptIDX = int.Parse(ddlrdeptidx.SelectedValue);
        searchlv1.IFSearch = int.Parse(ddlTypeSearchDate.SelectedValue);
        searchlv1.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
        searchlv1.DategetJob = AddStartdate.Text;
        searchlv1.DateCloseJob = AddEndDate.Text;
        searchlv1.AEmpIDX = int.Parse(ddladminaccept.SelectedValue);
        _dtchr.BoxCHRList[0] = searchlv1;
        _local_xml = serviceexcute.actionExec("conn_mis", "data_chr", "service_CHR", _dtchr, 224);
        _dtchr = (data_chr)_funcTool.convertXmlToObject(typeof(data_chr), _local_xml);

        //   text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtchr));
        if (_dtchr.ReturnCode == "0")
        {
            int count = _dtchr.BoxCHRList.Length;
            string[] deptname = new string[count];
            string[] code = new string[count];
            object[] counttime = new object[count];
            int i = 0;
            var caseclose = new List<object>();
            var countclose = new List<object>();
            var dept = new List<object>();

            foreach (var data in _dtchr.BoxCHRList)
            {
                deptname[i] = data.Name.ToString();
                code[i] = data.TypeClose1_name.ToString();
                //counttime[i] = data.countmodule1.ToString();

                //dept.Add(new object[] { data.Name.ToString() });
                caseclose.Add(new object[] { data.TypeClose1_name.ToString() });
                countclose.Add(new object[] { data.countmodule1.ToString() });

                i++;
            }




            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new Title { Text = "" });
            chart.SetXAxis(new XAxis { Categories = deptname });// new[] { con_dep } });  //deptname.ToArray() });//new[] { "MIS", "CO" } }); // deptname });//
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "จำนวนครั้ง" } } });
            chart.InitChart(new Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Column });
            chart.SetPlotOptions(new PlotOptions
            {
                Column = new PlotOptionsColumn
                {
                    Stacking = DotNet.Highcharts.Enums.Stackings.Normal,
                    ShowInLegend = true,
                    //  AllowPointSelect = true,
                    DataLabels = new PlotOptionsColumnDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        //Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                        Formatter = "function() { return this.y.toFixed(0)+' '; }"
                    }
                }
            });

            //DataTable table = new DataTable();
            //table.Columns.Add("Name", typeof(string));
            //table.Columns.Add("Drug", typeof(int));
            //table.Columns.Add("Patient", typeof(int));
            ////table.Columns.Add("Date", typeof(DateTime));

            //// Here we add five DataRows.
            //table.Rows.Add("Indocin", 5, 15);
            //table.Rows.Add("Enebrel", 7, 22);

            //Series[] series = new Series[table.Columns.Count];
            //for (int j = 0; j <= table.Columns.Count - 1; j++)
            //{
            //    series[j] = new Series
            //    {
            //        Name = table.Columns[j].ColumnName.ToString()
            //    ,
            //        for (int k = 1; k < table.Rows.Count; k++)
            //    {
            //        Data = new Data(new object[] { dt.Columns[j]("Page Visits") })
            //    };
            //}



            chart.SetSeries(new[]//new Series[]//
                   {
                    new Series {  Data = new Data(countclose.ToArray()) },
                    new Series { Name = "Audio", Data = new Data(new object[] { 17 }) },
                    new Series { Name = "Video", Data = new Data(new object[] { 15 }) },
                    new Series { Name = "Peripheral", Data = new Data(new object[] { 6 }) }

            });

            litReportChart.Text = chart.ToHtmlString();

        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void Select_ChartSAPLV4()
    {
        data_chr _dtchr = new data_chr();
        CHRList searchlv1 = new CHRList();
        _dtchr.BoxCHRList = new CHRList[1];

        searchlv1.DC_Mount = ddlmonth.SelectedValue;
        searchlv1.DC_Year = ddlyear.SelectedValue;
        searchlv1.AEmpIDX = int.Parse(ddladminaccept.SelectedValue);
        searchlv1.AEmpIDX1 = int.Parse(ddladminaccept1.SelectedValue);

        _dtchr.BoxCHRList[0] = searchlv1;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtchr));

        _local_xml = serviceexcute.actionExec("conn_mis", "data_chr", "service_CHR", _dtchr, 228);
        _dtchr = (data_chr)_funcTool.convertXmlToObject(typeof(data_chr), _local_xml);

        setGridData(GvChartSAP, _dtchr.BoxCHRList);


        if (_dtchr.ReturnCode == "0")
        {
            int count = _dtchr.BoxCHRList.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;
            foreach (var data in _dtchr.BoxCHRList)
            {
                lv1code[i] = data.monthdowntime.ToString();
                lv1count[i] = data.sumtime.ToString();

                for (int ii = 0; ii < lv1code[i].Length; ii++)
                {
                    switch (lv1code[i])
                    {
                        case "1":
                            lv1code[i] = "มกราคม";

                            break;
                        case "2":
                            lv1code[i] = "กุมภาพันธ์";

                            break;
                        case "3":
                            lv1code[i] = "มีนาคม";

                            break;
                        case "4":
                            lv1code[i] = "เมษายน";

                            break;
                        case "5":
                            lv1code[i] = "พฤษภาคม";

                            break;
                        case "6":
                            lv1code[i] = "มิถุนายน";

                            break;
                        case "7":
                            lv1code[i] = "กรกฎาคม";

                            break;
                        case "8":
                            lv1code[i] = "สิงหาคม";

                            break;
                        case "9":
                            lv1code[i] = "กันยายน";

                            break;
                        case "10":
                            lv1code[i] = "ตุลาคม";

                            break;
                        case "11":
                            lv1code[i] = "พฤศจิกายน";

                            break;
                        case "12":
                            lv1code[i] = "ธันวาคม";

                            break;
                    }
                }


                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });

            chart.SetSeries(new[]
            {
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Month / " + ddlyear.SelectedValue,
                    Data = new Data(lv1count)
                }
            }

            );
            litReportChart.Text = chart.ToHtmlString();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }

    }

    #endregion

    #region reuse

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //  convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกองค์กร....", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกฝ่าย....", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกแผนก....", "0"));
    }


    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void GenerateddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 5; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }

        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }

    #endregion reuse

    #region Gridview

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvChartSAP":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (GvChartSAP.EditIndex != e.Row.RowIndex) //to overlook header row
                {

                    var lit_month = ((Literal)e.Row.FindControl("lit_month"));

                    if (lit_month.Text == "1")
                    {
                        lit_month.Text = "มกราคม";
                    }
                    else if (lit_month.Text == "2")
                    {
                        lit_month.Text = "กุมภาพันธ์";
                    }
                    else if (lit_month.Text == "3")
                    {
                        lit_month.Text = "มีนาคม";
                    }
                    else if (lit_month.Text == "4")
                    {
                        lit_month.Text = "เมษายน";
                    }
                    else if (lit_month.Text == "5")
                    {
                        lit_month.Text = "พฤกษภาคม";
                    }
                    else if (lit_month.Text == "6")
                    {
                        lit_month.Text = "มิถุนายน";
                    }
                    else if (lit_month.Text == "7")
                    {
                        lit_month.Text = "กรกฎาคม";
                    }
                    else if (lit_month.Text == "8")
                    {
                        lit_month.Text = "สิงหาคม";
                    }
                    else if (lit_month.Text == "9")
                    {
                        lit_month.Text = "กันยายน";
                    }
                    else if (lit_month.Text == "10")
                    {
                        lit_month.Text = "ตุลาคม";
                    }
                    else if (lit_month.Text == "11")
                    {
                        lit_month.Text = "พฤศจิกายน";
                    }
                    else if (lit_month.Text == "12")
                    {
                        lit_month.Text = "ธันวาคม";
                    }


                }
                break;
        }
    }
    #endregion

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvCHR":

                GvCHR.PageIndex = e.NewPageIndex;
                GvCHR.DataBind();

                SelectMaster_List();


                break;

        }
    }

    #endregion

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        switch (ddName.ID)
        {
            case "ddltypereport":

                switch (ddltypereport.SelectedValue)
                {
                    case "0":
                        panel_table.Visible = false;
                        panel_chart.Visible = false;
                        div_button.Visible = false;
                        div_checkdate.Visible = true;
                        div_chart.Visible = false;
                        gvchart_priority.Visible = false;
                        break;

                    case "1":
                        panel_table.Visible = true;
                        panel_chart.Visible = false;
                        div_chart.Visible = false;
                        div_button.Visible = true;
                        btnexport.Visible = true;
                        grid1.Visible = false;
                        grant.Visible = false;
                        div_checkdate.Visible = true;
                        gvchart_priority.Visible = false;
                        break;


                    case "2":

                        panel_table.Visible = false;
                        panel_chart.Visible = true;
                        div_chart.Visible = true;
                        div_button.Visible = true;
                        btnexport.Visible = false;
                        grid1.Visible = false;
                        grant.Visible = false;
                        div_checkdate.Visible = false;
                        gvchart_priority.Visible = false;

                        break;
                }

                break;

            case "ddlorgidx":

                getDepartmentList(ddlrdeptidx, int.Parse(ddlorgidx.SelectedValue));
                break;


            case "ddlrdeptidx":
                getSectionList(ddlrsecidx, int.Parse(ddlorgidx.SelectedValue), int.Parse(ddlrdeptidx.SelectedValue));

                break;


            case "ddlTypeSearchDate":

                AddStartdate.Text = string.Empty;
                AddEndDate.Text = string.Empty;

                ddlSearchDate.AppendDataBoundItems = true;
                ddlSearchDate.Items.Clear();
                ddlSearchDate.Items.Add(new ListItem("เลือกเงื่อนไข ....", "00"));
                ddlSearchDate.Items.Add(new ListItem("มากกว่า >", "1"));
                ddlSearchDate.Items.Add(new ListItem("น้อยกว่า <", "2"));
                ddlSearchDate.Items.Add(new ListItem("ระหว่าง <>", "3"));
                AddEndDate.Enabled = false;

                break;

            case "ddlSearchDate":

                if (int.Parse(ddlSearchDate.SelectedValue) == 3)
                {
                    AddEndDate.Enabled = true;
                }
                else
                {
                    AddEndDate.Enabled = false;
                    AddEndDate.Text = string.Empty;
                }

                break;

            case "ddlchartsap":

                if (ddlchartsap.SelectedValue == "4")
                {
                    div_organite.Visible = false;
                    div_checkdate.Visible = false;
                    panel_searchdate.Visible = false;
                    div_month.Visible = true;
                }
                else
                {
                    div_organite.Visible = true;
                    div_checkdate.Visible = true;
                    panel_searchdate.Visible = true;
                    div_month.Visible = false;
                }

                break;

        }
    }
    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "btnIndex":

                //  Response.Write("<script>window.open('http://www.taokaenoi.co.th/MAS/CHR','_blank');</script>");
                string emp = ViewState["EmpIDX"].ToString();
                string rdept = ViewState["rdept_idx"].ToString();


                HttpResponse response = HttpContext.Current.Response;
                response.Clear();

                StringBuilder s = new StringBuilder();
                s.Append("<html>");
                s.AppendFormat("<body onload='document.forms[\"form\"].submit()'>");
                s.AppendFormat("<form name='form' action='{0}' method='post'>", url);
                s.Append("<input type='hidden' name='emp_idx' value=" + emp + " />");
                s.Append("<input type='hidden' name='rdept_idx' value=" + rdept + " />");

                s.Append("</form></body></html>");
                response.Write(s.ToString());
                response.End();


                break;

            case "btnreport":
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lblreport.BackColor = System.Drawing.Color.LightGray;
                break;

            case "btnsearch":

                switch (ddltypereport.SelectedValue)
                {
                    case "1":

                        grant.Visible = false;
                        grid1.Visible = true;
                        gvchart_priority.Visible = false;
                        SelectMaster_List();
                        break;

                    case "2":
                        grant.Visible = true;
                        grid1.Visible = false;
                        gvchart_priority.Visible = false;
                        if (ddlchartsap.SelectedValue == "1")
                        {
                            Select_ChartLV1();
                        }
                        else if (ddlchartsap.SelectedValue == "2")
                        {
                            Select_ChartLV2();
                        }
                        else if (ddlchartsap.SelectedValue == "4")
                        {
                            grant.Visible = true;
                            grid1.Visible = false;
                            Select_ChartSAPLV4();
                            gvchart_priority.Visible = true;
                        }
                        else if (ddlchartsap.SelectedValue == "3")
                        {
                            //Select_ChartSAPLV3();
                            if (ddlorgidx.SelectedValue == "0")
                            {
                                Session["ORGIDX"] = "0";
                            }
                            else
                            {
                                Session["ORGIDX"] = ddlorgidx.SelectedValue.ToString();
                            }

                            if (ddlrdeptidx.SelectedValue == "0")
                            {
                                Session["RDeptIDX"] = "0";
                            }
                            else
                            {
                                Session["RDeptIDX"] = ddlrdeptidx.SelectedValue.ToString();
                            }

                            if (ddlTypeSearchDate.SelectedValue == "0")
                            {
                                Session["IFSearch"] = "0";
                            }
                            else
                            {
                                Session["IFSearch"] = ddlTypeSearchDate.SelectedValue.ToString();
                            }

                            if (ddlSearchDate.SelectedValue == "0")
                            {
                                Session["IFSearchbetween"] = "0";
                            }
                            else
                            {
                                Session["IFSearchbetween"] = ddlSearchDate.SelectedValue.ToString();
                            }

                            if (AddStartdate.Text == "")
                            {
                                Session["AddStartdate"] = "";
                            }
                            else
                            {
                                Session["AddStartdate"] = AddStartdate.Text;
                            }

                            if (AddEndDate.Text == "")
                            {
                                Session["DateCloseJob"] = "";
                            }
                            else
                            {
                                Session["DateCloseJob"] = AddEndDate.Text;
                            }

                            if (ddladminaccept.SelectedValue == "0")
                            {
                                Session["AEmpIDX"] = "0";
                            }
                            else
                            {
                                Session["AEmpIDX"] = ddladminaccept.SelectedValue.ToString();
                            }


                            if (ddladminaccept1.SelectedValue == "0")
                            {
                                Session["AEmpIDX1"] = "0";
                            }
                            else
                            {
                                Session["AEmpIDX1"] = ddladminaccept1.SelectedValue.ToString();
                            }



                            Session["System"] = "CHR";
                            // Response.Write("<script>window.open('http://localhost/mas.taokaenoi.co.th/websystem/ITServices/stackedcolumn.aspx','_blank');</script>");
                            Response.Write("<script>window.open('http://mas.taokaenoi.co.th/stackedcolumn','_blank');</script>");

                        }


                        break;
                }

                break;



            case "btnexport":
                _dtchr = new data_chr();
                _dtchr.BoxCHRList = new CHRList[1];
                CHRList system = new CHRList();

                //system.Sysidx = int.Parse(ddlsystem.SelectedValue);
                system.MTIDX = int.Parse(ddlchr.SelectedValue);
                system.noidx = int.Parse(ddlSearchStatus.SelectedValue);
                system.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
                system.RDeptIDX = int.Parse(ddlrdeptidx.SelectedValue);
                system.MS1IDX = int.Parse(ddlSAPLV1.SelectedValue);
                system.TC1IDX = int.Parse(ddlSAPLV2.SelectedValue);
                system.TC2IDX = int.Parse(ddlSAPLV3.SelectedValue);
                system.AEmpIDX = int.Parse(ddlacceptsap.SelectedValue);
                system.IFSearch = int.Parse(ddlTypeSearchDate.SelectedValue);
                system.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
                system.DategetJob = AddStartdate.Text;
                system.DateCloseJob = AddEndDate.Text;

                _dtchr.BoxCHRList[0] = system;

                //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtchr));

                _dtchr = callServicePostCHR(urlSelect_ExportCHR, _dtchr);
                GridView2.DataSource = _dtchr.Export_BoxCHR;
                GridView2.DataBind();

                GridView2.AllowSorting = false;
                GridView2.AllowPaging = false;

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");


                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);


                GridView2.Columns[0].Visible = true;
                GridView2.HeaderRow.BackColor = Color.White;

                foreach (TableCell cell in GridView2.HeaderRow.Cells)
                {
                    cell.BackColor = GridView2.HeaderStyle.BackColor;
                }

                foreach (GridViewRow row in GridView2.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = GridView2.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = GridView2.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                GridView2.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { mso-number-format:\@; font-family: myFirstFont; } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();

                break;

            case "btnrefresh":

                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

        }
    }

    #endregion

}