﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_ecom_m0_saleunit : System.Web.UI.Page
{

    #region Connect
    function_tool _funcTool = new function_tool();
    data_ecom_saleunit data_ecom_saleunit = new data_ecom_saleunit();
    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //-- employee --//
    static string _urlGetEcomm0SaleUnit = _serviceUrl + ConfigurationManager.AppSettings["urlGetEcomM0SaleUnit"];
    static string _urlSetEcomM0SaleUnit = _serviceUrl + ConfigurationManager.AppSettings["urlSetEcomM0SaleUnit"];
    static string _urlDelEcomM0SaleUnit = _serviceUrl + ConfigurationManager.AppSettings["urlDelEcomM0SaleUnit"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());

    }
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

            Select_Place();

        }
    }
    #endregion Page Load

    #region initPage
    protected void initPage()
    {
        MvSystem.SetActiveView(ViewIndex);

    }
    #endregion


    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_place":
                Gv_select_place.PageIndex = e.NewPageIndex;
                Gv_select_place.DataBind();
                Select_Place();
                break;

        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_place":
                Gv_select_place.EditIndex = e.NewEditIndex;
                Select_Place();
                break;

        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "Gv_select_place":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbpplace_status = (Label)e.Row.Cells[3].FindControl("lbpplace_status");
                    Label place_statusOnline = (Label)e.Row.Cells[3].FindControl("place_statusOnline");
                    Label place_statusOffline = (Label)e.Row.Cells[3].FindControl("place_statusOffline");

                    ViewState["_place_status"] = lbpplace_status.Text;


                    if (ViewState["_place_status"].ToString() == "1")
                    {
                        place_statusOnline.Visible = true;
                    }
                    else if (ViewState["_place_status"].ToString() == "0")
                    {
                        place_statusOffline.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {

                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                    //btn_addplace.Visible = true;
                    //setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);

                    //MultiView1.Visible = true;
                    //Gv_select_unit.Visible = false;
                    btn_addplace.Visible = true;
                    FvInsert.Visible = false;

                }



                break;


        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_place":
                Gv_select_place.EditIndex = -1;
                Select_Place();

                btn_addplace.Visible = true;
                FvInsert.Visible = false;
                break;

        }
    }


    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_place":

                TextBox txt_place_idx_edit = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("txt_un_idx_edit");
                TextBox unit_name = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("Name_place");
                TextBox Name_material = (TextBox)Gv_select_place.Rows[e.RowIndex].FindControl("Name_material");

                DropDownList un_status = (DropDownList)Gv_select_place.Rows[e.RowIndex].FindControl("ddEdit_place");


                data_ecom_saleunit = new data_ecom_saleunit(); ;
                ecom_m0_saleunit m0_detail = new ecom_m0_saleunit();
                data_ecom_saleunit.ecom_m0_saleunit_list = new ecom_m0_saleunit[1];

                m0_detail.un_idx = int.Parse(txt_place_idx_edit.Text);
                m0_detail.unit_name = unit_name.Text;
                m0_detail.material = Name_material.Text;
                m0_detail.un_status = int.Parse(un_status.SelectedValue);
                m0_detail.emp_idx_update = _emp_idx;
                data_ecom_saleunit.ecom_m0_saleunit_list[0] = m0_detail;
                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ecom_saleunit));
                data_ecom_saleunit = callServicePostSaleUnit(_urlSetEcomM0SaleUnit, data_ecom_saleunit);
                Gv_select_place.EditIndex = -1;

                if (data_ecom_saleunit.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);

                }
                Select_Place();
                btn_addplace.Visible = true;
                FvInsert.Visible = false;
                break;
        }
    }




    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {
            case "cmdAddUnit":
                btn_addplace.Visible = false;
                setFormData(FvInsert, FormViewMode.Insert, null);
                FvInsert.Visible = true;
                div_searchmaster_page.Visible = false;
                break;
            case "CmdCancel":
                btn_addplace.Visible = true;
                FvInsert.Visible = false;
                div_searchmaster_page.Visible = true;
                break;
            case "CmdSave":
                Insert_Place();
                Select_Place();

                break;
            case "btnRefreshMaster_Supplier":
                txtmaster_search.Text = String.Empty;
                Src_status.Text = "999";
                Select_Place();
                break;
            case "CmdSearchMaster_src":
                Select_Place();
                break;
            case "btnTodelete":
                int un_idx_del = int.Parse(cmdArg);

                data_ecom_saleunit = new data_ecom_saleunit(); ;
                ecom_m0_saleunit m0_detail = new ecom_m0_saleunit();
                data_ecom_saleunit.ecom_m0_saleunit_list = new ecom_m0_saleunit[1];

                m0_detail.un_idx = un_idx_del;
                data_ecom_saleunit.ecom_m0_saleunit_list[0] = m0_detail;
             
                data_ecom_saleunit = callServicePostSaleUnit(_urlDelEcomM0SaleUnit, data_ecom_saleunit);
                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_place_del));
                Select_Place();
                btn_addplace.Visible = true;
                FvInsert.Visible = false;
                break;
        }
    }

    protected void Select_Place()
    {
        //TextBox txtmaster_search = (TextBox)FvInsert.FindControl("txtmaster_search");
        //DropDownList dropD_status_place = (DropDownList)FvInsert.FindControl("Src_status");

        data_ecom_saleunit = new data_ecom_saleunit();
        ecom_m0_saleunit m0_detail = new ecom_m0_saleunit();

        data_ecom_saleunit.ecom_m0_saleunit_list = new ecom_m0_saleunit[1];

        m0_detail.material = txtmaster_search.Text;
        m0_detail.un_status = int.Parse(Src_status.SelectedValue);
        data_ecom_saleunit.ecom_m0_saleunit_list[0] = m0_detail;


        data_ecom_saleunit = callServicePostSaleUnit(_urlGetEcomm0SaleUnit, data_ecom_saleunit);
       // litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ecom_saleunit));
        setGridData(Gv_select_place, data_ecom_saleunit.ecom_m0_saleunit_list);
      
    }

    #endregion btnCommand

    protected void Insert_Place()
    {

        TextBox tex_name = (TextBox)FvInsert.FindControl("txtunit_name");
        TextBox txtmaterial = (TextBox)FvInsert.FindControl("txtmaterial");
        
        DropDownList dropD_status_place = (DropDownList)FvInsert.FindControl("ddPlace");
        //litdebug.Text = tex_place_name.Text + dropD_status_place.SelectedValue;

        data_ecom_saleunit = new data_ecom_saleunit();
        ecom_m0_saleunit m0_detail = new ecom_m0_saleunit();
        data_ecom_saleunit.ecom_m0_saleunit_list = new ecom_m0_saleunit[1];

        m0_detail.unit_name = tex_name.Text;
        m0_detail.material = txtmaterial.Text;
        m0_detail.un_status = int.Parse(dropD_status_place.SelectedValue);
        m0_detail.cemp_idx = _emp_idx;
  
        data_ecom_saleunit.ecom_m0_saleunit_list[0] = m0_detail;

        data_ecom_saleunit = callServicePostSaleUnit(_urlSetEcomM0SaleUnit, data_ecom_saleunit);
        // litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ecom_saleunit));
        if (data_ecom_saleunit.return_code == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ข้อมูลซ้ำ กรุณาแก้ไขใหม่อีกครั้ง!!!');", true);

        }
        else
        {
            btn_addplace.Visible = true;
            FvInsert.Visible = false;
            div_searchmaster_page.Visible = true;
        }



    }


    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata


    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion



    #region callService 

    //protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    //{
    //    // call services
    //    _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

    //    // convert json to object
    //    _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

    //    return _dataEmployee;
    //}

    protected data_ecom_saleunit callServicePostSaleUnit(string _cmdUrl, data_ecom_saleunit _data)
    {
        _localJson = _funcTool.convertObjectToJson(_data);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data = (data_ecom_saleunit)_funcTool.convertJsonToObject(typeof(data_ecom_saleunit), _localJson);


        return _data;
    }
    #endregion callService Functions
}