using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot ("data_tkn_checkin")]
public class data_tkn_checkin {
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("return_idx")]
    public int return_idx { get; set; }

    [XmlElement("checkin_mode")]
    public string checkin_mode { get; set; }

    [XmlElement("tkn_checkin_list")]
    public tkn_checkin_detail[] tkn_checkin_list { get; set; }
    [XmlElement("tkn_checkin_setting_list_m0")]
    public tkn_checkin_setting_detail_m0[] tkn_checkin_setting_list_m0 { get; set; }
    [XmlElement("search_tkn_checkin_list")]
    public search_tkn_checkin_detail[] search_tkn_checkin_list { get; set; }
}

[Serializable]
public class tkn_checkin_detail {
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("latitude")]
    public string latitude { get; set; }
    [XmlElement("longitude")]
    public string longitude { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("checkin_name")]
    public string checkin_name { get; set; }
    [XmlElement("street")]
    public string street { get; set; }
    [XmlElement("iso_country_code")]
    public string iso_country_code { get; set; }
    [XmlElement("country")]
    public string country { get; set; }
    [XmlElement("postal_code")]
    public string postal_code { get; set; }
    [XmlElement("admin_area")]
    public string admin_area { get; set; }
    [XmlElement("sub_admin_area")]
    public string sub_admin_area { get; set; }
    [XmlElement("locality")]
    public string locality { get; set; }
    [XmlElement("sub_locality")]
    public string sub_locality { get; set; }
    [XmlElement("thoroughfare")]
    public string thoroughfare { get; set; }
    [XmlElement("sub_thoroughfare")]
    public string sub_thoroughfare { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("emp_firstname_th")]
    public string emp_firstname_th { get; set; }

    [XmlElement("emp_lastname_th")]
    public string emp_lastname_th { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("emp_firstname_en")]
    public string emp_firstname_en { get; set; }

    [XmlElement("emp_lastname_en")]
    public string emp_lastname_en { get; set; }

    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }

    [XmlElement("aff_idx")]
    public int aff_idx { get; set; }

    [XmlElement("aff_name_th")]
    public string aff_name_th { get; set; }

    [XmlElement("aff_name_en")]
    public string aff_name_en { get; set; }

    [XmlElement("jobgrade_idx")]
    public int jobgrade_idx { get; set; }

    [XmlElement("jobgrade_name")]
    public string jobgrade_name { get; set; }

    [XmlElement("joblevel_idx")]
    public int joblevel_idx { get; set; }

    [XmlElement("joblevel_name")]
    public string joblevel_name { get; set; }

    [XmlElement("cost_idx")]
    public int cost_idx { get; set; }

    [XmlElement("cost_no")]
    public string cost_no { get; set; }

    [XmlElement("cost_name")]
    public string cost_name { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }

    [XmlElement("wg_idx")]
    public int wg_idx { get; set; }

    [XmlElement("wg_name_th")]
    public string wg_name_th { get; set; }

    [XmlElement("wg_name_en")]
    public string wg_name_en { get; set; }

    [XmlElement("lw_idx")]
    public int lw_idx { get; set; }

    [XmlElement("lw_name_th")]
    public string lw_name_th { get; set; }

    [XmlElement("lw_name_en")]
    public string lw_name_en { get; set; }

    [XmlElement("dept_idx")]
    public int dept_idx { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }

    [XmlElement("sec_idx")]
    public int sec_idx { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }

    [XmlElement("pos_idx")]
    public int pos_idx { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("pos_name_en")]
    public string pos_name_en { get; set; }

    [XmlElement("empgroup_idx")]
    public int empgroup_idx { get; set; }

    [XmlElement("empgroup_name_th")]
    public string empgroup_name_th { get; set; }

    [XmlElement("empgroup_name_en")]
    public string empgroup_name_en { get; set; }

    [XmlElement("emp_start_date")]
    public string emp_start_date { get; set; }

    [XmlElement("emp_resign_date")]
    public string emp_resign_date { get; set; }

    [XmlElement("emp_org_mail")]
    public string emp_org_mail { get; set; }
    [XmlElement("emp_personal_mail")]
    public string emp_personal_mail { get; set; }


    [XmlElement("emp_status")]
    public int emp_status { get; set; }
}

[Serializable]
public class tkn_checkin_setting_detail_m0 : tkn_checkin_detail {
    [XmlElement("m0_idx")]
    public int m0_idx { get; set; }
    [XmlElement("range")]
    public decimal range { get; set; }
    
    [XmlElement("province_idx")]
    public int province_idx { get; set; }
    [XmlElement("province")]
    public string province { get; set; }
    [XmlElement("amphure_idx")]
    public int amphure_idx { get; set; }
    [XmlElement("amphure")]
    public string amphure { get; set; }
    [XmlElement("district_idx")]
    public int district_idx { get; set; }
    [XmlElement("district")]
    public string district { get; set; }
    [XmlElement("places")]
    public string places { get; set; } 

    [XmlElement("status_zone")]
    public int status_zone { get; set; }
    [XmlElement("zone_name")]
    public string zone_name { get; set; }  

    [XmlElement("setting_status")]
    public int setting_status { get; set; }
    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement("create_date")]
    public string create_date { get; set; }
    [XmlElement("update_date")]
    public string update_date { get; set; }
}

[Serializable]
public class search_tkn_checkin_detail {
    [XmlElement("s_emp_idx")]
    public string s_emp_idx { get; set; }
    [XmlElement("s_emp_code")]
    public string s_emp_code { get; set; }
    [XmlElement("s_emp_name")]
    public string s_emp_name { get; set; }
    [XmlElement("s_org_idx")]
    public string s_org_idx { get; set; }
    [XmlElement("s_wg_idx")]
    public string s_wg_idx { get; set; }
    [XmlElement("s_lw_idx")]
    public string s_lw_idx { get; set; }
    [XmlElement("s_dept_idx")]
    public string s_dept_idx { get; set; }
    [XmlElement("s_sec_idx")]
    public string s_sec_idx { get; set; }
    [XmlElement("s_pos_idx")]
    public string s_pos_idx { get; set; }
    [XmlElement("s_emptype_idx")]
    public string s_emptype_idx { get; set; }
    [XmlElement("s_emp_status")]
    public string s_emp_status { get; set; }
    [XmlElement("s_start_date")]
    public string s_start_date { get; set; }
    [XmlElement("s_end_date")]
    public string s_end_date { get; set; }

    [XmlElement("s_postal_code")]
    public string s_postal_code { get; set; }
    [XmlElement("s_admin_area")]
    public string s_admin_area { get; set; }

    //setting
    [XmlElement("s_m0_idx")]
    public string s_m0_idx { get; set; }
}