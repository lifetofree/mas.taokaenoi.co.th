﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_rpt_trainingplan.aspx.cs" Inherits="websystem_el_rpt_trainingplan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litDebug" runat="server" Text=""></asp:Literal>
    <asp:MultiView ID="MultiViewBody" runat="server" ActiveViewIndex="0">
        <asp:View ID="View_plan_pm" runat="server">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

                    <asp:Panel ID="pnlsearch" runat="server">
                        <div class="panel panel-primary m-t-10">
                            <div class="panel-heading f-bold">ค้นหา</div>
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>ปี</label>
                                            <asp:DropDownList ID="ddlYearSearch_L" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                            <asp:TextBox ID="txtFilterKeyword_L" runat="server"
                                                CssClass="form-control"
                                                placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                                        </div>
                                    </div>
                                    <div class="col-sm-2" runat="server" visible="false">
                                        <div class="form-group">
                                            <label>สถานะของเอกสาร</label>
                                            <asp:DropDownList ID="ddlStatusapprove_L" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="999" Text="-" Selected="True" />
                                                <asp:ListItem Value="0" Text="ดำเนินการ" />
                                                <asp:ListItem Value="4" Text="อนุมัติ" />
                                                <asp:ListItem Value="5" Text="กลับไปแก้ไข" />
                                                <asp:ListItem Value="6" Text="ไม่อนุมัติ" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>ประเภทหลักสูตร</label>
                                            <asp:DropDownList ID="ddlcourse_plan_status_L" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="" Text="-" Selected="True" />
                                                <asp:ListItem Value="I" Text="In Plan" />
                                                <asp:ListItem Value="O" Text="Out Plan" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>กลุ่มวิชา</label>
                                            <asp:DropDownList ID="ddltrn_group_L" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <label>
                                            &nbsp;
                                        </label>
                                        <div class="form-group">
                                            <asp:LinkButton ID="btnFilter" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                                OnCommand="btnCommand" CommandName="btnFilter" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>


                    <div class="col-md-11 col-md-offset-1">
                        <asp:Panel ID="panel2" runat="server" CssClass="text-right m-b-10 hidden-print">
                            <asp:LinkButton ID="btnexport1" CssClass="btn btn-primary"
                                runat="server" data-toggle="tooltip" Text="<i class='fa fa-file-excel'></i> Export Excel"
                                CommandName="btnexport1"
                                OnCommand="btnCommand"
                                title="Export"></asp:LinkButton>

                        </asp:Panel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <asp:GridView ID="Gvreport_training_plan" runat="server"
                CssClass="table table-striped table-responsive table-bordered word-wrap"
                GridLines="None" OnRowCommand="onRowCommand"
                OnRowDataBound="onRowDataBound"
                AutoGenerateColumns="false"
                Width="100%"
                HeaderStyle-CssClass="info">
                <EmptyDataTemplate>
                    <div style="text-align: center">ไม่พบข้อมูล</div>
                </EmptyDataTemplate>
                <Columns>

                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                        HeaderStyle-Width="3%">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbDataItemIndex" runat="server"
                                    Visible="true" Text='<%# (Container.DataItemIndex +1) %>' />
                                <asp:Label ID="lbu0_training_course_idx" runat="server"
                                    Visible="false" Text='<%# Eval("u0_training_course_idx") %>' />
                                <asp:Label ID="lblecturer_type" runat="server"
                                    Visible="false" Text='<%# Eval("lecturer_type") %>' />
                                <asp:Label ID="lbcourse_plan_status" runat="server"
                                    Visible="false" Text='<%# Eval("course_plan_status") %>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="Type"
                        ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small" >
                        <EditItemTemplate>
                            <asp:TextBox ID="txtztype_edit_L" runat="server" Text='<%# Bind("ztype") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtztype_item_L" runat="server" Text='<%# Eval("ztype") %>'
                                Width="100%" Visible="false"></asp:Label>
                            <asp:Panel runat="server" ID="pnlicon_inplan">
                                <center>
                                         <img src='<%# ResolveUrl("~/images/elearning/inplan.png") %>' width="40" height="40" title="In Plan" id="imgicon_inplan" runat="server" visible="true" />
                                    </center>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlicon_outplan" >
                                <center>
                                         <img src='<%# ResolveUrl("~/images/elearning/outplan.png") %>' width="40" height="40" title="Out Plan" id="imgicon_outplan" runat="server" visible="true" />
                                    </center>
                            </asp:Panel>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="Status on Training Plan"
                        ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtzstatus_training_plan_edit_L" runat="server" Text='<%# Bind("zstatus_training_plan") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtzstatus_training_plan_item_L" runat="server" Text='<%# Eval("zstatus_training_plan") %>'
                                Width="100%"></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="Catergories"
                        ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtzcatergories_edit_L" runat="server" Text='<%# Bind("zcatergories") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtzcatergories_item_L" runat="server" Text='<%# Eval("zcatergories") %>'
                                Width="100%"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="Year Plan"
                        ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtzyear_plan_edit_L" runat="server" Text='<%# Bind("zyear_plan") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtzyear_plan_item_L" runat="server" Text='<%# Eval("zyear_plan") %>'
                                Width="100%"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="TrainingDate"
                        ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtztraining_date_edit_L" runat="server" Text='<%# Bind("ztraining_date") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtztraining_date_item_L" runat="server" Text='<%# "" %>'
                                Width="100%" Visible="false"></asp:Label>
                            <table style="border: none; width: 100%;">
                                <asp:Repeater ID="rptu8trncoursedate" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# setTraining_date((int)Eval("zday"),(int)Eval("zmonth"),(int)Eval("zyear")) %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="Course name"
                        ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtzcourse_name_edit_L" runat="server" Text='<%# Bind("zcourse_name") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtzcourse_name_item_L" runat="server" Text='<%# Eval("zcourse_name") %>'
                                Width="200px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="Trainer"
                        ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtztrainer_edit_L" runat="server" Text='<%# Bind("ztrainer") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtztrainer_item_L" runat="server"  Text='<%# Eval("ztrainer") %>'
                                Width="150px" Visible="false"></asp:Label>
                           

                            <table style="border: none; width: 150px;">
                                <asp:Repeater ID="rpt_institue" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("institution_name") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>

                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="Institue"
                        ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtzinstitue_edit_L" runat="server" Text='<%# Bind("zinstitue") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtzinstitue_item_L" runat="server" Text='<%# Eval("zinstitue") %>'
                                Width="150px" Visible="true"></asp:Label>

                            <table style="border: none; width: 150px;">
                                <asp:Repeater ID="rpt_institue1" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Eval("institution_name") %></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>

                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="Level / Target Group"
                        ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtztarget_group_edit_L" runat="server" Text='<%# Bind("ztarget_group") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtztarget_group_item_L" runat="server" Text='<%# Eval("ztarget_group") %>'
                                Width="150px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="Quantity / Course"
                        ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtzquantity_course_edit_L" runat="server" Text='<%# Bind("zquantity_course") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtzquantity_course_item_L" runat="server" Text='<%# Eval("zquantity_course") %>'
                                Width="100%"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="Target"
                        ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtztarget_edit_L" runat="server" Text='<%# Bind("ztarget") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtztarget_item_L" runat="server" Text='<%# Eval("ztarget") %>'
                                Width="100%"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="Register ลงทะเบียน"
                        ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtzregister_edit_L" runat="server" Text='<%# Bind("zregister") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtzregister_item_L" runat="server" Text='<%# Eval("zregister") %>'
                                Width="100%"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="Register %"
                        ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtzpersen_edit_L" runat="server" Text='<%# Bind("zpersen") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtzpersen_item_L" runat="server" Text='<%# Eval("zpersen") %>'
                                Width="100%"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="ระยะเวลา"
                        ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtztime_length_edit_L" runat="server" Text='<%# Bind("ztime_length") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtztime_length_item_L" runat="server" Text='<%# Eval("ztime_length") %>'
                                Width="100px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="ชั่วโมง"
                        ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtzhh_edit_L" runat="server" Text='<%# Bind("zhh") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtzhh_item_L" runat="server" Text='<%# Eval("zhh") %>'
                                Width="100%"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="รวมจำนวนชั่วโมง"
                        ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtzhh_total_edit_L" runat="server" Text='<%# Bind("zhh_total") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtzhh_total_item_L" runat="server" Text='<%# Eval("zhh_total") %>'
                                Width="100%"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="AssessmentToolวิธีการประเมิน"
                        ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtzassessment_tool_edit_L" runat="server" Text='<%# Bind("zassessment_tool") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtzassessment_tool_item_L" runat="server" Text='<%# Eval("zassessment_tool") %>'
                                Width="200px" Visible="false"></asp:Label>

                            <table style="border: none; width: 200px;">
                                <asp:Repeater ID="rpt_objective" runat="server">
                                    <ItemTemplate>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Label78" runat="server" Visible="false"
                                                    Text="การวัด ประเมิน และติดตามผล" Font-Bold="true" />

                                                <div class="form-inline">
                                                    <asp:Label ID="lbzName_Detail" runat="server"
                                                        Text='<%# Eval("zName_Detail") %>' Visible="true"></asp:Label>
                                                </div>

                                                <%-- end --%>

                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>

                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="Pass"
                        ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtzpass_edit_L" runat="server" Text='<%# Bind("zpass") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtzpass_item_L" runat="server" Text='<%# Eval("zpass") %>'
                                Width="100%"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="Pass %"
                        ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtzpass_persen_edit_L" runat="server" Text='<%# Bind("zpass_persen") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtzpass_persen_item_L" runat="server" Text='<%# Eval("zpass_persen") %>'
                                Width="100%"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="Satisfaction"
                        ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtzsatisfaction_edit_L" runat="server" Text='<%# Bind("zsatisfaction") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtzsatisfaction_item_L" runat="server" Text='<%# Eval("zsatisfaction") %>'
                                Width="100%"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="Cost Center"
                        ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtzcost_center_edit_L" runat="server" Text='<%# Bind("zcost_center") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtzcost_center_item_L" runat="server" Text='<%# Eval("zcost_center") %>'
                                Width="100%"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="Plan"
                        ItemStyle-HorizontalAlign="Right"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtzplan_amount_edit_L" runat="server" Text='<%# Bind("zplan_amount") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtzplan_amount_item_L" runat="server" Text='<%# Eval("zplan_amount") %>'
                                Width="100%"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField
                        HeaderText="ชื่อผู้ขอคอร์ส"
                        ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center"
                        ItemStyle-Font-Size="Small">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtzcreate_name_edit_L" runat="server" Text='<%# Bind("zcreate_name") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="txtzcreate_name_item_L" runat="server" Text='<%# Eval("zcreate_name") %>'
                                Width="150px"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>


                </Columns>
            </asp:GridView>

            <br />

        </asp:View>
    </asp:MultiView>



    <script type="text/javascript">
        var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
    </script>

    <script type="text/javascript">
        function fnExcelReport() {
            var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange; var j = 0;
            tab = document.getElementById('Tableprint_sched'); // id of table

            for (j = 0; j < tab.rows.length; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }
    </script>

    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);
            var printWindow = window.open('', '', 'height=800,width=1000');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
                //printWindow.resizeTo(100,100);
                //printWindow.focus();
                //printwindow.window.close();
            }, 500);
            return false;
        }

        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }

        function dtPickerInput(inputClassFrom, inputClassTo) {
            $(inputClassFrom).datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $(inputClassTo).datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }

        function dtPickerBtn(inputClassFrom, inputClassTo, btnClassFrom, btnClassTo) {
            $(btnClassFrom).click(function () {
                $(inputClassFrom).data("DateTimePicker").show();
            });
            $(btnClassTo).click(function () {
                $(inputClassTo).data("DateTimePicker").show();
            });
        }

        $(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
    </script>

    <asp:HiddenField ID="hddf_empty" runat="server" />
</asp:Content>

