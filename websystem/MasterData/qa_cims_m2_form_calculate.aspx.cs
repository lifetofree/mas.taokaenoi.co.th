﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Data.OleDb;
using MessagingToolkit.QRCode.Codec;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;


public partial class websystem_MasterData_qa_cims_m2_form_calculate : System.Web.UI.Page
{
    #region Connect
    function_tool _funcTool = new function_tool();
    data_employee _dataEmployee = new data_employee();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //--master-- //
    static string _urlCimsGetFormCalName = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetFormCalName"];
    static string _urlCimsSetSignature = _serviceUrl + ConfigurationManager.AppSettings["urlCimsSetSignature"];
    static string _urlCimsGetSignature = _serviceUrl + ConfigurationManager.AppSettings["urlCimsGetSignature"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion Connect

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            getSignature();
        }
    }

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdAddSignature":
                setFormData(fvSignature, FormViewMode.Insert, null);
                getPositionList((DropDownList)fvSignature.FindControl("ddl_position"), "0");
                getFormNameList((DropDownList)fvSignature.FindControl("ddl_form_name"), "0");
                gvSignature.EditIndex = -1;
                break;

            case "cmdCancelSignature":
                setActiveView("docManageSignature", 0, "");
                setFocus.Focus();
                break;

            case "cmdSaveSignature":

                var txtDatetimeSignature = (TextBox)fvSignature.FindControl("txtDatetimeSignature");
                var txtTopicNameSignature = (TextBox)fvSignature.FindControl("txtTopicNameSignature");
                var txtNameSignaturePosition = (TextBox)fvSignature.FindControl("txtNameSignaturePosition");
                var _ddl_position = (DropDownList)fvSignature.FindControl("ddl_position");
                var _ddl_form_name = (DropDownList)fvSignature.FindControl("ddl_form_name");
                var _ddl_status_form_name = (DropDownList)fvSignature.FindControl("ddl_status_form_name");

                data_qa_cims _data_qa_cims = new data_qa_cims();
                _data_qa_cims.qa_cims_m2_form_calculate_list = new qa_cims_m2_form_calculate_details[1];
                qa_cims_m2_form_calculate_details _insert_signature = new qa_cims_m2_form_calculate_details();

                _insert_signature.cemp_idx = _emp_idx;
                _insert_signature.m2_signature_name = txtTopicNameSignature.Text;
                _insert_signature.m2_datetime_signature = txtDatetimeSignature.Text;
                _insert_signature.m2_position_signature = txtNameSignaturePosition.Text;
                _insert_signature.m0_formcal_idx = int.Parse(_ddl_form_name.SelectedValue);
                _insert_signature.m2_select_detail = int.Parse(_ddl_position.SelectedValue);
                _insert_signature.m2_formcal_status = int.Parse(_ddl_status_form_name.SelectedValue);

                _data_qa_cims.qa_cims_m2_form_calculate_list[0] = _insert_signature;

                _data_qa_cims = callServicePostMasterQACIMS(_urlCimsSetSignature, _data_qa_cims);

                if (_data_qa_cims.return_code == 0)
                {
                    txtTopicNameSignature.Text = "";
                    txtNameSignaturePosition.Text = "";
                    getSignature();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);
                }

                break;

            case "cmdDeleteSignature":

                int _m2_formcal_idx = int.Parse(cmdArg);

                data_qa_cims _data_signature_del = new data_qa_cims();
                _data_signature_del.qa_cims_m2_form_calculate_list = new qa_cims_m2_form_calculate_details[1];
                qa_cims_m2_form_calculate_details _del_signature = new qa_cims_m2_form_calculate_details();

                _del_signature.m2_formcal_idx = (_m2_formcal_idx);
                _del_signature.m2_formcal_status = 9;

                _data_signature_del.qa_cims_m2_form_calculate_list[0] = _del_signature;

                _data_signature_del = callServicePostMasterQACIMS(_urlCimsSetSignature, _data_signature_del);

                if (_data_signature_del.return_code == 0)
                {
                    getSignature();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินรายการไม่สำเร็จ กรุณาลองใหม่อีกครั้ง');", true);
                }


                break;
        }

    }

    #endregion

    #region setgriddata

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "gvSignature":
                gvSignature.PageIndex = e.NewPageIndex;
                getSignature();
                setFocus.Focus();
                break;
        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvSignature":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbformcal_table_status = (Label)e.Row.Cells[6].FindControl("lbformcal_table_status");
                    Label lbsignature_statusOnline = (Label)e.Row.Cells[6].FindControl("lbsignature_statusOnline");
                    Label lbsignature_statusOffline = (Label)e.Row.Cells[6].FindControl("lbsignature_statusOffline");

                    ViewState["_signature_status"] = lbformcal_table_status.Text;

                    if (ViewState["_signature_status"].ToString() == "1")
                    {
                        lbsignature_statusOnline.Visible = true;
                    }
                    else if (ViewState["_signature_status"].ToString() == "0")
                    {
                        lbsignature_statusOffline.Visible = true;
                    }
                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    var _select_detail_position = (TextBox)e.Row.Cells[0].FindControl("txt_update_select_detail");
                    getPositionList((DropDownList)e.Row.Cells[0].FindControl("ddl_update_position"), _select_detail_position.Text);
                    var _select_m0_form_idx = (TextBox)e.Row.Cells[0].FindControl("txt_update_m0_formcal_idx");
                    getFormNameList((DropDownList)e.Row.Cells[0].FindControl("ddl_formcal_update"), _select_m0_form_idx.Text);

                    setFormData(fvSignature, FormViewMode.ReadOnly, null);
                }
                break;

        }
    }

    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvSignature":

                var _update_m2_formcal_idx = (TextBox)gvSignature.Rows[e.RowIndex].FindControl("txt_m2_formcal_idx");
                var _update_signature_name = (TextBox)gvSignature.Rows[e.RowIndex].FindControl("txt_update_signature_name");
                var _update_signature_position = (TextBox)gvSignature.Rows[e.RowIndex].FindControl("txt_update_signature_position");
                var _update_datetime_signature = (TextBox)gvSignature.Rows[e.RowIndex].FindControl("txt_update_datetime_signature");
                var _update_position = (DropDownList)gvSignature.Rows[e.RowIndex].FindControl("ddl_update_position");
                var _update_form = (DropDownList)gvSignature.Rows[e.RowIndex].FindControl("ddl_formcal_update");
                var _update_status = (DropDownList)gvSignature.Rows[e.RowIndex].FindControl("ddlUpdateSignatureStatus");

                gvSignature.EditIndex = -1;

                data_qa_cims _data_update_signature = new data_qa_cims();
                _data_update_signature.qa_cims_m2_form_calculate_list = new qa_cims_m2_form_calculate_details[1];
                qa_cims_m2_form_calculate_details _update_signature = new qa_cims_m2_form_calculate_details();

                _update_signature.m2_formcal_idx = int.Parse(_update_m2_formcal_idx.Text);
                _update_signature.m0_formcal_idx = int.Parse(_update_form.Text);
                _update_signature.m2_select_detail = int.Parse(_update_position.Text);
                _update_signature.m2_signature_name = _update_signature_name.Text;
                _update_signature.m2_position_signature = _update_signature_position.Text;
                _update_signature.m2_datetime_signature = _update_datetime_signature.Text;
                _update_signature.m2_formcal_status = int.Parse(_update_status.SelectedValue);

                _data_update_signature.qa_cims_m2_form_calculate_list[0] = _update_signature;

                _data_update_signature = callServicePostMasterQACIMS(_urlCimsSetSignature, _data_update_signature);

                if (_data_update_signature.return_code == 1)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true); 
                    getSignature();
                }
                else
                {
                    getSignature();
                    setFocus.Focus();
                }

                break;

        }
    }

    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvSignature":
                gvSignature.EditIndex = -1;
                getSignature();
                setFocus.Focus();
                break;
        }
    }

    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvSignature":
                gvSignature.EditIndex = e.NewEditIndex;
                getSignature();
                break;
        }
    }

    #endregion

    #region dropdown list

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void getPositionList(DropDownList ddlName, string _position)
    {

        ddlName.Items.Insert(0, new ListItem("--- เลือกตำแหน่งของการจัดวาง ---", "0"));
        ddlName.Items.Insert(1, new ListItem("ตำแหน่งที่ 1", "1"));
        ddlName.Items.Insert(2, new ListItem("ตำแหน่งที่ 2", "2"));
        ddlName.Items.Insert(3, new ListItem("ตำแหน่งที่ 3", "3"));
        ddlName.Items.Insert(4, new ListItem("ตำแหน่งที่ 4", "4"));
        ddlName.SelectedValue = _position.ToString();
    }

    protected void getFormNameList(DropDownList ddlName, string _m0_form_idx)
    {
        data_qa_cims _dataqa = new data_qa_cims();
        _dataqa.qa_cims_m0_form_calculate_list = new qa_cims_m0_form_calculate_details[1];
        qa_cims_m0_form_calculate_details _m0form_name = new qa_cims_m0_form_calculate_details();

        _dataqa.qa_cims_m0_form_calculate_list[0] = _m0form_name;

        _dataqa = callServicePostMasterQACIMS(_urlCimsGetFormCalName, _dataqa);
        setDdlData(ddlName, _dataqa.qa_cims_m0_form_calculate_list, "m0_formcal_name", "m0_formcal_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฟอร์มสอบเทียบเครื่องมือ ---", "0"));
        ddlName.SelectedValue = _m0_form_idx.ToString();

    }

    #endregion



    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    #endregion setformdata

    #region setActiveView

    protected void setActiveView(string activeTab, int idx, string name)
    {
        mvMultiview.SetActiveView((View)mvMultiview.FindControl(activeTab));
        setFormData(fvSignature, FormViewMode.ReadOnly, null);

        switch (activeTab)
        {
            case "docManageSignature":
                getSignature();

                break;

        }
    }

    #endregion setActiveView

    #region getdata
    protected void getSignature()
    {
        data_qa_cims _data_signature = new data_qa_cims();
        _data_signature.qa_cims_m2_form_calculate_list = new qa_cims_m2_form_calculate_details[1];
        qa_cims_m2_form_calculate_details _select_signature = new qa_cims_m2_form_calculate_details();

        _data_signature.qa_cims_m2_form_calculate_list[0] = _select_signature;

        _data_signature = callServicePostMasterQACIMS(_urlCimsGetSignature, _data_signature);

        setGridData(gvSignature, _data_signature.qa_cims_m2_form_calculate_list);
    }

    #endregion getdata

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region callService 
    protected data_qa_cims callServicePostMasterQACIMS(string _cmdUrl, data_qa_cims _data_qa_cims)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa_cims);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa_cims = (data_qa_cims)_funcTool.convertJsonToObject(typeof(data_qa_cims), _localJson);

        return _data_qa_cims;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
    #endregion callService Functions
}