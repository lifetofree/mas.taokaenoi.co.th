using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_menu")]
public class data_menu
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("m0_menu_list")]
    public m0_menu_detail[] m0_menu_list { get; set; }
    [XmlElement("m0_system_list")]
    public m0_system_detail[] m0_system_list { get; set; }
    [XmlElement("m0_permissiontype_list")]
    public m0_permissiontype_detail[] m0_permissiontype_list { get; set; }
    [XmlElement("m0_permission_list")]
    public m0_permission_detail[] m0_permission_list { get; set; }
    [XmlElement("m0_role_list")]
    public m0_role_detail[] m0_role_list { get; set; }
}

[Serializable]
public class m0_menu_detail
{
    [XmlElement("menu_idx")]
    public int menu_idx { get; set; }

    [XmlElement("menu_name_th")]
    public string menu_name_th { get; set; }

    [XmlElement("menu_name_thlist")]
    public string menu_name_thlist { get; set; }

    [XmlElement("menu_name_thtest")]
    public string menu_name_thtest { get; set; }

    [XmlElement("menu_name_en")]
    public string menu_name_en { get; set; }

    [XmlElement("menu_url")]
    public string menu_url { get; set; }

    [XmlElement("menu_relation")]
    public int menu_relation { get; set; }

    [XmlElement("menu_order")]
    public int menu_order { get; set; }

    [XmlElement("menu_icon")]
    public string menu_icon { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("menu_status")]
    public int menu_status { get; set; }

    [XmlElement("menu_level")]
    public int menu_level { get; set; }

    [XmlElement("system_idx")]
    public int system_idx { get; set; }
}

[Serializable]
public class m0_system_detail
{
    [XmlElement("system_idx")]
    public int system_idx { get; set; }

    [XmlElement("system_name_th")]
    public string system_name_th { get; set; }

    [XmlElement("system_name_en")]
    public string system_name_en { get; set; }

    [XmlElement("system_order")]
    public int system_order { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("system_status")]
    public int system_status { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
}

[Serializable]
public class m0_permissiontype_detail
{
    [XmlElement("permissiontype_idx")]
    public int permissiontype_idx { get; set; }

    [XmlElement("permissiontype_name")]
    public string permissiontype_name { get; set; }
   
    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("permissiontype_status")]
    public int permissiontype_status { get; set; }

    [XmlElement("system_idx")]
    public int system_idx { get; set; }

    [XmlElement("menu_idx")]
    public int menu_idx { get; set; }

}

[Serializable]
public class m0_permission_detail
{

    [XmlElement("permission_idx")]
    public int permission_idx { get; set; }

    [XmlElement("permission_name")]
    public string permission_name { get; set; }

    [XmlElement("permission_type")]
    public int permission_type { get; set; }

    [XmlElement("permissiontype_idx")]
    public int permissiontype_idx { get; set; }

    [XmlElement("permissiontype_name")]
    public string permissiontype_name { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("permissiontype_status")]
    public int permissiontype_status { get; set; }

    [XmlElement("system_idx")]
    public int system_idx { get; set; }

    [XmlElement("menu_idx")]
    public int menu_idx { get; set; }

    [XmlElement("role_idx")]
    public int role_idx { get; set; }

    [XmlElement("role_name_th")]
    public string role_name_th { get; set; }

    [XmlElement("role_name_en")]
    public string role_name_en { get; set; }

}

[Serializable]
public class m0_role_detail
{

    [XmlElement("role_idx")]
    public int role_idx { get; set; }

    [XmlElement("role_name_th")]
    public string role_name_th { get; set; }

    [XmlElement("role_name_en")]
    public string role_name_en { get; set; }

    [XmlElement("role_status")]
    public int role_status { get; set; }

    [XmlElement("permission_idx")]
    public int permission_idx { get; set; }

    [XmlElement("permission_name")]
    public string permission_name { get; set; }

    [XmlElement("permission_type")]
    public int permission_type { get; set; }

    [XmlElement("permissiontype_idx")]
    public int permissiontype_idx { get; set; }

    [XmlElement("permissiontype_name")]
    public string permissiontype_name { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("permissiontype_status")]
    public int permissiontype_status { get; set; }

    [XmlElement("system_idx")]
    public int system_idx { get; set; }

    [XmlElement("menu_idx")]
    public int menu_idx { get; set; }

    [XmlElement("permission_idxstring")]
    public string permission_idxstring { get; set; }

}
