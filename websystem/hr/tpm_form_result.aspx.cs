﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class websystem_hr_tpm_form_result : System.Web.UI.Page
{
    #region Connect

    function_tool _funcTool = new function_tool();
    data_tpm_form _dtpmform = new data_tpm_form();
    data_employee _dtEmployee = new data_employee();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlSelectMaster = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMaster_FormResult"];
    static string _urlSelectSystem = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSystem_FormResult"];
    static string _urlInsertSystem = _serviceUrl + ConfigurationManager.AppSettings["urlInsertSystem_FormResult"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];

    int count = 0;
    int count_improve = 0;
    int count_star = 0;

    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] =  int.Parse(Session["emp_idx"].ToString());
            ViewState["PosGroupNameTH"] = "0";
            ViewState["Settab_cancel"] = "0";
            select_empIdx_present(); 
           
            SetDefaultpage(1);
            ViewState["unidx"] = "0";
            select_hradmin(int.Parse(ViewState["Pos_idx"].ToString()));

            if (ViewState["check_hradmin"].ToString() != "0")
            {
                _divMenuLiToViewReport.Visible = true;
            }
            else
            {
                _divMenuLiToViewReport.Visible = false;
            }

        }

        SetInsertOrgIDX(int.Parse(ViewState["Org_idx"].ToString()));
    }
    #endregion

    protected void SetInsertOrgIDX(int orgidx)
    {

        switch (orgidx)
        {
            case 1:
            case 6:

                _divMenuLiToViewAdd.Visible = false;
                _divMenuLiToViewApprove.Visible = false;
                break;

            default:
                _divMenuLiToViewAdd.Visible = true;
                _divMenuLiToViewApprove.Visible = true;
                break;
        }
    }

    #region Select
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());

        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["emp_start_date"] = _dtEmployee.employee_list[0].emp_start_date;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["TIDX"] = _dtEmployee.employee_list[0].TIDX;
        ViewState["PosGroupNameTH"] = _dtEmployee.employee_list[0].PosGroupNameTH;
        ViewState["JobGradeLevel"] = _dtEmployee.employee_list[0].jobgrade_level;


    }

    protected void select_empIdx_create()
    {
        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["cemp_idx_u0"].ToString());

        ViewState["rdept_name_create"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx_create"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName_create"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name_create"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx_create"] = _dtEmployee.employee_list[0].org_idx;
        ViewState["EmpCode_create"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname_create"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx_create"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email_create"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel_create"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname_create"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx_create"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX_create"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["JobGradeIDX_create"] = _dtEmployee.employee_list[0].jobgrade_idx;
        ViewState["emp_start_date_create"] = _dtEmployee.employee_list[0].emp_start_date;
        ViewState["PosGroupNameTH_create"] = _dtEmployee.employee_list[0].PosGroupNameTH;
        ViewState["TIDX_create"] = _dtEmployee.employee_list[0].TIDX;

    }

    protected void getOrganizationList(DropDownList ddlName)
    {

        _dtEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dtEmployee.organization_list[0] = _orgList;

        _dtEmployee = callServicePostEmp(_urlGetOrganizationList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร...", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dtEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dtEmployee.department_list[0] = _deptList;

        _dtEmployee = callServicePostEmp(_urlGetDepartmentList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกฝ่าย...", "0"));
    }

    protected void select_masterstatus(DropDownList ddlName, int unidx)
    {

        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 13;
        dataselect.unidx = unidx;
        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        setDdlData(ddlName, _dtpmform.Boxtpmm0_DocFormDetail, "status_name", "staidx");
        //ddlName.Items.Insert(0, new ListItem("...", "0"));
    }

    protected void select_masternode(DropDownList ddlName)
    {

        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 17;
        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        setDdlData(ddlName, _dtpmform.Boxtpmm0_DocFormDetail, "node_desc", "noidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสถานะดำเนินการ...", "0"));
    }

    protected void select_masterpoint(GridView gvName)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 8;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        setGridData(gvName, _dtpmform.Boxtpmm0_DocFormDetail);

    }

    protected void select_mastercorevalue(GridView gvName)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 9;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        setGridData(gvName, _dtpmform.Boxtpmm0_DocFormDetail);

    }

    protected void select_mastersubcorevalue(GridView gvName, int m1_coreidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 10;
        dataselect.m1_coreidx = m1_coreidx;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        setGridData(gvName, _dtpmform.Boxtpmm0_DocFormDetail);

    }

    protected void select_mastercompentency(GridView gvName, int posidx, int orgidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 11;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;

        _dtpmform.Boxtpmm0_DocFormDetail = new tpmm0_DocFormDetail[1];
        tpmm0_DocFormDetail dataselectform = new tpmm0_DocFormDetail();

        dataselectform.orgidx = orgidx;
        dataselectform.posidx = posidx;

        _dtpmform.Boxtpmm0_DocFormDetail[0] = dataselectform;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);

        if (_dtpmform.ReturnCode.ToString() == "0")
        {
            setGridData(gvName, _dtpmform.Boxtpmm0_DocFormDetail);

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณไม่มีแบบฟอร์มประเมิน กรุณาติดต่อทาง HR !!');", true);
            setGridData(gvName, null);

        }
        ViewState["rtcode_form"] = _dtpmform.ReturnCode.ToString();


    }

    protected void select_mastersubcompentency(GridView gvName, int u0typidx, int m1_typidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 12;
        dataselect.u0_typeidx = u0typidx;
        dataselect.m1_typeidx = m1_typidx;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;


        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        setGridData(gvName, _dtpmform.Boxtpmm0_DocFormDetail);

    }

    protected void select_index(GridView gvName, int org_idx, int rdept_idx, int rsec_idx, int jobgrade_level, int cemp_idx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 1;
        dataselect.cemp_idx = cemp_idx;
        dataselect.org_idx = org_idx;
        dataselect.rdept_idx = rdept_idx;
        dataselect.rsec_idx = rsec_idx;
        dataselect.jobgrade_level = jobgrade_level;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;


        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectSystem, _dtpmform);
        setGridData(gvName, _dtpmform.Boxtpmu0_DocFormDetail);

        ViewState["rtcode_checkcreate"] = _dtpmform.ReturnMsg.ToString();

    }

    protected void select_corevalue_answer(GridView gvName, int u0_docidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 2;
        dataselect.u0_docidx = u0_docidx;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectSystem, _dtpmform);
        setGridData(gvName, _dtpmform.Boxtpmu1_DocFormDetail);

    }

    protected void select_competencise_answer(GridView gvName, int u0_docidx, int tidx, int u0_typeidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 3;
        dataselect.u0_docidx = u0_docidx;
        dataselect.TIDX = tidx;
        dataselect.u0_typeidx = u0_typeidx;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectSystem, _dtpmform);
        setGridData(gvName, _dtpmform.Boxtpmu1_DocFormDetail);

    }

    protected void select_improve_detail(GridView gvName, int u0_docidx, int m0_typeidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 4;
        dataselect.u0_docidx = u0_docidx;
        dataselect.m0_typeidx = m0_typeidx;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectSystem, _dtpmform);
        setGridData(gvName, _dtpmform.Boxtpmu2_DocFormDetail);

    }

    protected void select_approveindex(GridView gvName, int org_idx, int rdept_idx, int rsec_idx, int jobgrade_level, int cemp_idx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 5;
        dataselect.cemp_idx = cemp_idx;
        dataselect.org_idx = org_idx;
        dataselect.rdept_idx = rdept_idx;
        dataselect.rsec_idx = rsec_idx;
        dataselect.jobgrade_level = jobgrade_level;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;


        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectSystem, _dtpmform);
        // PHON 2019-12-06
        try{
        tpmu0_DocFormDetail[] _data = (tpmu0_DocFormDetail[])_dtpmform.Boxtpmu0_DocFormDetail;
        var _linqFilter = (from m in _data
                            .Where(m => m.org_idx != 1 && m.org_idx != 6)
                            select m);
        // setGridData(gvName, _dtpmform.Boxtpmu0_DocFormDetail);
        setGridData(gvName, _linqFilter.ToList());
        } catch { setGridData(gvName, null); }
        // PHON 2019-12-06

        ViewState["rtcode_checkcreate"] = _dtpmform.ReturnMsg.ToString();

    }

    protected void select_sumapprove(int org_idx, int rdept_idx, int rsec_idx, int jobgrade_level, int cemp_idx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 7;
        dataselect.cemp_idx = cemp_idx;
        dataselect.org_idx = org_idx;
        dataselect.rdept_idx = rdept_idx;
        dataselect.rsec_idx = rsec_idx;
        dataselect.jobgrade_level = jobgrade_level;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;


        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectSystem, _dtpmform);

        nav_approve.Text = "<span class='badge progress-bar-danger' >" + _dtpmform.Boxtpmu0_DocFormDetail[0].countapprove.ToString() + "</span>";

    }

    protected void select_Log(Repeater rpName, int u0_docidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail _select = new tpmu0_DocFormDetail();

        _select.u0_docidx = u0_docidx;
        _select.condition = 6;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectSystem, _dtpmform);
        setRepeatData(rpName, _dtpmform.Boxtpmu0_DocFormDetail);
    }

    protected void select_report(GridView gvName, int orgidx, string rdeptidx_comma, int unidx, string empnameth, string empcode, int year)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 9;
        dataselect.org_idx = orgidx;
        dataselect.rdepidx_comma = rdeptidx_comma;
        dataselect.unidx = unidx;
        dataselect.emp_name_th = empnameth;
        dataselect.emp_code = empcode;
        dataselect.getyear = year;
        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectSystem, _dtpmform);
        ViewState["Export_ReportList"] = _dtpmform.Boxtpmu0_DocFormDetail;
        setGridData(gvName, _dtpmform.Boxtpmu0_DocFormDetail);

    }

    protected void select_hradmin(int rposidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 8;
        dataselect.rpos_idx = rposidx;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectSystem, _dtpmform);
        ViewState["check_hradmin"] = _dtpmform.Boxtpmu0_DocFormDetail[0].m0idx.ToString();

    }

    protected void select_mastercompentency_forimprove(DropDownList ddlName, int posidx, int orgidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 19;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;

        _dtpmform.Boxtpmm0_DocFormDetail = new tpmm0_DocFormDetail[1];
        tpmm0_DocFormDetail dataselectform = new tpmm0_DocFormDetail();

        dataselectform.orgidx = orgidx;
        dataselectform.posidx = posidx;

        _dtpmform.Boxtpmm0_DocFormDetail[0] = dataselectform;

        //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);

        setDdlData(ddlName, _dtpmform.Boxtpmm0_DocFormDetail, "m1_type_name", "m1_typeidx_comma");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกข้อมูล...", "0"));
        //ddlName.Items.Insert(1, new ListItem("การทำงานเป็นทีม (Team)", "1"));
        //ddlName.Items.Insert(2, new ListItem("มุ่งมั่นสู่ความเป็นเสิศ (Keep Excelling)", "2"));
        //ddlName.Items.Insert(3, new ListItem("สร้างสรรค์สิ่งใหม่ (New Idea)", "3"));

    }

    protected void select_checkpermission(int cempidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 20;
        dataselect.cemp_idx = cempidx;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        ViewState["check_permission"] = _dtpmform.ReturnCode.ToString();

    }

    protected void select_department(CheckBoxList chklist, int orgidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 21;
        dataselect.org_idx = orgidx;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        setChkData(chklist, _dtpmform.Boxtpmu0_DocFormDetail, "dept_name_th", "rdept_idx");
        //chklist.Items.Insert(0, new ListItem("ทั้งหมด", "0"));
    }

    protected void select_report_all_find_department(GridView gvName, string rdeptidx_comma)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 10;
        dataselect.rdepidx_comma = rdeptidx_comma;
        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectSystem, _dtpmform);
        setGridData(gvName, _dtpmform.Boxtpmu0_DocFormDetail);

    }

    #endregion

    #region Insert & Update

    protected void insert_system(int unidx, int acidx, int staidx, int cempidx, int condition, int u0_typeidx, GridView gvcorevalue, GridView gvcompetency)
    {
        _dtpmform = new data_tpm_form();

        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail datainsert = new tpmu0_DocFormDetail();

        datainsert.cemp_idx = cempidx;
        datainsert.unidx = unidx;
        datainsert.acidx = acidx;
        datainsert.staidx = staidx;
        datainsert.condition = condition;
        datainsert.u0_typeidx = u0_typeidx;
        _dtpmform.Boxtpmu0_DocFormDetail[0] = datainsert;

        int i = 0;
        var u1_corevalue = new tpmu1_DocFormDetail[gvcorevalue.Rows.Count + gvcompetency.Rows.Count];


        foreach (GridViewRow row in gvcorevalue.Rows)
        {
            RadioButtonList rdochoice_mine = (RadioButtonList)row.Cells[4].FindControl("rdochoice_mine");
            Label lblm1_coreidx = (Label)row.Cells[0].FindControl("lblm1_coreidx");
            Label lblcore_reason = (Label)row.Cells[4].FindControl("lblcore_reason");
            TextBox txtcore_insert = (TextBox)row.Cells[4].FindControl("txtcore_insert");


            _dtpmform.Boxtpmu1_DocFormDetail = new tpmu1_DocFormDetail[1];
            u1_corevalue[i] = new tpmu1_DocFormDetail();

            if (rdochoice_mine.SelectedValue != "0" && rdochoice_mine.SelectedValue != null && rdochoice_mine.SelectedValue != "")
            {
                u1_corevalue[i].m0_typeidx = 1;
                u1_corevalue[i].m1_typeidx = int.Parse(lblm1_coreidx.Text); // เอาเข้าแทน m1_coreidx
                u1_corevalue[i].m0_point_mine = rdochoice_mine.SelectedValue.ToString();
                u1_corevalue[i].remark_mine = txtcore_insert.Text;

            }

            _dtpmform.Boxtpmu1_DocFormDetail = u1_corevalue;

            i++;
        }


        foreach (GridViewRow row in gvcompetency.Rows)
        {
            RadioButtonList rdochoicecom_mine = (RadioButtonList)row.Cells[4].FindControl("rdochoicecom_mine");
            Label lblm1_typeidx = (Label)row.Cells[2].FindControl("lblm1_typeidx");
            TextBox txtcom_insert = (TextBox)row.Cells[4].FindControl("txtcom_insert");

            u1_corevalue[i] = new tpmu1_DocFormDetail();

            if (rdochoicecom_mine.SelectedValue != "0" && rdochoicecom_mine.SelectedValue != null && rdochoicecom_mine.SelectedValue != "")
            {
                u1_corevalue[i].m0_typeidx = 2;
                u1_corevalue[i].m1_typeidx = int.Parse(lblm1_typeidx.Text);
                u1_corevalue[i].m0_point_mine = rdochoicecom_mine.SelectedValue.ToString();
                u1_corevalue[i].remark_mine = txtcom_insert.Text;


            }

            _dtpmform.Boxtpmu1_DocFormDetail = u1_corevalue;

            i++;

        }

        int a = 0;
        var ds_insert_dev = (DataSet)ViewState["vsAddListTable_Dev"];
        var ds_insert_star = (DataSet)ViewState["vsAddListTable_Star"];

        var _insertdev = new tpmu2_DocFormDetail[ds_insert_dev.Tables[0].Rows.Count + ds_insert_star.Tables[0].Rows.Count];

        if (ds_insert_dev.Tables[0].Rows.Count != 0)
        {

            foreach (DataRow dtrow in ds_insert_dev.Tables[0].Rows)
            {
                if (dtrow["behavior"].ToString() != null)
                {
                    _insertdev[a] = new tpmu2_DocFormDetail();
                    string[] ToId = dtrow["m1_typeidx_comma"].ToString().Split(',');
                    int x = 0;
                    foreach (string To_check in ToId)
                    {
                        if (x == 0)
                        {
                            _insertdev[a].m0_typeidx_choose = int.Parse(To_check);
                        }
                        else
                        {
                            _insertdev[a].behavior_name = To_check;
                        }

                        x++;
                    }

                    _insertdev[a].m0_typeidx = 3;
                    //_insertdev[a].behavior_name = dtrow["behavior"].ToString();

                    _insertdev[a].comment_name = dtrow["comment"].ToString();

                }

                a++;
                _dtpmform.Boxtpmu2_DocFormDetail = _insertdev;
                count_improve = 1;
            }
        }
        else
        {
            count_improve = 0;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูลด้านที่ต้องพัฒนา!!!');", true);
            return;
        }

        if (ds_insert_star.Tables[0].Rows.Count != 0)
        {
            foreach (DataRow dtrow in ds_insert_star.Tables[0].Rows)
            {
                if (dtrow["behavior_star"].ToString() != null)
                {
                    _insertdev[a] = new tpmu2_DocFormDetail();

                    string[] ToId = dtrow["m1_typeidx_comma"].ToString().Split(',');
                    int y = 0;
                    foreach (string To_check in ToId)
                    {

                        if (y == 0)
                        {
                            _insertdev[a].m0_typeidx_choose = int.Parse(To_check);
                        }
                        else
                        {
                            _insertdev[a].behavior_name = To_check;
                        }

                        y++;
                    }
                    _insertdev[a].m0_typeidx = 4;
                    //_insertdev[a].behavior_name = dtrow["behavior_star"].ToString();
                    _insertdev[a].comment_name = dtrow["comment_star"].ToString();
                }

                a++;
                _dtpmform.Boxtpmu2_DocFormDetail = _insertdev;
                count_star = 1;
            }
        }
        else
        {
            count_star = 0;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูลด้านความสามารถโดดเด่น!!!');", true);
            return;
        }

       //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlInsertSystem, _dtpmform);

    }

    protected void update_system(int unidx, int acidx, int staidx, int cempidx, int condition, int u0_docidx, GridView gvcorevalue, GridView gvcompetency)
    {
        _dtpmform = new data_tpm_form();

        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail datainsert = new tpmu0_DocFormDetail();

        datainsert.cemp_idx = cempidx;
        datainsert.unidx = unidx;
        datainsert.acidx = acidx;
        datainsert.staidx = staidx;
        datainsert.condition = condition;
        datainsert.u0_docidx = u0_docidx;
        _dtpmform.Boxtpmu0_DocFormDetail[0] = datainsert;

        int i = 0;
        var u1_corevalue = new tpmu1_DocFormDetail[gvcorevalue.Rows.Count + gvcompetency.Rows.Count];

        foreach (GridViewRow row in gvcorevalue.Rows)
        {
            RadioButtonList rdochoice_mine_detail = (RadioButtonList)row.Cells[4].FindControl("rdochoice_mine_detail");
            RadioButtonList rdochoice_head_detail = (RadioButtonList)row.Cells[5].FindControl("rdochoice_head_detail");

            Label lblm1_coreidx = (Label)row.Cells[0].FindControl("lblm1_coreidx");
            Label lblu1_docidx = (Label)row.Cells[0].FindControl("lblu1_docidx");
            TextBox txtremarkmine_core = (TextBox)row.Cells[4].FindControl("txtremarkmine_core");
            TextBox txtremarkhead_core = (TextBox)row.Cells[5].FindControl("txtremarkhead_core");



            _dtpmform.Boxtpmu1_DocFormDetail = new tpmu1_DocFormDetail[1];
            u1_corevalue[i] = new tpmu1_DocFormDetail();

            u1_corevalue[i].u1_docidx = int.Parse(lblu1_docidx.Text); // เอาเข้าแทน m1_coreidx


            if (acidx == 1)
            {
                if (rdochoice_mine_detail.SelectedValue != "0" && rdochoice_mine_detail.SelectedValue != null && rdochoice_mine_detail.SelectedValue != "")
                {
                    u1_corevalue[i].m0_point_mine = rdochoice_mine_detail.SelectedValue.ToString();

                    if (ViewState["Org_idx_create"].ToString() == "1" || ViewState["Org_idx_create"].ToString() == "6")
                    {
                        if (rdochoice_mine_detail.SelectedValue == "4" || rdochoice_mine_detail.SelectedValue == "5" || rdochoice_mine_detail.SelectedValue == "1")
                        {
                            u1_corevalue[i].remark_mine = txtremarkmine_core.Text;
                        }
                        else
                        {
                            u1_corevalue[i].remark_mine = "-";
                        }
                    }
                    else
                    {
                        u1_corevalue[i].remark_mine = txtremarkmine_core.Text;
                    }
                }
            }
            else if (acidx == 2)
            {
                if (rdochoice_head_detail.SelectedValue != "0" && rdochoice_head_detail.SelectedValue != null && rdochoice_head_detail.SelectedValue != "")
                {
                    u1_corevalue[i].m0_point_mine = rdochoice_head_detail.SelectedValue.ToString();
                    u1_corevalue[i].u1_docidx = int.Parse(lblu1_docidx.Text); // เอาเข้าแทน m1_coreidx

                    if (ViewState["Org_idx_create"].ToString() == "1" || ViewState["Org_idx_create"].ToString() == "6")
                    {
                        if (rdochoice_head_detail.SelectedValue == "4" || rdochoice_head_detail.SelectedValue == "5" || rdochoice_head_detail.SelectedValue == "1")
                        {
                            u1_corevalue[i].remark_mine = txtremarkhead_core.Text;
                        }
                        else
                        {
                            u1_corevalue[i].remark_mine = "-";
                        }
                    }
                    else
                    {
                        u1_corevalue[i].remark_mine = txtremarkhead_core.Text;
                    }
                }
            }

            _dtpmform.Boxtpmu1_DocFormDetail = u1_corevalue;

            i++;
        }


        foreach (GridViewRow row in gvcompetency.Rows)
        {
            RadioButtonList rdochoicecom_mine_detail = (RadioButtonList)row.Cells[4].FindControl("rdochoicecom_mine_detail");
            RadioButtonList rdochoicecom_head_detail = (RadioButtonList)row.Cells[5].FindControl("rdochoicecom_head_detail");

            Label lblm1_typeidx = (Label)row.Cells[2].FindControl("lblm1_typeidx");
            Label lblu1_docidx = (Label)row.Cells[2].FindControl("lblu1_docidx");
            TextBox lblremark = (TextBox)row.Cells[4].FindControl("lblremark");
            TextBox txtremark_head = (TextBox)row.Cells[5].FindControl("txtremark_head");



            u1_corevalue[i] = new tpmu1_DocFormDetail();

            u1_corevalue[i].u1_docidx = int.Parse(lblu1_docidx.Text);

            if (acidx == 1)
            {
                if (rdochoicecom_mine_detail.SelectedValue != "0" && rdochoicecom_mine_detail.SelectedValue != null && rdochoicecom_mine_detail.SelectedValue != "")
                {
                    u1_corevalue[i].m0_point_mine = rdochoicecom_mine_detail.SelectedValue.ToString();

                    if (ViewState["Org_idx_create"].ToString() == "1" || ViewState["Org_idx_create"].ToString() == "6")
                    {
                        if (rdochoicecom_mine_detail.SelectedValue == "4" || rdochoicecom_mine_detail.SelectedValue == "5" || rdochoicecom_mine_detail.SelectedValue == "1")
                        {

                            u1_corevalue[i].remark_mine = lblremark.Text;

                        }
                        else
                        {
                            u1_corevalue[i].remark_mine = "-";
                        }
                    }
                    else
                    {
                        u1_corevalue[i].remark_mine = lblremark.Text;
                    }
                }

            }
            else if (acidx == 2)
            {
                if (rdochoicecom_head_detail.SelectedValue != "0" && rdochoicecom_head_detail.SelectedValue != null && rdochoicecom_head_detail.SelectedValue != "")
                {
                    u1_corevalue[i].m0_point_mine = rdochoicecom_head_detail.SelectedValue.ToString();

                    if (ViewState["Org_idx_create"].ToString() == "1" || ViewState["Org_idx_create"].ToString() == "6")
                    {
                        if (rdochoicecom_head_detail.SelectedValue == "4" || rdochoicecom_head_detail.SelectedValue == "5" || rdochoicecom_head_detail.SelectedValue == "1")
                        {
                            u1_corevalue[i].remark_mine = txtremark_head.Text;
                        }
                        else
                        {
                            u1_corevalue[i].remark_mine = "-";
                        }
                    }
                    else
                    {
                        u1_corevalue[i].remark_mine = txtremark_head.Text;
                    }
                }

            }
            _dtpmform.Boxtpmu1_DocFormDetail = u1_corevalue;

            i++;

        }

        int a = 0;


        var ds_insert_dev = (DataSet)ViewState["vsAddListTable_Dev"];
        var ds_insert_star = (DataSet)ViewState["vsAddListTable_Star"];

        var _insertdev = new tpmu2_DocFormDetail[ds_insert_dev.Tables[0].Rows.Count + ds_insert_star.Tables[0].Rows.Count];

        foreach (GridViewRow row in gvimprove_detail.Rows)
        {
            Label lblcemp_idx = (Label)row.Cells[0].FindControl("lblcemp_idx");

            if (lblcemp_idx.Text == ViewState["EmpIDX"].ToString())
            {
                count_improve = 1;
                break;
            }
            else
            {
                count_improve = 0;
            }
        }

        if (ds_insert_dev.Tables[0].Rows.Count == 0 && acidx != 3 && count_improve == 0)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูลด้านที่ต้องพัฒนา!!!');", true);
            return;
        }
        foreach (DataRow dtrow in ds_insert_dev.Tables[0].Rows)
        {
            if (dtrow["behavior"].ToString() != null)
            {
                _insertdev[a] = new tpmu2_DocFormDetail();

                string[] ToId = dtrow["m1_typeidx_comma"].ToString().Split(',');
                int x = 0;
                foreach (string To_check in ToId)
                {

                    if (x == 0)
                    {
                        _insertdev[a].m0_typeidx_choose = int.Parse(To_check);
                    }
                    else
                    {
                        _insertdev[a].behavior_name = To_check;
                    }

                    x++;
                }

                _insertdev[a].m0_typeidx = 3;
                //_insertdev[a].behavior_name = dtrow["behavior"].ToString();


                _insertdev[a].comment_name = dtrow["comment"].ToString();

            }
            a++;
            _dtpmform.Boxtpmu2_DocFormDetail = _insertdev;
            count_improve = 1;
        }


        foreach (GridViewRow row in gvstar_detail.Rows)
        {
            Label lblcemp_idx = (Label)row.Cells[0].FindControl("lblcemp_idx");

            if (lblcemp_idx.Text == ViewState["EmpIDX"].ToString())
            {
                count_star = 1;
                break;
            }
            else
            {
                count_star = 0;
            }
        }

        if (ds_insert_star.Tables[0].Rows.Count == 0 && acidx != 3 && count_star == 0)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกข้อมูลความสามารถโดดเด่น!!!');", true);
            return;
        }

        foreach (DataRow dtrow in ds_insert_star.Tables[0].Rows)
        {
            if (dtrow["behavior_star"].ToString() != null)
            {
                _insertdev[a] = new tpmu2_DocFormDetail();

                string[] ToId = dtrow["m1_typeidx_comma"].ToString().Split(',');
                int y = 0;
                foreach (string To_check in ToId)
                {

                    if (y == 0)
                    {
                        _insertdev[a].m0_typeidx_choose = int.Parse(To_check);
                    }
                    else
                    {
                        _insertdev[a].behavior_name = To_check;
                    }

                    y++;
                }
                _insertdev[a].m0_typeidx = 4;
                //_insertdev[a].behavior_name = dtrow["m1_typeidx_comma"].ToString();
                _insertdev[a].comment_name = dtrow["comment_star"].ToString();
            }

            a++;
            _dtpmform.Boxtpmu2_DocFormDetail = _insertdev;
            count_star = 1;
        }

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));
        _dtpmform = callServicePostTPMForm(_urlInsertSystem, _dtpmform);


    }

    protected void update_u2document(int u2_docidx, int cempidx, string behavior, string comment, int u0_docidx, int m0typidx)
    {
        _dtpmform = new data_tpm_form();

        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail datainsert = new tpmu0_DocFormDetail();

        datainsert.cemp_idx = cempidx;
        datainsert.condition = 3;
        datainsert.u0_docidx = u0_docidx;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = datainsert;

        _dtpmform.Boxtpmu2_DocFormDetail = new tpmu2_DocFormDetail[1];
        tpmu2_DocFormDetail dataupdate = new tpmu2_DocFormDetail();

        dataupdate.u2_docidx = u2_docidx;
        dataupdate.m0_typeidx = m0typidx;

        dataupdate.comment_name = comment;

        string[] ToId = behavior.Split(',');
        int x = 0;
        foreach (string To_check in ToId)
        {

            if (x == 0)
            {

                dataupdate.m0_typeidx_choose = int.Parse(To_check);
            }
            else
            {
                dataupdate.behavior_name = To_check;
            }

            x++;
        }

        _dtpmform.Boxtpmu2_DocFormDetail[0] = dataupdate;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));
        _dtpmform = callServicePostTPMForm(_urlInsertSystem, _dtpmform);

        if (_dtpmform.ReturnCode.ToString() == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีพฤติกรรมด้านนี้แล้ว!!!');", true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกการแก้ไขเสร็จสมบูรณ์');", true);

        }
    }

    protected void delete_u2document(int u2_docidx, int cempidx, int u0_docidx)
    {
        _dtpmform = new data_tpm_form();

        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail datainsert = new tpmu0_DocFormDetail();

        datainsert.cemp_idx = cempidx;
        datainsert.condition = 4;
        datainsert.u0_docidx = u0_docidx;
        _dtpmform.Boxtpmu0_DocFormDetail[0] = datainsert;

        _dtpmform.Boxtpmu2_DocFormDetail = new tpmu2_DocFormDetail[1];
        tpmu2_DocFormDetail dataupdate = new tpmu2_DocFormDetail();

        dataupdate.u2_docidx = u2_docidx;
        _dtpmform.Boxtpmu2_DocFormDetail[0] = dataupdate;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));
        _dtpmform = callServicePostTPMForm(_urlInsertSystem, _dtpmform);

    }

    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setChkData(CheckBoxList chkName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        chkName.Items.Clear();
        // bind items
        chkName.DataSource = obj;
        chkName.DataTextField = _data_text;
        chkName.DataValueField = _data_value;
        chkName.DataBind();
    }

    protected void setRdoData(RadioButtonList rdoName, Object obj, string _data_text, string _data_value)
    {
        rdoName.Items.Clear();
        rdoName.DataSource = obj;
        rdoName.DataTextField = _data_text;
        rdoName.DataValueField = _data_value;
        rdoName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeatData(Repeater rpName, Object obj)
    {
        rpName.DataSource = obj;
        rpName.DataBind();
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }

    #region data excel
    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName, int _set_value)
    {
        IWorkbook workbook;


        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }

        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);

        ICellStyle testeStyle = workbook.CreateCellStyle();
        testeStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Medium;
        testeStyle.FillForegroundColor = IndexedColors.BrightGreen.Index;
        testeStyle.FillPattern = FillPattern.SolidForeground;

        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }

        //set merge cell
        int max_row = dt.Rows.Count - 1;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);

            ////set merge cell
            //var cra = new NPOI.SS.Util.CellRangeAddress(1, max_row, 0, 0);
            //sheet1.AddMergedRegion(cra);

            //var cra1 = new NPOI.SS.Util.CellRangeAddress(1, max_row, 1, 1);
            //sheet1.AddMergedRegion(cra1);

            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());

                ////set export color total expenses green
                //if (_set_value == 1)
                //{
                //    if ((i + 1) == dt.Rows.Count && (dt.Rows[i][columnName].ToString() == "Total :")
                //        || ((i + 1) == dt.Rows.Count && (j + 1) == dt.Columns.Count)
                //        || ((i + 1) == dt.Rows.Count && (j == 5))
                //        )
                //    {
                //        cell.CellStyle = testeStyle;
                //    }


                //}

            }

        }

        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);

            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));

                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }


    }
    #endregion data excel


    #endregion

    #region CallService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }
    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }
    protected data_tpm_form callServicePostTPMForm(string _cmdUrl, data_tpm_form _dtpmform)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtpmform);
        // text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtpmform = (data_tpm_form)_funcTool.convertJsonToObject(typeof(data_tpm_form), _localJson);


        return _dtpmform;
    }
    #endregion

    #region SetDefault
    protected void SetDefaultpage(int page)
    {

        switch (page)
        {

            case 1: // Set Index
                _divMenuLiToViewIndex.Attributes.Add("class", "active");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewIndex);
                setOntop.Focus();
                select_index(GvIndex, int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["rdept_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()),
                    int.Parse(ViewState["JobGradeLevel"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));
                select_sumapprove(int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["rdept_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()),
                    int.Parse(ViewState["JobGradeLevel"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));
                break;

            case 2:// Set Create

                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Add("class", "active");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewInsert);
                setOntop.Focus();
                select_checkpermission(int.Parse(ViewState["EmpIDX"].ToString()));

                if (ViewState["check_permission"].ToString() == "0")
                {

                    FvDetailUser.ChangeMode(FormViewMode.Insert);
                    FvDetailUser.DataBind();
                    select_masterpoint(Gvmaster_point);
                    select_mastercorevalue(GvCoreValue);

                    select_mastercompentency(GvCompetencies, int.Parse(ViewState["TIDX"].ToString()), int.Parse(ViewState["Org_idx"].ToString()));


                    if (ViewState["rtcode_form"].ToString() == "0")
                    {
                        lblHeadForm.Text = ViewState["form_name"].ToString();// + "<br /><br />" + "แบบประเมินผลการปฏิบัติงานระดับ " + ViewState["PosGroupNameTH"].ToString();
                        mergeCell(GvCoreValue);
                        mergeCell(GvCompetencies);
                        div_saveinsert.Visible = true;
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถประเมินตนเองได้ เนื่องจากคุณไม่มีกลุ่มตำแหน่ง!!!');", true);
                        SetDefaultpage(1);
                        // div_saveinsert.Visible = false;

                    }

                    fvcomment.ChangeMode(FormViewMode.Insert);
                    fvcomment.DataBind();


                    GridView GvDevAdd = (GridView)fvcomment.FindControl("GvDevAdd");
                    CleardataSetFormList(GvDevAdd);

                    fvstar.ChangeMode(FormViewMode.Insert);
                    fvstar.DataBind();

                    GridView GvStarAdd = (GridView)fvstar.FindControl("GvStarAdd");
                    CleardataSetFormStarList(GvStarAdd);

                    //select_masterstatus(ddlstatus_insert, 1);

                    LinkButton btnAdddataset_dev = (LinkButton)fvcomment.FindControl("btnAdddataset_dev");
                    LinkButton btnAdddataset_star = (LinkButton)fvstar.FindControl("btnAdddataset_star");

                    linkBtnTrigger(btnAdddataset_dev);
                    linkBtnTrigger(btnAdddataset_star);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณไม่มีสิทธิ์ทำแบบประเมิน กรุณาติดต่อ HR');", true);
                    SetDefaultpage(1);
                }
                break;

            case 3: // Set Approve
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Add("class", "active");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewApprove);
                setOntop.Focus();

                select_approveindex(Gvapprove_index, int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["rdept_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()),
                    int.Parse(ViewState["JobGradeLevel"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));
                select_sumapprove(int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["rdept_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()),
                   int.Parse(ViewState["JobGradeLevel"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));
                break;

            case 4: // Set Detail
                _divMenuLiToViewIndex.Attributes.Add("class", "active");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewReport.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewDetail);
                setOntop.Focus();
                select_empIdx_create();
                Fvdetailusercreate.ChangeMode(FormViewMode.Insert);
                Fvdetailusercreate.DataBind();

                lblHeadForm_detail.Text = ViewState["form_name_create"].ToString();// + "<br /><br />" + "แบบประเมินผลการปฏิบัติงานระดับ " + ViewState["PosGroupNameTH_create"].ToString();
                select_masterpoint(gvpoint_view);
                select_corevalue_answer(gvcorevalue_view, int.Parse(ViewState["u0_docidx"].ToString()));
                select_competencise_answer(gvcoreompetencies_view, int.Parse(ViewState["u0_docidx"].ToString()), int.Parse(ViewState["TIDX_create"].ToString()), int.Parse(ViewState["u0_typeidx_detail"].ToString()));
                mergeCell(gvcorevalue_view);
                mergeCell(gvcoreompetencies_view);

                select_improve_detail(gvimprove_detail, int.Parse(ViewState["u0_docidx"].ToString()), 3);
                select_improve_detail(gvstar_detail, int.Parse(ViewState["u0_docidx"].ToString()), 4);

                fvimprove_edit.ChangeMode(FormViewMode.Insert);
                fvimprove_edit.DataBind();

                fvstart_edit.ChangeMode(FormViewMode.Insert);
                fvstart_edit.DataBind();

                GridView GvDevEdit = (GridView)fvimprove_edit.FindControl("GvDevEdit");
                CleardataSetFormList(GvDevEdit);


                GridView GvStarEdit = (GridView)fvstart_edit.FindControl("GvStarEdit");
                CleardataSetFormStarList(GvStarEdit);

                select_Log(rpLog, int.Parse(ViewState["u0_docidx"].ToString()));
                break;

            case 5:
                _divMenuLiToViewReport.Attributes.Add("class", "active");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewIndex.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewReport);
                getOrganizationList(ddlOrg_report);
                select_masternode(ddlnode_report);

                ddltypereport.SelectedValue = "0";
                ddlOrg_report.SelectedValue = "0";
                rdochoose_dept.ClearSelection();
                divdept.Visible = false;
                chkdept.ClearSelection();
                //ddlDep_report.SelectedValue = "0";
                ddlnode_report.SelectedValue = "0";
                txtempname_report.Text = String.Empty;
                txtempcode_report.Text = String.Empty;
                GenerateddlYear(ddlyear);
                GenerateddlYear(ddlyear_all);

                div_detail_table.Visible = false;
                div_all_table.Visible = false;

                btnexport.Visible = false;
                GvReport.Visible = false;
                GvReport_All.Visible = false;

                break;

        }
    }

    protected void GenerateddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 5; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }

        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }

    #endregion

    #region SetDefault Form

    #region 3.1 Dev
    protected void SetViewState_FormDev()
    {

        DataSet dsDevList = new DataSet();
        dsDevList.Tables.Add("dsAddListTable_Dev");
        dsDevList.Tables["dsAddListTable_Dev"].Columns.Add("m1_typeidx_comma", typeof(String));
        dsDevList.Tables["dsAddListTable_Dev"].Columns.Add("behavior", typeof(String));
        dsDevList.Tables["dsAddListTable_Dev"].Columns.Add("comment", typeof(String));

        ViewState["vsAddListTable_Dev"] = dsDevList;

    }

    protected void CleardataSetFormList(GridView GvName)
    {
        ViewState["vsAddListTable_Dev"] = null;
        GvName.DataSource = ViewState["vsAddListTable_Dev"];
        GvName.DataBind();
        SetViewState_FormDev();
    }

    protected void setAddList_FormDev(DropDownList ddlimprove, TextBox txtcomment, GridView gvName)
    {

        if (ViewState["vsAddListTable_Dev"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsAddListTable_Dev"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable_Dev"].Rows)
            {

                if (dr["behavior"].ToString() == ddlimprove.SelectedItem.Text)// && dr["comment"].ToString() == txtcomment.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีหัวข้อพฤติกรรมนี้นี้แล้ว!!!');", true);
                    return;
                }
            }

            DataRow drContacts = dsContacts.Tables["dsAddListTable_Dev"].NewRow();

            drContacts["m1_typeidx_comma"] = ddlimprove.SelectedValue;
            drContacts["behavior"] = ddlimprove.SelectedItem.Text;
            drContacts["comment"] = txtcomment.Text;

            dsContacts.Tables["dsAddListTable_Dev"].Rows.Add(drContacts);
            ViewState["vsAddListTable_Dev"] = dsContacts;
            setGridData(gvName, dsContacts.Tables["dsAddListTable_Dev"]);
            gvName.Visible = true;

        }

    }

    #endregion

    #region 3.2 Star
    protected void SetViewState_FormStar()
    {

        DataSet dsStarList = new DataSet();
        dsStarList.Tables.Add("dsAddListTable_Star");
        dsStarList.Tables["dsAddListTable_Star"].Columns.Add("m1_typeidx_comma", typeof(String));
        dsStarList.Tables["dsAddListTable_Star"].Columns.Add("behavior_star", typeof(String));
        dsStarList.Tables["dsAddListTable_Star"].Columns.Add("comment_star", typeof(String));

        ViewState["vsAddListTable_Star"] = dsStarList;

    }

    protected void CleardataSetFormStarList(GridView GvName)
    {
        ViewState["vsAddListTable_Star"] = null;
        GvName.DataSource = ViewState["vsAddListTable_Star"];
        GvName.DataBind();
        SetViewState_FormStar();
    }

    protected void setAddList_FormStar(DropDownList ddlstar, TextBox txtcomment, GridView gvName)
    {

        if (ViewState["vsAddListTable_Star"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsAddListTable_Star"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable_Star"].Rows)
            {

                if (dr["behavior_star"].ToString() == ddlstar.SelectedItem.Text)// && dr["comment_star"].ToString() == txtcomment.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีหัวข้อพฤติกรรมนี้นี้แล้ว!!!');", true);
                    return;
                }
            }

            DataRow drContacts = dsContacts.Tables["dsAddListTable_Star"].NewRow();


            drContacts["m1_typeidx_comma"] = ddlstar.SelectedValue;
            drContacts["behavior_star"] = ddlstar.SelectedItem.Text;
            drContacts["comment_star"] = txtcomment.Text;

            dsContacts.Tables["dsAddListTable_Star"].Rows.Add(drContacts);
            ViewState["vsAddListTable_Star"] = dsContacts;
            setGridData(gvName, dsContacts.Tables["dsAddListTable_Star"]);
            gvName.Visible = true;

        }

    }

    #endregion
    #endregion

    #region FormView

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvDetailUser":
                FormView FvDetailUser = (FormView)ViewInsert.FindControl("FvDetailUser");

                if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                    var txtstartdate = ((TextBox)FvDetailUser.FindControl("txtstartdate"));
                    var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txtstartdate.Text = ViewState["emp_start_date"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();
                }
                break;

            case "Fvdetailusercreate":
                FormView Fvdetailusercreate = (FormView)ViewDetail.FindControl("Fvdetailusercreate");

                if (Fvdetailusercreate.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)Fvdetailusercreate.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)Fvdetailusercreate.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)Fvdetailusercreate.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)Fvdetailusercreate.FindControl("txtsec"));
                    var txtsecidx = ((TextBox)Fvdetailusercreate.FindControl("txtsecidx"));
                    var txtpos = ((TextBox)Fvdetailusercreate.FindControl("txtpos"));
                    var txtemail = ((TextBox)Fvdetailusercreate.FindControl("txtemail"));
                    var txtstartdate = ((TextBox)Fvdetailusercreate.FindControl("txtstartdate"));
                    var txtorg = ((TextBox)Fvdetailusercreate.FindControl("txtorg"));
                    var txtorgidx = ((TextBox)Fvdetailusercreate.FindControl("txtorgidx"));

                    txtempcode.Text = ViewState["EmpCode_create"].ToString();
                    txtrequesname.Text = ViewState["FullName_create"].ToString();
                    txtorg.Text = ViewState["Org_name_create"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name_create"].ToString();
                    txtsec.Text = ViewState["Secname_create"].ToString();
                    txtpos.Text = ViewState["Positname_create"].ToString();
                    txtstartdate.Text = ViewState["emp_start_date_create"].ToString();
                    txtemail.Text = ViewState["Email_create"].ToString();
                    txtorgidx.Text = ViewState["Org_idx_create"].ToString();
                    txtsecidx.Text = ViewState["Sec_idx_create"].ToString();
                }
                break;

            case "fvcomment":
                if (fvcomment.CurrentMode == FormViewMode.Insert)
                {
                    var ddlimprove_insert = ((DropDownList)fvcomment.FindControl("ddlimprove_insert"));

                    select_mastercompentency_forimprove(ddlimprove_insert, int.Parse(ViewState["TIDX"].ToString()), int.Parse(ViewState["Org_idx"].ToString()));
                }
                break;

            case "fvstar":
                if (fvstar.CurrentMode == FormViewMode.Insert)
                {
                    var ddlstar_insert = ((DropDownList)fvstar.FindControl("ddlstar_insert"));

                    select_mastercompentency_forimprove(ddlstar_insert, int.Parse(ViewState["TIDX"].ToString()), int.Parse(ViewState["Org_idx"].ToString()));
                }
                break;

            case "fvimprove_edit":
                if (fvimprove_edit.CurrentMode == FormViewMode.Insert)
                {
                    var ddlimprove_update = ((DropDownList)fvimprove_edit.FindControl("ddlimprove_update"));

                    select_mastercompentency_forimprove(ddlimprove_update, int.Parse(ViewState["TIDX_create"].ToString()), int.Parse(ViewState["Org_idx_create"].ToString()));
                }
                break;

            case "fvstart_edit":
                if (fvstart_edit.CurrentMode == FormViewMode.Insert)
                {
                    var ddlstar_update = ((DropDownList)fvstart_edit.FindControl("ddlstar_update"));

                    select_mastercompentency_forimprove(ddlstar_update, int.Parse(ViewState["TIDX_create"].ToString()), int.Parse(ViewState["Org_idx_create"].ToString()));
                }
                break;
        }
    }
    #endregion

    #region SelectedIndexChanged
    protected void SelectedIndexChanged(object sender, EventArgs e)
    {
        if (sender is RadioButtonList)
        {
            RadioButtonList rdoname = (RadioButtonList)sender;

            switch (rdoname.ID)
            {
                case "rdochoice_mine":

                    var rdochoice_mine = (RadioButtonList)sender;
                    var rowGrid_insert = (GridViewRow)rdochoice_mine.NamingContainer;

                    Panel panel_add_comment_core = (Panel)rowGrid_insert.FindControl("panel_add_comment_core");
                    TextBox txtcore_insert = (TextBox)rowGrid_insert.FindControl("txtcore_insert");

                    if (ViewState["Org_idx"].ToString() == "1" || ViewState["Org_idx"].ToString() == "6")
                    {
                        if (rdochoice_mine.SelectedValue == "1" || rdochoice_mine.SelectedValue == "4" || rdochoice_mine.SelectedValue == "5")
                        {
                            panel_add_comment_core.Visible = true;
                            txtcore_insert.Text = String.Empty;
                        }
                        else
                        {
                            panel_add_comment_core.Visible = false;
                        }
                    }
                    else
                    {
                        panel_add_comment_core.Visible = true;
                        txtcore_insert.Text = String.Empty;
                    }

                    break;

                case "rdochoicecom_mine":
                    var rdochoicecom_mine = (RadioButtonList)sender;
                    var rowGrid_comemine_insert = (GridViewRow)rdochoicecom_mine.NamingContainer;

                    Panel panel_add_comment_competencies = (Panel)rowGrid_comemine_insert.FindControl("panel_add_comment_competencies");
                    TextBox txtcom_insert = (TextBox)rowGrid_comemine_insert.FindControl("txtcom_insert");


                    if (ViewState["Org_idx"].ToString() == "1" || ViewState["Org_idx"].ToString() == "6")
                    {
                        if (rdochoicecom_mine.SelectedValue == "1" || rdochoicecom_mine.SelectedValue == "4" || rdochoicecom_mine.SelectedValue == "5")
                        {
                            panel_add_comment_competencies.Visible = true;
                            txtcom_insert.Text = String.Empty;
                        }
                        else

                        {
                            panel_add_comment_competencies.Visible = false;
                        }
                    }
                    else
                    {
                        panel_add_comment_competencies.Visible = true;
                        txtcom_insert.Text = String.Empty;
                    }

                    break;

                case "rdochoice_mine_detail":
                    var rdochoice_mine_detail = (RadioButtonList)sender;
                    var rowGrid_coremine_update = (GridViewRow)rdochoice_mine_detail.NamingContainer;

                    Panel panel_update_comment_coremine = (Panel)rowGrid_coremine_update.FindControl("panel_update_comment_coremine");
                    TextBox txtremarkmine_core = (TextBox)rowGrid_coremine_update.FindControl("txtremarkmine_core");
                    Label lblanswer = (Label)rowGrid_coremine_update.FindControl("lblanswer");
                    Label lblremark_coremine = (Label)rowGrid_coremine_update.FindControl("lblremark_coremine");

                    int point_mine = 0;
                    decimal point_convert_coremine = 0;

                    if (ViewState["Org_idx_create"].ToString() == "1" || ViewState["Org_idx_create"].ToString() == "6")
                    {
                        if (rdochoice_mine_detail.SelectedValue == "1" || rdochoice_mine_detail.SelectedValue == "4" || rdochoice_mine_detail.SelectedValue == "5")
                        {
                            panel_update_comment_coremine.Visible = true;

                            point_convert_coremine = Convert.ToDecimal(lblanswer.Text);
                            point_mine = Decimal.ToInt32(point_convert_coremine);

                            if (point_mine == int.Parse(rdochoice_mine_detail.SelectedValue))
                            {
                                txtremarkmine_core.Text = lblremark_coremine.Text;
                            }
                            else
                            {
                                txtremarkmine_core.Text = String.Empty;

                            }
                        }
                        else

                        {
                            panel_update_comment_coremine.Visible = false;
                        }
                    }
                    else
                    {
                        panel_update_comment_coremine.Visible = true;
                        point_convert_coremine = Convert.ToDecimal(lblanswer.Text);
                        point_mine = Decimal.ToInt32(point_convert_coremine);

                        if (point_mine == int.Parse(rdochoice_mine_detail.SelectedValue))
                        {
                            txtremarkmine_core.Text = lblremark_coremine.Text;
                        }
                        else
                        {
                            txtremarkmine_core.Text = String.Empty;

                        }
                    }

                    break;

                case "rdochoicecom_mine_detail":
                    var rdochoicecom_mine_detail = (RadioButtonList)sender;
                    var rowGrid_comemine_update = (GridViewRow)rdochoicecom_mine_detail.NamingContainer;

                    Panel panel_update_comment_competenciesmine = (Panel)rowGrid_comemine_update.FindControl("panel_update_comment_competenciesmine");
                    TextBox lblremark = (TextBox)rowGrid_comemine_update.FindControl("lblremark");
                    Label lblanswer_com = (Label)rowGrid_comemine_update.FindControl("lblanswer");
                    Label lblremark_commine = (Label)rowGrid_comemine_update.FindControl("lblremark_commine");

                    int point_commine = 0;
                    decimal point_convert_commine = 0;

                    if (ViewState["Org_idx_create"].ToString() == "1" || ViewState["Org_idx_create"].ToString() == "6")
                    {
                        if (rdochoicecom_mine_detail.SelectedValue == "1" || rdochoicecom_mine_detail.SelectedValue == "4" || rdochoicecom_mine_detail.SelectedValue == "5")
                        {
                            panel_update_comment_competenciesmine.Visible = true;

                            point_convert_commine = Convert.ToDecimal(lblanswer_com.Text);
                            point_commine = Decimal.ToInt32(point_convert_commine);


                            if (point_commine == int.Parse(rdochoicecom_mine_detail.SelectedValue))
                            {
                                lblremark.Text = lblremark_commine.Text;
                            }
                            else
                            {
                                lblremark.Text = String.Empty;
                            }
                        }
                        else

                        {
                            panel_update_comment_competenciesmine.Visible = false;
                        }
                    }
                    else
                    {
                        panel_update_comment_competenciesmine.Visible = true;

                        point_convert_commine = Convert.ToDecimal(lblanswer_com.Text);
                        point_commine = Decimal.ToInt32(point_convert_commine);


                        if (point_commine == int.Parse(rdochoicecom_mine_detail.SelectedValue))
                        {
                            lblremark.Text = lblremark_commine.Text;
                        }
                        else
                        {
                            lblremark.Text = String.Empty;
                        }
                    }

                    break;

                case "rdochoice_head_detail":
                    var rdochoice_head_detail = (RadioButtonList)sender;
                    var rowGrid_corehead_update = (GridViewRow)rdochoice_head_detail.NamingContainer;

                    Panel panel_update_comment_corehead = (Panel)rowGrid_corehead_update.FindControl("panel_update_comment_corehead");
                    TextBox txtremarkhead_core = (TextBox)rowGrid_corehead_update.FindControl("txtremarkhead_core");
                    Label lblanswer_head = (Label)rowGrid_corehead_update.FindControl("lblanswer_head");
                    Label lblremark_corehead = (Label)rowGrid_corehead_update.FindControl("lblremark_corehead");

                    int point_head = 0;
                    decimal point_convert_corehead = 0;

                    if (ViewState["Org_idx_create"].ToString() == "1" || ViewState["Org_idx_create"].ToString() == "6")
                    {
                        if (rdochoice_head_detail.SelectedValue == "1" || rdochoice_head_detail.SelectedValue == "4" || rdochoice_head_detail.SelectedValue == "5")
                        {
                            panel_update_comment_corehead.Visible = true;

                            point_convert_corehead = Convert.ToDecimal(lblanswer_head.Text);
                            point_head = Decimal.ToInt32(point_convert_corehead);


                            if (point_head == int.Parse(rdochoice_head_detail.SelectedValue))
                            {
                                txtremarkhead_core.Text = lblremark_corehead.Text;

                            }
                            else
                            {
                                txtremarkhead_core.Text = String.Empty;
                            }
                        }
                        else

                        {
                            panel_update_comment_corehead.Visible = false;
                        }
                    }
                    else
                    {
                        panel_update_comment_corehead.Visible = true;

                        point_convert_corehead = Convert.ToDecimal(lblanswer_head.Text);
                        point_head = Decimal.ToInt32(point_convert_corehead);


                        if (point_head == int.Parse(rdochoice_head_detail.SelectedValue))
                        {
                            txtremarkhead_core.Text = lblremark_corehead.Text;

                        }
                        else
                        {
                            txtremarkhead_core.Text = String.Empty;
                        }
                    }
                    break;



                case "rdochoicecom_head_detail":
                    var rdochoicecom_head_detail = (RadioButtonList)sender;
                    var rowGrid_comehead_update = (GridViewRow)rdochoicecom_head_detail.NamingContainer;

                    Panel panel_update_comment_competencieshead = (Panel)rowGrid_comehead_update.FindControl("panel_update_comment_competencieshead");
                    TextBox txtremark_head = (TextBox)rowGrid_comehead_update.FindControl("txtremark_head");
                    Label lblanswer_comhead = (Label)rowGrid_comehead_update.FindControl("lblanswer_head");
                    Label lblremark_comhead = (Label)rowGrid_comehead_update.FindControl("lblremark_comhead");


                    int point_comhead = 0;
                    decimal point_convert_comhead = 0;

                    if (ViewState["Org_idx_create"].ToString() == "1" || ViewState["Org_idx_create"].ToString() == "6")
                    {
                        if (rdochoicecom_head_detail.SelectedValue == "1" || rdochoicecom_head_detail.SelectedValue == "4" || rdochoicecom_head_detail.SelectedValue == "5")
                        {
                            panel_update_comment_competencieshead.Visible = true;

                            point_convert_comhead = Convert.ToDecimal(lblanswer_comhead.Text);
                            point_comhead = Decimal.ToInt32(point_convert_comhead);


                            if (point_comhead == int.Parse(rdochoicecom_head_detail.SelectedValue))
                            {
                                txtremark_head.Text = lblremark_comhead.Text;

                            }
                            else
                            {
                                txtremark_head.Text = String.Empty;
                            }

                        }
                        else

                        {
                            panel_update_comment_competencieshead.Visible = false;
                        }
                    }
                    else
                    {
                        panel_update_comment_competencieshead.Visible = true;

                        point_convert_comhead = Convert.ToDecimal(lblanswer_comhead.Text);
                        point_comhead = Decimal.ToInt32(point_convert_comhead);


                        if (point_comhead == int.Parse(rdochoicecom_head_detail.SelectedValue))
                        {
                            txtremark_head.Text = lblremark_comhead.Text;

                        }
                        else
                        {
                            txtremark_head.Text = String.Empty;
                        }
                    }

                    break;

                case "rdochoose_dept":
                    if (rdochoose_dept.SelectedValue == "1")
                    {
                        foreach (ListItem li in chkdept.Items)
                        {
                            li.Selected = true;
                        }
                    }
                    else
                    {
                        foreach (ListItem li in chkdept.Items)
                        {
                            li.Selected = false;
                        }
                    }

                    divdept.Visible = true;


                    break;
            }

        }

        if (sender is DropDownList)
        {
            DropDownList ddlName = (DropDownList)sender;

            switch (ddlName.ID)
            {

                case "ddltypereport":
                    if (ddltypereport.SelectedValue == "1")
                    {
                        div_detail_table.Visible = true;
                        div_all_table.Visible = false;
                        GvReport_All.Visible = false;

                        ArrayList deviceStatus = new ArrayList() { "องค์กร", "ฝ่าย", "แผนก", "ตำแหน่ง", "Costcenter", "รหัสพนักงาน", "ชื่อ-สกุล(ผู้ถูกประเมิน)", "คะแนนประเมินตนเอง Core Value", "คะแนนประเมินตนเอง Competency", "ชื่อ-สกุล(ผู้มีสิทธิ์ประเมิน)", "วันที่ดำเนินการเสร็จสิ้น", "คะแนนผู้มีสิทธิ์ประเมิน Core Value", "คะแนนผู้มีสิทธิ์ประเมิน Competency", "สถานะรายการ" };

                        YrChkBoxColumns.DataSource = deviceStatus;
                        YrChkBoxColumns.DataBind();

                        foreach (ListItem li in YrChkBoxColumns.Items)
                        {
                            li.Selected = true;
                        }
                    }
                    else
                    {
                        div_all_table.Visible = true;
                        div_detail_table.Visible = false;
                        GvReport.Visible = false;
                    }

                    btnexport.Visible = false;
                    break;

                case "ddlOrg_report":
                    //getDepartmentList(ddlDep_report, int.Parse(ddlOrg_report.SelectedValue));
                    if (ddlOrg_report.SelectedValue != "0")
                    {
                        // divdept.Visible = true;
                        select_department(chkdept, int.Parse(ddlOrg_report.SelectedValue));
                    }
                    else
                    {
                        //  divdept.Visible = false;
                        rdochoose_dept.ClearSelection();
                        chkdept.ClearSelection();
                    }

                    rdochoose_dept.ClearSelection();
                    divdept.Visible = false;
                    break;
            }
        }
    }
    #endregion

    #region Gridview

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gvmaster_point":
            case "gvpoint_view":

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Cells[0].Text = "คะแนน" + "<br />" + "Score";
                    e.Row.Cells[1].Text = "ความหมาย" + "<br />" + "Rating Name";
                    e.Row.Cells[2].Text = "แนวทางของเกณฑ์ประเมินพฤติกรรม" + "<br />" + "Definition";
                }

                break;
            case "GvCoreValue":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblm1_coreidx = (Label)e.Row.FindControl("lblm1_coreidx");
                    GridView GvSub_CoreValue = (GridView)e.Row.FindControl("GvSub_CoreValue");
                    RadioButtonList rdochoice_mine = (RadioButtonList)e.Row.FindControl("rdochoice_mine");
                    RadioButtonList rdochoice_head = (RadioButtonList)e.Row.FindControl("rdochoice_head");

                    int rowindex = e.Row.RowIndex;
                    count++;

                    e.Row.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml("#DAEEF3");
                    e.Row.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml("#DAEEF3");

                    select_mastersubcorevalue(GvSub_CoreValue, int.Parse(lblm1_coreidx.Text));

                    _dtpmform = new data_tpm_form();
                    _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
                    tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

                    dataselect.condition = 8;

                    _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

                    _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);

                    setRdoData(rdochoice_mine, _dtpmform.Boxtpmm0_DocFormDetail, "namechoice", "m0_point");

                }

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].ColumnSpan = 3;
                    e.Row.Cells[1].Visible = false;
                    e.Row.Cells[2].Visible = false;

                    e.Row.Cells[0].Text = "ปัจจัยประเมิน" + "<br />" + "Core Values Factor";
                    e.Row.Cells[1].Text = "ปัจจัยประเมิน" + "<br />" + "Core Values Factor";
                    e.Row.Cells[2].Text = "ปัจจัยประเมิน" + "<br />" + "Core Values Factor";
                    e.Row.Cells[3].Text = "พฤติกรรมการทำงานที่บริษัทคาดหวัง" + "<br />" + "Behavioral Indicators";
                    e.Row.Cells[4].Text = "ประเมินตนเอง" + "<br />" + "Self Assessment (1-5)";
                }
                break;

            case "gvcorevalue_view":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblm1_coreidx = (Label)e.Row.FindControl("lblm1_coreidx");
                    GridView gvsubcorevalue_view = (GridView)e.Row.FindControl("gvsubcorevalue_view");
                    RadioButtonList rdochoice_mine_detail = (RadioButtonList)e.Row.FindControl("rdochoice_mine_detail");
                    RadioButtonList rdochoice_head_detail = (RadioButtonList)e.Row.FindControl("rdochoice_head_detail");
                    Panel panel_update_comment_coremine = (Panel)e.Row.FindControl("panel_update_comment_coremine");
                    Panel panel_update_comment_corehead = (Panel)e.Row.FindControl("panel_update_comment_corehead");
                    Label lblanswer = (Label)e.Row.FindControl("lblanswer");
                    Label lblanswer_head = (Label)e.Row.FindControl("lblanswer_head");

                    int point_convert_mine;
                    decimal point_mine;

                    int point_convert_head;
                    decimal point_head;

                    int rowindex = e.Row.RowIndex;
                    count++;

                    e.Row.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml("#DAEEF3");
                    e.Row.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml("#DAEEF3");


                    point_mine = Convert.ToDecimal(lblanswer.Text);
                    point_convert_mine = Decimal.ToInt32(point_mine);


                    point_head = Convert.ToDecimal(lblanswer_head.Text);
                    point_convert_head = Decimal.ToInt32(point_head);

                    select_mastersubcorevalue(gvsubcorevalue_view, int.Parse(lblm1_coreidx.Text));

                    _dtpmform = new data_tpm_form();
                    _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
                    tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

                    dataselect.condition = 8;

                    _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

                    _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);

                    setRdoData(rdochoice_mine_detail, _dtpmform.Boxtpmm0_DocFormDetail, "namechoice", "m0_point");
                    setRdoData(rdochoice_head_detail, _dtpmform.Boxtpmm0_DocFormDetail, "namechoice", "m0_point");


                    rdochoice_mine_detail.SelectedValue = point_convert_mine.ToString();
                    rdochoice_head_detail.SelectedValue = point_convert_head.ToString();

                    if (ViewState["unidx"].ToString() == "1" && ViewState["cemp_idx_u0"].ToString() == ViewState["EmpIDX"].ToString() && ViewState["History_Detail"].ToString() != "3") //ViewState["History_Detail"].ToString() == "2" ||
                    {
                        rdochoice_mine_detail.Enabled = true;
                    }
                    else
                    {
                        rdochoice_mine_detail.Enabled = false;
                    }

                    if (int.Parse(ViewState["unidx"].ToString()) > 2 || int.Parse(ViewState["unidx"].ToString()) == 2 && ViewState["History_Detail"].ToString() != "2")
                    {
                        rdochoice_head_detail.Enabled = false;

                    }
                    else
                    {
                        rdochoice_head_detail.Enabled = true;
                    }

                    if (ViewState["Org_idx_create"].ToString() == "1" || ViewState["Org_idx_create"].ToString() == "6")
                    {
                        if (rdochoice_mine_detail.SelectedValue == "1" || rdochoice_mine_detail.SelectedValue == "4" || rdochoice_mine_detail.SelectedValue == "5")
                        {
                            if (ViewState["unidx"].ToString() == "1" && ViewState["cemp_idx_u0"].ToString() == ViewState["EmpIDX"].ToString())
                            {
                                if (ViewState["History_Detail"].ToString() != "3")
                                {
                                    panel_update_comment_coremine.Enabled = true;
                                    panel_update_comment_coremine.Visible = true;
                                }
                                else
                                {
                                    panel_update_comment_coremine.Enabled = false;
                                    panel_update_comment_coremine.Visible = true;
                                }
                            }
                            else if (ViewState["unidx"].ToString() == "1" && ViewState["cemp_idx_u0"].ToString() != ViewState["EmpIDX"].ToString())
                            {
                                panel_update_comment_coremine.Enabled = false;
                                panel_update_comment_coremine.Visible = true;

                            }
                            else if (int.Parse(ViewState["unidx"].ToString()) == 2 && ViewState["History_Detail"].ToString() == "2")
                            {
                                panel_update_comment_coremine.Enabled = false;
                                panel_update_comment_coremine.Visible = true;
                            }
                            else if (int.Parse(ViewState["unidx"].ToString()) == 2 && ViewState["History_Detail"].ToString() != "2")
                            {
                                panel_update_comment_coremine.Enabled = false;
                                panel_update_comment_coremine.Visible = true;
                            }
                            else if (int.Parse(ViewState["unidx"].ToString()) > 2)
                            {
                                panel_update_comment_coremine.Enabled = false;
                                panel_update_comment_coremine.Visible = true;
                            }

                        }

                        if (rdochoice_head_detail.SelectedValue == "1" || rdochoice_head_detail.SelectedValue == "4" || rdochoice_head_detail.SelectedValue == "5")
                        {
                            if (int.Parse(ViewState["unidx"].ToString()) == 2 && ViewState["History_Detail"].ToString() == "2")
                            {
                                panel_update_comment_corehead.Visible = true;
                            }
                            else if (int.Parse(ViewState["unidx"].ToString()) == 2 && ViewState["History_Detail"].ToString() != "2")
                            {
                                panel_update_comment_corehead.Visible = true;
                                panel_update_comment_corehead.Enabled = false;
                            }
                            else if (int.Parse(ViewState["unidx"].ToString()) > 2)
                            {
                                panel_update_comment_corehead.Enabled = false;
                                panel_update_comment_corehead.Visible = true;
                            }

                        }
                    }
                    else
                    {
                        if (ViewState["unidx"].ToString() == "1" && ViewState["cemp_idx_u0"].ToString() == ViewState["EmpIDX"].ToString())
                        {
                            if (ViewState["History_Detail"].ToString() != "3")
                            {
                                panel_update_comment_coremine.Enabled = true;
                                panel_update_comment_coremine.Visible = true;
                            }
                            else
                            {
                                panel_update_comment_coremine.Enabled = false;
                                panel_update_comment_coremine.Visible = true;
                            }
                        }
                        else if (ViewState["unidx"].ToString() == "1" && ViewState["cemp_idx_u0"].ToString() != ViewState["EmpIDX"].ToString())
                        {

                            panel_update_comment_coremine.Enabled = false;
                            panel_update_comment_coremine.Visible = true;

                        }
                        else if (int.Parse(ViewState["unidx"].ToString()) == 2 && ViewState["History_Detail"].ToString() == "2" || ViewState["History_Detail"].ToString() != "2")
                        {
                            panel_update_comment_coremine.Enabled = false;
                            panel_update_comment_coremine.Visible = true;
                        }
                        //else if (int.Parse(ViewState["unidx"].ToString()) == 2 && ViewState["History_Detail"].ToString() == "1")
                        //{
                        //    panel_update_comment_coremine.Enabled = false;
                        //    panel_update_comment_coremine.Visible = true;
                        //}

                        if (int.Parse(ViewState["unidx"].ToString()) == 2 && ViewState["History_Detail"].ToString() == "2")
                        {
                            if (point_convert_head != 0)
                            {
                                panel_update_comment_corehead.Visible = true;
                            }
                            else
                            {
                                panel_update_comment_corehead.Visible = false;
                            }
                        }
                        else if (int.Parse(ViewState["unidx"].ToString()) == 2 && ViewState["History_Detail"].ToString() != "2")
                        {
                            panel_update_comment_corehead.Visible = true;
                            panel_update_comment_corehead.Enabled = false;
                        }
                        else if (int.Parse(ViewState["unidx"].ToString()) > 2)
                        {
                            panel_update_comment_coremine.Enabled = false;
                            panel_update_comment_coremine.Visible = true;
                            panel_update_comment_corehead.Enabled = false;
                            panel_update_comment_corehead.Visible = true;
                        }

                    }

                    if (ViewState["History_Detail"].ToString() == "2" && int.Parse(ViewState["unidx"].ToString()) >= 2
                        || ViewState["History_Detail"].ToString() != "2" && (int.Parse(ViewState["unidx"].ToString()) > 2 || ViewState["staidx"].ToString() == "1" && ViewState["unidx"].ToString() != "1" && ViewState["sum_head_core_value"].ToString() != "0"))
                    {
                        gvcorevalue_view.Columns[5].Visible = true;
                    }
                    else
                    {
                        gvcorevalue_view.Columns[5].Visible = false;
                    }

                }

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].ColumnSpan = 3;
                    e.Row.Cells[1].Visible = false;
                    e.Row.Cells[2].Visible = false;

                    e.Row.Cells[0].Text = "ปัจจัยประเมิน" + "<br />" + "Core Values Factor";
                    e.Row.Cells[1].Text = "ปัจจัยประเมิน" + "<br />" + "Core Values Factor";
                    e.Row.Cells[2].Text = "ปัจจัยประเมิน" + "<br />" + "Core Values Factor";
                    e.Row.Cells[3].Text = "พฤติกรรมการทำงานที่บริษัทคาดหวัง" + "<br />" + "Behavioral Indicators";
                    e.Row.Cells[4].Text = "ประเมินตนเอง" + "<br />" + "Self Assessment (1-5)";
                    e.Row.Cells[5].Text = "หัวหน้าประเมิน" + "<br />" + "Superior's Rating(1-5)";
                }
                break;

            case "GvCompetencies":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblu0_typeidx = (Label)e.Row.FindControl("lblu0_typeidx");
                    Label lblm1_typeidx = (Label)e.Row.FindControl("lblm1_typeidx");
                    Label lblform_name = (Label)e.Row.FindControl("lblform_name");
                    Label lblno = (Label)e.Row.FindControl("lblno");

                    GridView GvSub_Competencies = (GridView)e.Row.FindControl("GvSub_Competencies");
                    RadioButtonList rdochoicecom_mine = (RadioButtonList)e.Row.FindControl("rdochoicecom_mine");
                    RadioButtonList rdochoicecom_head = (RadioButtonList)e.Row.FindControl("rdochoicecom_head");
                    count++;
                    lblno.Text = count.ToString();
                    e.Row.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml("#E4DFEC");
                    e.Row.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml("#E4DFEC");


                    ViewState["form_name"] = lblform_name.Text;
                    ViewState["u0_typeidx"] = lblu0_typeidx.Text;

                    select_mastersubcompentency(GvSub_Competencies, int.Parse(lblu0_typeidx.Text), int.Parse(lblm1_typeidx.Text));


                    _dtpmform = new data_tpm_form();
                    _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
                    tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

                    dataselect.condition = 8;

                    _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

                    _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);

                    setRdoData(rdochoicecom_mine, _dtpmform.Boxtpmm0_DocFormDetail, "namechoice", "m0_point");
                }

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].ColumnSpan = 3;
                    e.Row.Cells[1].Visible = false;
                    e.Row.Cells[2].Visible = false;

                    e.Row.Cells[0].Text = "ปัจจัยประเมิน" + "<br />" + "Competency Factor";
                    e.Row.Cells[1].Text = "ปัจจัยประเมิน" + "<br />" + "Competency Factor";
                    e.Row.Cells[2].Text = "ปัจจัยประเมิน" + "<br />" + "Competency Factor";
                    e.Row.Cells[3].Text = "พฤติกรรมการทำงานที่บริษัทคาดหวัง" + "<br />" + "Behavioral Indicators";
                    e.Row.Cells[4].Text = "ประเมินตนเอง" + "<br />" + "Self Assessment (1-5)";

                }
                break;

            case "gvcoreompetencies_view":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblu0_typeidx = (Label)e.Row.FindControl("lblu0_typeidx");
                    Label lblm1_typeidx = (Label)e.Row.FindControl("lblm1_typeidx");
                    Label lblform_name = (Label)e.Row.FindControl("lblform_name");
                    Label lblno = (Label)e.Row.FindControl("lblno");
                    Label lblanswer = (Label)e.Row.FindControl("lblanswer");
                    Label lblanswer_head = (Label)e.Row.FindControl("lblanswer_head");
                    GridView gvsubcompetencies_view = (GridView)e.Row.FindControl("gvsubcompetencies_view");
                    RadioButtonList rdochoicecom_mine_detail = (RadioButtonList)e.Row.FindControl("rdochoicecom_mine_detail");
                    RadioButtonList rdochoicecom_head_detail = (RadioButtonList)e.Row.FindControl("rdochoicecom_head_detail");
                    Panel panel_update_comment_competenciesmine = (Panel)e.Row.FindControl("panel_update_comment_competenciesmine");
                    Panel panel_update_comment_competencieshead = (Panel)e.Row.FindControl("panel_update_comment_competencieshead");

                    int point_convert_mine;
                    decimal point_mine;

                    int point_convert_head;
                    decimal point_head;

                    point_mine = Convert.ToDecimal(lblanswer.Text);
                    point_convert_mine = Decimal.ToInt32(point_mine);


                    point_head = Convert.ToDecimal(lblanswer_head.Text);
                    point_convert_head = Decimal.ToInt32(point_head);

                    count++;
                    lblno.Text = count.ToString();
                    e.Row.Cells[0].BackColor = System.Drawing.ColorTranslator.FromHtml("#E4DFEC");
                    e.Row.Cells[2].BackColor = System.Drawing.ColorTranslator.FromHtml("#E4DFEC");


                    select_mastersubcompentency(gvsubcompetencies_view, int.Parse(lblu0_typeidx.Text), int.Parse(lblm1_typeidx.Text));


                    _dtpmform = new data_tpm_form();
                    _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
                    tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

                    dataselect.condition = 8;

                    _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

                    _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);

                    setRdoData(rdochoicecom_mine_detail, _dtpmform.Boxtpmm0_DocFormDetail, "namechoice", "m0_point");
                    setRdoData(rdochoicecom_head_detail, _dtpmform.Boxtpmm0_DocFormDetail, "namechoice", "m0_point");

                    rdochoicecom_mine_detail.SelectedValue = point_convert_mine.ToString();
                    rdochoicecom_head_detail.SelectedValue = point_convert_head.ToString();


                    if (ViewState["unidx"].ToString() == "1" && ViewState["cemp_idx_u0"].ToString() == ViewState["EmpIDX"].ToString() && ViewState["History_Detail"].ToString() != "3") //ViewState["History_Detail"].ToString() == "2" ||
                    {
                        rdochoicecom_mine_detail.Enabled = true;
                    }
                    else
                    {
                        rdochoicecom_mine_detail.Enabled = false;
                    }

                    if (ViewState["Org_idx_create"].ToString() == "1" || ViewState["Org_idx_create"].ToString() == "6")
                    {
                        if (rdochoicecom_mine_detail.SelectedValue == "1" || rdochoicecom_mine_detail.SelectedValue == "4" || rdochoicecom_mine_detail.SelectedValue == "5")
                        {
                            if (int.Parse(ViewState["unidx"].ToString()) == 1 && ViewState["cemp_idx_u0"].ToString() == ViewState["EmpIDX"].ToString()) //ViewState["History_Detail"].ToString() == "2" 
                            {
                                if (ViewState["History_Detail"].ToString() != "3")
                                {
                                    panel_update_comment_competenciesmine.Enabled = true;
                                    panel_update_comment_competenciesmine.Visible = true;
                                }
                                else
                                {
                                    panel_update_comment_competenciesmine.Enabled = false;
                                    panel_update_comment_competenciesmine.Visible = true;
                                }

                                //panel_update_comment_competenciesmine.Enabled = true;
                                //panel_update_comment_competenciesmine.Visible = true;
                            }
                            else if (ViewState["unidx"].ToString() == "1" && ViewState["cemp_idx_u0"].ToString() != ViewState["EmpIDX"].ToString())
                            {
                                panel_update_comment_competenciesmine.Enabled = false;
                                panel_update_comment_competenciesmine.Visible = true;

                            }
                            //else if (int.Parse(ViewState["unidx"].ToString()) == 1 && ViewState["History_Detail"].ToString() == "1")
                            //{
                            //    panel_update_comment_competenciesmine.Visible = true;
                            //    panel_update_comment_competenciesmine.Enabled = false;
                            //}
                            else if (int.Parse(ViewState["unidx"].ToString()) == 2 && (ViewState["History_Detail"].ToString() == "2" || ViewState["History_Detail"].ToString() != "2"))
                            {
                                panel_update_comment_competenciesmine.Enabled = false;
                                panel_update_comment_competenciesmine.Visible = true;
                            }
                            else if (int.Parse(ViewState["unidx"].ToString()) > 2)
                            {
                                panel_update_comment_competenciesmine.Enabled = false;
                                panel_update_comment_competenciesmine.Visible = true;
                            }

                        }

                        if (rdochoicecom_head_detail.SelectedValue == "1" || rdochoicecom_head_detail.SelectedValue == "4" || rdochoicecom_head_detail.SelectedValue == "5")
                        {
                            //if (int.Parse(ViewState["unidx"].ToString()) == 1 && ViewState["cemp_idx_u0"].ToString() == ViewState["EmpIDX"].ToString()) //if (int.Parse(ViewState["unidx"].ToString()) == 2 && ViewState["History_Detail"].ToString() == "2")
                            if (int.Parse(ViewState["unidx"].ToString()) == 2 && ViewState["History_Detail"].ToString() == "2")
                            {
                                panel_update_comment_competencieshead.Visible = true;
                            }
                            else if (int.Parse(ViewState["unidx"].ToString()) == 2 && ViewState["History_Detail"].ToString() != "2")
                            {
                                panel_update_comment_competencieshead.Visible = true;
                                panel_update_comment_competencieshead.Enabled = false;
                            }
                            else if (int.Parse(ViewState["unidx"].ToString()) > 2)
                            {
                                panel_update_comment_competencieshead.Enabled = false;
                                panel_update_comment_competencieshead.Visible = true;
                            }

                        }
                    }
                    else
                    {
                        if (int.Parse(ViewState["unidx"].ToString()) == 1 && ViewState["cemp_idx_u0"].ToString() == ViewState["EmpIDX"].ToString()) //ViewState["History_Detail"].ToString() == "2" 
                        {
                            if (ViewState["History_Detail"].ToString() != "3")
                            {
                                panel_update_comment_competenciesmine.Enabled = true;
                                panel_update_comment_competenciesmine.Visible = true;
                            }
                            else
                            {
                                panel_update_comment_competenciesmine.Enabled = false;
                                panel_update_comment_competenciesmine.Visible = true;
                            }
                            //panel_update_comment_competenciesmine.Enabled = true;
                            //panel_update_comment_competenciesmine.Visible = true;
                        }
                        else if (int.Parse(ViewState["unidx"].ToString()) == 1 && ViewState["cemp_idx_u0"].ToString() != ViewState["EmpIDX"].ToString())
                        {
                            panel_update_comment_competenciesmine.Enabled = false;
                            panel_update_comment_competenciesmine.Visible = true;
                        }
                        else if (int.Parse(ViewState["unidx"].ToString()) == 2 && (ViewState["History_Detail"].ToString() == "2" || ViewState["History_Detail"].ToString() != "2"))
                        {
                            panel_update_comment_competenciesmine.Enabled = false;
                            panel_update_comment_competenciesmine.Visible = true;
                        }


                        if (int.Parse(ViewState["unidx"].ToString()) == 2 && ViewState["History_Detail"].ToString() == "2")
                        {
                            if (point_convert_head != 0)
                            {
                                panel_update_comment_competencieshead.Visible = true;
                            }
                            else
                            {
                                panel_update_comment_competencieshead.Visible = false;
                            }

                        }
                        else if (int.Parse(ViewState["unidx"].ToString()) == 2 && ViewState["History_Detail"].ToString() != "2")
                        {
                            panel_update_comment_competencieshead.Visible = true;
                            panel_update_comment_competencieshead.Enabled = false;
                        }
                        else if (int.Parse(ViewState["unidx"].ToString()) > 2)
                        {
                            panel_update_comment_competenciesmine.Enabled = false;
                            panel_update_comment_competenciesmine.Visible = true;
                            panel_update_comment_competencieshead.Enabled = false;
                            panel_update_comment_competencieshead.Visible = true;
                        }
                    }


                    if (ViewState["History_Detail"].ToString() == "2" && int.Parse(ViewState["unidx"].ToString()) >= 2
                        || ViewState["History_Detail"].ToString() != "2" && (int.Parse(ViewState["unidx"].ToString()) > 2 || ViewState["staidx"].ToString() == "1" && ViewState["unidx"].ToString() != "1" && ViewState["sum_head_competency"].ToString() != "0"))
                    {
                        gvcoreompetencies_view.Columns[5].Visible = true;
                    }
                    else
                    {
                        gvcoreompetencies_view.Columns[5].Visible = false;
                    }


                    //if (ViewState["unidx"].ToString() == "1" && ViewState["cemp_idx_u0"].ToString() == ViewState["EmpIDX"].ToString()) // || ViewState["History_Detail"].ToString() == "2")
                    //{
                    //    rdochoicecom_mine_detail.Enabled = true;

                    //}
                    //else
                    //{
                    //    rdochoicecom_mine_detail.Enabled = false;

                    //}

                    if (int.Parse(ViewState["unidx"].ToString()) > 2 || int.Parse(ViewState["unidx"].ToString()) == 2 && ViewState["History_Detail"].ToString() != "2")
                    {
                        rdochoicecom_head_detail.Enabled = false;
                    }
                    else
                    {
                        rdochoicecom_head_detail.Enabled = true;
                    }
                }

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].ColumnSpan = 3;
                    e.Row.Cells[1].Visible = false;
                    e.Row.Cells[2].Visible = false;

                    e.Row.Cells[0].Text = "ปัจจัยประเมิน" + "<br />" + "Competency Factor";
                    e.Row.Cells[1].Text = "ปัจจัยประเมิน" + "<br />" + "Competency Factor";
                    e.Row.Cells[2].Text = "ปัจจัยประเมิน" + "<br />" + "Competency Factor";
                    e.Row.Cells[3].Text = "พฤติกรรมการทำงานที่บริษัทคาดหวัง" + "<br />" + "Behavioral Indicators";
                    e.Row.Cells[4].Text = "ประเมินตนเอง" + "<br />" + "Self Assessment (1-5)";
                    e.Row.Cells[5].Text = "หัวหน้าประเมิน" + "<br />" + "Superior's Rating(1-5)";
                }
                break;

            case "GvIndex":
            case "Gvapprove_index":
                // case "GvReport":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblunidx = (Label)e.Row.FindControl("lblunidx");
                    Label lblstaidx = (Label)e.Row.FindControl("lblstaidx");
                    Label lblStatusDoc = (Label)e.Row.FindControl("lblStatusDoc");

                    switch (int.Parse(lblunidx.Text))
                    {
                        case 1:
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff8c1a");
                            break;
                        case 2:

                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#cc66ff");
                            break;
                        case 3:

                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#996600");
                            break;
                        case 4:
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#F08080");

                            break;

                        case 5:
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#32CD32");

                            break;
                    }
                }
                break;

            case "GvDevAdd":
            case "GvStarAdd":
            case "GvDevEdit":
            case "GvStarEdit":

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[1].Text = "ชื่อพฤติกรรม/ปัจจัย" + "<br />" + "(Behavior/Factor Name)";
                    e.Row.Cells[2].Text = "ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" + "<br />" + "Comment & Development Actions Plan (please identify action, timeline and person in charge)";
                }
                break;
            case "gvimprove_detail":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (gvimprove_detail.EditIndex != e.Row.RowIndex)
                    {
                        Label lblcemp_idx = (Label)e.Row.FindControl("lblcemp_idx");
                        LinkButton Edit_improve = (LinkButton)e.Row.FindControl("Edit_improve");
                        LinkButton Delete_improve = (LinkButton)e.Row.FindControl("Delete_improve");

                        // if (ViewState["History_Detail"].ToString() == "2")
                        //if(ViewState["unidx"].ToString() == "1" && ViewState["acidx"].ToString() == "1")
                        // {
                        //txt.Text = ViewState["History_Detail"].ToString() + "," + ViewState["acidx"].ToString() + "," + ViewState["unidx"].ToString() + "," + lblcemp_idx.Text;

                        if (lblcemp_idx.Text == ViewState["EmpIDX"].ToString() && ViewState["acidx"].ToString() == "1" && ViewState["unidx"].ToString() == "1" && ViewState["History_Detail"].ToString() != "3")

                        {
                            if (ViewState["Org_idx"].ToString() != "1" && ViewState["Org_idx"].ToString() != "6")
                            {
                                gvimprove_detail.Columns[2].Visible = true;
                                Edit_improve.Visible = true;
                                Delete_improve.Visible = true;
                            }
                            else
                            {
                                Edit_improve.Visible = false;
                                Delete_improve.Visible = false;
                                gvimprove_detail.Columns[2].Visible = false;
                            }
                        }
                        else if (ViewState["History_Detail"].ToString() == "2" && lblcemp_idx.Text == ViewState["EmpIDX"].ToString() && ViewState["unidx"].ToString() == "2" && ViewState["acidx"].ToString() == "2")
                        {
                            gvimprove_detail.Columns[2].Visible = true;
                            Edit_improve.Visible = true;
                            Delete_improve.Visible = true;
                        }
                        else
                        {
                            Edit_improve.Visible = false;
                            Delete_improve.Visible = false;
                            gvimprove_detail.Columns[2].Visible = false;
                        }
                        //}
                        //else
                        //{
                        //    gvimprove_detail.Columns[2].Visible = false;
                        //}
                    }

                    if (gvimprove_detail.EditIndex == e.Row.RowIndex)
                    {
                        TextBox lblm0_typeidx_choose_edit = (TextBox)e.Row.FindControl("lblm0_typeidx_choose_edit");
                        TextBox txtbehavior_edit = (TextBox)e.Row.FindControl("txtbehavior_edit");
                        DropDownList ddlimprove_edit = (DropDownList)e.Row.FindControl("ddlimprove_edit");
                        string value = "0";

                        select_mastercompentency_forimprove(ddlimprove_edit, int.Parse(ViewState["TIDX_create"].ToString()), int.Parse(ViewState["Org_idx_create"].ToString()));
                        value = lblm0_typeidx_choose_edit.Text + "," + txtbehavior_edit.Text;
                        ddlimprove_edit.SelectedValue = value;

                        //txt.Text = value;

                    }
                }

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].Text = "ชื่อพฤติกรรม/ปัจจัย" + "<br />" + "(Behavior/Factor Name)";
                    e.Row.Cells[1].Text = "ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" + "<br />" + "Comment & Development Actions Plan (please identify action, timeline and person in charge)";
                }
                break;

            case "gvstar_detail":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (gvstar_detail.EditIndex != e.Row.RowIndex)
                    {
                        Label lblcemp_idx = (Label)e.Row.FindControl("lblcemp_idx");
                        LinkButton Edit_star = (LinkButton)e.Row.FindControl("Edit_star");
                        LinkButton Delete_star = (LinkButton)e.Row.FindControl("Delete_star");

                        //if (ViewState["History_Detail"].ToString() == "2")
                        //{
                        //    gvstar_detail.Columns[2].Visible = true;

                        //if (lblcemp_idx.Text == ViewState["EmpIDX"].ToString())
                        if (lblcemp_idx.Text == ViewState["EmpIDX"].ToString() && ViewState["acidx"].ToString() == "1" && ViewState["unidx"].ToString() == "1" & ViewState["History_Detail"].ToString() != "3")

                        {
                            if (ViewState["Org_idx"].ToString() != "1" && ViewState["Org_idx"].ToString() != "6")
                            {
                                gvstar_detail.Columns[2].Visible = true;
                                Edit_star.Visible = true;
                                Delete_star.Visible = true;
                            }
                            else
                            {
                                gvstar_detail.Columns[2].Visible = false;
                                Edit_star.Visible = false;
                                Delete_star.Visible = false;
                            }
                        }
                        else if (ViewState["History_Detail"].ToString() == "2" && lblcemp_idx.Text == ViewState["EmpIDX"].ToString() && ViewState["unidx"].ToString() == "2" && ViewState["acidx"].ToString() == "2")
                        {
                            gvstar_detail.Columns[2].Visible = true;
                            Edit_star.Visible = true;
                            Delete_star.Visible = true;
                        }
                        else
                        {
                            gvstar_detail.Columns[2].Visible = false;
                            Edit_star.Visible = false;
                            Delete_star.Visible = false;
                        }
                        //}
                        //else
                        //{
                        //    gvstar_detail.Columns[2].Visible = false;
                        //}
                    }

                    if (gvstar_detail.EditIndex == e.Row.RowIndex)
                    {
                        TextBox lblm0_typeidx_choose_edit = (TextBox)e.Row.FindControl("lblm0_typeidx_choose_edit");
                        TextBox txtbehavior_edit = (TextBox)e.Row.FindControl("txtbehavior_edit");
                        DropDownList ddlstar_edit = (DropDownList)e.Row.FindControl("ddlstar_edit");
                        string value = "0";

                        select_mastercompentency_forimprove(ddlstar_edit, int.Parse(ViewState["TIDX_create"].ToString()), int.Parse(ViewState["Org_idx_create"].ToString()));
                        value = lblm0_typeidx_choose_edit.Text + "," + txtbehavior_edit.Text;
                        ddlstar_edit.SelectedValue = value;


                    }
                }

                if (e.Row.RowType == DataControlRowType.Header)
                {

                    e.Row.Cells[0].Text = "ชื่อพฤติกรรม/ปัจจัย" + "<br />" + "(Behavior/Factor Name)";
                    e.Row.Cells[1].Text = "ข้อคิดเห็น & แผนพัฒนา (โปรดระบุขั้นตอนกิจกรรม กำหนดเวลา และผู้รับผิดชอบ)" + "<br />" + "Comment & Development Actions Plan (please identify action, timeline and person in charge)";
                }
                break;

            case "GvReport_All":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblrdept_idx = (Label)e.Row.Cells[0].FindControl("lblrdept_idx");
                    GridView GvDetailSec_Report = (GridView)e.Row.Cells[1].FindControl("GvDetailSec_Report");


                    _dtpmform = new data_tpm_form();
                    _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
                    tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

                    dataselect.condition = 11;
                    dataselect.rdept_idx = int.Parse(lblrdept_idx.Text);
                    dataselect.getyear = int.Parse(ddlyear_all.SelectedValue);

                    _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

                    _dtpmform = callServicePostTPMForm(_urlSelectSystem, _dtpmform);
                    ViewState["Data_ReportTable"] = _dtpmform.Boxtpmu0_DocFormDetail;
                    setGridData(GvDetailSec_Report, _dtpmform.Boxtpmu0_DocFormDetail);

                }
                break;

            case "GvDetailSec_Report":

                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    Label lit_total_qty = (Label)e.Row.FindControl("lit_total_qty");
                    Label lit_total_complete = (Label)e.Row.FindControl("lit_total_complete");
                    Label lit_total_uncomplete = (Label)e.Row.FindControl("lit_total_uncomplete");

                    int sum = 0;
                    int complete = 0;
                    int uncomplete = 0;


                    tpmu0_DocFormDetail[] _item_TotalQTY = (tpmu0_DocFormDetail[])ViewState["Data_ReportTable"];

                    var _linqTotalQty = (from data in _item_TotalQTY
                                         select new
                                         {
                                             data.qty,
                                             data.complete,
                                             data.uncomplete

                                         }).ToList();

                    foreach (var item in _linqTotalQty)
                    {
                        sum += item.qty;
                        complete += item.complete;
                        uncomplete += item.uncomplete;
                    }


                    lit_total_qty.Text = sum.ToString();
                    lit_total_complete.Text = complete.ToString();
                    lit_total_uncomplete.Text = uncomplete.ToString();
                }
                break;
            case "GvReport":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblunidx = (Label)e.Row.FindControl("lblunidx");
                    Label lblstaidx = (Label)e.Row.FindControl("lblstaidx");
                    Label lblStatusDoc = (Label)e.Row.FindControl("lblStatusDoc");

                    switch (int.Parse(lblunidx.Text))
                    {
                        case 1:
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff8c1a");
                            break;
                        case 2:

                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#cc66ff");
                            break;
                        case 3:

                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#996600");
                            break;
                        case 4:
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#F08080");

                            break;

                        case 5:
                            lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#32CD32");

                            break;
                    }

                    if (ViewState["Columns_Check_Search"].ToString() != "0")
                    {
                        string To = ViewState["Columns_Check_Search"].ToString();
                        string[] ToId = To.Split(',');
                        int _i = 0;
                        foreach (string ToColumn in ToId)
                        {
                            if (ToColumn != String.Empty)
                            {
                                if (ToColumn != "0")
                                {
                                    GvReport.Columns[_i].Visible = true;
                                }
                                else
                                {
                                    GvReport.Columns[_i].Visible = false;
                                }
                            }
                            _i++;
                        }
                    }

                }
                break;

            case "GvExport_Score":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    if (ViewState["Columns_Check_Search"].ToString() != "0")
                    {
                        string To = ViewState["Columns_Check_Search"].ToString();
                        string[] ToId = To.Split(',');
                        int _i = 0;
                        foreach (string ToColumn in ToId)
                        {
                            if (ToColumn != String.Empty)
                            {
                                if (ToColumn != "0")
                                {
                                    GvExport_Score.Columns[_i].Visible = true;
                                }
                                else
                                {
                                    GvExport_Score.Columns[_i].Visible = false;
                                }
                            }
                            _i++;
                        }
                    }

                }
                break;

        }
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "btndeletedev":
                    GridView GvDevAdd = (GridView)fvcomment.FindControl("GvDevAdd");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    //Control div_save = (Control)fvcomment.FindControl("div_save");

                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsAddListTable_Dev"];
                    dsContacts.Tables["dsAddListTable_Dev"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(GvDevAdd, dsContacts.Tables["dsAddListTable_Dev"]);

                    if (dsContacts.Tables["dsAddListTable_Dev"].Rows.Count < 1)
                    {
                        GvDevAdd.Visible = false;
                        //div_save.Visible = false;

                    }

                    break;


                case "btndeletestar":
                    GridView GvStarAdd = (GridView)fvstar.FindControl("GvStarAdd");
                    GridViewRow rowSelectstar = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    //Control div_save = (Control)fvcomment.FindControl("div_save");

                    int rowIndexstar = rowSelectstar.RowIndex;
                    DataSet dsContactsstar = (DataSet)ViewState["vsAddListTable_Star"];
                    dsContactsstar.Tables["dsAddListTable_Star"].Rows[rowIndexstar].Delete();
                    dsContactsstar.AcceptChanges();
                    setGridData(GvStarAdd, dsContactsstar.Tables["dsAddListTable_Star"]);

                    if (dsContactsstar.Tables["dsAddListTable_Star"].Rows.Count < 1)
                    {
                        GvStarAdd.Visible = false;
                        //div_save.Visible = false;

                    }

                    break;

                case "btndeletedev_edit":
                    GridView GvDevEdit = (GridView)fvimprove_edit.FindControl("GvDevEdit");
                    GridViewRow rowSelect_edit = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    //Control div_save = (Control)fvcomment.FindControl("div_save");

                    int rowIndex_edit = rowSelect_edit.RowIndex;
                    DataSet dsContacts_edit = (DataSet)ViewState["vsAddListTable_Dev"];
                    dsContacts_edit.Tables["dsAddListTable_Dev"].Rows[rowIndex_edit].Delete();
                    dsContacts_edit.AcceptChanges();
                    setGridData(GvDevEdit, dsContacts_edit.Tables["dsAddListTable_Dev"]);

                    if (dsContacts_edit.Tables["dsAddListTable_Dev"].Rows.Count < 1)
                    {
                        GvDevEdit.Visible = false;
                        //div_save.Visible = false;

                    }

                    break;

                case "btndeletestar_edit":
                    GridView GvStarEdit = (GridView)fvstart_edit.FindControl("GvStarEdit");
                    GridViewRow rowSelectstar_edit = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    //Control div_save = (Control)fvcomment.FindControl("div_save");

                    int rowIndexstar_edit = rowSelectstar_edit.RowIndex;
                    DataSet dsContactsstar_edit = (DataSet)ViewState["vsAddListTable_Star"];
                    dsContactsstar_edit.Tables["dsAddListTable_Star"].Rows[rowIndexstar_edit].Delete();
                    dsContactsstar_edit.AcceptChanges();
                    setGridData(GvStarEdit, dsContactsstar_edit.Tables["dsAddListTable_Star"]);

                    if (dsContactsstar_edit.Tables["dsAddListTable_Star"].Rows.Count < 1)
                    {
                        GvStarEdit.Visible = false;

                    }


                    break;
            }
        }
    }

    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {
            case "GvCoreValue":
            case "gvcorevalue_view":

                for (int rowIndex = GvName.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvName.Rows[rowIndex];
                    GridViewRow previousRow = GvName.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[0].FindControl("lblCorevalue")).Text == ((Label)previousRow.Cells[0].FindControl("lblCorevalue")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }

                }

                break;

            case "GvCompetencies":
            case "gvcoreompetencies_view":

                for (int rowIndex = GvName.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvName.Rows[rowIndex];
                    GridViewRow previousRow = GvName.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[0].FindControl("lblCompetencies")).Text == ((Label)previousRow.Cells[0].FindControl("lblCompetencies")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }

                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvimprove_detail":

                gvimprove_detail.EditIndex = e.NewEditIndex;
                select_improve_detail(gvimprove_detail, int.Parse(ViewState["u0_docidx"].ToString()), 3);
                break;

            case "gvstar_detail":

                gvstar_detail.EditIndex = e.NewEditIndex;
                select_improve_detail(gvstar_detail, int.Parse(ViewState["u0_docidx"].ToString()), 4);
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvimprove_detail":
                gvimprove_detail.EditIndex = -1;
                select_improve_detail(gvimprove_detail, int.Parse(ViewState["u0_docidx"].ToString()), 3);

                break;

            case "gvstar_detail":
                gvstar_detail.EditIndex = -1;
                select_improve_detail(gvstar_detail, int.Parse(ViewState["u0_docidx"].ToString()), 4);

                break;

        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvimprove_detail":

                int u2_docidx = Convert.ToInt32(gvimprove_detail.DataKeys[e.RowIndex].Values[0].ToString());
                var txtbehavior_edit = (TextBox)gvimprove_detail.Rows[e.RowIndex].FindControl("txtbehavior_edit");
                var ddlimprove_edit = (DropDownList)gvimprove_detail.Rows[e.RowIndex].FindControl("ddlimprove_edit");
                var txtcomment_edit = (TextBox)gvimprove_detail.Rows[e.RowIndex].FindControl("txtcomment_edit");

                gvimprove_detail.EditIndex = -1;

                update_u2document(u2_docidx, int.Parse(ViewState["EmpIDX"].ToString()), ddlimprove_edit.SelectedValue, txtcomment_edit.Text, int.Parse(ViewState["u0_docidx"].ToString()), 3);
                select_improve_detail(gvimprove_detail, int.Parse(ViewState["u0_docidx"].ToString()), 3);

                break;

            case "gvstar_detail":

                int u2_docidx1 = Convert.ToInt32(gvstar_detail.DataKeys[e.RowIndex].Values[0].ToString());
                var txtbehavior_edit1 = (TextBox)gvstar_detail.Rows[e.RowIndex].FindControl("txtbehavior_edit");
                var ddlstar_edit = (DropDownList)gvstar_detail.Rows[e.RowIndex].FindControl("ddlstar_edit");
                var txtcomment_edit1 = (TextBox)gvstar_detail.Rows[e.RowIndex].FindControl("txtcomment_edit");

                gvstar_detail.EditIndex = -1;

                update_u2document(u2_docidx1, int.Parse(ViewState["EmpIDX"].ToString()), ddlstar_edit.SelectedValue, txtcomment_edit1.Text, int.Parse(ViewState["u0_docidx"].ToString()), 4);
                select_improve_detail(gvstar_detail, int.Parse(ViewState["u0_docidx"].ToString()), 4);

                break;
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvReport_All":
                GvReport_All.PageIndex = e.NewPageIndex;
                select_report_all_find_department(GvReport_All, ViewState["dept"].ToString());
                break;

            case "GvReport":
                GvReport.PageIndex = e.NewPageIndex;
                select_report(GvReport, int.Parse(ddlOrg_report.SelectedValue), ViewState["dept"].ToString(), int.Parse(ddlnode_report.SelectedValue), txtempname_report.Text, txtempcode_report.Text, int.Parse(ddlyear.SelectedValue));
                break;

            case "GvIndex":
                GvIndex.PageIndex = e.NewPageIndex;
                select_index(GvIndex, int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["rdept_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()),
                   int.Parse(ViewState["JobGradeLevel"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));
                break;

            case "Gvapprove_index":
                Gvapprove_index.PageIndex = e.NewPageIndex;
                select_approveindex(Gvapprove_index, int.Parse(ViewState["Org_idx"].ToString()), int.Parse(ViewState["rdept_idx"].ToString()), int.Parse(ViewState["Sec_idx"].ToString()),
                   int.Parse(ViewState["JobGradeLevel"].ToString()), int.Parse(ViewState["EmpIDX"].ToString()));
                break;
        }
    }
    #endregion

    #region bind Actor

    protected void setFormDataActor(int unidx, int acidx, int staidx, int history)
    {

        switch (acidx)
        {

            case 1: // ผู้สร้าง
                if (unidx == 1 && ViewState["cemp_idx_u0"].ToString() == ViewState["EmpIDX"].ToString() & ViewState["History_Detail"].ToString() != "3")
                {
                    if (ViewState["Org_idx"].ToString() != "1" && ViewState["Org_idx"].ToString() != "6")
                    {
                        fvimprove_edit.Visible = true;
                        fvstart_edit.Visible = true;
                        divupdatebutton_createandapprove.Visible = true;
                        divuserapprove.Visible = false;
                        divcancel.Visible = false;
                    }
                    else
                    {
                        fvimprove_edit.Visible = false;
                        fvstart_edit.Visible = false;
                        divupdatebutton_createandapprove.Visible = false;
                        divuserapprove.Visible = false;
                        divcancel.Visible = true;
                    }
                }
                else if (history == 2 && unidx == 3)
                {
                    divupdatebutton_createandapprove.Visible = false;
                    divuserapprove.Visible = true;
                    fvimprove_edit.Visible = false;
                    fvstart_edit.Visible = false;
                    divcancel.Visible = false;
                }
                else
                {
                    fvimprove_edit.Visible = false;
                    fvstart_edit.Visible = false;
                    divupdatebutton_createandapprove.Visible = false;
                    divuserapprove.Visible = false;
                    divcancel.Visible = true;
                }
                divapprove2.Visible = false;
                break;

            case 2: // ผู้มีสิทธิ์ประเมิน
                if (history == 2 && unidx == 2)
                {
                    fvimprove_edit.Visible = true;
                    fvstart_edit.Visible = true;
                    divupdatebutton_createandapprove.Visible = true;
                    divcancel.Visible = false;
                }
                else
                {
                    fvimprove_edit.Visible = false;
                    fvstart_edit.Visible = false;
                    divupdatebutton_createandapprove.Visible = false;
                    divcancel.Visible = true;
                }
                divuserapprove.Visible = false;
                divapprove2.Visible = false;
                break;

            case 3: // ผู้อำนวยการฝ่าย
                fvimprove_edit.Visible = false;
                fvstart_edit.Visible = false;
                divupdatebutton_createandapprove.Visible = false;
                divuserapprove.Visible = false;

                if (unidx == 4 && history == 2)
                {
                    divapprove2.Visible = true;
                    divcancel.Visible = false;
                }
                else
                {
                    divapprove2.Visible = false;
                    divcancel.Visible = true;
                }

                break;

        }

    }

    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        GridView GvDevAdd = (GridView)fvcomment.FindControl("GvDevAdd");
        DropDownList ddlimprove_insert = (DropDownList)fvcomment.FindControl("ddlimprove_insert");
        TextBox txtcomment = (TextBox)fvcomment.FindControl("txtcomment");

        GridView GvStarAdd = (GridView)fvstar.FindControl("GvStarAdd");
        DropDownList ddlstar_insert = (DropDownList)fvstar.FindControl("ddlstar_insert");
        TextBox txtcomment_star = (TextBox)fvstar.FindControl("txtcomment_star");

        GridView GvDevEdit = (GridView)fvimprove_edit.FindControl("GvDevEdit");
        DropDownList ddlimprove_update = (DropDownList)fvimprove_edit.FindControl("ddlimprove_update");
        TextBox txtcomment_edit = (TextBox)fvimprove_edit.FindControl("txtcomment_edit");

        GridView GvStarEdit = (GridView)fvstart_edit.FindControl("GvStarEdit");
        DropDownList ddlstar_update = (DropDownList)fvstart_edit.FindControl("ddlstar_update");
        TextBox txtcomment_star_edit = (TextBox)fvstart_edit.FindControl("txtcomment_star_edit");

        switch (cmdName)
        {
            case "_divMenuBtnToDivIndex":
                SetDefaultpage(1);
                break;

            case "btnCancel":
                if (ViewState["Settab_cancel"].ToString() != "3")
                {
                    SetDefaultpage(1);
                }
                else
                {
                    _divMenuLiToViewReport.Attributes.Add("class", "active");
                    _divMenuLiToViewAdd.Attributes.Remove("class");
                    _divMenuLiToViewApprove.Attributes.Remove("class");
                    _divMenuLiToViewIndex.Attributes.Remove("class");

                    MvMaster.SetActiveView(ViewReport);
                }
                break;

            case "_divMenuBtnToDivAdd":
                if (ViewState["rtcode_checkcreate"].ToString() == "0")
                {
                    SetDefaultpage(2);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ไม่สามารถประเมินตนเองซ้ำได้ เนื่องจากคุณมีรายการประเมินแล้ว!!!');", true);

                }
                break;
            case "_divMenuBtnToDivReport":
                SetDefaultpage(5);
                break;

            case "CmdInsert_DatasetDev":
                setAddList_FormDev(ddlimprove_insert, txtcomment, GvDevAdd);
                ddlimprove_insert.SelectedValue = "0";
                txtcomment.Text = String.Empty;

                break;

            case "CmdInsert_Datasetstar":
                setAddList_FormStar(ddlstar_insert, txtcomment_star, GvStarAdd);
                ddlstar_insert.SelectedValue = "0";
                txtcomment_star.Text = String.Empty;

                break;

            case "CmdUpdate_DatasetDev":
                setAddList_FormDev(ddlimprove_update, txtcomment_edit, GvDevEdit);
                ddlimprove_update.SelectedValue = "0";
                txtcomment_edit.Text = String.Empty;

                break;

            case "CmdUpdate_Datasetstar":
                setAddList_FormStar(ddlstar_update, txtcomment_star_edit, GvStarEdit);
                ddlstar_update.SelectedValue = "0";
                txtcomment_star_edit.Text = String.Empty;

                break;
            case "CmdInsert":
                string arg_insert = e.CommandArgument.ToString();

                insert_system(1, 1, int.Parse(arg_insert), int.Parse(ViewState["EmpIDX"].ToString()), 1, int.Parse(ViewState["u0_typeidx"].ToString()), GvCoreValue, GvCompetencies);
                if (count_improve == 1 && count_star == 1)
                {
                    SetDefaultpage(1);
                }
                break;

            case "_divMenuBtnToDivApprove":
                SetDefaultpage(3);
                break;

            case "CmdDetail":
                string[] arg_detail = new string[11];
                arg_detail = e.CommandArgument.ToString().Split(';');

                ViewState["u0_docidx"] = arg_detail[0];
                ViewState["cemp_idx_u0"] = arg_detail[1];
                ViewState["History_Detail"] = arg_detail[2];
                ViewState["unidx"] = arg_detail[3];
                ViewState["acidx"] = arg_detail[4];
                ViewState["staidx"] = arg_detail[5];
                ViewState["form_name_create"] = arg_detail[6];
                ViewState["sum_head_core_value"] = arg_detail[7];
                ViewState["sum_head_competency"] = arg_detail[8];
                ViewState["u0_typeidx_detail"] = arg_detail[9];
                ViewState["Settab_cancel"] = arg_detail[10];

                SetDefaultpage(4);
                setFormDataActor(int.Parse(arg_detail[3]), int.Parse(arg_detail[4]), int.Parse(arg_detail[5]), int.Parse(arg_detail[2]));
                break;

            case "btnBack":
                SetDefaultpage(3);

                break;

            case "CmdUpdate":
                string arg_update = e.CommandArgument.ToString();

                update_system(int.Parse(ViewState["unidx"].ToString()), int.Parse(ViewState["acidx"].ToString()), int.Parse(arg_update), int.Parse(ViewState["EmpIDX"].ToString()), 2, int.Parse(ViewState["u0_docidx"].ToString()), gvcorevalue_view, gvcoreompetencies_view);

                if (count_improve == 1 && count_star == 1 && ViewState["acidx"].ToString() != "3" || ViewState["acidx"].ToString() == "3")
                {
                    SetDefaultpage(1);
                }
                break;

            case "CmdDel_comment":
                string[] arg_u2doc = new string[2];
                arg_u2doc = e.CommandArgument.ToString().Split(';');

                delete_u2document(int.Parse(arg_u2doc[0].ToString()), int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ViewState["u0_docidx"].ToString()));

                if (int.Parse(arg_u2doc[1].ToString()) == 3)
                {
                    select_improve_detail(gvimprove_detail, int.Parse(ViewState["u0_docidx"].ToString()), int.Parse(arg_u2doc[1].ToString()));
                }
                else
                {
                    select_improve_detail(gvstar_detail, int.Parse(ViewState["u0_docidx"].ToString()), int.Parse(arg_u2doc[1].ToString()));
                }
                break;

            case "CmdSearch":
                string dept = String.Empty;
                foreach (ListItem li in chkdept.Items)
                {
                    if (li.Selected == true)
                    {
                        dept += li.Value + ",";

                    }
                }

                ViewState["dept"] = dept;

                if (dept != String.Empty && dept != "")
                {
                    if (ddltypereport.SelectedValue == "1")
                    {

                        ViewState["Columns_Check_Search"] = null;
                        List<String> YrColumnsList = new List<string>();
                        foreach (ListItem item1 in YrChkBoxColumns.Items)
                        {
                            if (item1.Selected)
                            {
                                YrColumnsList.Add(item1.Value);
                            }
                            else
                            {
                                YrColumnsList.Add("0");
                            }
                        }
                        String YrColumnsList1 = String.Join(",", YrColumnsList.ToArray());
                        ViewState["Columns_Check_Search"] = YrColumnsList1;

                        select_report(GvReport, int.Parse(ddlOrg_report.SelectedValue), dept, int.Parse(ddlnode_report.SelectedValue), txtempname_report.Text, txtempcode_report.Text, int.Parse(ddlyear.SelectedValue));
                        GvReport.Visible = true;
                        GvReport_All.Visible = false;
                    }
                    else if (ddltypereport.SelectedValue == "2")
                    {
                        select_report_all_find_department(GvReport_All, dept);
                        GvReport.Visible = false;
                        GvReport_All.Visible = true;
                    }

                    btnexport.Visible = true;
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกฝ่ายที่ต้องการ!!!');", true);
                    btnexport.Visible = false;
                }
                break;

            case "btnrefresh":
                SetDefaultpage(5);
                break;

            case "CmdSearch_Export":
                int count_ = 0;

                _dtpmform = new data_tpm_form();
                _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
                tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

                if (ddltypereport.SelectedValue == "1")
                {

                    // dataselect.condition = 9;
                    //dataselect.org_idx = int.Parse(ddlOrg_report.SelectedValue);
                    //dataselect.rdepidx_comma = ViewState["dept"].ToString();
                    //dataselect.unidx = int.Parse(ddlnode_report.SelectedValue);
                    //dataselect.emp_name_th = txtempname_report.Text;
                    //dataselect.emp_code = txtempcode_report.Text;
                    //dataselect.getyear = int.Parse(ddlyear.SelectedValue);
                    //_dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
                    ////txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

                    //_dtpmform = callServicePostTPMForm(_urlSelectSystem, _dtpmform);

                    GvExport_Score.AllowPaging = false;
                    GvExport_Score.DataSource = ViewState["Export_ReportList"];// _dtpmform.Boxtpmu0_DocFormDetail;
                    GvExport_Score.DataBind();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=report-formlist.xls");
                    // Response.Charset = ""; set character 
                    Response.Charset = "utf-8";
                    Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                    Response.ContentType = "application/vnd.ms-excel";

                    Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                    Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                    //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                    StringWriter sw = new StringWriter();
                    HtmlTextWriter hw = new HtmlTextWriter(sw);

                    for (int i = 0; i < GvExport_Score.Rows.Count; i++)
                    {
                        //Apply text style to each Row
                        GvExport_Score.Rows[i].Attributes.Add("class", "number2");
                    }
                    GvExport_Score.RenderControl(hw);

                    //Amount is displayed in number format with 2 decimals
                    Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();

                    /* if (_dtpmform.ReturnCode.ToString() != "1")
                     {
                         DataTable tableFormList = new DataTable();
                         tableFormList.Columns.Add("องค์กร", typeof(String));
                         tableFormList.Columns.Add("ฝ่าย", typeof(String));
                         tableFormList.Columns.Add("แผนก", typeof(String));
                         tableFormList.Columns.Add("ตำแหน่ง", typeof(String));
                         tableFormList.Columns.Add("กลุ่มตำแหน่ง", typeof(String));
                         tableFormList.Columns.Add("รหัสพนักงาน", typeof(String));
                         tableFormList.Columns.Add("ชื่อผู้ถูกประเมิน", typeof(String));
                         tableFormList.Columns.Add("ฟอร์มประเมิน", typeof(String));
                         tableFormList.Columns.Add("คะแนนประเมินตนเอง Core Value", typeof(String));
                         tableFormList.Columns.Add("คะแนนประเมินตนเอง Competency", typeof(String));
                         tableFormList.Columns.Add("ชื่อผู้มีสิทธิ์ประเมิน", typeof(String));
                         tableFormList.Columns.Add("คะแนนหัวหน้าประเมิน Core Value", typeof(String));
                         tableFormList.Columns.Add("คะแนนหัวหน้าประเมิน Competency", typeof(String));
                         tableFormList.Columns.Add("สถานะรายการ", typeof(String));

                         foreach (var row_report in _dtpmform.Boxtpmu0_DocFormDetail)
                         {
                             DataRow addFormListRow = tableFormList.NewRow();

                             addFormListRow[0] = row_report.org_name_th.ToString();
                             addFormListRow[1] = row_report.dept_name_th.ToString();
                             addFormListRow[2] = row_report.sec_name_th.ToString();
                             addFormListRow[3] = row_report.pos_name_th.ToString();
                             addFormListRow[4] = row_report.pos_name.ToString();
                             addFormListRow[5] = row_report.emp_code.ToString();
                             addFormListRow[6] = row_report.emp_name_th.ToString();
                             addFormListRow[7] = row_report.form_name.ToString();
                             addFormListRow[8] = row_report.sum_mine_core_value.ToString();
                             addFormListRow[9] = row_report.sum_mine_competency.ToString();
                             addFormListRow[10] = row_report.approve1.ToString();
                             addFormListRow[11] = row_report.sum_head_core_value.ToString();
                             addFormListRow[12] = row_report.sum_head_competency.ToString();
                             addFormListRow[13] = row_report.StatusDoc.ToString();


                             tableFormList.Rows.InsertAt(addFormListRow, count_++);
                         }
                         WriteExcelWithNPOI(tableFormList, "xls", "report-formlist", 0);

                     }
                     else
                     {
                         ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                         break;
                     }

                     */
                }
                else if (ddltypereport.SelectedValue == "2")
                {
                    dataselect.condition = 12;
                    dataselect.getyear = int.Parse(ddlyear_all.SelectedValue);
                    dataselect.rdepidx_comma = ViewState["dept"].ToString();
                    _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;

                    _dtpmform = callServicePostTPMForm(_urlSelectSystem, _dtpmform);
                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

                    if (_dtpmform.ReturnCode.ToString() != "1")
                    {
                        DataTable tableFormResult = new DataTable();
                        tableFormResult.Columns.Add("ฝ่าย", typeof(String));
                        tableFormResult.Columns.Add("แผนก", typeof(String));
                        tableFormResult.Columns.Add("จำนวนคน", typeof(String));
                        tableFormResult.Columns.Add("จบการดำเนินการ (คน)", typeof(String));
                        tableFormResult.Columns.Add("กำลังดำเนินการ (คน)", typeof(String));

                        foreach (var row_report_all in _dtpmform.Boxtpmu0_DocFormDetail)
                        {
                            DataRow addFormResultRow = tableFormResult.NewRow();

                            addFormResultRow[0] = row_report_all.dept_name_th.ToString();
                            addFormResultRow[1] = row_report_all.sec_name_th.ToString();
                            addFormResultRow[2] = row_report_all.qty.ToString();
                            addFormResultRow[3] = row_report_all.complete.ToString();
                            addFormResultRow[4] = row_report_all.uncomplete.ToString();

                            tableFormResult.Rows.InsertAt(addFormResultRow, count_++);
                        }
                        WriteExcelWithNPOI(tableFormResult, "xls", "report-formresult", 0);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                        break;
                    }
                }

                break;
        }
    }
    #endregion

}