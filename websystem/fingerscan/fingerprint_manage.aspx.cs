using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Net;
using zkemkeeper;
using System.IO;

public partial class websystem_fingerscan_fingerprint_manage : System.Web.UI.Page
{
    #region initial function/data
    public CZKEM axCZKEM1 = new CZKEM();
    public bool bIsConnected = false;//the boolean value identifies whether the device is connected
    public int iMachineNumber = 1;//the serial number of the device.After connecting
    IPAddress defaultIP = new IPAddress(0);

    private string sdwEnrollNumber = "";
    private int iToFingerIndex = 0;
    private string sTmpData = "";
    private string sToIp = "";
    private int iToPort = 4370;
    private int idwBackupNumber = 0;

    function_tool _funcTool = new function_tool();
    function_scan _funcScan = new function_scan();

    data_times _data_times = new data_times();
    data_times _data_local = new data_times();
    data_times _data_local2 = new data_times();
    data_times _data_local3 = new data_times();
    data_times _data_local4 = new data_times();
    data_employee _data_employee = new data_employee();
    List<employee_detail_small> _data_emp_selected = new List<employee_detail_small>();
    List<finger_machine_detail> _data_machine_selected = new List<finger_machine_detail>();

    int emp_idx = 0;
    int emp_idx_action = 0;
    int _temp_idx = 0;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;
    string _emp_idx_list = String.Empty;
    string _admin_idx_list = String.Empty;

    static int[] _permission = {172, 24047};

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetViewEmployeeListSmall = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewEmployeeListSmall"];

    static string _urlTmGetMachine = _serviceUrl + ConfigurationManager.AppSettings["urlTmGetMachine"];
    static string _urlTmGetPlace = _serviceUrl + ConfigurationManager.AppSettings["urlTmGetPlace"];
    static string _urlTmGetLocation = _serviceUrl + ConfigurationManager.AppSettings["urlTmGetLocation"];
    static string _urlTmGetZone = _serviceUrl + ConfigurationManager.AppSettings["urlTmGetZone"];

    static string _urlTmSetTemplate = _serviceUrl + ConfigurationManager.AppSettings["urlTmSetTemplate"];
    static string _urlTmGetTemplate = _serviceUrl + ConfigurationManager.AppSettings["urlTmGetTemplate"];
    static string _urlTmGetAdminstratorList = _serviceUrl + ConfigurationManager.AppSettings["urlTmGetAdminstratorList"];
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx_action = _funcTool.convertToInt(Session["emp_idx"].ToString());

        if(!IsPostBack)
        {
            initPage();
        }
    }

    #region event command
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch(cmdName)
        {
            case "cmdFingerPrint":
                setActiveView("viewCreate", 0);
                break;
            case "cmdSave": case "cmdCancel":
                initPage();
                div_Action.Visible = true;
                break;
            case "cmdReset":
                initPage();
                setGridData(gvEmployeeList, null);
                break;
            case "cmdSearch":
                div_Action.Visible = true;

                DropDownList ddlOrg = (DropDownList)fvEmpSearch.FindControl("ddlOrg");
                DropDownList ddlDept = (DropDownList)fvEmpSearch.FindControl("ddlDept");
                DropDownList ddlSec = (DropDownList)fvEmpSearch.FindControl("ddlSec");
                DropDownList ddlEmpType = (DropDownList)fvEmpSearch.FindControl("ddlEmpType");
                DropDownList ddlEmpStatus = (DropDownList)fvEmpSearch.FindControl("ddlEmpStatus");

                search_key_employee _search_key = new search_key_employee();
                _search_key.s_emp_code = ((TextBox)fvEmpSearch.FindControl("tbEmpCode")).Text.Trim();
                _search_key.s_emp_name = ((TextBox)fvEmpSearch.FindControl("tbEmpName")).Text.Trim();
                _search_key.s_org_idx = ddlOrg.SelectedValue;
                _search_key.s_rdept_idx = ddlDept.SelectedValue;
                _search_key.s_rsec_idx = ddlSec.SelectedValue;
                _search_key.s_cost_center = ((TextBox)fvEmpSearch.FindControl("tbCostCenter")).Text.Trim();
                _search_key.s_emp_type = ddlEmpType.SelectedValue;
                _search_key.s_emp_status = ddlEmpStatus.SelectedValue;

                _data_employee.search_key_emp_list = new search_key_employee[1];
                _data_employee.search_key_emp_list[0] = _search_key;
                _data_employee = getViewEmployeeList(_data_employee);
                ViewState["EmpSearchList"] = _data_employee.employee_list_small;
                gvEmployeeList.PageIndex = 0;
                setGridData(gvEmployeeList, ViewState["EmpSearchList"]);
                break;
            case "cmdTransfer":
                div_transfer.Visible = true;
                div_heading2.Attributes.Add("class", "panel panel-primary");
                litHeadingTitle2.Text = "โอนลายนิ้วมือ";
                lbTransferItem.Visible = true;
                lbDeleteItem.Visible = false;

                _data_times = new data_times();
                search_machine_detail machine_transfer = new search_machine_detail();
                machine_transfer.s_machine_status = "1";
                _data_times.search_machine_list = new search_machine_detail[1];
                _data_times.search_machine_list[0] = machine_transfer;
                _data_times = getMachineList(_data_times);
                ViewState["FingerMachineList"] = _data_times.finger_machine_list;
                setGridData(gvMachineList, ViewState["FingerMachineList"]);

                getZoneDdl(ddlZone);
                getPlaceDdl(ddlPlace);
                getLocationDdl(ddlLoc);
                break;
            case "cmdDelete":
                div_transfer.Visible = true;
                div_heading2.Attributes.Add("class", "panel panel-danger");
                litHeadingTitle2.Text = "ลบลายนิ้วมือ";
                lbTransferItem.Visible = false;
                lbDeleteItem.Visible = true;

                _data_times = new data_times();
                search_machine_detail machine_delete = new search_machine_detail();
                machine_delete.s_machine_status = "1";
                _data_times.search_machine_list = new search_machine_detail[1];
                _data_times.search_machine_list[0] = machine_delete;
                _data_times = getMachineList(_data_times);
                ViewState["FingerMachineList"] = _data_times.finger_machine_list;
                setGridData(gvMachineList, ViewState["FingerMachineList"]);

                getZoneDdl(ddlZone);
                getPlaceDdl(ddlPlace);
                getLocationDdl(ddlLoc);
                break;
            case "cmdActionDelete":
                if(ViewState["EmpSelected"] != null)
                {
                    _temp_idx = _funcTool.convertToInt(cmdArg);
                    _data_emp_selected = (List<employee_detail_small>)ViewState["EmpSelected"];
                    _data_emp_selected.RemoveAll(l => l.emp_idx == _temp_idx);
                }
                ViewState["EmpSelected"] = _data_emp_selected;
                setGridData(gvEmployeeSelected, ViewState["EmpSelected"]);
                setGridData(gvEmployeeList, ViewState["EmpSearchList"]);
                break;
            case "cmdResetItem":
                ViewState["EmpSelected"] = null;
                ViewState["FingerMachineSelected"] = null;
                setGridData(gvEmployeeList, ViewState["EmpSearchList"]);
                gvEmployeeList.PageIndex = 0;
                setGridData(gvEmployeeSelected, ViewState["EmpSelected"]);
                gvEmployeeSelected.PageIndex = 0;
                setGridData(gvMachineList, ViewState["FingerMachineList"]);
                gvMachineList.PageIndex = 0;
                break;
            case "cmdMachineSearch":
                search_machine_detail _search_machine = new search_machine_detail();
                _search_machine.s_zone_idx = ddlZone.SelectedValue;
                _search_machine.s_place_idx = ddlPlace.SelectedValue;
                _search_machine.s_loc_idx = ddlLoc.SelectedValue;
                _search_machine.s_machine_status = "1";
                _data_times.search_machine_list = new search_machine_detail[1];
                _data_times.search_machine_list[0] = _search_machine;
                _data_times = getMachineList(_data_times);
                ViewState["FingerMachineList"] = _data_times.finger_machine_list;
                gvMachineList.PageIndex = 0;
                setGridData(gvMachineList, ViewState["FingerMachineList"]);
                break;
            case "cmdMachineReset":
                _data_times = new data_times();
                search_machine_detail _machine_reset = new search_machine_detail();
                _machine_reset.s_machine_status = "1";
                _data_times.search_machine_list = new search_machine_detail[1];
                _data_times.search_machine_list[0] = _machine_reset;
                _data_times = getMachineList(_data_times);
                ViewState["FingerMachineList"] = _data_times.finger_machine_list;
                setGridData(gvMachineList, ViewState["FingerMachineList"]);

                getZoneDdl(ddlZone);
                getPlaceDdl(ddlPlace);
                getLocationDdl(ddlLoc);
                break;
            case "cmdTransferItem":
                if(ViewState["EmpSelected"] != null && ViewState["FingerMachineList"] != null)
                {
                    // get employee template
                    _data_emp_selected = (List<employee_detail_small>)ViewState["EmpSelected"];
                    _data_machine_selected = (List<finger_machine_detail>)ViewState["FingerMachineSelected"];
                    foreach(employee_detail_small emp_list in _data_emp_selected)
                    {
                        if(emp_list.emp_idx > 0)
                        {
                            _emp_idx_list += emp_list.emp_idx.ToString() + ",";
                        }
                    }
                    // litDebug.Text = _emp_idx_list;
                    _emp_idx_list = _emp_idx_list.Remove(_emp_idx_list.Length - 1);
                    search_template_detail _templateTransferList = new search_template_detail();
                    _templateTransferList.s_emp_idx = _emp_idx_list;
                    _data_local.search_template_list = new search_template_detail[1];
                    _data_local.search_template_list[0] = _templateTransferList;
                    _data_local = callServicePostTimes(_urlTmGetTemplate, _data_local);
                    // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_local));
                    
                    // loop machine
                    foreach(finger_machine_detail machine_list in _data_machine_selected)
                    {
                        // loop template from _data_local
                        foreach(finger_template_detail emp_template in _data_local.finger_template_list)
                        {
                            for (int i = 0; i <= 1; i++) //index 0, index 1, index 2, index 3
                            {
                                sdwEnrollNumber = emp_template.emp_code;
                                iToFingerIndex = i;
                                
                                // write to db
                                finger_template_detail _templateList = new finger_template_detail();
                                _templateList.m0_idx = 0;
                                _templateList.emp_code = sdwEnrollNumber;
                                switch(i) 
                                {
                                    case 0:
                                        _templateList.index0 = emp_template.index0;
                                        sTmpData = emp_template.index0;
                                        break;
                                    case 1:
                                        _templateList.index1 = emp_template.index1;
                                        sTmpData = emp_template.index1;
                                        break;
                                    /*case 2:
                                        _templateList.index2 = emp_template.index2;
                                        sTmpData = emp_template.index2;
                                        break;
                                    case 3:
                                        _templateList.index3 = emp_template.index3;
                                        sTmpData = emp_template.index3;
                                        break;*/
                                }
                                sToIp = IPAddress.TryParse(machine_list.ip_host, out defaultIP) ? machine_list.ip_host : convertHostToIp(machine_list.ip_host);
                                _templateList.ip_host = machine_list.ip_host;
                                _templateList.template_status = 1;
                                _templateList.emp_idx_action = emp_idx_action;
                                _data_times.finger_template_list = new finger_template_detail[1];
                                _data_times.finger_template_list[0] = _templateList;
                                // litResult.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_times));

                                // transfer template to machine
                                if ((sToIp != "" || sToIp != "0") && iToPort != 0 && sTmpData != "")
                                {
                                    if (_funcScan.setMachineConnect(sToIp, iToPort))
                                    {
                                        _funcScan.setEnableDevice(iMachineNumber, false);

                                        // litResult.Text += _funcScan.setUserTemplate(iMachineNumber, sdwEnrollNumber, iToFingerIndex, 1, sTmpData) + setNewline();
                                        _funcScan.setUserTemplate(iMachineNumber, sdwEnrollNumber, iToFingerIndex, 1, sTmpData);

                                        _funcScan.setEnableDevice(iMachineNumber, true);
                                        _funcScan.setMachineDisconnect();

                                        // insert data to DB
                                        _data_times = callServicePostTimes(_urlTmSetTemplate, _data_times);
                                    }
                                }
                            }
                        }

                        // get machine administrator and re-insert it
                        setAdminMachine(machine_list.m0_idx, machine_list.ip_host);
                    }
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการเรียบร้อยแล้วค่ะ')", true);
                }
                break;
            case "cmdDeleteItem":
                if(ViewState["EmpSelected"] != null && ViewState["FingerMachineList"] != null)
                {
                    // get employee, machine data
                    _data_emp_selected = (List<employee_detail_small>)ViewState["EmpSelected"];
                    _data_machine_selected = (List<finger_machine_detail>)ViewState["FingerMachineSelected"];
                    
                    // loop machine
                    foreach(finger_machine_detail machine_list in _data_machine_selected)
                    {
                        sToIp = IPAddress.TryParse(machine_list.ip_host, out defaultIP) ? machine_list.ip_host : convertHostToIp(machine_list.ip_host);
                        // loop employee from _data_local
                        foreach(employee_detail_small emp_list in _data_emp_selected)
                        {
                            sdwEnrollNumber = emp_list.emp_code;

                            if ((sToIp != "" || sToIp != "0") && iToPort != 0)
                            {
                                idwBackupNumber = 12; //12:all fingerprint and password data
                                if (_funcScan.setMachineConnect(sToIp, iToPort))
                                {
                                    _funcScan.setEnableDevice(iMachineNumber, false);

                                    // litResult.Text += _funcScan.delUserEnroll(iMachineNumber, sdwEnrollNumber, idwBackupNumber) + setNewline();
                                    _funcScan.delUserEnroll(iMachineNumber, sdwEnrollNumber, idwBackupNumber);

                                    _funcScan.setEnableDevice(iMachineNumber, true);
                                    _funcScan.setMachineDisconnect();

                                    // write to db
                                    finger_template_detail _templateList = new finger_template_detail();
                                    _templateList.m0_idx = 0;
                                    _templateList.emp_code = sdwEnrollNumber;

                                    _templateList.ip_host = machine_list.ip_host;
                                    _templateList.template_status = 9;
                                    _templateList.emp_idx_action = emp_idx_action;
                                    _data_times.finger_template_list = new finger_template_detail[1];
                                    _data_times.finger_template_list[0] = _templateList;

                                    // insert data to DB
                                    _data_times = callServicePostTimes(_urlTmSetTemplate, _data_times);
                                }
                            }
                        }

                        // get machine administrator and re-insert it
                        setAdminMachine(machine_list.m0_idx, machine_list.ip_host);
                    }
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการเรียบร้อยแล้วค่ะ')", true);
                }
                break;
            case "cmdGetTemplate":
                setActiveView("viewTransfer", 0);
                litTransferResult.Text = "";
                ddlMachineList.SelectedValue = "0";
                break;
            case "cmdBackFromTransfer":
                initPage();
                litTransferResult.Text = "";
                ddlMachineList.SelectedValue = "0";
                break;
            case "cmdGetTransferTemplate":
                string transfer_ip = ddlMachineList.SelectedValue.ToString();
                // check machine data
                if (transfer_ip != "" && transfer_ip != "0" && iToPort != 0)
                {
                    litTransferResult.Text = "";

                    DataTable dtEmpCode = new DataTable();
                    dtEmpCode.Columns.Add("emp_code");

                    // connect to machine
                    if (_funcScan.setMachineConnect(transfer_ip, iToPort))
                    {
                        _funcScan.setEnableDevice(iMachineNumber, false);

                        string _tempList = _funcScan.getAllUserInfo(iMachineNumber);
                        int _in_machine = _funcTool.convertToInt(_funcScan.getDeviceStatus(1, 2));

                        _funcScan.setEnableDevice(iMachineNumber, true);
                        _funcScan.setMachineDisconnect();

                        string[] _s1 = _tempList.Split(new string[] { "<br />----------<br />" }, StringSplitOptions.None);
						
                        foreach (string _items in _s1)
                        {
                            string[] _s2 = _items.Split('|');
                            DataRow dr = dtEmpCode.NewRow();
                            dr["emp_code"] = _s2[0];
                            dtEmpCode.Rows.Add(dr);
                        }

                        if (_funcScan.setMachineConnect(transfer_ip, iToPort))
                        {
                            int _count = 0;
                            _funcScan.setEnableDevice(iMachineNumber, false);
                            foreach (DataRow _row in dtEmpCode.Rows)
                            {
                                if(_row["emp_code"].ToString() != "")
                                {
                                    for (int i = 0; i <= 1; i++) //index 0, index 1
                                    {
                                        string _tmp = _funcScan.getUserTemplate(iMachineNumber, _row["emp_code"].ToString(), i);
                                        if (_tmp != "0" && _tmp.Length > 10)
                                        {
                                            // write to db
                                            _data_times.finger_template_list = new finger_template_detail[1];
                                            finger_template_detail _templateList = new finger_template_detail();
                                            _templateList.m0_idx = 0;
                                            _templateList.emp_code = _row["emp_code"].ToString();
                                            switch(i) 
                                            {
                                                case 0:
                                                    _templateList.index0 = _tmp;
                                                    break;
                                                case 1:
                                                    _templateList.index1 = _tmp;
                                                    break;
                                            }
                                            _templateList.ip_host = transfer_ip;
                                            _data_times.finger_template_list[0] = _templateList;
                                            // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_times));
                                            _data_times = callServicePostTimes(_urlTmSetTemplate, _data_times);
                                        }
                                    }
                                    litTransferResult.Text += "add : " + _row["emp_code"].ToString() + setNewline();

                                    _count++;
                                }
                            }

                            if(_in_machine > 0 && _in_machine == _count)
                            {
                                // clear machine
                                int idwBackupNumber = 12; //12:all fingerprint and password data
                                //_funcScan.delAllUserEnroll(iMachineNumber, "", idwBackupNumber);

                                litTransferResult.Text += "transfer completed";
                            }
                            else
                            {
                                litTransferResult.Text += "transfer error";
                            }

                            _funcScan.setEnableDevice(iMachineNumber, true);
                            _funcScan.setMachineDisconnect();
                        }
                    }
                    else
                    {
                        litTransferResult.Text += "unable to connect the device, error code = " + _funcScan.getMachineError().ToString() + setNewline();
                    }
                }
                else
                {
                    litTransferResult.Text = "machine connection error" + setNewline();
                }
                break;
        }
    }

    protected void rblSelectedIndexChanged(object sender, EventArgs e)
    {
        RadioButtonList rblName = (RadioButtonList)sender;
        // _masterType = _funcTool.convertToInt(rblName.SelectedValue);

        // switch (rblName.ID)
        // {
            // case "rblMasterType":
            //     setActiveView("viewCreate", _masterType);
            //     break;
            // case "rblMasterType2":
                // setActiveView("viewList", _masterType);
                // break;
        // }
    }

    protected void cbCheckedChanged(object sender, EventArgs e)
    {
        var cb = (CheckBox)sender;
        switch (cb.ID)
        {
            case "cbEmpSelected" :
                string[] _val_in = cb.Text.Split('|');
                _temp_idx = _funcTool.convertToInt(_val_in[0]);
                if(ViewState["EmpSelected"] != null)
                {
                    _data_emp_selected = (List<employee_detail_small>)ViewState["EmpSelected"];
                    if(cb.Checked)
                    {
                        var check_selected = _data_emp_selected.Where(d => 
                        d.emp_idx == _temp_idx).FirstOrDefault();
                        if(check_selected == null)
                        {
                            employee_detail_small _emp_list = new employee_detail_small();
                            _emp_list.emp_idx = _temp_idx;
                            _emp_list.emp_code = _val_in[1];
                            _emp_list.emp_name_th = _val_in[2];
                            _data_emp_selected.Add(_emp_list);
                        }
                    }
                    else
                    {
                        _data_emp_selected.RemoveAll(l => l.emp_idx == _temp_idx);
                    }
                }
                else
                {
                    if(cb.Checked)
                    { 
                        employee_detail_small _emp_list = new employee_detail_small();
                        _emp_list.emp_idx = _temp_idx;
                        _emp_list.emp_code = _val_in[1];
                        _emp_list.emp_name_th = _val_in[2];
                        _data_emp_selected.Add(_emp_list);
                    }
                }

                ViewState["EmpSelected"] = _data_emp_selected;
                setGridData(gvEmployeeSelected, ViewState["EmpSelected"]);
                break;
            case "cbMachineSelected" :
                string[] _val_in2 = cb.Text.Split('|');
                _temp_idx = _funcTool.convertToInt(_val_in2[0]);
                if(ViewState["FingerMachineSelected"] != null)
                {
                    _data_machine_selected = (List<finger_machine_detail>)ViewState["FingerMachineSelected"];
                    if(cb.Checked)
                    {
                        var check_selected = _data_machine_selected.Where(d => 
                        d.m0_idx == _temp_idx).FirstOrDefault();
                        if(check_selected == null)
                        {
                            finger_machine_detail _machine_list = new finger_machine_detail();
                            _machine_list.m0_idx = _temp_idx;
                            _machine_list.ip_host = _val_in2[1];
                            _data_machine_selected.Add(_machine_list);
                        }
                    }
                    else
                    {
                        _data_machine_selected.RemoveAll(l => l.m0_idx == _temp_idx);
                    }
                }
                else
                {
                    if(cb.Checked)
                    { 
                        finger_machine_detail _machine_list = new finger_machine_detail();
                        _machine_list.m0_idx = _temp_idx;
                        _machine_list.ip_host = _val_in2[1];
                        _data_machine_selected.Add(_machine_list);
                    }
                }

                ViewState["FingerMachineSelected"] = _data_machine_selected;
                break;
        }
    }
    #endregion event command

    #region RowDatabound
    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvEmployeeList":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox cbEmpSelected = (CheckBox)e.Row.FindControl("cbEmpSelected");
                    HiddenField hfEmpIdx = (HiddenField)e.Row.FindControl("hfEmpIdx");
                    _temp_idx = _funcTool.convertToInt(hfEmpIdx.Value);
                    if(ViewState["EmpSelected"] != null)
                    {
                        _data_emp_selected = (List<employee_detail_small>)ViewState["EmpSelected"];
                        var check_selected = _data_emp_selected.Where(d => 
                        d.emp_idx == _temp_idx).FirstOrDefault();
                        if(check_selected != null)
                        {
                            cbEmpSelected.Checked = true;
                        }
                    }
                }
                break;
            case "gvMachineList":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox cbMachineSelected = (CheckBox)e.Row.FindControl("cbMachineSelected");
                    HiddenField hfM0Idx = (HiddenField)e.Row.FindControl("hfM0Idx");
                    _temp_idx = _funcTool.convertToInt(hfM0Idx.Value);
                    if(ViewState["FingerMachineSelected"] != null)
                    {
                        _data_machine_selected = (List<finger_machine_detail>)ViewState["FingerMachineSelected"];
                        var check_selected = _data_machine_selected.Where(d => 
                        d.m0_idx == _temp_idx).FirstOrDefault();
                        if(check_selected != null)
                        {
                            cbMachineSelected.Checked = true;
                        }
                    }
                }

                gvName.Columns[1].Visible = false;

                // check permission
                foreach(int _pass in _permission)
                {
                    if(emp_idx_action == _pass)
                    {
                        gvName.Columns[1].Visible = true;
                        continue;
                    }
                }
                break;
        }
    }
    #endregion RowDatabound

    #region paging
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch(gvName.ID)
        {
            case "gvEmployeeList":
                if(ViewState["EmpSearchList"] != null)
                {
                    setGridData(gvEmployeeList, ViewState["EmpSearchList"]);
                }
                else
                {
                    _data_employee = new data_employee();
                    _data_employee = getViewEmployeeList(_data_employee);
                    setGridData(gvEmployeeList, _data_employee.employee_list_small);
                }
                break;
            case "gvEmployeeSelected":
                if(ViewState["FingerMachineList"] != null)
                {
                    setGridData(gvEmployeeSelected, ViewState["EmpSelected"]);
                }
                break;
            case "gvMachineList":
                if(ViewState["FingerMachineList"] != null)
                {
                    setGridData(gvMachineList, ViewState["FingerMachineList"]);
                }
                else
                {
                    _data_times = new data_times();
                    _data_times = getMachineList(_data_times);
                    setGridData(gvMachineList, _data_times.finger_machine_list);
                }
                break;
        }
    }
    #endregion paging

    #region drorpdown list
    protected void getOrganizationList(DropDownList ddlName)
    {
        _data_employee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _data_employee.organization_list[0] = _orgList;

        _data_employee = callServicePostEmployee(_urlGetOrganizationList, _data_employee);
        setDdlData(ddlName, _data_employee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- องค์กร ---", "-1"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _data_employee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _data_employee.department_list[0] = _deptList;

        _data_employee = callServicePostEmployee(_urlGetDepartmentList, _data_employee);
        setDdlData(ddlName, _data_employee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- ฝ่าย ---", "-1"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _data_employee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _data_employee.section_list[0] = _secList;

        _data_employee = callServicePostEmployee(_urlGetSectionList, _data_employee);
        setDdlData(ddlName, _data_employee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("--- แผนก ---", "-1"));
    }

    protected void getPlaceDdl(DropDownList ddlName)
    {
        _data_times = callServicePostTimes(_urlTmGetPlace, _data_times);
        setDdlData(ddlName, _data_times.tm_place_list, "place_name", "m0_idx");
        ddlName.Items.Insert(0, new ListItem("--- สถานที่ตั้ง ---", "0"));
    }

    protected void getLocationDdl(DropDownList ddlName)
    {
        _data_times = new data_times();
        _data_times = callServicePostTimes(_urlTmGetLocation, _data_times);
        setDdlData(ddlName, _data_times.tm_location_list, "location_name", "m0_idx");
        ddlName.Items.Insert(0, new ListItem("--- ตำแหน่งที่ตั้ง ---", "0"));
    }

    protected void getZoneDdl(DropDownList ddlName)
    {
        _data_times = new data_times();
        _data_times = callServicePostTimes(_urlTmGetZone, _data_times);
        setDdlData(ddlName, _data_times.tm_zone_list, "zone_name", "m0_idx");
        ddlName.Items.Insert(0, new ListItem("--- Zone ---", "0"));
    }

    protected data_employee getViewEmployeeList(data_employee _data_employee)
    {
        _data_employee = callServicePostEmployee(_urlGetViewEmployeeListSmall, _data_employee);
        return _data_employee;
    }

    protected data_times getMachineList(data_times _data_times)
    {
        return callServicePostTimes(_urlTmGetMachine, _data_times);
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        DropDownList ddlOrg = (DropDownList)fvEmpSearch.FindControl("ddlOrg");
        DropDownList ddlDept = (DropDownList)fvEmpSearch.FindControl("ddlDept");
        DropDownList ddlSec = (DropDownList)fvEmpSearch.FindControl("ddlSec");

        switch (ddlName.ID)
        {
            case "ddlOrg":
                getDepartmentList(ddlDept, _funcTool.convertToInt(ddlOrg.SelectedItem.Value));
                ddlSec.Items.Clear();
                ddlSec.Items.Insert(0, new ListItem("--- แผนก ---", "-1"));
                break;
            case "ddlDept":
                getSectionList(ddlSec, _funcTool.convertToInt(ddlOrg.SelectedItem.Value), _funcTool.convertToInt(ddlDept.SelectedItem.Value));
                break;
        }
    }
    #endregion dropdown list

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveView("viewList", 0);
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        ViewState["EmpSearchList"] = null;
        ViewState["FingerMachineList"] = null;
        ViewState["EmpSelected"] = null;
        ViewState["FingerMachineSelected"] = null;
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        switch(fvName.ID)
        {
            case "fvEmpSearch":
                if(fvMode == FormViewMode.Insert)
                {
                    DropDownList ddlOrg = (DropDownList)fvEmpSearch.FindControl("ddlOrg");
                    // ddl org
                    getOrganizationList(ddlOrg);
                }
                break;
        }
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setActiveView(string activeTab, int masterType)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        switch(activeTab)
        {
            case "viewList":
                setFormData(fvEmpSearch, FormViewMode.Insert, null);
                
                div_Action.Visible = false;
                div_transfer.Visible = false;
                break;
            case "viewCreate":
                break;
        }
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected data_times callServicePostTimes(string _cmdUrl, data_times _data_times)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_times);
        // litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_times = (data_times)_funcTool.convertJsonToObject(typeof(data_times), _local_json);

        return _data_times;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _data_employee)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_employee);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _local_json);

        return _data_employee;
    }

    public string getStatusIcon(Int32 inStatus)
    {
        return (inStatus == 1 ? "<i class=\"fas fa-check text-success\"></i>" : "<i class=\"fas fa-times text-danger\"></i>");
    }

    private string convertHostToIp(string hostName)
    {
        IPHostEntry hostEntry = Dns.GetHostEntry(hostName);
        if (hostEntry.AddressList.Length > 0)
        {
            return hostEntry.AddressList[0].ToString();
        }
        else
        {
            return "0";
        }
    }

    private void setAdminMachine(int m0_idx, string ip_host)
    {
        // get machine administrator and re-insert it
        tm_machine_admin_detail _admin_list = new tm_machine_admin_detail();
        _admin_list.m0_idx = m0_idx;
        _data_local2.tm_machine_admin_list = new tm_machine_admin_detail[1];
        _data_local2.tm_machine_admin_list[0] = _admin_list;
        _data_local2 = callServicePostTimes(_urlTmGetAdminstratorList, _data_local2);
        if(_data_local2.tm_machine_admin_list != null)
        {
            _admin_idx_list = String.Empty;
            // set administrator list
            foreach(tm_machine_admin_detail admin_list in _data_local2.tm_machine_admin_list)
            {
                if(admin_list.emp_idx > 0)
                {
                    _admin_idx_list += admin_list.emp_idx.ToString() + ",";
                }
            }
            _admin_idx_list = _admin_idx_list.Remove(_admin_idx_list.Length - 1);

            // get template
            search_template_detail _templateTransferList = new search_template_detail();
            _templateTransferList.s_emp_idx = _admin_idx_list;
            _data_local3.search_template_list = new search_template_detail[1];
            _data_local3.search_template_list[0] = _templateTransferList;
            _data_local3 = callServicePostTimes(_urlTmGetTemplate, _data_local3);
            // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_local3));

            // re-insert to machine
            foreach(finger_template_detail emp_template in _data_local3.finger_template_list)
            {
                for (int i = 0; i <= 1; i++) //index 0, index 1
                {
                    sdwEnrollNumber = emp_template.emp_code;
                    iToFingerIndex = i;
                    
                    // write to db
                    finger_template_detail _templateList = new finger_template_detail();
                    _templateList.m0_idx = 0;
                    _templateList.emp_code = sdwEnrollNumber;
                    switch(i) 
                    {
                        case 0:
                            _templateList.index0 = emp_template.index0;
                            sTmpData = emp_template.index0;
                            break;
                        case 1:
                            _templateList.index1 = emp_template.index1;
                            sTmpData = emp_template.index1;
                            break;
                    }
                    sToIp = IPAddress.TryParse(ip_host, out defaultIP) ? ip_host : convertHostToIp(ip_host);
                    _templateList.ip_host = ip_host;
                    _templateList.template_status = 1;
                    _templateList.emp_idx_action = emp_idx_action;
                    _data_local4.finger_template_list = new finger_template_detail[1];
                    _data_local4.finger_template_list[0] = _templateList;

                    // transfer template to machine
                    if ((sToIp != "" || sToIp != "0") && iToPort != 0 && sTmpData != "")
                    {
                        if (_funcScan.setMachineConnect(sToIp, iToPort))
                        {
                            _funcScan.setEnableDevice(iMachineNumber, false);

                            _funcScan.setUserTemplate(iMachineNumber, sdwEnrollNumber, iToFingerIndex, 1, sTmpData);
                            // administrator privilege = 3 //* fixed value *//
                            _funcScan.setUserInfo(iMachineNumber, sdwEnrollNumber, "", "", 3, true);

                            _funcScan.setEnableDevice(iMachineNumber, true);
                            _funcScan.setMachineDisconnect();

                            // insert data to DB
                            _data_local4 = callServicePostTimes(_urlTmSetTemplate, _data_local4);
                        }
                    }
                }
            }
        }
    }

    private string setNewline()
    {
        return "<br />";
    }
    #endregion reuse
}