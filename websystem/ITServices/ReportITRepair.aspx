﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="ReportITRepair.aspx.cs" Inherits="websystem_ITServices_ReportITRepair" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <%----------- Menu Tab Start---------------%>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:Literal ID="text" runat="server"></asp:Literal>
            <div id="BoxTabMenuIndex" runat="server">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand">Menu</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav ulActiveMenu" runat="server">
                            <li id="IndexList" runat="server">
                                <asp:LinkButton ID="lbindex" CssClass="btn_menulist" runat="server" CommandName="btnIndex" OnCommand="btnCommand">ข้อมูลทั่วไป</asp:LinkButton>
                            </li>
                            <li id="Li1" runat="server">
                                <asp:LinkButton ID="lbalterrepair" CssClass="btn_menulist" runat="server" CommandName="btnalterrepair" OnCommand="btnCommand">แจ้งซ่อม</asp:LinkButton>
                            </li>
                            <%--  <li id="Li2" runat="server">
                                <asp:LinkButton ID="lbReportSap" CssClass="btn_menulist" runat="server" CommandName="btnReportSap" OnCommand="btnCommand">สรุปรายงาน SAP</asp:LinkButton>
                            </li>
                            <li id="Li3" runat="server">
                                <asp:LinkButton ID="lbReportIT" CssClass="btn_menulist" runat="server" CommandName="btnReportIT" OnCommand="btnCommand">สรุปรายงาน IT</asp:LinkButton>
                            </li>--%>
                            <li id="Li5" runat="server">
                                <asp:LinkButton ID="lbReportPOS" CssClass="btn_menulist" runat="server" CommandName="btnReportPOS" OnCommand="btnCommand">สรุปรายงาน</asp:LinkButton>
                            </li>
                            <li id="Li4" runat="server">
                                <asp:LinkButton ID="lbReportQuestIT" CssClass="btn_menulist" runat="server" CommandName="btnReportFB" OnCommand="btnCommand">สรุปรายงานแบบสอบถาม</asp:LinkButton>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

            <%-------------- Menu Tab End--------------%>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewReport" runat="server">

                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; ค้นหารายงาน</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทระบบงาน : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddSystemSearch" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <%--    <asp:ListItem Value="0">กรุณาเลือกประเภทระบบงาน</asp:ListItem>
                                            <asp:ListItem Value="2">SAP</asp:ListItem>
                                            <asp:ListItem Value="3">IT</asp:ListItem>
                                            <asp:ListItem Value="20">POS</asp:ListItem>
                                            <asp:ListItem Value="28">RES</asp:ListItem>--%>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="SearchReport" runat="server" Display="None" ControlToValidate="ddSystemSearch" Font-Size="11"
                                            ErrorMessage="กรุณาเลือกประเภทระบบงาน"
                                            ValidationExpression="กรุณาเลือกประเภทระบบงาน"
                                            SetFocusOnError="true" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />

                                    </div>

                                    <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทรายงาน : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddltypereport" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกประเภทรายงาน</asp:ListItem>
                                            <asp:ListItem Value="1">ตาราง</asp:ListItem>
                                            <asp:ListItem Value="2">กราฟ</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SearchReport" runat="server" Display="None" ControlToValidate="ddltypereport" Font-Size="11"
                                            ErrorMessage="กรุณาเลือกประเภทระบบงาน"
                                            ValidationExpression="กรุณาเลือกประเภทระบบงาน"
                                            SetFocusOnError="true" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                    </div>

                                </div>

                                <asp:Panel ID="panel_table" runat="server" Visible="false">

                                    <div class="form-group" runat="server">
                                        <asp:Label ID="Label8" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจากวันที่ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlTypeSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Text="เลือกประเภทการค้นหา ...." Value="00"></asp:ListItem>
                                                <asp:ListItem Text="วันที่รับงาน" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="วันที่ปิดงาน" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="วันที่แจ้งงาน" Value="3"></asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="rqyear" ValidationGroup="SearchReport" runat="server" Display="None"
                                                ControlToValidate="ddlTypeSearchDate" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทปี"
                                                ValidationExpression="กรุณาเลือกประเภทการค้นหา" InitialValue="00" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="rqyear" Width="160" />
                                        </div>
                                        <p class="help-block"><font color="red">**</font></p>
                                    </div>

                                    <div class="form-group" id="panel_searchdate" runat="server">
                                        <asp:Label ID="Label9" runat="server" Text="ระบุวันค้นหา" CssClass="col-sm-2 control-label"></asp:Label>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="00">เลือกเงื่อนไข</asp:ListItem>
                                                <asp:ListItem Value="1">มากกว่า  ></asp:ListItem>
                                                <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                                <asp:ListItem Value="3">ระหว่าง  <></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3 ">

                                            <div class='input-group date'>
                                                <asp:TextBox ID="AddStartdate" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SearchReport" runat="server" Display="None"
                                                    ControlToValidate="AddStartdate" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                    ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="AddEndDate" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            </div>
                                        </div>
                                        <p class="help-block"><font color="red">**</font></p>
                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจากองค์กร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlorgidx" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกองค์กร....</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ค้นหาจากฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlrdeptidx" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกฝ่าย....</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label10" CssClass="col-sm-2 control-label" runat="server" Text="สถานะดำเนินการ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlSearchStatus" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="88">กรุณาเลือกสถานะดำเนินการ....</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>
                                    </div>

                                    <div id="div_it" runat="server" visible="false">

                                        <div class="form-group">
                                            <asp:Label ID="Label3" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาผู้รับงาน : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddladminaccept" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกผู้รับงาน....</asp:ListItem>
                                                </asp:DropDownList>

                                            </div>

                                            <asp:Label ID="Label5" CssClass="col-sm-3 control-label" runat="server" Text="ค้นหาผู้ปิดงาน : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddladmindoing" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกผู้ปิดงาน....</asp:ListItem>
                                                </asp:DropDownList>

                                            </div>

                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label6" CssClass="col-sm-2 control-label" runat="server" Text="เคส LV1 :  " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlITLV1" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                            </div>

                                            <asp:Label ID="Label7" CssClass="col-sm-3 control-label" runat="server" Text="อาการ LV2 : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlITLV2" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">เลือกอาการแจ้งซ่อม</asp:ListItem>

                                                </asp:DropDownList>

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label70" CssClass="col-sm-2 control-label" runat="server" Text="สาเหตุ LV3 :  " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlITLV3" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">เลือกสาเหตุแจ้งซ่อม</asp:ListItem>

                                                </asp:DropDownList>
                                            </div>

                                            <asp:Label ID="Label73" CssClass="col-sm-3 control-label" runat="server" Text="แก้ไข LV4 : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlITLV4" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">เลือกแก้ไขแจ้งซ่อม</asp:ListItem>

                                                </asp:DropDownList>


                                            </div>
                                        </div>


                                    </div>

                                    <div id="div_Sap" runat="server" visible="false">

                                        <div class="form-group">
                                            <asp:Label ID="Label18" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาผู้รับงาน : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlacceptsap" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกผู้รับงาน....</asp:ListItem>
                                                </asp:DropDownList>

                                            </div>

                                            <asp:Label ID="Label23" CssClass="col-sm-3 control-label" runat="server" Text="ค้นหาผู้ปิดงาน : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddldoingsap" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกผู้ปิดงาน....</asp:ListItem>
                                                </asp:DropDownList>

                                            </div>

                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label24" CssClass="col-sm-2 control-label" runat="server" Text="สาเหตุการแจ้งซ่อม(Lv1) :  " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSAPLV1" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                            </div>

                                            <asp:Label ID="Label25" CssClass="col-sm-3 control-label" runat="server" Text="สาเหตุการแจ้งซ่อม(Lv2) : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSAPLV2" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Text="Select Topic ...." Value="0"></asp:ListItem>

                                                </asp:DropDownList>

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label26" CssClass="col-sm-2 control-label" runat="server" Text="สาเหตุการแจ้งซ่อม(Lv3) :  " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSAPLV3" AutoPostBack="true" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                            </div>

                                            <asp:Label ID="Label27" CssClass="col-sm-3 control-label" runat="server" Text="สาเหตุการแจ้งซ่อม(Lv4) : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSAPLV4" AutoPostBack="true" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label28" CssClass="col-sm-2 control-label" runat="server" Text="สาเหตุการแจ้งซ่อม(Lv5) :  " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSAPLV5" AutoPostBack="true" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                            <asp:Label ID="Label32" CssClass="col-sm-3 control-label" runat="server" Text="Priority : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlpriority_tb" AutoPostBack="true" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                            </div>

                                        </div>
                                    </div>


                                    <div id="div_pos" runat="server" visible="false">


                                        <div class="form-group">
                                            <asp:Label ID="Label14" CssClass="col-sm-2 control-label" runat="server" Text="หัวข้อแจ้งซ่อม(Lv1) : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlPOSLV1" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>

                                            </div>
                                            <asp:Label ID="Label15" CssClass="col-sm-3 control-label" runat="server" Text="อาการแจ้งซ่อม(Lv2) : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlPOSLV2" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="เลือกอาการแจ้งซ่อม"></asp:ListItem>
                                                </asp:DropDownList>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label16" CssClass="col-sm-2 control-label" runat="server" Text="แก้ไขแจ้งซ่อม(Lv3) : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlPOSLV3" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="เลือกแก้ไขแจ้งซ่อม"></asp:ListItem>
                                                </asp:DropDownList>

                                            </div>
                                            <asp:Label ID="Label17" CssClass="col-sm-3 control-label" runat="server" Text="สาเหตุแจ้งซ่อม(Lv4) : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlPOSLV4" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="เลือกสาเหตุแจ้งซ่อม"></asp:ListItem>
                                                </asp:DropDownList>

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label11" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาผู้รับงาน : " />
                                            <div class="col-sm-4">
                                                <asp:CheckBoxList ID="chkadminaccept"
                                                    runat="server"
                                                    CellPadding="5"
                                                    CellSpacing="5"
                                                    RepeatColumns="2"
                                                    RepeatDirection="Vertical"
                                                    RepeatLayout="Table"
                                                    TextAlign="Right"
                                                    Width="100%">
                                                </asp:CheckBoxList>

                                            </div>

                                            <asp:Label ID="Label13" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาผู้ปิดงาน : " />
                                            <div class="col-sm-4">
                                                <asp:CheckBoxList ID="chkadmindoing"
                                                    runat="server"
                                                    CellPadding="5"
                                                    CellSpacing="5"
                                                    RepeatColumns="2"
                                                    RepeatDirection="Vertical"
                                                    RepeatLayout="Table"
                                                    TextAlign="Right"
                                                    Width="100%">
                                                </asp:CheckBoxList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label12" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่แจ้งซ่อม : " />
                                            <div class="col-sm-4">
                                                <asp:CheckBoxList ID="chkplace"
                                                    runat="server"
                                                    CellPadding="5"
                                                    CellSpacing="5"
                                                    RepeatColumns="2"
                                                    RepeatDirection="Vertical"
                                                    RepeatLayout="Table"
                                                    TextAlign="Right"
                                                    Width="100%">
                                                </asp:CheckBoxList>

                                            </div>

                                        </div>
                                    </div>

                                    <div id="div_res" runat="server" visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label43" CssClass="col-sm-2 control-label" runat="server" Text="อุปกรณ์ : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddldevicesres" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกอุปกรณ์....</asp:ListItem>

                                                </asp:DropDownList>

                                            </div>
                                            <asp:Label ID="Label44" CssClass="col-sm-3 control-label" runat="server" Text="ช่างดำเนินการ : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlen" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกช่าง...</asp:ListItem>
                                                </asp:DropDownList>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label35" CssClass="col-sm-2 control-label" runat="server" Text="หัวข้อแจ้งซ่อม(Lv1) : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlRESLV1" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="0" Text="เลือกหัวข้อแจ้งซ่อม"></asp:ListItem>

                                                </asp:DropDownList>

                                            </div>
                                            <asp:Label ID="Label36" CssClass="col-sm-3 control-label" runat="server" Text="รายการแจ้งซ่อม(Lv2) : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlRESLV2" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="0" Text="เลือกรายการแจ้งซ่อม"></asp:ListItem>
                                                </asp:DropDownList>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label41" CssClass="col-sm-2 control-label" runat="server" Text="วิธีแก้ไข(Lv3) : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlRESLV3" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="0" Text="เลือกวิธีแก้ไข"></asp:ListItem>

                                                </asp:DropDownList>

                                            </div>
                                            <asp:Label ID="Label42" CssClass="col-sm-3 control-label" runat="server" Text="คำแนะนำ(Lv4) : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlRESLV4" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="เลือกคำแนะนำ"></asp:ListItem>
                                                </asp:DropDownList>

                                            </div>
                                        </div>



                                    </div>

                                </asp:Panel>

                                <asp:Panel ID="panel_chart" runat="server" Visible="false">

                                    <div class="form-group" runat="server" id="Chartitpos">
                                        <asp:Label ID="Label19" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทกราฟ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlchart" AutoPostBack="true" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="00">Select TypeGrant...</asp:ListItem>
                                                <asp:ListItem Value="1">เคส/จำนวนครั้ง</asp:ListItem>
                                                <asp:ListItem Value="2">อาการ/จำนวนครั้ง</asp:ListItem>
                                                <asp:ListItem Value="3">สาเหตุ/จำนวนครั้ง</asp:ListItem>
                                                <asp:ListItem Value="4">แก้ไข/จำนวนครั้ง</asp:ListItem>
                                                <asp:ListItem Value="5">จำนวนแจ้งซ่อม/หน่วยงาน</asp:ListItem>

                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SearchReport" runat="server" Display="None"
                                                ControlToValidate="ddlchart" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทกราฟ"
                                                ValidationExpression="กรุณาเลือกประเภทกราฟ" InitialValue="00" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                        </div>
                                        <p class="help-block"><font color="red">**</font></p>
                                    </div>

                                    <div class="form-group" runat="server" id="Chartsap" visible="false">
                                        <asp:Label ID="Label30" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทกราฟ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlchartsap" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="00">Select TypeGrant...</asp:ListItem>
                                                <asp:ListItem Value="1">Module /จำนวนครั้ง</asp:ListItem>
                                                <asp:ListItem Value="2">Module/Man-Hours</asp:ListItem>
                                                <asp:ListItem Value="3">Count of case LV3</asp:ListItem>
                                                <asp:ListItem Value="4">จำนวนแจ้งซ่อม/หน่วยงาน</asp:ListItem>
                                                <asp:ListItem Value="5">Summary by Dept</asp:ListItem>
                                                <asp:ListItem Value="6">Analysis Top 5 Dept</asp:ListItem>
                                                <asp:ListItem Value="7">Downtime with Priority</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="SearchReport" runat="server" Display="None"
                                                ControlToValidate="ddlchartsap" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทกราฟ"
                                                ValidationExpression="กรุณาเลือกประเภทกราฟ" InitialValue="00" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                            <%--   <p class="help-block"><font color="red">**</font></p>--%>
                                        </div>
                                        <div id="div_priority" runat="server" visible="false">
                                            <asp:Label ID="Label31" runat="server" Text="Priority" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-3" runat="server">
                                                <asp:DropDownList ID="ddlpriority" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group" runat="server" id="Chartres">
                                        <asp:Label ID="Label49" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทกราฟ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlchartres" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="00">Select TypeGrant...</asp:ListItem>
                                                <asp:ListItem Value="1">หัวข้อ/จำนวนครั้ง</asp:ListItem>
                                                <asp:ListItem Value="2">รายการซ่อม/จำนวนครั้ง</asp:ListItem>
                                                <asp:ListItem Value="3">วิธีแก้ไข/จำนวนครั้ง</asp:ListItem>
                                                <asp:ListItem Value="4">คำแนะนำ/จำนวนครั้ง</asp:ListItem>
                                                <asp:ListItem Value="5">จำนวนแจ้งซ่อม/หน่วยงาน</asp:ListItem>
                                                <asp:ListItem Value="6">ค่าใช้จ่าย/เดือน</asp:ListItem>

                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="SearchReport" runat="server" Display="None"
                                                ControlToValidate="ddlchart" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทกราฟ"
                                                ValidationExpression="กรุณาเลือกประเภทกราฟ" InitialValue="00" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />
                                        </div>
                                        <div id="divenreschart" runat="server" visible="false">
                                            <asp:Label ID="Label50" CssClass="col-sm-3 control-label" runat="server" Text="ช่างดำเนินการ : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlenres" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกช่างดำเนินการ</asp:ListItem>
                                                </asp:DropDownList>

                                            </div>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label814" runat="server" Text="เดือน" CssClass="col-sm-2 control-label"></asp:Label>
                                        <div class="col-sm-3" runat="server">
                                            <asp:DropDownList ID="ddlmonth" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="00">Select Month....</asp:ListItem>
                                                <asp:ListItem Value="1">มกราคม</asp:ListItem>
                                                <asp:ListItem Value="2">กุมภาพันธ์</asp:ListItem>
                                                <asp:ListItem Value="3">มีนาคม</asp:ListItem>
                                                <asp:ListItem Value="4">เมษายน</asp:ListItem>
                                                <asp:ListItem Value="5">พฤกษภาคม</asp:ListItem>
                                                <asp:ListItem Value="6">มิถุนายน</asp:ListItem>
                                                <asp:ListItem Value="7">กรกฎาคม</asp:ListItem>
                                                <asp:ListItem Value="8">สิงหาคม</asp:ListItem>
                                                <asp:ListItem Value="9">กันยายน</asp:ListItem>
                                                <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                                <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                                <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <asp:Label ID="Label165" runat="server" Text="ปี" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-3" runat="server">
                                            <asp:DropDownList ID="ddlyear" CssClass="form-control" runat="server">
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                    <div id="divadmin" runat="server" visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label20" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาผู้รับงาน : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddladminaccept_pos" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกผู้รับงาน....</asp:ListItem>
                                                </asp:DropDownList>

                                            </div>

                                            <asp:Label ID="Label21" CssClass="col-sm-3 control-label" runat="server" Text="ค้นหาผู้ปิดงาน : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddladmindoing_pos" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกผู้ปิดงาน....</asp:ListItem>
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="panel_tbsla" runat="server" Visible="false">

                                    <div class="form-group">
                                        <asp:Label ID="Label37" runat="server" Text="เดือน" CssClass="col-sm-2 control-label"></asp:Label>
                                        <div class="col-sm-3" runat="server">
                                            <asp:DropDownList ID="ddlsla_month" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="00">Select Month....</asp:ListItem>
                                                <asp:ListItem Value="1">มกราคม</asp:ListItem>
                                                <asp:ListItem Value="2">กุมภาพันธ์</asp:ListItem>
                                                <asp:ListItem Value="3">มีนาคม</asp:ListItem>
                                                <asp:ListItem Value="4">เมษายน</asp:ListItem>
                                                <asp:ListItem Value="5">พฤกษภาคม</asp:ListItem>
                                                <asp:ListItem Value="6">มิถุนายน</asp:ListItem>
                                                <asp:ListItem Value="7">กรกฎาคม</asp:ListItem>
                                                <asp:ListItem Value="8">สิงหาคม</asp:ListItem>
                                                <asp:ListItem Value="9">กันยายน</asp:ListItem>
                                                <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                                <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                                <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <asp:Label ID="Label38" runat="server" Text="ปี" CssClass="col-sm-3 control-label"></asp:Label>
                                        <div class="col-sm-3" runat="server">
                                            <asp:DropDownList ID="ddlsla_year" CssClass="form-control" runat="server">
                                            </asp:DropDownList>

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label39" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาผู้รับงาน : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsla_admingetjob" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกผู้รับงาน....</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                        <asp:Label ID="Label40" CssClass="col-sm-3 control-label" runat="server" Text="ค้นหาผู้ปิดงาน : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsla_adminclosejob" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกผู้ปิดงาน....</asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label33" runat="server" Text="Priority" CssClass="col-sm-2 control-label"></asp:Label>
                                        <div class="col-sm-3" runat="server">
                                            <asp:DropDownList ID="ddlsla_priority" CssClass="form-control" runat="server">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>


                                <div class="form-group" id="div_button" runat="server" visible="false">
                                    <div class="col-sm-4 col-sm-offset-8">
                                        <asp:LinkButton ID="btnsearch" CssClass="btn btn-success btn-sm" ValidationGroup="SearchReport" runat="server" CommandName="btnsearch" OnCommand="btnCommand" title="Search"><i class="fa fa-search"></i> Search</asp:LinkButton>
                                        <asp:LinkButton ID="btnexport" CssClass="btn btn-primary  btn-sm" runat="server" CommandName="btnexport" OnCommand="btnCommand" title="Export"><i class="glyphicon glyphicon-export"></i> Export Excel</asp:LinkButton>
                                        <asp:LinkButton ID="btnrefresh" CssClass="btn btn-info  btn-sm" runat="server" CommandName="btnrefresh" OnCommand="btnCommand" title="Refresh"><i class="glyphicon glyphicon-refresh"></i> Refresh</asp:LinkButton>

                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>



                    <%--GridView --%>
                    <div class="panel panel-primary" id="grid1" runat="server" visible="false">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-align-justify"></i><strong>&nbsp; ข้อมูลรายงาน</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <asp:GridView ID="GvMaster"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    DataKeyNames="URQIDX"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="info"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    PageSize="10"
                                    OnPageIndexChanging="Master_PageIndexChanging">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center;">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>


                                    <Columns>
                                        <asp:TemplateField HeaderText="ลำดับ" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                                </small>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("DocCode") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="เวลา" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="8%">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label12" runat="server">เวลารวม: </asp:Label></strong>
                                                    <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("ALLTIME") %>' />
                                                    </p>
                                            <strong>
                                                <asp:Label ID="Label15" runat="server">คิด KPI: </asp:Label></strong>
                                                    <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("Downtime_KPI") %>' />
                                                    </p>
                                            <strong>
                                                <asp:Label ID="Label13" runat="server">ไม่คิด KPI: </asp:Label></strong>
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("Downtim_NONKPI") %>' />
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชื่อผู้แจ้ง" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="12%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("EmpName") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ฝ่าย" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("RDeptName") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="สถานที่" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblsecname" runat="server" Text='<%# Eval("LocName") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลแจ้งซ่อม" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="13%">
                                            <ItemTemplate>
                                                <small>

                                                    <strong>
                                                        <asp:Label ID="Label15" runat="server">วันเวลาที่แจ้งงาน: </asp:Label></strong>
                                                    <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("CreateDateUser") %>' />
                                                    <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("TimeCreateJob") %>' />
                                                    </p>
                                                     <strong>
                                                         <asp:Label ID="Label51" runat="server">หมายเหตุแจ้งซ่อม: </asp:Label></strong>
                                                    <asp:Label ID="lblsecname" runat="server" Text='<%# Eval("detailUser") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="สถานะ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblstaname" runat="server" Text='<%# Eval("StaName") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label12" runat="server">ผู้รับงาน: </asp:Label></strong>
                                                    <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("AdminName") %>' />
                                                    </p>
                                            <strong>
                                                <asp:Label ID="Label15" runat="server">วันเวลาที่รับงาน: </asp:Label></strong>
                                                    <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("DateGetJob") %>' />
                                                    <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("TimegetJob") %>' />
                                                    </p>
                                            <strong>
                                                <asp:Label ID="Label13" runat="server">ผู้ปิดงาน: </asp:Label></strong>
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("AdminDoingName") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label22" runat="server">วันเวลาปิดงาน: </asp:Label></strong>
                                                    <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("DateCloseJob") %>' />
                                                    <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("TimecloseJob") %>' />
                                                    </p>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ความเห็นผู้ดูแล" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblcomment" runat="server" Text='<%# Eval("CommentAMDoing") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลปิดงาน" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label12" runat="server">เคส: </asp:Label></strong>
                                                    <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("Name_Code1") %>' />
                                                    </p>
                                            <strong>
                                                <asp:Label ID="Label15" runat="server">อาการ: </asp:Label></strong>
                                                    <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("Name_Code2") %>' />
                                                    </p>
                                            <strong>
                                                <asp:Label ID="Label13" runat="server">แก้ไข: </asp:Label></strong>
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("Name_Code3") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label22" runat="server">สาเหตุ: </asp:Label></strong>
                                                    <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("Name_Code4") %>' />
                                                    </p>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>

                                </asp:GridView>

                                <asp:GridView ID="GvSAP"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    DataKeyNames="URQIDX"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="info"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    PageSize="10"
                                    OnPageIndexChanging="Master_PageIndexChanging">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center;">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>


                                    <Columns>
                                        <asp:TemplateField HeaderText="#" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                                </small>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="No." HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("DocCode") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Downtime" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="8%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("ALLTIME") %>' />
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="AdminName" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="12%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("AdminName") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Dept." HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("RDeptName") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Cost Center" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblsecname" runat="server" Text='<%# Eval("CostCenter") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Module" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblsecname" runat="server" Text='<%# Eval("Name_Code1") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Title" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblstaname" runat="server" Text='<%# Eval("Name_Code2") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Type" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblstaname" runat="server" Text='<%# Eval("Name_Code3") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Reason" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblstaname" runat="server" Text='<%# Eval("Name_Code4") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="How to" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblstaname" runat="server" Text='<%# Eval("Name_Code5") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Man-Hours" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblstaname" runat="server" Text='<%# Eval("Downtime") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Prd/Year." HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblstaname" runat="server" Text='<%# Eval("DG_Mount") %>'></asp:Label>
                                                    /
                                                    <asp:Label ID="Label29" runat="server" Text='<%# Eval("DG_Year") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblstaname" runat="server" Text='<%# Eval("StaName") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>
                                    </Columns>

                                </asp:GridView>

                                <asp:GridView ID="GvSLA"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    DataKeyNames="URQIDX"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="info"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    PageSize="10"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center;">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>


                                    <Columns>
                                        <asp:TemplateField HeaderText="#" ItemStyle-CssClass="text-center" Visible="false">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                                </small>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Priority." HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="lit_pidx" Visible="false" runat="server" Text='<%# Eval("PIDX") %>' /></small>
                                                <asp:Literal ID="lit_prior" runat="server" Text='<%# Eval("Priority_name") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="หมายเลขเอกสาร" ItemStyle-CssClass="text-center" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="8%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="lit_doc" runat="server" Text='<%# Eval("DocCode") %>' />
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ปัญหาที่แจ้ง" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%" ItemStyle-CssClass="text-left">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="lit_detail" runat="server" Text='<%# Eval("detailUser") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Downtime(hr)" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="lit_sum" runat="server" Text='<%# Eval("sumtime") %>' /></small>
                                                <asp:Literal ID="lit_count" Visible="false" runat="server" Text='<%# Eval("countlist_downtime") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Base line(hr)" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="lit_base" runat="server" Text='<%# Eval("baseline") %>'></asp:Literal>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Over(hr)" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="lit_over" runat="server" Text='<%# Eval("overtime") %>'></asp:Literal>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Prd/Year" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="lit_closejob" runat="server" Text='<%# Eval("DatecloseJob") %>'></asp:Literal>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Total Downtime(hr)" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="7%">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Literal ID="Literal4" runat="server" Text="Downtime = " /></strong>

                                                    <asp:Literal ID="lit_sumtime" runat="server" Text='<%# Eval("alltime") %>'></asp:Literal>
                                                    </p>

                                            <strong>
                                                <asp:Label ID="Label15" runat="server">Average =  </asp:Label></strong>
                                                    <asp:Literal ID="lit_average_sumtime" Text='<%# Eval("countlist_downtime") %>' runat="server" />
                                                    </p>
                                           
                                                </small>

                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Total Over(hr)" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="7%">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Literal ID="Listeral4" runat="server" Text="Over = " /></strong>

                                                    <asp:Literal ID="lit_over_sumtime" runat="server"></asp:Literal>
                                                    </p>

                                            <strong>
                                                <asp:Label ID="Labdel15" runat="server">Average =  </asp:Label></strong>
                                                    <asp:Literal ID="lit_over_average" runat="server" />
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>



                                    </Columns>

                                </asp:GridView>

                                <asp:GridView ID="GvRES"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    DataKeyNames="URQIDX"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="info"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    PageSize="10"
                                    ShowFooter="true"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center;">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>


                                    <Columns>
                                        <asp:TemplateField HeaderText="ลำดับ" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                                </small>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="8%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("DocCode") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <%--   <asp:TemplateField HeaderText="เวลาในการดำเนินการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("ALLTIME") %>' />

                                                
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>

                                        <asp:TemplateField HeaderText="ข้อมูลผู้แจ้ง" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label12" runat="server">ชื่อผู้แจ้ง: </asp:Label></strong>
                                                    <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("EmpName") %>' />
                                                    </p>
                                            <strong>
                                                <asp:Label ID="Label15" runat="server">ฝ่าย: </asp:Label></strong>
                                                    <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("RDeptName") %>' />
                                                    </p>
                                            <strong>
                                                <asp:Label ID="Label13" runat="server">สถานที่: </asp:Label></strong>
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("LocName") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label22" runat="server">หมายเหตุแจ้งซ่อม: </asp:Label></strong>
                                                    <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("detailUser") %>' />
                                                    </p>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลเจ้าหน้าที่" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label12" runat="server">ผู้รับงาน: </asp:Label></strong>
                                                    <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("AdminName") %>' />
                                                    </p>
                                            <strong>
                                                <asp:Label ID="Label15" runat="server">วันเวลาที่รับงาน: </asp:Label></strong>
                                                    <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("DateGetJob") %>' />
                                                    <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("TimegetJob") %>' />
                                                    </p>
                                            <strong>
                                                <asp:Label ID="Label13" runat="server">ผู้ปิดงาน: </asp:Label></strong>
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("AdminDoingName") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label22" runat="server">วันเวลาปิดงาน: </asp:Label></strong>
                                                    <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("DateCloseJob") %>' />
                                                    <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("TimecloseJob") %>' />
                                                    </p>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="สถานะรายการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblstaname" runat="server" Text='<%# Eval("StaName") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ความเห็นผู้ดูแล" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblcomment" runat="server" Text='<%# Eval("CommentAMDoing") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <div style="text-align: right;">
                                                    <asp:Literal ID="lit_" runat="server" Text="รวม :"></asp:Literal>
                                                </div>
                                            </FooterTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ข้อมูลปิดงาน" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%">
                                            <ItemTemplate>
                                                <small>

                                                    <strong>
                                                        <asp:Label ID="Label46" runat="server">อุปกรณ์: </asp:Label></strong>
                                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("devices_name") %>' />
                                                    </p>

                                                    
                                                    <strong>
                                                        <asp:Label ID="Label47" runat="server">ช่างดำเนินการ: </asp:Label></strong>
                                                    <asp:Literal ID="Literal9" runat="server" Text='<%# Eval("en_name") %>' />
                                                    </p>

                                                    <strong>
                                                        <asp:Label ID="Label12" runat="server">หัวข้อ: </asp:Label></strong>
                                                    <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("RES1_Name") %>' />
                                                    </p>
                                            <strong>
                                                <asp:Label ID="Label15" runat="server">รายการซ่อม: </asp:Label></strong>
                                                    <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("RES2_Name") %>' />
                                                    </p>
                                            <strong>
                                                <asp:Label ID="Label13" runat="server">วิธีแก้ไข: </asp:Label></strong>
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("RES3_Name") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label22" runat="server">คำแนะนำ: </asp:Label></strong>
                                                    <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("RES4_Name") %>' />
                                                    </p>
                                                      <strong>
                                                          <asp:Label ID="Label45" runat="server">ค่าใช้จ่าย: </asp:Label></strong>
                                                    <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("price") %>' />
                                                    </p>
                                                      <strong>
                                                          <asp:Label ID="Label48" runat="server">เวลาในการดำเนินการ: </asp:Label></strong>
                                                    <asp:Literal ID="Literal10" runat="server" Text='<%# Eval("ALLTIME") %>' />
                                                    </p>
                                                </small>
                                            </ItemTemplate>

                                            <FooterTemplate>
                                                <div style="text-align: right;">
                                                    <asp:Literal ID="lit_total" runat="server"></asp:Literal>
                                                </div>
                                            </FooterTemplate>
                                        </asp:TemplateField>




                                    </Columns>

                                </asp:GridView>


                            </div>
                        </div>
                    </div>



                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="true" OnRowDataBound="Master_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับที่" ItemStyle-Width="100">
                                <ItemTemplate>
                                    <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>


                    <asp:GridView ID="GVExportRES" runat="server" AutoGenerateColumns="true" OnRowDataBound="Master_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับที่" ItemStyle-Width="100">
                                <ItemTemplate>
                                    <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="" ItemStyle-Width="100" Visible="false">
                                <ItemTemplate>
                                    <asp:Literal ID="litprice" runat="server" Text='<%# Eval("price") %>'></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รวมค่าใช้จ่าย" ItemStyle-Width="100">
                                <ItemTemplate>
                                    <asp:Literal ID="littotalprice" runat="server" Text=""></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>


                    <div class="panel panel-info" id="grant" runat="server" visible="false">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>&nbsp; ข้อมูลรายงาน</strong></h3>
                        </div>
                        <div class="panel-body">
                            <asp:Literal ID="litReportChart" runat="server" />
                            <asp:Literal ID="litReportChart1" runat="server" Visible="false" />
                        </div>
                    </div>

                    <div class="panel panel-info" id="grant_priority" runat="server" visible="false">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>&nbsp; ข้อมูลรายงาน</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <div class="panel panel-warning" runat="server">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>
                                            &nbsp; ด่วนทีสุด 
                                        </div>
                                        <div class="panel-body">
                                            <asp:Literal ID="litReportChart_urgent" runat="server" />

                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="panel panel-warning" runat="server">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>
                                            &nbsp; ด่วน 
                                        </div>
                                        <div class="panel-body">
                                            <asp:Literal ID="litReportChart_high" runat="server" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="col-lg-6">
                                    <div class="panel panel-warning" runat="server">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>
                                            &nbsp; ปานกลาง 
                                        </div>
                                        <div class="panel-body">
                                            <asp:Literal ID="litReportChart_medium" runat="server" />
                                        </div>
                                    </div>

                                </div>
                                <div class="col-lg-6">
                                    <div class="panel panel-warning" runat="server">
                                        <div class="panel-heading">
                                            <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>
                                            &nbsp; ไม่ด่วน 
                                        </div>
                                        <div class="panel-body">
                                            <asp:Literal ID="litReportChart_low" runat="server" />
                                        </div>
                                    </div>

                                </div>
                            </div>



                        </div>
                    </div>

                    <div id="gvchart_priority" runat="server" visible="false">
                        <div class="col-lg-12">

                            <asp:GridView ID="GvChartSAP"
                                runat="server"
                                AutoGenerateColumns="false"
                                DataKeyNames="URQIDX"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="info"
                                HeaderStyle-Height="40px"
                                OnRowDataBound="Master_RowDataBound"
                                ShowFooter="true">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center;">Data Cannot Be Found</div>
                                </EmptyDataTemplate>


                                <Columns>
                                    <asp:TemplateField HeaderText="Priority." HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="lit_prior" runat="server" Text='<%# Eval("Priority_name") %>' /></small>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <div style="text-align: right;">
                                                <asp:Literal ID="lit_tot" runat="server" Text="Total :"></asp:Literal>
                                            </div>

                                        </FooterTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Base line(hr)" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="lit_base" runat="server" Text='<%# Eval("baseline") %>'></asp:Literal>
                                            </small>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate>
                                            <div style="text-align: center; background-color: chartreuse;">
                                                <asp:Literal ID="lit_totbase" runat="server"></asp:Literal>
                                            </div>

                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Actual(hr)" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="lit_actual" runat="server" Text='<%# Eval("sumtime") %>'></asp:Literal>
                                            </small>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate>
                                            <div style="text-align: center; background-color: chartreuse;">
                                                <asp:Literal ID="lit_totactual" runat="server"></asp:Literal>
                                            </div>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Over(hr)" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="lit_over" runat="server" Text='<%# Eval("overtime") %>'></asp:Literal>
                                            </small>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate>
                                            <div style="text-align: center; background-color: chartreuse;">
                                                <asp:Literal ID="lit_totover" runat="server"></asp:Literal>
                                            </div>
                                        </FooterTemplate>
                                    </asp:TemplateField>



                                </Columns>

                            </asp:GridView>

                        </div>

                    </div>
                </asp:View>

            </asp:MultiView>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnsearch" />
            <asp:PostBackTrigger ControlID="btnexport" />
        </Triggers>
    </asp:UpdatePanel>

</asp:Content>

