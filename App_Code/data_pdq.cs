﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_roombooking
/// </summary>
/// 
[Serializable]
[XmlRoot("data_pdq")]
public class data_pdq
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    // --- master ---//
    [XmlElement("pdq_m0_detail_list")]
    public pdq_m0_detail[] pdq_m0_detail_list { get; set; }


}

[Serializable]
public class pdq_m0_detail
{

    [XmlElement("order_idx")]
    public int order_idx { get; set; }

    [XmlElement("order_name")]
    public string order_name { get; set; }

    [XmlElement("line_idx")]
    public int line_idx { get; set; }

    [XmlElement("line_name")]
    public string line_name { get; set; }


}