﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_Sir_TEST_JobINtroductory : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    service_execute _service_execute = new service_execute();
    data_job _data_job = new data_job();

    //DBConn DBConn = new DBConn();
    //private FunctionWeb _functionWeb = new FunctionWeb();
    private string JOBActionAc = "JOB";
    private string _jobs = "jobs";
    private string BoxXML;
    string localXml = String.Empty;

    //string _iBookingConn = "iBookingConn";
    //int _actionType = 0;
    //string _local_xml = "";
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!this.IsPostBack)
        {
            Select_ddPrefix();
            Select_education();
            Select_ddpositionpersons();
            Select_ddpositionpersons2();
            Select_dd_Y();
        }
    }

    #region Dataset
    protected void Profile_Show()
    {
        var Profile = new DataSet();
        Profile.Tables.Add("Profile_S");

        Profile.Tables[0].Columns.Add("ddPrefix", typeof(String));
        Profile.Tables[0].Columns.Add("tbname", typeof(String));
        Profile.Tables[0].Columns.Add("tblastname", typeof(String));
        Profile.Tables[0].Columns.Add("tbAge", typeof(String));
        Profile.Tables[0].Columns.Add("tbnumber", typeof(String));
        Profile.Tables[0].Columns.Add("tbEmail", typeof(String));
        Profile.Tables[0].Columns.Add("tbNameInstitute", typeof(String));
        Profile.Tables[0].Columns.Add("ddEducation", typeof(String));
        Profile.Tables[0].Columns.Add("tbCourseTaken", typeof(String));
        Profile.Tables[0].Columns.Add("tbposition1", typeof(String));
        Profile.Tables[0].Columns.Add("tbposition2", typeof(String));
        Profile.Tables[0].Columns.Add("tbmoney", typeof(String));

        if (ViewState["vsBuyProfile_S"] == null)
        {
            ViewState["vsBuyProfile_S"] = Profile;
        }
    }
    #endregion

    #region insert
    protected void Insert_Master_List()
    {
        string _local_xml = "";
        Insert_Empbasic_list AddMainEmp = new Insert_Empbasic_list();
        _data_job.Insert_Empbasic = new Insert_Empbasic_list[1];
        AddMainEmp.Prefix = int.Parse(ViewState["ddPrefix"].ToString()); //
        AddMainEmp.Name_Emp = ViewState["tbname"].ToString(); //
        AddMainEmp.Lastname_Emp = ViewState["tblastname"].ToString(); //
        AddMainEmp.Age_Emp = int.Parse(ViewState["tbAge"].ToString()); //
        AddMainEmp.Phone_number = ViewState["tbnumber"].ToString(); //
        AddMainEmp.Email_Emp = ViewState["tbEmail"].ToString(); //
        AddMainEmp.NameINstitute = ViewState["tbNameInstitute"].ToString();
        AddMainEmp.Education = int.Parse(ViewState["ddEducation"].ToString());
        AddMainEmp.Course = ViewState["tbCourseTaken"].ToString();
        AddMainEmp.position1 = int.Parse(ViewState["tbposition1"].ToString());
        AddMainEmp.position2 = int.Parse(ViewState["tbposition2"].ToString());
        AddMainEmp.WantMoney = ViewState["tbmoney"].ToString();
        _data_job.Insert_Empbasic[0] = AddMainEmp;
        
        //test.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_job));
        //data_job add_main = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 46));

        _local_xml = _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 46);
        _data_job = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _local_xml);

        ViewState["Rutrun"] = _data_job.return_code;
        if (ViewState["Rutrun"].ToString() != "มีข้อมูลอยู่แล้ว")
        {
            _funcTool.showAlert(this, "บันทึกข้อมูลสำเร็จ");
            MvtestMaster.SetActiveView(ViewAdd10);
        }
        else
        {
            _funcTool.showAlert(this, "มีข้อมูลอยู่แล้ว");
        }
    }
    #endregion

    #region เรียก Dropdown
    protected void Select_dd_Y()
    {
        DropDownList tbAge = (DropDownList)fvExample.FindControl("tbAge");
        tbAge.AppendDataBoundItems = true;
        tbAge.Items.Add(new ListItem("เลือกอายุ", "00"));
        for (int a = 17; a < 80; a++)
        {
            int b = 1;
            int id_Y = b + a;
            int st_id_Y = id_Y;
            tbAge.Items.Add(new ListItem(st_id_Y.ToString(), st_id_Y.ToString()));
        }
    }

    protected void Select_ddPrefix()
    {
        DropDownList ddPrefix = (DropDownList)fvExample.FindControl("ddPrefix");
        ddPrefix.AppendDataBoundItems = true;
        ddPrefix.Items.Add(new ListItem("กรุณาเลือกคำนำหน้าชื่อ", "00"));

        ddPrefix_list SelectddPrefix = new ddPrefix_list();
        _data_job.ddPrefix = new ddPrefix_list[1];
        _data_job.ddPrefix[0] = SelectddPrefix;

        data_job dd22 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 32));

        ddPrefix.DataSource = dd22.ddPrefix;
        ddPrefix.DataTextField = "PrefixNameTH";
        ddPrefix.DataValueField = "PrefixIDX";
        ddPrefix.DataBind();
    }

    protected void Select_education()
    {
        DropDownList ddEducation = (DropDownList)fvExample.FindControl("ddEducation");
        ddEducation.AppendDataBoundItems = true;
        ddEducation.Items.Add(new ListItem("กรุณาเลือกระดับการศึกษา", "00"));

        education_list Selectddeducation = new education_list();
        _data_job.education = new education_list[1];
        _data_job.education[0] = Selectddeducation;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd8 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 17));
        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        ddEducation.DataSource = dd8.education;
        ddEducation.DataTextField = "Edu_name";
        ddEducation.DataValueField = "Eduidx";
        ddEducation.DataBind();
    }

    protected void Select_ddpositionpersons()
    {
        DropDownList tbposition1 = (DropDownList)fvExample.FindControl("tbposition1");
        tbposition1.AppendDataBoundItems = true;
        tbposition1.Items.Add(new ListItem("กรุณาเลือกตำแหน่ง", "00"));

        ddpositionbusiness_list Selectddpositionbusiness = new ddpositionbusiness_list();
        _data_job.ddpositionbusiness = new ddpositionbusiness_list[1];
        _data_job.ddpositionbusiness[0] = Selectddpositionbusiness;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd12 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 18));

        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        tbposition1.DataSource = dd12.ddpositionbusiness;
        tbposition1.DataTextField = "PosGroupNameTH";
        tbposition1.DataValueField = "PosGroupIDX";
        tbposition1.DataBind();
    }

    protected void Select_ddpositionpersons2()
    {
        DropDownList tbposition2 = (DropDownList)fvExample.FindControl("tbposition2");
        tbposition2.AppendDataBoundItems = true;
        tbposition2.Items.Add(new ListItem("กรุณาเลือกตำแหน่ง", "00"));

        ddpositionbusiness_list Selectddpositionbusiness = new ddpositionbusiness_list();
        _data_job.ddpositionbusiness = new ddpositionbusiness_list[1];
        _data_job.ddpositionbusiness[0] = Selectddpositionbusiness;

        ///////////////////////////////////////////เทรนนิ่ง service พี่พร (ตัวอย่าง)
        data_job dd122 = (data_job)_funcTool.convertXmlToObject(typeof(data_job), _service_execute.actionExec(_jobs, "data_job", JOBActionAc, _data_job, 18));

        ///////////////////////////////////////////
        //localXml = _functionWeb.ConvertObjectToXml(data_job);
        //text.Text = localXml;

        tbposition2.DataSource = dd122.ddpositionbusiness;
        tbposition2.DataTextField = "PosGroupNameTH";
        tbposition2.DataValueField = "PosGroupIDX";
        tbposition2.DataBind();
    }
    #endregion

    #region TextChnag
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {
        string d = string.Format("{0:#,##0}", double.Parse(tbmoney.Text));
        tbmoney.Text = d;
    }
    #endregion

    #region บาย Repeater
    protected void Repeater1_PreRender(object sender, RepeaterItemEventArgs e)
    {
        Panel Panel0 = (Panel)e.Item.FindControl("Panel0");
        var Repet = (Repeater)sender;
        switch (Repet.ID)
        {
            case "RepeaterEmp":
                if(ViewState["tbNameInstitute"].ToString() != "ไม่มีข้อมูล")
                {
                    Panel0.Visible = true;
                }
                else
                {
                    Panel0.Visible = false;
                }
                break;
        }
    }
    #endregion

    #region Btncommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        Repeater RepeaterEmp = (Repeater)fvExample.FindControl("RepeaterEmp");
        //Panel Edu_hind = (Panel)RepeaterEmp.FindControl("Edu_hind");
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "pass":
                Insert_Master_List();
                break;

            case "AddBasic":
                Profile_Show();
                ViewState["ddPrefix"] = int.Parse(((DropDownList)fvExample.FindControl("ddPrefix")).SelectedValue); //
                DropDownList ddPrefix = (DropDownList)fvExample.FindControl("ddPrefix");
                ViewState["ddPrefix_txt"] = ddPrefix.SelectedItem.Text;
                TextBox tbname = (TextBox)fvExample.FindControl("tbname"); //
                ViewState["tbname"] = tbname.Text;
                TextBox tblastname = (TextBox)fvExample.FindControl("tblastname"); //
                ViewState["tblastname"] = tblastname.Text;
                DropDownList tbAge = (DropDownList)fvExample.FindControl("tbAge"); //
                ViewState["tbAge"] = tbAge.SelectedValue;
                TextBox tbnumber = (TextBox)fvExample.FindControl("tbnumber"); //
                ViewState["tbnumber"] = tbnumber.Text;
                TextBox tbEmail = (TextBox)fvExample.FindControl("tbEmail"); //
                ViewState["tbEmail"] = tbEmail.Text;
                TextBox tbNameInstitute = (TextBox)fvExample.FindControl("tbNameInstitute"); //
                if(tbNameInstitute.Text == "")
                {
                    ViewState["tbNameInstitute"] = "ไม่มีข้อมูล";
                }
                else
                {
                    ViewState["tbNameInstitute"] = tbNameInstitute.Text;
                }
                DropDownList ddEducationS = (DropDownList)fvExample.FindControl("ddEducation");
                if (ddEducationS.SelectedValue == "" || ddEducationS.SelectedValue == "0")
                {
                    ViewState["ddEducation"] = 0;
                }
                else
                {
                    ViewState["ddEducation"] = ddEducationS.SelectedValue;
                }
                DropDownList ddEducation = (DropDownList)fvExample.FindControl("ddEducation");
                ViewState["ddEducation_txt"] = ddEducation.SelectedItem.Text; //
                TextBox tbCourseTaken = (TextBox)fvExample.FindControl("tbCourseTaken"); //
                if (tbCourseTaken.Text == "")
                {
                    ViewState["tbCourseTaken"] = "ไม่มีข้อมูล";
                }
                else
                {
                    ViewState["tbCourseTaken"] = tbCourseTaken.Text;
                }
                ViewState["tbposition1"] = int.Parse(((DropDownList)fvExample.FindControl("tbposition1")).SelectedValue); //
                DropDownList tbposition1 = (DropDownList)fvExample.FindControl("tbposition1");
                ViewState["tbposition1_txt"] = tbposition1.SelectedItem.Text; //
                ViewState["tbposition2"] = int.Parse(((DropDownList)fvExample.FindControl("tbposition2")).SelectedValue); //
                DropDownList tbposition2 = (DropDownList)fvExample.FindControl("tbposition2");
                ViewState["tbposition2_txt"] = tbposition2.SelectedItem.Text; //
                TextBox tbmoney = (TextBox)fvExample.FindControl("tbmoney"); //
                ViewState["tbmoney"] = tbmoney.Text;
                
                var dsProfile = (DataSet)ViewState["vsBuyProfile_S"];
                dsProfile.Clear();
                var drProfile = dsProfile.Tables[0].NewRow();

                if (dsProfile.Tables[0].Rows.Count <= 0)
                {
                    Profile_Show();
                    drProfile["ddPrefix"] = ViewState["ddPrefix_txt"].ToString();
                    drProfile["tbname"] = ViewState["tbname"].ToString();
                    drProfile["tblastname"] = ViewState["tblastname"].ToString();
                    drProfile["tbAge"] = ViewState["tbAge"].ToString();
                    drProfile["tbnumber"] = ViewState["tbnumber"].ToString();
                    drProfile["tbEmail"] = ViewState["tbEmail"].ToString();
                    drProfile["tbNameInstitute"] = ViewState["tbNameInstitute"].ToString();
                    drProfile["ddEducation"] = ViewState["ddEducation_txt"].ToString();
                    drProfile["tbCourseTaken"] = ViewState["tbCourseTaken"].ToString();
                    drProfile["tbposition1"] = ViewState["tbposition1_txt"].ToString();
                    drProfile["tbposition2"] = ViewState["tbposition2_txt"].ToString();
                    drProfile["tbmoney"] = ViewState["tbmoney"].ToString();

                    dsProfile.Tables[0].Rows.Add(drProfile);
                    ViewState["vsBuyProfile_S"] = dsProfile;

                    RepeaterEmp.DataSource = dsProfile.Tables[0];
                    RepeaterEmp.DataBind();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal();", true);
                }

                //Insert_Master_List();
                //ddPrefix.SelectedValue = "00";
                //tbname.Text = String.Empty;
                //tblastname.Text = String.Empty;
                //tbAge.Text = String.Empty;
                //tbnumber.Text = String.Empty;
                //tbEmail.Text = String.Empty;
                //tbNameInstitute.Text = String.Empty;
                //ddEducation.SelectedValue = "00";
                //tbCourseTaken.Text = String.Empty;
                //tbposition1.SelectedValue = "00";
                //tbposition2.SelectedValue = "00";
                //tbmoney.Text = String.Empty;
                //Page.ClientScript.RegisterStartupScript(Page.GetType(), null, "btnSubmit_Edit_Click()", true);
                break;
        }
    }
    #endregion

    #region Checkbox ซ่อนหรือแสดง
    protected void Checkbox(object sender, EventArgs e)
    {
        var cb = (CheckBox)sender;
        Panel NoEdu = (Panel)fvExample.FindControl("NoEdu");
        TextBox tbNameInstitute = (TextBox)fvExample.FindControl("tbNameInstitute");
        DropDownList ddEducation = (DropDownList)fvExample.FindControl("ddEducation");
        TextBox tbCourseTaken = (TextBox)fvExample.FindControl("tbCourseTaken");
        switch (cb.ID)
        {
            case "hide_education":
                if (hide_education.Checked)
                {
                    tbNameInstitute.Text = "";
                    ddEducation.SelectedValue = "00";
                    tbCourseTaken.Text = "";
                    NoEdu.Visible = false;
                }
                else
                {
                    NoEdu.Visible = true;
                }
                break;
        }
    }
    #endregion
}