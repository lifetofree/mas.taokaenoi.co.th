﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ImproperCalendarEvent
/// </summary>
public class ImproperShiftCalendarEvent
{
    public int id { get; set; }  
    public string title { get; set; }
    public string description { get; set; }
    public string start { get; set; }
    public string end { get; set; }
    public string parttime_name_th { get; set; }
    public int u0_empshift_idx { get; set; }
    public bool allDay { get; set; }
}