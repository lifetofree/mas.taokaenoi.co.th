using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

public partial class websystem_conference_admin_conference : System.Web.UI.Page {
    #region initial function/data
    function_tool _funcTool = new function_tool ();

    data_employee _data_employee = new data_employee ();
    data_conference _data_conference = new data_conference (); //--rc4 >> hpR97A56kQ

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;

    const int CONF_TYPE = 2;
    const int CONF_ROUND = 2;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlConferenceGetConfData = _serviceUrl + ConfigurationManager.AppSettings["urlConferenceGetConfData"];
    static string _urlConferenceSetConfData = _serviceUrl + ConfigurationManager.AppSettings["urlConferenceSetConfData"];
    #endregion initial function/data

    protected void Page_Init (object sender, EventArgs e) {
        _emp_idx = _funcTool.convertToInt (Session["emp_idx"].ToString ());
    }

    protected void Page_Load (object sender, EventArgs e) {
        if (!IsPostBack) {
            initPage ();
        }

        linkBtnTrigger (lbExport);
        linkBtnTrigger (lbItemSearch);
        linkBtnTrigger (lbItemReset);
        try { linkGvTrigger (gvRegisterList); } catch { }
    }

    #region event command
    protected void navCommand (object sender, CommandEventArgs e) {
        string cmdName = e.CommandName.ToString ();
        string cmdArg = e.CommandArgument.ToString ();

        switch (cmdName) {
            case "navList":
                // clearViewState();
                // setActiveTab("viewList", 0);
                break;
        }
    }

    protected void btnCommand (object sender, CommandEventArgs e) {
        string cmdName = e.CommandName.ToString ();
        string cmdArg = e.CommandArgument.ToString ();

        switch (cmdName) {
            case "cmdUnlock":
                conf_user_detail_u0 _unlock = new conf_user_detail_u0 ();
                _data_conference.conf_user_list_u0 = new conf_user_detail_u0[1];
                _unlock.u0_idx = _funcTool.convertToInt (cmdArg);
                _unlock.user_flag = 0;
                _unlock.conf_type = 1;
                _unlock.conf_round = 1;
                _data_conference.conf_user_list_u0[0] = _unlock;
                _data_conference = callServicePostConference (_urlConferenceSetConfData, _data_conference);

                _funcTool.setGvData (gvUserList, _data_conference.conf_user_list_u0);
                hlSetTotop.Focus ();
                break;
            case "cmdJoin":
                conf_user_detail_u0 _join = new conf_user_detail_u0 ();
                _data_conference.conf_user_list_u0 = new conf_user_detail_u0[1];
                _join.u0_idx = _funcTool.convertToInt (cmdArg);
                _join.user_flag = 1;
                _join.conf_type = 2;
                _join.conf_round = 2;
                _data_conference.conf_user_list_u0[0] = _join;
                _data_conference = callServicePostConference (_urlConferenceSetConfData, _data_conference);

                _funcTool.setGvData (gvRegisterList, _data_conference.conf_user_list_u0);

                ViewState["DataList"] = _data_conference.conf_user_list_u0;
                ViewState["ReportListExport"] = _data_conference.conf_user_list_u0;
                hlSetTotop.Focus ();
                break;
            case "cmdUnjoin":
                conf_user_detail_u0 _unjoin = new conf_user_detail_u0 ();
                _data_conference.conf_user_list_u0 = new conf_user_detail_u0[1];
                _unjoin.u0_idx = _funcTool.convertToInt (cmdArg);
                _unjoin.user_flag = 0;
                _unjoin.conf_type = 2;
                _unjoin.conf_round = 2;
                _data_conference.conf_user_list_u0[0] = _unjoin;
                _data_conference = callServicePostConference (_urlConferenceSetConfData, _data_conference);

                _funcTool.setGvData (gvRegisterList, _data_conference.conf_user_list_u0);

                ViewState["DataList"] = _data_conference.conf_user_list_u0;
                ViewState["ReportListExport"] = _data_conference.conf_user_list_u0;
                hlSetTotop.Focus ();
                break;
            case "cmdItemSearch":
                conf_user_detail_u0[] _tempData = (conf_user_detail_u0[]) ViewState["DataList"];

                var _linqSearch = from o in _tempData
                select o;

                if (tbItemSearch.Text.Trim () != "")
                    _linqSearch = from o1 in _linqSearch 
                    where o1.user_firstname.Contains (tbItemSearch.Text) ||
                    o1.user_lastname.Contains (tbItemSearch.Text) ||
                    o1.user_mail.Contains (tbItemSearch.Text) ||
                    o1.user_company.Contains (tbItemSearch.Text) ||
                    o1.user_name.Contains (tbItemSearch.Text)
                    select o1;

                ViewState["ReportListExport"] = _linqSearch.ToArray ();
                _funcTool.setGvData (gvRegisterList, _linqSearch.ToList ());
                hlSetTotop.Focus ();
                break;
            case "cmdItemReset":
                tbItemSearch.Text = String.Empty;
                _funcTool.setGvData (gvRegisterList, ((conf_user_detail_u0[]) ViewState["DataList"]));
                break;
            case "cmdExport":
                // export data
                conf_user_detail_u0[] _reportData = (conf_user_detail_u0[]) ViewState["ReportListExport"];
                DataTable tableReport = new DataTable ();
                int _count_row = 0;

                if (_reportData.Count () == 0) {
                    ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('ไม่พบข้อมูลที่ต้องการ Export ค่ะ');", true);
                    return;
                }

                if (_reportData != null) {
                    tableReport.Columns.Add ("#", typeof (String));
                    tableReport.Columns.Add ("Name-Surname", typeof (String));
                    // tableReport.Columns.Add ("Surname", typeof (String));
                    tableReport.Columns.Add ("E-mail", typeof (String));
                    tableReport.Columns.Add ("Company / Fund name", typeof (String));
                    tableReport.Columns.Add ("WebEx display name", typeof (String));
                    tableReport.Columns.Add ("Status", typeof (String));

                    foreach (var temp_row in _reportData) {
                        DataRow add_row = tableReport.NewRow ();

                        add_row[0] = (_count_row + 1).ToString ();
                        add_row[1] = temp_row.user_firstname.ToString ();
                        // add_row[2] = temp_row.user_lastname.ToString ();
                        add_row[2] = temp_row.user_mail.ToString ();
                        add_row[3] = temp_row.user_company.ToString ();
                        add_row[4] = temp_row.user_name.ToString ();
                        add_row[5] = temp_row.user_flag == 0 ? "-" : "Join";

                        tableReport.Rows.InsertAt (add_row, _count_row++);
                    }

                    WriteExcelWithNPOI (tableReport, "xls", "register-list");
                } else {
                    ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('ไม่พบข้อมูลที่ต้องการ Export ค่ะ');", true);
                }

                setActiveTab ("viewList", 0);
                hlSetTotop.Focus ();
                break;
        }
    }
    #endregion event command

    #region reuse
    protected void initPage () {
        clearSession ();
        clearViewState ();

        setActiveTab ("viewList", 0);

        hlSetTotop.Focus ();
    }

    protected void clearSession () {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState () {
        ViewState["DataList"] = null;
        ViewState["ReportListExport"] = null; //linq result
    }

    protected void setActiveTab (string activeTab, int doc_idx) {
        setActiveView (activeTab, doc_idx);
        setActiveTabBar (activeTab);

        switch (activeTab) {
            case "viewList":
                search_conf_data_detail _search_key = new search_conf_data_detail ();
                _search_key.s_conf_type = CONF_TYPE.ToString ();
                _search_key.s_conf_round = CONF_ROUND.ToString ();
                _data_conference.search_conf_data_list = new search_conf_data_detail[1];
                _data_conference.search_conf_data_list[0] = _search_key;
                _data_conference.conf_mode = "20";
                _data_conference = callServicePostConference (_urlConferenceGetConfData, _data_conference);

                // litDebug.Text = _funcTool.convertObjectToJson(_data_conference);
                switch (CONF_TYPE) {
                    case 1:
                        _funcTool.setGvData (gvUserList, _data_conference.conf_user_list_u0);
                        break;
                    case 2:
                        _funcTool.setGvData (gvRegisterList, _data_conference.conf_user_list_u0);
                        break;
                }
                ViewState["DataList"] = _data_conference.conf_user_list_u0;
                ViewState["ReportListExport"] = _data_conference.conf_user_list_u0;
                break;
        }
    }

    protected void setActiveView (string activeTab, int doc_idx) {
        mvSystem.SetActiveView ((View) mvSystem.FindControl (activeTab));
    }

    protected void setActiveTabBar (string activeTab) {
        switch (activeTab) {
            case "viewList":
                li0.Attributes.Add ("class", "active");
                // li1.Attributes.Add("class", "");
                break;
                // case "viewCreate":
                //     li0.Attributes.Add("class", "");
                //     li1.Attributes.Add("class", "active");
                //     break;
        }
    }

    protected data_conference callServicePostConference (string _cmdUrl, data_conference _data_conference) {
        // convert to json
        _local_json = _funcTool.convertObjectToJson (_data_conference);
        // litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost (_cmdUrl, _local_json);
        // litDebug.Text = _local_json;

        // convert json to object
        _data_conference = (data_conference) _funcTool.convertJsonToObject (typeof (data_conference), _local_json);

        return _data_conference;
    }

    protected void linkBtnTrigger (LinkButton linkBtnID) {
        UpdatePanel updatePanel = Page.Master.FindControl ("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger ();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add (triggerLinkBtn);
    }

    protected void linkGvTrigger (GridView linkGvID) {
        UpdatePanel updatePanel = Page.Master.FindControl ("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerGridView = new PostBackTrigger ();
        triggerGridView.ControlID = linkGvID.UniqueID;
        updatePanel.Triggers.Add (triggerGridView);
    }

    protected void WriteExcelWithNPOI (DataTable dt, String extension, String fileName) {
        IWorkbook workbook;
        if (extension == "xlsx") {
            workbook = new XSSFWorkbook ();
        } else if (extension == "xls") {
            workbook = new HSSFWorkbook ();
        } else {
            throw new Exception ("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet ("Sheet 1");
        IRow row1 = sheet1.CreateRow (0);
        for (int j = 0; j < dt.Columns.Count; j++) {
            ICell cell = row1.CreateCell (j);
            String columnName = dt.Columns[j].ToString ();
            cell.SetCellValue (columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++) {
            IRow row = sheet1.CreateRow (i + 1);
            for (int j = 0; j < dt.Columns.Count; j++) {
                ICell cell = row.CreateCell (j);
                String columnName = dt.Columns[j].ToString ();
                cell.SetCellValue (dt.Rows[i][columnName].ToString ());
            }
        }
        using (var exportData = new MemoryStream ()) {
            Response.Clear ();
            workbook.Write (exportData);
            if (extension == "xlsx") {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader ("Content-Disposition", string.Format ("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite (exportData.ToArray ());
            } else if (extension == "xls") {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader ("Content-Disposition", string.Format ("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite (exportData.GetBuffer ());
            }
            Response.End ();
        }
    }
    #endregion reuse
}