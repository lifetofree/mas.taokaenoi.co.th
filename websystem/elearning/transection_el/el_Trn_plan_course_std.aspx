﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_Trn_plan_course_std.aspx.cs" Inherits="websystem_el_Trn_plan_course_std" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litDebug" runat="server" Text=""></asp:Literal>
    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <asp:MultiView ID="MultiViewBody" runat="server" ActiveViewIndex="0">
        <asp:View ID="View_plan_pm" runat="server">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

                    <asp:Panel ID="pnlsearch" runat="server">
                        <div class="panel panel-primary m-t-10">
                            <div class="panel-heading f-bold">ค้นหา</div>
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>ปี</label>
                                            <asp:DropDownList ID="ddlYearSearch_L" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                            <asp:TextBox ID="txtFilterKeyword_L" runat="server"
                                                CssClass="form-control"
                                                placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                                        </div>
                                    </div>
                                    <div class="col-sm-2" runat="server" visible="false">
                                        <div class="form-group">
                                            <label>สถานะของเอกสาร</label>
                                            <asp:DropDownList ID="ddlStatusapprove_L" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="999" Text="-" Selected="True" />
                                                <asp:ListItem Value="0" Text="ดำเนินการ" />
                                                <asp:ListItem Value="4" Text="อนุมัติ" />
                                                <asp:ListItem Value="5" Text="กลับไปแก้ไข" />
                                                <asp:ListItem Value="6" Text="ไม่อนุมัติ" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>ประเภทหลักสูตร</label>
                                            <asp:DropDownList ID="ddlcourse_plan_status_L" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="" Text="-" Selected="True" />
                                                <asp:ListItem Value="I" Text="In Plan" />
                                                <asp:ListItem Value="O" Text="Out Plan" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>กลุ่มวิชา</label>
                                            <asp:DropDownList ID="ddltrn_group_L" runat="server" CssClass="form-control">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <label>
                                            &nbsp;
                                        </label>
                                        <div class="form-group">
                                            <asp:LinkButton ID="btnFilter" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                                OnCommand="btnCommand" CommandName="btnFilter" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>

                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="row">

                <asp:GridView ID="GvList"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    DataKeyNames="u0_training_course_idx"
                    ShowFooter="false">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# (Container.DataItemIndex + 1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("training_course_no") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <%# Eval("zdate") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ประเภทหลักสูตร" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">

                                    <img src='<%# getImageIO((string)Eval("course_plan_status"),1) %>'
                                        width="50" height="50"
                                        title='<%# getImageIO((string)Eval("course_plan_status"),2) %>'
                                        id="imgicon_inplan" runat="server" visible="true" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียดหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="35%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">
                                    <strong>
                                        <asp:Label ID="Label60" runat="server"> หลักสูตร: </asp:Label></strong>
                                    <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("course_name") %>' />
                                    <br />
                                    <strong>
                                        <asp:Label ID="Label61" runat="server"> กลุ่มวิชา: </asp:Label></strong>
                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("training_group_name") %>' />
                                    <br />
                                    <strong>
                                        <asp:Label ID="Label22" runat="server"> สาขาวิชา: </asp:Label></strong>
                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("training_branch_name") %>' />

                                    <br>
                                    <asp:Panel ID="Panel1" runat="server">
                                        &nbsp;
                                    </asp:Panel>
                                    <div class="form-group">
                                        <table class="table table-striped f-s-12 table-empshift-responsive">
                                            <asp:Repeater ID="rptu8trncoursedate" runat="server">
                                                <HeaderTemplate>
                                                    <tr>
                                                        <th>วันที่อบรม</th>
                                                        <th>เวลาที่เริ่ม-เวลาที่สิ้นสุด</th>
                                                        <th>รวมชม.ที่เรียน</th>
                                                    </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Eval("zdate") %></td>
                                                        <td><%# Eval("ztime_start")+" - "+Eval("ztime_end") %></td>
                                                        <td align="center"><%# string.Format("{0:n2}",Eval("training_course_date_qty")) %></td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </table>
                                        <%# getstar_level((int)Eval("zcount_Lv")) %>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ต้องผ่านการเรียนหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <table class="table table-striped f-s-12 table-empshift-responsive">
                                    <asp:Repeater ID="rptcourse" runat="server">
                                        <HeaderTemplate>
                                            <tr>
                                                <th>หลักสูตร</th>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Eval("zName") %></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size:13px;">

                                    <asp:Label ID="lbu0_training_course_idx" runat="server"
                                        Visible="false" Text='<%# Eval("u0_training_course_idx") %>' />

                                    <asp:TextBox ID="txtGvtraining_course_no" runat="server"
                                        Visible="false" Text='<%# Eval("training_course_no") %>' />

                                    <asp:Label ID="lbu0_course_idx" runat="server"
                                        Visible="false" Text='<%# Eval("u0_course_idx") %>' />

                                    <asp:Label ID="lbzcourse_count" runat="server"
                                        Visible="false" Text='<%# Eval("zcourse_count") %>' />
                                    <asp:Label ID="lbgrade_status" runat="server"
                                        Visible="false" Text='<%# Eval("grade_status") %>' />

                                    
                                    <asp:LinkButton ID="btnqrcode"
                                        CssClass="btn btn-primary btn-sm" runat="server"
                                        data-original-title="Print QR Code" data-toggle="tooltip" Text="Print QR Code"
                                        OnClientClick="target ='_blank';"
                                        OnCommand="btnCommand" CommandName="btnqrcode"
                                        CommandArgument='<%# 
                                        Convert.ToString(Eval("u0_training_course_idx")) + "|" +  
                                        Convert.ToString(Eval("training_course_no"))
                                         %>'
                                        Visible="true">
                                    <i class="fa fa-qrcode"></i></asp:LinkButton>

                                    <asp:LinkButton ID="btnopen_flag"
                                        CssClass='<%# getLabelExamination((int)Eval("open_flag"),1) %>' runat="server"
                                        data-original-title='<%# getLabelExamination((int)Eval("open_flag"),2) %>' 
                                        data-toggle="tooltip" 
                                        OnCommand="btnCommand" CommandName="btnopen_flag"
                                        CommandArgument='<%# 
                                        Convert.ToString(Eval("u0_training_course_idx")) + "|" +  
                                        Convert.ToString(Eval("open_flag"))
                                         %>'
                                        >
                                        <i class='<%# getLabelExamination((int)Eval("open_flag"),3) %>'></i>
                                    </asp:LinkButton>



                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

            </div>

            <br />

        </asp:View>
    </asp:MultiView>



    <script type="text/javascript">
        var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
    </script>

    <script type="text/javascript">
        function fnExcelReport() {
            var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange; var j = 0;
            tab = document.getElementById('Tableprint_sched'); // id of table

            for (j = 0; j < tab.rows.length; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }
    </script>

    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);
            var printWindow = window.open('', '', 'height=800,width=1000');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
                //printWindow.resizeTo(100,100);
                //printWindow.focus();
                //printwindow.window.close();
            }, 500);
            return false;
        }

        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }

        function dtPickerInput(inputClassFrom, inputClassTo) {
            $(inputClassFrom).datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $(inputClassTo).datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }

        function dtPickerBtn(inputClassFrom, inputClassTo, btnClassFrom, btnClassTo) {
            $(btnClassFrom).click(function () {
                $(inputClassFrom).data("DateTimePicker").show();
            });
            $(btnClassTo).click(function () {
                $(inputClassTo).data("DateTimePicker").show();
            });
        }

        $(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
    </script>
    <script type="text/javascript">

        function SetTarget() {

            document.forms[0].target = "_blank";

        }
        function shwwindow(myurl) {
            window.open(myurl, '_blank');
        }

    </script>

    <asp:HiddenField ID="hddf_empty" runat="server" />
</asp:Content>

