﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_MasterCasePOSLV1 : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    private string ODSP_Reletion = "ODSP_Reletion";
    data_employee dtEmployee = new data_employee();

    dataODSP_Relation dtODSP = new dataODSP_Relation();
    DataReservation datareservation = new DataReservation();
    DataSupportIT _dtsupport = new DataSupportIT();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlInsertCaseLV1POS = _serviceUrl + ConfigurationManager.AppSettings["urlInsertCaseLV1POS"];
    static string urlSelectCaseLV1POS = _serviceUrl + ConfigurationManager.AppSettings["urlSelectCaseLV1POS"];
    static string urlUpdaeteCaseLV1POS = _serviceUrl + ConfigurationManager.AppSettings["urlUpdaeteCaseLV1POS"];
    static string urlDeleteCaseLV1POS = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteCaseLV1POS"];



    #endregion

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();

        }

        ViewState["EmpIDX"] = 173; //int.Parse(Session["emp_idx"].ToString());

    }

    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Insert_CaseLV1()
    {
        _dtsupport.BoxPOSList = new POSList[1];
        POSList insert = new POSList();

        insert.POS1_Name = txtlv.Text;
        insert.POS1_Code = txtcode.Text;
        insert.POS1Status = int.Parse(ddStatusadd.SelectedValue);
        insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtsupport.BoxPOSList[0] = insert;

        _dtsupport = callServicePostITRepair(urlInsertCaseLV1POS, _dtsupport);

    }

    protected void SelectMasterList()
    {
        _dtsupport.BoxPOSList = new POSList[1];
        POSList dtsupport = new POSList();

        _dtsupport.BoxPOSList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlSelectCaseLV1POS, _dtsupport);
        setGridData(GvMaster, _dtsupport.BoxPOSList);
    }

    protected void Update_Master_List()
    {
        _dtsupport.BoxPOSList = new POSList[1];
        POSList dtsupport = new POSList();


        dtsupport.POS1IDX = int.Parse(ViewState["POS1IDX"].ToString());
        dtsupport.POS1_Name = ViewState["txtname_edit"].ToString();
        dtsupport.POS1_Code = ViewState["txtcode_edit"].ToString();
        dtsupport.POS1Status = int.Parse(ViewState["StatusUpdate_Update"].ToString());
        dtsupport.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtsupport.BoxPOSList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlUpdaeteCaseLV1POS, _dtsupport);

    }

    protected void Delete_Master_List()
    {
        _dtsupport.BoxPOSList = new POSList[1];
        POSList dtsupport = new POSList();

        dtsupport.POS1IDX = int.Parse(ViewState["POS1IDX"].ToString());
        dtsupport.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dtsupport.BoxPOSList[0] = dtsupport;

        _dtsupport = callServicePostITRepair(urlDeleteCaseLV1POS, _dtsupport);
    }

    protected DataSupportIT callServicePostITRepair(string _cmdUrl, DataSupportIT _dtsupport)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtsupport);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtsupport = (DataSupportIT)_funcTool.convertJsonToObject(typeof(DataSupportIT), _localJson);

        return _dtsupport;
    }


    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }



                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int POS1IDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtname_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtname_edit");
                var txtcode_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtcode_edit");
                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster.EditIndex = -1;

                ViewState["POS1IDX"] = POS1IDX;
                ViewState["txtname_edit"] = txtname_edit.Text;
                ViewState["txtcode_edit"] = txtcode_edit.Text;
                ViewState["StatusUpdate_Update"] = StatusUpdate.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion


    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":

                btnadd.Visible = false;
                Panel_Add.Visible = true;
                break;

            case "btnCancel":
                btnadd.Visible = true;
                Panel_Add.Visible = false;
                txtcode.Text = String.Empty;
                txtlv.Text = String.Empty;

                break;

            case "btnAdd":
                Insert_CaseLV1();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "CmdDel":

                int POS1IDX = int.Parse(cmdArg);
                ViewState["POS1IDX"] = POS1IDX;
                Delete_Master_List();
                SelectMasterList();


                break;
        }



    }
    #endregion
}