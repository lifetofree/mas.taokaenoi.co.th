﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.Net.Sockets;

public partial class websystem_hr_hr_plansale : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    service_execute _serviceExecute = new service_execute();
    data_plansale _dtPlanSale = new data_plansale();
    data_emps _dataEmps = new data_emps();
    data_employee _dtEmployee = new data_employee();

    string _localXml = String.Empty;
    string _localString = String.Empty;
    string _localJson = String.Empty;
    int _actionType = 0;
    int _emp_idx = 0;
    int _default_int = 0;
    int emp_idx = 0;
    decimal tot_actual_all_time = 0;
    decimal tot_actual_all_time_1 = 0;
    decimal tot_actual_all_time_2 = 0;
    decimal tot_actual_all_time_salary = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _url_PlanSale_GetList = _serviceUrl + ConfigurationManager.AppSettings["url_PlanSale_GetList"];
    static string _url_PlanSale_UpdateList = _serviceUrl + ConfigurationManager.AppSettings["url_PlanSale_UpdateList"];
    static string _url_PlanSale_InsertList = _serviceUrl + ConfigurationManager.AppSettings["url_PlanSale_InsertList"];
    static string _url_PlanSale_UpdateDetailList = _serviceUrl + ConfigurationManager.AppSettings["url_PlanSale_UpdateDetailList"];

    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSelectParttime = _serviceUrl + ConfigurationManager.AppSettings["urlGetSelectParttime"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string urlGetEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployee"];

    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            //Menu_Color(1);
            ViewState["EmpIDX"] = Session["emp_idx"].ToString();
            Index_Default();
            lbindex.BackColor = System.Drawing.Color.LightGray;
            lbdetail.BackColor = System.Drawing.Color.Transparent;
            lbsetapprove.BackColor = System.Drawing.Color.Transparent;
            lbreport.BackColor = System.Drawing.Color.Transparent;
            SelectCountGvApproveList();
            
        }

        btnTrigger(lbindex);
        btnTrigger(lbdetail);
        btnTrigger(lbfileupload);
        btnTrigger(lbsetapprove);
        btnTrigger(lbreport);
        btnTrigger(lbguid);
    }
    #endregion

    #region Directories_File URL

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories_1(DirectoryInfo dir, String target, GridView Gv)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt1.Rows.Add(file.Name);
                    dt1.Rows[i][1] = f[0];
                    i++;
                }
            }

            // Panel gvFileLo1 = (Panel)FvDetailUser.FindControl("gvFileLo1");

            if (dt1.Rows.Count > 0)
            {

                ds1.Tables.Add(dt1);
                // gvFileLo1.Visible = true;
                Gv.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
                Gv.DataBind();
                ds1.Dispose();
                //ViewState["EmpCode1"] = dt1.Rows.Count;
            }
            else
            {

                Gv.DataSource = null;
                Gv.DataBind();

            }
        }
        catch
        {
            //ViewState["CheckFile"] = "0";
            //checkfile = "11";
        }
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        DataRow dr = dt.NewRow();
        DataColumn dc = new DataColumn("FileName", typeof(string));
        dc = new DataColumn("Download", typeof(string));
        dt.Columns.Add("FileName");
        dt.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt.Rows.Add(file.Name);
                    dt.Rows[i][1] = f[0];
                    i++;
                }
            }

            ViewState["path"] = "1";
        }
        catch
        {
            ViewState["path"] = "0";
        }
    }

    public static bool UrlExists(string url)
    {
        try
        {
            var request = WebRequest.Create(url) as HttpWebRequest;
            if (request == null) return false;
            request.Method = "HEAD";
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                return response.StatusCode == HttpStatusCode.OK;
            }
        }
        catch (UriFormatException)
        {
            //Invalid Url
            return false;
        }
        catch (WebException)
        {
            //Unable to access url
            return false;
        }
    }

    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Index_Default()
    {
        Select_Employee_ShiftTime();
        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());
        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;

        ViewState["vsTemp_PlansaleList"] = null;

        var dsEquipment = new DataSet();
        dsEquipment.Tables.Add("PlanList");

        dsEquipment.Tables[0].Columns.Add("ps_u0_idx", typeof(int));
        dsEquipment.Tables[0].Columns.Add("m0_typelist_idx", typeof(int));
        dsEquipment.Tables[0].Columns.Add("m0_typelist_name", typeof(String));
        dsEquipment.Tables[0].Columns.Add("emp_idx", typeof(int));
        dsEquipment.Tables[0].Columns.Add("ps_startdate", typeof(String));
        dsEquipment.Tables[0].Columns.Add("ps_enddate", typeof(String));
        dsEquipment.Tables[0].Columns.Add("ps_comment", typeof(String));
        dsEquipment.Tables[0].Columns.Add("ps_work_detail", typeof(String));
        dsEquipment.Tables[0].Columns.Add("u0_unidx", typeof(int));
        dsEquipment.Tables[0].Columns.Add("u0_acidx", typeof(int));
        dsEquipment.Tables[0].Columns.Add("doc_status", typeof(int));
        dsEquipment.Tables[0].Columns.Add("m0_leavetype_idx", typeof(int));
        dsEquipment.Tables[0].Columns.Add("m0_leavetype_name", typeof(String));
        dsEquipment.Tables[0].Columns.Add("shift_midx", typeof(int));
        dsEquipment.Tables[0].Columns.Add("shift_name", typeof(String));

        ViewState["vsTemp_PlansaleList"] = dsEquipment;
        GvPlansaleAdd.DataSource = null;
        GvPlansaleAdd.DataBind();

        GvApproveAll.DataSource = null;
        GvApproveAll.DataBind(); 

        Fv_Search_Shift_Index.ChangeMode(FormViewMode.Insert);
        Fv_Search_Shift_Index.DataBind();

        Fv_Search_approve.ChangeMode(FormViewMode.Insert);
        Fv_Search_approve.DataBind();

        select_parttime(ddl_shift_time);

        if (ViewState["time_idx"].ToString() == "1")
        {
            ddl_shift_time.SelectedValue = ViewState["midx"].ToString();
        }

        select_type_leave(ddlleavetime);
        //setFormData(fvLONDoc, FormViewMode.Insert, null);
        //clearFormView();
    }

    protected void Select_Employee_ShiftTime()
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.emp_idx = int.Parse(ViewState["EmpIDX"].ToString());
        _dataEmployee_.type_select_emp = 17;
        _dataEmployee_.emp_status = 1;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));

        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);
        ViewState["midx"] = _dataEmployee.ShiftTime_details[0].midx;
        ViewState["time_idx"] = _dataEmployee.ShiftTime_details[0].shif_use;
        ViewState["emp_code"] = _dataEmployee.ShiftTime_details[0].employee_code;

    }

    protected void Insert_Status()
    {
        int c = 0;

        var _dataPlanSale_ = new u0_plansale_detail[1];
        _dataPlanSale_[0] = new u0_plansale_detail();

        if (ViewState["vsTemp_PlansaleList"] != null)
        {
            var dsEdu = (DataSet)ViewState["vsTemp_PlansaleList"];
            var AddEdu = new u0_plansale_detail[dsEdu.Tables[0].Rows.Count];
            foreach (DataRow dr in dsEdu.Tables[0].Rows)
            {
                AddEdu[c] = new u0_plansale_detail();
                AddEdu[c].m0_typelist_idx = int.Parse(dr["m0_typelist_idx"].ToString());
                AddEdu[c].emp_idx = int.Parse(dr["emp_idx"].ToString());
                AddEdu[c].ps_startdate = dr["ps_startdate"].ToString();
                AddEdu[c].ps_enddate = dr["ps_enddate"].ToString();
                AddEdu[c].ps_comment = dr["ps_comment"].ToString();
                AddEdu[c].ps_work_detail = dr["ps_work_detail"].ToString();
                AddEdu[c].emp_shift = int.Parse(dr["shift_midx"].ToString());
                AddEdu[c].shift_name = dr["shift_name"].ToString();
                
                //AddEdu[c].m0_node_idx = int.Parse(dr["m0_node_idx"].ToString());
                //AddEdu[c].m0_actor_idx = int.Parse(dr["m0_actor_idx"].ToString());
                //AddEdu[c].ps_status = int.Parse(dr["ps_status"].ToString());
                AddEdu[c].m0_leavetype_idx = int.Parse(dr["m0_leavetype_idx"].ToString());

                c++;
            }

            if (dsEdu.Tables[0].Rows.Count != 0) // Save To Database
            {
                _dtPlanSale.u0_plansale_list = AddEdu;
            }

            //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtPlanSale));
            //fsa.Text = _funcTool.convertObjectToJson(_dtEmployee);
            _dtPlanSale = callServicePlanSale(_url_PlanSale_InsertList, _dtPlanSale);
            //Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
    }

    protected void SelectGvList()
    {
        DropDownList ddlyear_search_detail = (DropDownList)Fv_Search_Shift_Index.FindControl("ddlyear_search_detail");
        DropDownList ddlmonth_search_detail = (DropDownList)Fv_Search_Shift_Index.FindControl("ddlmonth_search_detail");
        
        _dtPlanSale.u0_plansale_list = new u0_plansale_detail[1];
        u0_plansale_detail dtPlanSale = new u0_plansale_detail();

        dtPlanSale.emp_idx = int.Parse(ViewState["EmpIDX"].ToString());
        dtPlanSale.year = int.Parse(ddlyear_search_detail.SelectedValue);
        dtPlanSale.month = int.Parse(ddlmonth_search_detail.SelectedValue);
        dtPlanSale.type_action = 1; //select view index

        _dtPlanSale.u0_plansale_list[0] = dtPlanSale;
        
        _dtPlanSale = callServicePlanSale(_url_PlanSale_GetList, _dtPlanSale);
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtPlanSale));
        //text.Text = _dataEmployee.ToString();
        setGridData(GvIndex, _dtPlanSale.u0_plansale_list);
    }

    protected void SelectGvApproveList()
    {
        TextBox txtempcode_search_approve = (TextBox)Fv_Search_approve.FindControl("txtempcode_search_approve");

        _dtPlanSale.u0_plansale_list = new u0_plansale_detail[1];
        u0_plansale_detail dtPlanSale = new u0_plansale_detail();

        dtPlanSale.emp_idx = int.Parse(ViewState["EmpIDX"].ToString());
        if (txtempcode_search_approve.Text != "")
        {
            dtPlanSale.emp_code = txtempcode_search_approve.Text;
        }
        else
        {
            dtPlanSale.emp_code = "";
        }
        
        dtPlanSale.type_action = 2; //select view index

        _dtPlanSale.u0_plansale_list[0] = dtPlanSale;

        _dtPlanSale = callServicePlanSale(_url_PlanSale_GetList, _dtPlanSale);
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtPlanSale));
        setGridData(GvApproveAll, _dtPlanSale.u0_plansale_list);      
    }

    protected void SelectCountGvApproveList()
    {
        _dtPlanSale.u0_plansale_list = new u0_plansale_detail[1];
        u0_plansale_detail dtPlanSale = new u0_plansale_detail();

        dtPlanSale.emp_idx = int.Parse(ViewState["EmpIDX"].ToString());
        dtPlanSale.type_action = 7;

        _dtPlanSale.u0_plansale_list[0] = dtPlanSale;

        _dtPlanSale = callServicePlanSale(_url_PlanSale_GetList, _dtPlanSale);
        ViewState["vs_CountWaitApprove"] = _dtPlanSale.return_code;
        lbsetapprove.Text = "รายการรออนุมัติ <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
    }

    protected void SelectGvReportList()
    {
        TextBox txtempcode_search = (TextBox)Fv_Search_Emp_Report.FindControl("txtempcode_search");
        DropDownList ddlyear_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlyear_search");
        DropDownList ddlmonth_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlmonth_search");

        _dtPlanSale.u0_plansale_list = new u0_plansale_detail[1];
        u0_plansale_detail dtPlanSale = new u0_plansale_detail();

        //dtPlanSale.emp_idx = int.Parse(ViewState["EmpIDX"].ToString());
        dtPlanSale.emp_code = txtempcode_search.Text; //select view index
        dtPlanSale.year = int.Parse(ddlyear_search.SelectedValue); //select view index
        dtPlanSale.month = int.Parse(ddlmonth_search.SelectedValue); //select view index
        dtPlanSale.type_action = 6; //select view index

        _dtPlanSale.u0_plansale_list[0] = dtPlanSale;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtPlanSale));
        _dtPlanSale = callServicePlanSale(_url_PlanSale_GetList, _dtPlanSale); 
        ViewState["Box_dataEmployee_Report"] = _dtPlanSale.u0_plansale_list;

        if (_dtPlanSale.return_code == 0)
        {
            setGridData(Gv_report, _dtPlanSale.u0_plansale_list);
        }
        else
        {
            setGridData(Gv_report, null);
        }
        
    }

    protected void SelectGetProfile()
    {
        TextBox txtempcode_search = (TextBox)Fv_Search_Emp_Report.FindControl("txtempcode_search");

        _dtPlanSale.u0_plansale_list = new u0_plansale_detail[1];
        u0_plansale_detail dtPlanSale = new u0_plansale_detail();

        dtPlanSale.emp_code = txtempcode_search.Text; //select view index
        dtPlanSale.type_action = 8; //select view index

        _dtPlanSale.u0_plansale_list[0] = dtPlanSale;
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtPlanSale));
        _dtPlanSale = callServicePlanSale(_url_PlanSale_GetList, _dtPlanSale);
        ViewState["emp_code_Report"] = _dtPlanSale.u0_plansale_list[0].emp_code;
        ViewState["emp_name_Report"] = _dtPlanSale.u0_plansale_list[0].emp_name_th;
        ViewState["emp_tel_Report"] = _dtPlanSale.u0_plansale_list[0].tel;
    }

    protected void Update_Master_List()
    {
        _dataEmployee.hospital_list = new Hospital[1];
        Hospital dtemployee = new Hospital();

        dtemployee.HosIDX = int.Parse(ViewState["HosIDX_update"].ToString());
        dtemployee.Name = ViewState["txtname_update"].ToString();
        dtemployee.HStatus = int.Parse(ViewState["ddStatus_update"].ToString());

        _dataEmployee.hospital_list[0] = dtemployee;
        //text.Text = dtemployee.ToString();
        //_dataEmployee = callService(_urlGetUpdateHospital, _dataEmployee);
    }

    protected void Delete_Master_List()
    {
        _dataEmployee.hospital_list = new Hospital[1];
        Hospital dtemployee = new Hospital();

        dtemployee.HosIDX = int.Parse(ViewState["HosIDX_delete"].ToString());

        _dataEmployee.hospital_list[0] = dtemployee;
        //text.Text = dtemployee.ToString();
        //_dataEmployee = callService(_urlGetDeleteHospital, _dataEmployee);
    }

    protected void select_org(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServiceEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกองค์กร....", "0"));
    }

    protected void select_dep(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServiceEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกฝ่าย....", "0"));
    }

    protected void SelectFvList(int ps_u0_idx)
    {
        _dtPlanSale.u0_plansale_list = new u0_plansale_detail[1];
        u0_plansale_detail dtPlanSale = new u0_plansale_detail();

        dtPlanSale.ps_u0_idx = ps_u0_idx;
        dtPlanSale.type_action = 1; //select view index

        _dtPlanSale.u0_plansale_list[0] = dtPlanSale;

        _dtPlanSale = callServicePlanSale(_url_PlanSale_GetList, _dtPlanSale);
        //text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtPlanSale));
        //text.Text = _dataEmployee.ToString();
        setFormData(fvPlanList, _dtPlanSale.u0_plansale_list);
    }

    protected void select_parttime(DropDownList ddlName)
    {
        _dtPlanSale.u0_plansale_list = new u0_plansale_detail[1];
        u0_plansale_detail dtPlanSale = new u0_plansale_detail();

        dtPlanSale.type_action = 3; //select view index

        _dtPlanSale.u0_plansale_list[0] = dtPlanSale;

        _dtPlanSale = callServicePlanSale(_url_PlanSale_GetList, _dtPlanSale);
        setDdlData(ddlName, _dtPlanSale.m0_leave_list, "shift_name", "shift_midx");
        ddlName.Items.Insert(0, new ListItem("เลือกกะการทำงาน....", "0"));
    }

    protected void select_type_leave(DropDownList ddlName)
    {
        _dtPlanSale.u0_plansale_list = new u0_plansale_detail[1];
        u0_plansale_detail dtPlanSale = new u0_plansale_detail();

        dtPlanSale.type_action = 4; //select view index

        _dtPlanSale.u0_plansale_list[0] = dtPlanSale;

        _dtPlanSale = callServicePlanSale(_url_PlanSale_GetList, _dtPlanSale);
        setDdlData(ddlName, _dtPlanSale.m0_leave_list, "type_name", "type_midx");
        ddlName.Items.Insert(0, new ListItem("เลือกประเภทลา....", "0"));
    }

    protected void Select_RptLog_Detail(int ps_u0_idx)
    {
        _dtPlanSale.u0_plansale_list = new u0_plansale_detail[1];
        u0_plansale_detail dtPlanSale = new u0_plansale_detail();

        dtPlanSale.ps_u0_idx = ps_u0_idx;
        dtPlanSale.type_action = 5; //select view index

        _dtPlanSale.u0_plansale_list[0] = dtPlanSale;

        _dtPlanSale = callServicePlanSale(_url_PlanSale_GetList, _dtPlanSale);
        //fsa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        setRptData(rptLog, _dtPlanSale.u1_plansale_list);
    }

    protected string getStatus(int u0_unidx, int u0_acidx, int doc_decision)
    {
        if (u0_unidx == 3 && u0_acidx == 2 && doc_decision == 2) //approve
        {
            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='fa fa-check-circle'></i></span>";
        }
        else if(u0_unidx == 2 && u0_acidx == 2 && doc_decision == 1) //wait
        {
            return "<span class='statusmaster-waiting' data-toggle='tooltip'><i class='fa fa-hourglass-half'></i></span>";
        }
        else if (u0_unidx == 1 && u0_acidx == 1 && doc_decision == 3) //edit
        {
            return "<span class='statusmaster-edit' data-toggle='tooltip'><i class='fa fa-pencil-square-o'></i></span>";
        }
        else //non approve
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip'><i class='fa fa-times-circle'></i></span>";
        }
    }

    protected void btnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger = new PostBackTrigger();
        trigger.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger);
    }


    #endregion

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setRptData(Repeater RpName, Object obj)
    {
        RpName.DataSource = obj;
        RpName.DataBind();
    }
    #endregion

    #region Gridview

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvPlansaleAdd":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvPlansaleAdd.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lbl_m0_typelist_idx = ((Label)e.Row.FindControl("lbl_m0_typelist_idx"));
                        //var lblStatusID = ((Label)e.Row.FindControl("lblStatusID"));

                        var lbl_m0_leavetype = ((Label)e.Row.FindControl("lbl_m0_leavetype"));
                        var lbl_m0_leavetype_1 = ((Label)e.Row.FindControl("lbl_m0_leavetype_1"));
                        var lbl_m0_typelist_1 = ((Label)e.Row.FindControl("lbl_m0_typelist_1"));
                        var img_profile = ((Image)e.Row.FindControl("img_profile"));

                        var lbl_comment = ((Label)e.Row.FindControl("lbl_comment"));
                        var lbl_ps_comment = ((Label)e.Row.FindControl("lbl_ps_comment"));
                        var lbl_work_detail = ((Label)e.Row.FindControl("lbl_work_detail"));
                        var lbl_ps_work_detail = ((Label)e.Row.FindControl("lbl_ps_work_detail"));

                        if (int.Parse(lbl_m0_typelist_idx.Text) == 1) // รอดำเนินการ
                        {
                            lbl_m0_typelist_1.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                            //lblStatus.Style["font-weight"] = "bold";
                            lbl_m0_leavetype.Visible = false;
                            lbl_m0_leavetype_1.Visible = false;
                            lbl_work_detail.Visible = true;
                            lbl_ps_work_detail.Visible = true;
                            lbl_comment.Visible = false;
                            lbl_ps_comment.Visible = false;

                            string getPathLotus = ConfigurationSettings.AppSettings["upload_profile_picture"];
                            string path = "icon_ps_status_work" + "/";
                            string namefile = "icon_ps_status_work" + ".PNG";
                            string pic = getPathLotus + namefile;

                            string filePathLotus = Server.MapPath(getPathLotus);
                            DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
                            SearchDirectories(myDirLotus, "icon_ps_status_work");

                            img_profile.Visible = true;
                            img_profile.ImageUrl = pic;
                            img_profile.Height = 35;
                            img_profile.Width = 35;
                        }
                        else
                        {
                            lbl_m0_typelist_1.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF9900");
                            lbl_m0_leavetype.Visible = true;
                            lbl_m0_leavetype_1.Visible = true;
                            lbl_work_detail.Visible = false;
                            lbl_ps_work_detail.Visible = false;
                            lbl_comment.Visible = true;
                            lbl_ps_comment.Visible = true;

                            string getPathLotus = ConfigurationSettings.AppSettings["upload_profile_picture"];
                            string path = "icon_ps_status_leave" + "/";
                            string namefile = "icon_ps_status_leave" + ".PNG";
                            string pic = getPathLotus + namefile;

                            string filePathLotus = Server.MapPath(getPathLotus);
                            DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
                            SearchDirectories(myDirLotus, "icon_ps_status_leave");

                            img_profile.Visible = true;
                            img_profile.ImageUrl = pic;
                            img_profile.Height = 35;
                            img_profile.Width = 35;
                        }
                      
                    }
                }

                break;
            case "GvIndex":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView GvIndex = (GridView)ViewDetail.FindControl("GvIndex");
                    if (GvIndex.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        /*var lblu0_unidx = ((Label)e.Row.FindControl("lbl_node_u0_unidx_detail"));
                        var lblu0_acidx = ((Label)e.Row.FindControl("lbl_node_u0_acidx_detail"));
                        var lblu0_doc_decision = ((Label)e.Row.FindControl("lbl_node_u0_doc_decision"));
                        var btnedit = ((LinkButton)e.Row.FindControl("btnedit"));

                        if (lblu0_unidx.Text == "2" && lblu0_acidx.Text == "2" && lblu0_doc_decision.Text == "1")
                        {
                            btnedit.Visible = true;
                        }
                        else if (lblu0_unidx.Text == "1" && lblu0_acidx.Text == "1" && lblu0_doc_decision.Text == "3")
                        {
                            btnedit.Visible = true;
                        }
                        else
                        {
                            btnedit.Visible = false;
                        }*/
                    }
                }
                break;
            case "GvApproveAll":
                /*if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }*/
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvApproveAll.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var img_profile1 = ((Image)e.Row.FindControl("img_profile1"));
                        var img_profile2 = ((Image)e.Row.FindControl("img_profile2"));
                        var img_profile3 = ((Image)e.Row.FindControl("img_profile3"));
                        
                        //lbl_m0_typelist_1.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                        //lblStatus.Style["font-weight"] = "bold";

                        string getPathLotus = ConfigurationSettings.AppSettings["upload_profile_picture"];
                        //string path = "icon_ps_status_work" + "/";
                        string icon_approve = "icon_approve" + ".PNG";
                        string icon_nonapprove = "icon_nonapprove" + ".PNG";
                        string icon_edit = "icon_edit" + ".PNG";
                        string pic1 = getPathLotus + icon_approve;
                        string pic2 = getPathLotus + icon_nonapprove;
                        string pic3 = getPathLotus + icon_edit;

                        string filePathLotus = Server.MapPath(getPathLotus);
                        DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
                        SearchDirectories(myDirLotus, "icon_ps_status_work");

                        img_profile1.Visible = true;
                        img_profile1.ImageUrl = pic1;
                        img_profile1.Height = 35;
                        img_profile1.Width = 35;

                        img_profile2.Visible = true;
                        img_profile2.ImageUrl = pic3;
                        img_profile2.Height = 35;
                        img_profile2.Width = 35;

                        img_profile3.Visible = true;
                        img_profile3.ImageUrl = pic2;
                        img_profile3.Height = 35;
                        img_profile3.Width = 35;                   

                    }

                }
                break;
            case "gvFileEmp_edit":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    //ddd4.Text = hidFile.Value;
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    //e.Row.Cells[1].Text = a + MapURL(e.Row.Cells[1].Text);
                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;
            case "Gv_file_report":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL112 = (HyperLink)e.Row.Cells[0].FindControl("btnDL112");
                    HiddenField hidFile112 = (HiddenField)e.Row.Cells[0].FindControl("hidFile112");
                    //ddd4.Text = hidFile.Value;
                    // Display the company name in italics.
                    string LinkHost112 = string.Format("http://{0}", Request.Url.Host);

                    //e.Row.Cells[1].Text = a + MapURL(e.Row.Cells[1].Text);
                    btnDL112.NavigateUrl = LinkHost112 + MapURL(hidFile112.Value);
                }

                break;

            case "Gv_report":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (Gv_report.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var ChkBox_Approve1 = ((CheckBox)e.Row.FindControl("YrChkBox_Approve1"));
                        var lblu0_unidx = ((Label)e.Row.FindControl("lblu0_unidx"));
                        var lblu0_acidx = ((Label)e.Row.FindControl("lblu0_acidx"));
                        var lbldoc_status = ((Label)e.Row.FindControl("lbldoc_status"));
                        var lblleave_type = ((Label)e.Row.FindControl("lblleave_type"));
                        var status_approve = ((Label)e.Row.FindControl("status_approve"));
                        var lblday_off = ((Label)e.Row.FindControl("lblday_off"));
                        var lblday_off_shift = ((Label)e.Row.FindControl("lblday_off_shift"));
                        var lblstatus_holiday = ((Label)e.Row.FindControl("lblstatus_holiday"));
                        
                        if (lblleave_type.Text == "ปฏิบัติการ")
                        {
                            if (lblu0_unidx.Text == "3" && lblu0_acidx.Text == "2" && lbldoc_status.Text == "2")
                            {
                                //ChkBox_Approve1.Checked = true;
                                status_approve.Text = "X";
                            }
                            else
                            {
                                //ChkBox_Approve1.Checked = false;
                                status_approve.Text = "";
                            }
                        }
                        else
                        {
                            if ((lblu0_unidx.Text == "5" && lblu0_acidx.Text == "2" && lbldoc_status.Text == "1") || (lblu0_unidx.Text == "5" && lblu0_acidx.Text == "3" && lbldoc_status.Text == "1"))
                            {
                                //ChkBox_Approve1.Checked = true;
                                status_approve.Text = "X";
                            }
                            else
                            {
                                //ChkBox_Approve1.Checked = false;
                                status_approve.Text = "";
                            }
                        }

                        //day work
                        string day_off_shift = lblday_off_shift.Text;
                        string day_condition_ = lblday_off.Text;
                        string[] set_test_ = day_off_shift.Split(',');

                        if (Array.IndexOf(set_test_, day_condition_.ToString()) > -1)
                        {
                            e.Row.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFFCC");
                            lblstatus_holiday.Text = "X";
                        }
                        else
                        {
                            e.Row.BackColor = System.Drawing.Color.White;
                        }

                    }

                    try
                    {

                        Label lbl_ot_x1_reportmonth = (Label)e.Row.FindControl("lbl_ot_x1_reportmonth");
                        Label lbl_ot_x15_reportmonth = (Label)e.Row.FindControl("lbl_ot_x15_reportmonth");
                        Label lbl_ot_x3_reportmonth = (Label)e.Row.FindControl("lbl_ot_x3_reportmonth");

                        decimal value = Convert.ToDecimal(lbl_ot_x1_reportmonth.Text);
                        decimal value1 = Convert.ToDecimal(lbl_ot_x15_reportmonth.Text);
                        decimal value2 = Convert.ToDecimal(lbl_ot_x3_reportmonth.Text);
                        int n = Convert.ToInt32(value);
                        int n1 = Convert.ToInt32(value1);
                        int n2 = Convert.ToInt32(value2);

                        tot_actual_all_time += Convert.ToDecimal(value);
                        tot_actual_all_time_1 += Convert.ToDecimal(value1);
                        tot_actual_all_time_2 += Convert.ToDecimal(value2);
                    }
                    catch
                    {

                    }
                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    try
                    {
                        Label lit_ot_x1_all_sum = (Label)e.Row.FindControl("lit_ot_x1_all_sum");
                        Label lit_ot_x15_all_sum = (Label)e.Row.FindControl("lit_ot_x15_all_sum");
                        Label lit_ot_x3_all_sum = (Label)e.Row.FindControl("lit_ot_x3_all_sum");

                        lit_ot_x1_all_sum.Text = String.Format("{0:N2}", tot_actual_all_time);
                        lit_ot_x15_all_sum.Text = String.Format("{0:N2}", tot_actual_all_time_1);
                        lit_ot_x3_all_sum.Text = String.Format("{0:N2}", tot_actual_all_time_2);
                    }
                    catch
                    {

                    }

                }
                break;
        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvIndex":
                GvIndex.PageIndex = e.NewPageIndex;
                GvIndex.DataBind();
                SelectGvList();
                break;
        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvIndex":

                //GvMaster.EditIndex = e.NewEditIndex;
                //SelectMasterList();

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvIndex":
                //GvMaster.EditIndex = -1;
                //SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvIndex":

                /*int HosIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtname_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtname_update");
                var ddStatus_update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatus_update");

                GvMaster.EditIndex = -1;

                ViewState["HosIDX_update"] = HosIDX;
                ViewState["txtname_update"] = txtname_update.Text;
                ViewState["ddStatus_update"] = ddStatus_update.SelectedValue;*/

                //Update_Master_List();
                //SelectMasterList();

                break;
        }
    }

    #endregion

    #region GvRowDeleting
    protected void gvRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvPlansaleAdd":
                //var GvEducation = (GridView)FvInsertEmp.FindControl("GvEducation");
                var dsvsBuyequipmentDelete2 = (DataSet)ViewState["vsTemp_PlansaleList"];
                var drDriving2 = dsvsBuyequipmentDelete2.Tables[0].Rows;

                drDriving2.RemoveAt(e.RowIndex);

                ViewState["vsTemp_PlansaleList"] = dsvsBuyequipmentDelete2;
                GvPlansaleAdd.EditIndex = -1;
                GvPlansaleAdd.DataSource = ViewState["vsTemp_PlansaleList"];
                GvPlansaleAdd.DataBind();
                break;
        }

    }
    #endregion

    #endregion

    #region FvDetail_DataBound
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {
            case "fvPlanList":
                Panel boxfv_detail_list = (Panel)ViewDetail.FindControl("boxfv_detail_list");
                FormView fvPlanList_ = (FormView)boxfv_detail_list.FindControl("fvPlanList");
                DropDownList ddlShift_1 = (DropDownList)fvPlanList.FindControl("ddlShiftType_");
                Label lbl_emp_ = (Label)fvPlanList.FindControl("lbl_emp_shift");

                if (fvPlanList_.CurrentMode == FormViewMode.Edit)
                {
                    select_parttime(ddlShift_1);
                    ddlShift_1.SelectedValue = lbl_emp_.Text;
                }
                break;
        }
    }
    #endregion


    #region callService

    protected data_plansale callServicePlanSale(string _cmdUrl, data_plansale _dataPlansale)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataPlansale);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataPlansale = (data_plansale)_funcTool.convertJsonToObject(typeof(data_plansale), _localJson);


        return _dataPlansale;
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    #endregion

    #region Switch-Munu Color
    protected void Menu_Color(int choice)
    {
        switch (choice)
        {
            case 1: //Index
                lbindex.BackColor = System.Drawing.Color.LightGray;
                lbdetail.BackColor = System.Drawing.Color.Transparent;
                lbfileupload.BackColor = System.Drawing.Color.Transparent;
                lbsetapprove.BackColor = System.Drawing.Color.Transparent;
                lbreport.BackColor = System.Drawing.Color.Transparent;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case 2: //detail
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbdetail.BackColor = System.Drawing.Color.LightGray;
                lbfileupload.BackColor = System.Drawing.Color.Transparent;
                lbsetapprove.BackColor = System.Drawing.Color.Transparent;
                lbreport.BackColor = System.Drawing.Color.Transparent;
                
                //DropDownList ddlIndexOrgSearch = (DropDownList)Fv_Search_Shift_Index.FindControl("ddlIndexOrgSearch");             
                //select_org(ddlIndexOrgSearch);
                SelectGvList();
                box_detail_back.Visible = false;
                break;
            case 3: //Approve
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbdetail.BackColor = System.Drawing.Color.Transparent;
                lbfileupload.BackColor = System.Drawing.Color.Transparent;
                lbsetapprove.BackColor = System.Drawing.Color.LightGray;
                lbreport.BackColor = System.Drawing.Color.Transparent;

                SelectGvApproveList();
                SelectCountGvApproveList();
                break;
            case 4: //Report
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbdetail.BackColor = System.Drawing.Color.Transparent;
                lbfileupload.BackColor = System.Drawing.Color.Transparent;
                lbsetapprove.BackColor = System.Drawing.Color.Transparent;
                lbreport.BackColor = System.Drawing.Color.LightGray;
                box_gvfile_report.Visible = false;             
                break;
            case 5: //file upload
                lbindex.BackColor = System.Drawing.Color.Transparent;
                lbdetail.BackColor = System.Drawing.Color.Transparent;
                lbfileupload.BackColor = System.Drawing.Color.LightGray;
                lbsetapprove.BackColor = System.Drawing.Color.Transparent;
                lbreport.BackColor = System.Drawing.Color.Transparent;

                try
                {
                    DateTime myDateTime = DateTime.Now;
                    string year = myDateTime.Year.ToString();
                    string getPathLotus1 = ConfigurationSettings.AppSettings["path_file_hr_plansale"];
                    string filePathLotus1 = Server.MapPath(getPathLotus1 + ViewState["EmpCode"].ToString());
                    DirectoryInfo myDirLotus1 = new DirectoryInfo(filePathLotus1);
                    SearchDirectories_1(myDirLotus1, year, gvFileEmp_edit);
                }
                catch
                {

                }
                break;
        }
    }

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;
        //DropDownList ddltypelist = (DropDownList)FvSearch_Report.FindControl("ddltypelist");
        //DropDownList ddl_dep_report = (DropDownList)FvSearch_Report.FindControl("ddl_dep_report");

        switch (ddName.ID)
        {
            case "ddltypelist":
                if (ddltypelist.SelectedValue == "1")
                {
                    box_leavetime.Visible = false;
                    box_comment.Visible = false;
                    box_workdetail.Visible = true;
                }
                else if (ddltypelist.SelectedValue == "2")
                {
                    box_leavetime.Visible = true;
                    box_comment.Visible = true;
                    box_workdetail.Visible = false;
                }
                break;
            case "ddlIndexOrgSearch":
                //DropDownList ddlIndexOrgSearch = (DropDownList)Fv_Search_Shift_Index.FindControl("ddlIndexOrgSearch");
                //DropDownList ddlIndexDeptSearch = (DropDownList)Fv_Search_Shift_Index.FindControl("ddlIndexDeptSearch");

                //select_dep(ddlIndexDeptSearch, int.Parse(ddlIndexOrgSearch.SelectedValue));
                break;
        }
    }

    #endregion

    #region chkSelectedIndexChanged

    protected void chkSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (CheckBox)sender;


        switch (ddName.ID)
        {

            /*case "chkAll":

                bool b = (sender as CheckBox).Checked;

                for (int i = 0; i < YrChkBox.Items.Count; i++)
                {
                    YrChkBox.Items[i].Selected = b;
                }

                break;

            case "chkallcolumns":

                bool c = (sender as CheckBox).Checked;

                for (int i = 0; i < YrChkBoxColumns.Items.Count; i++)
                {
                    YrChkBoxColumns.Items[i].Selected = c;
                }

                break;

            case "chkallType":

                bool d = (sender as CheckBox).Checked;

                for (int i1 = 0; i1 < YrChkBoxColumnsType.Items.Count; i1++)
                {
                    YrChkBoxColumnsType.Items[i1].Selected = d;
                }

                break;*/

            case "chkApproveAll":

                CheckBox chkAll = (CheckBox)GvApproveAll.HeaderRow.FindControl("chkApproveAll");

                if (chkAll.Checked == true)
                {
                    foreach (GridViewRow gvRow in GvApproveAll.Rows)
                    {
                        CheckBox chkSel = (CheckBox)gvRow.FindControl("YrChkBox_Approve");
                        chkSel.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow gvRow in GvApproveAll.Rows)
                    {
                        CheckBox chkSel = (CheckBox)gvRow.FindControl("YrChkBox_Approve");
                        chkSel.Checked = false;
                    }
                }

                break;

            case "chkApproveAll_no":

                CheckBox chkAll_no = (CheckBox)GvApproveAll.HeaderRow.FindControl("chkApproveAll_no");

                if (chkAll_no.Checked == true)
                {
                    foreach (GridViewRow gvRow in GvApproveAll.Rows)
                    {
                        CheckBox chkSel = (CheckBox)gvRow.FindControl("YrChkBox_no");
                        chkSel.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow gvRow in GvApproveAll.Rows)
                    {
                        CheckBox chkSel = (CheckBox)gvRow.FindControl("YrChkBox_no");
                        chkSel.Checked = false;
                    }
                }

                break;

            case "chkApproveAll_edit":

                CheckBox chkAll_edit = (CheckBox)GvApproveAll.HeaderRow.FindControl("chkApproveAll_edit");

                if (chkAll_edit.Checked == true)
                {
                    foreach (GridViewRow gvRow in GvApproveAll.Rows)
                    {
                        CheckBox chkSel = (CheckBox)gvRow.FindControl("YrChkBox_edit");
                        chkSel.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow gvRow in GvApproveAll.Rows)
                    {
                        CheckBox chkSel = (CheckBox)gvRow.FindControl("YrChkBox_edit");
                        chkSel.Checked = false;
                    }
                }

                break;

        }
    }

    #endregion

    #region custom data format and display
    protected string formatDate(string dateIN)
    {
        return DateTime.Parse(dateIN).ToString("DD/MM/YYYY HH:mm");
    }

    protected string formatDateTime(string dateIN)
    {
        return DateTime.Parse(dateIN).ToString("dd/MM/yyyy HH:mm");
    }

    protected string formatDateTimeCheck(string dateIN)
    {
        return DateTime.ParseExact(dateIN, "dd/MM/yyyy HH:mm", null).ToString();
    }

    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "btnIndex":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewIndex);
                break;

            case "btndetail":
                Menu_Color(int.Parse(cmdArg));
                //Fv_Search_Shift_Index.ChangeMode(FormViewMode.Insert);
                MvMaster.SetActiveView(ViewDetail);
                break;

            case "btnfile_uplad":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewFileUpload);
                break;

            case "btnsetapprove":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewApprove);
                break;

            case "btnreport":
                Menu_Color(int.Parse(cmdArg));

                Fv_Search_Emp_Report.ChangeMode(FormViewMode.Insert);
                MvMaster.SetActiveView(ViewReport);
                break;

            case "btnSave":
                Insert_Status();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "AddDetail":
                if (Convert.ToDateTime(formatDateTimeCheck(txtDate_Start.Text)) < Convert.ToDateTime(formatDateTimeCheck(txtDate_End.Text))) //compare datetime
                {
                    //true
                    var dsEdu = (DataSet)ViewState["vsTemp_PlansaleList"];
                    var drEdu = dsEdu.Tables[0].NewRow();
                    drEdu["ps_u0_idx"] = 0;
                    drEdu["m0_typelist_idx"] = int.Parse(ddltypelist.SelectedValue);
                    drEdu["m0_typelist_name"] = ddltypelist.SelectedItem.Text;
                    drEdu["emp_idx"] = int.Parse(ViewState["EmpIDX"].ToString());
                    drEdu["ps_startdate"] = txtDate_Start.Text;
                    drEdu["ps_enddate"] = txtDate_End.Text;
                    drEdu["ps_comment"] = txt_comment.Text;
                    drEdu["ps_work_detail"] = txt_work_detail.Text;
                    drEdu["u0_unidx"] = 0;
                    drEdu["u0_acidx"] = 0;

                    if (int.Parse(ddlleavetime.SelectedValue) == 0)
                    {
                        drEdu["m0_leavetype_idx"] = "0";
                        drEdu["m0_leavetype_name"] = "";
                    }
                    else
                    {
                        drEdu["m0_leavetype_idx"] = int.Parse(ddlleavetime.SelectedValue);
                        drEdu["m0_leavetype_name"] = ddlleavetime.SelectedItem.Text;
                    }

                    if (int.Parse(ddl_shift_time.SelectedValue) == 0)
                    {
                        drEdu["shift_midx"] = "0";
                        drEdu["shift_name"] = "";
                    }
                    else
                    {
                        drEdu["shift_midx"] = int.Parse(ddl_shift_time.SelectedValue); ;
                        drEdu["shift_name"] = ddl_shift_time.SelectedItem.Text;
                    }

                    dsEdu.Tables[0].Rows.Add(drEdu);
                    ViewState["vsTemp_PlansaleList"] = dsEdu;

                    GvPlansaleAdd.DataSource = dsEdu.Tables[0];
                    GvPlansaleAdd.DataBind();

                    //ddltypelist.SelectedValue = "0";
                    txtDate_Start.Text = null;
                    txtDate_End.Text = null;
                    txt_comment.Text = null;
                    txt_work_detail.Text = null;
                    btnSave.Visible = true;

                }
                else
                {
                    _funcTool.showAlert(this, "ไม่สามารถบันทึกข้อมูลได้ กรุณาเช็ควันที่");
                    break;
                }            
                break;
            case "btnedit_detail":
                string[] arg_sw = new string[4];
                arg_sw = e.CommandArgument.ToString().Split(';');
                int ps_u0_idx = int.Parse(arg_sw[0]);
                int u0_unidx = int.Parse(arg_sw[1]);
                int u0_acidx = int.Parse(arg_sw[2]);
                int doc_decision = int.Parse(arg_sw[3]);

                boxfv_detail_list.Visible = true;
                boxgv_index_list.Visible = false;
                box_detail_back.Visible = true;
                fvPlanList.ChangeMode(FormViewMode.Edit);
            
                SelectFvList(ps_u0_idx);
                Select_RptLog_Detail(ps_u0_idx);

                TextBox tbDocStartE = (TextBox)fvPlanList.FindControl("tbDocStartE");
                TextBox tbDocEndE = (TextBox)fvPlanList.FindControl("tbDocEndE");
                TextBox tbDocRemarkE = (TextBox)fvPlanList.FindControl("tbDocRemarkE");
                DropDownList ddlShiftType = (DropDownList)fvPlanList.FindControl("ddlShiftType_");
                LinkButton lbUpdate = (LinkButton)fvPlanList.FindControl("lbUpdate");
                LinkButton btncancel_user_update = (LinkButton)fvPlanList.FindControl("btncancel_user_update");

                if (u0_unidx == 2 && u0_acidx == 2 && doc_decision == 1)
                {
                    tbDocStartE.Enabled = false;
                    tbDocEndE.Enabled = false;
                    tbDocRemarkE.Enabled = false;
                    ddlShiftType.Enabled = false;
                    lbUpdate.Visible = false;
                    btncancel_user_update.Visible = true;
                }
                else if (u0_unidx == 1 && u0_acidx == 1 && doc_decision == 3)
                {
                    tbDocStartE.Enabled = true;
                    tbDocEndE.Enabled = true;
                    tbDocRemarkE.Enabled = true;
                    ddlShiftType.Enabled = true;
                    lbUpdate.Visible = true;
                    btncancel_user_update.Visible = true;
                }
                else
                {
                    tbDocStartE.Enabled = false;
                    tbDocEndE.Enabled = false;
                    tbDocRemarkE.Enabled = false;
                    ddlShiftType.Enabled = false;
                    lbUpdate.Visible = false;
                    btncancel_user_update.Visible = false;
                }
                break;
            case "cmdCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnCancel_edit":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "cmdCancel_approveall":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btn_search_index":
                SelectGvList();
                break;
            case "Cmdsearch_clear":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btn_search_approve":
                SelectGvApproveList();
                break;

            case "cmdApprove_All":

                #region ViewAddEquipment
                ViewState["vsBuyequipment_1"] = null;

                var dsEquipment_1 = new DataSet();
                dsEquipment_1.Tables.Add("Equipment_1");

                dsEquipment_1.Tables[0].Columns.Add("ps_u0_idx", typeof(int));
                dsEquipment_1.Tables[0].Columns.Add("approve", typeof(int));
                dsEquipment_1.Tables[0].Columns.Add("approve_no", typeof(int));
                dsEquipment_1.Tables[0].Columns.Add("approve_edit", typeof(int));
                dsEquipment_1.Tables[0].Columns.Add("empidx", typeof(int));
                dsEquipment_1.Tables[0].Columns.Add("_EmpIDX_user", typeof(int));
                dsEquipment_1.Tables[0].Columns.Add("_Empcode_user", typeof(int));
                dsEquipment_1.Tables[0].Columns.Add("status_approve_name", typeof(string));
                dsEquipment_1.Tables[0].Columns.Add("commentapprove", typeof(string));
                dsEquipment_1.Tables[0].Columns.Add("u0_unidx", typeof(int));
                dsEquipment_1.Tables[0].Columns.Add("u0_acidx", typeof(int));

                ViewState["vsBuyequipment_1"] = dsEquipment_1;

                #endregion

                int chk_row = 0;
                string empi = "";

                try
                {
                    CheckBox chkApproveAll = (CheckBox)GvApproveAll.HeaderRow.FindControl("chkApproveAll");
                    CheckBox chkApproveAll_no = (CheckBox)GvApproveAll.HeaderRow.FindControl("chkApproveAll_no");
                    CheckBox chkApproveAll_edit = (CheckBox)GvApproveAll.HeaderRow.FindControl("chkApproveAll_edit");

                    foreach (GridViewRow row in GvApproveAll.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            CheckBox YrChkBox_Approve = (CheckBox)row.Cells[3].FindControl("YrChkBox_Approve");
                            CheckBox YrChkBox_edit = (CheckBox)row.Cells[4].FindControl("YrChkBox_edit");
                            CheckBox YrChkBox_no = (CheckBox)row.Cells[5].FindControl("YrChkBox_no");                     
                            Label approve_empcode = (Label)row.Cells[0].FindControl("approve_empcode");
                            Label approve_empidx = (Label)row.Cells[0].FindControl("approve_empidx");
                            Label lbl_node_u0_unidx = (Label)row.Cells[0].FindControl("lbl_node_u0_unidx");
                            Label lbl_node_u0_acidx = (Label)row.Cells[0].FindControl("lbl_node_u0_acidx");
                            TextBox approve_comment = (TextBox)row.Cells[6].FindControl("txtcommentapproveall");

                            if ((YrChkBox_Approve.Checked == true && YrChkBox_no.Checked == false && YrChkBox_edit.Checked == false) ||
                                (YrChkBox_Approve.Checked == false && YrChkBox_no.Checked == true && YrChkBox_edit.Checked == false) ||
                                (YrChkBox_Approve.Checked == false && YrChkBox_no.Checked == false && YrChkBox_edit.Checked == true))
                            {

                                if (YrChkBox_Approve.Checked == true)
                                {
                                    empi = (row.Cells[0].FindControl("approve_uidx") as Label).Text;

                                    var dsEquiment_1 = (DataSet)ViewState["vsBuyequipment_1"];
                                    var drEquiment_1 = dsEquiment_1.Tables[0].NewRow();

                                    drEquiment_1["ps_u0_idx"] = int.Parse(empi);
                                    drEquiment_1["approve"] = 1;
                                    drEquiment_1["approve_no"] = 0;
                                    drEquiment_1["approve_edit"] = 0;
                                    drEquiment_1["empidx"] = int.Parse(ViewState["EmpIDX"].ToString()); //คน login
                                    drEquiment_1["_EmpIDX_user"] = approve_empidx.Text;
                                    drEquiment_1["_Empcode_user"] = approve_empcode.Text;
                                    drEquiment_1["status_approve_name"] = "อนุมัติ";
                                    drEquiment_1["commentapprove"] = approve_comment.Text;
                                    drEquiment_1["u0_unidx"] = lbl_node_u0_unidx.Text;
                                    drEquiment_1["u0_acidx"] = lbl_node_u0_acidx.Text;

                                    dsEquiment_1.Tables[0].Rows.Add(drEquiment_1);
                                    ViewState["vsBuyequipment_1"] = dsEquiment_1;
                                }
                                else if (YrChkBox_edit.Checked == true)
                                {
                                    empi = (row.Cells[0].FindControl("approve_uidx") as Label).Text;

                                    var dsEquiment_1 = (DataSet)ViewState["vsBuyequipment_1"];
                                    var drEquiment_1 = dsEquiment_1.Tables[0].NewRow();

                                    drEquiment_1["ps_u0_idx"] = int.Parse(empi);
                                    drEquiment_1["approve"] = 0;
                                    drEquiment_1["approve_edit"] = 1;
                                    drEquiment_1["approve_no"] = 0;                                 
                                    drEquiment_1["empidx"] = int.Parse(ViewState["EmpIDX"].ToString());
                                    drEquiment_1["_EmpIDX_user"] = approve_empidx.Text;
                                    drEquiment_1["_Empcode_user"] = approve_empcode.Text;
                                    drEquiment_1["status_approve_name"] = "ไม่อนุมัติ - แก้ไข";
                                    drEquiment_1["commentapprove"] = approve_comment.Text;
                                    drEquiment_1["u0_unidx"] = lbl_node_u0_unidx.Text;
                                    drEquiment_1["u0_acidx"] = lbl_node_u0_acidx.Text;

                                    dsEquiment_1.Tables[0].Rows.Add(drEquiment_1);
                                    ViewState["vsBuyequipment_1"] = dsEquiment_1;
                                }
                                else if (YrChkBox_no.Checked == true)
                                {
                                    empi = (row.Cells[0].FindControl("approve_uidx") as Label).Text;

                                    var dsEquiment_1 = (DataSet)ViewState["vsBuyequipment_1"];
                                    var drEquiment_1 = dsEquiment_1.Tables[0].NewRow();

                                    drEquiment_1["ps_u0_idx"] = int.Parse(empi);
                                    drEquiment_1["approve"] = 0;
                                    drEquiment_1["approve_edit"] = 0;
                                    drEquiment_1["approve_no"] = 1;
                                    drEquiment_1["empidx"] = int.Parse(ViewState["EmpIDX"].ToString());
                                    drEquiment_1["_EmpIDX_user"] = approve_empidx.Text;
                                    drEquiment_1["_Empcode_user"] = approve_empcode.Text;
                                    drEquiment_1["status_approve_name"] = "ไม่อนุมัติ - จบการดำเนินการ";
                                    drEquiment_1["commentapprove"] = approve_comment.Text;
                                    drEquiment_1["u0_unidx"] = lbl_node_u0_unidx.Text;
                                    drEquiment_1["u0_acidx"] = lbl_node_u0_acidx.Text;

                                    dsEquiment_1.Tables[0].Rows.Add(drEquiment_1);
                                    ViewState["vsBuyequipment_1"] = dsEquiment_1;
                                }
                                else
                                {
                                    empi = (row.Cells[0].FindControl("approve_uidx") as Label).Text;

                                    var dsEquiment_1 = (DataSet)ViewState["vsBuyequipment_1"];
                                    var drEquiment_1 = dsEquiment_1.Tables[0].NewRow();

                                    drEquiment_1["ps_u0_idx"] = int.Parse(empi);
                                    drEquiment_1["approve"] = 0;
                                    drEquiment_1["approve_no"] = 0;
                                    drEquiment_1["approve_edit"] = 0;
                                    drEquiment_1["empidx"] = int.Parse(ViewState["EmpIDX"].ToString());
                                    drEquiment_1["_EmpIDX_user"] = approve_empidx.Text;
                                    drEquiment_1["_Empcode_user"] = approve_empcode.Text;
                                    drEquiment_1["commentapprove"] = approve_comment.Text;
                                    drEquiment_1["u0_unidx"] = lbl_node_u0_unidx.Text;
                                    drEquiment_1["u0_acidx"] = lbl_node_u0_acidx.Text;

                                    dsEquiment_1.Tables[0].Rows.Add(drEquiment_1);
                                    ViewState["vsBuyequipment_1"] = dsEquiment_1;
                                }
                            }
                            else if (YrChkBox_Approve.Checked == false && YrChkBox_no.Checked == false && YrChkBox_edit.Checked == false)
                            {

                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือก Checkbox เพียงรายการละ 1 ช่องเท่านั้น');", true);
                                chk_row = 1;
                            }
                        }

                    }
                    if (chk_row == 0) //กรอกข้อมูลสำเร็จ
                    {
                        var dsBuyequipment_2 = (DataSet)ViewState["vsBuyequipment_1"];

                        var AddPur2 = new u1_plansale_detail[dsBuyequipment_2.Tables[0].Rows.Count];
                        int i11 = 0;

                        foreach (DataRow dr in dsBuyequipment_2.Tables[0].Rows)
                        {
                            AddPur2[i11] = new u1_plansale_detail();
                            AddPur2[i11].ps_u0_idx = int.Parse(dr["ps_u0_idx"].ToString());
                            AddPur2[i11]._approve = int.Parse(dr["approve"].ToString());
                            AddPur2[i11]._approve_no = int.Parse(dr["approve_no"].ToString());
                            AddPur2[i11]._approve_edit = int.Parse(dr["approve_edit"].ToString());
                            AddPur2[i11]._EmpIDX = int.Parse(dr["empidx"].ToString());
                            AddPur2[i11]._EmpIDX_user = int.Parse(dr["_EmpIDX_user"].ToString());
                            AddPur2[i11]._Empcode_user = int.Parse(dr["_Empcode_user"].ToString());
                            AddPur2[i11].comment_approve = dr["commentapprove"].ToString();
                            AddPur2[i11].u0_unidx = int.Parse(dr["u0_unidx"].ToString());
                            AddPur2[i11].u0_acidx = int.Parse(dr["u0_acidx"].ToString());

                            i11++;
                        }

                        if (dsBuyequipment_2.Tables[0].Rows.Count == 0)
                        {
                            //ffsa.Text = "1111";
                        }
                        else
                        {
                            data_plansale dtPlanSale = new data_plansale();

                            dtPlanSale.u1_plansale_list = AddPur2;
                            //ffsa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dtPlanSale));
                            dtPlanSale = callServicePlanSale(_url_PlanSale_UpdateDetailList, dtPlanSale);
                            Page.Response.Redirect(Page.Request.Url.ToString(), true);
                        }
                    }
                    else //กรอกข้อมูลผิดพลาด
                    {
                        //ffsa.Text = "3333";
                    }
                }
                catch { }
                break;

            case "cmdUpdate":

                TextBox tbDocStartE_ = (TextBox)fvPlanList.FindControl("tbDocStartE");
                TextBox tbDocEndE_ = (TextBox)fvPlanList.FindControl("tbDocEndE");
                TextBox tbDocRemarkE_ = (TextBox)fvPlanList.FindControl("tbDocRemarkE");
                DropDownList ddlShiftType_ = (DropDownList)fvPlanList.FindControl("ddlShiftType_");
                Label lbl_ps_u0_idx = (Label)fvPlanList.FindControl("lbl_ps_u0_idx");

                if (Convert.ToDateTime(formatDateTimeCheck(tbDocStartE_.Text)) < Convert.ToDateTime(formatDateTimeCheck(tbDocEndE_.Text))) //compare datetime
                {
                    _dtPlanSale.u0_plansale_list = new u0_plansale_detail[1];
                    u0_plansale_detail dtPlanSale_ = new u0_plansale_detail();

                    dtPlanSale_.ps_u0_idx = int.Parse(lbl_ps_u0_idx.Text);
                    dtPlanSale_.emp_idx = int.Parse(ViewState["EmpIDX"].ToString());
                    dtPlanSale_.ps_startdate = tbDocStartE_.Text;
                    dtPlanSale_.ps_enddate = tbDocEndE_.Text;
                    dtPlanSale_.emp_shift = int.Parse(ddlShiftType_.SelectedValue);
                    dtPlanSale_.ps_comment = tbDocRemarkE_.Text;
                    dtPlanSale_.type_action = 1; //update edit

                    _dtPlanSale.u0_plansale_list[0] = dtPlanSale_;
                    //sdfa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtPlanSale));
                    _dtPlanSale = callServicePlanSale(_url_PlanSale_UpdateList, _dtPlanSale);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    _funcTool.showAlert(this, "ไม่สามารถบันทึกข้อมูลได้ กรุณาเช็ควันที่");
                    break;
                }

                break;
            case "cmdcancel_user":
                Label lbl_ps_u0_idx_ = (Label)fvPlanList.FindControl("lbl_ps_u0_idx");

                _dtPlanSale.u0_plansale_list = new u0_plansale_detail[1];
                u0_plansale_detail dtPlanSale_1 = new u0_plansale_detail();

                dtPlanSale_1.ps_u0_idx = int.Parse(lbl_ps_u0_idx_.Text);
                dtPlanSale_1.emp_idx = int.Parse(ViewState["EmpIDX"].ToString());
                //dtPlanSale_.ps_startdate = tbDocStartE_.Text;
                //dtPlanSale_.ps_enddate = tbDocEndE_.Text;
                //dtPlanSale_.emp_shift = int.Parse(ddlShiftType_.SelectedValue);
                //dtPlanSale_.ps_comment = tbDocRemarkE_.Text;
                dtPlanSale_1.type_action = 2; //update edit

                _dtPlanSale.u0_plansale_list[0] = dtPlanSale_1;
                //sdfa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtPlanSale));
                _dtPlanSale = callServicePlanSale(_url_PlanSale_UpdateList, _dtPlanSale);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;
            case "btnedit_back":
                Menu_Color(int.Parse(cmdArg));
                box_detail_back.Visible = false;
                boxgv_index_list.Visible = true;
                boxfv_detail_list.Visible = false;
                break;

            case "savefile_picture":

                DateTime myDateTime = DateTime.Now;
                string day = myDateTime.Day.ToString();
                HttpFileCollection UploadImages1 = Request.Files;
                if (UploadImages1.Count > 0)
                {
                    for (int j = 0; j < UploadImages1.Count; j++)
                    {
                        HttpPostedFile hpfLoadfile = UploadImages1[j];
                        if (hpfLoadfile.ContentLength > 1)
                        {
                            string getPath_upload = ConfigurationSettings.AppSettings["path_file_hr_plansale"];
                            string RECode_uploadfile = ViewState["EmpCode"].ToString();
                            //string fileName_upload = ddlmonth.SelectedValue + ddlyear.SelectedItem.Text + "_" + RECode_uploadfile;
                            string fileName_upload = ddlmonth.SelectedValue + "_" + ddlyear.SelectedItem.Text + "_" + day + "_" + j;
                            string filePath_upload = Server.MapPath(getPath_upload + RECode_uploadfile);

                            if (!Directory.Exists(filePath_upload))
                            {
                                Directory.CreateDirectory(filePath_upload);
                            }
                            string extension = Path.GetExtension(hpfLoadfile.FileName);
                            //sdfa.Text = filePath_upload;
                            hpfLoadfile.SaveAs(Server.MapPath(getPath_upload + RECode_uploadfile) + "\\" + fileName_upload + extension);
                            //showimage_upload(txtidcard_upload.Text, img_profile);
                        }
                    }
                }

                string year = myDateTime.Year.ToString();
                string getPathLotus = ConfigurationSettings.AppSettings["path_file_hr_plansale"];
                string filePathLotus = Server.MapPath(getPathLotus + ViewState["EmpCode"].ToString());
                DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
                SearchDirectories_1(myDirLotus, year, gvFileEmp_edit);

                ddlmonth.SelectedValue = "0";
                ddlyear.SelectedValue = "0";

                break;

            case "btn_search_report":
                SelectGvReportList();
                //Fv_Search_Emp_Report.ChangeMode(FormViewMode.Insert);
                DropDownList ddlyear_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlyear_search");
                DropDownList ddlmonth_search = (DropDownList)Fv_Search_Emp_Report.FindControl("ddlmonth_search");
                TextBox txtempcode_search = (TextBox)Fv_Search_Emp_Report.FindControl("txtempcode_search");

                try
                {
                    box_gvfile_report.Visible = true;
                    string getPathLotus1 = ConfigurationSettings.AppSettings["path_file_hr_plansale"];
                    string filePathLotus1 = Server.MapPath(getPathLotus1 + txtempcode_search.Text);
                    //ffd.Text = filePathLotus1.ToString();
                    DirectoryInfo myDirLotus1 = new DirectoryInfo(filePathLotus1);
                    SearchDirectories_1(myDirLotus1, ddlmonth_search.SelectedValue + "_" + ddlyear_search.SelectedValue  , Gv_file_report);
                }
                catch
                {

                }
                break;

            case "btnexport_general":
                SelectGetProfile();
                if (ViewState["Box_dataEmployee_Report"] != null)
                {
                    Gv_report.AllowPaging = false;
                    Gv_report.DataSource = ViewState["Box_dataEmployee_Report"];
                    Gv_report.DataBind();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport1.xls");
                    // Response.Charset = ""; set character 
                    Response.Charset = "utf-8";
                    Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                    Response.ContentType = "application/vnd.ms-excel";

                    Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                    Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                    //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                    StringWriter sw = new StringWriter();
                    HtmlTextWriter hw = new HtmlTextWriter(sw);

                    for (int i = 0; i < Gv_report.Rows.Count; i++)
                    {
                        //Apply text style to each Row
                        Gv_report.Rows[i].Attributes.Add("class", "number2");
                    }

                    //Response.Write(ViewState["ddlOrg_Search_head_Name"].ToString());
                    //Response.Write("<br/>");
                    Response.Write("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;รายงานการทำงาน สำหรับพนักงาน PC");
                    Response.Write("<br/>");
                    Response.Write("รหัสพนักงาน : " + ViewState["emp_code_Report"].ToString() + " ชื่อ-นามสกุล " + ViewState["emp_name_Report"].ToString());
                    Response.Write("<br/>");
                    Response.Write("เบอร์โทรศัพท์ : " + ViewState["emp_tel_Report"].ToString() + " ห้าง " + "..........." + " เขตการขาย ภาค " + "..........." + " วันหยุดประจำสัปดาห์ " + "...........");

                    Gv_report.RenderControl(hw);

                    //Amount is displayed in number format with 2 decimals
                    Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                    Response.Output.Write(sw.ToString());
                    Response.Flush();
                    Response.End();
                }

                break;
            case "btnguid":
                Response.Write("<script>window.open('https://docs.google.com/document/d/19v0nIx6bbRDMSyz-PReZlemRhOT5cw2z0-Elj1xAQrU/edit?usp=sharing','_blank');</script>");
                break;
        }



    }
    #endregion
}