﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="Price-Reference.aspx.cs" Inherits="websystem_ITServices_Price_Reference" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <script type="text/javascript">
        function openModal() {
            $('#editit').modal('show');

        }
    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadFile").MultiFile();
            }
        })
    </script>

    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);

            var printWindow = window.open('', '', 'height=1000,width=800');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
        }
    </script>
    <div class="form-group">
        <div class="col-sm-1 col-sm-offset-10">
        <asp:HyperLink ID="HyperLink1" runat="server" CssClass="btn btn-sm btn-primary"
            NavigateUrl="https://docs.google.com/document/d/1D-czxYhJzsz3ZH4NiXEch_KKIDumYEXw1iPrBEQcIn8/edit?usp=sharing" Target="_blank"
            CommandName="_divMenuBtnToDivManual" OnCommand="btnCommand"><i class="fa fa-book"></i> คู่มือการใช้งาน</asp:HyperLink>
            </div>
    </div>

    <!--*** START Menu ***-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <%--   <a class="navbar-brand">Menu</a>--%>
            </div>
            <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left" runat="server">
                    <li id="_divMenuLiToViewIT" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivIT" runat="server"
                            CommandName="_divMenuBtnToDivIT"
                            CommandArgument="3"
                            OnCommand="btnCommand" Text="Computer & Software" />
                    </li>

                    <li id="_divMenuLiToViewHR" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivHR" runat="server"
                            CommandName="_divMenuBtnToDivHR"
                            CommandArgument="9"
                            OnCommand="btnCommand" Text="HR" />
                    </li>
                    <li id="_divMenuLiToViewEN" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivEN" runat="server"
                            CommandName="_divMenuBtnToDivEN"
                            CommandArgument="11"
                            OnCommand="btnCommand" Text="Engineering" />
                    </li>


                    <li id="_divMenuLiToViewSAP" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivSAP" runat="server"
                            CommandName="_divMenuBtnToDivSAP"
                            CommandArgument="2"
                            OnCommand="btnCommand" Text="SAP,BI,TM1" />
                    </li>

                    <li id="_divMenuLiToViewWeb" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivWeb" runat="server"
                            CommandName="_divMenuBtnToDivWeb"
                            CommandArgument="26"
                            OnCommand="btnCommand" Text="Domain & Hosting" />
                    </li>
                    <li id="_divMenuLiToViewQA" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivQA" runat="server"
                            CommandName="_divMenuBtnToDivQA"
                            CommandArgument="36"
                            OnCommand="btnCommand" Text="QA" />
                    </li>
                    <li id="_divMenuLiToViewPurchase" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivPurchase" runat="server"
                            CommandName="_divMenuBtnToDivPurchase"
                            CommandArgument="41"
                            OnCommand="btnCommand" Text="Purchase" />
                    </li>
                    <li id="_divMenuLiToViewAdd" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivAdd" runat="server"
                            CommandName="_divMenuBtnToDivAdd"
                            OnCommand="btnCommand" Text="Add Reference" />
                    </li>


                    <li id="_divMenuLiToViewWaiting" runat="server" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">รายการแนบ Memo 
                                                             <asp:Label ID="nav_approve" Font-Bold="true" runat="server"></asp:Label>

                            <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu">
                            <li id="_divMenuBtnToViewWaitingApprove" runat="server" class="form-group">
                                <asp:LinkButton ID="_divMenuBtnToDivWaitingApprove" runat="server" CssClass="col-lg-9"
                                    CommandName="_divMenuBtnToDivWaitingApprove"
                                    OnCommand="btnCommand" Text="รายการรออนุมัติ" />
                                <asp:Label ID="nav_approve1" Font-Bold="true" runat="server" CssClass="col-lg-3"></asp:Label>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li id="_divMenuBtnToViewHistory" runat="server">
                                <asp:LinkButton ID="_divMenuBtnToDivHistory" runat="server"
                                    CommandName="_divMenuBtnToDivHistory"
                                    OnCommand="btnCommand" Text="ประวัติรายการ" />
                            </li>
                        </ul>
                    </li>

                    <li id="Li1" runat="server">
                        <%--NavigateUrl="~/Fixit/FormMIS/ยอดปริ๊นเตอร์62.pdf"--%>
                        <asp:HyperLink ID="hplFlowOvertime" NavigateUrl="~/uploadfiles/ยอดปริ๊นเตอร์ 62.pdf" Target="_blank" runat="server" CommandName="cmdFile" OnCommand="btnCommand"> สถิติการปริ้น</asp:HyperLink>

                    </li>

<%--                    <li id="_divMenuLiToManual" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivManual" runat="server"
                            CommandName="_divMenuBtnToDivManual"
                            OnCommand="btnCommand" Text="คู่มือประกอบการใช้งาน" />
                    </li>--%>

                </ul>
            </div>
        </nav>
    </div>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIT" runat="server">
            <div id="div_btn" runat="server" class="col-lg-12">
                <div class="form-group">
                    <ul class="nav nav-tabs bg-default">

                        <li id="_divMenuLiToViewIndex" runat="server" class="active">
                            <asp:LinkButton ID="btndata_index" CssClass="btn btn-default  btn-sm" runat="server" CommandName="cmddata_index" OnCommand="btnCommand" title="ข้อมูลหลัก"><h5><i class="glyphicon glyphicon-home"></i> ข้อมูลอ้างอิง</h5></asp:LinkButton>
                        </li>
                        <li id="_divMenuLiToViewBuy_Normal" runat="server">
                            <asp:LinkButton ID="btndbuy_normal" CssClass="btn btn-default  btn-sm" runat="server" CommandName="cmddbuy_normal" OnCommand="btnCommand" title="ขอซื้อทั่วไป"><h5><i class="glyphicon glyphicon-shopping-cart"></i> ขอซื้อทั่วไป</h5></asp:LinkButton>
                        </li>
                        <li id="_divMenuLiToViewBuy_Special" runat="server">
                            <asp:LinkButton ID="btndbuy_special" CssClass="btn btn-default  btn-sm" runat="server" CommandName="cmdbuy_special" OnCommand="btnCommand" title="ขอซื้อพิเศษ"><h5><i class="glyphicon glyphicon-asterisk"></i> ขอซื้อพิเศษ</h5></asp:LinkButton>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-lg-12" id="div_index" runat="server">

                <div id="SETBoxAllSearch" runat="server">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-usd "></i><strong>&nbsp; ค้นหาข้อมูล</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <asp:Label ID="Label67" CssClass="col-sm-2 control-label" runat="server" Text="หน่วยงาน :" />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlSearchSystem" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="RqddSystemSearch" ValidationGroup="btnsearch" runat="server" Display="None"
                                            ControlToValidate="ddlSearchSystem" Font-Size="11"
                                            ErrorMessage="กรุณาเลือกหน่วยงานที่ต้องการ"
                                            ValidationExpression="กรุณาเลือกหน่วยงานที่ต้องการ" InitialValue="0" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutsExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqddSystemSearch" Width="160" />

                                    </div>



                                    <asp:Label ID="Label66" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ :" />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlSearchTypeDevices" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="0">กรุณาเลือกประเภทอุปกรณ์...</asp:ListItem>
                                        </asp:DropDownList>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="btnsearch" runat="server" Display="None"
                                            ControlToValidate="ddlSearchTypeDevices" Font-Size="11"
                                            ErrorMessage="กรุณาเลือกประเภทอุปกรณ์"
                                            ValidationExpression="กรุณาเลือกประเภทอุปกรณ์" InitialValue="0" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-5 col-sm-offset-2">
                                        <asp:LinkButton ID="btnsearch" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch" ValidationGroup="btnsearch" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                        <asp:LinkButton ID="btnRefresh" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Refresh" runat="server" CommandName="BtnBack" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="alert-message alert-message-success">
                    <blockquote class="danger" style="font-size: small; background-color: lightgoldenrodyellow;">
                        <h4><b>Notice:</b></h4>
                        <p>
                            <asp:Label ID="lbltopic" runat="server"></asp:Label>
                        </p>
                    </blockquote>

                </div>


                <asp:GridView ID="GvType" runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                    HeaderStyle-Height="40px"
                    AllowPaging="false"
                    DataKeyNames="typidx"
                    OnRowDataBound="Master_RowDataBound">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">Data Cannot Be Found</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="รายละเอียดอุปกรณ์" ItemStyle-Font-Size="Medium">
                            <ItemTemplate>


                                <div class="alert-message alert-message-success">
                                    <blockquote class="danger" style="background-color: powderblue;">
                                        <h4><b>
                                            <asp:Label ID="Label1" runat="server" Font-Bold="true" Text='<%# Eval("type_name") %>'></asp:Label>
                                        </b></h4>
                                    </blockquote>
                                </div>

                                <asp:Label ID="lbltypidx" runat="server" CssClass="col-sm-10" Visible="false" Text='<%# Eval("typidx") %>'></asp:Label>

                                <asp:GridView ID="GvIT" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="success small"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="false"
                                    DataKeyNames="m0idx"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="รูปภาพประกอบ" HeaderStyle-CssClass="text-center" ItemStyle-Width="6%" Visible="true" ItemStyle-Font-Size="Small">

                                            <ItemTemplate>
                                                <asp:Label ID="lblm0idx" runat="server" Visible="false" Text='<%# Eval("m0idx") %>' />
                                                <asp:Label ID="lbltypidx_it" runat="server" Visible="false" Text='<%# Eval("typidx") %>' />
                                                <asp:Image ID="image" runat="server"></asp:Image>
                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ระดับการใช้งาน" ItemStyle-CssClass="text-center" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbleveldevice" runat="server" Text='<%# Eval("leveldevice") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ชื่อ" ItemStyle-CssClass="text-center" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbname" runat="server" Text='<%# Eval("typedevices") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="อายุการใช้งาน" ItemStyle-CssClass="text-center" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbuse_year" runat="server" Text='<%# Eval("use_year") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ราคา" ItemStyle-CssClass="text-center" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbpricenotshow" Visible="false" runat="server" Text='<%# Eval("price") %>'></asp:Label>
                                                <asp:Label ID="lbprice" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ค่า MA ต่อปี" ItemStyle-CssClass="text-center" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbmapriceshow" Visible="false" runat="server" Text='<%# Eval("ma_price") %>'></asp:Label>
                                                <asp:Label ID="lbmaprice" runat="server"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbdetail" runat="server" Text='<%# Eval("detail") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbremark" runat="server" Text='<%# Eval("remark") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลผู้มีสิทธิ์" HeaderStyle-CssClass="text-center" ItemStyle-Width="23%" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>

                                                <asp:GridView ID="Gvperdevices" runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                    HeaderStyle-CssClass="info small"
                                                    HeaderStyle-Height="40px"
                                                    AllowPaging="false"
                                                    DataKeyNames="m1idx"
                                                    OnRowDataBound="Master_RowDataBound">

                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="องค์กร" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="lblorgidx" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="lblrdepidx" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="แผนก" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <asp:Literal ID="lblrsecidx" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblposidx" runat="server" Text='<%# Eval("pos_name") %>' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Management" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="CmdEditIT" OnCommand="btnCommand" CommandArgument='<%# Eval("m0idx") + ";" + Eval("typidx") + ";" + Eval("SysIDX") +";" + Eval("tdidx")%>' data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                                <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0idx") + ";" + Eval("SysIDX")%>'><i class="fa fa-trash"></i></asp:LinkButton>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>


                                    </Columns>
                                </asp:GridView>


                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>


            </div>

            <div class="col-lg-12" id="div_buynormal" runat="server" visible="false">

                <div class="col-lg-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                        </div>
                        <asp:FormView ID="FvDataUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>


                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>


                                        <%-------------- แผนก,ตำแหน่ง --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>

                                        <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                            <div class="col-sm-3">
                                                <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                                <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                            <div class="col-sm-3">
                                                <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                                <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>


                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="	fa fa-bookmark"></i><strong>&nbsp; เลือกประเภทที่ต้องการ</strong></h3>
                        </div>
                        <div class="form-horizontal" role="form">
                            <div class="panel-body">

                                <div class="col-lg-12">
                                    <div class="col-sm-4 col-sm-offset-2">

                                        <asp:ImageButton ID="imgbuynew" CssClass="img-responsive" runat="server" ToolTip="ซื้อใหม่" Width="70%" CommandName="btnsystem" CommandArgument="1" OnCommand="btnCommand" ImageUrl="~/images/price-reference/picbtnbuynew.png" />
                                    </div>

                                    <div class="col-sm-4">

                                        <asp:ImageButton ID="imgbuyreplace" CssClass="img-responsive" ToolTip="ซื้อทดแทน" runat="server" Width="70%" CommandName="btnsystem" CommandArgument="2" OnCommand="btnCommand" ImageUrl="~/images/price-reference/picbtnbuyreplace.png" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div id="divbuy_new" runat="server" visible="false">

                    <div class="col-lg-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-cart-plus"></i><strong>&nbsp; ระบุอุปกรณ์ที่ต้องการ</strong></h3>
                            </div>
                            <asp:FormView ID="fvbuynew_normal" runat="server" DefaultMode="Insert" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>

                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="alert-message alert-message-success">
                                                <blockquote class="danger" style="font-size: small; background-color: burlywood; text-align: left">
                                                    <h4 style="color: whitesmoke"><b>ระบุข้อมูลที่ต้องการซื้อใหม่</b></h4>
                                                </blockquote>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label59" runat="server" Text="องค์กร" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlOrg_buynew" ValidationGroup="SearchData" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="SearchData" runat="server" Display="None"
                                                        ControlToValidate="ddlOrg_buynew" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกองค์กร"
                                                        ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />

                                                </div>
                                                <asp:Label ID="Label4" runat="server" Text="ฝ่าย" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlDep_buynew" ValidationGroup="SearchData" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Value="0">กรุณาเลือกฝ่าย...</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="SearchData" runat="server" Display="None"
                                                        ControlToValidate="ddlDep_buynew" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกฝ่าย"
                                                        ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label19" runat="server" Text="แผนก" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlSec_buynew" ValidationGroup="SearchData" AutoPostBack="true" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">กรุณาเลือกแผนก...</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ValidationGroup="SearchData" runat="server" Display="None"
                                                        ControlToValidate="ddlSec_buynew" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกแผนก"
                                                        ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator13" Width="160" />

                                                </div>
                                                <asp:Label ID="Label21" runat="server" Text="ตำแหน่ง" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlpos_buynew" runat="server" ValidationGroup="SearchData" CssClass="form-control">
                                                        <asp:ListItem Value="0">กรุณาเลือกตำแหน่ง...</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="SearchData" runat="server" Display="None"
                                                        ControlToValidate="ddlpos_buynew" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกตำแหน่ง"
                                                        ValidationExpression="กรุณาเลือกตำแหน่ง" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator14" Width="160" />

                                                </div>
                                            </div>

                                            <div class="form-group" runat="server">
                                                <div class="col-sm-3 col-sm-offset-2">
                                                    <asp:LinkButton ID="btnsearchdata" CssClass="btn btn-info btn-sm" runat="server" CommandName="CmdSearch" OnCommand="btnCommand" ValidationGroup="SearchData" title="search"><i class="fa fa-search"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                </InsertItemTemplate>
                            </asp:FormView>


                        </div>
                    </div>
                </div>

                <div id="divbuy_replace" runat="server" visible="false">
                    <div class="col-lg-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-cart-plus"></i><strong>&nbsp; ระบุข้อมูลที่ต้องการซื้อ</strong></h3>
                            </div>


                            <asp:FormView ID="fvbuyreplace_normal" runat="server" DefaultMode="Insert" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>

                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="alert-message alert-message-success">
                                                <blockquote class="danger" style="font-size: small; background-color: burlywood; text-align: left">
                                                    <h4 style="color: whitesmoke"><b>ระบุข้อมูลที่ต้องการซื้อทดแทน</b></h4>
                                                </blockquote>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label59" runat="server" Text="รหัสพนักงาน" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtempcode_replace" MaxLength="8" OnTextChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RqRetxtpridce22" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                        ControlToValidate="txtempcode_replace" Font-Size="11"
                                                        ErrorMessage="กรุณารหัสพนักงาน" />
                                                    <asp:RegularExpressionValidator ID="Retxtprsice22" runat="server" ValidationGroup="SaveDataSet" Display="None"
                                                        ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                        ControlToValidate="txtempcode_replace"
                                                        ValidationExpression="^[0-9]{1,10}$" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtpridce22" Width="160" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprsice22" Width="160" />
                                                </div>

                                            </div>



                                            <div id="div_profileemp_replacenormal" runat="server" visible="false">


                                                <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                                <div class="form-group">

                                                    <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtorg_replacenormal" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <asp:TextBox ID="txtorgidx_replacenormal" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>

                                                    <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtrequesdept_replacenormal" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <asp:TextBox ID="txtrequesdeptidx_replacenormal" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>

                                                </div>


                                                <%-------------- แผนก,ตำแหน่ง --------------%>
                                                <div class="form-group">

                                                    <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtsec_replacenormal" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <asp:TextBox ID="txtsecidx_replacenormal" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>

                                                    <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtpos_replacenormal" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>

                                                </div>

                                                <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                                <div class="form-group">

                                                    <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtrequesname_replacenormal" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <asp:TextBox ID="txtempidx" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>

                                                    <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="กลุ่มพนักงาน : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtposgroup_replacenormal" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <asp:TextBox ID="txtposgroupidx_replacenormal" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Labsel4" runat="server" Text="เครื่องถือครอง" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddldevicesholder_replacenormal" ValidationGroup="SearchData" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="0">กรุณาเลือกเครื่องถือครอง...</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="SearchData" runat="server" Display="None"
                                                            ControlToValidate="ddldevicesholder_replacenormal" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกเครื่องถือครอง"
                                                            ValidationExpression="กรุณาเลือกเครื่องถือครอง" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-3 col-sm-offset-2">
                                                        <asp:LinkButton ID="btnsearchdata" CssClass="btn btn-info btn-sm" runat="server" CommandName="CmdSearch" OnCommand="btnCommand" ValidationGroup="SearchData" title="search"><i class="fa fa-search"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:FormView>

                        </div>
                    </div>


                </div>


                <div class="panel-body" id="div_product_normal" runat="server">
                    <div class="form-horizontal" role="form">


                        <div class="col-lg-12" id="div_historydevices" runat="server" visible="false">

                            <div class="alert-message alert-message-success">
                                <blockquote class="danger" style="font-size: small; background-color: rosybrown; text-align: left">
                                    <h4 style="color: whitesmoke"><b>ประวัติเครื่องถือครอง</b></h4>
                                </blockquote>

                            </div>
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-history"></i><strong>&nbsp; ประวัติเครื่องถือครอง</strong></h3>
                                </div>
                                <div class="panel-body">
                                    <asp:GridView ID="GvHistory_Devices" runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                        HeaderStyle-CssClass="danger small"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="false"
                                        DataKeyNames="software_name_idx"
                                        OnRowDataBound="Master_RowDataBound">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>
                                        <Columns>


                                            <asp:TemplateField HeaderText="ประเภทเครื่อง" ItemStyle-CssClass="text-center" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Literal ID="lbldevices" Text='<%# Eval("name_m0_typedevice") %>' runat="server"></asp:Literal>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="เครื่องถือครอง" ItemStyle-CssClass="text-center" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Literal ID="lbleveldevice" runat="server"></asp:Literal>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="โปรแกรมถือครอง" ItemStyle-CssClass="text-center" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblsoftware_holder" Text='<%# Eval("Softwarename") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="โปรแกรมแนะนำ" ItemStyle-CssClass="text-center" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblsoftware_rec" Text='<%# Eval("Softwarename_rec") %>' runat="server"></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="alert-message alert-message-success">
                                <blockquote class="danger" style="font-size: small; background-color: darksalmon; text-align: left">
                                    <h4 style="color: whitesmoke"><b>อุปกรณ์ Hardware</b></h4>
                                </blockquote>

                            </div>

                            <asp:GridView ID="GvHardware" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="success small"
                                HeaderStyle-Height="40px"
                                AllowPaging="false"
                                DataKeyNames="m0idx"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>


                                    <asp:TemplateField HeaderText="เลือกรายการ" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>

                                            <asp:RadioButton ID="rdohardware" AutoPostBack="true" OnCheckedChanged="ddlSelectedIndexChanged" GroupName="rdohardware" runat="server" />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ระดับการใช้งาน" ItemStyle-CssClass="text-center" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lblm0idx" runat="server" Visible="false" Text='<%# Eval("m0idx") %>' />
                                            <asp:Label ID="lbltdidx" runat="server" Visible="false" Text='<%# Eval("tdidx") %>' />
                                            <asp:Label ID="lbltypidx_it" runat="server" Visible="false" Text='<%# Eval("typidx") %>' />
                                            <asp:Label ID="lbleveldevice" runat="server" Text='<%# Eval("leveldevice") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชื่อ" ItemStyle-CssClass="text-center" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("typedevices") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="ราคา" ItemStyle-CssClass="text-center" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbpricenotshow" Visible="false" runat="server" Text='<%# Eval("price") %>'></asp:Label>
                                            <asp:Label ID="lbprice" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbdetail" runat="server" Text='<%# Eval("detail") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbremark" runat="server" Text='<%# Eval("remark") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>

                        <div class="col-lg-12">
                            <div class="alert-message alert-message-success" id="div_monitor" runat="server" visible="false">
                                <blockquote class="danger" style="font-size: small; background-color: goldenrod; text-align: left">
                                    <h4 style="color: whitesmoke"><b>อุปกรณ์ Monitor</b></h4>
                                </blockquote>

                            </div>

                            <asp:GridView ID="GvMonitor" runat="server" Visible="false"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="warning small"
                                HeaderStyle-Height="40px"
                                AllowPaging="false"
                                DataKeyNames="m0idx"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>


                                    <asp:TemplateField HeaderText="เลือกรายการ" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>

                                            <asp:RadioButton ID="rdomonitor" AutoPostBack="true" OnCheckedChanged="ddlSelectedIndexChanged" GroupName="rdomonitor" runat="server" />

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ระดับการใช้งาน" ItemStyle-CssClass="text-center" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lblm0idx" runat="server" Visible="false" Text='<%# Eval("m0idx") %>' />
                                            <asp:Label ID="lbltdidx" runat="server" Visible="false" Text='<%# Eval("tdidx") %>' />
                                            <asp:Label ID="lbltypidx_it" runat="server" Visible="false" Text='<%# Eval("typidx") %>' />
                                            <asp:Label ID="lbleveldevice" runat="server" Text='<%# Eval("leveldevice") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชื่อ" ItemStyle-CssClass="text-center" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("typedevices") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="ราคา" ItemStyle-CssClass="text-center" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbpricenotshow" Visible="false" runat="server" Text='<%# Eval("price") %>'></asp:Label>
                                            <asp:Label ID="lbprice" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbdetail" runat="server" Text='<%# Eval("detail") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbremark" runat="server" Text='<%# Eval("remark") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                        </div>
                        <div class="col-lg-12">
                            <div class="alert-message alert-message-success">
                                <blockquote class="danger" style="font-size: small; background-color: peru; text-align: left">
                                    <h4 style="color: whitesmoke"><b>อุปกรณ์ Software</b></h4>
                                </blockquote>

                            </div>

                            <asp:GridView ID="GvSoftware" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="info small"
                                HeaderStyle-Height="40px"
                                AllowPaging="false"
                                DataKeyNames="m0idx"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>


                                    <asp:TemplateField HeaderText="เลือกรายการ" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>

                                            <asp:CheckBox Checked="true" ID="chksoftware" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ระดับการใช้งาน" ItemStyle-CssClass="text-center" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lblm0idx" runat="server" Visible="false" Text='<%# Eval("m0idx") %>' />
                                            <asp:Label ID="lbltypidx_it" runat="server" Visible="false" Text='<%# Eval("typidx") %>' />
                                            <asp:Label ID="lbleveldevice" runat="server" Text='<%# Eval("leveldevice") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชื่อ" ItemStyle-CssClass="text-center" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("typedevices") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="ราคา" ItemStyle-CssClass="text-center" ItemStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbpricenotshow" Visible="false" runat="server" Text='<%# Eval("price") %>'></asp:Label>
                                            <asp:Label ID="lbprice" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbdetail" runat="server" Text='<%# Eval("detail") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbremark" runat="server" Text='<%# Eval("remark") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                            <div class="form-group">
                                <p class="help-block"><font color="red"><asp:Label ID="Label68" class="col-sm-8 control-label" runat="server" Text="ทางไอทีแนะนำแค่โปรแกรมพื้นฐานที่ต้องใช้งาน ส่วนโปรแกรมอื่นๆ ที่มีความจำเป็นต้องใช้งาน ให้เขียนระบุขอซื้อใน Memo " /></font></p>
                            </div>

                        </div>
                        <div class="col-lg-12">
                            <div class="form-group" runat="server" visible="false" id="div_btndataset">
                                <div class="col-sm-2">
                                    <asp:LinkButton ID="btnAdddataset" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdInsert_Dataset" OnCommand="btnCommand" ValidationGroup="SaveDataSet" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save">ADD +</asp:LinkButton>
                                </div>
                            </div>

                            <div class="form-group">


                                <div class="col-lg-12">
                                    <asp:GridView ID="GvProductAdd"
                                        runat="server"
                                        CssClass="table table-striped table-responsive info"
                                        GridLines="None"
                                        OnRowDataBound="Master_RowDataBound"
                                        OnRowCommand="onRowCommand"
                                        AutoGenerateColumns="false">


                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>

                                        <Columns>

                                            <asp:TemplateField HeaderText="ข้อมูลผู้ขอราคา" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" Visible="true" ItemStyle-Font-Size="Small">

                                                <ItemTemplate>
                                                    <asp:Label ID="lblorgidx" Visible="false" runat="server" Text='<%# Eval("orgidx") %>' />
                                                    <asp:Label ID="lblrdeptidx" Visible="false" runat="server" Text='<%# Eval("rdeptidx") %>' />
                                                    <asp:Label ID="lblrsecidx" Visible="false" runat="server" Text='<%# Eval("rsecidx") %>' />
                                                    <asp:Label ID="lblrposidx" Visible="false" runat="server" Text='<%# Eval("posidx") %>' />
                                                    <asp:Label ID="lblemp_idx" Visible="false" runat="server" Text='<%# Eval("emp_idx") %>' />
                                                    <asp:Label ID="lblu0_didx" Visible="false" runat="server" Text='<%# Eval("u0_didx") %>' />

                                                    <strong>
                                                        <asp:Label ID="Label60" runat="server"> องค์กร: </asp:Label></strong>
                                                    <asp:Literal ID="lbPosName" runat="server" Text='<%# Eval("OrgName") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label61" runat="server"> ฝ่าย: </asp:Label></strong>
                                                    <asp:Label ID="lbdep" runat="server" Text='<%# Eval("DeptName") %>'></asp:Label>
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="lbdat" runat="server"> แผนก: </asp:Label></strong>
                                                    <asp:Label ID="lblocate" runat="server" Text='<%# Eval("SecName") %>'></asp:Label>
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label31" runat="server"> ตำแหน่ง: </asp:Label></strong>
                                                    <asp:Label ID="Label32" runat="server" Text='<%# Eval("PosName") %>'></asp:Label>

                                                    <br />
                                                    <div id="div_buyreplace" runat="server" visible="false">
                                                        <strong>
                                                            <asp:Label ID="Label35" runat="server"> ผู้ถือครอง: </asp:Label></strong>
                                                        <asp:Label ID="Label36" runat="server" Text='<%# Eval("HolderName") %>'></asp:Label>
                                                        <br />
                                                        <strong>
                                                            <asp:Label ID="Label37" runat="server"> เครื่องถือครอง: </asp:Label></strong>
                                                        <asp:Label ID="Label38" runat="server" Text='<%# Eval("DevicesName") %>'></asp:Label>
                                                    </div>
                                                </ItemTemplate>

                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="อุปกรณ์ Hardware" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" Visible="true" ItemStyle-Font-Size="Small">

                                                <ItemTemplate>
                                                    <asp:Label ID="lblhdidx" Visible="false" runat="server" Text='<%# Eval("hdidx") %>' />
                                                    <asp:Literal ID="lbHardwarename" runat="server" Text='<%# Eval("Hardwarename") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="อุปกรณ์ Monitor" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" Visible="true" ItemStyle-Font-Size="Small">

                                                <ItemTemplate>
                                                    <asp:Label ID="lblmtidx" Visible="false" runat="server" Text='<%# Eval("mtidx") %>' />
                                                    <asp:Literal ID="lbMonitorname" runat="server" Text='<%# Eval("Monitorname") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="อุปกรณ์ Software" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" Visible="true" ItemStyle-Font-Size="Small">

                                                <ItemTemplate>
                                                    <asp:Label ID="lblswidx" Visible="false" runat="server" Text='<%# Eval("swidx") %>' />
                                                    <asp:Literal ID="lbSoftwarename" runat="server" Text='<%# Eval("Softwarename") %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteListMemo"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                </ItemTemplate>
                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                        </Columns>

                                    </asp:GridView>
                                </div>

                            </div>

                            <div class="form-group" runat="server" visible="false" id="div_save">
                                <div class="col-sm-2 col-sm-offset-10">
                                    <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert_Buynew" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                    <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-12" id="div_buyspecial" runat="server" visible="false">

                <div class="col-lg-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                        </div>
                        <asp:FormView ID="FvDetailUser_BuySpecial" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>


                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>


                                        <%-------------- แผนก,ตำแหน่ง --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>

                                        <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                            <div class="col-sm-3">
                                                <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                                <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                            <div class="col-sm-3">
                                                <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                                <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>


                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="	fa fa-bookmark"></i><strong>&nbsp; เลือกประเภทที่ต้องการ</strong></h3>
                        </div>
                        <div class="form-horizontal" role="form">
                            <div class="panel-body">

                                <div class="col-lg-12">
                                    <div class="col-sm-4 col-sm-offset-2">

                                        <asp:ImageButton ID="imgbuynew_s" CssClass="img-responsive" runat="server" ToolTip="ซื้อใหม่" Width="70%" CommandName="btnsystem" CommandArgument="3" OnCommand="btnCommand" ImageUrl="~/images/price-reference/picbtnbuynew_s.png" />
                                    </div>

                                    <div class="col-sm-4">
                                        <asp:ImageButton ID="imgbuyreplace_s" CssClass="img-responsive" ToolTip="ซื้อทดแทน" runat="server" Width="70%" CommandName="btnsystem" CommandArgument="4" OnCommand="btnCommand" ImageUrl="~/images/price-reference/picbtnbuyreplace_s.png" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div id="divbuy_newspecial" runat="server" visible="false">
                    <div class="col-lg-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-cart-plus"></i><strong>&nbsp; ระบุอุปกรณ์ที่ต้องการ</strong></h3>
                            </div>

                            <asp:FormView ID="FvBuyNew_Special" runat="server" DefaultMode="Insert" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>

                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="alert-message alert-message-success">
                                                <blockquote class="danger" style="font-size: small; background-color: dimgrey; text-align: left">
                                                    <h4 style="color: whitesmoke"><b>ระบุข้อมูลที่ต้องการซื้อใหม่</b></h4>
                                                </blockquote>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label59" runat="server" Text="องค์กร" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlOrg_buynew_s" ValidationGroup="SearchData" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="SearchData" runat="server" Display="None"
                                                        ControlToValidate="ddlOrg_buynew_s" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกองค์กร"
                                                        ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />

                                                </div>
                                                <asp:Label ID="Label4" runat="server" Text="ฝ่าย" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlDep_buynew_s" ValidationGroup="SearchData" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Value="0">กรุณาเลือกฝ่าย...</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="SearchData" runat="server" Display="None"
                                                        ControlToValidate="ddlDep_buynew_s" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกฝ่าย"
                                                        ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label19" runat="server" Text="แผนก" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlSec_buynew_s" ValidationGroup="SearchData" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">กรุณาเลือกแผนก...</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ValidationGroup="SearchData" runat="server" Display="None"
                                                        ControlToValidate="ddlSec_buynew_s" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกแผนก"
                                                        ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator13" Width="160" />

                                                </div>



                                                <asp:Label ID="Label21" runat="server" Text="ตำแหน่ง" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlPos_buynew_s" runat="server" ValidationGroup="SearchData" CssClass="form-control">
                                                        <asp:ListItem Value="0">กรุณาเลือกตำแหน่ง...</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="SearchData" runat="server" Display="None"
                                                        ControlToValidate="ddlPos_buynew_s" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกตำแหน่ง"
                                                        ValidationExpression="กรุณาเลือกตำแหน่ง" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator14" Width="160" />

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label46" runat="server" Text="อุปกรณ์" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddltypedevice_buynew_s" runat="server"  ValidationGroup="SearchData" CssClass="form-control">
                                                        <asp:ListItem Value="0">กรุณาเลือกรุ่นอุปกรณ์...</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ValidationGroup="SearchData" runat="server" Display="None"
                                                        ControlToValidate="ddltypedevice_buynew_s" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกรุ่นอุปกรณ์"
                                                        ValidationExpression="กรุณาเลือกรุ่นอุปกรณ์" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender26" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator14" Width="160" />
                                                </div>
                                            </div>
                                            <div class="form-group" runat="server">
                                                <div class="col-sm-3 col-sm-offset-2">
                                                    <asp:LinkButton ID="btnsearchdata" CssClass="btn btn-info btn-sm" runat="server" CommandName="CmdSearch_S" OnCommand="btnCommand" ValidationGroup="SearchData" title="search"><i class="fa fa-search"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:FormView>


                        </div>
                    </div>
                </div>


                <div id="divbuy_replacespecial" runat="server" visible="false">
                    <div class="col-lg-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-cart-plus"></i><strong>&nbsp; ระบุอุปกรณ์ที่ต้องการ</strong></h3>
                            </div>

                            <asp:FormView ID="FvBuyReplace_Special" runat="server" DefaultMode="Insert" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>

                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="alert-message alert-message-success">
                                                <blockquote class="danger" style="font-size: small; background-color: dimgrey; text-align: left">
                                                    <h4 style="color: whitesmoke"><b>ระบุข้อมูลที่ต้องการซื้อทดแทน</b></h4>
                                                </blockquote>
                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="Label59" runat="server" Text="รหัสพนักงาน" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtempcode_replace_s" MaxLength="8" OnTextChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RqRetxtpridce22" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                        ControlToValidate="txtempcode_replace_s" Font-Size="11"
                                                        ErrorMessage="กรุณารหัสพนักงาน" />
                                                    <asp:RegularExpressionValidator ID="Retxtprsice22" runat="server" ValidationGroup="SaveDataSet" Display="None"
                                                        ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                        ControlToValidate="txtempcode_replace_s"
                                                        ValidationExpression="^[0-9]{1,10}$" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtpridce22" Width="160" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprsice22" Width="160" />
                                                </div>

                                            </div>



                                            <div id="div_profileemp_replacespecial" runat="server" visible="false">


                                                <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                                <div class="form-group">

                                                    <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtorg_replacespecial" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <asp:TextBox ID="txtorgidx_replacespecial" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>

                                                    <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtrequesdept_replacespecial" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <asp:TextBox ID="txtrequesdeptidx_replacespecial" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>

                                                </div>


                                                <%-------------- แผนก,ตำแหน่ง --------------%>
                                                <div class="form-group">

                                                    <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtsec_replacespecial" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <asp:TextBox ID="txtsecidx_replacespecial" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>

                                                    <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtposidx_replacespecial" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <asp:TextBox ID="txtpos_replacespecial" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>

                                                </div>

                                                <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                                <div class="form-group">

                                                    <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtrequesname_replacespecial" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <asp:TextBox ID="txtempidx_special" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>

                                                    <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="กลุ่มพนักงาน : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtposgroup_replacespecial" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <asp:TextBox ID="txtposgroupidx_replacespecial" Visible="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Labsel4" runat="server" Text="เครื่องถือครอง" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddldevicesholder_replacespecial" ValidationGroup="SearchData" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="0">กรุณาเลือกเครื่องถือครอง...</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="SearchData" runat="server" Display="None"
                                                            ControlToValidate="ddldevicesholder_replacespecial" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกเครื่องถือครอง"
                                                            ValidationExpression="กรุณาเลือกเครื่องถือครอง" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <asp:Label ID="Label46" runat="server" Text="อุปกรณ์" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddltypedevice_buyreplace_s"  runat="server" ValidationGroup="SearchData" CssClass="form-control">
                                                            <asp:ListItem Value="0">กรุณาเลือกรุ่นอุปกรณ์...</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ValidationGroup="SearchData" runat="server" Display="None"
                                                            ControlToValidate="ddltypedevice_buyreplace_s" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกรุ่นอุปกรณ์"
                                                            ValidationExpression="กรุณาเลือกรุ่นอุปกรณ์" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender26" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator16" Width="160" />
                                                    </div>
                                                </div>
                                                <div class="form-group" runat="server">
                                                    <div class="col-sm-3 col-sm-offset-2">
                                                        <asp:LinkButton ID="btnsearchdata" CssClass="btn btn-info btn-sm" runat="server" CommandName="CmdSearch_S" OnCommand="btnCommand" ValidationGroup="SearchData" title="search"><i class="fa fa-search"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:FormView>



                        </div>
                    </div>
                </div>

                <div class="panel-body" id="divproduct_special" runat="server" visible="false">
                    <div class="form-horizontal" role="form">


                        <div class="col-lg-12" id="div_historydevices_s" runat="server" visible="false">
                            <div class="alert-message alert-message-success">
                                <blockquote class="danger" style="font-size: small; background-color: rosybrown; text-align: left">
                                    <h4 style="color: whitesmoke"><b>ประวัติเครื่องถือครอง</b></h4>
                                </blockquote>

                            </div>
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-history"></i><strong>&nbsp; ประวัติเครื่องถือครอง</strong></h3>
                                </div>
                                <div class="panel-body">
                                    <asp:GridView ID="GvHistory_Devices_S" runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                        HeaderStyle-CssClass="danger small"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="false"
                                        DataKeyNames="software_name_idx"
                                        OnRowDataBound="Master_RowDataBound">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>
                                        <Columns>


                                            <asp:TemplateField HeaderText="ประเภทเครื่อง" ItemStyle-CssClass="text-center" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Literal ID="lbldevices" Text='<%# Eval("name_m0_typedevice") %>' runat="server"></asp:Literal>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="เครื่องถือครอง" ItemStyle-CssClass="text-center" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Literal ID="lbleveldevice" runat="server"></asp:Literal>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="โปรแกรมถือครอง" ItemStyle-CssClass="text-center" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblsoftware_holder" Text='<%# Eval("Softwarename") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="โปรแกรมแนะนำ" ItemStyle-CssClass="text-center" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <asp:Label ID="lblsoftware_rec" Text='<%# Eval("Softwarename_rec") %>' runat="server"></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="alert-message alert-message-success">
                                <blockquote class="danger" style="font-size: small; background-color: darksalmon; text-align: left">
                                    <h4 style="color: whitesmoke"><b>อุปกรณ์ Hardware</b></h4>
                                </blockquote>

                            </div>
                            <asp:GridView ID="GvTypeDevice" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="success small"
                                HeaderStyle-Height="40px"
                                AllowPaging="false"
                                DataKeyNames="m0_tyidx"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="ประเภทอุปกรณ์" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>

                                            <asp:Label ID="lbtype_name" runat="server" Text='<%# Eval("type_name") %>'></asp:Label>
                                            <asp:Label ID="lblm0_tyidx" Visible="false" runat="server" Text='<%# Eval("m0_tyidx") %>'></asp:Label>
                                            <asp:Label ID="lblm0_naidx" Visible="false" runat="server" Text='<%# Eval("m0_naidx") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="อุปกรณ์" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:RadioButtonList ID="rdo_typedevice" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                            </asp:RadioButtonList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                                ValidationGroup="SaveDataSet_S" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบอุปกรณ์ให้ครบถ้วน"
                                                Display="None" SetFocusOnError="true" ControlToValidate="rdo_typedevice" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenxder7" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160"
                                                PopupPosition="BottomLeft" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>

                        <div class="col-lg-12" id="div_software_special" runat="server">
                            <div class="alert-message alert-message-success">
                                <blockquote class="danger" style="font-size: small; background-color: peru; text-align: left">
                                    <h4 style="color: whitesmoke"><b>อุปกรณ์ Software</b></h4>
                                </blockquote>

                            </div>
                            <asp:GridView ID="GvSoftWare_BuySpecial" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="info small"
                                HeaderStyle-Height="40px"
                                AllowPaging="false"
                                DataKeyNames="m0idx"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>


                                    <asp:TemplateField HeaderText="เลือกรายการ" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>

                                            <asp:CheckBox Checked="true" ID="chksoftware" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชื่อ" ItemStyle-CssClass="text-center" ItemStyle-Width="7%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("typedevices") %>'></asp:Label>
                                            <asp:Label ID="lblm0idx" Visible="false" runat="server" Text='<%# Eval("m0idx") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbdetail" runat="server" Text='<%# Eval("detail") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbremark" runat="server" Text='<%# Eval("remark") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                            <div class="form-group">
                                <p class="help-block"><font color="red"><asp:Label ID="Labesl14" class="col-sm-8 control-label" runat="server" Text="ทางไอทีแนะนำแค่โปรแกรมพื้นฐานที่ต้องใช้งาน ส่วนโปรแกรมอื่นๆ ที่มีความจำเป็นต้องใช้งาน ให้เขียนระบุขอซื้อใน Memo " /></font></p>
                            </div>
                        </div>

                        <div class="form-group">

                            <asp:Label ID="Label14" class="col-sm-2 control-label" runat="server" Text="ความต้องการอื่นๆ : " />
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtdetail_buyspecial" PlaceHolder="ยกตัวอย่าง เครื่องน้ำหนักเบา เป็นต้น" TextMode="MultiLine" Rows="3" class="form-control" runat="server" />

                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label ID="Label15" class="col-sm-2 control-label" runat="server" Text="เหตุผลที่ต้องใช้งานเครื่องแบบพิเศษ : " />
                            <div class="col-sm-8">
                                <asp:TextBox ID="txtremark_buyspecial" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control"></asp:TextBox>

                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="SaveDataSet_S" runat="server" Display="None" ControlToValidate="txtremark_buyspecial" Font-Size="11"
                                    ErrorMessage="กรุณากรอกหมายเหตุ"
                                    ValidationExpression="กรุณากรอกหมายเหตุ"
                                    SetFocusOnError="true" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                    ValidationGroup="SaveDataSet_S" Display="None"
                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                    ControlToValidate="txtremark_buyspecial"
                                    ValidationExpression="^[\s\S]{0,1000}$"
                                    SetFocusOnError="true" />

                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />


                            </div>
                        </div>

                        <div class="form-group" runat="server" visible="false" id="div_dataset_s">
                            <div class="col-sm-3 col-sm-offset-2">
                                <asp:LinkButton ID="btnInsert_Dataset_Special" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdInsert_Dataset_Special" OnCommand="btnCommand" ValidationGroup="SaveDataSet_S" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save">ADD +</asp:LinkButton>
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-lg-12">
                                <asp:GridView ID="GvProductSpecialAdd"
                                    runat="server"
                                    CssClass="table table-striped table-responsive info"
                                    GridLines="None"
                                    OnRowDataBound="Master_RowDataBound"
                                    OnRowCommand="onRowCommand"
                                    AutoGenerateColumns="false">


                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>

                                    <Columns>

                                        <asp:TemplateField HeaderText="ข้อมูลผู้ขอราคา" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Small">

                                            <ItemTemplate>
                                                <asp:Label ID="lblorgidx" Visible="false" runat="server" Text='<%# Eval("orgidx") %>' />
                                                <asp:Label ID="lblrdeptidx" Visible="false" runat="server" Text='<%# Eval("rdeptidx") %>' />
                                                <asp:Label ID="lblrsecidx" Visible="false" runat="server" Text='<%# Eval("rsecidx") %>' />
                                                <asp:Label ID="lblrposidx" Visible="false" runat="server" Text='<%# Eval("posidx") %>' />
                                                <asp:Label ID="lblemp_idx" Visible="false" runat="server" Text='<%# Eval("emp_idx") %>' />
                                                <asp:Label ID="lblu0_didx" Visible="false" runat="server" Text='<%# Eval("u0_didx") %>' />

                                                <strong>
                                                    <asp:Label ID="Label60" runat="server"> องค์กร: </asp:Label></strong>
                                                <asp:Literal ID="lbPosName" runat="server" Text='<%# Eval("OrgName") %>' />
                                                <br />
                                                <strong>
                                                    <asp:Label ID="Label61" runat="server"> ฝ่าย: </asp:Label></strong>
                                                <asp:Label ID="lbdep" runat="server" Text='<%# Eval("DeptName") %>'></asp:Label>
                                                <br />
                                                <strong>
                                                    <asp:Label ID="lbdat" runat="server"> แผนก: </asp:Label></strong>
                                                <asp:Label ID="lblocate" runat="server" Text='<%# Eval("SecName") %>'></asp:Label>
                                                <br />
                                                <strong>
                                                    <asp:Label ID="Label31" runat="server"> ตำแหน่ง: </asp:Label></strong>
                                                <asp:Label ID="Label32" runat="server" Text='<%# Eval("PosName") %>'></asp:Label>

                                                <br />
                                                <div id="div_buyreplace" runat="server" visible="false">
                                                    <strong>
                                                        <asp:Label ID="Label35" runat="server"> ผู้ถือครอง: </asp:Label></strong>
                                                    <asp:Label ID="Label36" runat="server" Text='<%# Eval("HolderName") %>'></asp:Label>
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="Label37" runat="server"> เครื่องถือครอง: </asp:Label></strong>
                                                    <asp:Label ID="Label38" runat="server" Text='<%# Eval("DevicesName") %>'></asp:Label>
                                                </div>
                                            </ItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ประเภท" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" Visible="true" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>

                                                <asp:Label ID="lblm0_naidx" Visible="false" runat="server" Text='<%# Eval("m0_naidx") %>' />
                                                <asp:Literal ID="lbname_gen" runat="server" Text='<%# Eval("name_gen") %>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ประเภทอุปกรณ์" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" Visible="true" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>

                                                <asp:Label ID="lblm0_tyidx" Visible="false" runat="server" Text='<%# Eval("m0_tyidx_comma") %>' />
                                                <asp:Label ID="lbltype_name" runat="server" Text='<%# Eval("type_name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="อุปกรณ์" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" Visible="true" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>

                                                <asp:Label ID="lblm2_tyidx" Visible="false" runat="server" Text='<%# Eval("m2_tyidx_comma") %>' />
                                                <asp:Literal ID="lbdevice_name" runat="server" Text='<%# Eval("device_name") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField HeaderText="อุปกรณ์" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%"  HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>

                                                    <asp:GridView ID="Gvdevices_Special" runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                        HeaderStyle-CssClass="info small"
                                                        HeaderStyle-Height="40px"
                                                        AllowPaging="false">

                                                        <PagerStyle CssClass="pageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="ประเภทอุปกรณ์" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblm0_tyidx" Visible="false" runat="server" Text='<%# Eval("m0_tyidx") %>' />
                                                                    <asp:Label ID="lbltype_name" runat="server" Text='<%# Eval("type_name") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="อุปกรณ์" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblm2_tyidx" Visible="false" runat="server" Text='<%# Eval("m2_tyidx") %>' />
                                                                    <asp:Literal ID="lbdevice_name" runat="server" Text='<%# Eval("device_name") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>
                                                    </asp:GridView>

                                                </ItemTemplate>
                                            </asp:TemplateField>--%>

                                        <asp:TemplateField HeaderText="โปรแกรม" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Small">

                                            <ItemTemplate>
                                                <asp:Literal ID="lblswidx" Visible="false" runat="server" Text='<%# Eval("swidx") %>' />
                                                <asp:Literal ID="lblSoftwarename" runat="server" Text='<%# Eval("Softwarename") %>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ความต้องการอื่นๆ" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Small">

                                            <ItemTemplate>
                                                <asp:Literal ID="lbdetail_more" runat="server" Text='<%# Eval("detail_more") %>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="เหตุผลที่ต้องใช้งานเครื่องแบบพิเศษ" ItemStyle-CssClass="text-left" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Small">

                                            <ItemTemplate>
                                                <asp:Literal ID="lbremark" runat="server" Text='<%# Eval("remark") %>' />

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="bnDeleteListMemo_S" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteListMemo_S"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                            </ItemTemplate>
                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                    </Columns>

                                </asp:GridView>
                            </div>

                        </div>

                        <div class="form-group" runat="server" visible="false" id="div_save_s">
                            <div class="col-sm-2 col-sm-offset-10">
                                <asp:LinkButton ID="LinkButton4" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert_BuySpecial" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                <asp:LinkButton ID="LinkButton5" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewEdit" runat="server">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-edit"></i><strong>&nbsp; แก้ไขข้อมูลรายการ</strong></h3>
                    </div>
                </div>


                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <div class="panel-heading">

                            <asp:FormView ID="FvEditIT" runat="server" DefaultMode="Edit" OnDataBound="FvDetail_DataBound" Width="100%">
                                <EditItemTemplate>

                                    <div class="form-group">
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtm0idx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0idx")%>' />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ระดับการใช้งาน :" />
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txteveldevice" CssClass="form-control" runat="server" Text='<%# Eval("leveldevice") %>'></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txteveldevice" Font-Size="11"
                                                ErrorMessage="กรุณากรอกระดับการใช้งาน"
                                                ValidationExpression="กรุณากรอกระดับการใช้งาน"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                ValidationGroup="Save_edit" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txteveldevice"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group" id="divtypedevice" runat="server">
                                        <asp:Label ID="Label5" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อ :" />
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtname" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("typedevices") %>'></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label7" CssClass="col-sm-3 control-label" runat="server" Text="อายุการใช้งาน :" />
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtyear" Visible="false" CssClass="form-control" runat="server" Text='<%# Eval("use_year") %>'></asp:TextBox>

                                            <asp:DropDownList ID="ddlyearedit" CssClass="form-control" runat="server">
                                                <asp:ListItem Text="กรุณาเลือกอายุการใช้งาน..." Value="0"></asp:ListItem>
                                                <asp:ListItem Text="1 เดือน" Value="1 เดือน"></asp:ListItem>
                                                <asp:ListItem Text="3 เดือน" Value="3 เดือน"></asp:ListItem>
                                                <asp:ListItem Text="6 เดือน" Value="6 เดือน"></asp:ListItem>
                                                <asp:ListItem Text="1 ปี" Value="1 ปี"></asp:ListItem>
                                                <asp:ListItem Text="2 ปี" Value="2 ปี"></asp:ListItem>
                                                <asp:ListItem Text="3 ปี" Value="3 ปี"></asp:ListItem>
                                                <asp:ListItem Text="4 ปี" Value="4 ปี"></asp:ListItem>
                                                <asp:ListItem Text="5 ปี" Value="5 ปี"></asp:ListItem>
                                                <asp:ListItem Text="ถาวร" Value="ถาวร"></asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RqddlEquipmentEditDeviceCode1" ValidationGroup="Save_close" runat="server" Display="None"
                                                ControlToValidate="ddlyearedit" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกอายุการใช้งาน"
                                                ValidationExpression="กรุณาเลือกอายุการใช้งาน" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqddlEquipmentEditDeviceCode1" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ราคา :" />
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtprice" CssClass="form-control" runat="server" Text='<%# Eval("price") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="Save_close" runat="server" Display="None"
                                                ControlToValidate="txtprice" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวน" />
                                            <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="Save_close" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtprice"
                                                ValidationExpression="^[0-9]{1,10}$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group" id="divedit_maprice" runat="server" visible="false">
                                        <asp:Label ID="Label18" CssClass="col-sm-3 control-label" runat="server" Text="ราคา MA ต่อปี :" />
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtmaprice" CssClass="form-control" runat="server" Text='<%# Eval("ma_price") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Save_close" runat="server" Display="None"
                                                ControlToValidate="txtprice" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวน" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ValidationGroup="Save_close" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtprice"
                                                ValidationExpression="^[0-9]{1,10}$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label26" runat="server" Text="รายละเอียด :" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtdetail" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("detail")%>' />

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtdetail" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายละเอียด"
                                                ValidationExpression="กรุณากรอกรายละเอียด"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                                ValidationGroup="Save_edit" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtdetail"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />


                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label13" runat="server" Text="หมายเหตุ :" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-8">
                                            <asp:TextBox ID="txtremark" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("remark")%>' />

                                        </div>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtremark" Font-Size="11"
                                            ErrorMessage="กรุณากรอกหมายเหตุ"
                                            ValidationExpression="กรุณากรอกหมายเหตุ"
                                            SetFocusOnError="true" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                                            ValidationGroup="Save_edit" Display="None"
                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                            ControlToValidate="txtremark"
                                            ValidationExpression="^[\s\S]{0,1000}$"
                                            SetFocusOnError="true" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-3">
                                            <asp:LinkButton ID="btnshowper" CssClass="btn btn-primary btn-sm" runat="server" OnCommand="btnCommand" CommandName="Cmdshowper">เพิ่มสิทธิ์ <i class="glyphicon glyphicon-user"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnhideper" Visible="false" CssClass="btn btn-danger btn-sm" runat="server" OnCommand="btnCommand" CommandName="Cmdhideper">ปิด <i class="fa fa-ban"></i></asp:LinkButton>

                                        </div>
                                    </div>


                                    <asp:Panel ID="Panel_Addper" runat="server" Visible="false">

                                        <div class="form-group">
                                            <asp:Label ID="Label22" runat="server" Text="ระบุสิทธิ์การใช้งาน" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlperedit" ValidationGroup="Save_editper" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Value="0" Text="กรุณาเลือกสิทธิ์การใช้งาน"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="ระบุสิทธิ์"></asp:ListItem>
                                                    <asp:ListItem Value="2" Text="ไม่ระบุสิทธิ์"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ValidationGroup="Save_editper" runat="server" Display="None"
                                                    ControlToValidate="ddlperedit" Font-Size="11"
                                                    ErrorMessage="กรุณาสิทธิ์การใช้งาน"
                                                    ValidationExpression="กรุณาสิทธิ์การใช้งาน" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender25" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="160" />

                                            </div>
                                        </div>

                                        <div id="div_showperedit" runat="server" visible="false">
                                            <div class="form-group">
                                                <asp:Label ID="Label59" runat="server" Text="องค์กร" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlOrg_edit" ValidationGroup="Save_editper" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="Save_editper" runat="server" Display="None"
                                                        ControlToValidate="ddlOrg_edit" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกองค์กร"
                                                        ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />

                                                </div>
                                                <asp:Label ID="Label4" runat="server" Text="ฝ่าย" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlDep_edit" ValidationGroup="Save_editper" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Value="0">กรุณาเลือกฝ่าย...</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="Save_editper" runat="server" Display="None"
                                                        ControlToValidate="ddlDep_edit" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกฝ่าย"
                                                        ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label19" runat="server" Text="แผนก" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlSec_edit" ValidationGroup="Save_editper" AutoPostBack="true" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">กรุณาเลือกแผนก...</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ValidationGroup="Save_editper" runat="server" Display="None"
                                                        ControlToValidate="ddlSec_edit" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกแผนก"
                                                        ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator13" Width="160" />

                                                </div>

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label21" runat="server" Text="ตำแหน่ง" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlpos_edit" runat="server" ValidationGroup="Save_editper" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกตำแหน่ง...</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="Save_editper" runat="server" Display="None"
                                                    ControlToValidate="ddlpos_edit" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกตำแหน่ง"
                                                    ValidationExpression="กรุณาเลือกตำแหน่ง" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator14" Width="160" />

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="LinkButton1" CssClass="btn btn-warning btn-sm" runat="server" OnCommand="btnCommand" Text="ADD +" ValidationGroup="Save_editper" CommandName="CmdAddPerEdit"></asp:LinkButton>

                                            </div>
                                        </div>


                                        <div class="form-group">


                                            <div class="col-sm-6 col-sm-offset-3">
                                                <asp:GridView ID="GvReportAdd_edit"
                                                    runat="server"
                                                    CssClass="table table-striped table-responsive"
                                                    GridLines="None"
                                                    OnRowCommand="onRowCommand"
                                                    Visible="false"
                                                    AutoGenerateColumns="false">


                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>

                                                    <Columns>


                                                        <asp:BoundField DataField="OrgName" HeaderText="องค์กร" ItemStyle-CssClass="text-center" ItemStyle-Width="5%"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="OrgIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />




                                                        <asp:BoundField DataField="DeptName" HeaderText="ฝ่าย" ItemStyle-CssClass="text-center" ItemStyle-Width="5%"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="RdeptIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />




                                                        <asp:BoundField DataField="SecName" HeaderText="แผนก" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="SecIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />



                                                        <asp:BoundField DataField="PosName" HeaderText="ตำแหน่ง" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                        <asp:BoundField DataField="PosIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />


                                                        <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteEditList"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                            </ItemTemplate>

                                                            <EditItemTemplate />
                                                            <FooterTemplate />
                                                        </asp:TemplateField>

                                                    </Columns>

                                                </asp:GridView>
                                            </div>

                                        </div>


                                    </asp:Panel>

                                    <div class="form-group">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-sm-8">
                                            <asp:GridView ID="Gvperdevices_edit" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                HeaderStyle-CssClass="info small"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="false"
                                                DataKeyNames="m1idx"
                                                OnRowDataBound="Master_RowDataBound">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="องค์กร" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Literal ID="lblorgidx" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Literal ID="lblrdepidx" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="แผนก" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Literal ID="lblrsecidx" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblposidx" runat="server" Text='<%# Eval("pos_name") %>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Management" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDelPer" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m1idx")%>'><i class="fa fa-trash"></i></asp:LinkButton>
                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>



                                    <asp:UpdatePanel ID="updatepanel" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-10">
                                                    <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" OnCommand="btnCommand" ValidationGroup="Save_edit" CommandName="CmdUpdate" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lbCmdCancel" class="btn btn-default btn-sm" data-toggle="tooltip" title="Close" runat="server" OnCommand="btnCommand" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>

                                                </div>
                                            </div>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbCmdUpdate" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                </EditItemTemplate>
                            </asp:FormView>
                        </div>

                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewAdd" runat="server">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Adding Equipment</strong></h3>
                    </div>
                    <asp:FormView ID="fv_insert" runat="server" DefaultMode="Insert" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <asp:UpdatePanel ID="update_test" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <asp:Label ID="Label3" class="col-sm-2 control-label" runat="server" Text="หน่วยงาน : " />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlsystem" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" class="form-control" runat="server">
                                                        <asp:ListItem Value="0" Text="กรุณาเลือกหน่วยงานที่ต้องการ..."></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidddator8" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddlsystem" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกหน่วยงานที่ต้องการ"
                                                        ValidationExpression="กรุณาเลือกหน่วยงานที่ต้องการ" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidddator8" Width="160" />

                                                </div>

                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlsystem" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <asp:Label ID="Label10" class="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddltypedevice" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" class="form-control" runat="server">
                                                        <asp:ListItem Text="กรุณาเลือกประเภทอุปกรณ์..." Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddltypedevice" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกประเภทอุปกรณ์"
                                                        ValidationExpression="กรุณาเลือกประเภทอุปกรณ์" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />

                                                </div>
                                                <div id="divdevice_it" runat="server" visible="false">
                                                    <asp:Label ID="Label17" class="col-sm-3 control-label" runat="server" Text="กลุ่มอุปกรณ์ : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddldevices" class="form-control" runat="server">
                                                            <asp:ListItem Value="0" Text="กรุณาเลือกข้อมูล..."></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddltypedevice" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <div class="form-group">

                                        <asp:Label ID="Label12" class="col-sm-2 control-label" runat="server" Text="ระดับการใช้งาน : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtlevel" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <asp:Label ID="Label2" class="col-sm-3 control-label" runat="server" Text="อายุการใช้งาน : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlyear" CssClass="form-control" runat="server">
                                                <asp:ListItem Text="กรุณาเลือกอายุการใช้งาน..." Value="0"></asp:ListItem>
                                                <asp:ListItem Text="1 เดือน" Value="1 เดือน"></asp:ListItem>
                                                <asp:ListItem Text="3 เดือน" Value="3 เดือน"></asp:ListItem>
                                                <asp:ListItem Text="6 เดือน" Value="6 เดือน"></asp:ListItem>
                                                <asp:ListItem Text="1 ปี" Value="1 ปี"></asp:ListItem>
                                                <asp:ListItem Text="2 ปี" Value="2 ปี"></asp:ListItem>
                                                <asp:ListItem Text="3 ปี" Value="3 ปี"></asp:ListItem>
                                                <asp:ListItem Text="4 ปี" Value="4 ปี"></asp:ListItem>
                                                <asp:ListItem Text="5 ปี" Value="5 ปี"></asp:ListItem>
                                                <asp:ListItem Text="ถาวร" Value="ถาวร"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddlyear" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกอายุการใช้งาน"
                                                ValidationExpression="กรุณาเลือกอายุการใช้งาน" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="Label11" class="col-sm-2 control-label" runat="server" Text="ราคา : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtprice" class="form-control" runat="server" />
                                            <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="txtprice" Font-Size="11"
                                                ErrorMessage="กรุณากรอกจำนวน" />
                                            <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="Save" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txtprice"
                                                ValidationExpression="^[0-9]{1,10}$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />

                                        </div>

                                        <div id="divma_price" runat="server" visible="false">
                                            <asp:Label ID="Label16" class="col-sm-3 control-label" runat="server" Text="ค่า MA ต่อปี : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtmaprice" class="form-control" runat="server" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="txtmaprice" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกจำนวน" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ValidationGroup="Save" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                    ControlToValidate="txtmaprice"
                                                    ValidationExpression="^[0-9]{1,10}$" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator6" Width="160" />

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label14" class="col-sm-2 control-label" runat="server" Text="รายละเอียด : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtdetail" TextMode="MultiLine" Rows="3" class="form-control" runat="server" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtdetail" Font-Size="11"
                                                ErrorMessage="กรุณากรอกรายละเอียด"
                                                ValidationExpression="กรุณากรอกรายละเอียด"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtdetail"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                        </div>
                                        <asp:Label ID="Label15" class="col-sm-3 control-label" runat="server" Text="หมายเหตุ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtremark" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtremark" Font-Size="11"
                                                ErrorMessage="กรุณากรอกหมายเหตุ"
                                                ValidationExpression="กรุณากรอกหมายเหตุ"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                ValidationGroup="Save" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />


                                        </div>
                                    </div>

                                    <hr />

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <asp:Label ID="Label20" runat="server" Text="ระบุสิทธิ์การใช้งาน" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlpermission" ValidationGroup="SaveDataSet" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Value="0" Text="กรุณาเลือกสิทธิ์การใช้งาน"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="ระบุสิทธิ์"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="ไม่ระบุสิทธิ์"></asp:ListItem>
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                        ControlToValidate="ddlpermission" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกสิทธิ์การใช้งาน"
                                                        ValidationExpression="กรุณาเลือกสิทธิ์การใช้งาน" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                                </div>

                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlpermission" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <div id="divpermission" runat="server" visible="false">
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <asp:Label ID="Label59" runat="server" Text="องค์กร" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlOrg" ValidationGroup="SaveDataSet" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                            ControlToValidate="ddlOrg" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกองค์กร"
                                                            ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />

                                                    </div>
                                                    <asp:Label ID="Label4" runat="server" Text="ฝ่าย" CssClass="col-sm-3 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlDep" ValidationGroup="SaveDataSet" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">กรุณาเลือกฝ่าย...</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                            ControlToValidate="ddlDep" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกฝ่าย"
                                                            ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label19" runat="server" Text="แผนก" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlSec" ValidationGroup="SaveDataSet" AutoPostBack="true" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0">กรุณาเลือกแผนก...</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                            ControlToValidate="ddlSec" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกแผนก"
                                                            ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator13" Width="160" />

                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlpermission" EventName="SelectedIndexChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="ddlOrg" EventName="SelectedIndexChanged" />
                                                <asp:AsyncPostBackTrigger ControlID="ddlDep" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label21" runat="server" Text="ตำแหน่ง" CssClass="col-sm-2 control-label"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlpos" runat="server" ValidationGroup="SaveDataSet" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกตำแหน่ง...</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="SaveDataSet" runat="server" Display="None"
                                                ControlToValidate="ddlpos" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกตำแหน่ง"
                                                ValidationExpression="กรุณาเลือกตำแหน่ง" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator14" Width="160" />

                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="btnAdddataset" CssClass="btn btn-primary btn-sm" runat="server" Text="ADD +" ValidationGroup="SaveDataSet" OnCommand="btnCommand" CommandName="CmdAdddataset"></asp:LinkButton>
                                        </div>


                                    </div>

                                    <div class="form-group">


                                        <div class="col-lg-6">
                                            <asp:GridView ID="GvReportAdd"
                                                runat="server"
                                                CssClass="table table-striped table-responsive"
                                                GridLines="None"
                                                OnRowCommand="onRowCommand"
                                                Visible="false"
                                                AutoGenerateColumns="false">


                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>


                                                    <asp:BoundField DataField="OrgName" HeaderText="องค์กร" ItemStyle-CssClass="text-center" ItemStyle-Width="5%"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="OrgIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />




                                                    <asp:BoundField DataField="DeptName" HeaderText="ฝ่าย" ItemStyle-CssClass="text-center" ItemStyle-Width="5%"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="RdeptIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />




                                                    <asp:BoundField DataField="SecName" HeaderText="แผนก" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="SecIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />



                                                    <asp:BoundField DataField="PosName" HeaderText="ตำแหน่ง" ItemStyle-CssClass="text-center" ItemStyle-Width="10%"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                    <asp:BoundField DataField="PosIDX" Visible="false" ItemStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />


                                                    <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteList"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>
                                        </div>

                                    </div>


                                    <asp:UpdatePanel ID="UpdatePnFileUpload" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <asp:Label ID="Label6" CssClass="col-sm-2 control-label" runat="server" Text="แนบไฟล์ : " />

                                                <div class="col-md-6">
                                                    <asp:FileUpload ID="UploadFile" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                        CssClass="btn btn-sm btn-warning  multi max-1 " accept="jpg" />
                                                    <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpg</font></p>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbladd" />
                                        </Triggers>
                                    </asp:UpdatePanel>



                                    <div class="form-group" id="divbtnsave" runat="server" visible="false">
                                        <div class="col-sm-2 col-sm-offset-2">
                                            <asp:LinkButton ID="lbladd" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>

            </div>
        </asp:View>

        <asp:View ID="ViewHistory" runat="server">

            <div class="col-lg-12">
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <asp:UpdatePanel ID="upActor1Node1Files" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="GvHistory" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="info small"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    PageSize="10"
                                    OnRowDataBound="Master_RowDataBound"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    DataKeyNames="u0_meidx">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="ลำดับที่" HeaderStyle-CssClass="text-center" ItemStyle-Width="5" ItemStyle-HorizontalAlign="center">

                                            <ItemTemplate>
                                                <%# (Container.DataItemIndex +1) %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รหัสเอกสาร" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbldoccode" runat="server" Text='<%# Eval("doccode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลอ้างอิง" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <strong>
                                                    <asp:Label ID="Label15" runat="server">ประเภท: </asp:Label>
                                                    <asp:Literal ID="litm0_tmidx" Visible="false" runat="server" Text='<%# Eval("m0_tmidx") %>' />
                                                    <asp:Label ID="lbltype_memo" runat="server" Text='<%# Eval("type_memo") %>' /></strong>
                                                </p>
                                                 <strong>
                                                     <asp:Label ID="Label24" runat="server">ขอซื้อ: </asp:Label>
                                                     <asp:Literal ID="lim0_type_idx" Visible="false" runat="server" Text='<%# Eval("m0_type_idx") %>' />
                                                     <asp:Label ID="lbltype_buy" runat="server" Text='<%# Eval("name_its_type") %>' /></strong>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลผู้สร้าง" ItemStyle-Width="20" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <%-- <small>--%>
                                                <strong>
                                                    <asp:Label ID="Labeld15" runat="server">องค์กร: </asp:Label></strong>
                                                <asp:Literal ID="Literael3" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel24" runat="server">ฝ่าย: </asp:Label></strong>
                                                <asp:Literal ID="Litereal1" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel25" runat="server">แผนก: </asp:Label></strong>
                                                <asp:Literal ID="Liteeral2" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                                </p>
                                                 <strong>
                                                     <asp:Label ID="Labeeel26" runat="server">ชื่อ-สกุล: </asp:Label></strong>
                                                <asp:Literal ID="Literael4" runat="server" Text='<%# Eval("FullNameTH") %>' />
                                                </p>
                                        <%--</small>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="วันที่สร้างรายการ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbname" runat="server" Text='<%# Eval("DateStart") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>

                                                <asp:Label ID="lblunidx" Visible="false" runat="server" Text='<%# Eval("unidx") %>'></asp:Label>
                                                <asp:Label ID="lblstaidx" Visible="false" runat="server" Text='<%# Eval("staidx") %>'></asp:Label>
                                                <b>
                                                    <asp:Label ID="lbStatusDoc" runat="server" Text='<%# Eval("StatusDoc") %>'></asp:Label></b>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btndetail" CssClass="btn btn-primary btn-sm" runat="server" OnCommand="btnCommand" CommandArgument='<%#  Eval("u0_meidx") + ";" +  Eval("CempIDX") + ";" +  Eval("m0_type_idx") + ";" +  Eval("m0_tmidx") + ";" + "1" + ";" +  Eval("unidx") + ";" +  Eval("staidx")+ ";" +  Eval("acidx") %>' CommandName="CmdDetail"><i class="far fa-file"></i></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>

                        </asp:UpdatePanel>

                    </div>
                </div>
            </div>





        </asp:View>

        <asp:View ID="ViewDetail_History" runat="server">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="FvUserRequest" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>


                </div>
            </div>

            <asp:UpdatePanel ID="Upd3ateaP2anel1" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="col-lg-12">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-laptop"></i><strong>&nbsp; ข้อมูลราคาอ้างอิงแนบ memo</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="alert-message alert-message-success">
                                        <blockquote class="danger" style="font-size: small; background-color: chocolate; text-align: left">
                                            <h4 style="color: whitesmoke"><b>ข้อมูลอ้างอิงระบบขอซื้อ</b></h4>
                                        </blockquote>

                                    </div>

                                    <asp:FormView ID="Fvdetail_Memo" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                        <ItemTemplate>
                                            <div class="panel-body">
                                                <div class="form-horizontal" role="form">

                                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                                    <div class="form-group">
                                                        <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("unidx") %>' />
                                                        <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("acidx") %>' />
                                                        <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("staidx") %>' />

                                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสเอกสาร : " />
                                                        <div class="col-sm-4">
                                                            <asp:Label ID="txtdoccode" class="col-sm-3 control-label" Text='<%# Eval("doccode") %>' runat="server"></asp:Label>
                                                            <asp:Label ID="txtrsecidx" Visible="false" class="col-sm-3 control-label" Text='<%# Eval("rsecidx") %>' runat="server"></asp:Label>
                                                            <asp:Label ID="txtrdeptidx" Visible="false" class="col-sm-3 control-label" Text='<%# Eval("rdeptidx") %>' runat="server"></asp:Label>
                                                        </div>

                                                        <asp:Label ID="Label9" CssClass="col-sm-2 control-label" runat="server" Text="วันที่สร้าง : " />
                                                        <div class="col-sm-4">
                                                            <asp:Label ID="txttype_buy" class="col-sm-3 control-label" Text='<%# Eval("DateStart") %>' runat="server"></asp:Label>
                                                        </div>

                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label42" CssClass="col-sm-2 control-label" runat="server" Text="ประเภท : " />
                                                        <div class="col-sm-4">
                                                            <asp:Label ID="Label43" class="col-sm-3 control-label" Text='<%# Eval("type_memo") %>' runat="server"></asp:Label>
                                                        </div>

                                                        <asp:Label ID="Label44" CssClass="col-sm-2 control-label" runat="server" Text="ขอซื้อ : " />
                                                        <div class="col-sm-4">
                                                            <asp:Label ID="Label45" class="col-sm-3 control-label" Text='<%# Eval("name_its_type") %>' runat="server"></asp:Label>
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:FormView>


                                    <asp:GridView ID="GvDetail_BuyNormal" runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                        HeaderStyle-CssClass="success small"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="false"
                                        OnRowDataBound="Master_RowDataBound"
                                        DataKeyNames="u1_meidx_n">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="ลำดับที่" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center">

                                                <ItemTemplate>
                                                    <%# (Container.DataItemIndex +1) %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ข้อมูลฝ่ายขอราคา" ItemStyle-VerticalAlign="Top" ItemStyle-Width="35%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <strong>
                                                            <asp:Label ID="Labeld15" runat="server">องค์กร: </asp:Label></strong>
                                                        <asp:Literal ID="Literael3" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel24" runat="server">ฝ่าย: </asp:Label></strong>
                                                        <asp:Literal ID="Litereal1" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel25" runat="server">แผนก: </asp:Label></strong>
                                                        <asp:Literal ID="Liteeral2" runat="server" Text='<%# Eval("SecNameTH") %>' />

                                                        </p>
                                                         <div id="div_fullname" runat="server" visible="false">
                                                             <strong>
                                                                 <asp:Label ID="Labeeel26" runat="server">ชื่อ-สกุล: </asp:Label></strong>
                                                             <asp:Literal ID="Literael4" runat="server" Text='<%# Eval("FullNameTH") %>' />
                                                             </p>
                                                             <strong>
                                                                 <asp:Label ID="Label39" runat="server">ทดแทนเครื่องถือครอง: </asp:Label></strong>
                                                             <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("u0_code") %>' />
                                                             </p>
                                                         </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="ข้อมูลอ้างอิงราคา" ItemStyle-VerticalAlign="Top" ItemStyle-Width="35%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <strong>
                                                            <asp:Label ID="Labeld15_lv" runat="server">ระดับ: </asp:Label></strong>
                                                        <asp:Literal ID="lblevel" runat="server" Text='<%# Eval("leveldevice") %>' />
                                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel24_ty" runat="server">ประเภท: </asp:Label></strong>
                                                        <asp:Literal ID="lbtypedevice" runat="server" Text='<%# Eval("typedevices") %>' />
                                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel25_sp" runat="server">สเปค: </asp:Label></strong>
                                                        <asp:Literal ID="lbdetail" runat="server" Text='<%# Eval("detail") %>' />

                                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Labeeel26_pr" runat="server">ราคา: </asp:Label></strong>
                                                        <asp:Label ID="lbprice" Visible="false" runat="server" Text='<%# Eval("price") %>'></asp:Label>
                                                        <asp:Label ID="lbprice_show" runat="server"></asp:Label>
                                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Label35_po" runat="server">ตำแหน่งที่ใช้: </asp:Label></strong>
                                                        <asp:Label ID="lbposname" runat="server" Text='<%# Eval("pos_name") %>'></asp:Label>
                                                        </p>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ข้อมูลอุปกรณ์เสริม" ItemStyle-Width="25%" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <strong>
                                                            <asp:Label ID="Labeld15_mo" runat="server">หน้าจอ: </asp:Label></strong>
                                                        <asp:Literal ID="lbtype_monitor" runat="server" Text='<%# Eval("typedevices_monitor") %>' />
                                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel24_so" runat="server">โปรแกรม: </asp:Label></strong>
                                                        <asp:Literal ID="lbsoftwarename" runat="server" Text='<%# Eval("Softwarename") %>' />
                                                        </p>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>


                                    <asp:GridView ID="GvDetail_BuySpecial" runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                        HeaderStyle-CssClass="success small"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="false"
                                        OnRowDataBound="Master_RowDataBound"
                                        DataKeyNames="u1_meidx_s">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="ลำดับที่" HeaderStyle-CssClass="text-center" ItemStyle-Width="3%" ItemStyle-HorizontalAlign="center">

                                                <ItemTemplate>
                                                    <%# (Container.DataItemIndex +1) %>
                                                    <asp:Label ID="lblu1_meidx_s" runat="server" Visible="false" Text='<%# Eval("u1_meidx_s") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ข้อมูลฝ่ายขอราคา" ItemStyle-VerticalAlign="Top" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <strong>
                                                            <asp:Label ID="Labeld15" runat="server">องค์กร: </asp:Label></strong>
                                                        <asp:Literal ID="Literael3" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel24" runat="server">ฝ่าย: </asp:Label></strong>
                                                        <asp:Literal ID="Litereal1" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel25" runat="server">แผนก: </asp:Label></strong>
                                                        <asp:Literal ID="Liteeral2" runat="server" Text='<%# Eval("SecNameTH") %>' />

                                                        </p>
                                                         <strong>
                                                             <asp:Label ID="Label47" runat="server">ตำแหน่ง: </asp:Label></strong>
                                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("pos_name") %>' />

                                                        </p>
                                                         <div id="div_fullname" runat="server" visible="false">
                                                             <strong>
                                                                 <asp:Label ID="Labeeel26" runat="server">ชื่อ-สกุล: </asp:Label></strong>
                                                             <asp:Literal ID="Literael4" runat="server" Text='<%# Eval("FullNameTH") %>' />
                                                             </p>
                                                             <strong>
                                                                 <asp:Label ID="Label39" runat="server">ทดแทนเครื่องถือครอง: </asp:Label></strong>
                                                             <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("u0_code") %>' />
                                                             </p>
                                                         </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ข้อมูลอ้างอิง" ItemStyle-VerticalAlign="Top" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>

                                                        <strong>
                                                            <asp:Label ID="Labsel47" runat="server">ประเภทเครื่อง: </asp:Label></strong>
                                                        <asp:Literal ID="Litersal1" runat="server" Text='<%# Eval("name_gen") %>' />

                                                        </p>

                                                         <strong>
                                                             <asp:Label ID="Label48" runat="server">รายละเอียดเพิ่มเติม: </asp:Label></strong>
                                                        <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("detail") %>' />

                                                        </p>

                                                         <strong>
                                                             <asp:Label ID="Label49" runat="server">สาเหตุที่ต้องซื้อพิเศษ: </asp:Label></strong>
                                                        <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("remark") %>' />

                                                        </p>

                                                        <asp:GridView ID="GvDevice_Special" runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                            HeaderStyle-CssClass="primary"
                                                            HeaderStyle-Height="40px"
                                                            AllowPaging="false"
                                                            DataKeyNames="m2_tyidx"
                                                            OnRowDataBound="Master_RowDataBound">

                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="#">

                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblm0_naidx" runat="server" Visible="false" Text='<%# Eval("m0_naidx") %>' />
                                                                        <asp:Label ID="lblm2_tyidx" runat="server" Visible="false" Text='<%# Eval("m2_tyidx") %>' />
                                                                        <%# (Container.DataItemIndex +1) %>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ประเภทอุปกรณ์" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbltypedevice" runat="server" Text='<%# Eval("type_name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="อุปกรณ์" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbldevice" runat="server" Text='<%# Eval("device_name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>

                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="โปรแกรม" ItemStyle-Width="15%" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>

                                                        <asp:Literal ID="lbsoftwarename" runat="server" Text='<%# Eval("Softwarename") %>' />
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สถานะและความคิดเห็น" ItemStyle-Width="20%" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:GridView ID="GvComment_Special" runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                            HeaderStyle-CssClass="info"
                                                            HeaderStyle-Height="40px"
                                                            AllowPaging="false"
                                                            DataKeyNames="u1_meidx_s"
                                                            OnRowDataBound="Master_RowDataBound">

                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>


                                                                <asp:TemplateField HeaderText="ความคิดเห็น Director ผู้สร้าง" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <p>
                                                                                <strong>
                                                                                    <asp:Label ID="Labsel47" runat="server">สถานะ: </asp:Label></strong>
                                                                                <asp:Label ID="listatus_dir" runat="server" Text='<%# Eval("status_dir") %>' />
                                                                                <asp:Literal ID="listaidx_dir" runat="server" Visible="false" Text='<%# Eval("staidx_dir") %>' />

                                                                            </p>
                                                                            <p>
                                                                                <strong>
                                                                                    <asp:Label ID="Label48" runat="server">ความคิดเห้น: </asp:Label></strong>
                                                                                <asp:Literal ID="licomment_dir" runat="server" Text='<%# Eval("comment_dir") %>' />

                                                                            </p>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ความคิดเห็นเจ้าหน้าที่ IT" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <p>
                                                                                <strong>
                                                                                    <asp:Label ID="Labssel47" runat="server">สถานะ: </asp:Label></strong>
                                                                                <asp:Label ID="litstatus_it" runat="server" Text='<%# Eval("status_it") %>' />
                                                                                <asp:Literal ID="litstaidx_it" runat="server" Visible="false" Text='<%# Eval("staidx_it") %>' />

                                                                            </p>
                                                                            <p>
                                                                                <strong>
                                                                                    <asp:Label ID="Labedl48" runat="server">ความคิดเห้น: </asp:Label></strong>
                                                                                <asp:Literal ID="litcomment_it" runat="server" Text='<%# Eval("comment_it") %>' />

                                                                            </p>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ความคิดเห็น Manager IT" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <p>
                                                                                <strong>
                                                                                    <asp:Label ID="Labseal47" runat="server">สถานะ: </asp:Label></strong>
                                                                                <asp:Label ID="listatus_headerit" runat="server" Text='<%# Eval("status_headerit") %>' />
                                                                                <asp:Literal ID="listaidx_headerit" runat="server" Visible="false" Text='<%# Eval("staidx_headerit") %>' />

                                                                            </p>
                                                                            <p>
                                                                                <strong>
                                                                                    <asp:Label ID="Lawbel48" runat="server">ความคิดเห้น: </asp:Label></strong>
                                                                                <asp:Literal ID="licomment_headerit" runat="server" Text='<%# Eval("comment_headerit") %>' />

                                                                            </p>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>


                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="พิจารณาผล" ItemStyle-Width="25%" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                <HeaderTemplate>
                                                    <div class="col-md-10 col-md-offset-1">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>พิจารณาผล</label>
                                                                <asp:UpdatePanel ID="UpdatePanel_Header" runat="server">
                                                                    <ContentTemplate>
                                                                        <small>
                                                                            <asp:DropDownList ID="ddl_choose" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" Font-Size="Small">
                                                                                <asp:ListItem Value="0">เลือกทีละรายการ</asp:ListItem>
                                                                                <asp:ListItem Value="1">อนุมัติทุกรายการ</asp:ListItem>
                                                                                <asp:ListItem Value="2">ไม่อนุมัติทุกรายการ</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </small>

                                                                    </ContentTemplate>
                                                                </asp:UpdatePanel>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <small>
                                                        <div id="div_rdochoose" runat="server">
                                                            <asp:RadioButtonList ID="rdochoose" AutoPostBack="true" OnCheckedChanged="ddlSelectedIndexChanged" runat="server">
                                                                <asp:ListItem Value="2">อนุมัติ</asp:ListItem>
                                                                <asp:ListItem Value="3">ไม่อนุมัติ</asp:ListItem>
                                                            </asp:RadioButtonList>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                                                ValidationGroup="SaveApprove" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                                Display="None" SetFocusOnError="true" ControlToValidate="rdochoose" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160"
                                                                PopupPosition="BottomLeft" />
                                                            <p>
                                                                <asp:Label ID="Label7" CssClass="control-label" runat="server" Text="ความคิดเห็นต่อรายการ : " />
                                                            </p>

                                                            <asp:TextBox ID="txtcomment" TextMode="MultiLine" Rows="3" CssClass="form-control" runat="server"></asp:TextBox>
                                                        </div>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>



                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-10">
                                            <asp:LinkButton ID="btnprint_normal" CssClass="btn btn-warning btn-sm" runat="server" ToolTip="Print" OnClientClick="return printDiv('printable_buynormal');" CommandName="CmdPrint"><i class="fa fa-print"></i> Print</asp:LinkButton>
                                            <asp:LinkButton ID="btnprint_special" CssClass="btn btn-warning btn-sm" runat="server" ToolTip="Print" OnClientClick="return printDiv('printable_buyspecial');" CommandName="CmdPrint"><i class="fa fa-print"></i> Print</asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" CssClass="btn btn-primary btn-sm" runat="server" OnCommand="btnCommand" CommandName="btnCancel_memo" data-toggle="tooltip" title="Back"><i class="fa fa-reply"></i></asp:LinkButton>

                                        </div>
                                    </div>




                                </div>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>

            </asp:UpdatePanel>

            <asp:Panel ID="panel_approve" runat="server" Visible="false">
                <div class="col-lg-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; ผลการอนุมัติ</strong></h3>
                        </div>

                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">

                                    <asp:Label ID="Label33" CssClass="col-sm-2 control-label text_right" runat="server" Text="สถานะอนุมัติ" />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddl_approve" AutoPostBack="true" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="0">เลือกสถานะอนุมัติ</asp:ListItem>
                                            <asp:ListItem Value="2">อนุมัติ</asp:ListItem>
                                            <asp:ListItem Value="3">ไม่อนุมัติ </asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ValidationGroup="SaveApprove" runat="server" Display="None"
                                            ControlToValidate="ddl_approve" Font-Size="11"
                                            ErrorMessage="เลือกสถานะอนุมัติ"
                                            ValidationExpression="เลือกสถานะอนุมัติ" InitialValue="0" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="160" />

                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label26" runat="server" Text="ความคิดเห็นเพิ่มเติม" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="txtremark_approve" TextMode="multiline" Width="510pt" Rows="5" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveApprove" runat="server" Display="None" ControlToValidate="txtremark_approve" Font-Size="11"
                                            ErrorMessage="กรุณากรอกความคิดเห็น"
                                            ValidationExpression="กรุณากรอกความคิดเห็น"
                                            SetFocusOnError="true" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                            ValidationGroup="SaveApprove" Display="None"
                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                            ControlToValidate="txtremark_approve"
                                            ValidationExpression="^[\s\S]{0,1000}$"
                                            SetFocusOnError="true" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorsCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                    </div>


                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-2">
                                        <asp:LinkButton ID="LinkButton3" ValidationGroup="SaveApprove" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdApprove" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                        <asp:LinkButton ID="LinkButton6" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel_memo" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </asp:Panel>

            <div class="col-lg-12">

                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ประวัติการอนุมัติ</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <asp:Repeater ID="rpLog" runat="server">
                                <HeaderTemplate>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"><small>วัน / เวลา</small></label>
                                        <label class="col-sm-3 control-label"><small>ผู้ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>ผลการดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>ความคิดเห็น</small></label>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <%-- <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small><%#Eval("createdate")%></small></span>--%>
                                            <div class="col-sm-2">
                                            </div>
                                            <div class="col-sm-8">
                                                <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small><%#Eval("CreateDate")%></small></span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <span>&nbsp;&nbsp;&nbsp;<small><%# Eval("FullNameTH") %>&nbsp;&nbsp;&nbsp;(<%# Eval("actor_name") %>)</small></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><small><%# Eval("node_name") %></small></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><small>&nbsp;&nbsp;&nbsp;<%# Eval("status_name") %></small></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><small><%# Eval("comment") %></small></span>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>


            <asp:Panel ID="box_print_officer" runat="server" Visible="true">
                <asp:FormView ID="FvTemplate_print" runat="server" HorizontalAlign="Center" DefaultMode="ReadOnly" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <div id="printable_buynormal" class="print_report hidden">
                            <div class="panel panel-default">

                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 14px">

                                    <h3 style="text-align: center;">เอกสารที่ใช้ในการแนบซื้ออุปกรณ์สารสนเทศ</h3>
                                </table>


                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 14px">
                                    <tr>
                                        <div class="form-horizontal">
                                            <div class="col-lg-10">
                                                <td style="text-align: center; vertical-align: middle; width: 200px;"><b>รหัสเอกสาร </td>
                                                <td style="text-align: left; vertical-align: middle; width: 400px;">
                                                    <asp:Label ID="Label23" runat="server" Text='<%# Eval("doccode") %>'></asp:Label></td>

                                            </div>
                                            <div class="col-lg-2">
                                                <td rowspan="4">
                                                    <asp:Image ID="imgprint" runat="server" Width="80%" ImageUrl="~/images/price-reference/logo_print.png" />
                                                </td>
                                            </div>
                                        </div>
                                    </tr>

                                    <tr>
                                        <div class="col-lg-10">
                                            <td style="text-align: center; vertical-align: middle;"><b>วันที่ทำรายการ </td>
                                            <td style="text-align: left; vertical-align: middle;">
                                                <asp:Label ID="Label25" runat="server" Text='<%# Eval("DateStart") %>'></asp:Label></td>
                                        </div>
                                    </tr>
                                    <tr>
                                        <div class="col-lg-10">
                                            <td style="text-align: center; vertical-align: middle;"><b>ประเภทรายการ </td>
                                            <td style="text-align: left; vertical-align: middle;">
                                                <asp:Label ID="Label65" runat="server" Text='<%# Eval("type_memo") %>'></asp:Label></td>
                                        </div>
                                    </tr>
                                    <tr>
                                        <div class="col-lg-10">
                                            <td style="text-align: center; vertical-align: middle;"><b>ประเภทขอซื้อ </td>
                                            <td style="text-align: left; vertical-align: middle;">
                                                <asp:Label ID="Label40" runat="server" Text='<%# Eval("name_its_type") %>'></asp:Label></td>
                                        </div>
                                    </tr>
                                    <tr>
                                        <div class="col-lg-10">
                                            <td style="text-align: center; vertical-align: middle;">
                                                <h4 style="text-align: left;">ข้อมูลผู้ทำรายการ</h4>
                                        </div>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;"><b>ชื่อ-นามสกุล </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <asp:Label ID="Label27" runat="server" Text='<%# Eval("FullNameTH") %>'></asp:Label></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;"><b>องค์กร </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <asp:Label ID="Label28" runat="server" Text='<%# Eval("OrgNameTH") %>'></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;"><b>ฝ่าย </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <asp:Label ID="Label29" runat="server" Text='<%# Eval("DeptNameTH") %>'></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;"><b>แผนก </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <asp:Label ID="Label30" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;"><b>ตำแหน่ง </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <asp:Label ID="Label33" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:Label></td>
                                    </tr>
                                </table>

                                <br />

                                <asp:GridView ID="GvDetail_Print_BuyNormal" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="info small"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="false"
                                    OnRowDataBound="Master_RowDataBound"
                                    DataKeyNames="u1_meidx_n">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="ลำดับที่" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center">

                                            <ItemTemplate>
                                                <%# (Container.DataItemIndex +1) %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลฝ่ายขอราคา" ItemStyle-VerticalAlign="Top" ItemStyle-Width="35%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Labeld15" runat="server">องค์กร: </asp:Label></strong>
                                                    <asp:Literal ID="Literael3" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel24" runat="server">ฝ่าย: </asp:Label></strong>
                                                    <asp:Literal ID="Litereal1" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel25" runat="server">แผนก: </asp:Label></strong>
                                                    <asp:Literal ID="Liteeral2" runat="server" Text='<%# Eval("SecNameTH") %>' />

                                                    </p>
                                                    <div id="div_fullname" runat="server" visible="false">
                                                        <strong>
                                                            <asp:Label ID="Labeeel26" runat="server">ชื่อ-สกุล: </asp:Label></strong>
                                                        <asp:Literal ID="Literael4" runat="server" Text='<%# Eval("FullNameTH") %>' />
                                                        </p>
                                                         <strong>
                                                             <asp:Label ID="Label41" runat="server">ทดแทนเครื่องถือครอง: </asp:Label></strong>
                                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("u0_code") %>' />
                                                        </p>
                                                    </div>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ข้อมูลอ้างอิงราคา" ItemStyle-VerticalAlign="Top" ItemStyle-Width="35%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Labeld15_lv" runat="server">ระดับ: </asp:Label></strong>
                                                    <asp:Literal ID="lblevel" runat="server" Text='<%# Eval("leveldevice") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel24_ty" runat="server">ประเภท: </asp:Label></strong>
                                                    <asp:Literal ID="lbtypedevice" runat="server" Text='<%# Eval("typedevices") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel25_sp" runat="server">สเปค: </asp:Label></strong>
                                                    <asp:Literal ID="lbdetail" runat="server" Text='<%# Eval("detail") %>' />

                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labeeel26_pr" runat="server">ราคา: </asp:Label></strong>
                                                    <asp:Label ID="lbprice" Visible="false" runat="server" Text='<%# Eval("price") %>'></asp:Label>
                                                    <asp:Label ID="lbprice_show" runat="server"></asp:Label>
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Label35_po" runat="server">ตำแหน่งที่ใช้: </asp:Label></strong>
                                                    <asp:Label ID="lbposname" runat="server" Text='<%# Eval("pos_name") %>'></asp:Label>
                                                    </p>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลอุปกรณ์เสริม" ItemStyle-Width="25%" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Labeld15_mo" runat="server">หน้าจอ: </asp:Label></strong>
                                                    <asp:Literal ID="lbtype_monitor" runat="server" Text='<%# Eval("typedevices_monitor") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel24_so" runat="server">โปรแกรม: </asp:Label></strong>
                                                    <asp:Literal ID="lbsoftwarename" runat="server" Text='<%# Eval("Softwarename") %>' />
                                                    </p>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>

                            </div>
                        </div>

                        <div id="printable_buyspecial" class="print_report hidden">
                            <div class="panel panel-default">

                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 14px">
                                    <h3 style="text-align: center;">เอกสารที่ใช้ในการแนบซื้ออุปกรณ์สารสนเทศ</h3>
                                </table>


                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 14px">
                                    <tr>
                                        <div class="form-horizontal">
                                            <div class="col-lg-10">
                                                <td style="text-align: center; vertical-align: middle; width: 200px;"><b>รหัสเอกสาร </td>
                                                <td style="text-align: left; vertical-align: middle; width: 400px;">
                                                    <asp:Label ID="Label50" runat="server" Text='<%# Eval("doccode") %>'></asp:Label></td>

                                            </div>
                                            <div class="col-lg-2">
                                                <td rowspan="4">
                                                    <asp:Image ID="Image1" runat="server" Width="80%" ImageUrl="~/images/price-reference/logo_print.png" />
                                                </td>
                                            </div>
                                        </div>
                                    </tr>

                                    <tr>
                                        <div class="col-lg-10">
                                            <td style="text-align: center; vertical-align: middle;"><b>วันที่ทำรายการ </td>
                                            <td style="text-align: left; vertical-align: middle;">
                                                <asp:Label ID="Label51" runat="server" Text='<%# Eval("DateStart") %>'></asp:Label></td>
                                        </div>
                                    </tr>
                                    <tr>
                                        <div class="col-lg-10">
                                            <td style="text-align: center; vertical-align: middle;"><b>ประเภทรายการ </td>
                                            <td style="text-align: left; vertical-align: middle;">
                                                <asp:Label ID="Label52" runat="server" Text='<%# Eval("type_memo") %>'></asp:Label></td>
                                        </div>
                                    </tr>
                                    <tr>
                                        <div class="col-lg-10">
                                            <td style="text-align: center; vertical-align: middle;"><b>ประเภทขอซื้อ </td>
                                            <td style="text-align: left; vertical-align: middle;">
                                                <asp:Label ID="Label64" runat="server" Text='<%# Eval("name_its_type") %>'></asp:Label></td>
                                        </div>
                                    </tr>
                                    <tr>
                                        <div class="col-lg-10">
                                            <td style="text-align: center; vertical-align: middle;">
                                                <h4 style="text-align: left;">ข้อมูลผู้ทำรายการ</h4>
                                        </div>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;"><b>ชื่อ-นามสกุล </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <asp:Label ID="Label53" runat="server" Text='<%# Eval("FullNameTH") %>'></asp:Label></td>
                                    </tr>

                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;"><b>องค์กร </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <asp:Label ID="Label54" runat="server" Text='<%# Eval("OrgNameTH") %>'></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;"><b>ฝ่าย </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <asp:Label ID="Label55" runat="server" Text='<%# Eval("DeptNameTH") %>'></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;"><b>แผนก </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <asp:Label ID="Label56" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;"><b>ตำแหน่ง </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <asp:Label ID="Label57" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;"><b>Comment ผู้อนุมัติ </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;"><b>ผู้อนุมัติ </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <asp:Label ID="Label58" runat="server" Text='<%# Eval("comment_dir") %>'></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;"><b>เจ้าหน้าที่ IT </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <asp:Label ID="Label62" runat="server" Text='<%# Eval("comment_it") %>'></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center; vertical-align: middle;"><b>Manager IT </td>
                                        <td style="text-align: left; vertical-align: middle;">
                                            <asp:Label ID="Label63" runat="server" Text='<%# Eval("comment_headerit") %>'></asp:Label></td>
                                    </tr>
                                </table>

                                <br />

                                <asp:GridView ID="GvDetail_Print_BuySpecial" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="success small"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="false"
                                    OnRowDataBound="Master_RowDataBound"
                                    DataKeyNames="u1_meidx_s">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="ลำดับที่" HeaderStyle-CssClass="text-center" ItemStyle-Width="3%" ItemStyle-HorizontalAlign="center">

                                            <ItemTemplate>
                                                <%# (Container.DataItemIndex +1) %>
                                                <asp:Label ID="lblu1_meidx_s" runat="server" Visible="false" Text='<%# Eval("u1_meidx_s") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลฝ่ายขอราคา" ItemStyle-VerticalAlign="Top" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Labeld15" runat="server">องค์กร: </asp:Label></strong>
                                                    <asp:Literal ID="Literael3" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel24" runat="server">ฝ่าย: </asp:Label></strong>
                                                    <asp:Literal ID="Litereal1" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                    </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel25" runat="server">แผนก: </asp:Label></strong>
                                                    <asp:Literal ID="Liteeral2" runat="server" Text='<%# Eval("SecNameTH") %>' />

                                                    </p>
                                                         <strong>
                                                             <asp:Label ID="Label47" runat="server">ตำแหน่ง: </asp:Label></strong>
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("pos_name") %>' />

                                                    </p>
                                                         <div id="div_fullname" runat="server" visible="false">
                                                             <strong>
                                                                 <asp:Label ID="Labeeel26" runat="server">ชื่อ-สกุล: </asp:Label></strong>
                                                             <asp:Literal ID="Literael4" runat="server" Text='<%# Eval("FullNameTH") %>' />
                                                             </p>
                                                             <strong>
                                                                 <asp:Label ID="Label39" runat="server">ทดแทนเครื่องถือครอง: </asp:Label></strong>
                                                             <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("u0_code") %>' />
                                                             </p>
                                                         </div>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลอ้างอิง" ItemStyle-VerticalAlign="Top" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>

                                                    <strong>
                                                        <asp:Label ID="Labsel47" runat="server">ประเภทเครื่อง: </asp:Label></strong>
                                                    <asp:Literal ID="Litersal1" runat="server" Text='<%# Eval("name_gen") %>' />

                                                    </p>

                                                         <strong>
                                                             <asp:Label ID="Label48" runat="server">รายละเอียดเพิ่มเติม: </asp:Label></strong>
                                                    <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("detail") %>' />

                                                    </p>

                                                         <strong>
                                                             <asp:Label ID="Label49" runat="server">สาเหตุที่ต้องซื้อพิเศษ: </asp:Label></strong>
                                                    <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("remark") %>' />

                                                    </p>

                                                        <asp:GridView ID="GvDevice_Print_Special" runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                            HeaderStyle-CssClass="primary"
                                                            HeaderStyle-Height="40px"
                                                            AllowPaging="false"
                                                            DataKeyNames="m2_tyidx"
                                                            OnRowDataBound="Master_RowDataBound">

                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="#">

                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblm0_naidx" runat="server" Visible="false" Text='<%# Eval("m0_naidx") %>' />
                                                                        <asp:Label ID="lblm2_tyidx" runat="server" Visible="false" Text='<%# Eval("m2_tyidx") %>' />
                                                                        <%# (Container.DataItemIndex +1) %>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ประเภทอุปกรณ์" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbltypedevice" runat="server" Text='<%# Eval("type_name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="อุปกรณ์" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbldevice" runat="server" Text='<%# Eval("device_name") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>

                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="โปรแกรม" ItemStyle-Width="15%" ItemStyle-VerticalAlign="Top" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <small>

                                                    <asp:Literal ID="lbsoftwarename" runat="server" Text='<%# Eval("Softwarename") %>' />
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>
            </asp:Panel>


        </asp:View>

        <asp:View ID="ViewApprove" runat="server">
            <div class="col-lg-12">
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="GvApprove" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="info small"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    PageSize="10"
                                    OnRowDataBound="Master_RowDataBound"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    DataKeyNames="u0_meidx">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField HeaderText="ลำดับที่" HeaderStyle-CssClass="text-center" ItemStyle-Width="5" ItemStyle-HorizontalAlign="center">

                                            <ItemTemplate>
                                                <%# (Container.DataItemIndex +1) %>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รหัสเอกสาร" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbldoccode" runat="server" Text='<%# Eval("doccode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลอ้างอิง" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <strong>
                                                    <asp:Label ID="Label15" runat="server">ประเภท: </asp:Label>
                                                    <asp:Literal ID="litm0_tmidx" Visible="false" runat="server" Text='<%# Eval("m0_tmidx") %>' />
                                                    <asp:Label ID="lbltype_memo" runat="server" Text='<%# Eval("type_memo") %>' /></strong>
                                                </p>
                                                 <strong>
                                                     <asp:Label ID="Label24" runat="server">ขอซื้อ: </asp:Label>
                                                     <asp:Literal ID="lim0_type_idx" Visible="false" runat="server" Text='<%# Eval("m0_type_idx") %>' />
                                                     <asp:Label ID="lbltype_buy" runat="server" Text='<%# Eval("name_its_type") %>' /></strong>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ข้อมูลผู้สร้าง" ItemStyle-Width="20" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <strong>
                                                    <asp:Label ID="Labeld15" runat="server">องค์กร: </asp:Label></strong>
                                                <asp:Literal ID="Literael3" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel24" runat="server">ฝ่าย: </asp:Label></strong>
                                                <asp:Literal ID="Litereal1" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                </p>
                                                 <strong>
                                                     <asp:Label ID="Labeel25" runat="server">แผนก: </asp:Label></strong>
                                                <asp:Literal ID="Liteeral2" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                                </p>
                                                 <strong>
                                                     <asp:Label ID="Labeeel26" runat="server">ชื่อ-สกุล: </asp:Label></strong>
                                                <asp:Literal ID="Literael4" runat="server" Text='<%# Eval("FullNameTH") %>' />
                                                </p>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="วันที่สร้างรายการ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lbname" runat="server" Text='<%# Eval("DateStart") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:Label ID="lblunidx" Visible="false" runat="server" Text='<%# Eval("unidx") %>'></asp:Label>
                                                <asp:Label ID="lblstaidx" Visible="false" runat="server" Text='<%# Eval("staidx") %>'></asp:Label>
                                                <b>
                                                    <asp:Label ID="lbStatusDoc" runat="server" Text='<%# Eval("StatusDoc") %>'></asp:Label></b>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btndetail" CssClass="btn btn-primary btn-sm" runat="server" OnCommand="btnCommand" CommandArgument='<%#  Eval("u0_meidx") + ";" +  Eval("CempIDX") + ";" +  Eval("m0_type_idx") + ";" +  Eval("m0_tmidx") + ";" + "2" + ";" +  Eval("unidx") + ";" +  Eval("staidx")+ ";" +  Eval("acidx")%>' CommandName="CmdDetail"><i class="far fa-file"></i></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>

                        </asp:UpdatePanel>

                    </div>
                </div>
            </div>

        </asp:View>
    </asp:MultiView>
</asp:Content>

