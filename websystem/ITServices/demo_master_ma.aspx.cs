﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Net;
using System.Drawing;
using System.Text;

using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Helpers;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using DotNet.Highcharts.Enums;


public partial class websystem_ITServices_demo_master_ma : System.Web.UI.Page
{


    #region initial function/data
    service_mail servicemail = new service_mail();
    function_tool _funcTool = new function_tool();

    data_master_ma _data_master_ma = new data_master_ma();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlbind_data = _serviceUrl + ConfigurationManager.AppSettings["urlbindMasterMA"];
    static string _urlSelectData = _serviceUrl + ConfigurationManager.AppSettings["urlselectMasterMA"];
    static string _urlUpdateData = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateMasterMA"];
    static string _urlDeleteData = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteMasterMA"];
    static string _urlSelectComID = _serviceUrl + ConfigurationManager.AppSettings["urlSelectComID"];
    static string _urlSelectAssetID = _serviceUrl + ConfigurationManager.AppSettings["urlSelectAssetID"];
    static string _urlSelectEmpID = _serviceUrl + ConfigurationManager.AppSettings["urlSelectEmpID"];
    static string _urlSelectEmpName = _serviceUrl + ConfigurationManager.AppSettings["urlSelectEmpName"];
    static string _urlInsertData = _serviceUrl + ConfigurationManager.AppSettings["urlinsertMasterMA"];
    static string _urlSelectReport = _serviceUrl + ConfigurationManager.AppSettings["urlSelectReport"];
    static string _urlMoreThan = _serviceUrl + ConfigurationManager.AppSettings["urlMoreThan"];
    static string _urlLessThan = _serviceUrl + ConfigurationManager.AppSettings["urlLessThan"];

    string _localJson = "";
    int _tempInt = 0;

    

    DataTable dt = new DataTable();
    bool flag = false;

    #endregion initial function/data


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
            actionIndex();
            btn_home.BackColor = System.Drawing.Color.LightGray;
        }
    }

   

    #region Selected 
    protected void actionIndex()
    {

        data_master_ma _data_master_ma = new data_master_ma();
        _data_master_ma.acction_bind_data = new m0_bind_data[1];

        m0_bind_data _m0_bing_data = new m0_bind_data();
        _m0_bing_data.log_id = 0;

        _data_master_ma.acction_bind_data[0] = _m0_bing_data;


        _data_master_ma = callServiceMasterData(_urlbind_data,_data_master_ma);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_master_ma));
       
        setGridData(GviewIndex,_data_master_ma.acction_bind_data);


    }

   

    
    #endregion Selected 

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        //--Select command button
        switch(cmdName)
        {
            case "btn_edit":
                try //edit data go View Edit
                {
                    MvMaster.SetActiveView(ViewEdit);

                    data_master_ma _data_master_ma = new data_master_ma();
                    _data_master_ma.acction_select_edit = new m0_select_data_edit[1];

                    m0_select_data_edit _m0_select_edit = new m0_select_data_edit();
                    _m0_select_edit.log_idEdit = Convert.ToInt32(cmdArg);

                    _data_master_ma.acction_select_edit[0] = _m0_select_edit;

                    _data_master_ma = callServiceMasterData(_urlSelectData, _data_master_ma);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_master_ma));
                    ((TextBox)ViewEdit.FindControl("lbMaster_id")).Text = Convert.ToString(_data_master_ma.acction_select_edit[0].log_id);
                    ((TextBox)ViewEdit.FindControl("txtEmp_id")).Text = Convert.ToString(_data_master_ma.acction_select_edit[0].emp_id);
                    ((TextBox)ViewEdit.FindControl("txtEmp_name")).Text = _data_master_ma.acction_select_edit[0].emp_name;
                    ((DropDownList)ViewEdit.FindControl("ddlDepartment")).SelectedValue = Convert.ToString(_data_master_ma.acction_select_edit[0].department_id);
                    ((TextBox)ViewEdit.FindControl("txtComID")).Text = _data_master_ma.acction_select_edit[0].com_id;
                    ((TextBox)ViewEdit.FindControl("txtAssetCode")).Text = _data_master_ma.acction_select_edit[0].asset_code;
                    ((TextBox)ViewEdit.FindControl("txtDetail")).Text = _data_master_ma.acction_select_edit[0].detail;
                    ((DropDownList)ViewEdit.FindControl("ddlStatus")).SelectedValue = _data_master_ma.acction_select_edit[0].status_ma;
                }
                catch(Exception ex)
                {
                    litDebug.Text = "error :" + ex;
                }
               break;

            case "btn_save":
                //MvMaster.SetActiveView(ViewIndex);
                try //update data
                {
                    data_master_ma _data_master_ma = new data_master_ma();
                    _data_master_ma.acction_update_data = new m0_update_data[1];

                    m0_update_data _m0_update_data = new m0_update_data();
                    _m0_update_data.log_index = int.Parse(((TextBox)ViewEdit.FindControl("lbMaster_id")).Text.Trim());

                    _m0_update_data.up_emp_id = int.Parse(((TextBox)ViewEdit.FindControl("txtEmp_id")).Text.Trim());
                    _m0_update_data.up_emp_name = ((TextBox)ViewEdit.FindControl("txtEmp_name")).Text.Trim();
                    _m0_update_data.up_department_id = int.Parse(((DropDownList)ViewEdit.FindControl("ddlDepartment")).SelectedValue.ToString());
                    _m0_update_data.up_department_name = ((DropDownList)ViewEdit.FindControl("ddlDepartment")).SelectedItem.Text.Trim();
                    _m0_update_data.up_com_id = ((TextBox)ViewEdit.FindControl("txtComID")).Text.Trim();
                    _m0_update_data.up_asset_code = ((TextBox)ViewEdit.FindControl("txtAssetCode")).Text.Trim();
                    _m0_update_data.up_detail = ((TextBox)ViewEdit.FindControl("txtDetail")).Text.Trim();
                    _m0_update_data.up_status_ma = ((DropDownList)ViewEdit.FindControl("ddlStatus")).SelectedValue.ToString();
                    _m0_update_data.up_support_id = 60003345;

                    _data_master_ma.acction_update_data[0] = _m0_update_data;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_master_ma));
                    _data_master_ma = callServiceMasterData(_urlUpdateData, _data_master_ma);

                    ((TextBox)ViewEdit.FindControl("lbMaster_id")).Text = "";
                    ((TextBox)ViewEdit.FindControl("txtEmp_id")).Text = "";
                    ((TextBox)ViewEdit.FindControl("txtEmp_name")).Text = "";
                    ((DropDownList)ViewEdit.FindControl("ddlDepartment")).SelectedValue = "0";
                    ((TextBox)ViewEdit.FindControl("txtComID")).Text = "";
                    ((TextBox)ViewEdit.FindControl("txtAssetCode")).Text = "";
                    ((TextBox)ViewEdit.FindControl("txtDetail")).Text = "";
                    ((DropDownList)ViewEdit.FindControl("ddlStatus")).SelectedValue = "NULL";


                    if (_data_master_ma.return_code == 0)
                    {
                        ////actionIndex();
                        //MvMaster.SetActiveView(ViewIndex);
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                        btn_home.BackColor = System.Drawing.Color.LightGray;

                    }
                    else
                    {
                        setError(_data_master_ma.return_code.ToString() + " - " + _data_master_ma.return_msg);
                    }
                }
                catch (Exception ex)
                {
                    litDebug.Text = "error :" + ex;
                }
                break;

            case "btn_cancel":

                ((TextBox)ViewEdit.FindControl("lbMaster_id")).Text = "";
                ((TextBox)ViewEdit.FindControl("txtEmp_id")).Text = "";
                ((TextBox)ViewEdit.FindControl("txtEmp_name")).Text = "";
                ((DropDownList)ViewEdit.FindControl("ddlDepartment")).SelectedValue = "0";
                ((TextBox)ViewEdit.FindControl("txtComID")).Text = "";
                ((TextBox)ViewEdit.FindControl("txtAssetCode")).Text = "";
                ((TextBox)ViewEdit.FindControl("txtDetail")).Text = "";
                ((DropDownList)ViewEdit.FindControl("ddlStatus")).SelectedValue = "NULL";
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                btn_home.BackColor = System.Drawing.Color.LightGray;
                break;

            case "btn_add":
               
                MvMaster.SetActiveView(ViewInsert);
                btn_add.BackColor = System.Drawing.Color.LightGray;
                btn_home.BackColor = System.Drawing.Color.WhiteSmoke;
                btn_report.BackColor = System.Drawing.Color.WhiteSmoke;
                PanelInsert.Visible = false;
                break;

            case "btn_insert":

                try
                {
                    int emp_id = Convert.ToInt32(((TextBox)ViewInsert.FindControl("txtAddEmp_id")).Text.Trim());
                    string emp_name = ((TextBox)ViewInsert.FindControl("txtAddEmp_name")).Text.Trim();
                    int department_id = Convert.ToInt32(((DropDownList)ViewInsert.FindControl("ddlAddDepartment")).SelectedValue.ToString());
                    string department_name = ((DropDownList)ViewInsert.FindControl("ddlAddDepartment")).SelectedItem.Text;
                    string com_id = ((TextBox)ViewInsert.FindControl("txtAddComid")).Text.Trim();
                    string asset_code = ((TextBox)ViewInsert.FindControl("txtAddAsset_code")).Text.Trim();
                    string detail = ((TextBox)ViewInsert.FindControl("txtAddDetail")).Text.Trim();
                    string status_ma = ((DropDownList)ViewInsert.FindControl("ddlAddStatus")).SelectedValue.ToString();
                    int support_id = Convert.ToInt32(((TextBox)ViewInsert.FindControl("txtSupport_id")).Text.Trim());

                    data_master_ma _data_master_ma = new data_master_ma();
                    _data_master_ma.acction_insert_data = new m0_insert_data[1];

                    m0_insert_data _m0_insert_data = new m0_insert_data();
                    _m0_insert_data.add_emp_id = emp_id;
                    _m0_insert_data.add_emp_name = emp_name;
                    _m0_insert_data.add_department_id = department_id;
                    _m0_insert_data.add_department_name = department_name;
                    _m0_insert_data.add_com_id = com_id;
                    _m0_insert_data.add_asset_code = asset_code;
                    _m0_insert_data.add_detail = detail;
                    _m0_insert_data.add_status_ma = status_ma;
                    _m0_insert_data.add_support_id = support_id;

                    _data_master_ma.acction_insert_data[0] = _m0_insert_data;
                    _data_master_ma = callServiceMasterData(_urlInsertData, _data_master_ma);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_master_ma));

                    _data_master_ma.acction_bind_data = new m0_bind_data[1];
                    m0_bind_data _m0_bing_data = new m0_bind_data();
                    _m0_bing_data.log_id = 0;

                    _data_master_ma.acction_bind_data[0] = _m0_bing_data;


                    _data_master_ma = callServiceMasterData(_urlbind_data, _data_master_ma);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_master_ma));

                    setGridData(GvInsert, _data_master_ma.acction_bind_data);
                    PanelInsert.Visible = true;

                    ((TextBox)ViewInsert.FindControl("txtAddEmp_id")).Text = "";
                    ((TextBox)ViewInsert.FindControl("txtAddEmp_name")).Text = "";
                    ((DropDownList)ViewInsert.FindControl("ddlAddDepartment")).SelectedValue = "0";
                    ((TextBox)ViewInsert.FindControl("txtAddComid")).Text = "";
                    ((TextBox)ViewInsert.FindControl("txtAddAsset_code")).Text = "";
                    ((TextBox)ViewInsert.FindControl("txtAddDetail")).Text = "";
                    ((DropDownList)ViewInsert.FindControl("ddlAddStatus")).SelectedValue = "NULL";
                    
                }
                catch (Exception ex)
                {
                    litDebug.Text = "error :" + ex;
                }
                    break;
            case "btn_delete":
                try
                {
                    ((TextBox)ViewEdit.FindControl("lbMaster_id")).Text = "";
                    ((TextBox)ViewEdit.FindControl("txtEmp_id")).Text = "";
                    ((TextBox)ViewEdit.FindControl("txtEmp_name")).Text = "";
                    ((DropDownList)ViewEdit.FindControl("ddlDepartment")).SelectedValue = "0";
                    ((TextBox)ViewEdit.FindControl("txtComID")).Text = "";
                    ((TextBox)ViewEdit.FindControl("txtAssetCode")).Text = "";
                    ((TextBox)ViewEdit.FindControl("txtDetail")).Text = "";
                    ((DropDownList)ViewEdit.FindControl("ddlStatus")).SelectedValue = "NULL";
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    btn_home.BackColor = System.Drawing.Color.LightGray;
                }
                catch(Exception ex)
                {
                    litDebug.Text = "error :" + ex;
                }
            break;
            case "btn_search":
                try
                {
                    if(ddlSearch.SelectedValue=="com_id")
                    {
                        
                        string com_index = ((TextBox)ViewIndex.FindControl("txtSearch")).Text.Trim();
                        data_master_ma _data_master_ma = new data_master_ma();
                        _data_master_ma.acction_select_comid = new m0_select_comid[1];

                        m0_select_comid _m0_select_comid = new m0_select_comid();
                        _m0_select_comid.comid_index = com_index;

                        _data_master_ma.acction_select_comid[0] = _m0_select_comid;


                        _data_master_ma = callServiceMasterData(_urlSelectComID, _data_master_ma);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_master_ma));

                        setGridData(GviewIndex, _data_master_ma.acction_select_comid);
                    }
                    else if(ddlSearch.SelectedValue== "asset_code")
                    {
                        
                        string asset_codeidx = ((TextBox)ViewIndex.FindControl("txtSearch")).Text.Trim();
                        data_master_ma _data_master_ma = new data_master_ma();
                        _data_master_ma.acction_select_asset = new m0_select_asset[1];

                        m0_select_asset _m0_select_asset = new m0_select_asset();
                        _m0_select_asset.asset_codeidx = asset_codeidx;

                        _data_master_ma.acction_select_asset[0] = _m0_select_asset;


                        _data_master_ma = callServiceMasterData(_urlSelectAssetID, _data_master_ma);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_master_ma));

                        setGridData(GviewIndex, _data_master_ma.acction_select_asset);
                    }
                    else if(ddlSearch.SelectedValue== "emp_id")
                    {
                        
                        int emp_index = Convert.ToInt32(((TextBox)ViewIndex.FindControl("txtSearch")).Text.Trim());
                        data_master_ma _data_master_ma = new data_master_ma();
                        _data_master_ma.acction_select_empid = new m0_select_empid[1];

                        m0_select_empid _m0_select_empid = new m0_select_empid();
                        _m0_select_empid.emp_index = emp_index;

                        _data_master_ma.acction_select_empid[0] = _m0_select_empid;


                        _data_master_ma = callServiceMasterData(_urlSelectEmpID, _data_master_ma);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_master_ma));

                        setGridData(GviewIndex, _data_master_ma.acction_select_empid);
                    }
                    else if(ddlSearch.SelectedValue== "name")
                    {
                        
                        string emp_name = ((TextBox)ViewIndex.FindControl("txtSearch")).Text.Trim();
                        data_master_ma _data_master_ma = new data_master_ma();
                        _data_master_ma.acction_select_empname = new m0_select_empname[1];

                        m0_select_empname _m0_select_empname = new m0_select_empname();
                        _m0_select_empname.emp_nameidx = emp_name;

                        _data_master_ma.acction_select_empname[0] = _m0_select_empname;


                        _data_master_ma = callServiceMasterData(_urlSelectEmpName, _data_master_ma);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_master_ma));

                        setGridData(GviewIndex, _data_master_ma.acction_select_empname);
                    }
                    else
                    {
                        GviewIndex.DataSource = null;
                        GviewIndex.DataBind();
                    }

                    ddlSearch.SelectedValue = "NULL";
                    txtSearch.Text = "";
                }
                catch(Exception ex)
                {
                    GviewIndex.DataSource = null;
                    GviewIndex.DataBind();
                }
                
            break;
            case "btn_report":
                MvMaster.SetActiveView(ViewReport);
                initPage();
                btn_report.BackColor = System.Drawing.Color.LightGray;
                btn_home.BackColor = System.Drawing.Color.WhiteSmoke;
                btn_add.BackColor = System.Drawing.Color.WhiteSmoke;
                GviewReport.DataSource = null;
                GviewReport.DataBind();
                lbhearder.Text = "";
                txtDate_End.Visible = false;
                txtDate_Start.Visible = false;
                break;
            case "btn_searchDate":
                try
                {
                    data_master_ma _data_master_ma = new data_master_ma();
                    string start_date = ((TextBox)ViewReport.FindControl("txtDate_Start")).Text;
                    string end_date = ((TextBox)ViewReport.FindControl("txtDate_End")).Text;
                    if (ddldateTime.SelectedValue == "between")
                    {
                        lbhearder.Text = "ข้อมูลรายงานวันที่" + ddldateTime.SelectedItem.Text + txtDate_Start.Text + "-" + txtDate_End.Text;
                        _data_master_ma.acction_select_report = new m0_select_report[1];
                        m0_select_report _m0_select_report = new m0_select_report();

                        _m0_select_report.start_date = start_date;
                        _m0_select_report.end_date = end_date;

                        _data_master_ma.acction_select_report[0] = _m0_select_report;
                        _data_master_ma = callServiceMasterData(_urlSelectReport, _data_master_ma);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_master_ma));

                        setGridData(GviewReport, _data_master_ma.acction_select_report);
                       
                    }
                    else if(ddldateTime.SelectedValue=="more than")
                    {
                        lbhearder.Text = "ข้อมูลรายงานวันที่" + ddldateTime.SelectedItem.Text + " " + txtDate_Start.Text;
                        _data_master_ma.acction_select_report = new m0_select_report[1];
                        m0_select_report _m0_select_report = new m0_select_report();
                        _m0_select_report.start_date = start_date;
                        _data_master_ma.acction_select_report[0] = _m0_select_report;

                        _data_master_ma = callServiceMasterData(_urlMoreThan, _data_master_ma);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_master_ma));

                        setGridData(GviewReport, _data_master_ma.acction_select_report);

                        
                    }
                    else if(ddldateTime.SelectedValue=="less than")
                    {
                        lbhearder.Text = "ข้อมูลรายงานวันที่" + ddldateTime.SelectedItem.Text + " " + txtDate_Start.Text;
                        _data_master_ma.acction_select_report = new m0_select_report[1];
                        m0_select_report _m0_select_report = new m0_select_report();
                        _m0_select_report.start_date = start_date;
                        _data_master_ma.acction_select_report[0] = _m0_select_report;

                        _data_master_ma = callServiceMasterData(_urlLessThan, _data_master_ma);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_master_ma));

                        setGridData(GviewReport, _data_master_ma.acction_select_report);
                        
                    }
                    else
                    {
                        GviewReport.DataSource = null;
                        GviewReport.DataBind();
                    }
                    
                    ((TextBox)ViewReport.FindControl("txtDate_Start")).Text = "";
                    ((TextBox)ViewReport.FindControl("txtDate_End")).Text = "";
                    ((DropDownList)ViewReport.FindControl("ddldateTime")).SelectedValue = "NULL";
                    txtDate_End.Visible = false;
                    txtDate_Start.Visible = false;
                }
                catch(Exception ex)
                {
                    litDebug.Text = "error :" + ex;

                }
                
                break;

            case "btndelete":
                try
                {
                    string status_ma = "OFF";
                    data_master_ma _data_master_ma = new data_master_ma();
                    _data_master_ma.acction_delete_data = new m0_delete_data[1];

                    m0_delete_data _m0_delete_data = new m0_delete_data();
                    _m0_delete_data.ma_index = Convert.ToInt32(cmdArg);

                    _m0_delete_data.ma_status = status_ma;
                    _data_master_ma.acction_delete_data[0] = _m0_delete_data;

                    _data_master_ma = callServiceMasterData(_urlDeleteData, _data_master_ma);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_master_ma));

                    if (_data_master_ma.return_code == 0)
                    {
                        ////actionIndex();
                        //MvMaster.SetActiveView(ViewIndex);
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                        btn_home.BackColor = System.Drawing.Color.LightGray;

                    }
                    else
                    {
                        setError(_data_master_ma.return_code.ToString() + " - " + _data_master_ma.return_msg);
                    }
                }
                catch (Exception ex)
                {
                    litDebug.Text = "error :" + ex;
                }

            break;

            case "btn_home":

                MvMaster.SetActiveView(ViewIndex);
                initPage();
                actionIndex();
                btn_home.BackColor = System.Drawing.Color.LightGray;
                btn_add.BackColor = System.Drawing.Color.WhiteSmoke;
                btn_report.BackColor = System.Drawing.Color.WhiteSmoke;

                break;
            //case "btn_export" :
            
            //    GviewReport.AllowSorting = false;
            //    GviewReport.AllowPaging = false;

            //    Response.Clear();
            //    Response.Buffer = true;
            //    Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
            //    // Response.Charset = ""; set character 
            //    Response.Charset = "utf-8";
            //    Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
            //    Response.ContentType = "application/vnd.ms-excel";

            //    Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            //    Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");


            //    StringWriter sw = new StringWriter();
            //    HtmlTextWriter hw = new HtmlTextWriter(sw);


            //    GviewReport.Columns[0].Visible = true;
            //    GviewReport.HeaderRow.BackColor = Color.White;

            //    foreach (TableCell cell in GviewReport.HeaderRow.Cells)
            //    {
            //        cell.BackColor = GviewReport.HeaderStyle.BackColor;
            //    }

            //    foreach (GridViewRow row in GviewReport.Rows)
            //    {
            //        row.BackColor = Color.White;
            //        foreach (TableCell cell in row.Cells)
            //        {
            //            if (row.RowIndex % 2 == 0)
            //            {
            //                cell.BackColor = GviewReport.AlternatingRowStyle.BackColor;
            //            }
            //            else
            //            {
            //                cell.BackColor = GviewReport.RowStyle.BackColor;
            //            }
            //            cell.CssClass = "textmode";
            //        }
            //    }

            //    GviewReport.RenderControl(hw);

            //    //style to format numbers to string
            //    string style = @"<style> .textmode { mso-number-format:\@; font-family: myFirstFont; } </style>";
            //    Response.Write(style);
            //    Response.Output.Write(sw.ToString());
            //    Response.Flush();
            //    Response.End();
            //break;
        }
    }
    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }
    

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GviewIndex.PageIndex = e.NewPageIndex;
                GviewIndex.DataBind();
                actionIndex();
                break;
        }
    }
    
     public override void VerifyRenderingInServerForm(Control control)
    {

    }



    #endregion bind data

    #region settingFrom
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

    }
    #endregion settingFrom


    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();
    }

    protected void setVisible()
    {

    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected data_master_ma callServiceMasterData(string _cmdUrl, data_master_ma _data_master_ma)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_master_ma);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_master_ma = (data_master_ma)_funcTool.convertJsonToObject(typeof(data_master_ma), _localJson);

        return _data_master_ma;
    }
    #endregion reuse





    protected void ddldateTime_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(ddldateTime.SelectedIndex==0)
        {
            txtDate_End.Visible = false;
            txtDate_Start.Visible = false;
        }
        else if(ddldateTime.SelectedIndex==1)
        {
            txtDate_Start.Visible = true;
            txtDate_End.Visible = false;
        }
        else if(ddldateTime.SelectedIndex==2)
        {
            txtDate_Start.Visible = true;
            txtDate_End.Visible = false;
        }
        else
        {
            txtDate_End.Visible = true;
            txtDate_Start.Visible = true;
        }
    }
}
