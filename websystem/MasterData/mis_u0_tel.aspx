﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="mis_u0_tel.aspx.cs" Inherits="websystem_MasterData_mis_u0_tel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="txt" runat="server"></asp:Literal>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-phone-alt"></i><strong>&nbsp; เบอร์ติดต่อภายใน</strong></h3>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">
                                <asp:LinkButton ID="btnshow" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="AddMasterTel" runat="server" CommandName="CmdAdd" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>


                            <%------------------------ Div ADD  ------------------------%>

                            <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; เพิ่มเบอร์ติดต่อภายใน</strong></h4>
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">
                                            <div class="form-group">
                                                <asp:Label ID="Label26" runat="server" Text="สถานที่" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:DropDownList ID="ddllocname" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidddator8" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddllocname" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกสถานที่"
                                                    ValidationExpression="กรุณาเลือกสถานที่" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidddator8" Width="160" />
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="องค์กร" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:DropDownList ID="ddlorgidx" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddlorgidx" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกองค์กร"
                                                    ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label4" runat="server" Text="ฝ่าย" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:DropDownList ID="ddldeptidx" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="กรุณาเลือกฝ่าย..." Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddldeptidx" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกฝ่าย"
                                                    ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label8" runat="server" Text="แผนก" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:DropDownList ID="ddlsecidx" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="กรุณาเลือกแผนก..." Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddlsecidx" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกแผนก"
                                                    ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label2" runat="server" Text="เบอร์ติดต่อองค์กร" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:DropDownList ID="ddlmaintel" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="กรุณาเลือกเบอร์ติดต่อภายใน..." Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddlmaintel" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกสถานที่"
                                                    ValidationExpression="กรุณาเลือกสถานที่" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="เบอร์ติดต่อภายใน" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:TextBox ID="txttel" MaxLength="15" runat="server" CssClass="form-control" PlaceHolder="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="txttel" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกเบอร์ติดต่อสถานที่"
                                                    ValidationExpression="กรุณากรอกเบอร์ติดต่อสถานที่" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="lbl" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                <div class="col-sm-5">
                                                    <asp:DropDownList ID="ddStatusadd" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="lbladd" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                </div>

                            </asp:Panel>

                            <asp:GridView ID="GvMaster" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="u0telidx"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#">

                                        <ItemTemplate>
                                            <asp:Label ID="lblu0telidx" runat="server" Visible="false" Text='<%# Eval("u0telidx") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>


                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" Text="สถานที่" CssClass="col-sm-3 control-label text_right"></asp:Label>

                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="lblLocIDX" Visible="false" CssClass="form-control" runat="server" Text='<%# Bind("LocIDX") %>' Enabled="false" />
                                                            <asp:DropDownList ID="ddllocname_edit" Enabled="false" runat="server" CssClass="form-control"></asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldVaslidddator8" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="ddllocname_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกสถานที่"
                                                            ValidationExpression="กรุณาเลือกสถานที่" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldVaslidddator8" Width="160" />

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label5" runat="server" Text="องค์กร" CssClass="col-sm-3 control-label text_right"></asp:Label>

                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txtorgidx" Visible="false" CssClass="form-control" runat="server" Text='<%# Bind("OrgIDX") %>' Enabled="false" />
                                                            <asp:DropDownList ID="ddlorgidx_edit" Enabled="false" runat="server" CssClass="form-control"></asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="ddlorgidx_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกองค์กร"
                                                            ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label6" runat="server" Text="ฝ่าย" CssClass="col-sm-3 control-label text_right"></asp:Label>

                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txtrdeptidx" Visible="false" CssClass="form-control" runat="server" Text='<%# Bind("RDeptIDX") %>' Enabled="false" />
                                                            <asp:DropDownList ID="ddlrdeptidx_edit" Enabled="false" runat="server" CssClass="form-control"></asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="ddlrdeptidx_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกฝ่าย"
                                                            ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />

                                                    </div>

                                                     <div class="form-group">
                                                        <asp:Label ID="Label9" runat="server" Text="แผนก" CssClass="col-sm-3 control-label text_right"></asp:Label>

                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txtrsectidx" Visible="false" CssClass="form-control" runat="server" Text='<%# Bind("RSecIDX") %>' Enabled="false" />
                                                            <asp:DropDownList ID="ddlrsecidx_edit" Enabled="false" runat="server" CssClass="form-control"></asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="ddlrsecidx_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกแผนก"
                                                            ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="เบอร์ติดต่อ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txtm0telidx_edit" MaxLength="15" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("m0telidx")%>' />
                                                            <asp:DropDownList ID="ddltelidx_edit" Enabled="false" runat="server" CssClass="form-control"></asp:DropDownList>
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="ddltelidx_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกเบอร์ติดต่อองค์กร"
                                                            ValidationExpression="กรุณาเลือกเบอร์ติดต่อองค์กร" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label7" runat="server" Text="เบอร์ติดต่อภายในองค์กร" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txtu0idx" MaxLength="15" runat="server" CssClass="form-control" Text='<%# Eval("u0_tel")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="txtu0idx" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกจำนวน" />
                                                        <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="Save" Display="None"
                                                            ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                            ControlToValidate="txtu0idx"
                                                            ValidationExpression="^[0-9]{1,10}$" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                        <div class="col-sm-5">
                                                            <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("u0status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="สถานที่" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbLocName" runat="server" Text='<%# Eval("LocName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="องค์กร" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbOrgNameTH" runat="server" Text='<%# Eval("OrgNameTH") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDeptName" runat="server" Text='<%# Eval("DeptName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="แผนก" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbSecName" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="เบอร์ติดต่อภายในองค์กร" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbPhoneNumber" runat="server" Text='<%# Eval("PhoneNumber") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstatus" runat="server" Text='<%# getStatus((int)Eval("u0status")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("u0telidx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

