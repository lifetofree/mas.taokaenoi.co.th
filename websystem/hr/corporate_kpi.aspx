﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="corporate_kpi.aspx.cs" Inherits="websystem_hr_corporate_kpi" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>


    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">

                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbCorparateKPIs" runat="server" CommandName="cmdCorparateKPIs" OnCommand="navCommand" CommandArgument="docCorparateKPIs"> Corporate KPIs</asp:LinkButton>
                        </li>

                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbDetailKPIs" runat="server" CommandName="cmdDetailKPIs" OnCommand="navCommand" CommandArgument="docDetailKPIs"> TQA</asp:LinkButton>
                        </li>

                    </ul>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->

            </div>
        </nav>
    </div>
    <!--tab menu-->

    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <asp:Literal ID="litDebug1" runat="server"></asp:Literal>
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">

        <!--View CorparateKPIs-->
        <asp:View ID="docCorparateKPIs" runat="server">
            <div class="col-md-12">

                <div id="GvCorporateKpi_scroll" style="overflow-x: auto; width: 100%" runat="server">
                    <asp:GridView ID="GvDetailCorporateKpi" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        ShowHeaderWhenEmpty="False"
                        AllowPaging="False"
                        PageSize="5"
                        BorderStyle="None"
                        CellSpacing="2">
                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="No" Visible="false" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>

                                    <%# (Container.DataItemIndex +1) %>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="KPI" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" ItemStyle-CssClass="left">
                                <ItemTemplate>
                                    <p>
                                        <asp:Label ID="lbl_u0_detail_idx" Visible="false" runat="server" Text='<%# Eval("u0_detail_idx") %>' />
                                        <asp:Label ID="lbl_details_title" runat="server" Text='<%# Eval("details_title") %>' />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Unit" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="left">
                                <ItemTemplate>
                                    <p>
                                        <asp:Label ID="lbl_unit_name" runat="server" Text='<%# Eval("unit_name") %>' />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Weight" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <p>
                                        <asp:Label ID="lbl_weight" runat="server" Text='<%# Eval("weight") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="type" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <p>
                                        <b>Target</b>
                                        <asp:Label ID="lbl_target" runat="server" />
                                    </p>
                                    <p>
                                        <b>Actual</b>
                                        <asp:Label ID="lbl_actual" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Annual Target" Visible="false" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <p>
                                        <asp:Label ID="lbl_annual_target" Visible="true" runat="server" Text='<%# Eval("annual_target") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="JAN" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_jan" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_jan" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_jan" Visible="true" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="FEB" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_feb" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_feb" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_feb" Visible="true" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="MAR" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_mar" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_mar" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_mar" Visible="true" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="APR" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_apr" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_apr" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_apr" Visible="true" runat="server" />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="MAY" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_may" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_may" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_may" Visible="true" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="JUN" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_jun" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_jun" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_jun" Visible="true" runat="server" />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="JUL" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_jul" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_jul" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_jul" Visible="true" runat="server" />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="AUG" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_aug" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_aug" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_aug" Visible="true" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="SEP" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_sep" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_sep" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_sep" Visible="true" runat="server" />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="OCT" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_oct" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_oct" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_oct" Visible="true" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="NOV" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_nov" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_nov" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_nov" Visible="true" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="DEC" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_dec" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_dec" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_dec" Visible="true" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                    </asp:GridView>
                </div>

                <br />
                <div class="form-group">
                    <ul class="nav nav-tabs bg-default">

                        <li id="liInsertCorporateKPI" runat="server">
                            <asp:LinkButton ID="lbInsertCorporateKPI" runat="server" CommandName="cmdInsertCorporateKPI" OnCommand="btnCommand" CommandArgument="1"><b> กรอก CorporateKPI</b></asp:LinkButton>
                        </li>

                        <%--<li id="liInsertTQA" runat="server">
                            <asp:LinkButton ID="lbInsertTQA" runat="server" CommandName="cmdInsertTQA" OnCommand="btnCommand" CommandArgument="2"><b> TQA</b></asp:LinkButton>
                        </li>--%>
                    </ul>
                </div>

                <!-- Form Detail Employee Create -->
                <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>

                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:FormView>
                <!-- Form Detail Employee Create -->

                <!-- Detail Create -->
                <asp:UpdatePanel ID="Pn_DetailInsertCorporateKpi" runat="server">
                    <ContentTemplate>

                        <div id="GvInsertCorporateKpi_scroll" style="overflow-x: auto; width: 100%" runat="server">
                            <asp:GridView ID="GvInsertCorporateKpi" runat="server"
                                AutoGenerateColumns="False"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                OnPageIndexChanging="gvPageIndexChanging"
                                OnRowDataBound="gvRowDataBound"
                                ShowHeaderWhenEmpty="False"
                                AllowPaging="False"
                                PageSize="5"
                                BorderStyle="None"
                                CellSpacing="2">
                                <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="No" Visible="false" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>

                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="KPI" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" ItemStyle-CssClass="left">
                                        <ItemTemplate>
                                            <p>
                                                <asp:Label ID="lbl_u0_detail_idx" Visible="false" runat="server" Text='<%# Eval("u0_detail_idx") %>' />
                                                <asp:Label ID="lbl_details_title" runat="server" Text='<%# Eval("details_title") %>' />
                                            </p>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Unit" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="left">
                                        <ItemTemplate>
                                            <p>
                                                <asp:Label ID="lbl_m0_unit_type_idx" Visible="false" runat="server" Text='<%# Eval("m0_unit_type_idx") %>' />
                                                <asp:Label ID="lbl_unit_name" runat="server" Text='<%# Eval("unit_name") %>' />
                                            </p>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Weight" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <p>
                                                <asp:Label ID="lbl_weight" runat="server" Text='<%# Eval("weight") %>' />
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="type" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <p>
                                                <b>Target</b>
                                                <asp:Label ID="lbl_target" runat="server" />
                                            </p>
                                            <p>
                                                <b>Actual</b>
                                                <asp:Label ID="lbl_actual" runat="server" />
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Annual Target" Visible="false" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <p>
                                                <asp:Label ID="lbl_annual_target" Visible="true" runat="server" Text='<%# Eval("annual_target") %>' />
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="JAN" Visible="false" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txt_jan_month" runat="server" Visible="false" CssClass="form-control"></asp:TextBox>
                                            <asp:Label ID="lbl_set_month_jan" Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lbl_u2_idx_jan" runat="server"></asp:Label>

                                            <p>
                                                <asp:TextBox ID="txt_jan_target" runat="server" CssClass="form-control" placeholder="Target ..."></asp:TextBox>
                                            </p>
                                            <p>
                                                <asp:TextBox ID="txt_jan_actual" runat="server" CssClass="form-control" placeholder="Actual ..."></asp:TextBox>
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="FEB" Visible="false" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_set_month_feb" Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lbl_u2_idx_feb" Visible="false" runat="server"></asp:Label>
                                            <p>
                                                <asp:TextBox ID="txt_feb_target" runat="server" CssClass="form-control" placeholder="Target ..."></asp:TextBox>
                                            </p>
                                            <p>
                                                <asp:TextBox ID="txt_feb_actual" runat="server" CssClass="form-control" placeholder="Actual ..."></asp:TextBox>
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="MAR" Visible="false" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_set_month_mar" Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lbl_u2_idx_mar" Visible="false" runat="server"></asp:Label>
                                            <p>
                                                <asp:TextBox ID="txt_mar_target" runat="server" CssClass="form-control" placeholder="Target ..."></asp:TextBox>
                                            </p>
                                            <p>
                                                <asp:TextBox ID="txt_mar_actual" runat="server" CssClass="form-control" placeholder="Actual ..."></asp:TextBox>
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="APR" Visible="false" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_set_month_apr" Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lbl_u2_idx_apr" Visible="false" runat="server"></asp:Label>
                                            <p>
                                                <asp:TextBox ID="txt_apr_target" runat="server" CssClass="form-control" placeholder="Target ..."></asp:TextBox>
                                            </p>
                                            <p>
                                                <asp:TextBox ID="txt_apr_actual" runat="server" CssClass="form-control" placeholder="Actual ..."></asp:TextBox>
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="MAY" Visible="false" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_set_month_may" Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lbl_u2_idx_may" Visible="false" runat="server"></asp:Label>
                                            <p>
                                                <asp:TextBox ID="txt_may_target" runat="server" CssClass="form-control" placeholder="Target ..."></asp:TextBox>
                                            </p>
                                            <p>
                                                <asp:TextBox ID="txt_may_actual" runat="server" CssClass="form-control" placeholder="Actual ..."></asp:TextBox>
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="JUN" Visible="false" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_set_month_jun" Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lbl_u2_idx_jun" Visible="false" runat="server"></asp:Label>
                                            <p>
                                                <asp:TextBox ID="txt_jun_target" runat="server" CssClass="form-control" placeholder="Target ..."></asp:TextBox>
                                            </p>
                                            <p>
                                                <asp:TextBox ID="txt_jun_actual" runat="server" CssClass="form-control" placeholder="Actual ..."></asp:TextBox>
                                            </p>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="JUL" Visible="false" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_set_month_jul" Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lbl_u2_idx_jul" Visible="false" runat="server"></asp:Label>
                                            <p>
                                                <asp:TextBox ID="txt_jul_target" runat="server" CssClass="form-control" placeholder="Target ..."></asp:TextBox>
                                            </p>
                                            <p>
                                                <asp:TextBox ID="txt_jul_actual" runat="server" CssClass="form-control" placeholder="Actual ..."></asp:TextBox>
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="AUG" Visible="false" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_set_month_aug" Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lbl_u2_idx_aug" Visible="false" runat="server"></asp:Label>
                                            <p>
                                                <asp:TextBox ID="txt_aug_target" runat="server" CssClass="form-control" placeholder="Target ..."></asp:TextBox>
                                            </p>
                                            <p>
                                                <asp:TextBox ID="txt_aug_actual" runat="server" CssClass="form-control" placeholder="Actual ..."></asp:TextBox>
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="SEP" Visible="false" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_set_month_sep" Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lbl_u2_idx_sep" Visible="false" runat="server"></asp:Label>
                                            <p>
                                                <asp:TextBox ID="txt_sep_target" runat="server" CssClass="form-control" placeholder="Target ..."></asp:TextBox>
                                            </p>
                                            <p>
                                                <asp:TextBox ID="txt_sep_actual" runat="server" CssClass="form-control" placeholder="Actual ..."></asp:TextBox>
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="OCT" Visible="false" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_set_month_oct" Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lbl_u2_idx_oct" Visible="false" runat="server"></asp:Label>
                                            <p>
                                                <asp:TextBox ID="txt_oct_target" runat="server" CssClass="form-control" placeholder="Target ..."></asp:TextBox>
                                            </p>
                                            <p>
                                                <asp:TextBox ID="txt_oct_actual" runat="server" CssClass="form-control" placeholder="Actual ..."></asp:TextBox>
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="NOV" Visible="false" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_set_month_nov" Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lbl_u2_idx_nov" Visible="false" runat="server"></asp:Label>
                                            <p>
                                                <asp:TextBox ID="txt_nov_target" runat="server" CssClass="form-control" placeholder="Target ..."></asp:TextBox>
                                            </p>
                                            <p>
                                                <asp:TextBox ID="txt_nov_actual" runat="server" CssClass="form-control" placeholder="Actual ..."></asp:TextBox>
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="DEC" Visible="false" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_set_month_dec" Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lbl_u2_idx_dec" Visible="false" runat="server"></asp:Label>
                                            <p>
                                                <asp:TextBox ID="txt_dec_target" runat="server" CssClass="form-control" placeholder="Target ..."></asp:TextBox>
                                            </p>
                                            <p>
                                                <asp:TextBox ID="txt_dec_actual" runat="server" CssClass="form-control" placeholder="Actual ..."></asp:TextBox>
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>
                            </asp:GridView>
                        </div>

                        <div id="div_InsertCorporateKpi" runat="server">
                            <label>&nbsp;</label>
                            <%-- <div class="clearfix"></div>--%>
                            <div class="form-group pull-right">
                                <asp:LinkButton ID="btnSaveCorporateKpi" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdSaveCorporateKpi" ValidationGroup="SaveCorporateKpi"> <i class="fas fa-save"></i> บันทึก</asp:LinkButton>

                                <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancel">
                                    <i class="fas fa-times"></i> ยกเลิก</asp:LinkButton>
                            </div>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Detail Create -->


            </div>
        </asp:View>
        <!--View CorparateKPIs-->

        <!--View DetailKPIs-->
        <asp:View ID="docDetailKPIs" runat="server">
            <div class="col-md-12">
               
                <div id="GvTQA_scroll" style="overflow-x: auto; width: 100%" runat="server">
                    <asp:GridView ID="GvDetailTQA" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        ShowHeaderWhenEmpty="False"
                        AllowPaging="False"
                        PageSize="5"
                        BorderStyle="None"
                        CellSpacing="2">
                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <%--    <asp:TemplateField HeaderText="No" Visible="false" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>

                                    <%# (Container.DataItemIndex +1) %>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                            <asp:TemplateField HeaderText="KPI" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" ItemStyle-CssClass="left">
                                <ItemTemplate>
                                    <p>
                                        <asp:Label ID="lbl_u1_detail_idx_u1" Visible="false" runat="server" Text='<%# Eval("u1_detail_idx") %>' />
                                        <asp:Label ID="lbl_details_title_u1" runat="server" Text='<%# Eval("details_title") %>' />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Weight" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <p>
                                        <asp:Label ID="lbl_weight_u1" runat="server" Text='<%# Eval("weight") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Measurement" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <p>
                                        <asp:Label ID="lbl_measurement_detail_u1" runat="server" Text='<%# Eval("measurement_detail") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="PIC" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <p>
                                        <%--<asp:Label ID="lbl_measurement_detail_u1" runat="server" Text='<%# Eval("measurement_detail") %>' />--%>
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Deadline" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <p>
                                        <%--<asp:Label ID="lbl_measurement_detail_u1" runat="server" Text='<%# Eval("measurement_detail") %>' />--%>
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="type" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <p>
                                        <b>Target</b>
                                        <asp:Label ID="lbl_target" runat="server" />
                                    </p>
                                    <p>
                                        <b>Actual</b>
                                        <asp:Label ID="lbl_actual" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Annual Target" Visible="false" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <p>
                                        <%--  <asp:Label ID="lbl_annual_target" Visible="true" runat="server" Text='<%# Eval("annual_target") %>' />--%>
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="JAN" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_jan" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_jan" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_jan" Visible="true" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="FEB" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_feb" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_feb" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_feb" Visible="true" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="MAR" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_mar" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_mar" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_mar" Visible="true" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="APR" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_apr" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_apr" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_apr" Visible="true" runat="server" />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="MAY" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_may" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_may" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_may" Visible="true" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="JUN" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_jun" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_jun" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_jun" Visible="true" runat="server" />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="JUL" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_jul" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_jul" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_jul" Visible="true" runat="server" />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="AUG" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_aug" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_aug" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_aug" Visible="true" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="SEP" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_sep" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_sep" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_sep" Visible="true" runat="server" />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="OCT" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_oct" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_oct" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_oct" Visible="true" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="NOV" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_nov" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_nov" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_nov" Visible="true" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="DEC" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_set_month_dec" Visible="false" runat="server" />
                                    <p>
                                        <asp:Label ID="lbl_target_dec" Visible="true" runat="server" />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_actual_dec" Visible="true" runat="server" />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                    </asp:GridView>
                </div>

            </div>
        </asp:View>
        <!--View DetailKPIs-->

    </asp:MultiView>
    <!--multiview-->

</asp:Content>
