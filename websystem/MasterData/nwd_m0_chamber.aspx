﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="nwd_m0_chamber.aspx.cs" Inherits="websystem_MasterData_nwd_m0_chamber" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">

    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <div class="col-md-12">
                <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary" runat="server" CommandName="btnToInsert" OnCommand="btnCommand"><i class="fa fa-plus-square"></i> สร้างห้องวางตำแหน่งอุปกรณ์ </asp:LinkButton>

                 <asp:GridView ID="GvMaster"
                    runat="server"
                    AutoGenerateColumns="false"
                    DataKeyNames="CHIDX"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowEditing="Master_RowEditing"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">No result</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="CHIDX" runat="server" CssClass="form-control"
                                    Visible="False" Text='<%# Eval("CHIDX")%>' />

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">ห้องวางตำแหน่งอุปกรณ์</label>
                                            <asp:TextBox ID="txtchamber_update" CssClass="form-control" runat="server" Text='<%# Bind("Chamber_name") %>' />

                                        </small>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">สถานะ</label>
                                            <asp:DropDownList ID="ddlFloorStatusUpdate" AutoPostBack="false" runat="server"
                                                CssClass="form-control" SelectedValue='<%# Eval("CH_status") %>'>
                                                <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                            </asp:DropDownList>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                            ValidationGroup="saveTypeNameUpdate" CommandName="Update"
                                            OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                         บันทึก
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                            CommandName="Cancel">ยกเลิก</asp:LinkButton>
                                    </div>
                                </div>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ห้องวางตำแหน่งอุปกรณ์" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Floorname" runat="server" Text='<%# Eval("Chamber_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Statuscategory" runat="server" Text='<%# getStatus((int)Eval("CH_status")) %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("CHIDX") %>'
                                    OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

                </div>
            </asp:View>

             <asp:View ID="ViewInsert" runat="server">
            <div class="row col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <asp:LinkButton CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                                CommandName="btnCancel" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>สร้างห้องวางตำแหน่งอุปกรณ์</label>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtaddchamber" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="save" runat="server" Display="None" ControlToValidate="txtaddchamber" Font-Size="11"
                                        ErrorMessage="กรุณาห้องชั้นวางตำแหน่งอุปกรณ์"
                                        ValidationExpression="กรุณาห้องชั้นวางตำแหน่งอุปกรณ์"
                                        SetFocusOnError="true" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                    <asp:RegularExpressionValidator ID="RegularExpressiondValidator1" runat="server"
                                        ValidationGroup="Save" Display="None"
                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                        ControlToValidate="txtaddchamber"
                                        ValidationExpression="^[\s\S]{0,1000}$"
                                        SetFocusOnError="true" />

                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressiondValidator1" Width="160" />

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label>สถานะ</label>
                            <asp:DropDownList ID="ddlChamberStatus" runat="server" CssClass="form-control">
                                <asp:ListItem Value="1" Text="Online" />
                                <asp:ListItem Value="0" Text="Offline" />
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <asp:Button CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="save" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
 

     
        </asp:MultiView>

</asp:Content>

