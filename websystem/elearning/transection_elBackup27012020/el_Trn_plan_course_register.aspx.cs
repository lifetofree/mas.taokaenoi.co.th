﻿using AjaxControlToolkit;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

public partial class websystem_el_Trn_plan_course_register : System.Web.UI.Page
{
    #region Init
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_employee = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_employee"];

    static string _urlGetel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u_plan_course"];
    static string _urlSetInsel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_u_plan_course"];
    static string _urlDelel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_u_plan_course"];
    static string _urlSetUpdel_u_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_u_plan_course"];

    static string _urlGetel_Report_plan_course = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_Report_plan_course"];

    static string _urlGetel_u_plan = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_u_plan"];
    static string _urlsendEmail_traningregister = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_traningregister"];
    static string _urlsendEmail_trn_summaryleadertohr = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_trn_summaryleadertohr"];

    string _localJson = "";
    int _tempInt = 0;

    int emp_idx = 0;

    string _FromcourseRunNo = "u_plan_course_summ";
    string _Folder_plan_course = "plan_course_summ_file";
    string _Folder_plan_course_bin = "plan_course_summ_file_bin";
    string _PathFileimage = ConfigurationSettings.AppSettings["path_flieimage_elearning"];

    string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
    string _Folder_course_video = "course_video";
    string _Folder_course_video_image = "course_video_image";

    #region Constant
    public static class Constants
    {
        public const int SELECT_ALL = 20;
        public const int SELECT_WHERE = 21;
        public const int NUMBER_NULL = -999;


    }
    #endregion Constant



    //start_object
    FormView _FormView;
    TextBox
        _txttraining_course_no
    , _txttraining_group_name
    , _txttraining_branch_name
    , _txttraining_course_date
    , _txttraining_course_year
    , _txttraining_course_qty
    , _txttraining_course_amount
    , _txttraining_course_model
    , _txttraining_course_budget
    , _txttraining_course_costperhead
    , _txttraining_course_description
    , _txtdatestart_create
    , _txt_timestart_create
    , _txtdateend_create
    , _txt_timeend_create
        , _txtexpenses_description
        , _txtamount
        , _txtvat
        , _txtwithholding_tax
        , _txttraining_course_total
        , _txttraining_course_total_avg
        , _txttraining_course_reduce_tax
        , _txttraining_course_net_charge
        , _txttraining_course_net_charge_tax
        , _txttraining_course_planbudget_total
        , _txttraining_course_budget_total
        , _txttraining_course_budget_balance
        , _txttraining_course_budget_total_per
        , _txtpass_test_per
        , _txthour_training_per
        , _txtpublish_training_description
        , _txtother_description
        , _txthrd_follow_day
        , _txttraining_course_remark
        , _txttraining_course_file_name
        , _txtu6_training_course_idx
        , _txtemp_idx_ref
        , _txtu3_training_course_idx
        , _txttraining_summary_report_remark
        , _txttraining_benefits_remark
        , _txtu0_training_course_idx_ref
        , _txtsupervisors_additional

        ;
    RadioButtonList
        _rdllecturer_type
        , _rdotraining_course_type
        , _rdotraining_course_planbudget_type

        ;
    DropDownList
        _ddltraining_course_status
        , _ddlm0_institution_idx_ref
        , _ddlm0_objective_idx_ref
        , _ddlm0_target_group_idx_ref
        , _ddlu0_training_plan_idx_ref
        , _ddlemp_idx_ref
        , _ddlcostcenter_idx_ref
        , _ddlRDeptID_ref
        , _ddlcourse_plan_status
        , _ddlplace_idx_ref

        ;
    GridView
        _GvMonthList
        , _GvMonthList_L
        , _GvMonthList_A
        , _Gvinstitution
        , _Gvobjective
        , _Gvemp
        , _Gvtrn_expenses
        , _GvCourse
        ;
    CheckBox
         _cbpass_test_flag
        , _cbhour_training_flag
        , _cbwrite_report_training_flag
        , _cbpublish_training_flag
        , _cbcourse_lecturer_flag
        , _cbother_flag
        , _cbhrd_nofollow_flag
        , _cbhrd_follow_flag
        ;

    Panel
        _pnl_institution
        , _pnl_objective
        , _pnl_emp
        , _pnl_expenses
        , _pnl_file
        , _pnl_file_btn
        , _pnl_super
        ;
    Label
        _lbFileName,
        _lbtraining_course_no,
        _lbzcourse_count
        ;

    #endregion Init

    private DateTime _dtMonth;
    private DateTime _selectedDate;
    private bool _specialDaySelected = true;
    private int _currentBindingMonth;

    public void zCrontrollFrom()
    {


    }
    protected void Select_DetailList()
    {
        _data_elearning.employeeM_action = new employeeM[1];
        employeeM obj = new employeeM();
        obj.EmpIDX = emp_idx;
        _data_elearning.employeeM_action[0] = obj;
        //litDebug.Text = _funcTool.convertObjectToXml(_data_elearning);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        _data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_employee, _data_elearning);
        _func_dmu.zSetFormViewData(FvDetailUser, _data_elearning.employeeM_action);
        if (_data_elearning.employeeM_action != null)
        {
            foreach (var item in _data_elearning.employeeM_action)
            {
                ViewState["DocCode"] = item.EmpCode;
                ViewState["CEmpIDX_Create"] = item.EmpIDX;
                ViewState["RPosIDX_J"] = item.RPosIDX_J;
                ViewState["RDeptID"] = item.RDeptID;
                ViewState["RSecID"] = item.RSecID;
                ViewState["JobLevel"] = item.JobLevel;
                ViewState["joblevel_idx"] = item.JobLevel;
            }
        }
    }
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());
        txtemp_idx.Text = Session["emp_idx"].ToString();
        Hddfld_emp_idx.Value = Session["emp_idx"].ToString();
        Hddfld_folder.Value = "fd_emp" + emp_idx.ToString() + DateTime.Now.ToString("yyyyMMdd");

        if (!IsPostBack)
        {

            zDeleteFileBin();
            HttpPostedFile file_Uploaded_txtfileimport = Request.Files["Uploaded_txtfileimport"];
            if (file_Uploaded_txtfileimport != null && file_Uploaded_txtfileimport.ContentLength > 0)
            {
                importProcessRequest(file_Uploaded_txtfileimport, 1);
            }


            Hddfld_training_course_no.Value = "";
            Hddfld_u0_training_course_idx.Value = "";

            // setStatusData("btnHR_Wait");

            zSetMode(9);
            // zbtnHR_Wait();
            Select_DetailList();

            zCrontrollFrom();
            ShowListHR_W_Status();

            if (getViewState("joblevel_idx") >= 7)
            {
                liMD_App.Visible = true;
            }
            else
            {
                liMD_App.Visible = false;
            }

            if (Session["_session_elearning_test"] != null)
            {
                if (Session["_session_elearning_test"].ToString() == "elearning")
                {
                    zSetMode(4);
                    ShowListHR_A();
                }

                Session.Remove("_session_elearning_test");
            }

            SETFOCUS.Focus();

        }
        linkBtnTrigger(btnListData);
        linkBtnTrigger(btnInsert);
        linkBtnTrigger(btnHR_Wait);
        linkBtnTrigger(btnHR_App);

        linkBtnTrigger(btnscheduler);
        linkBtnTrigger(btnS_SCHED);
        linkBtnTrigger(btnprint_sched);

        linkBtnTrigger(btnMD_App);
        linkBtnTrigger(btnSave_summary);
        linkBtnTrigger(btnsummary_back);
        linkBtnTrigger(btnMD_Wait);
        linkBtnTrigger(btnnoregister);
        linkBtnTrigger(btnopen_course);
        linkBtnTrigger(btnHR_App);
        linkBtnTrigger(btnsummary_course);
        linkBtnTrigger(btnsyllabus);
        //linkBtnTrigger(_divbtn_elearning);

    }
    #endregion Page Load

    public int getViewState(string sViewState)
    {
        int iReturn = 0;

        if ((ViewState[sViewState] == "") || (ViewState[sViewState] == null))
        {

        }
        else
        {
            iReturn = int.Parse(ViewState[sViewState].ToString());
        }

        return iReturn;

    }
    /* Start Std*/
    #region Std

    #region zSetMode
    public void zSetMode(int AiMode)
    {

        switch (AiMode)
        {
            case 0:  //insert mode
                {

                }
                break;
            case 1:  //update mode
                {

                }
                break;
            case 2://preview mode
                {


                }
                break;
            case 3://รายการที่รอ HRD อนุมัติ
                {
                    if (Hddfld_status.Value == "no_register")
                    {
                        setActiveTab("no_register");
                    }
                    else if (Hddfld_status.Value == "open_course")
                    {
                        setActiveTab("open_course");
                    }
                    else
                    {
                        setActiveTab("HR_W");
                    }

                    MultiViewBody.SetActiveView(View_HR_WList);
                    _func_dmu.zDropDownList(ddlYearSearch_HR_W,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-COURSE-YEAR"
                                );
                    txtFilterKeyword_HR_W.Text = "";
                }
                break;
            case 4://รายการที่ HRD อนุมัติเสร็จแล้ว
                {

                    setActiveTab("HR_A");
                    MultiViewBody.SetActiveView(View_HR_AList);
                    _func_dmu.zDropDownList(ddlYearSearch_HR_A,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-COURSE-YEAR"
                                );
                    txtFilterKeyword_HR_A.Text = "";
                }
                break;
            case 5://รายการ HRD Detail
                {

                    setActiveTab("");
                    MultiViewBody.SetActiveView(View_HR_WDetail);
                    fv_preview.Visible = true;
                    fv_preview.ChangeMode(FormViewMode.Edit);
                }
                break;
            case 6://รายการที่รอ MD อนุมัติ
                {

                    setActiveTab("MD_W");
                    MultiViewBody.SetActiveView(View_MD_WList);
                    _func_dmu.zDropDownList(ddlYearSearch_MD_W,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-COURSE-YEAR"
                                );
                    txtFilterKeyword_MD_W.Text = "";
                }
                break;
            case 7://รายการที่ MD อนุมัติเสร็จแล้ว
                {

                    setActiveTab("MD_A");
                    MultiViewBody.SetActiveView(View_MD_AList);
                    _func_dmu.zDropDownList(ddlYearSearch_MD_A,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-COURSE-YEAR"
                                );
                    txtFilterKeyword_MD_A.Text = "";
                }
                break;
            case 8://หลักสูตรที่จะต้องเรียน
                {

                    setActiveTab("syllabus");
                    MultiViewBody.SetActiveView(View_syllabusList);
                    _func_dmu.zDropDownList(ddlYearSearch_syllabus,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-COURSE-YEAR"
                                );
                    txtFilterKeyword_syllabus.Text = "";
                }
                break;
            case 9://ตารางแผนการอบรม
                {

                    setActiveTab("SCHEDULER");
                    MultiViewBody.SetActiveView(View_SCHEDList);
                    _func_dmu.zDropDownList(ddlYearSearch_SCHED,
                                "-",
                                "zyear",
                                "zyear",
                                "0",
                                "trainingLoolup",
                                DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                "TRN-PLAN-YEAR-SCHED"
                                );
                    txtFilterKeyword_SCHED.Text = "";
                    _func_dmu.zDropDownList(ddltrn_groupSearch_SCHED,
                                "-",
                                "zId",
                                "zName",
                                "0",
                                "trainingLoolup",
                                "",
                                "TRN-PLAN-GRP-SCHED"
                                );
                }
                break;
            case 10://ผลการฝึกอบรม
                {

                    //setActiveTab("SCHEDULER");
                    MultiViewBody.SetActiveView(View_summarycourse);
                }
                break;

        }
    }
    #endregion zSetMode

    private void ShowListHR_W() //รายการที่รอ HRD อนุมัติ
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.filter_keyword = txtFilterKeyword_HR_W.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_HR_W.SelectedValue);
        obj.emp_idx_ref = emp_idx;
        obj.zstatus = Hddfld_status.Value;
        // litDebug.Text = Hddfld_status.Value;
        obj.operation_status_id = "U0-LISTDATA-REGISTER";
        dataelearning.el_training_course_action[0] = obj;
        obj.zresulte_count = 9;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetGridData(GvHR_WList, dataelearning.el_training_course_action);
        ShowListHR_W_Status();
    }
    private void ShowListHR_W_Status() //รายการที่รอ HRD อนุมัติ
    {
        data_elearning dataelearning1 = new data_elearning();
        dataelearning1.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        // obj.filter_keyword = txtFilterKeyword_HR_W.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        //obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_HR_W.SelectedValue);
        obj.emp_idx_ref = emp_idx;
        obj.zstatus = "REGISTER-W";
        obj.operation_status_id = "U0-LISTDATA-REGISTER-COUNT";
        dataelearning1.el_training_course_action[0] = obj;
        //litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning1));
        dataelearning1 = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning1);

        int iCount = 0;
        if (dataelearning1.el_training_course_action != null)
        {
            iCount = dataelearning1.el_training_course_action[0].idx;
        }
        btnHR_Wait.Text = "รายการที่รอลงทะเบียน <span class='badge progress-bar-danger' >" + iCount.ToString() + "</span>";
        btnscheduler.Text = "ตารางแผนการอบรม <span class='badge progress-bar-danger' >" + iCount.ToString() + "</span>";

    }
    private void ShowListHR_A() //รายการที่ HRD อนุมัติ
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.filter_keyword = txtFilterKeyword_HR_A.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_HR_A.SelectedValue);
        obj.emp_idx_ref = emp_idx;
        obj.zstatus = "trn_course_all";// "REGISTER-A";
        obj.operation_status_id = "U0-LISTDATA-REGISTER";
        dataelearning.el_training_course_action[0] = obj;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetGridData(GvHR_AList, dataelearning.el_training_course_action);
        ShowListHR_W_Status();
    }

    private void ShowListMD_W() //รายการที่รอ MD อนุมัติ
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.filter_keyword = txtFilterKeyword_MD_W.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_MD_W.SelectedValue);
        obj.zstatus = "MD-W";
        obj.operation_status_id = "U0-LISTDATA-HR";
        dataelearning.el_training_course_action[0] = obj;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetGridData(GvMD_WList, dataelearning.el_training_course_action);
        ShowListMD_W_Status();
    }
    private void ShowListMD_W_Status() //รายการที่รอ MD อนุมัติ
    {
        data_elearning dataelearning1 = new data_elearning();
        dataelearning1.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        // obj.filter_keyword = txtFilterKeyword_HR_W.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        //obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_HR_W.SelectedValue);
        obj.zstatus = "MD-W";
        obj.operation_status_id = "U0-LISTDATA-HR-COUNT";
        dataelearning1.el_training_course_action[0] = obj;
        //litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning1));
        dataelearning1 = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning1);

        int iCount = 0;
        if (dataelearning1.el_training_course_action != null)
        {
            iCount = dataelearning1.el_training_course_action[0].idx;
        }
        //iCount = 99;
        if (iCount > 0)
        {
            btnMD_Wait.Text = "รายการที่รอ MD อนุมัติ <span class='badge progress-bar-danger' >" + iCount.ToString() + "</span>";

        }
        else
        {
            btnMD_Wait.Text = "รายการที่รอ MD อนุมัติ <span class='badge progress-bar-danger' >" + iCount.ToString() + "</span>";

        }

    }
    private void ShowListMD_A() //รายการที่ MD อนุมัติ
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.filter_keyword = txtFilterKeyword_MD_A.Text;
        obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_MD_A.SelectedValue);
        obj.idx = _func_dmu.zStringToInt(ddlSummary.SelectedValue);
        obj.zstatus = "MD-A";
        obj.RDeptID_ref = _func_dmu.zStringToInt(ViewState["RDeptID"].ToString());
        obj.RSecID = _func_dmu.zStringToInt(ViewState["RSecID"].ToString());
        obj.operation_status_id = "U6-LISTDATA-SUMMARY";
        dataelearning.el_training_course_action[0] = obj;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetGridData(GvMD_AList, dataelearning.el_training_course_action);
        ShowListMD_W_Status();
    }


    #region setActiveTab
    protected void setActiveTab(string activeTab)
    {
        liInsert.Attributes.Add("class", "");
        liListData.Attributes.Add("class", "");
        liHR_Wait.Attributes.Add("class", "");
        liHR_App.Attributes.Add("class", "");
        liMD_Wait.Attributes.Add("class", "");
        liMD_App.Attributes.Add("class", "");
        lisyllabus.Attributes.Add("class", "");
        linoregister.Attributes.Add("class", "");
        liopen_course.Attributes.Add("class", "");
        lischeduler.Attributes.Add("class", "");
        //li_elearning.Attributes.Add("class", "");

        switch (activeTab)
        {
            case "P":
                liListData.Attributes.Add("class", "active");

                break;
            case "I":
                liInsert.Attributes.Add("class", "active");
                break;
            case "HR_W":
                liHR_Wait.Attributes.Add("class", "active");
                break;
            case "HR_A":
                liHR_App.Attributes.Add("class", "active");
                break;
            case "MD_W":
                liMD_Wait.Attributes.Add("class", "active");
                break;
            case "MD_A":
                liMD_App.Attributes.Add("class", "active");
                break;
            case "syllabus":
                lisyllabus.Attributes.Add("class", "active");
                break;
            case "no_register":
                linoregister.Attributes.Add("class", "active");
                break;
            case "open_course":
                liopen_course.Attributes.Add("class", "active");
                break;
            case "SCHEDULER":
                lischeduler.Attributes.Add("class", "active");
                break;
            case "elearning":
                //li_elearning.Attributes.Add("class", "active");
                liHR_App.Attributes.Add("class", "active");
                break;

        }
    }
    #endregion setActiveTab

    #endregion Std

    #region Action
    protected void actionIndex()
    {

    }

    public Boolean getDataCheckbox(string id)
    {
        int i = 0;
        if ((id != "") && (id != null))
        {
            i = int.Parse(id);
        }
        if (i == 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    private int getcourse_plan_status(string value)
    {
        if (value == "I")
        {
            return 1;
        }
        else
        {
            return 2;
        }
    }

    protected void onSelectedIndexChanged(object sender, EventArgs e)
    {

        //var chkName = (CheckBoxList)sender;
        if (sender is CheckBox)
        {
            var rdName = (CheckBox)sender;
            switch (rdName.ID)
            {
                case "GvPosition_cb_RPosIDX_J":

                    break;

            }
        }

    }
    protected void onRowDataBound(Object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        string sGvName = GvName.ID;
        if (sGvName == "GvListData")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton btnUpdate_GvListData = (LinkButton)e.Row.Cells[9].FindControl("btnUpdate_GvListData");
                LinkButton btnDelete_GvListData = (LinkButton)e.Row.Cells[9].FindControl("btnDelete_GvListData");
                TextBox txtapprove_status = (TextBox)e.Row.Cells[9].FindControl("txtapprove_status_GvListData");
                TextBox txtmd_approve_status = (TextBox)e.Row.Cells[9].FindControl("txtmd_approve_status_GvListData");
                HyperLink btnDownloadFile = (HyperLink)e.Row.Cells[9].FindControl("btnDownloadFile");
                TextBox txtGvtraining_course_no = (TextBox)e.Row.Cells[9].FindControl("txtGvtraining_course_no");

                Boolean bBl = true, bBl_a = true;

                btnDownloadFile.NavigateUrl = DownloadFile(txtGvtraining_course_no.Text);
                if (btnDownloadFile.NavigateUrl == "")
                {
                    btnDownloadFile.Visible = false;
                }
                else
                {
                    btnDownloadFile.Visible = true;
                }

                if ((txtapprove_status.Text == "4") || (txtmd_approve_status.Text == "4"))// 4	อนุมัติ
                {
                    bBl = false;
                    bBl_a = true;
                }
                else if ((txtapprove_status.Text == "6") || (txtmd_approve_status.Text == "6"))
                {
                    bBl = false;
                    bBl_a = false;
                }
                btnUpdate_GvListData.Visible = bBl_a;
                btnDelete_GvListData.Visible = bBl;
            }
        }
        else if (sGvName == "GvMD_AList")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtGvtraining_course_no = (TextBox)e.Row.Cells[6].FindControl("txtGvtraining_course_no");
                TextBox txtGvemp_idx_ref = (TextBox)e.Row.Cells[6].FindControl("txtGvemp_idx_ref");
                HyperLink btnDownloadFile = (HyperLink)e.Row.Cells[6].FindControl("btnDownloadFile");
                Label lbu0_training_course_idx = (Label)e.Row.Cells[0].FindControl("lbu0_training_course_idx");
                Label lbu0_course_idx = (Label)e.Row.Cells[0].FindControl("lbu0_course_idx");

                btnDownloadFile.NavigateUrl = DownloadFile(txtGvtraining_course_no.Text + "_emp" + txtGvemp_idx_ref.Text);
                if (btnDownloadFile.NavigateUrl == "")
                {
                    btnDownloadFile.Visible = false;
                }
                else
                {
                    btnDownloadFile.Visible = true;
                }

                Repeater rptu8trncoursedate = (Repeater)e.Row.Cells[4].FindControl("rptu8trncoursedate");
                setU8StartEndDatetime(_func_dmu.zStringToInt(lbu0_training_course_idx.Text), rptu8trncoursedate);

            }

        }
        else if (sGvName == "GvHR_AList")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtGvtraining_course_no = (TextBox)e.Row.Cells[7].FindControl("txtGvtraining_course_no");
                Label lbu0_training_course_idx = (Label)e.Row.Cells[7].FindControl("lbu0_training_course_idx");
                Label lbu0_course_idx = (Label)e.Row.Cells[7].FindControl("lbu0_course_idx");
                //TextBox txtGvemp_idx_ref = (TextBox)e.Row.Cells[7].FindControl("txtGvemp_idx_ref");
                HyperLink btnDownloadFile = (HyperLink)e.Row.Cells[7].FindControl("btnDownloadFile");
                Repeater rptcourse = (Repeater)e.Row.Cells[7].FindControl("rptcourse");

                btnDownloadFile.NavigateUrl = DownloadFile(txtGvtraining_course_no.Text + "_emp" + emp_idx.ToString());
                if (btnDownloadFile.NavigateUrl == "")
                {
                    btnDownloadFile.Visible = false;
                }
                else
                {
                    btnDownloadFile.Visible = true;
                }

                setCoursePass(_func_dmu.zStringToInt(lbu0_course_idx.Text), rptcourse);

                Label lbgrade_status = (Label)e.Row.Cells[7].FindControl("lbgrade_status");
                Label lbsumm_super_status = (Label)e.Row.Cells[7].FindControl("lbsumm_super_status");
                Panel pnlicon_yes = (Panel)e.Row.Cells[6].FindControl("pnlicon_yes");
                Panel pnlicon_no = (Panel)e.Row.Cells[6].FindControl("pnlicon_no");

                pnlicon_yes.Visible = false;
                pnlicon_no.Visible = false;
                //คอร์สอบรมภายนอก
                if (_func_dmu.zStringToInt(lbsumm_super_status.Text) == 1)
                {
                    pnlicon_yes.Visible = true;
                }
                //คอร์สอบรมภายใน
                else if ((_func_dmu.zStringToInt(lbgrade_status.Text) == 1) ||
                    (_func_dmu.zStringToInt(lbgrade_status.Text) == 6))
                {
                    pnlicon_yes.Visible = true;
                }
                else if (_func_dmu.zStringToInt(lbgrade_status.Text) == 2)
                {
                    pnlicon_no.Visible = true;
                }

                /*
                if ((_func_dmu.zStringToInt(lbgrade_status.Text) == 1) || 
                    (_func_dmu.zStringToInt(lbgrade_status.Text) == 6))
                {
                    pnlicon_yes.Visible = true;
                    pnlicon_no.Visible = false;
                }
                else if (_func_dmu.zStringToInt(lbgrade_status.Text) == 2)
                {
                    pnlicon_yes.Visible = false;
                    pnlicon_no.Visible = true;
                }
                else
                {
                    pnlicon_yes.Visible = false;
                    pnlicon_no.Visible = false;
                }
                */
                Repeater rptu8trncoursedate = (Repeater)e.Row.Cells[6].FindControl("rptu8trncoursedate");
                setU8StartEndDatetime(_func_dmu.zStringToInt(lbu0_training_course_idx.Text), rptu8trncoursedate);


            }

        }
        else if (sGvName == "GvHR_WList")
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lbu0_course_idx = (Label)e.Row.Cells[7].FindControl("lbu0_course_idx");
                Label lbu0_training_course_idx = (Label)e.Row.Cells[7].FindControl("lbu0_training_course_idx");
                Repeater rptcourse = (Repeater)e.Row.Cells[7].FindControl("rptcourse");

                setCoursePass(_func_dmu.zStringToInt(lbu0_course_idx.Text), rptcourse);

                Label lbgrade_status = (Label)e.Row.Cells[7].FindControl("lbgrade_status");
                Panel pnlicon_yes = (Panel)e.Row.Cells[6].FindControl("pnlicon_yes");
                Panel pnlicon_no = (Panel)e.Row.Cells[6].FindControl("pnlicon_no");
                Label lbsumm_super_status = (Label)e.Row.Cells[7].FindControl("lbsumm_super_status");

                pnlicon_yes.Visible = false;
                pnlicon_no.Visible = false;
                //คอร์สอบรมภายนอก
                if (_func_dmu.zStringToInt(lbsumm_super_status.Text) == 1)
                {
                    pnlicon_yes.Visible = true;
                }
                //คอร์สอบรมภายใน
                else if ((_func_dmu.zStringToInt(lbgrade_status.Text) == 1) ||
                    (_func_dmu.zStringToInt(lbgrade_status.Text) == 6))
                {
                    pnlicon_yes.Visible = true;
                }
                else if (_func_dmu.zStringToInt(lbgrade_status.Text) == 2)
                {
                    pnlicon_no.Visible = true;
                }

                //if ((_func_dmu.zStringToInt(lbgrade_status.Text) == 1) || (_func_dmu.zStringToInt(lbgrade_status.Text) == 6))
                //{
                //    pnlicon_yes.Visible = true;
                //    pnlicon_no.Visible = false;
                //}
                //else if (_func_dmu.zStringToInt(lbgrade_status.Text) == 2)
                //{
                //    pnlicon_yes.Visible = false;
                //    pnlicon_no.Visible = true;
                //}
                //else
                //{
                //    pnlicon_yes.Visible = false;
                //    pnlicon_no.Visible = false;
                //}

                Repeater rptu8trncoursedate = (Repeater)e.Row.Cells[6].FindControl("rptu8trncoursedate");
                setU8StartEndDatetime(_func_dmu.zStringToInt(lbu0_training_course_idx.Text), rptu8trncoursedate);

            }

        }

    }


    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {

            case "GvHR_WList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListHR_W();
                SETFOCUS.Focus();
                break;
            case "GvHR_AList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListHR_A();
                SETFOCUS.Focus();
                break;
            case "GvMD_WList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListMD_W();
                SETFOCUS.Focus();
                break;
            case "GvMD_AList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListMD_A();
                SETFOCUS.Focus();
                break;
            case "GvsyllabusList":
                GvName.PageIndex = e.NewPageIndex;
                GvName.DataBind();
                ShowListsyllabus();
                SETFOCUS.Focus();
                break;

        }
    }

    protected string getStatus(int status)
    {

        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }
    protected string getStatusLevel(int status)
    {

        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='ผ่าน'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='ไม่ผ่าน'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }
    protected string getStatustest(int status)
    {

        if (status > 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected string getStatusEvalue(int status)
    {

        if (status > 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    #endregion Action

    private void zbtnHR_Wait()
    {

        zSetMode(3);

        ShowListHR_W();
    }

    private void zbtnMD_Wait()
    {
        zSetMode(6);
        ShowListMD_W();
    }


    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        int _m0_training_idx;
        int _md_app = 1;

        m0_training objM0_ProductType = new m0_training();
        setStatusData(cmdName);
        _lbzcourse_count = (Label)fv_preview.FindControl("lbzcourse_count");

        switch (cmdName)
        {

            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            //HR  
            case "btnHR_Wait_back":
                zbtnHR_Wait();
                SETFOCUS.Focus();
                break;

            case "btnHR_Wait":
                zbtnHR_Wait();
                SETFOCUS.Focus();
                break;
            case "btnS_HR_W":
                ShowListHR_W();
                break;
            case "btnDetail_GvHR_WList":
                // _m0_training_idx = int.Parse(cmdArg);
                string su0_idx1 = "", empidx1 = "", zresulte_app_status1 = "", zresulte_count1 = "";
                string[] calc1 = new string[4];
                if (cmdArg != "0")
                {
                    calc1 = cmdArg.Split('|');
                }
                else
                {
                    calc1 = new string[4] { "", "", "", "" };
                }
                su0_idx1 = calc1[0];
                empidx1 = calc1[1];
                zresulte_app_status1 = calc1[2];
                zresulte_count1 = calc1[3];
                _m0_training_idx = int.Parse(su0_idx1);
                if (_m0_training_idx > 0)
                {
                    zSetMode(5);
                    if (Hddfld_status.Value == "no_register")
                    {
                        setActiveTab("no_register");
                    }
                    else if (Hddfld_status.Value == "open_course")
                    {
                        setActiveTab("open_course");
                    }
                    else
                    {
                        setActiveTab("HR_W");
                    }

                    _md_app = zShowdataHR_WDetail(_m0_training_idx, "REGISTER-W", "HR");

                    setpanel_approve("btnDetail_GvHR_WList",
                                     _md_app,
                                     _func_dmu.zStringToInt(empidx1),
                                     _func_dmu.zStringToInt(zresulte_app_status1),
                                      _func_dmu.zStringToInt(zresulte_count1)
                                     );

                    zcrtl_btnback("HRW");
                }
                if (Hddfld_training_course_no.Value == "")
                {
                    zSetMode(3);
                }
                SETFOCUS.Focus();
                break;
            case "btnupdateApprove_HR":
                _m0_training_idx = int.Parse(cmdArg);

                if (_m0_training_idx > 0)
                {
                    zSaveApprove(_m0_training_idx);

                }
                zSetMode(3);
                SETFOCUS.Focus();
                ShowListHR_W();
                SETFOCUS.Focus();
                break;
            // HR อนุมัติแล้ว
            case "btnHR_App":
                zSetMode(4);
                ShowListHR_A();
                SETFOCUS.Focus();
                break;
            case "btnS_HR_A":
                ShowListHR_A();
                break;
            case "btnDetail_GvHR_AList":

                string su0idx1 = "", sitem1 = "", course_type_elearning1 = "", course_type_etraining1 = "";
                string[] calc2 = new string[4];
                if (cmdArg != "0")
                {
                    calc2 = cmdArg.Split('|');
                }
                else
                {
                    calc2 = new string[4] { "", "", "", "" };
                }
                su0idx1 = calc2[0];
                sitem1 = calc2[1];
                course_type_elearning1 = calc2[2];
                course_type_etraining1 = calc2[3];
                _m0_training_idx = _func_dmu.zStringToInt(su0idx1);
                if (_m0_training_idx > 0)
                {
                    zSetMode(5);
                    setActiveTab("HR_A");
                    _md_app = zShowdataHR_WDetail(_m0_training_idx, "HR-A", "HR");
                    if (_func_dmu.zStringToInt(sitem1) > 0)
                    {
                        _md_app = 0;
                    }
                    setpanel_approve("btnDetail_GvHR_AList", _md_app, 0, 0, 0, _func_dmu.zStringToInt(course_type_etraining1));
                    zcrtl_btnback("HRA");

                }
                if (Hddfld_training_course_no.Value == "")
                {
                    zSetMode(4);
                }
                SETFOCUS.Focus();
                break;
            case "btnupdateApprove_HR_A":
                _m0_training_idx = int.Parse(cmdArg);

                if (_m0_training_idx > 0)
                {
                    zSaveApprove(_m0_training_idx);
                }
                zSetMode(4);
                ShowListHR_A();
                SETFOCUS.Focus();
                break;
            //MD
            case "btnMD_Wait":
                zSetMode(6);
                ShowListMD_W();
                SETFOCUS.Focus();
                break;
            case "btnS_MD_W":
                ShowListMD_W();
                break;
            case "btnDetail_GvMD_WList":
                _m0_training_idx = int.Parse(cmdArg);

                if (_m0_training_idx > 0)
                {

                    zSetMode(5);
                    setActiveTab("MD_W");
                    zShowdataHR_WDetail(_m0_training_idx, "MD-W", "MD");
                    setpanel_approve("btnDetail_GvMD_WList", 0, 0, 0, 0);
                    zcrtl_btnback("MDW");


                }
                if (Hddfld_training_course_no.Value == "")
                {
                    zSetMode(6);
                }
                SETFOCUS.Focus();
                break;
            case "btnupdateApprove_MD":
                _m0_training_idx = int.Parse(cmdArg);

                if (_m0_training_idx > 0)
                {
                    zSaveApproveMD(_m0_training_idx);

                }
                zSetMode(6);
                ShowListMD_W();
                SETFOCUS.Focus();
                break;
            // HR อนุมัติแล้ว
            case "btnMD_App":
                zSetMode(7);
                ShowListMD_A();
                SETFOCUS.Focus();
                break;
            case "btnS_MD_A":
                ShowListMD_A();
                break;
            case "btnDetail_GvMD_AList":
                //_m0_training_idx = int.Parse(cmdArg);
                string su0_idx = "", su6_idx = "";
                string[] calc = new string[4];
                if (cmdArg != "0")
                {
                    calc = cmdArg.Split('|');
                }
                else
                {
                    calc = new string[4] { "", "", "", "" };
                }
                su0_idx = calc[0];
                su6_idx = calc[1];
                if ((_func_dmu.zStringToInt(su0_idx) > 0) && (_func_dmu.zStringToInt(su6_idx) > 0))
                {
                    zSetMode(10);
                    setActiveTab("MD_A");
                    zShowdata_SummaryDetail(_func_dmu.zStringToInt(su0_idx), _func_dmu.zStringToInt(su6_idx), "open_course");

                }
                if (Hddfld_training_course_no.Value == "")
                {
                    zSetMode(7);
                }
                SETFOCUS.Focus();
                break;
            case "btnupdateApprove_MD_A":
                _m0_training_idx = int.Parse(cmdArg);

                if (_m0_training_idx > 0)
                {
                    zSaveApproveMD(_m0_training_idx);

                }
                zSetMode(7);
                ShowListMD_A();
                SETFOCUS.Focus();
                break;
            case "btnsyllabus":
                zSetMode(8);
                ShowListsyllabus();
                SETFOCUS.Focus();
                break;
            case "btnS_syllabus":
                ShowListsyllabus();
                break;
            case "btnDetail_GvsyllabusList":
                ShowDetailsyllabus(int.Parse(cmdArg));
                break;
            case "btnnoregister":
                zbtnHR_Wait();
                SETFOCUS.Focus();
                break;
            case "btnopen_course":
                zbtnHR_Wait();
                SETFOCUS.Focus();
                break;
            case "btnscheduler":
                zSetMode(9);
                SETFOCUS.Focus();
                ShowListHR_W_Status();
                // getHtmlSched();
                break;
            case "btnDetail_sumcourseList":
                _m0_training_idx = int.Parse(cmdArg);

                if (_m0_training_idx > 0)
                {

                    zSetMode(10);
                    setActiveTab("HR_A");

                    zShowdata_SummaryDetail(_m0_training_idx, 0, "trn_course_all");

                }
                if (Hddfld_training_course_no.Value == "")
                {
                    zSetMode(4);
                }
                SETFOCUS.Focus();
                break;
            case "btnClearfile":
                zDeleteFileBin();
                break;
            case "btnDeletfileMAS":
                zDeleteFileMAS();
                break;
            case "btnSave_summary":
                zSave_summary();
                if (Hddfld_status.Value == "summ_std")
                {
                    zSetMode(4);
                    ShowListHR_A();
                    SETFOCUS.Focus();
                }
                else
                {
                    zSetMode(7);
                    ShowListMD_A();
                    SETFOCUS.Focus();
                }
                break;
            case "btnsummary_back":
                if ((Hddfld_status.Value == "summ_std") || (Hddfld_status.Value == "summ_preview"))
                {
                    zSetMode(4);
                    ShowListHR_A();
                    SETFOCUS.Focus();
                }
                else
                {
                    zSetMode(7);
                    ShowListMD_A();
                    SETFOCUS.Focus();
                }
                break;
            case "btnCmdask_perm_ok":
                zSave_perm();
                ShowListHR_W();
                break;
            case "btnDetail_sumcoursepreview":
                _m0_training_idx = int.Parse(cmdArg);

                if (_m0_training_idx > 0)
                {
                    Hddfld_status.Value = "summ_preview";
                    zSetMode(10);
                    setActiveTab("HR_A");

                    zShowdata_SummaryDetail(_m0_training_idx, 0, "trn_course_all");
                    Literal litsupervisors_additional = (Literal)fv_summaryLearner.FindControl("litsupervisors_additional");
                    litsupervisors_additional.Visible = true;
                    _pnl_super = (Panel)fv_summaryLearner.FindControl("pnl_super");
                    _pnl_super.Visible = true;
                    _txtsupervisors_additional = (TextBox)fv_summaryLearner.FindControl("txtsupervisors_additional");
                    _txtsupervisors_additional.Visible = false;
                    btnSave_summary.Visible = false;

                }
                if (Hddfld_training_course_no.Value == "")
                {
                    zSetMode(4);
                }
                SETFOCUS.Focus();
                break;
            case "btnupdate_register":
                _m0_training_idx = int.Parse(cmdArg);
                if (_m0_training_idx > 0)
                {
                    zSave_perm_user(_m0_training_idx);
                }
                zbtnHR_Wait();
                SETFOCUS.Focus();
                break;
            case "_divbtn_elearning":
                string _div_idx = "";
                string[] _div_calc = new string[4];
                if (cmdArg != "0")
                {
                    _div_calc = cmdArg.Split('|');
                }
                else
                {
                    _div_calc = new string[4] { "", "", "", "" };
                }
                _div_idx = _div_calc[0];
                ViewState["u0_training_course_idx"] = _func_dmu.zStringToInt(_div_idx);
                ViewState["u3_training_course_idx"] = _func_dmu.zStringToInt(_div_calc[1]);
                ViewState["elearning_status"] = "student";
                checkdata_elaerning();
                break;
            case "btn_msok":
                if (ViewState["u0_training_course_idx"] != null)
                {
                    string txt_hidden_massage = Request.Form["txt_hidden_massage"];
                    if (txt_hidden_massage == "1")
                    {
                        getUrlTest(_func_dmu.zStringToInt(ViewState["u0_training_course_idx"].ToString()));
                        ViewState["elearning_status"] = "replay";
                    }
                    else
                    {
                        if (ViewState["elearning_status"].ToString() == "replay")
                        {
                            showdata_elaerning_replay(_func_dmu.zStringToInt(ViewState["u0_training_course_idx"].ToString()));
                        }
                        else
                        {
                            showdata_elaerning(_func_dmu.zStringToInt(ViewState["u0_training_course_idx"].ToString()));
                        }
                    }

                }

                break;
            case "btn_mscancel":
                if (ViewState["u0_training_course_idx"] != null)
                {
                    if (ViewState["elearning_status"].ToString() == "replay")
                    {
                        showdata_elaerning_replay(_func_dmu.zStringToInt(ViewState["u0_training_course_idx"].ToString()));
                    }
                    else
                    {
                        showdata_elaerning(_func_dmu.zStringToInt(ViewState["u0_training_course_idx"].ToString()));
                        if (hdd_statusvideo.Value == "0")
                        {
                            ViewState["elearning_status"] = "replay";
                            showdata_elaerning_replay(_func_dmu.zStringToInt(ViewState["u0_training_course_idx"].ToString()));
                        }
                    }

                }

                break;
            case "btn_menuleft":
                showdata_elaerning_std("first");
                break;
            case "btn_menuright":
                showdata_elaerning_std("next");
                break;
            case "btn_msresualt_conf":
                ViewState["elearning_status"] = "replay";
                setActiveTab("elearning");
                MultiViewBody.SetActiveView(View_elearning);
                showdata_elaerning_replay(_func_dmu.zStringToInt(ViewState["u0_training_course_idx"].ToString()));
                break;
            case "btn_msok_conf":
                getUrlTest(_func_dmu.zStringToInt(ViewState["u0_training_course_idx"].ToString()));
                break;

        }
    }
    #endregion btnCommand

    public void setpanel_approve(string _Str,
        int _md_app,
        int _empid,
        int _zresulte_app_status,
        int _zresulte_count,
        int _course_type_etraining = 0
        )
    {
        Panel pnlDetailApp;
        Panel pnlapprove_hr;
        Panel pnlApprove_HR_W;
        Panel pnlApprove_HR_A;
        Label lbtitle_preview;

        Panel pnlapprove_md;
        Panel pnlApprove_MD_W;
        Panel pnlApprove_MD_A;
        Panel pnlregister;

        pnlDetailApp = (Panel)fv_preview.FindControl("pnlDetailApp");
        pnlApprove_HR_W = (Panel)fv_preview.FindControl("pnlApprove_HR_W");
        pnlApprove_HR_A = (Panel)fv_preview.FindControl("pnlApprove_HR_A");
        lbtitle_preview = (Label)fv_preview.FindControl("lbtitle_preview");

        pnlApprove_MD_W = (Panel)fv_preview.FindControl("pnlApprove_MD_W");
        pnlApprove_MD_A = (Panel)fv_preview.FindControl("pnlApprove_MD_A");

        pnlapprove_hr = (Panel)fv_preview.FindControl("pnlapprove_hr");
        pnlapprove_md = (Panel)fv_preview.FindControl("pnlapprove_md");
        pnlregister = (Panel)fv_preview.FindControl("pnlregister");

        pnlDetailApp.Visible = false;

        pnlApprove_HR_W.Visible = false;
        pnlApprove_HR_A.Visible = false;

        pnlApprove_MD_W.Visible = false;
        pnlApprove_MD_A.Visible = false;

        pnlapprove_hr.Visible = false;
        pnlapprove_md.Visible = false;
        pnlregister.Visible = false;

        if (_Str == "btnDetail_GvMD_WList")
        {
            pnlDetailApp.Visible = true;
            pnlApprove_MD_W.Visible = true;
            lbtitle_preview.Text = lbMD_Wait.Text;
        }
        else if (_Str == "btnDetail_GvHR_AList")
        {
            pnlDetailApp.Visible = _func_dmu.zIntToBoolean(_md_app);
            pnlApprove_HR_A.Visible = true;
            pnlapprove_hr.Visible = true;
            if (Hddfld_status.Value == "trn_course_all")
            {
                lbtitle_preview.Text = "คอร์สอบรมที่มีสิทธิ์เข้าเรียน";
            }
            lbtitle_preview.Text = lbHR_App.Text;
            if (_course_type_etraining == 0) // ไม่ใช่ e-training ไม่ต้องขอลงทะเบียนไดๆ
            {
                pnlDetailApp.Visible = false;
            }
            

        }
        else if (_Str == "btnDetail_GvHR_WList")
        {
            pnlDetailApp.Visible = _func_dmu.zIntToBoolean(_md_app);
            pnlApprove_HR_W.Visible = true;
            pnlapprove_hr.Visible = true;
            if (Hddfld_status.Value == "no_register")
            {
                pnlDetailApp.Visible = false;
                lbtitle_preview.Text = "รายการที่ลงทะเบียนไม่ทันเวลา";
            }
            else if (Hddfld_status.Value == "open_course")
            {
                pnlDetailApp.Visible = false;
                lbtitle_preview.Text = "คอร์สอบรมที่เปิด";
                if (emp_idx != _empid)
                {
                    if ((_zresulte_app_status == 6) || (_zresulte_count == 0))
                    {
                        pnlApprove_HR_W.Visible = false;
                        pnlapprove_hr.Visible = false;
                        pnlDetailApp.Visible = true;
                        pnlregister.Visible = true;
                    }
                }

            }
            else if (Hddfld_status.Value == "trn_course_all")
            {
                pnlDetailApp.Visible = false;
                lbtitle_preview.Text = "คอร์สอบรมที่มีสิทธิ์เข้าเรียน";
            }
            else
            {
                pnlDetailApp.Visible = true;
                lbtitle_preview.Text = lbHR_Wait.Text;
            }
        }
        else if (_Str == "")
        {

        }
        else if (_Str == "")
        {

        }

    }

    private void zcrtl_btnback(string _mode)
    {
        btncancelApp_HR_W.Visible = false;
        btncancelApp_HR.Visible = false;
        btncancelApp_MD_W.Visible = false;
        btncancelApp_MD.Visible = false;
        Boolean _Boolean = true;
        if (_mode == "HRW")
        {
            btncancelApp_HR_W.Visible = _Boolean;
        }
        else if (_mode == "HRA")
        {
            btncancelApp_HR.Visible = _Boolean;
        }
        else if (_mode == "MDW")
        {
            btncancelApp_MD_W.Visible = _Boolean;
        }
        else if (_mode == "MDA")
        {
            btncancelApp_MD.Visible = _Boolean;
        }

    }

    public void zshowdetailtraining_plan()
    {

    }

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {

        if (sender is FormView)
        {
            var FvName = (FormView)sender;

        }

    }
    #endregion FvDetail_DataBound



    #region Check Session Redirect
    protected void checkSessionRedirect()
    {
        //if (Session["sesLoginIdxBackEnd"] == null)
        //{
        //   Response.Redirect(ResolveUrl("~/login"));
        //}
    }
    #endregion Check Session Redirect

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void FileUploadTrigger(FileUpload _FileUpload)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = _FileUpload.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }


    public Boolean getValue(string id = "0")
    {
        Boolean rBoolean = false;

        if (id == "1")
        {
            rBoolean = true;
        }

        return rBoolean;
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {

        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            var GvName = (GridView)sender;
            string sGvName = GvName.ID;
            if (sGvName == "GvHR_AList")
            {

            }

        }
    }

    public int getValueInt(string Str)
    {
        int i = _func_dmu.zStringToInt(Str);

        return i;
    }

    public string getValueStr(string Str, string sValue)
    {
        string _string = "";
        if ((Str == null) || Str == "")
        {
            _string = sValue;
        }
        else
        {
            _string = Str;
        }
        return _string;
    }

    public string getValueIntTOStr(Int32 Str, string sValue)
    {
        string _string = "";
        if ((Str == null))
        {
            _string = sValue;
        }
        else
        {
            _string = Str.ToString();
        }
        return _string;
    }

    public void showAlert(string Str)
    {
        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + Str + "');", true);
    }

    private void CreateDs_Clone()
    {
        string sDs = "dsclone";
        string sVs = "vsclone";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("zId_G", typeof(String));
        ds.Tables[sDs].Columns.Add("zId", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ViewState[sVs] = ds;

    }

    private void CreateDs_el_u1_training_course_lecturer()
    {
        string sDs = "dsel_u1_training_course_lecturer";
        string sVs = "vsel_u1_training_course_lecturer";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u1_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("lecturer_type", typeof(String));
        ds.Tables[sDs].Columns.Add("m0_institution_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ViewState[sVs] = ds;

    }
    private void CreateDs_el_u2_training_course_objective()
    {
        string sDs = "dsel_u2_training_course_objective";
        string sVs = "vsel_u2_training_course_objective";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u2_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("m0_objective_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ds.Tables[sDs].Columns.Add("pass_test_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("pass_test_per", typeof(String));
        ds.Tables[sDs].Columns.Add("hour_training_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("hour_training_per", typeof(String));
        ds.Tables[sDs].Columns.Add("write_report_training_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("publish_training_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("publish_training_description", typeof(String));
        ds.Tables[sDs].Columns.Add("course_lecturer_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("other_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("other_description", typeof(String));
        ds.Tables[sDs].Columns.Add("hrd_nofollow_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("hrd_follow_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("hrd_follow_day", typeof(String));
        ViewState[sVs] = ds;

    }
    private void CreateDs_el_u3_training_course_employee()
    {
        string sDs = "dsel_u3_training_course_employee";
        string sVs = "vsel_u3_training_course_employee";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u3_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("emp_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ds.Tables[sDs].Columns.Add("RDeptID", typeof(String));
        ds.Tables[sDs].Columns.Add("RDeptName", typeof(String));
        ds.Tables[sDs].Columns.Add("zPostName", typeof(String));
        ds.Tables[sDs].Columns.Add("zCostId", typeof(String));
        ds.Tables[sDs].Columns.Add("zCostCenter", typeof(String));
        ds.Tables[sDs].Columns.Add("zTel", typeof(String));
        ViewState[sVs] = ds;

    }
    private void CreateDs_el_u4_training_course_expenses()
    {
        string sDs = "dsel_u4_training_course_expenses";
        string sVs = "vsel_u4_training_course_expenses";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u4_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("expenses_description", typeof(String));
        // ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        //  ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("amount", typeof(String));
        ds.Tables[sDs].Columns.Add("vat", typeof(String));
        ds.Tables[sDs].Columns.Add("withholding_tax", typeof(String));
        ViewState[sVs] = ds;

    }
    private void CreateDs_el_u5_training_course_follow()
    {
        string sDs = "dsel_u5_training_course_follow";
        string sVs = "vsel_u5_training_course_follow";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("u5_training_course_idx", typeof(String));
        ds.Tables[sDs].Columns.Add("u0_training_course_idx_ref", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_status", typeof(String));
        ds.Tables[sDs].Columns.Add("training_course_updated_by", typeof(String));
        ds.Tables[sDs].Columns.Add("pass_test_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("pass_test_per", typeof(String));
        ds.Tables[sDs].Columns.Add("hour_training_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("hour_training_per", typeof(String));
        ds.Tables[sDs].Columns.Add("write_report_training_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("publish_training_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("publish_training_description", typeof(String));
        ds.Tables[sDs].Columns.Add("course_lecturer_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("other_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("other_description", typeof(String));
        ds.Tables[sDs].Columns.Add("hrd_nofollow_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("hrd_follow_flag", typeof(String));
        ds.Tables[sDs].Columns.Add("hrd_follow_day", typeof(String));
        ViewState[sVs] = ds;

    }
    private void CreateDs_el_CourseBudget()
    {
        string sDs = "dsel_coursebudget";
        string sVs = "vsel_coursebudget";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("costno", typeof(String));
        ViewState[sVs] = ds;

    }
    private void CreateDs_el_CourseBudgetDept()
    {
        string sDs = "dsel_coursebudgetdept";
        string sVs = "vsel_coursebudgetdept";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ViewState[sVs] = ds;

    }
    private void CreateDs_el_CourseBudget_dept()
    {
        string sDs = "dsel_coursebudget_dept";
        string sVs = "vsel_coursebudget_dept";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("costno", typeof(String));
        ds.Tables[sDs].Columns.Add("RDeptID", typeof(String));
        ViewState[sVs] = ds;

    }
    private void CreateDs_el_CourseBudgetDept_dept()
    {
        string sDs = "dsel_coursebudgetdept_dept";
        string sVs = "vsel_coursebudgetdept_dept";
        DataSet ds = new DataSet();
        ds.Tables.Add(sDs);
        ds.Tables[sDs].Columns.Add("id", typeof(String));
        ds.Tables[sDs].Columns.Add("zName", typeof(String));
        ViewState[sVs] = ds;

    }

    protected void onTextChanged(Object sender, EventArgs e)
    {
        if (sender is TextBox)
        {
            TextBox textbox = (TextBox)sender;
            if (textbox.ID == "txttraining_course_planbudget_total")
            {

            }
        }
    }
    private void zDelete(int id)
    {
        if (id == 0)
        {
            return;
        }
        training_course obj_training_course = new training_course();
        _data_elearning.el_training_course_action = new training_course[1];
        obj_training_course.u0_training_course_idx = id;
        obj_training_course.training_course_updated_by = emp_idx;
        _data_elearning.el_training_course_action[0] = obj_training_course;
        _data_elearning = _func_dmu.zCallServicePostNetwork(_urlDelel_u_plan_course, _data_elearning);
    }
    public string getTextDoc(int register_status = 0, string register_date = "",
                             int empidx = 0, int empidx_status = 0,
                             int signup_status = 0, string signup_date = "",
                             int test_scores_status = 0, string test_scores_date = "",
                             int summ_std_status = 0, string summ_std_date = "",
                             int summ_super_status = 0, string summ_super_date = "",
                             string zregister_date = "",
                             int zresulte_app_status = 0,
                             int zresulte_count = 0,
                             int zcourse_count = 0,
                             int course_type_elearning = 0,
                             int course_type_etraining = 0

        )
    {
        string text = string.Empty;


        if ((summ_std_status == 1))
        {
            text = "<span style='color:#26A65B;'> เรียนเสร็จแล้ว วันทีติดตามผล " + summ_std_date + " </span>";

        }
        else if ((signup_status == 1))
        {
            text = "<span style='color:#26A65B;'> กำลังเรียน วันทีลงชื่อ " + signup_date + " </span>";
            if (test_scores_status == 1)
            {
                text = "<span style='color:#26A65B;'> ทำข้อสอบเสร็จแล้ว </span>";
                //if (grade_status == 1)
                //{
                //    text = "<span style='color:#26A65B;'> ทำข้อสอบเสร็จแล้ว  </span>";
                //}
                //else
                //{
                //    text = "<span style='color:#26A65B;'> กำลังเรียน วันทีลงชื่อ " + signup_date + " </span>";
                //}
            }
            
        }
        else if ((register_status == 1))
        {
            text = "<span style='color:#26A65B;'> ลงทะเบียนเสร็จแล้ว วันที " + register_date + " </span>";
        }
        else if ((empidx_status == 1))
        {
            text = "<span style='color:#FF0000;'> ไม่ได้ลงทะเบียน </span>";
        }
        else if ((zresulte_app_status == 0) && (zresulte_count > 0))
        {
            text = "<span > ขอสิทธื์เข้าร่วมคอร์สอบรม </span>";
        }
        else if ((zresulte_app_status == 6) && (zresulte_count > 0))
        {
            text = "<span style='color:#FF0000;'> สิทธื์เข้าร่วมคอร์สอบรมไม่ผ่านการอนุมัติ </span>";
        }
        else if ((empidx != emp_idx) || (zcourse_count > 0))
        {
            text = "<span style='color:#FF0000;'> ไม่มีสิทธิ์ลงทะเบียน </span>";
        }
        else
        {
            if ((course_type_elearning + course_type_etraining) == 2)
            {
                text = "รอลงทะเบียน (กรณีที่เป็น e-Learning สามารถเรียนได้เลย)";
            }
            else if (course_type_elearning == 1)
            {
                text = "e-Learning สามารถเรียนได้เลย";
                if (test_scores_status == 1)
                {
                    // text = "";
                    text = "<span style='color:#26A65B;'> ทำข้อสอบเสร็จแล้ว </span>";
                }

            }
            else if (course_type_etraining == 1)
            {
                text = "รอลงทะเบียน";
                if (test_scores_status == 1)
                {
                    // text = "";
                    text = "<span style='color:#26A65B;'> ทำข้อสอบเสร็จแล้ว </span>";
                }
            }
            else
            {
                text = "รอลงทะเบียน";
            }

        }

        return text;
    }

    private int zShowdataHR_WDetail(int id, string _zstatus, string _flag)
    {
        int _int = 1;
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.emp_idx_ref = emp_idx;
        obj.u0_training_course_idx = id;
        obj.operation_status_id = "U0-LISTDATA-REGISTER";
        if ((Hddfld_status.Value == "no_register") ||
            (Hddfld_status.Value == "open_course"))
        {
            obj.zstatus = Hddfld_status.Value;
        }
        else
        {
            obj.zstatus = _zstatus;// "HR-W";
        }

        dataelearning.el_training_course_action[0] = obj;

        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));

        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetFormViewData(fv_preview, dataelearning.el_training_course_action);
        Repeater rptu8trncoursedate = (Repeater)fv_preview.FindControl("rptu8trncoursedate");
        //End SetMode
        Hddfld_training_course_no.Value = "";
        Hddfld_u0_training_course_idx.Value = "";
        // Hddfld_status.Value = "E";
        if (dataelearning.el_training_course_action != null)
        {

            foreach (var item in dataelearning.el_training_course_action)
            {
                Hddfld_training_course_no.Value = item.training_course_no;
                Hddfld_u0_training_course_idx.Value = item.u0_training_course_idx.ToString();
                Panel pnlDetailApp = (Panel)fv_preview.FindControl("pnlDetailApp");
                if (_flag == "HR")
                {
                    if ((_func_dmu.zStringToInt(item.register_status.ToString()) > 0))
                    {
                        _int = 0;
                    }
                }

                setU8StartEndDatetime(_func_dmu.zStringToInt(Hddfld_u0_training_course_idx.Value), rptu8trncoursedate);

            }
        }


        return _int;
    }

    public string getlecturer_type(int id, string _a, string _b)
    {
        string _string = "";
        if (id == 0)
        {
            _string = "ภายใน";
        }
        else
        {
            _string = "ภายนอก";
        }
        _string = _string + " " + _a + " " + _b;
        return _string;

    }
    private void zSaveApprove(int id, int id_u3_training_course_idx = 0)
    {
        if (id == 0)
        {
            return;
        }

        DropDownList ddlStatusapprove = (DropDownList)fv_preview.FindControl("ddlStatusapprove");
        TextBox txtdetailApprove = (TextBox)fv_preview.FindControl("txtdetailApprove");
        TextBox txtu3_training_course_idx = (TextBox)fv_preview.FindControl("txtu3_training_course_idx");

        training_course obj_training_course = new training_course();
        //traning_req U1
        _data_elearning.el_training_course_action = new training_course[1];
        obj_training_course.training_course_updated_by = emp_idx;
        obj_training_course.u0_training_course_idx = id;
        if (id_u3_training_course_idx == 0)
        {
            obj_training_course.u3_training_course_idx = _func_dmu.zStringToInt(txtu3_training_course_idx.Text);
            if (txtdetailApprove.Text.Trim().Length > 150)
            {
                obj_training_course.register_remark = txtdetailApprove.Text.Trim().Substring(0, 150);
            }
            else
            {
                obj_training_course.register_remark = txtdetailApprove.Text.Trim();
            }
            obj_training_course.register_status = _func_dmu.zStringToInt(ddlStatusapprove.SelectedValue);
        }
        else
        {
            obj_training_course.u3_training_course_idx = id_u3_training_course_idx;
            obj_training_course.register_remark = "";
            obj_training_course.register_status = 1;
        }
        obj_training_course.emp_idx_ref = emp_idx;
        obj_training_course.operation_status_id = "APPROVE-REGISTER";

        obj_training_course.approve_status = 1;
        obj_training_course.u0_idx = 32;
        obj_training_course.node_idx = 10;
        obj_training_course.actor_idx = 2;
        obj_training_course.app_flag = 1;
        obj_training_course.app_user = emp_idx;
        obj_training_course.approve_remark = obj_training_course.register_remark;

        obj_training_course.register_user = emp_idx;
        _data_elearning.el_training_course_action[0] = obj_training_course;
        //litDebug.Text = _func_dmu.zJson(_urlSetUpdel_u_plan_course, _data_elearning);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));

        _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan_course, _data_elearning);




    }
    private void zSaveApproveMD(int id)
    {
        if (id == 0)
        {
            return;
        }

        DropDownList ddlStatusapprove = (DropDownList)fv_preview.FindControl("ddlStatusapprove_md");
        TextBox txtdetailApprove = (TextBox)fv_preview.FindControl("txtdetailApprove_md");

        training_course obj_training_course = new training_course();
        //traning_req U1
        _data_elearning.el_training_course_action = new training_course[1];
        obj_training_course.training_course_updated_by = emp_idx;
        obj_training_course.u0_training_course_idx = id;
        obj_training_course.operation_status_id = "APPROVE-MD";
        if (txtdetailApprove.Text.Trim().Length > 150)
        {
            obj_training_course.md_approve_remark = txtdetailApprove.Text.Trim().Substring(0, 150);
        }
        else
        {
            obj_training_course.md_approve_remark = txtdetailApprove.Text.Trim();
        }

        //node
        if (ddlStatusapprove.SelectedValue == "4")//อนุมัติ
        {
            obj_training_course.md_u0_idx = 22;
        }
        else if (ddlStatusapprove.SelectedValue == "5")//กลับไปแก้ไข
        {
            obj_training_course.md_u0_idx = 23;
        }
        else if (ddlStatusapprove.SelectedValue == "6")//ไม่อนุมัติ
        {
            obj_training_course.md_u0_idx = 24;
        }
        obj_training_course.md_approve_status = _func_dmu.zStringToInt(ddlStatusapprove.SelectedValue);

        obj_training_course.md_node_idx = 2;
        obj_training_course.md_actor_idx = 4;
        obj_training_course.md_app_flag = 1;
        obj_training_course.md_app_user = emp_idx;
        _data_elearning.el_training_course_action[0] = obj_training_course;
        // litDebug.Text = _func_dmu.zJson(_urlSetInsel_u0_training_req, _data_elearning);
        // return;
        _func_dmu.zCallServicePostNetwork(_urlSetUpdel_u_plan_course, _data_elearning);


    }

    public string getStrformate(string str, int iformat)
    {
        str = _func_dmu.zFormatfloat(str, iformat);
        return str;
    }


    public string course_plan_status_name(string staus)
    {
        string SetName = "";
        if (staus == "I")
        {
            SetName = "In Plan";
        }
        else if (staus == "O")
        {
            SetName = "Out Plan";
        }

        return SetName;
    }

    public string getImageIO(string _img, int _i)
    {
        string SetName = "";
        if (_i == 1)
        {
            if (_img == "I")
            {
                SetName = ResolveUrl("~/images/elearning/inplan.png");
            }
            else if (_img == "O")
            {
                SetName = ResolveUrl("~/images/elearning/outplan.png");
            }
        }
        else
        {
            if (_img == "I")
            {
                SetName = "In Plan";
            }
            else if (_img == "O")
            {
                SetName = "Out Plan";
            }
        }

        return SetName;
    }

    public string getlabelSave()
    {
        string SetName = "";
        if (Hddfld_training_course_no.Value != "")
        {
            SetName = "บันทึกการเปลี่ยนแปลง";
        }
        else
        {
            SetName = "บันทึก";
        }
        SetName = "บันทึก";
        return SetName;
    }
    public string DownloadFile(string sDocNo)
    {
        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
        _PathFile = _PathFile + _Folder_plan_course + "/" + sDocNo + "/";
        string sFileName = "", filePath = "";
        if (sDocNo == "")
        {

        }
        else
        {



            try
            {

                if (_PathFile != "")
                {
                    string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile));
                    List<ListItem> files = new List<ListItem>();
                    foreach (string file in filesLoc)
                    {
                        try
                        {
                            sFileName = Path.GetFileName(file);
                            filePath = file;
                        }
                        catch { }
                    }
                }
            }
            catch { }
        }
        if (sFileName != "")
        {
            filePath = _PathFile + sFileName;
            filePath = ResolveUrl(filePath);
        }
        else
        {
            filePath = "";
        }
        return filePath;

    }

    // start get value preview //
    public string getValue_plan_status(string Str)
    {
        string sName = "";
        if (Str == "I")
        {
            sName = "In Plan";
        }
        else if (Str == "O")
        {
            sName = "Out Plan";
        }
        return sName;
    }
    public string getValue_course_type(int Str)
    {
        string sName = "";
        if (Str.ToString() == "1")
        {
            sName = "คอร์สตามผู้ทำรายการ";
        }
        else if (Str.ToString() == "2")
        {
            sName = "คอร์สตามแผนก";
        }
        else if (Str.ToString() == "3")
        {
            sName = "คอร์สตามผู้เรียน";
        }
        return sName;
    }

    public string getValue_place(int Str)
    {
        string sName = "";
        if (Str.ToString() == "3")
        {
            sName = "นพวงศ์";
        }
        else if (Str.ToString() == "4")
        {
            sName = "โรจนะ";
        }
        else if (Str.ToString() == "5")
        {
            sName = "เมืองทองธานี";
        }
        else
        {
            sName = "ภายนอก";
        }
        return sName;
    }
    public string getValue_lecturer_type(int Str)
    {
        string sName = "";
        if (Str.ToString() == "0")
        {
            sName = "ภายใน";
        }
        else if (Str.ToString() == "1")
        {
            sName = "ภายนอก";
        }
        return sName;
    }
    public string getValue_planbudget_type(int Str)
    {
        string sName = "";
        if (Str.ToString() == "1")
        {
            sName = "มีค่าใช้จ่ายอยู่ในงบประมาณ";
        }
        else if (Str.ToString() == "2")
        {
            sName = "เกินงบประมาณ";
        }
        else if (Str.ToString() == "3")
        {
            sName = "ฟรี";
        }
        return sName;

    }
    public string getformatfloat(string Str, int i)
    {
        return _func_dmu.zFormatfloat(Str, i);
    }

    // end get value preview //

    private void ShowListsyllabus() //หลักสูตรที่จะต้องเรียน
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_report_plan_action = new training_rpt_plan[1];
        training_rpt_plan obj = new training_rpt_plan();
        obj.filter_keyword = txtFilterKeyword_syllabus.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        obj.zyear = _func_dmu.zStringToInt(ddlYearSearch_syllabus.SelectedValue);
        obj.emp_idx = emp_idx;
        // obj.zstatus = "REGISTER-A";
        obj.operation_status_id = "report_syllabus";
        dataelearning.el_report_plan_action[0] = obj;
        //litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));
        //litDebug.Text = _urlGetel_Report_plan_course + _funcTool.convertObjectToJson(dataelearning);
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_Report_plan_course, dataelearning);
        _func_dmu.zSetGridData(GvsyllabusList, dataelearning.el_report_plan_action);
        ShowListHR_W_Status();
    }
    private void ShowDetailsyllabus(int id)
    {
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_report_plan_action = new training_rpt_plan[1];
        training_rpt_plan obj = new training_rpt_plan();
        // obj.emp_idx = emp_idx;
        obj.zId_G = id;
        obj.operation_status_id = "report_syllabus";
        dataelearning.el_report_plan_action[0] = obj;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_Report_plan_course, dataelearning);
        _func_dmu.zSetFormViewData(FvDetailsyllabus, dataelearning.el_report_plan_action);

        _GvCourse = (GridView)FvDetailsyllabus.FindControl("GvCourse");
        data_elearning dataelearning_u4 = new data_elearning();
        trainingLoolup obj_u4 = new trainingLoolup();
        dataelearning_u4.trainingLoolup_action = new trainingLoolup[1];
        obj_u4.operation_status_id = "TRN-COURSE";
        obj_u4.idx = id;
        dataelearning_u4.trainingLoolup_action[0] = obj_u4;
        dataelearning_u4 = _func_dmu.zCallServiceNetwork(_func_dmu._urlGetel_lu_TraningAll, dataelearning_u4);
        _func_dmu.zSetGridData(_GvCourse, dataelearning_u4.trainingLoolup_action);

        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_Detailsyllabus();", true);

    }
    protected string getCourseType(int course_type_etraining, int course_type_elearning)
    {
        string a = "";
        if (course_type_etraining > 0)
        {
            a = "ClassRoom";
        }
        string b = "";
        if (course_type_elearning > 0)
        {
            b = "e-Learning";
        }
        string sValue = "";
        if ((a != "") && (b != ""))
        {
            sValue = a + " / " + b;
        }
        else if (a != "")
        {
            sValue = a;
        }
        else if (b != "")
        {
            sValue = b;
        }
        return sValue;
    }
    protected string getpriority(int status)
    {

        if (status == 1)
        {
            return "<span>Must</span>";
        }
        else if (status == 2)
        {
            return "<span>Need</span>";
        }
        else if (status == 3)
        {
            return "<span>Want</span>";
        }
        else
        {
            return "<span style='font-weight:700;'>-</span>";
        }
    }
    protected string getpriorityRemark(int status, string sRemark)
    {

        if (status == 1)
        {
            return "<span>Must</span> : " + sRemark;
        }
        else if (status == 2)
        {
            return "<span>Need</span> : " + sRemark;
        }
        else if (status == 3)
        {
            return "<span>Want</span> : " + sRemark;
        }
        else
        {
            return "<span style='font-weight:700;'>-</span>";
        }
    }
    protected string getcourse_assessment(int id = 0)
    {

        if (id == 1)
        {
            return "<span>ประเมิน</span>";
        }
        else
        {
            return "<span>ไม่ประเมิน</span>";
        }
    }
    protected void setStatusData(string sValue)
    {
        if (sValue == "btnHR_Wait")
        {
            Hddfld_status.Value = "REGISTER-W";// "register";
        }
        else if (sValue == "btnHR_App")
        {
            Hddfld_status.Value = "trn_course_all"; //register_complete
        }
        else if (sValue == "btnnoregister")
        {
            Hddfld_status.Value = "no_register";
        }
        else if (sValue == "btnsyllabus")
        {
            Hddfld_status.Value = "syllabus";
        }
        else if (sValue == "btnopen_course")
        {
            Hddfld_status.Value = "open_course";
        }
        else if (sValue == "btnDetail_sumcourseList")
        {
            Hddfld_status.Value = "summ_std";
        }
        else if (sValue == "btnDetail_GvMD_AList")
        {
            Hddfld_status.Value = "summ_super";
        }

    }

    // start scheduler  

    public string MonthTH(int AMonth)
    {
        return _func_dmu.zMonthTH(AMonth);
    }

    public string getHtmlSched()
    {

        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_plan_action = new training_plan[1];
        training_plan obj_trn_plan = new training_plan();
        obj_trn_plan.filter_keyword = txtFilterKeyword_SCHED.Text;
        //obj.zmonth = int.Parse(ddlMonthSearch_TrnNSur.SelectedValue);
        obj_trn_plan.zyear = _func_dmu.zStringToInt(ddlYearSearch_SCHED.SelectedValue);
        obj_trn_plan.u0_course_idx_ref = _func_dmu.zStringToInt(ddltrn_groupSearch_SCHED.SelectedValue);

        // obj.zstatus = "MD-A";
        obj_trn_plan.operation_status_id = "U0-LISTDATA-SCHED";
        dataelearning.el_training_plan_action[0] = obj_trn_plan;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_plan, dataelearning);
        //_func_dmu.zSetRepeaterData(repMonths, _data_elearning.el_training_plan_action);
        string sName = "", sHtml = "", sHtml1 = "", sHtml_Body1 = "";

        sHtml = "";
        if (dataelearning.el_training_plan_action != null)
        {
            foreach (var item in dataelearning.el_training_plan_action)
            {
                sHtml_Body1 = "<td style=\"width: 80px;padding: 5px 0px;\"> </td>";
                sHtml1 = "";
                //im_total = 0;
                //ic = 0;
                int im1 = 0, im2 = 0, im3 = 0, im4 = 0, im5 = 0
            , im6 = 0, im7 = 0, im8 = 0
            , im9 = 0, im10 = 0, im11 = 0, im12 = 0
            , im1_data = 0, im2_data = 0, im3_data = 0
            , im4_data = 0, im5_data = 0
            , im6_data = 0, im7_data = 0, im8_data = 0
            , im9_data = 0, im10_data = 0, im11_data = 0
            , im12_data = 0
            , im_total = 0, ic = 0
            ;
                sName = item.course_name;
                if (item.training_plan_m1 > 0)
                {
                    im1 = 1;
                    im1_data = item.training_plan_m1;
                }
                if (item.training_plan_m2 > 0)
                {
                    im2 = 1;
                    im2_data = item.training_plan_m2;
                }
                if (item.training_plan_m3 > 0)
                {
                    im3 = 1;
                    im3_data = item.training_plan_m3;
                }
                if (item.training_plan_m4 > 0)
                {
                    im4 = 1;
                    im4_data = item.training_plan_m4;
                }
                if (item.training_plan_m5 > 0)
                {
                    im5 = 1;
                    im5_data = item.training_plan_m5;
                }
                if (item.training_plan_m6 > 0)
                {
                    im6 = 1;
                    im6_data = item.training_plan_m6;
                }
                if (item.training_plan_m7 > 0)
                {
                    im7 = 1;
                    im7_data = item.training_plan_m7;
                }
                if (item.training_plan_m8 > 0)
                {
                    im8 = 1;
                    im8_data = item.training_plan_m8;
                }
                if (item.training_plan_m9 > 0)
                {
                    im9 = 1;
                    im9_data = item.training_plan_m9;
                }
                if (item.training_plan_m10 > 0)
                {
                    im10 = 1;
                    im10_data = item.training_plan_m10;
                }
                if (item.training_plan_m11 > 0)
                {
                    im11 = 1;
                    im11_data = item.training_plan_m11;
                }
                if (item.training_plan_m12 > 0)
                {
                    im12 = 1;
                    im12_data = item.training_plan_m12;
                }
                if (im1 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im2 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im3 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im4 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im5 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im6 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im7 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im8 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im9 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im10 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im11 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im12 > 0)
                {
                    im_total++;
                    ic++;
                }
                else
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                    sHtml1 = sHtml1 + sHtml_Body1;
                }
                if (im_total > 0)
                {
                    sHtml1 = sHtml1 + getHtml(im_total, sName, item.u0_training_plan_idx, item.training_group_color);
                    im_total = 0;
                }
                sHtml1 = "<tr>" + sHtml1 + "</tr>";
                sHtml = sHtml + sHtml1;
            }
        }

        return sHtml;
    }
    public string getHtml(int icolspan = 0, string sdata = "", int id = 0, string sColor = "")
    {
        if (sdata == null)
        {
            sdata = "";
        }
        string sName = sdata;
        if (sdata.Length > 8)
        {
            sdata = _func_dmu.zTruncate(sdata, icolspan * 8);
        }
        int iwidth = 90 * icolspan;
        if (icolspan >= 4)
        {
            iwidth = 93 * icolspan;
        }

        string sHtml_Body = " <td  style=\"padding: 5px 0px;\" colspan=" + icolspan.ToString() + "> " +
                          " <center>" +
            " <input id=\"btn_sched" + id.ToString() + "\" type=\"button\" value=\"" + sdata + "\" class=\"pull-left\" style=\"background-color: " + sColor + ";" +
        " border: none;" +
   "      color: white;" +
    "     text-align: center;" +
    "     text-decoration: none;" +
     "    display: inline-block;" +
     "      padding: 5px 5px;" +
      "   font-size: 14px;" +
    "  width: 100%;   " +
     "    cursor: pointer;\" " +
     "   onclick=\"show_register(" + id.ToString() + ")\"  " +
     " />" +

      "</center>" +
                             " </td>";

        //  " data-toggle=\"tooltip\" title=\"" + sName + "\" " +
        if (icolspan == 0)
        {
            sHtml_Body = "";
        }
        return sHtml_Body;
    }


    [WebMethod]
    public static dstrainingplan[] get_register(string id, string emp_id)
    {
        //string id = Request.Form["HiddenReportId"];
        List<dstrainingplan> _dstrainingplan = new List<dstrainingplan>();
        using (SqlConnection conn = new SqlConnection())
        {

            conn.ConnectionString = ConfigurationManager
                    .ConnectionStrings["conn_mas"].ConnectionString;
            using (SqlCommand cmd = new SqlCommand())
            {
                StringBuilder query = new StringBuilder();
                query.Clear();
                query.AppendLine("SELECT u0_training_plan_idx ");
                query.AppendLine(", training_plan_no ");
                query.AppendLine(", CONVERT(VARCHAR(30), training_plan_date, 103) as training_plan_date ");
                query.AppendLine(", training_plan_year ");
                query.AppendLine(", u0_course_idx_ref ");
                query.AppendLine(", lecturer_type ");
                query.AppendLine(", m0_institution_idx_ref ");
                query.AppendLine(", m0_target_group_idx_ref ");
                query.AppendLine(", training_plan_qty ");
                query.AppendLine(", training_plan_amount ");
                query.AppendLine(", training_plan_model ");
                query.AppendLine(", training_plan_m1 ");
                query.AppendLine(", training_plan_m2 ");
                query.AppendLine(", training_plan_m3 ");
                query.AppendLine(", training_plan_m4 ");
                query.AppendLine(", training_plan_m5 ");
                query.AppendLine(", training_plan_m6 ");
                query.AppendLine(", training_plan_m7 ");
                query.AppendLine(", training_plan_m8 ");
                query.AppendLine(", training_plan_m9 ");
                query.AppendLine(", training_plan_m10 ");
                query.AppendLine(", training_plan_m11 ");
                query.AppendLine(", training_plan_m12 ");
                query.AppendLine(", training_plan_budget ");
                query.AppendLine(", training_plan_costperhead ");
                query.AppendLine(", training_plan_status ");
                query.AppendLine(", training_plan_created_by ");
                query.AppendLine(", training_plan_created_at ");
                query.AppendLine(", training_plan_updated_by ");
                query.AppendLine(", training_plan_updated_at ");
                query.AppendLine(", isnull(a.priority_name, '') + '  ' + isnull(a.course_name, '') course_name ");
                query.AppendLine(", training_group_name ");
                query.AppendLine(", training_branch_name ");
                query.AppendLine(", priority_name ");
                query.AppendLine(", target_group_name ");
                query.AppendLine(", approve_status ");
                query.AppendLine(", u0_idx ");
                query.AppendLine(", node_idx ");
                query.AppendLine(", actor_idx ");
                query.AppendLine(", approve_remark ");
                query.AppendLine(", decision_name ");
                query.AppendLine(", node_name ");
                query.AppendLine(", actor_name ");
                query.AppendLine(", md_approve_status ");
                query.AppendLine(", md_approve_remark ");
                query.AppendLine(", md_u0_idx ");
                query.AppendLine(", md_node_idx ");
                query.AppendLine(", md_actor_idx ");
                query.AppendLine(", md_decision_name ");
                query.AppendLine(", md_node_name ");
                query.AppendLine(", md_actor_name ");
                query.AppendLine(", target_group_name ");
                query.AppendLine(",case when lecturer_type = 0 then ");
                query.AppendLine("(select  top 1 emp.FullNameTH from Centralized.dbo.ViewEmployee emp ");
                query.AppendLine("where 1 = 1 ");
                query.AppendLine("and emp.EmpStatus = 1 ");
                query.AppendLine("and emp.EmpIDX = a.m0_institution_idx_ref ");
                query.AppendLine(") ");
                query.AppendLine("else  ");
                query.AppendLine("(select  top 1 m0_t.institution_name from el_m0_institution m0_t ");
                query.AppendLine("where 1 = 1 ");
                query.AppendLine("and m0_t.institution_status = 1 ");
                query.AppendLine("and m0_t.m0_institution_idx = a.m0_institution_idx_ref ");
                query.AppendLine(") ");
                query.AppendLine("end institution_name ");
                query.AppendLine(",a.course_remark ");
                query.AppendLine(",a.level_name ");
                query.AppendLine(",a.course_score ");
                query.AppendLine(",a.course_type_etraining ");
                query.AppendLine(",a.course_type_elearning ");
                query.AppendLine(",a.priority_name ");
                query.AppendLine(",a.priority_remark ");
                query.AppendLine(",isnull(a.course_assessment,0) as course_assessment ");
                query.AppendLine(",a.u2_course_test_item ");
                query.AppendLine(",a.u3_course_evaluation_item ");
                query.AppendLine(",isnull((select COUNT(*)");
                query.AppendLine("from View_el_u0_training_course tc_u0, el_u3_training_course_employee tc_u3_emp");
                query.AppendLine("where 1 = 1");
                query.AppendLine("and tc_u3_emp.training_course_status = 1");
                query.AppendLine("and tc_u0.training_course_status = 1");
                query.AppendLine("and isnull(tc_u0.approve_status, 0) = 4");
                query.AppendLine("and isnull(tc_u0.md_approve_status, 0) = 4");
                query.AppendLine("and tc_u3_emp.u0_training_course_idx_ref = tc_u0.u0_training_course_idx");
                query.AppendLine("and tc_u3_emp.emp_idx_ref > 0");
                query.AppendLine("and a.u0_training_plan_idx = tc_u0.u0_training_plan_idx_ref");
                query.AppendLine("and tc_u3_emp.emp_idx_ref = " + emp_id);
                query.AppendLine("),0) as ztem_trn_plan");
                query.AppendLine(", isnull((select top 1 tc_u3_emp.register_status");
                query.AppendLine("from View_el_u0_training_course tc_u0, el_u3_training_course_employee tc_u3_emp");
                query.AppendLine("where 1 = 1");
                query.AppendLine("and tc_u3_emp.training_course_status = 1");
                query.AppendLine("and tc_u0.training_course_status = 1");
                query.AppendLine("and isnull(tc_u0.approve_status, 0) = 4");
                query.AppendLine("and isnull(tc_u0.md_approve_status, 0) = 4");
                query.AppendLine("and tc_u3_emp.u0_training_course_idx_ref = tc_u0.u0_training_course_idx");
                query.AppendLine("and tc_u3_emp.emp_idx_ref > 0");
                query.AppendLine("and a.u0_training_plan_idx = tc_u0.u0_training_plan_idx_ref");
                query.AppendLine("and tc_u3_emp.emp_idx_ref = " + emp_id);
                query.AppendLine("),0) as zregister_status");
                query.AppendLine(", isnull((select top 1 tc_u3_emp.u3_training_course_idx ");
                query.AppendLine("from View_el_u0_training_course tc_u0, el_u3_training_course_employee tc_u3_emp");
                query.AppendLine("where 1 = 1");
                query.AppendLine("and tc_u3_emp.training_course_status = 1");
                query.AppendLine("and tc_u0.training_course_status = 1");
                query.AppendLine("and isnull(tc_u0.approve_status, 0) = 4");
                query.AppendLine("and isnull(tc_u0.md_approve_status, 0) = 4");
                query.AppendLine("and tc_u3_emp.u0_training_course_idx_ref = tc_u0.u0_training_course_idx");
                query.AppendLine("and tc_u3_emp.emp_idx_ref > 0");
                query.AppendLine("and a.u0_training_plan_idx = tc_u0.u0_training_plan_idx_ref");
                query.AppendLine("and tc_u3_emp.emp_idx_ref = " + emp_id);
                query.AppendLine("),0) as zu3_training_course_idx");
                query.AppendLine(", isnull((select top 1 tc_u0.u0_training_course_idx ");
                query.AppendLine("from View_el_u0_training_course tc_u0 ");
                query.AppendLine("where 1 = 1");
                query.AppendLine("and tc_u0.training_course_status = 1");
                query.AppendLine("and isnull(tc_u0.approve_status, 0) = 4");
                query.AppendLine("and isnull(tc_u0.md_approve_status, 0) = 4");
                query.AppendLine("and a.u0_training_plan_idx = tc_u0.u0_training_plan_idx_ref");
                query.AppendLine("),0) as zu0_training_course_idx");
                query.AppendLine(", (select top 1 CONVERT(VARCHAR(30), tc_u3_emp.register_date , 103)+' '+ CONVERT(VARCHAR(30), tc_u3_emp.register_date , 108) ");
                query.AppendLine("from View_el_u0_training_course tc_u0, el_u3_training_course_employee tc_u3_emp");
                query.AppendLine("where 1 = 1");
                query.AppendLine("and tc_u3_emp.training_course_status = 1");
                query.AppendLine("and tc_u0.training_course_status = 1");
                query.AppendLine("and isnull(tc_u0.approve_status, 0) = 4");
                query.AppendLine("and isnull(tc_u0.md_approve_status, 0) = 4");
                query.AppendLine("and tc_u3_emp.u0_training_course_idx_ref = tc_u0.u0_training_course_idx");
                query.AppendLine("and tc_u3_emp.emp_idx_ref > 0");
                query.AppendLine("and a.u0_training_plan_idx = tc_u0.u0_training_plan_idx_ref");
                query.AppendLine("and tc_u3_emp.emp_idx_ref = " + emp_id);
                query.AppendLine(") as zregister_date");
                query.AppendLine(",isnull( (select top 1 tc_u3_emp.resulte_app_status ");
                query.AppendLine("from View_el_u0_training_course tc_u0, el_u7_training_course_resulte tc_u3_emp");
                query.AppendLine("where 1 = 1");
                query.AppendLine("and tc_u3_emp.training_course_status = 1");
                query.AppendLine("and tc_u0.training_course_status = 1");
                query.AppendLine("and isnull(tc_u0.approve_status, 0) = 4");
                query.AppendLine("and isnull(tc_u0.md_approve_status, 0) = 4");
                query.AppendLine("and tc_u3_emp.u0_training_course_idx_ref = tc_u0.u0_training_course_idx");
                query.AppendLine("and tc_u3_emp.emp_idx_ref > 0");
                query.AppendLine("and a.u0_training_plan_idx = tc_u0.u0_training_plan_idx_ref");
                query.AppendLine("and tc_u3_emp.emp_idx_ref = " + emp_id);
                query.AppendLine("order by tc_u3_emp.u7_training_course_idx  desc");
                query.AppendLine("),0) as zresulte_app_status");
                query.AppendLine(",isnull( (select count(*) ");
                query.AppendLine("from View_el_u0_training_course tc_u0, el_u7_training_course_resulte tc_u3_emp");
                query.AppendLine("where 1 = 1");
                query.AppendLine("and tc_u3_emp.training_course_status = 1");
                query.AppendLine("and tc_u0.training_course_status = 1");
                query.AppendLine("and isnull(tc_u0.approve_status, 0) = 4");
                query.AppendLine("and isnull(tc_u0.md_approve_status, 0) = 4");
                query.AppendLine("and tc_u3_emp.u0_training_course_idx_ref = tc_u0.u0_training_course_idx");
                query.AppendLine("and tc_u3_emp.emp_idx_ref > 0");
                query.AppendLine("and a.u0_training_plan_idx = tc_u0.u0_training_plan_idx_ref");
                query.AppendLine("and tc_u3_emp.emp_idx_ref = " + emp_id);
                query.AppendLine("),0) as zresulte_count");
                query.AppendLine(",(select count(*) from View_el_u4_course v_u4 ");
                query.AppendLine("where 1 = 1 ");
                query.AppendLine("and v_u4.u0_course_idx_ref = a.u0_course_idx_ref ");
                query.AppendLine("and not exists(select * from View_el_u4_course_pass_grade v_u4_pass ");
                query.AppendLine("where 1 = 1 ");
                query.AppendLine("and v_u4.u0_course_idx_ref = v_u4_pass.u0_course_idx_ref ");
                query.AppendLine("and v_u4.u0_course_idx_ref_pass = v_u4_pass.u0_course_idx ");
                query.AppendLine("and v_u4_pass.emp_idx_ref = " + emp_id);
                query.AppendLine(")");
                query.AppendLine(") as zcourse_count");
                query.AppendLine("FROM View_el_u0_training_plan a");
                query.AppendLine(" where 1=1 ");
                query.AppendLine(" and u0_training_plan_idx = " + id);

                cmd.CommandText = query.ToString();
                cmd.Connection = conn;
                conn.Open();
                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {

                        dstrainingplan dstrainingplan_row = new dstrainingplan();
                        dstrainingplan_row.emp_id = emp_id;
                        dstrainingplan_row.ztem_trn_plan = sdr["ztem_trn_plan"].ToString();
                        dstrainingplan_row.register_status = sdr["zregister_status"].ToString();
                        dstrainingplan_row.u3_training_course_idx = sdr["zu3_training_course_idx"].ToString();
                        dstrainingplan_row.u0_training_course_idx = sdr["zu0_training_course_idx"].ToString();
                        dstrainingplan_row.u0_training_plan_idx = sdr["u0_training_plan_idx"].ToString();
                        dstrainingplan_row.training_plan_no = sdr["training_plan_no"].ToString();
                        dstrainingplan_row.training_plan_no = sdr["training_plan_no"].ToString();
                        dstrainingplan_row.training_plan_date = sdr["training_plan_date"].ToString();
                        dstrainingplan_row.course_name = sdr["course_name"].ToString();
                        dstrainingplan_row.training_group_name = sdr["training_group_name"].ToString();
                        dstrainingplan_row.training_branch_name = sdr["training_branch_name"].ToString();
                        dstrainingplan_row.institution_name = sdr["institution_name"].ToString();
                        dstrainingplan_row.training_course_remark = sdr["course_remark"].ToString();
                        dstrainingplan_row.level_name = sdr["level_name"].ToString();
                        dstrainingplan_row.course_score_name = string.Format("{0:n2}", int.Parse(sdr["course_score"].ToString()));
                        dstrainingplan_row.zcourse_count = int.Parse(sdr["zcourse_count"].ToString());
                        dstrainingplan_row.course_type_etraining = int.Parse(sdr["course_type_etraining"].ToString());

                        string a = "";
                        int course_type_etraining = 0, course_type_elearning = 0;
                        course_type_etraining = int.Parse(sdr["course_type_etraining"].ToString());
                        course_type_elearning = int.Parse(sdr["course_type_elearning"].ToString());
                        if (course_type_etraining > 0)
                        {
                            a = "ClassRoom";
                        }
                        string b = "";
                        if (course_type_elearning > 0)
                        {
                            b = "e-Learning";
                        }
                        string sValue = "";
                        if ((a != "") && (b != ""))
                        {
                            sValue = a + " / " + b;
                        }
                        else if (a != "")
                        {
                            sValue = a;
                        }
                        else if (b != "")
                        {
                            sValue = b;
                        }
                        dstrainingplan_row.course_type = sValue;
                        dstrainingplan_row.priority_name = sdr["priority_name"].ToString() + " : " + sdr["priority_remark"].ToString();
                        b = "";
                        if (int.Parse(sdr["course_assessment"].ToString()) == 1)
                        {
                            b = "ประเมิน";
                        }
                        else
                        {
                            b = "ไม่ประเมิน";
                        }
                        dstrainingplan_row.course_assessment = b;

                        b = "";
                        if (int.Parse(sdr["u2_course_test_item"].ToString()) > 0)
                        {
                            b = string.Format("{0:n0}", int.Parse(sdr["u2_course_test_item"].ToString())) + " ข้อ ";
                        }
                        else
                        {
                            b = "-";
                        }
                        dstrainingplan_row.u2_course_test_name = b;
                        b = "";
                        if (int.Parse(sdr["u3_course_evaluation_item"].ToString()) > 0)
                        {
                            b = string.Format("{0:n0}", int.Parse(sdr["u3_course_evaluation_item"].ToString())) + " ข้อ ";
                        }
                        else
                        {
                            b = "-";
                        }
                        dstrainingplan_row.u3_course_evaluation_name = b;
                        b = "";
                        if (int.Parse(sdr["u3_course_evaluation_item"].ToString()) > 0)
                        {
                            b = string.Format("{0:n0}", int.Parse(sdr["u3_course_evaluation_item"].ToString())) + " ข้อ ";
                        }
                        else
                        {
                            b = "-";
                        }
                        dstrainingplan_row.u3_course_evaluation_name = b;
                        b = "";
                        if (int.Parse(sdr["zregister_status"].ToString()) == 1)
                        {
                            b = "ลงทะเบียนเสร็จแล้ว วันที " + sdr["zregister_date"].ToString();
                        }
                        else if (int.Parse(sdr["zu0_training_course_idx"].ToString()) == 0)
                        {
                            b = "ยังไม่เปิดคอร์สอบรม ";
                        }
                        else if (int.Parse(sdr["zcourse_count"].ToString()) > 0)
                        {
                            b = "ไม่มีสิทธิ์ลงทะเบียน ";
                        }
                        else if (int.Parse(sdr["zresulte_app_status"].ToString()) == 0)
                        {
                            // b = "ขอสิทธื์เข้าร่วมคอร์สอบรม ";
                            if ((course_type_elearning + course_type_etraining) == 2)
                            {
                                b = "ขอสิทธื์เข้าร่วมคอร์สอบรม (กรณีที่เป็น e-Learning สามารถเรียนได้เลย)";
                            }
                            else if (course_type_elearning == 1)
                            {
                                b = "e-Learning สามารถเรียนได้เลย";
                            }
                            else if (course_type_etraining == 1)
                            {
                                b = "ขอสิทธื์เข้าร่วมคอร์สอบรม ";
                            }
                            else
                            {
                                b = "ขอสิทธื์เข้าร่วมคอร์สอบรม ";
                            }
                        }
                        else if (int.Parse(sdr["zresulte_app_status"].ToString()) == 6)
                        {
                            b = "สิทธื์เข้าร่วมคอร์สอบรมไม่ผ่านการอนุมัติ ";
                        }
                        else
                        {
                            b = "";
                        }
                        dstrainingplan_row.zresulte_app_status = sdr["zresulte_app_status"].ToString();
                        dstrainingplan_row.status_name = b;
                        dstrainingplan_row.zresulte_count = sdr["zresulte_count"].ToString();

                        _dstrainingplan.Add(dstrainingplan_row);

                    }
                }

                conn.Close();
            }

        }
        return _dstrainingplan.ToArray();
        //return sStr;

    }



    [WebMethod]
    public static string CodebehindMethodName()
    {
        // string name = "";
        return "Hello 9000 " + Environment.NewLine + "The Current Time is: "
             + DateTime.Now.ToString();
    }

    private void zShowdataDetail_sched(int id, string _zstatus, string _flag)
    {

        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_plan_action = new training_plan[1];
        training_plan obj = new training_plan();
        obj.u0_training_plan_idx = id;
        obj.operation_status_id = "U0-LISTDATA-HR";
        obj.zstatus = _zstatus;// "HR-W";
        dataelearning.el_training_plan_action[0] = obj;

        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataelearning));

        dataelearning = _func_dmu.zCallServiceNetwork(_urlGetel_u_plan, dataelearning);
        //  _func_dmu.zSetFormViewData(fv_detail_sched, dataelearning.el_training_plan_action);


    }

    public class dstrainingplan
    {
        public string operation_status_id { get; set; }
        public int idx { get; set; }

        public string course_score_name { get; set; }
        public string course_type { get; set; }
        public string priority_name { get; set; }
        public string course_assessment { get; set; }
        public string u2_course_test_name { get; set; }
        public string u3_course_evaluation_name { get; set; }
        public string emp_id { get; set; }
        public string ztem_trn_plan { get; set; }
        public string register_status { get; set; }
        public string u3_training_course_idx { get; set; }
        public string u0_training_course_idx { get; set; }
        public string status_name { get; set; }
        public string zresulte_app_status { get; set; }
        public string zresulte_count { get; set; }


        //el_u0_training_plan    
        public string u0_training_plan_idx { get; set; }
        public string training_plan_no { get; set; }
        public string training_plan_date { get; set; }
        public int training_plan_year { get; set; }
        public int u0_course_idx_ref { get; set; }
        public int lecturer_type { get; set; }
        public int m0_institution_idx_ref { get; set; }
        public int m0_target_group_idx_ref { get; set; }
        public int training_plan_qty { get; set; }
        public decimal training_plan_amount { get; set; }
        public int training_plan_model { get; set; }
        public int training_plan_m1 { get; set; }
        public int training_plan_m2 { get; set; }
        public int training_plan_m3 { get; set; }
        public int training_plan_m4 { get; set; }
        public int training_plan_m5 { get; set; }
        public int training_plan_m6 { get; set; }
        public int training_plan_m7 { get; set; }
        public int training_plan_m8 { get; set; }
        public int training_plan_m9 { get; set; }
        public int training_plan_m10 { get; set; }
        public int training_plan_m11 { get; set; }
        public int training_plan_m12 { get; set; }
        public decimal training_plan_budget { get; set; }
        public decimal training_plan_costperhead { get; set; }
        public string training_plan_status { get; set; }
        public int training_plan_created_by { get; set; }
        public string training_plan_created_at { get; set; }
        public int training_plan_updated_by { get; set; }
        public string training_plan_updated_at { get; set; }
        public string training_course_remark { get; set; }
        public string level_name { get; set; }

        public int training_plan_mtt { get; set; }
        public int training_plan_npw { get; set; }
        public int training_plan_rjn { get; set; }
        public int trn_course_flag { get; set; }

        //u1
        public int u0_training_plan_idx_ref { get; set; }

        //node
        public int u0_idx { get; set; }
        public int node_idx { get; set; }
        public int actor_idx { get; set; }
        public int level_code { get; set; }
        public int approve_status { get; set; }
        public int app_flag { get; set; }
        public int app_user { get; set; }
        public string app_date { get; set; }
        public string approve_remark { get; set; }

        //node md
        public int md_u0_idx { get; set; }
        public int md_node_idx { get; set; }
        public int md_actor_idx { get; set; }
        public int md_approve_status { get; set; }
        public int md_app_flag { get; set; }
        public int md_app_user { get; set; }
        public string md_app_date { get; set; }
        public string md_approve_remark { get; set; }

        public string course_name { get; set; }
        public string node_name { get; set; }
        public string actor_name { get; set; }
        public string decision_name { get; set; }
        public string training_group_name { get; set; }
        public string training_group_color { get; set; }
        public string training_branch_name { get; set; }
        public string target_name { get; set; }
        public string target_group_name { get; set; }
        public string institution_name { get; set; }

        public string md_node_name { get; set; }
        public string md_actor_name { get; set; }
        public string md_decision_name { get; set; }

        // Filter
        public string filter_keyword { get; set; }

        public int JobLevel { get; set; }
        public int zmonth { get; set; }
        public int zyear { get; set; }
        public string zstatus { get; set; }
        public int zcourse_count { get; set; }

        public string currenttime { get; set; }
        public string duration { get; set; }
        public string currenttime_int { get; set; }
        public string u13_training_course_idx { get; set; }
        public int course_type_elearning { get; set; }
        public int course_type_etraining { get; set; }

    }


    protected void Button1_Click(object sender, EventArgs e)
    {

        string su0_training_course_idx = Request.Form["reg_dlgSch_txtu0_training_course_idx"];
        string su3_training_course_idx = Request.Form["reg_dlgSch_txtu3_training_course_idx"];
        int iu0_training_course_idx = int.Parse(su0_training_course_idx);
        int iu3_training_course_idx = int.Parse(su3_training_course_idx);

        if (iu0_training_course_idx > 0)
        {
            zSaveApprove(iu0_training_course_idx, iu3_training_course_idx);

        }
        ShowListHR_W();

    }
    public Boolean register_ok(string id)
    {
        int i = 0;
        if ((id != "") && (id != null))
        {
            i = int.Parse(id);
        }
        if (i == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    // end scheduler
    private void zShowdata_SummaryDetail(int id_u0, int id_u6, string _zstatus)
    {
        btnSave_summary.Visible = true;
        data_elearning dataelearning = new data_elearning();
        dataelearning.el_training_course_action = new training_course[1];
        training_course obj = new training_course();
        obj.emp_idx_ref = emp_idx;
        obj.u0_training_course_idx = id_u0;
        obj.operation_status_id = "U0-LISTDATA-REGISTER";
        obj.zstatus = _zstatus;
        dataelearning.el_training_course_action[0] = obj;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetFormViewData(fv_summarycourse, dataelearning.el_training_course_action);

        //End SetMode
        Hddfld_training_course_no.Value = "";
        Hddfld_u0_training_course_idx.Value = "";
        if (dataelearning.el_training_course_action != null)
        {
            foreach (var item in dataelearning.el_training_course_action)
            {
                Hddfld_training_course_no.Value = item.training_course_no;
                Hddfld_u0_training_course_idx.Value = item.u0_training_course_idx.ToString();
                // Panel pnlDetailApp = (Panel)fv_preview.FindControl("pnlDetailApp");
                data_elearning dataelearning_u6 = new data_elearning();
                dataelearning_u6.el_training_course_action = new training_course[1];
                training_course obj_u6 = new training_course();
                if (_zstatus == "open_course")
                {
                    obj_u6.u6_training_course_idx = id_u6;
                }
                else
                {
                    obj_u6.emp_idx_ref = emp_idx;
                    obj_u6.u6_training_course_idx = item.u6_training_course_idx;
                }

                obj_u6.operation_status_id = "U6-FULL";
                obj_u6.zstatus = _zstatus;
                dataelearning_u6.el_training_course_action[0] = obj_u6;
                dataelearning_u6 = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning_u6);
                if (_zstatus == "open_course")
                {
                    fv_summaryLearner.ChangeMode(FormViewMode.Edit);
                }
                else
                {
                    if (dataelearning_u6.el_training_course_action == null)
                    {
                        fv_summaryLearner.ChangeMode(FormViewMode.Insert);

                    }
                    else
                    {
                        fv_summaryLearner.ChangeMode(FormViewMode.Edit);
                    }
                }

                _func_dmu.zSetFormViewData(fv_summaryLearner, dataelearning_u6.el_training_course_action);
                setfv_summaryLearner();
                if (dataelearning_u6.el_training_course_action == null)
                {
                    _txtu3_training_course_idx.Text = item.u3_training_course_idx.ToString();
                    _txtemp_idx_ref.Text = item.EmpIDX.ToString();
                    _txtu0_training_course_idx_ref.Text = item.u0_training_course_idx.ToString();
                }


                _pnl_super = (Panel)fv_summaryLearner.FindControl("pnl_super");
                Panel pnl_file_btn = (Panel)fv_summaryLearner.FindControl("pnl_file_btn");
                Panel pnl_file = (Panel)fv_summaryLearner.FindControl("pnl_file");
                _pnl_super = (Panel)fv_summaryLearner.FindControl("pnl_super");
                _txttraining_summary_report_remark = (TextBox)fv_summaryLearner.FindControl("txttraining_summary_report_remark");
                _txttraining_benefits_remark = (TextBox)fv_summaryLearner.FindControl("txttraining_benefits_remark");
                Literal littraining_summary_report_remark = (Literal)fv_summaryLearner.FindControl("littraining_summary_report_remark");
                Literal littraining_benefits_remark = (Literal)fv_summaryLearner.FindControl("littraining_benefits_remark");
                Literal litsupervisors_additional = (Literal)fv_summaryLearner.FindControl("litsupervisors_additional");
                _txtsupervisors_additional = (TextBox)fv_summaryLearner.FindControl("txtsupervisors_additional");
                litsupervisors_additional.Visible = false;
                _txtsupervisors_additional.Visible = true;
                if (Hddfld_status.Value == "summ_std")
                {
                    _pnl_super.Visible = false;
                    pnl_file_btn.Visible = true;
                    pnl_file.Visible = true;
                    _txttraining_summary_report_remark.Visible = true;
                    _txttraining_benefits_remark.Visible = true;
                    littraining_summary_report_remark.Visible = false;
                    littraining_benefits_remark.Visible = false;
                }
                else
                {
                    _pnl_super.Visible = true;
                    pnl_file_btn.Visible = false;
                    pnl_file.Visible = false;
                    _txttraining_summary_report_remark.Visible = false;
                    _txttraining_benefits_remark.Visible = false;
                    littraining_summary_report_remark.Visible = true;
                    littraining_benefits_remark.Visible = true;
                }

            }
        }

    }
    public string getImg()
    {
        _lbtraining_course_no = (Label)fv_summarycourse.FindControl("txttraining_course_no");
        _txttraining_course_file_name = (TextBox)fv_summaryLearner.FindControl("txttraining_course_file_name");

        string sPathImage = "", sFileName = "";
        string strFinalFileName = "", sReturn = "";
        if (Hddfld_folder.Value != "")
        {
            string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
            _PathFile = _PathFile + _Folder_plan_course_bin + "/" + Hddfld_folder.Value;
            try
            {
                if (_PathFile != "")
                {
                    string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile + "/"));
                    List<ListItem> files = new List<ListItem>();
                    foreach (string file in filesLoc)
                    {
                        sFileName = Path.GetExtension(file);
                    }
                }
            }
            catch { }

        }
        if (sFileName == "")
        {
            if (_lbtraining_course_no.Text != "")
            {
                string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
                _PathFile = _PathFile + _Folder_plan_course + "/" + _lbtraining_course_no.Text + "_emp" + emp_idx.ToString();
                try
                {
                    if (_PathFile != "")
                    {
                        string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile + "/"));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string file in filesLoc)
                        {
                            try
                            {
                                strFinalFileName = Path.GetFileName(file);
                                sFileName = Path.GetExtension(file);
                            }
                            catch { }
                        }
                    }
                }
                catch { }
                if (_txttraining_course_file_name.Text != "")
                {
                    if (_txttraining_course_file_name.Text == strFinalFileName)
                    {

                    }
                    else
                    {
                        sFileName = "";
                    }
                }
            }

        }
        if (sFileName == "")
        {
            sReturn = _PathFileimage + _func_dmu._IconnoFile;
        }
        else
        {
            sReturn = _PathFileimage + _func_dmu._IconFile;
        }
        sReturn = ResolveUrl(sReturn);
        return sReturn;
    }
    public string getImgFileType()
    {
        _lbtraining_course_no = (Label)fv_summarycourse.FindControl("txttraining_course_no");
        _txttraining_course_file_name = (TextBox)fv_summaryLearner.FindControl("txttraining_course_file_name");

        string sPathImage = "", sFileName = "";
        string strFinalFileName = "", sReturn = "";
        if (Hddfld_folder.Value != "")
        {
            string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
            _PathFile = _PathFile + _Folder_plan_course_bin + "/" + Hddfld_folder.Value;
            try
            {
                if (_PathFile != "")
                {
                    string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile + "/"));
                    List<ListItem> files = new List<ListItem>();
                    foreach (string file in filesLoc)
                    {
                        sFileName = Path.GetExtension(file);
                    }
                }
            }
            catch { }

        }
        if (sFileName == "")
        {
            if (_lbtraining_course_no.Text != "")
            {
                string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
                _PathFile = _PathFile + _Folder_plan_course + "/" + _lbtraining_course_no.Text + "_emp" + emp_idx.ToString();
                try
                {
                    if (_PathFile != "")
                    {
                        string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile + "/"));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string file in filesLoc)
                        {
                            try
                            {
                                strFinalFileName = Path.GetFileName(file);
                                sFileName = Path.GetExtension(file);
                            }
                            catch { }
                        }
                    }
                }
                catch { }
                if (_txttraining_course_file_name.Text != "")
                {
                    if (_txttraining_course_file_name.Text == strFinalFileName)
                    {

                    }
                    else
                    {
                        sFileName = "";
                    }
                }
            }

        }
        if (sFileName == "")
        {
            sReturn = "";
        }
        else
        {
            sReturn = sFileName;
        }
        return sReturn;
    }
    public void zDeleteFileMAS()
    {
        _lbtraining_course_no = (Label)fv_summarycourse.FindControl("txttraining_course_no");
        _txttraining_course_file_name = (TextBox)fv_summaryLearner.FindControl("txttraining_course_file_name");
        if (_lbtraining_course_no.Text == "")
        {
            return;
        }
        if (_txttraining_course_file_name.Text == "")
        {
            return;
        }
        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
        _PathFile = _PathFile + _Folder_plan_course + "/" + _lbtraining_course_no.Text + "_emp" + emp_idx.ToString();
        int ic = 0;
        string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile + "/"));
        List<ListItem> files = new List<ListItem>();
        foreach (string file in filesLoc)
        {
            try
            {
                File.Delete(file);
                ic++;
            }
            catch { }
        }
        if (ic > 0)
        {
            _txttraining_course_file_name.Text = "";
            showAlert("ลบไฟล์เอกสารเสร็จแล้ว");
            try
            {
                Directory.Delete(Server.MapPath(_PathFile + "/"));
            }
            catch { }

        }

    }

    public void zDeleteFileBin()
    {
        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
        _PathFile = _PathFile + _Folder_plan_course_bin + "/" + Hddfld_folder.Value;
        try
        {
            if (_PathFile != "")
            {
                string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile + "/"));
                List<ListItem> files = new List<ListItem>();
                foreach (string file in filesLoc)
                {
                    try
                    {
                        File.Delete(file);
                    }
                    catch { }
                }
            }
        }
        catch { }
    }
    public void importProcessRequest(HttpPostedFile _HttpPostedFile, int item)
    {

        string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
        if (Directory.Exists(Server.MapPath(_PathFile)))
        {
            _PathFile = _PathFile + _Folder_plan_course_bin + "/" + Hddfld_folder.Value;
            zDeleteFileBin();
            if (_HttpPostedFile != null && _HttpPostedFile.ContentLength > 0)
            {
                Directory.CreateDirectory(Server.MapPath(_PathFile));
                string _itemExtension = Path.GetExtension(_HttpPostedFile.FileName);
                string _itemNameNew = item.ToString() + _itemExtension.ToLower();
                string _itemFilePath = "";
                string newFilePath = _PathFile + "/" + _itemNameNew;
                try
                {
                    Directory.Delete(Server.MapPath(newFilePath));
                }
                catch { }
                _itemFilePath = Server.MapPath(newFilePath);
                _HttpPostedFile.SaveAs(_itemFilePath);
            }
        }
        getImg();

    }

    private void setfv_summaryLearner()
    {
        _txtu6_training_course_idx = (TextBox)fv_summaryLearner.FindControl("txtu6_training_course_idx");
        _txtemp_idx_ref = (TextBox)fv_summaryLearner.FindControl("txtemp_idx_ref");
        _txtu3_training_course_idx = (TextBox)fv_summaryLearner.FindControl("txtu3_training_course_idx");
        _txttraining_summary_report_remark = (TextBox)fv_summaryLearner.FindControl("txttraining_summary_report_remark");
        _txttraining_course_file_name = (TextBox)fv_summaryLearner.FindControl("txttraining_course_file_name");
        _txttraining_benefits_remark = (TextBox)fv_summaryLearner.FindControl("txttraining_benefits_remark");
        _txtu0_training_course_idx_ref = (TextBox)fv_summaryLearner.FindControl("txtu0_training_course_idx_ref");
        _txtsupervisors_additional = (TextBox)fv_summaryLearner.FindControl("txtsupervisors_additional");
    }


    private Boolean zSave_summary()
    {
        int idx;
        Boolean _Boolean = false;
        string sDocno = "", sDocno_H = "", m0_prefix = "SUMM";
        _lbtraining_course_no = (Label)fv_summarycourse.FindControl("txttraining_course_no");
        sDocno_H = _lbtraining_course_no.Text + "_emp" + emp_idx.ToString();


        setfv_summaryLearner();
        idx = _func_dmu.zStringToInt(_txtu6_training_course_idx.Text);
        training_course obj_training_course = new training_course();
        _data_elearning.el_training_course_action = new training_course[1];
        obj_training_course.u6_training_course_idx = idx;
        obj_training_course.training_course_status = 1;
        obj_training_course.u0_training_course_idx_ref = _func_dmu.zStringToInt(_txtu0_training_course_idx_ref.Text);
        obj_training_course.u3_training_course_idx_ref = _func_dmu.zStringToInt(_txtu3_training_course_idx.Text);
        obj_training_course.emp_idx_ref = _func_dmu.zStringToInt(_txtemp_idx_ref.Text);
        obj_training_course.training_summary_report_remark = _txttraining_summary_report_remark.Text;
        obj_training_course.training_benefits_remark = _txttraining_benefits_remark.Text;
        obj_training_course.supervisors_additional = _txtsupervisors_additional.Text;
        TextBox txtsumm_no = (TextBox)fv_summaryLearner.FindControl("txtsumm_no");
        if (txtsumm_no.Text == "")
        {
            sDocno = _func_dmu.zRun_Number(_FromcourseRunNo, m0_prefix, "YYMM", "N", "0000");
            obj_training_course.summ_date = _func_dmu.zDateToDB(DateTime.Now.ToString("dd/MM/yyyy", CultureInfo.InstalledUICulture));
        }
        else
        {
            sDocno = txtsumm_no.Text;
        }
        obj_training_course.summ_no = sDocno;
        if (Hddfld_status.Value == "summ_std")
        {
            obj_training_course.zstatus = "STD";
            zUploadTOElearning(sDocno_H);

            obj_training_course.approve_status = 0;
            obj_training_course.u0_idx = 39;
            obj_training_course.node_idx = 14;
            obj_training_course.actor_idx = 2;
            obj_training_course.app_flag = 0;
            obj_training_course.app_user = emp_idx;
            obj_training_course.approve_remark = "";

        }
        else
        {
            obj_training_course.zstatus = "SUPER";

            obj_training_course.approve_status = 4;
            obj_training_course.u0_idx = 37;
            obj_training_course.node_idx = 2;//15
            obj_training_course.actor_idx = 3;
            obj_training_course.app_flag = 1;
            obj_training_course.app_user = emp_idx;
            obj_training_course.approve_remark = "";
        }
        obj_training_course.training_course_file_name = _txttraining_course_file_name.Text;
        obj_training_course.summ_std_user = emp_idx;
        obj_training_course.summ_super_user = emp_idx;
        obj_training_course.training_course_updated_by = emp_idx;
        obj_training_course.operation_status_id = "U6";



        _data_elearning.el_training_course_action[0] = obj_training_course;
        _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_plan_course, _data_elearning);

        idx = _data_elearning.return_code;
        if (Hddfld_status.Value == "summ_std")
        {
            if (idx > 0)
            {
                sendEmailtoheader(_func_dmu.zStringToInt(_txtu0_training_course_idx_ref.Text));
            }
        }
        else
        {
            if (idx > 0)
            {
                sendEmailleadertohr(idx);
            }
        }


        _Boolean = true;
        return _Boolean;
    }

    private void zUploadTOElearning(string sDocNoEl)
    {
        _txttraining_course_file_name = (TextBox)fv_summaryLearner.FindControl("txttraining_course_file_name");
        string sFile = "", sFileName = "";
        string _itemExtension = "";
        if (Hddfld_folder.Value != "")
        {
            string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
            _PathFile = _PathFile + _Folder_plan_course_bin + "/" + Hddfld_folder.Value;
            try
            {
                if (_PathFile != "")
                {
                    string[] filesLoc = Directory.GetFiles(Server.MapPath(_PathFile + "/"));
                    List<ListItem> files = new List<ListItem>();
                    foreach (string file in filesLoc)
                    {
                        sFile = Path.GetFileName(file);
                        _itemExtension = Path.GetExtension(file);
                    }
                }
            }
            catch { }

        }

        if ((sFile != "") && (sDocNoEl != ""))
        {
            string _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];

            string _itemNameNew = "";
            string OldFilePath = "";
            string newFilePath = "";

            OldFilePath = sFile;
            newFilePath = _PathFile + _Folder_plan_course + "/" + sDocNoEl + "/";
            try
            {
                Directory.CreateDirectory(Server.MapPath(newFilePath));
            }
            catch { }
            // Clear File Old In Folder El Mas
            try
            {

                if (newFilePath != "")
                {
                    string[] filesLoc = Directory.GetFiles(Server.MapPath(newFilePath));
                    List<ListItem> files = new List<ListItem>();
                    foreach (string file in filesLoc)
                    {
                        try
                        {
                            File.Delete(file);
                        }
                        catch { }
                    }
                }
            }
            catch { }

            string _folder = OldFilePath;
            for (int i = 1; i <= 1; i++)
            {
                _itemNameNew = sDocNoEl + _itemExtension.ToLower();
                string _OldFilePath, _newFilePath;
                _OldFilePath = ConfigurationSettings.AppSettings["path_flie_elearning"];
                _OldFilePath = _OldFilePath + _Folder_plan_course_bin + "/" + Hddfld_folder.Value + "/" + sFile;
                //  _OldFilePath = OldFilePath;
                _newFilePath = newFilePath + _itemNameNew;

                try
                {
                    Directory.Delete(Server.MapPath(_newFilePath));
                }
                catch { }

                try
                {
                    File.Move(Server.MapPath(_OldFilePath), Server.MapPath(_newFilePath));
                    _txttraining_course_file_name.Text = _itemNameNew;
                }
                catch { }
            }
            try
            {
                _PathFile = ConfigurationSettings.AppSettings["path_flie_elearning"];
                _PathFile = _PathFile + _Folder_plan_course_bin + "/" + Hddfld_folder.Value;
                Directory.Delete(Server.MapPath(_PathFile));
            }
            catch { }
        }
    }

    public Boolean getVisible_btnsumm(int register_status = 0,
                             int signup_status = 0,
                             int summ_std_status = 0,
                             int summ_super_status = 0,
                             int course_type_elearning = 0,
                             int course_type_etraining = 0,
                             int place_type = 0

        )
    {
        Boolean _Boolean = true;

        if (course_type_etraining == 0)
        {
            _Boolean = false;
            // litDebug.Text = _Boolean.ToString();
        }
        else if (place_type == 0)
        {
            _Boolean = false;
        }
        else
        {
            if ((summ_super_status == 1))
            {
                _Boolean = false;

            }
            else if ((summ_std_status == 0))//1
            {
                _Boolean = true;

            }
            else if ((signup_status == 0))
            {
                _Boolean = false;

            }
        }
        

        return _Boolean;
    }
    public Boolean getVisible_btnsumm_prview(int summ_super_status = 0)
    {
        Boolean _Boolean = false;

        if ((summ_super_status == 1))
        {
            _Boolean = true;

        }

        return _Boolean;
    }
    protected void zSave_perm()
    {

        string su0_training_course_idx = Request.Form["ask_perm_dlgSch_txtu0_training_course_idx"];
        string su3_training_course_idx = Request.Form["ask_perm_dlgSch_txtu3_training_course_idx"];
        int iu0_training_course_idx = int.Parse(su0_training_course_idx);
        int iu3_training_course_idx = int.Parse(su3_training_course_idx);

        if (iu0_training_course_idx > 0)
        {
            zSave_perm_user(iu0_training_course_idx);

        }
        ShowListHR_W();
        // SETFOCUS.Focus();

    }
    private void zSave_perm_user(int id)
    {
        if (id == 0)
        {
            return;
        }

        training_course obj_training_course = new training_course();
        //traning_req U1
        _data_elearning.el_training_course_action = new training_course[1];
        obj_training_course.training_course_updated_by = emp_idx;
        obj_training_course.u0_training_course_idx_ref = id;
        obj_training_course.remark = "";
        obj_training_course.training_course_status = 1;

        obj_training_course.approve_status = 0;
        obj_training_course.u0_idx = 29;
        obj_training_course.node_idx = 14;
        obj_training_course.actor_idx = 2;
        obj_training_course.app_flag = 0;
        obj_training_course.app_user = emp_idx;

        obj_training_course.emp_idx_ref = emp_idx;
        obj_training_course.resulte_std_user = emp_idx;
        obj_training_course.operation_status_id = "U7";
        obj_training_course.zstatus = "STD";
        _data_elearning.el_training_course_action[0] = obj_training_course;
        //litDebug.Text = _func_dmu.zJson(_urlSetUpdel_u_plan_course, _data_elearning);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));

        _func_dmu.zCallServicePostNetwork(_urlSetInsel_u_plan_course, _data_elearning);

    }
    private void sendEmailtoheader(int id)
    {
        if (id == 0)
        {
            return;
        }
        try
        {


            trainingLoolup obj_Loolup = new trainingLoolup();
            _data_elearning.trainingLoolup_action = new trainingLoolup[1];
            obj_Loolup.idx = id;
            obj_Loolup.EmpIDX = emp_idx;
            obj_Loolup.operation_status_id = "E-MAIL-RESULTS";
            _data_elearning.trainingLoolup_action[0] = obj_Loolup;
            //  litDebug.Text = _func_dmu.zJson(_urlsendEmail_traningregister, _data_elearning);
            //litDebug.Text = _funcTool.convertObjectToJson(_data_elearning);
            _func_dmu.zCallServicePostNetwork(_urlsendEmail_traningregister, _data_elearning);
        }
        catch { }


    }
    private void sendEmailleadertohr(int id)
    {
        if (id == 0)
        {
            return;
        }
        try
        {


            trainingLoolup obj_Loolup = new trainingLoolup();
            _data_elearning.trainingLoolup_action = new trainingLoolup[1];
            obj_Loolup.idx = id;
            obj_Loolup.EmpIDX = emp_idx;
            obj_Loolup.operation_status_id = "E-MAIL-LEADER-TO-HR";
            _data_elearning.trainingLoolup_action[0] = obj_Loolup;
            //litDebug.Text = _funcTool.convertObjectToJson(_data_elearning);
            _func_dmu.zCallServicePostNetwork(_urlsendEmail_trn_summaryleadertohr, _data_elearning);
        }
        catch { }

    }

    public string getstar_level(int ilevel)
    {
        string sStar = "", sStar_all = "";
        //int item = 2;
        sStar = "<i class=\"glyphicon glyphicon-star\" style=\"color: forestgreen; \"></i>";
        for (int i = 1; i <= ilevel; i++)
        {

            if (i > 1)
            {
                sStar_all = sStar_all + "&nbsp;" + sStar;
            }
            else
            {
                sStar_all = sStar;
            }
        }

        return sStar_all;
    }

    private void setCoursePass(int id, Repeater rptRepeater)
    {
        data_elearning dataelearning = new data_elearning();
        trainingLoolup obj_Loolup = new trainingLoolup();
        dataelearning.trainingLoolup_action = new trainingLoolup[1];
        obj_Loolup.EmpIDX = emp_idx;
        obj_Loolup.idx = id;
        obj_Loolup.operation_status_id = "course_pass";
        dataelearning.trainingLoolup_action[0] = obj_Loolup;
        dataelearning = _func_dmu.zCallServicePostNetwork(_func_dmu._urlGetel_lu_TraningAll, dataelearning);
        _func_dmu.zSetRepeaterData(rptRepeater, dataelearning.trainingLoolup_action);
    }
    private void setU8StartEndDatetime(int id, Repeater rptRepeater)
    {
        data_elearning dataelearning = new data_elearning();
        training_course obj_detail = new training_course();
        dataelearning.el_training_course_action = new training_course[1];
        //obj_detail.EmpIDX = emp_idx;
        obj_detail.u0_training_course_idx = id;
        obj_detail.operation_status_id = "U8-FULL";
        dataelearning.el_training_course_action[0] = obj_detail;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        _func_dmu.zSetRepeaterData(rptRepeater, dataelearning.el_training_course_action);

    }
    public void setImageResult(int id)
    {
        if ((id == 1) || (id == 6))
        {

        }
        else if (id == 2)
        {

        }
        else
        {

        }


    }

    [WebMethod]
    public static dstrainingplan[] pos_datavideo(string currenttime
        , string duration
        , string emp_id
        , string u6_course_idx
        , string u3_training_course_idx
        , string u13_training_course_idx
        , string item_count_h
        , string item_total
        , string result_flag
        )
    {
        //string id = Request.Form["HiddenReportId"];
        List<dstrainingplan> _dstrainingplan = new List<dstrainingplan>();
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = ConfigurationManager
                    .ConnectionStrings["conn_mas"].ConnectionString;
            using (SqlCommand cmd = new SqlCommand())
            {
                StringBuilder query = new StringBuilder();
                SqlDataAdapter da;
                DataTable results;

                if (currenttime == null) { currenttime = "00:00"; }
                if (duration == null) { duration = "00:00"; }
                if (emp_id == null) { emp_id = "0"; }
                if (u6_course_idx == null) { u6_course_idx = "0"; }
                if (u3_training_course_idx == null) { u3_training_course_idx = "0"; }
                if (u13_training_course_idx == null) { u13_training_course_idx = "0"; }
                if (result_flag == null) { result_flag = "0"; }

                if (currenttime == "") { currenttime = "00:00"; }
                if (duration == "") { duration = "00:00"; }
                if (emp_id == "") { emp_id = "0"; }
                if (u6_course_idx == "") { u6_course_idx = "0"; }
                if (u3_training_course_idx == "") { u3_training_course_idx = "0"; }
                if (u13_training_course_idx == "") { u13_training_course_idx = "0"; }
                if (result_flag == "") { result_flag = "0"; }


                query.Clear();
                query.AppendLine(" [el_update_u3_trn_course_playvideo] ");
                query.AppendLine("  '" + currenttime + "'");
                query.AppendLine(", '" + duration + "'");
                query.AppendLine(",  " + emp_id);
                query.AppendLine(",  " + u6_course_idx);
                query.AppendLine(",  " + u3_training_course_idx);
                query.AppendLine(",  " + u13_training_course_idx);
                query.AppendLine(",  " + result_flag);
                cmd.CommandText = query.ToString();
                cmd.Connection = conn;
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();


                query.Clear();
                query.AppendLine("select * from el_u13_training_course_playvideo ");
                query.AppendLine("where 1=1 ");
                query.AppendLine(" and result_flag <> 3 ");
                query.AppendLine(" and emp_idx = " + emp_id);
                query.AppendLine(" and u3_training_course_idx = " + u3_training_course_idx);
                cmd.CommandText = query.ToString();
                cmd.Connection = conn;
                da = new SqlDataAdapter(cmd);
                results = new DataTable("SearchResults");
                conn.Open();
                da.Fill(results);
                if (results.Rows.Count > 0)
                {
                    u13_training_course_idx = results.Rows[0]["u13_training_course_idx"].ToString();
                }
                conn.Close();

                if ((u13_training_course_idx == null)
                    ||
                    (u13_training_course_idx == ""))
                {
                    u13_training_course_idx = "0";
                }
                if (u13_training_course_idx == "0")
                {
                    /*
                    result_flag = "1";
                    conn.Close();
                    query.Clear();
                    query.AppendLine("insert into el_u13_training_course_playvideo  ");
                    query.AppendLine("( ");
                    query.AppendLine("emp_idx,u3_training_course_idx,item_count, item_total ");
                    query.AppendLine(",result_flag,training_course_created_by,training_course_updated_by ");
                    query.AppendLine(") ");
                    query.AppendLine("values ");
                    query.AppendLine("( ");
                    query.AppendLine("   " + emp_id);
                    query.AppendLine(",  " + u3_training_course_idx);
                    query.AppendLine(", 0 ");
                    query.AppendLine(",isnull((  ");
                    query.AppendLine(" select count(*)  ");
                    query.AppendLine(" from View_el_u6_course v_u6_c ");
                    query.AppendLine(" inner join View_el_u0_training_course v_u0_tc ");
                    query.AppendLine(" on v_u0_tc.u0_course_idx = v_u6_c.u0_course_idx_ref ");
                    query.AppendLine(" inner join View_el_u3_training_course_employee v_u3_emp ");
                    query.AppendLine(" on v_u3_emp.u0_training_course_idx_ref = v_u0_tc.u0_training_course_idx ");
                    query.AppendLine(" where 1=1 ");
                    query.AppendLine(" and v_u6_c.course_status = 1 ");
                    query.AppendLine(" and v_u0_tc.training_course_status  = 1 ");
                    query.AppendLine(" and v_u3_emp.emp_idx = " + emp_id);
                    query.AppendLine(" and v_u3_emp.u3_training_course_idx = " + u3_training_course_idx);
                    query.AppendLine(" ),0) ");
                    query.AppendLine(",  " + result_flag);
                    query.AppendLine(",   " + emp_id);
                    query.AppendLine(",   " + emp_id);
                    query.AppendLine(") ");
                    cmd.CommandText = query.ToString();
                    cmd.Connection = conn;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                    conn.Close();
                    */

                    query.Clear();
                    query.AppendLine("select * from el_u13_training_course_playvideo ");
                    query.AppendLine("where 1=1 ");
                    query.AppendLine(" and result_flag <> 3 ");
                    query.AppendLine(" and emp_idx = " + emp_id);
                    query.AppendLine(" and u3_training_course_idx = " + u3_training_course_idx);
                    cmd.CommandText = query.ToString();
                    cmd.Connection = conn;
                    da = new SqlDataAdapter(cmd);
                    results = new DataTable("SearchResults");
                    conn.Open();
                    da.Fill(results);
                    if (results.Rows.Count > 0)
                    {
                        u13_training_course_idx = results.Rows[0]["u13_training_course_idx"].ToString();
                    }
                    conn.Close();
                }
                /*
                if ((u13_training_course_idx != null)
                    &&
                    (u13_training_course_idx != "")
                     &&
                    (u13_training_course_idx != "0"))
                {


                    query.Clear();
                    query.AppendLine("select * from el_u12_training_course_playvideo b ");
                    query.AppendLine("inner join el_u13_training_course_playvideo a ");
                    query.AppendLine("on a.u13_training_course_idx = b.u13_training_course_idx ");
                    query.AppendLine("where 1=1 ");
                    query.AppendLine(" and a.result_flag <> 3 ");
                    query.AppendLine(" and a.emp_idx = " + emp_id);
                    query.AppendLine(" and b.u6_course_idx = " + u6_course_idx);
                    query.AppendLine(" and a.u3_training_course_idx = " + u3_training_course_idx);
                    query.AppendLine(" and a.u13_training_course_idx = " + u13_training_course_idx);
                    cmd.CommandText = query.ToString();
                    cmd.Connection = conn;
                    da = new SqlDataAdapter(cmd);
                    results = new DataTable("SearchResults");
                    conn.Open();
                    da.Fill(results);
                    if (results.Rows.Count > 0)
                    {
                        if (results.Rows[0]["currenttime"].ToString() != results.Rows[0]["duration"].ToString())
                        {
                            conn.Close();
                            query.Clear();
                            query.AppendLine("update el_u12_training_course_playvideo set ");
                            query.AppendLine(" currenttime = '" + currenttime + "'");
                            query.AppendLine(", duration = '" + duration + "'");
                            query.AppendLine(", training_course_updated_at = getdate()  ");
                            query.AppendLine("where 1=1 ");
                            query.AppendLine(" and emp_idx = " + emp_id);
                            query.AppendLine(" and u6_course_idx = " + u6_course_idx);
                            query.AppendLine(" and u3_training_course_idx = " + u3_training_course_idx);
                            query.AppendLine(" and u13_training_course_idx = " + u13_training_course_idx);
                            query.AppendLine(" and CAST(currenttime as time) <= CAST('" + currenttime + "' as time)");
                            cmd.CommandText = query.ToString();
                            cmd.Connection = conn;
                            conn.Open();
                            cmd.ExecuteNonQuery();

                        }

                        conn.Close();
                        query.Clear();
                        query.AppendLine("update el_u13_training_course_playvideo set ");
                        query.AppendLine(" item_count = ");
                        query.AppendLine(" isnull(( ");
                        query.AppendLine(" select count(*) from el_u12_training_course_playvideo b ");
                        query.AppendLine(" where 1=1 ");
                        query.AppendLine(" and b.training_course_status = 1 ");
                        query.AppendLine(" and b.u13_training_course_idx = el_u13_training_course_playvideo.u13_training_course_idx ");
                        query.AppendLine(" and b.currenttime = b.duration ");
                        query.AppendLine(" ),0) ");
                        query.AppendLine("where 1=1 ");
                        query.AppendLine(" and result_flag not in (3,4) ");
                        query.AppendLine(" and emp_idx = " + emp_id);
                        query.AppendLine(" and u13_training_course_idx = " + u13_training_course_idx);
                        cmd.CommandText = query.ToString();
                        cmd.Connection = conn;
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        query.Clear();
                        query.AppendLine("update el_u13_training_course_playvideo set ");
                        query.AppendLine(" result_flag = 2 ");
                        query.AppendLine("where 1=1 ");
                        query.AppendLine(" and result_flag not in (3,4) ");
                        query.AppendLine(" and item_count = item_total ");
                        query.AppendLine(" and emp_idx = " + emp_id);
                        query.AppendLine(" and u13_training_course_idx = " + u13_training_course_idx);
                        cmd.CommandText = query.ToString();
                        cmd.Connection = conn;
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                        query.Clear();
                        query.AppendLine(" update el_u11_training_course_test_th set status_flag = 9  ");
                        query.AppendLine(" where 1 = 1  ");
                        query.AppendLine(" and training_course_status = 1  ");
                        query.AppendLine(" and item_count = 2  ");
                        query.AppendLine(" and status_flag = 1  ");
                        query.AppendLine(" and result_flag = 0  ");
                        query.AppendLine(" and el_flag = 1  ");
                        query.AppendLine(" and exists(  ");
                        query.AppendLine(" select * from View_el_u12_training_course_playvideo v_u12  ");
                        query.AppendLine(" where 1 = 1  ");
                        query.AppendLine(" and v_u12.emp_idx = " + emp_id);
                        query.AppendLine(" and v_u12.u13_training_course_idx = " + u13_training_course_idx);
                        query.AppendLine(" and v_u12.result_flag = 2  ");
                        query.AppendLine(" and v_u12.u0_training_course_idx = el_u11_training_course_test_th.u0_training_course_idx_ref  ");
                        query.AppendLine(" and v_u12.emp_idx = el_u11_training_course_test_th.training_course_created_by ");
                        query.AppendLine(" ) ");
                        cmd.CommandText = query.ToString();
                        cmd.Connection = conn;
                        conn.Open();
                        cmd.ExecuteNonQuery();

                        conn.Close();
                    }
                    else
                    {
                        conn.Close();
                        query.Clear();
                        query.AppendLine("insert into el_u12_training_course_playvideo  ");
                        query.AppendLine("( ");
                        query.AppendLine("emp_idx,u6_course_idx,u13_training_course_idx,u3_training_course_idx,currenttime, duration ");
                        query.AppendLine(") ");
                        query.AppendLine("values ");
                        query.AppendLine("( ");
                        query.AppendLine("   " + emp_id);
                        query.AppendLine(",  " + u6_course_idx);
                        query.AppendLine(",  " + u13_training_course_idx);
                        query.AppendLine(",  " + u3_training_course_idx);
                        query.AppendLine(", '" + currenttime + "'");
                        query.AppendLine(", '" + duration + "'");
                        query.AppendLine(") ");
                        cmd.CommandText = query.ToString();
                        cmd.Connection = conn;
                        conn.Open();
                        cmd.ExecuteNonQuery();
                        conn.Close();
                    }
                }
                */

                dstrainingplan dstrainingplan_row = new dstrainingplan();
                dstrainingplan_row.emp_id = emp_id;
                dstrainingplan_row.zresulte_count = "0";
                dstrainingplan_row.currenttime = "0";
                dstrainingplan_row.currenttime_int = "0";
                dstrainingplan_row.u13_training_course_idx = u13_training_course_idx;
                query.Clear();
                query.AppendLine("select * ");
                query.AppendLine(",((CAST(LEFT(currenttime,LEN(currenttime) - 3) as int) * 60) +  ");
                query.AppendLine(" CAST(RIGHT(currenttime, 2) as int)) as currenttime_int ");
                query.AppendLine(" from el_u12_training_course_playvideo ");
                query.AppendLine("where 1=1 ");
                query.AppendLine(" and emp_idx = " + emp_id);
                query.AppendLine(" and u6_course_idx = " + u6_course_idx);
                query.AppendLine(" and u3_training_course_idx = " + u3_training_course_idx);
                query.AppendLine(" and u13_training_course_idx = " + u13_training_course_idx);
                cmd.CommandText = query.ToString();
                cmd.Connection = conn;
                da = new SqlDataAdapter(cmd);
                results = new DataTable("SearchResults");
                conn.Open();
                da.Fill(results);
                if (results.Rows.Count > 0)
                {
                    dstrainingplan_row.currenttime = results.Rows[0]["currenttime"].ToString();
                    dstrainingplan_row.currenttime_int = results.Rows[0]["currenttime_int"].ToString();
                }
                conn.Close();

                query.Clear();
                query.AppendLine("select count(*) zcount from el_u12_training_course_playvideo ");
                query.AppendLine("where 1=1 and currenttime = duration  ");
                query.AppendLine(" and emp_idx = " + emp_id);
                //query.AppendLine(" and u6_course_idx = " + u6_course_idx);
                query.AppendLine(" and u3_training_course_idx = " + u3_training_course_idx);
                query.AppendLine(" and u13_training_course_idx = " + u13_training_course_idx);
                cmd.CommandText = query.ToString();
                cmd.Connection = conn;
                da = new SqlDataAdapter(cmd);
                results = new DataTable("SearchResults");
                conn.Open();
                da.Fill(results);
                if (results.Rows.Count > 0)
                {

                    //dstrainingplan dstrainingplan_row = new dstrainingplan();
                    //dstrainingplan_row.emp_id = emp_id;
                    dstrainingplan_row.zresulte_count = results.Rows[0]["zcount"].ToString();
                    //_dstrainingplan.Add(dstrainingplan_row);

                }
                _dstrainingplan.Add(dstrainingplan_row);
                conn.Close();

            }

        }
        return _dstrainingplan.ToArray();
        //return sStr;

    }
    protected string url_video_image { get; set; }
    protected string url_video { get; set; }
    protected string getvideo_item { get; set; }
    protected string getvideo_title { get; set; }
    protected string getvideo_description { get; set; }
    protected string getu6_course_idx { get; set; }
    protected string getu3_training_course_idx { get; set; }
    protected string getitem_count { get; set; }
    protected string getitem_count_std { get; set; }
    protected string getcurrent_time_std { get; set; }
    protected string getcurrent_time_std_int { get; set; }
    protected string getu13_training_course_idx { get; set; }
    protected string getitem_count_h { get; set; }
    protected string getitem_total { get; set; }
    protected string getresult_flag { get; set; }
    protected string get_fullscreen { get; set; }
    private void showdata_elaerning(int id)
    {
        string _fullscreen = Request.Form["_fullscreen"];
        string _url_video = "", _url_image = "";
        data_elearning dataelearning = new data_elearning();
        training_course obj_detail = new training_course();
        dataelearning.el_training_course_action = new training_course[1];
        obj_detail.emp_idx_ref = emp_idx;
        obj_detail.u0_training_course_idx = id;
        obj_detail.operation_status_id = "u12";
        dataelearning.el_training_course_action[0] = obj_detail;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);

        if (dataelearning.el_training_course_action != null)
        {

            var item = dataelearning.el_training_course_action[0];

            Label lb_title_video = (Label)FV_elearning.FindControl("lb_title_video");

            lb_title_video.Text = item.course_name;

            getvideo_item = item.video_item.ToString();
            getvideo_title = item.video_title;
            getvideo_description = item.video_description;
            getu6_course_idx = item.u6_course_idx.ToString();
            getu3_training_course_idx = item.u3_training_course_idx.ToString();
            getitem_count = item.zitem_count.ToString();
            getitem_count_std = item.item_count_std.ToString();
            getcurrent_time_std = item.currenttime;
            getcurrent_time_std_int = item.currenttime_int.ToString();
            _url_video = getVideoUrl(item.course_no, item.video_name);
            _url_image = getImgUrl(item.course_no, item.video_images);

            hdd_statusvideo.Value = "1";
        }
        else
        {
            hdd_statusvideo.Value = "0";
        }
        this.get_fullscreen = _fullscreen;
        javascript_Postback();
        css_Postback();
        this.url_video = ResolveUrl(_url_video);
        this.url_video_image = ResolveUrl(_url_image);


    }
    public string getImgUrl(string ADocno = "", string AImage = "")
    {
        string sPathImage = "";
        if (AImage == "")
        {
            sPathImage = _PathFileimage + _func_dmu._IconnoImage;
        }
        else
        {
            sPathImage = _PathFile + _Folder_course_video_image + "/" + ADocno + "/" + AImage;
        }
        sPathImage = ResolveUrl(sPathImage);

        return sPathImage;
    }
    public string getVideoUrl(string ADocno = "", string AImage = "")
    {
        string sPathImage = "";
        if (AImage == "")
        {
            sPathImage = _PathFileimage + _func_dmu._IconnoImage;
        }
        else
        {
            sPathImage = _PathFile + _Folder_course_video + "/" + ADocno + "/" + AImage;
        }

        sPathImage = ResolveUrl(sPathImage);
        return sPathImage;
    }

    protected void javascript_Postback()
    {
        string _fullscreen = Request.Form["_fullscreen"];
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append(@"<script language='javascript'>");
        sb.Append(@"var vid = document.getElementById('myVideo');");
        sb.Append(@"var time_update_interval = 0;");
        // sb.Append(@"var time_update_interval_stop = 0;");
        sb.Append(@"var myVar_stop; ");
        sb.Append(@"$(vid).ready(function() { ");
        sb.Append(@"    initialize(); ");
        sb.Append(@"}); ");
        sb.Append(@"function restart() ");
        sb.Append(@"{ ");
        sb.Append(@"    document.getElementById('current_time').innerHTML = '0:00'; ");
        sb.Append(@"    vid.load(); ");
        sb.Append(@"    vid.play(); ");
        sb.Append(@"    myStop_videoFunction(); ");
        sb.Append(@"    stopvideo(); ");
        sb.Append(@"} ");
        sb.Append(@"function playVid() ");
        sb.Append(@"{ ");
        sb.Append(@"    if ($('#current_time').value = $('#duration').value) ");
        sb.Append(@"    { ");
        sb.Append(@"        document.getElementById('current_time').innerHTML = '0:00'; ");
        sb.Append(@"        vid.load(); ");
        sb.Append(@"    } ");
        sb.Append(@"    vid.play(); ");
        sb.Append(@"    pos_datavideo(1); ");
        sb.Append(@"    myStop_videoFunction(); ");
        sb.Append(@"    stopvideo(); ");
        sb.Append(@"} ");
        sb.Append(@"function pauseVid() ");
        sb.Append(@"{ ");
        sb.Append(@"    vid.pause(); ");
        sb.Append(@"    myStop_videoFunction(); ");
        sb.Append(@"} ");
        sb.Append(@"function stepbackward() ");
        sb.Append(@"{ ");
        sb.Append(@"   if(($('#txt_steptime').value = 0) ||  ");
        sb.Append(@"      ($('#txt_steptime').value = null))  ");
        sb.Append(@"  {  ");
        sb.Append(@"    $('#txt_steptime').val(1); ");
        sb.Append(@"  }   ");
        sb.Append(@"  vid.pause(); ");
        sb.Append(@"  var a = parseInt(vid.currentTime);   ");
        sb.Append(@"  var b = parseInt(document.getElementById('txt_steptime').value);   ");
        sb.Append(@"  var total = a-b ; ");
        sb.Append(@"  vid.currentTime = total;   ");
        sb.Append(@"    myStop_videoFunction(); ");
        sb.Append(@"} ");
        sb.Append(@"function stepforward() ");
        sb.Append(@"{ ");
        sb.Append(@"   if(($('#txt_steptime').value = 0) ||  ");
        sb.Append(@"      ($('#txt_steptime').value = null))  ");
        sb.Append(@"  {  ");
        sb.Append(@"    $('#txt_steptime').val('1'); ");
        sb.Append(@"  }   ");
        sb.Append(@"  vid.pause(); ");
        sb.Append(@"  var a = parseInt(vid.currentTime);   ");
        sb.Append(@"  var b = parseInt(document.getElementById('txt_steptime').value);   ");
        sb.Append(@"  var total = a+b;  ");
        sb.Append(@"   if(parseInt(document.getElementById('current_time_std_int').innerHTML) < parseInt(total))  ");
        sb.Append(@"   {  ");
        sb.Append(@"      alert('ไม่สามารถเลื่อนผ่านได้เนื่องจากเกินเวลาที่คุณดูวิดีโอล่าสุด !!!!'); ");
        sb.Append(@"   }  ");
        sb.Append(@"   else  ");
        sb.Append(@"   {  ");
        sb.Append(@"      vid.currentTime = total;   ");
        sb.Append(@"   }  ");
        sb.Append(@"    myStop_videoFunction(); ");
        sb.Append(@"} ");
        sb.Append(@"function initialize() ");
        sb.Append(@"{ ");
        sb.Append(@"    updateTimerDisplay(); ");
        sb.Append(@"    clearInterval(time_update_interval); ");
        sb.Append(@"    time_update_interval = setInterval(function() { ");
        sb.Append(@"       updateTimerDisplay(); ");
        sb.Append(@"    }, 1000); ");
        sb.Append(@"var myVar = setInterval(myTimer, 10000); ");
        sb.Append(@"var vid = document.getElementById('myVideo'); ");
        sb.Append(@"function myTimer() ");
        sb.Append(@"{ ");
        sb.Append(@"    pos_datavideo(1); ");
        sb.Append(@"} ");
        //sb.Append(@" func_fullscreen_setdefault(); ");
        sb.Append(@"} ");
        sb.Append(@"function updateTimerDisplay() ");
        sb.Append(@"{ ");
        sb.Append(@"    $('#current_time').text(formatTime(vid.currentTime)); ");
        sb.Append(@"    $('#duration').text(formatTime(vid.duration)); ");
        sb.Append(@"} ");
        sb.Append(@"function formatTime(time) ");
        sb.Append(@"{ ");
        sb.Append(@"    time = Math.round(time); ");
        sb.Append(@"    var minutes = Math.floor(time / 60), ");
        sb.Append(@"        seconds = time - minutes * 60; ");
        sb.Append(@"    seconds = seconds < 10 ? '0' + seconds : seconds; ");
        sb.Append(@"    return minutes + ':' + seconds; ");
        sb.Append(@"} ");
        sb.Append(@"function timelast() ");
        sb.Append(@"{ ");
        sb.Append(@"  vid.pause(); ");
        sb.Append(@"  vid.currentTime = parseInt(document.getElementById('current_time_std_int').innerHTML);   ");
        sb.Append(@"    myStop_videoFunction(); ");
        sb.Append(@"} ");
        //หยุดการเล่น video ทุก 1 นาที
        sb.Append(@"function stopvideo() ");
        sb.Append(@"{ ");
        //sb.Append(@" setTimeout(stopalert, 60000); ");
        sb.Append(@"myVar_stop = setTimeout(function(){ ");
        sb.Append(@"     vid.pause(); ");
        sb.Append(@"     alert('วิดีโอถูกหยุดเล่นกรุณากดเล่นวิดีโอใหม่ !!!!'); ");
        sb.Append(@"     myStop_videoFunction(); ");
        sb.Append(@"  }, 600000); ");
        sb.Append(@"} ");
        sb.Append(@"function myStop_videoFunction() ");
        sb.Append(@"{ ");
        sb.Append(@"    clearTimeout(myVar_stop); ");
        sb.Append(@"} ");
        sb.Append(@"function stopalert() ");
        sb.Append(@"{ ");
        sb.Append(@" vid.pause(); ");
        sb.Append(@" alert('วิดีโอถูกหยุดเล่นกรุณากดเล่นวิดีโอใหม่ !!!!'); ");
        sb.Append(@"} ");
        sb.Append(@"function openFullscreen() ");
        sb.Append(@"{ ");
        sb.Append(@"    if (vid.requestFullscreen) ");
        sb.Append(@"    { ");
        sb.Append(@"        vid.requestFullscreen(); ");
        sb.Append(@"    } ");
        sb.Append(@"    else if (vid.mozRequestFullScreen) ");
        sb.Append(@"    { ");
        sb.Append(@"        vid.mozRequestFullScreen(); ");
        sb.Append(@"    } ");
        sb.Append(@"    else if (vid.webkitRequestFullscreen) ");
        sb.Append(@"    { ");
        sb.Append(@"        vid.webkitRequestFullscreen(); ");
        sb.Append(@"    } ");
        sb.Append(@"    else if (vid.msRequestFullscreen) ");
        sb.Append(@"    { ");
        sb.Append(@"        vid.msRequestFullscreen(); ");
        sb.Append(@"    } ");
        sb.Append(@"} ");
        sb.Append(@"function func_fullscreen() { ");
        sb.Append(@"if (document.getElementById('_fullscreen').value == null)  ");
        sb.Append(@"{  ");
        sb.Append(@"    document.getElementById('_fullscreen').value = '0';  ");
        sb.Append(@"}  ");
        sb.Append(@"else if (document.getElementById('_fullscreen').value == '')  ");
        sb.Append(@"{  ");
        sb.Append(@"    document.getElementById('_fullscreen').value = '0';  ");
        sb.Append(@"}  ");
        sb.Append(@"if (document.getElementById('_fullscreen').value == '0')  ");
        sb.Append(@"{ ");
        sb.Append(@"    document.getElementById('_fullscreen').value = '1'; ");
        sb.Append(@"} ");
        sb.Append(@"else if (document.getElementById('_fullscreen').value == '1') ");
        sb.Append(@"{ ");
        sb.Append(@"    document.getElementById('_fullscreen').value = '0'; ");
        sb.Append(@"} ");
        sb.Append(@" ");
        sb.Append(@"var myVar_full = document.getElementById('_fullscreen').value; ");
        sb.Append(@"if (myVar_full == 1) ");
        sb.Append(@"{ ");
        sb.Append(@"   func_active(); ");
        sb.Append(@"} ");
        sb.Append(@"else ");
        sb.Append(@"{ ");
        sb.Append(@"   func_normal(); ");
        sb.Append(@"} ");
        sb.Append(@"} ");
        sb.Append(@"function func_active() { ");
        sb.Append(@"    document.getElementById('myVideo').className = 'myVideo_active_style'; ");
        sb.Append(@"    document.getElementById('dv_elearning_back').className = 'dv_elearning_back_active_style'; ");
        //sb.Append(@"    document.getElementById('hr_detail').className = 'detail_active_style'; ");
        sb.Append(@"    document.getElementById('dv_detail').className = 'detail_active_style'; ");
        sb.Append(@"    document.getElementById('dv_crontroll').className = 'dv_crontroll_active_style'; ");
        sb.Append(@"} ");
        sb.Append(@"function func_normal() { ");
        sb.Append(@"    document.getElementById('myVideo').className = 'myVideo_normal_style'; ");
        sb.Append(@"    document.getElementById('dv_elearning_back').className = 'dv_elearning_back_normal_style'; ");
        //sb.Append(@"    document.getElementById('hr_detail').className = 'detail_normal_style'; ");
        sb.Append(@"    document.getElementById('dv_detail').className = 'detail_normal_style'; ");
        sb.Append(@"    document.getElementById('dv_crontroll').className = ''; ");
        sb.Append(@"} ");
        sb.Append(@"function func_fullscreen_setdefault() { ");
        sb.Append(@"if (document.getElementById('_fullscreen').value == null)  ");
        sb.Append(@" {  ");
        sb.Append(@"    document.getElementById('_fullscreen').value = '0';  ");
        sb.Append(@" }  ");
        sb.Append(@"else if (document.getElementById('_fullscreen').value == '')  ");
        sb.Append(@" {  ");
        sb.Append(@"    document.getElementById('_fullscreen').value = '0';  ");
        sb.Append(@" }  ");
        sb.Append(@"var myVar_full_set = document.getElementById('_fullscreen').value; ");
        sb.Append(@"if (myVar_full_set == 1) ");
        sb.Append(@" { ");
        sb.Append(@"   func_active(); ");
        sb.Append(@" } ");
        sb.Append(@"else ");
        sb.Append(@" { ");
        sb.Append(@"   func_normal(); ");
        sb.Append(@" } ");
        sb.Append(@"} ");


        sb.Append(@" </script>");
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "JSCR", sb.ToString(), false);


    }

    private void showdata_elaerning_std(string _zstatus)
    {
        string video_item = Request.Form["txt_video_item"];
        string _fullscreen = Request.Form["_fullscreen"];
        // litDebug.Text = video_item;

        string _url_video = "", _url_image = "";
        data_elearning dataelearning = new data_elearning();
        training_course obj_detail = new training_course();
        dataelearning.el_training_course_action = new training_course[1];
        obj_detail.emp_idx_ref = emp_idx;
        obj_detail.video_item = _func_dmu.zStringToInt(video_item);
        obj_detail.u0_training_course_idx = _func_dmu.zStringToInt(ViewState["u0_training_course_idx"].ToString());
        obj_detail.zstatus = _zstatus;
        obj_detail.operation_status_id = "u12_student";
        dataelearning.el_training_course_action[0] = obj_detail;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);

        if (dataelearning.el_training_course_action != null)
        {

            var item = dataelearning.el_training_course_action[0];
            getvideo_item = item.video_item.ToString();
            getvideo_title = item.video_title;
            getvideo_description = item.video_description;
            getu6_course_idx = item.u6_course_idx.ToString();
            getu3_training_course_idx = item.u3_training_course_idx.ToString();
            getitem_count = item.zitem_count.ToString();
            getitem_count_std = item.item_count_std.ToString();
            getcurrent_time_std = item.currenttime;
            getcurrent_time_std_int = item.currenttime_int.ToString();
            _url_video = getVideoUrl(item.course_no, item.video_name);
            _url_image = getImgUrl(item.course_no, item.video_images);
            this.get_fullscreen = _fullscreen;
            javascript_Postback();
            css_Postback();
            this.url_video = ResolveUrl(_url_video);
            this.url_video_image = ResolveUrl(_url_image);

        }
        else
        {
            // showAlert("ไม่มีข้อมูลวิดีโอที่คุณต้องการ !!!!");
            if (ViewState["elearning_status"].ToString() == "replay")
            {
                showdata_elaerning_replay(_func_dmu.zStringToInt(ViewState["u0_training_course_idx"].ToString()));
            }
            else
            {
                showdata_elaerning(_func_dmu.zStringToInt(ViewState["u0_training_course_idx"].ToString()));
            }

        }

    }
    private void checkdata_elaerning()
    {

        data_elearning dataelearning = new data_elearning();
        training_course obj_detail = new training_course();
        dataelearning.el_training_course_action = new training_course[1];
        obj_detail.emp_idx_ref = emp_idx;
        obj_detail.u3_training_course_idx = _func_dmu.zStringToInt(ViewState["u3_training_course_idx"].ToString());
        obj_detail.operation_status_id = "u13";
        dataelearning.el_training_course_action[0] = obj_detail;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);
        int ic = 0;
        int iresult_flag = 0;
        int iu0_training_course_idx = 0;
        // litDebug.Text = "emp_idx = " + emp_idx.ToString()+ " u3_training_course_idx = " + ViewState["u3_training_course_idx"].ToString();
        if (dataelearning.el_training_course_action != null)
        {
            ic = dataelearning.el_training_course_action.Count();
            var item = dataelearning.el_training_course_action[0];
            iresult_flag = item.result_flag;
            getu13_training_course_idx = item.u13_training_course_idx.ToString();
            getitem_count_h = item.item_count.ToString();
            getitem_total = item.item_total.ToString();
            getresult_flag = item.result_flag.ToString();
            iu0_training_course_idx = item.u0_training_course_idx;

        }


        if (ic > 0)
        {
            if (iresult_flag == 2)
            {
                //ViewState["u0_training_course_idx"] = iu0_training_course_idx;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "openModal_dv_massage_confrim();", true);
            }
            else
            {
                setActiveTab("elearning");
                MultiViewBody.SetActiveView(View_elearning);

                if (iresult_flag == 4)
                {
                    ViewState["elearning_status"] = "replay";
                    showdata_elaerning_replay(_func_dmu.zStringToInt(ViewState["u0_training_course_idx"].ToString()));
                }
                else
                {
                    showdata_elaerning(_func_dmu.zStringToInt(ViewState["u0_training_course_idx"].ToString()));
                }

            }
        }
        else
        {

            setActiveTab("elearning");
            MultiViewBody.SetActiveView(View_elearning);
            showdata_elaerning(_func_dmu.zStringToInt(ViewState["u0_training_course_idx"].ToString()));
        }
    }

    private void showdata_elaerning_replay(int id)
    {
        string _fullscreen = Request.Form["_fullscreen"];
        string _url_video = "", _url_image = "";
        data_elearning dataelearning = new data_elearning();
        training_course obj_detail = new training_course();
        dataelearning.el_training_course_action = new training_course[1];
        obj_detail.emp_idx_ref = emp_idx;
        obj_detail.u0_training_course_idx = id;
        obj_detail.operation_status_id = "u12_replay";
        dataelearning.el_training_course_action[0] = obj_detail;
        dataelearning = _func_dmu.zCallServicePostNetwork(_urlGetel_u_plan_course, dataelearning);

        if (dataelearning.el_training_course_action != null)
        {

            var item = dataelearning.el_training_course_action[0];

            Label lb_title_video = (Label)FV_elearning.FindControl("lb_title_video");

            lb_title_video.Text = item.course_name;

            getvideo_item = item.video_item.ToString();
            getvideo_title = item.video_title;
            getvideo_description = item.video_description;
            getu6_course_idx = item.u6_course_idx.ToString();
            getu3_training_course_idx = item.u3_training_course_idx.ToString();
            getitem_count = item.zitem_count.ToString();
            getitem_count_std = item.item_count_std.ToString();
            getcurrent_time_std = item.currenttime;
            getcurrent_time_std_int = item.currenttime_int.ToString();
            _url_video = getVideoUrl(item.course_no, item.video_name);
            _url_image = getImgUrl(item.course_no, item.video_images);

        }
        this.get_fullscreen = _fullscreen;
        javascript_Postback();
        css_Postback();
        this.url_video = ResolveUrl(_url_video);
        this.url_video_image = ResolveUrl(_url_image);
        //litDebug.Text = this.url_video_image;

    }

    public void getUrlTest(int id)
    {

        string sUrl = _func_dmu._LikeGenQrCode_test + _funcTool.getEncryptRC4(id.ToString(), "id") + "/" + _funcTool.getEncryptRC4("elearning", "type");
        //Response.Redirect(sUrl);
        // btn_msok_conf.NavigateUrl = sUrl;
        Page.Response.Redirect(sUrl, true);
        //ScriptManager.RegisterClientScriptBlock(this, GetType(), "", "window.open('" + sUrl + "', '', '');", true);
    }

    public Boolean getvisible_btn_elearning(int empidx = 0,
                             int zcourse_count = 0,
                             int course_type_elearning = 0
                             )
    {
        Boolean _Boolean = true;


        if ((empidx != emp_idx) || (zcourse_count > 0))
        {
            _Boolean = false;
        }
        if (course_type_elearning == 0)
        {

            _Boolean = false;
        }

        return _Boolean;
    }

    protected void css_Postback()
    {
        /*
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        sb.Append(@"<style type='text/css'>");
        sb.Append(@"#myVideo{  ");
        sb.Append(@"height: auto; width: 100%; margin: 0 0; display: block; border: 3px solid #D9B4B4;  ");
        if(Hddfld_myVideo.Value == "fullscreen")
        {
            sb.Append(@"position: fixed;  ");
            sb.Append(@"right: 0;  ");
            sb.Append(@"bottom: 0;  ");
            sb.Append(@"min-width: 100%;  ");
            sb.Append(@"min-height: 100%;  ");
        }
        sb.Append(@" } ");
        sb.Append(@" </style>");
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "_calcss", sb.ToString(), false);
        */
    }
    public string getplace_type(int place_type = 0)
    {
        string _string;
        if(place_type == 1)
        {
            _string = "ภายนอก";
        }
        else
        {
            _string = "ภายใน";
        }
        return _string;
    }

}