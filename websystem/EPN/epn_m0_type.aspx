﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="epn_m0_type.aspx.cs" Inherits="websystem_epn_m0_type" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <div class="col-lg-12">
            <asp:Image ID="Image10" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/qmr-problem/banner_qmr_group.png" Style="height: 100%; width: 100%;" />
        </div>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
    
    <asp:UpdatePanel ID="UpdatePnl" runat="server">
        
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">


                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title "><i class="fa fa-gears"></i><strong>&nbsp;ข้อมูลรายละเอียดประเภท Certificate</strong></h3>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <asp:LinkButton ID="btnshow" CssClass="btn btn-info btn-sm" data-toggle="tooltip" CommandName="btnCmn" OnCommand="btnCommand" title="Add System" runat="server"><i class="fa fa-plus"></i></asp:LinkButton>

                        </div>
                    </div>
                    <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Add Data Document Type </strong></h4>
                            <div class="form-horizontal" role="form">
                                <div class="panel-heading">

                                    <%-- text box --%>
                                    <div class="form-group">
                                        <asp:Label ID="lbcer" runat="server" Text="ชื่อประเภทเอกสาร :" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtnamecer" runat="server" CssClass="form-control" PlaceHolder="ระบุชื่อประเภทเอกสาร" />
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="สถานะการใช้เอกสาร" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlstatus" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1" Text="Online" />
                                                <asp:ListItem Value="0" Text="Offline" />
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="" />
                                        <div class="col-sm-6">
                                            <asp:LinkButton ID="btnsave" CssClass="btn btn-success btn-sm" data-toggle="tooltip" CommandName="btnCmnA" OnCommand="btnCommand" title="Save" runat="server">บันทึก</asp:LinkButton>

                                            <asp:LinkButton ID="btncancle" CssClass="btn btn-danger btn-sm" data-toggle="tooltip" CommandName="btnCmnD" OnCommand="btnCommand" title="Cancle" runat="server">ยกเลิก</asp:LinkButton>
                                        </div>
                                    </div>



                                </div>
                            </div>
                    </asp:Panel>
                    <asp:GridView ID="GvMaster" runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                        HeaderStyle-CssClass="primary"
                        HeaderStyle-Height="40px"
                        AllowPaging="true"
                        DataKeyNames="type_idx"
                        PageSize="10"
                        OnRowDataBound="Master_RowDataBound"
                        OnRowEditing="Master_RowEditing"
                        OnRowCancelingEdit="Master_RowCancelingEdit"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        OnRowUpdating="Master_RowUpdating">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">Data Cannot Be Found</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <%--กรณีแก้ไข--%>
                            <asp:TemplateField HeaderText="#">


                                <ItemTemplate>
                                    <asp:Label ID="lbltype_idx" runat="server" Visible="false" Text='<%# Eval("type_idx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </ItemTemplate>

                                <EditItemTemplate>
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">


                                            <asp:TextBox ID="txttype_idx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("type_idx")%>' />

                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="Name" CssClass="col-sm-3 control-label text_right"></asp:Label>



                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtname_edit" runat="server" CssClass="form-control" Text='<%# Eval("type_name")%>' />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtname_edit" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกระบบ"
                                                    ValidationExpression="กรุณากรอกระบบ"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                    ValidationGroup="Save_edit" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtname_edit"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                            </div>




                                            <div class="form-group">
                                                <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                <div class="col-sm-8">
                                                    <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("type_status") %>'>
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-10">
                                                    <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </EditItemTemplate>
                            </asp:TemplateField>



                            <asp:TemplateField HeaderText="ชื่อประเภท certificate" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lbstatus" runat="server" Text='<%# Eval("type_name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะการใช้งาน" ItemStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lbtype_status" runat="server" Text='<%# Eval("status_type_Name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จัดการข้อมูล" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                    <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("type_idx") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                </ItemTemplate>

                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>

    </asp:UpdatePanel>


</asp:Content>

