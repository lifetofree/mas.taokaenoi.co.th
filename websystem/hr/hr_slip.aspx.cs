using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ConfidentialProtect;
using SelectPdf;

public partial class websystem_hr_hr_slip : System.Web.UI.Page {
    #region initial function/data
    function_tool _funcTool = new function_tool ();
    CProtect _cProtect = new CProtect ();

    data_employee _data_employee = new data_employee ();
    data_hr_payroll _data_hr_payroll = new data_hr_payroll ();

    int _emp_idx = 0;
    int _default_int = 0;
    int _serviceType = 1;
    bool _b_permission = false;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile_Mobile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile_Mobile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlSlipGetSlipData = _serviceUrl + ConfigurationManager.AppSettings["urlSlipGetSlipData"];
    static string _urlSlipSetSlipData = _serviceUrl + ConfigurationManager.AppSettings["urlSlipSetSlipData"];

    static string _path_file_hr_eslip = ConfigurationManager.AppSettings["path_file_hr_eslip"];

    static int[] _permission = { 172, 23069 };
    #endregion initial function/data

    protected void Page_Init (object sender, EventArgs e) {
        _emp_idx = _funcTool.convertToInt (Session["emp_idx"].ToString ());

        // check permission
        foreach (int _pass in _permission) {
            if (_emp_idx == _pass) {
                _b_permission = true;
                continue;
            }
        }

        // if (!_b_permission) {
        //     Response.Redirect (ResolveUrl ("~/"));
        // }
    }

    protected void Page_Load (object sender, EventArgs e) {
        if (!IsPostBack) {
            initPage ();
        }

        // if (Session["auth"] == null) {
        //     setActiveTab ("viewAuth", 0);
        // }

        setTrigger ();

        // string valIn = "25000";
        // byte[] valOut;
        // valOut = _cProtect.CProtectEncrypt (Encoding.ASCII.GetBytes (valIn));
        // litDebug.Text = String.Format ("result encrypt : {0}", Encoding.ASCII.GetString (valOut));
        // valOut = _cProtect.CProtectDecrypt (valOut);
        // litDebug.Text += " | " + String.Format ("result decrypt : {0}", Encoding.ASCII.GetString (valOut)) + "<br />";
        // litDebug.Text += " | " + String.Format ("result type : {0}", Encoding.ASCII.GetString (valOut).GetType ()) + "<br />";

        // linkBtnTrigger (lbTest);
        // linkBtnTrigger (lbImport);
    }

    #region event command
    protected void navCommand (object sender, CommandEventArgs e) {
        string cmdName = e.CommandName.ToString ();
        string cmdArg = e.CommandArgument.ToString ();

        switch (cmdName) {
            case "navList":
                clearViewState ();
                setActiveTab ("viewList", 0);
                break;
            case "navImport":
                clearViewState ();
                setActiveTab ("viewImport", 0);
                break;
            case "navReport":
                clearViewState ();
                setActiveTab ("viewReport", 0);
                break;
        }
    }

    protected void btnCommand (object sender, CommandEventArgs e) {
        string cmdName = e.CommandName.ToString ();
        string cmdArg = e.CommandArgument.ToString ();

        switch (cmdName) {
            case "cmdAuth":
                // check password
                _data_employee = callServiceGetEmployee (_urlGetMyProfile_Mobile + _emp_idx.ToString ());
                try {
                    if (_funcTool.getMd5Sum (tbPassword.Text.Trim ()) == _data_employee.employee_list[0].emp_password) {
                        Session["auth"] = _funcTool.getRandomPasswordUsingGuid (32);
                        setActiveTab ("viewList", 0);
                    } else {
                        ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('รหัสผ่านไม่ถูกต้อง');", true);
                        setActiveTab ("viewAuth", 0);
                    }
                } catch (Exception ex) {
                    ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('รหัสผ่านไม่ถูกต้อง');", true);
                    setActiveTab ("viewAuth", 0);
                }
                break;
            case "cmdRelease":
                clearSession ();
                setActiveTab ("viewAuth", 0);
                break;
            case "cmdDownload":
                // if (ViewState["ImportData"] == null) { return; }
                _data_hr_payroll = new data_hr_payroll ();
                search_slip_data_detail _search_slip_list = new search_slip_data_detail ();
                _search_slip_list.s_u0_idx = cmdArg;
                _search_slip_list.s_emp_idx = "0";
                _data_hr_payroll.search_slip_data_list = new search_slip_data_detail[1];
                _data_hr_payroll.search_slip_data_list[0] = _search_slip_list;
                _data_hr_payroll = callServicePostHrPayroll (_urlSlipGetSlipData, _data_hr_payroll);
                // litDebug.Text = _funcTool.convertObjectToJson (_data_hr_payroll);
                // _data_hr_payroll = (data_hr_payroll)ViewState["ImportData"];
                en_slip_data_detail_u0 _tempData = new en_slip_data_detail_u0 ();
                try {
                    _tempData = _data_hr_payroll.en_slip_data_list_u0[0];
                } catch {
                    ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('ไม่พบข้อมูลค่ะ');", true);
                    return;
                }

                string body = string.Empty;
                using (StreamReader reader = new StreamReader (HttpContext.Current.Server.MapPath ("~/template/template_slip_tknl.html"))) {
                    body = reader.ReadToEnd ();
                }

                body = body.Replace ("{emp_code}", _tempData.emp_code);
                body = body.Replace ("{sec_name_th}", _tempData.sec_name_th);
                body = body.Replace ("{account_no}", _tempData.account_no);
                body = body.Replace ("{start_date}", _tempData.start_date);
                body = body.Replace ("{payroll_date}", _tempData.payroll_date);

                body = body.Replace ("{salary_rate}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_salary_rate)), 2).ToString ("N2"));
                body = body.Replace ("{slip_days}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_days)), 0).ToString ());
                body = body.Replace ("{slip_e_salary}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_e_salary)), 2).ToString ("N2"));
                body = body.Replace ("{slip_e_position}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_e_position)), 2).ToString ("N2"));
                body = body.Replace ("{slip_e_ot10}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_e_ot10)), 2).ToString ("N2"));
                body = body.Replace ("{slip_e_ot15}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_e_ot15)), 2).ToString ("N2"));
                body = body.Replace ("{slip_e_ot20}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_e_ot20)), 2).ToString ("N2"));
                body = body.Replace ("{slip_e_ot30}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_e_ot30)), 2).ToString ("N2"));
                body = body.Replace ("{slip_e_living}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_e_living)), 2).ToString ("N2"));
                body = body.Replace ("{slip_e_telephone}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_e_telephone)), 2).ToString ("N2"));
                body = body.Replace ("{slip_e_allowance}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_e_allowance)), 2).ToString ("N2"));
                body = body.Replace ("{slip_e_operating}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_e_operating)), 2).ToString ("N2"));
                body = body.Replace ("{slip_e_travel}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_e_travel)), 2).ToString ("N2"));
                body = body.Replace ("{slip_e_commission}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_e_commission)), 2).ToString ("N2"));
                body = body.Replace ("{slip_e_incentive}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_e_incentive)), 2).ToString ("N2"));
                body = body.Replace ("{slip_e_guarantee}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_e_guarantee)), 2).ToString ("N2"));
                body = body.Replace ("{slip_e_bonus}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_e_bonus)), 2).ToString ("N2"));
                body = body.Replace ("{slip_e_other}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_e_other)), 2).ToString ("N2"));
                body = body.Replace ("{slip_earnings}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_earnings)), 2).ToString ("N2"));

                body = body.Replace ("{slip_d_absent}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_d_absent)), 2).ToString ("N2"));
                body = body.Replace ("{slip_d_late}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_d_late)), 2).ToString ("N2"));
                body = body.Replace ("{slip_d_goback}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_d_goback)), 2).ToString ("N2"));
                body = body.Replace ("{slip_d_over}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_d_over)), 2).ToString ("N2"));
                body = body.Replace ("{slip_d_broken}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_d_broken)), 2).ToString ("N2"));
                body = body.Replace ("{slip_d_aia}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_d_aia)), 2).ToString ("N2"));
                body = body.Replace ("{slip_d_uniform}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_d_uniform)), 2).ToString ("N2"));
                body = body.Replace ("{slip_d_welfare}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_d_welfare)), 2).ToString ("N2"));
                body = body.Replace ("{slip_d_damage}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_d_damage)), 2).ToString ("N2"));
                body = body.Replace ("{slip_d_other}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_d_other)), 2).ToString ("N2"));
                body = body.Replace ("{slip_d_tax}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_d_tax)), 2).ToString ("N2"));
                body = body.Replace ("{slip_d_fund}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_d_fund)), 2).ToString ("N2"));
                body = body.Replace ("{slip_d_social}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_d_social)), 2).ToString ("N2"));
                body = body.Replace ("{slip_d_guarantee}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_d_guarantee)), 2).ToString ("N2"));
                body = body.Replace ("{slip_d_loan}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_d_loan)), 2).ToString ("N2"));
                body = body.Replace ("{slip_d_legal}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_d_legal)), 2).ToString ("N2"));
                body = body.Replace ("{slip_deduction}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_deduction)), 2).ToString ("N2"));
                body = body.Replace ("{slip_net}", _funcTool.setDecimalPlaces (_funcTool.convertToDecimal (getDecrypted (_tempData.en_slip_net)), 2).ToString ("N2"));

                // page setting value
                PdfPageSize pageSize = (PdfPageSize) Enum.Parse (typeof (PdfPageSize),
                    "A4", true);
                PdfPageOrientation pdfOrientation =
                    (PdfPageOrientation) Enum.Parse (typeof (PdfPageOrientation),
                        "Portrait", true);
                int webPageWidth = 860;
                int webPageHeight = 0;

                // instantiate a html to pdf converter object
                HtmlToPdf converter = new HtmlToPdf ();

                // set converter options
                converter.Options.PdfPageSize = pageSize;
                converter.Options.PdfPageOrientation = pdfOrientation;
                converter.Options.WebPageWidth = webPageWidth;
                converter.Options.WebPageHeight = webPageHeight;

                // set permission value
                string userPassword = getDecrypted (_tempData.slip_password);
                string ownerPassword = ""; //"67890";

                bool canAssembleDocument = false;
                bool canCopyContent = false;
                bool canEditAnnotations = false;
                bool canEditContent = false;
                bool canFillFormFields = false;
                bool canPrint = false;

                // set document passwords
                if (!string.IsNullOrEmpty (userPassword)) {
                    converter.Options.SecurityOptions.UserPassword = userPassword;
                }
                if (!string.IsNullOrEmpty (ownerPassword)) {
                    converter.Options.SecurityOptions.OwnerPassword = ownerPassword;
                }

                //set document permissions
                converter.Options.SecurityOptions.CanAssembleDocument = canAssembleDocument;
                converter.Options.SecurityOptions.CanCopyContent = canCopyContent;
                converter.Options.SecurityOptions.CanEditAnnotations = canEditAnnotations;
                converter.Options.SecurityOptions.CanEditContent = canEditContent;
                converter.Options.SecurityOptions.CanFillFormFields = canFillFormFields;
                converter.Options.SecurityOptions.CanPrint = canPrint;

                // create a new pdf document converting an url
                PdfDocument doc = converter.ConvertHtmlString (body);

                // save pdf document
                doc.Save (Response, false, "slip-" + _tempData.emp_code + ".pdf");

                // close pdf document
                doc.Close ();
                break;
            case "cmdImport":
                gvImport.Visible = false;
                gvImport2.Visible = false;

                RadioButtonList rblImportType = (RadioButtonList) fvUploadFiles.FindControl ("rblImportType");
                TextBox tbPayDate = (TextBox) fvUploadFiles.FindControl ("tbPayDate");
                FileUpload fuUpload = (FileUpload) fvUploadFiles.FindControl ("fuUpload");
                if (fuUpload.HasFile) {
                    ViewState["ImportData"] = null;

                    string dateTimeNow = DateTime.Now.ToString ("yyyyMMdd_HHmmss_ffff");
                    string FileName = Path.GetFileName (fuUpload.PostedFile.FileName);
                    string extension = Path.GetExtension (fuUpload.PostedFile.FileName);
                    string newFileName = dateTimeNow + extension.ToLower ();
                    string folderPath = _path_file_hr_eslip;
                    string filePath = Server.MapPath (folderPath + newFileName);
                    if (extension.ToLower () == ".xlsx") //extension.ToLower() == ".xls" || 
                    {
                        fuUpload.SaveAs (filePath);
                        string conStr = String.Empty;
                        if (extension.ToLower () == ".xls") {
                            conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
                        } else {
                            conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
                        }

                        conStr = String.Format (conStr, filePath, "Yes");
                        OleDbConnection connExcel = new OleDbConnection (conStr);
                        OleDbCommand cmdExcel = new OleDbCommand ();
                        OleDbDataAdapter oda = new OleDbDataAdapter ();
                        DataTable dt = new DataTable ();

                        cmdExcel.Connection = connExcel;
                        connExcel.Open ();
                        DataTable dtExcelSchema;
                        dtExcelSchema = connExcel.GetOleDbSchemaTable (OleDbSchemaGuid.Tables, null);
                        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString ();
                        connExcel.Close ();
                        connExcel.Open ();
                        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                        oda.SelectCommand = cmdExcel;
                        oda.Fill (dt);
                        connExcel.Close ();

                        File.Delete (filePath);

                        DataTable tableImport = new DataTable ();

                        int _count_row = 0;
                        int _count_table_row = dt.Rows.Count;
                        int _count_dt = 0;

                        switch (rblImportType.SelectedValue) {
                            case "1":
                                gvImport.Visible = true;
                                gvImport2.Visible = false;
                                // row 1
                                tableImport.Columns.Add ("emp_code", typeof (String));
                                tableImport.Columns.Add ("emp_name_th", typeof (String));

                                tableImport.Columns.Add ("slip_days", typeof (String)); //วัน
                                tableImport.Columns.Add ("slip_e_salary", typeof (String)); //เงินเดือน
                                tableImport.Columns.Add ("slip_e_position", typeof (String)); //ตำแหน่ง
                                tableImport.Columns.Add ("slip_e_ot10", typeof (String)); //OTx1
                                tableImport.Columns.Add ("slip_e_ot15", typeof (String)); //OTx1.5
                                tableImport.Columns.Add ("slip_e_ot20", typeof (String)); //OTx2
                                tableImport.Columns.Add ("slip_e_ot30", typeof (String)); //OTx3
                                tableImport.Columns.Add ("slip_e_living", typeof (String)); //ค่าครองชีพ
                                tableImport.Columns.Add ("slip_e_telephone", typeof (String)); //ค่าโทรศัพท์
                                tableImport.Columns.Add ("slip_e_allowance", typeof (String)); //เบี้ยขยัน
                                tableImport.Columns.Add ("slip_e_operating", typeof (String)); //ค่าปฏิบัติงาน
                                tableImport.Columns.Add ("slip_e_travel", typeof (String)); //ค่าเดินทาง
                                tableImport.Columns.Add ("slip_e_commission", typeof (String)); //ค่าคอมมิชชั่น
                                tableImport.Columns.Add ("slip_e_incentive", typeof (String)); //Incentive
                                tableImport.Columns.Add ("slip_e_guarantee", typeof (String)); //คืนค้ำประกัน
                                tableImport.Columns.Add ("slip_e_bonus", typeof (String)); //โบนัส
                                tableImport.Columns.Add ("slip_e_other", typeof (String)); //เงินได้อื่น
                                tableImport.Columns.Add ("slip_earnings", typeof (String)); //รวมรับ
                                // row 2
                                tableImport.Columns.Add ("slip_d_absent", typeof (String)); //ขาดงาน
                                tableImport.Columns.Add ("slip_d_late", typeof (String)); //มาสาย
                                tableImport.Columns.Add ("slip_d_goback", typeof (String)); //หักกลับก่อน
                                tableImport.Columns.Add ("slip_d_over", typeof (String)); //หักลาเกินสิทธิ์
                                tableImport.Columns.Add ("slip_d_broken", typeof (String)); //หักบกพร่อง
                                tableImport.Columns.Add ("slip_d_aia", typeof (String)); //หักAIA
                                tableImport.Columns.Add ("slip_d_uniform", typeof (String)); //หักเครื่องแบบ
                                tableImport.Columns.Add ("slip_d_welfare", typeof (String)); //หักสวัสดิการ
                                tableImport.Columns.Add ("slip_d_damage", typeof (String)); //ของเสียหาย
                                tableImport.Columns.Add ("slip_d_other", typeof (String)); //หักอื่นๆ
                                tableImport.Columns.Add ("slip_d_tax", typeof (String)); //หักภาษี
                                tableImport.Columns.Add ("slip_d_fund", typeof (String)); //หักกองทุน
                                tableImport.Columns.Add ("slip_d_social", typeof (String)); //หักประกันสังคม
                                tableImport.Columns.Add ("slip_d_guarantee", typeof (String)); //ค้ำประกัน
                                tableImport.Columns.Add ("slip_d_loan", typeof (String)); //หักเงินกู้
                                tableImport.Columns.Add ("slip_d_legal", typeof (String)); //หักเงินกรมบังคับคดี
                                tableImport.Columns.Add ("slip_deduction", typeof (String)); //รวมจ่าย
                                tableImport.Columns.Add ("slip_net", typeof (String)); //สุทธิ

                                // others
                                tableImport.Columns.Add ("payroll_date", typeof (String)); //วันที่จ่าย
                                tableImport.Columns.Add ("slip_password", typeof (String));

                                for (int i = 0; i <= _count_table_row - 1; i++) {
                                    if (
                                        (dt.Rows[i][0].ToString ().Trim () != String.Empty &&
                                            dt.Rows[i][1].ToString ().Trim () == String.Empty &&
                                            dt.Rows[i][2].ToString ().Trim () != String.Empty &&
                                            dt.Rows[i][3].ToString ().Trim () != String.Empty) &&
                                        (dt.Rows[i][0].ToString ().Trim () != "รวมทั้งหมด")
                                    ) {
                                        DataRow add_row = tableImport.NewRow ();
                                        // row 1
                                        add_row[0] = dt.Rows[i][0].ToString ().Trim ().Replace (",", "");
                                        add_row[1] = dt.Rows[i][2].ToString ().Trim ().Replace (",", "");
                                        add_row[2] = dt.Rows[i][3].ToString ().Trim ().Replace (",", "");
                                        add_row[3] = dt.Rows[i][4].ToString ().Trim ().Replace (",", "");
                                        add_row[4] = dt.Rows[i][5].ToString ().Trim ().Replace (",", "");
                                        add_row[5] = dt.Rows[i][6].ToString ().Trim ().Replace (",", "");
                                        add_row[6] = dt.Rows[i][7].ToString ().Trim ().Replace (",", "");
                                        add_row[7] = dt.Rows[i][8].ToString ().Trim ().Replace (",", "");
                                        add_row[8] = dt.Rows[i][9].ToString ().Trim ().Replace (",", "");
                                        add_row[9] = dt.Rows[i][10].ToString ().Trim ().Replace (",", "");
                                        add_row[10] = dt.Rows[i][11].ToString ().Trim ().Replace (",", "");
                                        add_row[11] = dt.Rows[i][12].ToString ().Trim ().Replace (",", "");
                                        add_row[12] = dt.Rows[i][13].ToString ().Trim ().Replace (",", "");
                                        add_row[13] = dt.Rows[i][14].ToString ().Trim ().Replace (",", "");
                                        add_row[14] = dt.Rows[i][15].ToString ().Trim ().Replace (",", "");
                                        add_row[15] = dt.Rows[i][16].ToString ().Trim ().Replace (",", "");
                                        add_row[16] = dt.Rows[i][17].ToString ().Trim ().Replace (",", "");
                                        add_row[17] = dt.Rows[i][18].ToString ().Trim ().Replace (",", "");
                                        add_row[18] = dt.Rows[i][19].ToString ().Trim ().Replace (",", "");
                                        add_row[19] = dt.Rows[i][20].ToString ().Trim ().Replace (",", "");
                                        // row 2
                                        add_row[20] = dt.Rows[i + 1][4].ToString ().Trim ().Replace (",", "");
                                        add_row[21] = dt.Rows[i + 1][5].ToString ().Trim ().Replace (",", "");
                                        add_row[22] = dt.Rows[i + 1][6].ToString ().Trim ().Replace (",", "");
                                        add_row[23] = dt.Rows[i + 1][7].ToString ().Trim ().Replace (",", "");
                                        add_row[24] = dt.Rows[i + 1][8].ToString ().Trim ().Replace (",", "");
                                        add_row[25] = dt.Rows[i + 1][9].ToString ().Trim ().Replace (",", "");
                                        add_row[26] = dt.Rows[i + 1][10].ToString ().Trim ().Replace (",", "");
                                        add_row[27] = dt.Rows[i + 1][11].ToString ().Trim ().Replace (",", "");
                                        add_row[28] = dt.Rows[i + 1][12].ToString ().Trim ().Replace (",", "");
                                        add_row[29] = dt.Rows[i + 1][13].ToString ().Trim ().Replace (",", "");
                                        add_row[30] = dt.Rows[i + 1][14].ToString ().Trim ().Replace (",", "");
                                        add_row[31] = dt.Rows[i + 1][15].ToString ().Trim ().Replace (",", "");
                                        add_row[32] = dt.Rows[i + 1][16].ToString ().Trim ().Replace (",", "");
                                        add_row[33] = dt.Rows[i + 1][17].ToString ().Trim ().Replace (",", "");
                                        add_row[34] = dt.Rows[i + 1][18].ToString ().Trim ().Replace (",", "");
                                        add_row[35] = dt.Rows[i + 1][19].ToString ().Trim ().Replace (",", "");
                                        add_row[36] = dt.Rows[i + 1][20].ToString ().Trim ().Replace (",", "");
                                        add_row[37] = dt.Rows[i + 1][21].ToString ().Trim ().Replace (",", "");
                                        // others
                                        add_row[38] = tbPayDate.Text.Trim ();
                                        add_row[39] = _funcTool.getRandomPasswordUsingGuid (5);

                                        tableImport.Rows.InsertAt (add_row, _count_row++);
                                    }
                                }

                                if (dt.Rows.Count > 0) {
                                    _data_hr_payroll.slip_data_list_u0 = new slip_data_detail_u0[tableImport.Rows.Count];
                                    _data_hr_payroll.en_slip_data_list_u0 = new en_slip_data_detail_u0[tableImport.Rows.Count];
                                    foreach (DataRow dr in tableImport.Rows) {
                                        slip_data_detail_u0 _slip_detail = new slip_data_detail_u0 ();
                                        en_slip_data_detail_u0 _en_slip_detail = new en_slip_data_detail_u0 ();
                                        // row1
                                        _slip_detail.emp_code = dr["emp_code"].ToString ();
                                        _slip_detail.emp_name_th = dr["emp_name_th"].ToString ();
                                        _slip_detail.slip_days = _funcTool.convertToDecimal (dr["slip_days"].ToString ());
                                        _slip_detail.slip_e_salary = _funcTool.convertToDecimal (dr["slip_e_salary"].ToString ());
                                        _slip_detail.slip_e_position = _funcTool.convertToDecimal (dr["slip_e_position"].ToString ());
                                        _slip_detail.slip_e_ot10 = _funcTool.convertToDecimal (dr["slip_e_ot10"].ToString ());
                                        _slip_detail.slip_e_ot15 = _funcTool.convertToDecimal (dr["slip_e_ot15"].ToString ());
                                        _slip_detail.slip_e_ot20 = _funcTool.convertToDecimal (dr["slip_e_ot20"].ToString ());
                                        _slip_detail.slip_e_ot30 = _funcTool.convertToDecimal (dr["slip_e_ot30"].ToString ());
                                        _slip_detail.slip_e_living = _funcTool.convertToDecimal (dr["slip_e_living"].ToString ());
                                        _slip_detail.slip_e_telephone = _funcTool.convertToDecimal (dr["slip_e_telephone"].ToString ());
                                        _slip_detail.slip_e_allowance = _funcTool.convertToDecimal (dr["slip_e_allowance"].ToString ());
                                        _slip_detail.slip_e_operating = _funcTool.convertToDecimal (dr["slip_e_operating"].ToString ());
                                        _slip_detail.slip_e_travel = _funcTool.convertToDecimal (dr["slip_e_travel"].ToString ());
                                        _slip_detail.slip_e_commission = _funcTool.convertToDecimal (dr["slip_e_commission"].ToString ());
                                        _slip_detail.slip_e_incentive = _funcTool.convertToDecimal (dr["slip_e_incentive"].ToString ());
                                        _slip_detail.slip_e_guarantee = _funcTool.convertToDecimal (dr["slip_e_guarantee"].ToString ());
                                        _slip_detail.slip_e_bonus = _funcTool.convertToDecimal (dr["slip_e_bonus"].ToString ());
                                        _slip_detail.slip_e_other = _funcTool.convertToDecimal (dr["slip_e_other"].ToString ());
                                        _slip_detail.slip_earnings = _funcTool.convertToDecimal (dr["slip_earnings"].ToString ());
                                        // row2
                                        _slip_detail.slip_d_absent = _funcTool.convertToDecimal (dr["slip_d_absent"].ToString ());
                                        _slip_detail.slip_d_late = _funcTool.convertToDecimal (dr["slip_d_late"].ToString ());
                                        _slip_detail.slip_d_goback = _funcTool.convertToDecimal (dr["slip_d_goback"].ToString ());
                                        _slip_detail.slip_d_over = _funcTool.convertToDecimal (dr["slip_d_over"].ToString ());
                                        _slip_detail.slip_d_broken = _funcTool.convertToDecimal (dr["slip_d_broken"].ToString ());
                                        _slip_detail.slip_d_aia = _funcTool.convertToDecimal (dr["slip_d_aia"].ToString ());
                                        _slip_detail.slip_d_uniform = _funcTool.convertToDecimal (dr["slip_d_uniform"].ToString ());
                                        _slip_detail.slip_d_welfare = _funcTool.convertToDecimal (dr["slip_d_welfare"].ToString ());
                                        _slip_detail.slip_d_damage = _funcTool.convertToDecimal (dr["slip_d_damage"].ToString ());
                                        _slip_detail.slip_d_other = _funcTool.convertToDecimal (dr["slip_d_other"].ToString ());
                                        _slip_detail.slip_d_tax = _funcTool.convertToDecimal (dr["slip_d_tax"].ToString ());
                                        _slip_detail.slip_d_fund = _funcTool.convertToDecimal (dr["slip_d_fund"].ToString ());
                                        _slip_detail.slip_d_social = _funcTool.convertToDecimal (dr["slip_d_social"].ToString ());
                                        _slip_detail.slip_d_guarantee = _funcTool.convertToDecimal (dr["slip_d_guarantee"].ToString ());
                                        _slip_detail.slip_d_loan = _funcTool.convertToDecimal (dr["slip_d_loan"].ToString ());
                                        _slip_detail.slip_d_legal = _funcTool.convertToDecimal (dr["slip_d_legal"].ToString ());
                                        _slip_detail.slip_deduction = _funcTool.convertToDecimal (dr["slip_deduction"].ToString ());
                                        _slip_detail.slip_net = _funcTool.convertToDecimal (dr["slip_net"].ToString ());
                                        _slip_detail.payroll_date = dr["payroll_date"].ToString ();
                                        _slip_detail.slip_password = dr["slip_password"].ToString ();
                                        // ---------- encrypt ---------- //
                                        // row1
                                        _en_slip_detail.emp_code = dr["emp_code"].ToString ();
                                        _en_slip_detail.emp_name_th = dr["emp_name_th"].ToString ();
                                        _en_slip_detail.en_slip_days = getEncrypted (_funcTool.convertToDecimal (dr["slip_days"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_e_salary = getEncrypted (_funcTool.convertToDecimal (dr["slip_e_salary"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_e_position = getEncrypted (_funcTool.convertToDecimal (dr["slip_e_position"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_e_ot10 = getEncrypted (_funcTool.convertToDecimal (dr["slip_e_ot10"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_e_ot15 = getEncrypted (_funcTool.convertToDecimal (dr["slip_e_ot15"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_e_ot20 = getEncrypted (_funcTool.convertToDecimal (dr["slip_e_ot20"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_e_ot30 = getEncrypted (_funcTool.convertToDecimal (dr["slip_e_ot30"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_e_living = getEncrypted (_funcTool.convertToDecimal (dr["slip_e_living"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_e_telephone = getEncrypted (_funcTool.convertToDecimal (dr["slip_e_telephone"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_e_allowance = getEncrypted (_funcTool.convertToDecimal (dr["slip_e_allowance"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_e_operating = getEncrypted (_funcTool.convertToDecimal (dr["slip_e_operating"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_e_travel = getEncrypted (_funcTool.convertToDecimal (dr["slip_e_travel"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_e_commission = getEncrypted (_funcTool.convertToDecimal (dr["slip_e_commission"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_e_incentive = getEncrypted (_funcTool.convertToDecimal (dr["slip_e_incentive"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_e_guarantee = getEncrypted (_funcTool.convertToDecimal (dr["slip_e_guarantee"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_e_bonus = getEncrypted (_funcTool.convertToDecimal (dr["slip_e_bonus"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_e_other = getEncrypted (_funcTool.convertToDecimal (dr["slip_e_other"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_earnings = getEncrypted (_funcTool.convertToDecimal (dr["slip_earnings"].ToString ()).ToString ());
                                        // row2
                                        _en_slip_detail.en_slip_d_absent = getEncrypted (_funcTool.convertToDecimal (dr["slip_d_absent"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_d_late = getEncrypted (_funcTool.convertToDecimal (dr["slip_d_late"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_d_goback = getEncrypted (_funcTool.convertToDecimal (dr["slip_d_goback"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_d_over = getEncrypted (_funcTool.convertToDecimal (dr["slip_d_over"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_d_broken = getEncrypted (_funcTool.convertToDecimal (dr["slip_d_broken"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_d_aia = getEncrypted (_funcTool.convertToDecimal (dr["slip_d_aia"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_d_uniform = getEncrypted (_funcTool.convertToDecimal (dr["slip_d_uniform"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_d_welfare = getEncrypted (_funcTool.convertToDecimal (dr["slip_d_welfare"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_d_damage = getEncrypted (_funcTool.convertToDecimal (dr["slip_d_damage"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_d_other = getEncrypted (_funcTool.convertToDecimal (dr["slip_d_other"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_d_tax = getEncrypted (_funcTool.convertToDecimal (dr["slip_d_tax"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_d_fund = getEncrypted (_funcTool.convertToDecimal (dr["slip_d_fund"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_d_social = getEncrypted (_funcTool.convertToDecimal (dr["slip_d_social"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_d_guarantee = getEncrypted (_funcTool.convertToDecimal (dr["slip_d_guarantee"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_d_loan = getEncrypted (_funcTool.convertToDecimal (dr["slip_d_loan"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_d_legal = getEncrypted (_funcTool.convertToDecimal (dr["slip_d_legal"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_deduction = getEncrypted (_funcTool.convertToDecimal (dr["slip_deduction"].ToString ()).ToString ());
                                        _en_slip_detail.en_slip_net = getEncrypted (_funcTool.convertToDecimal (dr["slip_net"].ToString ()).ToString ());
                                        // others
                                        _en_slip_detail.payroll_date = dr["payroll_date"].ToString ();
                                        _en_slip_detail.slip_password = getEncrypted (dr["slip_password"].ToString ());
                                        _en_slip_detail.cemp_idx = _emp_idx;

                                        _data_hr_payroll.slip_data_list_u0[_count_dt] = _slip_detail;
                                        _data_hr_payroll.en_slip_data_list_u0[_count_dt] = _en_slip_detail;
                                        _count_dt++;
                                    }
                                    lbSaveData.Visible = true;

                                    _funcTool.setGvData (gvImport, _data_hr_payroll.slip_data_list_u0);
                                    // _funcTool.setGvData (gvImportTest, _data_hr_payroll.en_slip_data_list_u0);
                                    ViewState["ImportData"] = _data_hr_payroll;
                                }
                                break;
                            case "2":
                                gvImport.Visible = false;
                                gvImport2.Visible = true;
                                // row 1
                                tableImport.Columns.Add ("emp_code", typeof (String));
                                tableImport.Columns.Add ("emp_name_th", typeof (String));
                                tableImport.Columns.Add ("salary_rate", typeof (String));
                                tableImport.Columns.Add ("payroll_date", typeof (String));

                                for (int i = 0; i <= _count_table_row - 1; i++) {
                                    if (
                                        (dt.Rows[i][0].ToString ().Trim () != String.Empty &&
                                            dt.Rows[i][1].ToString ().Trim () == String.Empty &&
                                            dt.Rows[i][2].ToString ().Trim () != String.Empty &&
                                            dt.Rows[i][3].ToString ().Trim () == String.Empty &&
                                            dt.Rows[i][4].ToString ().Trim () != String.Empty) &&
                                        (dt.Rows[i][0].ToString ().Trim () != "รหัส")
                                    ) {
                                        DataRow add_row2 = tableImport.NewRow ();
                                        // row 1
                                        add_row2[0] = dt.Rows[i][0].ToString ().Trim ().Replace (",", "");
                                        add_row2[1] = dt.Rows[i][2].ToString ().Trim ().Replace (",", "");
                                        add_row2[2] = dt.Rows[i][4].ToString ().Trim ().Replace (",", "");
                                        add_row2[3] = tbPayDate.Text.Trim ();

                                        tableImport.Rows.InsertAt (add_row2, _count_row++);
                                    }
                                }

                                if (dt.Rows.Count > 0) {
                                    _data_hr_payroll.slip_data_list_u0 = new slip_data_detail_u0[tableImport.Rows.Count];
                                    _data_hr_payroll.en_slip_data_list_u0 = new en_slip_data_detail_u0[tableImport.Rows.Count];
                                    foreach (DataRow dr in tableImport.Rows) {
                                        slip_data_detail_u0 _slip_detail2 = new slip_data_detail_u0 ();
                                        en_slip_data_detail_u0 _en_slip_detail2 = new en_slip_data_detail_u0 ();
                                        // row1
                                        _slip_detail2.emp_code = dr["emp_code"].ToString ();
                                        _slip_detail2.emp_name_th = dr["emp_name_th"].ToString ();
                                        _slip_detail2.salary_rate = _funcTool.convertToDecimal (dr["salary_rate"].ToString ());
                                        // ---------- encrypt ---------- //
                                        // row1
                                        _en_slip_detail2.emp_code = dr["emp_code"].ToString ();
                                        _en_slip_detail2.emp_name_th = dr["emp_name_th"].ToString ();
                                        _en_slip_detail2.en_salary_rate = getEncrypted (_funcTool.convertToDecimal (dr["salary_rate"].ToString ()).ToString ());
                                        _en_slip_detail2.payroll_date = dr["payroll_date"].ToString ();
                                        _en_slip_detail2.cemp_idx = _emp_idx;

                                        _data_hr_payroll.slip_data_list_u0[_count_dt] = _slip_detail2;
                                        _data_hr_payroll.en_slip_data_list_u0[_count_dt] = _en_slip_detail2;
                                        _count_dt++;
                                    }
                                    lbSaveData.Visible = true;

                                    _funcTool.setGvData (gvImport2, _data_hr_payroll.slip_data_list_u0);
                                    // _funcTool.setGvData (gvImportTest, _data_hr_payroll.en_slip_data_list_u0);
                                    ViewState["ImportData"] = _data_hr_payroll;
                                }
                                break;
                        }
                    }
                } else {
                    ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('ไม่พบไฟล์ที่ต้องการอัพโหลดค่ะ');", true);
                    return;
                }
                break;
            case "cmdSaveData":
                _data_hr_payroll = (data_hr_payroll) ViewState["ImportData"];
                RadioButtonList rblImportTypeS = (RadioButtonList) fvUploadFiles.FindControl ("rblImportType");
                _data_hr_payroll.slip_mode = "11" + rblImportTypeS.SelectedValue;
                //  litDebug.Text = _funcTool.convertObjectToJson(_data_hr_payroll);
                _data_hr_payroll = callServicePostHrPayroll (_urlSlipSetSlipData, _data_hr_payroll);
                ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('ดำเนินการเรียบร้อยแล้วค่ะ');", true);
                setActiveTab ("viewImport", 0);
                break;
        }
    }

    protected void rblSelectedIndexChanged (object sender, EventArgs e) {
        RadioButtonList rblName = (RadioButtonList) sender;
        _serviceType = _funcTool.convertToInt (rblName.SelectedValue);

        // switch (rblName.ID)
        // {
        //     case "rblServicesType2":
        //         setActiveView("viewList", _serviceType);
        //         break;
        // }
    }
    #endregion event command

    #region reuse
    protected void initPage () {
        clearSession ();
        clearViewState ();

        setActiveTab ("viewAuth", 0);
        // setActiveTab ("viewList", 0);

        hlSetTotop.Focus ();
    }

    protected void clearSession () {
        Session["auth"] = null;
    }

    protected void clearViewState () {
        // ViewState["auth"] = null;
        // ViewState["VisitorSelected"] = null;
        // ViewState["ReportList"] = null;
    }

    protected void setActiveTab (string activeTab, int doc_idx) {
        setActiveView (activeTab, doc_idx);
        setActiveTabBar (activeTab);

        divMenu.Style.Remove ("display");
        lbRelease.Visible = false;
        lbSaveData.Visible = false;

        switch (activeTab) {
            case "viewAuth":
                divMenu.Style.Add ("display", "none");
                break;
            case "viewList":
                lbRelease.Visible = true;
                ViewState["ImportData"] = null;
                search_slip_data_detail _search_slip_list = new search_slip_data_detail ();
                _search_slip_list.s_u0_idx = "0";
                _search_slip_list.s_emp_idx = _emp_idx.ToString ();
                _data_hr_payroll.search_slip_data_list = new search_slip_data_detail[1];
                _data_hr_payroll.search_slip_data_list[0] = _search_slip_list;
                //  litDebug.Text = _funcTool.convertObjectToJson(_data_hr_payroll);
                _data_hr_payroll = callServicePostHrPayroll (_urlSlipGetSlipData, _data_hr_payroll);
                _funcTool.setGvData (gvList, _data_hr_payroll.en_slip_data_list_u0);
                break;
            case "viewImport":
                lbRelease.Visible = true;
                ViewState["ImportData"] = null;
                _funcTool.setFvData (fvUploadFiles, FormViewMode.Insert, null);
                _funcTool.setGvData (gvImport, null);
                _funcTool.setGvData (gvImport2, null);
                gvImport.Visible = false;
                gvImport2.Visible = false;
                break;
            case "viewReport":
                lbRelease.Visible = true;
                break;
        }
    }

    protected void setActiveView (string activeTab, int doc_idx) {
        mvSystem.SetActiveView ((View) mvSystem.FindControl (activeTab));
    }

    protected data_employee callServiceGetEmployee (string _cmdUrl) {
        // call services
        _local_json = _funcTool.callServiceGet (_cmdUrl);

        // convert json to object
        _data_employee = (data_employee) _funcTool.convertJsonToObject (typeof (data_employee), _local_json);

        return _data_employee;
    }

    protected data_employee callServicePostEmployee (string _cmdUrl, data_employee _data_employee) {
        // convert to json
        _local_json = _funcTool.convertObjectToJson (_data_employee);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost (_cmdUrl, _local_json);

        // convert json to object
        _data_employee = (data_employee) _funcTool.convertJsonToObject (typeof (data_employee), _local_json);

        return _data_employee;
    }

    protected data_hr_payroll callServicePostHrPayroll (string _cmdUrl, data_hr_payroll _data_hr_payroll) {
        // convert to json
        _local_json = _funcTool.convertObjectToJson (_data_hr_payroll);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost (_cmdUrl, _local_json);

        // convert json to object
        _data_hr_payroll = (data_hr_payroll) _funcTool.convertJsonToObject (typeof (data_hr_payroll), _local_json);

        return _data_hr_payroll;
    }

    protected data_employee getOrganizationList () {
        _data_employee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details ();
        _orgList.org_idx = 0;
        _data_employee.organization_list[0] = _orgList;

        _data_employee = callServicePostEmployee (_urlGetOrganizationList, _data_employee);
        return _data_employee;
    }

    protected void setActiveTabBar (string activeTab) {
        switch (activeTab) {
            case "viewList":
                li0.Attributes.Add ("class", "active");
                li1.Attributes.Add ("class", "");
                li2.Attributes.Add ("class", "");
                break;
            case "viewImport":
                li0.Attributes.Add ("class", "");
                li1.Attributes.Add ("class", "active");
                li2.Attributes.Add ("class", "");
                break;
            case "viewReport":
                li0.Attributes.Add ("class", "");
                li1.Attributes.Add ("class", "");
                li2.Attributes.Add ("class", "active");
                break;
        }
    }

    protected void linkBtnTrigger (LinkButton linkBtnID) {
        UpdatePanel updatePanel = Page.Master.FindControl ("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger ();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add (triggerLinkBtn);
    }

    protected void linkGvTrigger (GridView linkGvID) {
        UpdatePanel updatePanel = Page.Master.FindControl ("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerGridView = new PostBackTrigger ();
        triggerGridView.ControlID = linkGvID.UniqueID;
        updatePanel.Triggers.Add (triggerGridView);
    }

    protected void linkRblTrigger (RadioButtonList linkRblID) {
        UpdatePanel updatePanel = Page.Master.FindControl ("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerRadioButtonList = new PostBackTrigger ();
        triggerRadioButtonList.ControlID = linkRblID.UniqueID;
        updatePanel.Triggers.Add (triggerRadioButtonList);
    }

    protected void setTrigger () {
        linkBtnTrigger (lbAuth);
        linkBtnTrigger (lbRelease);

        // nav trigger
        linkBtnTrigger (lbList);
        linkBtnTrigger (lbImport);
        linkBtnTrigger (lbReport);

        // fvUploadFiles trigger
        RadioButtonList rblImportType = (RadioButtonList) fvUploadFiles.FindControl ("rblImportType");
        LinkButton lbImportData = (LinkButton) fvUploadFiles.FindControl ("lbImportData");
        try { linkRblTrigger (rblImportType); } catch { }
        try { linkBtnTrigger (lbImportData); } catch { }

        try { linkBtnTrigger (lbSaveData); } catch { }

        // slip list
        try { linkGvTrigger (gvList); } catch { }
    }

    protected string getEncrypted (string dataIn) {
        return _cProtect.CProtectEncrypt (dataIn);
    }

    protected string getDecrypted (string dataIn) {
        return _cProtect.CProtectDecrypt (dataIn);
    }
    #endregion reuse
}