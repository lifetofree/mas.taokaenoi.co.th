﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="labcal-tool-print.aspx.cs" Inherits="websystem_qa_labcal_tool_print" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />

    <title>Tool Print</title>
    <style type="text/css" media="print,screen">
        @page {
            size: A4 landscape;
            margin: 25px;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
            }
        }

        .formPrint {
            margin: 10pt; /*from 5 to 10*/
            padding: 10pt; /*add*/
            width: 842pt;
        }
    </style>
</head>
<body onload="window.print()">
    <form id="form1" runat="server" width="100%">
        <script src='<%=ResolveUrl("~/Scripts/jquery-3.1.1.min.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/bootstrap.js") %>'></script>
        <script src='<%=ResolveUrl("~/Scripts/custom.js") %>'></script>

        <div class="formPrint">
            <div class="headOrg" style="text-align: center; padding: 10px;">
                รายการเครื่องมือ
            </div>


            <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse: collapse;">

                <tbody>
                    <tr class="info" style="font-size: Small; height: 40px;">
                        <th scope="col" style="width: 2%;">ลำดับ</th>
                        <th scope="col" style="width: 10%;">ประเภทเครื่องมือ</th>
                        <th scope="col" style="width: 10%;">ชื่อเครื่องมือ</th>
                        <th scope="col" style="width: 10%;">รหัสเครื่อง</th>
                        <th scope="col" style="width: 10%;">หมายเลขเครื่อง</th>
                        <th scope="col" style="width: 10%;">ผู้ถือครอง</th>
                        <th scope="col" style="width: 10%;">ช่วงการใช้งาน</th>
                        <th scope="col" style="width: 5%;">สถานะ</th>
                        <th scope="col" style="width: 8%;">จำนวนครั้งสอบเทียบ</th>
                        <th scope="col" style="width: 13%;">วันที่สอบเทียบล่าสุด</th>
                        <th scope="col" style="width: 15%;">วันที่สอบเทียบครั้งถัดไป</th>
                    </tr>
                    <tr style="font-size: Small; text-align: center;">
                        <td>1</td>
                        <td>Digital</td>
                        <td>เครื่องชั่ง 1.5 Kg</td>
                        <td>LAB 1001 E</td>
                        <td>10092883707</td>
                        <td>QA LAB</td>
                        <td>1 - 300 g</td>
                        <td>Online</td>
                        <td>5</td>
                        <td>06/07/2017</td>
                        <td>06/08/2017</td>
                    </tr>

                </tbody>

            </table>
            <br />
            <div class="headPrint">
                PRINT DATE :
                 <asp:Label ID="lblPrintDate" runat="server" CssClass="headPrint" />
            </div>
        </div>

    </form>
</body>
</html>
