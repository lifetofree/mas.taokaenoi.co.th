using System;
using System.Xml.Serialization;

#region data_adonline
[Serializable]
[XmlRoot("data_adonline")]
public class data_adonline
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("on_table_u1")]
    public string on_table_u1 { get; set; }
    [XmlElement("return_perm")]
    public string return_perm { get; set; }
    [XmlElement("return_temp")]
    public string return_temp { get; set; }
    [XmlElement("return_ad")]
    public string return_ad { get; set; }

    [XmlElement("return_user")]
    public string return_user { get; set; }

    [XmlElement("return_typeemail")]
    public string return_typeemail { get; set; }

    [XmlElement("return_mailprivate")]
    public string return_mailprivate { get; set; }

    [XmlElement("return_mailcenter")]
    public string return_mailcenter { get; set; }

    [XmlElement("return_vpnsap")]
    public string return_vpnsap { get; set; }

    [XmlElement("return_vpnbi")]
    public string return_vpnbi { get; set; }

    [XmlElement("return_vpnexpress")]
    public string return_vpnexpress { get; set; }

    [XmlElement("return_mailreplace")]
    public string return_mailreplace { get; set; }

    [XmlElement("ad_permission_type_action")]
    public permission_type[] ad_permission_type_action { get; set; }
    [XmlElement("ad_permission_policy_action")]
    public permission_policy[] ad_permission_policy_action { get; set; }
    [XmlElement("ad_permission_category_action")]
    public permission_category[] ad_permission_category_action { get; set; }
    [XmlElement("ad_employee_lifetime_type_action")]
    public employee_lifetime_type[] ad_employee_lifetime_type_action { get; set; }
    [XmlElement("ad_online_action")]
    public ad_online[] ad_online_action { get; set; }
    [XmlElement("ad_online_perm_temp_insert_u1_action")]
    public ad_online_perm_temp_insert_u1[] ad_online_perm_temp_insert_u1_action { get; set; }
    [XmlElement("ad_log_online_action")]
    public ad_log_online[] ad_log_online_action { get; set; }
    [XmlElement("ad_crud_node_action")]
    public ad_crud_node[] ad_crud_node_action { get; set; }
    [XmlElement("ad_online_u2vpn_action")]
    public ad_u2vpn_online[] ad_online_u2vpn_action { get; set; }
    [XmlElement("ad_online_u3vpn_action")]
    public ad_u3vpn_online[] ad_online_u3vpn_action { get; set; }
}
#endregion data_adonline

#region permission_type
[Serializable]
public class permission_type
{
    [XmlElement("m0_perm_type_idx")]
    public int m0_perm_type_idx { get; set; }
    [XmlElement("perm_type_name")]
    public string perm_type_name { get; set; }
    [XmlElement("perm_type_status")]
    public int perm_type_status { get; set; }
    [XmlElement("m0_perm_cate_idx_ref")]
    public string m0_perm_cate_idx_ref { get; set; }
    [XmlElement("m0_emp_type_idx_ref")]
    public string m0_emp_type_idx_ref { get; set; }
    [XmlElement("keyword_search_perm_type")]
    public string keyword_search_perm_type { get; set; }
}
#endregion permission_type

#region permission_policy
[Serializable]
public class permission_policy
{
    [XmlElement("m0_perm_pol_idx")]
    public int m0_perm_pol_idx { get; set; }
    [XmlElement("perm_pol_name")]
    public string perm_pol_name { get; set; }
    [XmlElement("perm_pol_desc")]
    public string perm_pol_desc { get; set; }
    [XmlElement("perm_pol_status")]
    public int perm_pol_status { get; set; }
}
#endregion permission_policy

#region permission_category
[Serializable]
public class permission_category
{
    [XmlElement("m0_perm_cate_idx")]
    public int m0_perm_cate_idx { get; set; }
    [XmlElement("perm_cate_name")]
    public string perm_cate_name { get; set; }
    [XmlElement("perm_cate_status")]
    public int perm_cate_status { get; set; }
    [XmlElement("m0_perm_cate_idx_string")]
    public string m0_perm_cate_idx_string { get; set; }
    [XmlElement("keyword_search_perm_cate")]
    public string keyword_search_perm_cate { get; set; }
}
#endregion permission_category

#region employee_lifetime_type
[Serializable]
public class employee_lifetime_type
{
    [XmlElement("m0_emp_type_idx")]
    public int m0_emp_type_idx { get; set; }
    [XmlElement("emp_type_name")]
    public string emp_type_name { get; set; }
    [XmlElement("emp_type_status")]
    public int emp_type_status { get; set; }
    [XmlElement("m0_emp_type_idx_string")]
    public string m0_emp_type_idx_string { get; set; }
    [XmlElement("keyword_search_emp_type")]
    public string keyword_search_emp_type { get; set; }
}
#endregion employee_lifetime_type

#region ad_online
[Serializable]
public class ad_online
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("emp_code")]
    public string emp_code { get; set; }
    [XmlElement("emp_firstname_th")]
    public string emp_firstname_th { get; set; }
    [XmlElement("emp_lastname_th")]
    public string emp_lastname_th { get; set; }
    [XmlElement("emp_firstname_en")]
    public string emp_firstname_en { get; set; }
    [XmlElement("emp_lastname_en")]
    public string emp_lastname_en { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }
    [XmlElement("jobgrade_level")]
    public int jobgrade_level { get; set; }
    [XmlElement("costcenter_no")]
    public int costcenter_no { get; set; }

    [XmlElement("job_level")]
    public int job_level { get; set; }
    [XmlElement("EmpCode")]
    public string EmpCode { get; set; }
    [XmlElement("JobLevel")]
    public int JobLevel { get; set; }
    [XmlElement("jobgrade_idx")]
    public int jobgrade_idx { get; set; }
    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }
    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("on_table")]
    public string on_table { get; set; }
    [XmlElement("with_where")]
    public string with_where { get; set; }
    [XmlElement("emails_for_send")]
    public string emails_for_send { get; set; }

    [XmlElement("from_table")]
    public string from_table { get; set; }
    [XmlElement("u0_pk_perm_idx_ref")]
    public int u0_pk_perm_idx_ref { get; set; }

    [XmlElement("status_from_1")]
    public string status_from_1 { get; set; }
    [XmlElement("status_from_2")]
    public string status_from_2 { get; set; }
    [XmlElement("status_to_1")]
    public string status_to_1 { get; set; }
    [XmlElement("status_to_2")]
    public string status_to_2 { get; set; }
    [XmlElement("u0_node_idx_ref")]
    public int u0_node_idx_ref { get; set; }

    /** START Permission Temporary **/
    [XmlElement("perm_temp_alphabet")]
    public string perm_temp_alphabet { get; set; }
    [XmlElement("perm_temp_org")]
    public string perm_temp_org { get; set; }

    [XmlElement("u1_perm_temp_contact")]
    public string u1_perm_temp_contact { get; set; }
    [XmlElement("u1_perm_temp_contact_amount")]
    public string u1_perm_temp_contact_amount { get; set; }

    [XmlElement("u0_perm_temp_idx")]
    public int u0_perm_temp_idx { get; set; }
    [XmlElement("m0_emp_type_idx_ref")]
    public int m0_emp_type_idx_ref { get; set; }
    [XmlElement("u0_perm_temp_code_id")]
    public string u0_perm_temp_code_id { get; set; }
    [XmlElement("u0_perm_temp_comp_name")]
    public string u0_perm_temp_comp_name { get; set; }
    [XmlElement("u0_perm_temp_detail")]
    public string u0_perm_temp_detail { get; set; }
    [XmlElement("u0_cm_director_creator")]
    public string u0_cm_director_creator { get; set; }
    [XmlElement("u0_cm_head_it")]
    public string u0_cm_head_it { get; set; }
    [XmlElement("u0_cm_director_it")]
    public string u0_cm_director_it { get; set; }
    [XmlElement("u0_perm_temp_status")]
    public int u0_perm_temp_status { get; set; }
    [XmlElement("u0_perm_temp_created_at")]
    public string u0_perm_temp_created_at { get; set; }
    [XmlElement("u0_perm_temp_created_by")]
    public int u0_perm_temp_created_by { get; set; }
    [XmlElement("u0_perm_temp_updated_at")]
    public string u0_perm_temp_updated_at { get; set; }
    [XmlElement("u0_perm_temp_updated_by")]
    public int u0_perm_temp_updated_by { get; set; }

    [XmlElement("u1_perm_temp_idx")]
    public int u1_perm_temp_idx { get; set; }
    [XmlElement("u0_perm_temp_idx_ref")]
    public int u0_perm_temp_idx_ref { get; set; }
    [XmlElement("u1_perm_temp_idcard")]
    public string u1_perm_temp_idcard { get; set; }
    [XmlElement("u1_perm_temp_fullname")]
    public string u1_perm_temp_fullname { get; set; }
    [XmlElement("u1_perm_temp_start")]
    public string u1_perm_temp_start { get; set; }
    [XmlElement("u1_perm_temp_end")]
    public string u1_perm_temp_end { get; set; }
    [XmlElement("u1_perm_temp_username")]
    public string u1_perm_temp_username { get; set; }
    [XmlElement("u1_perm_temp_password")]
    public string u1_perm_temp_password { get; set; }
    [XmlElement("u1_perm_temp_status")]
    public int u1_perm_temp_status { get; set; }
    [XmlElement("u1_perm_temp_updated_at")]
    public string u1_perm_temp_updated_at { get; set; }
    [XmlElement("u1_perm_temp_updated_by")]
    public int u1_perm_temp_updated_by { get; set; }

    [XmlElement("filter_u0_perm_temp_code_id")]
    public string filter_u0_perm_temp_code_id { get; set; }
    [XmlElement("filter_u0_perm_temp_creator")]
    public string filter_u0_perm_temp_creator { get; set; }
    [XmlElement("filter_u0_perm_temp_comp_name")]
    public string filter_u0_perm_temp_comp_name { get; set; }
    [XmlElement("filter_u0_perm_temp_org")]
    public string filter_u0_perm_temp_org { get; set; }
    [XmlElement("filter_u0_perm_temp_dept")]
    public string filter_u0_perm_temp_dept { get; set; }
    [XmlElement("filter_u0_perm_temp_node")]
    public string filter_u0_perm_temp_node { get; set; }
    [XmlElement("filter_u1_perm_temp_contact")]
    public string filter_u1_perm_temp_contact { get; set; }
    [XmlElement("filter_u0_perm_temp_condition_date")]
    public string filter_u0_perm_temp_condition_date { get; set; }
    [XmlElement("filter_u0_perm_temp_date_only")]
    public string filter_u0_perm_temp_date_only { get; set; }
    [XmlElement("filter_u0_perm_temp_date_from")]
    public string filter_u0_perm_temp_date_from { get; set; }
    [XmlElement("filter_u0_perm_temp_date_to")]
    public string filter_u0_perm_temp_date_to { get; set; }
    /** END Permission Temporary **/

    /** START Permission Permanent **/
    [XmlElement("perm_perm_alphabet")]
    public string perm_perm_alphabet { get; set; }
    [XmlElement("perm_perm_org")]
    public string perm_perm_org { get; set; }
    [XmlElement("u0_node_idx_ref_current")]
    public int u0_node_idx_ref_current { get; set; }

    [XmlElement("m0_perm_pol_idx")]
    public int m0_perm_pol_idx { get; set; }
    [XmlElement("perm_pol_name")]
    public string perm_pol_name { get; set; }
    [XmlElement("perm_pol_desc")]
    public string perm_pol_desc { get; set; }
    [XmlElement("perm_pol_status")]
    public int perm_pol_status { get; set; }

    [XmlElement("u0_perm_perm_idx")]
    public int u0_perm_perm_idx { get; set; }
    [XmlElement("m0_perm_pol_idx_ref")]
    public int m0_perm_pol_idx_ref { get; set; }
    [XmlElement("u0_perm_perm_emp_idx_ref")]
    public int u0_perm_perm_emp_idx_ref { get; set; }
    [XmlElement("u0_perm_perm_code_id")]
    public string u0_perm_perm_code_id { get; set; }
    [XmlElement("u0_perm_perm_sap")]
    public string u0_perm_perm_sap { get; set; }
    [XmlElement("u0_perm_perm_mail_center")]
    public string u0_perm_perm_mail_center { get; set; }
    [XmlElement("u0_perm_perm_mail_priv")]
    public string u0_perm_perm_mail_priv { get; set; }
    [XmlElement("u0_perm_perm_ad")]
    public string u0_perm_perm_ad { get; set; }
    [XmlElement("u0_perm_perm_cm_head_emp")]
    public string u0_perm_perm_cm_head_emp { get; set; }
    [XmlElement("u0_perm_perm_cm_director_emp")]
    public string u0_perm_perm_cm_director_emp { get; set; }
    [XmlElement("u0_perm_perm_cm_director_hr")]
    public string u0_perm_perm_cm_director_hr { get; set; }
    [XmlElement("u0_perm_perm_cm_head_it")]
    public string u0_perm_perm_cm_head_it { get; set; }
    [XmlElement("u0_perm_perm_cm_director_it")]
    public string u0_perm_perm_cm_director_it { get; set; }
    [XmlElement("u0_perm_perm_cm_it")]
    public string u0_perm_perm_cm_it { get; set; }
    [XmlElement("u0_perm_perm_status")]
    public int u0_perm_perm_status { get; set; }
    [XmlElement("u0_perm_perm_created_at")]
    public string u0_perm_perm_created_at { get; set; }
    [XmlElement("u0_perm_perm_created_by")]
    public int u0_perm_perm_created_by { get; set; }
    [XmlElement("u0_perm_perm_updated_at")]
    public string u0_perm_perm_updated_at { get; set; }
    [XmlElement("u0_perm_perm_updated_by")]
    public int u0_perm_perm_updated_by { get; set; }

    [XmlElement("u1_perm_perm_idx")]
    public int u1_perm_perm_idx { get; set; }
    [XmlElement("u0_perm_perm_idx_ref")]
    public int u0_perm_perm_idx_ref { get; set; }
    [XmlElement("u1_perm_perm_cm_it")]
    public string u1_perm_perm_cm_it { get; set; }
    [XmlElement("u1_perm_perm_created_at")]
    public string u1_perm_perm_created_at { get; set; }
    [XmlElement("u1_perm_perm_created_at_date")]
    public string u1_perm_perm_created_at_date { get; set; }
    [XmlElement("u1_perm_perm_created_at_time")]
    public string u1_perm_perm_created_at_time { get; set; }
    [XmlElement("u1_perm_perm_created_by")]
    public int u1_perm_perm_created_by { get; set; }

    [XmlElement("emp_idx_creator")]
    public int emp_idx_creator { get; set; }
    [XmlElement("emp_code_creator")]
    public string emp_code_creator { get; set; }
    [XmlElement("emp_name_th_creator")]
    public string emp_name_th_creator { get; set; }

    [XmlElement("filter_u0_perm_perm_code_id")]
    public string filter_u0_perm_perm_code_id { get; set; }
    [XmlElement("filter_u0_perm_perm_empcode_fullnameth_applicant")]
    public string filter_u0_perm_perm_empcode_fullnameth_applicant { get; set; }
    [XmlElement("filter_u0_perm_perm_org")]
    public string filter_u0_perm_perm_org { get; set; }
    [XmlElement("filter_u0_perm_perm_dept")]
    public string filter_u0_perm_perm_dept { get; set; }
    [XmlElement("filter_u0_perm_perm_sec")]
    public string filter_u0_perm_perm_sec { get; set; }
    [XmlElement("filter_u0_perm_perm_pos")]
    public string filter_u0_perm_perm_pos { get; set; }
    [XmlElement("filter_u0_perm_perm_node")]
    public string filter_u0_perm_perm_node { get; set; }
    [XmlElement("filter_u0_perm_perm_condition_date")]
    public string filter_u0_perm_perm_condition_date { get; set; }
    [XmlElement("filter_u0_perm_perm_date_only")]
    public string filter_u0_perm_perm_date_only { get; set; }
    [XmlElement("filter_u0_perm_perm_date_from")]
    public string filter_u0_perm_perm_date_from { get; set; }
    [XmlElement("filter_u0_perm_perm_date_to")]
    public string filter_u0_perm_perm_date_to { get; set; }

    [XmlElement("u0_perm_perm_date_use")]
    public string u0_perm_perm_date_use { get; set; }
    [XmlElement("u0_perm_perm_mail_replace")]
    public string u0_perm_perm_mail_replace { get; set; }
    [XmlElement("u0_perm_perm_type_mail")]
    public int u0_perm_perm_type_mail { get; set; }
    [XmlElement("u0_perm_perm_reason")]
    public string u0_perm_perm_reason { get; set; }

    [XmlElement("u0_perm_perm_vpn_sap")]
    public int u0_perm_perm_vpn_sap { get; set; }
    [XmlElement("u0_perm_perm_vpn_sap_reason")]
    public string u0_perm_perm_vpn_sap_reason { get; set; }

    [XmlElement("u0_perm_perm_vpn_bi")]
    public int u0_perm_perm_vpn_bi { get; set; }
    [XmlElement("u0_perm_perm_vpn_bi_reason")]
    public string u0_perm_perm_vpn_bi_reason { get; set; }

    [XmlElement("u0_perm_perm_vpn_express")]
    public int u0_perm_perm_vpn_express { get; set; }
    [XmlElement("u0_perm_perm_vpn_express_reason")]
    public string u0_perm_perm_vpn_express_reason { get; set; }

    [XmlElement("u0_perm_perm_cm_it_before")]
    public string u0_perm_perm_cm_it_before { get; set; }


    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("u2_perm_perm_idx")]
    public int u2_perm_perm_idx { get; set; }


    [XmlElement("vpn1idx")]
    public int vpn1idx { get; set; }


    [XmlElement("vpnidx")]
    public int vpnidx { get; set; }


    [XmlElement("vpn_name")]
    public string vpn_name { get; set; }


    [XmlElement("vpn1_name")]
    public string vpn1_name { get; set; }

    [XmlElement("remark")]
    public string remark { get; set; }

    [XmlElement("condition_addmore")]
    public int condition_addmore { get; set; }

    

    /** END Permission Permanent **/

   


}
#endregion ad_online


#region ad_u2vpn_online
[Serializable]
public class ad_u2vpn_online
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement("uemp_idx")]
    public int uemp_idx { get; set; }
    [XmlElement("condition")]
    public int condition { get; set; }
    [XmlElement("u0_perm_temp_idx")]
    public int u0_perm_temp_idx { get; set; }

    /** use vpn **/
    [XmlElement("vpnidx")]
    public int vpnidx { get; set; }
    [XmlElement("vpn_name")]
    public string vpn_name { get; set; }
    [XmlElement("vpn1idx")]
    public int vpn1idx { get; set; }
    [XmlElement("vpn1_name")]
    public string vpn1_name { get; set; }
    [XmlElement("remark")]
    public string remark { get; set; }
    [XmlElement("table_vpn")]
    public string table_vpn { get; set; }
    [XmlElement("on_table")]
    public string on_table { get; set; }

    [XmlElement("u2_perm_perm_status")]
    public int u2_perm_perm_status { get; set; }

}

[Serializable]
public class ad_u3vpn_online
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }
    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }
    [XmlElement("uemp_idx")]
    public int uemp_idx { get; set; }
    [XmlElement("condition")]
    public int condition { get; set; }
    [XmlElement("u0_perm_temp_idx")]
    public int u0_perm_temp_idx { get; set; }

    /** use vpn **/
    [XmlElement("vpnidx")]
    public int vpnidx { get; set; }
    [XmlElement("vpn_name")]
    public string vpn_name { get; set; }
    [XmlElement("vpn1idx")]
    public int vpn1idx { get; set; }
    [XmlElement("vpn1_name")]
    public string vpn1_name { get; set; }
    [XmlElement("remark")]
    public string remark { get; set; }
    [XmlElement("table_vpn")]
    public string table_vpn { get; set; }
    [XmlElement("on_table")]
    public string on_table { get; set; }

    [XmlElement("u3_perm_perm_status")]
    public int u3_perm_perm_status { get; set; }

}
#endregion ad_online

#region ad_online_perm_temp_insert_u1
[Serializable]
public class ad_online_perm_temp_insert_u1
{
    [XmlElement("u0_perm_temp_idx_ref")]
    public int u0_perm_temp_idx_ref { get; set; }
    [XmlElement("u1_perm_temp_idcard")]
    public string u1_perm_temp_idcard { get; set; }
    [XmlElement("u1_perm_temp_fullname")]
    public string u1_perm_temp_fullname { get; set; }
    [XmlElement("u1_perm_temp_start")]
    public string u1_perm_temp_start { get; set; }
    [XmlElement("u1_perm_temp_end")]
    public string u1_perm_temp_end { get; set; }
    [XmlElement("u1_perm_temp_username")]
    public string u1_perm_temp_username { get; set; }
    [XmlElement("u1_perm_temp_password")]
    public string u1_perm_temp_password { get; set; }
    [XmlElement("u1_perm_temp_status")]
    public int u1_perm_temp_status { get; set; }
    [XmlElement("u1_perm_temp_updated_at")]
    public string u1_perm_temp_updated_at { get; set; }
    [XmlElement("u1_perm_temp_updated_by")]
    public int u1_perm_temp_updated_by { get; set; }
}
#endregion ad_online_insert_u1

#region ad_log_online
[Serializable]
public class ad_log_online
{
    [XmlElement("l0_perm_log_idx")]
    public int l0_perm_log_idx { get; set; }
    [XmlElement("from_table")]
    public string from_table { get; set; }
    [XmlElement("u0_pk_perm_idx_ref")]
    public int u0_pk_perm_idx_ref { get; set; }
    [XmlElement("u0_node_idx_ref")]
    public int u0_node_idx_ref { get; set; }
    [XmlElement("perm_log_created_at")]
    public string perm_log_created_at { get; set; }
    [XmlElement("perm_log_created_by")]
    public int perm_log_created_by { get; set; }

    [XmlElement("perm_log_created_at_date")]
    public string perm_log_created_at_date { get; set; }
    [XmlElement("perm_log_created_at_time")]
    public string perm_log_created_at_time { get; set; }
    [XmlElement("node_name_1")]
    public string node_name_1 { get; set; }
    [XmlElement("status_from_1")]
    public string status_from_1 { get; set; }
    [XmlElement("status_from_2")]
    public string status_from_2 { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }
}
#endregion ad_log_online

#region ad_crud_node
[Serializable]
public class ad_crud_node
{
    [XmlElement("from_table")]
    public string from_table { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }
    [XmlElement("node_name")]
    public string node_name { get; set; }
    [XmlElement("node_desc")]
    public string node_desc { get; set; }
    [XmlElement("node_status")]
    public int node_status { get; set; }
}
#endregion ad_crud_node

