﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_mis_u0_tel : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();
    data_employee _dtEmployee = new data_employee();


    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string urlSelectLocate_Tel = _serviceUrl + ConfigurationManager.AppSettings["urlSelectLocate_Tel"];
    static string urlSelectSubTel = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSubTel"];
    static string urlInsertSubTel = _serviceUrl + ConfigurationManager.AppSettings["urlInsertSubTel"];
    static string urlUpdateSubTel = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateSubTel"];
    static string urlDeleteSubTel = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteSubTel"];
    static string urlSelectMainTel = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMainTel"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];



    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            select_empIdx_present();
            SelectMasterList();

        }
    }

    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

    }

    protected void SelectLocate(DropDownList ddlName)
    {
        _dtEmployee = new data_employee();
        _dtEmployee.employee_list = new employee_detail[1];
        employee_detail _dtemp = new employee_detail();

        _dtEmployee.employee_list[0] = _dtemp;

        _dtEmployee = callServicePostEmp(urlSelectLocate_Tel, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.employee_list, "LocName", "LocIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสถานที่....", "0"));

    }

    protected void SelectTel(DropDownList ddlName, int locate)
    {
        _dtEmployee = new data_employee();
        _dtEmployee.BoxTel_list = new Tel_List[1];
        Tel_List _dtemp = new Tel_List();
        _dtemp.LocIDX = locate;

        _dtEmployee.BoxTel_list[0] = _dtemp;

        _dtEmployee = callServicePostEmp(urlSelectMainTel, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.BoxTel_list, "m0_tel", "m0telidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกเบอร์ติดต่อองค์กร....", "0"));

    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dtEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dtEmployee.organization_list[0] = _orgList;

        _dtEmployee = callServicePostEmp(_urlGetOrganizationList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร...", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dtEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dtEmployee.department_list[0] = _deptList;

        _dtEmployee = callServicePostEmp(_urlGetDepartmentList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกฝ่าย...", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dtEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dtEmployee.section_list[0] = _secList;

        _dtEmployee = callServicePostEmp(_urlGetSectionList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกแผนก...", "0"));
    }

    protected void InsertData()
    {
        _dtEmployee = new data_employee();
        _dtEmployee.BoxTel_list = new Tel_List[1];
        Tel_List _dtEmp = new Tel_List();

        _dtEmp.m0telidx = int.Parse(ddlmaintel.SelectedValue);
        _dtEmp.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
        _dtEmp.RSecIDX = int.Parse(ddlsecidx.SelectedValue);
        _dtEmp.RDeptIDX = int.Parse(ddldeptidx.SelectedValue);
        _dtEmp.u0_tel = txttel.Text;
        _dtEmp.u0status = int.Parse(ddStatusadd.SelectedValue);
        _dtEmp.CEmpIDX = int.Parse(Session["emp_idx"].ToString());
        _dtEmployee.BoxTel_list[0] = _dtEmp;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));

        _dtEmployee = callServicePostEmp(urlInsertSubTel, _dtEmployee);
    }

    protected void Update_Master_List()
    {
        _dtEmployee = new data_employee();
        _dtEmployee.BoxTel_list = new Tel_List[1];
        Tel_List _dtEmp = new Tel_List();

        _dtEmp.LocIDX = int.Parse(ViewState["ddllocname_edit"].ToString());
        _dtEmp.m0telidx = int.Parse(ViewState["ddltelidx_edit"].ToString());
        _dtEmp.u0_tel = ViewState["txtu0idx"].ToString();
        _dtEmp.m0telidx = int.Parse(ViewState["m0telidx"].ToString());
        _dtEmp.u0telidx = int.Parse(ViewState["u0telidx"].ToString());
        _dtEmp.RDeptIDX = int.Parse(ViewState["ddlrdeptidx_edit"].ToString());
        _dtEmp.RSecIDX = int.Parse(ViewState["ddlrsecidx_edit"].ToString());
        _dtEmp.OrgIDX = int.Parse(ViewState["ddlorgidx_edit"].ToString());
        _dtEmp.u0status = int.Parse(ViewState["StatusUpdate_Update"].ToString());
        _dtEmp.CEmpIDX = int.Parse(Session["emp_idx"].ToString());
        _dtEmployee.BoxTel_list[0] = _dtEmp;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));

        _dtEmployee = callServicePostEmp(urlUpdateSubTel, _dtEmployee);


    }

    protected void Delete_Master_List()
    {
        _dtEmployee = new data_employee();
        _dtEmployee.BoxTel_list = new Tel_List[1];
        Tel_List _dtEmp = new Tel_List();

        _dtEmp.u0telidx = int.Parse(ViewState["u0telidx"].ToString());
        _dtEmp.CEmpIDX = int.Parse(Session["emp_idx"].ToString());
        _dtEmployee.BoxTel_list[0] = _dtEmp;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));

        _dtEmployee = callServicePostEmp(urlDeleteSubTel, _dtEmployee);
    }

    protected void SelectMasterList()
    {
        _dtEmployee = new data_employee();
        _dtEmployee.BoxTel_list = new Tel_List[1];
        Tel_List _dtEmp = new Tel_List();

        _dtEmployee.BoxTel_list[0] = _dtEmp;
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));

        _dtEmployee = callServicePostEmp(urlSelectSubTel, _dtEmployee);
        setGridData(GvMaster, _dtEmployee.BoxTel_list);
    }


    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void SetDefaultAdd()
    {
        txttel.Text = String.Empty;
        SelectLocate(ddllocname);
        getOrganizationList(ddlorgidx);
        ddldeptidx.SelectedValue = "0";
        ddlsecidx.SelectedValue = "0";
        ddlmaintel.SelectedValue = "0";
        ddStatusadd.SelectedValue = "1";

    }

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        switch (ddName.ID)
        {
            case "ddllocname":
                SelectTel(ddlmaintel, int.Parse(ddllocname.SelectedValue));
                break;

            case "ddlorgidx":
                getDepartmentList(ddldeptidx, int.Parse(ddlorgidx.SelectedValue));
                break;

            case "ddldeptidx":
                getSectionList(ddlsecidx, int.Parse(ddlorgidx.SelectedValue), int.Parse(ddldeptidx.SelectedValue));
                break;


        }
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {

                        var ddllocname_edit = (DropDownList)e.Row.FindControl("ddllocname_edit");
                        var lblLocIDX = (TextBox)e.Row.FindControl("lblLocIDX");
                        var ddlorgidx_edit = (DropDownList)e.Row.FindControl("ddlorgidx_edit");
                        var txtorgidx = (TextBox)e.Row.FindControl("txtorgidx");
                        var ddlrdeptidx_edit = (DropDownList)e.Row.FindControl("ddlrdeptidx_edit");
                        var txtrdeptidx = (TextBox)e.Row.FindControl("txtrdeptidx");
                        var ddltelidx_edit = (DropDownList)e.Row.FindControl("ddltelidx_edit");
                        var txtm0telidx_edit = (TextBox)e.Row.FindControl("txtm0telidx_edit");
                        var ddlrsecidx_edit = (DropDownList)e.Row.FindControl("ddlrsecidx_edit");
                        var txtrsectidx = (TextBox)e.Row.FindControl("txtrsectidx");

                        SelectLocate(ddllocname_edit);
                        ddllocname_edit.SelectedValue = lblLocIDX.Text;
                        getOrganizationList(ddlorgidx_edit);
                        ddlorgidx_edit.SelectedValue = txtorgidx.Text;
                        getDepartmentList(ddlrdeptidx_edit, int.Parse(ddlorgidx_edit.SelectedValue));
                        ddlrdeptidx_edit.SelectedValue = txtrdeptidx.Text;
                        SelectTel(ddltelidx_edit, int.Parse(ddllocname_edit.SelectedValue));
                        ddltelidx_edit.SelectedValue = txtm0telidx_edit.Text;
                        getSectionList(ddlrsecidx_edit, int.Parse(ddlorgidx_edit.SelectedValue), int.Parse(ddlrdeptidx_edit.SelectedValue));
                        ddlrsecidx_edit.SelectedValue = txtrsectidx.Text;

                    }

                }


                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();
                btnshow.Visible = false;
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                btnshow.Visible = true;
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int u0telidx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var ddllocname_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddllocname_edit");
                var ddlorgidx_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlorgidx_edit");
                var ddlrdeptidx_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlrdeptidx_edit");
                var ddlrsecidx_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlrsecidx_edit");
                var ddltelidx_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddltelidx_edit");
                var txtu0idx = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtu0idx");

                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster.EditIndex = -1;

                ViewState["u0telidx"] = u0telidx;
                ViewState["ddllocname_edit"] = ddllocname_edit.SelectedValue;
                ViewState["ddlorgidx_edit"] = ddlorgidx_edit.SelectedValue;
                ViewState["ddlrdeptidx_edit"] = ddlrdeptidx_edit.SelectedValue;
                ViewState["ddlrsecidx_edit"] = ddlrsecidx_edit.SelectedValue;
                ViewState["ddltelidx_edit"] = ddltelidx_edit.SelectedValue;
                ViewState["txtu0idx"] = txtu0idx.Text;
                ViewState["StatusUpdate_Update"] = StatusUpdate.SelectedValue;
                ViewState["m0telidx"] = ddltelidx_edit.SelectedValue;
                

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion

    #region Call Services
    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }

    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":
                btnshow.Visible = false;
                Panel_Add.Visible = true;
                SetDefaultAdd();
                break;

            case "btnCancel":
                btnshow.Visible = true;
                Panel_Add.Visible = false;
                break;

            case "btnAdd":
                InsertData();
                Panel_Add.Visible = false;
                btnshow.Visible = true;
                SelectMasterList();
                break;
            case "CmdDel":
                int u0telidx = int.Parse(cmdArg);
                ViewState["u0telidx"] = u0telidx;
                Delete_Master_List();
                SelectMasterList();

                break;
        }



    }
    #endregion
}