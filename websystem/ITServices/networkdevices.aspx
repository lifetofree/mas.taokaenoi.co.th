﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="networkdevices.aspx.cs" Inherits="websystem_ITServices_networkdevices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        })
    </script>

    <script>
        $(function () {

            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepickerexpire').datetimepicker({
                    format: 'DD/MM/YYYY',
                    minDate: new Date()

                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        })
    </script>

    <asp:Literal ID="litDebug" runat="server"></asp:Literal>


    <span style="color: red; font-size: 16px; font-weight: bold; margin-left: 20px;">
        <asp:Literal ID="litShowDetail" runat="server"></asp:Literal></span>

    <!--*** START Menu ***-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav" runat="server">
                    <li id="liToIndexList" runat="server">
                        <asp:LinkButton ID="btnToIndexList" runat="server"
                            CommandName="btnToIndexList"
                            OnCommand="btnCommand" Text="ข้อมูลทั่วไป" />
                    </li>

                    <li id="li1" runat="server">
                        <asp:LinkButton ID="btnToDeviceNetwork" runat="server"
                            CommandName="btnToDeviceNetwork"
                            OnCommand="btnCommand" Text="เพิ่มทะเบียนอุปกรณ์ Network" />
                    </li>

                    <li id="li2" runat="server">
                        <asp:LinkButton ID="btnToDeviceNetworkMove" runat="server"
                            CommandName="btnToDeviceNetworkMove"
                            OnCommand="btnCommand" Text="ย้ายอุปกรณ์ Network" />
                    </li>

                    <li id="li3" runat="server">
                        <asp:LinkButton ID="btnToDeviceNetworkCut" runat="server"
                            CommandName="btnToDeviceNetworkCut"
                            OnCommand="btnCommand" Text="ตัดชำรุดอุปกรณ์ Network" />
                    </li>

                    <li id="li4" runat="server">
                        <asp:LinkButton ID="btnToReport" runat="server"
                            CommandName="btnToReport"
                            OnCommand="btnCommand" Text="รายงาน" />
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right" runat="server">

                    <li id="_divMenuLiToDocument" runat="server">
                        <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                            NavigateUrl="https://drive.google.com/open?id=18KnnMozSL3E_wV3Rn0q3iCcXD92I9N96fIBhzTTQ-Us" Target="_blank" Text="คู่มือการใช้งาน" />
                    </li>
                </ul>

            </div>
        </nav>
    </div>
    <!--*** END Menu ***-->

    <%----------- MultiView Start ---------------%>
    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <%----------- ViewIndex Network Deviecs Start ---------------%>
        <asp:View ID="ViewIndex" runat="server">

            <div class="form-group">
                <div class="col-md-12">
                    <asp:LinkButton ID="btnSearch" CssClass="btn btn-info" runat="server" data-original-title="Search" data-toggle="tooltip" CommandName="btnSearch" OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>
                </div>
            </div>


            <!--*** START Back To Index ***-->
            <div class="form-group">
                <div class="col-md-12">
                    <div id="fvBacktoIndex" class="row" runat="server" visible="false">
                        <%--<div class="col-md-12">--%>
                        <asp:LinkButton ID="btnSearchBack" CssClass="btn btn-info" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnSearchBack"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back</asp:LinkButton>
                        <%--</div>--%>
                    </div>
                </div>

            </div>
            <!--*** END Back To Index ***-->

            <!--*** START Search Index ***-->
            <div class="form-group">
                <div class="col-md-12">
                    <div id="showsearch" runat="server" visible="false">
                        <br />
                        <div class="panel panel-info">
                            <div class="panel-heading f-bold">ค้นหารายการอุปกรณ์ Network</div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- สถานที่,อาคาร --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbplacesearch" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ติดตั้ง : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlplacesearch" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                        </div>

                                        <asp:Label ID="lbroomsearch" CssClass="col-sm-2 control-label" runat="server" Text="อาคาร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlroomsearch" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="กรุณาเลือกอาคาร ...." Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                    <%-------------- สถานที่,อาคาร --------------%>

                                    <%-------------- ประเภท,ชนิด --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbtypeidxsearch" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypeidxsearch" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                        </div>

                                        <asp:Label ID="lbcategoryidxsearch" CssClass="col-sm-2 control-label" runat="server" Text="ชนิดอุปกรณ์ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlcategoryidxsearch" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="กรุณาเลือกชนิดอุปกรณ์ ...." Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                    <%-------------- ประเภท,ชนิด --------------%>

                                    <%-------------- ยี่ห้อ,รุ่น --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbbrandsearch" CssClass="col-sm-2 control-label" runat="server" Text="ยี่ห้อ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtbrandsearch" CssClass="form-control" placeholder="ยี่ห้อ..." runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="lbgenerationsearch" CssClass="col-sm-2 control-label" runat="server" Text="รุ่น : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtgenerationsearch" CssClass="form-control" placeholder="รุ่น..." runat="server"></asp:TextBox>
                                        </div>

                                    </div>
                                    <%-------------- ยี่ห้อ,รุ่น --------------%>

                                    <%-------------- ชื่อผู้รับผิดชอบ,บริษัทที่ต่อประกัน --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbnamesearch" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อผู้รับผิดชอบ : " />
                                        <div class="col-sm-3">
                                            <%-- <asp:TextBox ID="txtnamecreate" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                            <asp:DropDownList ID="ddlnamesearch" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>

                                        <asp:Label ID="lbcompanyidx" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทที่ต่อประกัน : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlcompanyidxsearch" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>

                                    </div>
                                    <%-------------- ชื่อผู้รับผิดชอบ,บริษัทที่ต่อประกัน  --------------%>

                                    <%-------------- วันที่เริ่ม / วันที่หมดประกัน --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbsearchbetween" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่ : " />
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">เลือกเงื่อนไข</asp:ListItem>
                                                <asp:ListItem Value="1">มากกว่า  ></asp:ListItem>
                                                <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                                <asp:ListItem Value="3">ระหว่าง  <></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                        <div class="col-sm-3">
                                            <div class='input-group date from-date-datepicker'>
                                                <asp:TextBox ID="txtdatepurchasesearch" CssClass="form-control" runat="server" placeholder="วันที่เริ่มประกัน..."></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                                <%--  <asp:RequiredFieldValidator ID="Requiredtxtdatepurchaseadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                    ControlToValidate="txtdatepurchaseadd" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกวันที่ซื้อ"
                                                    ValidationExpression="กรุณากรอกวันที่ซื้อ" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtdatepurchaseadd" Width="160" />--%>
                                            </div>
                                        </div>

                                        <div class="col-sm-3">
                                            <div class='input-group date from-date-datepicker'>
                                                <asp:TextBox ID="txtdateexpirsearch" CssClass="form-control" runat="server" Enabled="False" placeholder="วันที่หมดประกัน..."></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                                <%--  <asp:RequiredFieldValidator ID="Requiredtxtdatepurchaseadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                    ControlToValidate="txtdatepurchaseadd" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกวันที่ซื้อ"
                                                    ValidationExpression="กรุณากรอกวันที่ซื้อ" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtdatepurchaseadd" Width="160" />--%>
                                            </div>
                                        </div>


                                    </div>

                                    <%-------------- วันที่เริ่ม / วันที่หมดประกัน --------------%>

                                    <%-------------- ip,serial --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="lbipaddresssearch" CssClass="col-sm-2 control-label" runat="server" Text="IP Address : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtipaddresssearch" CssClass="form-control" runat="server" placeholder="IP Address..."></asp:TextBox>
                                        </div>

                                        <asp:Label ID="lbserialsearch" CssClass="col-sm-2 control-label" runat="server" Text="Serial Number : " />
                                        <div class="col-sm-3">
                                            <small>
                                                <asp:TextBox ID="txtserialsearch" CssClass="form-control" placeholder="Serial Number..." runat="server"></asp:TextBox></small>
                                        </div>

                                    </div>
                                    <%-------------- ip,serial --------------%>

                                    <%-------------- register --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="lbregistersearch" CssClass="col-sm-2 control-label" runat="server" Text="เลขทะเบียน : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtregistersearch" CssClass="form-control" runat="server" placeholder="เลขทะเบียนอุปกรณ์..."></asp:TextBox>
                                        </div>

                                        <%-- <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" Text="Serial Number : " />
                                <div class="col-sm-3">
                                    <small>
                                        <asp:TextBox ID="TextBox2" CssClass="form-control" placeholder="Serial Number..." runat="server"></asp:TextBox></small>
                                </div>--%>
                                    </div>
                                    <%-------------- register --------------%>

                                    <%-------------- btnsearch --------------%>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <%--<div class="col-sm-12"></div>--%>
                                            <div class="pull-left">
                                                <asp:LinkButton ID="btnSearchIndex" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchIndex" OnCommand="btnCommand" CommandName="btnSearchIndex" data-toggle="tooltip" title="Search" OnClientClick="return confirm('คุณต้องการค้นหารายการนี้ใช่หรือไม่ ?')"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>


                                                <asp:LinkButton ID="btnCancelIndex" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="btnCancelIndex" data-toggle="tooltip" title="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>

                                        </div>
                                    </div>
                                    <%-------------- btnsearch --------------%>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!--*** END Search Index ***-->

            <!--*** START GRIDVIEW Index ***-->
            <%-- col-md-12 m-t-10--%>
            <div class="col-md-12">

                <div id="gridviewindex" runat="server">
                    <br />
                    <asp:GridView ID="GvMaster"
                        runat="server"
                        AutoGenerateColumns="false"
                        DataKeyNames="u0_devicenetwork_idx"
                        CssClass="table table-striped table-bordered table-responsive"
                        HeaderStyle-CssClass="info"
                        OnRowEditing="Master_RowEditing"
                        OnRowUpdating="Master_RowUpdating"
                        OnRowCancelingEdit="Master_RowCancelingEdit"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        OnRowDataBound="Master_RowDataBound">

                        <EmptyDataTemplate>
                            <div style="text-align: center">No result</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" Visible="true">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbu0_idx" runat="server" Visible="false" Text='<%# Eval("u0_devicenetwork_idx") %>' />
                                        <asp:Label ID="lbunidx" runat="server" Visible="false" Text='<%# Eval("m0_node_idx") %>' />
                                        <%# (Container.DataItemIndex +1) %>
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="u0_devicenetwork_idx" runat="server" CssClass="form-control"
                                        Visible="False" Text='<%# Eval("u0_devicenetwork_idx")%>' />

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">ชื่อประเภทอุปกรณ์</label>
                                                <%-- <asp:Label ID="lbddlTypeName" runat="server" Text='<%# Bind("type_idx") %>' Visible="false" />--%>
                                                <asp:DropDownList ID="ddlTypeNameUpdate" AutoPostBack="true" runat="server"
                                                    CssClass="form-control">
                                                </asp:DropDownList>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">ชื่อชนิดอุปกรณ์</label>
                                                <asp:UpdatePanel ID="panelTypeUpdate" runat="server">
                                                    <ContentTemplate>

                                                        <%--<asp:TextBox ID="txtCategoryNameUpdate" runat="server" CssClass="form-control"
                                               Text='<%# Eval("category_name")%>' />--%>
                                                        <asp:RequiredFieldValidator ID="requiredCategoryNameUpdate"
                                                            ValidationGroup="saveTypeNameUpdate" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="txtCategoryNameUpdate"
                                                            Font-Size="1em" ForeColor="Red"
                                                            ErrorMessage="กรุณากรอกชื่อชนิดอุปกรณ์" />
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <small>
                                                <label class="pull-left">สถานะ</label>
                                                <%--<asp:DropDownList ID="ddlCategoryStatusUpdate" AutoPostBack="false" runat="server"
                                         CssClass="form-control" SelectedValue='<%# Eval("category_status") %>'>
                                         <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                         <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                      </asp:DropDownList>--%>
                                            </small>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="pull-left">
                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                                ValidationGroup="saveTypeNameUpdate" CommandName="Update"
                                                OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                         บันทึก
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="CmdCancel" CssClass="btn btn-danger" runat="server"
                                                CommandName="Cancel">ยกเลิก</asp:LinkButton>
                                        </div>
                                    </div>
                                </EditItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เลขทะเบียนอุปกรณ์" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="RegisterName" runat="server" Text='<%# Eval("register_number") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="DateCreate" runat="server" Text='<%# Eval("date_create") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ผู้สร้างรายการ" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="NameCreate" runat="server" Text='<%# Eval("name_create") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="StatusDesc" runat="server" Text='<%#Eval("status_desc") %>' />
                                        <asp:Label ID="lbStatus_deviecs" runat="server" Visible="false" Text='<%#Eval("status_deviecs") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ดำเนินการ" ItemStyle-HorizontalAlign="center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnViewindex" CssClass="btn btn-primary" Font-Size="11px" runat="server" CommandName="btnViewindex" OnCommand="btnCommand"
                                        data-toggle="tooltip" title="ดูข้อมูล" CommandArgument='<%# Eval("u0_devicenetwork_idx") + ";" + Eval("register_number") + ";" + Eval("m0_node_idx") %>'><i class="fa fa-file"></i></asp:LinkButton>
                                    <asp:LinkButton ID="btnEditindex" CssClass="btn btn-warning" Font-Size="11px" runat="server" CommandName="btnEditindex" OnCommand="btnCommand" Visible="false"
                                        data-toggle="tooltip" title="จัดการข้อมูล" CommandArgument='<%# Eval("u0_devicenetwork_idx") + ";" + Eval("register_number") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("doc_decision") + ";" + Eval("cemp_idx_create") + ";" + Eval("status_deviecs")+ ";" + Eval("status_move")+ ";" + Eval("status_ma") %>'><i class="fa fa-edit"></i></asp:LinkButton>
                                    <asp:LinkButton ID="btnRepairindex" CssClass="btn btn-default" Font-Size="11px" runat="server" CommandName="btnRepairindex" OnCommand="btnCommand" Visible="false"
                                        data-toggle="tooltip" title="เก็บประวัติซ่อม" CommandArgument='<%# Eval("u0_devicenetwork_idx") + ";" + Eval("register_number") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("doc_decision")+ ";" + Eval("status_deviecs") %>'><i class="fa fa-wrench"></i></asp:LinkButton>

                                    <asp:LinkButton ID="btnMAindex" CssClass="btn btn-danger" Font-Size="11px" runat="server" CommandName="btnMAindex" OnCommand="btnCommand" Visible="false"
                                        data-toggle="tooltip" title="เก็บประวัติ MA" CommandArgument='<%# Eval("u0_devicenetwork_idx") + ";" + Eval("register_number") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("doc_decision") + ";" + Eval("company_name")+ ";" + Eval("date_expire")+ ";" + Eval("status_deviecs") %>'><i class="fa fa-warning"></i></asp:LinkButton>

                                    <asp:LinkButton ID="btnQrCodeindex" CssClass="btn btn-info" Font-Size="11px" runat="server" CommandName="btnQrCodeindex" OnCommand="btnCommand" Visible="false"
                                        data-toggle="tooltip" title="Gen QrCode" CommandArgument='<%# Eval("u0_devicenetwork_idx") + ";" + Eval("register_number") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("doc_decision") + ";" + Eval("company_name")+ ";" + Eval("date_expire")+ ";" + Eval("status_deviecs") %>'><i class="fa fa-table"></i></asp:LinkButton>

                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <!--*** END GRIDVIEW Index ***-->

            <!--*** START Show Data in Viewindex ***-->
            <div class="row" id="div_showdata" runat="server" visible="false">

                <div class="col-md-12" id="panelview" runat="server" visible="false">
                    <div class="panel panel-info">
                        <div id="divshownamedetail" runat="server" visible="false" class="panel-heading f-bold">รายการทะเบียนอุปกรณ์ Network</div>
                    </div>
                </div>

                <div class="col-md-12" id="panelmanage" runat="server" visible="false">
                    <div class="panel panel-info">
                        <div id="divshownamedetailmanage" runat="server" visible="false" class="panel-heading f-bold">จัดการรายการทะเบียนอุปกรณ์ Network</div>
                    </div>
                </div>

                <div class="col-md-12" id="panelrepair" runat="server" visible="false">
                    <div class="panel panel-info">
                        <div id="divshownamedetailrepair" runat="server" visible="false" class="panel-heading f-bold">กรอกประวัติการซ่อมทะเบียนอุปกรณ์ Network</div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12" id="panelma" runat="server" visible="false">
                        <div class="panel panel-info">
                            <div id="divshownamedetailma" runat="server" visible="false" class="panel-heading f-bold">กรอกประวัติการ MA ทะเบียนอุปกรณ์ Network</div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <%--<div class="col-md-12">--%>
                    <div id="BackToIndexforQrCode" class="col-md-12" runat="server" visible="false">
                        <%--<div class="col-md-12">--%>
                        <div class="form-group">
                            <asp:LinkButton ID="btnBackToIndexforQrCode" CssClass="btn btn-info" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnBackToIndexforQrCode"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back</asp:LinkButton>
                        </div>
                    </div>
                    <%--  </div>--%>
                </div>



                <div class="form-group">
                    <br />
                    <div class="col-md-12" id="panelqrcode" runat="server" visible="false">
                        <div class="panel panel-info">
                            <div id="divshownamedetailqrcode" runat="server" visible="false" class="panel-heading f-bold">Gen QRCode</div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <asp:FormView ID="FvViewDetail" runat="server" OnDataBound="FvDetail_DataBound" class="col-sm-12 font_text">

                        <EditItemTemplate>

                            <asp:HiddenField ID="hf_m0_node_idx" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                            <asp:HiddenField ID="hf_m0_actor_idx" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                            <asp:HiddenField ID="hf_doc_decision" runat="server" Value='<%# Eval("doc_decision") %>' />


                            <%--START Show Detail Edit--%>
                            <%--<div class="form-group">--%>
                            <div class="col-sm-12">
                                <div class="panel panel-info">
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <%-------------- เลขทะเบียนอุปกรณ์,บริษัทที่ต่อประกัน --------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbregister_numberview" CssClass="col-sm-2 control-label" runat="server" Text="เลขทะเบียนอุปกรณ์ : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtregister_numberview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("register_number") %>'></asp:TextBox>
                                                </div>

                                                <asp:Label ID="lbcompany_nameview" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทที่ต่อประกัน : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtcompany_nameview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("company_name") %>'></asp:TextBox>
                                                </div>

                                            </div>
                                            <%-------------- เลขทะเบียนอุปกรณ์,บริษัทที่ต่อประกัน --------------%>

                                            <%-------------- ชื่อผู้ทำรายการ,ตำแหน่ง --------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbnamecreate" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ-นามสกุลผู้สร้าง : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtnamecreate" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("name_create") %>'></asp:TextBox>
                                                </div>

                                                <asp:Label ID="lbdeptcreate" CssClass="col-sm-2 control-label" runat="server" Text="ตำแหน่ง : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtdeptcreate" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("pos_name_th") %>'></asp:TextBox>
                                                </div>

                                            </div>
                                            <%-------------- ชื่อผู้ทำรายการ,ตำแหน่ง --------------%>

                                            <%-------------- ยี่ห้อ ,รุ่น  --------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbbrand_nameview" CssClass="col-sm-2 control-label" runat="server" Text="ยี่ห้อ : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtbrand_nameview" Enabled="true" CssClass="form-control" runat="server" Text='<%# Eval("brand_name") %>'></asp:TextBox>
                                                </div>

                                                <asp:Label ID="lbgeneration_nameview" CssClass="col-sm-2 control-label" runat="server" Text="รุ่น : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtgeneration_nameview" Enabled="true" CssClass="form-control" runat="server" Text='<%# Eval("generation_name") %>'></asp:TextBox>
                                                </div>

                                            </div>
                                            <%-------------- ยี่ห้อ ,รุ่น --------------%>

                                            <%-------------- ประเภทอุปกรณ์ ,ชนิดอุปกรณ์  --------------%>
                                            <%--  <asp:UpdatePanel ID="update_test" runat="server" UpdateMode="Always">
                                        <ContentTemplate>--%>
                                            <div class="form-group">
                                                <asp:Label ID="lbtype_nameview" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                                <asp:Label ID="lbtype_idxedit" CssClass="col-sm-2 control-label" runat="server" Text='<%# Eval("type_idx") %>' Visible="false" />

                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddltype_nameedit" AutoPostBack="true" Enabled="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>

                                                <asp:Label ID="lbcategory_nameedit" CssClass="col-sm-2 control-label" runat="server" Text="ชนิดอุปกรณ์ : " />

                                                <asp:Label ID="lbcategory_idxedit" CssClass="col-sm-2 control-label" runat="server" Text='<%# Eval("category_idx") %>' Visible="false" />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlcategory_nameedit" Enabled="false" CssClass="form-control" runat="server">
                                                        <%--<asp:ListItem Text="กรุณาเลือกชนิดอุปกรณ์ ...." Value="0"></asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                </div>

                                            </div>
                                            <%--   </ContentTemplate>

                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddltype_nameedit" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>--%>
                                            <%-------------- ประเภทอุปกรณ์ ,ชนิดอุปกรณ์ --------------%>

                                            <%-------------- IP Address ,Serial Number  --------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbip_addressview" CssClass="col-sm-2 control-label" runat="server" Text="IP Address : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtip_addressview" Enabled="true" CssClass="form-control" runat="server" Text='<%# Eval("ip_address") %>'></asp:TextBox>
                                                </div>

                                                <asp:Label ID="lbserial_numberview" CssClass="col-sm-2 control-label" runat="server" Text="Serial Number : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtserial_numberview" Enabled="true" CssClass="form-control" runat="server" Text='<%# Eval("serial_number") %>'></asp:TextBox>
                                                </div>

                                            </div>
                                            <%-------------- IP Address ,Serial Number --------------%>

                                            <%-------------- วันที่ซื้อ ,วันที่หมดประกัน  --------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbdate_purchaseview" CssClass="col-sm-2 control-label" runat="server" Text="วันที่ซื้อ : " />
                                                <div class="col-sm-3">
                                                    <div class='input-group date from-date-datepicker'>
                                                        <asp:TextBox ID="txtdate_purchaseview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("date_purchase") %>'></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>
                                                </div>

                                                <asp:Label ID="lbdate_expireview" CssClass="col-sm-2 control-label" runat="server" Text="วันที่หมดประกัน : " />
                                                <div class="col-sm-3">
                                                    <div class='input-group date from-date-datepicker'>
                                                        <asp:TextBox ID="txtdate_expireview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("date_expire") %>'></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                                    </div>
                                                </div>

                                            </div>
                                            <%-------------- วันที่ซื้อ ,วันที่หมดประกัน --------------%>

                                            <%-------------- สถานที่ติดตั้ง ,อาคาร  --------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbplace_nameview" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ติดตั้ง : " />
                                                <asp:Label ID="lbplace_idxedit" CssClass="col-sm-2 control-label" runat="server" Text='<%# Eval("place_idx") %>' Visible="false" />

                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddltxtplace_nameedit" AutoPostBack="true" Enabled="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>

                                                <asp:Label ID="lbroom_nameview" CssClass="col-sm-2 control-label" runat="server" Text="อาคาร : " />

                                                <asp:Label ID="lbroom_idxedit" CssClass="col-sm-2 control-label" runat="server" Text='<%# Eval("room_idx") %>' Visible="false" />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddltxtroom_nameedit" Enabled="false" CssClass="form-control" runat="server">
                                                        <%--<asp:ListItem Text="กรุณาเลือกอาคาร ...." Value="0"></asp:ListItem>--%>
                                                    </asp:DropDownList>
                                                </div>

                                            </div>
                                            <%-------------- สถานที่ติดตั้ง ,อาคาร --------------%>

                                            <%-------------- User Name ,Password  --------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbuser_nameview" CssClass="col-sm-2 control-label" runat="server" Text="User Name : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtuser_nameview" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("user_name") %>'></asp:TextBox>
                                                </div>

                                                <asp:Label ID="lbtxtpass_wordview" CssClass="col-sm-2 control-label" runat="server" Text="Password : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtpass_wordview" Enabled="false" CssClass="form-control" runat="server" Text='<%#getPassEdit((String) Eval("pass_word")) %>'></asp:TextBox>
                                                </div>

                                            </div>
                                            <%-------------- User Name ,Password --------------%>

                                            <%-------------- Asset Code ,Password  --------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbasset_codeview" CssClass="col-sm-2 control-label" runat="server" Text="Asset Code : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtasset_codeview" Enabled="true" CssClass="form-control" runat="server" Text='<%# Eval("asset_code") %>'></asp:TextBox>
                                                </div>

                                                <asp:Label ID="lbstatus_deviecsview" CssClass="col-sm-2 control-label" runat="server" Text="Status : " />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlstatus_deviecsview" Enabled="false" CssClass="form-control" runat="server" SelectedValue='<%# Eval("status_deviecs") %>'>

                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>

                                            </div>
                                            <%-------------- Asset Code ,Password --------------%>

                                            <%-------------- รายละเอียดอุปกรณ์  --------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbdetail_devicesview" CssClass="col-sm-2 control-label" runat="server" Text="รายละเอียดอุปกรณ์ : " />
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtdetail_devicesview" Enabled="false" TextMode="multiline" Rows="4" CssClass="form-control" runat="server" Text='<%# Eval("detail_devices") %>'></asp:TextBox>
                                                </div>

                                            </div>
                                            <%-------------- รายละเอียดอุปกรณ์ --------------%>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <%--</div>--%>
                            <%--END Show Detail Edit--%>


                            <%--START gvFile--%>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <%--<label class="col-md-10 col-lg-offset-1 ">--%>
                                        <%--<asp:Label ID="FileUser" runat="server" Visible="false" Text='<%# Eval("I_fileuser")%>' />--%>
                                        <asp:GridView ID="gvFile" Visible="true" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover " OnRowDataBound="Master_RowDataBound">

                                            <Columns>
                                                <asp:TemplateField HeaderText="  ไฟล์เอกสารเพิ่มเติม ">
                                                    <ItemTemplate>
                                                        <div class="col-lg-10">
                                                            <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                        </div>
                                                        <div class="col-lg-2">
                                                            <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                            <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                        </div>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                        <%--<h4></h4>--%>
                                    </label>
                                    </div>
                                </div>
                            </div>
                            <%--END gvFile--%>
                        </EditItemTemplate>

                        <ItemTemplate>

                            <asp:HiddenField ID="hf_m0_node_idx" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                            <asp:HiddenField ID="hf_m0_actor_idx" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                            <asp:HiddenField ID="hf_doc_decision" runat="server" Value='<%# Eval("doc_decision") %>' />


                            <div class="col-md-12">
                                <div class="form-horizontal" role="form">
                                    <div class="panel panel-default">

                                        <div class="panel-heading">
                                            <br />
                                            <div class="form-group">
                                                <div class="col-sm-1"></div>


                                                <asp:Label ID="lbname_createview" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ชื่อ-นามสกุลผู้สร้าง :" />
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lbcemp_idxview" Font-Size="Small" runat="server" Text='<%# Bind("cemp_idx") %>' Visible="False" />
                                                    <asp:Label ID="txtname_createview" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("name_create") %>' />
                                                </div>

                                                <asp:Label ID="lbpos_name_thview" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ตำแหน่ง :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txtpos_name_thview" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("pos_name_th") %>' />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-1"></div>
                                                <asp:Label ID="lbregister_numberview" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="เลขทะเบียนอุปกรณ์ :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txtregister_numberview" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("register_number") %>' />
                                                </div>

                                                <asp:Label ID="lbcompany_nameview" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="บริษัทที่ต่อประกัน :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txtcompany_nameview" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("company_name") %>' />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-1"></div>
                                                <asp:Label ID="lbbrand_nameview" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ยี่ห้อ :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txtbrand_nameview" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("brand_name") %>' />
                                                </div>

                                                <asp:Label ID="lbgeneration_nameview" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="รุ่น :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txtgeneration_nameview" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("generation_name") %>' />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-1"></div>
                                                <asp:Label ID="lbtype_nameview" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ประเภทอุปกรณ์ :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txttype_nameview" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("type_name") %>' />
                                                </div>

                                                <asp:Label ID="lbcategory_nameview" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ชนิดอุปกรณ์ :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txtcategory_nameview" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("category_name") %>' />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-1"></div>
                                                <asp:Label ID="lbip_addressview" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="IP Address :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txtip_addressview" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("ip_address") %>' />
                                                </div>

                                                <asp:Label ID="lbserial_numberview" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="Serial Number :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txtserial_numberview" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("serial_number") %>' />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-1"></div>

                                                <asp:Label ID="lbdate_purchaseview" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่ซื้อ :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txtdate_purchaseview" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("date_purchase") %>' />
                                                </div>


                                                <asp:Label ID="lbdate_expireview" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่หมดประกัน :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txtdate_expireview" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("date_expire") %>' />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-1"></div>
                                                <asp:Label ID="lbplace_nameview" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สถานที่ติดตั้ง :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txtplace_nameview" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("place_name") %>' />
                                                </div>

                                                <asp:Label ID="lbroom_nameview" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="อาคาร  :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txtroom_nameview" Font-Size="Small" CssClass="control-label" runat="server" Text='<%#getRoomView((string)Eval("room_name")) %>' />
                                                </div>

                                            </div>

                                            <%--<div class="form-group">
                                            <div class="col-sm-1"></div>
                                            <asp:Label ID="Label1" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สถานที่ติดตั้งใหม่ :" />
                                            <div class="col-sm-3">

                                                <asp:Label ID="Label2" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("place_name_new") %>' />
                                            </div>

                                            <asp:Label ID="Label3" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="อาคารใหม่  :" />
                                            <div class="col-sm-3">

                                                <asp:Label ID="Label4" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("room_name_new") %>' />
                                            </div>

                                        </div>--%>

                                            <div class="form-group">
                                                <div class="col-sm-1"></div>
                                                <asp:Label ID="lbuser_nameview" Font-Size="Small" Visible="true" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="User Name :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txtuser_nameview" Visible="true" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("user_name") %>' />
                                                </div>

                                                <asp:Label ID="lbpass_wordview" Font-Size="Small" Visible="true" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="Password  :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txtpass_wordview" Visible="true" Font-Size="Small" CssClass="control-label" runat="server" Text='<%#getPass((String) Eval("pass_word")) %>' />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-1"></div>
                                                <asp:Label ID="lbasset_codeview" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="Asset Code :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txtasset_codeview" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("asset_code") %>' />
                                                </div>

                                                <asp:Label ID="lbstatus_deviecsview" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="Status  :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txtstatus_deviecsview" Font-Size="Small" CssClass="control-label" runat="server" Text='<%#getStatus((int)Eval("status_deviecs")) %>' />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-1"></div>
                                                <asp:Label ID="lbdetail_devicesview" Font-Size="Small" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="รายละเอียดอุปกรณ์ :" />
                                                <div class="col-sm-3">

                                                    <asp:Label ID="txtdetail_devicesview" Font-Size="Small" CssClass="control-label" runat="server" Text='<%# Eval("detail_devices") %>' />
                                                </div>

                                            </div>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <%--START gvFile--%>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <%--<label class="col-md-10 col-lg-offset-1 ">--%>
                                    <%--<asp:Label ID="FileUser" runat="server" Visible="false" Text='<%# Eval("I_fileuser")%>' />--%>
                                    <asp:GridView ID="gvFile" Visible="true" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover " OnRowDataBound="Master_RowDataBound">

                                        <Columns>
                                            <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                                <ItemTemplate>
                                                    <div class="col-lg-10">
                                                        <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                    </div>
                                                    <div class="col-lg-2">
                                                        <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                        <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                    </div>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                    <h4></h4>
                                    </label>
                                </div>
                            </div>
                            <%--END gvFile--%>

                            <%-------------- START Move Show Place/ Room --------------%>
                            <div class="col-md-12">
                                <asp:Panel ID="div_showplacemove" class="row panel panel-default font_text" runat="server" Visible="false">
                                    <div class="panel-heading f-bold">รายละเอียดการย้ายอุปกรณ์ Network</div>
                                    <div class="form-group" id="showdetailmove" runat="server">
                                        <div class="panel-body">
                                            <div class="form-horizontal" role="form">
                                                <%-------------- สถานที่ติดตั้งใหม่, อาคารใหม่ --------------%>
                                                <div class="form-group">
                                                    <asp:Label ID="lbshow_place_idx" CssClass="col-sm-2 control-label" runat="server" Visible="false" Text='<%# Eval("place_new_idx") %>' />

                                                    <asp:Label ID="lbplace_name_newmove" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ติดตั้งใหม่ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtplace_name_newmove" Text='<%# Eval("place_name_new") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>

                                                    <asp:Label ID="lbshow_room_idx" CssClass="col-sm-2 control-label" runat="server" Visible="false" Text='<%# Eval("room_new_idx") %>' />

                                                    <asp:Label ID="lbroom_name_newmove" CssClass="col-sm-2 control-label" runat="server" Text="อาคารใหม่ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtroom_name_newmove" Text='<%# Eval("room_name_new") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>

                                                </div>
                                                <%-------------- สถานที่ติดตั้งใหม่, อาคารใหม่ --------------%>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <br />
                            </div>

                            <%-------------- END Move Show Place/ Room --------------%>

                            <%-------------- START MA วันที่เริ่มประกันใหม่/ วันที่หมดประกันใหม่ --------------%>
                            <div class="col-md-12">
                                <asp:Panel ID="div_showcompanyma" class="row panel panel-default font_text" runat="server" Visible="false">
                                    <div class="panel-heading f-bold">รายละเอียดการ MA</div>
                                    <div class="form-group" id="showdetailma" runat="server">
                                        <div class="panel-body">
                                            <div class="form-horizontal" role="form">

                                                <%-------------- บริษํทประกันใหม่ --------------%>
                                                <div class="form-group">

                                                    <asp:Label ID="lbcompany_idxma" CssClass="col-sm-2 control-label" runat="server" Visible="false" Text='<%# Eval("company_new_idx") %>' />

                                                    <asp:Label ID="Label3" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทประกันใหม่ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtcompany_idxma" Text='<%# Eval("company_name_new") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>

                                                </div>
                                                <%-------------- บริษํทประกันใหม่ --------------%>


                                                <%-------------- วันที่เริ่มประกันใหม่, วันที่หมดประกันใหม่ --------------%>
                                                <div class="form-group">
                                                    <%-- <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Visible="false" Text='<%# Eval("date_start") %>' />--%>

                                                    <asp:Label ID="lbldate_purchase_new" CssClass="col-sm-2 control-label" runat="server" Text="วันที่เริ่มประกันใหม่ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtdate_startma" Text='<%# Eval("date_purchase_new") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>

                                                    <%--<asp:Label ID="Label3" CssClass="col-sm-2 control-label" runat="server" Visible="false" Text='<%# Eval("room_new_idx") %>' />--%>

                                                    <asp:Label ID="lbldate_expire_new" CssClass="col-sm-2 control-label" runat="server" Text="วันที่หมดประกันใหม่ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtdate_endma" Text='<%# Eval("date_expire_new") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>

                                                </div>
                                                <%-------------- วันที่เริ่มประกันใหม่, วันที่หมดประกันใหม่ --------------%>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>
                                <br />
                            </div>

                            <%-------------- END MA วันที่เริ่มประกันใหม่/ วันที่หมดประกันใหม่ --------------%>
                        </ItemTemplate>
                    </asp:FormView>
                </div>

                <%--------------START Fv ซ่อม  --------------%>
                <div class="form-group">
                    <asp:FormView ID="FvViewRepair" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">

                        <InsertItemTemplate>
                            <div class="col-sm-12">
                                <div class="panel panel-info">
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <asp:Label ID="lb_u0_idxrepair" CssClass="col-sm-2 control-label" runat="server" Visible="false" />

                                            <%-------------- เลขทะเบียน/  วันที่ซ่อม  --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="lbregisternumberrepair" CssClass="col-sm-2 control-label" runat="server" Text="เลขทะเบียน Network : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtregisterrepair" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>

                                                    <%-- <asp:RequiredFieldValidator ID="Requiredtxtregisteradd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                ControlToValidate="txtregisteradd" Font-Size="11"
                                                ErrorMessage="กรุณากรอกชื่อ Register Number"
                                                ValidationExpression="กรุณากรอกชื่อ Register Number" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtregisteradd" Width="160" />--%>
                                                </div>

                                                <asp:Label ID="lbdaterepair" CssClass="col-sm-2 control-label" runat="server" Text="วันที่ซ่อมอุปกรณ์ : " />
                                                <div class="col-sm-3">
                                                    <div class='input-group date from-date-datepicker'>
                                                        <asp:TextBox ID="txtdaterepair" CssClass="form-control" runat="server" placeholder="วันที่ซ่อมอุปกรณ์..."></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                                        <asp:RequiredFieldValidator ID="Re_txtdaterepair" ValidationGroup="SaveRepair" runat="server" Display="None"
                                                            ControlToValidate="txtdaterepair" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกวันที่ซ่อมอุปกรณ์"
                                                            ValidationExpression="กรุณาเลือกวันที่ซ่อมอุปกรณ์" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtdaterepair" Width="160" />
                                                    </div>

                                                </div>


                                            </div>
                                            <%-------------- เลขทะเบียน/ วันที่ซ่อม --------------%>

                                            <%-------------- รายละเอียด --------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbdetail" CssClass="col-sm-2 control-label" runat="server" Text="รายละเอียด : " />
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtdetailrepair" TextMode="multiline" Rows="4" CssClass="form-control" placeholder="รายละเอียดการซ่อมอุปกรณ์ ..." runat="server"></asp:TextBox>
                                                </div>

                                            </div>
                                            <%-------------- รายละเอียด --------------%>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
                <%--------------END Fv ซ่อม  --------------%>

                <%--------------START Fv Detail ซ่อม  --------------%>
                <div class="form-group">
                    <asp:FormView ID="FvViewDetailRepair" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">

                        <InsertItemTemplate>
                            <div class="col-sm-12">
                                <div class="panel panel-info">
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <asp:Label ID="lb_u0_idxrepairview" CssClass="col-sm-2 control-label" runat="server" Visible="false" />

                                            <%-------------- เลขทะเบียน/    --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="lbregisternumberrepairview" CssClass="col-sm-2 control-label" runat="server" Text="เลขทะเบียน Network : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtregisterrepairview" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>

                                                </div>

                                            </div>
                                            <%-------------- เลขทะเบียน/  --------------%>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
                <%--------------END Fv ซ่อม  --------------%>

                <%-------------- START Fv MA  --------------%>
                <div class="form-group">
                    <asp:FormView ID="FvViewMA" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">

                        <InsertItemTemplate>
                            <div class="col-sm-12">
                                <div class="panel panel-info">
                                    <div class="panel-body">

                                        <div class="form-horizontal" role="form">
                                            <asp:Label ID="lb_u0_idxma" CssClass="col-sm-2 control-label" runat="server" Visible="false" />

                                            <%-------------- เลขทะเบียน/    --------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbregisternumberma" CssClass="col-sm-2 control-label" runat="server" Text="เลขทะเบียน Network : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtregisterma" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>

                                                </div>

                                                <%--  <asp:Label ID="lbcompanyma" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทประกันเดิม : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtcompany_idxma" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>

                                        </div>--%>
                                            </div>
                                            <%-------------- เลขทะเบียน/ --------------%>

                                            <%-------------- บริษัทประกันเดิม/  --------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbcompanyma" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทประกันเดิม : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtcompany_idxma" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>

                                                </div>

                                                <asp:Label ID="lbdate_expirema" CssClass="col-sm-2 control-label" runat="server" Text="วันที่หมดประกันเดิม : " />
                                                <div class="col-sm-3">
                                                    <div class='input-group date from-date-datepicker'>
                                                        <asp:TextBox ID="txtdate_expirema" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>

                                                    </div>

                                                </div>


                                            </div>
                                            <%--------------  บริษัทประกันเดิม --------------%>


                                            <%-------------- บริษัทประกันใหม่ --------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbcompanyidxma" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทประกันใหม่ : " />
                                                <div class="col-sm-3">
                                                    <%--<select class="selectpicker">--%>
                                                    <asp:DropDownList ID="ddlcompanyidxma" CssClass="form-control" runat="server"></asp:DropDownList>
                                                    <%-- </select>--%>

                                                    <asp:RequiredFieldValidator ID="Re_ddlcompanyidxma"
                                                        ValidationGroup="SaveMA" runat="server" Display="None"
                                                        ControlToValidate="ddlcompanyidxma" Font-Size="11" InitialValue="0"
                                                        ErrorMessage="กรุณาเลือกบริษัทประกันใหม่"
                                                        ValidationExpression="กรุณาเลือกบริษัทประกันใหม่" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                        TargetControlID="Re_ddlcompanyidxma" Width="160" />
                                                </div>


                                            </div>
                                            <%-------------- บริษัทประกันใหม่ --------------%>

                                            <%-------------- วันที่เริ่มประกัน/  วันที่หมดประกัน  --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="lbdatestartma" CssClass="col-sm-2 control-label" runat="server" Text="วันที่เริ่มประกัน : " />
                                                <div class="col-sm-3">
                                                    <div class='input-group date from-date-datepicker'>
                                                        <asp:TextBox ID="txtdatestartma" CssClass="form-control" runat="server" placeholder="วันที่เริ่มประกัน ..."></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>

                                                        <asp:RequiredFieldValidator ID="Re_txtdatestartma" ValidationGroup="SaveMA" runat="server" Display="None"
                                                            ControlToValidate="txtdatestartma" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกวันที่เริ่มประกัน"
                                                            ValidationExpression="กรุณาเลือกวันที่เริ่มประกัน" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12225" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtdatestartma" Width="160" />
                                                    </div>

                                                </div>

                                                <asp:Label ID="lbdateendma" CssClass="col-sm-2 control-label" runat="server" Text="วันที่หมดประกัน : " />
                                                <div class="col-sm-3">
                                                    <div class='input-group date from-date-datepicker'>
                                                        <asp:TextBox ID="txtdateendma" CssClass="form-control" runat="server" placeholder="วันที่หมดประกัน ..."></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>

                                                        <asp:RequiredFieldValidator ID="Re_txtdateendma" ValidationGroup="SaveMA" runat="server" Display="None"
                                                            ControlToValidate="txtdateendma" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกวันที่หมดประกัน"
                                                            ValidationExpression="กรุณาเลือกวันที่หมดประกัน" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5222" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txtdateendma" Width="160" />
                                                    </div>

                                                </div>


                                            </div>
                                            <%-------------- วันที่เริ่มประกัน/ วันที่หมดประกัน --------------%>

                                            <%-------------- รายละเอียด --------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbdetailma" CssClass="col-sm-2 control-label" runat="server" Text="รายละเอียด : " />
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtdetailma" TextMode="multiline" Rows="4" CssClass="form-control" placeholder="รายละเอียด ..." runat="server"></asp:TextBox>
                                                </div>

                                            </div>
                                            <%-------------- รายละเอียด --------------%>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
                <%-------------- END Fv MA  --------------%>

                <%-------------- START Fv MA Detail  --------------%>
                <div class="form-group">
                    <asp:FormView ID="FvViewDetailMA" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="col-sm-12">
                                <div class="panel panel-info">
                                    <div class="panel-body">

                                        <div class="form-horizontal" role="form">
                                            <asp:Label ID="lb_u0_idxmaview" CssClass="col-sm-2 control-label" runat="server" Visible="false" />

                                            <%-------------- เลขทะเบียน/    --------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbregisternumbermaview" CssClass="col-sm-2 control-label" runat="server" Text="เลขทะเบียน Network : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtregistermaview" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>

                                                </div>

                                                <%--  <asp:Label ID="lbcompanyma" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทประกันเดิม : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtcompany_idxma" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>

                                        </div>--%>
                                            </div>
                                            <%-------------- เลขทะเบียน/ --------------%>

                                            <%-------------- บริษัทประกันเดิม/  --------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbcompanymaview" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทประกัน : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtcompany_idxmaview" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>

                                                </div>

                                                <asp:Label ID="lbdate_expiremaview" CssClass="col-sm-2 control-label" runat="server" Text="วันที่หมดประกัน : " />
                                                <div class="col-sm-3">
                                                    <div class='input-group date from-date-datepicker'>
                                                        <asp:TextBox ID="txtdate_expiremaview" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>

                                                    </div>

                                                </div>


                                            </div>
                                            <%--------------  บริษัทประกันเดิม --------------%>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>

                <%-------------- END Fv MA  --------------%>

                <%-------------- START Fv QRCode  --------------%>
                <div class="form-group">
                    <asp:FormView ID="FvViewQRCode" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="col-sm-12">
                                <div class="panel panel-info">
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <asp:Label ID="lb_u0_idxqr" CssClass="col-sm-2 control-label" runat="server" Visible="false" />

                                            <%-------------- เลขทะเบียน/    --------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbregisternumberqr" CssClass="col-sm-2 control-label" runat="server" Text="เลขทะเบียน Network : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtregisterqr" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>

                                                </div>

                                                <%--  <asp:Label ID="lbcompanyma" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทประกันเดิม : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtcompany_idxma" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>

                                        </div>--%>
                                            </div>
                                            <%-------------- เลขทะเบียน/ --------------%>

                                            <%-------------- QRCode/--------------%>
                                            <div class="form-group">
                                                <asp:Label ID="lbnameqr" CssClass="col-sm-2 control-label" runat="server" Text="QRCode : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtCode" CssClass="form-control" runat="server" Enabled="true"></asp:TextBox>
                                                    <br />

                                                    <%--<asp:FileUpload ID="LogoUpload" runat="server" />--%>
                                                    <br />

                                                    <asp:LinkButton ID="btnCreateCode" CssClass="btn btn-success" runat="server" ValidationGroup="SaveQrCode" OnCommand="btnCommand" CommandName="btnCreateCode" data-toggle="tooltip" title="SaveQrCode" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>


                                                    <hr />
                                                    <asp:Image runat="server" ID="qrimage" />
                                                    <%--<br />--%>
                                                    <%--<asp:PlaceHolder runat="server" ID="qrimage"></asp:PlaceHolder>--%>

                                                    <%--<br />
                                                <asp:Image runat="server" ID="QRImage" />--%>
                                                </div>

                                                <%--  <asp:Label ID="lbcompanyma" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทประกันเดิม : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtcompany_idxma" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>

                                        </div>--%>
                                            </div>
                                            <%-------------- QRCode/ --------------%>
                                        </div>

                                    </div>

                                </div>
                            </div>


                        </InsertItemTemplate>

                    </asp:FormView>
                </div>

                <%-------------- END Fv QRCode  --------------%>

                <%-------------- QRCode/--------------%>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div id="div_qrcode" class="panel panel-info" runat="server" visible="false">
                            <div class="panel-body">

                                <div class="form-horizontal" role="form">
                                    <div class="form-group" id="imageqrshow" runat="server" visible="false">


                                        <%-------------- เลขทะเบียน/    --------------%>
                                        <div class="form-group">

                                            <asp:Label ID="lbregisternumberqr" CssClass="col-sm-2 control-label" runat="server" Text="เลขทะเบียน Network : " Visible="false" />
                                            <div class="col-sm-3">

                                                <asp:TextBox ID="txtu0_idx_qr" CssClass="form-control" runat="server" Enabled="false" Visible="false"></asp:TextBox>

                                                <asp:TextBox ID="txtregisterqr" CssClass="form-control" runat="server" Enabled="false" Visible="false"></asp:TextBox>

                                            </div>
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-2">
                                                <asp:Image ID="ImagShowQRCode" runat="server"></asp:Image>

                                            </div>

                                        </div>
                                        <%-------------- เลขทะเบียน/ --------------%>

                                        <%-------------- QRCode/    --------------%>
                                        <div class="form-group">
                                            <asp:Label ID="lbnameqr" CssClass="col-sm-2 control-label" runat="server" Text="QRCode : " Visible="false" />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtCode" CssClass="form-control" runat="server" Enabled="false" Visible="false"></asp:TextBox>
                                                <%-- <br />--%>

                                                <asp:LinkButton ID="btnCreateCode" CssClass="btn btn-success" runat="server" ValidationGroup="SaveQrCode" OnCommand="btnCommand" Visible="false" CommandName="btnCreateCode" data-toggle="tooltip" title="SaveQrCode" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')"> GenQRCode</asp:LinkButton>

                                                <%--<asp:LinkButton ID="btnCancelCode"  CssClass="btn btn-danger" runat="server" ValidationGroup="CancelQrCode" OnCommand="btnCommand" CommandName="btnCancelCode" data-toggle="tooltip" title="Cancel" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')"> Cancel</asp:LinkButton>--%>

                                                <%-- <hr />--%>
                                                <asp:Image runat="server" ID="qrimage" Visible="false" />

                                            </div>
                                        </div>
                                        <%-------------- QRCode/    --------------%>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%-------------- QRCode/ --------------%>

                <%-------------- START Show Edit --------------%>
                <div class="form-group">
                    <div class="col-md-12">
                        <asp:Panel ID="div_showedit" class="row panel panel-default font_text" runat="server" Visible="false">
                            <div class="panel-heading f-bold">รายละเอียดการแก้ไขข้อมูลอุปกรณ์ Network</div>
                            <div class="form-group" id="showdetailedit" runat="server">
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <%-------------- ยี้ห้อ, รุ่น --------------%>
                                        <div class="form-group">
                                            <%--<asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Visible="false" Text='<%# Eval("place_new_idx") %>' />--%>

                                            <asp:Label ID="lbbrand_nameedit" CssClass="col-sm-2 control-label" runat="server" Text="ยี่ห้อ : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtbrand_nameedit" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <%--<asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Visible="false" Text='<%# Eval("room_new_idx") %>' />--%>

                                            <asp:Label ID="lbgeneration_nameedit" CssClass="col-sm-2 control-label" runat="server" Text="รุ่น : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtgeneration_nameedit" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>
                                        <%-------------- ยี้ห้อ, รุ่น --------------%>

                                        <%-------------- ประเภท, ชนิด --------------%>
                                        <div class="form-group">
                                            <asp:Label ID="lbtype_idxedit" CssClass="col-sm-2 control-label" runat="server" Visible="false" />

                                            <asp:Label ID="lbtype_nameedit" CssClass="col-sm-2 control-label" runat="server" Text="ประเภท : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txttype_nameedit" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="lbcategory_idxedit" CssClass="col-sm-2 control-label" runat="server" Visible="false" />

                                            <asp:Label ID="lbcategory_nameedit" CssClass="col-sm-2 control-label" runat="server" Text="ชนิด : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtcategory_nameedit" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>
                                        <%-------------- ประเภท, ชนิด --------------%>

                                        <%-------------- IP Address, Serial Number --------------%>
                                        <div class="form-group">
                                            <%--<asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Visible="false" Text='<%# Eval("place_new_idx") %>' />--%>

                                            <asp:Label ID="lbip_addressedit" CssClass="col-sm-2 control-label" runat="server" Text="IP Address : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtip_addressedit" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <%--<asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Visible="false" Text='<%# Eval("room_new_idx") %>' />--%>

                                            <asp:Label ID="lbserial_numberedit" CssClass="col-sm-2 control-label" runat="server" Text="Serial Number : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtserial_numberedit" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>
                                        <%-------------- IP Address, Serial Number --------------%>
                                        <%-------------- สถานที่ติดตั้ง, อาคาร --------------%>
                                        <div class="form-group">
                                            <asp:Label ID="lbplace_idxedit" CssClass="col-sm-2 control-label" runat="server" Visible="false" />

                                            <asp:Label ID="lbplacenameedit" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ติดตั้ง : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtplace_nameedit" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="lbroom_idxedit" CssClass="col-sm-2 control-label" runat="server" Visible="false" />

                                            <asp:Label ID="lbroom_nameedit" CssClass="col-sm-2 control-label" runat="server" Text="อาคาร : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtroom_nameedit" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>
                                        <%-------------- สถานที่ติดตั้ง, อาคาร --------------%>

                                        <%-------------- Asset Code, Status --------------%>
                                        <div class="form-group">
                                            <%--<asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Visible="false" Text='<%# Eval("place_new_idx") %>' />--%>

                                            <asp:Label ID="lbasset_codeedit" CssClass="col-sm-2 control-label" runat="server" Text="Asset Code : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtasset_codeedit" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                            <%--<asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Visible="false" Text='<%# Eval("room_new_idx") %>' />--%>

                                            <asp:Label ID="lbstatus_deviecsedit" CssClass="col-sm-2 control-label" runat="server" Visible="false" Text="Status : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtstatus_deviecsedit" Visible="false" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                            </div>

                                        </div>
                                        <%-------------- Asset Code, Status --------------%>
                                    </div>
                                </div>
                            </div>

                        </asp:Panel>
                        <%--<br />--%>
                    </div>
                </div>
                <%-------------- END Show Edit --------------%>

                <%-------------- START DDL Approve Status --------------%>
                <div class="form-group">
                    <div class="col-md-12">
                        <div id="div_approvstatecreate" class="row panel panel-default font_text" runat="server" visible="false">
                            <%--<div class="panel panel-info">--%>
                            <div class="panel-heading f-bold">อนุมัติรายการ Network</div>
                            <div class="form-group" id="approvecreate" runat="server">

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <%--<div class="col-sm-1"></div>--%>
                                            <asp:Label ID="lbapprovecreate" CssClass="col-md-2 control-label" runat="server" Text="ผลการอนุมัติ :" />
                                            <div class="col-sm-4">
                                                <asp:DropDownList CssClass="form-control" ID="ddl_approvestatus" runat="server">
                                                    <asp:ListItem Value="0">กรุณาเลือกสถานะ...</asp:ListItem>
                                                    <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                                    <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="SaveApprove1" runat="server" Display="None"
                                                    ControlToValidate="ddl_approvestatus" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกสถานะการอนุมัติ"
                                                    ValidationExpression="กรุณาเลือกสถานะการอนุมัติ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />
                                            </div>

                                        </div>
                                    </div>


                                </div>
                            </div>
                            <%-- </div>--%>
                        </div>
                        <br />

                    </div>
                </div>
                <%-------------- END DDL Approve Status --------------%>

                <%-------------- START ปุ่มบันทึกผู้อนุมัติ --------------%>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div id="div_approve" class="row" runat="server" visible="false">
                            <div class="form-group">

                                <%--<div class="col-sm-6"></div>--%>
                                <%--<div class="col-sm-2">--%>
                                <div class="pull-right">
                                    <asp:LinkButton ID="btnsaveapprove" CssClass="btn btn-success" runat="server" ValidationGroup="SaveApprove1" OnCommand="btnCommand" CommandName="btnsaveapprove" data-toggle="tooltip" title="SaveApprove" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>


                                    <asp:LinkButton ID="btnCancelApprove" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="btnCancelApprove" data-toggle="tooltip" title="CancelApprove"><i class="fa fa-times"></i></asp:LinkButton>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

                <%-------------- END ปุ่มบันทึกผู้อนุมัติ --------------%>

                <%-------------- START ปุ่มบันทึกการแก้ไข --------------%>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div id="div_saveedit" class="row" runat="server" visible="false">

                            <%--<div class="form-group">--%>

                            <%--<div class="col-sm-10"></div>--%>
                            <div class="pull-right">
                                <%--<div class="col-sm-12">--%>
                                <asp:LinkButton ID="btnsaveedit" CssClass="btn btn-success" runat="server" ValidationGroup="SaveEdit" OnCommand="btnCommand" CommandName="btnsaveedit" data-toggle="tooltip" title="SaveEdit" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>


                                <asp:LinkButton ID="btnCancelEdit" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="btnCancelEdit" data-toggle="tooltip" title="CancelEdit"><i class="fa fa-times"></i></asp:LinkButton>

                                <%-- </div>--%>
                            </div>

                            <%--</div>--%>
                        </div>
                    </div>

                </div>
                <%-------------- END ปุ่มบันทึกการแก้ไข --------------%>

                <%-------------- START ปุ่มบันทึกการซ่อม --------------%>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div id="div_saverepair" class="row" runat="server" visible="false">
                            <%--<div class="form-group">--%>

                            <%-- <div class="col-sm-10"></div>--%>
                            <%--<div class="col-sm-2">--%>
                            <div class="pull-right">
                                <%--<div class="col-sm-12">--%>
                                <asp:LinkButton ID="btnsaverepair" CssClass="btn btn-success" runat="server" ValidationGroup="SaveRepair" Visible="false" OnCommand="btnCommand" CommandName="btnsaverepair" data-toggle="tooltip" title="SaveRepair" OnClientClick="return confirm('คุณต้องการเพิ่มรายการซ่อมนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>


                                <asp:LinkButton ID="btnCancelRepair" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="btnCancelRepair" data-toggle="tooltip" title="CancelRepair"><i class="fa fa-times"></i></asp:LinkButton>
                                <%-- </div>--%>
                            </div>

                            <%--</div>--%>
                        </div>
                    </div>

                </div>

                <%-------------- END ปุ่มบันทึกการซ่อม --------------%>

                <%-------------- START ปุ่มบันทึกการ MA --------------%>
                <div class="form-group">
                    <div class="col-sm-12">
                        <div id="div_savema" class="row" runat="server" visible="false">

                            <%-- <div class="form-group">--%>

                            <%--<div class="col-sm-10"></div>--%>
                            <%--<div class="col-sm-2">--%>
                            <div class="pull-right">
                                <asp:LinkButton ID="btnsaveMA" CssClass="btn btn-success" runat="server" ValidationGroup="SaveMA" Visible="false" OnCommand="btnCommand" CommandName="btnsaveMA" data-toggle="tooltip" title="SaveMA" OnClientClick="return confirm('คุณต้องการเพิ่มรายการ ma นี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>

                                <asp:LinkButton ID="btnCancelMA" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="btnCancelMA" data-toggle="tooltip" title="CancelMA"><i class="fa fa-times"></i></asp:LinkButton>
                            </div>

                            <%--</div>--%>
                        </div>
                    </div>
                </div>
                <%-------------- END ปุ่มบันทึกการ MA --------------%>


                <%-------------- START ปุ่มบันทึก --------------%>

                <div class="form-group">
                    <div id="div_btn" class="row" runat="server" visible="false">
                        <%--<div class="form-group">--%>
                        <div class="pull-right">
                            <div class="col-sm-12">
                                <asp:LinkButton ID="lbCmdCancelView" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="btnCancelView" Visible="false"><i class="fa fa-times"></i></asp:LinkButton>

                            </div>
                        </div>
                        <%--</div>--%>
                    </div>
                </div>
                <%-------------- END ปุ่มบันทึก --------------%>

                <!-- START history log -->
                <div class="form-group">
                    <div class="col-md-12">
                        <br />
                        <div class="row panel panel-default font_text" visible="false" runat="server" id="panel_history">
                            <div class="panel panel-info">
                                <%-- <h3 class="panel-heading f-bold"><strong>ประวัติข้อมูล</strong></h3>--%>
                                <div class="panel-heading f-bold">ประวัติข้อมูล Network</div>
                            </div>
                            <div class="panel-body">
                                <asp:Repeater ID="rptLog" runat="server">
                                    <HeaderTemplate>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><small>วัน / เวลา</small></label>
                                            <label class="col-sm-3 control-label"><small>ผู้ดำเนินการ</small></label>
                                            <label class="col-sm-2 control-label"><small>ดำเนินการ</small></label>
                                            <label class="col-sm-2 control-label"><small>ผลการดำเนินการ</small></label>
                                            <label class="col-sm-2 control-label"><small>ความคิดเห็น</small></label>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <span><%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%><small><%#Eval("create_date")%></small></span>
                                            </div>
                                            <div class="col-sm-3">
                                                <span><small><%# Eval("name_create") %>&nbsp;&nbsp;&nbsp;(<%# Eval("actor_des") %>)</small></span>
                                            </div>
                                            <div class="col-sm-2">
                                                <span><small><%# Eval("node_name") %></small></span>
                                            </div>
                                            <div class="col-sm-2">
                                                <span><small><%# Eval("status_name") %></small></span>
                                            </div>
                                            <div class="col-sm-2">
                                                <span><small><%# Eval("comment") %></small></span>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>

                        <%--  <h5>&nbsp;</h5>--%>
                    </div>
                </div>
                <!-- END history log -->

                <!-- START history log Repair -->
                <div class="form-group">
                    <div class="col-md-12">
                        <br />
                        <div class="row panel panel-default font_text" visible="false" runat="server" id="panel_history_repair">
                            <div class="panel panel-info">
                                <%-- <h3 class="panel-heading f-bold"><strong>ประวัติข้อมูล</strong></h3>--%>
                                <div class="panel-heading f-bold">ประวัติการซ่อมอุปกรณ์ Network</div>
                            </div>
                            <div class="panel-body">
                                <asp:Repeater ID="rptLogRepair" runat="server">
                                    <HeaderTemplate>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-3">
                                                    <label class="control-label"><small>วัน / เวลาสร้าง &nbsp;&nbsp; </small></label>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label"><small>ผู้ดำเนินการ</small></label>
                                                </div>
                                                <div class="col-md-3">
                                                    <label class="control-label"><small>วัน / เวลาซ่อม</small></label>
                                                </div>
                                                <%--<label class="col-sm-2 control-label"><small>ผลการดำเนินการ</small></label>--%>
                                                <div class="col-md-3">
                                                    <label class="control-label"><small>รายละเอียด</small></label>
                                                </div>
                                            </div>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="col-md-12">
                                            <div class="row">
                                                <%--<div class="col-md-1"></div>--%>
                                                <div class="col-sm-3">
                                                    <span><%--&nbsp;&nbsp;&nbsp;&nbsp;--%><small><%#Eval("create_date")%></small></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <span><small><%# Eval("name_create") %></small></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <span><small><%# Eval("date_repair") %></small></span>
                                                </div>
                                                <%-- <div class="col-sm-2">--%>
                                                <%-- <span><small><%# Eval("status_name") %></small></span>--%>
                                                <%-- </div>--%>
                                                <div class="col-sm-3">
                                                    <span><small><%# Eval("comment") %></small></span>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>

                        <%--  <h5>&nbsp;</h5>--%>
                    </div>
                </div>
                <!-- END history log Repair -->

                <!-- START history log MA -->
                <div class="form-group">
                    <div class="col-md-12">
                        <%-- <br />--%>
                        <div class="row panel panel-default font_text" visible="false" runat="server" id="panel_history_ma">
                            <div class="panel panel-info">
                                <%-- <h3 class="panel-heading f-bold"><strong>ประวัติข้อมูล</strong></h3>--%>
                                <div class="panel-heading f-bold">ประวัติการ MA</div>
                            </div>
                            <div class="panel-body">
                                <asp:Repeater ID="rptLogMA" runat="server">
                                    <HeaderTemplate>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><small>วัน / เวลา สร้าง</small></label>
                                            <label class="col-sm-3 control-label"><small>ผู้ดำเนินการ</small></label>
                                            <label class="col-sm-2 control-label"><small>วันที่เริ่มประกัน</small></label>
                                            <label class="col-sm-2 control-label"><small>วันที่หมดประกัน</small></label>
                                            <label class="col-sm-2 control-label"><small>ความคิดเห็น</small></label>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <span><%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%><small><%#Eval("create_date")%></small></span>
                                            </div>
                                            <div class="col-sm-3">
                                                <span><small><%# Eval("name_create") %></small></span>
                                            </div>
                                            <div class="col-sm-2">
                                                <span><small><%# Eval("date_start") %></small></span>
                                            </div>
                                            <div class="col-sm-2">
                                                <span><small><%# Eval("date_end") %></small></span>
                                            </div>
                                            <div class="col-sm-2">
                                                <span><small><%# Eval("comment") %></small></span>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>

                        <%--  <h5>&nbsp;</h5>--%>
                    </div>
                </div>
                <!-- END history log MA-->


                <!--*** END Show Data in Viewindex ***-->
            </div>
            <br />
        </asp:View>
        <%----------- ViewIndex Network Deviecs END ---------------%>

        <%----------- View Network Deviecs Insert Start ---------------%>
        <asp:View ID="ViewNetworkDevices" runat="server">
            <div class="col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading f-bold">สร้างรายการทะเบียนอุปกรณ์ Network</div>
                </div>
            </div>
            <asp:FormView ID="FvInsert" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                <InsertItemTemplate>
                    <div class="col-sm-12">
                        <div class="panel panel-info">
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbnamecreate" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtnamecreate" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="lbdeptcreate" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtdeptcreate" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>
                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>

                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="lbseccreate" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtseccreate" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="lbposcreate" CssClass="col-sm-2 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtposcreate" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>
                                    <%-------------- แผนก,ตำแหน่ง --------------%>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="lbtelcreate" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">

                                            <asp:TextBox ID="txttelcreate" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="lbemailcreate" CssClass="col-sm-2 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">

                                            <asp:TextBox ID="txtemailcreate" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>

                                        </div>

                                    </div>
                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                </div>

                            </div>
                        </div>

                    </div>

                </InsertItemTemplate>
            </asp:FormView>
            <%----Start ขั้น Form แสดงคนทำรายการ Start ------%>
            <%-- <hr />--%>
            <%--- END ขั้น Form แสดงคนทำรายการ END --------%>

            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading f-bold">กรอกข้อมูลอุปกรณ์ Network</div>
                </div>
            </div>


            <asp:FormView ID="FvAddNetworkDevices" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                <InsertItemTemplate>
                    <div class="col-sm-12">
                        <div class="panel panel-info">
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ยี่ห้อ,รุ่น --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbbrandadd" CssClass="col-sm-2 control-label" runat="server" Text="ยี่ห้อ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtbrandadd" CssClass="form-control" placeholder="ยี่ห้อ..." runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Requiredtxtbrandadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                ControlToValidate="txtbrandadd" Font-Size="11"
                                                ErrorMessage="กรุณากรอกชื่อยี่ห้อ"
                                                ValidationExpression="กรุณากรอกชื่อยี่ห้อ" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtbrandadd" Width="160" />

                                        </div>

                                        <asp:Label ID="lbgenerationadd" CssClass="col-sm-2 control-label" runat="server" Text="รุ่น : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtgenerationadd" CssClass="form-control" placeholder="รุ่น..." runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Requiredtxtgenerationadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                ControlToValidate="txtgenerationadd" Font-Size="11"
                                                ErrorMessage="กรุณากรอกชื่อรุ่น"
                                                ValidationExpression="กรุณากรอกชื่อรุ่น" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtgenerationadd" Width="160" />
                                        </div>

                                    </div>
                                    <%-------------- ยี่ห้อ,รุ่น --------------%>

                                    <%-------------- ประเภท,ชนิด --------------%>
                                    <asp:UpdatePanel ID="update_test" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <asp:Label ID="lbtypeidx" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddltypeidxadd" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="Requiredddltypeidxadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                        ControlToValidate="ddltypeidxadd" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกประเภทอุปกรณ์"
                                                        ValidationExpression="กรุณาเลือกประเภทอุปกรณ์" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddltypeidxadd" Width="160" />
                                                </div>

                                                <asp:Label ID="lbcategoryidx" CssClass="col-sm-2 control-label" runat="server" Text="ชนิดอุปกรณ์ : " />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlcategoryidxadd" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="กรุณาเลือกชนิดอุปกรณ์ ...." Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddltypeidxadd" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <%-------------- ประเภท,ชนิด --------------%>

                                    <%-------------- บริษัท --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbcompanyidx" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทที่ต่อประกัน : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlcompanyidxadd" runat="server" CssClass="form-control"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Requiredddlcompanyidxadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                ControlToValidate="ddlcompanyidxadd" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกบริษัทที่ต่อประกัน"
                                                ValidationExpression="กรุณาเลือกบริษัทที่ต่อประกัน" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlcompanyidxadd" Width="160" />
                                        </div>

                                    </div>

                                    <%-------------- บริษัท --------------%>

                                    <%-------------- ip,serial --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="lbipaddress" CssClass="col-sm-2 control-label" runat="server" Text="IP Address : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtipaddressadd" CssClass="form-control" runat="server" placeholder="IP Address..."></asp:TextBox>
                                        </div>

                                        <asp:Label ID="lbserial" CssClass="col-sm-2 control-label" runat="server" Text="Serial Number : " />
                                        <div class="col-sm-3">
                                            <small>
                                                <asp:TextBox ID="txtserialadd" CssClass="form-control" placeholder="Serial Number..." runat="server"></asp:TextBox>

                                            </small>

                                            <asp:RequiredFieldValidator ID="Requiredtxtserialadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                ControlToValidate="txtserialadd" Font-Size="11"
                                                ErrorMessage="กรุณากรอกชื่อยี่ห้อ"
                                                ValidationExpression="กรุณากรอกชื่อยี่ห้อ" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtserialadd" Width="160" />
                                        </div>

                                    </div>
                                    <%-------------- ip,serial --------------%>

                                    <%-------------- วันที่ซื้อ , วันที่หมดประกัน --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="lbdatepurchase" CssClass="col-sm-2 control-label" runat="server" Text="วันที่ซื้อ : " />
                                        <div class="col-sm-3">
                                            <div class='input-group date from-date-datepicker'>
                                                <asp:TextBox ID="txtdatepurchaseadd" CssClass="form-control" runat="server" placeholder="วันที่ซื้อ..."></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                                <asp:RequiredFieldValidator ID="Requiredtxtdatepurchaseadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                    ControlToValidate="txtdatepurchaseadd" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกวันที่ซื้อ"
                                                    ValidationExpression="กรุณากรอกวันที่ซื้อ" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtdatepurchaseadd" Width="160" />
                                            </div>

                                        </div>

                                        <asp:Label ID="lbdateexpire" CssClass="col-sm-2 control-label" runat="server" Text="วันที่หมดประกัน : " />
                                        <div class="col-sm-3">
                                            <div class='input-group date from-date-datepickerexpire'>
                                                <asp:TextBox ID="txtdateexpireadd" CssClass="form-control" placeholder="วันที่หมดประกัน..." runat="server"></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                                <asp:RequiredFieldValidator ID="Requiredtxtdateexpireadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                    ControlToValidate="txtdateexpireadd" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกวันที่หมดประกัน"
                                                    ValidationExpression="กรุณากรอกวันที่หมดประกัน" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtdateexpireadd" Width="160" />
                                            </div>
                                        </div>

                                    </div>
                                    <%-------------- วันที่ซื้อ , วันที่หมดประกัน --------------%>

                                    <%-------------- สถานที่,อาคาร --------------%>
                                    <asp:UpdatePanel ID="update_test1" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <asp:Label ID="lbplace" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ติดตั้ง : " />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlplaceadd" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="Requiredddlplaceadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                        ControlToValidate="ddlplaceadd" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกสถานที่ติดตั้ง"
                                                        ValidationExpression="กรุณาเลือกสถานที่ติดตั้ง" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlplaceadd" Width="160" />
                                                </div>

                                                <asp:Label ID="lbroom" CssClass="col-sm-2 control-label" runat="server" Text="อาคาร : " />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlroomadd" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="กรุณาเลือกอาคาร ...." Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <%--  <asp:RequiredFieldValidator ID="Requiredddlroomadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                ControlToValidate="ddlroomadd" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกอาคาร"
                                                ValidationExpression="กรุณาเลือกอาคาร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlroomadd" Width="160" />--%>
                                                </div>

                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddltypeidxadd" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <%-------------- สถานที่,อาคาร --------------%>

                                    <%-------------- username, password --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbusername" CssClass="col-sm-2 control-label" runat="server" Text="User Name : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtusernameadd" CssClass="form-control" placeholder="User Name..." runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="lbpassword" CssClass="col-sm-2 control-label" runat="server" Text="Password : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpasswordadd" CssClass="form-control" placeholder="Password..." runat="server"></asp:TextBox>
                                        </div>

                                    </div>
                                    <%-------------- username, password --------------%>


                                    <%-------------- asset code, เลขทะเบียนอุปกรณ์ --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbassedcode" CssClass="col-sm-2 control-label" runat="server" Text="Asset Code : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtassetcodeadd" CssClass="form-control" placeholder="Asset Code..." runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Requiredtxtassetcodeadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                ControlToValidate="txtassetcodeadd" Font-Size="11"
                                                ErrorMessage="กรุณากรอกชื่อ Asset Code"
                                                ValidationExpression="กรุณากรอกชื่อ Asset Code" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtassetcodeadd" Width="160" />
                                        </div>

                                        <asp:Label ID="lbregisternumber" CssClass="col-sm-2 control-label" runat="server" Text="เลขทะเบียน : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtregisteradd" CssClass="form-control" placeholder="Register Number..." runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="Requiredtxtregisteradd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                ControlToValidate="txtregisteradd" Font-Size="11"
                                                ErrorMessage="กรุณากรอกชื่อ Register Number"
                                                ValidationExpression="กรุณากรอกชื่อ Register Number" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtregisteradd" Width="160" />
                                        </div>

                                    </div>
                                    <%-------------- asset code, เลขทะเบียนอุปกรณ์ --------------%>

                                    <%-------------- status devices--------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbstatusdevices" CssClass="col-sm-2 control-label" runat="server" Text="Status : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlstatusdevicesadd" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="1" Text="Online" />
                                                <asp:ListItem Value="0" Text="Offline" />
                                            </asp:DropDownList>
                                        </div>

                                    </div>
                                    <%-------------- status devices --------------%>


                                    <%-------------- รายละเอียด --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbdetail" CssClass="col-sm-2 control-label" runat="server" Text="รายละเอียดอุปกรณ์ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtdetailadd" TextMode="multiline" Width="510pt" Rows="4" CssClass="form-control" placeholder="รายละเอียดอุปกรณ์..." runat="server"></asp:TextBox>
                                        </div>

                                    </div>
                                    <%-------------- รายละเอียด --------------%>

                                    <%-------------- เลือกรูปภาพ --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbSelectFile" CssClass="col-sm-2 control-label" runat="server" Text="เลือกไฟล์ : " />

                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <asp:FileUpload ID="upload_file" Font-Size="small" ViewStateMode="Enabled" runat="server" CssClass="control-label" accept="jpg" />
                                            </div>

                                        </div>
                                        <%-- <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpg เท่านั้น</font></p>--%>
                                        <%--<div class="form-group">--%>
                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-12">
                                                <asp:RequiredFieldValidator ID="ValidateF1" runat="server" ErrorMessage="***กรุณาเลือกเฉพาะไฟล์ jpg***" ForeColor="Red" ValidationGroup="Save" CssClass="row-validate" ControlToValidate="upload_file"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="ValidateEx" runat="server"
                                                    ValidationExpression="[a-zA-Z\\].*(.jpg|.JPG)$" ControlToValidate="upload_file" ValidationGroup="Saveinsert" Font-Size="Small" ForeColor="Red"
                                                    ErrorMessage="***กรุณาเลือกเฉพาะไฟล์ jpg***"></asp:RegularExpressionValidator>
                                            </div>
                                            <%-- </div>--%>


                                            <%--<ajaxToolkit:AjaxFileUpload ID="upload_file" runat="server" ThrobberID="myThrobber" OnClientUploadComplete="upload_file_UploadComplete" MaximumNumberOfFiles="1" AllowedFileTypes="jpg" />--%>


                                            <%--<ajaxToolkit:AjaxFileUpload ID="upload_file" ThrobberID="lbSelectFile"  AllowedFileTypes="jpg" MaximumNumberOfFiles="1" onuploadcomplete="upload_file_UploadComplete" runat="server"/>--%>
                                            <%-- <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpg ขนาดไม่เกิน 5 MB</font></p>--%>
                                        </div>


                                    </div>

                                    <%-------------- เลือกรูปภาพ --------------%>

                                    <br />
                                    <%-------------- ปุ่มบันทึก --------------%>

                                    <asp:UpdatePanel ID="updatet1" runat="server">
                                        <ContentTemplate>

                                            <div class="form-group">
                                                <div class="col-lg-offset-10 col-sm-2">
                                                    <%--<div class="pull-right">--%>
                                                    <asp:LinkButton ID="btnInsert" CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand" ValidationGroup="Saveinsert" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')">บันทึก</asp:LinkButton>

                                                    <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancel">ยกเลิก</asp:LinkButton>
                                                </div>

                                            </div>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnInsert" />
                                        </Triggers>

                                    </asp:UpdatePanel>

                                    <%-------------- ปุ่มบันทึก --------------%>
                                </div>

                            </div>
                        </div>

                    </div>

                </InsertItemTemplate>
            </asp:FormView>

        </asp:View>
        <%----------- View Network Deviecs Insert END ---------------%>

        <%----------- View Move Network Deviecs Start ---------------%>
        <asp:View ID="ViewNetworkDevicesMove" runat="server">

            <%----------- START FV Cut Search Network Deviecs  ---------------%>
            <asp:FormView ID="FvMoveNetworkDevices" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                <InsertItemTemplate>
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading f-bold">ค้นหาอุปกรณ์ที่ต้องการย้าย</div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- เลขทะเบียนอุปกรณ์ --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbregister_numbermove" CssClass="col-sm-3 control-label" runat="server" Text="เลขทะเบียนอุปกรณ์ Network : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlregister_numbermove" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>

                                        <div class="pull-left">
                                            <asp:LinkButton ID="btnSearchMove" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchMove" OnCommand="btnCommand" CommandName="btnSearchMove" data-toggle="tooltip" title="Search" OnClientClick="return confirm('คุณต้องการค้นหารายการนี้ใช่หรือไม่ ?')"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                                        </div>
                                    </div>
                                    <%-------------- เลขทะเบียนอุปกรณ์ --------------%>
                                </div>

                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>
            <%----------- END FV Cut Search Network Deviecs ---------------%>

            <!--*** START Back To Search Move ***-->
            <div class="form-group">
                <div class="col-md-12">
                    <div id="fvBacktoSearchMove" class="row" runat="server" visible="false">
                        <%--<div class="col-md-12">--%>
                        <asp:LinkButton ID="btnSearchMoveBack" CssClass="btn btn-info" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnSearchMoveBack"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back</asp:LinkButton>
                        <%--</div>--%>
                    </div>
                </div>
                <%--<br />--%>
            </div>
            <!--*** END Back To Search Move ***-->

            <%----------- START FV Move Show Detail Search Network Deviecs  ---------------%>
            <asp:FormView ID="FvDetailShowMove" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                <ItemTemplate>
                    <div class="col-sm-12">
                        <br />
                        <div class="panel panel-info">
                            <div class="panel-heading f-bold">รายละเอียดอุปกรณ์ Network(ย้าย) </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <asp:Label ID="litDebug3" runat="server"></asp:Label>
                                    <asp:TextBox ID="lbu0idx_networkmove" runat="server" Text='<%# Eval("u0_devicenetwork_idx") %>' Visible="false" />

                                    <%-------------- เลขทะเบียนอุปกรณ์, Asset Code --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbregister_numberdetailmove" CssClass="col-sm-2 control-label" runat="server" Text="เลขทะเบียนอุปกรณ์ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtregister_numberdetailmove" Text='<%# Eval("register_number") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="lbasset_codemove" CssClass="col-sm-2 control-label" runat="server" Text="Asset Code : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtasset_codemove" Text='<%# Eval("asset_code") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>
                                    <%-------------- เลขทะเบียนอุปกรณ์, Asset Code --------------%>

                                    <%-------------- ยี่ห้อ, รุ่น --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbbrand_namemove" CssClass="col-sm-2 control-label" runat="server" Text="ยี่ห้อ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtbrand_namemove" Text='<%# Eval("brand_name") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="lbgeneration_namemove" CssClass="col-sm-2 control-label" runat="server" Text="รุ่น : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtgeneration_namemove" Text='<%# Eval("generation_name") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>
                                    <%-------------- ยี่ห้อ, รุ่น --------------%>

                                    <%-------------- สถานที่ติดตั้งเดิม, อาคารเดิม --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lblplace_namemove" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ติดตั้งเดิม : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtplace_namemove" Text='<%# Eval("place_name") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="lblroom_namemove" CssClass="col-sm-2 control-label" runat="server" Text="อาคารเดิม : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtroom_namemove" Text='<%#getRoomname((string)Eval("room_name")) %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>
                                    <%-------------- สถานที่ติดตั้งเดิม, อาคารเดิม --------------%>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%--START gvFile--%>
                    <div class="col-md-12">
                        <div class="form-group">
                            <%--<label class="col-md-10 col-lg-offset-1 ">--%>
                            <%--<asp:Label ID="FileUser" runat="server" Visible="false" Text='<%# Eval("I_fileuser")%>' />--%>
                            <asp:GridView ID="gvFileMove" Visible="true" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover " OnRowDataBound="Master_RowDataBound">

                                <Columns>
                                    <asp:TemplateField HeaderText="  ไฟล์เอกสารเพิ่มเติม ">
                                        <ItemTemplate>
                                            <div class="col-lg-10">
                                                <asp:Literal ID="ltFileNameMove" runat="server" Text='<%# Eval("FileName") %>' />

                                            </div>
                                            <div class="col-lg-2">
                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                            </div>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                            <h4></h4>
                            <%-- </label>--%>
                        </div>
                    </div>
                    <%--END gvFile--%>
                </ItemTemplate>

            </asp:FormView>
            <%----------- START FV Move Show Detail Search Network Deviecs  ---------------%>

            <%-------------- START DDL Cut Status --------------%>
            <div class="col-md-12">
                <div id="div_statemove" class="row panel panel-default font_text" runat="server" visible="false">
                    <%--<div class="panel panel-info">--%>
                    <div class="panel-heading f-bold">ย้ายอุปกรณ์ Network</div>
                    <%--<div class="form-group" id="Div2" runat="server">--%>

                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <%-------------- สถานะการย้าย --------------%>
                            <div class="form-group">
                                <%--<div class="col-sm-1"></div>--%>
                                <asp:Label ID="lbstatus_move" CssClass="col-md-2 control-label" runat="server" Text="สถานะการย้าย :" />
                                <div class="col-sm-3">
                                    <asp:DropDownList CssClass="form-control" ID="ddlstatus_move" runat="server">
                                        <asp:ListItem Value="0">กรุณาเลือกสถานะ ...</asp:ListItem>
                                        <asp:ListItem Value="5">ย้ายอุปกรณ์</asp:ListItem>
                                        <%--<asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>--%>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredStatusMove" ValidationGroup="Saveinsertmove" runat="server" Display="None"
                                        ControlToValidate="ddlstatus_move" Font-Size="11"
                                        ErrorMessage="กรุณาเลือกสถานะการย้าย"
                                        ValidationExpression="กรุณาเลือกสถานะการย้าย" InitialValue="0" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredStatusMove" Width="160" />
                                </div>

                            </div>
                            <%-------------- สถานะการย้าย --------------%>

                            <%-------------- สถานที่,อาคาร --------------%>
                            <asp:UpdatePanel ID="update_move" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="form-group">
                                        <asp:Label ID="lbplacemove" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ติดตั้ง : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlplacemove" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Re_ddlplacemove"
                                                ValidationGroup="Saveinsertmove" runat="server" Display="None"
                                                ControlToValidate="ddlplacemove" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกสถานที่ติดตั้ง"
                                                ValidationExpression="กรุณาเลือกสถานที่ติดตั้ง" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender74444" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlplacemove" Width="160" />
                                        </div>

                                        <asp:Label ID="lbroommove" CssClass="col-sm-2 control-label" runat="server" Text="อาคาร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlroommove" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="กรุณาเลือกอาคาร ...." Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                            <%--<asp:RequiredFieldValidator ID="Re_ddlroommove" ValidationGroup="Saveinsertmove" runat="server" Display="None"
                                                ControlToValidate="ddlroommove" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกอาคาร"
                                                ValidationExpression="กรุณาเลือกอาคาร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender83333" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlroommove" Width="160" />--%>
                                        </div>

                                    </div>
                                </ContentTemplate>
                                <%--<Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddltypeidxadd" EventName="SelectedIndexChanged" />
                                        </Triggers>--%>
                            </asp:UpdatePanel>
                            <%-------------- สถานที่,อาคาร --------------%>

                            <%-------------- รายละเอียด --------------%>
                            <div class="form-group">
                                <asp:Label ID="lbdetailmove" CssClass="col-sm-2 control-label" runat="server" Text="หมายเหตุ : " />
                                <div class="col-sm-8">
                                    <asp:TextBox ID="txtdetailmove" TextMode="multiline" Rows="4" CssClass="form-control" placeholder="หมายเหตุการย้ายอุปกรณ์ ..." runat="server"></asp:TextBox>
                                </div>

                            </div>
                            <%-------------- รายละเอียด --------------%>

                            <%-------------- เลือกรูปภาพ --------------%>
                            <div class="form-group">
                                <asp:Label ID="lbSelectFileMove" CssClass="col-sm-2 control-label" runat="server" Text="เลือกไฟล์ : " Visible="false" />

                                <div class="col-sm-4">
                                    <asp:FileUpload ID="upload_filemove" Font-Size="small" Visible="false" ViewStateMode="Enabled" runat="server" CssClass="control-label" accept="jpg" />
                                    <%--<ajaxToolkit:AjaxFileUpload ID="upload_file" runat="server" ThrobberID="myThrobber" OnClientUploadComplete="upload_file_UploadComplete" MaximumNumberOfFiles="1" AllowedFileTypes="jpg" />--%>


                                    <%--<ajaxToolkit:AjaxFileUpload ID="upload_file" ThrobberID="lbSelectFile"  AllowedFileTypes="jpg" MaximumNumberOfFiles="1" onuploadcomplete="upload_file_UploadComplete" runat="server"/>--%>
                                    <%-- <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpg ขนาดไม่เกิน 5 MB</font></p>--%>
                                </div>


                            </div>

                            <%-------------- เลือกรูปภาพ --------------%>

                            <br />
                            <%-------------- ปุ่มบันทึก --------------%>

                            <asp:UpdatePanel ID="updatetmovesave" runat="server">
                                <ContentTemplate>

                                    <div class="form-group">
                                        <%--<div class="pull-right">--%>
                                        <div class="col-lg-offset-10 col-sm-2">
                                            <asp:LinkButton ID="btnInsertMove" CssClass="btn btn-success" runat="server" CommandName="btnInsertMove" OnCommand="btnCommand" data-toggle="tooltip" title="SaveMove" ValidationGroup="Saveinsertmove" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')">บันทึก</asp:LinkButton>

                                            <asp:LinkButton ID="btnCancelMove" CssClass="btn btn-danger" runat="server" data-toggle="tooltip" title="CancelMove" OnCommand="btnCommand" CommandName="btnCancelMove">ยกเลิก</asp:LinkButton>
                                        </div>

                                    </div>

                                </ContentTemplate>
                                <%--  <Triggers>
                                    <asp:PostBackTrigger ControlID="btnInsertMove" />
                                </Triggers>--%>
                            </asp:UpdatePanel>

                            <%-------------- ปุ่มบันทึก --------------%>
                        </div>


                    </div>
                    <%--</div>--%>
                    <%-- </div>--%>
                </div>
                <br />
            </div>
            <%-------------- END DDL Cut Status --------------%>
        </asp:View>

        <%----------- View Move Network Deviecs END ---------------%>

        <%----------- ViewCut Network Deviecs Start ---------------%>
        <asp:View ID="ViewNetworkDevicesCut" runat="server">

            <%----------- START FV Cut Search Network Deviecs  ---------------%>
            <asp:FormView ID="FvCutNetworkDevices" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                <InsertItemTemplate>
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-heading f-bold">ค้นหาอุปกรณ์ที่ต้องการตัดชำรุด</div>
                            <div class="panel-body">

                                <div class="form-horizontal" role="form">

                                    <%-------------- เลขทะเบียนอุปกรณ์ --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbregister_numbercut" CssClass="col-sm-3 control-label" runat="server" Text="เลขทะเบียนอุปกรณ์ Network : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlregister_numbercut" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>


                                        <div class="pull-left">
                                            <asp:LinkButton ID="btnSearchCut" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchCut" OnCommand="btnCommand" CommandName="btnSearchCut" data-toggle="tooltip" title="Search" OnClientClick="return confirm('คุณต้องการค้นหารายการนี้ใช่หรือไม่ ?')"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                                        </div>
                                    </div>
                                    <%-------------- เลขทะเบียนอุปกรณ์ --------------%>
                                </div>

                            </div>

                        </div>
                    </div>

                </InsertItemTemplate>
            </asp:FormView>
            <%----------- END FV Cut Search Network Deviecs ---------------%>


            <!--*** START Back To Search Cut ***-->
            <div class="form-group">
                <div class="col-md-12">
                    <div id="fvBacktoSearchCut" class="row" runat="server" visible="false">
                        <%--<div class="col-md-12">--%>
                        <asp:LinkButton ID="btnSearchCutBack" CssClass="btn btn-info" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnSearchCutBack"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back</asp:LinkButton>
                        <%--</div>--%>
                    </div>
                </div>
                <%--<br />--%>
            </div>
            <!--*** END Back To Search Cut ***-->

            <%----------- START FV Cut Show Detail Search Network Deviecs  ---------------%>
            <asp:FormView ID="FvDetailShowCut" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                <ItemTemplate>
                    <div class="col-sm-12">
                        <br />
                        <div class="panel panel-info">
                            <div class="panel-heading f-bold">รายละเอียดอุปกรณ์ Network(ตัดชำรุด) </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <asp:Label ID="litDebug1" runat="server"></asp:Label>
                                    <asp:TextBox ID="lbu0idx_networkcut" runat="server" Text='<%# Eval("u0_devicenetwork_idx") %>' Visible="false" />

                                    <%-------------- เลขทะเบียนอุปกรณ์, Asset Code --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbregister_numberdetailcut" CssClass="col-sm-2 control-label" runat="server" Text="เลขทะเบียนอุปกรณ์ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtregister_numberdetailcut" Text='<%# Eval("register_number") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="lbasset_codecut" CssClass="col-sm-2 control-label" runat="server" Text="Asset Code : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtasset_codecut" Text='<%# Eval("asset_code") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>
                                    <%-------------- เลขทะเบียนอุปกรณ์, Asset Code --------------%>

                                    <%-------------- ยี่ห้อ, รุ่น --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbbrand_namecut" CssClass="col-sm-2 control-label" runat="server" Text="ยี่ห้อ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtbrand_namecut" Text='<%# Eval("brand_name") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="lbgeneration_namecut" CssClass="col-sm-2 control-label" runat="server" Text="รุ่น : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtgeneration_namecut" Text='<%# Eval("generation_name") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>
                                    <%-------------- ยี่ห้อ, รุ่น --------------%>

                                    <%-------------- อายุการใช้งาน, วันหมดประกัน --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbdiff_datecut" CssClass="col-sm-2 control-label" runat="server" Text="อายุการใช้งาน(วัน) : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtdiff_datecut" Text='<%# Eval("diff_date") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="lbdate_expirecut" CssClass="col-sm-2 control-label" runat="server" Text="วันหมดประกัน : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtdate_expirecut" Text='<%# Eval("date_expire") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>
                                    <%-------------- อายุการใช้งาน, วันหมดประกัน --------------%>
                                </div>
                            </div>
                        </div>
                    </div>

                    <%--START gvFile--%>
                    <div class="col-md-12">
                        <div class="form-group">
                            <%--<label class="col-md-10 col-lg-offset-1 ">--%>
                            <%--<asp:Label ID="FileUser" runat="server" Visible="false" Text='<%# Eval("I_fileuser")%>' />--%>
                            <asp:GridView ID="gvFileCut" Visible="true" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover " OnRowDataBound="Master_RowDataBound">

                                <Columns>
                                    <asp:TemplateField HeaderText="  ไฟล์เอกสารเพิ่มเติม ">
                                        <ItemTemplate>
                                            <div class="col-lg-10">
                                                <asp:Literal ID="ltFileNameCut" runat="server" Text='<%# Eval("FileName") %>' />

                                            </div>
                                            <div class="col-lg-2">
                                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                            </div>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                            <h4></h4>
                            <%-- </label>--%>
                        </div>
                    </div>
                    <%--END gvFile--%>
                </ItemTemplate>
            </asp:FormView>

            <%----------- START FV Cut Show Detail Search Network Deviecs  ---------------%>


            <%-------------- START DDL Cut Status --------------%>
            <div class="col-md-12">
                <div id="div_statecut" class="row panel panel-default font_text" runat="server" visible="false">
                    <%--<div class="panel panel-info">--%>
                    <div class="panel-heading f-bold">ตัดชำรุดรายการ Network</div>
                    <div class="form-group" id="statuscut" runat="server">

                        <div class="panel-body">

                            <div class="form-group">
                                <div class="col-sm-1"></div>
                                <asp:Label ID="lbstatus_cut" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สถานะการตัดชำรุด :" />
                                <div class="col-sm-5">
                                    <asp:DropDownList CssClass="form-control" ID="ddl_statuscut" runat="server">
                                        <asp:ListItem Value="0">กรุณาเลือกสถานะ ...</asp:ListItem>
                                        <asp:ListItem Value="4">ตัดชำรุด</asp:ListItem>
                                        <%--<asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>--%>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredStatusCut" ValidationGroup="SaveCut" runat="server" Display="None"
                                        ControlToValidate="ddl_statuscut" Font-Size="11"
                                        ErrorMessage="กรุณาเลือกสถานะการตัดชำรุด"
                                        ValidationExpression="กรุณาเลือกสถานะการตัดชำรุด" InitialValue="0" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredStatusCut" Width="160" />
                                </div>

                            </div>


                        </div>
                    </div>
                    <%-- </div>--%>
                </div>
                <br />
            </div>
            <%-------------- END DDL Cut Status --------------%>


            <%-------------- START ปุ่มบันทึก --------------%>
            <div class="col-sm-12">
                <div id="div_savecut" class="row" runat="server" visible="false">

                    <div class="form-group">

                        <div class="col-sm-7"></div>
                        <div class="col-sm-2">
                            <asp:LinkButton ID="btnSaveCut" CssClass="btn btn-success" runat="server" ValidationGroup="SaveCut" OnCommand="btnCommand" CommandName="btnSaveCut" data-toggle="tooltip" title="SaveCut" OnClientClick="return confirm('คุณต้องการตัดชำรุดรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>


                            <asp:LinkButton ID="btnCancelCut" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="btnCancelCut" data-toggle="tooltip" title="CancelCut"><i class="fa fa-times"></i></asp:LinkButton>
                        </div>

                    </div>
                </div>
                <br />
            </div>

            <%-------------- END ปุ่มบันทึก --------------%>


            <!-- START history log Repair Cut -->
            <div class="col-md-12">

                <%-- <br />--%>
                <div class="row panel panel-default font_text" visible="false" runat="server" id="panel_history_repaircut">
                    <div class="panel panel-info">
                        <%-- <h3 class="panel-heading f-bold"><strong>ประวัติข้อมูล</strong></h3>--%>
                        <div class="panel-heading f-bold">ประวัติการซ่อมอุปกรณ์ Network</div>
                    </div>
                    <div class="panel-body">
                        <asp:Repeater ID="rptLogRepairCut" runat="server">
                            <HeaderTemplate>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-md-3">
                                            <label class="control-label"><small>วัน / เวลาสร้าง &nbsp;&nbsp; </small></label>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="control-label"><small>ผู้ดำเนินการ</small></label>
                                        </div>
                                        <div class="col-md-3">
                                            <label class="control-label"><small>วัน / เวลาซ่อม</small></label>
                                        </div>
                                        <%--<label class="col-sm-2 control-label"><small>ผลการดำเนินการ</small></label>--%>
                                        <div class="col-md-3">
                                            <label class="control-label"><small>รายละเอียด</small></label>
                                        </div>
                                    </div>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="col-md-12">
                                    <div class="row">
                                        <%--<div class="col-md-1"></div>--%>
                                        <div class="col-sm-3">
                                            <span><%--&nbsp;&nbsp;&nbsp;&nbsp;--%><small><%#Eval("create_date")%></small></span>
                                        </div>
                                        <div class="col-sm-3">
                                            <span><small><%# Eval("name_create") %></small></span>
                                        </div>
                                        <div class="col-sm-3">
                                            <span><small><%# Eval("date_repair") %></small></span>
                                        </div>
                                        <%-- <div class="col-sm-2">--%>
                                        <%-- <span><small><%# Eval("status_name") %></small></span>--%>
                                        <%-- </div>--%>
                                        <div class="col-sm-3">
                                            <span><small><%# Eval("comment") %></small></span>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>

                <%--  <h5>&nbsp;</h5>--%>
                <br />
            </div>
            <!-- END history log Repair Cut -->

            <!-- START history log MA Cut -->
            <div class="col-md-12">
                <%-- <br />--%>
                <div class="row panel panel-default font_text" visible="false" runat="server" id="panel_history_macut">
                    <div class="panel panel-info">
                        <%-- <h3 class="panel-heading f-bold"><strong>ประวัติข้อมูล</strong></h3>--%>
                        <div class="panel-heading f-bold">ประวัติการ MA</div>
                    </div>
                    <div class="panel-body">
                        <asp:Repeater ID="rptLogMACut" runat="server">
                            <HeaderTemplate>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"><small>วัน / เวลา สร้าง</small></label>
                                    <label class="col-sm-3 control-label"><small>ผู้ดำเนินการ</small></label>
                                    <label class="col-sm-2 control-label"><small>วันที่เริ่มประกัน</small></label>
                                    <label class="col-sm-2 control-label"><small>วันที่หมดประกัน</small></label>
                                    <label class="col-sm-2 control-label"><small>ความคิดเห็น</small></label>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <span><%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%><small><%#Eval("create_date")%></small></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <span><small><%# Eval("name_create") %></small></span>
                                    </div>
                                    <div class="col-sm-2">
                                        <span><small><%# Eval("date_start") %></small></span>
                                    </div>
                                    <div class="col-sm-2">
                                        <span><small><%# Eval("date_end") %></small></span>
                                    </div>
                                    <div class="col-sm-2">
                                        <span><small><%# Eval("comment") %></small></span>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>

                <%--  <h5>&nbsp;</h5>--%>
                <br />
            </div>
            <!-- END history log MA Cut -->


        </asp:View>
        <%----------- ViewCut Network Deviecs END ---------------%>

        <%----------- View Show Detail QRCode Start ---------------%>
        <asp:View ID="ViewDetailQrCode" runat="server">
            <div class="col-sm-12">
                <div class="panel panel-info">
                    <div class="panel-heading f-bold">รายละเอียดทะเบียนอุปกรณ์ Network</div>
                </div>
            </div>

            <asp:FormView ID="FvDetailShowQRCode" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                <ItemTemplate>

                    <div class="col-sm-12">
                        <div class="panel panel-info">
                            <div class="panel-heading f-bold">รายละเอียดอุปกรณ์ Network </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%--<asp:TextBox ID="lbu0idx_networkqrcode" runat="server" Text='<%# Eval("u0_devicenetwork_idx") %>' Visible="false" />--%>

                                    <%-------------- เลขทะเบียนอุปกรณ์, วันที่หมดประกัน --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbregister_numberdetailqrcode" CssClass="col-sm-2 control-label" runat="server" Text="เลขทะเบียนอุปกรณ์ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtregister_numberdetailmove" Text='<%# Eval("register_number") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                        </div>

                                        <asp:Label ID="lbdate_expireqrcode" CssClass="col-sm-2 control-label" runat="server" Text="วันที่หมดประกัน : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdate_expireqrcode" Text='<%# Eval("asset_code") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                        </div>

                                    </div>
                                    <%-------------- เลขทะเบียนอุปกรณ์, วันที่หมดประกัน --------------%>

                                    <%-------------- บริษัทที่ต่อประกัน --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbcompany_nameqrcode" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทที่ต่อประกัน : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtcompany_nameqrcode" Text='<%# Eval("register_number") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                        </div>


                                    </div>
                                    <%-------------- บริษัทที่ต่อประกัน--------------%>

                                    <%-------------- ยี่ห้อ, รุ่น --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbbrand_nameqrcode" CssClass="col-sm-2 control-label" runat="server" Text="ยี่ห้อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtbrand_nameqrcode" Text='<%# Eval("brand_name") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                        </div>

                                        <asp:Label ID="lbgeneration_namemove" CssClass="col-sm-2 control-label" runat="server" Text="รุ่น : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtgeneration_nameqrcode" Text='<%# Eval("generation_name") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                        </div>

                                    </div>
                                    <%-------------- ยี่ห้อ, รุ่น --------------%>

                                    <%-------------- สถานที่ติดตั้ง, อาคาร --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lblplace_nameqrcode" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ติดตั้ง : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtplace_nameqrcode" Text='<%# Eval("place_name") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                        </div>

                                        <asp:Label ID="lblroom_nameqrcode" CssClass="col-sm-2 control-label" runat="server" Text="อาคาร : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtroom_nameqrcode" Text='<%#getRoomname((string)Eval("room_name")) %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                        </div>

                                    </div>
                                    <%-------------- สถานที่ติดตั้ง, อาคาร --------------%>

                                    <%-------------- IPAddress, Searial Number --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbip_addressqrcode" CssClass="col-sm-2 control-label" runat="server" Text="IPAddress : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtip_addressqrcode" Text='<%# Eval("place_name") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                        </div>

                                        <asp:Label ID="lbserial_numberqrcode" CssClass="col-sm-2 control-label" runat="server" Text="Searial Number : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtserial_numberqrcode" Text='<%#getRoomname((string)Eval("room_name")) %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                        </div>

                                    </div>
                                    <%-------------- IPAddress, Searial Number --------------%>

                                    <%-------------- User Name, Password --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbuser_nameqrcode" CssClass="col-sm-2 control-label" runat="server" Text="User Name : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtuser_nameqrcode" Text='<%# Eval("place_name") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                        </div>

                                        <asp:Label ID="lbpass_wordqrcode" CssClass="col-sm-2 control-label" runat="server" Text="Password : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtpass_wordqrcode" Text='<%#getRoomname((string)Eval("room_name")) %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                        </div>

                                    </div>
                                    <%-------------- User Name, Password --------------%>
                                </div>


                            </div>
                        </div>
                    </div>

                </ItemTemplate>

            </asp:FormView>

        </asp:View>
        <%----------- View Show Detail QRCode END ---------------%>

        <%----------- View Report Start ---------------%>
        <asp:View ID="ViewReport" runat="server">
            <div class="col-md-12">
                <div id="showsearchreport" runat="server" visible="true">
                    <div class="panel panel-info">
                        <div class="panel-heading f-bold">Report Network Devices</div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <%-------------- สถานที่,อาคาร --------------%>
                                <div class="form-group">
                                    <asp:Label ID="lbplacesearchreport" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ติดตั้ง : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlplacesearchreport" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                    </div>

                                    <asp:Label ID="lbroomsearchreport" CssClass="col-sm-2 control-label" runat="server" Text="อาคาร : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlroomsearchreport" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="กรุณาเลือกอาคาร ...." Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>
                                <%-------------- สถานที่,อาคาร --------------%>

                                <%-------------- ประเภท,ชนิด --------------%>
                                <div class="form-group">
                                    <asp:Label ID="lbtypeidxsearchreport" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddltypeidxsearchreport" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                    </div>

                                    <asp:Label ID="lbcategoryidxsearchreport" CssClass="col-sm-2 control-label" runat="server" Text="ชนิดอุปกรณ์ : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlcategoryidxsearchreport" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="กรุณาเลือกชนิดอุปกรณ์ ...." Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>
                                <%-------------- ประเภท,ชนิด --------------%>

                                <%-------------- ยี่ห้อ,รุ่น --------------%>
                                <div class="form-group">
                                    <asp:Label ID="lbbrandsearchreport" CssClass="col-sm-2 control-label" runat="server" Text="ยี่ห้อ : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtbrandsearchreport" CssClass="form-control" placeholder="ยี่ห้อ..." runat="server"></asp:TextBox>
                                    </div>

                                    <asp:Label ID="lbgenerationsearchreport" CssClass="col-sm-2 control-label" runat="server" Text="รุ่น : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtgenerationsearchreport" CssClass="form-control" placeholder="รุ่น..." runat="server"></asp:TextBox>
                                    </div>

                                </div>
                                <%-------------- ยี่ห้อ,รุ่น --------------%>

                                <%-------------- ชื่อผู้รับผิดชอบ,บริษัทที่ต่อประกัน --------------%>
                                <div class="form-group">
                                    <asp:Label ID="lbnamesearchreport" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อผู้รับผิดชอบ : " />
                                    <div class="col-sm-3">
                                        <%-- <asp:TextBox ID="txtnamecreate" CssClass="form-control" runat="server"></asp:TextBox>--%>
                                        <asp:DropDownList ID="ddlnamesearchreport" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>

                                    <asp:Label ID="lbcompanyidxreport" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทที่ต่อประกัน : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlcompanyidxsearchreport" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>

                                </div>
                                <%-------------- ชื่อผู้รับผิดชอบ,บริษัทที่ต่อประกัน  --------------%>

                                <%-------------- วันที่เริ่ม / วันที่หมดประกัน --------------%>
                                <div class="form-group">
                                    <asp:Label ID="lbsearchbetweenreport" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่ : " />
                                    <div class="col-sm-2">
                                        <asp:DropDownList ID="ddlSearchDatereport" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            <asp:ListItem Value="0">เลือกเงื่อนไข</asp:ListItem>
                                            <asp:ListItem Value="1">มากกว่า  ></asp:ListItem>
                                            <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                            <asp:ListItem Value="3">ระหว่าง  <></asp:ListItem>
                                        </asp:DropDownList>

                                    </div>

                                    <div class="col-sm-3">
                                        <div class='input-group date from-date-datepicker'>
                                            <asp:TextBox ID="txtdatepurchasesearchreport" CssClass="form-control" runat="server" placeholder="วันที่เริ่มประกัน..."></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                            <%--  <asp:RequiredFieldValidator ID="Requiredtxtdatepurchaseadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                    ControlToValidate="txtdatepurchaseadd" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกวันที่ซื้อ"
                                                    ValidationExpression="กรุณากรอกวันที่ซื้อ" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtdatepurchaseadd" Width="160" />--%>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class='input-group date from-date-datepicker'>
                                            <asp:TextBox ID="txtdateexpirsearchreport" CssClass="form-control" runat="server" Enabled="False" placeholder="วันที่หมดประกัน..."></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                            <%--  <asp:RequiredFieldValidator ID="Requiredtxtdatepurchaseadd" ValidationGroup="Saveinsert" runat="server" Display="None"
                                                    ControlToValidate="txtdatepurchaseadd" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกวันที่ซื้อ"
                                                    ValidationExpression="กรุณากรอกวันที่ซื้อ" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtdatepurchaseadd" Width="160" />--%>
                                        </div>
                                    </div>


                                </div>

                                <%-------------- วันที่เริ่ม / วันที่หมดประกัน --------------%>

                                <%-------------- ip,serial --------------%>
                                <div class="form-group">

                                    <asp:Label ID="lbipaddresssearchreport" CssClass="col-sm-2 control-label" runat="server" Text="IP Address : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtipaddresssearchreport" CssClass="form-control" runat="server" placeholder="IP Address..."></asp:TextBox>
                                    </div>

                                    <asp:Label ID="lbserialsearchreport" CssClass="col-sm-2 control-label" runat="server" Text="Serial Number : " />
                                    <div class="col-sm-3">
                                        <small>
                                            <asp:TextBox ID="txtserialsearchreport" CssClass="form-control" placeholder="Serial Number..." runat="server"></asp:TextBox></small>
                                    </div>

                                </div>
                                <%-------------- ip,serial --------------%>

                                <%-------------- register --------------%>
                                <div class="form-group">

                                    <asp:Label ID="lbregistersearchreport" CssClass="col-sm-2 control-label" runat="server" Text="เลขทะเบียน : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtregistersearchreport" CssClass="form-control" runat="server" placeholder="เลขทะเบียนอุปกรณ์..."></asp:TextBox>
                                    </div>

                                    <%-- <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" Text="Serial Number : " />
                                <div class="col-sm-3">
                                    <small>
                                        <asp:TextBox ID="TextBox2" CssClass="form-control" placeholder="Serial Number..." runat="server"></asp:TextBox></small>
                                </div>--%>
                                </div>
                                <%-------------- register --------------%>

                                <%-------------- btnsearch --------------%>
                                <asp:UpdatePanel ID="updateexport" runat="server">
                                    <ContentTemplate>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <%--<div class="col-sm-12"></div>--%>
                                                <div class="pull-left">
                                                    <asp:LinkButton ID="btnSearchReport" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchReport" OnCommand="btnCommand" CommandName="btnSearchReport" data-toggle="tooltip" title="Search" OnClientClick="return confirm('คุณต้องการค้นหารายการนี้ใช่หรือไม่ ?')"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>


                                                    <asp:LinkButton ID="btnExport" CssClass="btn btn-primary" runat="server" OnCommand="btnCommand" CommandName="btnExport" data-toggle="tooltip" title="Cancel" Visible="false">Export Excel</asp:LinkButton>


                                                </div>

                                            </div>
                                        </div>

                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnExport" />
                                    </Triggers>

                                </asp:UpdatePanel>
                                <%-------------- btnsearch --------------%>
                            </div>

                        </div>




                    </div>

                </div>
            </div>

            <!--*** START GRIDVIEW Index ***-->
            <%-- col-md-12 m-t-10--%>
            <div class="col-md-12">

                <div id="gridviewreport" runat="server" visible="false">
                    <br />
                    <asp:GridView ID="GvReport"
                        runat="server"
                        AutoGenerateColumns="false"
                        DataKeyNames="u0_devicenetwork_idx"
                        CssClass="table table-striped table-bordered table-responsive"
                        HeaderStyle-CssClass="info"
                        AllowPaging="true"
                        PageSize="5"
                        OnRowEditing="Master_RowEditing"
                        OnRowUpdating="Master_RowUpdating"
                        OnRowCancelingEdit="Master_RowCancelingEdit"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        OnRowDataBound="Master_RowDataBound">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">No result</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" Visible="true">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbu0_idx" runat="server" Visible="false" Text='<%# Eval("u0_devicenetwork_idx") %>' />
                                        <asp:Label ID="lbunidx" runat="server" Visible="false" Text='<%# Eval("m0_node_idx") %>' />
                                        <%# (Container.DataItemIndex +1) %>
                                    </small>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เลขทะเบียนอุปกรณ์" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="RegisterNameReport" runat="server" Text='<%# Eval("register_number") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">

                                <ItemTemplate>
                                    <%-- &nbsp;&nbsp;<p><b>ชื่อผู้สร้าง :</b> <%# Eval("name_create") %></p>--%>

                                    <small>
                                        <asp:Label ID="DetailReport" runat="server">
                                        <p><b>&nbsp;&nbsp;&nbsp;สถานที่ติดตั้ง :</b> <%# Eval("place_name") %></p>                                                                             
                                        <p><b>&nbsp;&nbsp;&nbsp;อาคาร :</b> <%# Eval("room_name") %></p>
                                       
                                        <p><b>&nbsp;&nbsp;&nbsp;ประเภทอุปกรณ์ :</b> <%# Eval("type_name") %></p>

                                        <p><b>&nbsp;&nbsp;&nbsp;ชนิดอุปกรณ์ :</b> <%# Eval("category_name") %></p>

                                        <p><b>&nbsp;&nbsp;&nbsp;ยี่ห้อ :</b> <%# Eval("brand_name") %></p>

                                        <p><b>&nbsp;&nbsp;&nbsp;รุ่น :</b> <%# Eval("generation_name") %></p>

                                        <p><b>&nbsp;&nbsp;&nbsp;บริษัทประกัน :</b> <%# Eval("company_name") %></p>
                                      
                                        <p><b>&nbsp;&nbsp;&nbsp;วันที่เริ่มประกัน :</b> <%# Eval("date_purchase") %></p>
                                        
                                        <p><b>&nbsp;&nbsp;&nbsp;วันที่หมดประกัน :</b> <%# Eval("date_expire") %></p>

                                        <%-- <p><b>&nbsp;&nbsp;&nbsp;ชื่อผู้สร้าง :</b> <%# Eval("name_create") %></p>--%>
                                        </asp:Label>

                                    </small>

                                </ItemTemplate>

                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="DateCreateReport" runat="server" Text='<%# Eval("date_create") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อผู้สร้าง" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="NameCreateReport" runat="server" Text='<%# Eval("name_create") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อผู้อนุมัติ" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="NameCreateReport" runat="server" Text='<%# Eval("name_approve") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียดอุปกรณ์" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>&nbsp;&nbsp;&nbsp;<asp:Label ID="DetailNetworkReport" runat="server" Text='<%#Eval("detail_devices") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <!--*** END GRIDVIEW Index ***-->


            <asp:GridView ID="GvShowExportExcel" runat="server" AutoGenerateColumns="true">
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับที่" ItemStyle-Width="100">
                        <ItemTemplate>
                            <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>

        </asp:View>

        <%----------- View Report End ---------------%>
    </asp:MultiView>
    <%-----------END MultiView END ---------------%>
</asp:Content>
