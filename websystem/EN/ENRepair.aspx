﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="ENRepair.aspx.cs" Inherits="websystem_EN_ENRepair" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadImagesInsert").MultiFile();
            }
        })
    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadImagesComment").MultiFile();
            }
        })
    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadImagesClosejob").MultiFile();
            }
        })
    </script>


    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <!--*** START Menu ***-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav" runat="server">

                    <li id="litoindex" runat="server">
                        <asp:LinkButton ID="lblindex" runat="server"
                            CommandName="btnindex"
                            OnCommand="btnCommand" Text="ข้อมูลทั่วไป" />
                    </li>


                    <li id="litoinsert" runat="server">
                        <asp:LinkButton ID="lblinsert" runat="server"
                            CommandName="btninsert"
                            OnCommand="btnCommand" Text="แจ้งซ่อมเครื่องจักร" />
                    </li>

                    <li id="litoreport" runat="server" class="dropdown" visible="false">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">รายงาน <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li id="litoreportrepair" runat="server">
                                <asp:LinkButton ID="lblreport" runat="server"
                                    CommandName="btnreport"
                                    OnCommand="btnCommand" Text="รายงานแจ้งซ่อมเครื่องจักร" />
                            </li>
                            <li role="separator" class="divider"></li>
                            <li id="litoreportquestion" runat="server">
                                <asp:LinkButton ID="lblreportquestion" runat="server"
                                    CommandName="btnreportquestion"
                                    OnCommand="btnCommand" Text="รายงานสรุปแบบสอบถาม" />
                            </li>
                        </ul>
                    </li>


                    <li id="litoMaster" runat="server" class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">MasterData <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li id="limastertypemachine" runat="server">
                                <asp:LinkButton ID="lblmastertypemachine" runat="server"
                                    CommandName="btnmastertypemachine"
                                    OnCommand="btnCommand" Text="MasterData ประเภทเครื่องจักร" />
                            </li>
                            <li role="separator" class="divider"></li>
                            <li id="listatus" runat="server">
                                <asp:LinkButton ID="lblmasterstatus" runat="server"
                                    CommandName="btnmasterstatus"
                                    OnCommand="btnCommand" Text="MasterData สถานะรายการ" />
                            </li>
                        </ul>
                    </li>

                    <li id="liDetailSystem" runat="server" class="dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">เอกสารประกอบการใช้งาน <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li id="li2" runat="server">
                                <asp:LinkButton ID="lblmanual" runat="server"
                                    CommandName="btnmanual"
                                    OnCommand="btnCommand" Text="คู่มือการใช้งาน" />
                            </li>
                            <li role="separator" class="divider"></li>
                            <li id="li3" runat="server">
                                <asp:LinkButton ID="lblflow" runat="server"
                                    CommandName="btnflow"
                                    OnCommand="btnCommand" Text="FlowChart" />
                            </li>
                        </ul>
                    </li>

                </ul>

            </div>
        </nav>
    </div>
    <!--*** END Menu ***-->


    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">


        <%-------- Index Start----------%>
        <asp:View ID="ViewIndex" runat="server">

            <%-------------- BoxSearch Start--------------%>
            <div class="col-lg-12" runat="server" id="BoxSearch">
                <div id="BoxButtonSearchShow_search" runat="server">
                    <asp:LinkButton ID="LinkButton3" CssClass="btn btn-warning" data-toggle="tooltip" title="Search" runat="server" CommandName="BtnHideSETBoxAllSearchShow_Search" OnCommand="btnCommand"><i class="fa fa-search"></i> ShowSearch</asp:LinkButton>
                </div>

                <div id="BoxButtonSearchHide_search" runat="server">
                    <asp:LinkButton ID="LinkButton7" CssClass="btn btn-warning" data-toggle="tooltip" title="Search" runat="server" CommandName="BtnHideSETBoxAllSearchHide_Search" OnCommand="btnCommand"><i class="fa fa-search"></i> HideSearch</asp:LinkButton>
                </div>
                <br />
                <div id="SETBoxAllSearch" runat="server">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ค้นหาข้อมูล</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">

                                    <%------------ Search Start----------------%>

                                    <div id="SETBoxAllSearch2" runat="server">

                                        <div id="ViewSearchUser" runat="server">
                                            <div class="form-group">
                                                <asp:Label ID="Label13" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจากวันที่" />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlTypeSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Text="เลือกประเภทการค้นหา ...." Value="00"></asp:ListItem>
                                                        <asp:ListItem Text="วันที่แจ้งงาน" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="วันที่รับงาน" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="วันที่ปิดงาน" Value="3"></asp:ListItem>

                                                    </asp:DropDownList>
                                                </div>

                                            </div>

                                            <div id="Div3" runat="server">
                                                <div class="form-group">
                                                    <asp:Label ID="AddStart" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่ค้นหา" />
                                                    <div class="col-sm-2">
                                                        <asp:DropDownList ID="ddlSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Text="เลือกเงื่อนไข ...." Value="00"></asp:ListItem>
                                                            <asp:ListItem Text="มากกว่า >" Value="1"></asp:ListItem>
                                                            <asp:ListItem Text="น้อยกว่า <" Value="2"></asp:ListItem>
                                                            <asp:ListItem Text="ระหว่าง <>" Value="3"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div class="col-sm-3 ">

                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="AddStartdate" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="AddEndDate" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label32" CssClass="col-sm-2 control-label" runat="server" Text="ระบบ" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlsystem" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกระบบ...</asp:ListItem>
                                                    <asp:ListItem Value="11">แจ้งซ่อมวิศวะ</asp:ListItem>
                                                    <asp:ListItem Value="27">PMMS</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label55" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddllocate_search" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>

                                            <asp:Label ID="Label56" CssClass="col-sm-2 control-label" runat="server" Text="อาคาร" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlbuilding_search" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Value="0">กรุณาเลือกอาคาร....</asp:ListItem>

                                                </asp:DropDownList>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label57" CssClass="col-sm-2 control-label" runat="server" Text="ห้อง" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlroom_search" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Value="0">กรุณาเลือกห้อง...</asp:ListItem>

                                                </asp:DropDownList>

                                            </div>
                                            <asp:Label ID="Label59" CssClass="col-sm-2 control-label" runat="server" Text="ผู้ปิดงาน" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddladmin" AutoPostBack="true" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกผู้ปิดงาน...</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label58" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทเครื่องจักร" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddltype_search" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกประเภทเครื่องจักร....</asp:ListItem>

                                                </asp:DropDownList>
                                            </div>

                                            <asp:Label ID="Label60" CssClass="col-sm-2 control-label" runat="server" Text="เครื่องจักร" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlmachine_search" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0" Text="กรุณาเลือกเครื่องจักร ..." />
                                                </asp:DropDownList>
                                            </div>
                                        </div>


                                        <div id="ViewSearchReport" runat="server">

                                            <div class="form-group">
                                                <asp:Label ID="Label67" CssClass="col-sm-2 control-label" runat="server" Text="Organization" />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlSearchOrg" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>

                                                <asp:Label ID="Label66" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจากฝ่าย" />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlSearchDep" AutoPostBack="true" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">กรุณาเลือกฝ่าย....</asp:ListItem>

                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group" runat="server">
                                                <asp:Label ID="Label68" CssClass="col-sm-2 control-label" runat="server" Text="รหัสพนักงาน/ชื่อพนักงาน" />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtEmpIDX" runat="server" CssClass="form-control" placeholder="รหัสพนักงาน ....."></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label3" CssClass="col-sm-2 control-label" runat="server" Text="สถานะการดำเนินการ" />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlSearchStatus" AutoPostBack="true" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="88" Text="กรุณาเลือกสถานะดำเนินการ ..." />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group" runat="server">
                                                <asp:Label ID="Label36" CssClass="col-sm-2 control-label" runat="server" Text="รหัสเอกสาร" />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtrecode" runat="server" CssClass="form-control" placeholder="รหัสเอกสาร ....."></asp:TextBox>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <asp:LinkButton ID="btnsearch" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch" ValidationGroup="btnsearch" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                                <asp:LinkButton ID="btnRefresh" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Refresh" runat="server" CommandName="BtnBack" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                            </div>

                                        </div>
                                    </div>


                                    <%------------- Search End-----------------%>

                                    <%------------ ViewIndex Start----------------%>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <%-------------- BoxSearch End--------------%>

            <div id="gridviewindex" runat="server">
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <asp:UpdatePanel ID="upActor1Node1Files" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="GvViewENRepair"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    DataKeyNames="RPIDX"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="info"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    PageSize="10"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center;">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>

                                    <Columns>

                                        <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="7%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("RECode") %>' /></small>
                                                <asp:Literal ID="Literal5" Visible="false" runat="server" Text='<%# Eval("RPIDX") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ระบบ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="7%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lisystem" runat="server" Text='<%# Eval("SysNameTH") %>' /></small>
                                                <asp:Literal ID="litsys" Visible="false" runat="server" Text='<%# Eval("SysIDX") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="ประเภท" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="7%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="ltSys" runat="server" Text='<%# Eval("TypeNameTH") %>' /></small>
                                                <asp:Literal ID="litnode" Visible="false" runat="server" Text='<%# Eval("nodeidx") %>' /></small>
                                                <asp:Literal ID="liactor" Visible="false" runat="server" Text='<%# Eval("actoridx") %>' /></small>
                                                <asp:Literal ID="linode_status" Visible="false" runat="server" Text='<%# Eval("node_status") %>' /></small>
                                                <asp:Literal ID="DoccodeSys" runat="server" Text='<%# Eval("RECode") %>' Visible="False" /></small>
                                                 <asp:Literal ID="liquest" Visible="false" runat="server" Text='<%# Eval("RPIDXQuest") %>' /></small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="รหัสเครื่องจักร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="13%">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label106" runat="server">ชื่อเครื่องจักร: </asp:Label></strong>
                                                    <asp:Literal ID="Literal9" runat="server" Text='<%# Eval("MCNameTH") %>' />
                                                    </p>
                                                    <strong>
                                                        <asp:Label ID="Label1036" runat="server">รหัสใหม่: </asp:Label></strong>
                                                    <asp:Literal ID="Literal41" runat="server" Text='<%# Eval("MCCode") %>' />
                                                    </p>
                                                    <strong>
                                                        <asp:Label ID="Label132" runat="server">รหัสเก่า: </asp:Label></strong>
                                                    <asp:Literal ID="Literal12" runat="server" Text='<%# Eval("Abbreviation") %>' />

                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="วันเดือนปี" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="8%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="ltSystemName" runat="server" Text='<%# Eval("DateAlertJob") %>' /></small>
                                                </p>
                                        <small>( 
                                        <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("TimeAlertJob") %>' />
                                            )</small>
                                            </ItemTemplate>

                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ผู้แจ้งปัญหา" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("NameUser") %>' />
                                                    <asp:Label ID="RepairEmpIDX" runat="server" Visible="false" CssClass="col-sm-10" Text='<%# Eval("UserEmpIDX") %>'></asp:Label>
                                                    </p>
                                        
                                            
                                            <strong>
                                                <asp:Label ID="Label13" runat="server">ฝ่าย: </asp:Label></strong>
                                                    <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("UserDeptNameTH") %>' />
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ปัญหาที่แจ้ง" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="14%">
                                            <ItemTemplate>
                                                <small>

                                                    <strong>
                                                        <asp:Label ID="Label17" runat="server"> รายละเอียด: </asp:Label></strong>
                                                    <asp:Literal ID="DetailProblem" runat="server" Text='<%# Eval("CommentUser")%>' />
                                                    </p>
                                                    <strong>
                                                        <asp:Label ID="Label1037" runat="server"> สถานที่: </asp:Label></strong>
                                                    <asp:Literal ID="Literal1230" runat="server" Text='<%# Eval("LocName") %>' />
                                                    </p>
                                                      <strong>
                                                          <asp:Label ID="Label16" runat="server"> อาคาร: </asp:Label></strong>
                                                    <asp:Literal ID="Literdal6" runat="server" Text='<%# Eval("BuildingName") %>' />
                                                    </p>
                                                      <strong>
                                                          <asp:Label ID="Labedl18" runat="server"> ห้อง: </asp:Label></strong>
                                                    <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("Roomname") %>' />


                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="ผู้รับงาน" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="18%">
                                            <ItemTemplate>
                                                <small>
                                                    <strong>
                                                        <asp:Label ID="Label11" runat="server"> ผู้รับงาน: </asp:Label></strong>
                                                    <asp:Label ID="Label14" runat="server" Visible="False" Text='<%# Eval("AdminReciveEmpIDX")%>'></asp:Label>
                                                    <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("NameAdminRe") %>' />
                                                    <br />

                                                    <strong>
                                                        <asp:Label ID="Label61" runat="server"> ผู้ปิดงาน: </asp:Label></strong>
                                                    <asp:Label ID="lblAdminDoingIDX" runat="server" Visible="False" Text='<%# Eval("AdminEmpIDX")%>'></asp:Label>
                                                    <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("NameAdminClose") %>' />
                                                    <br />
                                                    <asp:Panel ID="BoxDate" runat="server">
                                                        <strong>
                                                            <asp:Label ID="lbdat" runat="server"> วันที่: </asp:Label></strong>
                                                        <asp:Literal ID="lbdaterecive" runat="server" Text='<%# Eval("DateCloseJob") %>' />
                                                        <br />
                                                        <strong>
                                                            <asp:Label ID="lbb1" runat="server"> เวลาปิดงาน: </asp:Label></strong>
                                                        <asp:Literal ID="lbtimerecive" runat="server" Text='<%# Eval("TimeCloseJob") %>' />
                                                        <br />
                                                    </asp:Panel>
                                                    <strong>
                                                        <asp:Label ID="Label521" runat="server"> หมายเหตุปิดงาน: </asp:Label></strong>
                                                    <asp:Literal ID="Literal47" runat="server" Text='<%# Eval("CommentAdmin") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="litma" runat="server" />
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="สถานะ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lblIDApprove" runat="server" Visible="false" Text='<%# Eval("STAppIDX")%>'></asp:Label>
                                                    <asp:Label ID="lblnameApprove" runat="server" Text='<%# Eval("status_name")%>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="7%" ItemStyle-CssClass="text-center">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbViewEN" runat="server" CssClass="btn btn-info btn-xs" Text="" OnCommand="btnCommand" CommandName="ViewENRepair" title="ViewDetail" CommandArgument='<%# Eval("RPIDX") +  ";" + Eval("SysIDX")%>'><i class="fa fa-file" ></i> </asp:LinkButton>
                                                </p>
                                                <asp:LinkButton ID="lbQuestionEN" runat="server" CssClass="btn btn-success btn-xs" Text="" OnCommand="btnCommand" CommandName="ViewENRepairQuestion" title="Question" CommandArgument='<%# Eval("RPIDX")%>'><i class="fa fa-user"></i> </asp:LinkButton>
                                                </p>
                                                <asp:LinkButton ID="lbViewHistory" runat="server" CssClass="btn btn-default btn-xs" Text="" OnCommand="btnCommand" CommandName="ViewHistory" title="History" CommandArgument='<%# Eval("MCIDX")%>'><i class="glyphicon glyphicon-eye-open" ></i> </asp:LinkButton>

                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                </div>
            </div>


        </asp:View>


        <asp:View ID="ViewDetailIndex" runat="server">

            <div class="col-lg-12">

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้แจ้งซ่อม</strong></h3>
                    </div>
                    <div class="panel-body">


                        <asp:FormView ID="FvDetailUserRepair" DefaultMode="ReadOnly" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <ItemTemplate>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสผู้ทำรายการ : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="LbEmpCode" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("UserEmpCode") %>' />
                                                    <asp:Label ID="LbEmpIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("UserEmpIDX")%>' />

                                                </div>

                                                <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbEmpName" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("NameUser") %>' />
                                                </div>
                                            </div>
                                        </div>


                                        <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัท : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="LbOrgNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("UserOrgNameTH")%>' />
                                                    <asp:Label ID="LbOrgIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("OrgIDX")%>' />
                                                </div>

                                                <asp:Label ID="Label34" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ฝ่าย : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbDeptNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("UserDeptNameTH")%>' />
                                                    <asp:Label ID="LbRDeptIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RDeptIDX")%>' />
                                                </div>
                                            </div>
                                        </div>


                                        <%-------------- แผนก,ตำแหน่ง --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Labedl1" class="col-xs-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label23" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("UserSecNameTH") %>' />
                                                    <asp:Label ID="LbSecIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RSecIDX")%>' />

                                                </div>

                                                <asp:Label ID="Label8" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ตำแหน่ง : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbPosNameTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("UserPosNameTH")%>' />
                                                    <asp:Label ID="LbPosIDX" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("RPosIDX")%>' />
                                                </div>
                                            </div>
                                        </div>

                                        <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label5" class="col-xs-2 control-labelnotop text_right" runat="server" Text="เบอร์ติดต่อ : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label25" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("MobileNo") %>' />
                                                </div>

                                                <asp:Label ID="Label6" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="E-mail : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="lblemail" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("Email") %>' />

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:FormView>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-wrench"></i><strong>&nbsp; ข้อมูลแจ้งซ่อม</strong></h3>
                    </div>
                    <div class="panel-body">
                        <asp:FormView ID="FvDetailRepair" DefaultMode="ReadOnly" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                            <ItemTemplate>

                                <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("nodeidx") %>' />
                                <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("actoridx") %>' />
                                <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("node_status") %>' />
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label35" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสเอกสาร : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="DocCode" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("RECode") %>' />

                                                    <asp:Label ID="lblRPIDX" runat="server" Visible="false" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("RPIDX")%>' />
                                                </div>
                                                <asp:Label ID="Label102" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ระบบงาน : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label103" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("SysNameTH") %>' />
                                                </div>
                                            </div>

                                        </div>


                                        <%-------------- วันที่แจ้งซ่อม,เวลา --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่แจ้งซ่อม : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="LbDategetJob" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("DateNotify") %>' />

                                                </div>

                                                <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลา : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="LbTimegetJob" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("TimeNotify") %>' />
                                                </div>

                                            </div>
                                        </div>

                                        <%-------------- สถานที่การแจ้งซ่อม --------------%>
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="สถานที่การแจ้งซ่อม : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="LbLockTH" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("LocName")%>' />
                                                </div>

                                                <asp:Label ID="Label28" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="อาคาร : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Lbldetailuser" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("BuildingName") %>' />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label75" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ห้อง : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label76" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("Roomname")%>' />
                                                </div>
                                                <asp:Label ID="Label88" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสเครื่องจักร : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label89" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("MCCode")%>' />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label74" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อเครื่องจักร : " />
                                                <div class="col-xs-3">
                                                    <asp:Label ID="Label79" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("MCNameTH")%>' />
                                                </div>

                                                <asp:Label ID="Label80" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รายละเอียดเพิ่มเติม : " />
                                                <div class="col-xs-4">
                                                    <asp:Label ID="Label82" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("CommentUser") %>' />
                                                </div>
                                            </div>
                                        </div>

                                        <div id="divshowdatemange" runat="server" visible="false">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <asp:Label ID="Label110" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ช่วงวันที่ดำเนินการ : " />
                                                    <div class="col-xs-3">
                                                        <asp:Label ID="Label111" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("datestart")%>' />
                                                    </div>
                                                    <asp:Label ID="Label104" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่เข้าดำเนินการ PMMS : " />
                                                    <div class="col-xs-4">
                                                        <asp:Label ID="Label105" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("DateManage")%>' />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="Label17" CssClass="col-sm-1 control-label" runat="server" />

                                            <div class="col-lg-8">
                                                <asp:GridView ID="gvFile" Visible="true" runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                                    HeaderStyle-CssClass="warning"
                                                    OnRowDataBound="Master_RowDataBound"
                                                    BorderStyle="None"
                                                    CellSpacing="2"
                                                    Font-Size="Small">

                                                    <Columns>
                                                        <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                                            <ItemTemplate>
                                                                <div class="col-lg-10">
                                                                    <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                                    <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                                </div>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>

                                            </div>
                                        </div>


                                        <div class="col-lg-12" runat="server" id="BoxAdmin">
                                            <div class="panel panel-success">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i><strong>&nbsp; ข้อมูลปิดงาน</strong></h3>
                                                </div>
                                                <div class="panel-body">
                                                    <div id="BoxAdminReceive" visible="false" runat="server">

                                                        <div class="form-group">
                                                            <div class="col-md-12">

                                                                <asp:Label ID="Label19" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ผู้รับงาน : " />
                                                                <div class="col-xs-3">
                                                                    <asp:Label ID="Label20" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("NameAdminRe")%>' />
                                                                </div>

                                                                <asp:Label ID="Label21" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ความคิดเห็นผู้รับงาน : " />
                                                                <div class="col-xs-4">
                                                                    <asp:Label ID="lblcommentadminreceive" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("CommentReciveAdmin") %>' />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-md-12">

                                                                <asp:Label ID="Label24" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่รับงาน : " />
                                                                <div class="col-xs-3">
                                                                    <asp:Label ID="Label26" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("DateReciveJob")%>' />
                                                                </div>

                                                                <asp:Label ID="Label27" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลาที่รับงาน : " />
                                                                <div class="col-xs-4">
                                                                    <asp:Label ID="Label29" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("TimeReciveJob") %>' />
                                                                </div>
                                                            </div>
                                                        </div>



                                                    </div>

                                                    <div id="BoxAdminCloseJob" visible="false" runat="server">

                                                        <div class="form-group">
                                                            <div class="col-md-12">

                                                                <asp:Label ID="Label22" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ผู้ปิดงาน : " />
                                                                <div class="col-xs-3">
                                                                    <asp:Label ID="Label33" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("NameAdminClose")%>' />
                                                                </div>

                                                                <asp:Label ID="Label38" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ความคิดเห็นผู้ปิดงาน : " />
                                                                <div class="col-xs-4">
                                                                    <asp:Label ID="lblcommentadminclosejob" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("CommentAdmin") %>' />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-md-12">

                                                                <asp:Label ID="Label40" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่ปิดงาน : " />
                                                                <div class="col-xs-3">
                                                                    <asp:Label ID="Label43" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("DateCloseJob")%>' />
                                                                </div>

                                                                <asp:Label ID="Label44" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลาที่ปิดงาน : " />
                                                                <div class="col-xs-4">
                                                                    <asp:Label ID="Label45" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("TimeCloseJob") %>' />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <asp:Label ID="Label53" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Downtime : " />
                                                                <div class="col-xs-3">
                                                                    <asp:Label ID="Label54" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("sumtime")%>' />
                                                                </div>

                                                                <%--      <asp:Label ID="Label32" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ราคา : " />
                                                                <div class="col-xs-1">
                                                                    <asp:Label ID="lblprice" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("Price")%>' />

                                                                </div>
                                                                <div class="col-xs-3">
                                                                    <asp:Label ID="Label36" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บาท " />

                                                                </div>--%>
                                                            </div>
                                                        </div>



                                                    </div>

                                                    <div id="BoxUserCloseJob" visible="false" runat="server">

                                                        <div class="form-group">
                                                            <div class="col-md-12">

                                                                <asp:Label ID="Label39" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อผู้ใช้ปิดงาน : " />
                                                                <div class="col-xs-3">
                                                                    <asp:Label ID="Label46" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("NameUser")%>' />
                                                                </div>

                                                                <asp:Label ID="Label47" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ความคิดเห็นผู้ใช้ปิดงาน : " />
                                                                <div class="col-xs-4">
                                                                    <asp:Label ID="Label48" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("CommentApprove") %>' />
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-md-12">

                                                                <asp:Label ID="Label49" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่ผู้ใช้ปิดงาน : " />
                                                                <div class="col-xs-3">
                                                                    <asp:Label ID="Label50" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("DateApprove")%>' />
                                                                </div>

                                                                <asp:Label ID="Label51" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เวลาที่ผู้ใช้ปิดงาน : " />
                                                                <div class="col-xs-4">
                                                                    <asp:Label ID="Label52" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("TimeApprove") %>' />
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </ItemTemplate>

                        </asp:FormView>


                    </div>
                </div>
            </div>

            <div id="Div_Comment" class="col-lg-12" runat="server">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; รายละเอียดคอมเม้นต์ / Upload File</strong></h3>
                    </div>
                    <div class="panel-body">

                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                            <ContentTemplate>

                                <asp:GridView ID="GvComment"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    DataKeyNames="CMIDX"
                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                    HeaderStyle-CssClass="default"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    PageSize="100"
                                    OnRowDataBound="Master_RowDataBound"
                                    OnRowEditing="Master_RowEditing"
                                    OnRowCancelingEdit="Master_RowCancelingEdit"
                                    OnRowUpdating="Master_RowUpdating"
                                    OnPageIndexChanging="Master_PageIndexChanging">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />


                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>

                                    <Columns>
                                        <asp:TemplateField HeaderText="#">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUIDX" runat="server" Visible="false" Text='<%# Eval("CMIDX") %>' />
                                                <%# (Container.DataItemIndex +1) %>
                                            </ItemTemplate>

                                            <EditItemTemplate>

                                                <div class="panel-body">
                                                    <div class="form-horizontal" role="form">

                                                        <div class="form-group">
                                                            <div class="col-sm-2">
                                                                <asp:TextBox ID="txtUIDX" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("CMIDX")%>' />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label95" Visible="true" CssClass="col-sm-3 control-label" runat="server" Text="Name" />
                                                            <div class="col-sm-5">
                                                                <asp:TextBox ID="txtName" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("FullNameTH")%>' />
                                                                <asp:TextBox ID="txtEmpIDX" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("UserEmpIDX")%>' />


                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save_comment" runat="server" Display="None"
                                                                ControlToValidate="txtEmpIDX" Font-Size="11"
                                                                ErrorMessage="Please check Name"
                                                                ValidationExpression="Please check Name" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="lbNameTH" CssClass="col-sm-3 control-label" runat="server" Text="Comment" />
                                                            <div class="col-sm-5">
                                                                <asp:TextBox ID="txtNameTypecodeEdit" runat="server" CssClass="form-control" Text='<%# Eval("CommentAuto")%>' />
                                                            </div>
                                                            <asp:RequiredFieldValidator ID="RqNameTH" ValidationGroup="Save_comment" runat="server" Display="None"
                                                                ControlToValidate="txtNameTypecodeEdit" Font-Size="11"
                                                                ErrorMessage="Please check NameTypecode"
                                                                ValidationExpression="Please check NameTypecode" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqNameTH" Width="160" />
                                                        </div>


                                                        <div class="form-group">
                                                            <div class="col-sm-4 col-sm-offset-8">
                                                                <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_comment" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </EditItemTemplate>

                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Comment" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="Labelw41" runat="server" CssClass="col-sm-10" Text='<%# Eval("FullNameTH") %>'></asp:Label>
                                                <asp:Label ID="lblIDXCMSAP" runat="server" Visible="false" CssClass="col-sm-10" Text='<%# Eval("UserEmpIDX") %>'></asp:Label>
                                                <br />
                                                <asp:Label ID="lblTypeCode" runat="server" CssClass="col-sm-10" Text='<%# Eval("CommentAuto") %>'></asp:Label>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Date/Time" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="Labelddw41" runat="server" CssClass="col-sm-10" Text='<%# Eval("CDate") %>'></asp:Label>
                                                <asp:Label ID="Label41" runat="server" CssClass="col-sm-10" Text='<%# Eval("CTime") %>'></asp:Label>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Management">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            </ItemTemplate>

                                            <EditItemTemplate />
                                            <FooterTemplate />
                                        </asp:TemplateField>



                                    </Columns>

                                </asp:GridView>

                            </ContentTemplate>
                        </asp:UpdatePanel>


                        <div class="panel-heading">
                            <h4 class="panel-title"><i class="glyphicon glyphicon-comment"></i><strong>&nbsp; เพิ่มรายการคอมเม้นต์ / Upload File</strong></h4>

                            <div class="form-horizontal" role="form">


                                <div class="panel-heading">

                                    <div class="form-group">
                                        <asp:Label ID="Label30" runat="server" Text="เพิ่ม Comment :" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtcomment" TextMode="multiline" Width="510pt" Rows="5" runat="server" CssClass="form-control" placeholder="Comment ..................." />

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator11" ValidationGroup="Save_comment" runat="server" Display="None" ControlToValidate="txtcomment" Font-Size="11"
                                                ErrorMessage="กรุณากรอกความคิดเห็น"
                                                ValidationExpression="กรุณากรอกความคิดเห็น"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server"
                                                ValidationGroup="Save_comment" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtcomment"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator6" Width="160" />

                                        </div>
                                    </div>


                                    <asp:UpdatePanel ID="Update_Comment" runat="server">
                                        <ContentTemplate>

                                            <%-------------- Upload File --------------%>

                                            <div class="form-group">

                                                <asp:Label ID="Label101" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                                <div class="col-sm-7">

                                                    <asp:FileUpload ID="UploadImagesComment" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-warning btn-sm multi" accept="gif|jpg|pdf|png|pdf" />
                                                    <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png|pdf</font></p>

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btn_savecomment" />

                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-9">
                                            <asp:LinkButton ID="btn_savecomment" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_comment" OnCommand="btnCommand" CommandName="CmdSaveComment" data-toggle="tooltip" title="Save" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnCancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="BtnBack" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <asp:Panel ID="panel_admin_getjob" runat="server">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-cog"></i><strong>&nbsp; ส่วนของเจ้าหน้าที่</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="col-lg-12">

                                    <div class="form-group">
                                        <asp:Label ID="Label37" CssClass="col-sm-2 control-label text_right" runat="server" Text="สถานะรายการ : " />
                                        <div class="col-sm-3">

                                            <asp:DropDownList ID="ddl_admin_getjob" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="10" Text="รับงาน"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="Save" runat="server" Display="None"
                                                ControlToValidate="ddl_admin_getjob" Font-Size="11"
                                                ErrorMessage="เลือกสถานะอนุมัติ"
                                                ValidationExpression="เลือกสถานะอนุมัติ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="CommentAdminSap1" CssClass="col-sm-2 control-label" runat="server" Text="ความเห็นผู้รับงาน :" />

                                        <div class="col-sm-4">
                                            <asp:TextBox CssClass="txtform form-control" ID="txtcommentreceive" runat="server" TextMode="MultiLine" Width="510pt" Rows="5"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="Save_Accept" runat="server" Display="None" ControlToValidate="txtcommentreceive" Font-Size="11"
                                                ErrorMessage="กรุณากรอกความคิดเห็น"
                                                ValidationExpression="กรุณากรอกความคิดเห็น"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                                                ValidationGroup="Save_Accept" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtcommentreceive"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator5" Width="160" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-sm-3 col-sm-offset-9">

                                            <asp:LinkButton ID="btnadmingetjob" ValidationGroup="Save_Accept" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdAdminGetJob" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-gavel"></i>บันทึก</asp:LinkButton>

                                            <asp:LinkButton ID="btncancelgetjobsap" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="BtnBack" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>


            <div class="col-lg-12">
                <asp:Panel ID="panel_linkpmms" runat="server">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-share"></i><strong>&nbsp; ข้อมูลเพิ่มเติม</strong></h3>
                        </div>
                        <asp:UpdatePanel ID="update1" runat="server">
                            <ContentTemplate>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="col-lg-12">

                                            <asp:LinkButton ID="btnlink" runat="server" OnCommand="btnCommand" CommandName="btnlink" CssClass="btn btn-success btn-sm" Text="กรอกผลระบบ PMMS"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnlink" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                </asp:Panel>
            </div>


            <div class="col-lg-12">
                <asp:Panel ID="panel_admin_closejob" runat="server">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-cog"></i><strong>&nbsp; ส่วนของเจ้าหน้าที่</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="col-lg-12">
                                    <asp:UpdatePanel ID="update_test" runat="server" UpdateMode="Always">
                                        <ContentTemplate>
                                            <div class="form-group" id="BoxChosseCloseJob" runat="server">
                                                <asp:Label ID="Label15" CssClass="col-sm-2 control-label text_right" runat="server" Text="ขั้นตอนดำเนินการ : " />
                                                <div class="col-sm-3">

                                                    <asp:DropDownList ID="ddl_admin_closejob" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0" Text="เลือกสถานะดำเนินการ..."></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="รออะไหล่"></asp:ListItem>
                                                        <asp:ListItem Value="5" Text="ส่งซ่อม"></asp:ListItem>
                                                        <asp:ListItem Value="6" Text="รอซัพภายนอก"></asp:ListItem>
                                                        <asp:ListItem Value="7" Text="ดำเนินการเสร็จสิ้น"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="Save_Accept" runat="server" Display="None"
                                                        ControlToValidate="ddl_admin_closejob" Font-Size="11"
                                                        ErrorMessage="เลือกสถานะอนุมัติ"
                                                        ValidationExpression="เลือกสถานะอนุมัติ" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddl_admin_closejob" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                    <div id="AdminAcceptCloseJob" runat="server" visible="false">
                                        <asp:FormView ID="FvCloseJob" DefaultMode="Insert" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                            <InsertItemTemplate>


                                                <div class="form-group" runat="server" visible="false">

                                                    <asp:Label ID="Label42" CssClass="col-sm-2 control-label" runat="server" Text="ค่าใช้จ่าย : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtprice" CssClass="form-control" runat="server"> </asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="Save_Accept" runat="server" Display="None"
                                                            ControlToValidate="txtprice" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกจำนวน" />
                                                        <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="Save_Accept" Display="None"
                                                            ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                            ControlToValidate="txtprice"
                                                            ValidationExpression="^[0-9]{1,10}$" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />


                                                    </div>

                                                </div>



                                                <div class="form-group">
                                                    <asp:Label ID="CommentAdminSap1" CssClass="col-sm-2 control-label" runat="server" Text="ความเห็นผู้ดูแล :" />

                                                    <div class="col-sm-3">
                                                        <asp:TextBox CssClass="txtform form-control" ID="CommentAdmin" runat="server" TextMode="MultiLine" Width="510pt" Rows="5"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="Save_Accept" runat="server" Display="None" ControlToValidate="CommentAdmin" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกความคิดเห็น"
                                                            ValidationExpression="กรุณากรอกความคิดเห็น"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                            ValidationGroup="Save_Accept" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="CommentAdmin"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                                    </div>
                                                </div>




                                                <div class="form-group">

                                                    <asp:Label ID="Label1051" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                                    <div class="col-sm-7">

                                                        <asp:FileUpload ID="UploadImagesClosejob" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-warning btn-sm multi" accept="gif|jpg|pdf|png|pdf" />
                                                        <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png|pdf</font></p>

                                                    </div>
                                                </div>

                                            </InsertItemTemplate>
                                        </asp:FormView>


                                    </div>

                                    <asp:UpdatePanel ID="Update_Closejob" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-3 col-sm-offset-9">

                                                    <asp:LinkButton ID="btnAdminCloseJob" ValidationGroup="Save_Accept" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdAdminCloseJob" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-gavel"></i>บันทึก</asp:LinkButton>

                                                    <asp:LinkButton ID="LinkButton4" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="BtnBack" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdminCloseJob" />

                                        </Triggers>
                                    </asp:UpdatePanel>


                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
            </div>

            <div class="col-lg-12">
                <asp:Panel ID="panel_user_closejob" runat="server" Visible="false">
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-cog"></i><strong>&nbsp; ส่วนของผู้ใช้งาน</strong></h3>
                        </div>

                        <div class="panel-body">

                            <asp:FormView ID="FvApproveUser" DefaultMode="Insert" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="form-group">
                                                <asp:Label ID="Label31" CssClass="col-sm-2" runat="server" Text="" />
                                                <div class="col-sm-3">
                                                    <asp:CheckBox ID="chkapprove" runat="server" Text=" ทำความสะอาดเรียบร้อย" />
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="CommentAdminSap1" CssClass="col-sm-2 control-label" runat="server" Text="ความเห็นผู้แจ้งซ่อม :" />

                                                <div class="col-sm-3">
                                                    <asp:TextBox CssClass="txtform form-control" ID="CommentUser" runat="server" TextMode="MultiLine" Width="510pt" Rows="5"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldVali33dator12" ValidationGroup="Save_Accept" runat="server" Display="None" ControlToValidate="CommentUser" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกความคิดเห็น"
                                                        ValidationExpression="กรุณากรอกความคิดเห็น"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldVali33dator12" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValwwidator3" runat="server"
                                                        ValidationGroup="Save_Accept" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="CommentUser"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValwwidator3" Width="160" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:FormView>

                            <div class="form-group">
                                <div class="col-sm-3 col-sm-offset-9">

                                    <asp:LinkButton ID="LinkButton5" ValidationGroup="Save_Accept" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdUserCloseJob" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-gavel"></i>บันทึก</asp:LinkButton>

                                    <asp:LinkButton ID="LinkButton6" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="BtnBack" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                </div>
                            </div>

                        </div>
                    </div>

                </asp:Panel>
            </div>



        </asp:View>

        <asp:View ID="ViewInsert" runat="server">

            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้แจ้งซ่อม</strong></h3>
                    </div>

                    <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>

                </div>
            </div>


            <div class="col-lg-12">

                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-wrench"></i><strong>&nbsp; ระบบแจ้งซ่อมเครื่องจักร</strong></h3>
                    </div>


                    <asp:FormView ID="FvInsertRepair" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group" runat="server">

                                        <asp:Label ID="Label98" class="col-sm-2 control-label" runat="server" Text="ประเภทรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsystem" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="11">แจ้งซ่อมวิศวะ</asp:ListItem>
                                                <asp:ListItem Value="27">PMMS</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>



                                    <div class="form-group" runat="server">

                                        <asp:Label ID="Label12" class="col-sm-2 control-label" runat="server" Text="ประเภทเครื่องจักร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltype" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกประเภทเครื่องจักร...</asp:ListItem>
                                            </asp:DropDownList>


                                            <asp:RequiredFieldValidator ID="RequiredFieldValidddator8" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddltype" Font-Size="11"
                                                ErrorMessage="เลือกประเภทเครื่องจักร"
                                                ValidationExpression="เลือกประเภทเครื่องจักร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidddator8" Width="160" />
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="สถานที่แจ้งซ่อม : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddllocate" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกสถานที่แจ้งซ่อม...</asp:ListItem>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddllocate" Font-Size="11"
                                                ErrorMessage="เลือกสถานที่แจ้งซ่อม"
                                                ValidationExpression="เลือกสถานที่แจ้งซ่อม" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />

                                        </div>

                                    </div>



                                    <div class="form-group" runat="server">

                                        <asp:Label ID="Label1" class="col-sm-2 control-label" runat="server" Text="อาคาร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlbuilding" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกอาคาร...</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlbuilding" Font-Size="11"
                                                ErrorMessage="เลือกอาคาร"
                                                ValidationExpression="เลือกอาคาร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                        </div>
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ห้อง : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlroom" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">เลือกห้อง...</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group" runat="server" id="divtypecode_planning" visible="false">


                                        <asp:Label ID="Label18" class="col-sm-2 control-label" runat="server" Text="รหัสกลุ่มเครื่องจักร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypecode" ValidationGroup="SaveAdd" class="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกรหัสกลุ่มเครื่องจักร..."></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddltypecode" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกรหัสกลุ่มเครื่องจักร"
                                                ValidationExpression="กรุณาเลือกรหัสกลุ่มเครื่องจักร" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator10" Width="160" />

                                        </div>



                                        <asp:Label ID="Label109" class="col-sm-3 control-label" runat="server" Text="รหัสกลุ่ม : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlgroupmachine" ValidationGroup="SaveAdd" class="form-control" runat="server">
                                                <asp:ListItem Value="0" Text="กรุณาเลือกรหัสกลุ่ม..."></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlgroupmachine" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกรหัสกลุ่ม"
                                                ValidationExpression="กรุณาเลือกรหัสกลุ่ม" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                        </div>


                                    </div>


                                    <div class="form-group" runat="server" id="divenrepair_machine">
                                        <asp:Label ID="Label3" class="col-sm-2 control-label" runat="server" Text="ชื่อเครื่องจักร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlmachine" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือกชื่ออุปกรณ์...</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddlmachine" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกชื่ออุปกรณ์"
                                                ValidationExpression="กรุณาเลือกชื่ออุปกรณ์" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />
                                        </div>
                                    </div>


                                    <div runat="server" id="divpmms_add" visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label108" class="col-sm-2 control-label" runat="server" Text="ความถี่ในการดำเนินการ : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlplan" AutoPostBack="true" ValidationGroup="SaveAdd" OnSelectedIndexChanged="ddlSelectedIndexChanged" class="form-control" runat="server">
                                                    <asp:ListItem Value="0" Text="เลือกความถี่ในการดำเนินการ..."></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator22" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="ddlplan" Font-Size="11"
                                                    ErrorMessage="เลือกความถี่ในการดำเนินการ"
                                                    ValidationExpression="เลือกความถี่ในการดำเนินการ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender28" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />
                                            </div>

                                            <asp:Label ID="Label107" class="col-sm-3 control-label" runat="server" Text="ช่วงเวลาที่ต้องการ PM : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddldate" AutoPostBack="true" ValidationGroup="SaveAdd" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกช่วงวันที่ดำเนินการ...</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator21" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="ddldate" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกช่วงวันที่ดำเนินการ"
                                                    ValidationExpression="กรุณาเลือกช่วงวันที่ดำเนินการ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender26" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator21" Width="160" />


                                            </div>

                                        </div>

                                        <div class="form-group">

                                            <asp:Label ID="Label100" class="col-sm-2 control-label" runat="server" Text="วันที่เข้าดำเนินการ : " />
                                            <div class="col-sm-3">
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="AddStartdate" runat="server" ValidationGroup="SaveAdd" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator20" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                        ControlToValidate="AddStartdate" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                        ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender27" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator20" Width="160" />
                                                </div>

                                            </div>

                                            <asp:Label ID="Label112" class="col-sm-3 control-label" runat="server" Text="ชื่อเครื่องจักร : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlmachine_pm" runat="server" ValidationGroup="SaveAdd" CssClass="form-control">
                                                    <asp:ListItem Value="0">เลือกชื่ออุปกรณ์...</asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator23" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="ddlmachine_pm" Font-Size="11"
                                                    ErrorMessage="เลือกชื่ออุปกรณ์"
                                                    ValidationExpression="เลือกชื่ออุปกรณ์" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender29" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator23" Width="160" />
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group">

                                        <asp:Label ID="Labedle1" class="col-sm-2 control-label" runat="server" Text="หมายเหตุการแจ้ง : " />
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtremark" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtremark" Font-Size="11"
                                                ErrorMessage="กรุณากรอกความคิดเห็น"
                                                ValidationExpression="กรุณากรอกความคิดเห็น"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                ValidationGroup="SaveAdd" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />
                                        </div>


                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>

                                            <%-------------- Upload File --------------%>

                                            <div class="form-group">

                                                <asp:Label ID="Label10" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                                <div class="col-sm-7">
                                                    <asp:FileUpload ID="UploadImagesInsert" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-primary btn-sm multi" accept="gif|jpg|pdf|png|pdf" />
                                                    <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png</font></p>

                                                </div>
                                            </div>


                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnAdddata" />
                                        </Triggers>
                                    </asp:UpdatePanel>


                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-10">
                                            <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>

                                    <%-- <div class="form-group" runat="server" visible="true">
                                        <div class="col-lg-offset-8 col-sm-4 control-labelnotop1">
                                            <h4><asp:Label ID="lbliso" runat="server" CssClass="text-size-14" Text="FM-EN-MP-001/7 Rev.03 (วันบังคับใช้ 1/08/2562)" /></h4>
                                        </div>
                                    </div>--%>

                                    <div class="col-md-12">
                                        <div class="form-group pull-right">
                                            <h6 class="panel-title" style="font-size:10pt">FM-EN-MP-001/7 Rev.03 (วันบังคับใช้ 1/08/2562)</h6>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>

            </div>

        </asp:View>

        <asp:View ID="ViewHistory" runat="server">


            <div class="panel-body">
                <asp:LinkButton ID="btnbackindex" runat="server" OnCommand="btnCommand" CssClass="btn btn-xs btn-danger" CommandName="btnindex"><i class="glyphicon glyphicon-arrow-left" ></i></asp:LinkButton>
            </div>

            <div class="panel-heading">
                <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ประวัติการแจ้งซ่อม</strong></h3>
            </div>

            <div class="panel-body">
                <div class="form-horizontal" role="form">

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="GvHistory"
                                runat="server"
                                AutoGenerateColumns="false"
                                DataKeyNames="RPIDX"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="info"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                PageSize="10"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center;">Data Cannot Be Found</div>
                                </EmptyDataTemplate>

                                <Columns>

                                    <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="7%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("RECode") %>' /></small>
                                            <asp:Literal ID="Literal5" Visible="false" runat="server" Text='<%# Eval("RPIDX") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="ประเภท" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="7%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="ltSys" runat="server" Text='<%# Eval("TypeNameTH") %>' /></small>
                                            <asp:Literal ID="DoccodeSys" runat="server" Text='<%# Eval("RECode") %>' Visible="False" /></small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รหัสเครื่องจักร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="13%">
                                        <ItemTemplate>
                                            <small>
                                                <strong>
                                                    <asp:Label ID="Label106" runat="server">ชื่อเครื่องจักร: </asp:Label></strong>
                                                <asp:Literal ID="Literal9" runat="server" Text='<%# Eval("MCNameTH") %>' />
                                                </p>
                                                    <strong>
                                                        <asp:Label ID="Label1036" runat="server">รหัสใหม่: </asp:Label></strong>
                                                <asp:Literal ID="Literal41" runat="server" Text='<%# Eval("MCCode") %>' />
                                                </p>
                                                    <strong>
                                                        <asp:Label ID="Label132" runat="server">รหัสเก่า: </asp:Label></strong>
                                                <asp:Literal ID="Literal12" runat="server" Text='<%# Eval("Abbreviation") %>' />

                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="วันเดือนปี" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="8%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="ltSystemName" runat="server" Text='<%# Eval("DateAlertJob") %>' /></small>
                                            </p>
                                        <small>( 
                                        <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("TimeAlertJob") %>' />
                                            )</small>
                                        </ItemTemplate>

                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="ผู้แจ้งปัญหา" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("NameUser") %>' />
                                                <asp:Label ID="RepairEmpIDX" runat="server" Visible="false" CssClass="col-sm-10" Text='<%# Eval("UserEmpIDX") %>'></asp:Label>
                                                </p>
                                        
                                            
                                            <strong>
                                                <asp:Label ID="Label13" runat="server">ฝ่าย: </asp:Label></strong>
                                                <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("UserDeptNameTH") %>' />
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="ปัญหาที่แจ้ง" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="14%">
                                        <ItemTemplate>
                                            <small>

                                                <strong>
                                                    <asp:Label ID="Label17" runat="server"> รายละเอียด: </asp:Label></strong>
                                                <asp:Literal ID="DetailProblem" runat="server" Text='<%# Eval("CommentUser")%>' />
                                                </p>
                                                    <strong>
                                                        <asp:Label ID="Label1037" runat="server"> สถานที่: </asp:Label></strong>
                                                <asp:Literal ID="Literal1230" runat="server" Text='<%# Eval("LocName") %>' />
                                                </p>
                                                      <strong>
                                                          <asp:Label ID="Label16" runat="server"> อาคาร: </asp:Label></strong>
                                                <asp:Literal ID="Literafl6" runat="server" Text='<%# Eval("BuildingName") %>' />
                                                </p>
                                                      <strong>
                                                          <asp:Label ID="Labedl18" runat="server"> ห้อง: </asp:Label></strong>
                                                <asp:Literal ID="Literaln7" runat="server" Text='<%# Eval("Roomname") %>' />


                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="ผู้รับงาน" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="18%">
                                        <ItemTemplate>
                                            <small>
                                                <strong>
                                                    <asp:Label ID="Label11" runat="server"> ผู้รับงาน: </asp:Label></strong>
                                                <asp:Label ID="Label14" runat="server" Visible="False" Text='<%# Eval("AdminReciveEmpIDX")%>'></asp:Label>
                                                <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("NameAdminRe") %>' />
                                                <br />

                                                <strong>
                                                    <asp:Label ID="Label61" runat="server"> ผู้ปิดงาน: </asp:Label></strong>
                                                <asp:Label ID="lblAdminDoingIDX" runat="server" Visible="False" Text='<%# Eval("AdminEmpIDX")%>'></asp:Label>
                                                <asp:Literal ID="Literal8" runat="server" Text='<%# Eval("NameAdminClose") %>' />
                                                <br />
                                                <asp:Panel ID="BoxDate" runat="server">
                                                    <strong>
                                                        <asp:Label ID="lbdat" runat="server"> วันที่: </asp:Label></strong>
                                                    <asp:Literal ID="lbdaterecive" runat="server" Text='<%# Eval("DateCloseJob") %>' />
                                                    <br />
                                                    <strong>
                                                        <asp:Label ID="lbb1" runat="server"> เวลาปิดงาน: </asp:Label></strong>
                                                    <asp:Literal ID="lbtimerecive" runat="server" Text='<%# Eval("TimeCloseJob") %>' />
                                                    <br />
                                                </asp:Panel>
                                                <strong>
                                                    <asp:Label ID="Label521" runat="server"> หมายเหตุปิดงาน: </asp:Label></strong>
                                                <asp:Literal ID="Literal47" runat="server" Text='<%# Eval("CommentAdmin") %>' />
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="สถานะ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblIDApprove" runat="server" Visible="false" Text='<%# Eval("STAppIDX")%>'></asp:Label>
                                                <asp:Label ID="lblnameApprove" runat="server" Text='<%# Eval("status_name")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="7%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lbViewEN" runat="server" CssClass="btn btn-info btn-xs" Text="" OnCommand="btnCommand" CommandName="ViewENRepair" title="ViewDetail" CommandArgument='<%# Eval("RPIDX")%>'><i class="fa fa-file-text-o" ></i> </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
            </div>


        </asp:View>

        <asp:View ID="ViewReport" runat="server">
            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>
                    <div class="col-lg-12">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; ค้นหารายงาน</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <asp:Label ID="Label83" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทรายงาน" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypereport" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Text="เลือกประเภทรายงาน ...." Value="0"></asp:ListItem>
                                                <asp:ListItem Text="รายงานรูปแบบตาราง" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="รายงานรูปแบบกราฟ" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="รายงานแบบฟอร์มใบบันทึกรายงานประจำวัน" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator71" ValidationGroup="SaveReport" runat="server" Display="None"
                                                ControlToValidate="ddltypereport" Font-Size="11"
                                                ErrorMessage="เลือกประเภทรายงาน"
                                                ValidationExpression="เลือกประเภทรายงาน" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator71" Width="160" />

                                        </div>
                                    </div>

                                    <asp:Panel ID="Panel_Table" runat="server" Visible="false">

                                        <div class="form-group">
                                            <asp:Label ID="Label98" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทการแจ้งซ่อม" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSysIDX" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Text="เลือกประเภทการค้นหา ...." Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="แจ้งซ่อม" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="PM" Value="27"></asp:ListItem>

                                                </asp:DropDownList>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label62" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจากวันที่" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlTypeSearchDate_Report" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Text="เลือกประเภทการค้นหา ...." Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="วันที่แจ้งงาน" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="วันที่รับงาน" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="วันที่ปิดงาน" Value="3"></asp:ListItem>

                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator14" ValidationGroup="SaveReport" runat="server" Display="None"
                                                    ControlToValidate="ddlTypeSearchDate_Report" Font-Size="11"
                                                    ErrorMessage="เลือกประเภทการค้นหา"
                                                    ValidationExpression="เลือกประเภทการค้นหา" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator14" Width="160" />

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label63" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่ค้นหา" />
                                            <div class="col-sm-2">
                                                <asp:DropDownList ID="ddlSearchDate_Report" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Text="เลือกเงื่อนไข ...." Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="มากกว่า >" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="น้อยกว่า <" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="ระหว่าง <>" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3 ">

                                                <div class='input-group date'>
                                                    <asp:TextBox ID="AddStartDate_Report" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator18" ValidationGroup="SaveReport" runat="server" Display="None"
                                                        ControlToValidate="AddStartDate_Report" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                        ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator18" Width="160" />

                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="AddEndDate_Report" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label64" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddllocate_report" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>

                                            <asp:Label ID="Label65" CssClass="col-sm-2 control-label" runat="server" Text="อาคาร" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlbuilding_report" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Value="0">กรุณาเลือกอาคาร....</asp:ListItem>

                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label69" CssClass="col-sm-2 control-label" runat="server" Text="ห้อง" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlroom_report" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Value="0">กรุณาเลือกห้อง...</asp:ListItem>

                                                </asp:DropDownList>

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label71" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทเครื่องจักร" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddltype_report" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกประเภทเครื่องจักร....</asp:ListItem>

                                                </asp:DropDownList>
                                            </div>

                                            <asp:Label ID="Label72" CssClass="col-sm-2 control-label" runat="server" Text="เครื่องจักร" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlmachine_report" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0" Text="กรุณาเลือกเครื่องจักร ..." />
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label73" CssClass="col-sm-2 control-label" runat="server" Text="Organization" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlReportOrg" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>

                                            <asp:Label ID="Label77" CssClass="col-sm-2 control-label" runat="server" Text="ค้นหาจากฝ่าย" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlReporthDep" AutoPostBack="true" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกฝ่าย....</asp:ListItem>

                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group" runat="server">
                                            <asp:Label ID="Label78" CssClass="col-sm-2 control-label" runat="server" Text="รหัสพนักงาน/ชื่อพนักงาน" />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtempname" runat="server" CssClass="form-control" placeholder="รหัสพนักงาน ....."></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label81" CssClass="col-sm-2 control-label" runat="server" Text="สถานะการดำเนินการ" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlReportStatus" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0" Text="กรุณาเลือกสถานะดำเนินการ ..." />
                                                </asp:DropDownList>
                                            </div>
                                        </div>


                                        <div class="form-group" runat="server">
                                            <asp:Label ID="Label84" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อเจ้าหน้าที่รับงาน" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddladminreceive_report" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0" Text="กรุณาเลือกสถานะดำเนินการ ..." />
                                                </asp:DropDownList>
                                            </div>

                                            <asp:Label ID="Label85" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อเจ้าหน้าที่ปิดงาน" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddladminaccept_report" AutoPostBack="true" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">กรุณาเลือกผู้ปิดงาน...</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <asp:LinkButton ID="btnsearch_report" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch_report" ValidationGroup="btnsearch_report" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton9" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="Refresh" runat="server" CommandName="BtnBack_report" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                                <asp:LinkButton ID="btnexport" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Export" runat="server" CommandName="CmdExport" ValidationGroup="btnsearch_report" OnCommand="btnCommand"><i class="fa fa-download"></i> </asp:LinkButton>

                                            </div>


                                        </div>

                                    </asp:Panel>

                                    <asp:Panel ID="Panel_Chart" runat="server" Visible="false">

                                        <div class="form-group">
                                            <asp:Label ID="Label100" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทการแจ้งซ่อม" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlSysIDX_chart" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Text="เลือกประเภทการค้นหา ...." Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="แจ้งซ่อม" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="PM" Value="27"></asp:ListItem>

                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredddlSysIDX_chart" ValidationGroup="SaveReport" runat="server" Display="None"
                                                    ControlToValidate="ddlSysIDX_chart" Font-Size="11"
                                                    ErrorMessage="เลือกประเภทการแจ้งซ่อม"
                                                    ValidationExpression="เลือกประเภทการแจ้งซ่อม" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender26" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlSysIDX_chart" Width="160" />

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label97" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทกราฟ" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddltypechart" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Text="เลือกประเภทกราฟ ...." Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="กราฟแสดงแจ้งซ่อมต่ออาคาร รายเดือน" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="กราฟแสดงแจ้งซ่อมเครื่องจักรต่ออาคาร" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="กราฟแสดงผลรวมรายการแจ้งซ่อมของทุกอาคาร" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="กราฟแสดงผลรวมรายการแจ้งซ่อมต่ออาคารรายปี" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="กราฟสรุปยอดรวม Downtime รายการแจ้งซ่อมของแต่ละอาคาร" Value="5"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ValidationGroup="SaveReport" runat="server" Display="None"
                                                    ControlToValidate="ddltypechart" Font-Size="11"
                                                    ErrorMessage="เลือกประเภทกราฟ"
                                                    ValidationExpression="เลือกประเภทกราฟ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="160" />


                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Labesl98" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddllocate_chart" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Text="เลือกสถานที่ ...." Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ValidationGroup="SaveReport" runat="server" Display="None"
                                                    ControlToValidate="ddllocate_chart" Font-Size="11"
                                                    ErrorMessage="เลือกสถานที่"
                                                    ValidationExpression="เลือกสถานที่" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator16" Width="160" />

                                            </div>
                                            <div id="divbuild" runat="server">
                                                <asp:Label ID="lblbuild" CssClass="col-sm-2 control-label" runat="server" Text="อาคาร" />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlbuilding_chart" CssClass="form-control" runat="server">
                                                        <asp:ListItem Text="เลือกอาคาร ...." Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator17" ValidationGroup="SaveReport" runat="server" Display="None"
                                                        ControlToValidate="ddlbuilding_chart" Font-Size="11"
                                                        ErrorMessage="เลือกอาคาร"
                                                        ValidationExpression="เลือกอาคาร" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator17" Width="160" />

                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" id="div_year" runat="server">
                                            <asp:Label ID="Labesal98" CssClass="col-sm-2 control-label" runat="server" Text="เดือน" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlmonth" CssClass="form-control" runat="server">
                                                    <asp:ListItem Text="เลือกเดือน ...." Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="มกราคม" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="กุมภาพันธ์" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="มีนาคม" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="เมษายน" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="พฤษภาคม" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="มิถุนายน" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="กรกฎาคม" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="สิงหาคา" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="กันยายน" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="ตุลาคา" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="พฤศจิกายน" Value="11"></asp:ListItem>
                                                    <asp:ListItem Text="ธันวาคม" Value="12"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <asp:Label ID="Labela99" CssClass="col-sm-2 control-label" runat="server" Text="ปี" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlyear" CssClass="form-control" runat="server">
                                                    <asp:ListItem Text="เลือกปี ...." Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group" id="div_lenght" runat="server">
                                            <asp:Label ID="Label99" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่ค้นหา" />
                                            <div class="col-sm-2">
                                                <asp:DropDownList ID="ddlSearchDate_Chart" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Text="เลือกเงื่อนไข ...." Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="มากกว่า >" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="น้อยกว่า <" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="ระหว่าง <>" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3 ">

                                                <div class='input-group date'>
                                                    <asp:TextBox ID="AddStartdate_Chart" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ValidationGroup="SaveReport" runat="server" Display="None"
                                                        ControlToValidate="AddStartdate_Chart" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                        ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender25" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator19" Width="160" />

                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="AddEndDate_Chart" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <%--<div id="div_machine" runat="server" visible="false">
                                            <asp:Label ID="Label98" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อเครื่องจักร" />
                                            <div class="col-sm-10">
                                                <asp:CheckBoxList ID="chkamachine"
                                                    runat="server"
                                                    CellPadding="5"
                                                    CellSpacing="5"
                                                    RepeatColumns="5"
                                                    RepeatDirection="Vertical"
                                                    RepeatLayout="Table"
                                                    TextAlign="Right"
                                                    Width="100%">
                                                </asp:CheckBoxList>
                                            </div>

                                        </div>--%>

                                        <div class="form-group">
                                            <div class="col-sm-5 col-sm-offset-1">
                                                <asp:LinkButton ID="btnsearch_chart" ValidationGroup="SaveReport" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch_chart" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton11" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Refresh" runat="server" CommandName="BtnBack_report" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                            </div>

                                        </div>

                                    </asp:Panel>

                                    <asp:Panel ID="Panel_from" runat="server" Visible="false">


                                        <div class="form-group">
                                            <asp:Label ID="Label116" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddllocate_from" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                            <asp:RequiredFieldValidator ID="Requiredddllocate_from" ValidationGroup="Search_from" runat="server" Display="None"
                                                ControlToValidate="ddllocate_from" Font-Size="11"
                                                ErrorMessage="เลือกสถานที่"
                                                ValidationExpression="เลือกสถานที่" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender33" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddllocate_from" Width="160" />

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label113" CssClass="col-sm-2 control-label" runat="server" Text="อาคารผลิต" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlbuilding_from" AutoPostBack="true" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Requiredddlbuilding_from" ValidationGroup="Search_from" runat="server" Display="None"
                                                    ControlToValidate="ddlbuilding_from" Font-Size="11"
                                                    ErrorMessage="เลือกอาคารผลิต"
                                                    ValidationExpression="เลือกอาคารผลิต" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender30" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlbuilding_from" Width="160" />

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label114" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อผู้ปฎิบัติงาน" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddladminreceive_from" AutoPostBack="true" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Requiredddladminreceive_from" ValidationGroup="Search_from" runat="server" Display="None"
                                                    ControlToValidate="ddladminreceive_from" Font-Size="11"
                                                    ErrorMessage="เลือกชื่อผู้ปฎิบัติงาน"
                                                    ValidationExpression="เลือกชื่อผู้ปฎิบัติงาน" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender31" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddladminreceive_from" Width="160" />

                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label115" CssClass="col-sm-2 control-label" runat="server" Text="วันที่" />
                                            <div class="col-sm-3">


                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txtStartDate_from" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    <asp:RequiredFieldValidator ID="RequiredtxtStartDate_from" ValidationGroup="Search_from" runat="server" Display="None"
                                                        ControlToValidate="txtStartDate_from" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                        ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender32" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtxtStartDate_from" Width="160" />


                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group">

                                            <div class="col-sm-5 col-sm-offset-2">
                                                <asp:LinkButton ID="btnsearch_from" ValidationGroup="Search_from" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Search" runat="server"
                                                    CommandName="btnsearch_from" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                                <asp:LinkButton ID="btnclearsearch_from" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Refresh" runat="server"
                                                    CommandName="btnclearsearch_from" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                                <button
                                                    onclick="tableToExcel('Tableprint_plan', 'แบบฟอร์มใบบันทึกรายงานประจำวัน')"
                                                    class="btn btn-primary btn-sm">
                                                    <i class='fa fa-file-excel'></i>
                                                    Export Excel
                                                </button>
                                                <asp:LinkButton ID="btnprint_plan" CssClass="btn btn-primary btn-sm" runat="server"
                                                    Text="<i class='fa fa-print'></i> พิมพ์" OnClientClick="return printDiv('print_plan');" />

                                            </div>

                                        </div>


                                    </asp:Panel>


                                </div>
                            </div>
                        </div>
                    </div>

                    <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="true" OnRowDataBound="Master_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับที่" ItemStyle-Width="100">
                                <ItemTemplate>
                                    <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <div class="col-lg-12" id="gridreport" runat="server" visible="false">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-signal"></i><strong>&nbsp; ข้อมูลรายงาน</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <asp:GridView ID="GvReport"
                                        runat="server"
                                        AutoGenerateColumns="false"
                                        DataKeyNames="RPIDX"
                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                        HeaderStyle-CssClass="info"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="true"
                                        PageSize="10"
                                        OnPageIndexChanging="Master_PageIndexChanging"
                                        OnRowDataBound="Master_RowDataBound">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center;">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>

                                        <Columns>

                                            <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="7%" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("RECode") %>' /></small>
                                                    <asp:Literal ID="Literal5" Visible="false" runat="server" Text='<%# Eval("RPIDX") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ประเภทการแจ้งซ่อม" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="7%" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="ltzSysIDX_name" runat="server" Text='<%# Eval("zSysIDX_name") %>' /></small>
                                                    <asp:Label ID="ltSysIDXreport" Visible="false" runat="server" Text='<%# Eval("SysIDX") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ข้อมูลเครื่องจักร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%">
                                                <ItemTemplate>
                                                    <small>
                                                        <strong>
                                                            <asp:Label ID="Label1037" runat="server"> สถานที่: </asp:Label></strong>
                                                        <asp:Literal ID="Literal1230" runat="server" Text='<%# Eval("LocName") %>' />
                                                        </p>
                                                      <strong>
                                                          <asp:Label ID="Label16" runat="server"> อาคาร: </asp:Label></strong>
                                                        <asp:Literal ID="Literaal6" runat="server" Text='<%# Eval("BuildingName") %>' />
                                                        </p>
                                                      <strong>
                                                          <asp:Label ID="Labxel18" runat="server"> ห้อง: </asp:Label></strong>
                                                        <asp:Literal ID="Literalk7" runat="server" Text='<%# Eval("Roomname") %>' />
                                                        </p>
                                                     <strong>
                                                         <asp:Label ID="Label70" runat="server">ประเภทเครื่องจักร: </asp:Label></strong>
                                                        <asp:Literal ID="Literal10" runat="server" Text='<%# Eval("TypeNameTH") %>' />
                                                        </p>
                                                    <strong>
                                                        <asp:Label ID="Label106" runat="server">ชื่อเครื่องจักร: </asp:Label></strong>
                                                        <asp:Literal ID="Literal9" runat="server" Text='<%# Eval("MCNameTH") %>' />
                                                        </p>
                                                    <strong>
                                                        <asp:Label ID="Label96" runat="server">รหัสเครื่องจักร: </asp:Label></strong>
                                                        <asp:Literal ID="Literal6" runat="server" Text='<%# Eval("MCCode") %>' />
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="วันเดือนปี" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <small>
                                                        <strong>
                                                            <asp:Label ID="Label1s7" runat="server"> วันที่แจ้งปัญหา: </asp:Label></strong>
                                                        <asp:Literal ID="Literal10a" runat="server" Text='<%# Eval("DateNotify") %>' />
                                                        </p>
                                                      <strong>
                                                          <asp:Label ID="Labela16" runat="server"> วันที่รับงาน: </asp:Label></strong>
                                                        <asp:Literal ID="Litaeral6" runat="server" Text='<%# Eval("DateReceive") %>' />
                                                        </p>
                                                      <strong>
                                                          <asp:Label ID="Label18l" runat="server"> วันที่ปิดงาน: </asp:Label></strong>
                                                        <asp:Literal ID="Literall7" runat="server" Text='<%# Eval("DateClose") %>' />

                                                    </small>
                                                </ItemTemplate>

                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="ข้อมูลผู้แจ้ง" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <small>

                                                        <strong>
                                                            <asp:Label ID="Label13" runat="server">ฝ่าย: </asp:Label></strong>
                                                        <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("UserDeptNameTH") %>' />
                                                        </p>
                                               <strong>
                                                   <asp:Label ID="Label86" runat="server">ชื่อผู้แจ้ง: </asp:Label></strong>
                                                        <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("NameUser") %>' />
                                                        </p>
                                        <strong>
                                            <asp:Label ID="Label87" runat="server">ปัญหาผู้แจ้ง: </asp:Label></strong>
                                                        <asp:Literal ID="Literal11" runat="server" Text='<%# Eval("CommentUser") %>' />
                                                        </p>
                                            
                                          <strong>
                                              <asp:Label ID="Label90" runat="server">ความคิดเห็นผู้แจ้ง: </asp:Label></strong>
                                                        <asp:Literal ID="Literal13" runat="server" Text='<%# Eval("CommentApprove") %>' />
                                                        </p>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="ข้อมูลเจ้าหน้าที่" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%">
                                                <ItemTemplate>
                                                    <small>

                                                        <strong>
                                                            <asp:Label ID="Label17" runat="server"> ชื่อผู้รับงาน: </asp:Label></strong>
                                                        <asp:Literal ID="DetailProblem" runat="server" Text='<%# Eval("NameAdminRe")%>' />
                                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Label91" runat="server"> ความคิดเห็นผู้รับงาน: </asp:Label></strong>
                                                        <asp:Literal ID="Literal14" runat="server" Text='<%# Eval("CommentReciveAdmin")%>' />
                                                        </p>
                                                   <strong>
                                                       <asp:Label ID="Label92" runat="server"> ชื่อผู้ปิดงาน: </asp:Label></strong>
                                                        <asp:Literal ID="Literal15" runat="server" Text='<%# Eval("NameAdminClose")%>' />
                                                        </p>
                                                 <strong>
                                                     <asp:Label ID="Label93" runat="server"> ความคิดเห็นผู้ปิดงาน: </asp:Label></strong>
                                                        <asp:Literal ID="Literal16" runat="server" Text='<%# Eval("CommentAdmin")%>' />

                                                        </p>
                                                 <%--<strong >
                                                     <asp:Label ID="Label96" runat="server"> ค่าใช้จ่าย: </asp:Label></strong>
                                                        <asp:Literal ID="Literal18" runat="server" Text='<%# Eval("Price")%>' />
                                                        </p>--%>
                                                        <strong>
                                                            <asp:Label ID="Label94" runat="server"> รวมเวลาในการดำเนินการ: </asp:Label></strong>
                                                        <asp:Literal ID="Literal17" runat="server" Text='<%# Eval("sumtime")%>' />

                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>




                                            <asp:TemplateField HeaderText="สถานะ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>

                                                        <asp:Label ID="lblnameApprove" runat="server" Text='<%# Eval("status_name")%>'></asp:Label>
                                                        <br />
                                                        <asp:Label ID="lblIDApprove" runat="server" Visible="false" Text='<%# Eval("STAppIDX")%>'></asp:Label>
                                                        <asp:Label ID="lblnameApprove1" runat="server" Text='<%# Eval("status_name")%>'></asp:Label>
                                                        <asp:Label ID="lblnode" Visible="false" runat="server" Text='<%# Eval("node_status")%>'></asp:Label>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                        </Columns>
                                    </asp:GridView>



                                </div>
                            </div>
                        </div>

                    </div>


                    <div class="col-lg-12" id="gridreport_from" runat="server" visible="true">

                        <div class="col-lg-12">
                            <asp:Panel ID="pnl_alert" runat="server">
                                <div class="alert alert-danger col-md-12" style="text-align: center" id="dv_alert" runat="server">
                                    ไม่พบข้อมูล
                                </div>
                            </asp:Panel>
                        </div>


                        <asp:Panel ID="pnl_print_plan" runat="server">
                            <div class="panel-body" style="height: 600px; overflow-y: scroll; overflow-x: scroll;">
                                <div id="print_plan" class="col-lg-12">

                                    <table id="Tableprint_plan"
                                        class="table table-bordered word-wrap"
                                        style="font-family: serif; border-collapse: collapse;">
                                        <thead style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px;">

                                            <tr>
                                                <th colspan="9" style="border-style: hidden; color: black; font-size: 14px; font-family: serif; background-color: white; width: 45%; padding: 0px 0px;">
                                                    <center>
                                                                       <asp:Literal ID="litTitle1" runat="server"></asp:Literal>
                                                                </center>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th colspan="9" style="border-style: hidden; color: black; font-size: 14px; font-family: serif; background-color: white; width: 45%; padding: 0px 0px;">
                                                    <center>
                                    
                                                                        <asp:Literal ID="litTitle2" runat="server"></asp:Literal>
                                                                 </center>
                                                </th>
                                            </tr>
                                            <tr>
                                                <th colspan="9" style="border-style: hidden; color: black; font-size: 14px; font-family: serif; background-color: white; width: 45%; padding: 0px 0px;">
                                                    <center>
                                                                         <asp:Literal ID="litTitle3" runat="server"></asp:Literal>
                                                                  </center>
                                                    <br />
                                                </th>
                                            </tr>



                                            <tr>

                                                <th colspan="3" style="font-size: 13px; color: black; font-family: serif; padding: 0px 0px; border-style: hidden; background-color: white;">
                                                    <b>อาคารผลิต : <%= getLocation_lb() %></b>
                                                </th>
                                                <th colspan="3" style="font-size: 13px; color: black; font-family: serif; padding: 0px 0px; border-style: hidden; background-color: white;">
                                                    <b>ชื่อผู้ปฎิบัติงาน : <%= getname_lb() %></b>
                                                </th>
                                                <th colspan="3" style="font-size: 13px; color: black; font-family: serif; padding: 0px 0px; border-style: hidden; background-color: white;">
                                                    <b>วันที่ : <%= getdate_lb() %></b>
                                                </th>

                                            </tr>
                                            <br />
                                            <tr></tr>

                                            <tr style="width: 100%;">
                                                <th style="background-color: #4CAF50; border: 1px solid #ddd; width: 70px; padding: 0px 0px;">

                                                    <center>
                                                         ลำดับ
                                                    </center>
                                                </th>
                                                <th style="background-color: #4CAF50; border: 1px solid #ddd; padding: 0px 0px; width: 150px; position: center;">

                                                    <center>
                                                         เวลาที่เริ่มดำเนินการ
                                                    </center>

                                                </th>
                                                <th style="background-color: #4CAF50; border: 1px solid #ddd; width: 150px; padding: 0px 0px;">

                                                    <center>
                                                         เวลาที่แก้ไขเสร็จ
                                                    </center>

                                                </th>

                                                <th style="background-color: #4CAF50; border: 1px solid #ddd; width: 300px; padding: 0px 0px;">
                                                    <center>
                                                         รายการงานแจ้งซ่อม (ปัญหา)        
                                                    </center>
                                                </th>
                                                <th style="background-color: #4CAF50; border: 1px solid #ddd; width: 300px; padding: 0px 0px;">
                                                    <center>
                                                         รายละเอียดการแก้ไข       
                                                    </center>
                                                </th>
                                                <th style="background-color: #4CAF50; border: 1px solid #ddd; width: 150px; padding: 0px 0px;">
                                                    <center>
                                                         เลขที่ใบแจ้งซ่อม   
                                                    </center>
                                                </th>
                                                <th style="background-color: #4CAF50; border: 1px solid #ddd; width: 200px; padding: 0px 0px;">
                                                    <center>
                                                         ผลดำเนินงาน   
                                                    </center>
                                                </th>
                                                <th style="background-color: #4CAF50; border: 1px solid #ddd; width: 200px; padding: 0px 0px;">
                                                    <center>
                                                         ผู้ดำเนินการ (ช่างเทคนิค)  
                                                    </center>
                                                </th>
                                                <th style="background-color: #4CAF50; border: 1px solid #ddd; width: 200px; padding: 0px 0px;">
                                                    <center>
                                                         ผู้แจ้ง
                                                    </center>
                                                </th>

                                            </tr>

                                        </thead>
                                        <tbody>
                                            <%= getHtml() %>

                                            <tr>

                                                <td style="font-size: 13px; font-family: serif; padding: 1px 0px; border: 1px solid #ddd;">
                                                    <center>
                                                         ลำดับ
                                                    </center>
                                                </td>
                                                <td colspan="3" style="font-size: 13px; font-family: serif; padding: 1px 0px; border: 1px solid #ddd;">
                                                    <center>
                                                         รายการ/ผลที่จะได้รับ
                                                    </center>
                                                </td>
                                                <td colspan="5" style="font-size: 13px; font-family: serif; padding: 1px 0px; border: 1px solid #ddd;">
                                                    <center>
                                                         รายการที่ช่างเทคนิคตรวจพบการผิดปกติที่จะนำไปสู่ความเสียหายของเครื่องจักรประจำวัน
                                                    </center>
                                                </td>

                                            </tr>

                                            <tr>

                                                <td style="font-size: 13px; font-family: serif; padding: 1px 0px; border: 1px solid #ddd;">
                                                    <center>
                                                         1
                                                    </center>
                                                </td>
                                                <td colspan="3" style="font-size: 13px; font-family: serif; padding: 1px 0px; border: 1px solid #ddd;">ข้อเสนอแนะ :
                                                </td>
                                                <td colspan="5" style="font-size: 13px; font-family: serif; padding: 1px 0px; border: 1px solid #ddd;"></td>

                                            </tr>

                                            <tr>

                                                <td style="font-size: 13px; font-family: serif; padding: 1px 0px; border: 1px solid #ddd;">
                                                    <center>
                                                         2
                                                    </center>
                                                </td>
                                                <td colspan="3" style="font-size: 13px; font-family: serif; padding: 1px 0px; border: 1px solid #ddd;">แนวทางป้องกัน :
                                                </td>
                                                <td colspan="5" style="font-size: 13px; font-family: serif; padding: 1px 0px; border: 1px solid #ddd;"></td>

                                            </tr>
                                            <tr>
                                            </tr>
                                            <tr>

                                                <td style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;">
                                                    <b>หมายเหตุ</b>
                                                </td>
                                                <td colspan="3" style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;">1.กะกลางวันส่งเอกสารในตอนเย็นก่อนจะออกกะของทุกๆวัน
                                                </td>
                                                <td colspan="2" style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;"></td>
                                                <td colspan="2" style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;">
                                                    <center>
                                                         
                                                     </center>
                                                </td>

                                            </tr>

                                            <tr>

                                                <td style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;"></td>
                                                <td colspan="3" style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;">2.กะกลางคืนส่งเอกสารในตอนเช้าก่อนจะออกกะของทุกๆวัน
                                                </td>
                                                <td colspan="2" style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden; text-align: right;">
                                                    <b>ผู้รับทราบ</b>
                                                </td>
                                                <td colspan="2" style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;">
                                                    <center>
                                                         <b>.................................................</b>
                                                     </center>
                                                </td>

                                            </tr>

                                            <tr>

                                                <td style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;"></td>
                                                <td colspan="3" style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;">3.เอกสารต้องเรียบร้อยก่อนนำมาส่งที่ฝ่ายวิศวกรรม
                                                </td>
                                                <td colspan="2" style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden; text-align: right;"></td>
                                                <td colspan="2" style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;">
                                                    <center>
                                                         
                                                         <b>(หัวหน้าฝ่ายผลิต)</b>
                                                     </center>
                                                </td>


                                            </tr>

                                            <tr>

                                                <td style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;">
                                                    <b>ผู้ทวนสอบ</b>
                                                </td>
                                                <td colspan="2" style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;">

                                                    <b>.................................................</b>

                                                </td>
                                                <td style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;"></td>
                                                <td colspan="2" style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden; text-align: right;"></td>
                                                <td colspan="2" style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;"></td>
                                                <td style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;"></td>

                                            </tr>
                                            <tr>

                                                <td style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;"></td>
                                                <td colspan="2" style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;">

                                                    <b>&nbsp;&nbsp;&nbsp;&nbsp;ผู้จัดการฝ่าย/ผู้จัดการแผนก</b>

                                                </td>
                                                <td style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;"></td>
                                                <td colspan="2" style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden; text-align: right;"></td>
                                                <td colspan="2" style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;"></td>
                                                <td style="font-size: 13px; font-family: serif; padding: 0px 0px; border-style: hidden;"></td>

                                            </tr>

                                            <tr>
                                                <td colspan="9" style="border-style: hidden; font-size: 13px; font-family: serif; text-align: right; padding: 0px 0px;">

                                                    <asp:Literal ID="litiso" runat="server"></asp:Literal>
                                                </td>
                                            </tr>

                                        </tbody>

                                    </table>

                                </div>
                            </div>

                        </asp:Panel>

                        <%--</div>
                            </div>
                        </div>--%>
                    </div>


                    <div class="col-lg-12" id="grant" runat="server" visible="false">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>&nbsp; ข้อมูลรายงาน</strong></h3>
                            </div>
                            <div class="panel-body">

                                <div class="form-group">
                                    <asp:Literal ID="litReportChart" runat="server" />
                                </div>

                                <div class="form-group">
                                    <asp:GridView ID="GridView1" runat="server" CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                                        HeaderStyle-CssClass="primary">
                                        <RowStyle HorizontalAlign="center" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>
                                    </asp:GridView>
                                </div>


                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnsearch_chart" />
                    <asp:PostBackTrigger ControlID="btnexport" />
                    <asp:PostBackTrigger ControlID="btnsearch_report" />
                </Triggers>
            </asp:UpdatePanel>
        </asp:View>

        <asp:View ID="ViewQuestion" runat="server">
            <div class="col-lg-12">
                <div class="form-horizontal" role="form">
                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; แบบสอบถาม</strong></h3>
                        </div>
                        <div class="panel-body">

                            <asp:GridView ID="GvQuestionEN"
                                runat="server"
                                AutoGenerateColumns="false"
                                DataKeyNames="TFBIDX"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="info"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center;">Data Cannot Be Found</div>
                                </EmptyDataTemplate>

                                <Columns>

                                    <asp:TemplateField HeaderText="ข้อที่" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="ltSys1" Visible="false" runat="server" Text='<%# Eval("TFBIDX") %>' /></small>
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="หัวข้อคำถาม" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="Literal5" runat="server" Text='<%# Eval("Topic") %>' /></small>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="คำถาม" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%" ItemStyle-CssClass="text-left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Literal ID="ltSystemName" runat="server" Text='<%# Eval("Question") %>' /></small>
                                        </ItemTemplate>

                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="ระดับความพึงพอใจ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <small>
                                                <asp:RadioButtonList ID="rdbChoice" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="4">ดีมาก</asp:ListItem>
                                                    <asp:ListItem Value="3">ดี</asp:ListItem>
                                                    <asp:ListItem Value="2">พอใช้</asp:ListItem>
                                                    <asp:ListItem Value="1">น้อย</asp:ListItem>
                                                </asp:RadioButtonList>
                                                <asp:RequiredFieldValidator ID="RqChoice" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                    ControlToValidate="rdbChoice" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกคำตอบ"
                                                    ValidationExpression="กรุณาเลือกคำตอบ" Value="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqChoice" Width="160" />
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                            </asp:GridView>


                            <div class="form-group">
                                <div class="col-md-12">

                                    <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ข้อเสนอะแนะ : " />
                                    <div class="col-xs-7">
                                        <asp:TextBox CssClass="txtform form-control" ID="txtother" runat="server" placeholder="ข้อเสนอแนะ ..................." TextMode="MultiLine" Rows="4"></asp:TextBox>
                                    </div>

                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-2 col-sm-offset-10">
                                    <asp:LinkButton ID="btnAddquest" CssClass="btn btn-success btn-xs" runat="server" CommandName="CmdInsertQuestion" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                    <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-xs" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewMasterTypeMachine" runat="server">
            <div class="row col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div id="BoxButtonSearchShow" runat="server">
                                <asp:LinkButton ID="BtnHideSETBoxAllSearchShow" CssClass="btn btn-primary" data-toggle="tooltip" title="Search" runat="server" CommandName="BtnHideSETBoxAllSearchShow" OnCommand="btnCommand"><i class="glyphicon glyphicon-plus-sign"></i> สร้างประเภทเครื่องจักร</asp:LinkButton>
                            </div>

                            <div id="BoxButtonSearchHide" runat="server">
                                <asp:LinkButton ID="BtnHideSETBoxAllSearchHide" CssClass="btn btn-warning" data-toggle="tooltip" title="Search" runat="server" CommandName="BtnHideSETBoxAllSearchHide" OnCommand="btnCommand"><i class="glyphicon glyphicon-minus-sign"></i> ยกเลิกสร้างประเภทเครื่องจักร</asp:LinkButton>
                            </div>

                        </div>
                    </div>


                    <%------------------------ Div ADD ผูกผู้ถือครอง Support and Holder ------------------------%>

                    <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                        <div class="row col-md-12">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>NameTH</label>
                                        <asp:TextBox ID="txtaddnameth" runat="server" CssClass="form-control" placeHolder="........................."></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="save_master" runat="server" Display="None" ControlToValidate="txtaddnameth" Font-Size="11"
                                            ErrorMessage="กรุณากรอกข้อมูล"
                                            ValidationExpression="กรุณากรอกข้อมูล"
                                            SetFocusOnError="true" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                        <asp:RegularExpressionValidator ID="RegularExpressiondValidator1" runat="server"
                                            ValidationGroup="save_master" Display="None"
                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                            ControlToValidate="txtaddnameth"
                                            ValidationExpression="^[\s\S]{0,1000}$"
                                            SetFocusOnError="true" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressiondValidator1" Width="160" />

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>NameEN</label>
                                        <asp:TextBox ID="txtaddnameen" runat="server" CssClass="form-control" placeHolder="........................."></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="save_master" runat="server" Display="None" ControlToValidate="txtaddnameen" Font-Size="11"
                                            ErrorMessage="กรุณากรอกข้อมูล"
                                            ValidationExpression="กรุณากรอกข้อมูล"
                                            SetFocusOnError="true" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator11" Width="160" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator22" runat="server"
                                            ValidationGroup="save_master" Display="None"
                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                            ControlToValidate="txtaddnameen"
                                            ValidationExpression="^[\s\S]{0,1000}$"
                                            SetFocusOnError="true" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator22" Width="160" />

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Code</label>
                                        <asp:TextBox ID="txtaddcode" runat="server" CssClass="form-control" placeHolder="........................."></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" ValidationGroup="save_master" runat="server" Display="None" ControlToValidate="txtaddcode" Font-Size="11"
                                            ErrorMessage="กรุณากรอกข้อมูล"
                                            ValidationExpression="กรุณากรอกข้อมูล"
                                            SetFocusOnError="true" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator13" Width="160" />
                                        <asp:RegularExpressionValidator ID="RegularExprejjssionValidator2" runat="server"
                                            ValidationGroup="save_master" Display="None"
                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                            ControlToValidate="txtaddcode"
                                            ValidationExpression="^[\s\S]{0,1000}$"
                                            SetFocusOnError="true" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExprejjssionValidator2" Width="160" />

                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>สถานะ</label>
                                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="1" Text="Online" />
                                            <asp:ListItem Value="0" Text="Offline" />
                                        </asp:DropDownList>
                                    </div>
                                </div>



                                <div class="col-md-12">
                                    <div class="form-group">
                                        <asp:Button CssClass="btn btn-success" runat="server" CommandName="btnInsertmaster_typemachine" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="save_master" />
                                        <asp:Button CssClass="btn btn-danger" runat="server" CommandName="btncancelmaster" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')" OnCommand="btnCommand" Text="ยกเลิก" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>


            <div class="col-md-12">

                <asp:GridView ID="GvMasterTypeMachine"
                    runat="server"
                    AutoGenerateColumns="false"
                    DataKeyNames="TmcIDX"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowEditing="Master_RowEditing"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">No result</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TmcIDX" runat="server" CssClass="form-control"
                                    Visible="false" Text='<%# Eval("TmcIDX")%>' />

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">CodeTM</label>
                                            <asp:TextBox ID="txtcodeupdate" CssClass="form-control" runat="server" Text='<%# Bind("CodeTM") %>' />

                                        </small>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">NameEN</label>
                                            <asp:TextBox ID="txtNameENupdate" CssClass="form-control" runat="server" Text='<%# Bind("NameEN") %>' />

                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">NameTH</label>
                                            <asp:TextBox ID="txtNameTHupdate" CssClass="form-control" runat="server" Text='<%# Bind("NameTH") %>' />

                                        </small>
                                    </div>
                                </div>



                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">สถานะ</label>
                                            <asp:DropDownList ID="ddlStatusUpdate" AutoPostBack="false" runat="server"
                                                CssClass="form-control" SelectedValue='<%# Eval("TStatus") %>'>
                                                <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                            </asp:DropDownList>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                            ValidationGroup="saveTypeNameUpdate" CommandName="Update"
                                            OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                         บันทึก
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                            CommandName="Cancel">ยกเลิก</asp:LinkButton>
                                    </div>
                                </div>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Code" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="detail" runat="server" Text='<%# Eval("CodeTM") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="NameEN" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="NameEN" runat="server" Text='<%# Eval("NameEN") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="NameTH" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="NameTH" runat="server" Text='<%# Eval("NameTH") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Statuscategory" runat="server" Text='<%# getStatus((int)Eval("TStatus")) %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btnDelete_mastermachine" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("TmcIDX") %>'
                                    OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </div>

        </asp:View>

        <asp:View ID="ViewMasterStatus" runat="server">
            <div class="row col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div id="BoxButtonSearchShowStatus" runat="server">
                                <asp:LinkButton ID="LinkButton1" CssClass="btn btn-primary" data-toggle="tooltip" title="Search" runat="server" CommandName="BtnHideSETBoxAllSearchShowStatus" OnCommand="btnCommand"><i class="glyphicon glyphicon-plus-sign"></i> สร้างสถานะรายการ</asp:LinkButton>
                            </div>

                            <div id="BoxButtonSearchHideStatus" runat="server">
                                <asp:LinkButton ID="LinkButton2" CssClass="btn btn-warning" data-toggle="tooltip" title="Search" runat="server" CommandName="BtnHideSETBoxAllSearchHideStatus" OnCommand="btnCommand"><i class="glyphicon glyphicon-minus-sign"></i> ยกเลิกสร้างสถานะรายการ</asp:LinkButton>
                            </div>

                        </div>
                    </div>


                    <%------------------------ Div ADD ผูกผู้ถือครอง Support and Holder ------------------------%>

                    <asp:Panel ID="Panel_AddStatus" runat="server" Visible="false">
                        <div class="row col-md-12">
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Status Name</label>
                                        <asp:TextBox ID="txtaddstatusname" runat="server" CssClass="form-control" placeHolder="........................."></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="save_master" runat="server" Display="None" ControlToValidate="txtaddstatusname" Font-Size="11"
                                            ErrorMessage="กรุณาชื่อประเภทเครื่องจักรภาษาไทย"
                                            ValidationExpression="กรุณาชื่อประเภทเครื่องจักรภาษาไทย"
                                            SetFocusOnError="true" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                                            ValidationGroup="save_master" Display="None"
                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                            ControlToValidate="txtaddstatusname"
                                            ValidationExpression="^[\s\S]{0,1000}$"
                                            SetFocusOnError="true" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator4" Width="160" />

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>คิด Downtime</label>
                                        <asp:DropDownList ID="ddl_downtime" AutoPostBack="true" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="0">กรุณาเลือกสถานะดาวไทม์.....</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="Save" runat="server" Display="None"
                                            ControlToValidate="ddl_downtime" Font-Size="11"
                                            ErrorMessage="กรุณาเลือกสถานะดาวไทม์"
                                            ValidationExpression="กรุณาเลือกสถานะดาวไทม์" InitialValue="0" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />

                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <asp:DropDownList ID="ddlStatus_master" runat="server" CssClass="form-control">
                                            <asp:ListItem Value="1" Text="Online" />
                                            <asp:ListItem Value="0" Text="Offline" />
                                        </asp:DropDownList>
                                    </div>
                                </div>



                                <div class="col-md-12">
                                    <div class="form-group">
                                        <asp:Button CssClass="btn btn-success" runat="server" CommandName="btnInsertmaster_status" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="save_master" />
                                        <asp:Button CssClass="btn btn-danger" runat="server" CommandName="btncancelmasterstatus" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')" OnCommand="btnCommand" Text="ยกเลิก" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>

            <div class="col-md-12">

                <asp:GridView ID="GvMasterStatus"
                    runat="server"
                    AutoGenerateColumns="false"
                    DataKeyNames="stidx"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowEditing="Master_RowEditing"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">No result</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="stidx" runat="server" CssClass="form-control"
                                    Visible="false" Text='<%# Eval("stidx")%>' />

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">Status Name</label>
                                            <asp:TextBox ID="txtstatusupdate" CssClass="form-control" runat="server" Text='<%# Bind("status_name") %>' />

                                        </small>
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">Downtime</label>
                                            <asp:Label ID="lbDTIDX" runat="server" Text='<%# Bind("dtidx") %>' Visible="true" />
                                            <asp:DropDownList ID="ddl_downtime_edit" AutoPostBack="true" CssClass="form-control" runat="server">
                                            </asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="Save_edit" runat="server" Display="None"
                                        ControlToValidate="ddl_downtime_edit" Font-Size="11"
                                        ErrorMessage="กรุณาเลือกสถานะดาวไทม์"
                                        ValidationExpression="กรุณาเลือกสถานะดาวไทม์" InitialValue="0" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />


                                    </small>
                                </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">สถานะ</label>
                                            <asp:DropDownList ID="ddlStatusUpdate_master" AutoPostBack="false" runat="server"
                                                CssClass="form-control" SelectedValue='<%# Eval("status_state") %>'>
                                                <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                            </asp:DropDownList>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                            ValidationGroup="Save_edit" CommandName="Update"
                                            OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                         บันทึก
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                            CommandName="Cancel">ยกเลิก</asp:LinkButton>
                                    </div>
                                </div>
                            </EditItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Status Name" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="status_name" runat="server" Text='<%# Eval("status_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="Downtime" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="downtime_name" runat="server" Text='<%# Eval("downtime_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Statuscategory" runat="server" Text='<%# getStatus((int)Eval("status_state")) %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btnDelete_masterstatus" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("stidx") %>'
                                    OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </div>

        </asp:View>

    </asp:MultiView>


    <script type="text/javascript">
        var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
    </script>

    <script type="text/javascript">
        function fnExcelReport() {
            var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange; var j = 0;
            tab = document.getElementById('Tableprint_sched'); // id of table

            for (j = 0; j < tab.rows.length; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }
    </script>

    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);
            var printWindow = window.open('', '', 'height=800,width=1000');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
                //printWindow.resizeTo(100,100);
                //printWindow.focus();
                //printwindow.window.close();
            }, 500);
            return false;
        }
    </script>
</asp:Content>

