﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_ManageEmployee_private_sarary : System.Web.UI.Page
{
    function_tool _funcTool = new function_tool();
    service_execute _service_exec = new service_execute();
    data_employee _data_employee = new data_employee();

    string Conn_show_profile_employee = "Conn_manage_employee";
    string _local_xml = "";



    protected void Page_Load(object sender, EventArgs e)
    {
        ViewState["emp_idx"] = int.Parse(Session["emp_idx"].ToString());
      
        select_sarary_employee();
    }


    #region sarary
    protected void select_sarary_employee()
    {

        data_employee data_sarary = new data_employee();
        employee_detail show_details = new employee_detail();
        data_sarary.employee_list = new employee_detail[1];
        show_details.emp_idx = int.Parse(ViewState["emp_idx"].ToString());
        data_sarary.employee_list[0] = show_details;

        _local_xml = _service_exec.actionExec(Conn_show_profile_employee, "data_employee", "Manage_Employee", data_sarary, 239);
        data_sarary = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        ViewState["retrun_sarary"] = data_sarary.return_code;

        if (data_sarary.return_code != "0")
        {
            repeater_sarary.DataSource = data_sarary.employee_list;
            repeater_sarary.DataBind();

        }
        else
        {
            repeater_sarary_empty.Visible = true;
            
        }
       

      

    }
    #endregion

    //#region btnCommand 

    //protected void btnCommand(object sender, CommandEventArgs e)
    //{
    //    string cmdName = e.CommandName.ToString();
    //    string cmdArg = e.CommandArgument.ToString();

    //    switch (cmdName)
    //    {
    //        case "btnViewPresonal":
    //            Response.Redirect(ResolveUrl("~/profile"));
    //            break;

    //    }

    //}

    //#endregion
}

