<%@ Page Title="Visitor Management" Language="C#" MasterPageFile="~/masterpage/master_blank.master" AutoEventWireup="true" CodeFile="security_main.aspx.cs" Inherits="websystem_visitors_security_main" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <!--tab menu-->
    <div id="divMenu" runat="server" class="row">
        <div class="col-md-12">
            <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="sub-navbar">
                            <a class="navbar-brand" href="#"><b>Menu</b></a>
                        </div>
                    </div>

                    <!--Collect the nav links, forms, and other content for toggling-->
                    <div class="collapse navbar-collapse" id="menu-bar">
                        <ul class="nav navbar-nav" id="uiNav" runat="server">
                            <li id="li0" runat="server">
                                <asp:LinkButton ID="lbList" runat="server" CommandName="navList" OnCommand="navCommand"> รายการทั่วไป</asp:LinkButton>
                            </li>
                            <li id="li1" runat="server">
                                <asp:LinkButton ID="lbCreate" runat="server" CommandName="navCreate" OnCommand="navCommand"> สร้างรายการ</asp:LinkButton>
                            </li>
                            <li id="li2" runat="server" style="display:none">
                                <asp:LinkButton ID="lbSpecial" runat="server" CommandName="navApprove" OnCommand="navCommand"> รายการพิเศษ</asp:LinkButton>
                            </li>
                        </ul>
                    </div>
                    <!--Collect the nav links, forms, and other content for toggling-->
                </div>
            </nav>
        </div>
    </div>
    <!--tab menu-->
    <div class="row">
        <script>
            (function () {
                // The width and height of the captured photo. We will set the
                // width to the value defined here, but the height will be
                // calculated based on the aspect ratio of the input stream.

                var width = 320;    // We will scale the photo width to this
                var height = 0;     // This will be computed based on the input stream

                // |streaming| indicates whether or not we're currently streaming
                // video from the camera. Obviously, we start at false.

                var streaming = false;

                // The various HTML elements we need to configure or control. These
                // will be set by the startup() function.

                var video = null;
                var canvas = null;
                var photo = null;
                var startbutton = null;

                function startup() {
                    video = document.getElementById('video');
                    canvas = document.getElementById('canvas');
                    photo = document.getElementById('photo');
                    startbutton = document.getElementById('startbutton');

                    navigator.getUserMedia = navigator.getUserMedia ||
                        navigator.webkitGetUserMedia ||
                        navigator.mozGetUserMedia;

                    navigator.getUserMedia({ audio: false, video: true },
                        function (stream) {
                            var video = document.querySelector('video');
                            video.srcObject = stream;
                            video.onloadedmetadata = function (e) {
                                video.play();
                            };
                        },
                        function (err) {
                            console.log("The following error occurred: " + err.name);
                        }
                    );

                    video.addEventListener('canplay', function (ev) {
                        if (!streaming) {
                            height = video.videoHeight / (video.videoWidth / width);

                            // Firefox currently has a bug where the height can't be read from
                            // the video, so we will make assumptions if this happens.

                            if (isNaN(height)) {
                                height = width / (4 / 3);
                            }

                            video.setAttribute('width', width);
                            video.setAttribute('height', height);
                            canvas.setAttribute('width', width);
                            canvas.setAttribute('height', height);
                            streaming = true;
                        }
                    }, false);

                    startbutton.addEventListener('click', function (ev) {
                        takepicture();
                        ev.preventDefault();
                    }, false);

                    clearphoto();
                }

                // Fill the photo with an indication that none has been
                // captured.

                function clearphoto() {
                    var context = canvas.getContext('2d');
                    context.fillStyle = "#AAA";
                    context.fillRect(0, 0, canvas.width, canvas.height);

                    var data = canvas.toDataURL('image/png');
                    photo.setAttribute('src', data);
                }

                // Capture a photo by fetching the current contents of the video
                // and drawing it into a canvas, then converting that to a PNG
                // format data URL. By drawing it on an offscreen canvas and then
                // drawing that to the screen, we can change its size and/or apply
                // other changes before drawing it.

                function takepicture() {
                    var context = canvas.getContext('2d');
                    if (width && height) {
                        canvas.width = width;
                        canvas.height = height;
                        context.drawImage(video, 0, 0, width, height);

                        var data = canvas.toDataURL('image/png');
                        photo.setAttribute('src', data);

                        $("#hiddenid").val(data);

                    } else {
                        clearphoto();
                    }
                }

                // Set up our event listener to run the startup process
                // once loading is complete.
                window.addEventListener('load', startup, false);
            })();

            // var prm = Sys.WebForms.PageRequestManager.getInstance();
            // prm.add_endRequest(
            // )
        </script>
        <style type="text/css">
            #video {
                border: 1px solid black;
                /* box-shadow: 2px 2px 3px black; */
                width: 320px;
                height: 240px;
            }

            #photo {
                border: 1px solid black;
                /* box-shadow: 2px 2px 3px black; */
                width: 320px;
                height: 240px;
            }

            #canvas {
                display: none;
            }

            .camera {
                width: 340px;
                display: inline-block;
            }

            .output {
                width: 340px;
                display: inline-block;
            }

            #startbutton {
                display: block;
                position: relative;
                margin-left: auto;
                margin-right: auto;
                bottom: 32px;
                background-color: rgba(0, 150, 0, 0.5);
                border: 1px solid rgba(255, 255, 255, 0.7);
                box-shadow: 0px 0px 1px 2px rgba(0, 0, 0, 0.2);
                font-size: 14px;
                font-family: "Lucida Grande", "Arial", sans-serif;
                color: rgba(255, 255, 255, 1.0);
            }
        </style>
        <input type="hidden" id="hiddenid" runat="server" ClientIDMode="static" />
        <!--multiview-->
        <asp:MultiView ID="mvSystem" runat="server">
            <asp:View ID="viewList" runat="server">
                <div class="col-md-12">
                    <div class="panel panel-success" id="div_special_heading" runat="server">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <asp:Literal ID="litSpecialHeadingTitle" runat="server" Text="รายการพิเศษ"></asp:Literal></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:Repeater ID="rptCarList" runat="server" OnItemDataBound="rptDataBound">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hfVehicleState" runat="server" Value='<%# Eval("vehicle_state") %>'></asp:HiddenField>
                                            <asp:HiddenField ID="hfVehiclePlateProvince" runat="server" Value='<%# Eval("vehicle_plate_province") %>'></asp:HiddenField>   
                                            <div class="col-md-2 text-center">
                                                <asp:LinkButton ID="lbSpecialSave" runat="server" CssClass='<%# getCssStatus((Int32)Eval("vehicle_state")) %>' OnCommand="btnCommand" CommandName="cmdSaveSpecial" CommandArgument='<%# Eval("m0_idx") %>'>
                                                    <i class="fas fa-car"></i>&nbsp;<%# Eval("vehicle_plate_no") %></asp:LinkButton>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-default" id="div_list_heading" runat="server">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <asp:Literal ID="litListHeadingTitle" runat="server" Text="ค้นหารายการ"></asp:Literal></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">หมายเลขบัตรประชาชน :</label>
                                    <div class="col-md-4">
                                        <asp:TextBox ID="tbSearchIdCard" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                    <label class="col-md-2 control-label">ชื่อผู้มาติดต่อ :</label>
                                    <div class="col-md-4">
                                        <asp:TextBox ID="tbSearchName" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-4">
                                        <asp:LinkButton ID="lbSearch" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearch" CommandArgument="0">
                                            <i class="fas fa-search" aria-hidden="true"></i>&nbsp;ค้นหา</asp:LinkButton>
                                        <asp:LinkButton ID="lbReset" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument="0">
                                            <i class="fas fa-sync-alt" aria-hidden="true"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                        <asp:LinkButton ID="lbDocCreate" runat="server" CssClass="btn btn-success" OnCommand="navCommand" CommandName="navCreate" CommandArgument="0"><i class="far fa-file-alt" aria-hidden="true"></i>&nbsp;สร้างรายการ</asp:LinkButton>
                                    </div>
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-4">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:GridView ID="gvDocList" runat="server" AutoGenerateColumns="False" 
                        CssClass="table table-striped table-bordered table-hover table-responsive" 
                        OnRowDataBound="gvRowDataBound" DataKeyNames="u0_idx"
                        OnPageIndexChanging="gvPageIndexChanging"
                        AllowPaging="true" PageSize="10">
                        <HeaderStyle CssClass="info" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                            FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                        <HeaderStyle CssClass="info" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'></asp:Label>
                                    <asp:HiddenField ID="hfFlagSafety" runat="server" Value='<%# Eval("flag_safety") %>'></asp:HiddenField>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันที่เข้าพบ">
                                <ItemTemplate>
                                    <asp:Label ID="lblVisitDate" runat="server" Text='<%# Eval("visit_date") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ชื่อผู้ที่เข้าพบ">
                                <ItemTemplate>
                                    <asp:Label ID="lblVisitEmpNameTh" runat="server" Text='<%# Eval("visit_emp_name_th") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันที่ทำรายการ">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ผู้ทำรายการ">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpNameTh" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สถานะ">
                                <ItemTemplate>
                                    <asp:Label ID="lblDocStatus" runat="server" Text='<%# Eval("doc_status_name") + " " + Eval("node_name") + " โดย " + Eval("actor_name") %>'></asp:Label>
                                    <asp:Literal ID="litFlagSafety" runat="server" Visible="False"><span class="" style="background-color:#FF0000; color:#FFFFFF;">***(จป)***</span></asp:Literal>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="การจัดการ">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbEdit" runat="server" CssClass="btn btn-sm btn-info" OnCommand="btnCommand" CommandName="cmdEdit" CommandArgument='<%# Eval("u0_idx") %>'><i class="far fa-file-alt"></i>&nbsp;รายละเอียด</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </asp:View>
            <asp:View ID="viewCreate" runat="server">
                <div class="col-md-12">
                    <div class="panel panel-info" id="div_create_heading" runat="server" style="display: none;">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <asp:Literal ID="litCreateHeadingTitle" runat="server" Text="ข้อมูลผู้ทำรายการ"></asp:Literal></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <asp:FormView ID="fvEmpProfile" runat="server" Width="100%" DefaultMode="ReadOnly" OnDataBound="fvDataBound">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hfRdeptIdx" runat="server" Value='<%# Eval("rdept_idx")%>'></asp:HiddenField>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">รหัสพนักงาน :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Enabled="False" Text='<%# Eval("emp_code") %>'></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">ชื่อผู้ทำรายการ :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Enabled="False" Text='<%# Eval("emp_name_th") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">บริษัท :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbOrgNameTh" runat="server" CssClass="form-control" Enabled="False" Text='<%# Eval("org_name_th") %>'></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">ฝ่าย :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbDeptNameTh" runat="server" CssClass="form-control" Enabled="False" Text='<%# Eval("dept_name_th") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">แผนก :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbSecNameTh" runat="server" CssClass="form-control" Enabled="False" Text='<%# Eval("sec_name_th") %>'></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">ตำแหน่ง :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbPosNameTh" runat="server" CssClass="form-control" Enabled="False" Text='<%# Eval("pos_name_th") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:FormView>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-success" id="div_create_form" runat="server">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <asp:Literal ID="litCreateFormTitle" runat="server" Text="สร้างรายการ"></asp:Literal></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <asp:FormView ID="fvDocCreate" runat="server" Width="100%" DefaultMode="Insert" OnDataBound="fvDataBound">
                                    <InsertItemTemplate>
                                        <asp:HiddenField ID="hfU0Idx" runat="server" Value="0"></asp:HiddenField>
                                        <asp:HiddenField ID="hfNodeIdx" runat="server" Value="1"></asp:HiddenField>
                                        <asp:HiddenField ID="hfActorIdx" runat="server" Value="2"></asp:HiddenField>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">วันที่ทำรายการ :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbDocCreateDate" runat="server" CssClass="form-control" placeholder="วันที่ทำรายการ" Enabled="False" Text='<%# DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") %>'></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">วันที่เข้าพบ<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitDate" runat="server" CssClass="form-control date-datepicker" placeholder="วันที่เข้าพบ" MaxLength="16" Text='<%# DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">หัวข้อ :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitTitle" runat="server" CssClass="form-control" placeholder="หัวข้อ" MaxLength="100"></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">รายละเอียด :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitDetail" runat="server" CssClass="form-control" placeholder="รายละเอียด" MaxLength="500"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">ชื่อผู้ที่ต้องการเข้าพบ<span class="text-danger">*</span> :</label>
                                                    <div class="col-md-8">
                                                        <asp:DropDownList ID="ddlEmpList" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="-1" Text="--- ชื่อผู้ที่ต้องการเข้าพบ ---"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        
                                                        <asp:RequiredFieldValidator ID="RequiredddlEmpList"
                                                            ValidationGroup="AddSave" runat="server" Display="None"
                                                            ControlToValidate="ddlEmpList" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกชื่อผู้ที่ต้องการเข้าพบ"
                                                            ValidationExpression="กรุณาเลือกชื่อผู้ที่ต้องการเข้าพบ"
                                                            SetFocusOnError="true"
                                                            InitialValue="-1"
                                                            />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlEmpList" Width="160" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">ประเภทผู้มาติดต่อ<span class="text-danger">* :</label>
                                                    <div class="col-md-8">
                                                        <asp:HiddenField ID="hfVisitorTypeIdx" runat="server" Value="0"></asp:HiddenField>
                                                        <asp:HiddenField ID="hfFlagLoading" runat="server" Value="0"></asp:HiddenField>
                                                        <asp:DropDownList ID="ddlVisitorType" runat="server" CssClass="form-control"></asp:DropDownList>
                                                    
                                                        <asp:RequiredFieldValidator ID="RequiredddlVisitorType"
                                                            ValidationGroup="AddSave" runat="server" Display="None"
                                                            ControlToValidate="ddlVisitorType" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกประเภทผู้มาติดต่อ"
                                                            ValidationExpression="กรุณาเลือกประเภทผู้มาติดต่อ"
                                                            SetFocusOnError="true"
                                                            InitialValue="-1"
                                                            />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlVisitorType" Width="160" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label"></label>
                                                    <div class="col-md-8">
                                                        <asp:Checkbox ID="cbFlagSafety" runat="server" CssClass="form-check-input" Text="Hot work"></asp:Checkbox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">ชื่อผู้ที่ต้องการพบประจำ :</label>
                                                    <div class="col-md-8">
                                                        <asp:DropDownList ID="ddlEmpListTop" runat="server" CssClass="form-control" size="5" style="width:100%; height: 150px; overflow-y: scroll;" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <label class="col-md-4 control-label-static">รายละเอียดผู้มาติดต่อ</label>
                                            <label class="col-md-2 control-label"></label>
                                            <label class="col-md-4"></label>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-4">
                                                <div class="camera">
                                                    <video id="video">Video stream not available.</video>
                                                    <button id="startbutton">Take photo</button>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <canvas id="canvas"></canvas>
                                                <div class="output">
                                                    <img id="photo" alt="The screen capture will appear in this box.">
                                                </div>
                                            </div>
                                            <div class="col-md-2"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-2 control-label" style="padding-top: 0px;">
                                                <asp:DropDownList ID="ddlIdentifyType" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="หมายเลขบัตรประชาชน * :"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="หมายเลขพาสปอร์ต * :"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitCardId" runat="server" CssClass="form-control" placeholder="หมายเลขบัตรประชาชน/พาสปอร์ต" MaxLength="13"></asp:TextBox>
                                            
                                                <asp:RequiredFieldValidator ID="RequiredtbVisitCardId"
                                                    ValidationGroup="AddRow" runat="server" Display="None"
                                                    ControlToValidate="tbVisitCardId" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกหมายเลขบัตรประชาชน/พาสปอร์ต"
                                                    ValidationExpression="กรุณาเลือกหมายเลขบัตรประชาชน/พาสปอร์ต"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RequiredtbVisitCardId" Width="160" />

                                            </div>
                                            <label class="col-md-2 control-label">หมายเลขโทรศัพท์ :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitPhoneNo" runat="server" CssClass="form-control" placeholder="หมายเลขโทรศัพท์" MaxLength="20"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ชื่อ(TH)<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4 clearpm">
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbVisitPrefix" runat="server" CssClass="form-control" placeholder="คำนำหน้าชื่อ" MaxLength="50"></asp:TextBox>
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:TextBox ID="tbVisitFirstname" runat="server" CssClass="form-control" placeholder="ชื่อ" MaxLength="100"></asp:TextBox>
                                                    
                                                <asp:RequiredFieldValidator ID="RequiredtbVisitFirstname"
                                                    ValidationGroup="AddRow" runat="server" Display="None"
                                                    ControlToValidate="tbVisitFirstname" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกชื่อ(TH)"
                                                    ValidationExpression="กรุณากรอกชื่อ(TH)"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RequiredtbVisitFirstname" Width="160" />

                                                </div>
                                            </div>
                                            <label class="col-md-2 control-label">นามสกุล(TH)<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitLastname" runat="server" CssClass="form-control" placeholder="นามสกุล" MaxLength="100"></asp:TextBox>
                                                
                                                <asp:RequiredFieldValidator ID="RequiredtbVisitLastname"
                                                    ValidationGroup="AddRow" runat="server" Display="None"
                                                    ControlToValidate="tbVisitLastname" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกนามสกุล(TH)"
                                                    ValidationExpression="กรุณากรอกนามสกุล(TH)"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RequiredtbVisitLastname" Width="160" />

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ชื่อ(EN)<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4 clearpm">
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbVisitPrefixEn" runat="server" CssClass="form-control" placeholder="คำนำหน้าชื่อ" MaxLength="50"></asp:TextBox>
                                                </div>
                                                <div class="col-md-8">
                                                    <asp:TextBox ID="tbVisitFirstnameEn" runat="server" CssClass="form-control" placeholder="ชื่อ" MaxLength="100" Text="-"></asp:TextBox>
                                                    
                                                <asp:RequiredFieldValidator ID="RequiredtbVisitFirstnameEn"
                                                    ValidationGroup="AddRow" runat="server" Display="None"
                                                    ControlToValidate="tbVisitFirstnameEn" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกชื่อ(EN)"
                                                    ValidationExpression="กรุณากรอกชื่อ(EN)"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RequiredtbVisitFirstnameEn" Width="160" />

                                                </div>
                                            </div>
                                            <label class="col-md-2 control-label">นามสกุล(EN)<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitLastnameEn" runat="server" CssClass="form-control" placeholder="นามสกุล" MaxLength="100" Text="-"></asp:TextBox>
                                                
                                                <asp:RequiredFieldValidator ID="RequiredtbVisitLastnameEn"
                                                    ValidationGroup="AddRow" runat="server" Display="None"
                                                    ControlToValidate="tbVisitLastnameEn" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกนามสกุล(EN)"
                                                    ValidationExpression="กรุณากรอกนามสกุล(EN)"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RequiredtbVisitLastnameEn" Width="160" />

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ที่อยู่ :</label>
                                            <div class="col-md-4 clearpm">
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tbVisitHouseNo" runat="server" CssClass="form-control" placeholder="บ้านเลขที่" MaxLength="100"></asp:TextBox>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tbVisitVillageNo" runat="server" CssClass="form-control" placeholder="หมู่ที่" MaxLength="50"></asp:TextBox>
                                                </div>
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4 clearpm">
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tbVisitLane" runat="server" CssClass="form-control" placeholder="ซอย" MaxLength="100"></asp:TextBox>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tbVisitRoad" runat="server" CssClass="form-control" placeholder="ถนน" MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4 clearpm">
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tbVisitDistrict" runat="server" CssClass="form-control" placeholder="ตำบล" MaxLength="100"></asp:TextBox>
                                                </div>
                                                <div class="col-md-6">
                                                    <asp:TextBox ID="tbVisitAmphure" runat="server" CssClass="form-control" placeholder="อำเภอ" MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitProvince" runat="server" CssClass="form-control" placeholder="จังหวัด" MaxLength="100"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">หมายเลขบัตร Visitor<span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitCardNo" runat="server" CssClass="form-control" placeholder="หมายเลขบัตร Visitor" MaxLength="50"></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="RequiredtbVisitCardNo"
                                                    ValidationGroup="AddRow" runat="server" Display="None"
                                                    ControlToValidate="tbVisitCardNo" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกหมายเลขบัตร Visitor"
                                                    ValidationExpression="กรุณากรอกหมายเลขบัตร Visitor"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="vceRequiredtbVisitCardNo" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight"
                                                    TargetControlID="RequiredtbVisitCardNo" Width="160" />
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                                <asp:LinkButton ID="lbCreate" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdItemInsert" CommandArgument="0" ValidationGroup="AddRow"><i class="fas fa-plus"></i>&nbsp;เพิ่มข้อมูลผู้มาติดต่อ</asp:LinkButton>
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <label class="col-md-4"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-10">
                                                <asp:GridView ID="gvVisitorList" runat="server" AutoGenerateColumns="False"
                                                    CssClass="table table-bordered table-responsive" OnRowDataBound="gvRowDataBound">
                                                    <HeaderStyle CssClass="info" Font-Size="Small" />
                                                    <RowStyle Font-Size="Small" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="หมายเลขบัตรประชาชน/พาสปอร์ต">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hfFlagPhoto" runat="server" Value='<%# Eval("flag_photo") %>'></asp:HiddenField>
                                                                <asp:HiddenField ID="hfIdentifyNo" runat="server" Value='<%# Eval("visit_identify_no") %>'></asp:HiddenField>
                                                                <asp:HiddenField ID="hfVisitCardNo" runat="server" Value='<%# Eval("visit_card_no") %>'></asp:HiddenField>
                                                                <asp:HiddenField ID="hfFirstname" runat="server" Value='<%# Eval("visit_firstname") %>'></asp:HiddenField>
                                                                <asp:HiddenField ID="hfLastname" runat="server" Value='<%# Eval("visit_lastname") %>'></asp:HiddenField>
                                                                <asp:Label ID="lblIdentifyNo" runat="server" Text='<%# Eval("visit_identify_no") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="หมายเลขโทรศัพท์">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPhoneNo" runat="server" Text='<%# Eval("visit_phone_no") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="คำนำหน้าชื่อ">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVistPrefix" runat="server" Text='<%# Eval("visit_prefix") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ชื่อ">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVistFirstname" runat="server" Text='<%# Eval("visit_firstname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="นามสกุล">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVistLastname" runat="server" Text='<%# Eval("visit_lastname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="หมายเลขบัตร Visitor">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVisitCardNo" runat="server" Text='<%# Eval("visit_card_no") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="การจัดการ">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lbDelete" runat="server" CssClass="btn btn-xs btn-danger" OnCommand="btnCommand" CommandName="cmdItemDelete" CommandArgument='<%# Eval("visit_identify_no") %>'><i class="far fa-trash-alt"></i></asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">หมายเลขทะเบียนรถ :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVehiclePlateNo" runat="server" CssClass="form-control" placeholder="หมายเลขทะเบียนรถ"></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">จังหวัด :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlProvince" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2"></label>
                                            <div class="col-md-4">
                                                <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="1" ValidationGroup="AddSave"><i class="fas fa-save"></i>&nbsp;บันทึก</asp:LinkButton>
                                                <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0"><i class="fas fa-times"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                            </div>
                                            <label class="col-md-6"></label>
                                        </div>
                                    </InsertItemTemplate>
                                    <EditItemTemplate>
                                        <asp:HiddenField ID="hfU0Idx" runat="server" Value='<%# Eval("u0_idx") %>'></asp:HiddenField>
                                        <asp:HiddenField ID="hfNodeIdx" runat="server" Value='<%# Eval("node_idx") %>'></asp:HiddenField>
                                        <asp:HiddenField ID="hfActorIdx" runat="server" Value='<%# Eval("actor_idx") %>'></asp:HiddenField>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">วันที่ทำรายการ :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbDocCreateDate" runat="server" CssClass="form-control" placeholder="วันที่ทำรายการ" Enabled="False" Text='<%# Eval("create_date") %>'></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">วันที่เข้าพบ<span class="text-danger">* :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitDate" runat="server" CssClass="form-control" placeholder="วันที่เข้าพบ" Enabled="False" Text='<%# Eval("visit_date") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">หัวข้อ :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitTitle" runat="server" CssClass="form-control" placeholder="หัวข้อ" Enabled="False" Text='<%# Eval("visit_title") %>'></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">รายละเอียด :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVisitDetail" runat="server" CssClass="form-control" placeholder="รายละเอียด" Enabled="False" Text='<%# Eval("visit_detail") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ชื่อผู้ที่ต้องการเข้าพบ<span class="text-danger">* :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlEmpList" runat="server" CssClass="form-control" Enabled="False"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <label class="col-md-4"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label" style="display: none;">Zone ที่มาติดต่อ<span class="text-danger">* :</label>
                                            <div class="col-md-4" style="display: none;">
                                                <asp:GridView ID="gvGroupList" runat="server" AutoGenerateColumns="False"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive" OnRowDataBound="gvRowDataBound" DataKeyNames="m0_idx">
                                                    <HeaderStyle CssClass="info" Font-Size="Small" />
                                                    <RowStyle Font-Size="Small" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="#">
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="cbGroupSelected" runat="server" AutoPostBack="true" OnCheckedChanged="cbCheckedChanged" Text='<%# Eval("m0_idx") + "|" + Eval("group_name") %>' CssClass="hiddenText" Style="color: transparent;" Enabled="False"></asp:CheckBox>
                                                                <asp:HiddenField ID="hfM0Idx" runat="server" Value='<%# Eval("m0_idx")%>'></asp:HiddenField>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Zone">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblGroupName" runat="server" Text='<%# Eval("group_name") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                            <label class="col-md-2 control-label">ประเภทผู้มาติดต่อ<span class="text-danger">* :</label>
                                            <div class="col-md-4">
                                                <asp:HiddenField ID="hfVisitorTypeIdx" runat="server" Value='<%# Eval("visitor_type_idx")%>'></asp:HiddenField>
                                                <asp:HiddenField ID="hfFlagLoading" runat="server" Value='<%# Eval("flag_loading")%>'></asp:HiddenField>
                                                <asp:DropDownList ID="ddlVisitorType" runat="server" CssClass="form-control" Enabled="False"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-4">
                                                <asp:Checkbox ID="cbFlagSafety" runat="server" CssClass="form-check-input" Text="Hot work"></asp:Checkbox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <label class="col-md-4 control-label-static">รายละเอียดผู้มาติดต่อ</label>
                                            <label class="col-md-2 control-label"></label>
                                            <label class="col-md-4"></label>
                                        </div>
                                        <asp:Panel ID="pnPersonalData" runat="server" Visible="False">
                                            <div class="form-group">
                                                <div class="col-md-2">
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="camera">
                                                        <video id="video">Video stream not available.</video>
                                                        <button id="startbutton">Take photo</button>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <canvas id="canvas"></canvas>
                                                    <div class="output">
                                                        <img id="photo" alt="The screen capture will appear in this box.">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-2 control-label" style="padding-top: 0px;">
                                                    <asp:DropDownList ID="ddlIdentifyType" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0" Text="หมายเลขบัตรประชาชน * :"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="หมายเลขพาสปอร์ต * :"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbVisitCardId" runat="server" CssClass="form-control" placeholder="หมายเลขบัตรประชาชน" MaxLength="13"></asp:TextBox>
                                                
                                                    <asp:RequiredFieldValidator ID="RequiredtbVisitCardId"
                                                        ValidationGroup="UpdatSave" runat="server" Display="None"
                                                        ControlToValidate="tbVisitCardId" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกหมายเลขบัตรประชาชน/พาสปอร์ต"
                                                        ValidationExpression="กรุณาเลือกหมายเลขบัตรประชาชน/พาสปอร์ต" SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtbVisitCardId" Width="160" />

                                                </div>
                                                <label class="col-md-2 control-label">หมายเลขโทรศัพท์ :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbVisitPhoneNo" runat="server" CssClass="form-control" placeholder="หมายเลขโทรศัพท์" MaxLength="20"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">ชื่อ(TH)<span class="text-danger">*</span> :</label>
                                                <div class="col-md-4 clearpm">
                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="tbVisitPrefix" runat="server" CssClass="form-control" placeholder="คำนำหน้าชื่อ" MaxLength="50"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <asp:TextBox ID="tbVisitFirstname" runat="server" CssClass="form-control" placeholder="ชื่อ" MaxLength="100"></asp:TextBox>
                                                    
                                                        <asp:RequiredFieldValidator ID="RequiredtbVisitFirstname" ValidationGroup="UpdatSave" runat="server" Display="None" ControlToValidate="tbVisitFirstname" Font-Size="11" ErrorMessage="กรุณากรอกชื่อ(TH)" ValidationExpression="กรุณากรอกชื่อ(TH)" SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtbVisitFirstname" Width="160" />

                                                    </div>
                                                </div>
                                                <label class="col-md-2 control-label">นามสกุล(TH)<span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbVisitLastname" runat="server" CssClass="form-control" placeholder="นามสกุล" MaxLength="100"></asp:TextBox>
                                                
                                                    <asp:RequiredFieldValidator ID="RequiredtbVisitLastname"
                                                        ValidationGroup="UpdatSave" runat="server" Display="None" ControlToValidate="tbVisitLastname" Font-Size="11" ErrorMessage="กรุณากรอกนามสกุล(TH)" ValidationExpression="กรุณากรอกนามสกุล(TH)" SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtbVisitLastname" Width="160" />

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">ชื่อ(EN)<span class="text-danger">*</span> :</label>
                                                <div class="col-md-4 clearpm">
                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="tbVisitPrefixEn" runat="server" CssClass="form-control" placeholder="คำนำหน้าชื่อ" MaxLength="50"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <asp:TextBox ID="tbVisitFirstnameEn" runat="server" CssClass="form-control" placeholder="ชื่อ" MaxLength="100" Text="-"></asp:TextBox>
                                                    
                                                        <asp:RequiredFieldValidator ID="RequiredtbVisitFirstnameEn" ValidationGroup="UpdatSave" runat="server" Display="None" ControlToValidate="tbVisitFirstnameEn" Font-Size="11" ErrorMessage="กรุณากรอกชื่อ(EN)" ValidationExpression="กรุณากรอกชื่อ(EN)" SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtbVisitFirstnameEn" Width="160" />

                                                    </div>
                                                </div>
                                                <label class="col-md-2 control-label">นามสกุล(EN)<span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbVisitLastnameEn" runat="server" CssClass="form-control" placeholder="นามสกุล" MaxLength="100" Text="-"></asp:TextBox>
                                               
                                                    <asp:RequiredFieldValidator ID="RequiredtbVisitLastnameEn" ValidationGroup="UpdatSave" runat="server" Display="None" ControlToValidate="tbVisitLastnameEn" Font-Size="11" ErrorMessage="กรุณากรอกนามสกุล(EN)" ValidationExpression="กรุณากรอกนามสกุล(EN)" SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtbVisitLastnameEn" Width="160" />

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">ที่อยู่ :</label>
                                                <div class="col-md-4 clearpm">
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tbVisitHouseNo" runat="server" CssClass="form-control" placeholder="บ้านเลขที่" MaxLength="100"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tbVisitVillageNo" runat="server" CssClass="form-control" placeholder="หมู่ที่" MaxLength="50"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <label class="col-md-2 control-label"></label>
                                                <div class="col-md-4 clearpm">
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tbVisitLane" runat="server" CssClass="form-control" placeholder="ซอย" MaxLength="100"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tbVisitRoad" runat="server" CssClass="form-control" placeholder="ถนน" MaxLength="100"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label"></label>
                                                <div class="col-md-4 clearpm">
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tbVisitDistrict" runat="server" CssClass="form-control" placeholder="ตำบล" MaxLength="100"></asp:TextBox>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <asp:TextBox ID="tbVisitAmphure" runat="server" CssClass="form-control" placeholder="อำเภอ" MaxLength="100"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <label class="col-md-2 control-label"></label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbVisitProvince" runat="server" CssClass="form-control" placeholder="จังหวัด" MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">หมายเลขบัตร Visitor<span class="text-danger">*</span> :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbVisitCardNo" runat="server" CssClass="form-control" placeholder="หมายเลขบัตร Visitor" MaxLength="50"></asp:TextBox>
                                                </div>
                                                <label class="col-md-2 control-label"></label>
                                                <div class="col-md-4">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label"></label>
                                                <div class="col-md-4">
                                                    <asp:LinkButton ID="lbUpdatePersonal" runat="server" CssClass="btn btn-success" 
                                                        OnCommand="btnCommand" CommandName="cmdUpdatePersonalData" CommandArgument="0" ValidationGroup="UpdatSave"><i class="fas fa-plus"></i>&nbsp;อัพเดทข้อมูลผู้มาติดต่อ</asp:LinkButton>
                                                </div>
                                                <label class="col-md-2 control-label"></label>
                                                <label class="col-md-4"></label>
                                            </div>
                                        </asp:Panel>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-10">
                                                <asp:GridView ID="gvVisitorList" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-responsive" OnRowDataBound="gvRowDataBound">
                                                    <HeaderStyle CssClass="info" Font-Size="Small" />
                                                    <RowStyle Font-Size="Small" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="หมายเลขบัตรประชาชน/พาสปอร์ต">
                                                            <ItemTemplate>
                                                                <asp:HiddenField ID="hfFlagPhoto" runat="server" Value='<%# Eval("flag_photo") %>'></asp:HiddenField>
                                                                <asp:HiddenField ID="hfIdentifyNo" runat="server" Value='<%# Eval("visit_identify_no") %>'></asp:HiddenField>
                                                                <asp:HiddenField ID="hfVisitCardNo" runat="server" Value='<%# Eval("visit_card_no") %>'></asp:HiddenField>
                                                                <asp:HiddenField ID="hfFirstname" runat="server" Value='<%# Eval("visit_firstname") %>'></asp:HiddenField>
                                                                <asp:HiddenField ID="hfLastname" runat="server" Value='<%# Eval("visit_lastname") %>'></asp:HiddenField>
                                                                <asp:Label ID="lblIdentifyNo" runat="server" Text='<%# Eval("visit_identify_no") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="หมายเลขโทรศัพท์">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPhoneNo" runat="server" Text='<%# Eval("visit_phone_no") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="คำนำหน้าชื่อ">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVistPrefix" runat="server" Text='<%# Eval("visit_prefix") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ชื่อ">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVistFirstname" runat="server" Text='<%# Eval("visit_firstname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="นามสกุล">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVistLastname" runat="server" Text='<%# Eval("visit_lastname") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="หมายเลขบัตร Visitor">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblVisitCardNo" runat="server" Text='<%# Eval("visit_card_no") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="การจัดการ">
                                                            <ItemTemplate>
                                                                <asp:LinkButton ID="lbCamera" runat="server" CssClass="btn btn-sm btn-primary" OnCommand="btnCommand" CommandName="cmdSavePhoto" CommandArgument='<%# Eval("u1_idx") %>' Visible="False"><i class="fas fa-camera"></i>&nbsp;บันทึกรูป</asp:LinkButton>
                                                                <asp:LinkButton ID="lbIdCard" runat="server" CssClass="btn btn-sm btn-info" OnCommand="btnCommand" CommandName="cmdSelectPersonalData" CommandArgument='<%# (Container.DataItemIndex) %>' Visible="False"><i class="far fa-address-card"></i>&nbsp;อัพเดทข้อมูล</asp:LinkButton>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">หมายเลขทะเบียนรถ :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbVehiclePlateNo" runat="server" CssClass="form-control" placeholder="หมายเลขทะเบียนรถ" Enabled="False"></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">จังหวัด :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlProvince" runat="server" CssClass="form-control" Enabled="False"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <asp:HiddenField ID="hfFlagSafety" runat="server" Value='<%# Eval("flag_safety")%>'></asp:HiddenField>
                                        <asp:Panel ID="pnNode6Comment" runat="server" Visible="False">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Safety Comment :</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbSafetyComment" runat="server" CssClass="form-control" placeholder="Safety Comment" Enabled="False" Text='<%# Eval("safety_comment") %>'></asp:TextBox>
                                                </div>
                                                <label class="col-md-2 control-label"></label>
                                                <label class="col-md-4"></label>
                                            </div>
                                        </asp:Panel>
                                        <div class="form-group">
                                            <div class="col-md-2">
                                                <asp:LinkButton ID="lbBack" runat="server" CssClass="btn btn-default" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0"><i class="fas fa-chevron-left"></i>&nbsp;กลับ</asp:LinkButton>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Panel ID="pnNode2" runat="server" Visible="False">
                                                    <asp:LinkButton ID="lbN2Approve" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="2"><i class="fas fa-check"></i>&nbsp;อนุมัติ</asp:LinkButton>
                                                    <asp:LinkButton ID="lbN2Reject" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="3"><i class="fas fa-times"></i>&nbsp;ไม่อนุมัติ</asp:LinkButton>
                                                </asp:Panel>
                                                <asp:Panel ID="pnNode3" runat="server" Visible="False">
                                                    <asp:LinkButton ID="lbN3Approve" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="4"><i class="fas fa-check"></i>&nbsp;อนุมัติ Zone</asp:LinkButton>
                                                    <asp:LinkButton ID="lbN3Reject" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="5"><i class="fas fa-times"></i>&nbsp;ไม่อนุมัติ Zone</asp:LinkButton>
                                                </asp:Panel>
                                                <asp:Panel ID="pnNode4" runat="server" Visible="False">
                                                    <asp:LinkButton ID="lbN4Approve" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="6"><i class="fas fa-save"></i>&nbsp;บันทึก</asp:LinkButton>
                                                </asp:Panel>
                                                <asp:Panel ID="pnNode5" runat="server" Visible="False">
                                                    <asp:LinkButton ID="lbN5Safety" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="8"><i class="fas fa-plus"></i>&nbsp;ติดต่อ Safety</asp:LinkButton>
                                                    <asp:LinkButton ID="lbN5NoSafety" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="9"><i class="fas fa-user"></i>&nbsp;ติดต่อ ต้นสังกัด</asp:LinkButton>
                                                </asp:Panel>
                                                <asp:Panel ID="pnNode6" runat="server" Visible="False">
                                                    <asp:LinkButton ID="lbN6Approve" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="10"><i class="fas fa-save"></i>&nbsp;บันทึก</asp:LinkButton>
                                                </asp:Panel>
                                                <asp:Panel ID="pnNode7" runat="server" Visible="False">
                                                    <asp:LinkButton ID="lbN7Approve" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="12"><i class="fas fa-save"></i>&nbsp;บันทึก</asp:LinkButton>
                                                </asp:Panel>
                                                <asp:Panel ID="pnNode8" runat="server" Visible="False">
                                                    <asp:LinkButton ID="lbN8Approve" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="14"><i class="fas fa-save"></i>&nbsp;ยืนยัน(จบงาน)</asp:LinkButton>
                                                </asp:Panel>
                                                <asp:Panel ID="pnNode9" runat="server" Visible="False">
                                                    <asp:LinkButton ID="lbN9Approve" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="16"><i class="fas fa-save"></i>&nbsp;ยืนยัน(ออกนอกพื้นที่)</asp:LinkButton>
                                                </asp:Panel>
                                            </div>
                                            <label class="col-md-6"></label>
                                        </div>
                                    </EditItemTemplate>
                                </asp:FormView>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default" id="div_display_log" runat="server">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <asp:Literal ID="litLogFormTitle" runat="server" Text="ประวัติรายการ"></asp:Literal></h3>
                        </div>
                        <div class="panel-body">
                            <asp:Repeater ID="rptLog" runat="server">
                                <HeaderTemplate>
                                    <div class="row">
                                        <label class="col-sm-2 control-label">วันที่ / เวลา</label>
                                        <label class="col-sm-3 control-label">ผู้ดำเนินการ</label>
                                        <label class="col-sm-2 control-label">ดำเนินการ</label>
                                        <label class="col-sm-2 control-label">ผลการดำเนินการ</label>
                                        <label class="col-sm-3 control-label">หมายเหตุ</label>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <span><%# Eval("create_date") %></span>
                                        </div>
                                        <div class="col-sm-3">
                                            <span><%# Eval("emp_name_th") %>(<%# Eval("actor_name") %>)</span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><%# Eval("node_name") %></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <span><%# Eval("decision_name") %></span>
                                        </div>
                                        <div class="col-sm-3">
                                            <span><%# Eval("remark") %></span>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </asp:View>
            <asp:View ID="viewApprove" runat="server">
                <div class="col-md-12">
                </div>
            </asp:View>
        </asp:MultiView>
    </div>

    <script type="text/javascript">
        $(function () {
            $('.date-datepicker').datetimepicker({
                format: 'YYYY-MM-DD HH:mm'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.date-datepicker').datetimepicker({
                    format: 'YYYY-MM-DD HH:mm'
                });
            });
        });
    </script>
</asp:Content>
