﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_elearning_transection_el_el_quiz : System.Web.UI.Page
{

    #region Pageload
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            SetDefaultpage(1);

        }
    }

    #endregion

    #region SetDefault
    protected void SetDefaultpage(int page)
    {

        switch (page)
        {

            case 1:
                _divMenuLiToViewIndex.Attributes.Add("class", "active");
                _divMenuLiToViewMaster.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                MvMaster.SetActiveView(ViewIndex);
                break;


            case 2:
                _divMenuLiToViewApprove.Attributes.Add("class", "active");
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewMaster.Attributes.Remove("class");

                break;


            case 3:
               
                _divMenuLiToViewMaster.Attributes.Add("class", "active");
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewMasterModule);
                break;
        }
    }
    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "_divMenuBtnToDivIndex":
                SetDefaultpage(1);
                break;
            case "_divMenuBtnToDivApprove":
                SetDefaultpage(2);
                break;

            case "_divMenuBtnToDivMasterModule":
                SetDefaultpage(3);
                break;

            case "_divMenuBtnToDivMasterProduct":

                break;

            case "_divMenuBtnToDivMasterGroupProduct":

                break;
            case "_divMenuBtnToDivMasterProduct_M1":

                break;
            case "_divMenuBtnToDivMasterProduct_M2":

                break;
            case "_divMenuBtnToDivMasterUnit":

                break;
            case "_divMenuBtnToDivMasterPlace":

                break;

        }
    }

    #endregion
}