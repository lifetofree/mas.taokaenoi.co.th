﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="group-diary.aspx.cs" Inherits="websystem_emps_group_diary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
   <!-- START GridView Group Diary -->
   <div id="divGvGroupDiary" runat="server" class="col-md-12">
      <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary" runat="server" CommandName="btnToInsert" OnCommand="btnCommand" Text="สร้างกลุ่ม" />
      <asp:GridView ID="gvGroupDiary"
         runat="server"
         AutoGenerateColumns="false"
         CssClass="table table-striped table-bordered table-responsive m-t-10"
         HeaderStyle-CssClass="info"
         AllowPaging="true"
         PageSize="10"
         OnRowDataBound="onRowDatabound"
         OnPageIndexChanging="onPageIndexChanging">
         <PagerStyle CssClass="pageCustom" />
         <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
         <EmptyDataTemplate>
            <div style="text-align: center">No result</div>
         </EmptyDataTemplate>
         <Columns>
            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
               HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <%# (Container.DataItemIndex + 1) %>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ชื่อกลุ่ม" ItemStyle-HorizontalAlign="Center"
               HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
               <ItemTemplate>
                  <small>
                     <asp:Label ID="groupDiaryName" runat="server" Text='<%# Eval("group_diary_name") %>' />
                  </small>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
               HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
               <ItemTemplate>
                  <small>
                     <p><b>องค์กร :</b> <%# Eval("OrgNameTH") %></p>
                     <p><b>ฝ่าย :</b> <%# Eval("DeptNameTH") %></p>
                     <b>แผนก :</b> <%# Eval("SecNameTH") %>
                  </small>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
               HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
               <ItemTemplate>
                  <small>
                     <asp:Label ID="groupDiaryStatus" runat="server" Text='<%# Eval("group_diary_status") %>' Visible="false" />
                     <asp:Label ID="groupDiaryStatusIcon" runat="server" Text='<%# getStatus((int)Eval("group_diary_status")) %>' />
                  </small>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-HorizontalAlign="center"
               HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
               <ItemTemplate>
                  <asp:LinkButton ID="btnUnBan" CssClass="text-edit" runat="server" Visible="false" data-toggle="tooltip"
                     title="คืนค่า" CommandName="btnUnBan" OnCommand="btnCommand"
                     CommandArgument='<%# Eval("m0_group_diary_idx") %>'
                     OnClientClick="return confirm('คุณต้องการคืนค่าข้อมูลนี้ใช่หรือไม่')">
                     <i class="fa fa-refresh fa-lg"></i>
                  </asp:LinkButton>
                  <asp:LinkButton ID="btnBan" CssClass="text-trash" runat="server" Visible="false" data-toggle="tooltip"
                     title="ลบ" CommandName="btnBan" OnCommand="btnCommand"
                     CommandArgument='<%# Eval("m0_group_diary_idx") %>'
                     OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                     <i class="fa fa-trash fa-lg"></i>
                  </asp:LinkButton>
               </ItemTemplate>
            </asp:TemplateField>
         </Columns>
      </asp:GridView>
   </div>
   <!-- END GridView Group Diary -->

   <!-- START Insert Form -->
   <div id="divInsertForm" runat="server" visible="false">
      <div class="col-md-12">
         <div class="form-group">
            <asp:LinkButton CssClass="btn btn-danger f-s-14" runat="server" CommandName="btnCancel" OnCommand="btnCommand">
               <i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ
            </asp:LinkButton>
         </div>
      </div>
      <div class="clearfix"></div>
      <div class="col-md-12">
         <div class="panel panel-info">
            <div class="panel-heading">สร้างกลุ่ม</div>
            <div class="panel-body">
               <div class="col-md-4">
                  <div class="form-group">
                     <label class="f-s-13">องค์กร</label>
                     <asp:DropDownList ID="ddlGroupDiaryOrg" runat="server" CssClass="form-control" AutoPostBack="true"
                        OnSelectedIndexChanged="onSelectedIndexChanged" />
                     <asp:RequiredFieldValidator ID="requiredGroupDiaryOrg"
                        ValidationGroup="saveGroupDiary" runat="server"
                        Display="Dynamic"
                        SetFocusOnError="true"
                        ControlToValidate="ddlGroupDiaryOrg"
                        Font-Size="13px" ForeColor="Red"
                        ErrorMessage="กรุณาเลือกองค์กร" />
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group">
                     <label class="f-s-13">ฝ่าย</label>
                     <asp:DropDownList ID="ddlGroupDiaryDept" runat="server" CssClass="form-control" AutoPostBack="true"
                        OnSelectedIndexChanged="onSelectedIndexChanged" />
                     <asp:RequiredFieldValidator ID="requiredGroupDiaryDept"
                        ValidationGroup="saveGroupDiary" runat="server"
                        Display="Dynamic"
                        SetFocusOnError="true"
                        ControlToValidate="ddlGroupDiaryDept"
                        Font-Size="13px" ForeColor="Red"
                        ErrorMessage="กรุณาเลือกฝ่าย" />
                  </div>
               </div>
               <div class="col-md-4" runat="server">
                  <div class="form-group">
                     <label class="f-s-13">แผนก</label>
                     <asp:DropDownList ID="ddlGroupDiarySec" runat="server" CssClass="form-control" />
                     <asp:RequiredFieldValidator ID="requiredGroupDiarySec"
                        ValidationGroup="saveGroupDiary" runat="server"
                        Display="Dynamic"
                        SetFocusOnError="true"
                        ControlToValidate="ddlGroupDiarySec"
                        Font-Size="13px" ForeColor="Red"
                        ErrorMessage="กรุณาเลือกแผนก" />
                     <asp:CustomValidator ID="customGroupDiarySection"
                        ValidationGroup="saveGroupDiary" runat="server"
                        Display="Dynamic"
                        SetFocusOnError="true"
                        ControlToValidate="ddlGroupDiarySec"
                        OnServerValidate="checkExistsGroupDiarySection"
                        Font-Size="13px" ForeColor="Red"
                        ErrorMessage="แผนกที่เลือกถูกสร้างกลุ่มแล้ว" />
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="f-s-13">ชื่อกลุ่ม</label>
                     <asp:TextBox ID="txtGroupDiaryName" runat="server" CssClass="form-control"
                        placeholder="ชื่อกลุ่ม..." />
                     <asp:RequiredFieldValidator ID="requiredGroupDiaryName"
                        ValidationGroup="saveGroupDiary" runat="server"
                        Display="Dynamic"
                        SetFocusOnError="true"
                        ControlToValidate="txtGroupDiaryName"
                        Font-Size="13px" ForeColor="Red"
                        ErrorMessage="กรุณากรอกชื่อกลุ่ม" />
                     <asp:CustomValidator ID="customGroupDiaryName"
                        ValidationGroup="saveGroupDiary" runat="server"
                        Display="Dynamic"
                        SetFocusOnError="true"
                        ControlToValidate="txtGroupDiaryName"
                        Font-Size="13px" ForeColor="Red"
                        ErrorMessage="ชื่อกลุ่มนี้มีในระบบแล้ว" />
                  </div>
               </div>
               <div class="col-md-6">
                  <div class="form-group">
                     <label class="f-s-13">สถานะ</label>
                     <asp:DropDownList ID="ddlGroupDiaryStatus" runat="server" CssClass="form-control">
                        <asp:ListItem Value="1" Text="Online" />
                        <asp:ListItem Value="0" Text="Offline" />
                     </asp:DropDownList>
                  </div>
               </div>
               <div class="col-md-12">
                  <div class="form-group">
                     <asp:Button CssClass="btn btn-success f-s-14" runat="server" CommandName="btnInsert" OnCommand="btnCommand" Text="บันทึก"
                        ValidationGroup="saveGroupDiary" />
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- END Insert Form -->
</asp:Content>
