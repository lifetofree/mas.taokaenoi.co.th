﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("DataSupportIT")]
public class DataSupportIT
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }

    [XmlElement("Code")]
    public string Code { get; set; }

    [XmlElement("ReturnIDX")]
    public string ReturnIDX { get; set; }

    [XmlElement("ReturnEmail")]
    public string ReturnEmail { get; set; }

    [XmlElement("ReturnUIDX")]
    public string ReturnUIDX { get; set; }

    [XmlElement("ReturnMFB")]
    public string ReturnMFB { get; set; }

    [XmlElement("ReturnMFB2")]
    public string ReturnMFB2 { get; set; }


    [XmlElement("ReturnCount")]
    public string ReturnCount { get; set; }

    [XmlElement("ReturnCount2")]
    public string ReturnCount2 { get; set; }

    [XmlElement("ReturnCount3")]
    public string ReturnCount3 { get; set; }

    [XmlElement("ReturnCount4")]
    public string ReturnCount4 { get; set; }

    [XmlElement("ReturnCount5")]
    public string ReturnCount5 { get; set; }

    [XmlElement("ReturnCount6")]
    public string ReturnCount6 { get; set; }

    [XmlElement("ReturnCount7")]
    public string ReturnCount7 { get; set; }

    [XmlElement("ReturnCount8")]
    public string ReturnCount8 { get; set; }

    [XmlElement("ReturnCount9")]
    public string ReturnCount9 { get; set; }

    [XmlElement("ReturnCount10")]
    public string ReturnCount10 { get; set; }

    [XmlElement("ReturnName")]
    public string ReturnName { get; set; }

    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }

    public string CountRows { get; set; }

    [XmlElement("BoxStatusSAP")]
    public StatusSAP[] BoxStatusSAP { get; set; }

    [XmlElement("BoxStatusPriority")]
    public PrioritySAP[] BoxStatusPriority { get; set; }

    [XmlElement("BoxUserRequest")]
    public UserRequestList[] BoxUserRequest { get; set; }

    [XmlElement("BoxSystem")]
    public SystemtList[] BoxSystem { get; set; }

    [XmlElement("BoxFeedBack")]
    public FeedBackList[] BoxFeedBack { get; set; }

    [XmlElement("BoxDowntimePOS")]
    public DowntimePOS[] BoxDowntimePOS { get; set; }

    [XmlElement("BoxPOSList")]
    public POSList[] BoxPOSList { get; set; }

    [XmlElement("BoxExportPOSList")]
    public ExportDataPOS[] BoxExportPOSList { get; set; }

    [XmlElement("BoxExportSAPList")]
    public ExportDataSAP[] BoxExportSAPList { get; set; }

    [XmlElement("BoxReportSAPList")]
    public ReportSAP[] BoxReportSAPList { get; set; }

    [XmlElement("BoxExportITList")]
    public ExportDataIT[] BoxExportITList { get; set; }

    [XmlElement("BoxExportRESList")]
    public ExportDataRES[] BoxExportRESList { get; set; }

    [XmlElement("BoxDepartmentList")]
    public ShortDepartment[] BoxDepartmentList { get; set; }

    [XmlElement("CaseIT_Detail")]
    public CaseITlist[] CaseIT_Detail { get; set; }

    [XmlElement("BoxMSAPlist")]
    public MSAPlist[] BoxMSAPlist { get; set; }

    [XmlElement("BoxRESList")]
    public RESList[] BoxRESList { get; set; }

}

[Serializable]
public class RESList
{
    [XmlElement("RES1IDX")]
    public int RES1IDX { get; set; }

    [XmlElement("RES1_Name")]
    public string RES1_Name { get; set; }

    [XmlElement("RES1Status")]
    public int RES1Status { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("RES2IDX")]
    public int RES2IDX { get; set; }

    [XmlElement("RES2_Name")]
    public string RES2_Name { get; set; }

    [XmlElement("RES2Status")]
    public int RES2Status { get; set; }

    [XmlElement("RES3IDX")]
    public int RES3IDX { get; set; }

    [XmlElement("RES3_Name")]
    public string RES3_Name { get; set; }

    [XmlElement("RES3Status")]
    public int RES3Status { get; set; }

    [XmlElement("RES4IDX")]
    public int RES4IDX { get; set; }

    [XmlElement("RES4_Name")]
    public string RES4_Name { get; set; }

    [XmlElement("RES4Status")]
    public int RES4Status { get; set; }


}


[Serializable]
public class MSAPlist
{
    [XmlElement("SysIDX")]
    public int SysIDX { get; set; }

    [XmlElement("StateTime")]
    public string StateTime { get; set; }

    [XmlElement("SysNameTH")]
    public string SysNameTH { get; set; }

    [XmlElement("MS1IDX")]
    public int MS1IDX { get; set; }
    [XmlElement("MS2IDX")]
    public int MS2IDX { get; set; }
    [XmlElement("MS3IDX")]
    public int MS3IDX { get; set; }
    [XmlElement("MS4IDX")]
    public int MS4IDX { get; set; }
    [XmlElement("MS5IDX")]
    public int MS5IDX { get; set; }


    [XmlElement("MS1_Name")]
    public string MS1_Name { get; set; }

    [XmlElement("MS2_Name")]
    public string MS2_Name { get; set; }

    [XmlElement("MS3_Name")]
    public string MS3_Name { get; set; }

    [XmlElement("MS4_Name")]
    public string MS4_Name { get; set; }

    [XmlElement("MS5_Name")]
    public string MS5_Name { get; set; }




    [XmlElement("MS1_Code")]
    public string MS1_Code { get; set; }

    [XmlElement("MS2_Code")]
    public string MS2_Code { get; set; }

    [XmlElement("MS3_Code")]
    public string MS3_Code { get; set; }

    [XmlElement("MS4_Code")]
    public string MS4_Code { get; set; }

    [XmlElement("MS5_Code")]
    public string MS5_Code { get; set; }




    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }
    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }

    [XmlElement("UpdateDate")]
    public string UpdateDate { get; set; }



    [XmlElement("MS1Status")]
    public int MS1Status { get; set; }
    [XmlElement("MS2Status")]
    public int MS2Status { get; set; }
    [XmlElement("MS3Status")]
    public int MS3Status { get; set; }
    [XmlElement("MS4Status")]
    public int MS4Status { get; set; }
    [XmlElement("MS5Status")]
    public int MS5Status { get; set; }


    [XmlElement("MS1StatusDetail")]
    public string MS1StatusDetail { get; set; }

    [XmlElement("MS2StatusDetail")]
    public string MS2StatusDetail { get; set; }

    [XmlElement("MS3StatusDetail")]
    public string MS3StatusDetail { get; set; }

    [XmlElement("MS4StatusDetail")]
    public string MS4StatusDetail { get; set; }

    [XmlElement("MS5StatusDetail")]
    public string MS5StatusDetail { get; set; }



    [XmlElement("Name_Code1")]
    public string Name_Code1 { get; set; }


    [XmlElement("Name_Code2")]
    public string Name_Code2 { get; set; }


    [XmlElement("Name_Code3")]
    public string Name_Code3 { get; set; }


    [XmlElement("Name_Code4")]
    public string Name_Code4 { get; set; }


    [XmlElement("Name_Code5")]
    public string Name_Code5 { get; set; }

}

[Serializable]
public class StatusSAP
{
    [XmlElement("dtidx")]
    public int dtidx { get; set; }

    [XmlElement("stidx")]
    public int stidx { get; set; }

    [XmlElement("status_state")]
    public int status_state { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("StatusDetail")]
    public string StatusDetail { get; set; }

    [XmlElement("downtime_name")]
    public string downtime_name { get; set; }


}

[Serializable]
public class DowntimePOS
{
    [XmlElement("r0lidx")]
    public int r0lidx { get; set; }

    [XmlElement("r0_Status")]
    public int r0_Status { get; set; }

    [XmlElement("CEmpIDX_dt")]
    public int CEmpIDX_dt { get; set; }

    [XmlElement("r0didx")]
    public int r0didx { get; set; }

    [XmlElement("StateTime")]
    public string StateTime { get; set; }

    [XmlElement("EndTime")]
    public string EndTime { get; set; }

}

[Serializable]
public class POSList
{
    [XmlElement("POS1IDX")]
    public int POS1IDX { get; set; }

    [XmlElement("POS2IDX")]
    public int POS2IDX { get; set; }

    [XmlElement("POS3IDX")]
    public int POS3IDX { get; set; }

    [XmlElement("POS4IDX")]
    public int POS4IDX { get; set; }

    [XmlElement("POS1_Name")]
    public string POS1_Name { get; set; }

    [XmlElement("POS2_Name")]
    public string POS2_Name { get; set; }

    [XmlElement("POS3_Name")]
    public string POS3_Name { get; set; }

    [XmlElement("POS4_Name")]
    public string POS4_Name { get; set; }

    [XmlElement("POS1_Code")]
    public string POS1_Code { get; set; }

    [XmlElement("POS2_Code")]
    public string POS2_Code { get; set; }

    [XmlElement("POS3_Code")]
    public string POS3_Code { get; set; }

    [XmlElement("POS4_Code")]
    public string POS4_Code { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("POS1Status")]
    public int POS1Status { get; set; }

    [XmlElement("POS2Status")]
    public int POS2Status { get; set; }

    [XmlElement("POS3Status")]
    public int POS3Status { get; set; }

    [XmlElement("POS4Status")]
    public int POS4Status { get; set; }

    [XmlElement("POSStatusDetail")]
    public string POSStatusDetail { get; set; }

    [XmlElement("Name_Code1")]
    public string Name_Code1 { get; set; }

    [XmlElement("Name_Code2")]
    public string Name_Code2 { get; set; }

    [XmlElement("Name_Code3")]
    public string Name_Code3 { get; set; }

    [XmlElement("Name_Code4")]
    public string Name_Code4 { get; set; }

    [XmlElement("POSIDX")]
    public int POSIDX { get; set; }

    [XmlElement("POSStatus")]
    public int POSStatus { get; set; }

}

[Serializable]
public class PrioritySAP
{
    [XmlElement("PIDX")]
    public int PIDX { get; set; }

    [XmlElement("Priority_name")]
    public string Priority_name { get; set; }

    [XmlElement("Priority_status")]
    public int Priority_status { get; set; }

    [XmlElement("CEmpIDX_PIDX")]
    public int CEmpIDX_PIDX { get; set; }

    [XmlElement("StatusDetail")]
    public string StatusDetail { get; set; }

    [XmlElement("baseline")]
    public string baseline { get; set; }
}

[Serializable]
public class UserRequestList
{

    [XmlElement("URQIDX")]
    public int URQIDX { get; set; }

    [XmlElement("MS1IDX")]
    public int MS1IDX { get; set; }


    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("Name_Code1")]
    public string Name_Code1 { get; set; }

    [XmlElement("MS2IDX")]
    public int MS2IDX { get; set; }

    [XmlElement("Name_Code2")]
    public string Name_Code2 { get; set; }

    [XmlElement("RecieveIDX")]
    public int RecieveIDX { get; set; }

    [XmlElement("NCEmpIDX")]
    public int NCEmpIDX { get; set; }

    [XmlElement("MS3IDX")]
    public int MS3IDX { get; set; }

    [XmlElement("Name_Code3")]
    public string Name_Code3 { get; set; }

    [XmlElement("POS1IDX")]
    public int POS1IDX { get; set; }

    [XmlElement("POS2IDX")]
    public int POS2IDX { get; set; }

    [XmlElement("POS3IDX")]
    public int POS3IDX { get; set; }

    [XmlElement("POS4IDX")]
    public int POS4IDX { get; set; }


    [XmlElement("MS4IDX")]
    public int MS4IDX { get; set; }

    [XmlElement("Name_Code4")]
    public string Name_Code4 { get; set; }

    [XmlElement("EmailIDX")]
    public int EmailIDX { get; set; }

    [XmlElement("MS5IDX")]
    public int MS5IDX { get; set; }

    [XmlElement("Link_IT")]
    public string Link_IT { get; set; }

    [XmlElement("Name_Code5")]
    public string Name_Code5 { get; set; }

    [XmlElement("CIT1IDX")]
    public int CIT1IDX { get; set; }

    [XmlElement("CIT2IDX")]
    public int CIT2IDX { get; set; }

    [XmlElement("CIT3IDX")]
    public int CIT3IDX { get; set; }

    [XmlElement("CIT4IDX")]
    public int CIT4IDX { get; set; }



    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("Priority_name")]
    public string Priority_name { get; set; }

    [XmlElement("PIDX")]
    public int PIDX { get; set; }


    [XmlElement("CStatus")]
    public int CStatus { get; set; }

    [XmlElement("IDXCMSAP")]
    public int IDXCMSAP { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("EmpIDX_add")]
    public int EmpIDX_add { get; set; }

    [XmlElement("SysIDX_add")]
    public int SysIDX_add { get; set; }

    [XmlElement("PIDX_Add")]
    public int PIDX_Add { get; set; }

    [XmlElement("DocCode")]
    public string DocCode { get; set; }

    [XmlElement("EmpCode")]
    public string EmpCode { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("Statusans")]
    public string Statusans { get; set; }

    [XmlElement("NameFull")]
    public string NameFull { get; set; }

    [XmlElement("NameCut")]
    public string NameCut { get; set; }


    [XmlElement("CommentAuto")]
    public string CommentAuto { get; set; }


    [XmlElement("CodeCut")]
    public string CodeCut { get; set; }

    [XmlElement("OLIDX")]
    public int OLIDX { get; set; }

    [XmlElement("RSecID")]
    public int RSecID { get; set; }

    [XmlElement("IFDate")]
    public int IFDate { get; set; }

    [XmlElement("r0idx")]
    public int r0idx { get; set; }

    [XmlElement("sumtime")]
    public string sumtime { get; set; }

    [XmlElement("nonkpi")]
    public string nonkpi { get; set; }

    [XmlElement("alltime")]
    public string alltime { get; set; }

    [XmlElement("overtime")]
    public string overtime { get; set; }

    [XmlElement("sumtime_tb")]
    public string sumtime_tb { get; set; }

    [XmlElement("EmpName")]
    public string EmpName { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("RDeptName")]
    public string RDeptName { get; set; }

    [XmlElement("NameEqDeviceETC")]
    public string NameEqDeviceETC { get; set; }

    [XmlElement("RPosIDX_J")]
    public int RPosIDX_J { get; set; }

    [XmlElement("PosNameTH")]
    public string PosNameTH { get; set; }

    [XmlElement("RDeptID")]
    public int RDeptID { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("LocIDX")]
    public int LocIDX { get; set; }

    [XmlElement("LocName")]
    public string LocName { get; set; }

    [XmlElement("IPAddress")]
    public string IPAddress { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("SysIDX")]
    public int SysIDX { get; set; }

    [XmlElement("SysName")]
    public string SysName { get; set; }

    [XmlElement("SysName1")]
    public string SysName1 { get; set; }

    [XmlElement("Email")]
    public string Email { get; set; }

    [XmlElement("MobileNo")]
    public string MobileNo { get; set; }

    [XmlElement("TelETC")]
    public string TelETC { get; set; }

    [XmlElement("EmailETC")]
    public string EmailETC { get; set; }

    [XmlElement("CostIDX")]
    public int CostIDX { get; set; }

    [XmlElement("CostNo")]
    public string CostNo { get; set; }

    [XmlElement("CostName")]
    public string CostName { get; set; }

    [XmlElement("CheckRemote")]
    public int CheckRemote { get; set; }

    [XmlElement("RemoteIDX")]
    public int RemoteIDX { get; set; }

    [XmlElement("RemoteName")]
    public string RemoteName { get; set; }

    [XmlElement("UserIDRemote")]
    public string UserIDRemote { get; set; }

    [XmlElement("PasswordRemote")]
    public string PasswordRemote { get; set; }

    [XmlElement("AdminIDX")]
    public int AdminIDX { get; set; }

    [XmlElement("AdminName")]
    public string AdminName { get; set; }

    [XmlElement("TimegetJob")]
    public string TimegetJob { get; set; }

    [XmlElement("DategetJob")]
    public string DategetJob { get; set; }

    [XmlElement("DategetJobSearch")]
    public string DategetJobSearch { get; set; }

    [XmlElement("TimecloseJob")]
    public string TimecloseJob { get; set; }

    [XmlElement("CreateDateUser")]
    public string CreateDateUser { get; set; }

    [XmlElement("TimeCreateJob")]
    public string TimeCreateJob { get; set; }

    [XmlElement("OtherName")]
    public string OtherName { get; set; }


    [XmlElement("DatecloseJob1")]
    public string DatecloseJob1 { get; set; }

    [XmlElement("DatecloseJob")]
    public string DatecloseJob { get; set; }

    [XmlElement("DatecloseJobSearch")]
    public string DatecloseJobSearch { get; set; }

    [XmlElement("DateReciveJobFirst")]
    public string DateReciveJobFirst { get; set; }

    [XmlElement("TimeReciveJobFirst")]
    public string TimeReciveJobFirst { get; set; }

    [XmlElement("AllContactAdmin")]
    public string AllContactAdmin { get; set; }

    [XmlElement("Comment")]
    public string Comment { get; set; }

    [XmlElement("AdminDoingIDX")]
    public int AdminDoingIDX { get; set; }


    [XmlElement("AdminDoingName")]
    public string AdminDoingName { get; set; }

    [XmlElement("CommentAMDoing")]
    public string CommentAMDoing { get; set; }

    [XmlElement("FileAMDoing")]
    public int FileAMDoing { get; set; }

    [XmlElement("DateCloseJob")]
    public string DateCloseJob { get; set; }

    [XmlElement("CCAIDX")]
    public int CCAIDX { get; set; }

    [XmlElement("SysIDXNoti")]
    public int SysIDXNoti { get; set; }

    [XmlElement("AddSort")]
    public string AddSort { get; set; }

    [XmlElement("CaseMean")]
    public string CaseMean { get; set; }

    [XmlElement("Abbreviation")]
    public string Abbreviation { get; set; }

    [XmlElement("addPlus")]
    public string addPlus { get; set; }

    [XmlElement("NameSys")]
    public string NameSys { get; set; }

    [XmlElement("StaIDX")]
    public int StaIDX { get; set; }

    [XmlElement("StaIDXComma")]
    public string StaIDXComma { get; set; }


    [XmlElement("amount")]
    public int amount { get; set; }

    [XmlElement("CountStatus")]
    public int CountStatus { get; set; }

    [XmlElement("StaName")]
    public string StaName { get; set; }

    [XmlElement("FileUser")]
    public int FileUser { get; set; }

    [XmlElement("detailUser")]
    public string detailUser { get; set; }

    [XmlElement("UserName")]
    public string UserName { get; set; }


    [XmlElement("UserLogonName")]
    public string UserLogonName { get; set; }


    [XmlElement("TransactionCode")]
    public string TransactionCode { get; set; }

    [XmlElement("DeviceIDX")]
    public int DeviceIDX { get; set; }

    [XmlElement("DeviceCode_IT")]
    public string DeviceCode_IT { get; set; }

    [XmlElement("DeviceETC")]
    public string DeviceETC { get; set; }

    [XmlElement("EmailIDX_LN")]
    public string EmailIDX_LN { get; set; }

    [XmlElement("EmailNameETC_LN")]
    public string EmailNameETC_LN { get; set; }

    [XmlElement("RepairEmail")]
    public string RepairEmail { get; set; }

    [XmlElement("DeviceIDX_LN")]
    public int DeviceIDX_LN { get; set; }

    [XmlElement("DeviceCode_LN")]
    public string DeviceCode_LN { get; set; }

    [XmlElement("DeviceETC_LN")]
    public string DeviceETC_LN { get; set; }

    [XmlElement("TypeDeviceIDX")]
    public int TypeDeviceIDX { get; set; }

    [XmlElement("TypeNameEquip_IT")]
    public string TypeNameEquip_IT { get; set; }

    [XmlElement("EndWork")]
    public string EndWork { get; set; }

    [XmlElement("SearchSystem")]
    public int SearchSystem { get; set; }

    [XmlElement("SearchLocation")]
    public int SearchLocation { get; set; }

    [XmlElement("SearchFirstName")]
    public string SearchFirstName { get; set; }


    [XmlElement("SearchLastName")]
    public string SearchLastName { get; set; }

    [XmlElement("SearchOrganize")]
    public int SearchOrganize { get; set; }

    [XmlElement("SearchDept")]
    public int SearchDept { get; set; }

    [XmlElement("SearchSatatus")]
    public int SearchSatatus { get; set; }

    [XmlElement("IFSearch")]
    public int IFSearch { get; set; }

    [XmlElement("IFSearchbetween")]
    public int IFSearchbetween { get; set; }

    [XmlElement("SearchDate")]
    public string SearchDate { get; set; }

    [XmlElement("Code")]
    public string Code { get; set; }

    [XmlElement("CountCase")]
    public int CountCase { get; set; }

    [XmlElement("CountEmail")]
    public int CountEmail { get; set; }

    [XmlElement("CountSAP")]
    public int CountSAP { get; set; }

    [XmlElement("CountIT")]
    public int CountIT { get; set; }

    [XmlElement("CountComputer")]
    public int CountComputer { get; set; }

    [XmlElement("CountMonitor")]
    public int CountMonitor { get; set; }

    [XmlElement("CountNotebook")]
    public int CountNotebook { get; set; }

    [XmlElement("CountPrinter")]
    public int CountPrinter { get; set; }

    [XmlElement("CountInternet")]
    public int CountInternet { get; set; }

    [XmlElement("CountSoftware")]
    public int CountSoftware { get; set; }

    [XmlElement("CountOther")]
    public int CountOther { get; set; }

    [XmlElement("Name1")]
    public string Name1 { get; set; }

    [XmlElement("Name2")]
    public string Name2 { get; set; }

    [XmlElement("Name3")]
    public string Name3 { get; set; }

    [XmlElement("Name4")]
    public string Name4 { get; set; }

    [XmlElement("Name5")]
    public string Name5 { get; set; }

    [XmlElement("Name6")]
    public string Name6 { get; set; }

    [XmlElement("Name7")]
    public string Name7 { get; set; }

    [XmlElement("Name8")]
    public string Name8 { get; set; }

    [XmlElement("Name9")]
    public string Name9 { get; set; }

    [XmlElement("Name10")]
    public string Name10 { get; set; }

    [XmlElement("Name11")]
    public string Name11 { get; set; }

    [XmlElement("Name12")]
    public string Name12 { get; set; }

    [XmlElement("Name13")]
    public string Name13 { get; set; }

    [XmlElement("ManHours")]
    public int ManHours { get; set; }

    [XmlElement("Link")]
    public string Link { get; set; }

    [XmlElement("SapMsg")]
    public string SapMsg { get; set; }

    [XmlElement("CMIDX")]
    public int CMIDX { get; set; }

    [XmlElement("node_status")]
    public int node_status { get; set; }

    [XmlElement("Name_Code6")]
    public string Name_Code6 { get; set; }

    [XmlElement("Name_Code7")]
    public string Name_Code7 { get; set; }

    [XmlElement("Name_Code8")]
    public string Name_Code8 { get; set; }


    [XmlElement("CommentAutoIT")]
    public string CommentAutoIT { get; set; }

    [XmlElement("CDate")]
    public string CDate { get; set; }

    [XmlElement("CTime")]
    public string CTime { get; set; }

    [XmlElement("ISO_User")]
    public string ISO_User { get; set; }

    [XmlElement("ChkOrg")]
    public string ChkOrg { get; set; }

    [XmlElement("ChkOrgGM")]
    public string ChkOrgGM { get; set; }

    [XmlElement("ChkSys")]
    public string ChkSys { get; set; }

    [XmlElement("SysIDXGM")]
    public int SysIDXGM { get; set; }


    [XmlElement("ChkPlace")]
    public string ChkPlace { get; set; }

    [XmlElement("ChkAdaccept")]
    public string ChkAdaccept { get; set; }

    [XmlElement("ChkAddoing")]
    public string ChkAddoing { get; set; }

    [XmlElement("DC_YEAR")]
    public string DC_YEAR { get; set; }

    [XmlElement("DC_Mount")]
    public string DC_Mount { get; set; }

    [XmlElement("countit1id")]
    public int countit1id { get; set; }

    [XmlElement("POS1_Name")]
    public string POS1_Name { get; set; }

    [XmlElement("countit2id")]
    public int countit2id { get; set; }

    [XmlElement("POS2_Name")]
    public string POS2_Name { get; set; }

    [XmlElement("countit3id")]
    public int countit3id { get; set; }

    [XmlElement("POS3_Name")]
    public string POS3_Name { get; set; }

    [XmlElement("countit4id")]
    public int countit4id { get; set; }

    [XmlElement("POS4_Name")]
    public string POS4_Name { get; set; }

    [XmlElement("CIT1_Code")]
    public string CIT1_Code { get; set; }

    [XmlElement("CIT2_Code")]
    public string CIT2_Code { get; set; }

    [XmlElement("CIT3_Name")]
    public string CIT3_Name { get; set; }

    [XmlElement("CIT4_Code")]
    public string CIT4_Code { get; set; }

    [XmlElement("MS1_Code")]
    public string MS1_Code { get; set; }

    [XmlElement("MS2_Code")]
    public string MS2_Code { get; set; }


    [XmlElement("MS3_Code")]
    public string MS3_Code { get; set; }

    [XmlElement("cc")]
    public int cc { get; set; }

    [XmlElement("Name")]
    public string Name { get; set; }

    [XmlElement("Downtime_KPI")]
    public string Downtime_KPI { get; set; }

    [XmlElement("Downtim_NONKPI")]
    public string Downtim_NONKPI { get; set; }

    [XmlElement("ALLTIME")]
    public string ALLTIME { get; set; }

    [XmlElement("baseline")]
    public string baseline { get; set; }


    [XmlElement("countlist_downtime")]
    public string countlist_downtime { get; set; }


    [XmlElement("total_overtime")]
    public string total_overtime { get; set; }

    [XmlElement("res_devicesidx")]
    public int res_devicesidx { get; set; }

    [XmlElement("devices_name")]
    public string devices_name { get; set; }

    [XmlElement("enidx")]
    public int enidx { get; set; }

    [XmlElement("en_name")]
    public string en_name { get; set; }

    [XmlElement("RES1IDX")]
    public int RES1IDX { get; set; }

    [XmlElement("RES2IDX")]
    public int RES2IDX { get; set; }

    [XmlElement("RES3IDX")]
    public int RES3IDX { get; set; }

    [XmlElement("RES4IDX")]
    public int RES4IDX { get; set; }

    [XmlElement("price")]
    public string price { get; set; }

    [XmlElement("RES1_Name")]
    public string RES1_Name { get; set; }

    [XmlElement("RES2_Name")]
    public string RES2_Name { get; set; }

    [XmlElement("RES3_Name")]
    public string RES3_Name { get; set; }

    [XmlElement("RES4_Name")]
    public string RES4_Name { get; set; }


}

[Serializable]
public class FeedBackList
{
    [XmlElement("URQIDX_Q")]
    public int URQIDX_Q { get; set; }

    [XmlElement("UIDX")]
    public int UIDX { get; set; }

    [XmlElement("SumPoint")]
    public int SumPoint { get; set; }

    [XmlElement("WFIDX")]
    public int WFIDX { get; set; }

    [XmlElement("AQSIDX")]
    public int AQSIDX { get; set; }

    [XmlElement("PO_MFBIDX2")]
    public int PO_MFBIDX2 { get; set; }

    [XmlElement("U1IDX")]
    public int U1IDX { get; set; }

    [XmlElement("MFBIDX")]
    public int MFBIDX { get; set; }

    [XmlElement("U0AQS")]
    public int U0AQS { get; set; }

    [XmlElement("POIDX")]
    public int POIDX { get; set; }

    [XmlElement("Other")]
    public string Other { get; set; }

    [XmlElement("Topic")]
    public string Topic { get; set; }

    [XmlElement("Question")]
    public string Question { get; set; }

}

[Serializable]
public class SystemtList
{
    [XmlElement("SysIDX")]
    public int SysIDX { get; set; }

    [XmlElement("SysNameTH")]
    public string SysNameTH { get; set; }

    [XmlElement("stidx")]
    public int stidx { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }



}

[Serializable]
public class ExportDataPOS
{
    [XmlElement("รหัสเอกสาร")]
    public string รหัสเอกสาร { get; set; }

    [XmlElement("ระบบ")]
    public string ระบบ { get; set; }

    [XmlElement("ชื่อผู้แจ้ง")]
    public string ชื่อผู้แจ้ง { get; set; }

    [XmlElement("ฝ่าย")]
    public string ฝ่าย { get; set; }

    [XmlElement("สถานที่")]
    public string สถานที่ { get; set; }

    [XmlElement("หมายเหตุแจ้งซ่อม")]
    public string หมายเหตุแจ้งซ่อม { get; set; }

    [XmlElement("วันที่แจ้งซ่อม")]
    public string วันที่แจ้งซ่อม { get; set; }


    [XmlElement("สถานะ")]
    public string สถานะ { get; set; }

    [XmlElement("ชื่อผู้รับงาน")]
    public string ชื่อผู้รับงาน { get; set; }

    [XmlElement("วันที่รับงาน")]
    public string วันที่รับงาน { get; set; }

    [XmlElement("ชื่อผู้ปิดงาน")]
    public string ชื่อผู้ปิดงาน { get; set; }

    [XmlElement("วันที่ปิดงาน")]
    public string วันที่ปิดงาน { get; set; }

    [XmlElement("ความเห็นผู้ดูแล")]
    public string ความเห็นผู้ดูแล { get; set; }

    [XmlElement("เคส")]
    public string เคส { get; set; }

    [XmlElement("อาการ")]
    public string อาการ { get; set; }

    [XmlElement("แก้ไข")]
    public string แก้ไข { get; set; }

    [XmlElement("สาเหตุ")]
    public string สาเหตุ { get; set; }


    [XmlElement("Downtime_KPI")]
    public string Downtime_KPI { get; set; }

    [XmlElement("Downtim_NONKPI")]
    public string Downtim_NONKPI { get; set; }

    [XmlElement("ALLTIME")]
    public string ALLTIME { get; set; }

}

[Serializable]
public class ExportDataSAP
{

    [XmlElement("หมายเลขเอกสาร")]
    public string หมายเลขเอกสาร { get; set; }

    [XmlElement("รหัสผู้สร้าง")]
    public string รหัสผู้สร้าง { get; set; }

    [XmlElement("ชื่อผู้สร้าง")]
    public string ชื่อผู้สร้าง { get; set; }

    [XmlElement("Email")]
    public string Email { get; set; }

    [XmlElement("ชื่อแผนกผู้สร้าง")]
    public string ชื่อแผนกผู้สร้าง { get; set; }

    [XmlElement("ชื่อฝ่ายผู้สร้าง")]
    public string ชื่อฝ่ายผู้สร้าง { get; set; }

    [XmlElement("ตัวย่อฝ่าย")]
    public string ตัวย่อฝ่าย { get; set; }

    [XmlElement("รหัสสถานที่")]
    public int รหัสสถานที่ { get; set; }

    [XmlElement("สถานที่")]
    public string สถานที่ { get; set; }

    [XmlElement("รหัสองค์กร")]
    public int รหัสองค์กร { get; set; }

    [XmlElement("ชื่อองค์กร")]
    public string ชื่อองค์กร { get; set; }

    [XmlElement("รหัสระบบ")]
    public int รหัสระบบ { get; set; }

    [XmlElement("ระบบ")]
    public string ระบบ { get; set; }

    [XmlElement("รหัสCostCenter")]
    public int รหัสCostCenter { get; set; }

    [XmlElement("CostCenter")]
    public string CostCenter { get; set; }

    [XmlElement("รหัสผู้รับงาน")]
    public string รหัสผู้รับงาน { get; set; }

    [XmlElement("ชื่อผู้รับงาน")]
    public string ชื่อผู้รับงาน { get; set; }

    [XmlElement("วันที่รับงาน")]
    public string วันที่รับงาน { get; set; }

    [XmlElement("เวลาที่รับงาน")]
    public string เวลาที่รับงาน { get; set; }

    [XmlElement("รหัสผู้ปิดงาน")]
    public string รหัสผู้ปิดงาน { get; set; }

    [XmlElement("ชื่อผู้ปิดงาน")]
    public string ชื่อผู้ปิดงาน { get; set; }

    [XmlElement("วันที่ปิดงาน")]
    public string วันที่ปิดงาน { get; set; }

    [XmlElement("เวลาที่ปิดงาน")]
    public string เวลาที่ปิดงาน { get; set; }

    [XmlElement("สถานะเอกสาร")]
    public string สถานะเอกสาร { get; set; }

    [XmlElement("SAP_Log_ON")]
    public string SAP_Log_ON { get; set; }

    [XmlElement("TCode")]
    public string TCode { get; set; }

    [XmlElement("ปิดงานLV1")]
    public string ปิดงานLV1 { get; set; }

    [XmlElement("ปิดงานLV2")]
    public string ปิดงานLV2 { get; set; }

    [XmlElement("ปิดงานLV3")]
    public string ปิดงานLV3 { get; set; }

    [XmlElement("ปิดงานLV4")]
    public string ปิดงานLV4 { get; set; }

    [XmlElement("ปิดงานLV5")]
    public string ปิดงานLV5 { get; set; }

    [XmlElement("ManHours")]
    public int ManHours { get; set; }

    [XmlElement("Link")]
    public string Link { get; set; }

    [XmlElement("SapMsg")]
    public string SapMsg { get; set; }

    [XmlElement("คอมเม้นท์ผู้ปิดงาน")]
    public string คอมเม้นท์ผู้ปิดงาน { get; set; }

    [XmlElement("คอมเม้นท์ผู้สร้าง")]
    public string คอมเม้นท์ผู้สร้าง { get; set; }

    [XmlElement("วันที่แจ้ง")]
    public string วันที่แจ้ง { get; set; }

    [XmlElement("รหัสความสำคัญ")]
    public int รหัสความสำคัญ { get; set; }

    [XmlElement("ความสำคัญ")]
    public string ความสำคัญ { get; set; }

    [XmlElement("DowntimeHour")]
    public string DowntimeHour { get; set; }

    [XmlElement("DowntimeMin")]
    public string DowntimeMin { get; set; }

}

[Serializable]
public class ReportSAP
{

    [XmlElement("Downtime")]
    public string Downtime { get; set; }

    [XmlElement("URQIDX")]
    public int URQIDX { get; set; }

    [XmlElement("DocCode")]
    public string DocCode { get; set; }

    [XmlElement("EmpIDX")]
    public int EmpIDX { get; set; }

    [XmlElement("AdminDoingIDX")]
    public int AdminDoingIDX { get; set; }

    [XmlElement("AdminIDX")]
    public int AdminIDX { get; set; }

    [XmlElement("EmpCode")]
    public string EmpCode { get; set; }

    [XmlElement("EmpName")]
    public string EmpName { get; set; }

    [XmlElement("Saplink")]
    public string Saplink { get; set; }

    [XmlElement("ITlink")]
    public string ITlink { get; set; }

    [XmlElement("AdminName")]
    public string AdminName { get; set; }

    [XmlElement("AdminDoingName")]
    public string AdminDoingName { get; set; }

    [XmlElement("RPosName")]
    public string RPosName { get; set; }

    [XmlElement("RDeptName")]
    public string RDeptName { get; set; }

    [XmlElement("alltime")]
    public string alltime { get; set; }

    [XmlElement("CostCenter")]
    public string CostCenter { get; set; }

    [XmlElement("Name_Code1")]
    public string Name_Code1 { get; set; }

    [XmlElement("Name_Code2")]
    public string Name_Code2 { get; set; }

    [XmlElement("Name_Code3")]
    public string Name_Code3 { get; set; }

    [XmlElement("Name_Code4")]
    public string Name_Code4 { get; set; }

    [XmlElement("Name_Code5")]
    public string Name_Code5 { get; set; }

    [XmlElement("DG_Mount")]
    public string DG_Mount { get; set; }

    [XmlElement("DG_Year")]
    public string DG_Year { get; set; }

    [XmlElement("StaName")]
    public string StaName { get; set; }




}


[Serializable]
public class ExportDataIT
{

    [XmlElement("ประเภท")]
    public string ประเภท { get; set; }

    [XmlElement("เวลา")]
    public string เวลา { get; set; }

    [XmlElement("ช่วงเวลาแจ้งซ่อมถึงช่วงรับงาน")]
    public string ช่วงเวลาแจ้งซ่อมถึงช่วงรับงาน { get; set; }

    [XmlElement("ช่วงเวลารับงานถึงช่วงปิดงาน")]
    public string ช่วงเวลารับงานถึงช่วงปิดงาน { get; set; }


    [XmlElement("รหัสเอกสาร")]
    public string รหัสเอกสาร { get; set; }

    [XmlElement("สถานที่แจ้งซ่อม")]
    public string สถานที่แจ้งซ่อม { get; set; }

    [XmlElement("วันที่แจ้งซ่อม")]
    public string วันที่แจ้งซ่อม { get; set; }

    [XmlElement("ชื่อผู้แจ้ง")]
    public string ชื่อผู้แจ้ง { get; set; }

    [XmlElement("ฝ่าย")]
    public string ฝ่าย { get; set; }

    [XmlElement("หมายเหตุแจ้งซ่อม")]
    public string หมายเหตุแจ้งซ่อม { get; set; }


    [XmlElement("สถานะ")]
    public string สถานะ { get; set; }

    [XmlElement("ชื่อผู้รับงาน")]
    public string ชื่อผู้รับงาน { get; set; }


    [XmlElement("วันที่รับงาน")]
    public string วันที่รับงาน { get; set; }

    [XmlElement("ชื่อผู้ปิดงาน")]
    public string ชื่อผู้ปิดงาน { get; set; }

    [XmlElement("วันที่ปิดงาน")]
    public string วันที่ปิดงาน { get; set; }


    [XmlElement("รวมเวลา")]
    public string รวมเวลา { get; set; }

    [XmlElement("หมายเหตุ")]
    public string หมายเหตุ { get; set; }

    [XmlElement("ปิดงาน")]
    public string ปิดงาน { get; set; }


    [XmlElement("เคสย่อ")]
    public string เคสย่อ { get; set; }

    [XmlElement("อาการย่อ")]
    public string อาการย่อ { get; set; }

    [XmlElement("แก้ไขย่อ")]
    public string แก้ไขย่อ { get; set; }

    [XmlElement("สาเหตุย่อ")]
    public string สาเหตุย่อ { get; set; }

    [XmlElement("เคส")]
    public string เคส { get; set; }

    [XmlElement("อาการ")]
    public string อาการ { get; set; }

    [XmlElement("แก้ไข")]
    public string แก้ไข { get; set; }

    [XmlElement("สาเหตุ")]
    public string สาเหตุ { get; set; }

}

[Serializable]
public class ShortDepartment
{
    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("DeptNameEN")]
    public string DeptNameEN { get; set; }

    [XmlElement("ShortDept")]
    public string ShortDept { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("DeptIDX")]
    public int DeptIDX { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }




}


[Serializable]
public class CaseITlist
{
    [XmlElement("CIT1IDX")]
    public int CIT1IDX { get; set; }

    [XmlElement("CIT2IDX")]
    public int CIT2IDX { get; set; }

    [XmlElement("CIT3IDX")]
    public int CIT3IDX { get; set; }

    [XmlElement("CIT4IDX")]
    public int CIT4IDX { get; set; }

    [XmlElement("CEmpIDX")]
    public int CEmpIDX { get; set; }

    [XmlElement("CIT1Status")]
    public int CIT1Status { get; set; }

    [XmlElement("CIT2Status")]
    public int CIT2Status { get; set; }

    [XmlElement("CIT3Status")]
    public int CIT3Status { get; set; }

    [XmlElement("CIT4Status")]
    public int CIT4Status { get; set; }



    [XmlElement("CIT1_Name")]
    public string CIT1_Name { get; set; }

    [XmlElement("CIT2_Name")]
    public string CIT2_Name { get; set; }

    [XmlElement("CIT3_Name")]
    public string CIT3_Name { get; set; }

    [XmlElement("CIT4_Name")]
    public string CIT4_Name { get; set; }


    [XmlElement("CIT1_Code")]
    public string CIT1_Code { get; set; }

    [XmlElement("CIT2_Code")]
    public string CIT2_Code { get; set; }

    [XmlElement("CIT3_Code")]
    public string CIT3_Code { get; set; }

    [XmlElement("CIT4_Code")]
    public string CIT4_Code { get; set; }

    [XmlElement("CIT1StatusDetail")]
    public string CIT1StatusDetail { get; set; }

    [XmlElement("CIT2StatusDetail")]
    public string CIT2StatusDetail { get; set; }

    [XmlElement("CIT3StatusDetail")]
    public string CIT3StatusDetail { get; set; }

    [XmlElement("CIT4StatusDetail")]
    public string CIT4StatusDetail { get; set; }

}

[Serializable]
public class ExportDataRES
{

    [XmlElement("price")]
    public string price { get; set; }

    [XmlElement("ประเภท")]
    public string ประเภท { get; set; }

    [XmlElement("เวลา")]
    public string เวลา { get; set; }

    [XmlElement("ช่วงเวลาแจ้งซ่อมถึงช่วงรับงาน")]
    public string ช่วงเวลาแจ้งซ่อมถึงช่วงรับงาน { get; set; }

    [XmlElement("ช่วงเวลารับงานถึงช่วงปิดงาน")]
    public string ช่วงเวลารับงานถึงช่วงปิดงาน { get; set; }

    [XmlElement("รหัสเอกสาร")]
    public string รหัสเอกสาร { get; set; }

    [XmlElement("สถานที่แจ้งซ่อม")]
    public string สถานที่แจ้งซ่อม { get; set; }

    [XmlElement("วันที่แจ้งซ่อม")]
    public string วันที่แจ้งซ่อม { get; set; }

    [XmlElement("ชื่อผู้แจ้ง")]
    public string ชื่อผู้แจ้ง { get; set; }

    [XmlElement("ฝ่าย")]
    public string ฝ่าย { get; set; }

    [XmlElement("อุปกรณ์")]
    public string อุปกรณ์ { get; set; }

    [XmlElement("หมายเหตุแจ้งซ่อม")]
    public string หมายเหตุแจ้งซ่อม { get; set; }

    [XmlElement("สถานะ")]
    public string สถานะ { get; set; }

    [XmlElement("ชื่อผู้รับงาน")]
    public string ชื่อผู้รับงาน { get; set; }


    [XmlElement("วันที่รับงาน")]
    public string วันที่รับงาน { get; set; }

    [XmlElement("ช่างดำเนินการ")]
    public string ช่างดำเนินการ { get; set; }


    [XmlElement("ชื่อผู้ปิดงาน")]
    public string ชื่อผู้ปิดงาน { get; set; }

    [XmlElement("วันที่ปิดงาน")]
    public string วันที่ปิดงาน { get; set; }


    [XmlElement("รวมเวลา")]
    public string รวมเวลา { get; set; }

    [XmlElement("หมายเหตุ")]
    public string หมายเหตุ { get; set; }

    [XmlElement("หัวข้อ")]
    public string หัวข้อ { get; set; }

    [XmlElement("รายการ")]
    public string รายการ { get; set; }

    [XmlElement("วิธีแก้ไข")]
    public string วิธีแก้ไข { get; set; }

    [XmlElement("คำแนะนำ")]
    public string คำแนะนำ { get; set; }


}
