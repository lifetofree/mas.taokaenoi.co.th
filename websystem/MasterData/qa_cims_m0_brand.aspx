﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_cims_m0_brand.aspx.cs" Inherits="websystem_MasterData_qa_cims_m0_brand" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="test_place" runat="server"></asp:Literal>
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">

            <div class="form-group">
                <asp:LinkButton ID="btn_addbrand" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่มชื่อยี่ห้อ/รุ่น" data-toggle="tooltip" title="เพิ่มชื่อยี่ห้อ/รุ่น" runat="server"
                    CommandName="cmdAddBrand" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มชื่อยี่ห้อ/รุ่น</asp:LinkButton>
            </div>

            <asp:FormView ID="Fv_Insert_Result" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มชื่อยี่ห้อ/รุ่น</h3>

                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">

                                <div class="form-horizontal" role="form">

                                    <div class="panel-heading">

                                        <div class="form-group">
                                            <asp:Label ID="lblbrand_name" runat="server" Text="ชื่อยี่ห้อ/รุ่น" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtbrand_name" runat="server" CssClass="form-control" placeholder="กรอกชื่อยี่ห้อ/รุ่น ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="Re_brand_name" runat="server"
                                                    ControlToValidate="txtbrand_name" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อยี่ห้อ/รุ่น" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_bname" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_brand_name" Width="220" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lblbrand_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddBrand" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="Lbtn_submit_brand" ValidationGroup="Save" CssClass="btn btn-success" Text="Save" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="Lbtn_submit_brand" OnCommand="btnCommand"></asp:LinkButton>
                                                <asp:LinkButton ID="Lbtn_cancel_brand" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="Lbtn_cancel_brand" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <%--select Place--%>
            <asp:GridView ID="Gv_select_brand" runat="server" Visible="true"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>ไม่มีข้อมูลยี่ห้อ/รุ่น</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center; padding-top: 5px;">
                                <asp:Label ID="brand_idx" runat="server" Visible="false" Text='<%# Eval("brand_idx") %>' />
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="ID_brand" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("brand_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_brand_name" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อยี่ห้อ/รุ่น" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="Name_brand" runat="server" CssClass="form-control " Text='<%# Eval("brand_name") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorbrand_name" runat="server"
                                                ControlToValidate="Name_brand" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อยี่ห้อ/รุ่น" ValidationGroup="Editbname" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_bname" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorbrand_name" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_brand" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddEdit_brand" Text='<%# Eval("brand_status") %>'
                                                CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editbname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อยี่ห้อ/รุ่น" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbbrand_name" runat="server"
                                        Text='<%# Eval("brand_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbpbrand_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("brand_status") %>'></asp:Label>
                                    <asp:Label ID="brand_statusOnline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="brand_statusOffline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="btnTodelete_brand"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบยี่ห้อ/รุ่นนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("brand_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
    </asp:MultiView>


</asp:Content>
