﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="el_Trn_plan_course.aspx.cs" Inherits="websystem_el_Trn_plan_course" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);

            var printWindow = window.open('', '', 'height=1000,width=800');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
        }
    </script>


    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:Literal ID="litkeg" runat="server"></asp:Literal>
    <asp:Panel ID="Panel3" runat="server" CssClass="m-t-10">
        <asp:Panel ID="panelMenu" runat="server">
            <nav class="navbar navbar-default">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <%-- <a class="navbar-brand">Menu</a>--%>
                </div>
                <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left" runat="server">
                        <li id="liListData" runat="server">
                            <asp:LinkButton ID="btnListData" runat="server" Font-Size="10"
                                OnCommand="btnCommand" CommandName="btnListData" Text="ข้อมูลทั่วไป" />
                        </li>

                        <li id="li_courseplan" runat="server">
                            <asp:LinkButton ID="btnInsert" runat="server" Font-Size="10"
                                OnCommand="btnCommand" CommandName="btnInsert" Text="คอร์สอบรม" />
                        </li>

                        <%-- <li id="li_courseplan" runat="server" class="dropdown" style="font-size: 13px;">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                                aria-expanded="false">คอร์สอบรม <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li id="liInsert" runat="server">
                                    <asp:LinkButton ID="btnInsert" runat="server" Font-Size="10"
                                        OnCommand="btnCommand" CommandName="btnInsert" Text="In Plan" />
                                </li>
                                <li id="licourse_out_plan" runat="server">
                                    <asp:LinkButton ID="btncourse_out_plan" runat="server" Font-Size="10"
                                        OnCommand="btnCommand" CommandName="btncourse_out_plan" Text="Out Plan" />
                                </li>
                            </ul>
                        </li>--%>



                        <li id="liplanReport" runat="server" class="dropdown" style="font-size: 13px;">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                                aria-expanded="false">แผนการอบรม <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li id="lischeduler" runat="server" visible="true">
                                    <asp:LinkButton ID="btnscheduler" runat="server" Font-Size="10"
                                        OnCommand="btnCommand" CommandName="btnscheduler" Text="ตารางแผนการอบรม" />
                                </li>
                                <li id="lirptscheduler" runat="server" visible="true">
                                    <asp:LinkButton ID="btnrptscheduler" runat="server" Font-Size="10"
                                        OnCommand="btnCommand" CommandName="btnrptscheduler" Text="รายงานแผนการอบรม" />
                                </li>
                            </ul>
                        </li>
                        <li id="liApprove" runat="server" visible="true">
                            <%--  <asp:Label ID="Label104" runat="server" Text="รายการรออนุมัติ" Visible="true"></asp:Label>--%>
                            <asp:LinkButton ID="lbApprove" runat="server" Font-Size="10"
                                OnCommand="btnCommand" CommandName="btnApprove" Text="รายการรออนุมัติ / รอแก้ไขข้อมูล">
                                <asp:Label ID="nav_approve" Font-Bold="true" runat="server"></asp:Label>
                            </asp:LinkButton>
                        </li>

                        <li id="liHR_Wait" runat="server" visible="false">
                            <asp:Label ID="lbHR_Wait" runat="server" Text="รายการที่รอ HROD Manager อนุมัติ" Visible="false"></asp:Label>
                            <asp:LinkButton ID="btnHR_Wait" runat="server" Font-Size="10"
                                OnCommand="btnCommand" CommandName="btnHR_Wait" Text="รายการที่รอ HROD Manager อนุมัติ" />
                        </li>
                        <li id="liHR_App" runat="server" visible="false">
                            <asp:Label ID="lbHR_App" runat="server" Text="รายการที่ HROD Manager อนุมัติเสร็จแล้ว" Visible="false"></asp:Label>
                            <asp:LinkButton ID="btnHR_App" runat="server" Font-Size="10"
                                OnCommand="btnCommand" CommandName="btnHR_App" Text="รายการที่ HROD Manager อนุมัติเสร็จแล้ว" />
                        </li>
                        <li id="liMD_Wait" runat="server" visible="false">
                            <asp:Label ID="lbMD_Wait" runat="server" Text="รายการที่รอ HROD Manager อนุมัติ" Visible="false"></asp:Label>
                            <asp:LinkButton ID="btnMD_Wait" runat="server" Font-Size="10"
                                OnCommand="btnCommand" CommandName="btnMD_Wait" Text="รายการที่รอ HROD Manager อนุมัติ" />
                        </li>
                        <li id="liMD_App" runat="server" visible="false">
                            <asp:Label ID="lbMD_App" runat="server" Text="รายการที่ Chief People Officer อนุมัติเสร็จแล้ว" Visible="false"></asp:Label>
                            <asp:LinkButton ID="btnMD_App" runat="server" Font-Size="10"
                                OnCommand="btnCommand" CommandName="btnMD_App" Text="รายการที่ Chief People Officer อนุมัติเสร็จแล้ว" />
                        </li>
                        <li id="lisummary" runat="server" visible="true">
                            <asp:Label ID="lbsummary" runat="server" Text="ผลการฝึกอบรม" Visible="false"></asp:Label>
                            <asp:LinkButton ID="btnsummary" runat="server" Font-Size="10"
                                OnCommand="btnCommand" CommandName="btnsummary" Text="ผลการฝึกอบรม" />
                        </li>
                        <%-- jobgerd 7 header --%>
                        <li id="liLeaderList" runat="server" visible="false">
                            <asp:Label ID="lbLeaderList" runat="server" Text="รายการที่รออนุมัติ" Visible="false"></asp:Label>
                            <asp:LinkButton ID="btnLeaderList" runat="server" Font-Size="10"
                                OnCommand="btnCommand" CommandName="btnLeaderList" Text="รายการที่รออนุมัติ" />
                        </li>
                        <li id="liLeaderApproveList" runat="server" visible="false">
                            <asp:Label ID="lbLeaderApproveList" runat="server" Text="รายการที่อนุมัติเสร็จแล้ว" Visible="false"></asp:Label>
                            <asp:LinkButton ID="btnLeaderApproveList" runat="server" Font-Size="10"
                                OnCommand="btnCommand" CommandName="btnLeaderApproveList" Text="รายการที่อนุมัติเสร็จแล้ว" />
                        </li>

                    </ul>
                </div>
            </nav>

            <div class="clearfix"></div>

        </asp:Panel>

    </asp:Panel>

    <asp:MultiView ID="MultiViewBody" runat="server" ActiveViewIndex="0">

        <asp:View ID="View_ListDataPag" runat="server">

            <asp:Panel ID="pnlListData" runat="server">

                <asp:Panel ID="pnlsearch" runat="server">
                    <div class="panel panel-primary m-t-10">
                        <div class="panel-heading f-bold">ค้นหา</div>
                        <div class="panel-body">
                            <asp:Panel ID="Panel6" runat="server" Visible="false">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>เดือน</label>
                                        <asp:DropDownList ID="ddlMonthSearch_L" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </asp:Panel>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>ปี</label>
                                    <asp:DropDownList ID="ddlYearSearch_L" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                    <asp:TextBox ID="txtFilterKeyword_L" runat="server"
                                        CssClass="form-control"
                                        placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                                </div>
                            </div>
                            <div class="col-sm-2" runat="server" visible="false">
                                <div class="form-group">
                                    <label>สถานะของเอกสาร</label>
                                    <asp:DropDownList ID="ddlStatusapprove_L" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="999" Text="-" Selected="True" />
                                        <asp:ListItem Value="0" Text="ดำเนินการ" />
                                        <asp:ListItem Value="4" Text="อนุมัติ" />
                                        <asp:ListItem Value="5" Text="กลับไปแก้ไข" />
                                        <asp:ListItem Value="6" Text="ไม่อนุมัติ" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <asp:Panel ID="pnlScourse_plan_status" runat="server">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>ประเภทหลักสูตร</label>
                                        <asp:DropDownList ID="ddlcourse_plan_status_L" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="" Text="-" Selected="True" />
                                            <asp:ListItem Value="I" Text="In Plan" />
                                            <asp:ListItem Value="O" Text="Out Plan" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </asp:Panel>
                            <div class="col-sm-2">
                                <label>
                                    &nbsp;
                               
                                </label>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnFilter" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                        OnCommand="btnCommand" CommandName="btnFilter" />

                                    <asp:LinkButton ID="btnRefresh" runat="server" CssClass="btn btn-default" Text="<i class='fa fa-refresh'></i> ล้างค่า"
                                        OnCommand="btnCommand" CommandName="btnRefresh" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <div class="row">
                    <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="GvListData"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                                HeaderStyle-CssClass="info"
                                AllowPaging="true"
                                PageSize="10"
                                OnRowCommand="onRowCommand"
                                OnRowDataBound="onRowDataBound"
                                OnPageIndexChanging="gvPageIndexChanging"
                                DataKeyNames="u0_training_course_idx"
                                ShowFooter="false">
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                        HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%">
                                        <ItemTemplate>
                                            <div class="word-wrap" style="font-size: 13px;">
                                                <%# (Container.DataItemIndex + 1) %>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                        HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                                        <ItemTemplate>
                                            <div class="word-wrap" style="font-size: 13px;">
                                                <asp:Label ID="lblunidx" runat="server" Visible="false" Text='<%# Eval("to_m0_node_idx") %>'></asp:Label>
                                                <%# Eval("training_course_no") %>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                        HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                                        <ItemTemplate>
                                            <div class="word-wrap" style="font-size: 13px;">
                                                <%# Eval("zdate") %>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ประเภทหลักสูตร" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                        HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                        <ItemTemplate>
                                            <div class="word-wrap" style="font-size: 13px;">
                                                <%-- <%# course_plan_status_name((string)Eval("course_plan_status")) %>--%>
                                                <img src='<%# getImageIO((string)Eval("course_plan_status"),1) %>'
                                                    width="50" height="50"
                                                    title='<%# getImageIO((string)Eval("course_plan_status"),2) %>'
                                                    id="imgicon_inplan" runat="server" visible="true" />
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชื่อหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                        HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <div class="word-wrap" style="font-size: 13px;">
                                                <%# Eval("course_name") %>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- <asp:TemplateField HeaderText="กลุ่มวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size: 13px;">
                                        <%# Eval("training_group_name") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สาขาวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size: 13px;">
                                        <%# Eval("training_branch_name") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>--%>

                                    <asp:TemplateField HeaderText="สถานะของเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                        HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <b>
                                                <asp:Label ID="lblStatusDoc" runat="server" Visible="true" Text='<%# Eval("StatusDoc") %>'></asp:Label>
                                                <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("el_approve_status") %>'></asp:Label>
                                                <asp:Label ID="lblCempIDX" runat="server" Visible="false" Text='<%# Eval("training_course_created_by") %>'></asp:Label>

                                            </b>


                                            <%-- <span>
                                        <%# getTextAction((int)Eval("zacter_status"),(string)Eval("zdecision")) %>
                                    </span>--%>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                        HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                        <ItemTemplate>
                                            <div class="word-wrap">
                                                <%# getStatus( (int)Eval("training_course_status")) %>
                                                <br />
                                                <%# getStatus_Resulte( (int)Eval("zcount_resulte")) %>
                                            </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                        <ItemTemplate>


                                            <asp:TextBox ID="txtapprove_status_GvListData" runat="server"
                                                Visible="false" Text='<%# Eval("approve_status") %>' />
                                            <asp:TextBox ID="txtmd_approve_status_GvListData" runat="server"
                                                Visible="false" Text='<%# Eval("md_approve_status") %>' />
                                            <asp:TextBox ID="txtGvtraining_course_no" runat="server"
                                                Visible="false" Text='<%# Eval("training_course_no") %>' />
                                            <asp:TextBox ID="txtGvzacter_status" runat="server"
                                                Visible="false" Text='<%# Eval("zacter_status") %>' />
                                            <asp:TextBox ID="txtGvsuper_app_status" runat="server"
                                                Visible="false" Text='<%# Eval("super_app_status") %>' />

                                            <asp:LinkButton ID="btnqrcode"
                                                CssClass="btn btn-primary btn-sm" runat="server"
                                                data-original-title="Print QR Code" data-toggle="tooltip" Text="Print QR Code"
                                                OnClientClick="target ='_blank';"
                                                OnCommand="btnCommand" CommandName="btnqrcode"
                                                CommandArgument='<%# 
                                        Convert.ToString(Eval("u0_training_course_idx")) + "|" +  
                                        Convert.ToString(Eval("training_course_no"))
                                         %>'
                                                Visible="false">
                                    <i class="fa fa-qrcode"></i></asp:LinkButton>

                                            <asp:HyperLink runat="server" ID="btnDownloadFile"
                                                CssClass="btn btn-default btn-sm" data-original-title="Download"
                                                data-toggle="tooltip" Text="Download"
                                                Target="_blank"
                                                CommandArgument='<%# Eval("training_course_no") %>'><i class="fa fa-download"></i>

                                    </asp:HyperLink>

                                            <asp:LinkButton ID="btnDetail"
                                                CssClass="btn btn-info btn-sm" runat="server"
                                                data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                                OnCommand="btnCommand" CommandName="btnDetail"
                                                CommandArgument='<%# Eval("u0_training_course_idx") + ";" + "1" + ";" + Eval("to_m0_node_idx")  + ";" + Eval("to_m0_actor_idx")+ ";" + Eval("el_approve_status") + ";" + Eval("type_request")  + ";" + Eval("training_course_created_by")  + ";" + Eval("costcenter_idx_ref") + ";" + Eval("training_course_budget_total")+ ";" + Eval("training_course_no") %>'>
                                            <i class="fa fa-file-alt"></i></asp:LinkButton>

                                            <asp:LinkButton CssClass="btn btn-warning btn-sm" runat="server"
                                                ID="btnUpdate_GvListData"
                                                CommandName="btnUpdate_GvListData" OnCommand="btnCommand"
                                                data-original-title="แก้ไข" data-toggle="tooltip" Text="แก้ไข"
                                                CommandArgument='<%# Eval("u0_training_course_idx") %>'>
                                            <i class="fa fa-pencil-alt"></i>
                                    </asp:LinkButton>

                                            <asp:LinkButton CssClass="btn btn-danger btn-sm" runat="server"
                                                data-original-title="ลบ" data-toggle="tooltip" Text="ลบ"
                                                ID="btnDelete_GvListData"
                                                CommandName="btnDelete" OnCommand="btnCommand"
                                                CommandArgument='<%# Eval("u0_training_course_idx") %>'
                                                OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                                            <i class="fa fa-trash"></i>
                                    </asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>


            </asp:Panel>
            <%-- Start Select --%>
        </asp:View>

        <asp:View ID="View_trainingPage" runat="server">
            <div class="col-md-12" id="div5" runat="server" style="color: transparent;">
                <asp:FileUpload ID="FileUpload2" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|pdf|png" />
            </div>
            <div class="form-horizontal" role="form">
                <asp:UpdatePanel ID="Upd3ateaP2anel1" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <div class="form-group">
                            <div class="col-sm-1 col-sm-offset-11">
                                <asp:LinkButton ID="btnprint_course" CssClass="btn btn-warning btn-sm" runat="server" ToolTip="Print" OnClientClick="return printDiv('printable_course');" CommandName="CmdPrint_Course"><i class="fa fa-print"></i> Print</asp:LinkButton>
                            </div>
                        </div>


                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-calendar"></i><strong>&nbsp;ตารางคอร์สอบรม</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal" role="form">

                        <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="form-group">
                                    <asp:Label ID="Label100" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="ประเภทคอร์สอบรม : " />
                                    <div class="col-md-4">
                                        <asp:DropDownList runat="server" ID="ddl_type_plan" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            <asp:ListItem Value="0">กรุณาเลือกประเภทคอร์สอบรม</asp:ListItem>
                                            <asp:ListItem Value="1">Inplan</asp:ListItem>
                                            <asp:ListItem Value="2">Outplan</asp:ListItem>
                                        </asp:DropDownList>

                                    </div>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddl_type_plan" />
                            </Triggers>
                        </asp:UpdatePanel>


                    </div>
                </div>
            </div>

            <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:FormView ID="fvCRUD" runat="server" Width="100%" OnDataBound="FvDetail_DataBound" Visible="false">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtcourse_assessment" runat="server"
                                Visible="false"
                                Text='<%# Eval("course_assessment") %>'
                                CssClass="form-control">
                    </asp:TextBox>
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; <%= getTilteCourse() %></strong></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <asp:Panel ID="Panel5" runat="server" Visible="false">
                                            <div class="row">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <asp:Label ID="Label20" class="col-md-2 control-labelnotop text_right" runat="server" Text="รหัสหลักสูตร :" />
                                                        <div class="col-md-4">
                                                        </div>



                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>


                                        <div class="form-group">

                                            <asp:Label ID="Label3" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่เอกสาร :" />
                                            <div class="col-md-4">
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txttraining_course_date" runat="server"
                                                        CssClass="form-control filter-order-from-createdocdate"
                                                        Enabled="true"
                                                        Text='<%# Eval("zdate") %>' />
                                                    <span class="input-group-addon show-order-sale-log-from-createdocdate-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>


                                            <asp:Label ID="Label5" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="เลขที่เอกสาร : " />
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txttraining_course_no" runat="server" CssClass="form-control" Enabled="false"
                                                    Text='<%# Eval("training_course_no") %>' />
                                                <asp:TextBox ID="txttraining_course_year" runat="server"
                                                    CssClass="form-control"
                                                    TextMode="Number"
                                                    MaxLength="4"
                                                    AutoPostBack="true"
                                                    Visible="false"
                                                    OnTextChanged="onTextChanged" />

                                            </div>

                                        </div>



                                        <asp:Panel ID="pnlcourse_plan_status" runat="server">
                                            <asp:UpdatePanel ID="UpdatePanel5" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Panel ID="pnlcourse_plan_status1" runat="server">
                                                        <div class="form-group">
                                                            <asp:Label ID="Label14" class="col-md-2 control-labelnotop text_right" runat="server" Text="ประเภทหลักสูตร :" />
                                                            <div class="col-md-4">
                                                                <asp:DropDownList ID="ddlcourse_plan_status" runat="server"
                                                                    AutoPostBack="true"
                                                                    OnSelectedIndexChanged="FvDetail_DataBound"
                                                                    SelectedValue='<%# getValueStr((string)Eval("course_plan_status"),"I") %>'
                                                                    CssClass="form-control">
                                                                    <asp:ListItem Value="I" Text="In Plan" Selected="True" />
                                                                    <asp:ListItem Value="O" Text="Out Plan" />
                                                                </asp:DropDownList>

                                                            </div>
                                                            <div class="col-md-6">
                                                            </div>
                                                        </div>
                                                    </asp:Panel>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="ddlcourse_plan_status" />
                                                </Triggers>
                                            </asp:UpdatePanel>

                                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div class="form-group">

                                                        <asp:Label ID="Label12" class="col-md-2 control-labelnotop text_right" runat="server" Text="ชื่อหลักสูตร :" />
                                                        <div class="col-md-9">
                                                            <asp:DropDownList ID="ddlu0_training_plan_idx_ref" runat="server"
                                                                AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                                CssClass="form-control" />

                                                            <asp:RequiredFieldValidator ID="rqf_ddlu0_training_plan_idx_ref"
                                                                ValidationGroup="btnSaveInsert" runat="server"
                                                                Display="Dynamic"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="ddlu0_training_plan_idx_ref"
                                                                Font-Size="1em" ForeColor="Red"
                                                                CssClass="pull-left"
                                                                InitialValue="0"
                                                                ErrorMessage="กรุณากรอกชื่อหลักสูตร" />
                                                        </div>

                                                    </div>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="ddlu0_training_plan_idx_ref" />
                                                </Triggers>
                                            </asp:UpdatePanel>


                                            <asp:Panel ID="box_detail_training_group" runat="server" Visible="false">
                                                <div class="form-group">

                                                    <asp:Label ID="Label15" class="col-md-2 control-labelnotop text_right" runat="server" Text="กลุ่มวิชา :" />
                                                    <div class="col-md-4">
                                                        <asp:TextBox ID="txttraining_group_name" runat="server"
                                                            Enabled="false"
                                                            Text='<%# Eval("training_group_name") %>'
                                                            CssClass="form-control">
                                            </asp:TextBox>
                                                    </div>

                                                    <asp:Label ID="Label25" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สาขาวิชา : " />
                                                    <div class="col-md-3">
                                                        <asp:TextBox ID="txttraining_branch_name" runat="server"
                                                            Enabled="false"
                                                            Text='<%# Eval("training_branch_name") %>'
                                                            CssClass="form-control">
                                            </asp:TextBox>
                                                    </div>

                                                </div>
                                            </asp:Panel>
                                        </asp:Panel>

                                        <asp:Panel ID="pnlcourse_outplan_status" runat="server">

                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div class="form-group">

                                                        <asp:Label ID="Label91" class="col-md-2 control-labelnotop text_right" runat="server" Text="ชื่อหลักสูตร :" />
                                                        <div class="col-md-9">
                                                            <asp:DropDownList ID="ddl_training_name_add" runat="server"
                                                                AutoPostBack="true"
                                                                OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                                CssClass="form-control" />

                                                            <asp:TextBox ID="txtcourse_name" runat="server"
                                                                CssClass="form-control" MaxLength="250"
                                                                Text='<%# Eval("course_name") %>' Visible="false" />
                                                            <asp:RequiredFieldValidator ID="rqf_txtcourse_name"
                                                                ValidationGroup="btnSaveInsert" runat="server"
                                                                Display="Dynamic"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="txtcourse_name"
                                                                Font-Size="1em" ForeColor="Red"
                                                                CssClass="pull-left"
                                                                ErrorMessage="กรุณากรอกชื่อหลักสูตร" Visible="false" />
                                                        </div>

                                                    </div>

                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="ddl_training_name_add" />
                                                </Triggers>
                                            </asp:UpdatePanel>

                                            <asp:Panel ID="box_training_group" runat="server" Visible="false">

                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <div class="form-group">

                                                            <asp:Label ID="Label92" class="col-md-2 control-labelnotop text_right" runat="server" Text="กลุ่มวิชา :" />
                                                            <div class="col-md-4">
                                                                <asp:DropDownList ID="ddlm0_training_group_idx_ref" runat="server"
                                                                    AutoPostBack="true"
                                                                    OnSelectedIndexChanged="FvDetail_DataBound"
                                                                    CssClass="form-control">
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rqf_ddm0_training_group_idx_ref"
                                                                    ValidationGroup="btnSaveInsert" runat="server"
                                                                    Display="Dynamic"
                                                                    SetFocusOnError="true"
                                                                    ControlToValidate="ddlm0_training_group_idx_ref"
                                                                    Font-Size="1em" ForeColor="Red"
                                                                    CssClass="pull-left"
                                                                    InitialValue="0"
                                                                    ErrorMessage="กรุณากรอกกลุ่มวิชา" />
                                                            </div>


                                                            <asp:Label ID="Label93" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สาขาวิชา : " />
                                                            <div class="col-md-3">
                                                                <asp:DropDownList ID="ddm0_training_branch_idx" runat="server"
                                                                    CssClass="form-control"
                                                                    AutoPostBack="true"
                                                                    OnSelectedIndexChanged="FvDetail_DataBound">
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="rqf_ddm0_training_branch_idx"
                                                                    ValidationGroup="btnSaveInsert" runat="server"
                                                                    Display="Dynamic"
                                                                    SetFocusOnError="true"
                                                                    ControlToValidate="ddm0_training_branch_idx"
                                                                    Font-Size="1em" ForeColor="Red"
                                                                    CssClass="pull-left"
                                                                    InitialValue="0"
                                                                    ErrorMessage="กรุณากรอกสาขาวิชา" />
                                                            </div>

                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlm0_training_group_idx_ref" />
                                                        <asp:AsyncPostBackTrigger ControlID="ddm0_training_branch_idx" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </asp:Panel>


                                        </asp:Panel>


                                        <div class="form-group">
                                            <asp:Label ID="Label98" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="คะแนนเต็ม :" />
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txtcourse_score" runat="server"
                                                    CssClass="form-control" Enabled="false"
                                                    TextMode="Number"
                                                    Text='<%# Eval("course_score") %>'>
                                        </asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Requiredcourse_score"
                                                    ValidationGroup="btnSaveInsert" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="txtcourse_score"
                                                    Font-Size="1em" ForeColor="Red"
                                                    CssClass="pull-left"
                                                    ErrorMessage="กรุณากรอกคะแนนเต็ม" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label11" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="คะแนนผ่าน % :" />
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txtscore_through_per" runat="server" Enabled="false"
                                                    CssClass="form-control"
                                                    TextMode="Number"
                                                    Text='<%# Eval("score_through_per") %>'>
                                        </asp:TextBox>
                                            </div>
                                        </div>

                                        <asp:Panel ID="pnlcourse_outplan_status2" runat="server">

                                            <div class="form-group">

                                                <asp:Label ID="Label9" class="col-md-2 control-labelnotop text_right" runat="server" Text="กลุ่มเป้าหมาย :" />
                                                <div class="col-md-4">

                                                    <asp:TextBox ID="txtm0_target_group_idx_ref" Visible="false" runat="server" Text='<%# Bind("m0_target_group_idx_ref") %>'></asp:TextBox>
                                                    <asp:DropDownList ID="ddlm0_target_group_idx_ref" runat="server" CssClass="form-control" />
                                                    <asp:RequiredFieldValidator ID="rqf_ddlm0_target_group_idx_ref"
                                                        ValidationGroup="btnSaveInsert" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlm0_target_group_idx_ref"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        InitialValue="0"
                                                        ErrorMessage="กรุณากรอกกลุ่มเป้าหมาย" />
                                                </div>

                                            </div>

                                        </asp:Panel>

                                        <div class="form-group">

                                            <asp:Label ID="Label26" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายละเอียดหลักสูตร :" />
                                            <div class="col-md-9">
                                                <asp:TextBox ID="txttraining_course_description" runat="server"
                                                    Enabled="true"
                                                    Text='<%# Eval("training_course_description") %>'
                                                    CssClass="form-control"
                                                    TextMode="MultiLine"
                                                    Rows="4">
                                        </asp:TextBox>
                                            </div>

                                        </div>

                                        <asp:UpdatePanel ID="UpdatePanel6" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>

                                                <div class="form-group">

                                                    <asp:Label ID="Label87" class="col-md-2 control-labelnotop text_right" runat="server" Text="สถานที่อบรม :" />
                                                    <div class="col-md-4">

                                                        <asp:TextBox ID="txtplace_idx_ref" Visible="false" runat="server" Text='<%# Bind("place_idx_ref") %>'></asp:TextBox>
                                                        <asp:RadioButtonList ID="rdolecturer_type_place" runat="server"
                                                            CssClass="radio-list-inline-emps" RepeatDirection="Horizontal"
                                                            AutoPostBack="true"
                                                            OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0" Text="ภายใน" Selected="True" />
                                                            <asp:ListItem Value="1" Text="ภายนอก" />
                                                        </asp:RadioButtonList>
                                                        <asp:Panel runat="server" ID="pn_training_location" Visible="true">
                                                            <asp:DropDownList ID="ddl_training_location" runat="server" CssClass="form-control">
                                                            </asp:DropDownList>
                                                        </asp:Panel>
                                                        <asp:Panel runat="server" ID="pnlplace" Visible="false">
                                                            <asp:DropDownList ID="ddlplace_idx_ref" runat="server"
                                                                CssClass="form-control">
                                                                <asp:ListItem Value="0">-- เลือก --</asp:ListItem>
                                                                <asp:ListItem Value="2">นพวงศ์</asp:ListItem>
                                                                <asp:ListItem Value="14">โรจนะ</asp:ListItem>
                                                                <asp:ListItem Value="1">เมืองทองธานี</asp:ListItem>
                                                            </asp:DropDownList>

                                                            <%--SelectedValue='<%# Eval("place_idx_ref") == null ? "0" : Eval("place_idx_ref") %>'--%>

                                                            <asp:RequiredFieldValidator ID="rqf_place_idx_ref"
                                                                ValidationGroup="btnSaveInsert" runat="server"
                                                                Display="Dynamic"
                                                                SetFocusOnError="true"
                                                                ControlToValidate="ddlplace_idx_ref"
                                                                Font-Size="1em" ForeColor="Red"
                                                                CssClass="pull-left"
                                                                InitialValue="0"
                                                                ErrorMessage="กรุณากรอกสถานที่อบรม" />
                                                        </asp:Panel>
                                                    </div>

                                                </div>

                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="rdolecturer_type_place" />
                                            </Triggers>
                                        </asp:UpdatePanel>

                                        <asp:UpdatePanel ID="UpdatePanel7" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>

                                                <div class="form-group">
                                                    <asp:Label ID="Label10" class="col-md-2 control-labelnotop text_right" runat="server" Text="วิทยากร :" />
                                                    <div class="col-md-9">
                                                        <asp:RadioButtonList ID="rdllecturer_type" runat="server"
                                                            CssClass="radio-list-inline-emps" RepeatDirection="Horizontal"
                                                            AutoPostBack="true"
                                                            SelectedValue='<%# Eval("lecturer_type") == null ? "0" : Eval("lecturer_type") %>'
                                                            OnSelectedIndexChanged="FvDetail_DataBound">
                                                            <asp:ListItem Value="0" Text="ภายใน" Selected="True" />
                                                            <asp:ListItem Value="1" Text="ภายนอก" />
                                                        </asp:RadioButtonList>

                                                    </div>

                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="rdllecturer_type" />
                                            </Triggers>
                                        </asp:UpdatePanel>



                                        <asp:Panel ID="pnl_institution" runat="server">

                                            <asp:UpdatePanel ID="UpdatePanel8" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label27" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                                        <div class="col-md-9">
                                                            <div>

                                                                <div class="input-group col-md-12 pull-left">
                                                                    <asp:DropDownList ID="ddlm0_institution_idx_ref" runat="server"
                                                                        Visible="true"
                                                                        CssClass="form-control" />

                                                                    <div class="input-group-btn">
                                                                        <asp:LinkButton ID="btnAdd_lecturer" Visible="true"
                                                                            CssClass="btn btn-primary" runat="server"
                                                                            data-original-title="เพิ่ม" data-toggle="tooltip" Text="เพิ่ม"
                                                                            OnCommand="btnCommand" CommandName="btnAdd_lecturer">
                                                       <i class="fa fa-plus"></i>
                                                        </asp:LinkButton>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnAdd_lecturer" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </asp:Panel>

                                        <div class="form-group">

                                            <asp:Label ID="Label28" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                            <div class="col-md-9">
                                                <div>

                                                    <div class="panel-info">
                                                        <div class="panel-heading f-bold">รายละเอียดวิทยากร</div>
                                                    </div>
                                                    <asp:GridView ID="Gvinstitution" runat="server"
                                                        CssClass="table table-striped table-responsive table-bordered word-wrap"
                                                        GridLines="None" OnRowCommand="onRowCommand"
                                                        AutoGenerateColumns="false">
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-Width="5%">
                                                                <ItemTemplate>
                                                                    <small>
                                                                        <%# (Container.DataItemIndex +1) %>
                                                            </small>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="ชื่อ - สกุล"
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtinstitution_name_edit_L" runat="server" Text='<%# Bind("zName") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtinstitution_name_item_L" runat="server" Text='<%# Eval("zName") %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                                                <ItemTemplate>
                                                                    <div class="word-wrap">
                                                                        <asp:LinkButton ID="btnDel_Gvinstitution_L"
                                                                            CssClass="btn-danger btn-sm" runat="server"
                                                                            data-original-title="ลบ" data-toggle="tooltip" Text="ลบ"
                                                                            OnCommand="btnCommand" CommandName="btnDel_Gvinstitution_L">
                                                                           <i class="fa fa-close"></i></asp:LinkButton>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                        </div>

                                        <asp:Panel ID="pnl_date" runat="server">
                                            <div class="form-group">
                                                <asp:Label ID="Label29" class="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่อบรม :" />
                                                <div class="col-sm-2">
                                                    <div class="input-group date">
                                                        <asp:TextBox ID="txtdatestart_create" runat="server" placeholder="วันที่..."
                                                            Text='<%# Eval("zdate_start") %>'
                                                            CssClass="form-control filter-order-from-createstart" />
                                                        <span class="input-group-addon show-order-sale-log-from-createstart-onclick">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8"
                                                        ValidationGroup="btnAdd_u8date" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txtdatestart_create"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        InitialValue=""
                                                        ErrorMessage="กรุณาเลือกวันที่เริ่มอบรม" />

                                                </div>
                                                <asp:Label ID="Label30" class="col-md-1 control-labelnotop text_right" runat="server" Text="เวลาที่เริ่ม:" />
                                                <div class="col-sm-2">
                                                    <div class="input-group time">
                                                        <asp:TextBox ID="txt_timestart_create" placeholder="เวลา ..."
                                                            runat="server" CssClass="form-control clockpicker cursor-pointer"
                                                            Text='<%# Eval("ztime_start") %>' />
                                                        <span class="input-group-addon show-time-onclick">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9"
                                                        ValidationGroup="btnAdd_u8date" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txt_timestart_create"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        InitialValue=""
                                                        ErrorMessage="กรุณาเลือกเวลาที่เริ่มอบรม" />

                                                </div>
                                                <asp:Label ID="Label31" class="col-md-2 control-labelnotop text_right" runat="server" Text="เวลาที่สิ้นสุด:" />
                                                <div class="col-sm-2">
                                                    <div class="input-group time">
                                                        <asp:TextBox ID="txt_timeend_create" placeholder="เวลา ..."
                                                            runat="server" CssClass="form-control clockpickerto cursor-pointer"
                                                            Text='<%# Eval("ztime_end") %>' />
                                                        <span class="input-group-addon show-timeto-onclick">
                                                            <span class="glyphicon glyphicon-time"></span>
                                                        </span>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11"
                                                        ValidationGroup="btnAdd_u8date" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txt_timeend_create"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณาเลือกเวลาที่สิ้นสุดอบรม" />

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label32" class="col-md-2 control-labelnotop text_right" runat="server" Text="รวมชม.ที่เรียน :" />
                                                <div class="col-sm-2">
                                                    <asp:TextBox ID="txttraining_course_date_qty" runat="server"
                                                        placeholder="รวมชม.ที่เรียน..."
                                                        TextMode="Number"
                                                        CssClass="form-control"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10"
                                                        ValidationGroup="btnAdd_u8date" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txttraining_course_date_qty"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        InitialValue=""
                                                        ErrorMessage="กรุณาเลือกวันที่สิ้นสุดอบรม" />


                                                </div>
                                                <asp:LinkButton ID="btnAdd_u8date" Visible="true"
                                                    CssClass="btn btn-primary pull-left" runat="server"
                                                    data-original-title="เพิ่ม" data-toggle="tooltip" Text='<i class="fa fa-plus"></i> เพิ่ม'
                                                    OnCommand="btnCommand" CommandName="btnAdd_u8date">
                                                       
                                        </asp:LinkButton>

                                            </div>

                                        </asp:Panel>

                                        <div class="form-group">
                                            <asp:Label ID="Label96" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                            <div class="col-md-9">
                                                <div>

                                                    <div class="panel-info">
                                                        <div class="panel-heading f-bold">รายละเอียดวันที่อบรม</div>
                                                    </div>
                                                    <asp:GridView ID="Gvu8trncoursedate" runat="server"
                                                        CssClass="table table-bordered word-wrap"
                                                        GridLines="None" OnRowCommand="onRowCommand"
                                                        AutoGenerateColumns="false">
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-Width="5%">
                                                                <ItemTemplate>
                                                                    <%# (Container.DataItemIndex +1) %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="วันที่อบรม"
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtzdate_edit_L" runat="server" Text='<%# Bind("zdate") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtzdate_item_L" runat="server" Text='<%# Eval("zdate") %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="เวลาที่เริ่ม-เวลาที่สิ้นสุด"
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtztime_edit_L" runat="server" Text='<%# Bind("ztime_start") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtztime_item_L" runat="server" Text='<%# Eval("ztime_start")+" - "+Eval("ztime_end") %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="รวมชม.ที่เรียน"
                                                                ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txttraining_course_date_qty_edit_L" runat="server" Text='<%# Bind("training_course_date_qty") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txttraining_course_date_qty_item_L" runat="server" Text='<%# string.Format("{0:n2}",float.Parse((string)Eval("training_course_date_qty"))) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                                                <ItemTemplate>
                                                                    <div class="word-wrap">
                                                                        <asp:LinkButton ID="btnDel_Gvu8"
                                                                            CssClass="btn-danger btn-sm" runat="server"
                                                                            data-original-title="ลบ" data-toggle="tooltip" Text="ลบ"
                                                                            OnCommand="btnCommand" CommandName="btnDel_Gvu8">
                                                                           <i class="fa fa-close"></i></asp:LinkButton>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                        </div>

                                        <%-- start วัตถุประสงค์ --%>


                                        <div class="form-group">

                                            <asp:Label ID="Label33" class="col-md-2 control-labelnotop text_right" runat="server" Text="วัตถุประสงค์ :" />

                                            <div class="col-md-9">
                                                <asp:Panel ID="pnl_objective" runat="server">
                                                    <div class="input-group col-md-12 pull-left">
                                                        <asp:DropDownList ID="ddlm0_objective_idx_ref" runat="server"
                                                            Visible="true"
                                                            CssClass="form-control" />


                                                        <div class="input-group-btn">
                                                            <asp:LinkButton ID="btnAdd_objective" Visible="true"
                                                                CssClass="btn btn-primary" runat="server"
                                                                data-original-title="เพิ่ม" data-toggle="tooltip" Text="เพิ่ม"
                                                                OnCommand="btnCommand" CommandName="btnAdd_objective">
                                                       <i class="fa fa-plus"></i>
                                                    </asp:LinkButton>
                                                        </div>

                                                    </div>
                                                </asp:Panel>
                                            </div>

                                        </div>


                                        <div class="form-group">

                                            <asp:Label ID="Label40" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                            <div class="col-md-9">
                                                <div>

                                                    <div class="panel-info">
                                                        <div class="panel-heading f-bold">รายละเอียดวัตถุประสงค์</div>
                                                    </div>
                                                    <asp:GridView ID="Gvobjective" runat="server"
                                                        CssClass="table table-striped table-responsive table-bordered word-wrap"
                                                        GridLines="None" OnRowCommand="onRowCommand"
                                                        AutoGenerateColumns="false">
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-Width="5%">
                                                                <ItemTemplate>
                                                                    <small>
                                                                        <%# (Container.DataItemIndex +1) %>
                                                            </small>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="วัตถุประสงค์"
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtobjective_name_edit_L" runat="server" Text='<%# Bind("zName") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbm0_objective_idx_ref" runat="server" Text='<%# Eval("m0_objective_idx_ref") %>'
                                                                        Visible="false"></asp:Label>
                                                                    <asp:Label ID="txtobjective_name_item_L" runat="server" Text='<%# Eval("zName") %>'
                                                                        Width="100%"></asp:Label>
                                                                    <hr />
                                                                    <%-- start --%>

                                                                    <asp:Label ID="Label78" class="col-md-12 control-labelnotop pull-left" runat="server"
                                                                        Text="การวัด ประเมิน และติดตามผล" Font-Bold="true" />

                                                                    <div class="form-group">

                                                                        <asp:Label ID="Label79" class="col-md-1 control-labelnotop text_right" runat="server" Text="" />

                                                                        <div class="col-md-5 form-inline">
                                                                            <asp:CheckBox ID="cbpass_test_flag" runat="server"
                                                                                CssClass="checkbox"
                                                                                Checked='<%# getValue((string)Eval("pass_test_flag")) %>'
                                                                                Text="ผ่านการทดสอบ" />
                                                                            <asp:TextBox ID="txtpass_test_per" runat="server"
                                                                                TextMode="Number"
                                                                                Width="40%"
                                                                                Text='<%# Eval("pass_test_per") %>'
                                                                                CssClass="form-control">
                                                                    </asp:TextBox>
                                                                            <asp:Label ID="Label80" CssClass="control-labelnotop textleft" runat="server"
                                                                                Text="%" />
                                                                        </div>


                                                                        <div class="col-md-6 form-inline">
                                                                            <asp:CheckBox ID="cbhour_training_flag" runat="server"
                                                                                CssClass="checkbox"
                                                                                Checked='<%# getValue((string)Eval("hour_training_flag")) %>'
                                                                                Text="ชั่วโมงเข้าอบรมครบ" />
                                                                            <asp:TextBox ID="txthour_training_per" runat="server"
                                                                                TextMode="Number"
                                                                                Width="40%"
                                                                                Text='<%# Eval("hour_training_per") %>'
                                                                                CssClass="form-control">
                                                                    </asp:TextBox>
                                                                            <asp:Label ID="Label81" CssClass="control-labelnotop textleft" runat="server"
                                                                                Text="%" />
                                                                        </div>

                                                                    </div>

                                                                    <div class="form-group">

                                                                        <asp:Label ID="Label82" class="col-md-1 control-labelnotop text_right" runat="server" Text="" />

                                                                        <div class="col-md-5 form-inline">
                                                                            <asp:CheckBox ID="cbwrite_report_training_flag" runat="server"
                                                                                CssClass="checkbox"
                                                                                Checked='<%# getValue((string)Eval("write_report_training_flag")) %>'
                                                                                Text="เขียนรายงานการอบรม" />
                                                                        </div>


                                                                        <div class="col-md-6 form-inline">
                                                                            <asp:CheckBox ID="cbpublish_training_flag" runat="server"
                                                                                CssClass="checkbox"
                                                                                Checked='<%# getValue((string)Eval("publish_training_flag")) %>'
                                                                                Text="เผยแพร่ต่อผู้เกี่ยวข้องในรูปแบบ" />
                                                                            <asp:TextBox ID="txtpublish_training_description" runat="server"
                                                                                Width="40%"
                                                                                Text='<%# Eval("publish_training_description") %>'
                                                                                CssClass="form-control">
                                                                    </asp:TextBox>
                                                                        </div>

                                                                    </div>

                                                                    <div class="form-group">

                                                                        <asp:Label ID="Label83" class="col-md-1 control-labelnotop text_right" runat="server" Text="" />

                                                                        <div class="col-md-5 form-inline">
                                                                            <asp:CheckBox ID="cbcourse_lecturer_flag" runat="server"
                                                                                CssClass="checkbox"
                                                                                Checked='<%# getValue((string)Eval("course_lecturer_flag")) %>'
                                                                                Text="จัดทำหลักสูตรและเป็นวิทยากรภายใน" />
                                                                        </div>


                                                                        <div class="col-md-6 form-inline">
                                                                            <asp:CheckBox ID="cbother_flag" runat="server"
                                                                                CssClass="checkbox"
                                                                                Checked='<%# getValue((string)Eval("other_flag")) %>'
                                                                                Text="อื่นๆ ระบุ" />
                                                                            <asp:TextBox ID="txtother_description" runat="server"
                                                                                Width="77%"
                                                                                Text='<%# Eval("other_description") %>'
                                                                                CssClass="form-control">
                                                                    </asp:TextBox>
                                                                        </div>

                                                                    </div>
                                                                    <asp:Label ID="Label88" class="col-md-12 control-labelnotop pull-left" runat="server"
                                                                        Text="การติดตามผลโดย HRD :" />
                                                                    <div class="form-group">

                                                                        <asp:Label ID="Label84" class="col-md-1 control-labelnotop text_right" runat="server" Text="" />

                                                                        <div class="col-md-5 form-inline">
                                                                            <asp:CheckBox ID="cbhrd_nofollow_flag" runat="server"
                                                                                CssClass="checkbox"
                                                                                Checked='<%# getValue((string)Eval("hrd_nofollow_flag")) %>'
                                                                                Text="ไม่ติดตามผล" />
                                                                        </div>


                                                                        <div class="col-md-6 form-inline">
                                                                            <asp:CheckBox ID="cbhrd_follow_flag" runat="server"
                                                                                CssClass="checkbox"
                                                                                Checked='<%# getValue((string)Eval("hrd_follow_flag")) %>'
                                                                                Text="ติดตามภายใน" />
                                                                            <asp:TextBox ID="txthrd_follow_day" runat="server"
                                                                                CssClass="form-control"
                                                                                Text='<%# Eval("hrd_follow_day") %>'
                                                                                TextMode="Number"
                                                                                Width="40%">
                                                                    </asp:TextBox>
                                                                            <asp:Label ID="Label85"
                                                                                CssClass="control-labelnotop textleft" runat="server"
                                                                                Text="วัน" />
                                                                        </div>

                                                                    </div>
                                                                    <%-- end --%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                                                <ItemTemplate>
                                                                    <div class="word-wrap">
                                                                        <asp:LinkButton ID="btnDel_Gvobjective"
                                                                            CssClass="btn-danger btn-sm" runat="server"
                                                                            data-original-title="ลบ" data-toggle="tooltip" Text="ลบ"
                                                                            OnCommand="btnCommand" CommandName="btnDel_Gvobjective">
                                                                           <i class="fa fa-close"></i></asp:LinkButton>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                        </div>


                                        <%-- end วัตถุประสงค์ --%>

                                        <div class="form-group">

                                            <asp:Label ID="Label41" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายละเอียดวัตถุประสงค์ :" />
                                            <div class="col-md-9">
                                                <asp:TextBox ID="txttraining_course_remark" runat="server"
                                                    Enabled="true"
                                                    Text='<%# Eval("training_course_remark") %>'
                                                    CssClass="form-control"
                                                    TextMode="MultiLine"
                                                    Rows="4">
                                        </asp:TextBox>
                                            </div>

                                        </div>

                                        <hr />

                                        <asp:Label ID="Label56" class="col-md-12 control-labelnotop pull-left h4" runat="server" Text="รายละเอียดผู้เข้าร่วมอบรม" />
                                        <asp:Panel ID="pnl_emp" runat="server">
                                            <div class="form-group">
                                                <div class="col-md-2">

                                                    <asp:Label ID="Label43" class="control-labelnotop pull-right" runat="server" Text="รหัสพนักงาน :" />
                                                </div>
                                                <div class="col-md-4">

                                                    <div class="input-group col-md-12 pull-left">
                                                        <asp:DropDownList ID="ddlemp_idx_ref" runat="server"
                                                            Visible="true"
                                                            CssClass="form-control" />

                                                        <div class="input-group-btn">
                                                            <asp:LinkButton ID="btnAdd_emp" Visible="true"
                                                                CssClass="btn btn-primary" runat="server"
                                                                data-original-title="เพิ่ม" data-toggle="tooltip" Text="เพิ่ม"
                                                                OnCommand="btnCommand" CommandName="btnAdd_emp">
                                                       <i class="fa fa-plus"></i>
                                                    </asp:LinkButton>
                                                        </div>


                                                    </div>

                                                </div>

                                                <div class="col-md-6">
                                                    <div class="input-group col-md-10">
                                                        <span class="input-group-addon"><i class="fa fa-file-excel"></i></span>
                                                        <input type="file" class="form-control"
                                                            id="file_import_employee"
                                                            onchange="ImportEmpProposUpload(this)">
                                                        <span class="input-group-btn">
                                                            <asp:LinkButton CssClass="btn btn-info" runat="server"
                                                                ID="btnimport_employee"
                                                                OnCommand="btnCommand" CommandName="btnimport_employee">
                                                        <i class="glyphicon glyphicon-import"></i>Import File
                                                    </asp:LinkButton>
                                                        </span>

                                                    </div>
                                                </div>
                                            </div>

                                            <asp:Panel ID="pnladd_emp_resulte" runat="server">
                                                <div class="form-group">
                                                    <div class="col-md-2">
                                                        <asp:Label ID="Label36" class="control-labelnotop pull-right" runat="server" Text="" />
                                                    </div>
                                                    <div class="col-md-4">
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="input-group col-md-10">
                                                            <asp:LinkButton CssClass="btn btn-primary" runat="server"
                                                                ID="btnadd_emp_resulte"
                                                                OnCommand="btnCommand" CommandName="btnadd_emp_resulte"
                                                                CommandArgument='<%# Eval("u0_training_course_idx") %>'>
                                                        <i class="glyphicon glyphicon-plus"></i>อนุมัติคำขอเข้าร่วมคอร์สอบรบ
                                                    </asp:LinkButton>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </asp:Panel>


                                        <div class="form-group">

                                            <asp:Label ID="Label44" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                            <div class="col-md-9">
                                                <div>

                                                    <div class="panel-info">
                                                        <div class="panel-heading f-bold">รายละเอียดพนักงาน</div>
                                                    </div>
                                                    <div class="panel-body" style="height: 400px; overflow-y: scroll; overflow-x: scroll;">
                                                        <asp:GridView ID="Gvemp" runat="server"
                                                            CssClass="table table-striped table-responsive table-bordered word-wrap"
                                                            GridLines="None" OnRowCommand="onRowCommand"
                                                            AutoGenerateColumns="false">
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">ไม่พบข้อมูล</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                                    HeaderStyle-Width="5%">
                                                                    <ItemTemplate>
                                                                        <small>
                                                                            <%# (Container.DataItemIndex +1) %>
                                                                            <asp:TextBox ID="txtemp_u0_training_course_idx_ref" runat="server"
                                                                                Visible="false"
                                                                                Text='<%# Bind("u0_training_course_idx_ref") %>'></asp:TextBox>
                                                                        </small>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="ชื่อ - สกุล"
                                                                    ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small"
                                                                    HeaderStyle-Width="25%">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtemp_empcode_edit_L" runat="server" Text='<%# Bind("empcode") %>'></asp:TextBox>
                                                                        <asp:TextBox ID="txtemp_zName_edit_L" runat="server" Text='<%# Bind("zName") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtemp_empcode_item_L" runat="server" Text='<%# Eval("empcode") %>'
                                                                            Width="100%"></asp:Label>
                                                                        <br />
                                                                        <asp:Label ID="txtemp_zName_item_L" runat="server" Text='<%# Eval("zName") %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="ตำแหน่ง"
                                                                    ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small"
                                                                    HeaderStyle-Width="15%">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtemp_zPostName_edit_L" runat="server" Text='<%# Bind("zPostName") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtemp_zPostName_item_L" runat="server" Text='<%# Eval("zPostName") %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="Cost Center"
                                                                    ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Width="7%"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtemp_zCostCenter_edit_L" runat="server" Text='<%# Bind("zCostCenter") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtemp_zCostCenter_item_L" runat="server" Text='<%# Eval("zCostCenter") %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="เบอร์ติดต่อ"
                                                                    ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtemp_zTel_edit_L" runat="server" Text='<%# Bind("zTel") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtemp_zTel_item_L" runat="server" Text='<%# Eval("zTel") %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>


                                                                <asp:TemplateField HeaderText="ลงทะเบียน" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btnGvemp_register"
                                                                            CssClass="btn-info btn-sm" runat="server"
                                                                            data-original-title="ลงทะเบียนเข้าร่วมคอร์สอบรม" data-toggle="tooltip" Text="ลงทะเบียนเข้าร่วมคอร์สอบรม"
                                                                            OnCommand="btnCommand" CommandName="btnGvemp_register"
                                                                            CommandArgument='<%# Eval("u3_training_course_idx") %>'
                                                                            Visible='<%#  getvalue_compare(Eval("register_status").ToString(),1) %>'>
                                                                           <i class="fa fa-registered"></i></asp:LinkButton>
                                                                        <span>
                                                                            <%# getTextEmp((string)Eval("register_status"),(string)Eval("zregister_date") ) %>
                                                                   
                                                                </span>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="เข้าอบรม" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>
                                                                        <%-- <%# Eval("register_status")  %>
                                                                <%# Eval("signup_status")  %>--%>
                                                                        <asp:LinkButton ID="btnGvemp_signup"
                                                                            CssClass="btn-info btn-sm" runat="server"
                                                                            data-original-title="เข้าอบรม" data-toggle="tooltip" Text="เข้าอบรม"
                                                                            OnCommand="btnCommand" CommandName="btnGvemp_signup"
                                                                            CommandArgument='<%# Eval("u3_training_course_idx") %>'
                                                                            Visible='<%# getsignup((string)Eval("register_status"),(string)Eval("signup_status")) %>'>
                                                                           <i class="fa fa-sign"></i></asp:LinkButton>
                                                                        <span>
                                                                            <%# getTextEmp_signup((string)Eval("register_status")
                                                                                           ,(string)Eval("signup_status")
                                                                                            ,(string)Eval("zsignup_date") ) %>
                                                                </span>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="สถานะการติดตามผล" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Small"
                                                                    Visible="true"
                                                                    HeaderStyle-Width="13%">
                                                                    <ItemTemplate>

                                                                        <div class="word-wrap">
                                                                            <span>
                                                                                <%# Eval("zstatus_name")  %>
                                                                                <%--<%# getTextEmp((string)Eval("register_status"),(string)Eval("zregister_date") ) %>--%>
                                                                    </span>
                                                                        </div>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="คะแนน" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>

                                                                        <asp:LinkButton ID="btnGvemp_test_scores"
                                                                            CssClass="btn-default btn-sm" runat="server"
                                                                            data-original-title="แก้ไขคะแนน" data-toggle="tooltip" Text="แก้ไขคะแนน"
                                                                            OnCommand="btnCommand" CommandName="btnGvemp_test_scores"
                                                                            CommandArgument='<%# Eval("u3_training_course_idx") %>'
                                                                            Visible='<%# getsignup((string)Eval("register_status"),(string)Eval("signup_status"),"scores")== false ? true : false %>'>
                                                                           <i class="fa fa-pen"></i></asp:LinkButton>
                                                                        <br />
                                                                        <span>
                                                                            <%# getformatfloat((string)Eval("test_scores"),2 ) %>
                                                                    
                                                                </span>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>


                                                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:LinkButton ID="btnDel_emp"
                                                                                CssClass="btn-danger btn-sm" runat="server"
                                                                                data-original-title="ลบ" data-toggle="tooltip" Text="ลบ"
                                                                                OnCommand="btnCommand" CommandName="btnDel_emp"
                                                                                Visible='<%# Eval("register_status").ToString() == "1" ? false : true %>'>
                                                                           <i class="fa fa-close"></i></asp:LinkButton>
                                                                        </div>
                                                                        <span>
                                                                            <%# getTextEmp_del((string)Eval("register_status") ) %>
                                                                </span>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>


                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <%-- end รายละเอียดผู้เข้าร่วมอบรม  : --%>

                                        <hr />

                                        <asp:Panel ID="box_course" runat="server" Visible="false">
                                            <div class="form-group">

                                                <asp:Label ID="Label45" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายละเอียดแผน และงบประมาณ  :" />
                                                <div class="col-md-9">
                                                    <asp:RadioButtonList ID="rdotraining_course_planbudget_type" runat="server"
                                                        CssClass="radio-list-inline-emps" RepeatDirection="Horizontal"
                                                        SelectedValue='<%# Eval("training_course_planbudget_type") == null ? "3" : Eval("training_course_planbudget_type") %>'>
                                                        <asp:ListItem Value="1" Text="มีค่าใช้จ่ายอยู่ในงบประมาณ" />
                                                        <asp:ListItem Value="2" Text="เกินงบประมาณ" />
                                                        <asp:ListItem Value="3" Text="ฟรี" Selected="True" />
                                                    </asp:RadioButtonList>

                                                </div>

                                            </div>
                                        </asp:Panel>


                                        <asp:Panel ID="box_course_type" runat="server" Visible="false">
                                            <div class="form-group">

                                                <asp:Label ID="Label86" class="col-md-2 control-labelnotop text_right" runat="server" Text="คอร์สอบรม :" />
                                                <div class="col-md-9">
                                                    <asp:RadioButtonList ID="rdotraining_course_type" runat="server"
                                                        CssClass="radio-list-inline-emps" RepeatDirection="Horizontal"
                                                        AutoPostBack="true"
                                                        OnSelectedIndexChanged="FvDetail_DataBound"
                                                        SelectedValue='<%# Eval("training_course_type") == null ? "1" : Eval("training_course_type") %>'>
                                                        <asp:ListItem Value="1" Text="คอร์สตามผู้ทำรายการ" Selected="True" />
                                                        <asp:ListItem Value="2" Text="คอร์สตามแผนก" />
                                                        <asp:ListItem Value="3" Text="คอร์สตามผู้เรียน" />
                                                    </asp:RadioButtonList>

                                                </div>

                                            </div>
                                        </asp:Panel>

                                        <asp:Label ID="Label42" class="col-md-12 control-labelnotop pull-left h4" runat="server" Text="ค่าใช้จ่ายฝึกอบรม" />


                                        <div class="form-group">
                                            <asp:Label ID="Label101" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="องค์กร :" />
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txtorgidx_cost" runat="server" Visible="false"
                                                    Enabled="false"
                                                    Text='<%# Eval("org_idx") %>'
                                                    CssClass="form-control">
                                                        </asp:TextBox>
                                                <asp:DropDownList ID="ddlOrgID" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"
                                                    CssClass="form-control"
                                                    Enabled="false">
                                                </asp:DropDownList>

                                            </div>
                                            <asp:Label ID="Label70" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="หน่วยงาน :" />
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txtdept_cost" runat="server" Visible="false"
                                                    Enabled="false"
                                                    Text='<%# Eval("RDeptID_ref") %>'
                                                    CssClass="form-control">
                                                        </asp:TextBox>
                                                <asp:DropDownList ID="ddlRDeptID_ref" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"
                                                    CssClass="form-control"
                                                    Enabled="false">
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                        <div class="form-group">

                                            <asp:Label ID="Label68" class="col-md-2 control-labelnotop text_right" runat="server" Text="งบประมาณลง Cost Center :" />
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txtcostcenter" runat="server" Visible="false"
                                                    Enabled="false"
                                                    Text='<%# Eval("costcenter_idx_ref") %>'
                                                    CssClass="form-control">
                                                        </asp:TextBox>
                                                <asp:DropDownList ID="ddlcostcenter_idx_ref" runat="server"
                                                    CssClass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                    <%-- OnSelectedIndexChanged="FvDetail_DataBound"--%>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3"
                                                    ValidationGroup="btnSaveInsert" runat="server"
                                                    Display="Dynamic"
                                                    SetFocusOnError="true"
                                                    ControlToValidate="ddlcostcenter_idx_ref"
                                                    Font-Size="1em" ForeColor="Red"
                                                    InitialValue="0"
                                                    CssClass="pull-left"
                                                    ErrorMessage="กรุณาเลือกงบประมาณลง Cost Center" />


                                            </div>


                                            <asp:Label ID="Label120" class="col-md-2 control-labelnotop text_right" runat="server" Text="งบประมาณที่มี ณ ปัจจุบัน:" />
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txtshow_planbudget_total" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                            <asp:Label ID="Label121" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="บาท" />

                                            <div class="col-md-2">
                                            </div>

                                        </div>




                                        <asp:Panel ID="pnl_expenses" runat="server">

                                            <div class="form-group">

                                                <asp:Label ID="Label47" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายการ :" />
                                                <div class="col-md-3">
                                                    <asp:DropDownList ID="ddlexpenses_description" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:TextBox ID="txtexpenses_description" runat="server"
                                                        CssClass="form-control" Visible="false">
                                            </asp:TextBox>
                                                </div>


                                            </div>

                                            <div class="form-group" id="divother_expense" runat="server" visible="false">
                                                <asp:Label ID="Label118" class="col-md-2 control-labelnotop text_right" runat="server" Text="ระบุค่าใช้จ่าย :" />
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtexpense_other" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="5">
                                                    </asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                        ValidationGroup="Saveexpense" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="txtexpense_other"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        InitialValue=""
                                                        ErrorMessage="กรุณาระบุค่าใช้จ่าย" />


                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label48" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="มูลค่า :" />
                                                <div class="col-md-2">
                                                    <asp:TextBox ID="txtamount" runat="server"
                                                        TextMode="Number"
                                                        CssClass="form-control" Enabled="false">
                                            </asp:TextBox>

                                                </div>
                                                <asp:Label ID="Label49" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="บาท" />

                                                <asp:Label ID="Label50" class="col-md-2 control-labelnotop text_right" runat="server" Text="Vat :" />
                                                <div class="col-md-2">
                                                    <asp:TextBox ID="txtvat" runat="server"
                                                        TextMode="Number"
                                                        CssClass="form-control" Enabled="false">
                                            </asp:TextBox>
                                                </div>
                                                <asp:Label ID="Label53" CssClass="col-md-2 control-labelnotop textleft" runat="server" Text="%" />



                                            </div>

                                            <div class="form-group">


                                                <asp:Label ID="Label51" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="หัก ณ ที่จ่าย :" />
                                                <div class="col-md-2">
                                                    <asp:TextBox ID="txtwithholding_tax" runat="server"
                                                        TextMode="Number" Text="0"
                                                        CssClass="form-control" Enabled="true">
                                            </asp:TextBox>

                                                </div>
                                                <asp:Label ID="Label52" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="%" />
                                            </div>

                                            <div class="form-group">

                                                <asp:Label ID="Label54" class="col-md-2 control-labelnotop text_right" runat="server" Text="" />
                                                <div class="col-md-9">
                                                    <asp:LinkButton ID="btnAdd_trn_expen" Visible="true" ValidationGroup="Saveexpense"
                                                        CssClass="btn btn-primary" runat="server"
                                                        data-original-title="เพิ่ม" data-toggle="tooltip" Text="<i class='fa fa-plus'></i> เพิ่ม"
                                                        OnCommand="btnCommand" CommandName="btnAdd_trn_expen">
                                                       
                                            </asp:LinkButton>
                                                </div>

                                            </div>
                                        </asp:Panel>
                                        <div class="form-group">

                                            <asp:Label ID="Label55" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                            <div class="col-md-9">
                                                <div>

                                                    <div class="panel-info">
                                                        <div class="panel-heading f-bold">รายละเอียดค่าใช้จ่ายฝึกอบรม</div>
                                                    </div>
                                                    <asp:GridView ID="Gvtrn_expenses" runat="server"
                                                        CssClass="table table-striped table-responsive table-bordered"
                                                        GridLines="None" OnRowCommand="onRowCommand"
                                                        AutoGenerateColumns="false">
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                                HeaderStyle-Width="5%">
                                                                <ItemTemplate>
                                                                    <small>
                                                                        <%# (Container.DataItemIndex +1) %>
                                                            </small>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="รายการ"
                                                                ItemStyle-HorizontalAlign="Left"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtemp_expenses_description_edit_L" runat="server" Text='<%# Bind("expenses_description") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtemp_expenses_description_item_L" runat="server" Text='<%# Eval("expenses_description") %>'
                                                                        Width="100%"></asp:Label>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="มูลค่า"
                                                                ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtemp_amount_edit_L" runat="server" Text='<%# Bind("amount") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtemp_amount_item_L" runat="server" Text='<%# getStrformate((string)Eval("amount"),2) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="Vat"
                                                                ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small"
                                                                HeaderStyle-Width="10%">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtemp_vat_edit_L" runat="server" Text='<%# Bind("vat") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtemp_vat_item_L" runat="server" Text='<%# getStrformate((string)Eval("vat"),2) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField
                                                                HeaderText="หัก ณ ที่จ่าย"
                                                                ItemStyle-HorizontalAlign="Right"
                                                                HeaderStyle-CssClass="text-center"
                                                                ItemStyle-Font-Size="Small"
                                                                HeaderStyle-Width="10%">
                                                                <EditItemTemplate>
                                                                    <asp:TextBox ID="txtemp_withholding_tax_edit_L" runat="server" Text='<%# Bind("withholding_tax") %>'></asp:TextBox>
                                                                </EditItemTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="txtemp_withholding_tax_item_L" runat="server" Text='<%# getStrformate((string)Eval("withholding_tax"),2) %>'
                                                                        Width="100%"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                                                <ItemTemplate>
                                                                    <div class="word-wrap">
                                                                        <asp:LinkButton ID="btnDel_trn_expenses_L"
                                                                            CssClass="btn-danger btn-sm" runat="server"
                                                                            data-original-title="ลบ" data-toggle="tooltip" Text="ลบ"
                                                                            OnCommand="btnCommand" CommandName="btnDel_trn_expenses_L">
                                                                           <i class="fa fa-close"></i></asp:LinkButton>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label46" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                            <div class="col-md-9">

                                                <div class="panel panel-default">
                                                    <div class="panel-body">

                                                        <div class="form-group">

                                                            <asp:Label ID="Label57" class="col-md-2 control-labelnotop text_right" runat="server" Text="รวม :" />
                                                            <div class="col-md-2">
                                                                <asp:TextBox ID="txttraining_course_total" runat="server"
                                                                    Enabled="false"
                                                                    Text='<%# Eval("training_course_total") %>'
                                                                    CssClass="form-control">
                                                        </asp:TextBox>
                                                            </div>
                                                            <asp:Label ID="Label60" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="บาท" />

                                                            <asp:Label ID="Label58" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="เฉลี่ย :" />
                                                            <div class="col-md-2">
                                                                <asp:TextBox ID="txttraining_course_total_avg" runat="server"
                                                                    Text='<%# Eval("training_course_total_avg") %>'
                                                                    Enabled="false"
                                                                    CssClass="form-control">
                                                        </asp:TextBox>

                                                            </div>
                                                            <asp:Label ID="Label59" CssClass="col-md-2 control-labelnotop textleft" runat="server" Text="บาท/ท่าน" />
                                                        </div>

                                                        <div class="form-group">

                                                            <asp:Label ID="Label61" class="col-md-2 control-labelnotop text_right" runat="server" Text="ลดหย่อนภาษี :" />
                                                            <div class="col-md-2">
                                                                <asp:TextBox ID="txttraining_course_reduce_tax" runat="server"
                                                                    TextMode="Number"
                                                                    Text='<%# Eval("training_course_reduce_tax") %>'
                                                                    CssClass="form-control">
                                                        </asp:TextBox>
                                                            </div>
                                                            <asp:Label ID="Label62" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="%" />

                                                            <div class="col-md-2">
                                                            </div>

                                                        </div>

                                                        <div class="form-group">

                                                            <asp:Label ID="Label63" class="col-md-2 control-labelnotop text_right" runat="server" Text="ค่าใช้จ่ายสุทธิ :" />
                                                            <div class="col-md-2">
                                                                <asp:TextBox ID="txttraining_course_net_charge" runat="server"
                                                                    Enabled="false"
                                                                    Text='<%# Eval("training_course_net_charge") %>'
                                                                    CssClass="form-control">
                                                        </asp:TextBox>
                                                            </div>
                                                            <asp:Label ID="Label64" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="บาท" />

                                                            <asp:Label ID="Label65" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="เฉลี่ย :" />
                                                            <div class="col-md-2">
                                                                <asp:TextBox ID="txttraining_course_net_charge_tax" runat="server"
                                                                    Enabled="false"
                                                                    Text='<%# Eval("training_course_net_charge_tax") %>'
                                                                    CssClass="form-control">
                                                        </asp:TextBox>

                                                            </div>
                                                            <asp:Label ID="Label66" CssClass="col-md-2 control-labelnotop textleft" runat="server" Text="บาท/ท่าน" />
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label67" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                            <div class="col-md-9">

                                                <div class="panel panel-default">
                                                    <div class="panel-body">



                                                        <div class="form-group">

                                                            <asp:Label ID="Label72" class="col-md-2 control-labelnotop text_right" runat="server" Text="แผนงบประมาณที่วางไว้ รวม :" />
                                                            <div class="col-md-3">
                                                                <asp:TextBox ID="txttraining_course_planbudget_total" runat="server" TextMode="Number"
                                                                    CssClass="form-control" Enabled="false" Text='<%# Eval("training_course_planbudget_total") %>'>
                                                        </asp:TextBox>
                                                                <%--<asp:TextBox ID="txttraining_course_planbudget_total" runat="server"
                                                            TextMode="Number"
                                                            AutoPostBack="true"
                                                            OnTextChanged="onTextChanged"
                                                            Text='<%# Eval("training_course_planbudget_total") %>'
                                                            CssClass="form-control">
                                                        </asp:TextBox>--%>

                                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5"
                                                            ValidationGroup="btnSaveInsert" runat="server"
                                                            Display="Dynamic"
                                                            SetFocusOnError="true"
                                                            ControlToValidate="txttraining_course_planbudget_total"
                                                            Font-Size="1em" ForeColor="Red"
                                                            CssClass="pull-left"
                                                            InitialValue=""
                                                            ErrorMessage="กรุณากรอกแผนงบประมาณที่วางไว้" />--%>
                                                            </div>
                                                            <asp:Label ID="Label73" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="บาท" />

                                                            <div class="col-md-2">
                                                            </div>

                                                        </div>

                                                        <div class="form-group">

                                                            <asp:Label ID="Label69" class="col-md-2 control-labelnotop text_right" runat="server" Text="งบประมาณที่ใช้รวมครั้งนี้ :" />
                                                            <div class="col-md-3">
                                                                <asp:TextBox ID="txttraining_course_budget_total" runat="server"
                                                                    Enabled="false"
                                                                    Text='<%# Eval("training_course_budget_total") %>'
                                                                    CssClass="form-control">
                                                        </asp:TextBox>
                                                            </div>
                                                            <asp:Label ID="Label71" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="บาท" />

                                                            <div class="col-md-2">
                                                            </div>

                                                        </div>

                                                        <div class="form-group">

                                                            <asp:Label ID="Label74" class="col-md-2 control-labelnotop text_right" runat="server" Text="งบประมาณคงเหลือ :" />
                                                            <div class="col-md-3">



                                                                <asp:TextBox ID="txttraining_course_budget_balance_show" runat="server" Visible="false" Text='<%# Eval("training_course_budget_balance") %>'
                                                                    Enabled="false"
                                                                    CssClass="form-control" />

                                                                <asp:TextBox ID="txttraining_course_budget_balance" runat="server"
                                                                    Enabled="false"
                                                                    CssClass="form-control">
                                                        </asp:TextBox>
                                                            </div>
                                                            <asp:Label ID="Label75" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="บาท" />

                                                            <div class="col-md-2">
                                                            </div>

                                                        </div>

                                                        <div class="form-group">

                                                            <asp:Label ID="Label76" class="col-md-2 control-labelnotop text_right" runat="server" Text="" />
                                                            <div class="col-md-2">
                                                                <asp:TextBox ID="txttraining_course_budget_total_per" runat="server"
                                                                    Enabled="false"
                                                                    Text='<%# Eval("training_course_budget_total_per") %>'
                                                                    CssClass="form-control">
                                                        </asp:TextBox>
                                                            </div>
                                                            <asp:Label ID="Label77" CssClass="col-md-7 control-labelnotop textleft-red" runat="server" Text="%(ค่าใช้จ่ายที่ลงทั้งหมดเป็นค่าใช้จ่ายก่อน Vat เสมอ )" />



                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <hr />
                                        <asp:UpdatePanel ID="updatechklist" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <div class="col-md-2">
                                                        <asp:Label ID="Label102" class="control-labelnotop pull-right" runat="server" Text="เอกสารแนบ :" />
                                                    </div>
                                                    <div class="col-md-8">
                                                        <asp:CheckBoxList ID="chkchoice_file" runat="server" OnSelectedIndexChanged="onSelectedIndexChanged" AutoPostBack="true" CssClass="radio-list-inline-emps" RepeatDirection="Horizontal">
                                                        </asp:CheckBoxList>

                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="chkchoice_file" />
                                            </Triggers>
                                        </asp:UpdatePanel>

                                        <div class="form-group" id="div_otherreason" runat="server" visible="false">
                                            <div class="col-md-2">
                                                <asp:Label ID="Label103" class="control-labelnotop pull-right" runat="server" Text="ระบุไฟล์เพิ่มเติม :" />
                                            </div>
                                            <div class="col-md-8">
                                                <asp:TextBox ID="txtother" colspan="4" CssClass="form-control" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>

                                            </div>
                                        </div>

                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                                            <ContentTemplate>

                                                <div class="form-group">
                                                    <asp:Label ID="Label10s" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                                    <div class="col-sm-7">
                                                        <asp:FileUpload ID="UploadFileCourse" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-warning btn-sm multi" accept="jpg|pdf|png" />
                                                        <small>
                                                            <p class="help-block"><font color="red">**Attach File name jpg,png,pdf</font></p>
                                                        </small>
                                                    </div>
                                                </div>

                                            </ContentTemplate>
                                         </asp:UpdatePanel>

                                        <div class="form-group" runat="server" visible="false">

                                            <div class="col-md-2">
                                                <asp:Label ID="Label90" class="control-labelnotop pull-right" runat="server" Text="ไฟล์เอกสาร :" />
                                            </div>
                                            <div class="col-md-2">
                                            </div>
                                            <div class="col-md-2" runat="server" visible="false">
                                                <asp:Panel ID="pnl_file" runat="server">
                                                    <input type="file" id="txtfileimport"
                                                        onchange="imageproposUpload(this)">

                                                    <asp:TextBox ID="txttraining_course_file_name" runat="server"
                                                        Visible="false"
                                                        Text='<%# Eval("training_course_file_name") %>'>
                                            </asp:TextBox>
                                                </asp:Panel>
                                                <img alt="" id="imgfile_data" class="pull-left"
                                                    style="height: 30px; width: 30px;"
                                                    src='<%= getImg() %>' />
                                                <label><%= getImgFileType() %></label>

                                            </div>
                                            <div class="col-md-4">
                                                <asp:Panel ID="pnl_file_btn" runat="server">
                                                    <asp:LinkButton ID="btninvestigatefile"
                                                        CssClass="btn-primary btn-sm" runat="server"
                                                        data-original-title="ตรวจสอบไฟล์" data-toggle="tooltip" Text="ตรวจสอบไฟล์"
                                                        OnCommand="btnCommand" CommandName="">
                                            </asp:LinkButton>
                                                    <asp:LinkButton ID="btnClearfile"
                                                        CssClass="btn-warning btn-sm" runat="server"
                                                        data-original-title="เคลียร์ไฟล์" data-toggle="tooltip" Text="เคลียร์ไฟล์"
                                                        OnCommand="btnCommand" CommandName="btnClearfile">
                                            </asp:LinkButton>

                                                    <asp:LinkButton ID="btnDeletfileMAS"
                                                        CssClass="btn-danger btn-sm" runat="server"
                                                        data-original-title="ลบไฟล์" data-toggle="tooltip" Text="ลบไฟล์"
                                                        OnCommand="btnCommand" CommandName="btnDeletfileMAS">
                                            </asp:LinkButton>
                                                </asp:Panel>
                                            </div>



                                        </div>

                                        <%--  --%>
                                        <asp:TextBox ID="txtsuper_app_status" runat="server"
                                            Visible="false" Text='<%# Eval("super_app_status") %>'>
                                </asp:TextBox>
                                        <asp:TextBox ID="txthr_status" runat="server"
                                            Visible="false" Text='<%# Eval("hr_status") %>'>
                                </asp:TextBox>

                                        <%--  --%>

                                        <div class="form-group">
                                            <asp:Label ID="Label119" CssClass="col-sm-2 control-label" runat="server" />

                                            <div class="col-lg-8">
                                                <asp:GridView ID="gvFile" Visible="true" runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                                    HeaderStyle-CssClass="default"
                                                    OnRowDataBound="onRowDataBound"
                                                    BorderStyle="None"
                                                    CellSpacing="2"
                                                    Font-Size="Small">

                                                    <Columns>
                                                        <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                                            <ItemTemplate>
                                                                <div class="col-lg-10">
                                                                    <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                                    <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                                </div>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                                <h4></h4>
                                            </div>
                                        </div>

                                        <div class="form-group">

                                            <asp:Label ID="Label17" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สถานะการใช้งาน :" />
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddltraining_course_status" runat="server" CssClass="form-control"
                                                    SelectedValue='<%# Eval("training_course_status") == null ? "1" : Eval("training_course_status") %>'>
                                                    <asp:ListItem Value="1" Text="ใช้งาน" />
                                                    <asp:ListItem Value="0" Text="ไม่ใช้งาน" />
                                                </asp:DropDownList>
                                            </div>

                                        </div>
                                        <asp:Panel ID="pnlhistory" runat="server" Visible="false">


                                            <div class="form-group">
                                                <asp:Label ID="Label97" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                                <div class="col-md-9">
                                                    <div>

                                                        <div class="panel-info">
                                                            <div class="panel-heading f-bold">รายละเอียดการบันทึกข้อมูล</div>
                                                        </div>
                                                        <asp:GridView ID="GvHistory" runat="server"
                                                            CssClass="table table-bordered word-wrap"
                                                            GridLines="None" OnRowCommand="onRowCommand"
                                                            AutoGenerateColumns="false">
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">ไม่พบข้อมูล</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField
                                                                    HeaderText="วันที่ / เวลา"
                                                                    ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtzdate_edit_L" runat="server" Text='<%# Bind("zdate") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtzdate_item_L" runat="server" Text='<%# Eval("zdate") %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="ผู้ดำเนินการ"
                                                                    ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtFullNameTH_edit_L" runat="server" Text='<%# Bind("FullNameTH") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtFullNameTH_item_L" runat="server" Text='<%# Eval("FullNameTH") %>'></asp:Label>
                                                                        &nbsp; 
                                                               (
                                                                <asp:Label ID="Label104" runat="server" Text='<%# Eval("actor_name") %>'></asp:Label>)
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="ดำเนินการ"
                                                                    ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtnode_name_edit_L" runat="server" Text='<%# Bind("node_name") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtnode_name_item_L" runat="server" Text='<%# Eval("node_name") %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="ผลการดำเนินการ"
                                                                    ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtdecision_name_edit_L" runat="server" Text='<%# Bind("decision_name") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtdecision_name_item_L" runat="server" Text='<%# Eval("decision_name") %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField
                                                                    HeaderText="สาเหตุ / หมายเหตุ"
                                                                    ItemStyle-HorizontalAlign="Left"
                                                                    HeaderStyle-CssClass="text-center"
                                                                    ItemStyle-Font-Size="Small"
                                                                    HeaderStyle-Width="25%">
                                                                    <EditItemTemplate>
                                                                        <asp:TextBox ID="txtapprove_remark_edit_L" runat="server" Text='<%# Bind("approve_remark") %>'></asp:TextBox>
                                                                    </EditItemTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="txtapprove_remark_item_L" runat="server" Text='<%# Eval("approve_remark") %>'
                                                                            Width="100%"></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>

                                            </div>

                                        </asp:Panel>
                                        <%-- End  --%>
                                    </div>
                                </div>
                            </div>
                        </EditItemTemplate>


                    </asp:FormView>

                    <div class="row">

                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-2"></div>
                                <div class="col-md-7">

                                    <%--<asp:Panel ID="pnlSave" runat="server">--%>
                                    <asp:LinkButton CssClass="btn btn-success" runat="server"
                                        CommandName="btnSaveInsert" OnCommand="btnCommand"
                                        data-toggle="tooltip" title="บันทึก"
                                        ID="btnSaveInsert"
                                        ValidationGroup="btnSaveInsert">
                                   <i class="fa fa-save fa-lg"></i> <%= getlabelSave() %>
                            </asp:LinkButton>
                                    <%--</asp:Panel>--%>

                                    <asp:LinkButton CssClass="btn btn-warning"
                                        ID="btnClear" Visible="false"
                                        OnCommand="btnCommand" CommandName="btnInsert"
                                        data-toggle="tooltip" title="ยกเลิก" runat="server"
                                        Text="<i class='fa fa-close fa-lg'></i> ยกเลิก" />
                                    <asp:LinkButton CssClass="btn btn-danger"
                                        data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                        Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                        ID="btnCancel"
                                        CommandName="btnCancel" OnCommand="btnCommand" />


                                </div>
                            </div>
                        </div>
                    </div>


                    <div id="div_approve" runat="server" visible="false">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-envelope"></i><strong>&nbsp; ข้อมูลส่วนอนุมัติรายการ</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">

                                        <asp:Label ID="Labsael33" CssClass="col-sm-2 control-label text_right" runat="server" Text="สถานะอนุมัติ" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_approve" CssClass="form-control" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกสถานะอนุมัติ...</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValisdator15" ValidationGroup="SaveApprove" runat="server" Display="None"
                                                ControlToValidate="ddl_approve" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกสถานะอนุมัติ"
                                                ValidationExpression="กรุณาเลือกสถานะอนุมัติ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenader16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValisdator15" Width="160" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="Label54" runat="server" Text="ความคิดเห็นเพิ่มเติม" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtremark_approve" TextMode="multiline" Rows="5" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator19" ValidationGroup="SaveApprove" runat="server" Display="None" ControlToValidate="txtremark_approve" Font-Size="11"
                                                ErrorMessage="กรุณากรอกความคิดเห็น"
                                                ValidationExpression="กรุณากรอกความคิดเห็น"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender27" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator19" Width="160" />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server"
                                                ValidationGroup="SaveApprove" Display="None"
                                                ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                ControlToValidate="txtremark_approve"
                                                ValidationExpression="^[\s\S]{0,1000}$"
                                                SetFocusOnError="true" />

                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorsCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator8" Width="160" />

                                        </div>


                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-2">
                                            <asp:LinkButton ID="btnApprove" ValidationGroup="SaveApprove" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdUpdateApprove" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton1" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnBack_Approve" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSaveInsert" />
                    <asp:PostBackTrigger ControlID="btnApprove" />
                </Triggers>
            </asp:UpdatePanel>
            <asp:Panel ID="box_print_course" runat="server" Visible="true">
                <asp:FormView ID="FvTemplate_print" runat="server" HorizontalAlign="Center" DefaultMode="ReadOnly" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <div id="printable_course" class="print_report hidden">
                            <div class="panel panel-default">

                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 15px">
                                    <tr>
                                        <td style="text-align: center;">
                                            <h3 style="text-align: center;">บริษัท เถ้าแก่น้อย ฟู๊ดแอนด์มาร็เก็ตติ้ง จำกัด (มหาชน)</h3>
                                            <h3 style="text-align: center;">Taokaenoi Food & Marketing Public Company Limited</h3>
                                            <h3 style="text-align: center;">แบบฟอร์มการขออนุมัติฝึกอบรม/สัมนา/ดูงานนอกสถานที่</h3>
                                        </td>

                                    </tr>
                                    <%--  <tr>
                                          <td>
                                            <h3 style="text-align: center;">Taokaenoi Food & Marketing Public Company Limited</h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: center;">
                                            <h3 style="text-align: center;">แบบฟอร์มการขออนุมัติฝึกอบรม/สัมนา/ดูงานนอกสถานที่</h3>
                                        </td>
                                         <td>
                                            <h3 style="text-align: center;">Taokaenoi Food & Marketing Public Company Limited แบบฟอร์มการขออนุมัติฝึกอบรม/สัมนา/ดูงานนอกสถานที่</h3>
                                        </td>
                                    </tr>--%>
                                    <tr>
                                        <td>
                                            <h5 style="text-align: right;">วันที่ออกเอกสาร.....<asp:Label ID="lbldate" runat="server"></asp:Label>
                                                ..... ผู้ประสานงาน ....<%# Eval("emp_name_th") %>....</h5>
                                        </td>
                                    </tr>

                                </table>

                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 15px">
                                    <tr>
                                        <td>
                                            <b style="background-color: #9bcfec; color: black">ส่วนที่ 1 รายละเอียดหลักสูตร</b>
                                        </td>
                                        <td>
                                            <asp:RadioButtonList ID="rdoplan" runat="server" RepeatDirection="Horizontal" Font-Size="15px">
                                                <asp:ListItem Value="I">ฝึกอบรมภายใน</asp:ListItem>
                                                <asp:ListItem Value="O">ฝึกอบรมภายนอก</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </td>
                                        <td>
                                            <h5 style="text-align: right;">
                                            วันที่................../................./................. ถึง ................./................./..................เวลา.......................ถึง.......................น.
                                        </td>
                                    </tr>
                                </table>

                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 15px">
                                    <tr>
                                        <td>หัวข้อ/หลักสูตร : &nbsp;&nbsp;&nbsp;  <%# Eval("zcourse_name") %>
                                        </td>
                                    </tr>
                                </table>

                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 15px">
                                    <tr>
                                        <td style="width: 100px;">สถานที่ : &nbsp;&nbsp;&nbsp;  <%# Eval("location_name") %>
                                        </td>
                                        <td>วิทยากร/สถาบัน : &nbsp;&nbsp;&nbsp; 
                                            <asp:Label ID="lblinsitution" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>

                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 15px">
                                    <tr>
                                        <td>
                                            <b style="background-color: #9bcfec; color: black">ส่วนที่ 2 เอกสารแนบ</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBoxList ID="chkfilelist" runat="server" RepeatDirection="Horizontal" Font-Size="15px">
                                            </asp:CheckBoxList>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="divfile_reason" runat="server" visible="false">
                                                ระบุข้อมูลเพิ่มเติม :&nbsp;&nbsp;&nbsp;   <%# Eval("file_reason") %>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 15px">
                                    <tr>
                                        <td>
                                            <b style="background-color: #9bcfec; color: black">ส่วนที่ 3 วัตถุประสงค์</b>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>วัตถุประสงค์ :&nbsp;&nbsp;&nbsp;      
                                            <asp:Label ID="lblobjective" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>

                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 15px">
                                    <tr>
                                        <td>
                                            <b style="background-color: #9bcfec; color: black">ส่วนที่ 4 รายละเอียดผู้เข้าร่วมอบรม</b>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>กลุ่มเป้าหมาย :&nbsp;&nbsp;&nbsp;    <%# Eval("target_group_name") %> 
                                        </td>
                                        <td>จำนวน :&nbsp;&nbsp;&nbsp;    
                                            <asp:Label ID="lblcountqty" runat="server"></asp:Label>
                                            &nbsp;&nbsp;&nbsp;คน
                                        </td>
                                    </tr>


                                </table>

                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 15px">
                                    <tr>
                                        <td>

                                            <asp:Literal ID="litEmpCode" runat="server" />

                                        </td>
                                    </tr>

                                    <tr>
                                        <td>(กรณีเกิน 3 ท่าน กรุณาแนบรายชื่อ)  &nbsp;&nbsp;&nbsp;
                                            <asp:RadioButton ID="rdoempfile" runat="server" Text="รายชื่อตามเอกสารแนบ " />
                                            &nbsp;&nbsp;
                                                <asp:RadioButton ID="rdoempform" runat="server" Text="รายชื่อตามบันทึกภายใน " />
                                        </td>
                                    </tr>
                                </table>

                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 15px">
                                    <tr>
                                        <td>
                                            <b style="background-color: #9bcfec; color: black">ส่วนที่ 5 รายละเอียดแผน และงบประมาณ</b>
                                        </td>
                                        <td>
                                            <asp:RadioButton ID="rdofree" runat="server" Text="ฟรี ไม่มีค่าใช้จ่าย" />
                                            <asp:RadioButton ID="rdocost_notover" runat="server" Text="มีค่าใช้จ่าย วงเงินอยู่ในงบประมาณประจำปี" />
                                            <asp:RadioButton ID="rdocost_over" runat="server" Text="มีค่าใช้จ่าย เกินวงเงินในงบประมาณประจำปี" />
                                        </td>
                                    </tr>
                                </table>

                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 15px">
                                    <tr>
                                        <td>
                                            <asp:GridView ID="Gvexpenses_Print" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                HeaderStyle-CssClass="info"
                                                Font-Names="Angsana New"
                                                Font-Size="15px"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="false"
                                                DataKeyNames="u4_training_course_idx"
                                                OnRowDataBound="onRowDataBound">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="ลำดับที่" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center">

                                                        <ItemTemplate>
                                                            <%# (Container.DataItemIndex +1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ค่าใช้จ่าย" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Literal ID="litexpenses_description" runat="server" Text='<%# Eval("expenses_description") %>' />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="ราคา" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="35%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Literal ID="litamount" Visible="false" runat="server" Text='<%# Eval("amount") %>' />
                                                            <asp:Literal ID="litamount_comma" runat="server" />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Vat" ItemStyle-Width="25%" ItemStyle-VerticalAlign="Middle" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Literal ID="litvat" runat="server" Text='<%# Eval("vat") %>' />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="หัก ณ ที่จ่าย" ItemStyle-Width="10%" ItemStyle-VerticalAlign="Middle" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Literal ID="litwithholding_tax" runat="server" Text='<%# Eval("withholding_tax") %>' />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>

                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 15px">
                                    <tr>
                                        <td>รวม :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                            <asp:Literal ID="littraining_course_total" Visible="false" runat="server" Text='<%# Eval("training_course_total") %>' />

                                            <asp:Literal ID="littraining_course_total_comma" runat="server" />


                                            &nbsp;&nbsp;&nbsp; บาท&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                                            <asp:Literal ID="littraining_course_total_avg" Visible="false" runat="server" Text='<%# Eval("training_course_total_avg") %>' />
                                            <asp:Literal ID="littraining_course_total_avg_comma" runat="server" />
                                            บาท&nbsp;&nbsp;&nbsp;ต่อท่าน
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>ลดหย่อนภาษี :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <%# Eval("training_course_reduce_tax") %> &nbsp;&nbsp;&nbsp; %
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>งบประมาณลง Cost Center :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   <%# Eval("CostNo") %> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; หน่วยงาน :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <%# Eval("DeptNameTH") %>    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;แผนงบประมาณที่วางไว้ รวม :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                                <asp:Literal ID="littraining_course_planbudget_total" Visible="false" runat="server" Text='<%# Eval("training_course_planbudget_total") %>' />
                                            <asp:Literal ID="training_course_planbudget_total_comma" runat="server" />
                                            &nbsp;&nbsp;&nbsp; บาท
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>งบประมาณที่ใช้รวมครั้งนี้ :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   
                                               <asp:Literal ID="littraining_course_budget_total" Visible="false" runat="server" Text='<%# Eval("training_course_budget_total") %>' />
                                            <asp:Literal ID="littraining_course_budget_total_comma" runat="server" />
                                            &nbsp;&nbsp;&nbsp; บาท&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; งบประมาณคงเหลือ :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                               <asp:Literal ID="littraining_course_budget_balance" Visible="false" runat="server" Text='<%# Eval("training_course_budget_balance") %>' />
                                            <asp:Literal ID="littraining_course_budget_balance_comma" runat="server" />

                                            &nbsp;&nbsp;&nbsp;บาท&nbsp;&nbsp;&nbsp;(  <%# Eval("training_course_budget_total_per") %>  % )&nbsp;&nbsp;(ค่าใช้จ่ายที่ลงทั้งหมดเป็นค่าใช้จ่ายก่อน Vat เสมอ)

                                        </td>
                                    </tr>
                                </table>

                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 15px">
                                    <tr>
                                        <td>
                                            <b style="background-color: #9bcfec; color: black">ส่วนที่ 6 การวัด ประเมิน และติดตามผล</b>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <div id="div_inplan" runat="server" visible="true">
                                                ฝึกอบรมภายใน &nbsp;&nbsp;&nbsp;
                                                <asp:RadioButton ID="rdopass" runat="server" Text="ผ่านการทดสอบ " />
                                                &nbsp;&nbsp;
                                                   <asp:Literal ID="litpass" runat="server" Text='<%# Eval("pass_test_per") %>' />&nbsp; %&nbsp;&nbsp;&nbsp;
                                                <asp:RadioButton ID="rdohour" runat="server" Text="ชั่วโมงเข้าฝึกอบรมครบ " />
                                                &nbsp;&nbsp;
                                                   <asp:Literal ID="lithour" runat="server" Text='<%# Eval("hour_training_per") %>' />&nbsp; %

                                            </div>

                                            <div id="div_outplan" runat="server" visible="true">
                                                ฝึกอบรมภายนอก &nbsp;&nbsp; 
                                                <asp:RadioButton ID="rdoreport" runat="server" Text="เขียนรายงานการอบรม " />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:RadioButton ID="rdoshare" runat="server" Text="เผยแพร่ต่อผู้เกี่ยวข้องในรูปแบบ " />
                                                &nbsp;&nbsp;
                                                <asp:Literal ID="litpublish_des" runat="server" Text='<%# Eval("pass_test_per") %>' />&nbsp;&nbsp;&nbsp;&nbsp;
                                                <br />
                                                &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:RadioButton ID="rdocourse" runat="server" Text="จัดทำสูตรและเป็นวิทยากรภายใน " />
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:RadioButton ID="rdoother" runat="server" Text="อื่นๆ ระบุ " />
                                                &nbsp;&nbsp;
                                                <asp:Literal ID="litother" runat="server" Text='<%# Eval("other_description") %>' />&nbsp;&nbsp;&nbsp;&nbsp;
                                              
                                            </div>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>การติดตามผลโดย HROD &nbsp;&nbsp;&nbsp;
                                                <asp:RadioButton ID="rdohrnofollow" runat="server" Text="ไม่ติดตามผล " />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <asp:RadioButton ID="rdohrfollow" runat="server" Text="ติดตามผลภายใน " />
                                            &nbsp;&nbsp;
                                                <asp:Literal ID="lithrdate" runat="server" />&nbsp;&nbsp;วัน
                                              
                                        </td>
                                    </tr>
                                </table>

                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 15px">
                                    <tr>

                                        <td style="text-align: center; vertical-align: middle; width: 400px;">&nbsp;&nbsp;&nbsp;
                                                <asp:RadioButton ID="rdoapprove1" runat="server" Text="เห็นชอบ" Checked="true" />
                                            &nbsp;&nbsp;&nbsp;
                                                <asp:RadioButton ID="rdounapprove1" runat="server" Text="ไม่เห็นชอบ" />
                                        </td>
                                        <td style="text-align: center; vertical-align: middle; width: 400px;">&nbsp;&nbsp;&nbsp;
                                                <asp:RadioButton ID="rdoapprove2" runat="server" Text="เห็นชอบ" Checked="true" />
                                            &nbsp;&nbsp;&nbsp;
                                                <asp:RadioButton ID="rdounapprove2" runat="server" Text="ไม่เห็นชอบ" />
                                        </td>
                                        <td style="text-align: center; vertical-align: middle; width: 400px;">&nbsp;&nbsp;&nbsp;
                                                <asp:RadioButton ID="rdoapprove3" runat="server" Text="เห็นชอบ" Checked="true" />
                                            &nbsp;&nbsp;&nbsp;
                                                <asp:RadioButton ID="rdounapprove3" runat="server" Text="ไม่เห็นชอบ" />
                                        </td>


                                    </tr>
                                    <br />
                                    <br />
                                    <tr>

                                        <td style="text-align: center; vertical-align: middle; width: 400px;" rowspan="3">
                                            <br />
                                            ลงชื่อ  .......................................................
                                           
                                            <br />
                                            (คุณ&nbsp;&nbsp; 
                                            <asp:Label ID="Label58" class="control-labelnotop" Text='<%# Eval("Approve1") %>' runat="server"> </asp:Label>
                                            &nbsp;&nbsp;)    
                                             <br />
                                            ตำแหน่ง &nbsp;&nbsp; 
                                            <asp:Label ID="Label105" class="control-labelnotop" Text='<%# Eval("Pos_Approve1") %>' runat="server"> </asp:Label>
                                            &nbsp;&nbsp;  
                                            <br />
                                            วันที่ 
                                            <asp:Label ID="Label63" class="control-labelnotop" Text='<%# Eval("dateApprove1") %>' runat="server"> </asp:Label>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle; width: 400px;" rowspan="3">
                                            <br />
                                            ลงชื่อ  .......................................................
                                           
                                            <br />
                                            <div id="nameapprove_hrdmana" runat="server" visible="false">
                                                (คุณ&nbsp;&nbsp; 
                                            <asp:Label ID="Label106" class="control-labelnotop" Text='<%# Eval("Approve2") %>' runat="server"> </asp:Label>
                                                &nbsp;&nbsp;)    
                                             <br />
                                                ตำแหน่ง &nbsp;&nbsp; 
                                            <asp:Label ID="Label107" class="control-labelnotop" Text='<%# Eval("Pos_Approve2") %>' runat="server"> </asp:Label>
                                                &nbsp;&nbsp;  
                                            <br />
                                                วันที่ 
                                            <asp:Label ID="Label108" class="control-labelnotop" Text='<%# Eval("dateApprove2") %>' runat="server"> </asp:Label>
                                            </div>

                                            <div id="nameapprove2to3_hrdmanager" runat="server" visible="false">
                                                (คุณ&nbsp;&nbsp; 
                                            <asp:Label ID="Label112" class="control-labelnotop" Text='<%# Eval("Approve3") %>' runat="server"> </asp:Label>
                                                &nbsp;&nbsp;)    
                                             <br />
                                                ตำแหน่ง &nbsp;&nbsp; 
                                            <asp:Label ID="Label113" class="control-labelnotop" Text='<%# Eval("Pos_Approve3") %>' runat="server"> </asp:Label>
                                                &nbsp;&nbsp;  
                                            <br />
                                                วันที่ 
                                            <asp:Label ID="Label114" class="control-labelnotop" Text='<%# Eval("dateApprove3") %>' runat="server"> </asp:Label>

                                            </div>
                                        </td>
                                        <td style="text-align: center; vertical-align: middle; width: 400px;" rowspan="3">
                                            <br />
                                            ลงชื่อ  .......................................................
                                           
                                            <br />

                                            <div id="nameapprove_md" runat="server" visible="false">
                                                (คุณ&nbsp;&nbsp; 
                                            <asp:Label ID="Label109" class="control-labelnotop" Text='<%# Eval("Approve3") %>' runat="server"> </asp:Label>
                                                &nbsp;&nbsp;)    
                                             <br />
                                                ตำแหน่ง &nbsp;&nbsp; 
                                            <asp:Label ID="Label110" class="control-labelnotop" Text='<%# Eval("Pos_Approve3") %>' runat="server"> </asp:Label>
                                                &nbsp;&nbsp;  
                                            <br />
                                                วันที่ 
                                            <asp:Label ID="Label111" class="control-labelnotop" Text='<%# Eval("dateApprove3") %>' runat="server"> </asp:Label>
                                            </div>

                                            <div id="nameapprove3to4_md" runat="server" visible="false">
                                                (คุณ&nbsp;&nbsp; 
                                            <asp:Label ID="Label115" class="control-labelnotop" Text='<%# Eval("Approve4") %>' runat="server"> </asp:Label>
                                                &nbsp;&nbsp;)    
                                             <br />
                                                ตำแหน่ง &nbsp;&nbsp; 
                                            <asp:Label ID="Label116" class="control-labelnotop" Text='<%# Eval("Pos_Approve4") %>' runat="server"> </asp:Label>
                                                &nbsp;&nbsp;  
                                            <br />
                                                วันที่ 
                                            <asp:Label ID="Label117" class="control-labelnotop" Text='<%# Eval("dateApprove4") %>' runat="server"> </asp:Label>

                                            </div>
                                        </td>
                                    </tr>

                                </table>

                                <br />
                                <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 15px">
                                    <tr>
                                        <td style="text-align: left">
                                            <h3>บังคับใช้ 09 ธ.ค.2562</h3>
                                        </td>
                                        <td style="text-align: right">
                                            <h3>FM-HR-TD-002/05 REV.05</h3>
                                        </td>
                                    </tr>
                                </table>

                                <div id="div_page2" runat="server" visible="false">
                                    <p style="page-break-before: always">

                                        <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 15px">
                                            <tr>
                                                <td>
                                                    <h3 style="text-align: center;">บริษัท เถ้าแก่น้อย ฟู๊ดแอนด์มาร็เก็ตติ้ง จำกัด (มหาชน)</h3>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <h3 style="text-align: center;">Taokaenoi Food & Marketing Public Company Limited แบบฟอร์มการขออนุมัติฝึกอบรม/สัมนา/ดูงานนอกสถานที่</h3>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="GvEmployee_Print" runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                        HeaderStyle-CssClass="info"
                                                        Font-Names="Angsana New"
                                                        Font-Size="15px"
                                                        HeaderStyle-Height="40px"
                                                        AllowPaging="false"
                                                        DataKeyNames="u3_training_course_idx">

                                                        <PagerStyle CssClass="pageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="ลำดับที่" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="center">

                                                                <ItemTemplate>
                                                                    <%# (Container.DataItemIndex +1) %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="litEmpCode" runat="server" Text='<%# Eval("EmpCode") %>' />

                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField HeaderText="ชื่อ-สกุล" ItemStyle-VerticalAlign="Middle" ItemStyle-Width="35%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="litFullNameTH" runat="server" Text='<%# Eval("FullNameTH") %>' />

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-Width="25%" ItemStyle-VerticalAlign="Middle" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="litPosNameTH" runat="server" Text='<%# Eval("PosNameTH") %>' />

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Cost Center" ItemStyle-Width="10%" ItemStyle-VerticalAlign="Middle" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="litCostNo" runat="server" Text='<%# Eval("CostNo") %>' />

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="เบอร์ติดต่อ" ItemStyle-Width="10%" ItemStyle-VerticalAlign="Middle" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Literal ID="litMobileNo" runat="server" Text='<%# Eval("MobileNo") %>' />

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                        </table>

                                        <br />
                                        <table class="table f-s-12 m-t-10 " style="border-collapse: collapse; font-family: 'Angsana New'; font-size: 15px">
                                            <tr>
                                                <td style="text-align: left">
                                                    <h3>บังคับใช้ 09 ธ.ค.2562</h3>
                                                </td>
                                                <td style="text-align: right">
                                                    <h3>FM-HR-TD-002/05 REV.05</h3>
                                                </td>
                                            </tr>
                                        </table>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:FormView>
            </asp:Panel>

        </asp:View>

        <asp:View ID="ViewApprove" runat="server">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:GridView ID="GvApprovelist"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                        HeaderStyle-CssClass="info"
                        AllowPaging="true"
                        PageSize="10"
                        DataKeyNames="u0_training_course_idx"
                        OnRowDataBound="onRowDataBound"
                        OnPageIndexChanging="gvPageIndexChanging"
                        ShowFooter="false">


                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size: 13px;">
                                        <%# (Container.DataItemIndex + 1) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size: 13px;">
                                        <asp:Label ID="lblunidx" runat="server" Visible="false" Text='<%# Eval("to_m0_node_idx") %>'></asp:Label>
                                        <%# Eval("training_course_no") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size: 13px;">
                                        <%# Eval("zdate") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทหลักสูตร" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size: 13px;">
                                        <%-- <%# course_plan_status_name((string)Eval("course_plan_status")) %>--%>
                                        <img src='<%# getImageIO((string)Eval("course_plan_status"),1) %>'
                                            width="50" height="50"
                                            title='<%# getImageIO((string)Eval("course_plan_status"),2) %>'
                                            id="imgicon_inplan" runat="server" visible="true" />
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size: 13px;">
                                        <%# Eval("course_name") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <%-- <asp:TemplateField HeaderText="กลุ่มวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                        HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <div class="word-wrap" style="font-size: 13px;">
                                <%# Eval("training_group_name") %>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สาขาวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                        HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <div class="word-wrap" style="font-size: 13px;">
                                <%# Eval("training_branch_name") %>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>--%>

                            <asp:TemplateField HeaderText="สถานะของเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                <ItemTemplate>
                                    <b>
                                        <asp:Label ID="lblStatusDoc" runat="server" Visible="true" Text='<%# Eval("StatusDoc") %>'></asp:Label>
                                        <asp:Label ID="lblstaidx" runat="server" Visible="false" Text='<%# Eval("el_approve_status") %>'></asp:Label>
                                    </b>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <div class="word-wrap">
                                        <%# getStatus( (int)Eval("training_course_status")) %>
                                        <br />
                                        <%# getStatus_Resulte( (int)Eval("zcount_resulte")) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                <ItemTemplate>


                                    <asp:TextBox ID="txtapprove_status_GvListData" runat="server"
                                        Visible="false" Text='<%# Eval("approve_status") %>' />
                                    <asp:TextBox ID="txtmd_approve_status_GvListData" runat="server"
                                        Visible="false" Text='<%# Eval("md_approve_status") %>' />
                                    <asp:TextBox ID="txtGvtraining_course_no" runat="server"
                                        Visible="false" Text='<%# Eval("training_course_no") %>' />
                                    <asp:TextBox ID="txtGvzacter_status" runat="server"
                                        Visible="false" Text='<%# Eval("zacter_status") %>' />
                                    <asp:TextBox ID="txtGvsuper_app_status" runat="server"
                                        Visible="false" Text='<%# Eval("super_app_status") %>' />

                                    <asp:LinkButton ID="btnDetail"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetail"
                                        CommandArgument='<%# Eval("u0_training_course_idx") + ";" + "2"  + ";" + Eval("to_m0_node_idx")  + ";" + Eval("to_m0_actor_idx") + ";" + Eval("el_approve_status") + ";" + Eval("type_request")  + ";" + Eval("training_course_created_by") + ";" + Eval("costcenter_idx_ref") + ";" + Eval("training_course_budget_total") + ";" + Eval("training_course_no") %>'>
                                            <i class="fa fa-file-alt"></i></asp:LinkButton>


                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>

        <asp:View ID="View_HR_WList" runat="server">

            <asp:Panel ID="Panel1" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_HR_W" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_HR_W" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                           
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_HR_W" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_HR_W" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvHR_WList"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    DataKeyNames="u0_training_course_idx"
                    ShowFooter="false">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# (Container.DataItemIndex + 1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("training_course_no") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("zdate") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ประเภทหลักสูตร" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%--<%# course_plan_status_name((string)Eval("course_plan_status")) %>--%>
                                    <img src='<%# getImageIO((string)Eval("course_plan_status"),1) %>'
                                        width="50" height="50"
                                        title='<%# getImageIO((string)Eval("course_plan_status"),2) %>'
                                        id="imgicon_inplan" runat="server" visible="true" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("course_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="กลุ่มวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("training_group_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สาขาวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("training_branch_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะของเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <span>

                                    <%# getTextAction((int)Eval("zacter_status"),(string)Eval("zdecision")) %>
                                </span>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <asp:LinkButton ID="btnDetail_GvHR_WList"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetail_GvHR_WList"
                                        CommandArgument='<%# Eval("u0_training_course_idx") %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

            </div>

        </asp:View>

        <asp:View ID="View_HR_AList" runat="server">

            <asp:Panel ID="Panel8" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_HR_A" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_HR_A" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                           
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_HR_A" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_HR_A" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvHR_AList"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    DataKeyNames="u0_training_course_idx"
                    ShowFooter="false">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# (Container.DataItemIndex + 1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("training_course_no") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("zdate") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ประเภทหลักสูตร" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%--<%# course_plan_status_name((string)Eval("course_plan_status")) %>--%>
                                    <img src='<%# getImageIO((string)Eval("course_plan_status"),1) %>'
                                        width="50" height="50"
                                        title='<%# getImageIO((string)Eval("course_plan_status"),2) %>'
                                        id="imgicon_inplan" runat="server" visible="true" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("course_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="กลุ่มวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("training_group_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สาขาวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("training_branch_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะของเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>

                                <span>
                                    <%--<%# getTextDoc((int)Eval("approve_status") , 
                                                       (int)Eval("md_approve_status") , 
                                                       (string)Eval("decision_name"),
                                                       (string)Eval("node_name"),
                                                       (string)Eval("actor_name"),
                                                       (string)Eval("md_decision_name"),
                                                       (string)Eval("md_node_name"),
                                                       (string)Eval("md_actor_name")
                                                       ) %>--%>
                                    <%# getTextAction((int)Eval("zacter_status"),(string)Eval("zdecision")) %>
                                </span>


                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <asp:LinkButton ID="btnDetail_GvHR_AList"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetail_GvHR_AList"
                                        CommandArgument='<%# Eval("u0_training_course_idx") %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

            </div>

        </asp:View>


        <%-- Start MD --%>


        <asp:View ID="View_MD_WList" runat="server">

            <asp:Panel ID="Panel9" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_MD_W" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_MD_W" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                           
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_MD_W" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_MD_W" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvMD_WList"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    DataKeyNames="u0_training_course_idx"
                    ShowFooter="false">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# (Container.DataItemIndex + 1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("training_course_no") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("zdate") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ประเภทหลักสูตร" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%--<%# course_plan_status_name((string)Eval("course_plan_status")) %>--%>
                                    <img src='<%# getImageIO((string)Eval("course_plan_status"),1) %>'
                                        width="50" height="50"
                                        title='<%# getImageIO((string)Eval("course_plan_status"),2) %>'
                                        id="imgicon_inplan" runat="server" visible="true" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("course_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="กลุ่มวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("training_group_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สาขาวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("training_branch_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะของเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>

                                <span>
                                    <%-- <%# getTextDoc((int)Eval("approve_status") , 
                                                       (int)Eval("md_approve_status") , 
                                                       (string)Eval("decision_name"),
                                                       (string)Eval("node_name"),
                                                       (string)Eval("actor_name"),
                                                       (string)Eval("md_decision_name"),
                                                       (string)Eval("md_node_name"),
                                                       (string)Eval("md_actor_name")
                                                       ) %>--%>
                                    <%# getTextAction((int)Eval("zacter_status"),(string)Eval("zdecision")) %>
                                </span>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <asp:LinkButton ID="btnDetail_GvMD_WList"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetail_GvMD_WList"
                                        CommandArgument='<%# Eval("u0_training_course_idx") %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

            </div>

        </asp:View>


        <asp:View ID="View_MD_AList" runat="server">

            <asp:Panel ID="Panel10" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_MD_A" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_MD_A" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                           
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_MD_A" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_MD_A" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvMD_AList"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    DataKeyNames="u0_training_course_idx"
                    ShowFooter="false">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# (Container.DataItemIndex + 1) %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("training_course_no") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("zdate") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ประเภทหลักสูตร" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%--<%# course_plan_status_name((string)Eval("course_plan_status")) %>--%>
                                    <img src='<%# getImageIO((string)Eval("course_plan_status"),1) %>'
                                        width="50" height="50"
                                        title='<%# getImageIO((string)Eval("course_plan_status"),2) %>'
                                        id="imgicon_inplan" runat="server" visible="true" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("course_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="กลุ่มวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("training_group_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สาขาวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <%# Eval("training_branch_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะของเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>

                                <span>
                                    <%-- <%# getTextDoc((int)Eval("approve_status") , 
                                                       (int)Eval("md_approve_status") , 
                                                       (string)Eval("decision_name"),
                                                       (string)Eval("node_name"),
                                                       (string)Eval("actor_name"),
                                                       (string)Eval("md_decision_name"),
                                                       (string)Eval("md_node_name"),
                                                       (string)Eval("md_actor_name")
                                                       ) %>--%>
                                    <%# getTextAction((int)Eval("zacter_status"),(string)Eval("zdecision")) %>
                                </span>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap" style="font-size: 13px;">
                                    <asp:LinkButton ID="btnDetail_GvMD_AList"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetail_GvMD_AList"
                                        CommandArgument='<%# Eval("u0_training_course_idx") %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

            </div>

        </asp:View>

        <%-- End MD --%>


        <%--  OnDataBound="FvDetail_DataBound" --%>
        <asp:View ID="View_HR_WDetail" runat="server">
            <asp:FormView ID="fv_preview" runat="server" Width="100%">
                <EditItemTemplate>


                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; 
                                       
                                <asp:Label ID="lbtitle_preview" runat="server" Text="ข้อมูลแผนการฝึกอบรม"></asp:Label>
                            </strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <asp:Panel ID="Panel5" runat="server" Visible="false">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label20" class="col-md-2 control-labelnotop text_right" runat="server" Text="รหัสหลักสูตร :" />
                                                <div class="col-md-4">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>


                                <div class="form-group">

                                    <asp:Label ID="Label3" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่เอกสาร :" />
                                    <div class="col-md-4">
                                        <div class='input-group date'>
                                            <asp:Label ID="txttraining_course_date" runat="server"
                                                CssClass="f-s-13 control-label"
                                                Text='<%# Eval("zdate") %>' />
                                        </div>
                                    </div>


                                    <asp:Label ID="Label5" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="เลขที่เอกสาร : " />
                                    <div class="col-md-3">
                                        <asp:Label ID="txttraining_course_no" runat="server"
                                            CssClass="f-s-13 control-label"
                                            Text='<%# Eval("training_course_no") %>' />

                                    </div>

                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label14" class="col-md-2 control-labelnotop text_right" runat="server" Text="ประเภทหลักสูตร :" />
                                    <div class="col-md-4">
                                        <asp:Label ID="Label1" runat="server"
                                            CssClass="f-s-13 control-label"
                                            Text='<%# getValue_plan_status((string)Eval("course_plan_status")) %>' />

                                    </div>

                                    <div class="col-md-6">
                                    </div>

                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label12" class="col-md-2 control-labelnotop text_right" runat="server" Text="ชื่อหลักสูตร :" />
                                    <div class="col-md-9">
                                        <asp:Label ID="Label2" runat="server"
                                            CssClass="f-s-13 control-label"
                                            Text='<%# Eval("course_name") %>' />
                                    </div>

                                </div>




                                <div class="form-group">

                                    <asp:Label ID="Label15" class="col-md-2 control-labelnotop text_right" runat="server" Text="กลุ่มวิชา :" />
                                    <div class="col-md-4">
                                        <asp:Label ID="txttraining_group_name" runat="server"
                                            Text='<%# Eval("training_group_name") %>'
                                            CssClass="f-s-13 control-label">
                                        </asp:Label>
                                    </div>

                                    <asp:Label ID="Label25" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สาขาวิชา : " />
                                    <div class="col-md-3">
                                        <asp:Label ID="txttraining_branch_name" runat="server"
                                            Text='<%# Eval("training_branch_name") %>'
                                            CssClass="f-s-13 control-label">
                                        </asp:Label>
                                    </div>

                                </div>

                                <asp:Panel ID="pnltarget_group" runat="server">
                                    <div class="form-group">

                                        <asp:Label ID="Label11" class="col-md-2 control-labelnotop text_right" runat="server" Text="กลุ่มเป้าหมาย :" />
                                        <div class="col-md-4">
                                            <asp:Label ID="Label13" runat="server"
                                                Text='<%# Eval("target_group_name") %>'
                                                CssClass="f-s-13 control-label">
                                            </asp:Label>
                                        </div>

                                    </div>

                                </asp:Panel>

                                <div class="form-group">

                                    <asp:Label ID="Label26" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายละเอียดหลักสูตร :" />
                                    <div class="col-md-9">
                                        <asp:Label ID="txttraining_course_description" runat="server"
                                            Text='<%# Eval("training_course_description") %>'
                                            CssClass="f-s-13 control-label"
                                            TextMode="MultiLine"
                                            Rows="4">
                                        </asp:Label>
                                    </div>

                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label86" class="col-md-2 control-labelnotop text_right" runat="server" Text="คอร์สอบรม :" />
                                    <div class="col-md-9">
                                        <asp:Label ID="Label4" runat="server"
                                            Text='<%# getValue_course_type((int)Eval("training_course_type")) %>'
                                            CssClass="f-s-13 control-label">
                                        </asp:Label>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label87" class="col-md-2 control-labelnotop text_right" runat="server" Text="สถานที่อบรม :" />
                                    <div class="col-md-4">
                                        <asp:Label ID="Label6" runat="server"
                                            Text='<%# getValue_place((int)Eval("place_idx_ref")) %>'
                                            CssClass="f-s-13 control-label">
                                        </asp:Label>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label10" class="col-md-2 control-labelnotop text_right" runat="server" Text="วิทยากร :" />
                                    <div class="col-md-9">
                                        <asp:Label ID="Label7" runat="server"
                                            Text='<%# getValue_lecturer_type((int)Eval("lecturer_type")) %>'
                                            CssClass="f-s-13 control-label">
                                        </asp:Label>


                                    </div>

                                </div>


                                <div class="form-group">

                                    <asp:Label ID="Label28" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                    <div class="col-md-9">
                                        <div>

                                            <div class="panel-info">
                                                <div class="panel-heading f-bold">รายละเอียดวิทยากร</div>
                                            </div>
                                            <asp:GridView ID="Gvinstitution" runat="server"
                                                CssClass="table table-bordered word-wrap"
                                                GridLines="None" OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <%# (Container.DataItemIndex +1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField
                                                        HeaderText="ชื่อ - สกุล"
                                                        ItemStyle-HorizontalAlign="Left"
                                                        HeaderStyle-CssClass="text-center"
                                                        ItemStyle-Font-Size="Small">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtinstitution_name_edit_L" runat="server" Text='<%# Bind("zName") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtinstitution_name_item_L" runat="server" Text='<%# Eval("zName") %>'
                                                                Width="100%"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <asp:Label ID="Label96" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                    <div class="col-md-9">
                                        <div>

                                            <div class="panel-info">
                                                <div class="panel-heading f-bold">รายละเอียดวันที่อบรม</div>
                                            </div>
                                            <asp:GridView ID="Gvu8trncoursedate" runat="server"
                                                CssClass="table table-bordered word-wrap"
                                                GridLines="None" OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <%# (Container.DataItemIndex +1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField
                                                        HeaderText="วันที่อบรม"
                                                        ItemStyle-HorizontalAlign="Left"
                                                        HeaderStyle-CssClass="text-center"
                                                        ItemStyle-Font-Size="Small">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtzdate_edit_L" runat="server" Text='<%# Bind("zdate") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtzdate_item_L" runat="server" Text='<%# Eval("zdate") %>'
                                                                Width="100%"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField
                                                        HeaderText="เวลาที่เริ่ม-เวลาที่สิ้นสุด"
                                                        ItemStyle-HorizontalAlign="Left"
                                                        HeaderStyle-CssClass="text-center"
                                                        ItemStyle-Font-Size="Small">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtztime_edit_L" runat="server" Text='<%# Bind("ztime_start") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtztime_item_L" runat="server" Text='<%# Eval("ztime_start")+" - "+Eval("ztime_end") %>'
                                                                Width="100%"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField
                                                        HeaderText="รวมชม.ที่เรียน"
                                                        ItemStyle-HorizontalAlign="Right"
                                                        HeaderStyle-CssClass="text-center"
                                                        ItemStyle-Font-Size="Small">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txttraining_course_date_qty_edit_L" runat="server" Text='<%# Bind("training_course_date_qty") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="txttraining_course_date_qty_item_L" runat="server" Text='<%# string.Format("{0:n2}",float.Parse((string)Eval("training_course_date_qty"))) %>'
                                                                Width="100%"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                </div>


                                <%-- start วัตถุประสงค์ --%>

                                <div class="form-group">

                                    <asp:Label ID="Label40" class="col-md-2 control-labelnotop text_right" runat="server" Text="วัตถุประสงค์ :" />
                                    <div class="col-md-9">
                                        <div>

                                            <div class="panel-info">
                                                <div class="panel-heading f-bold">รายละเอียดวัตถุประสงค์</div>
                                            </div>
                                            <%-- table table-striped f-s-12 table-empshift-responsive--%>
                                            <asp:GridView ID="Gvobjective" runat="server"
                                                CssClass="table table-bordered word-wrap"
                                                GridLines="None" OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <%# (Container.DataItemIndex +1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField
                                                        HeaderText="วัตถุประสงค์"
                                                        ItemStyle-HorizontalAlign="Left"
                                                        HeaderStyle-CssClass="text-center"
                                                        ItemStyle-Font-Size="Small">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtobjective_name_edit_L" runat="server" Text='<%# Bind("zName") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbm0_objective_idx_ref" runat="server" Text='<%# Eval("m0_objective_idx_ref") %>'
                                                                Visible="false"></asp:Label>
                                                            <asp:Label ID="txtobjective_name_item_L" runat="server" Text='<%# Eval("zName") %>'
                                                                Width="100%"></asp:Label>
                                                            <hr />
                                                            <%-- start --%>

                                                            <asp:Label ID="Label78" class="col-md-12 control-labelnotop pull-left" runat="server"
                                                                Text="การวัด ประเมิน และติดตามผล" Font-Bold="true" />

                                                            <div class="form-group">

                                                                <asp:Label ID="Label79" class="col-md-1 control-labelnotop text_right" runat="server" Text="" />

                                                                <div class="col-md-5 form-inline">
                                                                    <asp:CheckBox ID="cbpass_test_flag" runat="server"
                                                                        CssClass="checkbox"
                                                                        Enabled="false"
                                                                        Checked='<%# getValue((string)Eval("pass_test_flag")) %>'
                                                                        Text='<%# Eval("pass_test_per").ToString() == "0.00" ? "ผ่านการทดสอบ - %" : "ผ่านการทดสอบ "+Eval("pass_test_per")+" % " %>' />

                                                                </div>


                                                                <div class="col-md-6 form-inline">
                                                                    <asp:CheckBox ID="cbhour_training_flag" runat="server"
                                                                        CssClass="checkbox"
                                                                        Enabled="false"
                                                                        Checked='<%# getValue((string)Eval("hour_training_flag")) %>'
                                                                        Text='<%# Eval("hour_training_per").ToString() == "0.00" ? "ชั่วโมงเข้าอบรมครบ - %" : "ชั่วโมงเข้าอบรมครบ "+Eval("hour_training_per")+" % " %>' />

                                                                </div>

                                                            </div>

                                                            <div class="form-group">

                                                                <asp:Label ID="Label82" class="col-md-1 control-labelnotop text_right" runat="server" Text="" />

                                                                <div class="col-md-5 form-inline">
                                                                    <asp:CheckBox ID="cbwrite_report_training_flag" runat="server"
                                                                        CssClass="checkbox"
                                                                        Enabled="false"
                                                                        Checked='<%# getValue((string)Eval("write_report_training_flag")) %>'
                                                                        Text="เขียนรายงานการอบรม" />
                                                                </div>


                                                                <div class="col-md-6 form-inline">
                                                                    <asp:CheckBox ID="cbpublish_training_flag" runat="server"
                                                                        CssClass="checkbox"
                                                                        Enabled="false"
                                                                        Checked='<%# getValue((string)Eval("publish_training_flag")) %>'
                                                                        Text='<%# "เผยแพร่ต่อผู้เกี่ยวข้องในรูปแบบ : " +Eval("publish_training_description") %>' />

                                                                </div>

                                                            </div>

                                                            <div class="form-group">

                                                                <asp:Label ID="Label83" class="col-md-1 control-labelnotop text_right" runat="server" Text="" />

                                                                <div class="col-md-5 form-inline">
                                                                    <asp:CheckBox ID="cbcourse_lecturer_flag" runat="server"
                                                                        CssClass="checkbox"
                                                                        Enabled="false"
                                                                        Checked='<%# getValue((string)Eval("course_lecturer_flag")) %>'
                                                                        Text="จัดทำหลักสูตรและเป็นวิทยากรภายใน" />
                                                                </div>


                                                                <div class="col-md-6 form-inline">
                                                                    <asp:CheckBox ID="cbother_flag" runat="server"
                                                                        CssClass="checkbox"
                                                                        Enabled="false"
                                                                        Checked='<%# getValue((string)Eval("other_flag")) %>'
                                                                        Text='<%# "อื่นๆ ระบุ : "+ Eval("other_description") %>' />

                                                                </div>

                                                            </div>
                                                            <asp:Label ID="Label88" class="col-md-12 control-labelnotop pull-left" runat="server"
                                                                Text="การติดตามผลโดย HRD :" />
                                                            <div class="form-group">

                                                                <asp:Label ID="Label84" class="col-md-1 control-labelnotop text_right" runat="server" Text="" />

                                                                <div class="col-md-5 form-inline">
                                                                    <asp:CheckBox ID="cbhrd_nofollow_flag" runat="server"
                                                                        CssClass="checkbox"
                                                                        Enabled="false"
                                                                        Checked='<%# getValue((string)Eval("hrd_nofollow_flag")) %>'
                                                                        Text="ไม่ติดตามผล" />
                                                                </div>


                                                                <div class="col-md-6 form-inline">
                                                                    <asp:CheckBox ID="cbhrd_follow_flag" runat="server"
                                                                        CssClass="checkbox"
                                                                        Enabled="false"
                                                                        Checked='<%# getValue((string)Eval("hrd_follow_flag")) %>'
                                                                        Text='<%# Eval("hrd_follow_day").ToString() == "0" ? "ติดตามภายใน - วัน" : "ติดตามภายใน "+Eval("hrd_follow_day")+" วัน " %>' />

                                                                </div>

                                                            </div>
                                                            <%-- end --%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                </div>


                                <%-- end วัตถุประสงค์ --%>

                                <div class="form-group">

                                    <asp:Label ID="Label41" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายละเอียดวัตถุประสงค์ :" />
                                    <div class="col-md-9">
                                        <asp:Label ID="txttraining_course_remark" runat="server"
                                            Text='<%# Eval("training_course_remark") %>'
                                            CssClass="f-s-13 control-label"
                                            TextMode="MultiLine"
                                            Rows="4">
                                        </asp:Label>
                                    </div>

                                </div>

                                <%-- start รายละเอียดผู้เข้าร่วมอบรม  : --%>

                                <hr />
                                <asp:Panel ID="Panel7" runat="server" Visible="false">
                                    <asp:Label ID="Label56" class="col-md-12 control-labelnotop pull-left h4" runat="server" Text="รายละเอียดผู้เข้าร่วมอบรม" />

                                    <div class="form-group">

                                        <asp:Label ID="Label44" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                        <div class="col-md-9">
                                            <div>

                                                <div class="panel-info">
                                                    <div class="panel-heading f-bold">รายละเอียดพนักงาน</div>
                                                </div>
                                                <asp:GridView ID="Gvemp" runat="server"
                                                    CssClass="table table-striped f-s-12 table-empshift-responsive word-wrap"
                                                    GridLines="None" OnRowCommand="onRowCommand"
                                                    AutoGenerateColumns="false">
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Width="5%">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <%# (Container.DataItemIndex +1) %>
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField
                                                            HeaderText="ชื่อ - สกุล"
                                                            ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-CssClass="text-center"
                                                            ItemStyle-Font-Size="Small">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtemp_zName_edit_L" runat="server" Text='<%# Bind("zName") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="txtemp_zName_item_L" runat="server" Text='<%# Eval("zName") %>'
                                                                    Width="100%"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField
                                                            HeaderText="ตำแหน่ง"
                                                            ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-CssClass="text-center"
                                                            ItemStyle-Font-Size="Small">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtemp_zPostName_edit_L" runat="server" Text='<%# Bind("zPostName") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="txtemp_zPostName_item_L" runat="server" Text='<%# Eval("zPostName") %>'
                                                                    Width="100%"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField
                                                            HeaderText="Cost Center"
                                                            ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-CssClass="text-center"
                                                            ItemStyle-Font-Size="Small">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtemp_zCostCenter_edit_L" runat="server" Text='<%# Bind("zCostCenter") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="txtemp_zCostCenter_item_L" runat="server" Text='<%# Eval("zCostCenter") %>'
                                                                    Width="100%"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField
                                                            HeaderText="เบอร์ติดต่อ"
                                                            ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-CssClass="text-center"
                                                            ItemStyle-Font-Size="Small">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtemp_zTel_edit_L" runat="server" Text='<%# Bind("zTel") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="txtemp_zTel_item_L" runat="server" Text='<%# Eval("zTel") %>'
                                                                    Width="100%"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>

                                    </div>
                                    <hr />
                                </asp:Panel>
                                <%-- end รายละเอียดผู้เข้าร่วมอบรม  : --%>

                                <div class="form-group">

                                    <asp:Label ID="Label21" class="col-md-2 control-labelnotop text_right" runat="server" Text="จำนวนผู้เข้าร่วมอบรม  :" />
                                    <div class="col-md-3">

                                        <asp:Label ID="Label24" runat="server"
                                            CssClass="f-s-13 control-label"
                                            Text='<%# getformatfloat(((int)Eval("zcount_emp")).ToString(),0)+"    คน" %>' />

                                    </div>

                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label45" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายละเอียดแผน และงบประมาณ  :" />
                                    <div class="col-md-9">

                                        <asp:Label ID="Label16" runat="server"
                                            CssClass="f-s-13 control-label"
                                            Text='<%# getValue_planbudget_type((int)Eval("training_course_planbudget_type")) %>' />

                                    </div>

                                </div>

                                <hr />

                                <asp:Label ID="Label42" class="col-md-12 control-labelnotop pull-left h4" runat="server" Text="ค่าใช้จ่ายฝึกอบรม" />

                                <div class="form-group">

                                    <asp:Label ID="Label55" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                    <div class="col-md-9">
                                        <div>

                                            <div class="panel-info">
                                                <div class="panel-heading f-bold">รายละเอียดค่าใช้จ่ายฝึกอบรม</div>
                                            </div>
                                            <asp:GridView ID="Gvtrn_expenses" runat="server"
                                                CssClass="table table-bordered word-wrap"
                                                GridLines="None" OnRowCommand="onRowCommand"
                                                AutoGenerateColumns="false">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                        HeaderStyle-Width="5%">
                                                        <ItemTemplate>

                                                            <%# (Container.DataItemIndex +1) %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField
                                                        HeaderText="รายการ"
                                                        ItemStyle-HorizontalAlign="Left"
                                                        HeaderStyle-CssClass="text-center"
                                                        ItemStyle-Font-Size="Small">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtemp_expenses_description_edit_L" runat="server" Text='<%# Bind("expenses_description") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtemp_expenses_description_item_L" runat="server" Text='<%# Eval("expenses_description") %>'
                                                                Width="100%"></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField
                                                        HeaderText="มูลค่า"
                                                        ItemStyle-HorizontalAlign="Right"
                                                        HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Width="15%"
                                                        ItemStyle-Font-Size="Small">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtemp_amount_edit_L" runat="server" Text='<%# Bind("amount") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtemp_amount_item_L" runat="server" Text='<%# getStrformate((string)Eval("amount"),2) %>'
                                                                Width="100%"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField
                                                        HeaderText="Vat"
                                                        ItemStyle-HorizontalAlign="Right"
                                                        HeaderStyle-CssClass="text-center"
                                                        ItemStyle-Font-Size="Small"
                                                        HeaderStyle-Width="10%">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtemp_vat_edit_L" runat="server" Text='<%# Bind("vat") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtemp_vat_item_L" runat="server" Text='<%# getStrformate((string)Eval("vat"),2) %>'
                                                                Width="100%"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField
                                                        HeaderText="หัก ณ ที่จ่าย"
                                                        ItemStyle-HorizontalAlign="Right"
                                                        HeaderStyle-CssClass="text-center"
                                                        ItemStyle-Font-Size="Small"
                                                        HeaderStyle-Width="15%">
                                                        <EditItemTemplate>
                                                            <asp:TextBox ID="txtemp_withholding_tax_edit_L" runat="server" Text='<%# Bind("withholding_tax") %>'></asp:TextBox>
                                                        </EditItemTemplate>
                                                        <ItemTemplate>
                                                            <asp:Label ID="txtemp_withholding_tax_item_L" runat="server" Text='<%# getStrformate((string)Eval("withholding_tax"),2) %>'
                                                                Width="100%"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label46" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                    <div class="col-md-9">

                                        <div class="panel panel-default">
                                            <div class="panel-body">

                                                <div class="form-group">

                                                    <asp:Label ID="Label57" class="col-md-2 control-labelnotop text_right" runat="server" Text="รวม :" />
                                                    <div class="col-md-2">
                                                        <asp:Label ID="txttraining_course_total" runat="server"
                                                            Text='<%# getformatfloat(((decimal)Eval("training_course_total")).ToString(),2) %>'
                                                            CssClass="f-s-13 control-label">
                                                        </asp:Label>
                                                    </div>
                                                    <asp:Label ID="Label60" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="บาท" />

                                                    <asp:Label ID="Label58" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="เฉลี่ย :" />
                                                    <div class="col-md-2">
                                                        <asp:Label ID="txttraining_course_total_avg" runat="server"
                                                            Text='<%# getformatfloat(((decimal)Eval("training_course_total_avg")).ToString(),2) %>'
                                                            CssClass="f-s-13 control-label">
                                                        </asp:Label>

                                                    </div>
                                                    <asp:Label ID="Label59" CssClass="col-md-2 control-labelnotop textleft" runat="server" Text="บาท/ท่าน" />
                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label61" class="col-md-2 control-labelnotop text_right" runat="server" Text="ลดหย่อนภาษี :" />
                                                    <div class="col-md-2">
                                                        <asp:Label ID="txttraining_course_reduce_tax" runat="server"
                                                            Text='<%# getformatfloat(((decimal)Eval("training_course_reduce_tax")).ToString(),2) %>'
                                                            CssClass="f-s-13 control-label">
                                                        </asp:Label>
                                                    </div>
                                                    <asp:Label ID="Label62" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="%" />

                                                    <div class="col-md-2">
                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label63" class="col-md-2 control-labelnotop text_right" runat="server" Text="ค่าใช้จ่ายสุทธิ :" />
                                                    <div class="col-md-2">
                                                        <asp:Label ID="txttraining_course_net_charge" runat="server"
                                                            Text='<%# getformatfloat(((decimal)Eval("training_course_net_charge")).ToString(),2) %>'
                                                            CssClass="f-s-13 control-label">
                                                        </asp:Label>
                                                    </div>
                                                    <asp:Label ID="Label64" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="บาท" />

                                                    <asp:Label ID="Label65" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="เฉลี่ย :" />
                                                    <div class="col-md-2">
                                                        <asp:Label ID="txttraining_course_net_charge_tax" runat="server"
                                                            Text='<%# getformatfloat(((decimal)Eval("training_course_net_charge_tax")).ToString(),2) %>'
                                                            CssClass="f-s-13 control-label">
                                                        </asp:Label>

                                                    </div>
                                                    <asp:Label ID="Label66" CssClass="col-md-2 control-labelnotop textleft" runat="server" Text="บาท/ท่าน" />
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label67" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                    <div class="col-md-9">

                                        <div class="panel panel-default">
                                            <div class="panel-body">

                                                <div class="form-group">

                                                    <asp:Label ID="Label68" class="col-md-3 control-labelnotop text_right" runat="server" Text="งบประมาณลง Cost Center :" />
                                                    <div class="col-md-3">
                                                        <asp:Label ID="Label18" runat="server"
                                                            Text='<%# Eval("CostNo") %>'
                                                            CssClass="f-s-13 control-label">
                                                        </asp:Label>
                                                    </div>

                                                    <asp:Label ID="Label70" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="หน่วยงาน :" />
                                                    <div class="col-md-4">
                                                        <asp:Label ID="Label19" runat="server"
                                                            Text='<%# Eval("dept_name_th") %>'
                                                            CssClass="f-s-13 control-label">
                                                        </asp:Label>

                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label72" class="col-md-3 control-labelnotop text_right" runat="server" Text="แผนงบประมาณที่วางไว้ รวม :" />
                                                    <div class="col-md-3">
                                                        <asp:Label ID="txttraining_course_planbudget_total" runat="server"
                                                            Text='<%# getformatfloat(((decimal)Eval("training_course_planbudget_total")).ToString(),2) %>'
                                                            CssClass="f-s-13 control-label">
                                                        </asp:Label>

                                                    </div>
                                                    <asp:Label ID="Label73" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="บาท" />

                                                    <div class="col-md-2">
                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label69" class="col-md-3 control-labelnotop text_right" runat="server" Text="งบประมาณที่ใช้รวมครั้งนี้ :" />
                                                    <div class="col-md-3">
                                                        <asp:Label ID="txttraining_course_budget_total" runat="server"
                                                            Text='<%#   getformatfloat(((decimal)Eval("training_course_budget_total")).ToString(),2) %>'
                                                            CssClass="f-s-13 control-label">
                                                        </asp:Label>
                                                    </div>
                                                    <asp:Label ID="Label71" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="บาท" />

                                                    <div class="col-md-2">
                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label74" class="col-md-3 control-labelnotop text_right" runat="server" Text="งบประมาณคงเหลือ :" />
                                                    <div class="col-md-3">
                                                        <asp:Label ID="txttraining_course_budget_balance" runat="server"
                                                            Text='<%# getformatfloat(((decimal)Eval("training_course_budget_balance")).ToString(),2) %>'
                                                            CssClass="f-s-13 control-label">
                                                        </asp:Label>
                                                    </div>
                                                    <asp:Label ID="Label75" CssClass="col-md-1 control-labelnotop textleft" runat="server" Text="บาท" />

                                                    <div class="col-md-2">
                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label76" class="col-md-3 control-labelnotop text_right" runat="server" Text="" />
                                                    <div class="col-md-2">
                                                        <asp:Label ID="txttraining_course_budget_total_per" runat="server"
                                                            Text='<%# getformatfloat(((decimal)Eval("training_course_budget_total_per")).ToString(),2) %>'
                                                            CssClass="f-s-13 control-label">
                                                        </asp:Label>
                                                    </div>
                                                    <asp:Label ID="Label77" CssClass="col-md-7 control-labelnotop textleft-red" runat="server" Text="%(ค่าใช้จ่ายที่ลงทั้งหมดเป็นค่าใช้จ่ายก่อน Vat เสมอ )" />



                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <hr />

                                <div class="form-group">

                                    <asp:Label ID="Label17" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="สถานะของเอกสาร :" />
                                    <div class="col-md-10">
                                        <asp:Label ID="Label37"
                                            runat="server"
                                            CssClass="f-s-13 control-label"
                                            Text='<%# getTextAction((int)Eval("zacter_status"),(string)Eval("zdecision")) %>' />
                                    </div>

                                </div>
                                <%--<%# getTextDoc((int)Eval("approve_status") , 
                                                       (int)Eval("md_approve_status") , 
                                                       (string)Eval("decision_name"),
                                                       (string)Eval("node_name"),
                                                       (string)Eval("actor_name"),
                                                       (string)Eval("md_decision_name"),
                                                       (string)Eval("md_node_name"),
                                                       (string)Eval("md_actor_name")
                                                       )
                                                        %>--%>
                                <%-- End  --%>
                            </div>
                        </div>
                    </div>

                    <asp:Panel ID="pnlDetailApp" runat="server">

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h2 class="panel-title">รายละเอียดการอนุมัติ
                                </h2>
                            </div>
                            <div class="panel-body">

                                <asp:Panel ID="pnlapprove_hr" runat="server">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="form-group col-lg-12">
                                                <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ผลอนุมัติ" />
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlStatusapprove" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Text="ผลการอนุมัติ....." />
                                                        <asp:ListItem Value="4" Text="อนุมัติ" />
                                                        <asp:ListItem Value="5" Text="กลับไปแก้ไข" />
                                                        <asp:ListItem Value="6" Text="ไม่อนุมัติ" />
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredddlStatusapprove"
                                                        ValidationGroup="btnupdateApprove_HR" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlStatusapprove"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกผลอนุมัติ" />
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-12">
                                                <asp:Label ID="Label35" CssClass="col-sm-3 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม" />
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtdetailApprove" CssClass="form-control" runat="server"
                                                        placeholder="ความคิดเห็นเพิ่มเติม ....." MaxLength="150" TextMode="multiline" Rows="4"></asp:TextBox>
                                                </div>
                                            </div>

                                            <asp:Panel ID="pnlApprove_HR_W" runat="server">
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3 col-sm-offset-9">
                                                        <asp:LinkButton ID="btnupdateApprove_HR" CssClass="btn btn-success" runat="server"
                                                            ValidationGroup="btnupdateApprove_HR"
                                                            OnCommand="btnCommand" CommandName="btnupdateApprove_HR"
                                                            CommandArgument='<%# Eval("u0_training_course_idx") %>'
                                                            data-toggle="tooltip" title="Save"
                                                            OnClientClick="return confirm('คุณต้องการให้ผลรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btncancelApprove_HR"
                                                            CssClass="btn btn-default" runat="server" Text="Cancel"
                                                            OnCommand="btnCommand" CommandName="btnHR_Wait"
                                                            data-toggle="tooltip"
                                                            Visible="false"
                                                            title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="pnlApprove_HR_A" runat="server">
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3 col-sm-offset-9">
                                                        <asp:LinkButton ID="btnupdateApprove_HR_A" CssClass="btn btn-success" runat="server"
                                                            ValidationGroup="btnupdateApprove_HR_A"
                                                            OnCommand="btnCommand" CommandName="btnupdateApprove_HR_A"
                                                            CommandArgument='<%# Eval("u0_training_course_idx") %>'
                                                            data-toggle="tooltip" title="Save"
                                                            OnClientClick="return confirm('คุณต้องการให้ผลรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btncancelApprove_HR_A"
                                                            CssClass="btn btn-default" runat="server" Text="Cancel"
                                                            OnCommand="btnCommand" CommandName="btnHR_App"
                                                            data-toggle="tooltip"
                                                            Visible="false"
                                                            title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>


                                        </div>
                                    </div>

                                </asp:Panel>

                                <asp:Panel ID="pnlapprove_md" runat="server">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="form-group col-lg-12">
                                                <asp:Label ID="Label38" CssClass="col-sm-3 control-label" runat="server" Text="ผลอนุมัติ" />
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlStatusapprove_md" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Text="ผลการอนุมัติ....." />
                                                        <asp:ListItem Value="4" Text="อนุมัติ" />
                                                        <asp:ListItem Value="5" Text="กลับไปแก้ไข" />
                                                        <asp:ListItem Value="6" Text="ไม่อนุมัติ" />
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredddlStatusapprove_md"
                                                        ValidationGroup="btnupdateApprove_md" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlStatusapprove_md"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกผลอนุมัติ" />
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-12">
                                                <asp:Label ID="Label39" CssClass="col-sm-3 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม" />
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtdetailApprove_md" CssClass="form-control" runat="server"
                                                        placeholder="ความคิดเห็นเพิ่มเติม ....." MaxLength="150" TextMode="multiline" Rows="4"></asp:TextBox>
                                                </div>
                                            </div>

                                            <asp:Panel ID="pnlApprove_MD_W" runat="server">
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3 col-sm-offset-9">
                                                        <asp:LinkButton ID="btnupdateApprove_MD" CssClass="btn btn-success" runat="server"
                                                            ValidationGroup="btnupdateApprove_md"
                                                            OnCommand="btnCommand" CommandName="btnupdateApprove_MD"
                                                            CommandArgument='<%# Eval("u0_training_course_idx") %>'
                                                            data-toggle="tooltip" title="Save"
                                                            OnClientClick="return confirm('คุณต้องการให้ผลรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btncancelApprove_MD"
                                                            CssClass="btn btn-default" runat="server" Text="Cancel"
                                                            OnCommand="btnCommand" CommandName="btnMD_Wait"
                                                            data-toggle="tooltip"
                                                            Visible="false"
                                                            title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="pnlApprove_MD_A" runat="server">
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3 col-sm-offset-9">
                                                        <asp:LinkButton ID="btnupdateApprove_MD_A" CssClass="btn btn-success" runat="server"
                                                            ValidationGroup="btnupdateApprove_md"
                                                            OnCommand="btnCommand" CommandName="btnupdateApprove_MD_A"
                                                            CommandArgument='<%# Eval("u0_training_course_idx") %>'
                                                            data-toggle="tooltip" title="Save"
                                                            OnClientClick="return confirm('คุณต้องการให้ผลรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btncancelApprove_MD_A"
                                                            CssClass="btn btn-default" runat="server" Text="Cancel"
                                                            OnCommand="btnCommand" CommandName="btnMD_App"
                                                            data-toggle="tooltip"
                                                            Visible="false"
                                                            title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="pnlapprove_leader" runat="server">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="form-group col-lg-12">
                                                <asp:Label ID="Label94" CssClass="col-sm-3 control-label" runat="server" Text="ผลอนุมัติ" />
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlStatusapprove_leader" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="" Text="ผลการอนุมัติ....." />
                                                        <asp:ListItem Value="4" Text="อนุมัติ" />
                                                        <asp:ListItem Value="5" Text="กลับไปแก้ไข" />
                                                        <asp:ListItem Value="6" Text="ไม่อนุมัติ" />
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                        ValidationGroup="btnupdateApprove_leader" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlStatusapprove_leader"
                                                        Font-Size="1em" ForeColor="Red"
                                                        CssClass="pull-left"
                                                        ErrorMessage="กรุณากรอกผลอนุมัติ" />
                                                </div>
                                            </div>

                                            <div class="form-group col-lg-12">
                                                <asp:Label ID="Label95" CssClass="col-sm-3 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม" />
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txtdetailApprove_leader" CssClass="form-control" runat="server"
                                                        placeholder="ความคิดเห็นเพิ่มเติม ....." MaxLength="150" TextMode="multiline" Rows="4"></asp:TextBox>
                                                </div>
                                            </div>

                                            <asp:Panel ID="pnlApprove_leader_W" runat="server">
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3 col-sm-offset-9">
                                                        <asp:LinkButton ID="btnupdateApprove_leader" CssClass="btn btn-success" runat="server"
                                                            ValidationGroup="btnupdateApprove_leader"
                                                            OnCommand="btnCommand" CommandName="btnupdateApprove_leader"
                                                            CommandArgument='<%# Eval("u0_training_course_idx") %>'
                                                            data-toggle="tooltip" title="Save"
                                                            OnClientClick="return confirm('คุณต้องการให้ผลรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btncancelApprove_leader"
                                                            CssClass="btn btn-default" runat="server" Text="Cancel"
                                                            OnCommand="btnCommand" CommandName="btnleader_Wait"
                                                            data-toggle="tooltip"
                                                            Visible="false"
                                                            title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="pnlApprove_leader_A" runat="server">
                                                <div class="form-group col-lg-12">
                                                    <div class="col-sm-3 col-sm-offset-9">
                                                        <asp:LinkButton ID="btnupdateApprove_leader_A" CssClass="btn btn-success" runat="server"
                                                            ValidationGroup="btnupdateApprove_leader"
                                                            OnCommand="btnCommand" CommandName="btnupdateApprove_leader_A"
                                                            CommandArgument='<%# Eval("u0_training_course_idx") %>'
                                                            data-toggle="tooltip" title="Save"
                                                            OnClientClick="return confirm('คุณต้องการให้ผลรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="btncancelApprove_leader_A"
                                                            CssClass="btn btn-default" runat="server" Text="Cancel"
                                                            OnCommand="btnCommand" CommandName="btnleader_App"
                                                            data-toggle="tooltip"
                                                            Visible="false"
                                                            title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>
                                        </div>
                                    </div>
                                </asp:Panel>


                            </div>
                        </div>
                    </asp:Panel>
                </EditItemTemplate>


            </asp:FormView>

            <div class="row">
                <div class="form-group">
                    <div class="col-md-12">

                        <div class="col-md-2">
                        </div>
                        <div class="col-md-10">

                            <asp:LinkButton CssClass="btn btn-danger"
                                data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                ID="btncancelApp_HR_W"
                                CommandName="btnHR_Wait" OnCommand="btnCommand" />

                            <asp:LinkButton CssClass="btn btn-danger"
                                data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                ID="btncancelApp_HR"
                                CommandName="btnHR_App" OnCommand="btnCommand" />

                            <asp:LinkButton CssClass="btn btn-danger"
                                data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                ID="btncancelApp_MD_W"
                                CommandName="btnMD_Wait" OnCommand="btnCommand" />

                            <asp:LinkButton CssClass="btn btn-danger"
                                data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                ID="btncancelApp_MD"
                                CommandName="btnMD_App" OnCommand="btnCommand" />

                            <asp:LinkButton CssClass="btn btn-danger"
                                data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                ID="btncancelApp_LEADER_W"
                                CommandName="btnLeaderList" OnCommand="btnCommand" />

                            <asp:LinkButton CssClass="btn btn-danger"
                                data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                ID="btncancelApp_LEADER"
                                CommandName="btnLeaderApproveList" OnCommand="btnCommand" />

                        </div>

                    </div>
                </div>
            </div>
            <br />

        </asp:View>

        <asp:View ID="View_SCHEDList" runat="server">

            <asp:Panel ID="Panel11" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_SCHED" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_SCHED" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>กลุ่มวิชา</label>
                                <asp:DropDownList ID="ddltrn_groupSearch_SCHED" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                           
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_SCHED" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_SCHED" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="col-md-11 col-md-offset-1">
                <asp:Panel ID="panel13" runat="server" CssClass="text-right m-b-10 hidden-print">
                    <%--<button
                        onclick="tableToExcel('Tableprint_sched', 'ตารางแผนการอบรม')"
                        class="btn btn-primary">
                        <i class='fa fa-file-excel'></i>
                        Export Excel
                    </button>--%>

                    <%--<button id="btnExport" onclick="fnExcelReport();"> EXPORT </button>--%>

                    <asp:LinkButton ID="btnprint_sched" CssClass="btn btn-primary" runat="server"
                        Text="<i class='fa fa-print'></i> พิมพ์" OnClientClick="return printDiv('print_sched');" />
                </asp:Panel>
            </div>
            <div id="print_sched" class="row">

                <table id="Tableprint_sched"
                    class="table table-bordered">
                    <thead>
                        <tr style="background-color: #e7e7e7; padding: 5px 0px;">

                            <% for (int iWeek = 1; iWeek <= 12; iWeek++)
                                {
                            %>
                            <th style="width: 80px;">
                                <center>
                                <%= MonthTH(iWeek) %>
                                    </center>
                            </th>
                            <% 
                                } %>
                        </tr>
                    </thead>
                    <tbody>

                        <%= getHtmlSched() %>
                    </tbody>

                </table>


            </div>

            <br />

        </asp:View>

        <asp:View ID="View_summaryList" runat="server">

            <asp:Panel ID="Panel14" runat="server" Visible="true">
                <div class="panel panel-primary m-t-10">
                    <div class="panel-heading f-bold">ค้นหา</div>
                    <div class="panel-body">

                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ปี</label>
                                <asp:DropDownList ID="ddlYearSearch_summary" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                <asp:TextBox ID="txtFilterKeyword_summary" runat="server"
                                    CssClass="form-control"
                                    placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div class="form-group">
                                <label>ผลการฝึกอบรม</label>
                                <asp:DropDownList ID="ddlSummary" runat="server"
                                    CssClass="form-control">
                                    <asp:ListItem Selected="True" Value="1">ยังไม่ให้ผล</asp:ListItem>
                                    <asp:ListItem Value="2">ให้ผลแล้ว</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <label>
                                &nbsp;
                           
                            </label>
                            <div class="form-group">
                                <asp:LinkButton ID="btnS_summary" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                    OnCommand="btnCommand" CommandName="btnS_summary" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>

            <div class="row">

                <asp:GridView ID="GvsummaryList"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowCommand="onRowCommand"
                    OnRowDataBound="onRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    DataKeyNames="u6_training_course_idx"
                    ShowFooter="false">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>


                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# (Container.DataItemIndex + 1) %>

                                    <asp:Label ID="lbu0_training_course_idx" runat="server"
                                        Visible="false" Text='<%# Eval("u0_training_course_idx") %>' />

                                    <asp:Label ID="lbu0_course_idx" runat="server"
                                        Visible="false" Text='<%# Eval("u0_course_idx") %>' />

                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("training_course_no") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("zdate") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="หลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("course_name") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่อบรม" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="25%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <table class="table table-striped f-s-12 table-empshift-responsive">
                                        <asp:Repeater ID="rptu8trncoursedate" runat="server">
                                            <HeaderTemplate>
                                                <tr>
                                                    <th>วันที่อบรม</th>
                                                    <th>เวลาที่เริ่ม-เวลาที่สิ้นสุด</th>
                                                    <th>รวมชม.ที่เรียน</th>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("zdate") %></td>
                                                    <td><%# Eval("ztime_start")+" - "+Eval("ztime_end") %></td>
                                                    <td align="center"><%# string.Format("{0:n2}",Eval("training_course_date_qty")) %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ผู้เข้าอบรม" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <%# Eval("EmpCode")+" - "+Eval("FullNameTH") %>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                            <ItemTemplate>
                                <div class="word-wrap">
                                    <asp:TextBox ID="txtGvtraining_course_no" runat="server"
                                        Visible="false" Text='<%# Eval("training_course_no") %>' />
                                    <asp:TextBox ID="txtGvemp_idx_ref" runat="server"
                                        Visible="false" Text='<%# Eval("emp_idx_ref") %>' />

                                    <asp:HyperLink runat="server" ID="btnDownloadFile"
                                        CssClass="btn btn-default btn-sm" data-original-title="Download"
                                        data-toggle="tooltip" Text="Download"
                                        Target="_blank"
                                        CommandArgument='<%# Eval("training_course_no") %>'><i class="fa fa-download"></i>

                                    </asp:HyperLink>

                                    <asp:LinkButton ID="btnDetail_GvsummaryList"
                                        CssClass="btn btn-success btn-sm" runat="server"
                                        data-original-title="ผลการฝึกอบรม" data-toggle="tooltip" Text="ผลการฝึกอบรม"
                                        OnCommand="btnCommand" CommandName="btnDetail_GvsummaryList"
                                        CommandArgument='<%#
                                        Convert.ToString(Eval("u0_training_course_idx_ref")) + "|" +  
                                        Convert.ToString(Eval("u6_training_course_idx"))
                                        %>'>
                                    <i class="fa fa-file-alt"></i></asp:LinkButton>
                                    <%--  --%>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>
                </asp:GridView>

            </div>

        </asp:View>


        <asp:View ID="View_summarycourse" runat="server">

            <asp:FormView ID="fv_summarycourse" runat="server" DefaultMode="Edit" Width="100%">
                <EditItemTemplate>


                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-file"></i><strong>&nbsp; 
                                       
                                <asp:Label ID="lbtitle_preview" runat="server" Text="รายละเอียดหลักสูตร"></asp:Label>
                            </strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <asp:Panel ID="Panel5" runat="server" Visible="false">
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label20" class="col-md-2 control-labelnotop text_right" runat="server" Text="รหัสหลักสูตร :" />
                                                <div class="col-md-4">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <div class="form-group" runat="server" visible="true">
                                    <asp:Label ID="Label123" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="เลขที่เอกสาร : " />
                                    <div class="col-md-10">
                                        <asp:TextBox ID="txtu3_training_course_idx" runat="server"
                                            Visible="false"
                                            Text='<%# Eval("u3_training_course_idx") %>'>
                                        </asp:TextBox>
                                        <asp:TextBox ID="txtu0_training_course_idx_ref" runat="server"
                                            Visible="false"
                                            Text='<%# Eval("u0_training_course_idx_ref") %>'>
                                        </asp:TextBox>
                                        <asp:Label ID="txttraining_course_no" runat="server"
                                            CssClass="f-s-13 control-label"
                                            Text='<%# Eval("training_course_no") %>' />

                                    </div>
                                </div>

                                <div class="form-group" runat="server" visible="true">

                                    <asp:Label ID="Label3" CssClass="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่เอกสาร :" />
                                    <div class="col-md-10">
                                        <div class='input-group date'>
                                            <asp:Label ID="txttraining_course_date" runat="server"
                                                CssClass="f-s-13 control-label"
                                                Text='<%# Eval("zdate") %>' />
                                        </div>
                                    </div>

                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label29" class="col-md-2 control-labelnotop text_right" runat="server" Text="วันที่เริ่มอบรม :" />
                                    <div class="col-sm-10">
                                        <asp:Label ID="Label8" runat="server"
                                            CssClass="f-s-13 control-label"
                                            Text='<%# Eval("zdate_start")+"   ถึง   "+Eval("zdate_end")
                                                      +"   เวลา :   "+Eval("ztime_start")+"   ถึง   "+Eval("ztime_end")
                                                    
                                                      %>' />


                                    </div>

                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label12" class="col-md-2 control-labelnotop text_right" runat="server" Text="ชื่อหลักสูตร :" />
                                    <div class="col-md-9">
                                        <asp:Label ID="Label2" runat="server"
                                            CssClass="f-s-13 control-label"
                                            Text='<%# Eval("course_name") %>' />
                                    </div>

                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label87" class="col-md-2 control-labelnotop text_right" runat="server" Text="สถานที่อบรม :" />
                                    <div class="col-md-4">
                                        <asp:Label ID="Label6" runat="server"
                                            Text='<%# getValue_place((int)Eval("place_idx_ref")) %>'
                                            CssClass="f-s-13 control-label">
                                        </asp:Label>

                                    </div>

                                </div>

                                <div class="form-group">

                                    <asp:Label ID="Label10" class="col-md-2 control-labelnotop text_right" runat="server" Text="วิทยากร :" />
                                    <div class="col-md-9">
                                        <asp:Label ID="Label7" runat="server"
                                            Text='<%# getValue_lecturer_type((int)Eval("lecturer_type")) %>'
                                            CssClass="f-s-13 control-label">
                                        </asp:Label>


                                    </div>

                                </div>

                                <asp:Panel ID="Panel2" runat="server" Visible="false">
                                    <div class="form-group">

                                        <asp:Label ID="Label28" class="col-md-2 control-labelnotop text_right" runat="server" Text=" " />
                                        <div class="col-md-9">
                                            <div>

                                                <div class="panel-info">
                                                    <div class="panel-heading f-bold">รายละเอียดวิทยากร</div>
                                                </div>
                                                <asp:GridView ID="Gvinstitution" runat="server"
                                                    CssClass="table table-bordered word-wrap"
                                                    GridLines="None" OnRowCommand="onRowCommand"
                                                    AutoGenerateColumns="false">
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">ไม่พบข้อมูล</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                            HeaderStyle-Width="5%">
                                                            <ItemTemplate>
                                                                <%# (Container.DataItemIndex +1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField
                                                            HeaderText="ชื่อ - สกุล"
                                                            ItemStyle-HorizontalAlign="Left"
                                                            HeaderStyle-CssClass="text-center"
                                                            ItemStyle-Font-Size="Small">
                                                            <EditItemTemplate>
                                                                <asp:TextBox ID="txtinstitution_name_edit_L" runat="server" Text='<%# Bind("zName") %>'></asp:TextBox>
                                                            </EditItemTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="txtinstitution_name_item_L" runat="server" Text='<%# Eval("zName") %>'
                                                                    Width="100%"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>

                                    </div>
                                </asp:Panel>

                            </div>
                        </div>
                    </div>

                </EditItemTemplate>
            </asp:FormView>

            <asp:FormView ID="fv_summaryLearner" runat="server" DefaultMode="Edit" Width="100%">
                <EditItemTemplate>
                    <asp:TextBox ID="txtu6_training_course_idx" runat="server"
                        Visible="false" Text='<%# Eval("u6_training_course_idx") %>' />
                    <asp:TextBox ID="txtu3_training_course_idx" runat="server"
                        Visible="false" Text='<%# Eval("u3_training_course_idx_ref") %>' />
                    <asp:TextBox ID="txtemp_idx_ref" runat="server"
                        Visible="false" Text='<%# Eval("emp_idx_ref") %>' />
                    <asp:TextBox ID="txtu0_training_course_idx_ref" runat="server"
                        Visible="false"
                        Text='<%# Eval("u0_training_course_idx_ref") %>'>
                    </asp:TextBox>
                    <asp:TextBox ID="txtsumm_no" runat="server"
                        Visible="false"
                        Text='<%# Eval("summ_no") %>'>
                    </asp:TextBox>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-file"></i><strong>&nbsp; 
                                       
                                <asp:Label ID="lbtitle_preview" runat="server" Text="ผลการฝึกอบรม"></asp:Label>
                            </strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <asp:Label ID="Label29" class="col-md-2 control-labelnotop text_right" runat="server" Text="รายงานสรุปเนื้อหาการฝึกอบรม :" />
                                    <div class="col-sm-9">
                                        <asp:Literal ID="littraining_summary_report_remark" runat="server"
                                            Text='<%# Eval("training_summary_report_remark") %>'
                                            Visible="true">
                                        </asp:Literal>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label124" class="col-md-2 control-labelnotop text_right" runat="server" Text="ประโยชน์ที่ได้รับจากการฝึกอบรม :" />
                                    <div class="col-sm-9">
                                        <asp:Literal ID="littraining_benefits_remark" runat="server"
                                            Text='<%# Eval("training_benefits_remark") %>'
                                            Visible="true">
                                        </asp:Literal>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label89" class="col-md-2 control-labelnotop text_right" runat="server" Text="ความเห็นเพิ่มเติมของผู้บังคับบัญชาตามสายงาน :" />
                                    <div class="col-sm-9">
                                        <asp:Literal ID="litsupervisors_additional" runat="server"
                                            Text='<%# Eval("supervisors_additional") %>'
                                            Visible="true">
                                        </asp:Literal>
                                    </div>
                                </div>

                                <asp:Panel ID="pnl_super" runat="server">
                                    <hr />

                                    <div class="form-group">
                                        <asp:Label ID="Label125" class="col-md-2 control-labelnotop text_right" runat="server" Text="ความเห็นเพิ่มเติมของฝ่ายทรัพยาการบุคคล :" />
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="txtsumm_hr_remark" runat="server"
                                                CssClass="f-s-13 form-control"
                                                Rows="10"
                                                TextMode="MultiLine"
                                                Text='<%# Eval("summ_hr_remark") %>' />
                                        </div>
                                    </div>
                                </asp:Panel>

                            </div>
                        </div>
                    </div>

                </EditItemTemplate>
            </asp:FormView>

            <div class="row">
                <div class="form-group">
                    <div class="col-md-12">

                        <div class="col-md-2">
                        </div>
                        <div class="col-md-10">
                            <asp:LinkButton CssClass="btn btn-success" runat="server"
                                CommandName="btnSave_summary" OnCommand="btnCommand"
                                data-toggle="tooltip" title="บันทึก"
                                ID="btnSave_summary"
                                ValidationGroup="btnSaveInsert">
                                   <i class="fa fa-save fa-lg"></i> บันทึก
                            </asp:LinkButton>
                            <asp:LinkButton CssClass="btn btn-danger"
                                data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ"
                                ID="btnsummary_back"
                                CommandName="btnsummary" OnCommand="btnCommand" />


                        </div>

                    </div>
                </div>
            </div>

            <br />

        </asp:View>

        <asp:View ID="View_ListDataLeader" runat="server">

            <asp:Panel ID="pnlListDataLeader" runat="server">

                <asp:Panel ID="Panel15" runat="server">
                    <div class="panel panel-primary m-t-10">
                        <div class="panel-heading f-bold">ค้นหา</div>
                        <div class="panel-body">
                            <asp:Panel ID="Panel16" runat="server" Visible="false">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>เดือน</label>
                                        <asp:DropDownList ID="ddlMonthSearch_Leader" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                            </asp:Panel>
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label>ปี</label>
                                    <asp:DropDownList ID="ddlYearSearch_Leader" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label>เลขที่เอกสาร / ชื่อหลักสูตร</label>
                                    <asp:TextBox ID="txtFilterKeyword_Leader" runat="server"
                                        CssClass="form-control"
                                        placeholder="เลขที่เอกสาร / ชื่อหลักสูตร..." />
                                </div>
                            </div>

                            <asp:Panel ID="pnlStatusapprove_Leader" runat="server">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>สถานะของเอกสาร</label>
                                        <asp:DropDownList ID="ddlStatusapprove_Leader" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="999" Text="-" Selected="True" />
                                            <asp:ListItem Value="0" Text="ดำเนินการ" />
                                            <asp:ListItem Value="4" Text="อนุมัติ" />
                                            <asp:ListItem Value="5" Text="กลับไปแก้ไข" />
                                            <asp:ListItem Value="6" Text="ไม่อนุมัติ" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </asp:Panel>

                            <div class="col-sm-2">
                                <label>
                                    &nbsp;
                               
                                </label>
                                <div class="form-group">
                                    <asp:LinkButton ID="btnFilterLeader" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                        OnCommand="btnCommand" CommandName="btnFilterLeader" />
                                    <asp:LinkButton ID="btnFilterLeader_app" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                        OnCommand="btnCommand" CommandName="btnFilterLeader_app" />
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>


                <div class="row">

                    <asp:GridView ID="GvListDataLeader"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-responsive col-md-12 word-wrap"
                        HeaderStyle-CssClass="info"
                        AllowPaging="true"
                        PageSize="10"
                        OnRowCommand="onRowCommand"
                        OnRowDataBound="onRowDataBound"
                        OnPageIndexChanging="gvPageIndexChanging"
                        DataKeyNames="u0_training_course_idx"
                        ShowFooter="false">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size: 13px;">
                                        <%# (Container.DataItemIndex + 1) %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เลขที่เอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size: 13px;">
                                        <%# Eval("training_course_no") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่สร้างเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size: 13px;">
                                        <%# Eval("zdate") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทหลักสูตร" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="7%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size: 13px;">
                                        <%--<%# course_plan_status_name((string)Eval("course_plan_status")) %>--%>
                                        <img src='<%# getImageIO((string)Eval("course_plan_status"),1) %>'
                                            width="50" height="50"
                                            title='<%# getImageIO((string)Eval("course_plan_status"),2) %>'
                                            id="imgicon_inplan" runat="server" visible="true" />
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อหลักสูตร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size: 13px;">
                                        <%# Eval("course_name") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="กลุ่มวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size: 13px;">
                                        <%# Eval("training_group_name") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สาขาวิชา" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                                <ItemTemplate>
                                    <div class="word-wrap" style="font-size: 13px;">
                                        <%# Eval("training_branch_name") %>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะของเอกสาร" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                                <ItemTemplate>

                                    <span>
                                        <%-- <%# getTextDoc((int)Eval("approve_status") , 
                                                       (int)Eval("md_approve_status") , 
                                                       (string)Eval("decision_name"),
                                                       (string)Eval("node_name"),
                                                       (string)Eval("actor_name"),
                                                       (string)Eval("md_decision_name"),
                                                       (string)Eval("md_node_name"),
                                                       (string)Eval("md_actor_name")
                                                       ) %>--%>
                                        <%# getTextAction((int)Eval("zacter_status"),(string)Eval("zdecision")) %>
                                    </span>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                <ItemTemplate>


                                    <asp:TextBox ID="txtapprove_status_GvListData" runat="server"
                                        Visible="false" Text='<%# Eval("approve_status") %>' />
                                    <asp:TextBox ID="txtmd_approve_status_GvListData" runat="server"
                                        Visible="false" Text='<%# Eval("md_approve_status") %>' />
                                    <asp:TextBox ID="txtGvtraining_course_no" runat="server"
                                        Visible="false" Text='<%# Eval("training_course_no") %>' />


                                    <asp:LinkButton ID="btnDetailWaitApprove"
                                        CssClass="btn btn-info btn-sm" runat="server"
                                        data-original-title="รายละเอียด" data-toggle="tooltip" Text="รายละเอียด"
                                        OnCommand="btnCommand" CommandName="btnDetailWaitApprove"
                                        CommandArgument='<%#  
                                           Convert.ToString(Eval("u0_training_course_idx")) + "|" +  
                                           Convert.ToString(Eval("super_app_status"))+ "|" + 
                                           Convert.ToString(Eval("approve_status"))+ "|" + 
                                           Convert.ToString(Eval("hr_status"))
                                             %>'>
                                            <i class="fa fa-file-alt"></i></asp:LinkButton>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>


            </asp:Panel>
            <%-- Start Select --%>
        </asp:View>


    </asp:MultiView>


    <%-- end search needs --%>

    <%-- start modal --%>

    <div class="col-lg-12" runat="server" id="Div2">
        <div id="DvRemarkRePrint" class="modal open" role="dialog">
            <div class="modal-dialog" style="width: 50%;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">ช่องแสดงเหตุผลในการ Reprint</h4>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="row">
                                    <div class="form-group">
                                        <asp:Label ID="Label22" runat="server" Text="เหตุผล" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtRemarkReprint" TextMode="multiline" Rows="5" runat="server" CssClass="form-control embed-responsive-item" PlaceHoldaer="........" />

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-2"></div>
                                        <div class="col-sm-10">
                                            <asp:Label ID="Label23" runat="server" Text="*** ถ้าไม่ใส่เหตุผลจะไม่สามารถ Reprint ได้ !"
                                                ForeColor="Red"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-sm-4 col-sm-offset-10">

                                            <asp:LinkButton ID="Cancel" class="btn btn-default btn-sm" data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal" CommandName="Cancel">&nbsp;Close</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-lg-12" runat="server" id="DivEdit_IT">
        <div id="edit_register" class="modal open" role="dialog">
            <div class="modal-dialog" style="width: 70%;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <%--ข้อมูลอุปกรณ์ register--%>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="panel-heading">

                                    <asp:FormView ID="FvEdit_register" runat="server" DefaultMode="Edit" OnDataBound="FvDetail_DataBound" Width="100%">
                                        <EditItemTemplate>

                                            <div class="form-group">
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtu3_training_course_idx" runat="server"
                                                        CssClass="form-control"
                                                        Visible="false" Text='<%# Eval("u3_training_course_idx")%>' />
                                                    <asp:TextBox ID="txtu0_training_course_idx_ref" runat="server"
                                                        CssClass="form-control"
                                                        Visible="false" Text='<%# Eval("u0_training_course_idx_ref")%>' />
                                                </div>
                                            </div>

                                            <asp:Panel ID="pnlapprove_hr" runat="server">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="form-group col-lg-12">
                                                            <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ผลการลงทะเบียน" />
                                                            <div class="col-sm-4">
                                                                <asp:DropDownList ID="ddlStatusapprove"
                                                                    CssClass="form-control" runat="server"
                                                                    Enabled="false">

                                                                    <asp:ListItem Value="1" Text="ยืนยันการลงทะเบียน" Selected="True" />
                                                                    <%--<asp:ListItem Value="2" Text="ไม่ลงทะเบียน" />
                                                                        SelectedValue='<%# Eval("register_status") %>'
                                                                        <asp:ListItem Value="0" Text="ผลการลงทะเบียน....." />
                                                                    --%>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="RequiredddlStatusapprove"
                                                                    ValidationGroup="lbCmdUpdate" runat="server"
                                                                    Display="Dynamic"
                                                                    SetFocusOnError="true"
                                                                    InitialValue="0"
                                                                    ControlToValidate="ddlStatusapprove"
                                                                    Font-Size="1em" ForeColor="Red"
                                                                    CssClass="pull-left"
                                                                    ErrorMessage="กรุณากรอกผลการลงทะเบียน" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-lg-12">
                                                            <asp:Label ID="Label35" CssClass="col-sm-3 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม" />
                                                            <div class="col-sm-9">
                                                                <asp:TextBox ID="txtdetailApprove" CssClass="form-control" runat="server"
                                                                    placeholder="ความคิดเห็นเพิ่มเติม ....." MaxLength="150" TextMode="multiline" Rows="4"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </asp:Panel>

                                            <asp:UpdatePanel ID="updatepanel" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate"
                                                                CssClass="btn btn-success btn-sm" runat="server"
                                                                OnCommand="btnCommand"
                                                                ValidationGroup="lbCmdUpdate"
                                                                CommandName="lbCmdUpdate"
                                                                CommandArgument='<%# Eval("u3_training_course_idx") %>'>
                                                                <i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" class="btn btn-default btn-sm"
                                                                data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal"
                                                                CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>

                                                        </div>
                                                    </div>

                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="lbCmdUpdate" />
                                                </Triggers>
                                            </asp:UpdatePanel>

                                        </EditItemTemplate>
                                    </asp:FormView>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--  --%>

    <div class="col-lg-12" runat="server" id="Div1">
        <div id="edit_signup" class="modal open" role="dialog">
            <div class="modal-dialog" style="width: 70%;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <%--ข้อมูล employee signup--%>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="panel-heading">

                                    <asp:FormView ID="FvEdit_signup" runat="server" DefaultMode="Edit" OnDataBound="FvDetail_DataBound" Width="100%">
                                        <EditItemTemplate>


                                            <div class="form-group">
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtu3_training_course_idx" runat="server"
                                                        CssClass="form-control"
                                                        Visible="false" Text='<%# Eval("u3_training_course_idx")%>' />
                                                    <asp:TextBox ID="txtu0_training_course_idx_ref" runat="server"
                                                        CssClass="form-control"
                                                        Visible="false" Text='<%# Eval("u0_training_course_idx_ref")%>' />
                                                </div>
                                            </div>


                                            <asp:Panel ID="pnlapprove_hr" runat="server">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <div class="form-group col-lg-12">
                                                            <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ผลการลงทะเบียน" />
                                                            <div class="col-sm-4">
                                                                <asp:DropDownList ID="ddlStatusapprove"
                                                                    CssClass="form-control" runat="server"
                                                                    Enabled="false">

                                                                    <asp:ListItem Value="1" Text="ยืนยันการลงทะเบียนเข้าอบรม" Selected="True" />
                                                                    <%--<asp:ListItem Value="2" Text="ไม่ลงทะเบียน" />
                                                                        SelectedValue='<%# Eval("register_status") %>'
                                                                        <asp:ListItem Value="0" Text="ผลการลงทะเบียน....." />
                                                                    --%>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="RequiredddlStatusapprove"
                                                                    ValidationGroup="lbCmdUpdate_signup" runat="server"
                                                                    Display="Dynamic"
                                                                    SetFocusOnError="true"
                                                                    InitialValue="0"
                                                                    ControlToValidate="ddlStatusapprove"
                                                                    Font-Size="1em" ForeColor="Red"
                                                                    CssClass="pull-left"
                                                                    ErrorMessage="กรุณากรอกผลการลงทะเบียน" />
                                                            </div>
                                                        </div>

                                                        <div class="form-group col-lg-12">
                                                            <asp:Label ID="Label35" CssClass="col-sm-3 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม" />
                                                            <div class="col-sm-9">
                                                                <asp:TextBox ID="txtdetailApprove" CssClass="form-control" runat="server"
                                                                    placeholder="ความคิดเห็นเพิ่มเติม ....." MaxLength="150" TextMode="multiline" Rows="4"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </asp:Panel>

                                            <asp:UpdatePanel ID="updatepanel" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate_signup"
                                                                CssClass="btn btn-success btn-sm" runat="server"
                                                                OnCommand="btnCommand"
                                                                ValidationGroup="lbCmdUpdate_signup"
                                                                CommandName="lbCmdUpdate_signup"
                                                                CommandArgument='<%# Eval("u3_training_course_idx") %>'>
                                                                <i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel_signup" class="btn btn-default btn-sm"
                                                                data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal"
                                                                CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>

                                                        </div>
                                                    </div>

                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="lbCmdUpdate_signup" />
                                                </Triggers>
                                            </asp:UpdatePanel>

                                        </EditItemTemplate>
                                    </asp:FormView>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--  --%>

    <div class="col-lg-12" runat="server" id="Div3">
        <div id="edit_test_scores" class="modal open" role="dialog">
            <div class="modal-dialog" style="width: 70%;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <%--ข้อมูล employee signup--%>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="panel-heading">

                                    <asp:FormView ID="FvEdit_test_scores" runat="server" DefaultMode="Edit" OnDataBound="FvDetail_DataBound" Width="100%">
                                        <EditItemTemplate>


                                            <div class="form-group">
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtu3_training_course_idx" runat="server"
                                                        CssClass="form-control"
                                                        Visible="false" Text='<%# Eval("u3_training_course_idx")%>' />
                                                    <asp:TextBox ID="txtu0_training_course_idx_ref" runat="server"
                                                        CssClass="form-control"
                                                        Visible="false" Text='<%# Eval("u0_training_course_idx_ref")%>' />
                                                </div>
                                            </div>


                                            <asp:Panel ID="pnlapprove_hr" runat="server">
                                                <div class="row">


                                                    <div class="form-group">
                                                        <div class="form-group col-lg-12">
                                                            <asp:Label ID="Label99" CssClass="col-sm-3 control-label" runat="server" Text="คะแนนเต็ม" />
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txttest_scores_total"
                                                                    CssClass="form-control" runat="server"
                                                                    Enabled="false"
                                                                    TextMode="Number">
                                                                </asp:TextBox>

                                                            </div>
                                                        </div>

                                                        <div class="form-group col-lg-12">
                                                            <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="คะแนนสอบ" />
                                                            <div class="col-sm-4">
                                                                <asp:TextBox ID="txttest_scores"
                                                                    CssClass="form-control" runat="server"
                                                                    Enabled="true"
                                                                    TextMode="Number">
                                                                </asp:TextBox>

                                                            </div>
                                                        </div>

                                                        <div class="form-group col-lg-12">
                                                            <asp:Label ID="Label35" CssClass="col-sm-3 control-label" runat="server" Text="ความคิดเห็นเพิ่มเติม" />
                                                            <div class="col-sm-9">
                                                                <asp:TextBox ID="txtdetailApprove" CssClass="form-control" runat="server"
                                                                    placeholder="ความคิดเห็นเพิ่มเติม ....." MaxLength="150" TextMode="multiline" Rows="4"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>

                                            </asp:Panel>

                                            <asp:UpdatePanel ID="updatepanel" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate_test_scores"
                                                                CssClass="btn btn-success btn-sm" runat="server"
                                                                OnCommand="btnCommand"
                                                                ValidationGroup="lbCmdUpdate_test_scores"
                                                                CommandName="lbCmdUpdate_test_scores"
                                                                CommandArgument='<%# Eval("u3_training_course_idx") %>'>
                                                                <i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel_test_scores" class="btn btn-default btn-sm"
                                                                data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal"
                                                                CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>

                                                        </div>
                                                    </div>

                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="lbCmdUpdate_test_scores" />
                                                </Triggers>
                                            </asp:UpdatePanel>

                                        </EditItemTemplate>
                                    </asp:FormView>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--  --%>

    <div class="col-lg-12" runat="server" id="Div4">
        <div id="edit_emp_resulte" class="modal open" role="dialog">
            <div class="modal-dialog" style="width: 70%;">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">รายชื่อผู้ขอสิทธิ์เข้าร่วมคอร์สอบรม</h4>
                        <%--ข้อมูล employee resulte--%>
                    </div>
                    <div class="modal-body">
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="panel-heading">

                                    <div class="panel-body" style="height: 400px; overflow-y: scroll; overflow-x: scroll;">
                                        <asp:GridView ID="Gvemp_resulte" runat="server"
                                            CssClass="table table-striped table-responsive table-bordered word-wrap"
                                            GridLines="None" OnRowCommand="onRowCommand"
                                            AutoGenerateColumns="false">
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">ไม่พบข้อมูล</div>
                                            </EmptyDataTemplate>
                                            <Columns>

                                                <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center"
                                                    HeaderStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <small>
                                                            <%# (Container.DataItemIndex +1) %>
                                                            <asp:Label ID="lbemp_u0_training_course_idx_ref" runat="server"
                                                                Visible="false"
                                                                Text='<%# Bind("u0_training_course_idx_ref") %>'></asp:Label>
                                                            <asp:Label ID="lbemp_u7_training_course_idx" runat="server"
                                                                Visible="false"
                                                                Text='<%# Bind("u7_training_course_idx") %>'></asp:Label>
                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField
                                                    HeaderText="รหัสพนักงาน"
                                                    ItemStyle-HorizontalAlign="Left"
                                                    HeaderStyle-CssClass="text-center"
                                                    ItemStyle-Font-Size="Small">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtemp_zEmpCode_edit_L"
                                                            Width="100%"
                                                            runat="server" Text='<%# Bind("EmpCode") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="txtemp_zEmpCode_item_L" runat="server" Text='<%# Eval("EmpCode") %>'
                                                            Width="100%"></asp:Label>
                                                        <asp:Label ID="txtemp_zemp_idx_ref_item_L" runat="server" Text='<%# Eval("emp_idx_ref") %>'
                                                            Visible="false"
                                                            Width="100%"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField
                                                    HeaderText="ชื่อ - สกุล"
                                                    ItemStyle-HorizontalAlign="Left"
                                                    HeaderStyle-CssClass="text-center"
                                                    ItemStyle-Font-Size="Small">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtemp_zName_edit_L" runat="server" Text='<%# Bind("FullNameTH") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="txtemp_zName_item_L" runat="server" Text='<%# Eval("FullNameTH") %>'
                                                            Width="100%"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField
                                                    HeaderText="ตำแหน่ง"
                                                    ItemStyle-HorizontalAlign="Left"
                                                    HeaderStyle-CssClass="text-center"
                                                    HeaderStyle-Width="15%"
                                                    ItemStyle-Font-Size="Small">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtemp_zPostName_edit_L" runat="server" Text='<%# Bind("PosNameTH") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="txtemp_zPostName_item_L" runat="server" Text='<%# Eval("PosNameTH") %>'
                                                            Width="100%"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField
                                                    HeaderText="Cost Center"
                                                    ItemStyle-HorizontalAlign="Left"
                                                    HeaderStyle-CssClass="text-center"
                                                    HeaderStyle-Width="7%"
                                                    ItemStyle-Font-Size="Small">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtemp_zCostNo_edit_L" runat="server" Text='<%# Bind("CostNo") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="txtemp_zCostNo_item_L" runat="server" Text='<%# Eval("CostNo") %>'
                                                            Width="100%"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField
                                                    HeaderText="เบอร์ติดต่อ"
                                                    ItemStyle-HorizontalAlign="Left"
                                                    HeaderStyle-CssClass="text-center"
                                                    ItemStyle-Font-Size="Small">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtemp_zMobileNo_edit_L" runat="server" Text='<%# Bind("MobileNo") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="txtemp_zMobileNo_item_L" runat="server" Text='<%# Eval("MobileNo") %>'
                                                            Width="100%"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Action" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                    HeaderStyle-Font-Size="Small"
                                                    Visible="true">
                                                    <EditItemTemplate>
                                                        <asp:DropDownList ID="cbemp_sel_edit_L" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="0" Text="....." Selected="True" />
                                                            <asp:ListItem Value="4" Text="อนุมัติ" />
                                                            <asp:ListItem Value="6" Text="ไม่อนุมัติ" />
                                                        </asp:DropDownList>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:DropDownList ID="cbemp_sel_item_L" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="0" Text="....." Selected="True" />
                                                            <asp:ListItem Value="4" Text="อนุมัติ" />
                                                            <asp:ListItem Value="6" Text="ไม่อนุมัติ" />
                                                        </asp:DropDownList>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField
                                                    HeaderText="หมายเหตุ"
                                                    ItemStyle-HorizontalAlign="Left"
                                                    HeaderStyle-CssClass="text-center"
                                                    ItemStyle-Font-Size="Small">
                                                    <EditItemTemplate>
                                                        <asp:TextBox ID="txtemp_zRemark_edit_L" runat="server" Text='<%# Bind("remark") %>'></asp:TextBox>
                                                    </EditItemTemplate>
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="txtemp_zRemark_item_L" runat="server" Text='<%# Eval("remark") %>'
                                                            Width="100%" CssClass="form-control"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>
                                        </asp:GridView>
                                    </div>

                                    <asp:UpdatePanel ID="updatepanel" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-10">
                                                    <asp:LinkButton ID="btnCmdUpdate_emp_resulte"
                                                        CssClass="btn btn-success btn-sm" runat="server"
                                                        OnCommand="btnCommand"
                                                        ValidationGroup="btnCmdUpdate_emp_resulte"
                                                        CommandName="btnCmdUpdate_emp_resulte">
                                                                <i class="fa fa-check"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lbCmdCancel_emp_resulte" class="btn btn-default btn-sm"
                                                        data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal"
                                                        CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>

                                                </div>
                                            </div>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnCmdUpdate_emp_resulte" />
                                        </Triggers>
                                    </asp:UpdatePanel>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--  --%>

    <%-- end modal --%>

    <asp:HiddenField ID="Hddfld_status" runat="server" />
    <asp:HiddenField ID="Hddfld_training_course_no" runat="server" />
    <asp:HiddenField ID="Hddfld_u0_training_course_idx" runat="server" />
    <asp:HiddenField ID="Hddfld_folder" runat="server" />
    <asp:HiddenField ID="Hddfld_mode" runat="server" />
    <asp:HiddenField ID="Hddfld_in_out_plan" runat="server" />
    <asp:HiddenField ID="Hddfld_permission" runat="server" />
    <asp:HiddenField ID="Hddfld_permission_approve" runat="server" />
    <asp:HiddenField ID="Hddfld_leader_status" runat="server" />
    <asp:HiddenField ID="Hddfld_costno" runat="server" />
    <asp:HiddenField ID="Hddfld_rdept" runat="server" />



    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadFileCourse").MultiFile();
            }
        })
    </script>

    <script type="text/javascript">
        function openModal_register() {
            $('#edit_register').modal('show');

        }
    </script>
    <script type="text/javascript">
        function openModal_signup() {
            $('#edit_signup').modal('show');

        }
    </script>
    <script type="text/javascript">
        function openModal_test_scores() {
            $('#edit_test_scores').modal('show');

        }
    </script>
    <script type="text/javascript">
        function openModal_emp_resulte() {
            $('#edit_emp_resulte').modal('show');

        }
    </script>
    <script type="text/javascript">
        function imageproposUpload(input) {
            if (input.files && input.files[0]) {

                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    //$('#img_1').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
                var data_proposition = new FormData();
                var filename = $("#txtfileimport").val();
                var extension = filename.replace(/^.*\./, '');
                if (extension == filename) {
                    extension = '';
                } else {
                    extension = extension.toLowerCase();
                }
                if (
                    (extension != "jpg") &&
                    (extension != "gif") &&
                    (extension != "png") &&
                    (extension != "jpeg") &&
                    (extension != "xls") &&
                    (extension != "xlsx") &&
                    (extension != "doc") &&
                    (extension != "docx") &&
                    (extension != "pdf")
                ) {
                    alert('กรุณาเลือกไฟล์เอกสาร .jpg,.gif,.png,.jpeg,.xls,.xlsx,.doc,.docx,.pdf ' + "เท่านั้น");
                    $("#txtfileimport").val("");
                }
                else {

                    var files = $("#txtfileimport").get(0).files;
                    if (files.length > 0) {
                        data_proposition.append("Uploaded_txtfileimport", files[0]);
                    }

                    var ajaxRequest = $.ajax({
                        type: "POST",
                        url: '<%=ResolveUrl("el_Trn_plan_course.aspx") %>',
                        contentType: false,
                        processData: false,
                        data: data_proposition,
                        async: false,
                        success: function (response) {
                            alert('ไฟล์เอกสารพร้อมสำหรับตรวจสอบไฟล์');
                        },
                        error: function (error) {
                            alert('ไฟล์เอกสารไม่พร้อม.!');
                        }
                    });

                    ajaxRequest.done(function (xhr, textStatus) {
                        // Do other operation
                    });
                }
            }

        }


        function LoadProgressBar(result) {
            var progressbar = $("#progressbar-5");
            var progressLabel = $(".progress-label");
            progressbar.show();
            $("#progressbar-5").progressbar({
                //value: false,
                change: function () {
                    progressLabel.text(
                        progressbar.progressbar("value") + "%");
                },
                complete: function () {
                    progressLabel.text("Loading Completed!");
                    progressbar.progressbar("value", 0);
                    progressLabel.text("");
                    progressbar.hide();
                    var markup = "<tr><td>" + result + "</td><td><a href='#' onclick='DeleteFile(\"" + result + "\")'><span class='glyphicon glyphicon-remove red'></span></a></td></tr>";
                    $("#ListofFiles tbody").append(markup);
                    $('#Files').val('');
                    $('#FileBrowse').find("*").prop("disabled", false);
                }
            });
            function progress() {
                var val = progressbar.progressbar("value") || 0;
                progressbar.progressbar("value", val + 1);
                if (val < 99) {
                    setTimeout(progress, 25);
                }
            }
            setTimeout(progress, 100);
        }

    </script>

    <script type="text/javascript">
        function ImportEmpProposUpload(input) {
            if (input.files && input.files[0]) {

                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    //$('#img_1').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
                var data_proposition = new FormData();
                var filename = $("#file_import_employee").val();
                var extension = filename.replace(/^.*\./, '');
                if (extension == filename) {
                    extension = '';
                } else {
                    extension = extension.toLowerCase();
                }
                if (
                    (extension != "xls") &&
                    (extension != "xlsx")
                ) {
                    alert('กรุณาเลือกไฟล์ .xls,.xlsx ' + "เท่านั้น");
                    $("#file_import_employee").val("");
                }
                else {

                    var files = $("#file_import_employee").get(0).files;
                    if (files.length > 0) {
                        data_proposition.append("Uploaded_file_import_employee", files[0]);
                    }

                    var ajaxRequest = $.ajax({
                        type: "POST",
                        url: '<%=ResolveUrl("el_Trn_plan_course.aspx") %>',
                        contentType: false,
                        processData: false,
                        data: data_proposition,
                        async: false,
                        success: function (response) {
                            alert('ไฟล์พนักงานพร้อม Import');
                        },
                        error: function (error) {
                            alert('ไฟล์พนักงานไม่พร้อม Import.!');
                        }
                    });

                    ajaxRequest.done(function (xhr, textStatus) {
                        // Do other operation
                    });
                }
            }

        }


    </script>



    <script type="text/javascript">
        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            encoding: "xml",
            theme: "modern",
            menubar: false,
            resize: false,
            statusbar: false,
            plugins: ["advlist autolink lists charmap preview hr anchor",
                "pagebreak code nonbreaking table contextmenu directionality paste"],
            toolbar1: "styleselect | bold italic underline | undo redo",
            toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            setup: function (editor) {
                editor.on('change', function () { tinymce.triggerSave(); });
            }
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            tinymce.remove(".tinymce");

            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "tinymce",
                encoding: "xml",
                theme: "modern",
                menubar: false,
                resize: false,
                statusbar: false,
                plugins: ["advlist autolink lists charmap preview hr anchor",
                    "pagebreak code nonbreaking table contextmenu directionality paste"],
                toolbar1: "styleselect | bold italic underline | undo redo",
                toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                setup: function (editor) {
                    editor.on('change', function () { tinymce.triggerSave(); });
                }
            });

            $(".multi").MultiFile();
        })
    </script>

    <script type="text/javascript">
        //http://localhost/mas.taokaenoi.co.th/websystem/elearning/transection_el/el_TrnPlan.aspx
        function ShowCurrentTime() {
            $.ajax({
                type: "POST",
                url: '<%=this.Request.Url.OriginalString %>/GetCurrentTime',
                <%--data: '{name: "' + $("#<%=txtUserName.ClientID%>")[0].value + '" }',--%>
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
        function OnSuccess(response) {
            alert(response.d);
        }
        <%--function ShowCurrentTime() {
            alert('<%=this.Request.Url.OriginalString %>');
        }--%>
</script>


    <script type="text/javascript">

        function CallJS() {
            PageMethods.CodebehindMethodName(onSucceed, onError);
            return false;
        }
        function onSucceed(value) {
            alert(value)
        }
        function onError(value) {
            alert(value)
        }
    </script>

    <script type="text/javascript">
        function ShowCurrentTime1(value) {

            //var connection = new ActiveXObject("ADODB.Connection");

            //var connectionstring = "Data Source=<server>;Initial Catalog=<catalog>;User ID=<user>;Password=<password>;Provider=SQLOLEDB";

            //connection.Open(connectionstring);
            //var rs = new ActiveXObject("ADODB.Recordset");

            //rs.Open("SELECT * FROM table", connection);
            //rs.MoveFirst
            //while (!rs.eof) {
            //    document.write(rs.fields(1));
            //    rs.movenext;
            //}

            //rs.close;
            //connection.close;   url: '<%=this.Request.Url.OriginalString %>/GetCurrentTime',

           // alert('<%=ResolveUrl("el_TrnPlan.aspx") %>');
            $.ajax({
                type: "POST",
                url: '<%=ResolveUrl("el_TrnPlan.aspx") %>/GetCurrentTime',
                data: '{name: "' + value + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnSuccess,
                failure: function (response) {
                    alert(response.d);
                    //$("#success_alert").hide();

                }
            });
        }
        function OnSuccess(response) {
            alert(response.d);
            //$('#DvRemarkRePrint').modal('show');
            //  $("#success_alert").show();
            //        $("#success_alert").fadeTo(500, 500).slideUp(500, function () {
            //        $("#success_alert").slideUp(500);
            //});
        }
    </script>

    <script type="text/javascript">
        var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
    </script>

    <script type="text/javascript">
        function fnExcelReport() {
            var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange; var j = 0;
            tab = document.getElementById('Tableprint_sched'); // id of table

            for (j = 0; j < tab.rows.length; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            //This function will raise when click on html button control
            $("#Button1_sched").click(function () {
                showalert('HTML Button Clicked');
            });
            //This function will raise when click on asp button control

        });
        function showalert(btnText) {
            alert(btnText)
        }
    </script>

    <style type="text/css">
        . rapwor
        p {
            word-wrap: ak-word;
        }
        /* END
    </style>
    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);
            var printWindow = window.open('', '', 'height=800,width=1000');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
                //printWindow.resizeTo(100,100);
                //printWindow.focus();
                //printwindow.window.close();
            }, 500);
            return false;

        }

        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }

        function dtPickerInput(inputClassFrom, inputClassTo) {
            $(inputClassFrom).datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $(inputClassTo).datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }

        function dtPickerBtn(inputClassFrom, inputClassTo, btnClassFrom, btnClassTo) {
            $(btnClassFrom).click(function () {
                $(inputClassFrom).data("DateTimePicker").show();
            });
            $(btnClassTo).click(function () {
                $(inputClassTo).data("DateTimePicker").show();
            });
        }

        $(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-createdocdate-onclick').click(function () {
                $('.filter-order-from-createdocdate').data("DateTimePicker").show();
            });
            $('.filter-order-from-createdocdate').datetimepicker({
                format: 'DD/MM/YYYY'
            });

            $('.show-order-sale-log-from-createstart-onclick').click(function () {
                $('.filter-order-from-createstart').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-createend-onclick').click(function () {
                $('.filter-order-to-createend').data("DateTimePicker").show();
            });
            $('.filter-order-from-createstart').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to-createend').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-sale-log-from-createstart-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-createend-onclick').click(function () {
                $('.filter-order-to-createend').data("DateTimePicker").show();
            });
            $('.filter-order-from-createstart').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to-createend').datetimepicker({
                format: 'DD/MM/YYYY'
            });

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-createdocdate-onclick').click(function () {
                $('.filter-order-from-createdocdate').data("DateTimePicker").show();
            });
            $('.filter-order-from-createdocdate').datetimepicker({
                format: 'DD/MM/YYYY'
            });

            $('.show-order-sale-log-from-createstart-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-createend-onclick').click(function () {
                $('.filter-order-to-createend').data("DateTimePicker").show();
            });
            $('.filter-order-from-createstart').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to-createend').datetimepicker({
                format: 'DD/MM/YYYY'
            });


        });
    </script>


    <script type="text/javascript">


        //no post back
        $('.clockpicker').datetimepicker({
            format: 'HH:mm',
            //  stepping: 30,
            ignoreReadonly: true
        });
        $('.show-time-onclick').click(function () {
            $('.clockpicker').data("DateTimePicker").show();
        });

        $('.clockpickerto').datetimepicker({
            format: 'HH:mm',
            //   stepping: 30,
            ignoreReadonly: true
        });
        $('.show-timeto-onclick').click(function () {
            $('.clockpickerto').data("DateTimePicker").show();
        });

        //is
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {

            $('.loading-icon-approve').hide();

            $('.clockpicker').datetimepicker({
                format: 'HH:mm',
                // stepping: 30,
                ignoreReadonly: true
            });
            $('.show-time-onclick').click(function () {
                $('.clockpicker').data("DateTimePicker").show();
            });

            $('.clockpickerto').datetimepicker({
                format: 'HH:mm',
                // stepping: 30,
                ignoreReadonly: true
            });
            $('.show-timeto-onclick').click(function () {
                $('.clockpickerto').data("DateTimePicker").show();
            });
        });


        function getAlertSuccess() {
            $("#success_alert").show();
            $("#success_alert").fadeTo(500, 500).slideUp(500, function () {
                $("#success_alert").slideUp(500);
            });
        }

        function ValidatePage(toHide, toShow) {

            $(toHide).hide();
            $(toShow).show();

        }
    </script>
    <script type="text/javascript">

        function SetTarget() {

            document.forms[0].target = "_blank";

        }
        function shwwindow(myurl) {
            window.open(myurl, '_blank');
        }

    </script>

    <div>
    </div>

</asp:Content>

