﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="system.aspx.cs" Inherits="websystem_controlpanel_system" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
    <div class="col-md-12" role="main">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>

        <div class="col-md-6">
            <div id="fvViewSystem" runat="server" class="panel panel-info">
                <div class="panel-heading">System</div>
                    <div class="panel-body">
                        <asp:Formview ID="fvSystem" runat="server" DefaultMode="Insert" width="100%">
                            <InsertItemTemplate>
                                <%--<asp:HiddenField ID="hfSysIdx" runat="server" Value="0"></asp:HiddenField>
							    <asp:HiddenField ID="hfMenuRelation" runat="server" Value="0"></asp:HiddenField>--%>
                                <div class="form-horizontal" role="form">
								<div class="form-group">
									<label class="col-sm-4 control-label">
									<asp:Label ID="lblSysNameTH" runat="server" Text="SysName TH"></asp:Label></label>
									<div class="col-sm-8">
										<asp:TextBox ID="tbSysNameTH" runat="server" CssClass="form-control" placeholder="SysName TH" MaxLength="30" ValidationGroup="formInsert" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-4 control-label">
									<asp:Label ID="lblSysNameEN" runat="server" Text="SysName EN"></asp:Label></label>
									<div class="col-sm-8">
										<asp:TextBox ID="tbSysNameEN" runat="server" CssClass="form-control" placeholder="SysName EN" MaxLength="30" ValidationGroup="formInsert" />
									</div>
								</div>
								<%--<div class="form-group">
									<label class="col-sm-4 control-label">
									<asp:Label ID="lblMenuRelation" runat="server" Text="Parent Menu"></asp:Label></label>
									<div class="col-sm-8">
										<asp:DropDownList ID="ddlMenuRelation" runat="server" CssClass="form-control" ValidationGroup="formInsert" />
									</div>
								</div>--%>
								
								<div class="form-group">
									<div class="col-sm-offset-4 col-sm-8">
										<asp:LinkButton ID="lbInsertSystem" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandName="cmdSaveSystem" CommandArgument="0" ValidationGroup="formInsert"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></asp:LinkButton>
										<asp:LinkButton ID="lbClear" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancelSystem"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></asp:LinkButton>
									</div>
								</div>
							</div>


                            </InsertItemTemplate>


                        </asp:Formview>

                    </div>

            </div>

        </div>

    </div>

</asp:Content>