﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_adonline_permission_type : System.Web.UI.Page
{
   #region Init
   data_adonline dataADOnline = new data_adonline();
   service_execute servExec = new service_execute();
   function_tool _funcTool = new function_tool();
   string misConn = "conn_mis";
   string misConnReal = "conn_mis_real";
   string adPermissionCategoryService = "adPermissionCategoryService";
   string _local_xml = String.Empty;
   int[] empIDX = { 172, 173, 1374, 1413, 3760, 4839 }; //P'Phon, P'Mai, Bonus, P'Toei, Mod, Jame
   #endregion Init

   #region Constant
   public static class Constants
   {
      public const int SELECT_ALL = 20;
      public const int CREATE = 10;
      public const int UPDATE = 11;
      public const int SELECT_WHERE = 21;
      public const int SELECT_WHERE_SEARCH = 22;
      public const int BAN = 30;
      public const int UNBAN = 31;
      public const int SELECT_WHERE_EXISTS_NAME = 28;
      public const int SELECT_WHERE_EXISTS_NAME_UPDATE = 29;
      public const string VALID_FALSE = "101";
   }
   #endregion Constant

   #region Page Load
   protected void Page_Load(object sender, EventArgs e)
   {
      if (!IsPostBack)
      {
         divIndex.Visible = true;
         divInsert.Visible = false;
         ViewState["empIDX"] = Session["emp_idx"];
         ViewState["keywordSearch"] = "";
         ViewState["statusSearch"] = 99;
         actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
      }
   }
   #endregion Page Load

   #region Action
   protected void actionIndex(string keywordSearch = "", int statusSearch = 99)
   {
      permission_category objADOnline = new permission_category();
      dataADOnline.ad_permission_category_action = new permission_category[1];
      objADOnline.keyword_search_perm_cate = keywordSearch;
      objADOnline.perm_cate_status = statusSearch;
      dataADOnline.ad_permission_category_action[0] = objADOnline;
      _local_xml = servExec.actionExec(misConn, "data_adonline", adPermissionCategoryService, dataADOnline, Constants.SELECT_WHERE_SEARCH);
      dataADOnline = (data_adonline)_funcTool.convertXmlToObject(typeof(data_adonline), _local_xml);
      setGridData(gvPermCate, dataADOnline.ad_permission_category_action);
   }

   protected void actionCreate()
   {
      Page.Validate();
      if (Page.IsValid == true)
      {
         permission_category objADOnline = new permission_category();
         dataADOnline.ad_permission_category_action = new permission_category[1];
         objADOnline.perm_cate_name = txtPermCateName.Text.Trim();
         objADOnline.perm_cate_status = int.Parse(ddlPermCateStatus.SelectedValue);
         dataADOnline.ad_permission_category_action[0] = objADOnline;
         _local_xml = servExec.actionExec(misConn, "data_adonline", adPermissionCategoryService, dataADOnline, Constants.CREATE);
         dataADOnline = (data_adonline)_funcTool.convertXmlToObject(typeof(data_adonline), _local_xml);
         if (dataADOnline.return_code.ToString() == "0")
         {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
         }
         else
         {
            _funcTool.showAlert(this, "ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง");
         }
      }
   }

   protected void actionUpdate(int idx, string name, int status)
   {
      permission_category objADOnline = new permission_category();
      dataADOnline.ad_permission_category_action = new permission_category[1];
      objADOnline.m0_perm_cate_idx = idx;
      objADOnline.perm_cate_name = name;
      objADOnline.perm_cate_status = status;
      dataADOnline.ad_permission_category_action[0] = objADOnline;
      servExec.actionExec(misConn, "data_adonline", adPermissionCategoryService, dataADOnline, Constants.UPDATE);
   }

   protected void actionBan(int id)
   {
      permission_category objADOnline = new permission_category();
      dataADOnline.ad_permission_category_action = new permission_category[1];
      objADOnline.m0_perm_cate_idx = id;
      dataADOnline.ad_permission_category_action[0] = objADOnline;
      servExec.actionExec(misConn, "data_adonline", adPermissionCategoryService, dataADOnline, Constants.BAN);
   }

   protected void actionUnban(int id)
   {
      permission_category objADOnline = new permission_category();
      dataADOnline.ad_permission_category_action = new permission_category[1];
      objADOnline.m0_perm_cate_idx = id;
      dataADOnline.ad_permission_category_action[0] = objADOnline;
      servExec.actionExec(misConn, "data_adonline", adPermissionCategoryService, dataADOnline, Constants.UNBAN);
   }
   #endregion Action

   #region btnCommand
   protected void btnCommand(object sender, CommandEventArgs e)
   {
      string cmdName = e.CommandName.ToString();
      string cmdArg = e.CommandArgument.ToString();
      switch (cmdName)
      {
         case "btnToInsert":
            divIndex.Visible = false;
            divInsert.Visible = true;
            break;

         case "btnInsert":
            actionCreate();
            break;

         case "btnSearch":
            ViewState["keywordSearch"] = keywordSearch.Text.Trim();
            ViewState["statusSearch"] = ddlStatus.SelectedValue;
            actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
            break;

         case "btnBan":
            actionBan(int.Parse(cmdArg));
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnUnBan":
            actionUnban(int.Parse(cmdArg));
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;

         case "btnCancel":
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
            break;
      }
   }
   #endregion btnCommand

   #region Custom Functions
   protected void setGridData(GridView gvName, Object obj)
   {
      gvName.DataSource = obj;
      gvName.DataBind();
   }

   protected string getStatus(int status)
   {
      if (status == 1)
      {
         return "<span class='status-online f-bold'>Online</span>";
      }
      else if (status == 0)
      {
         return "<span class='status-offline f-bold'>Offline</span>";
      }
      else
      {
         return "<span class='status-offline f-bold'>Delete</span>";
      }
   }

   protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPermCate":
            gvName.PageIndex = e.NewPageIndex;
            gvName.DataBind();
            actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
            break;
      }
   }

   protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPermCate":
            if (e.Row.RowState.ToString().Contains("Edit"))
            {
               GridView editGrid = sender as GridView;
               int colSpan = editGrid.Columns.Count;
               for (int i = 1; i < colSpan; i++)
               {
                  e.Row.Cells[i].Visible = false;
                  e.Row.Cells[i].Controls.Clear();
               }
               e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
               e.Row.Cells[0].CssClass = "";
            }
            break;
      }
   }

   protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPermCate":
            gvName.EditIndex = -1;
            actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
            break;
      }
   }

   protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPermCate":
            int dataKeyId = Convert.ToInt32(gvName.DataKeys[e.RowIndex].Values[0].ToString());
            var txtPermCateNameUpdate = (TextBox)gvName.Rows[e.RowIndex].FindControl("txtPermCateNameUpdate");
            var ddlPermCateStatusUpdate = (DropDownList)gvName.Rows[e.RowIndex].FindControl("ddlPermCateStatusUpdate");
            gvName.EditIndex = -1;
            actionUpdate(dataKeyId, txtPermCateNameUpdate.Text.Trim(), int.Parse(ddlPermCateStatusUpdate.SelectedValue));
            actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
            break;
      }
   }

   protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPermCate":
            gvName.EditIndex = e.NewEditIndex;
            actionIndex(ViewState["keywordSearch"].ToString(), int.Parse(ViewState["statusSearch"].ToString()));
            break;
      }
   }

   protected void checkExistsPermCateName(object sender, ServerValidateEventArgs e)
   {
      permission_category objADOnline = new permission_category();
      dataADOnline.ad_permission_category_action = new permission_category[1];
      objADOnline.perm_cate_name = txtPermCateName.Text.Trim();
      dataADOnline.ad_permission_category_action[0] = objADOnline;
      _local_xml = servExec.actionExec(misConn, "data_adonline", adPermissionCategoryService, dataADOnline, Constants.SELECT_WHERE_EXISTS_NAME);
      dataADOnline = (data_adonline)_funcTool.convertXmlToObject(typeof(data_adonline), _local_xml);
      if (dataADOnline.return_code.ToString() == Constants.VALID_FALSE)
      {
         e.IsValid = false;
      }
      else
      {
         e.IsValid = true;
      }
   }
   #endregion Custom Functions
}
