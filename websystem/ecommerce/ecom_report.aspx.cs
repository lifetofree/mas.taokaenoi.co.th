using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using MySql.Data.MySqlClient;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

public partial class websystem_ecommerce_ecom_report : System.Web.UI.Page {
    #region initial function/data
    function_tool _funcTool = new function_tool ();

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;
    #endregion initial function/data

    protected void Page_Init (object sender, EventArgs e) {
        _emp_idx = _funcTool.convertToInt (Session["emp_idx"].ToString ());
    }

    protected void Page_Load (object sender, EventArgs e) {
        if (!IsPostBack) {
            initPage ();
        }

        setTrigger ();
    }

    #region event command

    protected void btnCommand (object sender, CommandEventArgs e) {
        string cmdName = e.CommandName.ToString ();
        string cmdArg = e.CommandArgument.ToString ();

        switch (cmdName) {
            case "cmdSearch":
                string _dateStart = tbDateStart.Text.Trim () + " 00:00:00";
                string _dateEnd = tbDateEnd.Text.Trim () + " 23:59:59";

                MySqlConnection dbConn = new MySqlConnection ("Persist Security Info=False;server=172.16.11.28;database=shoponline_tkn;uid=shoptkn;password=$F3PdE");

                MySqlCommand cmd = dbConn.CreateCommand ();
                cmd.CommandText = "select cr.code as pro_code  , r.name as pro_name,o.date_add,o.reference,concat(c.firstname,' ',c.lastname)as customername ,o.total_paid from  st_orders  o left outer join st_order_cart_rule  r on o.id_order = r.id_order left outer join st_customer c on o.id_customer = c.id_customer left outer join st_cart_rule cr on r.id_cart_rule = cr.id_cart_rule where  o.current_state in (2,4,5) and o.date_add  between '" + _dateStart + "' and '" + _dateEnd + "' order by o.date_add ,r.name";

                MySqlDataAdapter da = new MySqlDataAdapter (cmd);
                DataTable dt = new DataTable ();
                da.Fill (dt);
                gvReport.DataSource = dt;
                gvReport.DataBind ();

                ViewState["ReportListExport"] = dt;
                break;
            case "cmdReset":
                tbDateStart.Text = String.Empty;
                tbDateEnd.Text = String.Empty;
                _funcTool.setGvData (gvReport, null);
                break;
            case "cmdExport":
                DataTable _tempTable = new DataTable ();
                DataTable tableReport = new DataTable ();
                int _count_row = 0;
                int _temp_sum = 0;

                _tempTable = (DataTable) ViewState["ReportListExport"];

                if (_tempTable == null) {
                    ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('ไม่พบข้อมูลที่ต้องการ Export ค่ะ');", true);
                    return;
                }

                tableReport.Columns.Add ("รหัสโปรโมชั่น", typeof (String));
                tableReport.Columns.Add ("โปรโมชั่น", typeof (String));
                tableReport.Columns.Add ("วันที่ซื้อ", typeof (String));
                tableReport.Columns.Add ("เลขที่เอกสาร", typeof (String));
                tableReport.Columns.Add ("ชื่อลูกค้า", typeof (String));
                tableReport.Columns.Add ("มูลค่า", typeof (String));

                foreach (DataRow temp_row in _tempTable.Rows) {
                    DataRow add_row = tableReport.NewRow ();

                    add_row[0] = temp_row["pro_code"];
                    add_row[1] = temp_row["pro_name"];
                    add_row[2] = temp_row["date_add"];
                    add_row[3] = temp_row["reference"];
                    add_row[4] = temp_row["customername"];
                    add_row[5] = String.Format("{0:N}",temp_row["total_paid"]);

                    tableReport.Rows.InsertAt(add_row, _count_row++);
                }

                WriteExcelWithNPOI (tableReport, "xls", "report");
                break;
        }
    }
    #endregion event command

    #region reuse
    protected void initPage () {
        clearSession ();
        clearViewState ();

        setActiveTab ("viewList", 0);

        hlSetTotop.Focus ();
    }

    protected void clearSession () {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState () {
        ViewState["ReportListExport"] = null;
    }

    protected void setTrigger () {
        // trigger
        linkBtnTrigger (lbSearch);
        linkBtnTrigger (lbReset);
        linkBtnTrigger (lbExport);
    }

    protected void setActiveTab (string activeTab, int doc_idx) {
        setActiveView (activeTab, doc_idx);
        // setActiveTabBar(activeTab);

        switch (activeTab) {
            case "viewList":
                break;
        }
    }

    protected void setActiveView (string activeTab, int doc_idx) {
        mvSystem.SetActiveView ((View) mvSystem.FindControl (activeTab));
    }

    protected void linkBtnTrigger (LinkButton linkBtnID) {
        UpdatePanel updatePanel = Page.Master.FindControl ("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger ();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add (triggerLinkBtn);
    }

    protected void WriteExcelWithNPOI (DataTable dt, String extension, String fileName) {
        IWorkbook workbook;
        if (extension == "xlsx") {
            workbook = new XSSFWorkbook ();
        } else if (extension == "xls") {
            workbook = new HSSFWorkbook ();
        } else {
            throw new Exception ("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet ("Sheet 1");
        IRow row1 = sheet1.CreateRow (0);
        for (int j = 0; j < dt.Columns.Count; j++) {
            ICell cell = row1.CreateCell (j);
            String columnName = dt.Columns[j].ToString ();
            cell.SetCellValue (columnName);
        }
        for (int i = 0; i < dt.Rows.Count; i++) {
            IRow row = sheet1.CreateRow (i + 1);
            for (int j = 0; j < dt.Columns.Count; j++) {
                ICell cell = row.CreateCell (j);
                String columnName = dt.Columns[j].ToString ();
                cell.SetCellValue (dt.Rows[i][columnName].ToString ());
            }
        }
        using (var exportData = new MemoryStream ()) {
            Response.Clear ();
            workbook.Write (exportData);
            if (extension == "xlsx") {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader ("Content-Disposition", string.Format ("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite (exportData.ToArray ());
            } else if (extension == "xls") {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader ("Content-Disposition", string.Format ("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite (exportData.GetBuffer ());
            }
            Response.End ();
        }
    }
    #endregion reuse
}