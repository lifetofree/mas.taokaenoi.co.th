﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_elearning_master_el_el_m0_training_location : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_elearning _data_elearning = new data_elearning();
    service_execute _serviceExecute = new service_execute();

    string _localXml = String.Empty;
    string _localString = String.Empty;
    string _localJson = String.Empty;
    int _actionType = 0;
    int _emp_idx = 0;
    int _default_int = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlget_training_location = _serviceUrl + ConfigurationManager.AppSettings["urlget_training_location"];
    //static string _urlGetCostcenterOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetCostcenterOld"];
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            ViewState["EmpIDX"] = Session["emp_idx"].ToString();
            ViewState["ddl_cost_center_search"] = 0;
            SelectMasterList();
            //Select_Coscenter(ddl_cost_center_search);
        }
    }
    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Insert_Status()
    {
        _data_elearning.m0_training_location_action = new m0_training_location[1];
        m0_training_location dtelearning = new m0_training_location();

        dtelearning.location_type = int.Parse(ddllocation_type.SelectedValue);
        dtelearning.location_name = txtlocation_name.Text;
        dtelearning.location_address = txtlocation_address.Text;
        dtelearning.location_create_idx = int.Parse(ViewState["EmpIDX"].ToString());
        dtelearning.type_select_location = 1; //insert

        _data_elearning.m0_training_location_action[0] = dtelearning;
        //text.Text = dtemployee.ToString();
        _data_elearning = callServiceElearning(urlget_training_location, _data_elearning);
    }

    protected void SelectMasterList()
    {
        _data_elearning.m0_training_location_action = new m0_training_location[1];
        m0_training_location dtelearning = new m0_training_location();

        //dtelearning.cost_idx = int.Parse(ViewState["ddl_cost_center_search"].ToString()); //select
        dtelearning.type_select_location = 2; //select

        _data_elearning.m0_training_location_action[0] = dtelearning;

        _data_elearning = callServiceElearning(urlget_training_location, _data_elearning);
        //text.Text = _dataEmployee.ToString();
        setGridData(GvMaster, _data_elearning.m0_training_location_action);
    }

    protected data_employee callService(string _cmdUrl, data_employee _dtmaster)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmaster);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtmaster = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtmaster;
    }

    protected void Update_Master_List()
    {
        _data_elearning.m0_training_location_action = new m0_training_location[1];
        m0_training_location dtelearning = new m0_training_location();

        dtelearning.LOIDX = int.Parse(ViewState["LOIDX_update"].ToString());
        dtelearning.location_type = int.Parse(ViewState["ddllocation_type_edit"].ToString());
        dtelearning.location_name = ViewState["txtlocation_name_edit"].ToString();
        dtelearning.location_address = ViewState["txtlocation_address_edit"].ToString();

        dtelearning.location_create_idx = int.Parse(ViewState["EmpIDX"].ToString());
        dtelearning.type_select_location = 3; //update

        _data_elearning.m0_training_location_action[0] = dtelearning;
        //text.Text = dtemployee.ToString();
        _data_elearning = callServiceElearning(urlget_training_location, _data_elearning);
    }

    protected void Delete_Master_List()
    {
        _data_elearning.m0_training_location_action = new m0_training_location[1];
        m0_training_location dtelearning = new m0_training_location();

        dtelearning.LOIDX = int.Parse(ViewState["LOIDX_delete"].ToString());
        dtelearning.type_select_location = 4; //select

        _data_elearning.m0_training_location_action[0] = dtelearning;

        _data_elearning = callServiceElearning(urlget_training_location, _data_elearning);
        //text.Text = _dataEmployee.ToString();
        setGridData(GvMaster, _data_elearning.m0_training_location_action);
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);
        return _dataEmployee;
    }

    protected data_elearning callServiceElearning(string _cmdUrl, data_elearning _data_elearning)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_elearning);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _data_elearning = (data_elearning)_funcTool.convertJsonToObject(typeof(data_elearning), _localJson);
        return _data_elearning;
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        var ddllocation_type_edit = (DropDownList)e.Row.FindControl("ddllocation_type_edit");
                        var lbllocation_type_edit = (Label)e.Row.FindControl("lbllocation_type_edit");

                        //Select_Coscenter(ddlcost_center_edit);
                        ddllocation_type_edit.SelectedValue = lbllocation_type_edit.Text;

                    }

                }

                break;
        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();
                break;
        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int LOIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var ddllocation_type_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddllocation_type_edit");
                var txtlocation_name_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtlocation_name_edit");
                var txtlocation_address_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtlocation_address_edit");
                
                //var ddStatus_update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatus_update");

                GvMaster.EditIndex = -1;

                ViewState["LOIDX_update"] = LOIDX;
                ViewState["ddllocation_type_edit"] = ddllocation_type_edit.SelectedValue;
                ViewState["txtlocation_name_edit"] = txtlocation_name_edit.Text;
                ViewState["txtlocation_address_edit"] = txtlocation_address_edit.Text;
                //ViewState["ddStatus_update"] = ddStatus_update.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion


    #endregion


    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAddHolder":
                btnaddholder.Visible = false;
                Panel_Add.Visible = true;
                //Select_Coscenter(ddlcost_center);
                break;

            case "btnCancel":
                btnaddholder.Visible = true;
                Panel_Add.Visible = false;
                break;

            case "btnAdd":
                Insert_Status();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "CmdDel":
                int LOIDX = int.Parse(cmdArg);
                ViewState["LOIDX_delete"] = LOIDX;
                Delete_Master_List();
                SelectMasterList();
                break;

            case "CmdSearch":
                //ViewState["ddl_cost_center_search"] = ddl_cost_center_search.SelectedValue;
                SelectMasterList();
                break;
        }

    }
    #endregion
}