using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.IO;

public partial class websystem_visitors_security_main : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_visitors _data_visitors = new data_visitors();
    data_employee _data_employee = new data_employee();
    data_times _data_times = new data_times();
    data_geo _data_geo = new data_geo();

    List<tm_group_detail> _data_group_selected = new List<tm_group_detail>();
    List<vm_visitors_doc_detail_u1> _data_visitor_selected = new List<vm_visitors_doc_detail_u1>();

    int _emp_idx = 0;
    int _org_idx = 0;
    int _temp_idx = 0;
    int _se_idx = 0;
    int _actor_idx = 0;
    int _temp_photo_idx = 0;
    string _temp_data = "";
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;
    string dirName = "";

    static string imgPath = ConfigurationSettings.AppSettings["path_file_visitors"];

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetViewEmployeeListSmall = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewEmployeeListSmall"];

    static string _urlExGetGeoList = _serviceUrl + ConfigurationManager.AppSettings["urlExGetGeoList"];

    static string _urlTmGetGroup = _serviceUrl + ConfigurationManager.AppSettings["urlTmGetGroup"];
    static string _urlVmGetVisitorType = _serviceUrl + ConfigurationManager.AppSettings["urlVmGetVisitorType"];
    static string _urlVmSetTransaction = _serviceUrl + ConfigurationManager.AppSettings["urlVmSetTransaction"];
    static string _urlVmGetTransaction = _serviceUrl + ConfigurationManager.AppSettings["urlVmGetTransaction"];
    static string _urlVmSetVehicleSpecial = _serviceUrl + ConfigurationManager.AppSettings["urlVmSetVehicleSpecial"];
    static string _urlVmGetVehicleSpecial = _serviceUrl + ConfigurationManager.AppSettings["urlVmGetVehicleSpecial"];

    static string _urlsend_email_visitors = _serviceUrl + ConfigurationManager.AppSettings["urlsend_email_visitors"];

    static string _urlVmGetVisitEmployeeTop = _serviceUrl + ConfigurationManager.AppSettings["urlVmGetVisitEmployeeTop"];

    #endregion initial function/data

    private void Page_Init(object sender, EventArgs e)
    {
        // if (Session["se_idx"] != null)
        // {
        //     int _tempInt = _funcTool.convertToInt(Session["se_idx"].ToString());
        //     if (_tempInt > 0)
        //     {
        //         ViewState["se_idx"] = Session["se_idx"];
        //     }
        //     else
        //     {
        //         Response.Redirect(ResolveUrl("~/vm-security-login"));
        //     }
        // }
        // else
        // {
        //     Response.Redirect(ResolveUrl("~/vm-security-login"));
        // }
        ViewState["se_idx"] = "1";
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        // _emp_idx = 27694; // fix emp_idx for walk-in :: create new employee 80000001 DEV
        _emp_idx = 32202; // fix emp_idx for walk-in :: create new employee 90000282 PRD
        _org_idx = 1; // TKN
        _actor_idx = 5; // security
        _se_idx = _funcTool.convertToInt(ViewState["se_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
        }
    }

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "navList":
                setActiveTab("viewList", 0);
                break;
            case "navCreate":
                setActiveTab("viewCreate", 0);
                break;
            case "navApprove":
                setActiveTab("viewApprove", 0);
                break;
        }
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        HiddenField hfU0Idx = (HiddenField)fvDocCreate.FindControl("hfU0Idx");
        HiddenField hfNodeIdx = (HiddenField)fvDocCreate.FindControl("hfNodeIdx");
        HiddenField hfActorIdx = (HiddenField)fvDocCreate.FindControl("hfActorIdx");
        HiddenField hfFlagLoading = (HiddenField)fvDocCreate.FindControl("hfFlagLoading");
        HiddenField hfFlagSafety = (HiddenField)fvDocCreate.FindControl("hfFlagSafety");

        GridView gvVisitorList = (GridView)fvDocCreate.FindControl("gvVisitorList");
        DropDownList ddlIdentifyType = (DropDownList)fvDocCreate.FindControl("ddlIdentifyType");
        TextBox tbVisitCardId = (TextBox)fvDocCreate.FindControl("tbVisitCardId");
        TextBox tbVisitPhoneNo = (TextBox)fvDocCreate.FindControl("tbVisitPhoneNo");
        TextBox tbVisitPrefix = (TextBox)fvDocCreate.FindControl("tbVisitPrefix");
        TextBox tbVisitFirstname = (TextBox)fvDocCreate.FindControl("tbVisitFirstname");
        TextBox tbVisitLastname = (TextBox)fvDocCreate.FindControl("tbVisitLastname");
        TextBox tbVisitPrefixEn = (TextBox)fvDocCreate.FindControl("tbVisitPrefixEn");
        TextBox tbVisitFirstnameEn = (TextBox)fvDocCreate.FindControl("tbVisitFirstnameEn");
        TextBox tbVisitLastnameEn = (TextBox)fvDocCreate.FindControl("tbVisitLastnameEn");
        TextBox tbVisitHouseNo = (TextBox)fvDocCreate.FindControl("tbVisitHouseNo");
        TextBox tbVisitVillageNo = (TextBox)fvDocCreate.FindControl("tbVisitVillageNo");
        TextBox tbVisitLane = (TextBox)fvDocCreate.FindControl("tbVisitLane");
        TextBox tbVisitRoad = (TextBox)fvDocCreate.FindControl("tbVisitRoad");
        TextBox tbVisitDistrict = (TextBox)fvDocCreate.FindControl("tbVisitDistrict");
        TextBox tbVisitAmphure = (TextBox)fvDocCreate.FindControl("tbVisitAmphure");
        TextBox tbVisitProvince = (TextBox)fvDocCreate.FindControl("tbVisitProvince");
        TextBox tbVisitCardNo = (TextBox)fvDocCreate.FindControl("tbVisitCardNo");

        Panel pnPersonalData = (Panel)fvDocCreate.FindControl("pnPersonalData");
        LinkButton lbUpdatePersonal = (LinkButton)fvDocCreate.FindControl("lbUpdatePersonal");

        switch (cmdName)
        {
            case "cmdItemInsert":
                if (ViewState["VisitorSelected"] != null)
                {
                    _data_visitor_selected = (List<vm_visitors_doc_detail_u1>)ViewState["VisitorSelected"];
                    // var v_dt = _data_visitor_selected.Where(l => l.visit_identify_no == tbVisitCardId.Text.Trim()).FirstOrDefault();
                    // if (v_dt != null)
                    // {
                    //     ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูล "+ ddlIdentifyType.Items[_funcTool.convertToInt(ddlIdentifyType.SelectedValue)].Text + " " + tbVisitCardId.Text.Trim() + " แล้ว .!');", true);
                    //     return;
                    // }
                    // else
                    // {
                        vm_visitors_doc_detail_u1 _visitor_list = new vm_visitors_doc_detail_u1();
                        _visitor_list.visit_identify_idx = _funcTool.convertToInt(ddlIdentifyType.SelectedValue);
                        _visitor_list.visit_identify_no = tbVisitCardId.Text.Trim();
                        _visitor_list.visit_phone_no = tbVisitPhoneNo.Text.Trim();
                        _visitor_list.visit_prefix = tbVisitPrefix.Text.Trim();
                        _visitor_list.visit_firstname = tbVisitFirstname.Text.Trim();
                        _visitor_list.visit_lastname = tbVisitLastname.Text.Trim();
                        _visitor_list.visit_prefix_en = tbVisitPrefixEn.Text.Trim();
                        _visitor_list.visit_firstname_en = tbVisitFirstnameEn.Text.Trim();
                        _visitor_list.visit_lastname_en = tbVisitLastnameEn.Text.Trim();
                        _visitor_list.visit_house_no = tbVisitHouseNo.Text.Trim();
                        _visitor_list.visit_village_no = tbVisitVillageNo.Text.Trim();
                        _visitor_list.visit_lane = tbVisitLane.Text.Trim();
                        _visitor_list.visit_road = tbVisitRoad.Text.Trim();
                        _visitor_list.visit_district = tbVisitDistrict.Text.Trim();
                        _visitor_list.visit_amphure = tbVisitAmphure.Text.Trim();
                        _visitor_list.visit_province = tbVisitProvince.Text.Trim();
                        _visitor_list.visit_card_no = tbVisitCardNo.Text.Trim();
                        _data_visitor_selected.Add(_visitor_list);
                    // }
                   
                }
                else
                {
                    vm_visitors_doc_detail_u1 _visitor_list = new vm_visitors_doc_detail_u1();
                    _visitor_list.visit_identify_idx = _funcTool.convertToInt(ddlIdentifyType.SelectedValue);
                    _visitor_list.visit_identify_no = tbVisitCardId.Text.Trim();
                    _visitor_list.visit_phone_no = tbVisitPhoneNo.Text.Trim();
                    _visitor_list.visit_prefix = tbVisitPrefix.Text.Trim();
                    _visitor_list.visit_firstname = tbVisitFirstname.Text.Trim();
                    _visitor_list.visit_lastname = tbVisitLastname.Text.Trim();
                    _visitor_list.visit_prefix_en = tbVisitPrefixEn.Text.Trim();
                    _visitor_list.visit_firstname_en = tbVisitFirstnameEn.Text.Trim();
                    _visitor_list.visit_lastname_en = tbVisitLastnameEn.Text.Trim();
                    _visitor_list.visit_house_no = tbVisitHouseNo.Text.Trim();
                    _visitor_list.visit_village_no = tbVisitVillageNo.Text.Trim();
                    _visitor_list.visit_lane = tbVisitLane.Text.Trim();
                    _visitor_list.visit_road = tbVisitRoad.Text.Trim();
                    _visitor_list.visit_district = tbVisitDistrict.Text.Trim();
                    _visitor_list.visit_amphure = tbVisitAmphure.Text.Trim();
                    _visitor_list.visit_province = tbVisitProvince.Text.Trim();
                    _visitor_list.visit_card_no = tbVisitCardNo.Text.Trim();
                    _data_visitor_selected.Add(_visitor_list);
                }

                // save photo
                string y = hiddenid.Value.Replace("data:image/png;base64,", "");
                if (y != "")
                {
                    dirName = "RJN-0";
                    //set directory
                    setDir(imgPath + dirName);
                    // Convert Base64 String to byte[]
                    byte[] imageBytes = Convert.FromBase64String(y);
                    MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

                    // Convert byte[] to Image
                    ms.Write(imageBytes, 0, imageBytes.Length);
                    System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                    image.Save(Server.MapPath(imgPath + dirName + "/" + "visitor-" + tbVisitCardId.Text.Trim() + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png"), System.Drawing.Imaging.ImageFormat.Png);
                }

                // clear data
                tbVisitCardId.Text = String.Empty;
                tbVisitPhoneNo.Text = String.Empty;
                tbVisitPrefix.Text = "-";
                tbVisitFirstname.Text = String.Empty;
                tbVisitLastname.Text = String.Empty;
                tbVisitPrefixEn.Text = "-";
                tbVisitFirstnameEn.Text = "-";
                tbVisitLastnameEn.Text = "-";
                tbVisitHouseNo.Text = String.Empty;
                tbVisitVillageNo.Text = String.Empty;
                tbVisitLane.Text = String.Empty;
                tbVisitRoad.Text = String.Empty;
                tbVisitDistrict.Text = String.Empty;
                tbVisitAmphure.Text = String.Empty;
                tbVisitProvince.Text = String.Empty;
                tbVisitCardNo.Text = String.Empty;

                ViewState["VisitorSelected"] = _data_visitor_selected;
                setGridData(gvVisitorList, ViewState["VisitorSelected"]);
                break;
            case "cmdItemDelete":
                GridView gvVisitorList1 = (GridView)fvDocCreate.FindControl("gvVisitorList");
                if (ViewState["VisitorSelected"] != null)
                {
                    _temp_data = cmdArg;
                    _data_visitor_selected = (List<vm_visitors_doc_detail_u1>)ViewState["VisitorSelected"];
                    _data_visitor_selected.RemoveAll(l => l.visit_identify_no == _temp_data);
                }
                ViewState["VisitorSelected"] = _data_visitor_selected;
                setGridData(gvVisitorList1, ViewState["VisitorSelected"]);
                break;
            case "cmdCancel":
                initPage();
                break;
            case "cmdSave":
                TextBox tbVisitDate = (TextBox)fvDocCreate.FindControl("tbVisitDate");
                TextBox tbVisitTitle = (TextBox)fvDocCreate.FindControl("tbVisitTitle");
                TextBox tbVisitDetail = (TextBox)fvDocCreate.FindControl("tbVisitDetail");
                CheckBox cbFlagSafety = (CheckBox)fvDocCreate.FindControl("cbFlagSafety");
                DropDownList ddlEmpList = (DropDownList)fvDocCreate.FindControl("ddlEmpList");
                DropDownList ddlVisitorType = (DropDownList)fvDocCreate.FindControl("ddlVisitorType");
                TextBox tbVehiclePlateNo = (TextBox)fvDocCreate.FindControl("tbVehiclePlateNo");
                DropDownList ddlProvince = (DropDownList)fvDocCreate.FindControl("ddlProvince");
                TextBox tbSafetyComment = (TextBox)fvDocCreate.FindControl("tbSafetyComment");
                _data_group_selected = (List<tm_group_detail>)ViewState["GroupSelected"];
                _data_visitor_selected = (List<vm_visitors_doc_detail_u1>)ViewState["VisitorSelected"];

                // document : vm_visitors_doc_detail_u0
                vm_visitors_doc_detail_u0 _u0_doc = new vm_visitors_doc_detail_u0();
                _u0_doc.u0_idx = _funcTool.convertToInt(hfU0Idx.Value);
                _u0_doc.emp_idx = _emp_idx;
                _u0_doc.visitor_type_idx = _funcTool.convertToInt(ddlVisitorType.SelectedValue.ToString());
                _u0_doc.visit_date = tbVisitDate.Text.Trim();
                _u0_doc.visit_title = tbVisitTitle.Text.Trim();
                _u0_doc.visit_detail = tbVisitDetail.Text.Trim();
                _u0_doc.visit_emp_idx = _funcTool.convertToInt(ddlEmpList.SelectedValue.ToString());
                // flag_safety
                if(cbFlagSafety.Checked)
                {
                    _u0_doc.flag_safety = 1;
                }
                // safety comment
                if (hfNodeIdx.Value == "6")
                {
                    _u0_doc.safety_comment = tbSafetyComment.Text.Trim();
                }
                // flag_vehicle
                if (tbVehiclePlateNo.Text != String.Empty)
                {
                    _u0_doc.flag_vehicle = 1;
                }
                _u0_doc.flag_walkin = 1;
                _u0_doc.node_idx = _funcTool.convertToInt(hfNodeIdx.Value);
                _u0_doc.actor_idx = _funcTool.convertToInt(hfActorIdx.Value);
                if (hfFlagLoading.Value == "1")
                {
                    _u0_doc.decision_idx = 19;
                }
                else
                {
                    _u0_doc.decision_idx = _funcTool.convertToInt(cmdArg);
                }

                _data_visitors.vm_visitors_doc_list_u0 = new vm_visitors_doc_detail_u0[1];
                _data_visitors.vm_visitors_doc_list_u0[0] = _u0_doc;

                // visitor list : vm_visitors_doc_detail_u1
                int ic = 0;
                if (_data_visitor_selected != null)
                {
                    _data_visitors.vm_visitors_doc_list_u1 = new vm_visitors_doc_detail_u1[_data_visitor_selected.Count];

                   
                    foreach (vm_visitors_doc_detail_u1 _item in _data_visitor_selected)
                    {
                        vm_visitors_doc_detail_u1 _visitor_list = new vm_visitors_doc_detail_u1();
                        _visitor_list.visit_prefix = _item.visit_prefix;
                        _visitor_list.visit_firstname = _item.visit_firstname;
                        _visitor_list.visit_lastname = _item.visit_lastname;
                        _visitor_list.visit_prefix_en = _item.visit_prefix_en;
                        _visitor_list.visit_firstname_en = _item.visit_firstname_en;
                        _visitor_list.visit_lastname_en = _item.visit_lastname_en;
                        _visitor_list.visit_house_no = _item.visit_house_no;
                        _visitor_list.visit_village_no = _item.visit_village_no;
                        _visitor_list.visit_lane = _item.visit_lane;
                        _visitor_list.visit_road = _item.visit_road;
                        _visitor_list.visit_district = _item.visit_district;
                        _visitor_list.visit_amphure = _item.visit_amphure;
                        _visitor_list.visit_province = _item.visit_province;
                        _visitor_list.visit_identify_idx = _item.visit_identify_idx;
                        _visitor_list.visit_identify_no = _item.visit_identify_no;
                        _visitor_list.visit_phone_no = _item.visit_phone_no;
                        _visitor_list.visit_card_no = _item.visit_card_no;

                        _data_visitors.vm_visitors_doc_list_u1[ic] = _visitor_list;
                        ic++;
                    }
                }
                if(ic == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณากรอกผู้มาติดต่อ .!');", true);
                    return;
                }


                // visitor vehicle : vm_visitors_doc_detail_u2
                vm_visitors_doc_detail_u2 _vehicle_list = new vm_visitors_doc_detail_u2();
                _vehicle_list.vehicle_plate_no = tbVehiclePlateNo.Text.Trim();
                _vehicle_list.vehicle_plate_province = _funcTool.convertToInt(ddlProvince.SelectedValue.ToString());

                _data_visitors.vm_visitors_doc_list_u2 = new vm_visitors_doc_detail_u2[1];
                _data_visitors.vm_visitors_doc_list_u2[0] = _vehicle_list;

                // area list : vm_visitors_doc_detail_u3
                if (_data_group_selected != null)
                {
                    _data_visitors.vm_visitors_doc_list_u3 = new vm_visitors_doc_detail_u3[_data_group_selected.Count];

                    int i = 0;
                    foreach (tm_group_detail _item in _data_group_selected)
                    {
                        vm_visitors_doc_detail_u3 _group_list = new vm_visitors_doc_detail_u3();
                        _group_list.group_m0_idx = _item.m0_idx;

                        _data_visitors.vm_visitors_doc_list_u3[i] = _group_list;
                        i++;
                    }
                }

                // debug
                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_visitors));
                _data_visitors = callServicePostVisitors(_urlVmSetTransaction, _data_visitors);
                if (_data_visitors.return_code == 0)
                {

                    try
                    {
                        data_visitors dtvisitors = new data_visitors();
                        vm_visitors_sendemail_detail _sendemail = new vm_visitors_sendemail_detail();
                        dtvisitors.vm_visitors_sendemail_list = new vm_visitors_sendemail_detail[1];
                        _sendemail.u0_idx = _data_visitors.return_idx;
                        _sendemail.node_idx = _data_visitors.return_node_idx;
                        _sendemail.actor_idx = _data_visitors.return_actor_idx;
                        dtvisitors.vm_visitors_sendemail_list[0] = _sendemail;
                        callServicePostVisitors(_urlsend_email_visitors, dtvisitors);
                    }
                    catch { }
                    

                    if (hfFlagSafety != null)
                    {
                        if (hfFlagSafety.Value == "1")
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาติดต่อเจ้าหน้าที่ความปลอดภัยในการทำงาน(จป)')", true);
                        }
                    }

                    _temp_idx = _data_visitors.return_idx;
                    if(hfU0Idx.Value == "0")
                    {
                        // change dir name
                        changeDirName(imgPath, "RJN-0", "RJN-" + _temp_idx.ToString());
                    }
                    
                    initPage();
                    
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('error code " + _data_visitors.return_code + ": " + _data_visitors.return_msg + "')", true);
                }
                break;
            case "cmdEdit":
                setActiveTab("viewCreate", _funcTool.convertToInt(cmdArg));
                break;
            case "cmdSaveSpecial":
                vm_visitor_vehicle_special_detail_m0 _save_car = new vm_visitor_vehicle_special_detail_m0();
                _save_car.m0_idx = _funcTool.convertToInt(cmdArg);
                _save_car.se_idx = _se_idx;
                _data_visitors.vm_visitor_vehicle_special_list_m0 = new vm_visitor_vehicle_special_detail_m0[1];
                _data_visitors.vm_visitor_vehicle_special_list_m0[0] = _save_car;
                _data_visitors = setVehicleSpecialList(_data_visitors);
                setRepeaterData(rptCarList, _data_visitors.vm_visitor_vehicle_special_list_m0);
                break;
            case "cmdSelectPersonalData":
                pnPersonalData.Visible = true;
                if (ViewState["document_detail"] != null)
                {
                    _data_visitors = (data_visitors)ViewState["document_detail"];
                    _temp_idx = _funcTool.convertToInt(cmdArg);
                    lbUpdatePersonal.CommandArgument = _temp_idx.ToString();
                    if (_data_visitors.vm_visitors_doc_list_u1 != null)
                    {
                        ddlIdentifyType.SelectedValue = _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_identify_idx.ToString();
                        tbVisitCardId.Text = _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_identify_no;
                        tbVisitPhoneNo.Text = _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_phone_no;
                        tbVisitPrefix.Text = _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_prefix;
                        tbVisitFirstname.Text = _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_firstname;
                        tbVisitLastname.Text = _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_lastname;
                        tbVisitPrefixEn.Text = _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_prefix_en;
                        tbVisitFirstnameEn.Text = _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_firstname_en;
                        tbVisitLastnameEn.Text = _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_lastname_en;
                        tbVisitHouseNo.Text = _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_house_no;
                        tbVisitVillageNo.Text = _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_village_no;
                        tbVisitLane.Text = _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_lane;
                        tbVisitRoad.Text = _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_road;
                        tbVisitDistrict.Text = _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_district;
                        tbVisitAmphure.Text = _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_amphure;
                        tbVisitProvince.Text = _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_province;
                        tbVisitCardNo.Text = _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_card_no;
                    }
                }
                break;
            case "cmdUpdatePersonalData":
                if (ViewState["document_detail"] != null)
                {
                    _data_visitors = (data_visitors)ViewState["document_detail"];
                    _temp_idx = _funcTool.convertToInt(cmdArg);

                    if (_data_visitors.vm_visitors_doc_list_u1 != null)
                    {
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_identify_idx = _funcTool.convertToInt(ddlIdentifyType.SelectedValue);
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_identify_no = tbVisitCardId.Text.Trim();
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_phone_no = tbVisitPhoneNo.Text.Trim();
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_prefix = tbVisitPrefix.Text.Trim();
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_firstname = tbVisitFirstname.Text.Trim();
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_lastname = tbVisitLastname.Text.Trim();
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_prefix_en = tbVisitPrefixEn.Text.Trim();
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_firstname_en = tbVisitFirstnameEn.Text.Trim();
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_lastname_en = tbVisitLastnameEn.Text.Trim();
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_house_no = tbVisitHouseNo.Text.Trim();
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_village_no = tbVisitVillageNo.Text.Trim();
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_lane = tbVisitLane.Text.Trim();
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_road = tbVisitRoad.Text.Trim();
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_district = tbVisitDistrict.Text.Trim();
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_amphure = tbVisitAmphure.Text.Trim();
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_province = tbVisitProvince.Text.Trim();
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].visit_card_no = tbVisitCardNo.Text.Trim();
                    }

                    // save photo
                    string x = hiddenid.Value.Replace("data:image/png;base64,", "");
                    if (x != "")
                    {
                        dirName = "RJN-" + hfU0Idx.Value;
                        //set directory
                        setDir(imgPath + dirName);
                        // Convert Base64 String to byte[]
                        byte[] imageBytes = Convert.FromBase64String(x);
                        MemoryStream ms = new MemoryStream(imageBytes, 0, imageBytes.Length);

                        // Convert byte[] to Image
                        ms.Write(imageBytes, 0, imageBytes.Length);
                        System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);
                        image.Save(Server.MapPath(imgPath + dirName + "/" + "visitor-" + tbVisitCardId.Text.Trim() + "-" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".png"), System.Drawing.Imaging.ImageFormat.Png);

                        // set flag_photo
                        _data_visitors.vm_visitors_doc_list_u1[_temp_idx].flag_photo = 1;
                    }

                    //setGridData(gvVisitorList, _data_visitors.vm_visitors_doc_list_u1);

                    int i = 0;
                    if (_data_visitors.vm_visitors_doc_list_u1 != null)
                    {
                        foreach (var item in _data_visitors.vm_visitors_doc_list_u1)
                        {
                            _data_visitor_selected.Add(_data_visitors.vm_visitors_doc_list_u1[i]);
                            i++;
                        }
                    }
                    
                    ViewState["VisitorSelected"] = _data_visitor_selected;
                    setGridData(gvVisitorList, ViewState["VisitorSelected"]);
                }
                pnPersonalData.Visible = false;
                break;
            case "cmdSearch":
                getShowDataIndex();
                break;
            case "cmdReset":
                setSearchDefault();
                break;
        }
    }

    protected void cbCheckedChanged(object sender, EventArgs e)
    {
        var cb = (CheckBox)sender;
        switch (cb.ID)
        {
            case "cbGroupSelected":
                string[] _val_in = cb.Text.Split('|');
                _temp_idx = _funcTool.convertToInt(_val_in[0]);
                if (ViewState["GroupSelected"] != null)
                {
                    _data_group_selected = (List<tm_group_detail>)ViewState["GroupSelected"];
                    if (cb.Checked)
                    {
                        var check_selected = _data_group_selected.Where(d =>
                        d.m0_idx == _temp_idx).FirstOrDefault();
                        if (check_selected == null)
                        {
                            tm_group_detail _group_list = new tm_group_detail();
                            _group_list.m0_idx = _temp_idx;
                            _group_list.group_name = _val_in[1];
                            _data_group_selected.Add(_group_list);
                        }
                    }
                    else
                    {
                        _data_group_selected.RemoveAll(l => l.m0_idx == _temp_idx);
                    }
                }
                else
                {
                    if (cb.Checked)
                    {
                        tm_group_detail _group_list = new tm_group_detail();
                        _group_list.m0_idx = _temp_idx;
                        _group_list.group_name = _val_in[1];
                        _data_group_selected.Add(_group_list);
                    }
                }

                ViewState["GroupSelected"] = _data_group_selected;
                break;
        }
    }
    #endregion event command

    #region RowDatabound
    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var gvName = (GridView)sender;

        switch (gvName.ID)
        {
            case "gvDocList":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField hfFlagSafety = (HiddenField)e.Row.FindControl("hfFlagSafety");
                    Literal litFlagSafety = (Literal)e.Row.FindControl("litFlagSafety");
                    if (hfFlagSafety.Value == "1")
                    {
                        litFlagSafety.Visible = true;
                    }
                }
                break;
            // case "gvGroupList":
            //     if (e.Row.RowType == DataControlRowType.DataRow)
            //     {
            //         CheckBox cbGroupSelected = (CheckBox)e.Row.FindControl("cbGroupSelected");
            //         HiddenField hfM0Idx = (HiddenField)e.Row.FindControl("hfM0Idx");
            //         _temp_idx = _funcTool.convertToInt(hfM0Idx.Value);
            //         if (ViewState["document_detail"] != null)
            //         {
            //             _data_visitors = (data_visitors)ViewState["document_detail"];
            //             if (_data_visitors.vm_visitors_doc_list_u3 != null)
            //             {
            //                 var check_selected = _data_visitors.vm_visitors_doc_list_u3.Where(d =>
            //                 d.group_m0_idx == _temp_idx).FirstOrDefault();
            //                 if (check_selected != null)
            //                 {
            //                     cbGroupSelected.Checked = true;
            //                 }
            //             }
            //         }
            //     }
            //     break;
            case "gvVisitorList":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HiddenField hfNodeIdx = (HiddenField)fvDocCreate.FindControl("hfNodeIdx");
                    if (hfNodeIdx.Value == "4")
                    {
                        // LinkButton lbCamera = (LinkButton)e.Row.FindControl("lbCamera");
                        LinkButton lbIdCard = (LinkButton)e.Row.FindControl("lbIdCard");
                        // lbCamera.Visible = true;
                        lbIdCard.Visible = true;
                    }

                    HiddenField hfFlagPhoto = (HiddenField)e.Row.FindControl("hfFlagPhoto");
                    HiddenField hfIdentifyNo = (HiddenField)e.Row.FindControl("hfIdentifyNo");
                    HiddenField hfVisitCardNo = (HiddenField)e.Row.FindControl("hfVisitCardNo");
                    HiddenField hfFirstname = (HiddenField)e.Row.FindControl("hfFirstname");
                    HiddenField hfLastname = (HiddenField)e.Row.FindControl("hfLastname");

                    if (hfFlagPhoto.Value == "1" && hfIdentifyNo.Value != "" && hfVisitCardNo.Value == "" && hfFirstname.Value == "" && hfLastname.Value == "")
                    {
                        e.Row.CssClass = "bg-success";
                    }
                }
                break;
        }
    }
    #endregion RowDatabound

    #region databound
    protected void fvDataBound(object sender, EventArgs e)
    {
        var fvName = (FormView)sender;
        switch (fvName.ID)
        {
            case "fvEmpProfile":
                break;
            case "fvDocCreate":
                DropDownList ddlDeptList = (DropDownList)fvName.FindControl("ddlDeptList");
                DropDownList ddlEmpList = (DropDownList)fvName.FindControl("ddlEmpList");
                DropDownList ddlEmpListTop = (DropDownList)fvName.FindControl("ddlEmpListTop");
                DropDownList ddlVisitorType = (DropDownList)fvName.FindControl("ddlVisitorType");
                DropDownList ddlProvince = (DropDownList)fvName.FindControl("ddlProvince");
                // GridView gvGroupList = (GridView)fvName.FindControl("gvGroupList");
                GridView gvVisitorList = (GridView)fvName.FindControl("gvVisitorList");
                HiddenField hfNodeIdx = (HiddenField)fvName.FindControl("hfNodeIdx");
                HiddenField hfActorIdx = (HiddenField)fvName.FindControl("hfActorIdx");

                CheckBox cbFlagSafety = (CheckBox)fvDocCreate.FindControl("cbFlagSafety");

                // check formview mode
                if (fvName.CurrentMode == FormViewMode.Insert)
                {
                    // dept list
                    // getDepartmentList(ddlDeptList, _org_idx);
                    // ddlEmpList
                    search_key_employee _search_key = new search_key_employee();
                    // _search_key.s_rdept_idx = ddlDeptList.SelectedValue.ToString();
                    _search_key.s_org_idx = "1";
                    _search_key.s_emp_type = "2";
                    _search_key.s_emp_status = "1";
                    _data_employee.search_key_emp_list = new search_key_employee[1];
                    _data_employee.search_key_emp_list[0] = _search_key;
                    _data_employee = getViewEmployeeList(_data_employee);

                    setDdlData(ddlEmpList, _data_employee.employee_list_small, "emp_name_th", "emp_idx");
                    ddlEmpList.Items.Insert(0, new ListItem("--- ชื่อผู้ที่ต้องการเข้าพบ ---", "-1"));

                    _data_employee = new data_employee();
                    _data_employee = getVisitEmployeeTop(_data_employee);
                    setDdlData(ddlEmpListTop, _data_employee.employee_list_small, "emp_name_th", "emp_idx");
                    ddlEmpListTop.Items.Insert(0, new ListItem("--- ชื่อผู้ที่ต้องการพบประจำ ---", "-1"));

                    // log history
                    setRepeaterData(rptLog, null);
                }

                if (fvName.CurrentMode == FormViewMode.Insert || fvName.CurrentMode == FormViewMode.Edit)
                {
                    // ddlVisitorType
                    search_visitor_type_detail _search_type = new search_visitor_type_detail();
                    _search_type.s_type_status = "1";
                    _data_visitors.search_visitor_type_list = new search_visitor_type_detail[1];
                    _data_visitors.search_visitor_type_list[0] = _search_type;
                    _data_visitors = getVisitorTypeList(_data_visitors);

                    // debug
                    // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_visitors));
                    setDdlData(ddlVisitorType, _data_visitors.vm_visitors_type_list_m0, "visitor_type_name", "m0_idx");
                    ddlVisitorType.Items.Insert(0, new ListItem("--- ประเภทผู้มาติดต่อ ---", "-1"));

                    // ddlProvince
                    geo_detail _province_select = new geo_detail();
                    _province_select.geography_idx = -1;
                    _data_geo.geo_list = new geo_detail[1];
                    _data_geo.geo_list[0] = _province_select;
                    _data_geo = getGeoList(_data_geo);

                    setDdlData(ddlProvince, _data_geo.geo_list, "name_th", "idx");
                    ddlProvince.Items.Insert(0, new ListItem("--- จังหวัด ---", "-1"));

                    // gvGroupList
                    // _data_times.search_group_list = new search_group_detail[1];
                    // search_group_detail _groupList = new search_group_detail();
                    // _groupList.s_group_status = "1";
                    // _data_times.search_group_list[0] = _groupList;
                    // _data_times = getGroupList(_data_times);
                    // setGridData(gvGroupList, _data_times.tm_group_list);
                }

                if (fvName.CurrentMode == FormViewMode.Edit)
                {
                    if (ViewState["document_detail"] != null)
                    {
                        _data_visitors = (data_visitors)ViewState["document_detail"];
                        ddlProvince.Items.Insert(0, new ListItem("--- จังหวัด ---", "-1"));
                        ddlEmpList.Items.Insert(0, new ListItem(_data_visitors.vm_visitors_doc_list_u0[0].visit_emp_name_th, _data_visitors.vm_visitors_doc_list_u0[0].visit_emp_idx.ToString()));

                        // visitor type
                        HiddenField hfVisitorTypeIdx = (HiddenField)fvName.FindControl("hfVisitorTypeIdx");
                        ddlVisitorType.SelectedValue = hfVisitorTypeIdx.Value.ToString();

                        // visitor list
                        setGridData(gvVisitorList, _data_visitors.vm_visitors_doc_list_u1);

                        if (_data_visitors.vm_visitors_doc_list_u2 != null)
                        {
                            TextBox tbVehiclePlateNo = (TextBox)fvName.FindControl("tbVehiclePlateNo");
                            tbVehiclePlateNo.Text = _data_visitors.vm_visitors_doc_list_u2[0].vehicle_plate_no;
                            ddlProvince.SelectedValue = _data_visitors.vm_visitors_doc_list_u2[0].vehicle_plate_province.ToString();
                        }

                        // flag safety
                        HiddenField hfFlagSafety = (HiddenField)fvName.FindControl("hfFlagSafety");
                        int _flag_safety = _funcTool.convertToInt(hfFlagSafety.Value);

                        Panel pnNode2 = (Panel)fvName.FindControl("pnNode2");
                        Panel pnNode3 = (Panel)fvName.FindControl("pnNode3");
                        Panel pnNode4 = (Panel)fvName.FindControl("pnNode4");
                        Panel pnNode5 = (Panel)fvName.FindControl("pnNode5");
                        Panel pnNode6 = (Panel)fvName.FindControl("pnNode6");
                        Panel pnNode6Comment = (Panel)fvName.FindControl("pnNode6Comment");
                        Panel pnNode7 = (Panel)fvName.FindControl("pnNode7");
                        Panel pnNode8 = (Panel)fvName.FindControl("pnNode8");
                        Panel pnNode9 = (Panel)fvName.FindControl("pnNode9");

                        LinkButton lbN5Safety = (LinkButton)fvName.FindControl("lbN5Safety");
                        TextBox tbSafetyComment = (TextBox)fvName.FindControl("tbSafetyComment");

                        switch (hfNodeIdx.Value)
                        {
                            // case "2":
                            //     pnNode2.Visible = true;
                            //     break;
                            // case "3":
                            //     pnNode3.Visible = true;
                            //     break;
                            case "4":
                                pnNode4.Visible = true;
                                break;
                                // case "5":
                                //     pnNode5.Visible = true;
                                //     switch(_flag_safety)
                                //     {
                                //         case 0:
                                //             lbN5Safety.Visible = false;
                                //             break;
                                //         case 1:
                                //             pnNode6Comment.Visible = true;
                                //             break;
                                //     }
                                //     break;
                                // case "6":
                                //     pnNode6.Visible = true;
                                //     pnNode6Comment.Visible = true;
                                //     tbSafetyComment.Enabled = true;
                                //     break;
                                // case "7":
                                //     pnNode7.Visible = true;
                                //     if(_flag_safety == 1)
                                //     {
                                //         pnNode6Comment.Visible = true;
                                //     }
                                //     break;
                                // case "8":
                                //     pnNode8.Visible = true;
                                //     if(_flag_safety == 1)
                                //     {
                                //         pnNode6Comment.Visible = true;
                                //     }
                                //     break;
                                case "9":
                                    pnNode9.Visible = true;
                                    if(_flag_safety == 1)
                                    {
                                        pnNode6Comment.Visible = true;
                                    }
                                    break;
                        }

                        // log history
                        setRepeaterData(rptLog, _data_visitors.vm_visitors_doc_list_l0);

                        // flag_safety
                        if(hfFlagSafety.Value == "1")
                        {
                            cbFlagSafety.Checked = true;
                        }
                    }
                }
                break;
        }
    }

    protected void rptDataBound(object sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)sender;
        switch (rptName.ID)
        {
            case "rptCarList" :
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem) 
                {
                    HiddenField hfVehicleState = (HiddenField)e.Item.FindControl("hfVehicleState");
                    HiddenField hfVehiclePlateProvince = (HiddenField)e.Item.FindControl("hfVehiclePlateProvince");
                    LinkButton lbSpecialSave = (LinkButton)e.Item.FindControl("lbSpecialSave");
                    switch(hfVehiclePlateProvince.Value)
                    {
                        case "9999":
                            switch(hfVehicleState.Value)
                            {
                                case "0":
                                    lbSpecialSave.Attributes.Add("class", "btn btn-lg btn-info");
                                    break;
                                case "1":
                                    lbSpecialSave.Attributes.Add("class", "btn btn-lg btn-danger");
                                    break;
                            }
                            break;
                        default:
                            switch(hfVehicleState.Value)
                            {
                                case "0":
                                    lbSpecialSave.Attributes.Add("class", "btn btn-lg btn-success");
                                    break;
                                case "1":
                                    lbSpecialSave.Attributes.Add("class", "btn btn-lg btn-danger");
                                    break;
                            }
                            break;
                    }
                }
                break;
        }
    }
    #endregion databound

    #region drorpdown list
    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _data_employee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _data_employee.department_list[0] = _deptList;

        _data_employee = callServicePostEmployee(_urlGetDepartmentList, _data_employee);
        setDdlData(ddlName, _data_employee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- ฝ่าย ---", "-1"));
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        DropDownList ddlEmpList = (DropDownList)fvDocCreate.FindControl("ddlEmpList");
        DropDownList ddlEmpListTop = (DropDownList)fvDocCreate.FindControl("ddlEmpListTop");

        switch (ddlName.ID)
        {
            case "ddlEmpListTop":
                ddlEmpList.SelectedValue = ddlEmpListTop.SelectedValue;
                break;
        }
    }
    #endregion drorpdown list

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        setActiveTab("viewList", 0);

        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "scrollToTop", "window.scrollTo(0, 0)", true);
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        ViewState["GroupSelected"] = null;
        ViewState["VisitorSelected"] = null;
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        switch (fvName.ID)
        {
            case "fvDocCreate":
                if (fvMode == FormViewMode.Edit)
                {

                }
                break;
        }
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }


    protected void setActiveView(string activeTab, int doc_idx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        switch (activeTab)
        {
            case "viewList":
                getShowDataIndex();
                break;
            case "viewCreate":
                if (doc_idx == 0)
                {
                    search_key_employee _search_key = new search_key_employee();
                    _search_key.s_emp_idx = _emp_idx.ToString();
                    _data_employee.search_key_emp_list = new search_key_employee[1];
                    _data_employee.search_key_emp_list[0] = _search_key;
                    _data_employee = getViewEmployeeList(_data_employee);
                    setFormData(fvEmpProfile, FormViewMode.ReadOnly, _data_employee.employee_list_small);

                    div_create_form.Attributes.Add("class", "panel panel-success");
                    litCreateFormTitle.Text = "สร้างรายการ";
                    setFormData(fvDocCreate, FormViewMode.Insert, null);
                    ViewState["VisitorSelected"] = null;
                }
                else
                {
                    div_create_heading.Visible = false;
                    setFormData(fvEmpProfile, FormViewMode.ReadOnly, null);

                    div_create_form.Attributes.Add("class", "panel panel-warning");
                    litCreateFormTitle.Text = "รายละเอียดรายการ";

                    vm_visitors_doc_detail_u0 _u0_edit = new vm_visitors_doc_detail_u0();
                    _u0_edit.u0_idx = doc_idx;
                    _data_visitors.vm_visitors_doc_list_u0 = new vm_visitors_doc_detail_u0[1];
                    _data_visitors.vm_visitors_doc_list_u0[0] = _u0_edit;
                    _data_visitors = callServicePostVisitors(_urlVmGetTransaction, _data_visitors);
                    ViewState["document_detail"] = _data_visitors;
                    int i = 0;
                    if (_data_visitors.vm_visitors_doc_list_u1 != null)
                    {
                        foreach (var item in _data_visitors.vm_visitors_doc_list_u1)
                        {
                            _data_visitor_selected.Add(_data_visitors.vm_visitors_doc_list_u1[i]);
                            i++;
                        }
                    }
                    ViewState["VisitorSelected"] = _data_visitor_selected;
                    setFormData(fvDocCreate, FormViewMode.Edit, ((data_visitors)ViewState["document_detail"]).vm_visitors_doc_list_u0);
                }
                break;
            case "viewApprove":
                break;
        }
    }

    protected void setActiveTab(string activeTab, int doc_idx)
    {
        setActiveView(activeTab, doc_idx);
        switch (activeTab)
        {
            case "viewList":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                break;
            case "viewCreate":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                break;
            case "viewApprove":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                break;
        }
    }

    protected data_visitors callServicePostVisitors(string _cmdUrl, data_visitors _data_visitors)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_visitors);
        // litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_visitors = (data_visitors)_funcTool.convertJsonToObject(typeof(data_visitors), _local_json);

        return _data_visitors;
    }

    protected data_times callServicePostTimes(string _cmdUrl, data_times _data_times)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_times);
        // litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_times = (data_times)_funcTool.convertJsonToObject(typeof(data_times), _local_json);

        return _data_times;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _data_employee)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_employee);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _local_json);

        return _data_employee;
    }

    protected data_geo callServicePostGeo(string _cmdUrl, data_geo _data_geo)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_geo);
        // litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_geo = (data_geo)_funcTool.convertJsonToObject(typeof(data_geo), _local_json);

        return _data_geo;
    }

    protected data_times getGroupList(data_times _data_times)
    {
        _data_times = callServicePostTimes(_urlTmGetGroup, _data_times);
        return _data_times;
    }

    protected data_employee getViewEmployeeList(data_employee _data_employee)
    {
        _data_employee = callServicePostEmployee(_urlGetViewEmployeeListSmall, _data_employee);
        return _data_employee;
    }

    protected data_employee getVisitEmployeeTop(data_employee _data_employee)
    {
        _data_employee = callServicePostEmployee(_urlVmGetVisitEmployeeTop, _data_employee);
        return _data_employee;
    }

    protected data_geo getGeoList(data_geo _data_geo)
    {
        _data_geo = callServicePostGeo(_urlExGetGeoList, _data_geo);
        return _data_geo;
    }

    protected data_visitors getVisitorTypeList(data_visitors _data_visitors)
    {
        _data_visitors = callServicePostVisitors(_urlVmGetVisitorType, _data_visitors);
        return _data_visitors;
    }

    protected data_visitors getVehicleSpecialList()
    {
        vm_visitor_vehicle_special_detail_m0 _getVehicle = new vm_visitor_vehicle_special_detail_m0();
        _getVehicle.m0_idx = 0;
        _data_visitors.vm_visitor_vehicle_special_list_m0 = new vm_visitor_vehicle_special_detail_m0[1];
        _data_visitors.vm_visitor_vehicle_special_list_m0[0] = _getVehicle;

        _data_visitors = callServicePostVisitors(_urlVmGetVehicleSpecial, _data_visitors);
        return _data_visitors;
    }

    protected data_visitors setVehicleSpecialList(data_visitors _data_visitors)
    {
        _data_visitors = callServicePostVisitors(_urlVmSetVehicleSpecial, _data_visitors);
        return _data_visitors;
    }

    public string getCssStatus(Int32 inStatus)
    {
        // return (inStatus == 0 ? "btn btn-lg btn-success" : "btn btn-lg btn-danger");
        return "";
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void changeDirName(string pathName, string fromName, string toName)
    {
        try
        {
            string path = Server.MapPath(pathName);

            string fromDir = "\\" + fromName + "\\";
            string toDir = "\\" + toName + "\\";
            Directory.Move(path + fromDir, path + toDir);
        }
        catch { }
        
    }
    #endregion reuse

    #region PageIndexChanging
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gridViewName = (GridView)sender;

        if (gridViewName.ID == "gvDocList")
        {
            gridViewName.PageIndex = e.NewPageIndex;
            gridViewName.DataBind();
            setGridData(gridViewName, ViewState["document_detail"]);
            SETFOCUS.Focus();
        }
    }
    #endregion PageIndexChanging

    #region get data index
    private void getShowDataIndex()
    {
        vm_visitors_doc_detail_u0 _u0_doc = new vm_visitors_doc_detail_u0();
        _u0_doc.u0_idx = 0;
        _u0_doc.node_idx = 4; // decision(security)
        _u0_doc.actor_idx = _actor_idx;
        _u0_doc.s_idcard = tbSearchIdCard.Text.Trim();
        _u0_doc.s_filter_keyword = tbSearchName.Text.Trim();
        _data_visitors.vm_visitors_doc_list_u0 = new vm_visitors_doc_detail_u0[1];
        _data_visitors.vm_visitors_doc_list_u0[0] = _u0_doc;
        _data_visitors = callServicePostVisitors(_urlVmGetTransaction, _data_visitors);
        ViewState["document_detail"] = _data_visitors.vm_visitors_doc_list_u0;
        setGridData(gvDocList, _data_visitors.vm_visitors_doc_list_u0);
        // clear document detail
        //ViewState["document_detail"] = null;

        _data_visitors = getVehicleSpecialList();
        setRepeaterData(rptCarList, _data_visitors.vm_visitor_vehicle_special_list_m0);
    }

    private void setSearchDefault()
    {
        tbSearchIdCard.Text = "";
        tbSearchName.Text = "";
    }
    #endregion get data index
}