﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_qa_qmr_problem_supplier : System.Web.UI.Page
{

    #region Connect
    function_tool _funcTool = new function_tool();
    data_employee _dtEmployee = new data_employee();
    data_qmr_problem _dtproblem = new data_qmr_problem();

    string _localJson = String.Empty;
    public string checkfile;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetMaster = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMaster_Problem"];
    static string _urlInsertSystem = _serviceUrl + ConfigurationManager.AppSettings["urlInsertSystem_Problem"];
    static string _urlGetSystem = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSystem_Problem"];
    static string _urlInsertMaster = _serviceUrl + ConfigurationManager.AppSettings["urlInsertMaster_Problem"];

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = "9999999";
            ViewState["UpdateDate"] = "0";
            ViewState["count_date"] = "0";
            ViewState["check_gvmaster_risk"] = "1";
            if (Session["User"] != null || Session["Pass"] != null)
            {
                ViewState["User"] = Session["User"].ToString();
                ViewState["Pass"] = Session["Pass"].ToString();

                selectLogin(ViewState["User"].ToString(), ViewState["Pass"].ToString());
                SetDefault(1);
            }
            else
            {
                Response.Redirect(ResolveUrl("~/sdp-login"));
            }
        }
        else
        {
            try
            {

            }
            catch (Exception ex)
            {
                Response.Redirect(ResolveUrl("~/sdp-login"));

            }
        }



    }


    #region Select && Update System

    protected void selectindexlist(GridView gvName, int m0_supidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.m0_supidx = m0_supidx;
        _select.condition = 10;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        setGridData(gvName, _dtproblem.Boxu0_ProblemDocument);
    }

    protected void selectsearch_indexlist(GridView gvName, int m0_supidx, int m0_tpidx, int m0_pridx, int ifsearch, string datestart, string dateend, int m0_group_tpidx, int m1_tpidx, int m2_tpidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.m0_supidx = m0_supidx;
        _select.condition = 19;
        _select.m0_tpidx = m0_tpidx;
        _select.m0_pridx = m0_pridx;
        _select.ifsearch = ifsearch;
        _select.datecreate = datestart;
        _select.dateend = dateend;
        _select.m0_group_tpidx = m0_group_tpidx;
        _select.m1_tpidx = m1_tpidx;
        _select.m2_tpidx = m2_tpidx;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        setGridData(gvName, _dtproblem.Boxu0_ProblemDocument);
    }

    protected void select_empIdx_create()
    {
        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["CEmpIDX_u0"].ToString());

        ViewState["rdept_name_create"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx_create"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName_create"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name_create"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx_create"] = _dtEmployee.employee_list[0].org_idx;
        ViewState["EmpCode_create"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname_create"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx_create"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email_create"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel_create"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname_create"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx_create"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX_create"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["JobGradeIDX_create"] = _dtEmployee.employee_list[0].jobgrade_idx;
    }

    protected void select_detail_list(FormView fvName, int u0_docidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 3;
        _select.u0_docidx = u0_docidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        setFormViewData(fvName, _dtproblem.Boxu0_ProblemDocument);
        ViewState["UpdateDate"] = _dtproblem.Boxu0_ProblemDocument[0].UpdateDate.ToString();
        ViewState["count_date"] = _dtproblem.Boxu0_ProblemDocument[0].count_date.ToString();

    }

    protected void select_log_list(GridView gvName, int u0_docidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 20;
        _select.u0_docidx = u0_docidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        ViewState["rtcode_comment"] = _dtproblem.ReturnCode.ToString();
        setGridData(gvName, _dtproblem.Boxm0_ProblemDocument);

    }

    protected void select_adminqa_comment(FormView fvName, int u1_docidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 9;
        _select.u1_docidx = u1_docidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        setFormViewData(fvName, _dtproblem.Boxu0_ProblemDocument);

    }

    protected void selectstatus(DropDownList ddlName, int unidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 5;
        _select.unidx = unidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "status_name", "staidx");
        //ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสถานะอนุมัติ...", "0"));

    }

    protected void selectLogin(string user, string pass)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.userlogin = user;
        _select.passlogin = pass;
        _select.condition = 11;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);
        ViewState["rtcode"] = _dtproblem.ReturnCode.ToString();
        if (ViewState["rtcode"].ToString() == "0")
        {
            ViewState["m0_supidx"] = _dtproblem.Boxu0_ProblemDocument[0].m0_supidx.ToString();
        }
    }

    protected void UpdateSupplier_System(int unidx, int acidx, int staidx, int u0_docidx, string comment, int empidx, int condition, int m0_tpidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _update = new U0_ProblemDocument();

        _update.unidx = unidx;
        _update.acidx = acidx;
        _update.staidx = staidx;
        _update.u0_docidx = u0_docidx;
        _update.comment = comment;
        _update.m0_tpidx = m0_tpidx;
        _update.CEmpIDX = empidx;
        _update.condition = condition;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem.Boxu0_ProblemDocument[0] = _update;


        string getPath = ConfigurationSettings.AppSettings["path_file_qmr_problem"];
        string filePath = Server.MapPath(getPath + ViewState["doc_code"].ToString());
        DirectoryInfo dir = new DirectoryInfo(filePath);
        HttpFileCollection hfc = Request.Files;
        int ocount = 0;

        SearchDirectories(dir, ViewState["doc_code"].ToString());

        if (checkfile != "11")
        {
            ocount = dir.GetFiles().Length;
        }
        int x = ocount;

        if (UploadFileSupplier.HasFile)
        {
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            string extension = Path.GetExtension(UploadFileSupplier.FileName);

            foreach (HttpPostedFile uploadedFile in UploadFileSupplier.PostedFiles)
            {
                if (uploadedFile.FileName != "" && uploadedFile.FileName != String.Empty)
                {
                    uploadedFile.SaveAs(Server.MapPath(getPath + ViewState["doc_code"].ToString()) + "\\" + "Supplier_" + ViewState["doc_code"].ToString() + x + extension);
                }
                x++;
            }
        }
        _dtproblem = callServicePostQMR_Problem(_urlInsertSystem, _dtproblem);

    
    }

    protected void UpdateResetPass(string user, string password)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxm0_ProblemDocument = new M0_ProblemDocument[1];
        M0_ProblemDocument _update = new M0_ProblemDocument();

        _update.user_login = user;
        _update.pass_login = password;

        _dtproblem.Boxm0_ProblemDocument[0] = _update;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 6;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;

        _dtproblem = callServicePostQMR_Problem(_urlInsertMaster, _dtproblem);
    }

    protected void selecttypeproblem(DropDownList ddlName, int m0_tpidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 9;
        _select.m0_tpidx = m0_tpidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "problem_name", "m0_pridx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภทปัญหา...", "0"));

    }

    protected void selectgroupproduct(DropDownList ddlName)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 19;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "group_name", "m0_group_tpidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกกลุ่มวัตถุดิบ...", "0"));

    }

    protected void selecttypeproduct(DropDownList ddlName, int groupidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 6;
        _select.m0_group_tpidx = groupidx;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "typeproduct_name", "m0_tpidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภทวัตถุดิบ...", "0"));

    }

    protected void selecttypeproduct_m1(DropDownList ddlName, int m0_tpidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 20;
        _select.m0_tpidx = m0_tpidx;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "typeproduct_name_m1", "m1_tpidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาโครงสร้างวัตถุดิบ...", "0"));

    }

    protected void selecttypeproduct_m2(DropDownList ddlName, int m1_tpidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 21;
        _select.m1_tpidx = m1_tpidx;
        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtproblem = callServicePostQMR_Problem(_urlGetMaster, _dtproblem);

        setDdlData(ddlName, _dtproblem.Boxm0_ProblemDocument, "typeproduct_name_m2", "m2_tpidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกโครงสร้างย่อยวัตถุดิบ...", "0"));

    }

    protected void selectmaterial_show(GridView gvName, int u2_docidx)
    {
        _dtproblem = new data_qmr_problem();

        _dtproblem.Boxu0_ProblemDocument = new U0_ProblemDocument[1];
        U0_ProblemDocument _select = new U0_ProblemDocument();

        _select.condition = 13;
        _select.u2_docidx = u2_docidx;

        _dtproblem.Boxu0_ProblemDocument[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtproblem));

        _dtproblem = callServicePostQMR_Problem(_urlGetSystem, _dtproblem);

        ViewState["check_gvmaster_risk"] = _dtproblem.ReturnCode.ToString();
        setGridData(gvName, _dtproblem.Boxu3_ProblemDocument);
    }

    #endregion

    #region reuse
    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_qmr_problem callServicePostQMR_Problem(string _cmdUrl, data_qmr_problem _dtproblem)
    {
        _localJson = _funcTool.convertObjectToJson(_dtproblem);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _dtproblem = (data_qmr_problem)_funcTool.convertJsonToObject(typeof(data_qmr_problem), _localJson);


        return _dtproblem;
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setRepeatData(Repeater rpName, Object obj)
    {
        rpName.DataSource = obj;
        rpName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        ddlName.Items.Clear();
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    #endregion

    #region Directories_File URL


    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt1.Rows.Add(file.Name);
                    dt1.Rows[i][1] = f[0];
                    i++;
                }
            }

            GridView gvFile = (GridView)FvdetailProblem.FindControl("gvFile");
            if (dt1.Rows.Count > 0)
            {

                ds1.Tables.Add(dt1);
                // gvFileLo1.Visible = true;
                gvFile.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
                gvFile.DataBind();
                ds1.Dispose();
            }
            else
            {

                gvFile.DataSource = null;
                gvFile.DataBind();

            }
        }
        catch
        {
            checkfile = "11";
        }
    }

    #endregion

    #region GridView
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvIndex":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblStatusDoc = (Label)e.Row.FindControl("lblStatusDoc");
                    LinkButton btnviewdetail = (LinkButton)e.Row.FindControl("btnviewdetail");


                    lblStatusDoc.ForeColor = System.Drawing.ColorTranslator.FromHtml("#803DA9");
                    linkBtnTrigger(btnviewdetail);
                }
                break;

            case "gvFile":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvIndex":
                GvIndex.PageIndex = e.NewPageIndex;

                if (ViewState["CheckSearch"].ToString() == "0")
                {
                    selectindexlist(GvIndex, int.Parse(ViewState["m0_supidx"].ToString()));
                }
                else
                {
                    selectsearch_indexlist(GvIndex, int.Parse(ViewState["m0_supidx"].ToString()), int.Parse(ddltypeproduct_List.SelectedValue), int.Parse(ddlproblem_List.SelectedValue), int.Parse(ddlSearchDate_List.SelectedValue), AddStartdate_List.Text, AddEndDate_List.Text, int.Parse(ddlgroupproduct_List.SelectedValue), int.Parse(ddltypeproduct_list_m1.SelectedValue), int.Parse(ddltypeproduct_list_m2.SelectedValue));
                }

                break;
        }
    }

    #endregion

    #region FormView

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {
            case "Fvdetailusercreate":
                FormView Fvdetailusercreate = (FormView)ViewDetail.FindControl("Fvdetailusercreate");

                if (Fvdetailusercreate.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)Fvdetailusercreate.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)Fvdetailusercreate.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)Fvdetailusercreate.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)Fvdetailusercreate.FindControl("txtsec"));
                    var txtsecidx = ((TextBox)Fvdetailusercreate.FindControl("txtsecidx"));
                    var txtpos = ((TextBox)Fvdetailusercreate.FindControl("txtpos"));
                    var txtemail = ((TextBox)Fvdetailusercreate.FindControl("txtemail"));
                    //var txttel = ((TextBox)Fvdetailusercreate.FindControl("txttel"));
                    var txtorg = ((TextBox)Fvdetailusercreate.FindControl("txtorg"));
                    var txtorgidx = ((TextBox)Fvdetailusercreate.FindControl("txtorgidx"));

                    txtempcode.Text = ViewState["EmpCode_create"].ToString();
                    txtrequesname.Text = ViewState["FullName_create"].ToString();
                    txtorg.Text = ViewState["Org_name_create"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name_create"].ToString();
                    txtsec.Text = ViewState["Secname_create"].ToString();
                    txtpos.Text = ViewState["Positname_create"].ToString();
                    //txttel.Text = ViewState["Tel_create"].ToString();
                    txtemail.Text = ViewState["Email_create"].ToString();
                    txtorgidx.Text = ViewState["Org_idx_create"].ToString();
                    txtsecidx.Text = ViewState["Sec_idx_create"].ToString();
                }

                break;

            case "FvdetailProblem":
                if (FvdetailProblem.CurrentMode == FormViewMode.ReadOnly)
                {
                    var lbllocidx_show = ((Label)FvdetailProblem.FindControl("lbllocidx_show"));
                    var lblm0_tpidx_show = ((Label)FvdetailProblem.FindControl("lblm0_tpidx_show"));

                    ViewState["LocRef_Exchange"] = lbllocidx_show.Text;
                    ViewState["m0_tpidx_check_seaweed"] = lblm0_tpidx_show.Text;
                }
                break;

            case "FvQA_Comment":
                FormView FvQA_Comment = (FormView)ViewDetail.FindControl("FvQA_Comment");

                if (FvQA_Comment.CurrentMode == FormViewMode.ReadOnly)
                {
                    var lblu2_docidx = ((Label)FvQA_Comment.FindControl("lblu2_docidx"));
                    var Gvshow_Material_risk = ((GridView)FvQA_Comment.FindControl("Gvshow_Material_risk"));
                    var divshow_gvmaterial_risk = ((Control)FvQA_Comment.FindControl("divshow_gvmaterial_risk"));


                    selectmaterial_show(Gvshow_Material_risk, int.Parse(lblu2_docidx.Text));

                    if (ViewState["check_gvmaster_risk"].ToString() == "1")
                    {
                        divshow_gvmaterial_risk.Visible = false;
                    }
                    else
                    {
                        divshow_gvmaterial_risk.Visible = true;
                    }

                }
                break;
        }
    }
    #endregion

    #region SelectedIndexchange

    protected void SelectedIndexChanged(object sender, EventArgs e)
    {

        if (sender is DropDownList)
        {
            DropDownList ddlName = (DropDownList)sender;

            switch (ddlName.ID)
            {
                case "ddlSearchDate_List":

                    if (int.Parse(ddlSearchDate_List.SelectedValue) == 3)
                    {
                        AddEndDate_List.Enabled = true;
                    }
                    else
                    {
                        AddEndDate_List.Enabled = false;
                        AddEndDate_List.Text = string.Empty;
                    }

                    break;

                case "ddlgroupproduct_List":
                    selecttypeproduct(ddltypeproduct_List, int.Parse(ddlgroupproduct_List.SelectedValue));

                    break;

                case "ddltypeproduct_List":
                    selecttypeproduct_m1(ddltypeproduct_list_m1, int.Parse(ddltypeproduct_List.SelectedValue));
                    selecttypeproblem(ddlproblem_List, int.Parse(ddltypeproduct_List.SelectedValue));
                    break;


                case "ddltypeproduct_list_m1":
                    selecttypeproduct_m2(ddltypeproduct_list_m2, int.Parse(ddltypeproduct_list_m1.SelectedValue));

                    break;
            }
        }
    }
    #endregion

    protected void SetDefault(int page)
    {
        switch (page)
        {

            case 1:
                MvMaster.SetActiveView(ViewList);
                selectindexlist(GvIndex, int.Parse(ViewState["m0_supidx"].ToString()));
                //selecttypeproduct(ddltypeproduct_List);
                selectgroupproduct(ddlgroupproduct_List);
                ddlgroupproduct_List.SelectedValue = "0";
                ddltypeproduct_List.SelectedValue = "0";
                ddltypeproduct_list_m1.SelectedValue = "0";
                ddltypeproduct_list_m2.SelectedValue = "0";
                ddlSearchDate_List.SelectedValue = "0";
                AddStartdate_List.Text = String.Empty;
                AddEndDate_List.Text = String.Empty;
                ddlproblem_List.SelectedValue = "0";

                ViewState["CheckSearch"] = "0";
                break;

            case 2:
                MvMaster.SetActiveView(ViewDetail);
                select_empIdx_create();
                Fvdetailusercreate.ChangeMode(FormViewMode.Insert);
                Fvdetailusercreate.DataBind();

                FvdetailProblem.ChangeMode(FormViewMode.ReadOnly);
                select_detail_list(FvdetailProblem, int.Parse(ViewState["u0_docidx"].ToString()));

                Label lblu1_docidx = (Label)FvdetailProblem.FindControl("lblu1_docidx");
                CheckBox chkmat_show = (CheckBox)FvdetailProblem.FindControl("chkmat_show");
                Label lblmat_no = (Label)FvdetailProblem.FindControl("lblmat_no");
                Control divmatshow_no = (Control)FvdetailProblem.FindControl("divmatshow_no");

                HiddenField hfM0NodeIDX = (HiddenField)FvdetailProblem.FindControl("hfM0NodeIDX");
                HiddenField hfM0ActoreIDX = (HiddenField)FvdetailProblem.FindControl("hfM0ActoreIDX");

                ViewState["m0_node"] = hfM0NodeIDX.Value;
                ViewState["m0_actor"] = hfM0ActoreIDX.Value;

                if (lblmat_no.Text == null || lblmat_no.Text == "0" || lblmat_no.Text == String.Empty)
                {
                    chkmat_show.Checked = false;
                    divmatshow_no.Visible = false;
                }
                else
                {
                    chkmat_show.Checked = true;
                    divmatshow_no.Visible = true;
                }
                chkmat_show.Enabled = false;
                try
                {

                    string getPathLotus = ConfigurationSettings.AppSettings["path_file_qmr_problem"];

                    string filePathLotus = Server.MapPath(getPathLotus + ViewState["doc_code"].ToString());
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                    SearchDirectories(myDirLotus, ViewState["doc_code"].ToString());
                }
                catch
                {

                }

                FvQA_Comment.ChangeMode(FormViewMode.ReadOnly);
                select_adminqa_comment(FvQA_Comment, int.Parse(lblu1_docidx.Text));
                selectstatus(ddl_approve, 6);
                txtremark_approve.Text = String.Empty;
                txtcount_typeproblem.Text = ViewState["count_date"].ToString();
                txtdateupdate.Text = ViewState["UpdateDate"].ToString();
                select_log_list(GvComment, int.Parse(ViewState["u0_docidx"].ToString()));
                if (ViewState["rtcode_comment"].ToString() == "0")
                {
                    GvComment.Visible = true;
                }
                else
                {
                    GvComment.Visible = false;
                }
                break;
        }
    }

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();


        switch (cmdName)
        {
            case "CmdDetail":
                string[] arg1_detail = new string[3];
                arg1_detail = e.CommandArgument.ToString().Split(';');

                ViewState["u0_docidx"] = arg1_detail[0];
                ViewState["doc_code"] = arg1_detail[1];
                ViewState["CEmpIDX_u0"] = arg1_detail[2];

                SetDefault(2);
                break;

            case "CmdUpdateApprove":
                UpdateSupplier_System(int.Parse(ViewState["m0_node"].ToString()), int.Parse(ViewState["m0_actor"].ToString()), int.Parse(ddl_approve.SelectedValue), int.Parse(ViewState["u0_docidx"].ToString()), txtremark_approve.Text, int.Parse(ViewState["EmpIDX"].ToString()), 2, int.Parse(ViewState["m0_tpidx_check_seaweed"].ToString()));
                SetDefault(1);
                break;

            case "btnCancel":
                if (ViewState["CheckSearch"].ToString() == "0")
                {
                    SetDefault(1);
                }
                else

                {
                    MvMaster.SetActiveView(ViewList);
                }
                break;

            case "btnsearch_list":
                ViewState["CheckSearch"] = "1";
                selectsearch_indexlist(GvIndex, int.Parse(ViewState["m0_supidx"].ToString()), int.Parse(ddltypeproduct_List.SelectedValue), int.Parse(ddlproblem_List.SelectedValue), int.Parse(ddlSearchDate_List.SelectedValue), AddStartdate_List.Text, AddEndDate_List.Text, int.Parse(ddlgroupproduct_List.SelectedValue), int.Parse(ddltypeproduct_list_m1.SelectedValue), int.Parse(ddltypeproduct_list_m2.SelectedValue));
                break;

            case "btnrefresh_list":
                SetDefault(1);
                break;

        }
    }

    #endregion
}