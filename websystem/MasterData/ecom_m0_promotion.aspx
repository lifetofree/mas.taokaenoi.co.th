﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="ecom_m0_promotion.aspx.cs" Inherits="websystem_MasterData_ecom_m0_promotion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litdebug" runat="server"></asp:Literal>

    <asp:MultiView ID="MvSystem" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btn_addplace" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่ม โปรโมชั่น" data-toggle="tooltip" title="เพิ่ม โปรโมชั่น" runat="server" CommandName="cmdAddUnit" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่ม โปรโมชั่น</asp:LinkButton>

                <div class="clearfix"></div>
            </div>
            <asp:FormView ID="FvInsert" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class=" panel panel-info">
                        <div class=" panel-heading">
                            <h3 class="panel-title">เพิ่ม โปรโมชั่น
                            </h3>

                        </div>
                        <div class=" panel-body">

                            <div class=" form-horizontal" style="padding: 20px 30px;">


                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="รายละเอียดโปรโมชั่น"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 ">
                                        <asp:TextBox ID="txtpro_name" runat="server" CssClass="form-control" placeholder="กรอกรายละเอียดโปรโมชั่น ..." Enabled="true" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                            ControlToValidate="txtpro_name" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกรายละเอียดโปรโมชั่น" ValidationGroup="Save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_place_name" Width="220" />
                                    </div>
                                </div>
                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="รหัสโปรโมชั่น"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 ">
                                        <asp:TextBox ID="txt_ProID" runat="server" CssClass="form-control" placeholder="กรอกรหัสโปรโมชั่น ..." Enabled="true" />
                                        <asp:RequiredFieldValidator ID="Re_place_name" runat="server"
                                            ControlToValidate="txt_ProID" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกรหัส โปรโมชั่น" ValidationGroup="Save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="aj_pname" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_place_name" Width="220" />
                                    </div>
                                </div>
                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="ZAP Code"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 ">
                                        <asp:TextBox ID="txt_ZAP_code" runat="server" CssClass="form-control" placeholder="กรอก ZAP Code ..." Enabled="true" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                            ControlToValidate="txt_ZAP_code" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอก ZAP Code" ValidationGroup="Save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_place_name" Width="220" />
                                    </div>
                                </div>
                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="ประเภทโปรโมชั่น"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <asp:DropDownList ID="ddType" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1" Text="จำนวนเงิน(บาท)" />
                                            <asp:ListItem Value="2" Text="เปอร์เซ็น(%)" />
                                            <asp:ListItem Value="3" Text="ของแถม" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="จำนวนที่ลด"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6 ">
                                        <asp:TextBox ID="txt_amount" runat="server" CssClass="form-control" TextMode="Number" placeholder="กรอกจำนวนที่ลด ..." Enabled="true" />

                                    </div>
                                </div>
                                <div class=" form-group">
                                    <div class="col-xs-12 col-sm-3  control-label">
                                        <asp:Label runat="server" Text="สถานะ"></asp:Label>
                                    </div>
                                    <div class="col-xs-12 col-sm-6  col-md-6">
                                        <asp:DropDownList ID="ddPlace" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1" Text="Online" />
                                            <asp:ListItem Value="0" Text="Offline" />
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12 col-sm-offset-3 col-sm-9">
                                        <asp:LinkButton runat="server" OnCommand="btnCommand" CommandName="CmdSave" CssClass="btn btn-success" data-toggle="tooltip" title="บันทึก" Text="Save" ValidationGroup="Save"></asp:LinkButton>
                                        <asp:LinkButton runat="server" OnCommand="btnCommand" CommandName="CmdCancel" CssClass="btn btn-danger" data-toggle="tooltip" title="ยกเลิก" Text="Cancel"></asp:LinkButton>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>
            <div id="div_searchmaster_page" runat="server">


                <div class=" panel panel-info">
                    <div class=" panel-heading">
                        <h3 class="panel-title">ค้นหา
                        </h3>
                    </div>
                    <div class=" panel-body">

                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <asp:Label ID="Label130" runat="server" Text="รหัสโปรโมชั่น" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:TextBox ID="txtmaster_search" runat="server" CssClass="form-control" PlaceHolder="........" />

                                </div>
                                <asp:Label ID="Label131" runat="server" Text="สถานะ" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="Src_status" CssClass="form-control" runat="server">
                                        <asp:ListItem Value="999" Text="ALL" />
                                        <asp:ListItem Value="1" Text="Online" />
                                        <asp:ListItem Value="0" Text="Offline" />
                                    </asp:DropDownList>
                                </div>

                            </div>

                            <div class="form-group">
                                <div class="col-sm-2 col-sm-offset-2">
                                    <asp:LinkButton ID="LinkButton25" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSearchMaster_src" OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i>&nbsp;ค้นหา</asp:LinkButton>
                                    <asp:LinkButton ID="LinkButton26" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnRefreshMaster_Supplier" data-toggle="tooltip" title="Refresh"><i class="glyphicon glyphicon-refresh"></i></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <asp:GridView ID="Gv_select_place" runat="server" Visible="true" AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                OnRowEditing="Master_RowEditing"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                PageSize="20"
                AutoPostBack="FALSE">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                </EmptyDataTemplate>

                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="place_idx" runat="server" Visible="false" Text='<%# Eval("pro_idx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>

                        </ItemTemplate>
                        <EditItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_pro_idx_edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("pro_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="lbupdates_place_name" CssClass="col-sm-3 control-label" runat="server" Text="รายละเอียดโปรโมชั่น" />
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="Name_prodes" runat="server" CssClass="form-control " Text='<%# Eval("PromotionDesc") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorplace_name" runat="server"
                                                    ControlToValidate="Name_prodes" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกรายละเอียดโปรโมชั่น" ValidationGroup="Editpname" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_pname" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorplace_name" Width="220" />
                                            </div>
                                        </div>


                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="รหัสโปรโมชั่น" />
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="Name_proId" runat="server" CssClass="form-control " Text='<%# Eval("PromotionID") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                    ControlToValidate="Name_proId" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกรหัสโปรโมชั่น" ValidationGroup="Editpname" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="220" />
                                            </div>
                                        </div>


                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label ID="Label3" CssClass="col-sm-3 control-label" runat="server" Text="ZAP Code" />
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="Name_ZAPcode" runat="server" CssClass="form-control " Text='<%# Eval("ZAP_code") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                                                    ControlToValidate="Name_ZAPcode" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอก ZAP Code" ValidationGroup="Editpname" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="220" />
                                            </div>
                                        </div>


                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddEdit_type" Text='<%# Eval("type_promotion") %>'
                                                CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1" Text="จำนวนเงิน(บาท)" />
                                                <asp:ListItem Value="2" Text="เปอร์เซ็น(%)" />
                                                <asp:ListItem Value="3" Text="ของแถม" />
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <asp:Label CssClass="col-sm-3 control-label" runat="server" Text="จำนวนที่ลด" />
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="Name_amount" runat="server" CssClass="form-control " Text='<%# Eval("amount") %>'></asp:TextBox>

                                            </div>
                                        </div>


                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_place" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddEdit_place" Text='<%# Eval("pro_status") %>'
                                                CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editpname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียดโปรโมชั่น" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("PromotionDesc") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>


                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รหัส โปรโมชั่น" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="7%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("PromotionID") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>


                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ZAP Code" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="7%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("ZAP_code") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>


                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ประเภท โปรโมชั่น" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label ID="lbptype_promotion" runat="server" Visible="false"
                                        Text='<%# Eval("type_promotion") %>'></asp:Label>
                                    <asp:Label ID="show_type_1" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="จำนวนเงิน(บาท)"
                                        CssClass="col-sm-12">
                                               
                                                    <span>จำนวนเงิน(บาท)</span>
                                    </asp:Label>
                                    <asp:Label ID="show_type_2" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="เปอร์เซ็น(%)" CssClass="col-sm-12">
                                            
                                                <span>เปอร์เซ็น(%)</span>
                                    </asp:Label>
                                    <asp:Label ID="show_type_3" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="ของแถม" CssClass="col-sm-12">
                                              
                                                <span>ของแถม</span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จำนวนที่ลด" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="7%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px; text-align: left;">
                                    <asp:Label runat="server"
                                        Text='<%# Eval("amount") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <small>
                                <div style="padding: 5px 10px 0px; text-align: left;">
                                    <asp:Label ID="lbpplace_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("pro_status") %>'></asp:Label>
                                    <asp:Label ID="place_statusOnline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style=" color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="place_statusOffline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style=" color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style=" text-align: center;">
                                <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="btnTodelete"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("pro_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </asp:View>

    </asp:MultiView>


</asp:Content>
