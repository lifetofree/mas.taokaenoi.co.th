﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="en_rpt_plan.aspx.cs" Inherits="websystem_en_rpt_plan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litDebug" runat="server" Text=""></asp:Literal>
    <asp:Literal ID="litDebug1" runat="server" Text=""></asp:Literal>
    <nav class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand">Menu</a>
        </div>
        <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-left" runat="server">
                <li id="liplan_pm" runat="server">
                    <asp:LinkButton ID="btnplan_pm" runat="server"
                        OnCommand="btnCommand" CommandName="btnplan_pmx" Text="แผนการทำ PM เครื่องจักร" />
                </li>
                <li id="lireport" runat="server" visible="false">
                    <asp:LinkButton ID="btnreport" runat="server"
                        OnCommand="btnCommand" CommandName="btnreport" Text="Report" />
                </li>

            </ul>
        </div>
    </nav>
    <div class="clearfix"></div>
    <asp:MultiView ID="MultiViewBody" runat="server" ActiveViewIndex="0">
        <asp:View ID="View_plan_pm" runat="server">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Panel ID="pnlsearch_plan_pm" runat="server">
                        <div class="panel panel-primary m-t-10">
                            <div class="panel-heading f-bold">ค้นหา</div>
                            <div class="panel-body">

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>ปี</label>
                                        <asp:DropDownList ID="ddlYearSearch_plan_pm" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>โรงงาน</label>
                                        <asp:DropDownList ID="ddllocate" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>


                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>ประเภทเครื่องจักร</label>
                                        <asp:DropDownList ID="ddltypemachine" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" class="form-control" runat="server">
                                            <asp:ListItem Value="0" Text="เลือก..."></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>รหัสกลุ่มเครื่องจักร</label>
                                        <asp:DropDownList ID="ddltypecode" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" class="form-control" runat="server">
                                            <asp:ListItem Value="0" Text="เลือก..."></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>


                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>อาคาร</label>
                                        <asp:DropDownList ID="ddlbuilding" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <label>
                                        &nbsp;
                                    </label>
                                    <div class="form-group">
                                        <asp:LinkButton ID="btnFilter_plan_pm" runat="server" CssClass="btn btn-primary" Text="<i class='fa fa-search'></i> ค้นหา"
                                            OnCommand="btnCommand" CommandName="btnFilter_plan_pm" />
                                    </div>
                                </div>

                            </div>
                        </div>
                    </asp:Panel>

                    <div class="col-md-11 col-md-offset-1">
                        <asp:Panel ID="panel2" runat="server" CssClass="text-right m-b-10 hidden-print">

                            <button
                                onclick="tableToExcel('Tableprint_plan', 'แบบฟอร์มใบบันทึกแผนการทำ PM เครื่องจักร')"
                                class="btn btn-primary">
                                <i class='fa fa-file-excel'></i>
                                Export Excel
                            </button>
                            <asp:LinkButton ID="btnprint_plan" CssClass="btn btn-primary" runat="server"
                                Text="<i class='fa fa-print'></i> พิมพ์" OnClientClick="return printDiv('print_plan');" />
                        </asp:Panel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <%--<div class="panel-body" style="height: 600px; overflow-y: scroll; overflow-x: scroll;">
            --%>
            <asp:Panel ID="pnl_alert" runat="server">
                <div class="alert alert-danger col-md-12" style="text-align: center" id="dv_alert" runat="server">
                    ไม่พบข้อมูล
                </div>
            </asp:Panel>

            <%-- table table-bordered  
                    style="padding-top: 12px;padding-bottom: 12px;text-align: left;
                                   background-color: #4CAF50;border: 1px solid #ddd; color: white;
                                   font-size: 12px;padding: 0px 0px;
                                   font-family: Trebuchet MS, Arial, Helvetica, sans-serif;
                                   border-collapse: collapse;"
            --%>

            <asp:Panel ID="pnl_print_plan" runat="server">
                <div id="print_plan" class="pull-left">
                    <%-- table table-bordered  --%>
                    <table id="Tableprint_plan"
                        class="table table-bordered word-wrap"
                        style="font-family: Trebuchet MS, Arial, Helvetica, sans-serif; border-collapse: collapse;">
                        <thead style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; font-size: 12px; padding: 0px 0px;">
                            <tr>
                                <td colspan="51" style="border-style: hidden; font-size: 14px; font-family: serif; background-color: white; width: 45%;">
                                    <center>
                                     <asp:Literal ID="litTitle1" runat="server"></asp:Literal>
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="51" style="border-style: hidden; font-size: 14px; font-family: serif; background-color: white; width: 45%;">
                                    <center>
                                    
                                    <asp:Literal ID="litTitle2" runat="server"></asp:Literal>
                                </center>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="51" style="border-style: hidden; font-size: 14px; font-family: serif; background-color: white; width: 45%;">
                                    <center>
                                    <asp:Literal ID="litTitle3" runat="server"></asp:Literal>
                                </center>
                                </td>
                            </tr>
                            <tr>
                                <td rowspan="4" style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; width: 2%;">
                                    <center> No. </center>
                                </td>
                                <td rowspan="4" style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; width: 30%;">
                                    <center>Machine Name / Equipment</center>
                                </td>
                                <td colspan="0" style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; width: 25%;">
                                    <center>สัปดาห์ที่</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>1</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>2</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>3</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>4</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>5</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>6</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>7</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>8</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>9</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>10</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>11</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>12</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>13</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>14</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>15</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>16</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>17</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>18</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>19</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>20</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>21</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>22</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>23</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>24</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>25</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>26</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>27</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>28</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>29</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>30</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>31</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>32</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>33</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>34</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>35</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>36</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>37</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>38</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>39</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>40</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>41</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>42</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>43</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>44</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>45</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>46</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>47</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px; width: 2%;">
                                    <center>48</center>
                                </td>

                            </tr>
                            <tr>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>เดือน</center>
                                </td>
                                <td colspan="4" style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>January</center>
                                </td>
                                <td colspan="4" style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>February</center>
                                </td>
                                <td colspan="4" style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>March</center>
                                </td>
                                <td colspan="4" style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>April</center>
                                </td>
                                <td colspan="4" style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>May</center>
                                </td>
                                <td colspan="4" style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>June</center>
                                </td>
                                <td colspan="4" style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>July</center>
                                </td>
                                <td colspan="4" style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>August</center>
                                </td>
                                <td colspan="4" style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>September</center>
                                </td>
                                <td colspan="4" style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>October</center>
                                </td>
                                <td colspan="4" style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>November</center>
                                </td>
                                <td colspan="4" style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>December</center>
                                </td>

                            </tr>
                            <tr>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>สัปดาห์</center>
                                </td>
                                <%-- 1 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>1</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>2</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>3</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>4</center>
                                </td>
                                <%-- 2 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>1</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>2</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>3</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>4</center>
                                </td>
                                <%-- 3 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>1</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>2</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>3</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>4</center>
                                </td>
                                <%-- 4 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>1</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>2</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>3</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>4</center>
                                </td>
                                <%-- 5 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>1</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>2</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>3</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>4</center>
                                </td>
                                <%-- 6 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>1</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>2</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>3</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>4</center>
                                </td>
                                <%-- 7 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>1</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>2</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>3</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>4</center>
                                </td>
                                <%-- 8 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>1</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>2</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>3</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>4</center>
                                </td>
                                <%-- 9 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>1</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>2</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>3</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>4</center>
                                </td>
                                <%-- 10 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>1</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>2</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>3</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>4</center>
                                </td>
                                <%-- 11 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>1</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>2</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>3</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>4</center>
                                </td>
                                <%-- 12 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>1</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>2</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>3</center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>4</center>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center>CODE</center>
                                </td>
                                <%-- 1 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <%-- 2 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <%-- 3 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <%-- 4 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <%-- 5 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <%-- 6 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <%-- 7 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <%-- 8 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <%-- 9 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <%-- 10 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <%-- 11 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <%-- 12 --%>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                                <td style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #4CAF50; border: 1px solid #ddd; color: white; font-size: 12px; padding: 0px 0px;">
                                    <center> </center>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <%= getHtml_RptPlanPM() %>
                            <tr>
                            </tr>

                            <tr>
                                <td style="border-style: hidden;">
                                    <br />
                                </td>
                                <td colspan="6" style="border-style: hidden; font-size: 13px; font-family: serif; padding: 0px 0px;">
                                    <br />
                                    <center> 
                                        <asp:Literal ID="litapp_t1_1" runat="server"></asp:Literal>
                                        <br />
                                        <asp:Literal ID="litapp_t1_2" runat="server"></asp:Literal>
                                        <br />
                                        <asp:Literal ID="litapp_t1_3" runat="server"></asp:Literal>
                                    </center>
                                </td>
                                <td colspan="14" style="border-style: hidden; font-size: 13px; font-family: serif; padding: 0px 0px;">
                                    <br />
                                    <center> 
                                        <asp:Literal ID="litapp_t2_1" runat="server"></asp:Literal>
                                        <br />
                                        <asp:Literal ID="litapp_t2_2" runat="server"></asp:Literal>
                                        <br />
                                        <asp:Literal ID="litapp_t2_3" runat="server"></asp:Literal>
                                    </center>
                                </td>
                                <td colspan="14" style="border-style: hidden; font-size: 13px; font-family: serif; padding: 0px 0px;">
                                    <br />
                                    <center> 
                                        <asp:Literal ID="litapp_t3_1" runat="server"></asp:Literal>
                                        <br />
                                        <asp:Literal ID="litapp_t3_2" runat="server"></asp:Literal>
                                        <br />
                                        <asp:Literal ID="litapp_t3_3" runat="server"></asp:Literal>
                                    </center>
                                </td>
                                <td colspan="16" style="border-style: hidden; font-size: 13px; font-family: serif; padding: 0px 0px;">
                                    <br />
                                    <center> 
                                        <asp:Literal ID="litapp_t4_1" runat="server"></asp:Literal>
                                        <br />
                                        <asp:Literal ID="litapp_t4_2" runat="server"></asp:Literal>
                                        <br />
                                        <asp:Literal ID="litapp_t4_3" runat="server"></asp:Literal>
                                    </center>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="51" style="border-style: hidden; font-size: 13px; font-family: serif;text-align: right; padding: 0px 0px;">
                                    
                                        <asp:Literal ID="litiso" runat="server"></asp:Literal>
                                </td>
                            </tr>
                        </tbody>

                    </table>

                </div>
            </asp:Panel>
            <%--</div>--%>

            <br />



        </asp:View>
    </asp:MultiView>



    <script type="text/javascript">
        var tableToExcel = (function () {
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            return function (table, name) {
                if (!table.nodeType) table = document.getElementById(table)
                var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
                window.location.href = uri + base64(format(template, ctx))
            }
        })()
    </script>

    <script type="text/javascript">
        function fnExcelReport() {
            var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
            var textRange; var j = 0;
            tab = document.getElementById('Tableprint_sched'); // id of table

            for (j = 0; j < tab.rows.length; j++) {
                tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //tab_text=tab_text+"</tr>";
            }

            tab_text = tab_text + "</table>";
            tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
            tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
            tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
            {
                txtArea1.document.open("txt/html", "replace");
                txtArea1.document.write(tab_text);
                txtArea1.document.close();
                txtArea1.focus();
                sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
            }
            else                 //other browser not tested on IE 11
                sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));

            return (sa);
        }
    </script>

    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);
            var printWindow = window.open('', '', 'height=800,width=1000');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
                //printWindow.resizeTo(100,100);
                //printWindow.focus();
                //printwindow.window.close();
            }, 500);
            return false;
        }

        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }

        function dtPickerInput(inputClassFrom, inputClassTo) {
            $(inputClassFrom).datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $(inputClassTo).datetimepicker({
                format: 'DD/MM/YYYY'
            });
        }

        function dtPickerBtn(inputClassFrom, inputClassTo, btnClassFrom, btnClassTo) {
            $(btnClassFrom).click(function () {
                $(inputClassFrom).data("DateTimePicker").show();
            });
            $(btnClassTo).click(function () {
                $(inputClassTo).data("DateTimePicker").show();
            });
        }

        $(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.header-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-manual-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.cutstock-import-date').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.show-from-onclick').click(function () {
                $('.cutstock-manual-date').data("DateTimePicker").show();
            });

            $('.show-order-sale-log-from-onclick').click(function () {
                $('.filter-order-from').data("DateTimePicker").show();
            });
            $('.show-order-sale-log-to-onclick').click(function () {
                $('.filter-order-to').data("DateTimePicker").show();
            });
            $('.filter-order-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-order-fix-log-from-onclick').click(function () {
                $('.filter-order-fix-from').data("DateTimePicker").show();
            });
            $('.show-order-fix-log-to-onclick').click(function () {
                $('.filter-order-fix-to').data("DateTimePicker").show();
            });
            $('.filter-order-fix-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-order-fix-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });


            $('.show-u1-from-onclick').click(function () {
                $('.filter-u1-from').data("DateTimePicker").show();
            });
            $('.show-u1-to-onclick').click(function () {
                $('.filter-u1-to').data("DateTimePicker").show();
            });
            $('.filter-u1-from').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.filter-u1-to').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
    </script>

    <asp:HiddenField ID="hddf_empty" runat="server" />
</asp:Content>

