﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for CalendarCarEvent
/// </summary>
public class CalendarCarEvent
{
    public int id { get; set; }
    public int place_idx { get; set; }
    public int m0_car_idx { get; set; }
    public string title { get; set; }
    public string description { get; set; }
    public string place_name { get; set; }
    public string car_register { get; set; }
    public DateTime start { get; set; }
    public DateTime end { get; set; }
    public bool allDay { get; set; }
    public string topic_booking { get; set; }
    public string emp_name_th { get; set; }
    public string detailtype_car_name { get; set; }
    public int detailtype_car_idx { get; set; }
    public int car_use_idx { get; set; }
    public string car_use_name { get; set; }
    public string type_booking_name { get; set; }
}