﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_nwd_r0_position : System.Web.UI.Page
{

    #region initial function/data

    function_tool _funcTool = new function_tool();
    data_networkdevices _dtnetde = new data_networkdevices();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlSelectLocation = _serviceUrl + ConfigurationManager.AppSettings["urlSelectLocation"];
    static string urlSelectFloor = _serviceUrl + ConfigurationManager.AppSettings["urlSelectFloor"];
    static string urlSelectChamber = _serviceUrl + ConfigurationManager.AppSettings["urlSelectChamber"];
    static string urlSelectPosition = _serviceUrl + ConfigurationManager.AppSettings["urlSelectPosition"];
    static string urlInsertPosition = _serviceUrl + ConfigurationManager.AppSettings["urlInsertPosition"];
    static string urlDeletePosition = _serviceUrl + ConfigurationManager.AppSettings["urlDeletePosition"];
    static string urlSelectRegisNumber = _serviceUrl + ConfigurationManager.AppSettings["urlSelectRegisNumber"];
    static string urlSelectImage = _serviceUrl + ConfigurationManager.AppSettings["urlSelectImage"];

    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;

    #endregion

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            SelectList();
            mergeCell();
        }
    }

    #endregion

    #region CallService
    protected data_networkdevices callServiceNetwork(string _cmdUrl, data_networkdevices _dtnetde)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dtnetde);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _dtnetde = (data_networkdevices)_funcTool.convertJsonToObject(typeof(data_networkdevices), _localJson);

        return _dtnetde;
    }

    #endregion

    #region Select SQL

    protected void SelectList()
    {

        data_networkdevices _dtnetde = new data_networkdevices();
        _dtnetde.r0position_list = new r0position_detail[1];
        r0position_detail position_add = new r0position_detail();

        position_add.REIDX = 0;

        _dtnetde.r0position_list[0] = position_add;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _dtnetde = callServiceNetwork(urlSelectPosition, _dtnetde);

        setGridData(GvMaster, _dtnetde.r0position_list);
    }

    /*protected void SelectLocation()
    {
        data_networkdevices _dtnetde = new data_networkdevices();
        _dtnetde.r0position_list = new r0position_detail[1];
        r0position_detail position_add = new r0position_detail();

        position_add.CHIDX = 0;

        _dtnetde.r0position_list[0] = position_add;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _dtnetde = callServiceNetwork(urlSelectLocation, _dtnetde);

        setDdlData(ddlchamber, _dtnetde.m0chamber_list, "Locname", "LocIDX");

    }

    protected void SelectChamberList(DropDownList ddlName)
    {

        data_networkdevices _dtnetde = new data_networkdevices();
        _dtnetde.m0chamber_list = new m0chamber_detail[1];
        m0chamber_detail chamber_add = new m0chamber_detail();

        chamber_add.CHIDX = 0;

        _dtnetde.m0chamber_list[0] = chamber_add;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _dtnetde = callServiceNetwork(urlSelectChamber, _dtnetde);

        setDdlData(ddlName, _dtnetde.m0chamber_list, "Chamber_name", "CHIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกห้องที่ตั้งอุปกรณ์...", "0"));
    }
    */

    protected void SelectFloorList(DropDownList ddlName)
    {

        data_networkdevices _dtnetde = new data_networkdevices();
        _dtnetde.m0floor_list = new m0floor_detail[1];

        m0floor_detail Floor_add = new m0floor_detail();

        Floor_add.FLIDX = 0;

        _dtnetde.m0floor_list[0] = Floor_add;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _dtnetde = callServiceNetwork(urlSelectFloor, _dtnetde);

        setDdlData(ddlName, _dtnetde.m0floor_list, "Floor_name", "FLIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกชั้นที่ตั้งอุปกรณ์...", "0"));
    }

    protected void SelectImageList(DropDownList ddlName)
    {

        data_networkdevices _dtnetde = new data_networkdevices();
        _dtnetde.m0image_list = new m0image_detail[1];

        m0image_detail image_add = new m0image_detail();

        image_add.FLIDX = 0;

        _dtnetde.m0image_list[0] = image_add;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _dtnetde = callServiceNetwork(urlSelectImage, _dtnetde);

        setDdlData(ddlName, _dtnetde.m0image_list, "img_name", "imgidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกรูปภาพ...", "0"));
    }


    protected void SelectRegisNumber(DropDownList ddlName)
    {

        data_networkdevices _dtnetde = new data_networkdevices();
        _dtnetde.r0position_list = new r0position_detail[1];

        r0position_detail r0_add = new r0position_detail();

        r0_add.REIDX = 0;

        _dtnetde.r0position_list[0] = r0_add;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _dtnetde = callServiceNetwork(urlSelectRegisNumber, _dtnetde);

        setDdlData(ddlName, _dtnetde.r0position_list, "register_number", "u0idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกอุปกรณ์...", "0"));
    }

    #endregion

    #region Directories_File URL

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        DataRow dr = dt.NewRow();
        DataColumn dc = new DataColumn("FileName", typeof(string));
        dc = new DataColumn("Download", typeof(string));
        dt.Columns.Add("FileName");
        dt.Columns.Add("Download");

        FileInfo[] files = dir.GetFiles();
        int i = 0;
        foreach (FileInfo file in files)
        {
            if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
            {
                string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                dt.Rows.Add(file.Name);
                dt.Rows[i][1] = f[0];
                i++;
            }
        }

        GridView GvMaster = (GridView)ViewIndex.FindControl("GvMaster");
        // Panel gvFileLo1 = (Panel)FormViewDetails_CMP.FindControl("gvFileLo1");

        if (dt.Rows.Count > 0)
        {

            ds.Tables.Add(dt);
            //gvFileLo1.Visible = true;
            GvMaster.DataSource = ds.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            GvMaster.DataBind();
            ds.Dispose();
        }
        else
        {

            GvMaster.DataSource = null;
            GvMaster.DataBind();

        }

    }

    #endregion


    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        switch (ddName.ID)
        {
            case "ddlimage":

                div_position.Visible = true;

                Image img = (Image)ViewInsert.FindControl("img");

                SetData();

                string path = ddlimage.SelectedItem.Text + "/";
                string namefile = ddlimage.SelectedItem.Text + ".jpg";

                string getPathLotus = ConfigurationSettings.AppSettings["pathfile_networklayout"];


                img.ImageUrl = (getPathLotus + path + namefile);

                //    litDebug.Text = getPathLotus + path + namefile;

                break;
        }
    }

    #endregion

    #region SetFunction

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        ddlName.Items.Clear();
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected void ClearDefault()
    {
        DropDownList ddlStatus = (DropDownList)ViewInsert.FindControl("ddlStatus");
        //DropDownList ddlchamber = (DropDownList)ViewInsert.FindControl("ddlchamber");
        //DropDownList ddlfloor = (DropDownList)ViewInsert.FindControl("ddlfloor");
        //DropDownList ddlregis = (DropDownList)ViewInsert.FindControl("ddlregis");

        ddlStatus.Items.Clear();
        ddlStatus.AppendDataBoundItems = true;
        ddlStatus.Items.Add(new ListItem("Online", "1"));
        ddlStatus.Items.Add(new ListItem("Offline", "0"));

        // SelectLocation();
        //  SelectChamberList(ddlchamber);
        SelectImageList(ddlimage);
        //SelectFloorList(ddlfloor);
        SelectRegisNumber(ddlregis);
    }

    protected void SetData() //Bind Gridview
    {
        DataTable dt = new DataTable();

        if (dt.Columns.Count == 0)
        {
            for (int j = 0; j < 50; j++)
            {
                for (int b = j; b < 50; b++)
                {
                    dt.Columns.Add(Convert.ToString(b), typeof(string));
                    DataRow dr = dt.NewRow();
                    dr[j] = "";
                    dt.Rows.Add(dr);

                    j++;
                }
            }
        }


        GvCount.DataSource = dt;// files;
        GvCount.DataBind();
    }

    #endregion

    #region GridView

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                SelectList();
                mergeCell();

                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {

                        var ddlFloorUpdate = (DropDownList)e.Row.FindControl("ddlFloorUpdate");
                        var lblu0idx = (Label)e.Row.FindControl("lblu0idx");
                        var ddlRegisUpdate = (DropDownList)e.Row.FindControl("ddlRegisUpdate");
                        SelectRegisNumber(ddlRegisUpdate);
                        ddlRegisUpdate.SelectedValue = lblu0idx.Text;

                    }

                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                else if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[4].FindControl("btnDL11");  //Cells[3] เลือกเซลล์ว่าช่องที่ต้องการแสดงอยู่คอลัมน์ไหน
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[4].FindControl("hidFile11");

                    // TextBox1.Text = hidFile11.Value;
                    // Display the company name in italics.
                    //string LinkHost11 = string.Format("http://{0}", Request.Url.Host);


                    string getPath = ConfigurationSettings.AppSettings["pathfile_networklayout"];
                    if (Directory.Exists(Server.MapPath(getPath + hidFile11.Value)))
                    {
                        btnDL11.Visible = true;
                        btnDL11.NavigateUrl = getPath + hidFile11.Value + "/" + hidFile11.Value + ".jpg";//LinkHost11 + MapURL(hidFile11.Value);
                    }
                    else
                    {
                        btnDL11.Visible = false;

                    }

                }


                break;

            case "GvCount":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    int index = e.Row.RowIndex;
                    string dd = "40";
                    if (index == 27)
                    {
                        e.Row.Cells[int.Parse(dd)].ToolTip = "แสดงข้อมูลอุปกรณ์ได้แก่ รหัสทะเบียนอุปกรณ์ ประเภทอุปกรณ์ ชนิดอุปกรณ์ ไอพี เลขอุปกรณ์"; //e.Row.Cells[i].Text;
                        e.Row.Cells[int.Parse(dd)].Text = "ee";
                    }
                    else if (index == 38)
                    {
                        e.Row.Cells[int.Parse(dd)].ToolTip = "111"; //e.Row.Cells[i].Text;

                    }
                }
                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectList();

                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                int REIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var ddlRegisUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlRegisUpdate");
                //var ddlFloorUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlFloorUpdate");
                //var ddlChamberUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlChamberUpdate");
                var ddlStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlStatusUpdate");
                var txtnoregis_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtnoregis_update");
                var txtrowupdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtrowupdate");
                var txtcellupdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtcellupdate");

                GvMaster.EditIndex = -1;

                _dtnetde.r0position_list = new r0position_detail[1];
                r0position_detail _r0posit_update = new r0position_detail();

                _r0posit_update.REIDX = REIDX;
                // _r0posit_update.CHIDX = int.Parse(ddlChamberUpdate.SelectedValue);
                //    _r0posit_update.FLIDX = int.Parse(ddlFloorUpdate.SelectedValue);
                _r0posit_update.u0idx = int.Parse(ddlRegisUpdate.SelectedValue);
                _r0posit_update.no_regis = txtnoregis_update.Text;
                _r0posit_update.row_ = int.Parse(txtrowupdate.Text);
                _r0posit_update.cell_ = int.Parse(txtcellupdate.Text);

                _r0posit_update.RE_status = int.Parse(ddlStatusUpdate.SelectedValue);
                _r0posit_update.CEmpIDX = emp_idx;

                _dtnetde.r0position_list[0] = _r0posit_update;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));
                _dtnetde = callServiceNetwork(urlInsertPosition, _dtnetde);

                if (_dtnetde.return_code == 0)
                {
                    //initPage();
                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);  
                    SelectList();


                }
                else
                {
                    setError(_dtnetde.return_code.ToString() + " - " + _dtnetde.return_msg);
                }


                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectList();
                break;
        }
    }

    #endregion

    #region MergeCell
    protected void mergeCell()
    {
        for (int rowIndex = GvMaster.Rows.Count - 2; rowIndex >= 0; rowIndex--)
        {
            GridViewRow currentRow = GvMaster.Rows[rowIndex];
            GridViewRow previousRow = GvMaster.Rows[rowIndex + 1];

            if (((Label)currentRow.Cells[1].FindControl("Locname")).Text == ((Label)previousRow.Cells[1].FindControl("Locname")).Text &&
                ((Label)currentRow.Cells[2].FindControl("room_name")).Text == ((Label)previousRow.Cells[2].FindControl("room_name")).Text &&
                ((Label)currentRow.Cells[3].FindControl("Floor_name")).Text == ((Label)previousRow.Cells[3].FindControl("Floor_name")).Text)
            {
                if (previousRow.Cells[1].RowSpan < 2 && previousRow.Cells[2].RowSpan < 2 && previousRow.Cells[3].RowSpan < 2 )
                {
                    currentRow.Cells[1].RowSpan = 2;
                    currentRow.Cells[2].RowSpan = 2;
                    currentRow.Cells[3].RowSpan = 2;
                    currentRow.Cells[8].RowSpan = 2;
                }
                else
                {
                    currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;
                    currentRow.Cells[2].RowSpan = previousRow.Cells[2].RowSpan + 1;
                    currentRow.Cells[3].RowSpan = previousRow.Cells[3].RowSpan + 1;
                    currentRow.Cells[8].RowSpan = previousRow.Cells[8].RowSpan + 1;
                }
                previousRow.Cells[1].Visible = false;
                previousRow.Cells[2].Visible = false;
                previousRow.Cells[3].Visible = false;
                previousRow.Cells[8].Visible = false;
            }
        }
    }

    #endregion

    #region BTN
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        int _cemp_idx;

        r0position_detail _r0posit = new r0position_detail();

        switch (cmdName)
        {
            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);
                ClearDefault();
                break;

            case "btnInsert":
                // DropDownList ddlStatus = ((DropDownList)ViewInsert.FindControl("ddlStatus"));
                _cemp_idx = emp_idx;

                _dtnetde.r0position_list = new r0position_detail[1];
                // _r0posit.CHIDX = int.Parse(ddlchamber.SelectedValue);
                _r0posit.imgidx = int.Parse(ddlimage.SelectedValue);
                _r0posit.u0idx = int.Parse(ddlregis.SelectedValue);
                _r0posit.row_ = int.Parse(txtrow.Text);
                _r0posit.cell_ = int.Parse(txtcell.Text);
                _r0posit.no_regis = txtaddnoregis.Text;
                _r0posit.RE_status = int.Parse(ddlStatus.SelectedValue);
                _r0posit.CEmpIDX = _cemp_idx;

                _dtnetde.r0position_list[0] = _r0posit;
                //   litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtnetde));

                _dtnetde = callServiceNetwork(urlInsertPosition, _dtnetde);

                if (_dtnetde.return_code == 0)
                {
                    //ClearDefault();

                    MvMaster.SetActiveView(ViewIndex);
                    SelectList();
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);

                }
                else
                {
                    setError(_dtnetde.return_code.ToString() + " - " + _dtnetde.return_msg);
                }
                break;


            case "btnCancel":

                ClearDefault();
                SelectList();
                MvMaster.SetActiveView(ViewIndex);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnDelete":

                int REIDX = int.Parse(cmdArg);
                _cemp_idx = emp_idx;


                _dtnetde.r0position_list = new r0position_detail[1];
                _r0posit.REIDX = REIDX;
                _r0posit.CEmpIDX = _cemp_idx;

                _dtnetde.r0position_list[0] = _r0posit;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                _dtnetde = callServiceNetwork(urlDeletePosition, _dtnetde);


                if (_dtnetde.return_code == 0)
                {

                    SelectList();

                }
                else
                {
                    setError(_dtnetde.return_code.ToString() + " - " + _dtnetde.return_msg);
                }

                break;
        }
    }
    #endregion


}