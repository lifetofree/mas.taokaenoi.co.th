<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true"
    CodeFile="hr_checkin_report.aspx.cs" Inherits="websystem_hr_hr_checkin_report" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
    </asp:Content>

    <asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
        <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>

        <!--tab menu-->
        <div id="divMenu" runat="server" class="col-md-12">
            <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="sub-navbar">
                            <a class="navbar-brand" href="#"><b>Menu</b></a>
                        </div>
                    </div>

                    <!--Collect the nav links, forms, and other content for toggling-->
                    <div class="collapse navbar-collapse" id="menu-bar">
                        <ul class="nav navbar-nav" id="uiNav" runat="server">
                            <li id="li0" runat="server">
                                <asp:LinkButton ID="lbList" runat="server" CommandName="navList" OnCommand="navCommand">
                                    รายงาน</asp:LinkButton>
                            </li>
                            <li id="li1" runat="server" Visible="False">
                                <asp:LinkButton ID="lbSetting" runat="server" CommandName="navSetting"
                                    OnCommand="navCommand">
                                    พื้นที่เสี่ยง</asp:LinkButton>
                            </li>
                            <!-- <li id="li2" runat="server">
                            <asp:LinkButton ID="lbReport" runat="server" CommandName="navReport" OnCommand="navCommand">
                                ส่วนของ HR</asp:LinkButton>
                        </li> -->
                        </ul>
                    </div>
                    <!--Collect the nav links, forms, and other content for toggling-->
                </div>
            </nav>
        </div>
        <!--tab menu-->

        <asp:MultiView ID="mvSystem" runat="server">
            <asp:View ID="viewList" runat="server">
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">ค้นหาข้อมูล</h3>
                        </div>
                        <div class="panel-body">
                            <asp:FormView ID="fvSearch" runat="server" Width="100%" DefaultMode="Insert">
                                <InsertItemTemplate>
                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">วันที่เริ่มต้น</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbStartDate" runat="server"
                                                        CssClass="form-control clockpicker" MaxLength="20"
                                                        placeholder="วันที่เริ่มต้น"></asp:TextBox>
                                                </div>
                                                <label class="col-md-2 control-label">วันที่สิ้นสุด</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbEndDate" runat="server"
                                                        CssClass="form-control clockpicker-to" MaxLength="20"
                                                        placeholder="วันที่สิ้นสุด"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">รหัสพนักงาน</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control"
                                                        MaxLength="200" placeholder="รหัสพนักงาน"></asp:TextBox>
                                                </div>
                                                <label class="col-md-2 control-label">ชื่อ-นามสกุล</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control"
                                                        MaxLength="200" placeholder="ชื่อ-นามสกุล"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">องค์กร</label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlOrganization" runat="server"
                                                        Cssclass="form-control"
                                                        OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                        AutoPostBack="true"></asp:DropDownList>
                                                </div>
                                                <label class="col-md-2 control-label">กลุ่มงาน</label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlWorkGroup" runat="server"
                                                        Cssclass="form-control"
                                                        OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">สายงาน</label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlLineWork" runat="server"
                                                        Cssclass="form-control"
                                                        OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                                <label class="col-md-2 control-label">ฝ่าย</label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlDepartment" runat="server"
                                                        Cssclass="form-control"
                                                        OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                        AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">แผนก</label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlSection" runat="server"
                                                        Cssclass="form-control" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                </div>
                                                <label class="col-md-2"></label>
                                                <label class="col-md-4"></label>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2"></label>
                                                <div class="col-md-10">
                                                    <asp:LinkButton ID="lbSearchData" runat="server"
                                                        CssClass="btn btn-md btn-primary" OnCommand="btnCommand"
                                                        CommandName="cmdSearch"><i class="fas fa-search"></i>&nbsp;ค้นหา
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="lbResetData" runat="server"
                                                        CssClass="btn btn-md btn-info" OnCommand="btnCommand"
                                                        CommandName="cmdReset"><i
                                                            class="fas fa-redo"></i>&nbsp;เคลียร์ค่า
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="lbExportData" runat="server"
                                                        CssClass="btn btn-md btn-success" OnCommand="btnCommand"
                                                        CommandName="cmdExport"><i
                                                            class="far fa-file-excel"></i>&nbsp;Export
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <asp:GridView ID="gvDataList" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                        ShowFooter="False" OnRowDataBound="gvRowDataBound" PageSize="10"
                        OnPageIndexChanging="gvPageIndexChanging"
                        CssClass="table table-striped table-bordered table-responsive">
                        <HeaderStyle CssClass="info" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                            LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รหัสพนักงาน">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpCode" runat="server" Text='<%# Eval("emp_code") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpNameTh" runat="server" Text='<%# Eval("emp_name_th") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Latitude">
                                <ItemTemplate>
                                    <asp:Label ID="lblLatitude" runat="server" Text='<%# Eval("latitude") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Longitude">
                                <ItemTemplate>
                                    <asp:Label ID="lblLongitude" runat="server" Text='<%# Eval("longitude") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วันเวลาที่เช็คอิน">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("create_date") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Checkin Name" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblCheckinName" runat="server" Text='<%# Eval("checkin_name") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สถานที่เช็คอิน">
                                <ItemTemplate>
                                    <asp:Label ID="lblStreet" runat="server" Text='<%# Eval("street") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รหัสประเทศ">
                                <ItemTemplate>
                                    <asp:Label ID="lblCountryCode" runat="server"
                                        Text='<%# Eval("iso_country_code") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ประเทศ">
                                <ItemTemplate>
                                    <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("country") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รหัสไปรษณีย์">
                                <ItemTemplate>
                                    <asp:Label ID="lblPostalCode" runat="server" Text='<%# Eval("postal_code") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="จังหวัด">
                                <ItemTemplate>
                                    <asp:Label ID="lblAdminArea" runat="server" Text='<%# Eval("admin_area") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="อำเภอ">
                                <ItemTemplate>
                                    <asp:Label ID="lblSubAdminArea" runat="server" Text='<%# Eval("sub_admin_area") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ตำบล">
                                <ItemTemplate>
                                    <asp:Label ID="lblLocality" runat="server" Text='<%# Eval("locality") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sub Locality" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblSubLocality" runat="server" Text='<%# Eval("sub_locality") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Thoroughfare" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblThoroughfare" runat="server" Text='<%# Eval("thoroughfare") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Sub Thoroughfare" Visible="False">
                                <ItemTemplate>
                                    <asp:Label ID="lblSubThoroughfare" runat="server"
                                        Text='<%# Eval("sub_thoroughfare") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="องค์กร">
                                <ItemTemplate>
                                    <asp:Label ID="lblOrganization" runat="server" Text='<%# Eval("org_name_th") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="กลุ่มงาน">
                                <ItemTemplate>
                                    <asp:Label ID="lblWorkGroup" runat="server" Text='<%# Eval("wg_name_th") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สายงาน">
                                <ItemTemplate>
                                    <asp:Label ID="lblLineWork" runat="server" Text='<%# Eval("lw_name_th") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ฝ่าย">
                                <ItemTemplate>
                                    <asp:Label ID="lblDepartment" runat="server" Text='<%# Eval("dept_name_th") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="แผนก">
                                <ItemTemplate>
                                    <asp:Label ID="lblSection" runat="server" Text='<%# Eval("sec_name_th") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
            </asp:View>
            <asp:View ID="viewSetting" runat="server">
                <div class="col-md-12">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">พื้นที่เสี่ยง</h3>
                        </div>
                        <div class="panel-body">
                            <asp:FormView ID="fvSettingList" runat="server" Width="100%" DefaultMode="Insert">
                                <InsertItemTemplate>
                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Postal Code</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbPostalCode" runat="server"
                                                        CssClass="form-control" MaxLength="5"></asp:TextBox>
                                                </div>
                                                <label class="col-md-2 control-label">Province</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbProvince" runat="server" CssClass="form-control"
                                                        MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Amphure</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbAmphure" runat="server" CssClass="form-control"
                                                        MaxLength="100"></asp:TextBox>
                                                </div>
                                                <label class="col-md-2 control-label">District</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbDistrict" runat="server" CssClass="form-control"
                                                        MaxLength="100"></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Places</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbPlaces" runat="server" CssClass="form-control"
                                                        MaxLength="200"></asp:TextBox>
                                                </div>
                                                <label class="col-md-2 control-label">Zone</label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlZone" runat="server"
                                                        Cssclass="form-control">
                                                        <asp:ListItem Text="แดง" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="ส้ม" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="เหลือง" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="เขียว" Value="4"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label"></label>
                                                <div class="col-md-10">
                                                    <asp:LinkButton ID="lbSaveForm" runat="server"
                                                        CssClass="btn btn-md btn-success" OnCommand="btnCommand"
                                                        CommandName="cmdSaveForm" CommandArgument="0"><i
                                                            class="far fa-save"></i>&nbsp;บันทึก
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="lbResetForm" runat="server"
                                                        CssClass="btn btn-md btn-info" OnCommand="btnCommand"
                                                        CommandName="cmdResetForm"><i
                                                            class="fas fa-redo"></i>&nbsp;เคลียร์ค่า
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </InsertItemTemplate>
                                <EditItemTemplate>
                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Postal Code</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbPostalCode" runat="server"
                                                        CssClass="form-control" MaxLength="5"
                                                        Text='<%#Eval("postal_code") %>'></asp:TextBox>
                                                </div>
                                                <label class="col-md-2 control-label">Province</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbProvince" runat="server" CssClass="form-control"
                                                        MaxLength="100" Text='<%#Eval("province") %>'></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Amphure</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbAmphure" runat="server" CssClass="form-control"
                                                        MaxLength="100" Text='<%#Eval("amphure") %>'></asp:TextBox>
                                                </div>
                                                <label class="col-md-2 control-label">District</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbDistrict" runat="server" CssClass="form-control"
                                                        MaxLength="100" Text='<%#Eval("district") %>'></asp:TextBox>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Places</label>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="tbPlaces" runat="server" CssClass="form-control"
                                                        MaxLength="200" Text='<%#Eval("places") %>'></asp:TextBox>
                                                </div>
                                                <label class="col-md-2 control-label">Zone</label>
                                                <div class="col-md-4">
                                                    <asp:DropDownList ID="ddlZone" runat="server"
                                                        Cssclass="form-control"
                                                        SelectedValue='<%#Eval("status_zone") %>'>
                                                        <asp:ListItem Text="แดง" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="ส้ม" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="เหลือง" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="เขียว" Value="4"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label"></label>
                                                <div class="col-md-10">
                                                    <asp:LinkButton ID="lbSaveForm" runat="server"
                                                        CssClass="btn btn-success" OnCommand="btnCommand"
                                                        CommandName="cmdSaveForm"
                                                        CommandArgument='<%#Eval("m0_idx") %>'><i
                                                            class="far fa-save"></i>&nbsp;บันทึก
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="lbCancelForm" runat="server"
                                                        CssClass="btn btn-danger" OnCommand="btnCommand"
                                                        CommandName="cmdCancelForm"><i
                                                            class="fas fa-times"></i>&nbsp;ยกเลิก
                                                    </asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </EditItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <asp:GridView ID="gvSettingList" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                        ShowFooter="False" OnRowDataBound="gvRowDataBound" PageSize="10"
                        OnPageIndexChanging="gvPageIndexChanging"
                        CssClass="table table-striped table-bordered table-responsive">
                        <HeaderStyle CssClass="info" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                            LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Postal Code">
                                <ItemTemplate>
                                    <asp:Label ID="lblPostalCode" runat="server" Text='<%# Eval("postal_code") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Province">
                                <ItemTemplate>
                                    <asp:Label ID="lblProvince" runat="server" Text='<%# Eval("province") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Amphure">
                                <ItemTemplate>
                                    <asp:Label ID="lblAmphure" runat="server" Text='<%# Eval("amphure") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="District">
                                <ItemTemplate>
                                    <asp:Label ID="lblDistrict" runat="server" Text='<%# Eval("district") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Places">
                                <ItemTemplate>
                                    <asp:Label ID="lblPlaces" runat="server" Text='<%# Eval("places") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Zone">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlZone" runat="server" Cssclass="form-control"
                                        SelectedValue='<%# Eval("status_zone") %>' Enabled="False">
                                        <asp:ListItem Text="แดง" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="ส้ม" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="เหลือง" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="เขียว" Value="4"></asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbEdit" CssClass="btn btn-warning" runat="server"
                                        CommandName="cmdEdit" OnCommand="btnCommand"
                                        CommandArgument='<%#Eval("m0_idx") %>'><i
                                            class="glyphicon glyphicon-edit"></i>&nbsp;แก้ไข</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </asp:View>
        </asp:MultiView>

        <script type="text/javascript">
            //no post back form
            $(document).ready(TimePickerCtrl);
            function TimePickerCtrl($) {

                var startTime = $('.clockpicker').datetimepicker({
                    format: 'DD/MM/YYYY',


                });

                var endTime = $('.clockpicker-to').datetimepicker({
                    format: 'DD/MM/YYYY',
                    // minDate: startTime.data("DateTimePicker").date(),

                });


                $('.clockpicker').on('dp.change', function (e) {
                    var dateTo = $('.clockpicker-to').data("DateTimePicker").date();
                    if (e.date > dateTo) {
                        $('.clockpicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                    }

                });

                $('.clockpicker-to').on('dp.change', function (e) {
                    var dateFrom = $('.clockpicker').data("DateTimePicker").date();
                    if (e.date < dateFrom) {
                        $('.clockpicker').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                    }
                });


                function setMinDate() {
                    return endTime
                        .data("DateTimePicker").minDate(
                            startTime.data("DateTimePicker").date()
                        )
                        ;
                }
                var bound = false;
                function bindMinEndTimeToStartTime() {

                    return bound || startTime.on('dp.change', setMinDate);
                }

                endTime.on('dp.change', () => {
                    bindMinEndTimeToStartTime();
                    bound = true;
                    setMinDate();
                });

            }
            //is form
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {

                $(document).ready(TimePickerCtrl);
                function TimePickerCtrl($) {

                    var startTime = $('.clockpicker').datetimepicker({
                        format: 'DD/MM/YYYY',


                    });

                    var endTime = $('.clockpicker-to').datetimepicker({
                        format: 'DD/MM/YYYY',
                        //minDate: startTime.data("DateTimePicker").date(),

                    });

                    $('.clockpicker').click(function () {
                        $('.clockpicker').data("DateTimePicker").show();
                    });
                    $('.clockpicker').on('dp.change', function (e) {
                        var dateTo = $('.clockpicker-to').data("DateTimePicker").date();
                        if (e.date > dateTo) {
                            $('.clockpicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                        }

                    });
                    $('.clockpicker-to').on('dp.change', function (e) {
                        var dateFrom = $('.clockpicker').data("DateTimePicker").date();
                        if (e.date < dateFrom) {
                            $('.clockpicker').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                        }
                    });

                    //$('.show-from-onclick').click(function () {
                    //    $('.datetimepicker-from').data("DateTimePicker").show();
                    //});


                    function setMinDate() {
                        return endTime
                            .data("DateTimePicker").minDate(
                                startTime.data("DateTimePicker").date()
                            )
                            ;
                    }
                    var bound = false;
                    function bindMinEndTimeToStartTime() {

                        return bound || startTime.on('dp.change', setMinDate);
                    }

                    endTime.on('dp.change', () => {
                        bindMinEndTimeToStartTime();
                        bound = true;
                        setMinDate();
                    });

                }

            });
        </script>
    </asp:Content>