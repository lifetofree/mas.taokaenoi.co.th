﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="softwarelicense.aspx.cs" Inherits="websystem_ITServices_softwarelicense" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        })
    </script>

    <script>
        $(function () {

            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepickerexpire').datetimepicker({
                    format: 'DD/MM/YYYY',
                    minDate: new Date()

                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        })
    </script>

    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <!--*** START Menu ***-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav" runat="server">
                    <li id="liToIndexList" runat="server">
                        <asp:LinkButton ID="btnToIndexList" runat="server"
                            CommandName="btnToIndexList"
                            OnCommand="btnCommand" Text="ข้อมูลทั่วไป" />
                    </li>

                    <%-- <% if(Session["emp_idx"].ToString() == "172" || Session["emp_idx"].ToString() == "1413" || Session["emp_idx"].ToString() == "178" || Session["emp_idx"].ToString() == "3657" || Session["emp_idx"].ToString() == "3593" || Session["emp_idx"].ToString() == "3834" || Session["emp_idx"].ToString() == "3752") { %>--%>
                    <li id="li1" runat="server">
                        <asp:LinkButton ID="btnToDeviceSoftware" Visible="false" runat="server"
                            CommandName="btnToDeviceSoftware"
                            OnCommand="btnCommand" Text="เพิ่มรายการลิขสิทธิ์" />
                    </li>
                    <%--<% } %>--%>


                    <%-- <% if(Session["emp_idx"].ToString() == "172" || Session["emp_idx"].ToString() == "1413" || Session["emp_idx"].ToString() == "178" || Session["emp_idx"].ToString() == "3657" || Session["emp_idx"].ToString() == "3593" || Session["emp_idx"].ToString() == "3834" || Session["emp_idx"].ToString() == "3752") { %>--%>
                    <li id="li4" runat="server">
                        <asp:LinkButton ID="btnToReport" Visible="false" runat="server"
                            CommandName="btnToReport"
                            OnCommand="btnCommand" Text="รายงาน" />
                    </li>
                    <%-- <% } %>--%>
                </ul>

                <ul class="nav navbar-nav navbar-right" runat="server">
                    
                    <li id="_divMenuLiToDocument" runat="server">
                        <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                             NavigateUrl="https://docs.google.com/document/d/1ERDtcQK9FCWQ3D2hkYnyAfBwc8FxKYXIhN0aLpVF9Ko" Target="_blank" Text="คู่มือการใช้งาน" />
                    </li>
                </ul>


            </div>
        </nav>
    </div>
    <!--*** END Menu ***-->

    <%----------- MultiView Start ---------------%>
    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <%----------- ViewIndex Software License Start ---------------%>
        <asp:View ID="ViewIndex" runat="server">

            <div class="form-group">
                <div class="col-md-12">
                    <asp:LinkButton ID="btnSearch" Visible="false" CssClass="btn btn-info" runat="server" data-original-title="Search" data-toggle="tooltip" CommandName="btnSearch" OnCommand="btnCommand"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>
                </div>
            </div>

            <!--*** START Back To Index ***-->
            <div class="form-group">
                <div class="col-md-12">
                    <div id="fvBacktoIndex" class="row" runat="server" visible="false">
                        <%--<div class="col-md-12">--%>
                        <asp:LinkButton ID="btnSearchBack" CssClass="btn btn-info" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnSearchBack"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back</asp:LinkButton>
                        <%--</div>--%>
                    </div>
                </div>

            </div>
            <!--*** END Back To Index ***-->

            <!--*** START Search Index ***-->
            <div class="form-group">
                <div class="col-md-12">
                    <div id="showsearch" runat="server" visible="false">
                        <br />
                        <div class="panel panel-info">
                            <div class="panel-heading f-bold">ค้นหารายการลิขสิทธิ์</div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- องค์กร,ฝ่าย --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lborg_idxsearch" CssClass="col-sm-2 control-label" runat="server" Text="องค์กร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlorg_idxsearch" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>

                                        </div>

                                        <asp:Label ID="lbrdept_idxsearch" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlrdept_idxsearch" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Text="เลือกฝ่าย ...." Value="00"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                    </div>
                                    <%-------------- องค์กร,ฝ่าย --------------%>

                                    <%-------------- แผนก,ชื่อพนักงาน --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbrsec_idxsearch" CssClass="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlrsec_idxsearch" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Text="เลือกแผนก ...." Value="00"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                        <asp:Label ID="lbemp_idxsearch" CssClass="col-sm-2 control-label" Visible="true" runat="server" Text="ชื่อพนักงาน : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlemp_idxsearch" runat="server" CssClass="form-control" Visible="true" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Text="เลือกพนักงาน ...." Value="00"></asp:ListItem>
                                            </asp:DropDownList>

                                        </div>

                                    </div>
                                    <%-------------- แผนก,ชื่อพนักงาน --------------%>

                                    <%-------------- btnsearch --------------%>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <%--<div class="col-sm-12"></div>--%>
                                            <div class="pull-left">
                                                <asp:LinkButton ID="btnSearchIndex_First" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchIndex" OnCommand="btnCommand" CommandName="btnSearchIndex_First" data-toggle="tooltip" title="Search" OnClientClick="return confirm('คุณต้องการค้นหารายการนี้ใช่หรือไม่ ?')"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>


                                                <asp:LinkButton ID="btnCancelIndex_First" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="btnCancelIndex_First" data-toggle="tooltip" title="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>

                                        </div>
                                    </div>
                                    <%-------------- btnsearch --------------%>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!--*** END Search Index ***-->

            <!--*** START GRIDVIEW Index ***-->
            <%-- col-md-12 m-t-10--%>
            <div class="col-md-12">

                <div id="gridviewindex" runat="server">
                    <br />
                    <asp:GridView ID="GvMaster"
                        runat="server"
                        AutoGenerateColumns="false"
                        DataKeyNames="u0_software_idx"
                        CssClass="table table-striped table-bordered table-responsive"
                        HeaderStyle-CssClass="info"
                        AllowPaging="true"
                        PageSize="10"
                        OnRowEditing="Master_RowEditing"
                        OnRowUpdating="Master_RowUpdating"
                        OnRowCancelingEdit="Master_RowCancelingEdit"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        OnRowDataBound="Master_RowDataBound">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">No result</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" Visible="true">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbu0_idx" runat="server" Visible="false" Text='<%# Eval("u0_software_idx") %>' />
                                        <asp:Label ID="lbunidx" runat="server" Visible="false" Text='<%# Eval("m0_node_idx") %>' />
                                        <%# (Container.DataItemIndex +1) %>
                                    </small>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbemp_idx" runat="server" Visible="false" Text='<%# Eval("emp_idx") %>' />
                                        <asp:Label ID="DetailSoftwareLicense" runat="server">


                                            <p><b>&nbsp;&nbsp;&nbsp;ชื่อผู้ถือครอง :</b> <%# Eval("name_license") %></p>   

                                            <p><b>&nbsp;&nbsp;&nbsp;องค์กร :</b> <%# Eval("org_name_th_license") %></p>                                                                             
                                            <p><b>&nbsp;&nbsp;&nbsp;ฝ่าย :</b> <%# Eval("dept_name_th_license") %></p>
                                       
                                            <p><b>&nbsp;&nbsp;&nbsp;แผนก :</b> <%# Eval("sec_name_th_license") %></p>

                                            <p><b>&nbsp;&nbsp;&nbsp;เครื่องที่ถือครอง :</b> <%# Eval("u0_code") %></p>

                                            <p><b>&nbsp;&nbsp;&nbsp;เลขที่ Asset :</b> <%# Eval("u0_acc") %></p>
                                        
                                            <p><b>&nbsp;&nbsp;&nbsp;เลขที่ PO :</b> <%# Eval("u0_po") %></p>

                                            <p><b>&nbsp;&nbsp;&nbsp;เลขที่ Serial :</b> <%# Eval("u0_serial") %></p>
                                                                                  
                                     
                                        </asp:Label>


                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่สร้าง" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="DateCreate" runat="server" Text='<%# Eval("date_create") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียด License" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>

                                        <asp:Label ID="DetailLicense" runat="server">

                                        <p><b>&nbsp;&nbsp;&nbsp;จำนวน License :</b> <%# Eval("count_license") %></p>                                       
                                     
                                        </asp:Label>

                                        <div class="panel-body">
                                            <table class="table table-striped f-s-12">
                                                <asp:Repeater ID="rpcountogdept" runat="server">
                                                    <HeaderTemplate>
                                                        <tr>
                                                            <%--<th>#</th>--%>
                                                            <th>ชื่อ software</th>
                                                            <th>จำนวน License</th>

                                                        </tr>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>

                                                            <%--<td data-th="#"><%# (Container.ItemIndex + 1) %></td>--%>
                                                            <td data-th="ชื่อ software"><%# Eval("software_name") %></td>
                                                            <td data-th="จำนวน License"><%# Eval("count_indept") %></td>

                                                        </tr>

                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                            </table>
                                        </div>

                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ผู้สร้างรายการ" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="NameCreate" runat="server" Text='<%# Eval("name_create") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="StatusDesc" runat="server" Text='<%#Eval("status_desc") %>' />

                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ดำเนินการ" ItemStyle-HorizontalAlign="center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnViewindex" CssClass="btn btn-primary" Font-Size="11px" runat="server" CommandName="btnViewindex" OnCommand="btnCommand" data-toggle="tooltip" title="ดูข้อมูล" CommandArgument='<%# Eval("u0_software_idx") + ";" + Eval("m0_node_idx") %>'><i class="fa fa-file"></i></asp:LinkButton>
                                    <br />
                                    <asp:Label ID="status_emp_computer" Visible="true" Text='<%# getStatusemp((int)Eval("emp_idx")) %>' runat="server" />

                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <!--*** END GRIDVIEW Index ***-->

            <!--*** START Show Data in Viewindex ***-->
            <div class="row" id="div_showdata" runat="server" visible="false">
                <div class="col-md-12" id="panelview" runat="server" visible="false">
                    <div class="panel panel-info">
                        <div id="divshownamedetail" runat="server" visible="false" class="panel-heading f-bold">รายละเอียด Software License ที่ถือครอง</div>
                    </div>
                </div>

                <div class="form-group">
                    <asp:FormView ID="FvViewDetail" runat="server" OnDataBound="FvDetail_DataBound" class="col-sm-12 font_text">

                        <ItemTemplate>

                            <asp:HiddenField ID="hf_m0_node_idx" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                            <asp:HiddenField ID="hf_m0_actor_idx" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                            <asp:HiddenField ID="hf_doc_decision" runat="server" Value='<%# Eval("doc_decision") %>' />


                            <div class="col-md-12">
                                <div class="form-horizontal" role="form">
                                    <div class="panel panel-default">

                                        <div class="panel-heading">
                                            <br />


                                            <div class="form-group">
                                                <asp:Label ID="lbname_licenseview" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อผู้ถือครอง : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtemp_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("emp_idx") %>'>
                                                    </asp:TextBox>
                                                    <asp:TextBox ID="txtname_license_view" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("name_license") %>'>
                                                    </asp:TextBox>
                                                </div>

                                                <asp:Label ID="lborg_name_th_licenseview" CssClass="col-sm-2 control-label" runat="server" Text="องค์กรผู้ถือครอง : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtorg_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("org_idx") %>'>
                                                    </asp:TextBox>
                                                    <asp:TextBox ID="txtorg_name_th_licenseview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th_license") %>'>
                                                    </asp:TextBox>

                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="lbdept_name_th_licenseview" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่ายผู้ถือครอง : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtrdept_idxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("rdept_idx") %>'>
                                                    </asp:TextBox>
                                                    <asp:TextBox ID="txtdept_name_th_licenseview" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("dept_name_th_license") %>'>
                                                    </asp:TextBox>
                                                </div>

                                                <asp:Label ID="lbsec_name_th_licenseview" CssClass="col-sm-2 control-label" runat="server" Text="แผนกผู้ถือครอง : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtrsec_idxview" Enabled="false" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("rsec_idx") %>'>
                                                    </asp:TextBox>
                                                    <asp:TextBox ID="txtsec_name_th_licenseview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th_license") %>'>
                                                    </asp:TextBox>

                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="lbu0_codeview" CssClass="col-sm-2 control-label" runat="server" Text="เครื่องที่ถือครอง : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtu0_didxview" runat="server" Visible="false" Enabled="false" CssClass="form-control" Text='<%# Eval("u0_didx") %>'>
                                                    </asp:TextBox>
                                                    <asp:TextBox ID="txtu0_codeview" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("u0_code") %>'>
                                                    </asp:TextBox>
                                                </div>

                                                <asp:Label ID="lbu0_accview" CssClass="col-sm-2 control-label" runat="server" Text="เลขที่ Asset : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtu0_accview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("u0_acc") %>'>
                                                    </asp:TextBox>

                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="lbu0_poview" CssClass="col-sm-2 control-label" runat="server" Text="เลขที่ PO : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtu0_poview" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("u0_po") %>'>
                                                    </asp:TextBox>
                                                </div>

                                                <asp:Label ID="lbu0_serialview" CssClass="col-sm-2 control-label" runat="server" Text="เลขที่ Serial : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtu0_serialview" Enabled="false" runat="server" CssClass="form-control" Text='<%# Eval("u0_serial") %>'>
                                                    </asp:TextBox>

                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                        </ItemTemplate>
                    </asp:FormView>
                </div>

            </div>
            <!--*** END Show Data in Viewindex ***-->


            <!-- START Gridview จำนวน Software ที่ถือครอง -->
            <div class="form-group" visible="false" runat="server" id="gridview_showsoftware">

                <div class="col-md-12" id="div_showdetailswname" runat="server" visible="false">
                    <div class="panel panel-info">
                        <div id="div_swname" runat="server" visible="false" class="panel-heading f-bold">รายละเอียด Software </div>
                    </div>
                </div>



                <div class="col-sm-12">


                    <asp:GridView ID="GvSoftwaredetail"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-responsive"
                        HeaderStyle-CssClass="primary"
                        AllowPaging="true"
                        PageSize="10"
                        OnRowEditing="Master_RowEditing"
                        OnRowUpdating="Master_RowUpdating"
                        OnRowCancelingEdit="Master_RowCancelingEdit"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        OnRowDataBound="Master_RowDataBound">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">No result</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" Visible="true">
                                <ItemTemplate>
                                    <small>
                                        <%--<asp:Label ID="lbu0_idx" runat="server" Visible="false" Text='<%# Eval("u0_software_idx") %>' />
                                                <asp:Label ID="lbunidx" runat="server" Visible="false" Text='<%# Eval("m0_node_idx") %>' />--%>
                                        <%# (Container.DataItemIndex +1) %>
                                    </small>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อ software" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>

                                        <asp:Label ID="lblsoftware_nameview" runat="server" Text='<%# Eval("software_name") %>' />
                                        <asp:Label ID="lbsoftware_name_idxview" Visible="false" runat="server" Text='<%# Eval("software_name_idx") %>' />

                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภท Software" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbtype_nameview" runat="server" Text='<%# Eval("type_name") %>' />
                                        <asp:Label ID="lbsoftware_typeview" Visible="false" runat="server" Text='<%# Eval("software_type") %>' />

                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน License" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>

                                        <asp:Label ID="lbcount_indeptview" runat="server" Text='<%# Eval("count_indept") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>


                    <%--</div>--%>
                    <%--</div>--%>
                </div>


            </div>

            <!--END  Gridview จำนวน Software ที่ถือครอง -->


            <%-------------- START DDL Approve Status --------------%>
            <div class="form-group">
                <div class="col-md-12">
                    <div id="div_approvstatecreate" class="row panel panel-default font_text" runat="server" visible="false">
                        <%--<div class="panel panel-info">--%>
                        <div class="panel-heading f-bold">อนุมัติรายการ Software License</div>
                        <div class="form-group" id="approvecreate" runat="server">

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <%--<div class="col-sm-1"></div>--%>
                                        <asp:Label ID="lbapprovecreate" CssClass="col-md-2 control-label" runat="server" Text="ผลการอนุมัติ :" />
                                        <div class="col-sm-3">
                                            <asp:DropDownList CssClass="form-control" ID="ddl_approvestatus" runat="server">
                                                <asp:ListItem Value="0">กรุณาเลือกสถานะ...</asp:ListItem>
                                                <asp:ListItem Value="1">รับทราบ</asp:ListItem>
                                                <%--<asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>--%>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ValidationGroup="SaveApprove1" runat="server" Display="None"
                                                ControlToValidate="ddl_approvestatus" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกสถานะ"
                                                ValidationExpression="กรุณาเลือกสถานะ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator9" Width="160" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <%--<div class="col-sm-1"></div>--%>
                                        <asp:Label ID="lbtxtComment_approve" CssClass="col-md-2 control-label" runat="server" Text="ความเห็น :" />
                                        <div class="col-sm-3">
                                            <asp:TextBox CssClass="form-control" ID="txtComment_approve" TextMode="multiline" Rows="2" placeholder="แสดงความเห็นเพิ่มเติม ..." Width="450pt" runat="server">
                                                
                                            </asp:TextBox>

                                        </div>

                                    </div>

                                </div>


                            </div>
                        </div>
                        <%-- </div>--%>
                    </div>
                    <br />

                </div>
            </div>
            <%-------------- END DDL Approve Status --------------%>

            <%-------------- START ปุ่มบันทึก --------------%>

            <div class="form-group">
                <div id="div_btn" class="row" runat="server" visible="false">
                    <%--<div class="form-group">--%>
                    <div class="pull-right">
                        <div class="col-sm-12">
                            <asp:LinkButton ID="btnCancelView" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="btnCancelView" Visible="false"><i class="fa fa-times"></i></asp:LinkButton>

                        </div>
                    </div>
                    <%--</div>--%>
                </div>
            </div>
            <%-------------- END ปุ่มบันทึก --------------%>

            <%-------------- START ปุ่มบันทึกผู้อนุมัติ --------------%>
            <div class="form-group">
                <div class="col-sm-12">
                    <div id="div_approve" class="row" runat="server" visible="false">
                        <div class="form-group">

                            <%--<div class="col-sm-6"></div>--%>
                            <%--<div class="col-sm-2">--%>
                            <div class="pull-right">
                                <asp:LinkButton ID="btnsaveapprove" CssClass="btn btn-success" runat="server" ValidationGroup="SaveApprove1" OnCommand="btnCommand" CommandName="btnsaveapprove" data-toggle="tooltip" title="SaveApprove" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>


                                <asp:LinkButton ID="btnCancelApprove" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="btnCancelApprove" data-toggle="tooltip" title="CancelApprove"><i class="fa fa-times"></i></asp:LinkButton>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <%-------------- END ปุ่มบันทึกผู้อนุมัติ --------------%>

            <!-- START Log Software ที่ถือครอง -->
            <div class="form-group">
                <div class="col-md-12">

                    <br />
                    <div class="row panel panel-default font_text" visible="false" runat="server" id="panel_detail_softwarelicense">
                        <div class="panel panel-info">
                            <%-- <h3 class="panel-heading f-bold"><strong>ประวัติข้อมูล</strong></h3>--%>
                            <div class="panel-heading f-bold">ประวัติรายการ Software License</div>
                        </div>


                        <%-- <asp:TextBox ID="testbind" runat="server"></asp:TextBox>--%>
                        <div class="panel-body">
                            <table class="table table-striped f-s-12">
                                <%--<asp:GridView ID="gvTest" runat="server"></asp:GridView>--%>
                                <asp:Repeater ID="rptDetailSoftwareName" runat="server">
                                    <HeaderTemplate>
                                        <tr>
                                            <th>วัน/ เวลา</th>
                                            <th>ผู้ดำเนินการ</th>
                                            <th>ดำเนินการ</th>
                                            <th>ผลการดำเนินการ</th>
                                            <th>ความคิดเห็น</th>

                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <%--<td data-th="ลำดับ">
                                                <%# (Container.ItemIndex + 1) %>

                                            </td>--%>

                                            <td data-th="วัน/ เวลา">
                                                <%--<%# Eval("OrgNameTH") %>--%>
                                                <asp:Label ID="lbcreate_datelog" runat="server" Text='<%# Eval("create_date") %>' />
                                                <%-- <asp:Label ID="lbsoftware_name_idxview" Visible="false" runat="server" Text='<%# Eval("software_name_idx") %>' />--%>

                                            </td>

                                            <td data-th="ผู้ดำเนินการ">
                                                <%--<%# Eval("OrgNameTH") %>--%>
                                                <asp:Label ID="lbname_createlog" runat="server" Text='<%# Eval("name_create") %>' />&nbsp;<asp:Label ID="lbactor_deslog" runat="server" Text='<%# Eval("actor_des") %>' />
                                                <%-- <asp:Label ID="lbsoftware_typeview" Visible="false" runat="server" Text='<%# Eval("software_type") %>' />--%>

                                            </td>

                                            <td data-th="ดำเนินการ">
                                                <%--<%# Eval("OrgNameTH") %>--%>
                                                <asp:Label ID="lbnode_namelog" runat="server" Text='<%# Eval("node_name") %>' />
                                                <%--<asp:Label ID="Label8" Visible="false" runat="server" Text='<%# Eval("software_name_idx") %>' />--%>
                                           
                                            </td>

                                            <td data-th="ผลการดำเนินการ">
                                                <%--<%# Eval("OrgNameTH") %>--%>
                                                <asp:Label ID="lbstatus_namelog" runat="server" Text='<%# Eval("status_name") %>' />
                                                <%--<asp:Label ID="Label8" Visible="false" runat="server" Text='<%# Eval("software_name_idx") %>' />--%>
                                           
                                            </td>

                                            <td data-th="ความคิดเห็น">
                                                <%--<%# Eval("OrgNameTH") %>--%>
                                                <asp:Label ID="lbcommentlog" runat="server" Text='<%# Eval("comment") %>' />
                                                <%--<asp:Label ID="Label8" Visible="false" runat="server" Text='<%# Eval("software_name_idx") %>' />--%>
                                           
                                            </td>


                                        </tr>

                                    </ItemTemplate>

                                </asp:Repeater>
                            </table>
                        </div>
                    </div>

                    <%--  <h5>&nbsp;</h5>--%>
                    <br />
                </div>
            </div>
            <!-- END จำนวน Software ที่ถือครอง -->

        </asp:View>

        <%----------- ViewIndex Software License END ---------------%>

        <%----------- View Software Deviecs Insert Start ---------------%>
        <asp:View ID="ViewSoftwareDevices" runat="server">


            <%----------- FormView Software Deviecs Insert START ---------------%>
            <asp:FormView ID="FvInsert" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                <InsertItemTemplate>
                    <div class="col-sm-12">
                        <div class="panel panel-info">
                            <div class="panel-heading f-bold">เพิ่มข้อมูล Software License</div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อ Software,รุ่น --------------%>
                                    <%--<div class="form-group">
                                        <asp:Label ID="lbsoftware_idxinsert" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ Software : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsoftware_idxinsert" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Requiredddlsoftware_idxinsert" ValidationGroup="Saveinsertdetail" runat="server" Display="None"
                                                ControlToValidate="ddlsoftware_idxinsert" Font-Size="11"
                                                ErrorMessage="กรุณากรอกชื่อ Software"
                                                ValidationExpression="กรุณากรอกชื่อ Software" InitialValue="00" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlsoftware_idxinsert" Width="160" />
                                        </div>

                                        <asp:Panel ID="detail_license" runat="server" Visible="false">
                                            <asp:Label ID="lbcount_licenseinsert" CssClass="col-sm-2 control-label" runat="server" Text="License ทั้งหมด : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtcount_licenseinsert" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>

                                            </div>
                                        </asp:Panel>

                                    </div>--%>
                                    <%-------------- ชื่อ Software, รุ่น --------------%>

                                    <%--<asp:Panel ID="show_detailinsert" runat="server" Visible="true">--%>

                                    <%-------------- องค์กร,ฝ่าย --------------%>
                                    <%--<asp:UpdatePanel ID="update_test" runat="server" UpdateMode="Always">
                                        <ContentTemplate>--%>
                                    <div class="form-group">
                                        <asp:Label ID="lborg_idxinsert" CssClass="col-sm-2 control-label" runat="server" Text="องค์กร : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlorg_idxinsert" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                            <%-- <asp:RequiredFieldValidator ID="Requireddlorg_idxinsert" ValidationGroup="SearchIndex" runat="server" Display="None"
                                                    
                                                    ControlToValidate="ddlorg_idxinsert" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกองค์กร"
                                                    ValidationExpression="กรุณาเลือกองค์กร" InitialValue="00" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" PopupPosition="BottomLeft" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requireddlorg_idxinsert" Width="160" />--%>
                                        </div>

                                        <asp:Label ID="lbrdept_idxinsert" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlrdept_idxinsert" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Text="เลือกฝ่าย ...." Value="00"></asp:ListItem>
                                            </asp:DropDownList>

                                            <%--<asp:RequiredFieldValidator ID="Requiredddlrdept_idxinsert" ValidationGroup="Saveinsertdetail" runat="server" Display="None"
                                                    ControlToValidate="ddlrdept_idxinsert" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกฝ่าย"
                                                    ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="00" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlrdept_idxinsert" Width="160" />--%>
                                        </div>

                                    </div>
                                    <%--</ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlorg_idxinsert" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>--%>
                                    <%-------------- องค์กร,ฝ่าย --------------%>

                                    <%-------------- แผนก,ชื่อ --------------%>
                                    <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                        <ContentTemplate>--%>
                                    <div class="form-group">
                                        <asp:Label ID="lbrsec_idxinsert" CssClass="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlrsec_idxinsert" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Text="เลือกแผนก ...." Value="00"></asp:ListItem>
                                            </asp:DropDownList>
                                            <%-- <asp:RequiredFieldValidator ID="Requireddlrsec_idxinsert" ValidationGroup="Saveinsertdetail" runat="server" Display="None"
                                                    ControlToValidate="ddlrsec_idxinsert" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกตำแหน่ง"
                                                    ValidationExpression="กรุณาเลือกตำแหน่ง" InitialValue="00" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requireddlrsec_idxinsert" Width="160" />--%>
                                        </div>

                                        <asp:Label ID="lbemp_idxinsert" CssClass="col-sm-2 control-label" Visible="false" runat="server" Text="ชื่อพนักงาน : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlemp_idxinsert" runat="server" CssClass="form-control" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Text="เลือกพนักงาน ...." Value="00"></asp:ListItem>
                                            </asp:DropDownList>
                                            <%--<asp:RequiredFieldValidator ID="Requiredddlemp_idxinsert" ValidationGroup="Saveinsertdetail" runat="server" Display="None"
                                                    ControlToValidate="ddlemp_idxinsert" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกชื่อพนักงาน"
                                                    ValidationExpression="กรุณาเลือกชื่อพนักงาน" InitialValue="00" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlemp_idxinsert" Width="160" />--%>
                                        </div>

                                    </div>
                                    <%--</ContentTemplate>
                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="ddlorg_idxinsert" EventName="SelectedIndexChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>--%>
                                    <%-------------- แผนก, ชื่อ --------------%>

                                    <%-------------- ประเภทอุปกรณ์ / รหัสอุปกรณ์ --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="lbtype_idxinsert" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_type_idxinsert" runat="server" CssClass="form-control" AutoPostBack="true">
                                                <%--<asp:ListItem Text="กรุณาเลือกประเภทอุปกรณ์ ...." Value="00"></asp:ListItem>--%>
                                            </asp:DropDownList>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Saveinsertdetail" runat="server" Display="None"
                                                    ControlToValidate="ddlemp_idxinsert" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกชื่อพนักงาน"
                                                    ValidationExpression="กรุณาเลือกชื่อพนักงาน" InitialValue="00" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlemp_idxinsert" Width="160" />--%>
                                        </div>

                                        <%--<asp:Panel id="Panel1" runat="server" Visible="false">--%>
                                        <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="รหัสอุปกรณ์ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txttype_idx" CssClass="form-control" MaxLength="10" placeholder="Ex. PC00000001" runat="server"></asp:TextBox>
                                        </div>
                                        <%--</asp:Panel>--%>
                                    </div>
                                    <%-------------- ประเภทอุปกรณ์ / รหัสอุปกรณ์  --------------%>

                                    <%-------------- เครื่องคอมพิวเตอร์ --------------%>
                                    <%--<div class="form-group">

                                            <asp:Label ID="lbholder_idxinsert" CssClass="col-sm-2 control-label" runat="server" Text="เครื่องคอมพิวเตอร์ : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlholder_idxinsert" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Text="กรุณาเลือกเครื่องคอมพิวเตอร์ ...." Value="00"></asp:ListItem>
                                                </asp:DropDownList>--%>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Saveinsertdetail" runat="server" Display="None"
                                                    ControlToValidate="ddlemp_idxinsert" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกชื่อพนักงาน"
                                                    ValidationExpression="กรุณาเลือกชื่อพนักงาน" InitialValue="00" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlemp_idxinsert" Width="160" />--%>
                                    <%--</div>--%>


                                    <%--<asp:Panel id="Panel1" runat="server" Visible="false">--%>
                                    <%-- <asp:Label ID="lbcount_license_dept" CssClass="col-sm-2 control-label" runat="server" Text="จำนวน License ตามฝ่าย ทั้งหมด : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtcount_license_dept" CssClass="form-control" Enabled="false" runat="server"></asp:TextBox>
                                            </div>--%>
                                    <%--</asp:Panel>--%>
                                    <%--</div>--%>
                                    <%-------------- เครื่องคอมพิวเตอร์ --------------%>


                                    <%-------------- btnsearch --------------%>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <%--<div class="col-sm-12"></div>--%>
                                            <div class="pull-left">
                                                <asp:LinkButton ID="btnSearchIndex" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchIndex" OnCommand="btnCommand" CommandName="btnSearchIndex" data-toggle="tooltip" title="Search" OnClientClick="return confirm('คุณต้องการค้นหารายการนี้ใช่หรือไม่ ?')"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>


                                                <asp:LinkButton ID="btnCancelIndex" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="btnCancelIndex" data-toggle="tooltip" title="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>

                                        </div>
                                    </div>
                                    <%-------------- btnsearch --------------%>



                                    <%-------------- เพิ่มรายการ --------------%>
                                    <%-- <asp:Panel ID="show_btninsertdataset" runat="server" Visible="false">
                                            <div class="form-group">--%>
                                    <%--<div class="col-sm-2"></div>--%>
                                    <%-- <div class="col-lg-offset-2 col-sm-10">
                                                    <asp:LinkButton ID="btnInsertDetail" CssClass="btn btn-default" runat="server" CommandName="btnInsertDetail" OnCommand="btnCommand" ValidationGroup="Saveinsertdetail" title="เพิ่มรายการ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>--%>

                                    <%--<asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancel">ยกเลิก</asp:LinkButton>--%>
                                    <%--</div>

                                            </div>
                                        </asp:Panel>--%>
                                    <%-------------- เพิ่มรายการ --------------%>

                                    <%-------------- GridView แสดงรายละเอียด  จำนวน License แต่ละฝ่าย  --------------%>
                                    <asp:Panel ID="gidviewdetaillicense" runat="server" Visible="false" AutoPostBack="true">
                                        <div class="col-sm-12">
                                            <asp:GridView ID="GVShowdetailLicense"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-5"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="true"
                                                PageSize="10"
                                                OnRowEditing="Master_RowEditing"
                                                OnRowUpdating="Master_RowUpdating"
                                                OnRowDeleting="Master_RowDeleting"
                                                OnRowDataBound="Master_RowDataBound"
                                                OnPageIndexChanging="Master_PageIndexChanging">
                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>
                                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <%# (Container.DataItemIndex +1) %>

                                                            </small>

                                                        </ItemTemplate>

                                                        <EditItemTemplate>

                                                            <div class="col-sm-12">
                                                                <div class="form-horizontal" role="form">
                                                                    <div class="panel-body">
                                                                        <div class="form-group">

                                                                            <asp:Label ID="lbsoftware_idxedit" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ Software : " />
                                                                            <div class="col-sm-4">
                                                                                <asp:HiddenField ID="ddlsoftware_idx" runat="server" Value='<%# Eval("SoftwareIDX_License_dataset") %>' />
                                                                                <asp:DropDownList ID="ddlsoftware_idxupdate" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server">
                                                                                </asp:DropDownList>

                                                                                <%--<asp:RequiredFieldValidator ID="Requiredddlsoftware_idxinsert" ValidationGroup="Saveinsertdetail" runat="server" Display="None"
                                                                                        ControlToValidate="ddlsoftware_idxinsert" Font-Size="11"
                                                                                        ErrorMessage="กรุณากรอกชื่อ Software"
                                                                                        ValidationExpression="กรุณากรอกชื่อ Software" InitialValue="00" />
                                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddlsoftware_idxinsert" Width="160" />--%>
                                                                            </div>




                                                                            <%--<div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <small>
                                                                                    <label class="pull-left">ชื่อ Software</label>
                                                                                    <%-- <asp:Label ID="lbddlTypeName" runat="server" Text='<%# Bind("type_idx") %>' Visible="false" />--%>
                                                                            <%--<asp:HiddenField ID="ddlsoftware_idx" runat="server" Value='<%# Eval("SoftwareIDX_License_dataset") %>' />

                                                                                <asp:DropDownList ID="ddlsoftware_idxupdate" AutoPostBack="true" runat="server"
                                                                                    CssClass="form-control">
                                                                                </asp:DropDownList>
                                                                                </small>
                                                                            </div>
                                                                        </div>--%>


                                                                            <%--<div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <small>
                                                                                    <label class="pull-left">องค์กร</label>
                                                                                    <%--<asp:Label ID="lbddlTypeName" runat="server" Text='<%# Bind("OrgIDX_dataset") %>' Visible="false" />--%>
                                                                            <%-- <asp:HiddenField ID="ddlorg_idx" runat="server" Value='<%# Eval("SoftwareIDX_License_dataset") %>' />
                                                                                    <asp:DropDownList ID="ddlorg_idxupdate" AutoPostBack="true" runat="server"
                                                                                        CssClass="form-control">
                                                                                    </asp:DropDownList>
                                                                                </small>
                                                                            </div>
                                                                        </div>--%>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>






                                                            <%--<asp:TextBox ID="software_idx" runat="server" CssClass="form-control"
                                                                    Visible="False" Text='<%# Eval("SoftwareIDX_License_dataset")%>' />--%>
                                                            <%-- <div class="panel-body">
                                                                    <div class="form-horizontal" role="form">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <small>
                                                                                    <label class="pull-left">ชื่อ Software</label>--%>
                                                            <%--<asp:Label ID="lbSoftwareIDX_License_dataset" runat="server" Text='<%# Bind("SoftwareIDX_License_dataset") %>' Visible="false" />--%>
                                                            <%--<asp:HiddenField ID="ddlsoftware_idx" runat="server" Value='<%# Eval("SoftwareIDX_License_dataset") %>' />
                                                                                    <asp:DropDownList ID="ddlsoftware_idxupdate" AutoPostBack="true" runat="server"
                                                                                        CssClass="form-control">
                                                                                    </asp:DropDownList>
                                                                                </small>
                                                                            </div>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <hr />
                                                                            <div class="col-sm-4 col-sm-offset-8">
                                                                                <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="formbrethren" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                                            </div>
                                                                        </div>

                                                                    </div>

                                                                </div>--%>
                                                        </EditItemTemplate>


                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ชื่อ Software" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lbSoftware_License_dataset" runat="server" Visible="true" CssClass="col-sm-12" Text='<%# Eval("Software_License_dataset") %>'></asp:Label>
                                                                <asp:Label ID="lbSoftwareIDX_License_dataset" runat="server" Visible="true" CssClass="col-sm-12" Text='<%# Eval("SoftwareIDX_License_dataset") %>'></asp:Label>
                                                                <%-- <asp:Label ID="lbOrgNameTH_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("OrgNameTH_dataset") %>'></asp:Label>
                                                                    <asp:Label ID="lbOrgIDX_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("OrgIDX_dataset") %>'></asp:Label>--%>
                                                            </small>
                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="องค์กร" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <%--<asp:Label ID="lbSoftware_License_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("Software_License_dataset") %>'></asp:Label>--%>

                                                                <asp:Label ID="lbOrgNameTH_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("OrgNameTH_dataset") %>'></asp:Label>
                                                                <asp:Label ID="lbOrgIDX_dataset" runat="server" Visible="true" CssClass="col-sm-12" Text='<%# Eval("OrgIDX_dataset") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lbDeptNameTH_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("DeptNameTH_dataset") %>'></asp:Label>
                                                                <asp:Label ID="lbRDeptIDX_dataset" runat="server" Visible="true" CssClass="col-sm-12" Text='<%# Eval("RDeptIDX_dataset") %>'></asp:Label>
                                                            </small>

                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ตำแหน่ง" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lbSecNameTH_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("SecNameTH_dataset") %>'></asp:Label>
                                                                <asp:Label ID="lbRSecIDX_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("RSecIDX_dataset") %>'></asp:Label>
                                                            </small>

                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ชื่อพนักงาน" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lbFullNameTH_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("FullNameTH_dataset") %>'></asp:Label>
                                                                <asp:Label ID="lbemp_idx_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("emp_idx_dataset") %>'></asp:Label>
                                                            </small>

                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="จำนวน License" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lbcount_license_dept_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("count_license_dept_dataset") %>'></asp:Label>
                                                                <asp:Label ID="lbnumlicense_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("numlicense_dataset") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="การจัดการ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>

                                                            <asp:LinkButton ID="btnDrivingEdit" runat="server" Font-Size="11px" Visible="false" CssClass="btn btn-warning" data-toggle="tooltip" title="แก้ไขข้อมูล" CommandName="Edit"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>

                                                            <asp:LinkButton ID="btnDrivingDelete" runat="server" Font-Size="11px" CssClass="btn btn-danger" data-toggle="tooltip" title="ลบข้อมูล" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่?')" CommandName="Delete"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                                            <%--<asp:LinkButton ID="btnViewindex" CssClass="btn btn-primary" Font-Size="11px" runat="server" CommandName="btnViewindex" OnCommand="btnCommand"  
                                        data-toggle="tooltip" title="ดูข้อมูล" ><i class="fa fa-file-o"></i></asp:LinkButton>--%>
                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>
                                        </div>

                                    </asp:Panel>
                                    <%-------------- GridView แสดงรายละเอียด  จำนวน License แต่ละฝ่าย --------------%>



                                    <%-------------- CheckBox --------------%>
                                    <%--<div class="form-group">
                                            
                                            <div class="col-sm-1"></div>
                                            <div class="col-sm-4">
                                                <div class="checkbox">
                                                    <asp:CheckBox ID="chk_licenseinsert" runat="server" Font-Bold="true" Text="แบ่งจำนวน License ตามคอมพิวเตอร์" OnCheckedChanged="CheckboxChanged" AutoPostBack="true" RepeatDirection="Vertical" />
                                                    <span style="color: red; font-size: 16px; font-weight: bold; margin-left: 20px;">
                                                        <asp:Literal ID="litShowDetail" runat="server"></asp:Literal></span>
                                                </div>
                                            </div>
                                        </div>--%>

                                    <%-------------- CheckBox --------------%>

                                    <%-------------- ปุ่มบันทึก --------------%>
                                    <%-- <asp:UpdatePanel ID="updatet1" runat="server">
                                        <ContentTemplate>--%>
                                    <asp:Panel ID="show_btninsert" runat="server" Visible="false">
                                        <%--<div class="col-sm-12">--%>
                                        <div class="form-group">

                                            <div class="col-lg-offset-10 col-sm-2">
                                                <%--<div class="pull-right">--%>
                                                <asp:LinkButton ID="btnInsert" CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand" ValidationGroup="Saveinsert" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')">บันทึก</asp:LinkButton>

                                                <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancel">ยกเลิก</asp:LinkButton>
                                            </div>
                                        </div>

                                        <%--</div>--%>
                                    </asp:Panel>


                                    <%--</ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnInsert" />
                                        </Triggers>

                                    </asp:UpdatePanel>--%>

                                    <%-------------- ปุ่มบันทึก --------------%>
                                    <%--</asp:Panel>--%>
                                </div>

                            </div>

                        </div>
                    </div>
                </InsertItemTemplate>

            </asp:FormView>
            <%----------- FormView Software Deviecs Insert END ---------------%>

            <!--*** START Back To Search Move ***-->
            <div class="col-sm-12">
                <div class="form-group">
                    <div id="fvBacktoSearch" class="row" runat="server" visible="false">
                        <%--<div class="col-md-12">--%>
                        <asp:LinkButton ID="btnBackToSearch" CssClass="btn btn-info" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandName="btnBackToSearch"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back</asp:LinkButton>
                        <%--</div>--%>
                    </div>
                </div>
                <%--<br />--%>
            </div>
            <!--*** END Back To Search Move ***-->

            <%-------------- เลือกรายการคอมพิวเตอร์ --------------%>
            <asp:Panel ID="ShowSelectcom" runat="server" Visible="false">
                <div class="col-sm-12">
                    <div class="panel panel-info">
                        <div class="panel-heading f-bold">เพิ่มรายการ Software License</div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <%-------------- เลือกรหัสอุปกรณ์,Asset Code --------------%>

                                <div class="form-group">
                                    <asp:Label ID="lbholderdevicesinsert" CssClass="col-sm-2 control-label" runat="server" Text="เลือกรหัสอุปกรณ์ : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlholderdevicesinsert" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            <asp:ListItem Text="กรุณาเลือกรหัสอุปกรณ์ ...." Value="00"></asp:ListItem>
                                        </asp:DropDownList>

                                    </div>

                                    <%--<asp:Panel id="Panel1" runat="server" Visible="false">--%>
                                    <asp:Label ID="lbasset_codedevices" CssClass="col-sm-2 control-label" runat="server" Text="Asset Code : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtasset_codedevices" Enabled="false" CssClass="form-control" placeholder="Asset Code ..." runat="server"></asp:TextBox>
                                    </div>
                                    <%--</asp:Panel>--%>
                                </div>

                                <%-------------- เลือกรหัสอุปกรณ์, Asset Code --------------%>


                                <asp:Panel ID="ShowddlSoftware" runat="server" Visible="false">

                                    <%-------------- ชื่อผู้ถือครอง/ บริษัทผู้ถือครอง  --------------%>
                                    <%--<asp:UpdatePanel ID="update_test" runat="server" UpdateMode="Always">
                                        <ContentTemplate>--%>
                                    <div class="form-group">

                                        <%--<asp:Panel id="Panel1" runat="server" Visible="false">--%>
                                        <asp:Label ID="Label3" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทของผู้ถือครอง : " />
                                        <div class="col-sm-3">
                                            <asp:Label ID="lborg_idx" Visible="false" CssClass="form-control" runat="server"></asp:Label>
                                            <asp:TextBox ID="txtorginsert" Enabled="false" CssClass="form-control" placeholder="บริษัทผู้ถือครอง ..." runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่ายของผู้ถือครอง : " />
                                        <div class="col-sm-3">
                                            <asp:Label ID="lbrdept_idx" Visible="false" CssClass="form-control" runat="server"></asp:Label>
                                            <asp:TextBox ID="txtrdeptinsert" Enabled="false" CssClass="form-control" placeholder="ฝ่ายของผู้ถือครอง ..." runat="server"></asp:TextBox>
                                        </div>
                                        <%--</asp:Panel>--%>
                                    </div>

                                    <%-------------- ชื่อผู้ถือครอง, บริษัทผู้ถือครอง --------------%>

                                    <%-------------- ฝ่ายของผู้ถือของ/ แผนกของผู้ถือครอง  --------------%>
                                    <%--<asp:UpdatePanel ID="update_test" runat="server" UpdateMode="Always">
                                        <ContentTemplate>--%>
                                    <div class="form-group">



                                        <%--<asp:Panel id="Panel1" runat="server" Visible="false">--%>
                                        <asp:Label ID="Label5" CssClass="col-sm-2 control-label" runat="server" Text="แผนกของผู้ถือครอง : " />
                                        <div class="col-sm-3">
                                            <asp:Label ID="lbrsec_idx" Visible="false" CssClass="form-control" runat="server"></asp:Label>
                                            <asp:TextBox ID="txtrsecinsert" Enabled="false" CssClass="form-control" placeholder="แผนกของผู้ถือครอง ..." runat="server"></asp:TextBox>
                                        </div>


                                        <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อผู้ถือครอง : " />
                                        <div class="col-sm-3">
                                            <asp:Label ID="lbemp_idx" Visible="false" CssClass="form-control" runat="server"></asp:Label>
                                            <asp:TextBox ID="txtcempnameinsert" Enabled="false" CssClass="form-control" placeholder="ชื่อผู้ถือครอง ..." runat="server"></asp:TextBox>

                                            <asp:TextBox ID="txtemail_holderinsert" Enabled="false" CssClass="form-control" placeholder="E-Mail ..." runat="server" Visible="false"></asp:TextBox>
                                        </div>



                                        <%--</asp:Panel>--%>
                                    </div>

                                    <%-------------- ฝ่ายของผู้ถือของ, บริษัทผู้ถือครอง --------------%>


                                    <%-------------- ชื่อ Software / ประเภท Software  --------------%>
                                    <div class="form-group">
                                        <asp:TextBox ID="lbsoftware_idx" runat="server" Visible="false" />
                                        <asp:Label ID="lbddlsoftware_nameinsert" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ Software : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddlsoftware_nameinsert" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Requiredtxtsoftware_nameinsert" ValidationGroup="Saveinsertlicense" runat="server" Display="None"
                                                ControlToValidate="ddlsoftware_nameinsert" Font-Size="11"
                                                ErrorMessage="กรุณากรอกชื่อ Software"
                                                ValidationExpression="กรุณากรอกชื่อ Software" InitialValue="00" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtsoftware_nameinsert" Width="160" />

                                        </div>

                                        <asp:Label ID="lbtype_insert" CssClass="col-sm-2 control-label" Visible="true" runat="server" Text="ประเภท Software : " />
                                        <div class="col-sm-3">
                                            <asp:Label ID="lbsoftware_idxinsert" CssClass="form-control" Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="lblghost_statusinsert" CssClass="form-control" Visible="false" runat="server"></asp:Label>
                                            <asp:Label ID="txttypeidx_insert" CssClass="form-control" Visible="false" runat="server"></asp:Label>
                                            <asp:TextBox ID="txttype_insert" CssClass="form-control" Enabled="false" placeholder="ประเภท Software  ..." runat="server"></asp:TextBox>

                                        </div>


                                    </div>
                                    <%-------------- ชื่อ Software / ประเภท Software  --------------%>

                                    <%-------------- จำนวน License ที่เพิ่ม  --------------%>
                                    <div class="form-group" runat="server" visible="false">

                                        <asp:Label ID="lbcountlicense_insert" CssClass="col-sm-2 control-label" Visible="false" runat="server" Text="จำนวน License : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtcount_licenseinsert" CssClass="form-control" Visible="false" Enabled="false" runat="server"></asp:TextBox>

                                        </div>


                                    </div>
                                    <%-------------- จำนวน License ที่เพิ่ม  --------------%>

                                    <%-------------- เพิ่มรายการ --------------%>
                                    <asp:Panel ID="show_btninsertlicensedataset" runat="server" Visible="true">
                                        <div class="form-group">
                                            <%--<div class="col-sm-2"></div>--%>
                                            <div class="col-lg-offset-2 col-sm-10">
                                                <asp:LinkButton ID="btnInsertSoftware" CssClass="btn btn-default" runat="server" CommandName="btnInsertSoftware" OnCommand="btnCommand" ValidationGroup="Saveinsertlicense" title="เพิ่มรายการ Software"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>

                                                <%--<asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="btnCancel">ยกเลิก</asp:LinkButton>--%>
                                            </div>

                                        </div>
                                    </asp:Panel>
                                    <%-------------- เพิ่มรายการ --------------%>

                                    <%-------------- GridView แสดงรายละเอียด  จำนวน License แต่ละฝ่าย  --------------%>
                                    <asp:Panel ID="gidviewlicensedetaillicense" runat="server" Visible="false" AutoPostBack="true">


                                        <div class="col-sm-12">
                                            <asp:GridView ID="GVInsertSoftwareLicense"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-5"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="true"
                                                PageSize="10"
                                                OnRowEditing="Master_RowEditing"
                                                OnRowUpdating="Master_RowUpdating"
                                                OnRowDeleting="Master_RowDeleting"
                                                OnRowDataBound="Master_RowDataBound"
                                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                                OnPageIndexChanging="Master_PageIndexChanging">
                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>

                                                <Columns>
                                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <%# (Container.DataItemIndex +1) %>

                                                            </small>

                                                        </ItemTemplate>

                                                        <EditItemTemplate>

                                                            <div class="col-sm-12">
                                                                <div class="form-horizontal" role="form">
                                                                    <div class="panel-body">

                                                                        <div class="form-group">

                                                                            <asp:Label ID="lbsoftware_idxedit" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ Software : " />
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="ddlsoftware_idx" Visible="false" runat="server" Text='<%# Eval("swlicense_idx_dataset") %>' />
                                                                                <asp:DropDownList ID="ddlsoftware_idxupdate" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server">
                                                                                </asp:DropDownList>
                                                                            </div>

                                                                            <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="ประเภท Software : " />
                                                                            <div class="col-sm-3">
                                                                                <asp:TextBox ID="texttypeidx_update" Enabled="false" Visible="false" CssClass="form-control" runat="server" Text='<%# Eval("type_idx_dataset") %>'></asp:TextBox>
                                                                                <asp:TextBox ID="txttypename_update" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("type_name_dataset") %>'></asp:TextBox>
                                                                            </div>


                                                                        </div>

                                                                        <div class="form-group">

                                                                            <%--<div class="col-sm-4 col-sm-offset-8">--%>
                                                                            <div class="pull-right">
                                                                                <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="formbrethren" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                                            </div>

                                                                        </div>



                                                                    </div>
                                                                </div>

                                                            </div>


                                                        </EditItemTemplate>


                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ชื่อ Software" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lbsoftware_idx_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("software_idx_dataset") %>'></asp:Label>

                                                                <asp:Label ID="lbSoftware_License_dataset" runat="server" Visible="true" CssClass="col-sm-12" Text='<%# Eval("swlicense_name_dataset") %>'></asp:Label>
                                                                <asp:Label ID="lbSoftwareIDX_License_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("swlicense_idx_dataset") %>'></asp:Label>

                                                            </small>
                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ประเภท Software" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <%-- <asp:Label ID="lbSoftware_License_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("Software_License_dataset") %>'></asp:Label>--%>

                                                                <asp:Label ID="lbtypename_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("type_name_dataset") %>'></asp:Label>
                                                                <asp:Label ID="lbtypeidx_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("type_idx_dataset") %>'></asp:Label>
                                                                 <asp:Label ID="lbghoststatus_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("ghoststatus_dataset") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="จำนวน License" ItemStyle-HorizontalAlign="center" Visible="true" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <small>
                                                                <%--<asp:Label ID="lbDeptNameTH_dataset" runat="server" CssClass="col-sm-12" Text='<%# Eval("DeptNameTH_dataset") %>'></asp:Label>--%>

                                                                <asp:Label ID="lborg_idx_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("org_idx_dataset") %>'></asp:Label>
                                                                <asp:Label ID="lbrdept_idx_dataset" runat="server" Visible="false" CssClass="col-sm-12" Text='<%# Eval("rdept_idx_dataset") %>'></asp:Label>

                                                                <asp:Label ID="lbcountlicenseinsert_dataset" runat="server" Visible="true" CssClass="col-sm-12" Text='<%# Eval("countinsert_dataset") %>'></asp:Label>
                                                            </small>

                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="การจัดการ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>

                                                            <asp:LinkButton ID="btnDrivingEdit" runat="server" Visible="false" Font-Size="11px" CssClass="btn btn-warning" data-toggle="tooltip" title="แก้ไขข้อมูล" CommandName="Edit"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>

                                                            <asp:LinkButton ID="btnDrivingDelete" runat="server" Font-Size="11px" CssClass="btn btn-danger" data-toggle="tooltip" title="ลบข้อมูล" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่?')" CommandName="Delete"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                        </ItemTemplate>

                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                </Columns>

                                            </asp:GridView>
                                        </div>
                                    </asp:Panel>
                                    <%-------------- GridView แสดงรายละเอียด  จำนวน License แต่ละฝ่าย  --------------%>
                                </asp:Panel>

                            </div>
                        </div>


                    </div>
                </div>
            </asp:Panel>
            <%-------------- เลือกรายการคอมพิวเตอร์ --------------%>

            <%-------------- ปุ่มบันทึก / ยกเลิก --------------%>
            <%--<div class="form-group" id="div_saveinsert" runat="server" visible="false">--%>
            <div class="col-sm-12">
                <div class="form-group">
                    <div id="div_saveinsert" class="row" runat="server" visible="false">
                        <%--<div class="form-group">--%>
                        <div class="pull-right">
                            <asp:LinkButton ID="btnSaveInsert" CssClass="btn btn-success" runat="server" ValidationGroup="SaveInsert" OnCommand="btnCommand" CommandName="btnSaveInsert" data-toggle="tooltip" title="Save" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>


                            <asp:LinkButton ID="btnCancelInsert" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="btnCancelInsert" data-toggle="tooltip" title="Cancel"><i class="fa fa-times"></i></asp:LinkButton>

                        </div>
                    </div>

                </div>
                <%--</div>--%>


                <%--</div>--%>
            </div>

            <%-------------- ปุ่มบันทึก / ยกเลิก --------------%>
        </asp:View>

        <%----------- View Software Deviecs Insert END ---------------%>

        <%----------- View Software Deviecs Report Start ---------------%>
        <asp:View ID="ViewReport" runat="server">
            <div class="col-md-12">

                <div id="showsearchreport" runat="server" visible="true">
                    <div class="panel panel-info">
                        <div class="panel-heading f-bold">Report Software License</div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <%-------------- ชื่อ software, ประเภท Software --------------%>
                                <div class="form-group">
                                    <asp:Label ID="lbsoftware_name_idxreport" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ Software : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlsoftware_name_idxreport" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                    </div>

                                    <asp:Label ID="lbsoftware_typereport" CssClass="col-sm-2 control-label" runat="server" Text="ประเภท Software : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlsoftware_typereport" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="เลือกประเภท Software ...." Value="00"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>
                                <%-------------- ชื่อ software, ประเภท Software --------------%>

                                <%-------------- องค์กร, ฝ่าย --------------%>
                                <div class="form-group">
                                    <asp:Label ID="lborg_idxreport" CssClass="col-sm-2 control-label" runat="server" Text="องค์กร : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlorg_idxreport" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged"></asp:DropDownList>
                                    </div>

                                    <asp:Label ID="lbrdept_idxreport" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlrdept_idxreport" AutoPostBack="true" runat="server" CssClass="form-control">
                                            <asp:ListItem Text="เลือกฝ่าย ...." Value="00"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>

                                </div>
                                <%-------------- องค์กร, ฝ่าย --------------%>

                                <%-------------- btnsearch --------------%>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <%--<div class="col-sm-12"></div>--%>
                                        <div class="pull-left">
                                            <asp:LinkButton ID="btnSearchReport" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchReport" OnCommand="btnCommand" CommandName="btnSearchReport" data-toggle="tooltip" title="Search" OnClientClick="return confirm('คุณต้องการค้นหารายการนี้ใช่หรือไม่ ?')"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>


                                            <asp:LinkButton ID="btnCancelReport" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" CommandName="btnCancelReport" data-toggle="tooltip" title="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                        </div>

                                    </div>
                                </div>
                                <%-------------- btnsearch --------------%>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!--*** START GRIDVIEW Index ***-->
            <%-- col-md-12 m-t-10--%>
            <div class="col-md-12">

                <div id="gridviewreport" runat="server" visible="false">
                    <br />
                    <asp:GridView ID="GvReport"
                        runat="server"
                        AutoGenerateColumns="false"
                        DataKeyNames="software_name_idx"
                        CssClass="table table-striped table-bordered table-responsive"
                        HeaderStyle-CssClass="info"
                        AllowPaging="true"
                        PageSize="10"
                        OnRowEditing="Master_RowEditing"
                        OnRowUpdating="Master_RowUpdating"
                        OnRowCancelingEdit="Master_RowCancelingEdit"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        OnRowDataBound="Master_RowDataBound">
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">No result</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center" Visible="true">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbu0_software_idx" runat="server" Visible="false" Text='<%# Eval("u0_software_idx") %>' />
                                        <asp:Label ID="lbunidx" runat="server" Visible="false" Text='<%# Eval("m0_node_idx") %>' />
                                        <%# (Container.DataItemIndex +1) %>
                                    </small>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อโปรแกรม" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lbsoftware_name_idxReport" Visible="false" runat="server" Text='<%# Eval("software_name_idx") %>' />
                                        <asp:Label ID="software_nameReport" runat="server" Text='<%# Eval("software_name") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Version" ItemStyle-HorizontalAlign="center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">

                                <ItemTemplate>
                                    <%-- &nbsp;&nbsp;<p><b>ชื่อผู้สร้าง :</b> <%# Eval("name_create") %></p>--%>

                                    <small>

                                        <asp:Label ID="versionReport" runat="server" Text='<%# Eval("version") %>' />

                                        <asp:Label ID="DetailReport" runat="server">
                                        
                                        </asp:Label>

                                    </small>

                                </ItemTemplate>

                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภท Software" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="type_nameReport" runat="server" Text='<%# Eval("type_name") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน License ทั้งหมด" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="count_software_licenseReport" Visible="true" runat="server" Text='<%# Eval("count_software_license") %>' />

                                        <table class="table table-striped f-s-4">

                                            <asp:Repeater ID="rpcountalllicense" runat="server" Visible="false" OnItemDataBound="rptRecords_ItemDataBound">
                                                <HeaderTemplate>
                                                    <tr>
                                                    </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>

                                                        <asp:Label ID="detail_licensecouny_all" runat="server">
                                                   
                                                            <p><%#Eval("software_count_all")%></p>
                                                 
                                                        </asp:Label>
                                                        <%-- </td>--%>
                                                    </tr>

                                                </ItemTemplate>
                                                <FooterTemplate>
                                                </FooterTemplate>
                                            </asp:Repeater>


                                        </table>



                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน License ที่ถูกใช้ไป" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>

                                        <asp:Label ID="software_useReport" Visible="true" runat="server" Text='<%# Eval("software_use") %>' />


                                        <table class="table table-striped f-s-4">

                                            <asp:Repeater ID="rpcountuselicense" Visible="false" runat="server" OnItemDataBound="rptRecords_ItemDataBound">
                                                <HeaderTemplate>
                                                    <tr>
                                                    </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>

                                                        <asp:Label ID="detail_use" runat="server">
                                                  
                                                            <%--Text='<%#getStatus((int)Eval("status_deviecs")) %>'--%>
                                                            <p><%# getStatus_u0_use((int)Eval("u0_use"))%></p>
                                                 
                                                        </asp:Label>
                                                    </tr>

                                                </ItemTemplate>
                                                <FooterTemplate>
                                                </FooterTemplate>
                                            </asp:Repeater>

                                        </table>




                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน License ถูกลิขสิทธิ์ที่ถูกใช้ไป" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>

                                        <asp:Label ID="software_licenseuseassetReport" Visible="true" runat="server" Text='<%# Eval("software_licenseuseasset") %>' />




                                        <table class="table table-striped f-s-4">
                                            <asp:Repeater ID="rpcountuseoklicense" Visible="false" runat="server" OnItemDataBound="rptRecords_ItemDataBound">
                                                <HeaderTemplate>
                                                    <tr>
                                                    </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>

                                                        <asp:Label ID="detail_countuseok" runat="server">
                                                    
                                                            <p><%# Eval("u0_uselicense") %></p>
                                                                                                    
                                                        </asp:Label>

                                                    </tr>

                                                </ItemTemplate>
                                                <FooterTemplate>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </table>

                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน License ไม่ถูกลิขสิทธิ์ที่ถูกใช้ไป" ItemStyle-HorizontalAlign="Center"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>

                                        <asp:Label ID="software_ghostReport" Visible="true" runat="server" Text='<%# Eval("software_ghost") %>' />


                                        <table class="table table-striped f-s-4">
                                            <asp:Repeater ID="rpcountghostlicense" Visible="false" runat="server" OnItemDataBound="rptRecords_ItemDataBound">
                                                <HeaderTemplate>
                                                    <tr>
                                                    </tr>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>

                                                        <asp:Label ID="detail_countghost" runat="server">
                                                    
                                                            <p><%# Eval("u0_ghost") %></p>                                               
                                                   
                                                        </asp:Label>

                                                    </tr>

                                                </ItemTemplate>
                                                <FooterTemplate>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </table>

                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="บริษัทที่ซื้อ" ItemStyle-HorizontalAlign="Left"
                                HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <small>&nbsp;&nbsp;&nbsp;<asp:Label ID="company_nameReport" runat="server" Text='<%#Eval("company_name") %>' />
                                    </small>
                                </ItemTemplate>
                                <EditItemTemplate />
                                <FooterTemplate />
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <!--*** END GRIDVIEW Index ***-->




        </asp:View>


        <%----------- View Software Deviecs Report END ---------------%>
    </asp:MultiView>
    <%-----------END MultiView END ---------------%>
</asp:Content>

