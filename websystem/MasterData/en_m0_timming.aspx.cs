﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_en_m0_timming : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();
    data_en_planning _dtenplan = new data_en_planning();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlSelect_MasterData = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_timing"];
    static string urlnsert_MasterData = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Master_timing"];
    static string urlUpdate_MasterData = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_Master_timing"];
    static string urlDelete_MasterData = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_Master_timing"];



    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();

        }

        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());

    }

    protected void SelectMasterList()
    {

        _dtenplan.BoxEN_TimeList = new Timing_Detail[1];
        Timing_Detail dtsupport = new Timing_Detail();
        dtsupport.time_desc = txtsearchtime.Text;
        _dtenplan.BoxEN_TimeList[0] = dtsupport;

        _dtenplan = callServicePostENPlanning(urlSelect_MasterData, _dtenplan);
        setGridData(GvMaster, _dtenplan.BoxEN_TimeList);
    }

    protected void Insert_MasterData()
    {
        _dtenplan.BoxEN_TimeList = new Timing_Detail[1];
        Timing_Detail insert = new Timing_Detail();


        insert.time_desc = txtnameth.Text;
        insert.time_short = txtnameen.Text;
        insert.qty = int.Parse(txtqty.Text);
        insert.unit = ddlunit.SelectedValue;
        insert.unit_doing = ddlunitday.SelectedValue;
        insert.count_doing = int.Parse(ddlday.SelectedValue);
        insert.unit_director = ddlunitdirector.SelectedValue;
        insert.count_director = int.Parse(ddldirec.SelectedValue);
        insert.unit_coo = ddlunitcoo.SelectedValue;
        insert.count_coo = int.Parse(ddlcoo.SelectedValue);


        insert.time_status = int.Parse(ddStatusadd.SelectedValue);
        insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtenplan.BoxEN_TimeList[0] = insert;
        // text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtenplan));
        _dtenplan = callServicePostENPlanning(urlnsert_MasterData, _dtenplan);

    }

    protected void Update_Master_List()
    {
        _dtenplan.BoxEN_TimeList = new Timing_Detail[1];
        Timing_Detail update = new Timing_Detail();

        update.time_desc = ViewState["time_desc"].ToString();
        update.time_short = ViewState["time_short"].ToString();
        update.qty = int.Parse(ViewState["qty"].ToString());
        update.unit = ViewState["unit"].ToString();

        update.unit_doing = ViewState["unit_doing"].ToString();
        update.count_doing = int.Parse(ViewState["count_doing"].ToString());
        update.unit_director = ViewState["unit_director"].ToString();
        update.count_director = int.Parse(ViewState["count_director"].ToString());
        update.unit_coo = ViewState["unit_coo"].ToString();
        update.count_coo = int.Parse(ViewState["count_coo"].ToString());
        update.time_status = int.Parse(ViewState["StatusUpdate_Update"].ToString());
        update.m0tidx = int.Parse(ViewState["m0tidx"].ToString());
        update.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtenplan.BoxEN_TimeList[0] = update;
        //    text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtenplan));
        _dtenplan = callServicePostENPlanning(urlUpdate_MasterData, _dtenplan);

    }

    protected void Delete_Master_List()
    {
        _dtenplan.BoxEN_TimeList = new Timing_Detail[1];
        Timing_Detail delete = new Timing_Detail();

        delete.m0tidx = int.Parse(ViewState["m0tidx"].ToString());
        delete.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dtenplan.BoxEN_TimeList[0] = delete;

        _dtenplan = callServicePostENPlanning(urlDelete_MasterData, _dtenplan);
    }

    protected data_en_planning callServicePostENPlanning(string _cmdUrl, data_en_planning _dtenplan)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtenplan);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtenplan = (data_en_planning)_funcTool.convertJsonToObject(typeof(data_en_planning), _localJson);

        return _dtenplan;
    }


    #region SetDropdownlist 

    protected void GenerateddlDay(DropDownList ddlName)
    {

        //ddlName.AppendDataBoundItems = true;
        //ddlName.Items.Clear();

        //var currentYear = DateTime.Today.Year;
        //for (int i = 5; i >= 0; i--)
        //{
        //    ddlName.Items.Add((currentYear - i).ToString());
        //}

        //ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;

        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("1", "1"));
        ddlName.Items.Add(new ListItem("2", "2"));
        ddlName.Items.Add(new ListItem("3", "3"));
        ddlName.Items.Add(new ListItem("4", "4"));
        ddlName.Items.Add(new ListItem("5", "5"));
        ddlName.Items.Add(new ListItem("6", "6"));
        ddlName.Items.Add(new ListItem("7", "7"));
        ddlName.Items.Add(new ListItem("8", "8"));
        ddlName.Items.Add(new ListItem("9", "9"));
        ddlName.Items.Add(new ListItem("10", "10"));
        ddlName.Items.Add(new ListItem("11", "11"));
        ddlName.Items.Add(new ListItem("12", "12"));
        ddlName.Items.Add(new ListItem("13", "13"));
        ddlName.Items.Add(new ListItem("14", "14"));
        ddlName.Items.Add(new ListItem("15", "15"));
        ddlName.Items.Add(new ListItem("16", "16"));
        ddlName.Items.Add(new ListItem("17", "17"));
        ddlName.Items.Add(new ListItem("18", "18"));
        ddlName.Items.Add(new ListItem("19", "19"));
        ddlName.Items.Add(new ListItem("20", "20"));
        ddlName.Items.Add(new ListItem("21", "21"));
        ddlName.Items.Add(new ListItem("22", "22"));
        ddlName.Items.Add(new ListItem("23", "23"));
        ddlName.Items.Add(new ListItem("24", "24"));
        ddlName.Items.Add(new ListItem("25", "25"));
        ddlName.Items.Add(new ListItem("26", "26"));
        ddlName.Items.Add(new ListItem("27", "27"));
        ddlName.Items.Add(new ListItem("28", "28"));
        ddlName.Items.Add(new ListItem("29", "29"));
        ddlName.Items.Add(new ListItem("30", "30"));
        ddlName.Items.Add(new ListItem("31", "31"));


    }

    protected void GenerateddlWeek(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("1", "1"));
        ddlName.Items.Add(new ListItem("2", "2"));
        ddlName.Items.Add(new ListItem("3", "3"));
        ddlName.Items.Add(new ListItem("4", "4"));
        ddlName.Items.Add(new ListItem("5", "5"));
        ddlName.Items.Add(new ListItem("6", "6"));
        ddlName.Items.Add(new ListItem("7", "7"));

    }


    protected void GenerateddlMonth(DropDownList ddlName)
    {

        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("1", "1"));
        ddlName.Items.Add(new ListItem("2", "2"));
        ddlName.Items.Add(new ListItem("3", "3"));
        ddlName.Items.Add(new ListItem("4", "4"));
        ddlName.Items.Add(new ListItem("5", "5"));
        ddlName.Items.Add(new ListItem("6", "6"));
        ddlName.Items.Add(new ListItem("7", "7"));
        ddlName.Items.Add(new ListItem("8", "8"));
        ddlName.Items.Add(new ListItem("9", "9"));
        ddlName.Items.Add(new ListItem("10", "10"));
        ddlName.Items.Add(new ListItem("11", "11"));
        ddlName.Items.Add(new ListItem("12", "12"));
    }

    protected void GenerateddlYear(DropDownList ddlName)
    {

        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("1", "1"));
    }

    #endregion

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        var ddlday_edit = (DropDownList)e.Row.FindControl("ddlday_edit");
                        var ddldirec_edit = (DropDownList)e.Row.FindControl("ddldirec_edit");
                        var ddlcoo_edit = (DropDownList)e.Row.FindControl("ddlcoo_edit");
                        var ddlunitday_edit = (DropDownList)e.Row.FindControl("ddlunitday_edit");
                        var ddlunitdirector_edit = (DropDownList)e.Row.FindControl("ddlunitdirector_edit");
                        var ddlunitcoo_edit = (DropDownList)e.Row.FindControl("ddlunitcoo_edit");

                        var lblunitday_edit = (Label)e.Row.FindControl("lblunitday_edit");
                        var lblday_edit = (Label)e.Row.FindControl("lblday_edit");
                        var lbldirec_edit = (Label)e.Row.FindControl("lbldirec_edit");
                        var lblunitdirector_edit = (Label)e.Row.FindControl("lblunitdirector_edit");
                        var lblunitcoo_edit = (Label)e.Row.FindControl("lblunitcoo_edit");
                        var lblcoo_edit = (Label)e.Row.FindControl("lblcoo_edit");

                        GenerateddlDay(ddlday_edit);
                        ddlday_edit.SelectedValue = lblday_edit.Text;
                        GenerateddlDay(ddldirec_edit);
                        ddldirec_edit.SelectedValue = lbldirec_edit.Text;
                        GenerateddlDay(ddlcoo_edit);
                        ddlcoo_edit.SelectedValue = lblcoo_edit.Text;

                        ddlunitday_edit.SelectedValue = lblunitday_edit.Text;
                        ddlunitdirector_edit.SelectedValue = lblunitdirector_edit.Text;
                        ddlunitcoo_edit.SelectedValue = lblunitcoo_edit.Text;




                    }

                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();
                btnshow.Visible = false;
                SETBoxAllSearch.Visible = false;

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                btnshow.Visible = true;
                SETBoxAllSearch.Visible = true;
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int m0tidx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtnameth_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtnameth_edit");
                var txtnameen_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtnameen_edit");
                var txteditqty = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txteditqty");
                var txteditunit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txteditunit");
                var ddlunitday_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlunitday_edit");
                var ddlday_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlday_edit");
                var ddlunitdirector_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlunitdirector_edit");
                var ddldirec_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddldirec_edit");
                var ddlunitcoo_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlunitcoo_edit");
                var ddlcoo_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlcoo_edit");

                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster.EditIndex = -1;

                ViewState["m0tidx"] = m0tidx;
                ViewState["time_desc"] = txtnameth_edit.Text;
                ViewState["time_short"] = txtnameen_edit.Text;
                ViewState["qty"] = txteditqty.Text;
                ViewState["unit"] = txteditunit.Text;
                ViewState["unit_doing"] = ddlunitday_edit.SelectedValue;
                ViewState["count_doing"] = ddlday_edit.SelectedValue;
                ViewState["unit_director"] = ddlunitdirector_edit.SelectedValue;
                ViewState["count_director"] = ddldirec_edit.SelectedValue;
                ViewState["unit_coo"] = ddlunitcoo_edit.SelectedValue;
                ViewState["count_coo"] = ddlcoo_edit.SelectedValue;
                ViewState["StatusUpdate_Update"] = StatusUpdate.SelectedValue;

                Update_Master_List();
                SelectMasterList();
                btnshow.Visible = true;
                SETBoxAllSearch.Visible = true;


                break;
        }
    }

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;
        var row = (GridViewRow)ddName.NamingContainer;

        switch (ddName.ID)
        {
            case "ddlunitday":
                if (ddlunitday.SelectedValue == "Day")
                {
                    GenerateddlDay(ddlday);
                }
                else if (ddlunitday.SelectedValue == "Week")
                {
                    GenerateddlWeek(ddlday);
                }
                else if (ddlunitday.SelectedValue == "Month")
                {
                    GenerateddlMonth(ddlday);
                }
                else if (ddlunitday.SelectedValue == "Year")
                {
                    GenerateddlYear(ddlday);
                }

                break;

            case "ddlunitdirector":
                if (ddlunitdirector.SelectedValue == "Day")
                {
                    GenerateddlDay(ddldirec);
                }
                else if (ddlunitdirector.SelectedValue == "Week")
                {
                    GenerateddlWeek(ddldirec);
                }
                else if (ddlunitdirector.SelectedValue == "Month")
                {
                    GenerateddlMonth(ddldirec);
                }
                else if (ddlunitdirector.SelectedValue == "Year")
                {
                    GenerateddlYear(ddldirec);
                }

                break;

            case "ddlunitcoo":
                if (ddlunitcoo.SelectedValue == "Day")
                {
                    GenerateddlDay(ddlcoo);
                }
                else if (ddlunitcoo.SelectedValue == "Week")
                {
                    GenerateddlWeek(ddlcoo);
                }
                else if (ddlunitcoo.SelectedValue == "Month")
                {
                    GenerateddlMonth(ddlcoo);
                }
                else if (ddlunitcoo.SelectedValue == "Year")
                {
                    GenerateddlYear(ddlcoo);
                }

                break;

            case "ddlunitday_edit":


                var ddlunitday_edit = (DropDownList)row.FindControl("ddlunitday_edit");
                var ddlday_edit = (DropDownList)row.FindControl("ddlday_edit");

                if (ddlunitday_edit.SelectedValue == "Day")
                {
                    GenerateddlDay(ddlday_edit);
                }
                else if (ddlunitday_edit.SelectedValue == "Week")
                {
                    GenerateddlWeek(ddlday_edit);
                }
                else if (ddlunitday_edit.SelectedValue == "Month")
                {
                    GenerateddlMonth(ddlday_edit);
                }
                else if (ddlunitday_edit.SelectedValue == "Year")
                {
                    GenerateddlYear(ddlday_edit);
                }

                break;

            case "ddlunitdirector_edit":

                var ddlunitdirector_edit = (DropDownList)row.FindControl("ddlunitdirector_edit");
                var ddldirec_edit = (DropDownList)row.FindControl("ddldirec_edit");

                if (ddlunitdirector_edit.SelectedValue == "Day")
                {
                    GenerateddlDay(ddldirec_edit);
                }
                else if (ddlunitdirector_edit.SelectedValue == "Week")
                {
                    GenerateddlWeek(ddldirec_edit);
                }
                else if (ddlunitdirector_edit.SelectedValue == "Month")
                {
                    GenerateddlMonth(ddldirec_edit);
                }
                else if (ddlunitdirector_edit.SelectedValue == "Year")
                {
                    GenerateddlYear(ddldirec_edit);
                }

                break;

            case "ddlunitcoo_edit":
                var ddlunitcoo_edit = (DropDownList)row.FindControl("ddlunitcoo_edit");
                var ddlcoo_edit = (DropDownList)row.FindControl("ddlcoo_edit");

                if (ddlunitcoo_edit.SelectedValue == "Day")
                {
                    GenerateddlDay(ddlcoo_edit);
                }
                else if (ddlunitcoo_edit.SelectedValue == "Week")
                {
                    GenerateddlWeek(ddlcoo_edit);
                }
                else if (ddlunitcoo_edit.SelectedValue == "Month")
                {
                    GenerateddlMonth(ddlcoo_edit);
                }
                else if (ddlunitcoo_edit.SelectedValue == "Year")
                {
                    GenerateddlYear(ddlcoo_edit);
                }

                break;
        }
    }
    #endregion

    protected void SetDefaultAdd()
    {
        txtnameth.Text = String.Empty;
        txtnameen.Text = String.Empty;
        txtqty.Text = String.Empty;
        ddStatusadd.SelectedValue = "1";
        SETBoxAllSearch.Visible = false;
        GenerateddlDay(ddlday);
        GenerateddlDay(ddldirec);
        GenerateddlDay(ddlcoo);

    }


    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":
                btnshow.Visible = false;
                Panel_Add.Visible = true;
                SetDefaultAdd();
                break;

            case "btnCancel":
                btnshow.Visible = true;
                Panel_Add.Visible = false;
                SETBoxAllSearch.Visible = true;
                break;

            case "btnAdd":
                Insert_MasterData();
                Panel_Add.Visible = false;
                btnshow.Visible = true;
                SelectMasterList();
                SETBoxAllSearch.Visible = true;
                break;
            case "CmdDel":
                int m0tidx = int.Parse(cmdArg);
                ViewState["m0tidx"] = m0tidx;
                Delete_Master_List();
                SelectMasterList();

                break;
            case "btnsearch":
                SelectMasterList();
                break;
            case "BtnBack":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;
        }



    }
    #endregion
}