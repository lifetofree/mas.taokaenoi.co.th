<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_slip.aspx.cs" Inherits="websystem_hr_hr_slip" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbList" runat="server" CommandName="navList" OnCommand="navCommand">
                                ส่วนของพนักงาน</asp:LinkButton>
                        </li>
                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbImport" runat="server" CommandName="navImport" OnCommand="navCommand">
                                นำเข้าข้อมูล</asp:LinkButton>
                        </li>
                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbReport" runat="server" CommandName="navReport" OnCommand="navCommand">
                                ส่วนของ HR</asp:LinkButton>
                        </li>
                    </ul>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->
            </div>
        </nav>
    </div>
    <!--tab menu-->
    <div class="col-md-12">
        <asp:LinkButton ID="lbRelease" runat="server" CssClass="btn btn-warning" OnCommand="btnCommand"
            CommandName="cmdRelease">
            <i class="fas fa-sign-out-alt"></i>&nbsp;Release</asp:LinkButton>
    </div>
    <asp:MultiView ID="mvSystem" runat="server">
        <asp:View ID="viewAuth" runat="server">
            <div class="row vertical">
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-horizontal" role="form">
                        <div class="col-md-12">
                        <div class="form-group">
                            <!-- <label class="col-md-4 control-label">authentication<span class="text-danger">*</span>
                                :</label> -->
                            <div class="col-md-6">
                                <asp:TextBox ID="tbPassword" runat="server" CssClass="form-control"
                                    placeholder="2nd authentication" TextMode="Password"></asp:TextBox>
                            </div>
                            <label class="col-md-2"></label>
                        </div>
                        <div class="form-group">
                            <!-- <label class="col-md-4 control-label"></label> -->
                            <div class="col-md-6">
                                <asp:LinkButton ID="lbAuth" runat="server" CssClass="btn btn-success"
                                    OnCommand="btnCommand" CommandName="cmdAuth">
                                    <i class="fas fa-check-double"></i>&nbsp;authentication</asp:LinkButton>
                            </div>
                            <label class="col-md-2"></label>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
        <asp:View ID="viewList" runat="server">
            <div class="col-md-12" style="margin-top: 20px;">
                <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-responsive" DataKeyNames="u0_idx">
                    <HeaderStyle CssClass="info" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <ItemTemplate>
                                <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รอบการจ่าย">
                            <ItemTemplate>
                                <asp:Label ID="lblPayrollDate" runat="server" Text='<%# Eval("payroll_date") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="การดำเนินการ">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbDownloadData" runat="server" CssClass="btn btn-sm btn-primary"
                                    OnCommand="btnCommand" CommandName="cmdDownload"
                                    CommandArgument='<%# Eval("u0_idx") %>'
                                    OnClientClick="return confirm('คำเตือน\r\nข้อมูลเงินเดือนเป็นความลับส่วนบุคคล\r\nกรุณาใช้ความระมัดระวังในการเปิดหรือสั่งพิมพ์(Print)')">
                                    <i class="fas fa-download"></i>&nbsp;ดาวน์โหลดไฟล์
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>
        <asp:View ID="viewImport" runat="server">
            <div class="col-md-12">
                <asp:FormView ID="fvUploadFiles" runat="server" Width="100%" DefaultMode="Insert">
                    <InsertItemTemplate>
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-md-2 col-md-offset-3 control-label">ประเภทข้อมูล :<span
                                        class="text-danger">*</span></label>
                                <div class="col-md-4 control-label">
                                    <asp:RadioButtonList ID="rblImportType" runat="server" RepeatDirection="Horizontal"
                                        OnSelectedIndexChanged="rblSelectedIndexChanged" AutoPostBack="True">
                                        <asp:ListItem Text="&nbsp;ข้อมูลสลิป&nbsp;" Value="1" Selected="True">
                                        </asp:ListItem>
                                        <asp:ListItem Text="&nbsp;ข้อมูลเงินเดือน&nbsp;" Value="2"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 col-md-offset-3 control-label">วันที่จ่าย :<span
                                        class="text-danger">*</span></label>
                                <div class="col-md-4">
                                    <asp:TextBox ID="tbPayDate" runat="server" CssClass="form-control"
                                        placeholder="dd/mm/yyyy" MaxLength="10">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 col-md-offset-3 control-label">ไฟล์ :<span
                                        class="text-danger">*</span></label>
                                <div class="col-md-4">
                                    <asp:FileUpload ID="fuUpload" ViewStateMode="Enabled" ClientIDMode="Static"
                                        runat="server" CssClass="btn btn-md btn-default multi max-1 accept-xlsx" />
                                    <span class="text-danger">*เฉพาะไฟล์ xlsx เท่านั้น</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 col-md-offset-3 control-label"></label>
                                <div class="col-md-4">
                                    <asp:LinkButton ID="lbImportData" runat="server" CssClass="btn btn-md btn-success"
                                        OnCommand="btnCommand" CommandName="cmdImport"><i
                                            class="fas fa-upload"></i>&nbsp;อัพโหลดข้อมูล
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>

                <div class="pre-scrollable">
                    <asp:GridView ID="gvImport" runat="server" AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-responsive">
                        <HeaderStyle CssClass="info" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รหัสพนักงาน">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpCode" runat="server" Text='<%# Eval("emp_code") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("emp_name_th") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="วัน">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDays" runat="server" Text='<%# Eval("slip_days") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="เงินเดือน">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipESalary" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_e_salary")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ตำแหน่ง">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipEPosition" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_e_position")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="OTx1">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipEOt10" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_e_ot10")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="OTx1.5">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipEOt15" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_e_ot15")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="OTx2">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipEOt20" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_e_ot20")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="OTx3">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipEOt30" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_e_ot30")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ค่าครองชีพ">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipELiving" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_e_living")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ค่าโทรศัพท์">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipETelephone" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_e_telephone")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="เบี้ยขยัน">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipEAllowance" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_e_allowance")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ค่าปฏิบัติงาน">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipEOperating" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_e_operating")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ค่าเดินทาง">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipETravel" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_e_travel")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ค่าคอมมิชชั่น">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipECommission" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_e_commission")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Incentive">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipEIncentive" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_e_incentive")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="คืนค้ำประกัน">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipEGuarantee" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_e_guarantee")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="โบนัส">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipEBonus" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_e_bonus")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="เงินได้อื่น">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipEOther" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_e_other")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รวมรับ">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipEEarnings" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_earnings")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ขาดงาน">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDAbsent" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_d_absent")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="มาสาย">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDLate" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_d_late")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หักกลับก่อน">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDGoback" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_d_goback")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หักลาเกินสิทธิ์">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDOver" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_d_over")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หักบกพร่อง">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDBroken" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_d_broken")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หักAIA">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDAia" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_d_aia")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หักเครื่องแบบ">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDUniform" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_d_uniform")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หักสวัสดิการ">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDWelfare" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_d_welfare")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ของเสียหาย">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDDamage" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_d_damage")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หักอื่นๆ">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDOther" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_d_other")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หักภาษี">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDTax" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_d_tax")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หักกองทุน">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDFund" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_d_fund")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หักประกันสังคม">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDSocial" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_d_social")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ค้ำประกัน">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDGuarantee" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_d_guarantee")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หักเงินกู้">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDLoan" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_d_loan")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หักเงินกรมบังคับคดี">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDLegal" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_d_legal")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รวมจ่าย">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipDeduction" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_deduction")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สุทธิ">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSlipNet" runat="server"
                                        Text='<%# String.Format("{0:n}", Eval("slip_net")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                    <asp:GridView ID="gvImport2" runat="server" AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-responsive">
                        <HeaderStyle CssClass="info" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#">
                                <ItemTemplate>
                                    <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รหัสพนักงาน">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpCode" runat="server" Text='<%# Eval("emp_code") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                                <ItemTemplate>
                                    <asp:Label ID="lblEmpName" runat="server" Text='<%# Eval("emp_name_th") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="อัตรา">
                                <ItemStyle CssClass="text-right" />
                                <ItemTemplate>
                                    <asp:Label ID="lblSalaryRate" runat="server" Text='<%# String.Format("{0:n}", Eval("salary_rate")) %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:GridView ID="gvImportTest" runat="server" AutoGenerteColumns="True"></asp:GridView>
                </div>

                <div class="row" style="margin-top: 10px;">
                    <asp:LinkButton ID="lbSaveData" runat="server" CssClass="btn btn-md btn-success"
                        OnCommand="btnCommand" CommandName="cmdSaveData"><i class="far fa-save"></i>&nbsp;บันทึกข้อมูล
                    </asp:LinkButton>
                </div>
            </div>
        </asp:View>
        <asp:View ID="viewReport" runat="server">
            <div class="col-md-12">
                4nd page Hr report
            </div>
        </asp:View>
    </asp:MultiView>

    <script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            $(".multi").MultiFile();
        })
    </script>

    <%--<br />
    <asp:LinkButton ID="lbTest" runat="server" CommandName="cmdTest" OnCommand="btnCommand">
        test</asp:LinkButton><br />--%>
</asp:Content>