﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="mis_m0_tel.aspx.cs" Inherits="websystem_MasterData_mis_m0_tel" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="txt" runat="server"></asp:Literal>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-earphone"></i><strong>&nbsp; เบอร์ติดต่อองค์กร</strong></h3>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">

                                <asp:LinkButton ID="btnshow" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="AddMasterTel" runat="server" CommandName="CmdAdd" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>

                            <%------------------------ Div ADD  ------------------------%>

                            <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; เพิ่มเบอร์หลักแต่ละสถานที่</strong></h4>
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">
                                            <div class="form-group">
                                                <asp:Label ID="Label26" runat="server" Text="สถานที่" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:DropDownList ID="ddllocname" runat="server" CssClass="form-control"></asp:DropDownList>
                                                </div>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidddator8" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddllocname" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกสถานที่"
                                                    ValidationExpression="กรุณาเลือกสถานที่" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidddator8" Width="160" />


                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="เบอร์ติดต่อ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-5">
                                                    <asp:TextBox ID="txttel" MaxLength="15"  runat="server" CssClass="form-control" PlaceHolder="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="txttel" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกเบอร์ติดต่อสถานที่"
                                                    ValidationExpression="กรุณากรอกเบอร์ติดต่อสถานที่" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="lbl" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                <div class="col-sm-5">
                                                    <asp:DropDownList ID="ddStatusadd" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="lbladd" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                </div>

                            </asp:Panel>



                            <asp:GridView ID="GvMaster" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="m0telidx"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#">

                                        <ItemTemplate>
                                            <asp:Label ID="lblm0telidx" runat="server" Visible="false" Text='<%# Eval("m0telidx") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>


                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" Text="สถานที่" CssClass="col-sm-3 control-label text_right"></asp:Label>

                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="lblLocIDX" Visible="false" CssClass="form-control" runat="server" Text='<%# Bind("LocIDX") %>' Enabled="false" />
                                                            <asp:DropDownList ID="ddllocname_edit" runat="server" CssClass="form-control"></asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldVaslidddator8" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="ddllocname_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกสถานที่"
                                                            ValidationExpression="กรุณาเลือกสถานที่" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldVaslidddator8" Width="160" />

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="เบอร์ติดต่อ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txtlocate_edit" MaxLength="15" runat="server" CssClass="form-control" Text='<%# Eval("m0_tel")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None"
                                                            ControlToValidate="txtlocate_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกเบอร์ติดต่อสถานที่"
                                                            ValidationExpression="กรุณากรอกเบอร์ติดต่อสถานที่" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                        <div class="col-sm-5">
                                                            <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("m0status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="สถานที่" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("LocName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="เบอร์ติดต่อสถานที่" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbcode" runat="server" Text='<%# Eval("m0_tel") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstatus" runat="server" Text='<%# getStatus((int)Eval("m0status")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0telidx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>


                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

