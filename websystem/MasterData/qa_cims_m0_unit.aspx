﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_cims_m0_unit.aspx.cs" Inherits="websystem_MasterData_qa_cims_m0_unit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="test_place" runat="server"></asp:Literal>
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
        <asp:View ID="View1" runat="server">

            <div class="form-group">
                <asp:LinkButton ID="btn_addunit" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่มหน่วยวัด" data-toggle="tooltip" title="เพิ่มหน่วยวัด" runat="server"
                    CommandName="cmdAddUnit" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มหน่วยวัด</asp:LinkButton>
            </div>

            <asp:FormView ID="Fv_Insert_Result" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มหน่วยวัด</h3>

                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">

                                <div class="form-horizontal" role="form">

                                    <div class="panel-heading">

                                        <div class="form-group">
                                            <asp:Label ID="lblunit_name_th" runat="server" Text="ชื่อหน่วยวัดภาษาไทย" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtunit_name_th" runat="server" CssClass="form-control" placeholder="กรอกชื่อหน่วยวัดภาษาไทย ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="Re_unit_name_th" runat="server"
                                                    ControlToValidate="txtunit_name_th" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อหน่วยวัดภาษาไทย" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_uname_th" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_unit_name_th" Width="220" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lblunit_name_en" runat="server" Text="ชื่อหน่วยวัดภาษาอังกฤษ" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtunit_name_en" runat="server" CssClass="form-control" placeholder="กรอกชื่อหน่วยวัดภาษาอังกฤษ ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="Re_unit_name_en" runat="server"
                                                    ControlToValidate="txtunit_name_en" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อหน่วยวัดภาษาอังกฤษ" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_uname_en" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_unit_name_en" Width="220" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lblunit_symbol_th" runat="server" Text="ชื่อย่อหน่วยวัดภาษาไทย" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtunit_symbol_th" runat="server" CssClass="form-control" placeholder="กรอกชื่อย่อหน่วยวัดภาษาไทย ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="Re_unit_symbol_th" runat="server"
                                                    ControlToValidate="txtunit_symbol_th" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อย่อหน่วยวัดภาษาไทย" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_usymbol_th" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_unit_symbol_th" Width="220" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lblunit_symbol_en" runat="server" Text="ชื่อย่อหน่วยวัดภาษาอังกฤษ" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtunit_symbol_en" runat="server" CssClass="form-control" placeholder="กรอกชื่อย่อหน่วยวัดภาษาอังกฤษ ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="Re_unit_symbol_en" runat="server"
                                                    ControlToValidate="txtunit_symbol_en" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อย่อหน่วยวัดภาษาอังกฤษ" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_usymbol_en" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_unit_symbol_en" Width="220" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lab_statusplace" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddUnit" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="Lbtn_submit_unit" ValidationGroup="Save" CssClass="btn btn-success" Text="Save" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="Lbtn_submit_unit" OnCommand="btnCommand"></asp:LinkButton>
                                                <asp:LinkButton ID="Lbtn_cancel_unit" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="Lbtn_cancel_unit" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <%--select Place--%>
            <asp:GridView ID="Gv_select_unit" runat="server" Visible="true"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>ไม่มีข้อมูลหน่วยวัด</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="text-align: center; padding-top: 5px;">
                                    <asp:Label ID="unit_idx" runat="server" Visible="false" Text='<%# Eval("unit_idx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                </div>
                            </small>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="ID_unit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("unit_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อหน่วยวัดภาษาไทย" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="Name_unit_th" runat="server" CssClass="form-control " Text='<%# Eval("unit_name_th") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorunit_name_th" runat="server"
                                                ControlToValidate="Name_unit_th" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อหน่วยวัดภาษาไทย" ValidationGroup="Editunitname" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_unameth" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorunit_name_th" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อหน่วยวัดภาษาอังกฤษ" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="Name_unit_en" runat="server" CssClass="form-control " Text='<%# Eval("unit_name_en") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorunit_name_en" runat="server"
                                                ControlToValidate="Name_unit_en" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อหน่วยวัดภาษาอังกฤษ" ValidationGroup="Editunitname" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_unameen" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorunit_name_en" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label3" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อย่อหน่วยวัดภาษาไทย" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="Symbol_unit_th" runat="server" CssClass="form-control " Text='<%# Eval("unit_symbol_th") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorunit_symbol_th" runat="server"
                                                ControlToValidate="Symbol_unit_th" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อย่อหน่วยวัดภาษาไทย" ValidationGroup="Editunitname" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_usymbolth" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorunit_symbol_th" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label4" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อย่อหน่วยวัดภาษาอังกฤษ" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="Symbol_unit_en" runat="server" CssClass="form-control " Text='<%# Eval("unit_symbol_en") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorunit_symbol_en" runat="server"
                                                ControlToValidate="Symbol_unit_en" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อย่อหน่วยวัดภาษาอังกฤษ" ValidationGroup="Editunitname" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_usymbolen" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorunit_symbol_en" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_unit" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddEdit_unit" Text='<%# Eval("unit_status") %>'
                                                CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editunitname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อหน่วยวัดภาษาไทย" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbunit_name_th" runat="server"
                                        Text='<%# Eval("unit_name_th") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อหน่วยวัดภาษาอังกฤษ" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbunit_name_en" runat="server"
                                        Text='<%# Eval("unit_name_en") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อย่อหน่วยวัดภาษาไทย" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbunit_symbol_th" runat="server"
                                        Text='<%# Eval("unit_symbol_th") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อย่อหน่วยวัดภาษาอังกฤษ" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbunit_symbol_en" runat="server"
                                        Text='<%# Eval("unit_symbol_en") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbpunit_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("unit_status") %>'></asp:Label>
                                    <asp:Label ID="unit_statusOnline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="unit_statusOffline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="btnEdit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="btnTodelete_unit"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบหน่วยวัดนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("unit_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
    </asp:MultiView>


</asp:Content>
