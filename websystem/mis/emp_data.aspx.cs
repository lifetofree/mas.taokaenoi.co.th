using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_mis_emp_data : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];

    static string _urlGetViewEmployeeList = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewEmployeeList"];
    #endregion initial function/data

    protected void Page_Load(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());

        if(!IsPostBack)
        {
            initPage();
        }
    }

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0);
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch(cmdName)
        {
            case "cmdSearch":
                DropDownList ddlOrg = (DropDownList)fvEmpSearch.FindControl("ddlOrg");
                DropDownList ddlDept = (DropDownList)fvEmpSearch.FindControl("ddlDept");
                DropDownList ddlSec = (DropDownList)fvEmpSearch.FindControl("ddlSec");
                DropDownList ddlSelectedDate = (DropDownList)fvEmpSearch.FindControl("ddlSelectedDate");
                DropDownList ddlEmpStatus = (DropDownList)fvEmpSearch.FindControl("ddlEmpStatus");

                _dataEmployee.search_key_emp_list = new search_key_employee[1];
                search_key_employee _search_key = new search_key_employee();

                _search_key.s_emp_code = ((TextBox)fvEmpSearch.FindControl("tbEmpCode")).Text.Trim();
                _search_key.s_emp_name = ((TextBox)fvEmpSearch.FindControl("tbEmpName")).Text.Trim();
                _search_key.s_org_idx = ddlOrg.SelectedItem.Value;
                _search_key.s_rdept_idx = ddlDept.SelectedItem.Value;
                _search_key.s_rsec_idx = ddlSec.SelectedItem.Value;
                _search_key.s_cost_center = ((TextBox)fvEmpSearch.FindControl("tbCostCenter")).Text.Trim();
                _search_key.s_date_field = ddlSelectedDate.SelectedItem.Value;
                _search_key.s_month_year = ((TextBox)fvEmpSearch.FindControl("tbMonthYear")).Text.Trim();
                _search_key.s_emp_status = ddlEmpStatus.SelectedItem.Value;

                _dataEmployee.search_key_emp_list[0] = _search_key;

                //--debug
                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));

                _dataEmployee = getViewEmployeeList(_dataEmployee);
                ViewState["EmpSearchList"] = _dataEmployee.employee_list;
                gvEmployeeList.PageIndex = 0;
                setGridData(gvEmployeeList, ViewState["EmpSearchList"]);
                break;
            case "cmdReset":
                initPage();
                break;
        }
    }
    #endregion event command

    #region paging
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch(gvName.ID)
        {
            case "gvEmployeeList":
                if(ViewState["EmpSearchList"] != null)
                {
                    setGridData(gvEmployeeList, ViewState["EmpSearchList"]);
                }
                else
                {
                    _dataEmployee = new data_employee();
                    _dataEmployee = getViewEmployeeList(_dataEmployee);
                    setGridData(gvEmployeeList, _dataEmployee.employee_list);
                }
                break;
        }
    }
    #endregion paging

    #region drorpdown list
    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_en", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- organization ---", "-1"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_en", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- department ---", "-1"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_en", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("--- section ---", "-1"));
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        DropDownList ddlOrg = (DropDownList)fvEmpSearch.FindControl("ddlOrg");
        DropDownList ddlDept = (DropDownList)fvEmpSearch.FindControl("ddlDept");
        DropDownList ddlSec = (DropDownList)fvEmpSearch.FindControl("ddlSec");

        switch (ddlName.ID)
        {
            case "ddlOrg":
                getDepartmentList(ddlDept, int.Parse(ddlOrg.SelectedItem.Value));
                ddlSec.Items.Clear();
                ddlSec.Items.Insert(0, new ListItem("--- section ---", "-1"));
                break;
            case "ddlDept":
                getSectionList(ddlSec, int.Parse(ddlOrg.SelectedItem.Value), int.Parse(ddlDept.SelectedItem.Value));
                break;
        }
    }
    #endregion dropdown list

    #region formview
    protected void fvDataBound(object sender, EventArgs e)
    {
        FormView fvName = (FormView)sender;
        switch(fvName.ID)
        {
            case "fvEmpSearch":
                getOrganizationList((DropDownList)fvEmpSearch.FindControl("ddlOrg"));
                break;
        }
    }
    #endregion formview

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        gvEmployeeList.PageIndex = 0;
        setActiveTab("docList", 0);
        setFormData(fvEmpSearch, FormViewMode.Insert, null);
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
        ViewState["EmpSearchList"] = null;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _data_employee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_employee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_employee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _data_employee;
    }

    protected data_employee getViewEmployeeList(data_employee _data_employee)
    {
        _data_employee = callServicePostEmployee(_urlGetViewEmployeeList, _data_employee);
        return _data_employee;
    }

    protected void setActiveTab(string activeTab, int uidx)
    {
        setActiveView(activeTab, uidx);
        switch(activeTab)
        {
            // case "docCreate":
            //     li0.Attributes.Add("class", "active");
            //     li1.Attributes.Add("class", "");
            //     li2.Attributes.Add("class", "");
            //     li3.Attributes.Add("class", "");
            // break;
            // case "docListNew":
            //     li0.Attributes.Add("class", "");
            //     li1.Attributes.Add("class", "active");
            //     li2.Attributes.Add("class", "");
            //     li3.Attributes.Add("class", "");
            // break;
            // case "docListDoing":
            //     li0.Attributes.Add("class", "");
            //     li1.Attributes.Add("class", "");
            //     li2.Attributes.Add("class", "active");
            //     li3.Attributes.Add("class", "");
            // break;
            // case "docListDone":
            //     li0.Attributes.Add("class", "");
            //     li1.Attributes.Add("class", "");
            //     li2.Attributes.Add("class", "");
            //     li3.Attributes.Add("class", "active");
            // break;
        }
    }

    protected void setActiveView(string activeTab, int uidx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        switch(activeTab)
        {
            case "docList":
                _dataEmployee = getViewEmployeeList(_dataEmployee);
                setGridData(gvEmployeeList, _dataEmployee.employee_list);
                break;
        }
    }
    #endregion reuse
}