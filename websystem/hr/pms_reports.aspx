<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="pms_reports.aspx.cs" Inherits="websystem_hr_pms_reports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbList" runat="server" CommandName="navList" OnCommand="navCommand">
                                รายงาน</asp:LinkButton>
                        </li>
                        <!-- <li id="li1" runat="server">
                            <asp:LinkButton ID="lbImport" runat="server" CommandName="navImport" OnCommand="navCommand">
                                นำเข้าข้อมูล</asp:LinkButton>
                        </li>
                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbReport" runat="server" CommandName="navReport" OnCommand="navCommand">
                                ส่วนของ HR</asp:LinkButton>
                        </li> -->
                    </ul>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->
            </div>
        </nav>
    </div>
    <!--tab menu-->

    <asp:MultiView ID="mvSystem" runat="server">
        <asp:View ID="viewList" runat="server">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">ค้นหาข้อมูล</h3>
                    </div>
                    <div class="panel-body">
                        <asp:FormView ID="fvSearch" runat="server" Width="100%" DefaultMode="Insert">
                            <InsertItemTemplate>
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="form-horizontal">
                                        <!-- <div class="form-group">
                                            <label class="col-md-2 control-label">รหัสพนักงาน</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control"
                                                    MaxLength="100" placeholder="รหัสพนักงาน"></asp:TextBox>
                                            </div>
                                            <label class="col-md-2 control-label">ชื่อ-นามสกุล</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control"
                                                    MaxLength="250" placeholder="ชื่อ-นามสกุล"></asp:TextBox>
                                            </div>
                                        </div> -->
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">องค๋กร<span
                                                    class="text-danger">*</span></label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlOrganization" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">กลุ่มงาน</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlWorkGroup" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">สายงาน</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlLineWork" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">ฝ่าย</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlDepartment" runat="server"
                                                    Cssclass="form-control"
                                                    OnSelectedIndexChanged="ddlSelectedIndexChanged"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">แผนก</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlSection" runat="server" Cssclass="form-control"
                                                    AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                            <!-- <label class="col-md-2 control-label">ตำแหน่ง</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlPosition" runat="server"
                                                    Cssclass="form-control">
                                                </asp:DropDownList>
                                            </div> -->
                                            <!-- <label class="col-md-2 control-label">กลุ่มพนักงาน</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlEmployeeGroup" runat="server"
                                                    Cssclass="form-control">
                                                </asp:DropDownList>
                                            </div> -->
                                            <label class="col-md-2"></label>
                                            <label class="col-md-4"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2"></label>
                                            <div class="col-md-10">
                                                <!-- <asp:UpdatePanel ID="upExportData" runat="server">
                                                    <ContentTemplate> -->
                                                <asp:LinkButton ID="lbSearchData" runat="server"
                                                    CssClass="btn btn-md btn-primary" OnCommand="btnCommand"
                                                    CommandName="cmdSearch"><i class="fas fa-search"></i>&nbsp;ค้นหา
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="lbResetData" runat="server"
                                                    CssClass="btn btn-md btn-info" OnCommand="btnCommand"
                                                    CommandName="cmdReset"><i class="fas fa-redo"></i>&nbsp;เคลียร์ค่า
                                                </asp:LinkButton>

                                                <asp:LinkButton ID="lbExportData" runat="server"
                                                    CssClass="btn btn-md btn-success" OnCommand="btnCommand"
                                                    CommandName="cmdExport"><i
                                                        class="far fa-file-excel"></i>&nbsp;Export
                                                </asp:LinkButton>
                                                <!-- </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="lbExportData" />
                                                    </Triggers>
                                                </asp:UpdatePanel> -->
                                            </div>
                                            <!-- <label class="col-md-2"></label>
                                            <label class="col-md-4"></label> -->
                                        </div>
                                    </div>
                                </div>
                                <label class="col-md-1"></label>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Overall</h3>
                    </div>
                    <div class="panel-body">
                        <div id="chartOverall" runst="server">
                            <asp:Literal ID="litChartOverall" runat="server"></asp:Literal>

                            <!-- <div class="progress">
                                    <div class="progress-bar" role="progressbar" style="width: 20%" aria-valuenow="20"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                    <div class="progress-bar progress-bar-danger" role="progressbar" style="width: 20%"
                                        aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                    <div class="progress-bar progress-bar-success" role="progressbar" style="width: 20%"
                                        aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div> -->
                            <span class="btn-xs btn-primary">&nbsp;</span>&nbsp;อยู่ในระหว่างดำเนินการ<br>
                            <span class="btn-xs btn-danger">&nbsp;</span>&nbsp;ยังไม่เริ่มดำเนินการ<br>
                            <span class="btn-xs btn-success">&nbsp;</span>&nbsp;ดำเนินการแล้วเสร็จ
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <h3 class="panel-title">On Process</h3>
                    </div>
                    <div class="panel-body">
                        <asp:Literal ID="litChartOnProgress" runat="server"></asp:Literal>

                        <!-- <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: 20%" aria-valuenow="20"
                                    aria-valuemin="0" aria-valuemax="100"></div>
                                <div class="progress-bar progress-bar-info" role="progressbar" style="width: 20%"
                                    aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                <div class="progress-bar progress-bar-danger" role="progressbar" style="width: 20%"
                                    aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                <div class="progress-bar progress-bar-warning" role="progressbar" style="width: 20%"
                                    aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                                <div class="progress-bar progress-bar-success" role="progressbar" style="width: 20%"
                                    aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
                            </div> -->

                        <span class="btn-xs btn-primary">&nbsp;</span>&nbsp;รอหัวหน้าประเมิน (Solid)<br>
                        <span class="btn-xs btn-info">&nbsp;</span>&nbsp;รอหัวหน้าประเมิน (Dotted)<br>
                        <span class="btn-xs btn-danger">&nbsp;</span>&nbsp;รอผู้อนุมัติการประเมิน (ลำดับ 1)<br>
                        <span class="btn-xs btn-warning">&nbsp;</span>&nbsp;รอผู้อนุมัติการประเมิน (ลำดับ 2)<br>
                        <span class="btn-xs btn-success">&nbsp;</span>&nbsp;รอผู้อนุมัติการประเมิน (ลำดับ 3)
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <asp:GridView ID="gvReportList" runat="server" AutoGenerateColumns="False" AllowPaging="False"
                    ShowFooter="False" OnRowDataBound="gvRowDataBound" PageSize="10"
                    OnPageIndexChanging="gvPageIndexChanging"
                    CssClass="table table-striped table-bordered table-responsive">
                    <HeaderStyle CssClass="info" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First"
                        LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <ItemTemplate>
                                <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="แผนก">
                            <ItemTemplate>
                                <asp:Label ID="lblSecNameTh" runat="server" Text='<%# Eval("sec_name_th") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ฝ่าย">
                            <ItemTemplate>
                                <asp:Label ID="lblDeptNameTh" runat="server" Text='<%# Eval("dept_name_th") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สายงาน">
                            <ItemTemplate>
                                <asp:Label ID="lblLwNameTh" runat="server" Text='<%# Eval("lw_name_th") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="กลุ่มงาน">
                            <ItemTemplate>
                                <asp:Label ID="lblWgNameTh" runat="server" Text='<%# Eval("wg_name_th") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="จำนวนพนักงานทั้งหมด">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpCount" runat="server" Text='<%# Eval("emp_count") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumEmpCount" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="อยู่ในระหว่างดำเนินการ">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpOnProcessCount" runat="server">
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumOnProcessCount" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="%">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpOnProcessPercent" runat="server">
                                </asp:Label>%
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumOnProcessPercent" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ยังไม่เริ่มดำเนินการ">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpNotStartCount" runat="server"
                                    Text='<%# Eval("emp_count_not_start") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumNotStartCount" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="%">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpNotStartPercent" runat="server">
                                </asp:Label>%
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumNotStartPercent" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ดำเนินการแล้วเสร็จ">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpFinishedCount" runat="server"
                                    Text='<%# Eval("emp_count_finished") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumFinishedCount" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="%">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpFinishedPercent" runat="server">
                                </asp:Label>%
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumFinishedPercent" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ประเมินตัวเองเสร็จแล้ว">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpMyselfCount" runat="server" Text='<%# Eval("emp_count_myself") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumMyselfCount" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="%">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpMyselfPercent" runat="server">
                                </asp:Label>%
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumMyselfPercent" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รอหัวหน้าประเมิน (Solid)">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpSolidCount" runat="server" Text='<%# Eval("emp_count_solid") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumSolidCount" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="%">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpSolidPercent" runat="server">
                                </asp:Label>%
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumSolidPercent" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รอหัวหน้าประเมิน (Dotted)">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpDottedCount" runat="server" Text='<%# Eval("emp_count_dotted") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumDottedCount" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="%">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpDottedPercent" runat="server">
                                </asp:Label>%
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumDottedPercent" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รอผู้อนุมัติการประเมิน (ลำดับ 1)">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpApprove1Count" runat="server"
                                    Text='<%# Eval("emp_count_approve1") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumApprove1Count" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="%">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpApprove1Percent" runat="server">
                                </asp:Label>%
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumApprove1Percent" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รอผู้อนุมัติการประเมิน (ลำดับ 2)">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpApprove2Count" runat="server"
                                    Text='<%# Eval("emp_count_approve2") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumApprove2Count" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="%">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpApprove2Percent" runat="server">
                                </asp:Label>%
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumApprove2Percent" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รอผู้อนุมัติการประเมิน (ลำดับ 3)">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpApprove3Count" runat="server"
                                    Text='<%# Eval("emp_count_approve3") %>'>
                                </asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumApprove3Count" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="%">
                            <ItemStyle CssClass="text-right"></ItemStyle>
                            <ItemTemplate>
                                <asp:Label ID="lblEmpApprove3Percent" runat="server">
                                </asp:Label>%
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblSumApprove3Percent" runat="server"></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>