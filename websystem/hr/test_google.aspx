﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="test_google.aspx.cs" Inherits="websystem_hr_test_google" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <div class="col-md-12">
        <asp:Label ID="lblCalendarAnnounce" runat="server" CssClass="lblCalendarAnnounce" />
        <%-- <button type="button" id="btnAnnounce" class="btn btn-success m-t-10 f-s-14 btnAnnounce pull-right">
            บันทึก
        </button>--%>
        <%-- <div class="img-loading-announce pull-right m-t-10">
            <img src='<%= ResolveUrl("~/masterpage/images/loading.gif") %>'
                width="30" height="30" />
        </div>--%>
    </div>
    

    <!--*** START JS Area ***-->
    <script type="text/javascript">

        var empIds = [[], []];
        var empIdxDiaryArr = [];
        var empCodeDiaryArr = [];
        var empFullNameDiaryArr = [];
        var empIdxArr = [];
        var empCodeArr = [];
        var empFullNameArr = [];
        var empCount = 0;
        var empDiaryCount = 0;
        var groupDiaryIdxArr = [];
        var groupDiaryNameArr = [];
        var groupDiaryDescriptionArr = [];
        var groupDiaryCount = 0;

        function getContrastTextColor(hexcolor) {
            var r = parseInt(hexcolor.substr(1, 2), 16);
            var g = parseInt(hexcolor.substr(3, 2), 16);
            var b = parseInt(hexcolor.substr(5, 2), 16);
            var textColor = (r * 0.2126) + (g * 0.7152) + (b * 0.0722);
            return parseInt(textColor) > 255 / 2 ? '#000000' : '#ffffff';
        }

        /** START Change Month in Calendar After Change Datepicker **/
        $('.from-date-announce-datepicker').on('dp.change', function (e) {
            if ($(this).val() != "") {
                var dateAnnounceVal = $(this).val();
                var splitDateAnnounce = dateAnnounceVal.split("/");
                var interDateAnnounce = splitDateAnnounce[2] + '-' + splitDateAnnounce[1] + '-' + splitDateAnnounce[0];
                $('.lblCalendarAnnounce').fullCalendar('gotoDate', interDateAnnounce);
                $('.to-date-announce-datepicker').data("DateTimePicker").minDate(e.date);
            }
        });
        /** END Change Month in Calendar After Change Datepicker **/


        /** START Get calendar for use announce **/
        $('.lblCalendarAnnounce').fullCalendar({

            ////customButtons: {
            ////    removeAllEvents: {
            ////        text: 'ล้างค่า',
            ////        click: function () {
            ////            $('.lblCalendarAnnounce').fullCalendar('removeEvents');
            ////        }
            ////    }
            ////},
            header: {
                left: 'removeAllEvents today prev,next',
                center: 'title',
                right: 'month,agendaWeek,agendaDay',
            },

            resourceLabelText: 'Rooms',
           
            editable: true,
            eventDurationEditable: false,
            droppable: true,
            dragRevertDuration: 150,
            eventRender: function (event, element) {
                element.append("<span class='remove-event'><i class='fa fa-remove'></i></span>");
                element.find(".remove-event").click(function () {
                    $('.lblCalendarAnnounce').fullCalendar('removeEvents', event._id);
                });s
                element.append("<div class='clearfix'></div>");
            },

            eventConstraint: {
                start: moment().format('YYYY-MM-DD'),
                end: '2200-01-01'
            }
            
});
        /** END Get calendar for use announce **/



    </script>
</asp:Content>
