﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_nwd_m0_chamber : System.Web.UI.Page
{
    #region initial function/data

    function_tool _funcTool = new function_tool();
    data_networkdevices _dtnetde = new data_networkdevices();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlSelectChamber = _serviceUrl + ConfigurationManager.AppSettings["urlSelectChamber"];
    static string urlInsertChamber = _serviceUrl + ConfigurationManager.AppSettings["urlInsertChamber"];
    static string urlDeleteChamber = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteChamber"];


    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;

    #endregion

    #region PageLoad

    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            SelectList();
        }
    }

    #endregion

    #region CallService
    protected data_networkdevices callServiceNetwork(string _cmdUrl, data_networkdevices _dtnetde)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dtnetde);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _dtnetde = (data_networkdevices)_funcTool.convertJsonToObject(typeof(data_networkdevices), _localJson);

        return _dtnetde;
    }

    #endregion

    #region Select SQL
    protected void SelectList()
    {

        data_networkdevices _dtnetde = new data_networkdevices();
        _dtnetde.m0chamber_list = new m0chamber_detail[1];
        m0chamber_detail chamber_add = new m0chamber_detail();

        chamber_add.CHIDX = 0;

        _dtnetde.m0chamber_list[0] = chamber_add;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_data_system1));
        _dtnetde = callServiceNetwork(urlSelectChamber, _dtnetde);

        setGridData(GvMaster, _dtnetde.m0chamber_list);
    }

    #endregion

    #region SetFunction

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected void ClearDefault()
    {
        txtaddchamber.Text = String.Empty;
        ddlChamberStatus.Items.Clear();
        ddlChamberStatus.AppendDataBoundItems = true;
        ddlChamberStatus.Items.Add(new ListItem("Online", "1"));
        ddlChamberStatus.Items.Add(new ListItem("Offline", "0"));

    }

    #endregion

    #region GridView

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                SelectList();

                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                 break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectList();

                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                int CHIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtchamber_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtchamber_update");
                var ddlFloorStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlFloorStatusUpdate");

                GvMaster.EditIndex = -1;

                _dtnetde.m0chamber_list = new m0chamber_detail[1];
                m0chamber_detail _m0chamber_update = new m0chamber_detail();

                _m0chamber_update.CHIDX = CHIDX;
                _m0chamber_update.Chamber_name = txtchamber_update.Text;
                _m0chamber_update.CH_status = int.Parse(ddlFloorStatusUpdate.SelectedValue);
                _m0chamber_update.CEmpIDX = emp_idx;

                _dtnetde.m0chamber_list[0] = _m0chamber_update;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));
                _dtnetde = callServiceNetwork(urlInsertChamber, _dtnetde);

                if (_dtnetde.return_code == 0)
                {
                    //initPage();
                    //setDataList(dtlMenu, _dataMenu.m0_menu_list);  
                    SelectList();


                }
                else
                {
                    setError(_dtnetde.return_code.ToString() + " - " + _dtnetde.return_msg);
                }


                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectList();
                break;
        }
    }

    #endregion

    #region BTN
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        int _cemp_idx;
        string _chamber_name;
        int CHIDX;

        m0chamber_detail _m0chamber = new m0chamber_detail();

        switch (cmdName)
        {
            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);
                break;

            case "btnInsert":
                _chamber_name = ((TextBox)ViewIndex.FindControl("txtaddchamber")).Text.Trim();
                _cemp_idx = emp_idx;

                _dtnetde.m0chamber_list = new m0chamber_detail[1];
                _m0chamber.Chamber_name = _chamber_name;
                _m0chamber.CH_status = int.Parse(ddlChamberStatus.SelectedValue);
                _m0chamber.CEmpIDX = _cemp_idx;

                _dtnetde.m0chamber_list[0] = _m0chamber;

                _dtnetde = callServiceNetwork(urlInsertChamber, _dtnetde);

                if (_dtnetde.return_code == 0)
                {
                    //ClearDefault();
                    //SelectList();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);

                }
                else
                {
                    setError(_dtnetde.return_code.ToString() + " - " + _dtnetde.return_msg);
                }
                break;


            case "btnCancel":

                ClearDefault();
                SelectList();
                MvMaster.SetActiveView(ViewIndex);

                break;

            case "btnDelete":

                CHIDX = int.Parse(cmdArg);
                _cemp_idx = emp_idx;


                _dtnetde.m0chamber_list = new m0chamber_detail[1];
                _m0chamber.CHIDX = CHIDX;
                _m0chamber.CEmpIDX = _cemp_idx;

                _dtnetde.m0chamber_list[0] = _m0chamber;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_networkdevices));

                _dtnetde = callServiceNetwork(urlDeleteChamber, _dtnetde);


                if (_dtnetde.return_code == 0)
                {

                    SelectList();

                }
                else
                {
                    setError(_dtnetde.return_code.ToString() + " - " + _dtnetde.return_msg);
                }

                break;
        }
    }
    #endregion

}