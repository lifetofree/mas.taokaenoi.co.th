﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="itr_m0_CaseRESLV3.aspx.cs" Inherits="websystem_MasterData_itr_m0_CaseRESLV3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="text" runat="server"></asp:Literal>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; Case Close Job RES LV3</strong></h3>
                        </div>

                        <div class="panel-body">
                            <%-- <div class="form-horizontal" role="form">--%>
                            <div class="form-group">

                                <asp:LinkButton ID="btnadd" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Add" runat="server" CommandName="CmdAdd" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>

                            <%------------------------ Div ADD ผูกผู้ถือครอง Support and Holder ------------------------%>

                            <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp;Add Case Close Job RES LV3</strong></h4>
                                    <div class="form-horizontal" role="form">
                                        <%--<div class="panel panel-default">--%>
                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="Case LV1." CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:DropDownList ID="ddlLV1" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddlLV1" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกเลือกเคสปิดงานLV.1"
                                                        ValidationExpression="กรุณากรอกเลือกเคสปิดงานLV.1" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="Case LV2." CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:DropDownList ID="ddlLV2" AutoPostBack="true" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="0">กรุณาเลือก Case ปิดงาน LV.2</asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidddator8" ValidationGroup="Save" runat="server" Display="None"
                                                        ControlToValidate="ddlLV2" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกเลือกเคสปิดงานLV.2"
                                                        ValidationExpression="กรุณากรอกเลือกเคสปิดงานLV.2" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidddator8" Width="160" />

                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="Label26" runat="server" Text="Name" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-8">
                                                    <asp:TextBox ID="txtlv" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtlv" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกชื่อเคสปิดงาน"
                                                    ValidationExpression="กรุณากรอกชื่อเคสปิดงาน"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressiondValidator1" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtlv"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressiondValidator1" Width="160" />

                                            </div>



                                            <div class="form-group">
                                                <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                                <div class="col-sm-8">
                                                    <asp:DropDownList ID="ddStatusadd" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="lbladd" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <hr />
                                    <%--  </div>--%>
                                </div>
                            </asp:Panel>


                            <asp:GridView ID="GvMaster" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="RES3IDX"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#">

                                        <ItemTemplate>
                                            <asp:Label ID="lblpidx" runat="server" Visible="false" Text='<%# Eval("RES3IDX") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>


                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtPIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("RES3IDX")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="Case LV1." />
                                                        <div class="col-sm-8">
                                                            <asp:Label ID="lbLV1" runat="server" Text='<%# Bind("RES1IDX") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlRES1IDX_edit" Enabled="false" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save_edit" runat="server" Display="None"
                                                            ControlToValidate="ddlRES1IDX_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกเลือกเคสปิดงานLV.1"
                                                            ValidationExpression="กรุณากรอกเลือกเคสปิดงานLV.1" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label25" CssClass="col-sm-3 control-label" runat="server" Text="Case LV2." />
                                                        <div class="col-sm-8">
                                                            <asp:Label ID="lbLV2" runat="server" Text='<%# Bind("RES2IDX") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlRES2IDX_edit" AutoPostBack="true" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="Save_edit" runat="server" Display="None"
                                                            ControlToValidate="ddlRES2IDX_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกเลือกเคสปิดงานLV.2"
                                                            ValidationExpression="กรุณากรอกเลือกเคสปิดงานLV.2" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="Name" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:TextBox ID="txtname_edit" runat="server" CssClass="form-control" Text='<%# Eval("RES3_Name")%>' />
                                                        </div>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save_edit" runat="server" Display="None" ControlToValidate="txtname_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกชื่อเคสปิดงาน"
                                                            ValidationExpression="กรุณากรอกชื่อเคสปิดงาน"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                            ValidationGroup="Save_edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtname_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                                    </div>


                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("RES3Status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Case LV1 Name" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lborg1" runat="server" Text='<%# Eval("RES1_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Case LV2 Name" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lborg2" runat="server" Text='<%# Eval("RES2_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Name" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lborg3" runat="server" Text='<%# Eval("RES3_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbdep" runat="server" Text='<%# getStatus((int)Eval("RES3Status")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("RES3IDX") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

