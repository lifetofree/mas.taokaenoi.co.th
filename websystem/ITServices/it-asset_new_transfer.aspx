﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="it-asset_new_transfer.aspx.cs" Inherits="websystem_ITServices_it_asset_new_transfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <!--*** START Menu ***-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden;" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-left" runat="server">

                    <li id="_divMenuLiToDivChangeOwn" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivChangeOwn" runat="server"
                            CommandName="_divMenuBtnToDivChangeOwn"
                            OnCommand="btnCommand" Text="รายการทั่วไป" />
                    </li>
                    <li id="_divMenuLiTohr" runat="server">
                        <asp:LinkButton ID="_divMenuBtnTohr" runat="server"
                            CommandName="btntrnf_hr"
                            OnCommand="btnCommand" Text="โอนย้ายอุปกรณ์" />
                    </li>

                    <%--  --%>

                    <li id="_divMenuLiToDivWaitApprove" runat="server" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true" aria-expanded="false">
                            <asp:Label ID="_lbMenuLiToDivWaitApprove" runat="server" Text="รายการรออนุมัติ"></asp:Label>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">

                            <li id="_divMenuLiTooffice" runat="server">

                                <asp:LinkButton ID="btnTooffice" runat="server"
                                    CommandName="btnTooffice"
                                    OnCommand="btnCommand" Text="เจ้าหน้าที่" />
                            </li>

                            <li role="separator" class="divider" runat="server" visible="true"
                                id="_liMenuLiTomanager">

                                <li id="_divMenuLiToDivmanager" runat="server">

                                    <asp:LinkButton ID="btnTomanager" runat="server"
                                        CommandName="btnTomanager"
                                        OnCommand="btnCommand" Text="ผู้จัดการฝ่าย" />
                                </li>

                            </li>


                            <li role="separator" class="divider" runat="server" visible="true"
                                id="_liMenuLiTodirector">

                                <li id="_divMenuLiTodirector" runat="server">
                                    <asp:LinkButton ID="btnTodirector" runat="server"
                                        CommandName="btnTodirector"
                                        OnCommand="btnCommand" Text="ผู้อำนวยการฝ่าย" />
                                </li>
                            </li>


                        </ul>
                    </li>

                    <%--  --%>

                    <li id="_divMenuLiToDivAgency" runat="server" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true" aria-expanded="false">
                            <asp:Label ID="_divMenuLiToLbAgency" runat="server" Text="รายการรออนุมัติโดยต้นสังกัดที่ดูแล"></asp:Label>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">

                            <li role="separator" runat="server" visible="true"
                                id="_liMenuLiTooffice_Agency">

                                <li id="_divMenuLiTooffice_Agency" runat="server">
                                    <asp:LinkButton ID="btnTooffice_Agency" runat="server"
                                        CommandName="btnTooffice_Agency"
                                        OnCommand="btnCommand" Text="เจ้าหน้าที่" />
                                </li>
                            </li>

                            <li role="separator" class="divider" runat="server" visible="true"
                                id="_liMenuLiTomanager_Agency">

                                <li id="_divMenuLiTomanager_Agency" runat="server">
                                    <asp:LinkButton ID="btnTomanager_Agency" runat="server"
                                        CommandName="btnTomanager_Agency"
                                        OnCommand="btnCommand" Text="ผู้จัดการฝ่าย" />
                                </li>

                            </li>

                            <li role="separator" class="divider" runat="server" visible="true"
                                id="_liMenuLiTodirector_Agency">

                                <li id="_divMenuLiTodirector_Agency" runat="server">
                                    <asp:LinkButton ID="btnTodirector_Agency" runat="server"
                                        CommandName="btnTodirector_Agency"
                                        OnCommand="btnCommand" Text="ผู้อำนวยการฝ่าย" />
                                </li>
                            </li>

                        </ul>
                    </li>

                    <%--  --%>

                    <li id="_divMenuLiToDivAsset" runat="server" class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                            aria-haspopup="true" aria-expanded="false">
                            <asp:Label ID="_divMenuLiToLbAsset" runat="server" Text="รายการรออนุมัติโดย Asset"></asp:Label>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">

                            <li role="separator" runat="server" visible="true"
                                id="_liMenuLiTooffice_Asset">

                                <li id="_divMenuLiTooffice_Asset" runat="server">
                                    <asp:LinkButton ID="btnTooffice_Asset" runat="server"
                                        CommandName="btnTooffice_Asset"
                                        OnCommand="btnCommand" Text="เจ้าหน้าที่" />
                                </li>
                            </li>

                            <li role="separator" class="divider" runat="server" visible="true"
                                id="_liMenuLiTomanager_Asset">

                                <li id="_divMenuLiTomanager_Asset" runat="server">
                                    <asp:LinkButton ID="btnTomanager_Asset" runat="server"
                                        CommandName="btnTomanager_Asset"
                                        OnCommand="btnCommand" Text="ผู้จัดการฝ่าย" />
                                </li>
                            </li>

                            <li role="separator" class="divider" runat="server" visible="true"
                                id="_liMenuLiTodirector_Asset">

                                <li id="_divMenuLiTodirector_Asset" runat="server">
                                    <asp:LinkButton ID="btnTodirector_Asset" runat="server"
                                        CommandName="btnTodirector_Asset"
                                        OnCommand="btnCommand" Text="ผู้อำนวยการฝ่าย" />
                                </li>

                            </li>
                        </ul>
                    </li>

                    <%--  --%>
                </ul>
                <ul class="nav navbar-nav navbar-right" runat="server">

                    <li id="_divMenuLiToDocument" runat="server" visible="true">
                        <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                            NavigateUrl="https://docs.google.com/document/d/1Hsl4df275kVJUhqpnMV0cK0_TbcXr2g4nc_CxXKodPs/edit" Target="_blank" Text="คู่มือการใช้งาน" />
                    </li>
                </ul>
            </div>
        </nav>
    </div>
    <!--*** END Menu ***-->

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <%-- start โอนย้ายอุปกรณ์ --%>

        <asp:View ID="View_AssetTransfer_IndexList" runat="server">

            <asp:Panel ID="pnl_trnfcreate" runat="server" Visible="false">
                <div class="row">
                    <div class="col-sm-8"></div>
                    <div class="col-sm-4">
                        <div class="form-group">

                            <asp:LinkButton CssClass="btn btn-default"
                                ID="btntrnf_it" data-toggle="tooltip" title="โอนย้ายอุปกรณ์ IT"
                                runat="server"
                                Visible="false"
                                CommandName="btntrnf_it"
                                OnCommand="btnCommand"><%--<i class="glyphicon glyphicon-transfer"></i>--%> โอนย้ายอุปกรณ์ IT</asp:LinkButton>

                            <asp:LinkButton CssClass="btn btn-primary "
                                ID="btntrnf_hr" data-toggle="tooltip"
                                title="โอนย้ายอุปกรณ์ HR" runat="server"
                                CommandName="btntrnf_hr"
                                OnCommand="btnCommand"><%--<i class="glyphicon glyphicon-transfer"></i>--%> โอนย้ายอุปกรณ์ HR</asp:LinkButton>

                            <asp:LinkButton CssClass="btn btn-success"
                                ID="btntrnf_en" data-toggle="tooltip"
                                title="โอนย้ายอุปกรณ์ EN" runat="server"
                                CommandName="btntrnf_en"
                                OnCommand="btnCommand"><%--<i class="glyphicon glyphicon-transfer"></i>--%> โอนย้ายอุปกรณ์ EN</asp:LinkButton>

                        </div>
                    </div>
                </div>
            </asp:Panel>


            <div class="col-lg-12">

                <asp:Panel runat="server" ID="content_Homepage">
                    <div class="form-group">

                        <asp:LinkButton CssClass="btn btn-info btn-group-vertical"
                            ID="btnshowBoxsearch" data-toggle="tooltip" title="ค้นหารายการ" runat="server"
                            CommandName="showBoxsearch"
                            OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i> ค้นหารายการ</asp:LinkButton>

                        <asp:LinkButton CssClass="btn btn-danger btn-group-vertical"
                            ID="btnhiddenBoxsearch" data-toggle="tooltip" title="ยกเลิกค้นหารายการ"
                            runat="server"
                            CommandName="hiddenBoxsearch" Visible="false"
                            OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i> ยกเลิกค้นหารายการ</asp:LinkButton>

                    </div>
                    <%--  ค้นหา--%>
                    <div class="row">
                        <asp:Panel ID="panel_search" runat="server" Visible="false">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <i class="glyphicon glyphicon-search"></i>&nbsp;
                                    <b>search</b> (ค้นหารายการ)
                                </div>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <%-- เงื่อนไขการค้นหา--%>

                                        <div class="form-group">
                                            <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่ค้นหา : " />
                                            <div class="col-sm-2">
                                                <asp:DropDownList ID="ddlcondition" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control">
                                                    <asp:ListItem Text="เลือกเงื่อนไข...." Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="มากกว่า >" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="น้อยกว่า <" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="ระหว่าง <>" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <div class="col-sm-3">
                                                <div class='input-group date from-date-datepicker'>
                                                    <asp:TextBox ID="txtstartdate" runat="server" placeholder="จากวันที่..."
                                                        CssClass="form-control from-date-datepicker"></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                                </div>
                                            </div>


                                            <div class="col-sm-3">
                                                <div class='input-group date from-date-datepicker'>
                                                    <asp:TextBox ID="txtenddate" runat="server" Enabled="false" placeholder="ถึงวันที่..."
                                                        CssClass="form-control from-date-datepicker"></asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label17" CssClass="col-sm-2 control-label" runat="server" Text="รหัสเอกสาร : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtdocumentcode" runat="server" placeholder="รหัสเอกสาร"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label18" CssClass="col-sm-2 control-label" runat="server" Text="รหัสพนักงาน : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtempcode" runat="server" MaxLength="8" placeholder="รหัสพนักงาน"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label19" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อ-นามสกุล : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txtfirstname_lastname" runat="server" placeholder="ชื่อ-นามสกุล"
                                                    CssClass="form-control"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label20" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlsearch_organization"
                                                    runat="server" CssClass="form-control fa-align-left"
                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" Enabled="true">
                                                </asp:DropDownList>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label21" CssClass="col-sm-2 control-label" runat="server" Text="ฝ่าย : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlsearch_department"
                                                    runat="server" CssClass="form-control fa-align-left"
                                                    AutoPostBack="true" Enabled="true">
                                                </asp:DropDownList>
                                            </div>

                                            <asp:Label ID="Label22" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddllocation_search"
                                                    runat="server" CssClass="form-control fa-align-left"
                                                    Enabled="true">
                                                </asp:DropDownList>
                                            </div>

                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label25" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlsearch_typequipment"
                                                    runat="server" CssClass="form-control fa-align-left"
                                                    AutoPostBack="true" Enabled="true">
                                                </asp:DropDownList>
                                            </div>

                                            <%--<asp:Label ID="Label38" CssClass="col-sm-2 control-label" runat="server" Text="สถานะรายการ : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlsearch_status"
                                                    runat="server" CssClass="form-control fa-align-left"
                                                    AutoPostBack="true" Enabled="true">
                                                </asp:DropDownList>
                                            </div>--%>
                                        </div>

                                        <div class="form-group">
                                            <%--   <div class="col-sm-2"></div>--%>
                                            <div class="col-lg-2 col-lg-offset-2">
                                                <asp:LinkButton CssClass="btn btn-info btn-sm" ID="btnsearch" data-toggle="tooltip" title="ค้นหารายการ" runat="server"
                                                    CommandName="searching" Visible="true" OnCommand="btnCommand"><i class="glyphicon glyphicon-search"></i> ค้นหา</asp:LinkButton>
                                                <%--    </div>
                                        <div class="col-sm-1">--%>
                                                <asp:LinkButton CssClass="btn btn-default btn-sm" ID="btnresetsearch" data-toggle="tooltip" title="รีเซ็ต" runat="server"
                                                    CommandName="reset_search" Visible="true" OnCommand="btnCommand"><i class="glyphicon glyphicon-refresh"></i> รีเซ็ต</asp:LinkButton>
                                            </div>
                                        </div>
                                        <%-- <div class="row">
                                    <h5>&nbsp;</h5>
                                      
                                </div>--%>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                </asp:Panel>

                <%--  select all อนุมัติทั้งหมด --%>
                <div class="form-horizontal" role="form">
                    <asp:Panel ID="Panel_approve_trnf" runat="server" Visible="false">

                        <div class="panel panel-default">
                            <!-- Add Panel Heading Here -->
                            <div class="panel-body">

                                <div class="row">
                                    <div class="form-group">


                                        <asp:Label ID="Label90" class="col-md-2 control-label"
                                            runat="server" Text="" />
                                        <div class="col-md-8">
                                            <asp:CheckBox ID="check_approve_all_trnf" runat="server" AutoPostBack="true"
                                                RepeatDirection="Vertical" CssClass="checkbox checkbox-primary"
                                                Font-Bold="true"
                                                Text="เลือกทั้งหมด "
                                                OnCheckedChanged="checkindexchange" /><%--&nbsp;--%>
                                            <%--<strong>เลือกทั้งหมด</strong>--%>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <asp:Label ID="Label91" class="col-md-2 control-label"
                                            runat="server" Text="หมายเหตุ : "
                                            Font-Bold="true" />
                                        <div class="col-md-8">
                                            <asp:TextBox ID="txtremark_approve_all_trnf" TextMode="multiline" Rows="3" CssClass="form-control" runat="server"></asp:TextBox>

                                            <asp:RequiredFieldValidator ID="Requiredtxtremark_approve_all_trnf"
                                                ValidationGroup="all_confirm" runat="server" Display="None"
                                                ControlToValidate="txtremark_approve_all_trnf" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกหมายเหตุ"
                                                SetFocusOnError="true" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20"
                                                runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                TargetControlID="Requiredtxtremark_approve_all_trnf" Width="160" />


                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group">
                                        <asp:Label ID="Label92" class="col-md-2 control-label" runat="server" Text="" />
                                        <div class="col-md-8">
                                            <asp:LinkButton CssClass="btn btn-success btn-sm" ID="btnapproveall_trnf"
                                                data-toggle="tooltip" title="อนุมัติ" runat="server"
                                                CommandName="confirm_approve_trnf" Visible="true"
                                                CommandArgument="1"
                                                ValidationGroup="all_confirm"
                                                OnClientClick="return confirm('คุณต้องการอนุมัติรายการโอนย้ายนี้ใช่หรือไม่ ?')"
                                                OnCommand="btnCommand"><i class="fa fa-check-circle"></i> &nbsp; อนุมัติ</asp:LinkButton>
                                            <asp:LinkButton CssClass="btn btn-warning btn-sm" ID="btnnotapprove_trnf"
                                                data-toggle="tooltip" title="ไม่อนุมัติ" runat="server"
                                                CommandName="confirm_no_approve_trnf" Visible="true"
                                                CommandArgument="2"
                                                ValidationGroup="all_confirm"
                                                OnClientClick="return confirm('คุณต้องการอนุมัติรายการโอนย้ายนี้ใช่หรือไม่ ?')"
                                                OnCommand="btnCommand"><i class="fa fa-reply-all"></i> &nbsp; ไม่อนุมัติ</asp:LinkButton>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                    </asp:Panel>
                </div>


                <asp:GridView ID="GvTrnfIndex" Visible="true" runat="server"
                    AutoGenerateColumns="false" DataKeyNames="u0idx"
                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                    HeaderStyle-CssClass="info" HeaderStyle-Height="30px"
                    AllowPaging="true" PageSize="5"
                    OnPageIndexChanging="gvPageIndexChanging"
                    OnRowDataBound="gvRowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                        FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="เลือก" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%"
                            ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9"
                            Visible="false">
                            <ItemTemplate>

                                <asp:Label ID="lb_flow_item" runat="server" Visible="false" Text='<%# Eval("flow_item") %>' />
                                <asp:CheckBox ID="cbrecipients_trnf" runat="server"
                                    AutoPostBack="true" Visible="true"
                                    CssClass="checkbox checkbox-primary"
                                    OnCheckedChanged="checkindexchange"
                                    Text='<%# Container.DataItemIndex %>'
                                    Style="color: transparent;"></asp:CheckBox>
                                <asp:HiddenField ID="hfSelected" runat="server" Value='<%# Eval("selected") %>' />
                                <asp:Label ID="lb_Selected" runat="server" Text="" Visible="false" />

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Width="5%" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                            <ItemTemplate>

                                <asp:Label ID="lb_u0_transf_idx" runat="server" Visible="false" Text='<%# Eval("u0_transf_idx") %>' />
                                <asp:Label ID="lb_u0_idx" runat="server" Visible="false" Text='<%# Eval("u0_idx") %>' />
                                <asp:Label ID="lb_node_idx" runat="server" Visible="false" Text='<%# Eval("node_idx") %>' />
                                <asp:Label ID="lb_actor_idx" runat="server" Visible="false" Text='<%# Eval("actor_idx") %>' />

                                <asp:Label ID="idx" Visible="false" runat="server"><%# (Container.DataItemIndex +1) %></asp:Label>
                                <asp:Label ID="lb_doccode" runat="server" CssClass="col-sm-12">
                               <p></b> &nbsp;<%# Eval("doccode") %></p>
                                </asp:Label>
                                <asp:Label ID="lbdoccode" runat="server" Visible="false" Text='<%# Eval("doccode") %>' />

                                <%-- </div>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รายละเอียดพนักงาน" HeaderStyle-Width="30%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                            <ItemTemplate>
                                <asp:Label ID="lb_emp_code" runat="server" CssClass="col-sm-12">
                                 <p><b>รหัสพนักงาน:</b> &nbsp;<%# Eval("emp_code") %></p>
                                </asp:Label>
                                <asp:Label ID="lb_emp_name_th" runat="server" CssClass="col-sm-12">
                                 <p><b>ชื่อ - นามสกุล:</b> &nbsp;<%# Eval("emp_name_th") %></p>
                                </asp:Label>
                                <asp:Label ID="lb_org_name_th" runat="server" CssClass="col-sm-12">
                                 <p><b>องค์กร:</b> &nbsp;<%# Eval("org_name_th") %></p> 
                                </asp:Label>
                                <asp:Label ID="lb_dept_name_th" runat="server" CssClass="col-sm-12">
                                 <p><b>ฝ่าย:</b> &nbsp;<%# Eval("dept_name_th") %></p>
                                </asp:Label>
                                <asp:Label ID="lb_sec_name_th" runat="server" CssClass="col-sm-12">
                                 <p><b>แผนก:</b> &nbsp;<%# Eval("sec_name_th") %></p>
                                </asp:Label>
                                <asp:Label ID="lb_pos_name_th" runat="server" CssClass="col-sm-12">
                                  <p><b>ตำแหน่ง:</b> &nbsp;<%# Eval("pos_name_th") %></p>
                                </asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รายละเอียดการโอนย้าย" HeaderStyle-Width="30%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                            <ItemTemplate>

                                <asp:Label ID="lb_zdocdate" runat="server" CssClass="col-sm-12 control-label">
                                 <p><b>วันที่จัดทำรายการ:</b> &nbsp;<%# Eval("zdocdate") %></p>
                                </asp:Label>


                                <asp:Label ID="lb_sys_name" runat="server" CssClass="col-sm-12">
                                 <p><b>ประเภทอุปกรณ์:</b> &nbsp;<%# Eval("sys_name") %></p>
                                </asp:Label>


                                <asp:Label ID="lb_n_org_name_th" runat="server" CssClass="col-sm-12">
                                 <p><b>องค์กร:</b> &nbsp;<%# Eval("n_org_name_th") %></p> 
                                </asp:Label>
                                <asp:Label ID="lb_n_dept_name_th" runat="server" CssClass="col-sm-12">
                                 <p><b>ฝ่าย:</b> &nbsp;<%# Eval("n_dept_name_th") %></p>
                                </asp:Label>

                                <asp:Label ID="lb_location_name" runat="server" CssClass="col-sm-12">
                                 <p><b>สถานที่:</b> &nbsp;<%# Eval("location_name") %></p>
                                </asp:Label>

                                <%--<asp:Label ID="lb_remark" runat="server" CssClass="col-sm-12 control-label">
                                 <p><b>หมายเหตุ:</b> &nbsp;<%# Eval("remark") %></p>
                                </asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สถานะการดำเนินการ" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                            <ItemTemplate>

                                <asp:Label ID="lb_status_node" runat="server" CssClass="col-sm-12">
                                 <%# Eval("to_name") %>
                                </asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" ControlStyle-Font-Size="10" HeaderStyle-Font-Size="9">
                            <ItemTemplate>

                                <asp:UpdatePanel ID="updatebtnsaveEdit" runat="server">
                                    <ContentTemplate>

                                        <asp:LinkButton ID="btnmanage_trnf" CssClass="btn btn-info btn-sm" runat="server"
                                            data-toggle="tooltip" title="รายละเอียด" OnCommand="btnCommand"
                                            CommandArgument='<%#
                                            Eval("u0_transf_idx")
                                            %>'
                                            CommandName="btnmanage_trnf">
                                           <i class="fa fa-file"></i>
                                        </asp:LinkButton>

                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnmanage_trnf" />
                                    </Triggers>
                                </asp:UpdatePanel>

                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>



        <asp:View ID="View_AssetTransfer_Insert" runat="server">

            <div class="col-lg-12" id="detailMain" runat="server" visible="true">
                <div class="panel panel-info">

                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียดข้อมูลผู้ทำรายการ</strong></h3>
                    </div>

                    <asp:FormView ID="FvDetailUser_Main" runat="server" Width="100%"
                        DefaultMode="Edit">
                        <EditItemTemplate>

                            <div class="panel-body">

                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="lb_emps_code_purchase" Text='<%# Eval("emp_code") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>

                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล  :</label>
                                        <div class="col-sm-4 control-label textleft">

                                            <asp:TextBox ID="fullname_purchase_approve" Text='<%# Eval("emp_name_th") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="Label7" Text='<%# Eval("org_name_th") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย :</label>

                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="lbdreptnamepurchase" Text='<%# Eval("dept_name_th") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="txt_rsec_idx" Text='<%# Eval("rsec_idx") %>'
                                                runat="server" CssClass="form-control" Visible="false" />
                                            <asp:TextBox ID="Label9" Text='<%# Eval("sec_name_th") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง :</label>

                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="Label10" Text='<%# Eval("pos_name_th") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">เบอร์ติดต่อ :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="Label11" Text='<%# Eval("emp_mobile_no") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">e-mail :</label>

                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="lbemail" Text='<%# Eval("emp_email") %>'
                                                runat="server" CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group" runat="server" id="divshowapprover">

                                        <label class="col-sm-2 control-label">ผู้อนุมัติลำดับที่ 1 :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="text_approve1" CssClass="form-control" Enabled="false" runat="server" Text="" />
                                        </div>
                                        <label class="col-sm-2 control-label">ผู้อนุมัติลำดับที่ 2 :</label>
                                        <div class="col-sm-4 control-label textleft">
                                            <asp:TextBox ID="text_approve2" CssClass="form-control" Enabled="false" runat="server" Text="" />
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </EditItemTemplate>
                    </asp:FormView>

                </div>

                <asp:FormView ID="FvTrnfData" runat="server" Width="100%"
                    DefaultMode="Edit">
                    <EditItemTemplate>
                        <asp:HiddenField ID="hfu0_transf_idx" runat="server" Value='<%# Eval("u0_transf_idx") %>'></asp:HiddenField>
                        <asp:HiddenField ID="hfflow_item" runat="server" Value='<%# Eval("flow_item") %>'></asp:HiddenField>
                        <asp:HiddenField ID="hfu0_idx" runat="server" Value='<%# Eval("u0_idx") %>'></asp:HiddenField>
                        <asp:HiddenField ID="hfNodeIdx" runat="server" Value='<%# Eval("node_idx") %>'></asp:HiddenField>
                        <asp:HiddenField ID="hfActorIdx" runat="server" Value='<%# Eval("actor_idx") %>'></asp:HiddenField>
                        <asp:HiddenField ID="hfsysidx" runat="server" Value='<%# Eval("sysidx") %>'></asp:HiddenField>
                        <asp:HiddenField ID="hfdoc_status" runat="server" Value='<%# Eval("doc_status") %>'></asp:HiddenField>

                        <asp:HiddenField ID="hfemp_idx" runat="server" Value='<%# Eval("emp_idx") %>'></asp:HiddenField>
                        <asp:HiddenField ID="hforg_idx" runat="server" Value='<%# Eval("org_idx") %>'></asp:HiddenField>
                        <asp:HiddenField ID="hfrdept_idx" runat="server" Value='<%# Eval("rdept_idx") %>'></asp:HiddenField>
                        <asp:HiddenField ID="hfrsec_idx" runat="server" Value='<%# Eval("rsec_idx") %>'></asp:HiddenField>
                        <asp:HiddenField ID="hfrpos_idx" runat="server" Value='<%# Eval("rpos_idx") %>'></asp:HiddenField>

                        <asp:HiddenField ID="hfn_emp_idx" runat="server" Value='<%# Eval("n_emp_idx") %>'></asp:HiddenField>
                        <asp:HiddenField ID="hfn_org_idx" runat="server" Value='<%# Eval("n_org_idx") %>'></asp:HiddenField>
                        <asp:HiddenField ID="hfn_rdept_idx" runat="server" Value='<%# Eval("n_rdept_idx") %>'></asp:HiddenField>
                        <asp:HiddenField ID="hfn_rsec_idx" runat="server" Value='<%# Eval("n_rsec_idx") %>'></asp:HiddenField>
                        <asp:HiddenField ID="hfn_rpos_idx" runat="server" Value='<%# Eval("n_rpos_idx") %>'></asp:HiddenField>

                    </EditItemTemplate>
                </asp:FormView>


                <asp:FormView ID="FvTrnfInsert" runat="server" Width="100%"
                    OnDataBound="FvDetail_DataBound"
                    DefaultMode="Insert">

                    <EditItemTemplate>
                        <%--  edit--%>
                        <asp:HiddenField ID="hfu0_transf_idx" runat="server" Value='<%# Eval("u0_transf_idx") %>'></asp:HiddenField>
                        <div class="panel panel-info" runat="server" visible="true">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-file"></i><strong>&nbsp; รายละเอียด</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="col-md-12">
                                    <div class="form-horizontal" role="form">

                                        <div id="SETBoxAllSearch" runat="server">

                                            <asp:Panel ID="pnl_doc" runat="server" Visible="false">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">รหัสเอกสาร :</label>
                                                    <div class="col-sm-4 control-label textleft">
                                                        <asp:Label ID="lbdocument_code" Text='<%# Eval("doccode") %>' runat="server"></asp:Label>
                                                    </div>
                                                    <label class="col-sm-2 control-label">วันที่ขอซื้อ :</label>
                                                    <div class="col-sm-4 control-label textleft">
                                                        <asp:Label ID="Labellld" Text='<%# Eval("zdocdate") %>' runat="server"></asp:Label>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">ประเภทอุปกรณ์ :</label>
                                                    <div class="col-sm-4 control-label textleft">
                                                        <asp:Label ID="lb_sys_name" Text='<%# Eval("sys_name") %>' runat="server"></asp:Label>
                                                    </div>
                                                    <label class="col-sm-2 control-label"></label>
                                                    <div class="col-sm-4 control-label textleft">
                                                    </div>
                                                </div>

                                                <hr style="border-top: 1px solid #8c8b8b;">
                                            </asp:Panel>

                                            <%-- ประเภทอุปกรณ์ --%>
                                            <asp:Panel ID="pnl_system" runat="server">

                                                <div class="form-group">
                                                    <asp:Label ID="Label12" class="col-md-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                                    <div class="col-md-3">
                                                        <asp:DropDownList ID="ddlsystem" runat="server" CssClass="form-control"
                                                            AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="Requiredddlsystem"
                                                            ValidationGroup="SaveAdd"
                                                            runat="server" Display="None"
                                                            ControlToValidate="ddlsystem" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกประเภทอุปกรณ์"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8"
                                                            runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Requiredddlsystem" Width="160" />

                                                    </div>
                                                </div>
                                                <hr style="border-top: 1px solid #8c8b8b;">
                                            </asp:Panel>


                                            <%-- it --%>
                                            <asp:Panel ID="pnl_it" runat="server">
                                                <h4 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียดการโอนย้ายอุปกรณ์ IT</strong></h4>
                                                <br />
                                                <div class="form-group">
                                                    <asp:Label ID="Label59" runat="server" Text="องค์กร : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlAddOrg_trnf" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- องค์กร ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlAddOrg_trnf"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlAddOrg_trnf" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกองค์กร"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender28" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlAddOrg_trnf" Width="160" />

                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label4" runat="server" Text="ฝ่าย : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlAddDep_trnf" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- เลือกฝ่าย ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlAddDep_trnf"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlAddDep_trnf" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกฝ่าย"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender29" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlAddDep_trnf" Width="160" />

                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label86" runat="server" Text="แผนก : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlAddSec_trnf" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- เลือกแผนก ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlAddSec_trnf"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlAddSec_trnf" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกฝ่าย"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender30" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlAddSec_trnf" Width="160" />

                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label3" runat="server" Text="ผู้ถือครอง : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlAddemp_idx_trnf"
                                                            AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- เลือกผู้ถือครอง ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlAddemp_idx_trnf"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlAddemp_idx_trnf" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกผู้ถือครอง"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender31" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlAddemp_idx_trnf" Width="160" />

                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label35" CssClass="col-sm-2 control-label text_right" runat="server" Text="สถานที่ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtplace_idx"
                                                            Text='<%# Eval("n_place_idx") %>'
                                                            runat="server" Visible="false">
                                                        </asp:TextBox>

                                                        <asp:TextBox ID="txtplace_trnf"
                                                            Enabled="false"
                                                            Text='<%# Eval("location_name") %>'
                                                            runat="server" CssClass="form-control fa-align-left">
                                                        </asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="Requiredtxtplace_trnf"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="txtplace_trnf" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกสถานที่"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender32" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Requiredtxtplace_trnf" Width="160" />

                                                    </div>
                                                </div>

                                                <asp:Panel ID="pnl_search" runat="server" Visible="true">
                                                    <hr style="border-top: 1px solid #8c8b8b;">

                                                    <h4 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; ค้นหาอุปกรณ์</strong></h4>
                                                    <br />


                                                    <div class="form-group">
                                                        <asp:Label ID="Label111" runat="server" Text="สถานที่ : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddllocate_it_search" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                                <asp:ListItem Value="0">--- สถานที่ ---</asp:ListItem>
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="Requiredddllocate_it_search"
                                                                ValidationGroup="AddDataGv" runat="server" Display="None"
                                                                ControlToValidate="ddllocate_it_search" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกสถานที่"
                                                                InitialValue="0"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender45" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="Requiredddllocate_it_search" Width="160" />

                                                        </div>

                                                    </div>


                                                    <div class="form-group">

                                                        <asp:Label ID="Label87" runat="server" Text="ประเภทอุปกรณ์" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddlSearch_devices_trnf"
                                                                AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="RequiredddlSearch_devices_trnf"
                                                                ValidationGroup="AddDataGv" runat="server" Display="None"
                                                                ControlToValidate="ddlSearch_devices_trnf" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกประเภทอุปกรณ์"
                                                                InitialValue="0"
                                                                SetFocusOnError="true" />
                                                            <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender33" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="RequiredddlSearch_devices_trnf" Width="160" />--%>
                                                        </div>

                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label88" runat="server" Text="Asset Num. : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddlasset_no_trnf" runat="server" CssClass="form-control">
                                                            </asp:DropDownList>

                                                            <asp:RequiredFieldValidator ID="Requiredddlasset_no_trnf"
                                                                ValidationGroup="AddDataGv" runat="server" Display="None"
                                                                ControlToValidate="ddlasset_no_trnf" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือก Asset Num."
                                                                InitialValue="0"
                                                                SetFocusOnError="true" />
                                                            <%-- <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender34" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="Requiredddlasset_no_trnf" Width="160" />--%>
                                                        </div>

                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-md-2 col-md-offset-2">
                                                            <asp:LinkButton ID="tbnAddTransferDevice" CssClass="btn btn-warning btn-sm" runat="server"
                                                                CommandName="tbnAddTransferDevice" OnCommand="btnCommand"
                                                                ValidationGroup="AddDataGv" title="Save">
                                                <i class="glyphicon glyphicon-plus-sign"></i> ADD</asp:LinkButton>
                                                        </div>

                                                    </div>
                                                </asp:Panel>

                                            </asp:Panel>
                                            <%-- en --%>
                                            <asp:Panel ID="pnl_en" runat="server">
                                                <h4 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียดการโอนย้ายอุปกรณ์ EN</strong></h4>
                                                <br />

                                                <div class="form-group">
                                                    <asp:Label ID="Label93" runat="server" Text="องค์กร : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlAddOrg_trnf_en" AutoPostBack="true" runat="server"
                                                            CssClass="form-control"
                                                            OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- องค์กร ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlAddOrg_trnf_en"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlAddOrg_trnf_en" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกองค์กร"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender35" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlAddOrg_trnf_en" Width="160" />

                                                    </div>



                                                </div>

                                                <div class="form-group" runat="server" visible="true">

                                                    <asp:Label ID="Label94" runat="server" Text="ฝ่าย : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlAddDep_trnf_en"
                                                            AutoPostBack="true" CssClass="form-control" runat="server"
                                                            OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- เลือกฝ่าย ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlAddDep_trnf_en"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlAddDep_trnf_en" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกฝ่าย"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender36" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlAddDep_trnf_en" Width="160" />

                                                    </div>

                                                </div>

                                                <div class="form-group" runat="server" visible="true">
                                                    <asp:Label ID="Label47" runat="server" Text="แผนก : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlAddSec_trnf_en" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- เลือกแผนก ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlAddSec_trnf_en"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlAddSec_trnf_en" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกฝ่าย"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlAddSec_trnf_en" Width="160" />

                                                    </div>




                                                </div>




                                                <div class="form-group">

                                                    <asp:Label ID="Label48" CssClass="col-sm-2 control-label text_right" runat="server" Text="รหัสหน่วยงาน : " />
                                                    <div class="col-sm-3">

                                                        <asp:DropDownList ID="ddlcostcenter_en"
                                                            runat="server" CssClass="form-control fa-align-left">
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="Rqddlcostcenter_en" runat="server" InitialValue="0"
                                                            ControlToValidate="ddlcostcenter_en" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="กรุณาเลือกรหัสหน่วยงาน" ValidationGroup="SaveAdd" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Rqddlcostcenter_en" Width="180" />

                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label2" class="col-sm-2 control-label" runat="server" Text="สถานที่ : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddllocate_en" ValidationGroup="Save" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                            <asp:ListItem Value="0" Text="--- เลือกสถานที่ ---"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="Requiredddllocate_en"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddllocate_en" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกสถานที่"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2"
                                                            runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Requiredddllocate_en" Width="160" />

                                                    </div>

                                                    <div class="col-md-2">
                                                        <asp:CheckBox ID="check_transfer_flag_en" runat="server" AutoPostBack="true"
                                                            RepeatDirection="Vertical" CssClass="checkbox checkbox-primary"
                                                            Text="โอนย้ายข้าม Plant"
                                                            OnCheckedChanged="checkindexchange" />
                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label13" class="col-sm-2 control-label" runat="server" Text="อาคาร : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlbuilding_en" ValidationGroup="Save" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0">--- เลือกอาคาร ---</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="Requiredddlbuilding_en"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlbuilding_en" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกอาคาร"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10"
                                                            runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Requiredddlbuilding_en" Width="160" />

                                                    </div>



                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label14" class="col-sm-2 control-label" runat="server" Text="ห้อง : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlroom_en" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                            <asp:ListItem Value="0">--- เลือกห้อง ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                    </div>

                                                </div>


                                                <asp:Panel ID="pnl_en_search" runat="server" Visible="true">
                                                    <hr style="border-top: 1px solid #8c8b8b;">

                                                    <h4 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; ค้นหาอุปกรณ์</strong></h4>
                                                    <br />


                                                    <div class="form-group">

                                                        <asp:Label ID="Label96" class="col-sm-2 control-label" runat="server" Text="สถานที่ : " />
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddllocate_en_search" ValidationGroup="Save" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                                <asp:ListItem Value="0" Text="--- เลือกสถานที่ ---"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="Requiredddllocate_en_search" ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="ddllocate_en_search" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกสถานที่"
                                                                InitialValue="0"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender37"
                                                                runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="Requiredddllocate_en_search" Width="160" />

                                                        </div>

                                                        <asp:Label ID="Label97" class="col-sm-3 control-label" runat="server" Text="ประเภทเครื่องจักร : " />
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddltypemachine_en_search" ValidationGroup="Save" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" class="form-control" runat="server">
                                                                <asp:ListItem Value="0" Text="--- เลือกประเภทเครื่องจักร ---"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="Requiredddltypemachine_en_search" ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="ddltypemachine_en_search" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกประเภทเครื่องจักร"
                                                                InitialValue="0"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender38"
                                                                runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="Requiredddltypemachine_en_search" Width="160" />

                                                        </div>

                                                    </div>

                                                    <div class="form-group" runat="server" visible="false">



                                                        <asp:Label ID="Label98" class="col-sm-3 control-label" runat="server" Text="รหัสกลุ่มเครื่องจักร : " />
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddltypecode_en_search" ValidationGroup="Save" class="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                                <asp:ListItem Value="0" Text="--- เลือกรหัสกลุ่มเครื่องจักร ---"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%--<asp:RequiredFieldValidator ID="Requiredddltypecode_en_search" ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="ddltypecode_en_search" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกรหัสกลุ่มเครื่องจักร"
                                                                 InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender39" runat="Server"
                                                                HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="Requiredddltypecode_en_search" Width="160" />--%>
                                                        </div>
                                                    </div>

                                                    <div class="form-group" runat="server" visible="false">
                                                        <asp:Label ID="Label99" class="col-sm-2 control-label" runat="server" Text="รหัสกลุ่ม : " />
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddlgroupmachine_en_search" ValidationGroup="Save" class="form-control" runat="server">
                                                                <asp:ListItem Value="0" Text="--- เลือกรหัสกลุ่ม ---"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%--<asp:RequiredFieldValidator ID="Requiredddlgroupmachine_en_search" ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="ddlgroupmachine_en_search" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกรหัสกลุ่ม"
                                                                 InitialValue="0" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender40"
                                                                runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="Requiredddlgroupmachine_en_search" Width="160" />--%>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label100" class="col-sm-2 control-label" runat="server" Text="อาคาร : " />
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddlbuilding_en_search" ValidationGroup="Save" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">--- เลือกอาคาร ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="Requiredddlbuilding_en_search"
                                                                ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="ddlbuilding_en_search" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกอาคาร"
                                                                InitialValue="0"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender41"
                                                                runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="Requiredddlbuilding_en_search" Width="160" />

                                                        </div>

                                                        <asp:Label ID="Label102" class="col-sm-3 control-label" runat="server" Text="ห้อง : " />
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddlroom_en_search" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">--- เลือกห้อง ---</asp:ListItem>
                                                            </asp:DropDownList>

                                                        </div>

                                                    </div>

                                                    <div class="form-group">

                                                        <asp:Label ID="Label95" class="col-sm-2 control-label" runat="server" Text="Asset Num. : " />
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddlmachine_en_search" ValidationGroup="Save" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0">--- เลือกเครื่องจักร ---</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <asp:RequiredFieldValidator ID="Requiredddlmachine_en_search"
                                                                ValidationGroup="Save" runat="server" Display="None"
                                                                ControlToValidate="ddlmachine_en_search" Font-Size="11"
                                                                ErrorMessage="กรุณาเลือกเครื่องจักร"
                                                                InitialValue="0"
                                                                SetFocusOnError="true" />
                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender42"
                                                                runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                                TargetControlID="Requiredddlmachine_en_search" Width="160" />

                                                        </div>


                                                    </div>

                                                    <div class="form-group">

                                                        <div class="col-md-2 col-md-offset-2">
                                                            <asp:LinkButton ID="tbnAddTransferDevice_en" CssClass="btn btn-warning btn-sm" runat="server"
                                                                CommandName="tbnAddTransferDevice_en" OnCommand="btnCommand"
                                                                ValidationGroup="AddDataGv_en" title="Save">
                                                               <i class="glyphicon glyphicon-plus-sign"></i> ADD</asp:LinkButton>
                                                        </div>

                                                    </div>
                                                </asp:Panel>


                                            </asp:Panel>
                                            <%-- hr --%>

                                            <asp:Panel ID="pnl_hr_qa" runat="server">
                                                <h4 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียดการโอนย้ายอุปกรณ์</strong></h4>
                                                <br />

                                                <div class="form-group">
                                                    <asp:Label ID="Label103" runat="server" Text="องค์กร : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlAddOrg_trnf_hr" Enabled="false" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- องค์กร ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlAddOrg_trnf_hr"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlAddOrg_trnf_hr" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกองค์กร"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender33" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlAddOrg_trnf_hr" Width="160" />

                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label104" runat="server" Text="ฝ่าย : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlAddDep_trnf_hr" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- เลือกฝ่าย ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlAddDep_trnf_hr"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlAddDep_trnf_hr" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกฝ่าย"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender34" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlAddDep_trnf_hr" Width="160" />

                                                    </div>

                                                </div>

                                                <div class="form-group" runat="server" visible="true">
                                                    <asp:Label ID="Label105" runat="server" Text="แผนก : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlAddSec_trnf_hr" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- เลือกแผนก ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlAddSec_trnf_hr"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlAddSec_trnf_hr" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกฝ่าย"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender39" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlAddSec_trnf_hr" Width="160" />

                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label45" CssClass="col-md-2 control-label" runat="server" Text="รหัสหน่วยงาน : " />
                                                    <div class="col-md-3">

                                                        <asp:DropDownList ID="ddlcostcenter_hr"
                                                            runat="server" CssClass="form-control fa-align-left">
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="Rqddlcostcenter_hr" runat="server" InitialValue="0"
                                                            ControlToValidate="ddlcostcenter_hr" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="กรุณาเลือกรหัสหน่วยงาน" ValidationGroup="SaveAdd" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Rqddlcostcenter_hr" Width="180" />

                                                    </div>

                                                </div>

                                                <div class="form-group" runat="server" visible="false">

                                                    <asp:Label ID="Label106" runat="server" Text="ผู้ถือครอง : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">


                                                        <asp:DropDownList ID="ddlAddemp_idx_trnf_hr"
                                                            AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- เลือกผู้ถือครอง ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlAddemp_idx_trnf_hr"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlAddemp_idx_trnf_hr" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกผู้ถือครอง"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender40" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlAddemp_idx_trnf_hr" Width="160" />

                                                    </div>

                                                </div>

                                                <div class="form-group" runat="server" visible="false">
                                                    <asp:Label ID="Label107" CssClass="col-sm-2 control-label text_right" runat="server" Text="สถานที่ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtplace_idx_hr"
                                                            Text='<%# Eval("n_place_idx") %>'
                                                            runat="server" Visible="false">
                                                        </asp:TextBox>

                                                        <asp:TextBox ID="txtplace_trnf_hr"
                                                            Enabled="false"
                                                            Text='<%# Eval("location_name") %>'
                                                            runat="server" CssClass="form-control fa-align-left">
                                                        </asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="Requiredtxtplace_trnf_hr"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="txtplace_trnf_hr" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกสถานที่"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender43" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Requiredtxtplace_trnf_hr" Width="160" />

                                                    </div>
                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label112" class="col-sm-2 control-label" runat="server" Text="สถานที่ : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddllocate_hr" ValidationGroup="Save" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                            <asp:ListItem Value="0" Text="--- เลือกสถานที่ ---"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="Requiredddllocate_hr"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddllocate_hr" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกสถานที่"
                                                            InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender46"
                                                            runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Requiredddllocate_hr" Width="160" />

                                                    </div>
                                                    <div class="col-md-2">
                                                        <asp:CheckBox ID="check_transfer_flag" runat="server" AutoPostBack="true"
                                                            RepeatDirection="Vertical" CssClass="checkbox checkbox-primary"
                                                            Text="โอนย้ายข้าม Plant"
                                                            OnCheckedChanged="checkindexchange" />
                                                    </div>

                                                </div>

                                            </asp:Panel>

                                            <asp:Panel ID="pnl_hr_search" runat="server" Visible="true">

                                                <hr style="border-top: 1px solid #8c8b8b;">
                                                <h4 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; ค้นหาอุปกรณ์</strong></h4>
                                                <br />

                                                <div class="form-group">

                                                    <asp:Label ID="Label110" class="col-sm-2 control-label" runat="server" Text="สถานที่ : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddllocate_hr_search" ValidationGroup="Save" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                            <asp:ListItem Value="0" Text="--- เลือกสถานที่ ---"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="Requiredddllocate_hr_search"
                                                            ValidationGroup="AddDataGv" runat="server" Display="None"
                                                            ControlToValidate="ddllocate_hr_search" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกสถานที่"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender44"
                                                            runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Requiredddllocate_hr_search" Width="160" />

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label49" CssClass="col-md-2 control-label" runat="server" Text="รหัสหน่วยงาน : " />
                                                    <div class="col-md-3">

                                                        <asp:DropDownList ID="ddlcostcenter_hr_search" Enabled="false"
                                                            runat="server" CssClass="form-control fa-align-left">
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="Rqddlcostcenter_hr_search" runat="server" InitialValue="0"
                                                            ControlToValidate="ddlcostcenter_hr_search" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="กรุณาเลือกรหัสหน่วยงาน" ValidationGroup="AddDataGv" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Rqddlcostcenter_hr_search" Width="180" />

                                                    </div>

                                                </div>

                                                <div class="form-group" runat="server" visible="false">

                                                    <asp:Label ID="Label108" runat="server" Text="ประเภทอุปกรณ์" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlSearch_devices_trnf_hr"
                                                            AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlSearch_devices_trnf_hr"
                                                            ValidationGroup="AddDataGv" runat="server" Display="None"
                                                            ControlToValidate="ddlSearch_devices_trnf_hr" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกประเภทอุปกรณ์"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />



                                                    </div>

                                                </div>



                                                <div class="form-group">

                                                    <asp:Label ID="Label109" runat="server" Text="Asset Num. : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlasset_no_trnf_hr" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="Requiredddlasset_no_trnf_hr"
                                                            ValidationGroup="AddDataGv" runat="server" Display="None"
                                                            ControlToValidate="ddlasset_no_trnf_hr" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือก Asset Num."
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />

                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-2 col-md-offset-2">
                                                        <asp:LinkButton ID="tbnAddTransferDevice_hr" CssClass="btn btn-warning btn-sm" runat="server"
                                                            CommandName="tbnAddTransferDevice_hr" OnCommand="btnCommand"
                                                            ValidationGroup="AddDataGv" title="Save">
                                                               <i class="glyphicon glyphicon-plus-sign"></i> ADD</asp:LinkButton>
                                                    </div>

                                                </div>
                                            </asp:Panel>

                                            <%-- qa --%>
                                            <asp:Panel ID="pnl_qa" runat="server" Visible="false">


                                                <h4 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียดการโอนย้ายอุปกรณ์ QA</strong></h4>
                                                <br />



                                                <div class="form-group">
                                                    <asp:Label ID="Label1" runat="server" Text="องค์กร : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlAddOrg_trnf_qa" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- องค์กร ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlAddOrg_trnf_qa"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlAddOrg_trnf_qa" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกองค์กร"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlAddOrg_trnf_qa" Width="160" />

                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label5" runat="server" Text="ฝ่าย : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlAddDep_trnf_qa" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- เลือกฝ่าย ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlAddDep_trnf_qa"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlAddDep_trnf_qa" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกฝ่าย"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlAddDep_trnf_qa" Width="160" />

                                                    </div>

                                                </div>

                                                <div class="form-group" runat="server" visible="false">
                                                    <asp:Label ID="Label6" runat="server" Text="แผนก : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlAddSec_trnf_qa" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- เลือกแผนก ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlAddSec_trnf_qa"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlAddSec_trnf_qa" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกฝ่าย"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlAddSec_trnf_qa" Width="160" />

                                                    </div>

                                                </div>



                                                <div class="form-group" runat="server" visible="false">
                                                    <asp:Label ID="Label21" CssClass="col-sm-2 control-label text_right" runat="server" Text="สถานที่ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtplace_idx_qa"
                                                            Text='<%# Eval("n_place_idx") %>'
                                                            runat="server" Visible="false">
                                                        </asp:TextBox>

                                                        <asp:TextBox ID="txtplace_trnf_qa"
                                                            Enabled="false"
                                                            Text='<%# Eval("location_name") %>'
                                                            runat="server" CssClass="form-control fa-align-left">
                                                        </asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="Requiredtxtplace_trnf_qa"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="txtplace_trnf_qa" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกสถานที่"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Requiredtxtplace_trnf_qa" Width="160" />

                                                    </div>
                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label24" class="col-sm-2 control-label" runat="server" Text="สถานที่ : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddllocate_qa" ValidationGroup="Save" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                            <asp:ListItem Value="0" Text="--- เลือกสถานที่ ---"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="Requiredddllocate_qa"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddllocate_qa" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกสถานที่"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6"
                                                            runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Requiredddllocate_qa" Width="160" />

                                                    </div>

                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="pnl_qa_search" runat="server" Visible="true">

                                                <hr style="border-top: 1px solid #8c8b8b;">
                                                <h4 class="panel-title"><i class="glyphicon glyphicon-search"></i><strong>&nbsp; ค้นหาอุปกรณ์</strong></h4>
                                                <br />

                                                <div class="form-group">

                                                    <asp:Label ID="Label25" class="col-sm-2 control-label" runat="server" Text="สถานที่ : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddllocate_qa_search" ValidationGroup="Save" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                            <asp:ListItem Value="0" Text="--- เลือกสถานที่ ---"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="Requiredddllocate_qa_search"
                                                            ValidationGroup="AddDataGv" runat="server" Display="None"
                                                            ControlToValidate="ddllocate_qa_search" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกสถานที่"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7"
                                                            runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Requiredddllocate_qa_search" Width="160" />

                                                    </div>
                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label26" runat="server" Text="ประเภทอุปกรณ์" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlSearch_devices_trnf_qa"
                                                            AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlSearch_devices_trnf_qa"
                                                            ValidationGroup="AddDataGv" runat="server" Display="None"
                                                            ControlToValidate="ddlSearch_devices_trnf_qa" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกประเภทอุปกรณ์"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />



                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label27" runat="server" Text="Asset Num. : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlasset_no_trnf_qa" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="Requiredddlasset_no_trnf_qa"
                                                            ValidationGroup="AddDataGv" runat="server" Display="None"
                                                            ControlToValidate="ddlasset_no_trnf_hr" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือก Asset Num."
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />



                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-2 col-md-offset-2">
                                                        <asp:LinkButton ID="tbnAddTransferDevice_qa" CssClass="btn btn-warning btn-sm" runat="server"
                                                            CommandName="tbnAddTransferDevice_qa" OnCommand="btnCommand"
                                                            ValidationGroup="AddDataGv" title="Save">
                                                               <i class="glyphicon glyphicon-plus-sign"></i> ADD</asp:LinkButton>
                                                    </div>

                                                </div>
                                            </asp:Panel>



                                            <%-- other --%>
                                            <asp:Panel ID="pnl_other" runat="server" Visible="false">
                                                <h4 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียดการโอนย้ายอุปกรณ์ Other</strong></h4>
                                                <br />



                                                <div class="form-group">
                                                    <asp:Label ID="Label28" runat="server" Text="องค์กร : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlAddOrg_trnf_other" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- องค์กร ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlAddOrg_trnf_other"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlAddOrg_trnf_other" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกองค์กร"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlAddOrg_trnf_other" Width="160" />

                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label29" runat="server" Text="ฝ่าย : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlAddDep_trnf_other" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- เลือกฝ่าย ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlAddDep_trnf_other"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlAddDep_trnf_other" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกฝ่าย"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlAddDep_trnf_other" Width="160" />

                                                    </div>

                                                </div>

                                                <div class="form-group" runat="server" visible="false">
                                                    <asp:Label ID="Label33" runat="server" Text="แผนก : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlAddSec_trnf_other" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">--- เลือกแผนก ---</asp:ListItem>
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="RequiredddlAddSec_trnf_other"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddlAddSec_trnf_other" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกฝ่าย"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="RequiredddlAddSec_trnf_other" Width="160" />

                                                    </div>

                                                </div>



                                                <div class="form-group" runat="server" visible="false">
                                                    <asp:Label ID="Label37" CssClass="col-sm-2 control-label text_right" runat="server" Text="สถานที่ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtplace_idx_other"
                                                            Text='<%# Eval("n_place_idx") %>'
                                                            runat="server" Visible="false">
                                                        </asp:TextBox>

                                                        <asp:TextBox ID="txtplace_trnf_other"
                                                            Enabled="false"
                                                            Text='<%# Eval("location_name") %>'
                                                            runat="server" CssClass="form-control fa-align-left">
                                                        </asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="Requiredtxtplace_trnf_other"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="txtplace_trnf_other" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกสถานที่"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Requiredtxtplace_trnf_other" Width="160" />

                                                    </div>
                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label38" class="col-sm-2 control-label" runat="server" Text="สถานที่ : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddllocate_other" ValidationGroup="Save" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                            <asp:ListItem Value="0" Text="--- เลือกสถานที่ ---"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="Requiredddllocate_other"
                                                            ValidationGroup="SaveAdd" runat="server" Display="None"
                                                            ControlToValidate="ddllocate_other" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกสถานที่"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14"
                                                            runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Requiredddllocate_other" Width="160" />

                                                    </div>

                                                </div>

                                            </asp:Panel>


                                            <asp:Panel ID="pnl_other_search" runat="server" Visible="true">

                                                <hr style="border-top: 1px solid #8c8b8b;">
                                                <h4 class="panel-title"><i class="glyphicon glyphicon-pencil"></i><strong>&nbsp; อุปกรณ์</strong></h4>
                                                <br />


                                                <div class="form-group">

                                                    <asp:Label ID="Label44" class="col-sm-2 control-label" runat="server" Text="สถานที่ : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddllocate_other_search" ValidationGroup="Save" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server">
                                                            <asp:ListItem Value="0" Text="--- เลือกสถานที่ ---"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="Requiredddllocate_other_search"
                                                            ValidationGroup="AddDataGv" runat="server" Display="None"
                                                            ControlToValidate="ddllocate_other_search" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกสถานที่"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18"
                                                            runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Requiredddllocate_other_search" Width="160" />

                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label50" CssClass="col-md-2 control-label" runat="server" Text="รหัสหน่วยงาน : " />
                                                    <div class="col-md-3">

                                                        <asp:DropDownList ID="ddlcostcenter_other_search" Enabled="false"
                                                            runat="server" CssClass="form-control fa-align-left">
                                                        </asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="Rqddlcostcenter_other_search" runat="server" InitialValue="0"
                                                            ControlToValidate="ddlcostcenter_other_search" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="กรุณาเลือกรหัสหน่วยงาน" ValidationGroup="AddDataGv" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender25" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Rqddlcostcenter_other_search" Width="180" />

                                                    </div>

                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label39" class="col-sm-2 control-label" runat="server" Text="Asset Num. : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtasset_no" ValidationGroup="Save" MaxLength="50" CssClass="form-control" runat="server">
                                                        </asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="Requiredtxtasset_no"
                                                            ValidationGroup="AddDataGv" runat="server" Display="None"
                                                            ControlToValidate="txtasset_no" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกAsset Num."
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15"
                                                            runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Requiredtxtasset_no" Width="160" />

                                                    </div>
                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label46" class="col-sm-2 control-label" runat="server" Text=" " />
                                                    <div class="col-sm-3">
                                                        <asp:CheckBox ID="check_subno_other" runat="server" AutoPostBack="true"
                                                            RepeatDirection="Vertical" CssClass="checkbox checkbox-primary"
                                                            Text="Sub. No." />
                                                    </div>
                                                </div>

                                                <div class="form-group">

                                                    <asp:Label ID="Label40" runat="server" Text="Description : " CssClass="col-sm-2 control-label text_right"></asp:Label>
                                                    <div class="col-sm-10">

                                                        <asp:TextBox ID="txtdesc_name" Enabled="true"
                                                            TextMode="multiline"
                                                            Rows="3" CssClass="form-control" runat="server"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="Requiredtxtdesc_name"
                                                            ValidationGroup="AddDataGv" runat="server" Display="None"
                                                            ControlToValidate="txtdesc_name" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกDescription"
                                                            SetFocusOnError="true" />

                                                    </div>

                                                </div>

                                                <div class="form-group" runat="server" visible="true">
                                                    <asp:Label ID="Label41" CssClass="col-md-2 control-label" runat="server" Text="Acquis. Val. : " />
                                                    <div class="col-md-3">
                                                        <asp:TextBox ID="txtacquis_val"
                                                            CssClass="form-control"
                                                            runat="server"
                                                            TextMode="Number"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="Requiredtxtacquis_val"
                                                            ValidationGroup="AddDataGv" runat="server" Display="None"
                                                            ControlToValidate="txtacquis_val" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกAcquis. Val." />
                                                        <asp:RegularExpressionValidator ID="RegularExpressiontxtacquis_val" runat="server" ValidationGroup="SaveAdd" Display="None"
                                                            ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                            ControlToValidate="txtacquis_val"
                                                            ValidationExpression="^[0-9]+(\.[0-9]{1,2})?$" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxtacquis_val" Width="160" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressiontxtacquis_val" Width="160" />
                                                    </div>
                                                </div>

                                                <div class="form-group"  runat="server" visible="false">

                                                    <asp:Label ID="Label42" class="col-md-2 control-label" runat="server" Text="Ref. IT Num. : " />
                                                    <div class="col-md-3">
                                                        <asp:TextBox ID="txtref_itnum" ValidationGroup="Save" MaxLength="50" CssClass="form-control" runat="server">
                                                        </asp:TextBox>

                                                    </div>

                                                </div>

                                                <div class="form-group"  runat="server" visible="false">

                                                    <asp:Label ID="Label43" class="col-md-2 control-label" runat="server" Text="IO. Num. : " />
                                                    <div class="col-md-3">
                                                        <asp:TextBox ID="txtionum" ValidationGroup="Save" MaxLength="50" CssClass="form-control" runat="server">
                                                        </asp:TextBox>

                                                    </div>

                                                </div>

                                                <div class="form-group">
                                                    <div class="col-md-2 col-md-offset-2">
                                                        <asp:LinkButton ID="tbnAddTransferDevice_other" CssClass="btn btn-warning btn-sm" runat="server"
                                                            CommandName="tbnAddTransferDevice_other" OnCommand="btnCommand"
                                                            ValidationGroup="AddDataGv" title="Save">
                                                               <i class="glyphicon glyphicon-plus-sign"></i> ADD</asp:LinkButton>
                                                    </div>

                                                </div>
                                            </asp:Panel>


                                        </div>

                                    </div>

                                    <asp:GridView ID="GvTransferDevice"
                                        runat="server"
                                        HeaderStyle-CssClass="info"
                                        CssClass="table table-striped table-responsive table-bordered word-wrap"
                                        GridLines="None"
                                        OnRowCommand="onRowCommand"
                                        OnRowDataBound="gvRowDataBound"
                                        AutoGenerateColumns="false">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>

                                        <Columns>

                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%" HeaderText="Item" ItemStyle-HorizontalAlign="center">
                                                <ItemTemplate>

                                                    <%# (Container.DataItemIndex +1) %>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%" HeaderText="Asset Num." ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_asset_no" runat="server" CssClass="col-sm-12" Text='<%# Eval("asset_no") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%" HeaderText="Sub.No." ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_subno_flag" Visible="false" runat="server" CssClass="col-sm-12" Text='<%# Eval("subno_flag") %>'></asp:Label>

                                                    <asp:Label ID="lb_status_subno_flag" runat="server" Text='<%# getStatus_subno((string)Eval("subno_flag")) %>' />

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="40%" HeaderText="Description" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_tdidx_name" Visible="false" runat="server" CssClass="col-sm-12" Text='<%# Eval("tdidx_name") %>'></asp:Label>
                                                    <asp:Label ID="lb_desc_name" runat="server" CssClass="col-sm-12" Text='<%# Eval("desc_name") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="5%" HeaderText="Qty." ItemStyle-HorizontalAlign="Right">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_qty" runat="server" CssClass="col-sm-12" Text='<%# getformatfloat(((string)Eval("qty")).ToString(),0) %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="3%" HeaderText="Acquis. Val." ItemStyle-HorizontalAlign="Right">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_acquis_val" runat="server" CssClass="col-sm-12" Text='<%# getformatfloat(((string)Eval("acquis_val")).ToString(),2) %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="12%" HeaderText="Book Val." ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_book_val" runat="server" CssClass="pull-right" Text='<%# getformatfloat(((string)Eval("book_val")).ToString(),2) %>'></asp:Label>

                                                    <asp:TextBox ID="txt_book_val" runat="server" CssClass="form-control"
                                                        TextMode="Number"
                                                        Text='<%# Eval("book_val") %>'></asp:TextBox>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="12%" HeaderText="Price"
                                                ItemStyle-HorizontalAlign="Right" Visible="false">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_price" runat="server" CssClass="pull-right" Text='<%# getformatfloat(((string)Eval("price")).ToString(),2) %>'></asp:Label>

                                                    <asp:TextBox ID="txt_price" runat="server" CssClass="form-control"
                                                        TextMode="Number"
                                                        Text='<%# Eval("price") %>'></asp:TextBox>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="8%" HeaderText="Ref. IT Num." ItemStyle-HorizontalAlign="Left" Visible="false">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_ref_itnum" runat="server" CssClass="col-sm-12" Text='<%# Eval("ref_itnum") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="8%" HeaderText="IO. Num." ItemStyle-HorizontalAlign="Left" Visible="false">
                                                <ItemTemplate>

                                                    <asp:Label ID="lb_ionum" runat="server" CssClass="col-sm-12" Text='<%# Eval("ionum") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="จัดการ"
                                                HeaderStyle-Font-Size="Small"
                                                HeaderStyle-CssClass="text-center"
                                                ItemStyle-CssClass="text-center">
                                                <ItemTemplate>

                                                    <asp:UpdatePanel ID="Updatepnl_btnDeleteList_trnf" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:LinkButton ID="btnDeleteList_trnf" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="btnDeleteList_trnf"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="btnDeleteList_trnf" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>

                                    </asp:GridView>
                                    <div class="form-horizontal" role="form">

                                        <%-- flow_item = 1 --%>


                                        <asp:Panel ID="pnl_flow_item_1_remark" runat="server">
                                            <div class="form-group">
                                                <asp:Label ID="Labedle1" class="col-sm-2 control-label text_right" runat="server" Text="หมายเหตุ : " />
                                                <div class="col-md-10">
                                                    <asp:TextBox ID="txtremark" Enabled="true"
                                                        TextMode="multiline"
                                                        Rows="3" CssClass="form-control" runat="server"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="Requiredtxtremark"
                                                        ValidationGroup="SaveAdd" runat="server" Display="None"
                                                        ControlToValidate="txtremark" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกหมายเหตุ"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19"
                                                        runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                        TargetControlID="Requiredtxtremark" Width="160" />

                                                </div>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel ID="pnl_flow_item_2_uploadfile" runat="server">
                                            <asp:UpdatePanel ID="Updatepnl_uploadfile_trnsf" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <div class="form-group">

                                                        <asp:Label ID="Label101" class="col-md-2 control-label" runat="server" Text="แนบเอกสาร : " />
                                                        <div class="col-md-7">
                                                            <asp:FileUpload ID="uploadfile_trnsf" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small"
                                                                ClientIDMode="Static" runat="server"
                                                                CssClass="btn btn-primary btn-sm multi with-preview"
                                                                accept="jpeg|gif|jpg|png|pdf|xls|xlsx|doc|docx|ppt|pptx" />
                                                            <p class="help-block">
                                                                <font color="red">
                                                            **แนบเอกสารเพิ่มเติม เฉพาะนามสกุล .jpeg,.gif,.jpg,.png,.pdf,.xls,.xlsx,.doc,.docx,.ppt,.pptx

                                                        </font>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnAdddata" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </asp:Panel>



                                        <asp:Panel ID="pnl_flow_item_2_listfile" runat="server">
                                            <div class="form-group">

                                                <div class="col-md-12">

                                                    <asp:GridView ID="gvFile_transfer" Visible="true" runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover "
                                                        OnRowDataBound="gvRowDataBound">

                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ไฟล์เอกสาร">
                                                                <ItemTemplate>
                                                                    <div class="col-lg-10">
                                                                        <asp:Label ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                                    </div>
                                                                    <div class="col-lg-2">
                                                                        <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-primary btn-sm" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>
                                                                        <asp:Label ID="lb_Download" runat="server" Visible="false"></asp:Label>
                                                                        <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />


                                                                        <asp:LinkButton ID="bnDeletefile" runat="server" Text="Delete" CssClass="btn btn-danger btn-sm"
                                                                            OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')"
                                                                            CommandName="bnDeletefile"
                                                                            OnCommand="btnCommand"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                                    </div>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </asp:Panel>


                                        <asp:Panel ID="pnl_flow_item_1" runat="server">
                                            <div class="col-md-4 col-md-offset-8">

                                                <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="btnSaveAdd_trnf" OnCommand="btnCommand" CommandArgument="0"
                                                    ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i>&nbsp;บันทึก</asp:LinkButton>

                                                <asp:LinkButton ID="btnAddReject" CssClass="btn btn-danger  btn-sm" runat="server" CommandName="btnSaveAddReject_trnf" OnCommand="btnCommand" CommandArgument="8"
                                                    title="Reject" OnClientClick="return confirm('คุณต้องการยกเลิกโอนย้ายอุปกรณ์รายการนี้ใช่หรือไม่ ?')"><i class="fa fa-trash"></i>&nbsp;ยกเลิกโอนย้ายอุปกรณ์</asp:LinkButton>

                                                <asp:LinkButton ID="btnAddCancel" CssClass="btn btn-default  btn-sm" runat="server" CommandName="btnAddCancel_trnf" OnCommand="btnCommand"
                                                    title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i>&nbsp;ออก</asp:LinkButton>
                                            </div>

                                        </asp:Panel>

                                        <asp:Panel ID="pnl_flow_item_2" runat="server">
                                            <asp:Panel ID="pnl_flow_item_2_h" runat="server">
                                                <div class="form-group">
                                                    <asp:Label ID="Label20" class="col-md-2 control-label" runat="server" Text="สถานะดำเนินการ : " />
                                                    <div class="col-md-3">
                                                        <asp:DropDownList ID="ddlapprove_flowi2" CssClass="form-control" runat="server">
                                                            <asp:ListItem Value="0">กรุณาเลือกสถานะดำเนินการ...</asp:ListItem>
                                                            <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                                            <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                                        </asp:DropDownList>


                                                        <asp:RequiredFieldValidator ID="Requiredddlapprove_flowi2"
                                                            ValidationGroup="btnAdddata_flowi2" runat="server" Display="None"
                                                            ControlToValidate="ddlapprove_flowi2" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกสถานะดำเนินการ"
                                                            InitialValue="0"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortbVisitDate" runat="Server"
                                                            HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Requiredddlapprove_flowi2" Width="160" />


                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label89" class="col-sm-2 control-label text_right" runat="server" Text="หมายเหตุ : " />
                                                    <div class="col-md-10">
                                                        <asp:TextBox ID="txtremark_flowi2" Enabled="true"
                                                            TextMode="multiline"
                                                            Rows="3" CssClass="form-control" runat="server"></asp:TextBox>


                                                        <asp:RequiredFieldValidator ID="Requiredtxtremark_flowi2"
                                                            ValidationGroup="btnAdddata_flowi2" runat="server" Display="None"
                                                            ControlToValidate="txtremark_flowi2" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกหมายเหตุ"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20"
                                                            runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                            TargetControlID="Requiredtxtremark_flowi2" Width="160" />


                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <div class="col-md-2 col-md-offset-10">
                                                <asp:LinkButton ID="btnAdddata_flowi2" CssClass="btn btn-success btn-sm" runat="server" CommandName="btnSaveAdd_trnf_app" OnCommand="btnCommand" CommandArgument="0"
                                                    ValidationGroup="btnAdddata_flowi2" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i>&nbsp;บันทึก</asp:LinkButton>

                                                <asp:LinkButton ID="btnAddCancel_flowi2" CssClass="btn btn-default  btn-sm" runat="server" CommandName="btnAddCancel_trnf_app" OnCommand="btnCommand"
                                                    title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i>&nbsp;ออก</asp:LinkButton>
                                            </div>

                                        </asp:Panel>

                                    </div>

                                </div>
                            </div>
                        </div>

                        <%-- History tranfer--%>
                        <asp:Panel ID="pnl_history" runat="server" Visible="false">


                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <i class="fa fa-list-ul" aria-hidden="true"></i>&nbsp;
                                            <b>ประวัติรายการ</b>
                                </div>
                                <div class="panel-body">
                                    <asp:Repeater ID="rpt_history" runat="server">
                                        <HeaderTemplate>
                                            <div class="row">
                                                <label class="col-sm-2 control-label">วันที่ดำเนินการ</label>
                                                <label class="col-sm-3 control-label">ผู้ดำเนินการ</label>
                                                <label class="col-sm-2 control-label">ดำเนินการ</label>
                                                <label class="col-sm-2 control-label">ผลการดำเนินการ</label>
                                                <label class="col-sm-3 control-label">หมายเหตุ</label>
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <span><%# Eval("zdate") %></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <span><%# Eval("emp_name_th")+"("+Eval("actor_name")+")" %></span>
                                                </div>
                                                <div class="col-sm-2">
                                                    <span><%# Eval("node_name") %></span>
                                                </div>
                                                <div class="col-sm-2">
                                                    <span><%# Eval("status_name") %></span>
                                                </div>
                                                <div class="col-sm-3">
                                                    <span><%# Eval("app_remark") %></span>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>

                        </asp:Panel>


                    </EditItemTemplate>
                </asp:FormView>

                <%-- holder-devices --%>
                <asp:Panel ID="pnl_holder_devices" runat="server" Visible="false">

                    <asp:FormView ID="FvDetailUser_holder_devices" runat="server" Width="100%" DefaultMode="ReadOnly">
                        <ItemTemplate>
                            <div class="form-horizontal" role="form">


                                <div class="form-group">
                                    <div class="col-md-12">
                                        <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("unidx") %>' />
                                        <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("acidx") %>' />
                                        <asp:Label ID="lblholderidx" Visible="false" CssClass="control-label" runat="server" Text='<%# Eval("HolderIDX") %>' />
                                        <asp:Label ID="lblDeviceIDX" Visible="false" CssClass="control-label" runat="server" Text='<%# Eval("DeviceIDX") %>' />

                                        <asp:Label ID="Label23" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสอุปกรณ์ :" />
                                        <div class="col-xs-3">
                                            <asp:Label ID="Label22" CssClass="control-labelnotop" runat="server" Text='<%# Eval("DeviceCode") %>' />
                                            <asp:Label ID="lblm0_tdidx" Visible="false" CssClass="control-labelnotop" runat="server" Text='<%# Eval("m0_tdidx") %>' />


                                        </div>
                                        <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ปรเะภทอุปกรณ์ :" />
                                        <div class="col-xs-4">
                                            <asp:Label ID="Label17" CssClass="control-labelnotop" runat="server" Text='<%# Eval("EqtName") %>' />
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-12">
                                        <asp:Label ID="Label30" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัท :" />
                                        <div class="col-xs-3">
                                            <asp:Label ID="Label34" CssClass=" control-labelnotop" runat="server" Text='<%# Eval("Organization") %>' />
                                            <asp:Label ID="lblorg" CssClass=" control-labelnotop" Visible="true" runat="server" Text='<%# Eval("OrgIDX") %>' />
                                        </div>
                                        <asp:Label ID="Label35" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ฝ่าย :" />
                                        <div class="col-xs-4">
                                            <asp:Label ID="Label36" CssClass=" control-labelnotop" runat="server" Text='<%# Eval("Dept") %>' />
                                            <asp:Label ID="lblrdept" CssClass=" control-labelnotop" Visible="true" runat="server" Text='<%# Eval("RDeptIDX") %>' />
                                            <asp:Label ID="lblNOrgIDX" CssClass=" control-labelnotop" Visible="true" runat="server" Text='<%# Eval("NOrgIDX") %>' />
                                            <asp:Label ID="lblNRDeptIDX" CssClass=" control-labelnotop" Visible="true" runat="server" Text='<%# Eval("NRDeptIDX") %>' />
                                            <asp:Label ID="lblNRSecIDX" CssClass=" control-labelnotop" Visible="true" runat="server" Text='<%# Eval("NRSecIDX") %>' />
                                            <asp:Label ID="lblNHDEmpIDX" CssClass=" control-labelnotop" Visible="true" runat="server" Text='<%# Eval("NHDEmpIDX") %>' />
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-12">

                                        <asp:Label ID="Label15" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Asset Code :" />
                                        <div class="col-xs-3">
                                            <asp:Label ID="lbltel" CssClass="control-labelnotop " runat="server" Text='<%# Eval("AccessCode") %>' />
                                        </div>
                                        <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="PO CODE :" />
                                        <div class="col-xs-4">
                                            <asp:Label ID="Label18" CssClass=" control-labelnotop " runat="server" Text='<%# Eval("POCode") %>' />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">

                                        <asp:Label ID="Label14" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="Serial Number :" />
                                        <div class="col-xs-3">
                                            <asp:Label ID="lbledit_sysidx" CssClass="control-labelnotop" runat="server" Text='<%# Eval("SerialNumber") %>' />
                                        </div>

                                        <asp:Label ID="Label77" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัทประกัน :" />
                                        <div class="col-xs-4">
                                            <asp:Label ID="txtedittopic" CssClass="  control-labelnotop" runat="server" Text='<%# Eval("InsuranceName") %>' />

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">

                                        <asp:Label ID="Label67" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่ซื้อ :" />
                                        <div class="col-sm-3">
                                            <asp:Label ID="lbledit_mtidx" CssClass=" control-labelnotop" runat="server" Text='<%# Eval("PurchaseDate") %>' />
                                        </div>
                                        <asp:Label ID="Label16" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่หมดประกัน :" />
                                        <div class="col-xs-4">
                                            <asp:Label ID="Label19" CssClass="control-labelnotop" runat="server" Text='<%# Eval("InsuranceExp") %>' />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" runat="server" id="divshowapprover" visible="true">
                                    <div class="col-md-12">

                                        <asp:Label ID="Label8" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ผู้อนุมัติลำดับที่ 1 :" />
                                        <div class="col-sm-3">
                                            <asp:Label ID="lblstatus" Visible="false" CssClass=" control-labelnotop" runat="server" Text='<%# Eval("HolderStatus") %>' />
                                            <asp:Label ID="Label11" CssClass=" control-labelnotop" runat="server" Text='<%# Eval("approver1") %>' />
                                        </div>
                                        <asp:Label ID="Label31" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ผู้อนุมัติลำดับที่ 2 :" />
                                        <div class="col-xs-4">
                                            <asp:Label ID="Label32" CssClass="control-labelnotop" runat="server" Text='<%# Eval("approver2") %>' />
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </ItemTemplate>

                    </asp:FormView>


                    <%--<div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-gift"></i><strong>&nbsp; รายละเอียดซอฟแวร์</strong></h3>
                        </div>
                        <div class="panel-body">

                    --%>

                    <%--  <div class="form-group">
                                <div class="checkbox checkbox-primary">
                                    <asp:Label ID="Label10" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ซอฟแวร์ : " />
                                    <div class="col-sm-4">--%>
                    <asp:CheckBoxList ID="chcksoft" Enabled="true"
                        runat="server"
                        CellPadding="10"
                        CellSpacing="10"
                        RepeatColumns="2"
                        RepeatDirection="Vertical"
                        RepeatLayout="Table"
                        TextAlign="Right"
                        Width="100%">
                    </asp:CheckBoxList>
                    <%-- </div>
                                </div>--%>


                    <%--</div>--%>

                    <%--</div>
                    </div>--%>
                </asp:Panel>

            </div>


        </asp:View>

        <%-- end โอนย้ายอุปกรณ์ --%>
    </asp:MultiView>



    <script type="text/javascript">
        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            encoding: "xml",
            theme: "modern",
            menubar: false,
            resize: false,
            statusbar: false,
            plugins: ["advlist autolink lists charmap preview hr anchor",
                "pagebreak code nonbreaking table contextmenu directionality paste"],
            toolbar1: "styleselect | bold italic underline | undo redo",
            toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            setup: function (editor) {
                editor.on('change', function () { tinymce.triggerSave(); });
            }
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            tinymce.remove(".tinymce");

            tinymce.init({
                mode: "specific_textareas",
                editor_selector: "tinymce",
                encoding: "xml",
                theme: "modern",
                menubar: false,
                resize: false,
                statusbar: false,
                plugins: ["advlist autolink lists charmap preview hr anchor",
                    "pagebreak code nonbreaking table contextmenu directionality paste"],
                toolbar1: "styleselect | bold italic underline | undo redo",
                toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
                setup: function (editor) {
                    editor.on('change', function () { tinymce.triggerSave(); });
                }
            });

            $(".multi").MultiFile();
        })
    </script>

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        })
    </script>


    <script>
        $(function () {

            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepickerexpire').datetimepicker({
                    format: 'DD/MM/YYYY',
                    minDate: new Date()

                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            $('.from-date-datepickerexpire').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: new Date()

            });
        })
    </script>

    <script type="text/javascript">

        function SetTarget() {

            document.forms[0].target = "_blank";

        }
        function shwwindow(myurl) {
            window.open(myurl, '_blank');

        }

    </script>

    <style type="text/css">
        .checkbox {
            padding-left: 20px;
        }

            .checkbox label {
                display: inline-block;
                vertical-align: middle;
                position: relative;
                padding-left: 5px;
                font-weight: bold;
            }

                .checkbox label::before {
                    content: "";
                    display: inline-block;
                    position: absolute;
                    width: 17px;
                    height: 17px;
                    left: 0;
                    margin-left: -20px;
                    border: 1px solid #cccccc;
                    border-radius: 3px;
                    background-color: #fff;
                    -webkit-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
                    -o-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
                    transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
                }

                .checkbox label::after {
                    display: inline-block;
                    position: absolute;
                    width: 16px;
                    height: 16px;
                    left: 0;
                    top: 0;
                    margin-left: -20px;
                    padding-left: 3px;
                    padding-top: 1px;
                    font-size: 11px;
                    color: #555555;
                }

            .checkbox input[type="checkbox"] {
                opacity: 0;
                z-index: 1;
            }

                .checkbox input[type="checkbox"]:checked + label::after {
                    font-family: "FontAwesome";
                    content: "\f00c";
                }

        .checkbox-primary input[type="checkbox"]:checked + label::before {
            background-color: #337ab7;
            border-color: #337ab7;
        }

        .checkbox-primary input[type="checkbox"]:checked + label::after {
            color: #fff;
        }
    </style>

    <style type="text/css">
        .checkbox_nolabel {
            /*padding-left: 20px;*/
        }

            .checkbox_nolabel label {
                display: inline-block;
                vertical-align: middle;
                position: center;
                padding-left: 3px;
                padding-top: 5px;
                font-weight: bold;
            }

                .checkbox_nolabel label::before {
                    content: "";
                    display: inline-block;
                    position: center;
                    width: 17px;
                    height: 17px;
                    left: 0;
                    margin-left: -25px;
                    border: 1px solid #cccccc;
                    border-radius: 3px;
                    background-color: #fff;
                    -webkit-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
                    -o-transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
                    transition: border 0.15s ease-in-out, color 0.15s ease-in-out;
                }

                .checkbox_nolabel label::after {
                    display: inline-block;
                    position: center;
                    width: 16px;
                    height: 16px;
                    left: 0;
                    top: 0;
                    margin-left: -25px;
                    padding-left: 3px;
                    padding-top: 1px;
                    font-size: 11px;
                    color: #555555;
                }

            .checkbox_nolabel input[type="checkbox"] {
                opacity: 0;
                z-index: 1;
            }

                .checkbox_nolabel input[type="checkbox"]:checked + label::after {
                    font-family: "FontAwesome";
                    content: "\f00c";
                }

        .checkbox_nolabel-primary input[type="checkbox"]:checked + label::before {
            background-color: #337ab7;
            border-color: #337ab7;
        }

        .checkbox_nolabel-primary input[type="checkbox"]:checked + label::after {
            color: #fff;
        }
    </style>


    <script src='<%=ResolveUrl("~/Scripts/jquery.MultiFileNew.js")%>'></script>
    <script type="text/javascript">
        $(".multi").MultiFile();

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {

            $(".multi").MultiFile();
        })
    </script>


</asp:Content>

