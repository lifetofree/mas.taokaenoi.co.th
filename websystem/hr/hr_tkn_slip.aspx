<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_tkn_slip.aspx.cs" Inherits="websystem_hr_hr_tkn_slip" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbList" runat="server" CommandName="navList" OnCommand="navCommand">
                                ส่วนของพนักงาน</asp:LinkButton>
                        </li>
                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbImport" runat="server" CommandName="navImport" OnCommand="navCommand">
                                นำเข้าข้อมูล</asp:LinkButton>
                        </li>
                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbReport" runat="server" CommandName="navReport" OnCommand="navCommand">
                                ส่วนของ HR</asp:LinkButton>
                        </li>
                    </ul>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->
            </div>
        </nav>
    </div>
    <!--tab menu-->
    <div class="col-md-12">
        <asp:LinkButton ID="lbRelease" runat="server" CssClass="btn btn-warning" OnCommand="btnCommand"
            CommandName="cmdRelease">
            <i class="fas fa-sign-out-alt"></i>&nbsp;Release</asp:LinkButton>
    </div>
    <asp:MultiView ID="mvSystem" runat="server">
        <asp:View ID="viewAuth" runat="server">
            <div class="row vertical">
                <div class="col-md-6 col-md-offset-3">
                    <div class="form-horizontal" role="form">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <asp:TextBox ID="tbPassword" runat="server" CssClass="form-control"
                                        placeholder="2nd authentication" TextMode="Password"></asp:TextBox>
                                </div>
                                <label class="col-md-2"></label>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <asp:LinkButton ID="lbAuth" runat="server" CssClass="btn btn-success"
                                        OnCommand="btnCommand" CommandName="cmdAuth">
                                        <i class="fas fa-check-double"></i>&nbsp;authentication</asp:LinkButton>
                                </div>
                                <label class="col-md-2"></label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>
        <asp:View ID="viewList" runat="server">
            <asp:GridView ID="gvList" runat="server" AutoGenerateColumns="False"
                CssClass="table table-striped table-bordered table-responsive" DataKeyNames="u0_idx">
                <HeaderStyle CssClass="info" Font-Size="Small" />
                <RowStyle Font-Size="Small" />
                <EmptyDataTemplate>
                    <div style="text-align: center">ไม่พบข้อมูล</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="#">
                        <ItemTemplate>
                            <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
        <asp:View ID="viewImport" runat="server">
            <div class="col-md-12">
                <asp:FormView ID="fvUploadFiles" runat="server" Width="100%" DefaultMode="Insert">
                    <InsertItemTemplate>
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-md-2 col-md-offset-3 control-label">บริษัท :<span
                                        class="text-danger">*</span></label>
                                <div class="col-md-4">
                                    <asp:DropDownList ID="ddlOrg" runat="server" CssClass="form-control"
                                        Enabled="False">
                                    </asp:DropDownList>
                                </div>
                                <label class="col-md-3 control-label"></label>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 col-md-offset-3 control-label">ประเภทพนักงาน :<span
                                        class="text-danger">*</span></label>
                                <div class="col-md-4">
                                    <asp:DropDownList ID="ddlEmpType" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="2">รายเดือน</asp:ListItem>
                                        <asp:ListItem Value="1">รายวัน</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <label class="col-md-3 control-label"></label>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 col-md-offset-3 control-label">วันที่จ่าย :<span
                                        class="text-danger">*</span></label>
                                <div class="col-md-4">
                                    <asp:TextBox ID="tbPayDate" runat="server" CssClass="form-control datepicker"
                                        placeholder="mm/dd/yyyy" MaxLength="10">
                                    </asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 col-md-offset-3 control-label">ไฟล์ :<span
                                        class="text-danger">*</span></label>
                                <div class="col-md-4">
                                    <asp:FileUpload ID="fuUpload" ViewStateMode="Enabled" ClientIDMode="Static"
                                        runat="server" CssClass="btn btn-md btn-default multi max-1 accept-xlsx" />
                                    <span class="text-danger">*เฉพาะไฟล์ xlsx เท่านั้น</span>
                                </div>
                                <label class="col-md-3 control-label"></label>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 col-md-offset-3 control-label"></label>
                                <div class="col-md-4">
                                    <asp:LinkButton ID="lbImportData" runat="server" CssClass="btn btn-md btn-success"
                                        OnCommand="btnCommand" CommandName="cmdImport"><i
                                            class="fas fa-upload"></i>&nbsp;อัพโหลดข้อมูล
                                    </asp:LinkButton>
                                </div>
                                <label class="col-md-3 control-label"></label>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <div class="row" style="margin-top: 10px;">
                    <div class="pre-scrollable">
                        <asp:GridView ID="gvImport" runat="server" AutoGenerateColumns="False"
                            CssClass="table table-striped table-bordered table-responsive">
                            <HeaderStyle CssClass="info" Font-Size="Small" />
                            <RowStyle Font-Size="Small" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">ไม่พบข้อมูล</div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="#">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>รหัส</td>
                                            </tr>
                                            <tr>
                                                <td>4001</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl4001" runat="server" Text='<%# Eval("4001") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ชื่อพนักงาน</td>
                                            </tr>
                                            <tr>
                                                <td>4002</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl4002" runat="server" Text='<%# Eval("4002") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>วันที่เริ่มงาน</td>
                                            </tr>
                                            <tr>
                                                <td>4003</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl4003" runat="server" Text='<%# Eval("4003") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>สาขา</td>
                                            </tr>
                                            <tr>
                                                <td>4004</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl4004" runat="server" Text='<%# Eval("4004") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ประเภทการจ้าง</td>
                                            </tr>
                                            <tr>
                                                <td>4005</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl4005" runat="server" Text='<%# Eval("4005") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>แผนก</td>
                                            </tr>
                                            <tr>
                                                <td>4006</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl4006" runat="server" Text='<%# Eval("4006") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ตำแหน่ง</td>
                                            </tr>
                                            <tr>
                                                <td>4007</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl4007" runat="server" Text='<%# Eval("4007") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เลขที่บัญชี</td>
                                            </tr>
                                            <tr>
                                                <td>4008</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl4008" runat="server" Text='<%# Eval("4008") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>วันที่จ่าย</td>
                                            </tr>
                                            <tr>
                                                <td>4009</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lbl4009" runat="server" Text='<%# Eval("4009") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินได้สะสมต่อปี</td>
                                            </tr>
                                            <tr>
                                                <td>4010</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl4010" runat="server" Text='<%# Convert.ToDouble(Eval("4010")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ภาษีสะสมต่อปี</td>
                                            </tr>
                                            <tr>
                                                <td>4011</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl4011" runat="server" Text='<%# Convert.ToDouble(Eval("4011")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินสะสมกองทุนต่อปี</td>
                                            </tr>
                                            <tr>
                                                <td>4012</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl4012" runat="server" Text='<%# Convert.ToDouble(Eval("4012")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินประกันสังคมต่อปี</td>
                                            </tr>
                                            <tr>
                                                <td>4013</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl4013" runat="server" Text='<%# Convert.ToDouble(Eval("4013")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าลดหย่อนอื่นๆ</td>
                                            </tr>
                                            <tr>
                                                <td>4014</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl4014" runat="server" Text='<%# Convert.ToDouble(Eval("4014")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>อัตราเงินเดือน/ค่าตอบแทน</td>
                                            </tr>
                                            <tr>
                                                <td>1000</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1000" runat="server" Text='<%# Convert.ToDouble(Eval("1000")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินเดือน</td>
                                            </tr>
                                            <tr>
                                                <td>1100</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1100" runat="server" Text='<%# Convert.ToDouble(Eval("1100")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าตำแหน่ง</td>
                                            </tr>
                                            <tr>
                                                <td>1230</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1230" runat="server" Text='<%# Convert.ToDouble(Eval("1230")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าปฏิบัติงาน (ประจำ)</td>
                                            </tr>
                                            <tr>
                                                <td>1272</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1272" runat="server" Text='<%# Convert.ToDouble(Eval("1272")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าประสบการณ์</td>
                                            </tr>
                                            <tr>
                                                <td>1440</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1440" runat="server" Text='<%# Convert.ToDouble(Eval("1440")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าน้ำมัน/เดินทาง</td>
                                            </tr>
                                            <tr>
                                                <td>1510</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1510" runat="server" Text='<%# Convert.ToDouble(Eval("1510")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าเดินทางอื่นๆ</td>
                                            </tr>
                                            <tr>
                                                <td>1511</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1511" runat="server" Text='<%# Convert.ToDouble(Eval("1511")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าที่พัก</td>
                                            </tr>
                                            <tr>
                                                <td>1310</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1310" runat="server" Text='<%# Convert.ToDouble(Eval("1310")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าชำนาญ</td>
                                            </tr>
                                            <tr>
                                                <td>1270</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1270" runat="server" Text='<%# Convert.ToDouble(Eval("1270")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าทักษะ</td>
                                            </tr>
                                            <tr>
                                                <td>1410</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1410" runat="server" Text='<%# Convert.ToDouble(Eval("1410")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าโทรศัพท์</td>
                                            </tr>
                                            <tr>
                                                <td>1350</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1350" runat="server" Text='<%# Convert.ToDouble(Eval("1350")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าซ่อมบำรุงรถ</td>
                                            </tr>
                                            <tr>
                                                <td>1551</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1551" runat="server" Text='<%# Convert.ToDouble(Eval("1551")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าปฏิบัติงาน SAP</td>
                                            </tr>
                                            <tr>
                                                <td>1271</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1271" runat="server" Text='<%# Convert.ToDouble(Eval("1271")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินช่วยเหลือคนพิการ</td>
                                            </tr>
                                            <tr>
                                                <td>1341</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1341" runat="server" Text='<%# Convert.ToDouble(Eval("1341")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ประธานกรรมการบริษัท</td>
                                            </tr>
                                            <tr>
                                                <td>1225</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1225" runat="server" Text='<%# Convert.ToDouble(Eval("1225")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>กรรมการและกรรมการบริหาร</td>
                                            </tr>
                                            <tr>
                                                <td>1226</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1226" runat="server" Text='<%# Convert.ToDouble(Eval("1226")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>กรรมการอิสระ</td>
                                            </tr>
                                            <tr>
                                                <td>1224</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1224" runat="server" Text='<%# Convert.ToDouble(Eval("1224")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ประธานกรรมการตรวจสอบ</td>
                                            </tr>
                                            <tr>
                                                <td>1228</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1228" runat="server" Text='<%# Convert.ToDouble(Eval("1228")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>กรรมการตรวจสอบ</td>
                                            </tr>
                                            <tr>
                                                <td>1227</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1227" runat="server" Text='<%# Convert.ToDouble(Eval("1227")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เบี้ยขยัน(พิเศษเหมาประจำ)</td>
                                            </tr>
                                            <tr>
                                                <td>1412</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1412" runat="server" Text='<%# Convert.ToDouble(Eval("1412")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>โบนัส(พิเศษ)</td>
                                            </tr>
                                            <tr>
                                                <td>1555</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1555" runat="server" Text='<%# Convert.ToDouble(Eval("1555")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>โบนัสประจำปี</td>
                                            </tr>
                                            <tr>
                                                <td>1554</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1554" runat="server" Text='<%# Convert.ToDouble(Eval("1554")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าสวัสดิการ</td>
                                            </tr>
                                            <tr>
                                                <td>1430</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1430" runat="server" Text='<%# Convert.ToDouble(Eval("1430")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าเบี้ยประชุม(Board)</td>
                                            </tr>
                                            <tr>
                                                <td>1250</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1250" runat="server" Text='<%# Convert.ToDouble(Eval("1250")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าทอด</td>
                                            </tr>
                                            <tr>
                                                <td>1444</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1444" runat="server" Text='<%# Convert.ToDouble(Eval("1444")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าจ้างค้างรับ</td>
                                            </tr>
                                            <tr>
                                                <td>1531</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1531" runat="server" Text='<%# Convert.ToDouble(Eval("1531")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินได้ค้างรับ</td>
                                            </tr>
                                            <tr>
                                                <td>1530</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1530" runat="server" Text='<%# Convert.ToDouble(Eval("1530")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินได้อื่นๆ</td>
                                            </tr>
                                            <tr>
                                                <td>1550</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1550" runat="server" Text='<%# Convert.ToDouble(Eval("1550")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินช่วยเหลือพิเศษ</td>
                                            </tr>
                                            <tr>
                                                <td>1342</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1342" runat="server" Text='<%# Convert.ToDouble(Eval("1342")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าปฏิบัติงาน (พิเศษ)</td>
                                            </tr>
                                            <tr>
                                                <td>1273</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1273" runat="server" Text='<%# Convert.ToDouble(Eval("1273")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าโอนย้าย RJN</td>
                                            </tr>
                                            <tr>
                                                <td>1567</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1567" runat="server" Text='<%# Convert.ToDouble(Eval("1567")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>คืนเงินภาษี</td>
                                            </tr>
                                            <tr>
                                                <td>1552</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1552" runat="server" Text='<%# Convert.ToDouble(Eval("1552")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>คืนเงินประกันสังคม</td>
                                            </tr>
                                            <tr>
                                                <td>1557</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1557" runat="server" Text='<%# Convert.ToDouble(Eval("1557")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>คืนเงินพักร้อน</td>
                                            </tr>
                                            <tr>
                                                <td>1423</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1423" runat="server" Text='<%# Convert.ToDouble(Eval("1423")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>Incentive</td>
                                            </tr>
                                            <tr>
                                                <td>1541</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1541" runat="server" Text='<%# Convert.ToDouble(Eval("1541")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินค่าบอกกล่าวล่วงหน้า</td>
                                            </tr>
                                            <tr>
                                                <td>1422</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1422" runat="server" Text='<%# Convert.ToDouble(Eval("1422")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินเกษียณ</td>
                                            </tr>
                                            <tr>
                                                <td>1424</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1424" runat="server" Text='<%# Convert.ToDouble(Eval("1424")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าตอบแทนพิเศษ</td>
                                            </tr>
                                            <tr>
                                                <td>1229</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1229" runat="server" Text='<%# Convert.ToDouble(Eval("1229")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินค่าชดเชยกรณีเลิกจ้าง</td>
                                            </tr>
                                            <tr>
                                                <td>1421</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1421" runat="server" Text='<%# Convert.ToDouble(Eval("1421")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าล่วงเวลา 1.0</td>
                                            </tr>
                                            <tr>
                                                <td>1110</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1110" runat="server" Text='<%# Convert.ToDouble(Eval("1110")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าล่วงเวลา 1.5</td>
                                            </tr>
                                            <tr>
                                                <td>1120</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1120" runat="server" Text='<%# Convert.ToDouble(Eval("1120")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าล่วงเวลา 3.0</td>
                                            </tr>
                                            <tr>
                                                <td>1140</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1140" runat="server" Text='<%# Convert.ToDouble(Eval("1140")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าล่วงเวลาค้างรับ</td>
                                            </tr>
                                            <tr>
                                                <td>1153</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1153" runat="server" Text='<%# Convert.ToDouble(Eval("1153")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่ากะ</td>
                                            </tr>
                                            <tr>
                                                <td>1210</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1210" runat="server" Text='<%# Convert.ToDouble(Eval("1210")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่ากะ(คนลาออกไม่คิด ปกสค.)</td>
                                            </tr>
                                            <tr>
                                                <td>1211</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1211" runat="server" Text='<%# Convert.ToDouble(Eval("1211")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เบี้ยเลี้ยง</td>
                                            </tr>
                                            <tr>
                                                <td>1260</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1260" runat="server" Text='<%# Convert.ToDouble(Eval("1260")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เบี้ยขยัน</td>
                                            </tr>
                                            <tr>
                                                <td>1411</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1411" runat="server" Text='<%# Convert.ToDouble(Eval("1411")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าล่วงเวลา(เหมาไม่คงที่)</td>
                                            </tr>
                                            <tr>
                                                <td>1152</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1152" runat="server" Text='<%# Convert.ToDouble(Eval("1152")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าล่วงเวลา(เหมาวันหยุดนักขัตฯ)</td>
                                            </tr>
                                            <tr>
                                                <td>1151</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1151" runat="server" Text='<%# Convert.ToDouble(Eval("1151")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าล่วงเวลาเหมา(เงินได้ประจำ)</td>
                                            </tr>
                                            <tr>
                                                <td>1150</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1150" runat="server" Text='<%# Convert.ToDouble(Eval("1150")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>รวมเงินได้</td>
                                            </tr>
                                            <tr>
                                                <td>1999</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl1999" runat="server" Text='<%# Convert.ToDouble(Eval("1999")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>หักขาด</td>
                                            </tr>
                                            <tr>
                                                <td>2110</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2110" runat="server" Text='<%# Convert.ToDouble(Eval("2110")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>Leave without pay</td>
                                            </tr>
                                            <tr>
                                                <td>2119</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2119" runat="server" Text='<%# Convert.ToDouble(Eval("2119")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>หักวันหยุด(ไม่รับค่าจ้าง)</td>
                                            </tr>
                                            <tr>
                                                <td>2192</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2192" runat="server" Text='<%# Convert.ToDouble(Eval("2192")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ไม่สแกนนิ้วเข้า(หักขาดงาน)</td>
                                            </tr>
                                            <tr>
                                                <td>2410</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2410" runat="server" Text='<%# Convert.ToDouble(Eval("2410")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ไม่สแกนนิ้วออก(หักขาดงาน)</td>
                                            </tr>
                                            <tr>
                                                <td>2420</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2420" runat="server" Text='<%# Convert.ToDouble(Eval("2420")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>หักพักงาน</td>
                                            </tr>
                                            <tr>
                                                <td>2190</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2190" runat="server" Text='<%# Convert.ToDouble(Eval("2190")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>หักลากิจ</td>
                                            </tr>
                                            <tr>
                                                <td>2151</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2151" runat="server" Text='<%# Convert.ToDouble(Eval("2151")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>จ่ายค่าแรง 75% /วัน</td>
                                            </tr>
                                            <tr>
                                                <td>2116</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2116" runat="server" Text='<%# Convert.ToDouble(Eval("2116")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>หักลากิจ(เกิน 7 วัน)</td>
                                            </tr>
                                            <tr>
                                                <td>2150</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2150" runat="server" Text='<%# Convert.ToDouble(Eval("2150")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>หักลาป่วยเกิน 30 วัน</td>
                                            </tr>
                                            <tr>
                                                <td>2131</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2131" runat="server" Text='<%# Convert.ToDouble(Eval("2131")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>หักลาบวช (วันที่ 8-15 นับรวมวันหยุด)</td>
                                            </tr>
                                            <tr>
                                                <td>2180</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2180" runat="server" Text='<%# Convert.ToDouble(Eval("2180")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ป่วยในงาน(จ่าย40% ใบรับรองฯไม่เกิน 1 ปี)</td>
                                            </tr>
                                            <tr>
                                                <td>2142</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2142" runat="server" Text='<%# Convert.ToDouble(Eval("2142")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>หักลาคลอด(วันที่ 46-98)</td>
                                            </tr>
                                            <tr>
                                                <td>2171</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2171" runat="server" Text='<%# Convert.ToDouble(Eval("2171")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>หักลากิจ(พิเศษ)</td>
                                            </tr>
                                            <tr>
                                                <td>2155</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2155" runat="server" Text='<%# Convert.ToDouble(Eval("2155")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>หักลาป่วย(พิเศษ)</td>
                                            </tr>
                                            <tr>
                                                <td>2141</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2141" runat="server" Text='<%# Convert.ToDouble(Eval("2141")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>หักค่าแรง (บาท)</td>
                                            </tr>
                                            <tr>
                                                <td>2114</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2114" runat="server" Text='<%# Convert.ToDouble(Eval("2114")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>หักค่าแรง (วัน)</td>
                                            </tr>
                                            <tr>
                                                <td>2115</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2115" runat="server" Text='<%# Convert.ToDouble(Eval("2115")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>หักคืนค่าล่วงเวลา</td>
                                            </tr>
                                            <tr>
                                                <td>2342</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2342" runat="server" Text='<%# Convert.ToDouble(Eval("2342")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินหักธนาคารอาคารสงเคราะห์</td>
                                            </tr>
                                            <tr>
                                                <td>2350</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2350" runat="server" Text='<%# Convert.ToDouble(Eval("2350")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินหักกรมบังคับคดี</td>
                                            </tr>
                                            <tr>
                                                <td>2351</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2351" runat="server" Text='<%# Convert.ToDouble(Eval("2351")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินหักกยศ.</td>
                                            </tr>
                                            <tr>
                                                <td>2353</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2353" runat="server" Text='<%# Convert.ToDouble(Eval("2353")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินหักกรอ.</td>
                                            </tr>
                                            <tr>
                                                <td>2354</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2354" runat="server" Text='<%# Convert.ToDouble(Eval("2354")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>หักอื่นๆ(คิดภาษี)</td>
                                            </tr>
                                            <tr>
                                                <td>2341</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2341" runat="server" Text='<%# Convert.ToDouble(Eval("2341")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินหักชำระสินเชื่อ​ Pico</td>
                                            </tr>
                                            <tr>
                                                <td>2352</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2352" runat="server" Text='<%# Convert.ToDouble(Eval("2352")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินสะสมประกันสังคม</td>
                                            </tr>
                                            <tr>
                                                <td>2901</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2901" runat="server" Text='<%# Convert.ToDouble(Eval("2901")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินสมทบกองทุนสำรองเลี้ยงชีพลูกจ้าง</td>
                                            </tr>
                                            <tr>
                                                <td>2902</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2902" runat="server" Text='<%# Convert.ToDouble(Eval("2902")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ภาษี</td>
                                            </tr>
                                            <tr>
                                                <td>2903</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2903" runat="server" Text='<%# Convert.ToDouble(Eval("2903")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>รวมเงินหัก</td>
                                            </tr>
                                            <tr>
                                                <td>2999</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl2999" runat="server" Text='<%# Convert.ToDouble(Eval("2999")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>เงินได้สุทธิ</td>
                                            </tr>
                                            <tr>
                                                <td>3000</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl3000" runat="server" Text='<%# Convert.ToDouble(Eval("3000")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าล่วงเวลา 1.0(จำนวน)</td>
                                            </tr>
                                            <tr>
                                                <td>5110</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl5110" runat="server" Text='<%# Convert.ToDouble(Eval("5110")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าล่วงเวลา 1.5(จำนวน)</td>
                                            </tr>
                                            <tr>
                                                <td>5120</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl5120" runat="server" Text='<%# Convert.ToDouble(Eval("5120")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ค่าล่วงเวลา 3.0(จำนวน)</td>
                                            </tr>
                                            <tr>
                                                <td>5140</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl5140" runat="server" Text='<%# Convert.ToDouble(Eval("5140")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ลากิจ(จ่ายค่าจ้าง)(จำนวน)</td>
                                            </tr>
                                            <tr>
                                                <td>6152</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl6152" runat="server" Text='<%# Convert.ToDouble(Eval("6152")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ลาป่วยมีใบรับรองแพทย์(จำนวน)</td>
                                            </tr>
                                            <tr>
                                                <td>6140</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl6140" runat="server" Text='<%# Convert.ToDouble(Eval("6140")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ลาป่วยไม่มีใบรับรองแพทย์(จำนวน)</td>
                                            </tr>
                                            <tr>
                                                <td>6130</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl6130" runat="server" Text='<%# Convert.ToDouble(Eval("6130")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>พักผ่อนประจำปี(จำนวน)</td>
                                            </tr>
                                            <tr>
                                                <td>6160</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl6160" runat="server" Text='<%# Convert.ToDouble(Eval("6160")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ลาวันเกิด(จำนวน)</td>
                                            </tr>
                                            <tr>
                                                <td>6162</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl6162" runat="server" Text='<%# Convert.ToDouble(Eval("6162")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ลาคลอด( วันที่ 1-45 )(จำนวน)</td>
                                            </tr>
                                            <tr>
                                                <td>6170</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl6170" runat="server" Text='<%# Convert.ToDouble(Eval("6170")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ลาดูแลภรรยาคลอดบุตร(จำนวน)</td>
                                            </tr>
                                            <tr>
                                                <td>6163</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl6163" runat="server" Text='<%# Convert.ToDouble(Eval("6163")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ลาบวช (จ่ายค่าจ้าง 1-7 วันแรก)(จำนวน)</td>
                                            </tr>
                                            <tr>
                                                <td>6181</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl6181" runat="server" Text='<%# Convert.ToDouble(Eval("6181")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ลารับราชการทหาร(60วัน)(จำนวน)</td>
                                            </tr>
                                            <tr>
                                                <td>6183</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl6183" runat="server" Text='<%# Convert.ToDouble(Eval("6183")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <table>
                                            <tr>
                                                <td>ลาทำหมันชาย(จ่ายค่าจ้างตามใบรับรองแทพย์)(จำนวน)</td>
                                            </tr>
                                            <tr>
                                                <td>6182</td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemStyle CssClass="text-right" />
                                    <ItemTemplate>
                                        <asp:Label ID="lbl6183" runat="server" Text='<%# Convert.ToDouble(Eval("6183")).ToString("#,##0.00") %>'>
                                        </asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <asp:GridView ID="gvTestTranspose" runat="server"></asp:GridView>
                    <asp:LinkButton ID="lbSaveData" runat="server" CssClass="btn btn-md btn-success"
                        OnCommand="btnCommand" CommandName="cmdSaveData"><i class="far fa-save"></i>&nbsp;บันทึกข้อมูล
                    </asp:LinkButton>
                    <asp:LinkButton ID="lbExportData" runat="server" CssClass="btn btn-md btn-success"
                        OnCommand="btnCommand" CommandName="cmdExportData"><i class="fa fa-file-excel-o"></i>&nbsp;Export
                    </asp:LinkButton>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>

    <script type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $('.datepicker').datetimepicker({
                format: 'MM/DD/YYYY'
            });

            $(".multi").MultiFile();
        })
    </script>
</asp:Content>