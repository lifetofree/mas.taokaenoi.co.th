﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_lab_place.aspx.cs" Inherits="websystem_MasterData_qa_lab_place" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

     <asp:HyperLink ID="SETFOCUS_ONTOP" runat="server"></asp:HyperLink>
    <asp:Literal ID="test_place" runat="server"></asp:Literal>

    <asp:MultiView ID="MvMaster_place" runat="server">
        <asp:View ID="view1_place" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btn_addplace" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่มข้อมูลสถานที่" data-toggle="tooltip" title="เพิ่มข้อมูลสถานที่" runat="server"
                    CommandName="cmdAddplace" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มข้อมูลสถานที่</asp:LinkButton>
            </div>

            <%--  DIV ADD--%>
            <asp:FormView ID="Fv_Insert_Result" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มข้อมูลสถานที่</h3>

                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">

                                <div class="form-horizontal" role="form">

                                    <div class="panel-heading">

                                        <div class="form-group">
                                            <asp:Label ID="lanames" runat="server" Text="ชื่อสถานที่ส่งตรวจ" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtplace_names" runat="server" CssClass="form-control" placeholder="กรอกชื่อสถานที่ ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="Re_place_name" runat="server"
                                                    ControlToValidate="txtplace_names" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อสถานที่ส่งตรวจ" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_pname" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_place_name" Width="220" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="laname_code" runat="server" Text="ชื่อย่อสถานที่ส่งตรวจ(EN)" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtplace_code" runat="server" CssClass="form-control" placeholder="ป้อนชื่อย่อสถานที่ส่งตรวจ ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="Re_code_name" runat="server"
                                                    ControlToValidate="txtplace_code" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อย่อสถานที่ส่งตรวจ(EN)" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_name_code" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_code_name" Width="250" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lab_statusplace" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddPlaceadd" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>                                              
                                            </div>
                                              <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-3">
                                                <asp:LinkButton ID="Lbtn_submit_place" ValidationGroup="Save" CssClass="btn btn-success" Text="Save" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="Lbtn_submit_place" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                                <asp:LinkButton ID="Lbtn_cancel_place" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="Lbtn_cancel_place" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>


            <%--select Place--%>
            <asp:GridView ID="Gv_select_place" runat="server" Visible="true"      
                AutoGenerateColumns="false"              
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>ไม่มีข้อมูลสถานที่</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center; padding-top: 5px;">
                                <asp:Label ID="place_idx" runat="server" Visible="false" Text='<%# Eval("place_idx") %>' />
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="ID_place" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("place_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อสถานที่ส่งตรวจ" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="Name_place" runat="server" CssClass="form-control " Text='<%# Eval("place_name") %>'></asp:TextBox> 
                                             <asp:RequiredFieldValidator ID="RequiredFieldValidatorplace_name" runat="server"
                                                    ControlToValidate="Name_place" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อสถานที่ส่งตรวจ" ValidationGroup="Editplacename" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_pname" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorplace_name" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อย่อสถานที่ส่งตรวจ(EN)" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="Code_place" runat="server" CssClass="form-control " Text='<%# Eval("place_code") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorCode_place" runat="server"
                                                    ControlToValidate="Code_place" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อย่อสถานที่ส่งตรวจ" ValidationGroup="Editplacename" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenderCode_place" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorCode_place" Width="220" />
                                        </div>
                                         <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdates_place" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddEdit_place" Text='<%# Eval("place_status") %>'
                                                CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editplacename" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อสถานที่" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="30%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="place_names" runat="server"
                                    Text='<%# Eval("place_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อย่อสถานที่(EN)" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="30%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbtitle_it_news" runat="server"
                                     Text='<%# Eval("place_code") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbplace_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("place_status") %>'></asp:Label>
                                    <asp:Label ID="place_statusOpen" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="สถานที่นี้เปิดใช้งานอยู่"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="place_statusClose" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="สถานที่นี้ปิดการใช้งานแล้ว" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="btnTodelete_Place"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบสถานที่นี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("place_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
    </asp:MultiView>
</asp:Content>

