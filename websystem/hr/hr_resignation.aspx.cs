﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class websystem_hr_hr_resignation : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    data_employee _dtEmployee = new data_employee();
    data_resignation _dtresign = new data_resignation();
    function_tool _funcTool = new function_tool();
    data_softwarelicense_devices _dtswl = new data_softwarelicense_devices();

    string _localJson = String.Empty;
    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    public string checkfile;
    int[] posidx_hradmin = { 5908, 1019 };
    int[] posidx_hrdirector = { 5916, 8150, 1018 };

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlSelect_ReasonExit = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ReasonExit"];
    static string _urlGetM0_Asset_Select = _serviceUrl + ConfigurationManager.AppSettings["urlGetM0_Asset_Select"];
    static string _urlGetWHEREHolder = _serviceUrl + ConfigurationManager.AppSettings["urlGetWHEREHolder"];
    static string _urlGetDetailSoftwareProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailSoftwareProfile"];
    static string _urlSelect_DetailProfile = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_DetailProfile"];
    static string _urlSelect_ASSETProfile = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ASSETProfile"];
    static string _urlInsert_Resignation = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Resignation"];
    static string _urlSelect_TypeAnswer_Resignation = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeAnswer_Resignation"];
    static string _urlSelect_Question_Resignation = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Question_Resignation"];
    static string _urlSelect_SubQuestion_Resignation = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_SubQuestion_Resignation"];
    static string _urlInsert_Data_ExitInterview = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Data_ExitInterview"];
    static string _urlSelect_ShowDataList = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ShowDataList"];
    static string _urlSelect_ShowData_Asset = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ShowData_Asset"];
    static string _urlSelect_ShowData_Holder = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ShowData_Holder"];
    static string _urlSelect_ShowData_Detail = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_ShowData_Detail"];
    static string _urlSelect_Index_Approve = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Index_Approve"];
    static string _urlUpdate_Approver = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_Approver_resignation"];
    static string _urlSelect_Log = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Log"];
    static string _urlSelect_Detail_Edit = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Detail_Edit"];
    static string _urlSelect_Detail_Answer = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Detail_Answer"];

    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlInsert_Exchanget = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Exchange"];
    static string _urlSelect_Detail_Exchange = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Detail_Exchange"];
    static string _urlUpdate_Approver_Exchange = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_Approver_Exchange"];
    static string _urlSelect_Sumlist = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Sumlist"];
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] =  int.Parse(Session["emp_idx"].ToString());
            select_empIdx_present();
            _divMenuLiToViewIndex.Attributes.Add("class", "active");
            ViewState["m0_toidx"] = "0";
            SetDefaultpage(1);
            select_sumlist(int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ViewState["Pos_idx"].ToString()), int.Parse(ViewState["Org_idx"].ToString()));
        }

        linkBtnTrigger(_divMenuBtnToDivIndex);
        linkBtnTrigger(_divMenuBtnToDivAdd);
        linkBtnTrigger(_divMenuBtnToDivApprove);
        linkBtnTrigger(_divMenuBtnToDivManual);
        linkBtnTrigger(_divMenuBtnToDivFlowChart);
        ImangeBtnTrigger(imgcreate);
        ImangeBtnTrigger(imgedit);
        //ImangeBtnTrigger(imgchange);
    }

    #endregion

    #region Select
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());

        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;
        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["JobGradeIDX"] = _dtEmployee.employee_list[0].jobgrade_idx;
    }

    protected void select_reasonexit(RadioButtonList rdoName)
    {
        _dtresign = new data_resignation();

        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit _reason = new M0_ReasonExit();

        _dtresign.Boxm0_ReasonExit[0] = _reason;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtresign = callServicePostResignation(_urlSelect_ReasonExit, _dtresign);

        setRdoData(rdoName, _dtresign.Boxm0_ReasonExit, "reason_exit", "m0_rsidx");

    }

    protected void SelectGetHolderList(GridView gvName, int cempidx)
    {

        _dtEmployee = new data_employee();
        _dtEmployee.BoxHolder_list = new Holder_List[1];
        Holder_List _dtemp = new Holder_List();

        _dtemp.NHDEmpIDX = cempidx; //1374;//
        _dtEmployee.BoxHolder_list[0] = _dtemp;
        ///  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtEmployee));

        _dtEmployee = callServicePostEmp(_urlGetWHEREHolder, _dtEmployee);
        setGridData(gvName, _dtEmployee.BoxHolder_list);



    }

    protected void select_asset(GridView gvName, int cempidx)
    {
        _dtresign = new data_resignation();

        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit _reason = new M0_ReasonExit();

        _reason.CEmpIDX = cempidx;

        _dtresign.Boxm0_ReasonExit[0] = _reason;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtresign = callServicePostResignation(_urlSelect_ASSETProfile, _dtresign);

        setGridData(gvName, _dtresign.Boxm0_ASSET);

    }

    protected void select_detailprofile(TextBox textyear, int cempidx, TextBox txtexp, TextBox txtin)
    {
        _dtresign = new data_resignation();

        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit _reason = new M0_ReasonExit();

        _reason.CEmpIDX = cempidx;
        _dtresign.Boxm0_ReasonExit[0] = _reason;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtresign = callServicePostResignation(_urlSelect_DetailProfile, _dtresign);

        textyear.Text = _dtresign.Boxm0_ReasonExit[0].EmpIN_Ex.ToString();
        txtexp.Text = _dtresign.Boxm0_ReasonExit[0].dateexp.ToString();
        txtin.Text = _dtresign.Boxm0_ReasonExit[0].EmpIN.ToString();
    }

    protected void select_gettypeanswer(GridView gvName, int m0_toidx)
    {

        _dtresign = new data_resignation();

        _dtresign.Boxm0_Question = new M0_Question[1];
        M0_Question _typeans = new M0_Question();

        _typeans.m0_toidx = m0_toidx;

        _dtresign.Boxm0_Question[0] = _typeans;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtrcm));

        _dtresign = callServicePostResignation(_urlSelect_TypeAnswer_Resignation, _dtresign);
        setGridData(gvName, _dtresign.Boxm0_Question);
    }

    protected void select_getquestion(GridView gvName, int m0_taidx, int m0_toidx)
    {

        _dtresign = new data_resignation();

        _dtresign.Boxm0_Question = new M0_Question[1];
        M0_Question _qustion = new M0_Question();

        _qustion.m0_toidx = m0_toidx;
        _qustion.m0_taidx = m0_taidx;

        _dtresign.Boxm0_Question[0] = _qustion;

        _dtresign = callServicePostResignation(_urlSelect_Question_Resignation, _dtresign);
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        setGridData(gvName, _dtresign.Boxm0_Question);
    }

    protected void select_showdatalist(GridView gvName, int cempidx)
    {

        _dtresign = new data_resignation();

        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit _select = new M0_ReasonExit();

        _select.CEmpIDX = cempidx;

        _dtresign.Boxm0_ReasonExit[0] = _select;
    //    txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtresign = callServicePostResignation(_urlSelect_ShowDataList, _dtresign);
        setGridData(gvName, _dtresign.Boxm0_ReasonExit);
    }

    protected void select_empIdx_create()
    {
        //txt.Text =  ViewState["CEmpIDX_u0"].ToString();
        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["CEmpIDX_u0"].ToString());

        ViewState["rdept_name_create"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx_create"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName_create"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name_create"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx_create"] = _dtEmployee.employee_list[0].org_idx;
        ViewState["EmpCode_create"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname_create"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx_create"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email_create"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel_create"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname_create"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx_create"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX_create"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["JobGradeIDX_create"] = _dtEmployee.employee_list[0].jobgrade_idx;
    }

    protected void select_asset_detail(GridView gvName, int cempidx, int u0_docidx)
    {
        _dtresign = new data_resignation();

        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit _reason = new M0_ReasonExit();

        _reason.CEmpIDX = cempidx;
        _reason.u0_docidx = u0_docidx;
        _dtresign.Boxm0_ReasonExit[0] = _reason;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtresign = callServicePostResignation(_urlSelect_ShowData_Asset, _dtresign);

        setGridData(gvName, _dtresign.Boxm0_ASSET);

    }

    protected void select_holder_detail(GridView gvName, int cempidx, int u0_docidx)
    {
        _dtresign = new data_resignation();

        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit _reason = new M0_ReasonExit();

        _reason.CEmpIDX = cempidx;
        _reason.u0_docidx = u0_docidx;
        _dtresign.Boxm0_ReasonExit[0] = _reason;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtresign = callServicePostResignation(_urlSelect_ShowData_Holder, _dtresign);

        setGridData(gvName, _dtresign.Boxm0_ASSET);

    }

    protected void select_detail(FormView fvName, int cempidx, int u0_docidx)
    {
        _dtresign = new data_resignation();

        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit _reason = new M0_ReasonExit();

        _reason.CEmpIDX = cempidx;
        _reason.u0_docidx = u0_docidx;
        _dtresign.Boxm0_ReasonExit[0] = _reason;

        _dtresign = callServicePostResignation(_urlSelect_ShowData_Detail, _dtresign);
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        setFormViewData(fvName, _dtresign.Boxm0_ReasonExit);

    }

    protected void select_index_approve(GridView gvName, int cempidx, int rposidx, int orgidx)
    {

        _dtresign = new data_resignation();

        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit _select = new M0_ReasonExit();

        _select.CEmpIDX = cempidx;
        _select.orgidx =  orgidx;
        _select.rposidx = rposidx;
        _dtresign.Boxm0_ReasonExit[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtresign = callServicePostResignation(_urlSelect_Index_Approve, _dtresign);
        setGridData(gvName, _dtresign.Boxm0_ReasonExit);
    }

    protected void select_log(Repeater rptLog, int u0_docidx)
    {

        _dtresign = new data_resignation();

        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit _select = new M0_ReasonExit();

        _select.u0_docidx = u0_docidx;

        _dtresign.Boxm0_ReasonExit[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtresign = callServicePostResignation(_urlSelect_Log, _dtresign);

        rptLog.DataSource = _dtresign.Boxm0_ReasonExit;
        rptLog.DataBind();
    }

    protected void select_detail_foredit(FormView fvName, int cempidx)
    {
        _dtresign = new data_resignation();

        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit _reason = new M0_ReasonExit();

        _reason.CEmpIDX = cempidx;
        _dtresign.Boxm0_ReasonExit[0] = _reason;

        _dtresign = callServicePostResignation(_urlSelect_Detail_Edit, _dtresign);
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        setFormViewData(fvName, _dtresign.Boxm0_ReasonExit);
        ViewState["Edit_insert"] = _dtresign.ReturnCode.ToString();
    }

    protected void select_getquestion_answer(GridView gvName, int m0_taidx, int m0_toidx, int u0_docidx)
    {

        _dtresign = new data_resignation();

        _dtresign.Boxm0_Question = new M0_Question[1];
        M0_Question _qustion = new M0_Question();

        _qustion.m0_toidx = m0_toidx;
        _qustion.m0_taidx = m0_taidx;
        _qustion.u0_docidx = u0_docidx;
        _dtresign.Boxm0_Question[0] = _qustion;

        _dtresign = callServicePostResignation(_urlSelect_Detail_Answer, _dtresign);
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        setGridData(gvName, _dtresign.Boxm0_Question);
    }

    protected void getOrganizationList(DropDownList ddlName)
    {

        _dtEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dtEmployee.organization_list[0] = _orgList;

        _dtEmployee = callServicePostEmp(_urlGetOrganizationList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("องค์กร...", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dtEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dtEmployee.department_list[0] = _deptList;

        _dtEmployee = callServicePostEmp(_urlGetDepartmentList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("ฝ่าย...", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dtEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dtEmployee.section_list[0] = _secList;

        _dtEmployee = callServicePostEmp(_urlGetSectionList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("แผนก...", "0"));
    }

    protected void select_exchange_detail(GridView gvName, int u0_docidx)
    {
        _dtresign = new data_resignation();

        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit _reason = new M0_ReasonExit();

        _reason.u0_docidx = u0_docidx;
        _dtresign.Boxm0_ReasonExit[0] = _reason;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtresign = callServicePostResignation(_urlSelect_Detail_Exchange, _dtresign);

        setGridData(gvName, _dtresign.Boxu2_Exchange);

    }

    protected void select_sumlist(int cempidx, int rposidx, int orgidx)
    {
        _dtresign = new data_resignation();

        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit _select = new M0_ReasonExit();

        _select.CEmpIDX = cempidx;
        _select.orgidx =  orgidx;
        _select.rposidx = rposidx;
        _dtresign.Boxm0_ReasonExit[0] = _select;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtresign = callServicePostResignation(_urlSelect_Sumlist, _dtresign);

        nav_approve.Text = "<span class='badge progress-bar-danger' >" + _dtresign.ReturnMsg.ToString() + "</span>";
    }

    #endregion

    #region Insert && Update
    protected void Insert_Resignation()
    {
        TextBox txtremark = (TextBox)fv_insert.FindControl("txtremark");
        TextBox dateexit = (TextBox)fv_insert.FindControl("dateexit");
        RadioButtonList rdochoice = (RadioButtonList)fv_insert.FindControl("rdochoice");
        TextBox txtrdo = (TextBox)fv_insert.FindControl("txtrdo");

        HttpFileCollection hfcit2 = Request.Files;

        _dtresign = new data_resignation();
        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit insert = new M0_ReasonExit();

        insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        insert.m0_tidx = int.Parse(ViewState["m0_tidx"].ToString());
        insert.reason_resign = txtremark.Text;
        insert.date_resign = dateexit.Text;
        insert.m0_rsidx = int.Parse(rdochoice.SelectedValue);
        insert.orgidx = int.Parse(ViewState["Org_idx"].ToString());
        insert.rdeptidx = int.Parse(ViewState["rdept_idx"].ToString());
        insert.rsecidx = int.Parse(ViewState["Sec_idx"].ToString());
        insert.jobgradeidx = int.Parse(ViewState["JobGradeIDX"].ToString());
        insert.unidx = 1;
        insert.acidx = 1;
        insert.staidx = 1;

        if (rdochoice.SelectedValue == "5")
        {
            insert.comment_m0rsidx = txtrdo.Text;
        }
        else
        {
            insert.comment_m0rsidx = "-";
        }

        _dtresign.Boxm0_ReasonExit[0] = insert;

        int i = 0;
        var ds_udoc1_insert = (DataSet)ViewState["vsBuyequipment"];
        var _document1 = new M0_ASSET[ds_udoc1_insert.Tables[0].Rows.Count];


        foreach (DataRow dtrow in ds_udoc1_insert.Tables[0].Rows)
        {
            if (dtrow["u0_asidx"].ToString() != null && dtrow["u0_asidx"].ToString() != "0" && dtrow["u0_asidx"].ToString() != "")
            {
                _document1[i] = new M0_ASSET();

                _document1[i].u0_asidx = int.Parse(dtrow["u0_asidx"].ToString());
                _document1[i].type_asset = int.Parse(dtrow["type_asset"].ToString());

                i++;

                _dtresign.Boxm0_ASSET = _document1;

            }

        }
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));
        _dtresign = callServicePostResignation(_urlInsert_Resignation, _dtresign);
        string rtcode = _dtresign.ReturnMsg.ToString();
        string rtexit = _dtresign.ReturnExit.ToString();

        ViewState["rtcode"] = rtcode;
        ViewState["m0_toidx"] = rtexit;

        hfcit2 = Request.Files;
        if (hfcit2.Count > 0)
        {
            for (int ii = 0; ii < hfcit2.Count; ii++)
            {
                HttpPostedFile hpfLo = hfcit2[ii];
                if (hpfLo.ContentLength > 1)
                {
                    string getPath = ConfigurationSettings.AppSettings["path_file_resignation"];
                    string fileName1 = rtcode + ii;
                    string filePath1 = Server.MapPath(getPath + rtcode);
                    if (!Directory.Exists(filePath1))
                    {
                        Directory.CreateDirectory(filePath1);
                    }
                    string extension = Path.GetExtension(hpfLo.FileName);

                    hpfLo.SaveAs(Server.MapPath(getPath + rtcode) + "\\" + fileName1 + extension);
                }
            }
        }


    }

    protected void EditInsert_Resignation()
    {
        TextBox txtremark_edit = (TextBox)fv_edit.FindControl("txtremark_edit");
        TextBox dateexit_edit = (TextBox)fv_edit.FindControl("dateexit_edit");
        RadioButtonList rdochoice_edit = (RadioButtonList)fv_edit.FindControl("rdochoice_edit");
        TextBox txtrdo_edit = (TextBox)fv_edit.FindControl("txtrdo_edit");


        _dtresign = new data_resignation();
        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit insert = new M0_ReasonExit();

        insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        insert.m0_tidx = int.Parse(ViewState["m0_tidx"].ToString());
        insert.reason_resign = txtremark_edit.Text;
        insert.date_resign = dateexit_edit.Text;
        insert.m0_rsidx = int.Parse(rdochoice_edit.SelectedValue);
        insert.orgidx = int.Parse(ViewState["Org_idx"].ToString());
        insert.rdeptidx = int.Parse(ViewState["rdept_idx"].ToString());
        insert.rsecidx = int.Parse(ViewState["Sec_idx"].ToString());
        insert.jobgradeidx = int.Parse(ViewState["JobGradeIDX"].ToString());
        insert.unidx = 1;
        insert.acidx = 1;
        insert.staidx = 1;
        insert.u0_docidx_exit = int.Parse(ViewState["u0_docidx"].ToString());
        if (rdochoice_edit.SelectedValue == "5")
        {
            insert.comment_m0rsidx = txtrdo_edit.Text;
        }
        else
        {
            insert.comment_m0rsidx = "-";
        }

        _dtresign.Boxm0_ReasonExit[0] = insert;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));
        _dtresign = callServicePostResignation(_urlInsert_Resignation, _dtresign);



    }

    protected void Insert_Data_Exitinterview()
    {
        GridView GvTypeAns = (GridView)ViewExitinterview.FindControl("GvTypeAns");
        _dtresign = new data_resignation();

        foreach (GridViewRow row in GvTypeAns.Rows)
        {
            Label lblm0_taidx = (Label)row.Cells[0].FindControl("lblm0_taidx");
            GridView GvQuestion = (GridView)row.Cells[0].FindControl("GvQuestion");

            int i = 0;
            var u1_ans = new M1_ReasonExit[GvQuestion.Rows.Count];

            foreach (GridViewRow row_quest in GvQuestion.Rows)
            {

                Label lblm0_quidx = (Label)row_quest.Cells[1].FindControl("lblm0_quidx");
                RadioButtonList rdochoice_exit = (RadioButtonList)row_quest.Cells[1].FindControl("rdochoice_exit");
                CheckBoxList chkchoice_exit = (CheckBoxList)row_quest.Cells[1].FindControl("chkchoice_exit");
                TextBox txtquest = (TextBox)row_quest.Cells[1].FindControl("txtquest");

                HtmlTextArea txtchk = ((HtmlTextArea)row_quest.Cells[1].FindControl("txtchk"));
                HtmlTextArea txtrdo = ((HtmlTextArea)row_quest.Cells[1].FindControl("txtrdo"));
                //TextBox txtrdo = FindControl("txtrdo") as TextBox;
                //TextBox txtchk = FindControl("txtchk") as TextBox;

                int a = 0;
                _dtresign.Boxm1_ReasonExit = new M1_ReasonExit[1];
                u1_ans[i] = new M1_ReasonExit();

                u1_ans[i].m0_quidx = int.Parse(lblm0_quidx.Text);

                if (lblm0_taidx.Text == "1")
                {

                    if (rdochoice_exit.SelectedValue != "0" && rdochoice_exit.SelectedValue != null && rdochoice_exit.SelectedValue != "")
                    {
                        u1_ans[i].answer = rdochoice_exit.SelectedValue.ToString();

                        if (Request.Form["txtrdo" + rdochoice_exit.SelectedValue + a + ""].ToString() != null)
                        {
                            u1_ans[i].remark = Request.Form["txtrdo" + rdochoice_exit.SelectedValue + a + ""].ToString(); //Request.Form["txtrdo"].ToString();
                        }
                        else
                        {
                            u1_ans[i].remark = "-";
                        }
                    }
                    else if (chkchoice_exit.SelectedValue != "0" && chkchoice_exit.SelectedValue != null && chkchoice_exit.SelectedValue != "")
                    {
                        ViewState["ChkAns"] = null;
                        List<String> AddoingList = new List<string>();
                        foreach (ListItem item in chkchoice_exit.Items)
                        {
                            if (item.Selected)
                            {
                                AddoingList.Add(item.Value);

                                if (Request.Form["txtchk" + item.Value + a + ""].ToString() != null)
                                {
                                    u1_ans[i].remark = Request.Form["txtchk" + item.Value + a + ""].ToString();
                                }
                                else
                                {
                                    u1_ans[i].remark = "-";
                                }
                            }
                        }

                        String Addoing = String.Join(",", AddoingList.ToArray());
                        ViewState["ChkAns"] = Addoing;

                        u1_ans[i].answer = ViewState["ChkAns"].ToString();//chkchoice_exit.SelectedValue.ToString();

                    }

                }
                else if (lblm0_taidx.Text == "2")
                {
                    if (txtquest.Text != "" && txtquest.Text != null && txtquest.Text != "")
                    {
                        u1_ans[i].answer = txtquest.Text;
                    }
                }
                _dtresign.Boxm1_ReasonExit = u1_ans;
                i++;
            }

            M0_ReasonExit u0_ans = new M0_ReasonExit();
            _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];

            u0_ans.u0_docidx = int.Parse(ViewState["rtcode"].ToString());
            u0_ans.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
            u0_ans.m0_toidx = int.Parse(ViewState["m0_toidx"].ToString());

            _dtresign.Boxm0_ReasonExit[0] = u0_ans;

            //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));
            _dtresign = callServicePostResignation(_urlInsert_Data_ExitInterview, _dtresign);
        }
    }

    protected void UpdateApprover(int acidx, int unidx, int staidx, int type_job_flow, int m0_tidx, int cempidx, string comment_approver, int u0_docidx, int docidx_ref)
    {
        _dtresign = new data_resignation();
        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit _update = new M0_ReasonExit();

        _update.acidx = acidx;
        _update.unidx = unidx;
        _update.staidx = staidx;
        _update.type_jobgrade_flow = type_job_flow;
        _update.m0_tidx = m0_tidx;
        _update.CEmpIDX = cempidx;
        _update.comment_approver = comment_approver;
        _update.u0_docidx = u0_docidx;
        _update.docidx_ref = docidx_ref;

        _dtresign.Boxm0_ReasonExit[0] = _update;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));
        _dtresign = callServicePostResignation(_urlUpdate_Approver, _dtresign);

    }

    protected void Update_Edit(int u0_docidx, string reason_resign, string date_resign, int cempidx, int unidx, int acidx, int staidx, int m0_tidx, int jobgradeidx)
    {
        _dtresign = new data_resignation();
        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit insert = new M0_ReasonExit();

        insert.CEmpIDX = cempidx;
        insert.reason_resign = reason_resign;
        insert.date_resign = date_resign;
        insert.u0_docidx = u0_docidx;
        insert.unidx = unidx;
        insert.acidx = acidx;
        insert.staidx = staidx;
        insert.m0_tidx = m0_tidx;
        insert.jobgradeidx = jobgradeidx;

        _dtresign.Boxm0_ReasonExit[0] = insert;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));
        _dtresign = callServicePostResignation(_urlInsert_Resignation, _dtresign);
    }

    protected void Insert_Exchange()
    {
        _dtresign = new data_resignation();
        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit insert = new M0_ReasonExit();

        insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        insert.m0_tidx = 4;
        insert.jobgradeidx = int.Parse(ViewState["JobGradeIDX"].ToString());
        insert.unidx = 1;
        insert.acidx = 1;
        insert.staidx = 0;
        insert.u0_docidx = int.Parse(ViewState["u0_docidx"].ToString());


        _dtresign.Boxm0_ReasonExit[0] = insert;

        int i = 0;
        var ds_udoc2_insert = (DataSet)ViewState["vsBuyequipment_exchange"];
        var _document2 = new U2_Exchange_Response[ds_udoc2_insert.Tables[0].Rows.Count];


        foreach (DataRow dtrow in ds_udoc2_insert.Tables[0].Rows)
        {

            _document2[i] = new U2_Exchange_Response();

            _document2[i].u0_docidx = int.Parse(ViewState["u0_docidx"].ToString());
            _document2[i].orgidx = int.Parse(dtrow["OrgIDX_exchange"].ToString());
            _document2[i].rdeptidx = int.Parse(dtrow["RDeptIDX_exchange"].ToString());
            _document2[i].rsecidx = int.Parse(dtrow["RSecIDX_exchange"].ToString());
            _document2[i].date_exchange = dtrow["date_exchange"].ToString();
            _document2[i].detail_exchange = dtrow["detail_exchange"].ToString();

            i++;

            _dtresign.Boxu2_Exchange = _document2;



        }
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));
        _dtresign = callServicePostResignation(_urlInsert_Exchanget, _dtresign);
    }

    protected void UpdateApprove_Exchange(int cempidx, int u0_docidx, int unidx, int acidx, int staidx, string comment_approver, int jobgradeidx)
    {
        _dtresign = new data_resignation();
        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit insert = new M0_ReasonExit();

        insert.CEmpIDX = cempidx;
        insert.u0_docidx = u0_docidx;
        insert.unidx = unidx;
        insert.acidx = acidx;
        insert.staidx = staidx;
        insert.type_jobgrade_flow = jobgradeidx;
        insert.comment_approver = comment_approver;

        _dtresign.Boxm0_ReasonExit[0] = insert;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));
        _dtresign = callServicePostResignation(_urlUpdate_Approver_Exchange, _dtresign);
    }

    protected void Insert_Cancel()
    {
        TextBox txtremark_detail = (TextBox)fvinsert_detail.FindControl("txtremark_detail");
        TextBox dateexit_detail = (TextBox)fvinsert_detail.FindControl("dateexit_detail");
        TextBox txtrdo = (TextBox)fvinsert_detail.FindControl("txtrdo");
        TextBox lblm0_rsidx = (TextBox)fvinsert_detail.FindControl("lblm0_rsidx");
        TextBox txtremrk_cancel = (TextBox)fvinsert_detail.FindControl("txtremrk_cancel");


        _dtresign = new data_resignation();
        _dtresign.Boxm0_ReasonExit = new M0_ReasonExit[1];
        M0_ReasonExit insert = new M0_ReasonExit();

        insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        insert.m0_tidx = 3;
        insert.reason_resign = txtremrk_cancel.Text;
        insert.date_resign = dateexit_detail.Text;
        insert.m0_rsidx = int.Parse(lblm0_rsidx.Text);
        insert.comment_m0rsidx = txtrdo.Text;
        insert.orgidx = int.Parse(ViewState["Org_idx"].ToString());
        insert.rdeptidx = int.Parse(ViewState["rdept_idx"].ToString());
        insert.rsecidx = int.Parse(ViewState["Sec_idx"].ToString());
        insert.jobgradeidx = int.Parse(ViewState["JobGradeIDX"].ToString());
        insert.u0_docidx_exit = int.Parse(ViewState["u0_docidx"].ToString());
        insert.unidx = 11;
        insert.acidx = 1;
        insert.staidx = 1;

        _dtresign.Boxm0_ReasonExit[0] = insert;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

        _dtresign = callServicePostResignation(_urlInsert_Resignation, _dtresign);
        string rtcode = _dtresign.ReturnMsg.ToString();
    }

    #endregion

    #region CallService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_resignation callServicePostResignation(string _cmdUrl, data_resignation _dtresign)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtresign);
        // text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtresign = (data_resignation)_funcTool.convertJsonToObject(typeof(data_resignation), _localJson);


        return _dtresign;
    }

    protected data_softwarelicense_devices callServiceSoftwareLicenseDevices(string _cmdUrl, data_softwarelicense_devices _data_softwarelicense_devices)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_softwarelicense_devices);
        // litDebug.Text = _localJson;

        // call services
        //_localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_softwarelicense_devices = (data_softwarelicense_devices)_funcTool.convertJsonToObject(typeof(data_softwarelicense_devices), _localJson);

        return _data_softwarelicense_devices;
    }
    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }
    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setRdoData(RadioButtonList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }
    #endregion

    #region Trigger
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }


    protected void ImangeBtnTrigger(ImageButton ImgBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = ImgBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);

    }

    protected void linkBtnAsynTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger asynctrigger1 = new AsyncPostBackTrigger();
        asynctrigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(asynctrigger1);
    }


    #endregion

    #region SetDefault
    protected void SetDefaultpage(int page)
    {

        switch (page)
        {

            case 1:
                _divMenuLiToViewIndex.Attributes.Add("class", "active");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                MvMaster.SetActiveView(ViewIndex);
                select_showdatalist(GvListEX, int.Parse(ViewState["EmpIDX"].ToString()));
                break;

            case 2:

                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Add("class", "active");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                MvMaster.SetActiveView(ViewInsert);

                imgcreate.ImageUrl = "~/images/hr_resignation/Icon_ปุ่มระบบลาออก-01.png";
                imgedit.ImageUrl = "~/images/hr_resignation/Icon_ปุ่มระบบลาออก-09.png";
                //imgchange.ImageUrl = "~/images/hr_resignation/Icon_ปุ่มระบบลาออก-05.png";

                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();


                div_create_quit.Visible = false;
                div_edit_quit.Visible = false;
                break;

            case 3:


                _divMenuLiToViewIndex.Attributes.Add("class", "active");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                MvMaster.SetActiveView(ViewExitinterview);

                select_gettypeanswer(GvTypeAns, int.Parse(ViewState["m0_toidx"].ToString()));

                break;

            case 4:
                _divMenuLiToViewIndex.Attributes.Add("class", "active");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                MvMaster.SetActiveView(ViewDetail);

                select_empIdx_create();
                Fvdetailusercreate.ChangeMode(FormViewMode.Insert);
                Fvdetailusercreate.DataBind();
                div_detail_create.Visible = true;
                div_detailasset.Visible = true;
                div_exitinterview.Visible = false;
                _divMenuLiToViewDetail_exitinterivew.Visible = false;

                if (((posidx_hradmin.Contains(int.Parse(ViewState["Pos_idx"].ToString())) || posidx_hrdirector.Contains(int.Parse(ViewState["Pos_idx"].ToString()))) && ViewState["m0_tidx_create"].ToString() == "1") ||
                   (ViewState["CEmpIDX_u0"].ToString() == ViewState["EmpIDX"].ToString() && ViewState["m0_tidx_create"].ToString() == "1"))
                {
                    _divMenuLiToViewDetail_exitinterivew.Visible = true;

                }
                else
                {
                    _divMenuLiToViewDetail_exitinterivew.Visible = false;

                }

                if (ViewState["m0_tidx_create"].ToString() == "1")
                {
                    div_selectasset.Visible = true;
                }
                else
                {
                    div_selectasset.Visible = false;
                }


                select_asset_detail(GvAsset_detail, int.Parse(ViewState["CEmpIDX_u0"].ToString()), int.Parse(ViewState["u0_docidx"].ToString()));
                select_holder_detail(GvHolder_detail, int.Parse(ViewState["CEmpIDX_u0"].ToString()), int.Parse(ViewState["u0_docidx"].ToString()));

                select_detail(fvinsert_detail, int.Parse(ViewState["CEmpIDX_u0"].ToString()), int.Parse(ViewState["u0_docidx"].ToString()));

                fvinsert_detail.ChangeMode(FormViewMode.Edit);
                fvinsert_detail.DataBind();

                var txtstartdate_detail = (TextBox)fvinsert_detail.FindControl("txtstartdate_detail");
                var txtyear_detail = (TextBox)fvinsert_detail.FindControl("txtyear_detail");
                var txtin_detail = (TextBox)fvinsert_detail.FindControl("txtin_detail");
                var hfM0NodeIDX_EX = (HiddenField)fvinsert_detail.FindControl("hfM0NodeIDX_EX");
                var AddCancel = (LinkButton)fvinsert_detail.FindControl("AddCancel");
                var div_cancelquit = (Control)fvinsert_detail.FindControl("div_cancelquit");
                var hfm0_toidx = (HiddenField)fvinsert_detail.FindControl("hfm0_toidx");

                //var dateexit_detail = (TextBox)fvinsert_detail.FindControl("dateexit_detail");
                //var txtremark_detail = (TextBox)fvinsert_detail.FindControl("txtremark_detail");

                ViewState["m0_toidx"] = hfm0_toidx.Value;

                select_detailprofile(txtstartdate_detail, int.Parse(ViewState["CEmpIDX_u0"].ToString()), txtyear_detail, txtin_detail);
                string date_detail = DateTime.Now.ToString("yyyy-MM-dd");

                txtyear_detail.Text = _funcTool.calYearMonthDay(DateTime.Parse(txtin_detail.Text), DateTime.Parse(date_detail));
                select_log(rpLog, int.Parse(ViewState["u0_docidx"].ToString()));

                try
                {
                    string getPathLotus = ConfigurationSettings.AppSettings["path_file_resignation"];

                    string filePathLotus = Server.MapPath(getPathLotus + ViewState["u0_docidx"].ToString());
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                    SearchDirectories(myDirLotus, ViewState["u0_docidx"].ToString());
                }
                catch
                {

                }

                if (ViewState["arg1_detail"].ToString() == "2")
                {
                    divexchange.Visible = true;
                    AddCancel.Visible = false;
                    div_cancelquit.Visible = false;

                    if (hfM0NodeIDX_EX.Value == "0")
                    {
                        fvexchange.ChangeMode(FormViewMode.Insert);
                        fvexchange.DataBind();
                        GvSelectExchange.Visible = false;

                    }
                    else
                    {
                        fvexchange.Visible = false;
                        select_exchange_detail(GvSelectExchange, int.Parse(ViewState["u0_docidx"].ToString()));
                        GvSelectExchange.Visible = true;
                    }
                    //txt.Text = hfM0NodeIDX_EX.Value;
                }
                else
                {
                    divexchange.Visible = false;

                    if (ViewState["arg1_detail"].ToString() == "3")
                    {
                        div_cancelquit.Visible = true;
                    }
                    else
                    {
                        div_cancelquit.Visible = false;
                    }
                }

                setFormData();

                break;

            case 5:
                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Add("class", "active");
                MvMaster.SetActiveView(ViewAproove);
                select_index_approve(GvApprove, int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ViewState["Pos_idx"].ToString()), int.Parse(ViewState["Org_idx"].ToString()));
                ddl_approve.SelectedValue = "0";
                txtremark_approve.Text = String.Empty;
                break;

        }
    }


    #endregion

    #region SetViewState


    protected void SetViewState()
    {

        DataSet dsPersonList = new DataSet();
        dsPersonList.Tables.Add("dsAddListTable");

        dsPersonList.Tables["dsAddListTable"].Columns.Add("type_asset", typeof(int));
        dsPersonList.Tables["dsAddListTable"].Columns.Add("u0_asidx", typeof(int));

        ViewState["vsBuyequipment"] = dsPersonList;

    }

    protected void CleardataSetPersonFormList()
    {
        ViewState["vsBuyequipment"] = null;
        //GvName.DataSource = ViewState["vsBuyequipment"];
        //GvName.DataBind();
        SetViewState();
    }

    protected void setAddList_GvAsset()
    {
        if (ViewState["vsBuyequipment"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];



            foreach (GridViewRow row in GvAsset.Rows)
            {
                DataRow drContacts = dsContacts.Tables["dsAddListTable"].NewRow();

                CheckBox chklist = (CheckBox)row.Cells[0].FindControl("chklist");
                Label lbu0_asidx = (Label)row.Cells[0].FindControl("lbu0_asidx");

                if (chklist.Checked)
                {

                    drContacts["u0_asidx"] = int.Parse(lbu0_asidx.Text);
                    drContacts["type_asset"] = 1;

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกทรัพย์สินในการส่งคืนอุปกรณ์สำนักงาน');", true);
                }
                dsContacts.Tables["dsAddListTable"].Rows.Add(drContacts);
            }



            foreach (GridViewRow row_ in GvHolder.Rows)
            {
                DataRow drContacts_ = dsContacts.Tables["dsAddListTable"].NewRow();

                CheckBox chklist = (CheckBox)row_.Cells[0].FindControl("chklist");
                Label lbu0_didx = (Label)row_.Cells[0].FindControl("lbu0_didx");

                if (chklist.Checked)
                {
                    drContacts_["u0_asidx"] = int.Parse(lbu0_didx.Text);
                    drContacts_["type_asset"] = 2;

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกทรัพย์สินในการส่งคืนอุปกรณ์คอมพิวเตอร์');", true);
                }
                dsContacts.Tables["dsAddListTable"].Rows.Add(drContacts_);
            }

            ViewState["vsBuyequipment"] = dsContacts;

        }

    }

    protected void SetViewState_Exchange()
    {

        DataSet dsPersonList = new DataSet();
        dsPersonList.Tables.Add("dsAddListTable_exchange");

        dsPersonList.Tables["dsAddListTable_exchange"].Columns.Add("date_exchange", typeof(String));
        dsPersonList.Tables["dsAddListTable_exchange"].Columns.Add("type_jobidx", typeof(int));
        dsPersonList.Tables["dsAddListTable_exchange"].Columns.Add("type_job", typeof(String));
        dsPersonList.Tables["dsAddListTable_exchange"].Columns.Add("OrgIDX_exchange", typeof(int));
        dsPersonList.Tables["dsAddListTable_exchange"].Columns.Add("OrgNameTH", typeof(String));
        dsPersonList.Tables["dsAddListTable_exchange"].Columns.Add("RDeptIDX_exchange", typeof(int));
        dsPersonList.Tables["dsAddListTable_exchange"].Columns.Add("DeptNameTH", typeof(String));
        dsPersonList.Tables["dsAddListTable_exchange"].Columns.Add("RSecIDX_exchange", typeof(int));
        dsPersonList.Tables["dsAddListTable_exchange"].Columns.Add("SecNameTH", typeof(String));
        dsPersonList.Tables["dsAddListTable_exchange"].Columns.Add("detail_exchange", typeof(String));

        ViewState["vsBuyequipment_exchange"] = dsPersonList;

    }

    protected void setAddList_Exchange(GridView gvName, string date, int typejob, int orgidx, string orgname, int rdeptidx, string deptname, int rsecidx, string secname, string detail)
    {

        if (ViewState["vsBuyequipment_exchange"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsBuyequipment_exchange"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable_exchange"].Rows)
            {

                if (dr["date_exchange"].ToString() == date &&
                   int.Parse(dr["type_jobidx"].ToString()) == typejob &&
                    int.Parse(dr["OrgIDX_exchange"].ToString()) == orgidx &&
                     int.Parse(dr["RDeptIDX_exchange"].ToString()) == rdeptidx &&
                     int.Parse(dr["RSecIDX_exchange"].ToString()) == rsecidx &&
                       dr["detail_exchange"].ToString() == detail)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }

                setGridData(gvName, (DataSet)ViewState["vsBuyequipment_exchange"]);

            }

            DataRow drContacts = dsContacts.Tables["dsAddListTable_exchange"].NewRow();


            drContacts["date_exchange"] = date;
            drContacts["type_jobidx"] = typejob;
            drContacts["OrgIDX_exchange"] = orgidx;
            drContacts["RDeptIDX_exchange"] = rdeptidx;
            drContacts["RSecIDX_exchange"] = rsecidx;
            drContacts["detail_exchange"] = detail;

            if (typejob == 1)
            {
                drContacts["type_job"] = "ทุกหน่วยงาน";
                drContacts["OrgNameTH"] = "ทุกองค์กร";
                drContacts["DeptNameTH"] = "ทุกฝ่าย";
                drContacts["SecNameTH"] = "ทุกหน่วยงาน";
            }
            else
            {
                drContacts["type_job"] = "เฉพาะหน่วยงาน";

            }

            if (orgidx == 0)
            {
                drContacts["OrgNameTH"] = "ทุกองค์กร";
            }
            else
            {
                drContacts["OrgNameTH"] = orgname;
            }

            if (rdeptidx == 0)
            {
                drContacts["DeptNameTH"] = "ทุกฝ่าย";
            }
            else
            {
                drContacts["DeptNameTH"] = deptname;
            }
            if (rsecidx == 0)
            {
                drContacts["SecNameTH"] = "ทุกแผนก";
            }
            else
            {
                drContacts["SecNameTH"] = secname;
            }


            dsContacts.Tables["dsAddListTable_exchange"].Rows.Add(drContacts);
            ViewState["vsBuyequipment_exchange"] = dsContacts;
            // GvReportAdd.Visible = true;
            setGridData(gvName, dsContacts.Tables["dsAddListTable_exchange"]);
            gvName.Visible = true;

        }

    }

    protected void CleardataSetExchangeFormList(GridView GvName)
    {
        ViewState["vsBuyequipment_exchange"] = null;
        GvName.DataSource = ViewState["vsBuyequipment_exchange"];
        GvName.DataBind();
        SetViewState_Exchange();
    }


    #endregion

    #region FormView

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvDetailUser":
                FormView FvDetailUser = (FormView)ViewInsert.FindControl("FvDetailUser");

                if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();
                }
                break;
            case "Fvdetailusercreate":
                FormView Fvdetailusercreate = (FormView)ViewDetail.FindControl("Fvdetailusercreate");

                if (Fvdetailusercreate.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)Fvdetailusercreate.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)Fvdetailusercreate.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)Fvdetailusercreate.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)Fvdetailusercreate.FindControl("txtsec"));
                    var txtpos = ((TextBox)Fvdetailusercreate.FindControl("txtpos"));
                    var txtemail = ((TextBox)Fvdetailusercreate.FindControl("txtemail"));
                    var txttel = ((TextBox)Fvdetailusercreate.FindControl("txttel"));
                    var txtorg = ((TextBox)Fvdetailusercreate.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode_create"].ToString();
                    txtrequesname.Text = ViewState["FullName_create"].ToString();
                    txtorg.Text = ViewState["Org_name_create"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name_create"].ToString();
                    txtsec.Text = ViewState["Secname_create"].ToString();
                    txtpos.Text = ViewState["Positname_create"].ToString();
                    txttel.Text = ViewState["Tel_create"].ToString();
                    txtemail.Text = ViewState["Email_create"].ToString();
                }
                break;

            case "fvinsert_detail":
                FormView fvinsert_detail = (FormView)ViewDetail.FindControl("fvinsert_detail");

                if (fvinsert_detail.CurrentMode == FormViewMode.Edit)
                {
                    var rdochoice_detail = ((RadioButtonList)fvinsert_detail.FindControl("rdochoice_detail"));
                    var lblm0_rsidx = ((TextBox)fvinsert_detail.FindControl("lblm0_rsidx"));
                    var divrdoreason_detail = (Control)fvinsert_detail.FindControl("divrdoreason_detail");
                    //var ddlpermission_detail = (DropDownList)fvinsert_detail.FindControl("ddlpermission_detail");

                    select_reasonexit(rdochoice_detail);
                    rdochoice_detail.SelectedValue = lblm0_rsidx.Text;

                    if (rdochoice_detail.SelectedValue == "5")
                    {
                        divrdoreason_detail.Visible = true;
                    }
                    else
                    {
                        divrdoreason_detail.Visible = false;
                    }

                    //ddlpermission_detail.SelectedValue = ViewState["peridx_create"].ToString();
                }

                break;
        }
    }
    #endregion

    #region RowDataBound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvHolder":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvHolder.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lbu0_didx = ((Label)e.Row.FindControl("lbu0_didx"));
                        var btnsoftware = ((Label)e.Row.FindControl("btnsoftware"));
                        int i = 0;
                        string enter = "\n";

                        _dtswl = new data_softwarelicense_devices();
                        _dtswl.bind_softwarelicense_list = new bind_softwarelicense_detail[1];
                        bind_softwarelicense_detail _dtswldetail = new bind_softwarelicense_detail();
                        _dtswldetail.u0_didx = int.Parse(lbu0_didx.Text);

                        _dtswl.bind_softwarelicense_list[0] = _dtswldetail;
                        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtswl));

                        _dtswl = callServiceSoftwareLicenseDevices(_urlGetDetailSoftwareProfile, _dtswl);

                        if (_dtswl.return_code.ToString() == "0")
                        {

                            foreach (var rows in _dtswl.bind_softwarelicense_list)
                            {
                                e.Row.ToolTip += _dtswl.bind_softwarelicense_list[i].software_name + " " + enter;
                                i++;

                            }

                            btnsoftware.Visible = true;
                        }
                        else
                        {

                            btnsoftware.Visible = false;
                        }

                    }
                }

                break;
            case "GvTypeAns":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvTypeAns.EditIndex != e.Row.RowIndex)
                    {
                        Label lblm0_taidx = (Label)e.Row.Cells[0].FindControl("lblm0_taidx");
                        GridView GvQuestion = (GridView)e.Row.Cells[0].FindControl("GvQuestion");
                        ViewState["m0_taidx_"] = lblm0_taidx.Text;
                        select_getquestion(GvQuestion, int.Parse(lblm0_taidx.Text), int.Parse(ViewState["m0_toidx"].ToString()));

                    }
                }
                break;

            case "GvQuestion":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblm0_taidx = (Label)e.Row.Cells[0].FindControl("lblm0_taidx");
                    Label lblm0_tqidx = (Label)e.Row.Cells[0].FindControl("lblm0_tqidx");
                    Label lblm0_quidx = (Label)e.Row.Cells[1].FindControl("lblm0_quidx");
                    Panel panel_choice_rdo = (Panel)e.Row.Cells[1].FindControl("panel_choice_rdo");
                    Panel panel_choice_chk = (Panel)e.Row.Cells[1].FindControl("panel_choice_chk");
                    Panel panel_comment = (Panel)e.Row.Cells[1].FindControl("panel_comment");
                    RadioButtonList rdochoice_exit = (RadioButtonList)e.Row.Cells[1].FindControl("rdochoice_exit");
                    CheckBoxList chkchoice_exit = (CheckBoxList)e.Row.Cells[1].FindControl("chkchoice_exit");

                    int b = 0;
                    _dtresign = new data_resignation();

                    _dtresign.Boxm0_Question = new M0_Question[1];
                    M0_Question _qustion = new M0_Question();

                    _qustion.m0_quidx = int.Parse(lblm0_quidx.Text);
                    _qustion.m0_taidx = int.Parse(ViewState["m0_taidx_"].ToString());

                    _dtresign.Boxm0_Question[0] = _qustion;
                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

                    _dtresign = callServicePostResignation(_urlSelect_SubQuestion_Resignation, _dtresign);

                    if (ViewState["m0_taidx_"].ToString() == "1")
                    {

                        for (int ex = 0; ex < _dtresign.Boxm0_Question.Count(); ex++)
                        {

                            ////chkchoice.Items.Add(new ListItem(String.Format("{"+ ex + "'}<input id=\"txtchk{" +ex+"}\" name=\"txtchk{" + ex +"}\" / >" )+ _dtresign.Boxm0_Question[ex].choice_name, _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));

                            //if (_dtresign.Boxm0_Question[ex].m0_chidx.ToString() == "2")
                            //{
                            // ver 0  ยัง bind ไม่ได้เรียกได้จากด้านหน้าอย่างเดียว
                            //chkchoice_exit.Items.Add(new ListItem(String.Format(Request.Form["txtchk"] + "") + _dtresign.Boxm0_Question[ex].choice_name, _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));
                            //rdochoice_exit.Items.Add(new ListItem(String.Format(Request.Form["txtrdo"] + "") + _dtresign.Boxm0_Question[ex].choice_name, _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));


                            //chkchoice_exit.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name + String.Format("<input name =\"txtchk\" type=\"text\" runat=\"server\"  class=\"form-control\"/>"), _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));


                            //rdochoice_exit.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name + String.Format(" <input name =\"txtrdo\" type=\"text\" runat=\"server\" class=\"form-control \"/>"), _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));

                            //chkchoice_exit.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name + String.Format("<textarea  id =\"txtchk" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" name =\"txtchk" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" runat=\"server\"  class=\"form-control\" row=\"5\" cols=\"50\">" + "</textarea>"), _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));


                            //}
                            //txtSingleDate.Text = "sdfsdf";
                            //}
                            //else
                            //{
                            //    chkchoice_exit.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name, _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));
                            //    rdochoice_exit.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name, _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));
                            //}

                            //rdochoice_exit.Items.Add(new ListItem("Other - Specify<input name=\"OtherWorkPlace\" type=\"text\" value=\"test\" onblur=\"document.getElementById('" & rbOtherText.ClientID & "').value = this.value;\" />", "-1"));

                            if (_dtresign.Boxm0_Question[ex].m0_chidx.ToString() == "2")
                            {
                                chkchoice_exit.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name + String.Format("<textarea  id =\"txtchk" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" name =\"txtchk" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" runat=\"server\"  class=\"form-control\" row=\"5\" cols=\"50\">" + "</textarea>"), _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));

                                rdochoice_exit.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name + String.Format("<textarea  id =\"txtrdo" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" name =\"txtrdo" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" runat=\"server\"  class=\"form-control\" row=\"5\" cols=\"50\">" + "</textarea>"), _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));

                            }
                            else
                            {
                                //chkchoice_exit.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name + String.Format("<textarea  id =\"txtchk\" style=\"display:none\" name =\"txtchk\" runat=\"server\"  class=\"form-control\" row=\"5\" cols=\"50\">" + "</textarea>"), _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));

                                chkchoice_exit.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name + String.Format("<textarea  id =\"txtchk" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" name =\"txtchk" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" style=\"display:none\" runat=\"server\"  class=\"form-control\" row=\"5\" cols=\"50\">" + "</textarea>"), _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));


                                rdochoice_exit.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name + String.Format("<textarea  id =\"txtrdo" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" name =\"txtrdo" + _dtresign.Boxm0_Question[ex].m1_quidx.ToString() + b + "\" style=\"display:none\" runat=\"server\"  class=\"form-control\" row=\"5\" cols=\"50\">" + "</textarea>"), _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));
                            }


                            if (lblm0_tqidx.Text == "1")
                            {
                                panel_choice_chk.Visible = true;
                                panel_choice_rdo.Visible = false;
                            }
                            else if (lblm0_tqidx.Text == "2")
                            {
                                panel_choice_chk.Visible = false;
                                panel_choice_rdo.Visible = true;
                            }
                        }
                        b++;
                        panel_comment.Visible = false;
                    }
                    else if (ViewState["m0_taidx_"].ToString() == "2")
                    {
                        panel_choice_rdo.Visible = false;
                        panel_choice_chk.Visible = false;
                        panel_comment.Visible = true;
                    }

                }
                break;

            case "GvListEX":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbm0_tidx = (Label)e.Row.Cells[0].FindControl("lbm0_tidx");
                    Label lbltype_menu = (Label)e.Row.Cells[0].FindControl("lbltype_menu");
                    Label lbdate_resign = (Label)e.Row.Cells[2].FindControl("lbdate_resign");

                    Literal ltunidx = (Literal)e.Row.Cells[3].FindControl("ltunidx");
                    Literal ltstaidx = (Literal)e.Row.Cells[3].FindControl("ltstaidx");
                    Label lblstasus = (Label)e.Row.Cells[3].FindControl("lblstasus");
                    LinkButton btnexchange = (LinkButton)e.Row.Cells[4].FindControl("btnexchange");
                    LinkButton btnremove = (LinkButton)e.Row.Cells[4].FindControl("btnremove");
                    Label lblunidx_ex = (Label)e.Row.Cells[0].FindControl("lblunidx_ex");
                    Label lblstasus_ex = (Label)e.Row.Cells[3].FindControl("lblstasus_ex");
                    Control divstatus_ex = (Control)e.Row.Cells[3].FindControl("divstatus_ex");


                    var parameterDate = DateTime.ParseExact(lbdate_resign.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    var todaysDate = DateTime.Today;



                    if (GvListEX.EditIndex != e.Row.RowIndex)
                    {
                        if (lblunidx_ex.Text == "0")
                        {
                            divstatus_ex.Visible = false;
                        }
                        else
                        {
                            divstatus_ex.Visible = true;
                        }

                        switch (int.Parse(lbm0_tidx.Text))
                        {
                            case 1:
                                lbltype_menu.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff0066");
                                break;
                            case 2:
                                lbltype_menu.ForeColor = System.Drawing.ColorTranslator.FromHtml("#009900");
                                break;
                            case 3:
                                lbltype_menu.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff8000");
                                break;

                        }

                        switch (int.Parse(ltunidx.Text))
                        {
                            case 1:
                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff8c1a");
                                btnexchange.Visible = false;

                                if (lbm0_tidx.Text != "3")
                                {
                                    if (parameterDate > todaysDate)
                                    {
                                        btnremove.Visible = true;
                                    }
                                    else
                                    {
                                        btnremove.Visible = false;
                                    }

                                }
                                else
                                {
                                    btnremove.Visible = false;
                                }
                                break;
                            case 2:

                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#cc66ff");
                                btnexchange.Visible = false;

                                if (lbm0_tidx.Text != "3")
                                {
                                    if (parameterDate > todaysDate)
                                    {
                                        btnremove.Visible = true;
                                    }
                                    else
                                    {
                                        btnremove.Visible = false;
                                    }

                                }
                                else
                                {
                                    btnremove.Visible = false;
                                }

                                break;
                            case 3:

                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#996600");
                                btnexchange.Visible = false;

                                if (lbm0_tidx.Text != "3")
                                {
                                    if (parameterDate > todaysDate)
                                    {
                                        btnremove.Visible = true;
                                    }
                                    else
                                    {
                                        btnremove.Visible = false;
                                    }
                                }
                                else
                                {
                                    btnremove.Visible = false;
                                }
                                break;
                            case 4:

                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#990033");
                                btnexchange.Visible = false;

                                if (lbm0_tidx.Text != "3")
                                {
                                    if (parameterDate > todaysDate)
                                    {
                                        btnremove.Visible = true;
                                    }
                                    else
                                    {
                                        btnremove.Visible = false;
                                    }
                                }
                                else
                                {
                                    btnremove.Visible = false;
                                }
                                break;
                            case 5:
                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff6666");
                                btnexchange.Visible = false;
                                btnremove.Visible = false;
                                break;
                            case 6:

                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#33adff");
                                btnexchange.Visible = false;
                                btnremove.Visible = false;
                                break;
                            case 7:

                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#b3b300");
                                btnexchange.Visible = false;
                                btnremove.Visible = false;

                                break;
                            case 8:
                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff3300");
                                btnexchange.Visible = false;
                                btnremove.Visible = false;
                                break;
                            case 9:
                                btnremove.Visible = false;
                                if (ltstaidx.Text == "3" || ltstaidx.Text == "8")
                                {
                                    lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff0000");
                                    btnexchange.Visible = false;
                                }
                                else
                                {
                                    lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                                    if (lbm0_tidx.Text == "1")
                                    {
                                        btnexchange.Visible = true;
                                        divstatus_ex.Visible = true;

                                        if (lblunidx_ex.Text == "0")
                                        {
                                            lblstasus_ex.Text = "รอผู้สร้างดำเนินการส่งมอบงาน";
                                            lblstasus_ex.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff0000");
                                        }

                                    }
                                    else
                                    {
                                        btnexchange.Visible = false;
                                        divstatus_ex.Visible = false;
                                    }
                                }


                                break;
                        }

                        switch (int.Parse(lblunidx_ex.Text))
                        {

                            case 2:
                                lblstasus_ex.ForeColor = System.Drawing.ColorTranslator.FromHtml("#cc66ff");
                                break;

                            case 3:
                                lblstasus_ex.ForeColor = System.Drawing.ColorTranslator.FromHtml("#996600");
                                break;
                            case 9:
                                lblstasus_ex.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                                break;
                        }




                    }
                }
                break;
            case "GvHolder_detail":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvHolder_detail.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lbu0_didx = ((Label)e.Row.FindControl("lbu0_didx"));
                        var btnsoftware = ((Label)e.Row.FindControl("btnsoftware"));
                        int i = 0;
                        string enter = "\n";

                        _dtswl = new data_softwarelicense_devices();
                        _dtswl.bind_softwarelicense_list = new bind_softwarelicense_detail[1];
                        bind_softwarelicense_detail _dtswldetail = new bind_softwarelicense_detail();
                        _dtswldetail.u0_didx = int.Parse(lbu0_didx.Text);

                        _dtswl.bind_softwarelicense_list[0] = _dtswldetail;
                        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtswl));

                        _dtswl = callServiceSoftwareLicenseDevices(_urlGetDetailSoftwareProfile, _dtswl);

                        if (_dtswl.return_code.ToString() == "0")
                        {

                            foreach (var rows in _dtswl.bind_softwarelicense_list)
                            {
                                e.Row.ToolTip += _dtswl.bind_softwarelicense_list[i].software_name + " " + enter;
                                i++;

                            }

                            btnsoftware.Visible = true;
                        }
                        else
                        {

                            btnsoftware.Visible = false;
                        }

                    }
                }

                break;

            case "gvFile":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;

            case "GvTypeAns_detail":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvTypeAns_detail.EditIndex != e.Row.RowIndex)
                    {
                        Label lblm0_taidx = (Label)e.Row.Cells[0].FindControl("lblm0_taidx");
                        GridView GvQuestion_detail = (GridView)e.Row.Cells[0].FindControl("GvQuestion_detail");
                        ViewState["m0_taidx_"] = lblm0_taidx.Text;
                        select_getquestion_answer(GvQuestion_detail, int.Parse(lblm0_taidx.Text), int.Parse(ViewState["m0_toidx"].ToString()), int.Parse(ViewState["u0_docidx"].ToString()));

                    }
                }
                break;

            case "GvQuestion_detail":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblm0_taidx = (Label)e.Row.Cells[0].FindControl("lblm0_taidx");
                    Label lblm0_tqidx = (Label)e.Row.Cells[0].FindControl("lblm0_tqidx");
                    Label lblanswer = (Label)e.Row.Cells[0].FindControl("lblanswer");
                    Label lblremark = (Label)e.Row.Cells[0].FindControl("lblremark");
                    Label lblm0_quidx = (Label)e.Row.Cells[1].FindControl("lblm0_quidx");
                    Panel panel_choice_rdo = (Panel)e.Row.Cells[1].FindControl("panel_choice_rdo");
                    Panel panel_choice_chk = (Panel)e.Row.Cells[1].FindControl("panel_choice_chk");
                    Panel panel_comment = (Panel)e.Row.Cells[1].FindControl("panel_comment");
                    RadioButtonList rdochoice_exit = (RadioButtonList)e.Row.Cells[1].FindControl("rdochoice_exit");
                    CheckBoxList chkchoice_exit = (CheckBoxList)e.Row.Cells[1].FindControl("chkchoice_exit");


                    _dtresign = new data_resignation();

                    _dtresign.Boxm0_Question = new M0_Question[1];
                    M0_Question _qustion = new M0_Question();

                    _qustion.m0_quidx = int.Parse(lblm0_quidx.Text);
                    _qustion.m0_taidx = int.Parse(ViewState["m0_taidx_"].ToString());

                    _dtresign.Boxm0_Question[0] = _qustion;
                    //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtresign));

                    _dtresign = callServicePostResignation(_urlSelect_SubQuestion_Resignation, _dtresign);

                    if (ViewState["m0_taidx_"].ToString() == "1")
                    {

                        for (int ex = 0; ex < _dtresign.Boxm0_Question.Count(); ex++)
                        {
                            if (_dtresign.Boxm0_Question[ex].m0_chidx.ToString() == "2")
                            {
                                // ver 0  ยัง bind ไม่ได้เรียกได้จากด้านหน้าอย่างเดียว
                                //chkchoice_exit.Items.Add(new ListItem(String.Format(Request.Form["txtchk"] + "") + _dtresign.Boxm0_Question[ex].choice_name, _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));
                                //rdochoice_exit.Items.Add(new ListItem(String.Format(Request.Form["txtrdo"] + "") + _dtresign.Boxm0_Question[ex].choice_name, _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));


                                //chkchoice_exit.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name + String.Format("<input name =\"txtchk\" type=\"text\" runat=\"server\"  class=\"form-control\" value=\" " + lblremark.Text + "\" />"), _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));


                                //rdochoice_exit.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name + String.Format(" <input name =\"txtrdo\" type=\"text\" runat=\"server\" class=\"form-control\" value=\" " + lblremark.Text + "\" />"), _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));
                                chkchoice_exit.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name + String.Format("<textarea  id =\"txtchk\" runat=\"server\"  class=\"form-control\" row=\"5\" cols=\"50\">" + lblremark.Text + "</textarea>"), _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));

                                rdochoice_exit.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name + String.Format("<textarea  id =\"txtrdo\" runat=\"server\"  class=\"form-control\" row=\"5\" cols=\"50\">" + lblremark.Text + "</textarea>"), _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));

                            }
                            else
                            {
                                chkchoice_exit.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name, _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));
                                rdochoice_exit.Items.Add(new ListItem(_dtresign.Boxm0_Question[ex].choice_name, _dtresign.Boxm0_Question[ex].m1_quidx.ToString()));
                            }

                            //rdochoice_exit.Items.Add(new ListItem("Other - Specify<input name=\"OtherWorkPlace\" type=\"text\" value=\"test\" onblur=\"document.getElementById('" & rbOtherText.ClientID & "').value = this.value;\" />", "-1"));

                        }

                        if (lblm0_tqidx.Text == "1")
                        {
                            panel_choice_chk.Visible = true;
                            panel_choice_rdo.Visible = false;

                            string[] ToId = lblanswer.Text.Split(',');

                            foreach (ListItem li in chkchoice_exit.Items)
                            {
                                foreach (string To_check in ToId)
                                {

                                    if (li.Value == To_check)
                                    {
                                        li.Selected = true;

                                        break;
                                    }
                                    else
                                    {
                                        li.Selected = false;
                                    }
                                }
                            }
                        }
                        else if (lblm0_tqidx.Text == "2")
                        {
                            panel_choice_chk.Visible = false;
                            panel_choice_rdo.Visible = true;
                            rdochoice_exit.SelectedValue = lblanswer.Text;


                        }

                        panel_comment.Visible = false;
                    }
                    else if (ViewState["m0_taidx_"].ToString() == "2")
                    {
                        panel_choice_rdo.Visible = false;
                        panel_choice_chk.Visible = false;
                        panel_comment.Visible = true;
                    }

                }
                break;

            case "GvApprove":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbm0_tidx = (Label)e.Row.Cells[0].FindControl("lbm0_tidx");
                    Label lbltype_menu = (Label)e.Row.Cells[0].FindControl("lbltype_menu");
                    Label lblunidx_ex = (Label)e.Row.Cells[0].FindControl("lblunidx_ex");
                    Literal ltunidx = (Literal)e.Row.Cells[3].FindControl("ltunidx");
                    Label lblstasus = (Label)e.Row.Cells[3].FindControl("lblstasus");
                    LinkButton btnexchange = (LinkButton)e.Row.Cells[4].FindControl("btnexchange");
                    Literal ltstaidx = (Literal)e.Row.Cells[3].FindControl("ltstaidx");
                    Control divstatus_ex = (Control)e.Row.Cells[3].FindControl("divstatus_ex");
                    Label lblstasus_ex = (Label)e.Row.Cells[3].FindControl("lblstasus_ex");


                    if (GvApprove.EditIndex != e.Row.RowIndex)
                    {
                        if (lblunidx_ex.Text == "0")
                        {
                            divstatus_ex.Visible = false;
                        }
                        else
                        {
                            divstatus_ex.Visible = true;
                        }

                        switch (int.Parse(lbm0_tidx.Text))
                        {
                            case 1:
                                lbltype_menu.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff0066");
                                break;
                            case 2:
                                lbltype_menu.ForeColor = System.Drawing.ColorTranslator.FromHtml("#009900");
                                break;
                            case 3:
                                lbltype_menu.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff8000");
                                break;

                        }
                        switch (int.Parse(ltunidx.Text))
                        {
                            case 1:
                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff8c1a");
                                btnexchange.Visible = false;

                                break;
                            case 2:
                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#cc66ff");
                                btnexchange.Visible = false;

                                break;
                            case 3:
                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#996600");
                                btnexchange.Visible = false;

                                break;
                            case 4:
                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#990033");
                                btnexchange.Visible = false;

                                break;
                            case 5:
                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff6666");
                                btnexchange.Visible = false;

                                break;
                            case 6:
                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#33adff");
                                btnexchange.Visible = false;

                                break;
                            case 7:
                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#b3b300");
                                btnexchange.Visible = false;

                                break;
                            case 8:
                                lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff3300");
                                btnexchange.Visible = false;

                                break;
                            case 9:
                                if (ltstaidx.Text == "3")
                                {
                                    lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff0000");
                                    btnexchange.Visible = false;
                                }
                                else
                                {
                                    lblstasus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                                    btnexchange.Visible = true;

                                    if (lblunidx_ex.Text == "0")
                                    {
                                        lblstasus_ex.Text = "รอผู้สร้างดำเนินการส่งมอบงาน";
                                        lblstasus_ex.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff0000");
                                    }
                                    else
                                    {

                                    }
                                }

                                break;
                        }

                        switch (int.Parse(lblunidx_ex.Text))
                        {

                            case 2:
                                lblstasus_ex.ForeColor = System.Drawing.ColorTranslator.FromHtml("#cc66ff");
                                break;

                            case 3:
                                lblstasus_ex.ForeColor = System.Drawing.ColorTranslator.FromHtml("#996600");
                                break;
                            case 9:
                                lblstasus_ex.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                                break;
                        }
                    }
                }
                break;

            case "GvHolder_edit":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvHolder_edit.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lbu0_didx = ((Label)e.Row.FindControl("lbu0_didx"));
                        var btnsoftware = ((Label)e.Row.FindControl("btnsoftware"));
                        int i = 0;
                        string enter = "\n";

                        _dtswl = new data_softwarelicense_devices();
                        _dtswl.bind_softwarelicense_list = new bind_softwarelicense_detail[1];
                        bind_softwarelicense_detail _dtswldetail = new bind_softwarelicense_detail();
                        _dtswldetail.u0_didx = int.Parse(lbu0_didx.Text);

                        _dtswl.bind_softwarelicense_list[0] = _dtswldetail;
                        //  text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtswl));

                        _dtswl = callServiceSoftwareLicenseDevices(_urlGetDetailSoftwareProfile, _dtswl);

                        if (_dtswl.return_code.ToString() == "0")
                        {

                            foreach (var rows in _dtswl.bind_softwarelicense_list)
                            {
                                e.Row.ToolTip += _dtswl.bind_softwarelicense_list[i].software_name + " " + enter;
                                i++;

                            }

                            btnsoftware.Visible = true;
                        }
                        else
                        {

                            btnsoftware.Visible = false;
                        }

                    }
                }

                break;
        }

    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "bnDeleteList":
                    GridView GvExchange = (GridView)fvexchange.FindControl("GvExchange");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    //DropDownList ddltypeans = (DropDownList)fvexchange.FindControl("ddltypeans");
                    //Control div_save = (Control)fvexchange.FindControl("div_save");

                    int rowselect = rowSelect.RowIndex;

                    DataSet dsContacts = (DataSet)ViewState["vsBuyequipment_exchange"];
                    dsContacts.Tables["dsAddListTable_exchange"].Rows[rowselect].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(GvExchange, dsContacts.Tables["dsAddListTable_exchange"]);


                    if (dsContacts.Tables["dsAddListTable_exchange"].Rows.Count < 1)
                    {
                        GvExchange.Visible = false;
                        //div_save.Visible = false;
                    }


                    break;
            }
        }
    }

    #endregion

    #region Directories_File URL


    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt1.Rows.Add(file.Name);
                    dt1.Rows[i][1] = f[0];
                    i++;
                }
            }

            GridView gvFile = (GridView)fvinsert_detail.FindControl("gvFile");
            // Panel gvFileLo1 = (Panel)FvDetailUser.FindControl("gvFileLo1");

            if (dt1.Rows.Count > 0)
            {

                ds1.Tables.Add(dt1);
                // gvFileLo1.Visible = true;
                gvFile.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
                gvFile.DataBind();
                ds1.Dispose();
            }
            else
            {

                gvFile.DataSource = null;
                gvFile.DataBind();

            }
        }
        catch
        {
            checkfile = "11";
        }
    }

    #endregion

    #region SelectedIndexChanged
    protected void SelectedIndexChanged(object sender, EventArgs e)
    {
        if (sender is RadioButtonList)
        {
            RadioButtonList rdoName = (RadioButtonList)sender;

            switch (rdoName.ID)
            {
                case "rdochoice":
                    RadioButtonList rdochoice = (RadioButtonList)fv_insert.FindControl("rdochoice");
                    Control divrdoreason = (Control)fv_insert.FindControl("divrdoreason");

                    if (rdochoice.SelectedValue == "5")
                    {
                        divrdoreason.Visible = true;
                    }
                    else
                    {
                        divrdoreason.Visible = false;
                    }

                    break;

                case "rdotype":
                    RadioButtonList rdotype = (RadioButtonList)fvexchange.FindControl("rdotype");
                    Control div_exchange = (Control)fvexchange.FindControl("div_exchange");
                    DropDownList ddlorg_exchange = (DropDownList)fvexchange.FindControl("ddlorg_exchange");
                    DropDownList ddlrdept_exchange = (DropDownList)fvexchange.FindControl("ddlrdept_exchange");
                    DropDownList ddlrsec_exchange = (DropDownList)fvexchange.FindControl("ddlrsec_exchange");

                    if (rdotype.SelectedValue == "2")
                    {
                        div_exchange.Visible = true;
                        getOrganizationList(ddlorg_exchange);
                        ddlrdept_exchange.SelectedValue = "0";
                        ddlrsec_exchange.SelectedValue = "0";
                    }
                    else
                    {
                        div_exchange.Visible = false;
                    }
                    break;

            }
        }
        else if (sender is DropDownList)
        {
            DropDownList ddlName = (DropDownList)sender;

            DropDownList ddlorg_exchange = (DropDownList)fvexchange.FindControl("ddlorg_exchange");
            DropDownList ddlrdept_exchange = (DropDownList)fvexchange.FindControl("ddlrdept_exchange");
            DropDownList ddlrsec_exchange = (DropDownList)fvexchange.FindControl("ddlrsec_exchange");

            switch (ddlName.ID)
            {
                case "ddlorg_exchange":
                    getDepartmentList(ddlrdept_exchange, int.Parse(ddlorg_exchange.SelectedValue));

                    break;
                case "ddlrdept_exchange":
                    getSectionList(ddlrsec_exchange, int.Parse(ddlorg_exchange.SelectedValue), int.Parse(ddlrdept_exchange.SelectedValue));

                    break;


            }
        }
    }

    #endregion

    #region bind Node

    protected void setFormData()
    {

        HiddenField hfM0NodeIDX = (HiddenField)fvinsert_detail.FindControl("hfM0NodeIDX");
        //HiddenField hfM0NodeIDX_EX = (HiddenField)fvinsert_detail.FindControl("hfM0NodeIDX_EX");

        ViewState["m0_node"] = hfM0NodeIDX.Value;

        int m0_node = int.Parse(ViewState["m0_node"].ToString());


        switch (m0_node)
        {
            case 1: // สร้าง/แก้ไข

                setFormDataActor();

                break;


            case 2: // พิจารณาผลโดย manager

                setFormDataActor();

                break;


            case 3: // ดำเนินการโดยเจ้าหน้าที่SAP
            case 4: // ดำเนินการโดยเจ้าหน้าที่ IT
            case 5: // ดำเนินการโดยเจ้าหน้าที่ Google Apps
            case 6: // พิจารณาผลโดยเจ้าหน้าที่ SAP
            case 7: // พิจารณาผลโดยเจ้าหน้าที่ IT
            case 8: // พิจารณาผลโดยเจ้าหน้าที่ IT

                setFormDataActor();

                break;


            case 9: // จบการดำเนินการ

                setFormDataActor();
                setFormDataActor_Exchange();

                break;


        }

    }

    #endregion

    #region bind Actor

    protected void setFormDataActor()
    {

        HiddenField hfM0ActoreIDX = (HiddenField)fvinsert_detail.FindControl("hfM0ActoreIDX");
        HiddenField hfM0CEmpIDX = (HiddenField)fvinsert_detail.FindControl("hfM0CEmpIDX");
        HiddenField hfm0_tidx = (HiddenField)fvinsert_detail.FindControl("hfm0_tidx");
        TextBox dateexit_detail = (TextBox)fvinsert_detail.FindControl("dateexit_detail");
        TextBox txtremark_detail = (TextBox)fvinsert_detail.FindControl("txtremark_detail");
        //Control divbtn = (Control)fvinsert_detail.FindControl("divbtn");
        LinkButton btnAdddata = (LinkButton)fvinsert_detail.FindControl("btnAdddata");
        LinkButton AddCancel = (LinkButton)fvinsert_detail.FindControl("AddCancel");

        ViewState["m0_actor"] = hfM0ActoreIDX.Value;
        int m0_actor = int.Parse(hfM0ActoreIDX.Value);

        //txt.Text = ViewState["m0_node"].ToString() + "," + ViewState["m0_actor"].ToString();

        switch (m0_actor)
        {

            case 1: // ผู้สร้าง

                if (ViewState["m0_node"].ToString() == "1" && m0_actor == 1 && hfM0CEmpIDX.Value == ViewState["EmpIDX"].ToString())
                {
                    //dateexit_detail.Enabled = true;
                    //txtremark_detail.Enabled = true;
                    btnAdddata.Visible = true;

                    if (ViewState["arg1_detail"].ToString() == "3")
                    {
                        dateexit_detail.Enabled = false;
                        txtremark_detail.Enabled = false;

                    }
                    else
                    {
                        dateexit_detail.Enabled = true;
                        txtremark_detail.Enabled = true;

                    }
                }
                else
                {
                    dateexit_detail.Enabled = false;
                    txtremark_detail.Enabled = false;
                    btnAdddata.Visible = false;

                    AddCancel.Visible = true;
                }
                panel_approveholder.Visible = false;
                break;

            case 2: // Manager

                if (ViewState["m0_node"].ToString() == "2" && m0_actor == 2 && hfM0CEmpIDX.Value != ViewState["EmpIDX"].ToString())
                {
                    panel_approveholder.Visible = true;
                    if (hfm0_tidx.Value == "3")
                    {
                        setdefaultddlapprove_cancel();
                    }
                }
                else
                {
                    panel_approveholder.Visible = false;
                    AddCancel.Visible = true;
                }

                if (ViewState["arg1_detail"].ToString() == "3")
                {
                    btnAdddata.Visible = true;

                }
                else
                {
                    btnAdddata.Visible = false;
                }


                break;

            case 3: //Director
                if (ViewState["m0_node"].ToString() == "3" && m0_actor == 3 && hfM0CEmpIDX.Value != ViewState["EmpIDX"].ToString())
                {
                    panel_approveholder.Visible = true;
                    if (hfm0_tidx.Value == "3")
                    {
                        setdefaultddlapprove_cancel();
                    }
                }
                else
                {
                    panel_approveholder.Visible = false;
                    AddCancel.Visible = true;
                }


                if (ViewState["arg1_detail"].ToString() == "3")
                {
                    btnAdddata.Visible = true;
                }
                else
                {
                    btnAdddata.Visible = false;
                }
                break;
            case 4: // COO/CFO
                if (ViewState["m0_node"].ToString() == "4" && m0_actor == 4 && hfM0CEmpIDX.Value != ViewState["EmpIDX"].ToString())
                {
                    panel_approveholder.Visible = true;
                    if (hfm0_tidx.Value == "3")
                    {
                        setdefaultddlapprove_cancel();
                    }
                }
                else
                {
                    panel_approveholder.Visible = false;
                    AddCancel.Visible = true;
                }

                if (ViewState["arg1_detail"].ToString() == "3")
                {
                    btnAdddata.Visible = true;
                }
                else
                {
                    btnAdddata.Visible = false;
                }
                break;

            case 5: // HR Admin

                if (ViewState["m0_node"].ToString() == "5" && m0_actor == 5 && posidx_hradmin.Contains(int.Parse(ViewState["Pos_idx"].ToString())))//ViewState["Pos_idx"].ToString() == "5908")
                {
                    panel_approveholder.Visible = true;
                    if (hfm0_tidx.Value == "3")
                    {
                        setdefaultddlapprove_cancel();
                    }
                }
                else
                {
                    panel_approveholder.Visible = false;

                    AddCancel.Visible = true;
                }

                btnAdddata.Visible = false;
                break;

            case 6: // Director HR
                if (ViewState["m0_node"].ToString() == "6" && m0_actor == 6 && posidx_hrdirector.Contains(int.Parse(ViewState["Pos_idx"].ToString()))) // 8150
                {
                    panel_approveholder.Visible = true;
                    if (hfm0_tidx.Value == "3")
                    {
                        setdefaultddlapprove_cancel();
                    }
                }
                else
                {
                    panel_approveholder.Visible = false;

                    AddCancel.Visible = true;
                }

                btnAdddata.Visible = false;
                break;
            case 7: // MD
                if (ViewState["m0_node"].ToString() == "7" && m0_actor == 7 && ViewState["EmpIDX"].ToString() == "3640")
                {
                    panel_approveholder.Visible = true;
                    if (hfm0_tidx.Value == "3")
                    {
                        setdefaultddlapprove_cancel();
                    }
                }
                else
                {
                    panel_approveholder.Visible = false;


                    AddCancel.Visible = true;

                }

                btnAdddata.Visible = false;
                break;
            case 8: // CEO
                if (ViewState["m0_node"].ToString() == "8" && m0_actor == 8 && ViewState["EmpIDX"].ToString() == "3639")
                {
                    panel_approveholder.Visible = true;
                }
                else
                {
                    panel_approveholder.Visible = false;


                    AddCancel.Visible = true;

                }
                btnAdddata.Visible = false;
                break;


        }

    }

    protected void setFormDataActor_Exchange()
    {
        HiddenField hfM0NodeIDX_EX = (HiddenField)fvinsert_detail.FindControl("hfM0NodeIDX_EX");
        HiddenField hfM0ActoreIDX_EX = (HiddenField)fvinsert_detail.FindControl("hfM0ActoreIDX_EX");
        HiddenField hfM0StatusIDX_EX = (HiddenField)fvinsert_detail.FindControl("hfM0StatusIDX_EX");
        HiddenField hfM0CEmpIDX = (HiddenField)fvinsert_detail.FindControl("hfM0CEmpIDX");
        LinkButton AddCancel = (LinkButton)fvinsert_detail.FindControl("AddCancel");
        LinkButton btnAdddata = (LinkButton)fvinsert_detail.FindControl("btnAdddata");

        int m0_actor = int.Parse(hfM0ActoreIDX_EX.Value);

        switch (m0_actor)
        {
            case 1: // ผู้สร้าง

                break;

            case 2: // Header
                if (hfM0NodeIDX_EX.Value == "2" && m0_actor == 2 && hfM0CEmpIDX.Value != ViewState["EmpIDX"].ToString() && ViewState["arg1_detail"].ToString() == "2")
                {
                    panel_approveholder.Visible = true;

                    if (ViewState["m0_node"].ToString() == "9")
                    {
                        ddl_approve.AppendDataBoundItems = true;
                        ddl_approve.Items.Clear();
                        ddl_approve.Items.Add(new ListItem("เลือกสถานะอนุมัติ", "0"));
                        ddl_approve.Items.Add(new ListItem("รับทราบ", "2"));

                        ddl_approve.SelectedValue = "2";
                    }
                }
                else
                {
                    panel_approveholder.Visible = false;
                    AddCancel.Visible = true;
                }
                btnAdddata.Visible = false;
                break;

            case 3: // Director
                if (hfM0NodeIDX_EX.Value == "3" && m0_actor == 3 && hfM0CEmpIDX.Value != ViewState["EmpIDX"].ToString() && ViewState["arg1_detail"].ToString() == "2")
                {
                    panel_approveholder.Visible = true;

                    if (ViewState["m0_node"].ToString() == "9")
                    {
                        ddl_approve.AppendDataBoundItems = true;
                        ddl_approve.Items.Clear();
                        ddl_approve.Items.Add(new ListItem("เลือกสถานะอนุมัติ", "0"));
                        ddl_approve.Items.Add(new ListItem("รับทราบ", "2"));

                        ddl_approve.SelectedValue = "2";
                    }
                }
                else
                {
                    panel_approveholder.Visible = false;
                    AddCancel.Visible = true;
                }
                btnAdddata.Visible = false;
                break;
        }

    }


    #endregion

    #region SetDropDownlistApprove

    protected void setdefaultddlapprove_cancel()
    {
        ddl_approve.AppendDataBoundItems = true;
        ddl_approve.Items.Clear();
        ddl_approve.Items.Add(new ListItem("เลือกสถานะอนุมัติ", "0"));
        ddl_approve.Items.Add(new ListItem("อนุมัติ(ยกเลิก)", "7"));
        ddl_approve.Items.Add(new ListItem("ไม่อนุมัติ(ยกเลิก)", "8"));
    }

    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();
        CheckBox chkagree = (CheckBox)fv_insert.FindControl("chkagree");
        TextBox txtremark = (TextBox)fv_insert.FindControl("txtremark");
        TextBox dateexit = (TextBox)fv_insert.FindControl("dateexit");
        RadioButtonList rdochoice = (RadioButtonList)fv_insert.FindControl("rdochoice");
        TextBox txtrdo = (TextBox)fv_insert.FindControl("txtrdo");
        HiddenField hftype_jobgrade_flow = (HiddenField)fvinsert_detail.FindControl("hftype_jobgrade_flow");
        HiddenField hfm0_tidx = (HiddenField)fvinsert_detail.FindControl("hfm0_tidx");
        HiddenField hfM0ActoreIDX = (HiddenField)fvinsert_detail.FindControl("hfM0ActoreIDX");
        HiddenField hfM0NodeIDX = (HiddenField)fvinsert_detail.FindControl("hfM0NodeIDX");
        HiddenField hfM0StatusIDX = (HiddenField)fvinsert_detail.FindControl("hfM0StatusIDX");
        TextBox txtremark_detail = (TextBox)fvinsert_detail.FindControl("txtremark_detail");
        TextBox dateexit_detail = (TextBox)fvinsert_detail.FindControl("dateexit_detail");
        TextBox dateexchange = (TextBox)fvexchange.FindControl("dateexchange");
        RadioButtonList rdotype = (RadioButtonList)fvexchange.FindControl("rdotype");
        DropDownList ddlorg_exchange = (DropDownList)fvexchange.FindControl("ddlorg_exchange");
        DropDownList ddlrdept_exchange = (DropDownList)fvexchange.FindControl("ddlrdept_exchange");
        DropDownList ddlrsec_exchange = (DropDownList)fvexchange.FindControl("ddlrsec_exchange");
        TextBox txtremark_exchange = (TextBox)fvexchange.FindControl("txtremark_exchange");
        Control div_save = (Control)fvexchange.FindControl("div_save");
        HiddenField hfM0NodeIDX_EX = (HiddenField)fvinsert_detail.FindControl("hfM0NodeIDX_EX");
        HiddenField hfM0ActoreIDX_EX = (HiddenField)fvinsert_detail.FindControl("hfM0ActoreIDX_EX");
        HiddenField hfdocidx_ref = (HiddenField)fvinsert_detail.FindControl("hfdocidx_ref");


        switch (cmdName)
        {
            case "_divMenuBtnToDivIndex":
            case "BtnBack":
                SetDefaultpage(1);
                break;

            case "_divMenuBtnToDivAdd":

                SetDefaultpage(2);
                break;

            case "_divMenuBtnToDivApprove":
                SetDefaultpage(5);
                break;

            case "btnsystem":
                string cmdArg_sys = e.CommandArgument.ToString();
                ViewState["m0_tidx"] = cmdArg_sys;
                switch (cmdArg_sys)
                {
                    case "1":
                        imgcreate.ImageUrl = "~/images/hr_resignation/Icon_ปุ่มระบบลาออก-02.png";
                        imgedit.ImageUrl = "~/images/hr_resignation/Icon_ปุ่มระบบลาออก-09.png";
                        //imgchange.ImageUrl = "~/images/hr_resignation/Icon_ปุ่มระบบลาออก-05.png";

                        div_create_quit.Visible = true;
                        txtremark.Text = String.Empty;
                        dateexit.Text = String.Empty;
                        txtrdo.Text = String.Empty;
                        chkagree.Checked = false;
                        CleardataSetPersonFormList();


                        fv_insert.ChangeMode(FormViewMode.Insert);
                        fv_insert.DataBind();

                        var rdochoice_ = (RadioButtonList)fv_insert.FindControl("rdochoice");
                        var txtstartdate = (TextBox)fv_insert.FindControl("txtstartdate");
                        var txtyear = (TextBox)fv_insert.FindControl("txtyear");
                        var txtin = (TextBox)fv_insert.FindControl("txtin");

                        select_reasonexit(rdochoice_);
                        select_detailprofile(txtstartdate, int.Parse(ViewState["EmpIDX"].ToString()), txtyear, txtin);
                        string date = DateTime.Now.ToString("yyyy-MM-dd");

                        txtyear.Text = _funcTool.calYearMonthDay(DateTime.Parse(txtin.Text), DateTime.Parse(date));


                        select_asset(GvAsset, int.Parse(ViewState["EmpIDX"].ToString()));
                        SelectGetHolderList(GvHolder, int.Parse(ViewState["EmpIDX"].ToString()));

                        div_edit_quit.Visible = false;
                        break;
                    case "2":
                        imgcreate.ImageUrl = "~/images/hr_resignation/Icon_ปุ่มระบบลาออก-01.png";
                        imgedit.ImageUrl = "~/images/hr_resignation/Icon_ปุ่มระบบลาออก-10.png";
                        //imgchange.ImageUrl = "~/images/hr_resignation/Icon_ปุ่มระบบลาออก-05.png";

                        div_create_quit.Visible = false;
                        div_edit_quit.Visible = true;

                        fv_edit.ChangeMode(FormViewMode.Edit);
                        fv_edit.DataBind();
                        select_detail_foredit(fv_edit, int.Parse(ViewState["EmpIDX"].ToString()));

                        if (ViewState["Edit_insert"].ToString() != "1")
                        {
                            var txtstartdate_edit = (TextBox)fv_edit.FindControl("txtstartdate_edit");
                            var txtyear_edit = (TextBox)fv_edit.FindControl("txtyear_edit");
                            var txtin_edit = (TextBox)fv_edit.FindControl("txtin_edit");
                            var rdochoice_edit = (RadioButtonList)fv_edit.FindControl("rdochoice_edit");
                            var lblm0_rsidx_edit = (TextBox)fv_edit.FindControl("lblm0_rsidx_edit");
                            var divrdoreason_edit = (Control)fv_edit.FindControl("divrdoreason_edit");
                            var hfu0_docidx = (HiddenField)fv_edit.FindControl("hfu0_docidx");


                            select_detailprofile(txtstartdate_edit, int.Parse(ViewState["EmpIDX"].ToString()), txtyear_edit, txtin_edit);
                            string date_edit = DateTime.Now.ToString("yyyy-MM-dd");

                            txtyear_edit.Text = _funcTool.calYearMonthDay(DateTime.Parse(txtin_edit.Text), DateTime.Parse(date_edit));

                            select_reasonexit(rdochoice_edit);
                            rdochoice_edit.SelectedValue = lblm0_rsidx_edit.Text;


                            if (lblm0_rsidx_edit.Text == "5")
                            {
                                divrdoreason_edit.Visible = true;

                            }
                            else
                            {
                                divrdoreason_edit.Visible = false;
                            }

                            select_asset_detail(GvAsset_edit, int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(hfu0_docidx.Value));
                            select_holder_detail(GvHolder_edit, int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(hfu0_docidx.Value));
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('คุณไม่มีรายการลาออก');", true);
                            div_edit_quit.Visible = false;
                        }
                        break;
                   
                }

                break;

            case "CmdInsert":

                if (chkagree.Checked == false)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกยืนยันการรับทราบเงื่อนไขในการลาออก');", true);
                }
                else
                {
                    setAddList_GvAsset();
                    Insert_Resignation();
                    SetDefaultpage(3);
                }
                break;

            case "CmdSaveList":
                Insert_Data_Exitinterview();
                SetDefaultpage(1);
                break;

            case "CmdDetail":
                string[] arg1_detail = new string[4];
                arg1_detail = e.CommandArgument.ToString().Split(';');

                ViewState["u0_docidx"] = arg1_detail[0];
                ViewState["CEmpIDX_u0"] = arg1_detail[1];
                ViewState["m0_tidx_create"] = arg1_detail[2];
                ViewState["arg1_detail"] = arg1_detail[3];


                SetDefaultpage(4);
                CleardataSetExchangeFormList(GvExchange);
                break;

            case "cmddata_resign":
                _divMenuLiToViewDetail_resign.Attributes.Add("class", "active");
                _divMenuLiToViewDetail_exitinterivew.Attributes.Remove("class");

                div_detailasset.Visible = true;
                div_exitinterview.Visible = false;

                break;

            case "cmddata_exitinterview":
                _divMenuLiToViewDetail_resign.Attributes.Remove("class");
                _divMenuLiToViewDetail_exitinterivew.Attributes.Add("class", "active");

                div_detailasset.Visible = false;
                div_exitinterview.Visible = true;
                select_gettypeanswer(GvTypeAns_detail, int.Parse(ViewState["m0_toidx"].ToString()));

                break;
            case "CmdUpdateApprove":
                if (ViewState["m0_node"].ToString() != "9")
                {
                    UpdateApprover(int.Parse(ViewState["m0_node"].ToString()), int.Parse(ViewState["m0_actor"].ToString()), int.Parse(ddl_approve.SelectedValue),
                        int.Parse(hftype_jobgrade_flow.Value), int.Parse(hfm0_tidx.Value), int.Parse(ViewState["EmpIDX"].ToString()), txtremark_approve.Text, int.Parse(ViewState["u0_docidx"].ToString()), int.Parse(hfdocidx_ref.Value));
                }
                else
                {
                    UpdateApprove_Exchange(int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ViewState["u0_docidx"].ToString()), int.Parse(hfM0NodeIDX_EX.Value), int.Parse(hfM0ActoreIDX_EX.Value), int.Parse(ddl_approve.SelectedValue), txtremark_approve.Text, int.Parse(hftype_jobgrade_flow.Value));
                }

                select_sumlist(int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(ViewState["Pos_idx"].ToString()), int.Parse(ViewState["Org_idx"].ToString()));
                SetDefaultpage(5);
                setOntop.Focus();
                break;

            case "CmdEdit":
                if (ViewState["arg1_detail"].ToString() != "3")
                {
                    Update_Edit(int.Parse(ViewState["u0_docidx"].ToString()), txtremark_detail.Text, dateexit_detail.Text, int.Parse(ViewState["EmpIDX"].ToString()),
                int.Parse(hfM0NodeIDX.Value), int.Parse(hfM0ActoreIDX.Value), int.Parse(hfM0StatusIDX.Value), int.Parse(hfm0_tidx.Value),
                int.Parse(ViewState["JobGradeIDX"].ToString()));
                }
                else
                {
                    Insert_Cancel();
                }
                SetDefaultpage(1);
                setOntop.Focus();
                break;

            case "CmdInsert_Edit":
                EditInsert_Resignation();
                SetDefaultpage(1);
                break;

            case "CmdAdd_Exchange":
                setAddList_Exchange(GvExchange, dateexchange.Text, int.Parse(rdotype.SelectedValue), int.Parse(ddlorg_exchange.SelectedValue), ddlorg_exchange.SelectedItem.Text
                    , int.Parse(ddlrdept_exchange.SelectedValue), ddlrdept_exchange.SelectedItem.Text, int.Parse(ddlrsec_exchange.SelectedValue), ddlrsec_exchange.SelectedItem.Text, txtremark_exchange.Text);
                div_save.Visible = true;
                break;

            case "btnAdd_Exchange":
                Insert_Exchange();
                SetDefaultpage(1);
                break;


            case "_divMenuBtnToDivManual":
                Response.Write("<script>window.open('https://docs.google.com/document/d/1Q_yEHR8WZJZ404wh8eEeY6QlWizpkpSz72BA2SFQUas/edit?usp=sharing','_blank');</script>");

                break;

            case "_divMenuBtnToDivFlowChart":
                Response.Write("<script>window.open('https://drive.google.com/file/d/1G-XE0r858-kL1hG9uJ0K62UlenpKepb4/view?usp=sharing','_blank');</script>");

                break;
        }
    }
    #endregion
}