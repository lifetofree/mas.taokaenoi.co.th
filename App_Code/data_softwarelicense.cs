﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_softwarelicense
/// </summary>

[Serializable]
[XmlRoot("data_softwarelicense")]
public class data_softwarelicense
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

    [XmlElement("m0software_list")]
    public m0software_detail[] m0software_list { get; set; }

    [XmlElement("m0software_dataset_list")]
    public m0software_dataset_detail[] m0software_dataset_list { get; set; }

    [XmlElement("m0software_datasetedit_list")]
    public m0software_datasetedit_detail[] m0software_datasetedit_list { get; set; }

    [XmlElement("bind_m0software_list")]
    public bind_m0software_detail[] bind_m0software_list { get; set; }

    [XmlElement("log_m0software_list")]
    public log_m0software_detail[] log_m0software_list { get; set; }

    [XmlElement("m0_itswl_company_list")]
    public m0_itswl_company_detail[] m0_itswl_company_list { get; set; }

    [XmlElement("organization_list")]
    public organization_detail[] organization_list { get; set; }

    [XmlElement("software_list")]
    public software_detail[] software_list { get; set; }

    [XmlElement("softwarename_list")]
    public softwarename_detail[] softwarename_list { get; set; }

    [XmlElement("bindm0_typedevice_list")]
    public bindm0_typedevice_detail[] bindm0_typedevice_list { get; set; }

    [XmlElement("bindu0_device_list")]
    public bindu0_device_detail[] bindu0_device_list { get; set; }

    

}


[Serializable]
public class softwarename_detail
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("software_idx")]
    public int software_idx { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("name")]
    public string name { get; set; }

    [XmlElement("version")]
    public string version { get; set; }

    [XmlElement("software_type")]
    public int software_type { get; set; }

    [XmlElement("status")]
    public int status { get; set; }    

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("license_idx")]
    public int license_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("num_license")]
    public int num_license { get; set; }

    [XmlElement("check_checkbox")]
    public int check_checkbox { get; set; }

    [XmlElement("po_code")]
    public string po_code { get; set; }

    [XmlElement("count_software_license")]
    public int count_software_license { get; set; }

    [XmlElement("u0_use")]
    public int u0_use { get; set; }

    [XmlElement("software_use")]
    public int software_use { get; set; }

    [XmlElement("software_licenseuseasset")]
    public int software_licenseuseasset { get; set; }

    [XmlElement("software_ghost")]
    public int software_ghost { get; set; }






}

[Serializable]
public class m0software_detail
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("software_idx")]
    public int software_idx { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("software_version")]
    public string software_version { get; set; }

    [XmlElement("software_license")]
    public string software_license { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("mobile_no")]
    public string mobile_no { get; set; }

    [XmlElement("software_status")]
    public int software_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("license_idx")]
    public int license_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("num_license")]
    public int num_license { get; set; }

    [XmlElement("check_checkbox")]
    public int check_checkbox { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("po_code")]
    public string po_code { get; set; }

    [XmlElement("software_price")]
    public int software_price { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }


}

[Serializable]
public class m0software_dataset_detail
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("software_idx")]
    public int software_idx { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("software_version")]
    public string software_version { get; set; }

    [XmlElement("software_license")]
    public string software_license { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("mobile_no")]
    public string mobile_no { get; set; }

    [XmlElement("software_status")]
    public int software_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("license_idx")]
    public int license_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("num_license")]
    public int num_license { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }



}

[Serializable]
public class m0software_datasetedit_detail
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("software_idx")]
    public int software_idx { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("software_version")]
    public string software_version { get; set; }

    [XmlElement("software_license")]
    public string software_license { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("mobile_no")]
    public string mobile_no { get; set; }

    [XmlElement("software_status")]
    public int software_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("license_idx")]
    public int license_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("num_license")]
    public int num_license { get; set; }



}

[Serializable]
public class bind_m0software_detail
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("software_idx")]
    public int software_idx { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("software_version")]
    public string software_version { get; set; }

    [XmlElement("software_license")]
    public string software_license { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("mobile_no")]
    public string mobile_no { get; set; }

    [XmlElement("software_status")]
    public int software_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("license_idx")]
    public int license_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("num_license")]
    public int num_license { get; set; }

    [XmlElement("count_license")]
    public int count_license { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("po_code")]
    public string po_code { get; set; }

    [XmlElement("software_price")]
    public int software_price { get; set; }



}

[Serializable]
public class log_m0software_detail
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("software_idx")]
    public int software_idx { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("software_version")]
    public string software_version { get; set; }

    [XmlElement("software_license")]
    public string software_license { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("mobile_no")]
    public string mobile_no { get; set; }

    [XmlElement("software_status")]
    public int software_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("license_idx")]
    public int license_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("num_license")]
    public int num_license { get; set; }

    [XmlElement("count_license")]
    public int count_license { get; set; }

    [XmlElement("license_org_idx")]
    public int license_org_idx { get; set; }

    [XmlElement("num_license_rdept_idx")]
    public int num_license_rdept_idx { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("license_rdept_idx")]
    public int license_rdept_idx { get; set; }

    [XmlElement("count_asset")]
    public int count_asset { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }



}

[Serializable]
public class m0_itswl_company_detail
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("mobile_no")]
    public string mobile_no { get; set; }

    [XmlElement("email")]
    public string email { get; set; }

    [XmlElement("company_status")]
    public int company_status { get; set; }

    [XmlElement("software_idx")]
    public int software_idx { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("software_version")]
    public string software_version { get; set; }

    [XmlElement("software_license")]
    public string software_license { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("software_status")]
    public int software_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }



}

[Serializable]
public class organization_detail
{
    

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }


    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("OrgNameEN")]
    public string OrgNameEN { get; set; }

    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("DeptIDX")]
    public int DeptIDX { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("DeptNameEN")]
    public string DeptNameEN { get; set; }

    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("SecIDX")]
    public int SecIDX { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("SecNameEN")]
    public string SecNameEN { get; set; }

    [XmlElement("RPosIDX")]
    public int RPosIDX { get; set; }

    [XmlElement("PosIDX")]
    public int PosIDX { get; set; }

    [XmlElement("PosNameTH")]
    public string PosNameTH { get; set; }

    [XmlElement("PosNameEN")]
    public string PosNameEN { get; set; }

    [XmlElement("EODSPIDX")]
    public int EODSPIDX { get; set; }

    //[XmlElement("EmpIDX")]
    //public int EmpIDX { get; set; }

    [XmlElement("EmpIDXUser")]
    public int EmpIDXUser { get; set; }

    [XmlElement("EStatus")]
    public int EStatus { get; set; }

    [XmlElement("CreateDate")]
    public string CreateDate { get; set; }

    [XmlElement("EStaOut")]
    public int EStaOut { get; set; }

    [XmlElement("JobGradeIDX1")]
    public int JobGradeIDX1 { get; set; }

    [XmlElement("JobGradeIDX2")]
    public int JobGradeIDX2 { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("selected")]
    public bool selected { get; set; }

}

[Serializable]
public class software_detail
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("software_idx")]
    public int software_idx { get; set; }

    [XmlElement("software_name")]
    public string software_name { get; set; }

    [XmlElement("software_version")]
    public string software_version { get; set; }

    [XmlElement("software_license")]
    public string software_license { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("mobile_no")]
    public string mobile_no { get; set; }

    [XmlElement("software_status")]
    public int software_status { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("license_idx")]
    public int license_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("num_license")]
    public int num_license { get; set; }

    [XmlElement("check_checkbox")]
    public int check_checkbox { get; set; }

    [XmlElement("count_license")]
    public int count_license { get; set; }

    [XmlElement("count_licensedept")]
    public int count_licensedept { get; set; }


}

[Serializable]
public class bindm0_typedevice_detail
{
    [XmlElement("m0_tdidx")]
    public int m0_tdidx { get; set; }

    [XmlElement("status_m0_typedevice")]
    public int status_m0_typedevice { get; set; }

    [XmlElement("name_m0_typedevice")]
    public string name_m0_typedevice { get; set; }

   
}

[Serializable]
public class bindu0_device_detail
{
    [XmlElement("u0_didx")]
    public int u0_didx { get; set; }

    [XmlElement("u0_code")]
    public string u0_code { get; set; }

    [XmlElement("u0_acc")]
    public string u0_acc { get; set; }

    [XmlElement("u0_po")]
    public string u0_po { get; set; }

    [XmlElement("m0_iridx")]
    public int m0_iridx { get; set; }
    [XmlElement("u0_purchase")]
    public string u0_purchase { get; set; }
    [XmlElement("u0_expDate")]
    public string u0_expDate { get; set; }
    [XmlElement("m0_bidx")]
    public int m0_bidx { get; set; }
    [XmlElement("m0_cpidx")]
    public int m0_cpidx { get; set; }
    [XmlElement("m0_sidx")]
    public int m0_sidx { get; set; }
    [XmlElement("m0_lvidx")]
    public int m0_lvidx { get; set; }
    [XmlElement("u0_serial")]
    public int u0_serial { get; set; }
    [XmlElement("m0_tdidx")]
    public int m0_tdidx { get; set; }
    [XmlElement("u0_empcreate")]
    public int u0_empcreate { get; set; }
    [XmlElement("u0_createdate")]
    public string u0_createdate { get; set; }
    [XmlElement("m0_orgidx")]
    public int m0_orgidx { get; set; }
    
    [XmlElement("u0_relation_didx")]
    public int u0_relation_didx { get; set; }

    [XmlElement("u0_dvdidx")]
    public int u0_dvdidx { get; set; }
    [XmlElement("m0_hidx")]
    public int m0_hidx { get; set; }
    [XmlElement("m0_ridx")]
    public int m0_ridx { get; set; }
    [XmlElement("m0_vidx")]
    public int m0_vidx { get; set; }
    [XmlElement("m0_cidx")]
    public int m0_cidx { get; set; }
    [XmlElement("m0_pidx")]
    public int m0_pidx { get; set; }
    [XmlElement("m0_midx")]
    public int m0_midx { get; set; }

    [XmlElement("name_m0_typedevice")]
    public string name_m0_typedevice { get; set; }
    [XmlElement("name_m0_band")]
    public string name_m0_band { get; set; }
    [XmlElement("detail_m0_band")]
    public string detail_m0_band { get; set; }

    [XmlElement("band_ram")]
    public string band_ram { get; set; }
    [XmlElement("type_ram")]
    public string type_ram { get; set; }
    [XmlElement("capacity_ram")]
    public string capacity_ram { get; set; }

    [XmlElement("band_hdd")]
    public string band_hdd { get; set; }
    [XmlElement("type_hdd")]
    public string type_hdd { get; set; }
    [XmlElement("capacity_hdd")]
    public string capacity_hdd { get; set; }

    [XmlElement("type_vga")]
    public string type_vga { get; set; }
    [XmlElement("generation_vga")]
    public string generation_vga { get; set; }

    [XmlElement("band_cpu")]
    public string band_cpu { get; set; }
    [XmlElement("generation_cpu")]
    public string generation_cpu { get; set; }
    [XmlElement("ddl_band_cpu")]
    public string ddl_band_cpu { get; set; }

    [XmlElement("band_moniter")]
    public string band_moniter { get; set; }
    [XmlElement("size_moniter")]
    public string size_moniter { get; set; }

    [XmlElement("band_print")]
    public string band_print { get; set; }
    [XmlElement("type_print")]
    public string type_print { get; set; }
    [XmlElement("type_ink")]
    public string type_ink { get; set; }
    [XmlElement("generation_print")]
    public string generation_print { get; set; }

    [XmlElement("m0_Master")]
    public int m0_Master { get; set; }
    [XmlElement("u0_unit")]
    public int u0_unit { get; set; }
    [XmlElement("name_m0_status")]
    public string name_m0_status { get; set; }
    [XmlElement("u0_relation_List")]
    public string u0_relation_List { get; set; }

    [XmlElement("u0_unidx")]
    public int u0_unidx { get; set; }
    [XmlElement("u0_acidx")]
    public int u0_acidx { get; set; }
    [XmlElement("u0_doc_decision")]
    public int u0_doc_decision { get; set; }

    [XmlElement("u1_unidx")]
    public int u1_unidx { get; set; }
    [XmlElement("u1_acidx")]
    public int u1_acidx { get; set; }
    [XmlElement("u1_doc_decision")]
    public int u1_doc_decision { get; set; }
    [XmlElement("doc_status")]
    public string doc_status { get; set; }
    [XmlElement("u0_referent")]
    public string u0_referent { get; set; }
    [XmlElement("name_doc_status")]
    public string name_doc_status { get; set; }

    [XmlElement("u0_didx_reference")]
    public int u0_didx_reference { get; set; }
    [XmlElement("u0_referent_code")]
    public string u0_referent_code { get; set; }

    [XmlElement("u0_status_approve")]
    public int u0_status_approve { get; set; }
    [XmlElement("comment")]
    public string comment { get; set; }
    [XmlElement("u0_didx_reference_transfer")]
    public int u0_didx_reference_transfer { get; set; }
    [XmlElement("u0_slotram")]
    public int u0_slotram { get; set; }

    [XmlElement("software_name_idx")]
    public int software_name_idx { get; set; }

    [XmlElement("count_indept")]
    public int count_indept { get; set; }

    [XmlElement("OrgIDX")]
    public int OrgIDX { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("RDeptIDX")]
    public int RDeptIDX { get; set; }

    [XmlElement("DeptNameTH")]
    public string DeptNameTH { get; set; }

    [XmlElement("RSecIDX")]
    public int RSecIDX { get; set; }

    [XmlElement("SecNameTH")]
    public string SecNameTH { get; set; }

    [XmlElement("emp_holder")]
    public int emp_holder { get; set; }

    [XmlElement("emp_name_th_holder")]
    public string emp_name_th_holder { get; set; }

    [XmlElement("u0_use")]
    public int u0_use { get; set; }

    [XmlElement("email_holder")]
    public string email_holder { get; set; }

    [XmlElement("u0_ghost")]
    public int u0_ghost { get; set; }

    [XmlElement("u0_uselicense")]
    public int u0_uselicense { get; set; }

    [XmlElement("software_count_all")]
    public int software_count_all { get; set; }


}


