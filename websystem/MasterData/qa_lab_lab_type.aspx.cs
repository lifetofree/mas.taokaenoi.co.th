﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_qa_lab_lab_type : System.Web.UI.Page
{

    #region Connect
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();

    data_qa _data_qa = new data_qa();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//


    static string _urlQaSet_labtype = _serviceUrl + ConfigurationManager.AppSettings["urlQaSet_labtype"];
    static string _urlQaget_labtype = _serviceUrl + ConfigurationManager.AppSettings["urlQaget_labtype"];
    static string _urlQaDelete_labtype = _serviceUrl + ConfigurationManager.AppSettings["urlQaDelete_labtype"];


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    bool _flag_qmr = false;
    #endregion

    #region Page_Init
    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
     
        if (!IsPostBack)
        {
            initPage();
        }
    }
    #endregion

    #region initPage
    protected void initPage()
    {
        MvMaster_lab_type.SetActiveView(view1_lab_type);
        Select_lab_type();
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region Insert&Select&Update
    protected void Insert_lab_type()
    {
        FormView fvDocDetail_insert_qa = (FormView)view1_lab_type.FindControl("Fv_Insert_Result");
        TextBox tex_name_lab_type = (TextBox)fvDocDetail_insert_qa.FindControl("txtlab_type_name");
        DropDownList dropD_status_lab_type = (DropDownList)fvDocDetail_insert_qa.FindControl("ddlab_type_add");

        data_qa lab_type_BIN = new data_qa();
        qa_m0_lab_type_detail lab_type_sIN = new qa_m0_lab_type_detail();
        lab_type_BIN.qa_m0_lab_type_list = new qa_m0_lab_type_detail[1];

        lab_type_sIN.lab_type_name = tex_name_lab_type.Text;
        lab_type_sIN.cemp_idx = _emp_idx;
        lab_type_sIN.lab_type_status = int.Parse(dropD_status_lab_type.SelectedValue);
        lab_type_BIN.qa_m0_lab_type_list[0] = lab_type_sIN;
        //lab_type.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(lab_type_BIN));
        lab_type_BIN = callServicePostMasterQA(_urlQaSet_labtype, lab_type_BIN);
        if (lab_type_BIN.return_code == 101)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว! ไม่สามารถบันทึกข้อมูลได้ กรุณาลองใหม่อีกครั้ง');", true);

        }
        else
        {
            tex_name_lab_type.Text = String.Empty;
            
        }
    }
    protected void Select_lab_type()
    {
        data_qa lab_type_BSE = new data_qa();
        qa_m0_lab_type_detail lab_type_sSE = new qa_m0_lab_type_detail();

        lab_type_BSE.qa_m0_lab_type_list = new qa_m0_lab_type_detail[1];
        lab_type_BSE.qa_m0_lab_type_list[0] = lab_type_sSE;
        //lab_type.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(lab_type_BSE));
        lab_type_BSE = callServicePostMasterQA(_urlQaget_labtype, lab_type_BSE);
        setGridData(Gv_select_labtype, lab_type_BSE.qa_m0_lab_type_list);
        Gv_select_labtype.DataSource = lab_type_BSE.qa_m0_lab_type_list;
        Gv_select_labtype.DataBind();
    }
    #endregion

    #region setGridData
    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region setformdata
    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }
    #endregion

    #region callService
    protected data_qa callServicePostMasterQA(string _cmdUrl, data_qa _data_qa)
    {
        _localJson = _funcTool.convertObjectToJson(_data_qa);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_qa = (data_qa)_funcTool.convertJsonToObject(typeof(data_qa), _localJson);


        return _data_qa;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
    #endregion

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();
        switch (cmdName)
        {

            case "cmdAddlab_type":
                btn_addlab_type.Visible = false;
                setFormData(Fv_Insert_Result, FormViewMode.Insert, null);
                Fv_Insert_Result.Visible = true;
                Select_lab_type();
                break;

            case "Lbtn_cancel_lab_type":
                btn_addlab_type.Visible = true;
                setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);
                Fv_Insert_Result.Visible = false;
                SETFOCUS.Focus();
                break;

            case "Lbtn_submit_lab_type":
                Insert_lab_type();
                setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);
                Fv_Insert_Result.Visible = true;
                btn_addlab_type.Visible = true;
                Select_lab_type();
                SETFOCUS.Focus();
                break;
            case "btnTodelete_lab_type":
                // ViewState["DELETE_PLACE"] = int.Parse(cmdArg);

                int a = int.Parse(cmdArg);
                data_qa labtype_Bde = new data_qa();
                qa_m0_lab_type_detail labtype_Sde = new qa_m0_lab_type_detail();

                labtype_Bde.qa_m0_lab_type_list = new qa_m0_lab_type_detail[1];

                labtype_Sde.lab_type_idx = int.Parse(a.ToString());

                labtype_Bde.qa_m0_lab_type_list[0] = labtype_Sde;
                //lab_type.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(labtype_Bde));
                labtype_Bde = callServicePostMasterQA(_urlQaDelete_labtype, labtype_Bde);

                if (labtype_Bde.return_code == 0)
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการสำเร็จ');", true);

                }

                else

                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ');", true);


                }

                Select_lab_type();
                SETFOCUS.Focus();
                break;
        }

    }
    #endregion

    #region Masterbound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "Gv_select_labtype":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblab_type_status = (Label)e.Row.Cells[3].FindControl("lblab_type_status");
                    Label lab_type_statusOpen = (Label)e.Row.Cells[3].FindControl("lab_type_statusOpen");
                    Label labtype_statusClose = (Label)e.Row.Cells[3].FindControl("labtype_statusClose");

                    ViewState["_labtype_status"] = lblab_type_status.Text;


                    if (ViewState["_labtype_status"].ToString() == "1")
                    {
                        lab_type_statusOpen.Visible = true;
                    }
                    else if (ViewState["_labtype_status"].ToString() == "0")
                    {
                        labtype_statusClose.Visible = true;
                    }

                }


                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    btn_addlab_type.Visible = true;
                    setFormData(Fv_Insert_Result, FormViewMode.ReadOnly, null);
                }



                break;


        }
    }
    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "Gv_select_labtype":
                Gv_select_labtype.EditIndex = e.NewEditIndex;
                Select_lab_type();
                break;

        }
    }
    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_labtype":
                var id_labtype = (TextBox)Gv_select_labtype.Rows[e.RowIndex].FindControl("ID_lab_type");
                var name_labtype = (TextBox)Gv_select_labtype.Rows[e.RowIndex].FindControl("Name_lab_type");
                var status_labtype = (DropDownList)Gv_select_labtype.Rows[e.RowIndex].FindControl("ddEdit_lab_type");


                Gv_select_labtype.EditIndex = -1;


                data_qa labtype_bUP = new data_qa();

                qa_m0_lab_type_detail labtype_sUP = new qa_m0_lab_type_detail();

                labtype_bUP.qa_m0_lab_type_list = new qa_m0_lab_type_detail[1];

                labtype_sUP.lab_type_idx = int.Parse(id_labtype.Text);
                labtype_sUP.lab_type_status = int.Parse(status_labtype.SelectedValue);
                labtype_sUP.lab_type_name = name_labtype.Text;
            

                //ViewState["_NAMEPLACE"] = name_place.Text;
                //ViewState["_CODEPLACE"] = code_place.Text;

                labtype_bUP.qa_m0_lab_type_list[0] = labtype_sUP;
                //lab_type.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(labtype_bUP));
                labtype_bUP = callServicePostMasterQA(_urlQaSet_labtype, labtype_bUP);
                Select_lab_type();
                if (labtype_bUP.return_code == 0)
                {

                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการสำเร็จ');", true);

                }

                else

                {


                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการไม่สำเร็จ');", true);


                }
                break;

        }
    }
    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_labtype":
                Gv_select_labtype.EditIndex = -1;
                Select_lab_type();
                btn_addlab_type.Visible = true;
                SETFOCUS.Focus();
                break;

        }
    }
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "Gv_select_labtype":
                Gv_select_labtype.PageIndex = e.NewPageIndex;
                Gv_select_labtype.DataBind();
                Select_lab_type();
                break;

        }
    }
    #endregion
}