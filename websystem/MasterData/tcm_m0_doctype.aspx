﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="tcm_m0_doctype.aspx.cs" Inherits="websystem_MasterData_tcm_m0_doctype" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="litdebug" runat="server"></asp:Literal>

    <asp:MultiView ID="mvSystem" runat="server" ActiveViewIndex="0">
        <asp:View ID="ViewIndex" runat="server">
            <div class="col-md-12">

                <div class="form-group">
                    <asp:LinkButton ID="btnInsert" CssClass="btn btn-primary" Visible="true" data-original-title="Document Type" data-toggle="tooltip" title="Document Type" runat="server"
                        CommandName="cmdInsert" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i> Document Type</asp:LinkButton>
                </div>

                <asp:FormView ID="FvInsert" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"> Create Document Type</h3>

                            </div>
                            <div class="panel-body">
                                <div class="panel-heading">

                                    <div class="form-horizontal" role="form">

                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="lbltype_name_th" runat="server" Text="Document Type Name(TH)" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txttype_name_th" runat="server" CssClass="form-control" placeholder="Document Type Name(TH) ..." Enabled="true" />

                                                    <asp:RequiredFieldValidator ID="Re_txttype_name_th" runat="server"
                                                        ControlToValidate="txttype_name_th" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*Document Type Name(TH)" ValidationGroup="Save" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="aj_Re_txttype_name_th" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txttype_name_th" Width="200" />
                                                </div>
                                                <div class="col-sm-3"></div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="lbltype_name_en" runat="server" Text="Document Type Name(EN)" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txttype_name_en" runat="server" CssClass="form-control" placeholder="Document Type Name(EN) ..." Enabled="true" />

                                                    <asp:RequiredFieldValidator ID="Re_txttype_name_en" runat="server"
                                                        ControlToValidate="txttype_name_en" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*Document Type Name(EN)" ValidationGroup="Save" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txttype_name_en" Width="200" />
                                                </div>
                                                <div class="col-sm-3"></div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="lblstatus" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlStatus" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3"></div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="btnSave" ValidationGroup="Save" CssClass="btn btn-success" Text="Save" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdSave" OnCommand="btnCommand"></asp:LinkButton>
                                                    <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="CmdCancel" data-toggle="tooltip" title="Cancel"></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>

               
                <asp:GridView ID="GvDetail" runat="server" Visible="true"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                    HeaderStyle-CssClass="success"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowDataBound="Master_RowDataBound"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowEditing="Master_RowEditing"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    AutoPostBack="false">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center; color: red"><b>-- no data --</b> </div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style="text-align: center; padding-top: 5px;">
                                        <asp:Label ID="lbl_type_idx" runat="server" Visible="false" Text='<%# Eval("type_idx") %>' />
                                        <%# (Container.DataItemIndex +1) %>
                                    </div>
                                </small>

                            </ItemTemplate>
                            <EditItemTemplate>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="txt_type_idx_edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("type_idx") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lbl_type_name_th_edit" CssClass="col-sm-3 control-label" runat="server" Text="Document Type Name(TH)" />
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_type_name_th_edit" runat="server" CssClass="form-control " Text='<%# Eval("type_name_th") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Re_txt_type_name_th_edit" runat="server"
                                                    ControlToValidate="txt_type_name_th_edit" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Document Type Name(TH)" ValidationGroup="Edit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_pname" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_type_name_th_edit" Width="200" />
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbl_type_name_en_edit" CssClass="col-sm-3 control-label" runat="server" Text="Document Type Name(EN)" />
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_type_name_en_edit" runat="server" CssClass="form-control " Text='<%# Eval("type_name_en") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Re_txt_type_name_en_edit" runat="server"
                                                    ControlToValidate="txt_type_name_en_edit" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*Document Type Name(EN)" ValidationGroup="Edit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_type_name_en_edit" Width="200" />
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lbresult_use_status_edit" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddltype_status_edit" Text='<%# Eval("type_status") %>'
                                                    CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1">Online</asp:ListItem>
                                                    <asp:ListItem Value="0">Offline</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-9">
                                                <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Edit" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                    Text="Save" CommandName="Update"></asp:LinkButton>
                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Document Type Name(TH)" ItemStyle-HorizontalAlign="left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style="padding-top: 5px;">
                                        <asp:Label ID="lbl_type_name_th" runat="server"
                                            Text='<%# Eval("type_name_th") %>'></asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Document Type Name(EN)" ItemStyle-HorizontalAlign="left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style="padding-top: 5px;">
                                        <asp:Label ID="lbl_type_name_en" runat="server"
                                            Text='<%# Eval("type_name_en") %>'></asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <small>
                                    <div style="padding-top: 5px;">
                                        <asp:Label ID="lbl_status" Visible="false" runat="server"
                                            CssClass="col-sm-12" Text='<%# Eval("type_status") %>'></asp:Label>
                                        <asp:Label ID="lbl_statusOnline" runat="server" Visible="false"
                                            data-toggle="tooltip" data-placement="top" title="Online"
                                            CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                        </asp:Label>
                                        <asp:Label ID="lbl_statusOffline" runat="server" Visible="false"
                                            data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                        </asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Management" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <div style="padding-top: 5px">
                                    <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                        data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                    <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="CmdDelete"
                                        data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                        CommandArgument='<%#Eval("type_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

            </div>

        </asp:View>
    </asp:MultiView>

</asp:Content>

