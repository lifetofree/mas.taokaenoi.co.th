﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_lab_setname.aspx.cs" Inherits="websystem_MasterData_qa_lab_setname" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <div class="col-sm-12">
        <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvMaster" runat="server">
        <asp:View ID="pageGenaral" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="lnkbtn_addset" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มชุดข้อมูล" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdAddSet" CommandArgument="0" title="เพิ่มชุดข้อมูล"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มชุดข้อมูล</asp:LinkButton>

            </div>
            <!--formview insert-->
            <asp:FormView ID="fvformInsert" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มชุดข้อมูล</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:RequiredFieldValidator ID="ReqSetname" runat="server"
                                        ControlToValidate="tbSetName" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกชื่อชุดข้อมูล" ValidationGroup="saveSetName" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValaSetname" runat="Server" PopupPosition="TopLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqSetname" Width="180" />

                                    <label class="col-sm-2 control-label">ชื่อชุดข้อมูล</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbSetName" runat="server" CssClass="form-control" placeholder="กรอกชื่อชุดข้อมูล ..." Enabled="true" />
                                    </div>

                                    <label class="col-sm-1 control-label">สถานะ</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddl_setname" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <asp:RequiredFieldValidator ID="ReqtbDetailSet" runat="server"
                                    ControlToValidate="tbDetailSet" Display="None" SetFocusOnError="true"
                                    ErrorMessage="*กรุณากรอกรายละเอียดชุดข้อมูล" ValidationGroup="saveSetName" />
                                <ajaxToolkit:ValidatorCalloutExtender ID="ValitbDetailSet" runat="Server" PopupPosition="BottomRight"
                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbDetailSet" Width="180" />
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">รายละเอียดชุดข้อมูล</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbDetailSet" runat="server" CssClass="form-control" Rows="3" TextMode="MultiLine"
                                            placeholder="กรอกรายละเอียดชุดข้อมูล ..." Enabled="true" />
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" ValidationGroup="saveSetName" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvMaster"
                runat="server"
                AutoGenerateColumns="false"
                DataKeyNames="set_idx"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="5"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="lbsetidx" runat="server" CssClass=" font_text text_center" Visible="false" Text='<%# Eval("set_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="_update_setidx" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("set_idx")%>' />

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อชุดข้อมูล" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdate_setname" runat="server" CssClass="form-control" Text='<%# Eval("set_name")%>' />
                                            <asp:RequiredFieldValidator ID="Reqtbupdate_setname" runat="server"
                                                ControlToValidate="tbupdate_setname" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อชุดข้อมูล" ValidationGroup="saveSetNameUpdate" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valatbupdate_setname" runat="Server" PopupPosition="TopLeft"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqtbupdate_setname" Width="180" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="รายละเอียดชุดข้อมูล" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdate_set_detail" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("set_detail")%>' />
                                        </div>
                                        
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_setname_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlupdate_set_status" Text='<%# Eval("set_name_status") %>'
                                                CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">online</asp:ListItem>
                                                <asp:ListItem Value="0">offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update" ValidationGroup="saveSetNameUpdate" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อชุดข้อมูล" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblsetname" runat="server" Text='<%# Eval("set_name")%>'></asp:Label>
                                <asp:TextBox ID="tbSetIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("setroot_idx")%>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียดชุดข้อมูล" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbsetdetails" runat="server" Text='<%# Eval("set_detail")%>'></asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbstatus_setname" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("set_name_status") %>'></asp:Label>
                                <asp:Label ID="status_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="status_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>

                            <asp:LinkButton ID="btnviewset" CssClass="text-read small" runat="server" CommandName="cmdViewSet"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("set_idx") + "," + "0" %>' title="view">
                                        <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i></asp:LinkButton>
                            <asp:LinkButton ID="Edit" CssClass="text-edit small" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelettypeNews" CssClass="text-trash small" runat="server" CommandName="cmdDeleteSet"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("set_idx") +"," + "0" %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </asp:View>

        <asp:View ID="pageCreateRoot" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnBack" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
                <asp:LinkButton ID="btnAddRoot" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มชุดข้อมูล" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdAddSet" CommandArgument="1" title=""><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มสถานะชุดข้อมูล</asp:LinkButton>
            </div>
            <!--formview insert Level 2-->
            <asp:FormView ID="fvformInsertRoot" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มสถานะชุดข้อมูล</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อชุดข้อมูล</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbSetNameList" runat="server" CssClass="form-control"
                                            placeholder="กรอกชื่อสถานะ ..." Enabled="false" />

                                    </div>
                                    <label class="col-sm-1 control-label">สถานะ</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlRootSetStatus" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>

                                <div class="form-group">

                                    <asp:RequiredFieldValidator ID="ReqtbRootSetName" runat="server"
                                        ControlToValidate="tbRootSetName" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกชื่อสถานะ" ValidationGroup="saveRootSetName" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValtbRootSetName" runat="Server" PopupPosition="BottomLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbRootSetName" Width="180" />
                                    <label class="col-sm-2 control-label">ชื่อสถานะ</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbRootSetName" runat="server" CssClass="form-control"
                                            placeholder="กรอกชื่อสถานะ ..." Enabled="true" />

                                    </div>

                                    <div class="col-sm-1"></div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">รายละเอียดชุดข้อมูล</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbDetailRootSet" runat="server" CssClass="form-control" Rows="3" TextMode="MultiLine"
                                            placeholder="กรอกรายละเอียดชุดข้อมูล ..." Enabled="true" />
                                    </div>
                                    <asp:RequiredFieldValidator ID="ReqtbDetailRootSet" runat="server"
                                        ControlToValidate="tbDetailRootSet" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกชื่อสถานะ" ValidationGroup="saveRootSetName" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="VtbDetailRootSet" runat="Server" PopupPosition="BottomLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbDetailRootSet" Width="180" />
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSave" ValidationGroup="saveRootSetName" CommandArgument="1"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="1"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>
            <asp:GridView ID="gvMasterRoot"
                runat="server"
                AutoGenerateColumns="false"
                DataKeyNames="set_idx"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="5"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="lbrootsetidx" runat="server" CssClass=" font_text text_center" Visible="False" Text='<%# Eval("set_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="_updateRootSetidx" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("set_idx")%>' />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อสถานะชุดข้อมูล" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdateRootSetName" runat="server" CssClass="form-control" Text='<%# Eval("set_name")%>' />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="รายละเอียดชุดข้อมูล" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdateRootSetDetail" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("set_detail")%>' />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_setname_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlupdateRootSetStatus" Text='<%# Eval("set_name_status") %>'
                                                CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">online</asp:ListItem>
                                                <asp:ListItem Value="0">offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อสถานะชุดข้อมูล" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblRootSetName" runat="server" Text='<%# Eval("set_name")%>'></asp:Label>
                                <asp:TextBox ID="tbSetRootIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("setroot_idx")%>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียดสถานะชุดข้อมูล" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbRootSetDetails" runat="server" Text='<%# Eval("set_detail")%>'></asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbRootStatus" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("set_name_status") %>'></asp:Label>
                                <asp:Label ID="status_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="status_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>

                            <asp:LinkButton ID="btnViewRootSet" CssClass="text-read small" runat="server" CommandName="cmdViewSet"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("set_idx") + "," + "1" %>' title="view">
                                        <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i></asp:LinkButton>
                            <asp:LinkButton ID="Edit" CssClass="text-edit small" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelettypeNews" CssClass="text-trash small" runat="server" CommandName="cmdDeleteSet"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("set_idx") +"," + "1" %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>
        <asp:View ID="pageRootSubSet" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="lnkbtnBack" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="1" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
                <asp:LinkButton ID="lnkbtnRootSet" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มชุดข้อมูล" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdAddSet" CommandArgument="2" title=""><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มสถานะชุดข้อมูลย่อย</asp:LinkButton>
            </div>
            <!--formview insert Level 3-->
            <asp:FormView ID="fvformInsertSubSet" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มสถานะชุดข้อมูลย่อย</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อสถานะชุดข้อมูล</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txtSubSetName" runat="server" CssClass="form-control" Enabled="false" />


                                    </div>
                                    <label class="col-sm-1 control-label">สถานะ</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlSubSetStatus" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">ชื่อสถานะ</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbSubSetName" runat="server" CssClass="form-control"
                                            placeholder="กรอกชื่อสถานะ ..." Enabled="true" />
                                    </div>
                                    <asp:RequiredFieldValidator ID="ReqtbSubSetName" runat="server"
                                        ControlToValidate="tbSubSetName" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกชื่อสถานะ" ValidationGroup="saveSubSetName" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="VtbSubSetName" runat="Server" PopupPosition="BottomLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbSubSetName" Width="180" />
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">รายละเอียดชุดข้อมูล</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="tbDetailSubSet" runat="server" CssClass="form-control" Rows="3" TextMode="MultiLine"
                                            placeholder="กรอกรายละเอียดชุดข้อมูล ..." Enabled="true" />
                                    </div>
                                    <asp:RequiredFieldValidator ID="ReqtbDetailSubSet" runat="server"
                                        ControlToValidate="tbDetailSubSet" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณากรอกรายละเอียดชุดข้อมูล" ValidationGroup="saveSubSetName" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValtbDetailSubSet" runat="Server" PopupPosition="BottomLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqtbDetailSubSet" Width="180" />
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" ValidationGroup="saveSubSetName" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="2"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="2"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>
            <asp:GridView ID="gvMasterSubSet"
                runat="server"
                AutoGenerateColumns="false"
                DataKeyNames="set_idx"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="5"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center;">
                                <asp:Label ID="lbsubsetidx" runat="server" CssClass=" font_text text_center" Visible="False" Text='<%# Eval("set_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="_updateSubSetidx" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("set_idx")%>' />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อสถานะชุดข้อมูล" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdateSubSetName" runat="server" CssClass="form-control" Text='<%# Eval("set_name")%>' />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="รายละเอียดชุดข้อมูล" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdateSubSetDetail" TextMode="MultiLine" Rows="3" runat="server" CssClass="form-control" Text='<%# Eval("set_detail")%>' />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_setname_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlupdateSubSetStatus" Text='<%# Eval("set_name_status") %>'
                                                CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">online</asp:ListItem>
                                                <asp:ListItem Value="0">offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อสถานะชุดข้อมูล" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblSubSetName" runat="server" Text='<%# Eval("set_name")%>'></asp:Label>
                                <asp:TextBox ID="tbSubSetRootIDX" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("setroot_idx")%>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รายละเอียดสถานะชุดข้อมูล" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbSubSetDetails" runat="server" Text='<%# Eval("set_detail")%>'></asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbRootSubStatus" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("set_name_status") %>'></asp:Label>
                                <asp:Label ID="status_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="status_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="text-align: center; color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>

                            <%--   <asp:LinkButton ID="btnViewRootSubSet" CssClass="text-read small" runat="server" CommandName="cmdViewSet"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("set_idx") + "," + "2" %>' title="view">--%>
                            <%--                 <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i></asp:LinkButton>--%>
                            <asp:LinkButton ID="Edit" CssClass="text-edit small" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelRootSubSet" CssClass="text-trash small" runat="server" CommandName="cmdDeleteSet"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("set_idx") +"," + "2" %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>


        </asp:View>
    </asp:MultiView>
</asp:Content>

