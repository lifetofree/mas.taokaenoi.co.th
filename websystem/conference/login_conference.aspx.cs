using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_login_conference : System.Web.UI.Page {
    #region initial function/data
    function_tool _funcTool = new function_tool ();

    data_conference _data_conference = new data_conference (); //--rc4 >> hpR97A56kQ

    int _default_int = 0;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;

    const int CONF_TYPE = 2;
    const int CONF_ROUND = 2;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlConferenceGetConfData = _serviceUrl + ConfigurationManager.AppSettings["urlConferenceGetConfData"];
    static string _urlConferenceSetConfData = _serviceUrl + ConfigurationManager.AppSettings["urlConferenceSetConfData"];

    static String _meetingUrl = "https://taokaenoi1.webex.com/meet/mis_meeting";
    #endregion initial function/data

    protected void Page_Load (object sender, EventArgs e) {
        //check conf_type
        switch (CONF_TYPE) {
            case 1:
                body_main.Attributes.Add ("style", "background-color: #f0393c;");
                div_conf_1.Visible = true;
                div_conf_2.Visible = false;
                break;
            case 2:
                body_main.Attributes.Add ("style", "background-color: #ffffff;");
                div_conf_1.Visible = false;
                div_conf_2.Visible = true;
                break;
            default:
                break;
        }

        tbUserName.Focus ();
    }

    #region btn command
    protected void btnCommand (object sender, CommandEventArgs e) {
        string cmdName = e.CommandName.ToString ();
        string cmdArg = e.CommandArgument.ToString ();

        switch (cmdName) {
            case "cmdLogin":
                search_conf_data_detail _search_key = new search_conf_data_detail ();
                _search_key.s_user_name = tbUserName.Text.Trim ().ToString ();
                _search_key.s_user_pass = _funcTool.getMd5Sum (tbUserPass.Text.Trim ()).ToString ();
                _data_conference.search_conf_data_list = new search_conf_data_detail[1];
                _data_conference.search_conf_data_list[0] = _search_key;
                _data_conference.conf_mode = "10";
                _data_conference = callServicePostConference (_urlConferenceGetConfData, _data_conference);

                switch (_data_conference.return_code) {
                    case 0: //success
                        // login success
                        Response.Redirect (_meetingUrl);
                        break;
                    default:
                        ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('Username / Password ไม่ถูกต้องหรือไม่พบสิทธิ์ในการเข้าใช้งานค่ะ');", true);
                        break;
                }
                break;
            case "cmdRegister":
                conf_user_detail_u0 _reg_data = new conf_user_detail_u0();
                _reg_data.u0_idx = 0;
                _reg_data.user_firstname = tbUserFirstname.Text.Trim();
                _reg_data.user_lastname = tbUserLastname.Text.Trim();
                _reg_data.user_mail = tbUserMail.Text.Trim();
                _reg_data.user_company = tbUserCompany.Text.Trim();
                _reg_data.user_name = tbUserWebEx.Text.Trim();
                _reg_data.conf_type = 2;
                _reg_data.conf_round = 2;
                _data_conference.conf_user_list_u0 = new conf_user_detail_u0[1];
                _data_conference.conf_user_list_u0[0] = _reg_data;
                _data_conference.conf_mode = "10";
                _data_conference = callServicePostConference (_urlConferenceSetConfData, _data_conference);

                switch (_data_conference.return_code) {
                    case 0: //success
                        // login success
                        Response.Redirect (_meetingUrl);
                        break;
                    default:
                        ScriptManager.RegisterClientScriptBlock (this.Page, this.Page.GetType (), "alert", "alert('ข้อมูลไม่ถูกต้องค่ะ');", true);
                        break;
                }
                break;
        }
    }
    #endregion btn command

    #region reuse
    protected data_conference callServicePostConference (string _cmdUrl, data_conference _data_conference) {
        // convert to json
        _local_json = _funcTool.convertObjectToJson (_data_conference);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost (_cmdUrl, _local_json);

        // convert json to object
        _data_conference = (data_conference) _funcTool.convertJsonToObject (typeof (data_conference), _local_json);

        return _data_conference;
    }
    #endregion reuse
}