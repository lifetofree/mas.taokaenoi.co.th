﻿using System;
using System.Xml.Serialization;

[Serializable]
[XmlRoot("data_pms")]
public class data_pms
{
    [XmlElement("ReturnCode")]
    public string ReturnCode { get; set; }

    [XmlElement("ReturnMsg")]
    public string ReturnMsg { get; set; }

    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }
    [XmlElement("pms_mode")]
    public string pms_mode { get; set; }

    [XmlElement("Boxpmsm0_DocFormDetail")]
    public pmsm0_DocFormDetail[] Boxpmsm0_DocFormDetail { get; set; }

    [XmlElement("Boxpmsu0_DocFormDetail")]
    public pmsu0_DocFormDetail[] Boxpmsu0_DocFormDetail { get; set; }

    [XmlElement("Boxpmsu1_DocFormDetail")]
    public pmsu1_DocFormDetail[] Boxpmsu1_DocFormDetail { get; set; }

    [XmlElement("Boxpmsu2_DocFormDetail")]
    public pmsu2_DocFormDetail[] Boxpmsu2_DocFormDetail { get; set; }

    [XmlElement("Boxpmsm2_individual_list")]
    public pmsm2_individualDetail[] Boxpmsm2_individual_list { get; set; }

    [XmlElement("Boxpmsm0_factor_list")]
    public pmsm0_factorDetail[] Boxpmsm0_factor_list { get; set; }

    [XmlElement("pms_approve_list_m0")]
    public pms_approve_detail_m0[] pms_approve_list_m0 { get; set; }

    [XmlElement("pms_time_list_m0")]
    public pms_time_detail_m0[] pms_time_list_m0 { get; set; }


    [XmlElement("search_pms_report_list")]
    public search_pms_report_detail[] search_pms_report_list { get; set; }
    [XmlElement("pms_report_list")]
    public pms_report_detail[] pms_report_list { get; set; }
}

[Serializable]
public class pmsm0_DocFormDetail
{
    [XmlElement("empgroup_idx")]
    public int empgroup_idx { get; set; }

    [XmlElement("empgroup_nameth")]
    public string empgroup_nameth { get; set; }

    [XmlElement("m0_typeidx")]
    public int m0_typeidx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("m1_typeidx_comma")]
    public string m1_typeidx_comma { get; set; }

    [XmlElement("m1_typeidx")]
    public int m1_typeidx { get; set; }

    [XmlElement("m1_type_name")]
    public string m1_type_name { get; set; }

    [XmlElement("status")]
    public int status { get; set; }

    [XmlElement("m2_type_name")]
    public string m2_type_name { get; set; }

    [XmlElement("m2_typeidx")]
    public int m2_typeidx { get; set; }

    [XmlElement("pos_name")]
    public string pos_name { get; set; }

    [XmlElement("posidx_comma")]
    public string posidx_comma { get; set; }

    [XmlElement("form_name")]
    public string form_name { get; set; }

    [XmlElement("posidx")]
    public int posidx { get; set; }

    [XmlElement("orgidx")]
    public int orgidx { get; set; }

    [XmlElement("m2_typeidx_comma")]
    public string m2_typeidx_comma { get; set; }

    [XmlElement("OrgNameTH")]
    public string OrgNameTH { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("u0_typeidx")]
    public int u0_typeidx { get; set; }

    [XmlElement("r0_typeidx")]
    public int r0_typeidx { get; set; }

    [XmlElement("m0_point")]
    public int m0_point { get; set; }

    [XmlElement("point_name")]
    public int point_name { get; set; }

    [XmlElement("setting_point")]
    public int setting_point { get; set; }

    [XmlElement("rat_nameth")]
    public string rat_nameth { get; set; }

    [XmlElement("namechoice")]
    public string namechoice { get; set; }

    [XmlElement("rat_nameen")]
    public string rat_nameen { get; set; }

    [XmlElement("definition_th")]
    public string definition_th { get; set; }

    [XmlElement("definition_en")]
    public string definition_en { get; set; }

    [XmlElement("m1_coreidx")]
    public int m1_coreidx { get; set; }

    [XmlElement("core_name")]
    public string core_name { get; set; }

    [XmlElement("type_core")]
    public string type_core { get; set; }

    [XmlElement("m2_coreidx")]
    public int m2_coreidx { get; set; }

    [XmlElement("core_nameen")]
    public string core_nameen { get; set; }

    [XmlElement("core_nameth")]
    public string core_nameth { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("node_desc")]
    public string node_desc { get; set; }

    [XmlElement("noidx")]
    public int noidx { get; set; }

    [XmlElement("decision_idx")]
    public int decision_idx { get; set; }

    [XmlElement("decision_name")]
    public string decision_name { get; set; }

    [XmlElement("decision_desc")]
    public string decision_desc { get; set; }

    [XmlElement("decision_status")]
    public string decision_status { get; set; }

    [XmlElement("m0_kpi_idx")]
    public int m0_kpi_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("sum_point")]
    public string sum_point { get; set; }

    [XmlElement("m0_kpi_status")]
    public int m0_kpi_status { get; set; }

    [XmlElement("m1_kpi_idx")]
    public int m1_kpi_idx { get; set; }

    [XmlElement("perf_indicators")]
    public string perf_indicators { get; set; }

    [XmlElement("kpi_unit")]
    public string kpi_unit { get; set; }

    [XmlElement("kpi_weight")]
    public string kpi_weight { get; set; }

    [XmlElement("kpi_target")]
    public string kpi_target { get; set; }

    [XmlElement("m1_kpi_status")]
    public int m1_kpi_status { get; set; }

    [XmlElement("m2_kpi_idx")]
    public int m2_kpi_idx { get; set; }

    [XmlElement("perf_detail")]
    public string perf_detail { get; set; }

    [XmlElement("pref_point")]
    public int pref_point { get; set; }

    [XmlElement("pref_selected")]
    public int pref_selected { get; set; }

    [XmlElement("m2_kpi_status")]
    public int m2_kpi_status { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }


}

[Serializable]
public class pmsu0_DocFormDetail
{
    [XmlElement("condition")]
    public int condition { get; set; }

    [XmlElement("group_idx")]
    public int group_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("m1_typeidx")]
    public int m1_typeidx { get; set; }

    [XmlElement("u0_typeidx")]
    public int u0_typeidx { get; set; }

    [XmlElement("m1_coreidx")]
    public int m1_coreidx { get; set; }

    [XmlElement("unidx")]
    public int unidx { get; set; }

    [XmlElement("acidx")]
    public int acidx { get; set; }

    [XmlElement("staidx")]
    public int staidx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    
    [XmlElement("wg_idx")]
    public int wg_idx { get; set; }
    
    [XmlElement("lw_idx")]
    public int lw_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("TIDX")]
    public int TIDX { get; set; }

    [XmlElement("jobgrade_level")]
    public int jobgrade_level { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }

    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }

    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("pos_name")]
    public string pos_name { get; set; }

    [XmlElement("u0_docidx")]
    public int u0_docidx { get; set; }

    [XmlElement("sum_kpis_value")]
    public string sum_kpis_value { get; set; }

    [XmlElement("sum_mine_core_value")]
    public string sum_mine_core_value { get; set; }

    [XmlElement("sum_solid_core_value")]
    public string sum_solid_core_value { get; set; }

    [XmlElement("sum_dotted_core_value")]
    public string sum_dotted_core_value { get; set; }

    [XmlElement("sum_mine_competency")]
    public string sum_mine_competency { get; set; }

    [XmlElement("sum_dotted_competency")]
    public string sum_dotted_competency { get; set; }

    [XmlElement("sum_solid_competency")]
    public string sum_solid_competency { get; set; }

    [XmlElement("StatusDoc")]
    public string StatusDoc { get; set; }

    [XmlElement("form_name")]
    public string form_name { get; set; }

    [XmlElement("m0_typeidx")]
    public int m0_typeidx { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("time_create")]
    public string time_create { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("actor_name")]
    public string actor_name { get; set; }

    [XmlElement("countapprove")]
    public int countapprove { get; set; }

    [XmlElement("m2_typeidx")]
    public int m2_typeidx { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

    [XmlElement("email1")]
    public string email1 { get; set; }

    [XmlElement("email2")]
    public string email2 { get; set; }

    [XmlElement("email_hr")]
    public string email_hr { get; set; }

    [XmlElement("approve1")]
    public string approve1 { get; set; }

    [XmlElement("approve2")]
    public string approve2 { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("m0idx")]
    public int m0idx { get; set; }

    [XmlElement("getyear")]
    public int getyear { get; set; }

    [XmlElement("rdepidx_comma")]
    public string rdepidx_comma { get; set; }

    [XmlElement("qty")]
    public int qty { get; set; }

    [XmlElement("complete")]
    public int complete { get; set; }

    [XmlElement("uncomplete")]
    public int uncomplete { get; set; }

    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_kpi_idx")]
    public int m0_kpi_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("sum_point")]
    public string sum_point { get; set; }

    [XmlElement("m0_kpi_status")]
    public int m0_kpi_status { get; set; }

    [XmlElement("m1_kpi_idx")]
    public int m1_kpi_idx { get; set; }

    [XmlElement("perf_indicators")]
    public string perf_indicators { get; set; }

    [XmlElement("kpi_unit")]
    public string kpi_unit { get; set; }

    [XmlElement("kpi_weight")]
    public string kpi_weight { get; set; }

    [XmlElement("kpi_target")]
    public string kpi_target { get; set; }

    [XmlElement("m1_kpi_status")]
    public int m1_kpi_status { get; set; }

    [XmlElement("m2_kpi_idx")]
    public int m2_kpi_idx { get; set; }

    [XmlElement("perf_detail")]
    public string perf_detail { get; set; }

    [XmlElement("pref_point")]
    public int pref_point { get; set; }

    [XmlElement("pref_selected")]
    public int pref_selected { get; set; }

    [XmlElement("m2_kpi_status")]
    public int m2_kpi_status { get; set; }

    [XmlElement("noidx")]
    public int noidx { get; set; }

    [XmlElement("decision")]
    public int decision { get; set; }

    [XmlElement("current_status")]
    public string current_status { get; set; }

    [XmlElement("current_decision")]
    public string current_decision { get; set; }

    [XmlElement("current_actor")]
    public string current_actor { get; set; }

    [XmlElement("eval_emp_type")]
    public int eval_emp_type { get; set; }

    [XmlElement("eval_emp_type_name")]
    public string eval_emp_type_name { get; set; }

    [XmlElement("cal_ratio")]
    public decimal cal_ratio { get; set; }

    [XmlElement("eval_emp_idx")]
    public int eval_emp_idx { get; set; }

    [XmlElement("eval_emp_code")]
    public string eval_emp_code { get; set; }

    [XmlElement("eval_emp_name_th")]
    public string eval_emp_name_th { get; set; }

    [XmlElement("flag_doing")]
    public int flag_doing { get; set; }

    [XmlElement("approve_status")]
    public int approve_status { get; set; }

    [XmlElement("solid")]
    public string solid { get; set; }

    [XmlElement("dotted")]
    public string dotted { get; set; }

    [XmlElement("total_sum_kpis_value")]
    public string total_sum_kpis_value { get; set; }

    [XmlElement("total_corevalue")]
    public string total_corevalue { get; set; }

    [XmlElement("total_competencies")]
    public string total_competencies { get; set; }

    [XmlElement("total_score")]
    public string total_score { get; set; }

    [XmlElement("lw_name_th")]
    public string lw_name_th { get; set; }

    [XmlElement("wg_name_th")]
    public string wg_name_th { get; set; }


}

[Serializable]
public class pmsu1_DocFormDetail
{
    [XmlElement("u0_docidx")]
    public int u0_docidx { get; set; }

    [XmlElement("u1_docidx")]
    public int u1_docidx { get; set; }

    [XmlElement("m0_typeidx")]
    public int m0_typeidx { get; set; }

    [XmlElement("m1_typeidx")]
    public int m1_typeidx { get; set; }

    [XmlElement("m0_point_mine")]
    public string m0_point_mine { get; set; }

    [XmlElement("remark_mine")]
    public string remark_mine { get; set; }

    [XmlElement("m0_point_solid")]
    public string m0_point_solid { get; set; }

    [XmlElement("remark_solid")]
    public string remark_solid { get; set; }

    [XmlElement("m0_point_dotted")]
    public string m0_point_dotted { get; set; }

    [XmlElement("remark_dotted")]
    public string remark_dotted { get; set; }

    [XmlElement("type_core")]
    public string type_core { get; set; }

    [XmlElement("core_name")]
    public string core_name { get; set; }

    [XmlElement("m1_coreidx")]
    public int m1_coreidx { get; set; }

    [XmlElement("u0_typeidx")]
    public int u0_typeidx { get; set; }

    [XmlElement("m1_type_name")]
    public string m1_type_name { get; set; }
}

[Serializable]
public class pmsu2_DocFormDetail
{
    [XmlElement("u0_docidx")]
    public int u0_docidx { get; set; }

    [XmlElement("u2_docidx")]
    public int u2_docidx { get; set; }

    [XmlElement("m0_typeidx")]
    public int m0_typeidx { get; set; }

    [XmlElement("m0_typeidx_choose")]
    public int m0_typeidx_choose { get; set; }

    [XmlElement("behavior_name")]
    public string behavior_name { get; set; }

    [XmlElement("comment_name")]
    public string comment_name { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("FullNameTH")]
    public string FullNameTH { get; set; }

    [XmlElement("m1_typeidx_comma")]
    public string m1_typeidx_comma { get; set; }

    [XmlElement("typename")]
    public string typename { get; set; }

    [XmlElement("noidx")]
    public int noidx { get; set; }

}

[Serializable]
public class pmsm2_individualDetail
{
    [XmlElement("m2_kpi_idx")]
    public int m2_kpi_idx { get; set; }

    [XmlElement("m1_kpi_idx")]
    public int m1_kpi_idx { get; set; }

    [XmlElement("pref_selected")]
    public int pref_selected { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }


}

[Serializable]
public class pmsm0_factorDetail
{
    [XmlElement("m0_idx")]
    public int m0_idx { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("group_idx")]
    public int group_idx { get; set; }

    [XmlElement("cal_1")]
    public string cal_1 { get; set; }

    [XmlElement("cal_2")]
    public string cal_2 { get; set; }

    [XmlElement("cal_3")]
    public string cal_3 { get; set; }

    [XmlElement("cal_4")]
    public string cal_4 { get; set; }

    [XmlElement("cal_5")]
    public string cal_5 { get; set; }

    [XmlElement("cal_detail")]
    public string cal_detail { get; set; }

    [XmlElement("factor_name")]
    public string factor_name { get; set; }

    [XmlElement("score_before")]
    public string score_before { get; set; }

    [XmlElement("score_dotted_before")]
    public string score_dotted_before { get; set; }

    [XmlElement("score_solid_before")]
    public string score_solid_before { get; set; }

    [XmlElement("score_after")]
    public string score_after { get; set; }

    [XmlElement("total_score")]
    public string total_score { get; set; }


}

[Serializable]
public class pms_time_detail_m0
{
    [XmlElement("m0_time_idx")]
    public int m0_time_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("sick")]
    public string sick { get; set; }

    [XmlElement("sick_score")]
    public string sick_score { get; set; }

    [XmlElement("affair")]
    public string affair { get; set; }

    [XmlElement("affair_score")]
    public string affair_score { get; set; }

    [XmlElement("lack")]
    public string lack { get; set; }

    [XmlElement("lack_score")]
    public string lack_score { get; set; }

    [XmlElement("late")]
    public string late { get; set; }

    [XmlElement("late_score")]
    public string late_score { get; set; }

    [XmlElement("total_score")]
    public string total_score { get; set; }

    [XmlElement("time_performance_name")]
    public string time_performance_name { get; set; }

    [XmlElement("time_score_detail")]
    public string time_score_detail { get; set; }

    [XmlElement("score_detail")]
    public string score_detail { get; set; }

}

[Serializable]
public class pms_approve_detail_m0
{
    [XmlElement("m0_idx")]
    public int m0_idx { get; set; }

    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }

    [XmlElement("emp_code")]
    public string emp_code { get; set; }

    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("eval_emp_type")]
    public int eval_emp_type { get; set; }

    [XmlElement("eval_emp_type_name")]
    public string eval_emp_type_name { get; set; }

    [XmlElement("cal_ratio")]
    public decimal cal_ratio { get; set; }

    [XmlElement("eval_emp_idx")]
    public int eval_emp_idx { get; set; }

    [XmlElement("eval_emp_code")]
    public string eval_emp_code { get; set; }

    [XmlElement("eval_emp_name_th")]
    public string eval_emp_name_th { get; set; }

    [XmlElement("flag_doing")]
    public int flag_doing { get; set; }

    [XmlElement("approve_status")]
    public int approve_status { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("u0_docidx")]
    public int u0_docidx {get;set;}
}

[Serializable]
public class search_pms_report_detail {
    [XmlElement("s_emp_idx")]
    public string s_emp_idx { get; set; }
    [XmlElement("s_emp_code")]
    public string s_emp_code { get; set; }
    [XmlElement("s_emp_name")]
    public string s_emp_name { get; set; }
    [XmlElement("s_org_idx")]
    public string s_org_idx { get; set; }
    [XmlElement("s_wg_idx")]
    public string s_wg_idx { get; set; }
    [XmlElement("s_lw_idx")]
    public string s_lw_idx { get; set; }
    [XmlElement("s_dept_idx")]
    public string s_dept_idx { get; set; }
    [XmlElement("s_sec_idx")]
    public string s_sec_idx { get; set; }
    [XmlElement("s_pos_idx")]
    public string s_pos_idx { get; set; }
    [XmlElement("s_empgroup_idx")]
    public string s_empgroup_idx { get; set; }
}

[Serializable]
public class pms_report_detail {
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("wg_idx")]
    public int wg_idx { get; set; }
    [XmlElement("wg_name_th")]
    public string wg_name_th { get; set; }
    [XmlElement("lw_idx")]
    public int lw_idx { get; set; }
    [XmlElement("lw_name_th")]
    public string lw_name_th { get; set; }
    [XmlElement("dept_idx")]
    public int dept_idx { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("sec_idx")]
    public int sec_idx { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("empgroup_idx")]
    public int empgroup_idx { get; set; }
    [XmlElement("empgroup_name_th")]
    public string empgroup_name_th { get; set; }

    [XmlElement("emp_count")]
    public int emp_count { get; set; }
    [XmlElement("emp_count_not_start")]
    public int emp_count_not_start { get; set; }
    [XmlElement("emp_count_finished")]
    public int emp_count_finished { get; set; }
    [XmlElement("emp_count_myself")]
    public int emp_count_myself { get; set; }
    [XmlElement("emp_count_dotted")]
    public int emp_count_dotted { get; set; }
    [XmlElement("emp_count_solid")]
    public int emp_count_solid { get; set; }
    [XmlElement("emp_count_approve1")]
    public int emp_count_approve1 { get; set; }
    [XmlElement("emp_count_approve2")]
    public int emp_count_approve2 { get; set; }
    [XmlElement("emp_count_approve3")]
    public int emp_count_approve3 { get; set; }
}