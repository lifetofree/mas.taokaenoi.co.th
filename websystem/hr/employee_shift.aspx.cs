﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_employee_shift : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    private string ConCL = "conn_centralized";

    //dataODSP_Relation dtODSP = new dataODSP_Relation();
    data_employee _dtEmployee = new data_employee();

    //function_scan _funcScan = new function_scan();
    function_db _funcdb = new function_db();
    data_employee _dataEmployee = new data_employee();
    data_permission_set _dataPermission = new data_permission_set();
    data_emps _dataEmps = new data_emps();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];

    static string _urlSetApprove_employeeList = _serviceUrl + ConfigurationManager.AppSettings["urlSetApprove_employeeList"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string urlGetEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployee"];
    static string urlGetSelectParttime = _serviceUrl + ConfigurationManager.AppSettings["urlGetSelectParttime"];
    static string _urlGetSelectGroupDialy = _serviceUrl + ConfigurationManager.AppSettings["urlGetSelectGroupDialy"];
    static string _urlGetInsertShifttime = _serviceUrl + ConfigurationManager.AppSettings["urlGetInsertShifttime"];
    static string _urlGetSelectApprove = _serviceUrl + ConfigurationManager.AppSettings["urlGetSelectApprove"];
    static string _urlGetUpdateApprove = _serviceUrl + ConfigurationManager.AppSettings["urlGetUpdateApprove"];

    static string _urlGetSelectParttime = _serviceUrl + ConfigurationManager.AppSettings["urlGetSelectParttime"];
    static string _urlGetDetail_Permssion = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetail_Permssion"];

    int emp_idx = 0;
    int defaultInt = 0;
    string check_name_row = "";

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";
    int _tempInt = 0;
    string temp = "";

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = Session["emp_idx"].ToString();
            select_empIdx_present();
            permission_check();
            Menu_Color(1);
            MvMaster.SetActiveView(ViewIndex);
            TabName.Value = Request.Form[TabName.UniqueID];
            Select_Employee_index();         
        }
        linkBtnTrigger(lbkindex);
        linkBtnTrigger(lbkshiftday);
        linkBtnTrigger(lbkshiftmount);
        linkBtnTrigger(lbkshiftchange);
        linkBtnTrigger(btnholidayswap);
        linkBtnTrigger(lbkwaiting);
        linkBtnTrigger(lbkapprove);
        /*if (ViewState["EmpIDX"].ToString() == "173")
        {
            lbkhr.Visible = true;
        }
        else
        {
            lbkhr.Visible = false;
        }*/
        linkBtnTrigger(lbkhr);
        linkBtnTrigger(_divMenuBtnToDivManual);

    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    #region callService
    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_emps callService(string _cmdUrl, data_emps _dtmaster)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmaster);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtmaster = (data_emps)_funcTool.convertJsonToObject(typeof(data_emps), _localJson);

        return _dtmaster;
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_permission_set callServicePermission(string _cmdUrl, data_permission_set _dtmaster_)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmaster_);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtmaster_ = (data_permission_set)_funcTool.convertJsonToObject(typeof(data_permission_set), _localJson);

        return _dtmaster_;
    }
    #endregion

    #region Switch-Munu Color
    protected void Menu_Color(int choice)
    {
        switch (choice)
        {
            case 1: //Index รายการทั่วไป
                lbkindex.BackColor = System.Drawing.Color.LightGray;
                lbkshiftday.BackColor = System.Drawing.Color.Transparent;
                lbkshiftmount.BackColor = System.Drawing.Color.Transparent;
                lbkshiftchange.BackColor = System.Drawing.Color.Transparent;
                btnholidayswap.BackColor = System.Drawing.Color.Transparent;
                lbkwaiting.BackColor = System.Drawing.Color.Transparent;
                lbkapprove.BackColor = System.Drawing.Color.Transparent;
                lbkhr.BackColor = System.Drawing.Color.Transparent;

                DropDownList ddlIndexOrgSearch = (DropDownList)Fv_Search_Shift_Index.FindControl("ddlIndexOrgSearch");

                select_org(ddlIndexOrgSearch);
                ddlSec_edit_CreateAll();
                boxgv_shiftime.Visible = true;
                boxsearch_shiftime.Visible = true;
                boxedit_shiftime.Visible = false;

                //lbkshiftday.Visible = false;
                //lbkshiftmount.Visible = false;
                break;
            case 2: //รายวัน
                lbkindex.BackColor = System.Drawing.Color.Transparent;
                lbkshiftday.BackColor = System.Drawing.Color.LightGray;
                lbkshiftmount.BackColor = System.Drawing.Color.Transparent;
                lbkshiftchange.BackColor = System.Drawing.Color.Transparent;
                btnholidayswap.BackColor = System.Drawing.Color.Transparent;
                lbkwaiting.BackColor = System.Drawing.Color.Transparent;
                lbkapprove.BackColor = System.Drawing.Color.Transparent;
                lbkhr.BackColor = System.Drawing.Color.Transparent;

                BoxGroupDay.Visible = true;
                //BoxGroupMount.Visible = true;
                lblshiftype.Text = "รายวัน";
                ViewState["emptype_search"] = 1; //รายวัน

                //select_org(ddlorg);
                ddlSec_Add_CreateAll();
                select_groupname(ddlgroupname);

                GvEmployeeDB.DataSource = null;
                GvEmployeeDB.DataBind();
                GvGroupName.DataSource = null;
                GvGroupName.DataBind();

                GvEmployeeTemp.DataSource = null;
                GvEmployeeTemp.DataBind();
                GvGroupName_Temp.DataSource = null;
                GvGroupName_Temp.DataBind();

                var dsemp_1 = new DataSet();
                dsemp_1.Tables.Add("dsTemp_Emps");
                dsemp_1.Tables["dsTemp_Emps"].Columns.Add("emp_idx", typeof(int));
                dsemp_1.Tables["dsTemp_Emps"].Columns.Add("emp_code", typeof(string));
                dsemp_1.Tables["dsTemp_Emps"].Columns.Add("emp_name_th", typeof(string));
                ViewState["vsTemp_Emps"] = dsemp_1;

                break;
            case 3: //รายเดือน
                lbkindex.BackColor = System.Drawing.Color.Transparent;
                lbkshiftday.BackColor = System.Drawing.Color.Transparent;
                lbkshiftmount.BackColor = System.Drawing.Color.LightGray;
                lbkshiftchange.BackColor = System.Drawing.Color.Transparent;
                btnholidayswap.BackColor = System.Drawing.Color.Transparent;
                lbkwaiting.BackColor = System.Drawing.Color.Transparent;
                lbkapprove.BackColor = System.Drawing.Color.Transparent;
                lbkhr.BackColor = System.Drawing.Color.Transparent;

                BoxGroupDay.Visible = true;
                //BoxGroupMount.Visible = true;
                lblshiftype.Text = "รายเดือน";
                ViewState["emptype_search"] = 2; //รายเดือน
                //select_org(ddlorg);
                ddlSec_Add_CreateAll();
                select_groupname(ddlgroupname);

                GvEmployeeDB.DataSource = null;
                GvEmployeeDB.DataBind();
                GvGroupName.DataSource = null;
                GvGroupName.DataBind();

                GvEmployeeTemp.DataSource = null;
                GvEmployeeTemp.DataBind();
                GvGroupName_Temp.DataSource = null;
                GvGroupName_Temp.DataBind();

                var dsemp_2 = new DataSet();
                dsemp_2.Tables.Add("dsTemp_Emps");
                dsemp_2.Tables["dsTemp_Emps"].Columns.Add("emp_idx", typeof(int));
                dsemp_2.Tables["dsTemp_Emps"].Columns.Add("emp_code", typeof(string));
                dsemp_2.Tables["dsTemp_Emps"].Columns.Add("emp_name_th", typeof(string));
                ViewState["vsTemp_Emps"] = dsemp_2;

                break;
            case 4: //รายการที่รออนุมัติ
                lbkindex.BackColor = System.Drawing.Color.Transparent;
                lbkshiftday.BackColor = System.Drawing.Color.Transparent;
                lbkshiftmount.BackColor = System.Drawing.Color.Transparent;
                lbkshiftchange.BackColor = System.Drawing.Color.Transparent;
                btnholidayswap.BackColor = System.Drawing.Color.Transparent;
                lbkwaiting.BackColor = System.Drawing.Color.Transparent;
                lbkapprove.BackColor = System.Drawing.Color.LightGray;
                lbkhr.BackColor = System.Drawing.Color.Transparent;
                break;
            case 5: //ขอเปลี่ยนกะการทำงาน
                lbkindex.BackColor = System.Drawing.Color.Transparent;
                lbkshiftday.BackColor = System.Drawing.Color.Transparent;
                lbkshiftmount.BackColor = System.Drawing.Color.Transparent;
                lbkshiftchange.BackColor = System.Drawing.Color.LightGray;
                btnholidayswap.BackColor = System.Drawing.Color.Transparent;
                lbkwaiting.BackColor = System.Drawing.Color.Transparent;
                lbkapprove.BackColor = System.Drawing.Color.Transparent;
                lbkhr.BackColor = System.Drawing.Color.Transparent;
                break;
            /*case 5: //ขอสลับวันหยุด
                lbkindex.BackColor = System.Drawing.Color.Transparent;
                lbkshiftday.BackColor = System.Drawing.Color.Transparent;
                lbkshiftmount.BackColor = System.Drawing.Color.Transparent;
                lbkshiftchange.BackColor = System.Drawing.Color.Transparent;
                btnholidayswap.BackColor = System.Drawing.Color.LightGray;
                lbkwaiting.BackColor = System.Drawing.Color.Transparent;
                lbkapprove.BackColor = System.Drawing.Color.Transparent;
                lbkhr.BackColor = System.Drawing.Color.Transparent;
                break;*/
            case 6: //รายการที่รอแก้ไข
                lbkindex.BackColor = System.Drawing.Color.Transparent;
                lbkshiftday.BackColor = System.Drawing.Color.Transparent;
                lbkshiftmount.BackColor = System.Drawing.Color.Transparent;
                lbkshiftchange.BackColor = System.Drawing.Color.Transparent;
                btnholidayswap.BackColor = System.Drawing.Color.Transparent;
                lbkwaiting.BackColor = System.Drawing.Color.LightGray;
                lbkapprove.BackColor = System.Drawing.Color.Transparent;
                lbkhr.BackColor = System.Drawing.Color.Transparent;

                break;

            case 8: //ส่วนของ HR
                lbkindex.BackColor = System.Drawing.Color.Transparent;
                lbkshiftday.BackColor = System.Drawing.Color.Transparent;
                lbkshiftmount.BackColor = System.Drawing.Color.Transparent;
                lbkshiftchange.BackColor = System.Drawing.Color.Transparent;
                btnholidayswap.BackColor = System.Drawing.Color.Transparent;
                lbkwaiting.BackColor = System.Drawing.Color.Transparent;
                lbkapprove.BackColor = System.Drawing.Color.Transparent;
                lbkhr.BackColor = System.Drawing.Color.LightGray;

                Select_Employee_Index_Report();
                //Select_Employee_Report();

                FvSearch_Report.ChangeMode(FormViewMode.Insert);
                FvSearch_Report.DataBind();
                DropDownList ddl_org_report = (DropDownList)FvSearch_Report.FindControl("ddl_org_report");
                //DropDownList ddl_year_report = (DropDownList)FvSearch_Report.FindControl("ddl_year_report");
                //ddl_year_report.SelectedValue = "2019";
                select_org(ddl_org_report);
                break;
        }
    }

    protected void Menu_Color_Page_Add(int choice)
    {
        switch (choice)
        {
            case 1: //Group DB
                lbListGroup.BackColor = System.Drawing.Color.LightGray;
                lbListEmp.BackColor = System.Drawing.Color.Transparent;
                BoxSearchGroup.Visible = true;
                BoxSearchListEmp.Visible = false;
                break;
            case 2: //Emp DB
                lbListGroup.BackColor = System.Drawing.Color.Transparent;
                lbListEmp.BackColor = System.Drawing.Color.LightGray;
                BoxSearchGroup.Visible = false;
                BoxSearchListEmp.Visible = true;
                break;
            case 3: //Group Temp
                lbListGroup_temp.BackColor = System.Drawing.Color.LightGray;
                lbListEmp_temp.BackColor = System.Drawing.Color.Transparent;
                BoxSearchGroup_Temp.Visible = true;
                BoxSearchListEmp_Temp.Visible = false;
                break;
            case 4: //Emp Temp
                lbListGroup_temp.BackColor = System.Drawing.Color.Transparent;
                lbListEmp_temp.BackColor = System.Drawing.Color.LightGray;
                BoxSearchGroup_Temp.Visible = false;
                BoxSearchListEmp_Temp.Visible = true;
                break;

            case 5: //Group DB Edit
                lbListGroup_edit.BackColor = System.Drawing.Color.LightGray;
                lbListEmp_edit.BackColor = System.Drawing.Color.Transparent;
                BoxSearchGroup_edit.Visible = true;
                BoxSearchListEmp_edit.Visible = false;
                break;
            case 6: //Emp DB Edit
                lbListGroup_edit.BackColor = System.Drawing.Color.Transparent;
                lbListEmp_edit.BackColor = System.Drawing.Color.LightGray;
                BoxSearchGroup_edit.Visible = false;
                BoxSearchListEmp_edit.Visible = true;
                break;
            case 7: //Emp Temp Edit
                lbListGroup_temp_edit.BackColor = System.Drawing.Color.Transparent;
                lbListEmp_temp_edit.BackColor = System.Drawing.Color.LightGray;
                BoxSearchGroup_Temp_edit.Visible = false;
                BoxSearchListEmp_Temp_edit.Visible = true;
                break;
            case 8: //Group Temp Edit
                lbListGroup_temp_edit.BackColor = System.Drawing.Color.LightGray;
                lbListEmp_temp_edit.BackColor = System.Drawing.Color.Transparent;
                BoxSearchGroup_Temp_edit.Visible = true;
                BoxSearchListEmp_Temp_edit.Visible = false;
                break;
            case 9: //Emp Temp Add
                lbListGroup_temp_add.BackColor = System.Drawing.Color.Transparent;
                lbListEmp_temp_add.BackColor = System.Drawing.Color.LightGray;
                BoxSearchGroup_Temp_add.Visible = false;
                BoxSearchListEmp_Temp_add.Visible = true;
                break;
            case 10: //Group Temp Add
                lbListGroup_temp_add.BackColor = System.Drawing.Color.LightGray;
                lbListEmp_temp_add.BackColor = System.Drawing.Color.Transparent;
                BoxSearchGroup_Temp_add.Visible = true;
                BoxSearchListEmp_Temp_add.Visible = false;
                break;
            case 11: //Emp Temp approve
                lbListGroup_temp_approve.BackColor = System.Drawing.Color.Transparent;
                lbListEmp_temp_approve.BackColor = System.Drawing.Color.LightGray;
                BoxSearchGroup_Temp_approve.Visible = false;
                BoxSearchListEmp_Temp_approve.Visible = true;
                break;
            case 12: //Group Temp approve
                lbListGroup_temp_approve.BackColor = System.Drawing.Color.LightGray;
                lbListEmp_temp_approve.BackColor = System.Drawing.Color.Transparent;
                BoxSearchGroup_Temp_approve.Visible = true;
                BoxSearchListEmp_Temp_approve.Visible = false;
                break;

        }
    }

    protected void permission_check()
    {
        _dataPermission.permission_list = new permission_set_detail[1];
        permission_set_detail _perList = new permission_set_detail();
        _perList.p_system = 2; // empshift
        _perList.p_permission_type = 1; //create
        _perList.rpos_idx_set = ViewState["Pos_idx"].ToString();
        _perList.type_action = 1;
        _dataPermission.permission_list[0] = _perList;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataPermission));
        _dataPermission = callServicePermission(_urlGetDetail_Permssion, _dataPermission);
        ViewState["ArrayRsec"] = "0";

        if (_dataPermission.return_code.ToString() == "0")
        {
            ViewState["permission_check"] = 1; //มีสิทธิ์
            Check_ddl_Permission(int.Parse(ViewState["Pos_idx"].ToString()),int.Parse(ViewState["Sec_idx"].ToString()));
        }
        else
        {
            ViewState["ArrayRsec"] = 0;
            ViewState["permission_check"] = 0;
        }

        if (ViewState["permission_check"].ToString() == "1") // เปิด เมนู
        {
            lbkshiftday.Visible = true;
            lbkshiftmount.Visible = true;
        }
        else
        {
            lbkshiftday.Visible = true;
            lbkshiftmount.Visible = true;
            /*if (ViewState["emptype_search"].ToString() == "1") //day
            {
                lbkshiftday.Visible = true;
                lbkshiftmount.Visible = false;
            }
            else if (ViewState["emptype_search"].ToString() == "2") //mounth
            {
                lbkshiftday.Visible = false;
                lbkshiftmount.Visible = true;
            }
            else
            {
                lbkshiftday.Visible = false;
                lbkshiftmount.Visible = false;
            }*/
        }

        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataPermission));
        //fs.Text = ViewState["permission_check"].ToString();
    }

    protected void Check_ddl_Permission(int RposIDX, int RsceIDX)
    {
        ViewState["ArrayRsec"] = null;

        _dataPermission.permission_list = new permission_set_detail[1];
        permission_set_detail _perList = new permission_set_detail();
        _perList.p_system = 2; // empshift
        _perList.p_permission_type = 1; //create
        _perList.rpos_idx_set = RposIDX.ToString();
        _perList.type_action = 2;
        _perList.p_status = 1;
        _dataPermission.permission_list[0] = _perList;
        //fds.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataPermission));
        _dataPermission = callServicePermission(_urlGetDetail_Permssion, _dataPermission);
        ViewState["ArrayRsec"] = "0";
        //aaa
        if (_dataPermission.return_code != "0")
        {
            ViewState["ArrayRsec"] = _dataPermission.return_code;
            ViewState["permission_check"] = 1; //มีสิทธิ์
        }
        else
        {
            ViewState["ArrayRsec"] = 0; ;
        }

        //fs.Text = ViewState["ArrayRsec"].ToString();
    }

    protected void ddlSec_Add_CreateAll()
    {
        ddlsec.AppendDataBoundItems = true;
        ddlsec.Items.Clear();
        ddlsec.Items.Add(new ListItem("เลือกหน่วยงาน....", "0"));
        ddlsec.SelectedValue = "0";

        _dataPermission.permission_list = new permission_set_detail[1];
        permission_set_detail _perList = new permission_set_detail();
        _perList.p_system = 2; // empshift
        _perList.p_permission_type = 1; //create
        _perList.rsec_idx_set = ViewState["ArrayRsec"].ToString();
        _perList.rpos_idx_set = ViewState["Pos_idx"].ToString();
        _perList.type_action = 3;
        _perList.p_status = 1;
        _dataPermission.permission_list[0] = _perList;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataPermission));
        _dataPermission = callServicePermission(_urlGetDetail_Permssion, _dataPermission);

        ddlsec.DataSource = _dataPermission.permission_list;
        ddlsec.DataTextField = "SecNameTH";
        ddlsec.DataValueField = "RSecIDX";
        ddlsec.DataBind();
        //setDdlData(ddlsec, _dataPermission.permission_list, "SecNameTH", "RSecIDX");
    }

    protected void ddlSec_edit_CreateAll()
    {
        ddlsec_add.AppendDataBoundItems = true;
        ddlsec_add.Items.Clear();
        ddlsec_add.Items.Add(new ListItem("เลือกหน่วยงาน....", "0"));
        ddlsec_add.SelectedValue = "0";

        _dataPermission.permission_list = new permission_set_detail[1];
        permission_set_detail _perList = new permission_set_detail();
        _perList.p_system = 2; // empshift
        _perList.p_permission_type = 1; //create
        _perList.rsec_idx_set = ViewState["ArrayRsec"].ToString();
        _perList.rpos_idx_set = ViewState["Pos_idx"].ToString();
        _perList.type_action = 3;
        _perList.p_status = 1;
        _dataPermission.permission_list[0] = _perList;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataPermission));
        _dataPermission = callServicePermission(_urlGetDetail_Permssion, _dataPermission);

        ddlsec_add.DataSource = _dataPermission.permission_list;
        ddlsec_add.DataTextField = "SecNameTH";
        ddlsec_add.DataValueField = "RSecIDX";
        ddlsec_add.DataBind();
        //setDdlData(ddlsec, _dataPermission.permission_list, "SecNameTH", "RSecIDX");
    }

    protected void ddlPos_Add_CreateAll(int rsec_idx , DropDownList ddl)
    {
        ddl.AppendDataBoundItems = true;
        ddl.Items.Clear();
        ddl.Items.Add(new ListItem("เลือกตำแหน่งงาน....", "0"));
        ddl.SelectedValue = "0";

        _dataPermission.permission_list = new permission_set_detail[1];
        permission_set_detail _perList = new permission_set_detail();
        _perList.p_system = 2; // empshift
        _perList.p_permission_type = 1; //create
        _perList.rsec_idx_set = rsec_idx.ToString();
        _perList.type_action = 4; //ddl_pos
        _perList.p_status = 1;
        _dataPermission.permission_list[0] = _perList;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataPermission));
        _dataPermission = callServicePermission(_urlGetDetail_Permssion, _dataPermission);

        ddl.DataSource = _dataPermission.permission_list;
        ddl.DataTextField = "PosNameTH";
        ddl.DataValueField = "RPosIDX";
        ddl.DataBind();
        //setDdlData(ddlsec, _dataPermission.permission_list, "SecNameTH", "RSecIDX");
    }

    protected string Check_holiday(string To_Day)
    {
        var day = "";

        if (To_Day == "1")
        {
            day = "SUN" + " ";
        }
        else if (To_Day == "2")
        {
            day = "MON" + " ";
        }
        else if (To_Day == "3")
        {
            day = "TUE" + " ";
        }
        else if (To_Day == "4")
        {
            day = "WED" + " ";
        }
        else if (To_Day == "5")
        {
            day = "THU" + " ";
        }
        else if (To_Day == "6")
        {
            day = "FRI" + " ";
        }
        else if (To_Day == "7")
        {
            day = "SAT" + " ";
        }
        else
        {
            day = "";
        }

        return day;
    }

    protected string check_holiday_2(string To_Day)
    {
        var day = "";

        if (To_Day == "1")
        {
            day = "อา";
        }
        else if (To_Day == "2")
        {
            day = "จ";
        }
        else if (To_Day == "3")
        {
            day = "อ";
        }
        else if (To_Day == "4")
        {
            day = "พ";
        }
        else if (To_Day == "5")
        {
            day = "พฤ";
        }
        else if (To_Day == "6")
        {
            day = "ศ";
        }
        else if (To_Day == "7")
        {
            day = "ส";
        }
        else
        {
            day = "";
        }

        return day;
    }

    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setRptData(Repeater RpName, Object obj)
    {
        RpName.DataSource = obj;
        RpName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvIndex":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView GvIndex = (GridView)ViewIndex.FindControl("GvIndex");
                    if (GvIndex.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lbl_countdown = ((Label)e.Row.FindControl("lbl_countdown"));
                        var lblu0_unidx = ((Label)e.Row.FindControl("lblu0_unidx"));
                        var lblu0_acidx = ((Label)e.Row.FindControl("lblu0_acidx"));
                        var lblu0_doc_decision = ((Label)e.Row.FindControl("lblu0_doc_decision"));
                        var btnedit = ((LinkButton)e.Row.FindControl("btnedit"));
                        if (lblu0_unidx.Text == "2" && lblu0_acidx.Text == "2" && lblu0_doc_decision.Text == "1")
                        {
                            btnedit.Visible = true;
                        }
                        else if (lblu0_unidx.Text == "1" && lblu0_acidx.Text == "1" && lblu0_doc_decision.Text == "3")
                        {
                            btnedit.Visible = true;
                        }
                        else
                        {
                            btnedit.Visible = false;
                        }
                    }
                }
                break;
            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    /*var txtEditorgIDX_Select = (TextBox)e.Row.FindControl("txtEditorgIDX_Select");
                    var txtEditDepIDX_Select = (TextBox)e.Row.FindControl("txtEditDepIDX_Select");
                    var txtEditSecIDX_Select = (TextBox)e.Row.FindControl("txtEditSecIDX_Select");

                    var ddlOrganizationEdit_Select = (DropDownList)e.Row.FindControl("ddlOrganizationEdit_Select");
                    var ddlDepartmentEdit_Select = (DropDownList)e.Row.FindControl("ddlDepartmentEdit_Select");
                    var ddlSectionEdit_Select = (DropDownList)e.Row.FindControl("ddlSectionEdit_Select");

                    select_org(ddlOrganizationEdit_Select);
                    ddlOrganizationEdit_Select.SelectedValue = txtEditorgIDX_Select.Text;
                    select_dep(ddlDepartmentEdit_Select, int.Parse(txtEditorgIDX_Select.Text));
                    ddlDepartmentEdit_Select.SelectedValue = txtEditDepIDX_Select.Text;
                    //select_sec(ddlSectionEdit_Select, int.Parse(txtEditorgIDX_Select.Text), int.Parse(txtEditDepIDX_Select.Text));
                    ddlSectionEdit_Select.SelectedValue = txtEditSecIDX_Select.Text;*/
                }

                break;
            case "GvEmployeeTemp":
                string aa = "";
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvEmployeeTemp.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lbl_emp_code = ((Label)e.Row.FindControl("lbfm_name1"));
                        aa += lbl_emp_code.Text.ToString() + ",";

                    }

                    foreach (GridViewRow row in GvEmployeeTemp.Rows)
                    {
                        //if (row.RowType == DataControlRowType.DataRow)
                        //{
                            Label lbfm_emp_code = (Label)row.Cells[1].FindControl("lbfm_name1");
                            if (lbfm_emp_code.Text == "57000001")
                            {
                                //fs.Text = "Yes";
                                //GvEmployeeTemp.Rows[row].Visible = false;
                            }
                            else
                            {
                                //fs.Text = "No";
                            }

                        //}
                    }
                    //fs.Text += aa.ToString();

                }
                

                /*if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridView GvEmployeeTemp = (GridView)ViewShift.FindControl("GvEmployeeTemp");
                    if (GvEmployeeTemp.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        //GridView gv = (GridView)ViewShift.FindControl("GvEmployeeTemp");
                        var lbl_emp_code = ((Label)e.Row.FindControl("lbfm_name1"));

                        if (lbl_emp_code.Text.ToString() == "57000001" )
                        {
                            GvEmployeeTemp.Rows[0].Visible = false;
                        }

                    }*/

                /*string oldValue = string.Empty;
                string newValue = string.Empty;
                for (int j = 0; j < 1; j++)
                {
                    for (int count = 0; count < GvEmployeeTemp.Rows.Count; count++)
                    {
                        oldValue = GvEmployeeTemp.Rows[count].Cells[j].Text;
                        if (oldValue == newValue)
                        {
                            GvEmployeeTemp.Rows[count].Visible = false;
                        }
                        newValue = oldValue;
                    }
                }*/

                //cast the viewstate as a datatable 
                /*DataTable dt = ViewState["vsTemp_Emps_1"] as DataTable;

                //find the nested grid and cast it 
                //GridView gv = (GridView)e.Row.FindControl("GvEmployeeTemp");

                //get the CourseID from the main grid 
                DataRowView drv = e.Row.DataItem as DataRowView;
                string CourseID = (drv["emp_code"]).ToString();

                //filter the datatable with Linq to display only the one row needed and bind it to the nested grid 
                gv.DataSource = dt.AsEnumerable().Where(myRow => myRow.Field<string>("emp_code") == CourseID).CopyToDataTable();
                //gv.DataSource = dt.AsEnumerable().GroupBy(x => x.Field<string>("CourseID") == CourseID).Select(g => g.First()).CopyToDataTable();
                gv.DataBind();*/



                /*if (GvEmployeeTemp.EditIndex != e.Row.RowIndex) //to overlook header row
                {
                    var lb_emp_code = ((Label)e.Row.FindControl("lbfm_name1"));

                    /*if (lb_emp_code.Text == "57000001")
                    {
                        //e.Row.Visible = false;
                        //GridView GvEmployeeTemp_mount_ = (GridView)ViewShift.FindControl("GvEmployeeTemp");


                        for (int counter = 0; counter < dsD_sw_1.Tables[0].Rows.Count; counter++)
                        {
                            if (dsD_sw_1.Tables[0].Rows[counter]["emp_code"].ToString() == "") //ViewState["emp_idx_mount"].ToString())
                            {
                                dsD_sw_1.Tables[0].Rows[counter].Delete();
                                break;
                            }
                        }
                    }*/

                //var lbl_countdown = ((Label)e.Row.FindControl("lbl_countdown"));
                //var lblu0_unidx = ((Label)e.Row.FindControl("lblu0_unidx"));
                //var lblu0_acidx = ((Label)e.Row.FindControl("lblu0_acidx"));
                //var lblu0_doc_decision = ((Label)e.Row.FindControl("lblu0_doc_decision"));
                //var btnedit = ((LinkButton)e.Row.FindControl("btnedit"));

                //}
                //}



                break;
            case "GvEmployee_Report":

                if (e.Row.RowType == DataControlRowType.Header)
                {
                    e.Row.Visible = false;

                    AddHeaderRow1();
                    AddHeaderRow3();                  
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    GridView GvEmployee_Report = (GridView)ViewHr.FindControl("GvEmployee_Report");
                    if (GvEmployee_Report.EditIndex != e.Row.RowIndex) //to overlook header row
                    {

                        var lblcheck_1 = ((Label)e.Row.FindControl("lblcheck_1"));
                        var lblcheck_2 = ((Label)e.Row.FindControl("lblcheck_2"));
                        var lblcheck_3 = ((Label)e.Row.FindControl("lblcheck_3"));
                        var lblcheck_4 = ((Label)e.Row.FindControl("lblcheck_4"));
                        var lblcheck_5 = ((Label)e.Row.FindControl("lblcheck_5"));
                        var lblcheck_6 = ((Label)e.Row.FindControl("lblcheck_6"));
                        var lblcheck_7 = ((Label)e.Row.FindControl("lblcheck_7"));
                        var lblcheck_8 = ((Label)e.Row.FindControl("lblcheck_8"));
                        var lblcheck_9 = ((Label)e.Row.FindControl("lblcheck_9"));
                        var lblcheck_10 = ((Label)e.Row.FindControl("lblcheck_10"));
                        var lblcheck_11 = ((Label)e.Row.FindControl("lblcheck_11"));
                        var lblcheck_12 = ((Label)e.Row.FindControl("lblcheck_12"));
                        var lblcheck_13 = ((Label)e.Row.FindControl("lblcheck_13"));
                        var lblcheck_14 = ((Label)e.Row.FindControl("lblcheck_14"));
                        var lblcheck_15 = ((Label)e.Row.FindControl("lblcheck_15"));
                        var lblcheck_16 = ((Label)e.Row.FindControl("lblcheck_16"));
                        var lblcheck_17 = ((Label)e.Row.FindControl("lblcheck_17"));
                        var lblcheck_18 = ((Label)e.Row.FindControl("lblcheck_18"));
                        var lblcheck_19 = ((Label)e.Row.FindControl("lblcheck_19"));
                        var lblcheck_20 = ((Label)e.Row.FindControl("lblcheck_20"));
                        var lblcheck_21 = ((Label)e.Row.FindControl("lblcheck_21"));
                        var lblcheck_22 = ((Label)e.Row.FindControl("lblcheck_22"));
                        var lblcheck_23 = ((Label)e.Row.FindControl("lblcheck_23"));
                        var lblcheck_24 = ((Label)e.Row.FindControl("lblcheck_24"));
                        var lblcheck_25 = ((Label)e.Row.FindControl("lblcheck_25"));
                        var lblcheck_26 = ((Label)e.Row.FindControl("lblcheck_26"));
                        var lblcheck_27 = ((Label)e.Row.FindControl("lblcheck_27"));
                        var lblcheck_28 = ((Label)e.Row.FindControl("lblcheck_28"));
                        var lblcheck_29 = ((Label)e.Row.FindControl("lblcheck_29"));
                        var lblcheck_30 = ((Label)e.Row.FindControl("lblcheck_30"));
                        var lblcheck_31 = ((Label)e.Row.FindControl("lblcheck_31"));
                        var lblholiday = ((Label)e.Row.FindControl("lblholiday"));
                        var lblshow_holiday = ((Label)e.Row.FindControl("lblshow_holiday"));

                        var lblday_1 = ((Label)e.Row.FindControl("lblday_1"));
                        var lblday_2 = ((Label)e.Row.FindControl("lblday_2"));
                        var lblday_3 = ((Label)e.Row.FindControl("lblday_3"));
                        var lblday_4 = ((Label)e.Row.FindControl("lblday_4"));
                        var lblday_5 = ((Label)e.Row.FindControl("lblday_5"));
                        var lblday_6 = ((Label)e.Row.FindControl("lblday_6"));
                        var lblday_7 = ((Label)e.Row.FindControl("lblday_7"));
                        var lblday_8 = ((Label)e.Row.FindControl("lblday_8"));
                        var lblday_9 = ((Label)e.Row.FindControl("lblday_9"));
                        var lblday_10 = ((Label)e.Row.FindControl("lblday_10"));
                        var lblday_11 = ((Label)e.Row.FindControl("lblday_11"));
                        var lblday_12 = ((Label)e.Row.FindControl("lblday_12"));
                        var lblday_13 = ((Label)e.Row.FindControl("lblday_13"));
                        var lblday_14 = ((Label)e.Row.FindControl("lblday_14"));
                        var lblday_15 = ((Label)e.Row.FindControl("lblday_15"));
                        var lblday_16 = ((Label)e.Row.FindControl("lblday_16"));
                        var lblday_17 = ((Label)e.Row.FindControl("lblday_17"));
                        var lblday_18 = ((Label)e.Row.FindControl("lblday_18"));
                        var lblday_19 = ((Label)e.Row.FindControl("lblday_19"));
                        var lblday_20 = ((Label)e.Row.FindControl("lblday_20"));
                        var lblday_21 = ((Label)e.Row.FindControl("lblday_21"));
                        var lblday_22 = ((Label)e.Row.FindControl("lblday_22"));
                        var lblday_23 = ((Label)e.Row.FindControl("lblday_23"));
                        var lblday_24 = ((Label)e.Row.FindControl("lblday_24"));
                        var lblday_25 = ((Label)e.Row.FindControl("lblday_25"));
                        var lblday_26 = ((Label)e.Row.FindControl("lblday_26"));
                        var lblday_27 = ((Label)e.Row.FindControl("lblday_27"));
                        var lblday_28 = ((Label)e.Row.FindControl("lblday_28"));
                        var lblday_29 = ((Label)e.Row.FindControl("lblday_29"));
                        var lblday_30 = ((Label)e.Row.FindControl("lblday_30"));
                        var lblday_31 = ((Label)e.Row.FindControl("lblday_31"));

                        string To = lblholiday.Text.ToString();
                        string[] ToId = To.Split(',');
                        int _i = 0;
                        foreach (string To_Day in ToId)
                        {
                            if (To_Day != String.Empty)
                            {
                                if (To_Day != "0")
                                {
                                    #region Check Holiday

                                    if (lblcheck_1.Text.ToString() == To_Day) { e.Row.Cells[4].BackColor = Color.LightGray;
                                        string sub = lblday_1.Text.ToString().Substring(0, 1);
                                        if (sub == "D") {
                                            lblday_1.Text = lblday_1.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_1.Text = lblday_1.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_2.Text.ToString() == To_Day) { e.Row.Cells[5].BackColor = Color.LightGray;
                                        string sub = lblday_2.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_2.Text = lblday_2.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_2.Text = lblday_2.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_3.Text.ToString() == To_Day) { e.Row.Cells[6].BackColor = Color.LightGray;
                                        string sub = lblday_3.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_3.Text = lblday_3.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_3.Text = lblday_3.Text.ToString().Replace("N", "NH");
                                        }
                                    }                         
                                    if (lblcheck_4.Text.ToString() == To_Day) { e.Row.Cells[7].BackColor = Color.LightGray;
                                        string sub = lblday_4.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_4.Text = lblday_4.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_4.Text = lblday_4.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_5.Text.ToString() == To_Day) { e.Row.Cells[8].BackColor = Color.LightGray;
                                        string sub = lblday_5.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_5.Text = lblday_5.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_5.Text = lblday_5.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_6.Text.ToString() == To_Day) { e.Row.Cells[9].BackColor = Color.LightGray;
                                        string sub = lblday_6.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_6.Text = lblday_6.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_6.Text = lblday_6.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_7.Text.ToString() == To_Day) { e.Row.Cells[10].BackColor = Color.LightGray;
                                        string sub = lblday_7.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_7.Text = lblday_7.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_7.Text = lblday_7.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_8.Text.ToString() == To_Day) { e.Row.Cells[11].BackColor = Color.LightGray;
                                        string sub = lblday_8.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_8.Text = lblday_8.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_8.Text = lblday_8.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_9.Text.ToString() == To_Day) { e.Row.Cells[12].BackColor = Color.LightGray;
                                        string sub = lblday_9.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_9.Text = lblday_9.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_9.Text = lblday_9.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_10.Text.ToString() == To_Day) { e.Row.Cells[13].BackColor = Color.LightGray;
                                        string sub = lblday_10.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_10.Text = lblday_10.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_10.Text = lblday_10.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_11.Text.ToString() == To_Day) { e.Row.Cells[14].BackColor = Color.LightGray;
                                        string sub = lblday_11.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_11.Text = lblday_11.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_11.Text = lblday_11.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_12.Text.ToString() == To_Day) { e.Row.Cells[15].BackColor = Color.LightGray;
                                        string sub = lblday_12.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_12.Text = lblday_12.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_12.Text = lblday_12.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_13.Text.ToString() == To_Day) { e.Row.Cells[16].BackColor = Color.LightGray;
                                        string sub = lblday_13.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_13.Text = lblday_13.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_13.Text = lblday_13.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_14.Text.ToString() == To_Day) { e.Row.Cells[17].BackColor = Color.LightGray;
                                        string sub = lblday_14.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_14.Text = lblday_14.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_14.Text = lblday_14.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_15.Text.ToString() == To_Day) { e.Row.Cells[18].BackColor = Color.LightGray;
                                        string sub = lblday_15.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_15.Text = lblday_15.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_15.Text = lblday_15.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_16.Text.ToString() == To_Day) { e.Row.Cells[19].BackColor = Color.LightGray;
                                        string sub = lblday_16.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_16.Text = lblday_16.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_16.Text = lblday_16.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_17.Text.ToString() == To_Day) { e.Row.Cells[20].BackColor = Color.LightGray;
                                        string sub = lblday_17.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_17.Text = lblday_17.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_17.Text = lblday_17.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_18.Text.ToString() == To_Day) { e.Row.Cells[21].BackColor = Color.LightGray;
                                        string sub = lblday_18.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_18.Text = lblday_18.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_18.Text = lblday_18.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_19.Text.ToString() == To_Day) { e.Row.Cells[22].BackColor = Color.LightGray;
                                        string sub = lblday_19.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_19.Text = lblday_19.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_19.Text = lblday_19.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_20.Text.ToString() == To_Day) { e.Row.Cells[23].BackColor = Color.LightGray;
                                        string sub = lblday_20.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_20.Text = lblday_20.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_20.Text = lblday_20.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_21.Text.ToString() == To_Day) { e.Row.Cells[24].BackColor = Color.LightGray;
                                        string sub = lblday_21.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_21.Text = lblday_21.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_21.Text = lblday_21.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_22.Text.ToString() == To_Day) { e.Row.Cells[25].BackColor = Color.LightGray;
                                        string sub = lblday_22.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_22.Text = lblday_22.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_22.Text = lblday_22.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_23.Text.ToString() == To_Day) { e.Row.Cells[26].BackColor = Color.LightGray;
                                        string sub = lblday_23.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_23.Text = lblday_23.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_23.Text = lblday_23.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_24.Text.ToString() == To_Day) { e.Row.Cells[27].BackColor = Color.LightGray;
                                        string sub = lblday_24.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_24.Text = lblday_24.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_24.Text = lblday_24.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_25.Text.ToString() == To_Day) { e.Row.Cells[28].BackColor = Color.LightGray;
                                        string sub = lblday_25.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_25.Text = lblday_25.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_25.Text = lblday_25.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_26.Text.ToString() == To_Day) { e.Row.Cells[29].BackColor = Color.LightGray;
                                        string sub = lblday_26.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_26.Text = lblday_26.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_26.Text = lblday_26.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_27.Text.ToString() == To_Day) { e.Row.Cells[30].BackColor = Color.LightGray;
                                        string sub = lblday_27.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_27.Text = lblday_27.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_27.Text = lblday_27.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_28.Text.ToString() == To_Day) { e.Row.Cells[31].BackColor = Color.LightGray;
                                        string sub = lblday_28.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_28.Text = lblday_28.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_28.Text = lblday_28.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_29.Text.ToString() == To_Day) { e.Row.Cells[32].BackColor = Color.LightGray;
                                        string sub = lblday_29.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_29.Text = lblday_29.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_29.Text = lblday_29.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_30.Text.ToString() == To_Day) { e.Row.Cells[33].BackColor = Color.LightGray;
                                        string sub = lblday_30.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_30.Text = lblday_30.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_30.Text = lblday_30.Text.ToString().Replace("N", "NH");
                                        }
                                    }
                                    if (lblcheck_31.Text.ToString() == To_Day) { e.Row.Cells[34].BackColor = Color.LightGray;
                                        string sub = lblday_31.Text.ToString().Substring(0, 1);
                                        if (sub == "D")
                                        {
                                            lblday_31.Text = lblday_31.Text.ToString().Replace("D", "DH");
                                        }
                                        else
                                        {
                                            lblday_31.Text = lblday_31.Text.ToString().Replace("N", "NH");
                                        }
                                    }

                                    #endregion

                                    lblshow_holiday.Text += Check_holiday(To_Day);
                                }
                            }
                            _i++;
                            
                        }

                        AddHeaderRow2(lblcheck_1.Text.ToString(), lblcheck_2.Text.ToString(), lblcheck_3.Text.ToString(), lblcheck_4.Text.ToString(), lblcheck_5.Text.ToString(), lblcheck_6.Text.ToString(), lblcheck_7.Text.ToString(), lblcheck_8.Text.ToString(), lblcheck_9.Text.ToString(), lblcheck_10.Text.ToString(), lblcheck_11.Text.ToString(), lblcheck_12.Text.ToString(), lblcheck_13.Text.ToString(), lblcheck_14.Text.ToString(), lblcheck_15.Text.ToString(), lblcheck_16.Text.ToString(), lblcheck_17.Text.ToString(), lblcheck_18.Text.ToString(), lblcheck_19.Text.ToString(), lblcheck_20.Text.ToString(), lblcheck_21.Text.ToString(), lblcheck_22.Text.ToString(), lblcheck_23.Text.ToString(), lblcheck_24.Text.ToString(), lblcheck_25.Text.ToString(), lblcheck_26.Text.ToString(), lblcheck_27.Text.ToString(), lblcheck_28.Text.ToString(), lblcheck_29.Text.ToString(), lblcheck_30.Text.ToString(), lblcheck_31.Text.ToString());
                        
                    }
                    
                }
                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvEmployeeDB":
                GvEmployeeDB.PageIndex = e.NewPageIndex;
                GvEmployeeDB.DataBind();
                Select_Employee_DB(GvEmployeeDB);
                break;
            case "GvGroupName":
                GvGroupName.PageIndex = e.NewPageIndex;
                GvGroupName.DataBind();
                SelectGvGroupNameList(GvGroupName, txtgroupname.Text, int.Parse(ddlgroupname.SelectedValue));
                break;
            case "GvEmployeeTemp":
                //Temp
                GvEmployeeTemp.PageIndex = e.NewPageIndex;
                GvEmployeeTemp.DataSource = (DataSet)ViewState["vsTemp_Emps"]; //ds_sw_.Tables[0];
                GvEmployeeTemp.DataBind();
                break;
            case "GvGroupName_Temp":
                //Temp
                GvGroupName_Temp.PageIndex = e.NewPageIndex;
                GvGroupName_Temp.DataSource = (DataSet)ViewState["vsTemp_ODSP"]; //ds_sw_.Tables[0];
                GvGroupName_Temp.DataBind();
                break;
            case "GvIndex":
                GvIndex.PageIndex = e.NewPageIndex;
                GvIndex.DataBind();
                Select_Employee_index();
                break;
            case "GvApprove":
                GvApprove.PageIndex = e.NewPageIndex;
                GvApprove.DataBind();
                Select_Employee_Approve();
                break;
            case "GvEmployeeDB_add":
                GvEmployeeDB_add.PageIndex = e.NewPageIndex;
                GvEmployeeDB_add.DataBind();
                Select_Employee_DB(GvEmployeeDB_add);
                break;
            case "GvGroupName_add":
                GvGroupName_add.PageIndex = e.NewPageIndex;
                GvGroupName_add.DataBind();
                SelectGvGroupNameList(GvGroupName_add, txtgroupname_add.Text, int.Parse(ddlgroupname_add.SelectedValue));
                break;
            case "GvEmployeeTemp_add":
                //Temp
                GvEmployeeTemp_add.PageIndex = e.NewPageIndex;
                GvEmployeeTemp_add.DataSource = (DataSet)ViewState["vsTemp_Emps_add"]; //ds_sw_.Tables[0];
                GvEmployeeTemp_add.DataBind();
                break;
            case "GvGroupName_Temp_add":
                //Temp
                break;
            case "GvListDB_edit":
                GvListDB_edit.PageIndex = e.NewPageIndex;              
                GvListDB_edit.DataBind();
                SelectGvListDB_edit(GvListDB_edit, int.Parse(ViewState["u0_empshift_idx_edit"].ToString()));
                break;
            case "GvGroupDB_edit":
                GvGroupDB_edit.PageIndex = e.NewPageIndex;
                GvGroupDB_edit.DataBind();
                SelectGvGroupDB_edit(GvGroupDB_edit, int.Parse(ViewState["u0_empshift_idx_edit"].ToString()));
                break;
            case "GvListDB_approve":
                GvListDB_approve.PageIndex = e.NewPageIndex;
                GvListDB_approve.DataBind();
                SelectGvListDB_edit(GvListDB_approve, int.Parse(ViewState["u0_empshift_idx_approve"].ToString()));
                break;
            case "GvGroupDB_approve":
                GvListDB_approve.PageIndex = e.NewPageIndex;
                GvListDB_approve.DataBind();
                SelectGvGroupDB_edit(GvGroupDB_approve, int.Parse(ViewState["u0_empshift_idx_approve"].ToString()));
                break;

        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                //GvMaster.EditIndex = e.NewEditIndex;
                //SelectMasterList();

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                //GvMaster.EditIndex = -1;
                //SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                /*int m0_group_diary_idx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtName_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtName_update");
                var ddStatus_update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatus_update");
                var ddlSectionEdit_Select = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlSectionEdit_Select");

                GvMaster.EditIndex = -1;

                _dataEmp.emps_group_diary_action = new group_diary[1];
                group_diary dtemployee_ = new group_diary();

                dtemployee_.m0_group_diary_idx = m0_group_diary_idx;
                dtemployee_.group_diary_name = txtName_update.Text;
                dtemployee_.rsec_idx_ref = int.Parse(ddlSectionEdit_Select.SelectedValue);
                dtemployee_.group_diary_status = int.Parse(ddStatus_update.SelectedValue);

                _dataEmp.emps_group_diary_action[0] = dtemployee_;
                _dataEmp = callService(_urlGetUpdateGroupDialy, _dataEmp);

                SelectMasterList();*/

                break;
        }
    }

    #endregion

    #region GvRowDeleting
    protected void gvRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvListDB_edit":
                //Delete_GvListDB();
                GvListDB_edit.EditIndex = -1;              
                break;
        }

    }
    #endregion

    #endregion

    #region SELECT

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }

    protected void select_org(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServiceEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกองค์กร....", "0"));
    }

    protected void select_dep(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServiceEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกฝ่าย....", "0"));
    }

    protected void select_sec(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServiceEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกหน่วยงาน....", "0"));
    }

    protected void select_pos(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dataEmployee.position_list = new position_details[1];
        position_details _posList = new position_details();
        _posList.org_idx = _org_idx;
        _posList.rdept_idx = _rdept_idx;
        _posList.rsec_idx = _rsec_idx;
        _dataEmployee.position_list[0] = _posList;

        _dataEmployee = callServiceEmployee(_urlGetPositionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.position_list, "pos_name_th", "rpos_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกตำแหน่ง....", "0"));
    }

    protected void Select_Employee_DB(GridView GName)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _dataEmployee_ = new employee_detail();

        _dataEmployee_.emp_code = ViewState["empcode_search"].ToString();
        //_dataEmployee_.org_idx = int.Parse(ViewState["org_idx_search"].ToString());     
        _dataEmployee_.rsec_idx = int.Parse(ViewState["rsec_idx_search"].ToString());
        _dataEmployee_.rpos_idx = int.Parse(ViewState["rpos_idx_search"].ToString());
        //_dataEmployee_.prefix_idx = int.Parse(ViewState["prefix_name_th_search"].ToString());
        _dataEmployee_.emp_name_th = ViewState["name_th_search"].ToString();
        _dataEmployee_.emp_type_idx = int.Parse(ViewState["emptype_search"].ToString());
        _dataEmployee_.type_select_emp = 16;
        _dataEmployee_.emp_status = 1;

        _dataEmployee.employee_list[0] = _dataEmployee_;
        //fs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));

        _dataEmployee = callServiceEmployee(urlGetEmployee, _dataEmployee);

        if(int.Parse(_dataEmployee.return_code) == 1)
        {
            setGridData(GName, null);
        }
        else
        {
            setGridData(GName, _dataEmployee.employee_list);
        }
        
    }

    protected void select_empIdx_present()
    {
        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());

        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["firsName_TH"] = _dtEmployee.employee_list[0].emp_firstname_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;
        ViewState["jobgrade_level"] = _dtEmployee.employee_list[0].jobgrade_level;
        ViewState["return_code"] = 0;
        ViewState["Gv_emp_idx"] = 0;
        ViewState["Box_dataEmployee"] = null;
        ViewState["Box_dataEmployee_Report"] = null;
        ViewState["search_name"] = "";
        ViewState["lbl_rsec_idx"] = "";
        ViewState["rsec_idx_insert_add"] = 0;
        ViewState["emp_idx_insert_add"] = 0;
        ViewState["org_idx_search"] = 0;
        ViewState["rdept_idx_search"] = 0;
        ViewState["rsec_idx_search"] = 0;
        ViewState["name_th_search"] = "";
        ViewState["empcode_search"] = "";
        ViewState["emp_idx_insert"] = 0;
        ViewState["rsec_idx_insert"] = 0;
        ViewState["permission_check"] = 0;
        ViewState["emptype_search"] = 0;
        Session["u0_empshift_idx"] = 0;
        ViewState["recheck_permission"] = "";
        ViewState["recheck_permission_name"] = "";

        Fv_Search_Shift_Index.ChangeMode(FormViewMode.Insert);
        Fv_Search_Shift_Index.DataBind();
        DropDownList ddlshift_s = (DropDownList)Fv_Search_Shift_Index.FindControl("ddlshift_s");
        select_parttime(ddlshift_s);

        var dsodsp = new DataSet();
        dsodsp.Tables.Add("vsTemp_ODSP");
        dsodsp.Tables[0].Columns.Add("rsec_idx_ref", typeof(int));
        dsodsp.Tables[0].Columns.Add("m0_group_diary_idx", typeof(int));
        dsodsp.Tables[0].Columns.Add("group_diary_name", typeof(string));
        ViewState["vsTemp_ODSP"] = dsodsp;

        var dsemp = new DataSet();
        dsemp.Tables.Add("dsTemp_Emps");
        dsemp.Tables["dsTemp_Emps"].Columns.Add("emp_idx", typeof(int));
        dsemp.Tables["dsTemp_Emps"].Columns.Add("emp_code", typeof(string));
        dsemp.Tables["dsTemp_Emps"].Columns.Add("emp_name_th", typeof(string));
        ViewState["vsTemp_Emps"] = dsemp;

        var dsodsp_add = new DataSet();
        dsodsp_add.Tables.Add("vsTemp_ODSP_add");
        dsodsp_add.Tables[0].Columns.Add("rsec_idx_ref", typeof(int));
        dsodsp_add.Tables[0].Columns.Add("m0_group_diary_idx", typeof(int));
        dsodsp_add.Tables[0].Columns.Add("group_diary_name", typeof(string));
        ViewState["vsTemp_ODSP_add"] = dsodsp_add;

        var dsemp_add = new DataSet();
        dsemp_add.Tables.Add("vsTemp_Emps_add");
        dsemp_add.Tables[0].Columns.Add("emp_idx", typeof(int));
        dsemp_add.Tables[0].Columns.Add("emp_code", typeof(string));
        dsemp_add.Tables[0].Columns.Add("emp_name_th", typeof(string));
        ViewState["vsTemp_Emps_add"] = dsemp_add;

    }

    protected void select_parttime(DropDownList ddlName)
    {
        /*_dataEmps.emps_parttime_action = new parttime[1];
        parttime  emps_parttime_action = new parttime();

        emps_parttime_action.parttime_status = 1;

        _dataEmps.emps_parttime_action[0] = emps_parttime_action;
        _dataEmps = callService(urlGetSelectParttime, _dataEmps);
        setDdlData(ddlName, _dataEmps.emps_parttime_action, "parttime_name_th", "m0_parttime_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกกะการทำงาน....", "0"));*/
        //ddlparttime
        _dataEmps.emps_parttime_action = new parttime[1];
        parttime dtemployee_ = new parttime();

        _dataEmps.emps_parttime_action[0] = dtemployee_;

        _dataEmps = callService(_urlGetSelectParttime, _dataEmps);
        setDdlData(ddlName, _dataEmps.emps_parttime_action, "parttime_name_th", "m0_parttime_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกกะการทำงาน....", "0"));
        //setGridData(GvMaster, _dataEmps.emps_parttime_action);
    }

    protected void Select_Employee_index()
    {
        TextBox txtcode_s = (TextBox)Fv_Search_Shift_Index.FindControl("txtcode_s");
        DropDownList ddlshift_s = (DropDownList)Fv_Search_Shift_Index.FindControl("ddlshift_s");
        DropDownList ddlIndexDeptSearch = (DropDownList)Fv_Search_Shift_Index.FindControl("ddlIndexDeptSearch");

        _dataEmps.emps_empshift_action = new empshift[1];
        empshift _dataEmps_ = new empshift();

        _dataEmps_.m0_parttime_idx_ref = int.Parse(ddlshift_s.SelectedValue);
        _dataEmps_.rdept_idx = int.Parse(ddlIndexDeptSearch.SelectedValue);
        _dataEmps_.parttime_code = txtcode_s.Text;
        _dataEmps_.type_action = 5; //Index   
        _dataEmps_.emp_idx = int.Parse(ViewState["EmpIDX"].ToString());

        _dataEmps.emps_empshift_action[0] = _dataEmps_;
        _dataEmps = callService(_urlGetSelectApprove, _dataEmps);
        ViewState["Box_dataEmployee_index_"] = _dataEmps.emps_empshift_action;
        setGridData(GvIndex, _dataEmps.emps_empshift_action);
    }

    protected void Select_Employee_Index_Report()
    {
        TextBox txtcode_s = (TextBox)Fv_Search_Shift_Index.FindControl("txtcode_s");
        DropDownList ddlshift_s = (DropDownList)Fv_Search_Shift_Index.FindControl("ddlshift_s");
        DropDownList ddlIndexDeptSearch = (DropDownList)Fv_Search_Shift_Index.FindControl("ddlIndexDeptSearch");

        _dataEmps.emps_empshift_action = new empshift[1];
        empshift _dataEmps_ = new empshift();

        _dataEmps_.m0_parttime_idx_ref = int.Parse(ddlshift_s.SelectedValue);
        _dataEmps_.rdept_idx = int.Parse(ddlIndexDeptSearch.SelectedValue);
        _dataEmps_.parttime_code = txtcode_s.Text;
        _dataEmps_.type_action = 6; //Index   
        _dataEmps_.emp_idx = int.Parse(ViewState["EmpIDX"].ToString());

        _dataEmps.emps_empshift_action[0] = _dataEmps_;
        _dataEmps = callService(_urlGetSelectApprove, _dataEmps);
        ViewState["Box_dataEmployee_index_"] = _dataEmps.emps_empshift_action;
        setGridData(GvIndex_Report, _dataEmps.emps_empshift_action);
    }

    protected void SelectGvGroupNameList(GridView GName , string group_diary_name, int m0_group_diary_idx)
    {
        _dataEmps.emps_group_diary_action = new group_diary[1];
        group_diary dtemployee_ = new group_diary();

        dtemployee_.group_diary_name = group_diary_name;
        dtemployee_.m0_group_diary_idx = m0_group_diary_idx;
        dtemployee_.group_diary_created_by = int.Parse(ViewState["EmpIDX"].ToString());

        _dataEmps.emps_group_diary_action[0] = dtemployee_;

        _dataEmps = callService(_urlGetSelectGroupDialy, _dataEmps);
        //text.Text = _dataEmployee.ToString();
        setGridData(GName, _dataEmps.emps_group_diary_action);
    }

    protected void select_groupname(DropDownList ddlName)
    {
        _dataEmps.emps_group_diary_action = new group_diary[1];
        group_diary dtemployee_ = new group_diary(); 

        //dtemployee_.group_diary_name = txtgroupname.Text;
        //dtemployee_.rsec_idx_ref = int.Parse(ddlsec.SelectedValue);
        dtemployee_.group_diary_created_by = int.Parse(ViewState["EmpIDX"].ToString());

        _dataEmps.emps_group_diary_action[0] = dtemployee_;

        _dataEmps = callService(_urlGetSelectGroupDialy, _dataEmps);
        setDdlData(ddlName, _dataEmps.emps_group_diary_action, "group_diary_name", "m0_group_diary_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกกลุ่มงาน....", "0"));
    }

    protected void Insert_create()
    {
        if (ViewState["vsTemp_Emps"] != null)
        {
            foreach (GridViewRow row in GvEmployeeTemp_Insert.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_emp_idx = (Label)row.Cells[0].FindControl("lbl_emp_idx_insert");
                    ViewState["emp_idx_insert"] += lbl_emp_idx.Text + ",";
                }
            }
        }

        if (ViewState["vsTemp_ODSP"] != null)
        {
            foreach (GridViewRow row in GvGroupName_Temp.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_rsec_idx = (Label)row.Cells[0].FindControl("lbl_rsec_idx");
                    ViewState["rsec_idx_insert"] += lbl_rsec_idx.Text + ",";
                }
            }
        }
            
        _dataEmps.emps_empshift_action = new empshift[1];
        empshift emps_empshift_action = new empshift();

        emps_empshift_action.emp_idx_ref = int.Parse(ViewState["EmpIDX"].ToString()); //ผู้สร้าง
        emps_empshift_action.m0_parttime_idx_ref = int.Parse(ddlparttime.SelectedValue); //ประเภทกะทำงาน
        emps_empshift_action.announce_diary_date_start = txtDate_Start.Text; //วันที่เริ่มประกาศใช้
        emps_empshift_action.announce_diary_date_end = txtDate_End.Text; //วันที่สิ้นสุดกะ

        if (ViewState["emp_idx_insert"].ToString() != "0")
        {
            emps_empshift_action.m0_group_emp_idx_ref = ViewState["emp_idx_insert"].ToString();
        }
        else
        {
            emps_empshift_action.m0_group_emp_idx_ref = ",";
        }

        if (ViewState["rsec_idx_insert"].ToString() != "0")
        {
            emps_empshift_action.m0_group_rsec_idx_ref = ViewState["rsec_idx_insert"].ToString();
        }
        else
        {
            emps_empshift_action.m0_group_rsec_idx_ref = ",";
        }

        _dataEmps.emps_empshift_action[0] = emps_empshift_action;
        //sdfa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmps));

        if (ViewState["emp_idx_insert"].ToString() != "0" || ViewState["rsec_idx_insert"].ToString() != "0")
        {           
            _dataEmps = callService(_urlGetInsertShifttime, _dataEmps);
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "แจ้งเตือน", "alert('" + "รบกวนกรอกข้อมูลให้ครบ" + "')", true);
        }      
    }

    protected void Insert_edit(int u0_empshift_idx)
    {
        TextBox txtstartdate_edit = (TextBox)FvEdit_Detail.FindControl("txtstartdate_edit");
        TextBox txtenddate_edit = (TextBox)FvEdit_Detail.FindControl("txtenddate_edit");
        DropDownList ddlpartime_edit = (DropDownList)FvEdit_Detail.FindControl("ddlpartime_edit");

        if (ViewState["vsTemp_Emps_add"] != null)
        {
            foreach (GridViewRow row in GvEmployeeTemp_add_Edit.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_emp_idx = (Label)row.Cells[0].FindControl("lbl_emp_idx_edit");
                    ViewState["emp_idx_insert_add"] += lbl_emp_idx.Text + ",";
                }
            }
        }
        
        if (ViewState["vsTemp_ODSP_add"] != null)
        {
            foreach (GridViewRow row in GvGroupName_Temp_add.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_rsec_idx = (Label)row.Cells[0].FindControl("lbl_rsec_idx");
                    ViewState["rsec_idx_insert_add"] += lbl_rsec_idx.Text + ",";
                }
            }
        }
        
        _dataEmps.emps_empshift_action = new empshift[1];
        empshift emps_empshift_action = new empshift();

        emps_empshift_action.u0_empshift_idx = u0_empshift_idx; //รหัสเอกสาร
        emps_empshift_action.emp_idx_ref = int.Parse(ViewState["EmpIDX"].ToString()); //ผู้สร้าง
        emps_empshift_action.m0_parttime_idx_ref = int.Parse(ddlpartime_edit.SelectedValue); //ประเภทกะทำงาน
        emps_empshift_action.announce_diary_date_start = txtstartdate_edit.Text; //วันที่เริ่มประกาศใช้
        emps_empshift_action.announce_diary_date_end = txtenddate_edit.Text; //วันที่สิ้นสุดกะ

        if (ViewState["emp_idx_insert_add"].ToString() != "0")
        {
            emps_empshift_action.m0_group_emp_idx_ref = ViewState["emp_idx_insert_add"].ToString();
        }
        else
        {
            emps_empshift_action.m0_group_emp_idx_ref = ",";
        }

        if (ViewState["rsec_idx_insert_add"].ToString() != "0")
        {
            emps_empshift_action.m0_group_rsec_idx_ref = ViewState["rsec_idx_insert_add"].ToString();
        }
        else
        {
            emps_empshift_action.m0_group_rsec_idx_ref = ",";
        }

        
        emps_empshift_action.type_action = 2; //Approve

        _dataEmps.emps_empshift_action[0] = emps_empshift_action;
        //sdfa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmps));
        _dataEmps = callService(_urlGetUpdateApprove, _dataEmps);
    }

    protected void Select_Employee_Approve()
    {
        _dataEmps.emps_empshift_action = new empshift[1];
        empshift _dataEmps_ = new empshift();

        _dataEmps_.emp_idx_ref = int.Parse(ViewState["EmpIDX"].ToString());
        _dataEmps_.emp_idx = int.Parse(ViewState["EmpIDX"].ToString());
        _dataEmps_.rpos_idx = int.Parse(ViewState["Pos_idx"].ToString()); // int.Parse(ddlstatus_s.SelectedValue);
        _dataEmps_.type_action = 1; //Index   
        //เลขที่กะงาน

        _dataEmps.emps_empshift_action[0] = _dataEmps_;
        //sdfa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmps));
        _dataEmps = callService(_urlGetSelectApprove, _dataEmps);  
        ViewState["Box_dataEmployee_Approve_"] = _dataEmps.emps_empshift_action;
        setGridData(GvApprove, _dataEmps.emps_empshift_action);
    }

    protected void Update_Approve()
    {
        var ddl_view_Approve = (DropDownList)FvViewApprove.FindControl("ddl_view_Approve");
        var tbDocRemark = (TextBox)FvViewApprove.FindControl("tbDocRemark");

        _dataEmps.emps_empshift_action = new empshift[1];
        empshift _dataEmps_ = new empshift();

        _dataEmps_.emp_idx = int.Parse(ViewState["EmpIDX"].ToString());
        _dataEmps_.u0_empshift_idx = int.Parse(ViewState["u0_empshift_idx_approve"].ToString());
        _dataEmps_.Approve_status = int.Parse(ddl_view_Approve.SelectedValue);
        _dataEmps_.u0_doc_decision = int.Parse(ddl_view_Approve.SelectedValue);
        _dataEmps_.comment_approve = tbDocRemark.Text;
        _dataEmps_.type_action = 1; //Approve 

        //_dataEmps_.u0_unidx = int.Parse(ViewState["u0_unidx_approve"].ToString());
        //_dataEmps_.u0_acidx = int.Parse(ViewState["u0_acidx_approve"].ToString());
        //_dataEmps_.u0_doc_decision = int.Parse(ViewState["u0_doc_decision_approve"].ToString());

        _dataEmps.emps_empshift_action[0] = _dataEmps_;
        _dataEmps = callService(_urlGetUpdateApprove, _dataEmps);
    }

    protected void Select_RptLog_Detail(int u0_empshift_idx)
    {
        _dataEmps.emps_empshift_action = new empshift[1];
        empshift _dataEmps_ = new empshift();

        _dataEmps_.u0_empshift_idx = u0_empshift_idx;
        _dataEmps_.type_action = 2;

        _dataEmps.emps_empshift_action[0] = _dataEmps_;
        _dataEmps = callService(_urlGetSelectApprove, _dataEmps);
        //fsa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        setRptData(rptLog, _dataEmps.emps_empshift_action);
    }

    protected void Select_FvEdit_Detail(int u0_empshift_idx)
    {
        _dataEmps.emps_empshift_action = new empshift[1];
        empshift _dataEmps_ = new empshift();

        _dataEmps_.u0_empshift_idx = u0_empshift_idx;
        _dataEmps_.type_action = 5; //Index   

        _dataEmps.emps_empshift_action[0] = _dataEmps_;
        _dataEmps = callService(_urlGetSelectApprove, _dataEmps);
        //ViewState["Box_dataEmployee_index_"] = _dataEmps.emps_empshift_action;
        //setGridData(GvIndex, _dataEmps.emps_empshift_action);
        //sdfs.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmps));
        setFormData(FvEdit_Detail, _dataEmps.emps_empshift_action);
    }

    protected void SelectGvListDB_edit(GridView Gvname, int u0_empshift_idx)
    {
        _dataEmps.emps_empshift_action = new empshift[1];
        empshift _dataEmps_ = new empshift();

        _dataEmps_.u0_empshift_idx = u0_empshift_idx;
        _dataEmps_.type_action = 3; //Select List Edit 

        _dataEmps.emps_empshift_action[0] = _dataEmps_;
        _dataEmps = callService(_urlGetSelectApprove, _dataEmps);
        ViewState["Box_dataEmployee_edit"] = _dataEmps.emps_empshift_action;
        setGridData(Gvname, _dataEmps.emps_empshift_action);
    }

    protected void SelectGvGroupDB_edit(GridView Gvname, int u0_empshift_idx)
    {
        _dataEmps.emps_empshift_action = new empshift[1];
        empshift _dataEmps_ = new empshift();

        _dataEmps_.u0_empshift_idx = u0_empshift_idx;
        _dataEmps_.type_action = 4; //Select Group Edit 

        _dataEmps.emps_empshift_action[0] = _dataEmps_;
        _dataEmps = callService(_urlGetSelectApprove, _dataEmps);
        ViewState["Box_dataGroup_edit"] = _dataEmps.emps_empshift_action;
        setGridData(Gvname, _dataEmps.emps_empshift_action);
    }

    protected void Delete_GvListDB(int u0_empshift_idx , int emp_idx)
    {
        var GvListDB_edit = (GridView)boxedit_shiftime.FindControl("GvListDB_edit");
        //var lbl_u0_empshift_idx = (Label)boxedit_shiftime.FindControl("lbl_u0_empshift_idx");
        //var lbl_emp_idx = (Label)boxedit_shiftime.FindControl("lbl_emp_idx");

        _dataEmps.emps_empshift_action = new empshift[1];
        empshift _dataEmps_ = new empshift();

        _dataEmps_.u0_empshift_idx = u0_empshift_idx;
        _dataEmps_.emp_idx_ref = emp_idx;
        _dataEmps_.type_action = 3; //Delete 

        _dataEmps.emps_empshift_action[0] = _dataEmps_;
        _dataEmps = callService(_urlGetUpdateApprove, _dataEmps);
        //setGridData(GvListDB_edit, _dataEmps.emps_empshift_action);
        //ViewState["Box_dataEmployee_edit"] = _dataEmps.emps_empshift_action;
    }

    protected void Delete_GvGroupDB(int u0_empshift_idx , string u0_group_idx)
    {
        var GvGroupDB_edit = (GridView)boxedit_shiftime.FindControl("GvGroupDB_edit");
        //var lbl_u0_empshift_idx_ = (Label)boxedit_shiftime.FindControl("lbl_u0_empshift_idx");
        //var lbl_rsec_idx = (Label)boxedit_shiftime.FindControl("lbl_rsec_idx");

        _dataEmps.emps_empshift_action = new empshift[1];
        empshift _dataEmps_1 = new empshift();

        _dataEmps_1.u0_empshift_idx = u0_empshift_idx;
        //_dataEmps_1.m0_group_rsec_idx_ref = rsec;
        _dataEmps_1.m0_group_diary_idx = int.Parse(u0_group_idx);      
        _dataEmps_1.type_action = 4; //Delete 

        _dataEmps.emps_empshift_action[0] = _dataEmps_1;
        _dataEmps = callService(_urlGetUpdateApprove, _dataEmps);
        //setGridData(GvGroupDB_edit, _dataEmps.emps_empshift_action);
        //ViewState["Box_dataEmployee_edit"] = _dataEmps.emps_empshift_action;
    }

    protected void Select_shift_month_report(DropDownList ddlName, string month_report)
    {
        /*_dataEmps.emps_empshift_action = new empshift[1];
        empshift _dataEmps_ = new empshift();

        _dataEmps_.month = month_report;
        _dataEmps_.type_action = 11; 

        _dataEmps.emps_empshift_action[0] = _dataEmps_;
        _dataEmps = callService(_urlGetSelectApprove, _dataEmps);
        setDdlData(ddlName, _dataEmps.emps_empshift_action, "parttime_name_th", "m0_parttime_idx_ref");
        ddlName.Items.Insert(0, new ListItem("เลือกกะทำงาน....", "0"));*/
    }

    protected void Select_Employee_Report()
    {
        FormView FvSearch_Report = (FormView)ViewHr.FindControl("FvSearch_Report");
        TextBox txt_search_empcode_report = (TextBox)FvSearch_Report.FindControl("txt_search_empcode_report");
        DropDownList ddl_month_report = (DropDownList)FvSearch_Report.FindControl("ddl_month_report");
        DropDownList ddl_year_report = (DropDownList)FvSearch_Report.FindControl("ddl_year_report");
        //DropDownList ddl_shift_report = (DropDownList)FvSearch_Report.FindControl("ddl_shift_report");
        DropDownList ddl_org_report = (DropDownList)FvSearch_Report.FindControl("ddl_org_report");
        DropDownList ddl_dep_report = (DropDownList)FvSearch_Report.FindControl("ddl_dep_report");
        DropDownList ddl_sec_report = (DropDownList)FvSearch_Report.FindControl("ddl_sec_report");
        DropDownList ddl_pos_report = (DropDownList)FvSearch_Report.FindControl("ddl_pos_report");

        _dataEmps.emps_empshift_action = new empshift[1];
        empshift _dataEmps_ = new empshift();

        _dataEmps_.month = ddl_month_report.SelectedValue.ToString();
        _dataEmps_.year = ddl_year_report.SelectedValue.ToString();
        //_dataEmps_.m0_parttime_idx_ref = int.Parse(ddl_shift_report.SelectedValue);
        _dataEmps_.org_idx = int.Parse(ddl_org_report.SelectedValue);
        _dataEmps_.rdept_idx = int.Parse(ddl_dep_report.SelectedValue);
        _dataEmps_.rsec_idx = int.Parse(ddl_sec_report.SelectedValue);
        _dataEmps_.rpos_idx = int.Parse(ddl_pos_report.SelectedValue);
        _dataEmps_.m0_group_emp_idx_ref = txt_search_empcode_report.Text.ToString();

        if (ddl_month_report.SelectedValue == "1" || ddl_month_report.SelectedValue == "3" || ddl_month_report.SelectedValue == "5" || ddl_month_report.SelectedValue == "7" || ddl_month_report.SelectedValue == "8" || ddl_month_report.SelectedValue == "10" || ddl_month_report.SelectedValue == "12") //Day 31
        {
            _dataEmps_.type_action = 8; //Report Shift
        }
        else if (ddl_month_report.SelectedValue == "4" || ddl_month_report.SelectedValue == "6" || ddl_month_report.SelectedValue == "9" || ddl_month_report.SelectedValue == "11") //Day 30
        {
            _dataEmps_.type_action = 9; //Report Shift
        }
        else //Day 29
        {
            _dataEmps_.type_action = 10; //Report Shift
        }
        
        _dataEmps.emps_empshift_action[0] = _dataEmps_;
        //sdfa.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmps));
        _dataEmps = callService(_urlGetSelectApprove, _dataEmps);
        setGridData(GvEmployee_Report, _dataEmps.emps_empshift_action);
        ViewState["Box_dataEmployee_Report"] = _dataEmps.emps_empshift_action;
    }

    protected void AddHeaderRow1()
    {
        GridView GvEmployee_Report = (GridView)ViewHr.FindControl("GvEmployee_Report");
        DropDownList ddl_month_report = (DropDownList)FvSearch_Report.FindControl("ddl_month_report");
        
        GridViewRow gr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

        TableHeaderCell thc0 = new TableHeaderCell();
        TableHeaderCell thc1 = new TableHeaderCell();
        TableHeaderCell thc2 = new TableHeaderCell();
        TableHeaderCell thc3 = new TableHeaderCell();
        TableHeaderCell thc4 = new TableHeaderCell();

        thc0.Text = "ลำดับ";
        thc1.Text = "รหัสพนักงาน";
        thc2.Text = "ชื่อพนักงาน";
        thc3.Text = "วันหยุด";
        thc4.Text = "ตารางกะการทำงานประจำเดือน" + ddl_month_report.SelectedItem.Text + " 2562";

        thc0.ColumnSpan = 1;
        thc1.ColumnSpan = 1;
        thc2.ColumnSpan = 1;
        thc3.ColumnSpan = 1;
        thc4.ColumnSpan = 31;
        thc0.RowSpan = 3;
        thc1.RowSpan = 3;
        thc2.RowSpan = 3;
        thc3.RowSpan = 3;

        thc0.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc1.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc2.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc3.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc4.Style.Add(HtmlTextWriterStyle.TextAlign, "center");

        gr.Cells.AddRange(new TableCell[] { thc0, thc1, thc2, thc3, thc4 });
        GvEmployee_Report.Controls[0].Controls.AddAt(0, gr);
    }

    protected void AddHeaderRow2(string a_1, string a_2, string a_3, string a_4, string a_5, string a_6, string a_7, string a_8, string a_9, string a_10, string a_11, string a_12, string a_13, string a_14, string a_15, string a_16, string a_17, string a_18, string a_19, string a_20, string a_21, string a_22, string a_23, string a_24, string a_25, string a_26, string a_27, string a_28, string a_29, string a_30, string a_31)
    {
        GridView GvEmployee_Report = (GridView)ViewHr.FindControl("GvEmployee_Report");
        GridViewRow gr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

        TableHeaderCell thc1 = new TableHeaderCell();
        TableHeaderCell thc2 = new TableHeaderCell();
        TableHeaderCell thc3 = new TableHeaderCell();
        TableHeaderCell thc4 = new TableHeaderCell();
        TableHeaderCell thc5 = new TableHeaderCell();
        TableHeaderCell thc6 = new TableHeaderCell();
        TableHeaderCell thc7 = new TableHeaderCell();
        TableHeaderCell thc8 = new TableHeaderCell();
        TableHeaderCell thc9 = new TableHeaderCell();
        TableHeaderCell thc10 = new TableHeaderCell();
        TableHeaderCell thc11 = new TableHeaderCell();
        TableHeaderCell thc12 = new TableHeaderCell();
        TableHeaderCell thc13 = new TableHeaderCell();
        TableHeaderCell thc14 = new TableHeaderCell();
        TableHeaderCell thc15 = new TableHeaderCell();
        TableHeaderCell thc16 = new TableHeaderCell();
        TableHeaderCell thc17 = new TableHeaderCell();
        TableHeaderCell thc18 = new TableHeaderCell();
        TableHeaderCell thc19 = new TableHeaderCell();
        TableHeaderCell thc20 = new TableHeaderCell();
        TableHeaderCell thc21 = new TableHeaderCell();
        TableHeaderCell thc22 = new TableHeaderCell();
        TableHeaderCell thc23 = new TableHeaderCell();
        TableHeaderCell thc24 = new TableHeaderCell();
        TableHeaderCell thc25 = new TableHeaderCell();
        TableHeaderCell thc26 = new TableHeaderCell();
        TableHeaderCell thc27 = new TableHeaderCell();
        TableHeaderCell thc28 = new TableHeaderCell();
        TableHeaderCell thc29 = new TableHeaderCell();
        TableHeaderCell thc30 = new TableHeaderCell();
        TableHeaderCell thc31 = new TableHeaderCell();

        thc1.Text = check_holiday_2(a_1);
        thc2.Text = check_holiday_2(a_2);
        thc3.Text = check_holiday_2(a_3);
        thc4.Text = check_holiday_2(a_4);
        thc5.Text = check_holiday_2(a_5);
        thc6.Text = check_holiday_2(a_6);
        thc7.Text = check_holiday_2(a_7);
        thc8.Text = check_holiday_2(a_8);
        thc9.Text = check_holiday_2(a_9);
        thc10.Text = check_holiday_2(a_10);
        thc11.Text = check_holiday_2(a_11);
        thc12.Text = check_holiday_2(a_12);
        thc13.Text = check_holiday_2(a_13);
        thc14.Text = check_holiday_2(a_14);
        thc15.Text = check_holiday_2(a_15);
        thc16.Text = check_holiday_2(a_16);
        thc17.Text = check_holiday_2(a_17);
        thc18.Text = check_holiday_2(a_18);
        thc19.Text = check_holiday_2(a_19);
        thc20.Text = check_holiday_2(a_20);
        thc21.Text = check_holiday_2(a_21);
        thc22.Text = check_holiday_2(a_22);
        thc23.Text = check_holiday_2(a_23);
        thc24.Text = check_holiday_2(a_24);
        thc25.Text = check_holiday_2(a_25);
        thc26.Text = check_holiday_2(a_26);
        thc27.Text = check_holiday_2(a_27);
        thc28.Text = check_holiday_2(a_28);
        thc29.Text = check_holiday_2(a_29);
        thc30.Text = check_holiday_2(a_30);
        thc31.Text = check_holiday_2(a_31);

        thc1.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc2.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc3.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc4.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc5.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc6.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc7.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc8.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc9.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc10.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc11.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc12.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc13.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc14.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc15.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc16.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc17.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc18.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc19.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc20.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc21.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc22.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc23.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc24.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc25.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc26.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc27.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc28.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc29.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc30.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc31.Style.Add(HtmlTextWriterStyle.TextAlign, "center");


        if (check_name_row != a_1)
        {
            gr.Cells.AddRange(new TableCell[] { thc1, thc2, thc3, thc4, thc5, thc6, thc7, thc8, thc9, thc10, thc11, thc12, thc13, thc14, thc15, thc16, thc17, thc18, thc19, thc20, thc21, thc22, thc23, thc24, thc25, thc26, thc27, thc28, thc29, thc30, thc31 });
            GvEmployee_Report.Controls[0].Controls.AddAt(1, gr);
        }

        check_name_row = a_1;

    }

    protected void AddHeaderRow3()
    {
        GridView GvEmployee_Report = (GridView)ViewHr.FindControl("GvEmployee_Report");
        DropDownList ddl_month_report = (DropDownList)FvSearch_Report.FindControl("ddl_month_report");
        GridViewRow gr = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

        TableHeaderCell thc1 = new TableHeaderCell();
        TableHeaderCell thc2 = new TableHeaderCell();
        TableHeaderCell thc3 = new TableHeaderCell();
        TableHeaderCell thc4 = new TableHeaderCell();
        TableHeaderCell thc5 = new TableHeaderCell();
        TableHeaderCell thc6 = new TableHeaderCell();
        TableHeaderCell thc7 = new TableHeaderCell();
        TableHeaderCell thc8 = new TableHeaderCell();
        TableHeaderCell thc9 = new TableHeaderCell();
        TableHeaderCell thc10 = new TableHeaderCell();
        TableHeaderCell thc11 = new TableHeaderCell();
        TableHeaderCell thc12 = new TableHeaderCell();
        TableHeaderCell thc13 = new TableHeaderCell();
        TableHeaderCell thc14 = new TableHeaderCell();
        TableHeaderCell thc15 = new TableHeaderCell();
        TableHeaderCell thc16 = new TableHeaderCell();
        TableHeaderCell thc17 = new TableHeaderCell();
        TableHeaderCell thc18 = new TableHeaderCell();
        TableHeaderCell thc19 = new TableHeaderCell();
        TableHeaderCell thc20 = new TableHeaderCell();
        TableHeaderCell thc21 = new TableHeaderCell();
        TableHeaderCell thc22 = new TableHeaderCell();
        TableHeaderCell thc23 = new TableHeaderCell();
        TableHeaderCell thc24 = new TableHeaderCell();
        TableHeaderCell thc25 = new TableHeaderCell();
        TableHeaderCell thc26 = new TableHeaderCell();
        TableHeaderCell thc27 = new TableHeaderCell();
        TableHeaderCell thc28 = new TableHeaderCell();
        TableHeaderCell thc29 = new TableHeaderCell();
        TableHeaderCell thc30 = new TableHeaderCell();
        TableHeaderCell thc31 = new TableHeaderCell();

        thc1.Text = "1";
        thc2.Text = "2";
        thc3.Text = "3";
        thc4.Text = "4";
        thc5.Text = "5";
        thc6.Text = "6";
        thc7.Text = "7";
        thc8.Text = "8";
        thc9.Text = "9";
        thc10.Text = "10";
        thc11.Text = "11";
        thc12.Text = "12";
        thc13.Text = "13";
        thc14.Text = "14";
        thc15.Text = "15";
        thc16.Text = "16";
        thc17.Text = "17";
        thc18.Text = "18";
        thc19.Text = "19";
        thc20.Text = "20";
        thc21.Text = "21";
        thc22.Text = "22";
        thc23.Text = "23";
        thc24.Text = "24";
        thc25.Text = "25";
        thc26.Text = "26";
        thc27.Text = "27";
        thc28.Text = "28";

        DateTime myDateTime = DateTime.Now;
        string year = myDateTime.Year.ToString();
        int days = DateTime.DaysInMonth(int.Parse(year), int.Parse(ddl_month_report.SelectedValue));

        if (ddl_month_report.SelectedValue == "1" || ddl_month_report.SelectedValue == "3" || ddl_month_report.SelectedValue == "5" || ddl_month_report.SelectedValue == "7" || ddl_month_report.SelectedValue == "8" || ddl_month_report.SelectedValue == "10" || ddl_month_report.SelectedValue == "12") //Day 31
        {
            thc29.Text = "29";
            thc30.Text = "30";
            thc31.Text = "31";
            thc31.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        }
        else if (ddl_month_report.SelectedValue == "2")
        {
            if (days == 29)
            {
                thc29.Text = "29";
            }
        }
        else 
        {
            thc29.Text = "29";
            thc30.Text = "30";
            thc31.Text = "";
        }

        thc1.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc2.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc3.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc4.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc5.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc6.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc7.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc8.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc9.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc10.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc11.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc12.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc13.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc14.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc15.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc16.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc17.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc18.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc19.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc20.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc21.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc22.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc23.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc24.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc25.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc26.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc27.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc28.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc29.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc30.Style.Add(HtmlTextWriterStyle.TextAlign, "center");
        thc31.Style.Add(HtmlTextWriterStyle.TextAlign, "center");


        gr.Cells.AddRange(new TableCell[] { thc1, thc2, thc3, thc4, thc5, thc6, thc7, thc8, thc9, thc10, thc11, thc12, thc13, thc14, thc15, thc16, thc17, thc18, thc19, thc20, thc21, thc22, thc23, thc24, thc25, thc26, thc27, thc28, thc29, thc30, thc31 });
        GvEmployee_Report.Controls[0].Controls.AddAt(1, gr);
    }

    #endregion

    #region custom data format and display
    protected string formatDate(string dateIN)
    {
        return DateTime.Parse(dateIN).ToString("DD/MM/YYYY HH:mm");
    }

    protected string formatDateTime(string dateIN)
    {
        return DateTime.Parse(dateIN).ToString("dd/MM/yyyy HH:mm");
    }

    #endregion

    #region FvDetail_DataBound
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {
            case "FvViewApprove":

                break;
            case "FvEdit_Detail":

                DropDownList ddlpartime_edit = (DropDownList)FvEdit_Detail.FindControl("ddlpartime_edit");
                Label lbl_partime_idx = (Label)FvEdit_Detail.FindControl("lbl_partime_idx");
                
                if (FvEdit_Detail.CurrentMode == FormViewMode.Edit)
                {
                    select_parttime(ddlpartime_edit);
                    ddlpartime_edit.SelectedValue = lbl_partime_idx.Text;
                }

                break;
            case "FvSearch_Report":

                DropDownList ddl_year_report = (DropDownList)FvSearch_Report.FindControl("ddl_year_report");
                //Label lbl_partime_idx = (Label)FvEdit_Detail.FindControl("lbl_partime_idx");

                if (FvSearch_Report.CurrentMode == FormViewMode.Insert)
                {
                    //select_parttime(ddlpartime_edit);
                    //ddl_year_report.SelectedValue = "2019";
                }

                break;
        }
    }
    #endregion

    #region ChkSelectedIndexChanged
    protected void chkSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (CheckBox)sender;

        switch (ddName.ID)
        {
            case "CheckBox1":

                var BoxGroupDay = (Panel)ViewShift.FindControl("BoxGroupDay");
                var BoxSearchGroup = (Panel)BoxGroupDay.FindControl("BoxSearchGroup");
                var GvEmployeeDB_ = (GridView)BoxSearchGroup.FindControl("GvEmployeeDB");
                //var chk_db = (CheckBox)GvEmployeeDB_.FindControl("CheckBox1");
                //var lbemp_code = (Label)GvEmployeeDB.FindControl("lbemp_code");
                //var BoxReference = (Panel)FvInsertEmp.FindControl("BoxReference");
                //fs.Text = "22222";//lbemp_code.Text.ToString();
                //CheckBox chkAll = (CheckBox)GvEmployeeDB_.HeaderRow.FindControl("checkAll");
                CheckBox chk_db = (CheckBox)GvEmployeeDB_.FindControl("checkAll");
                //CheckBox chkSel = (CheckBox)gvRow.FindControl("YrChkBox_Approve");
                //if (chk_db.Checked)
                //{
                    //CultureInfo culture = new CultureInfo("en-US");
                    //Thread.CurrentThread.CurrentCulture = culture;

                    //DataSet dsContacts = (DataSet)ViewState["vsTemp_Emps"];

                    /*foreach (DataRow dr in dsContacts.Tables["dsTemp_Emps"].Rows)
                    {

                        if (dr["emp_code"].ToString() == "57000001")
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                            fs.Text = "11111";
                            //chk_db.Enabled = true;
                            //chk_db.Checked = true;
                            return;
                        }
                        else
                        {
                            //chk_db.Enabled = false;
                            fs.Text = "22222";
                        }
                    }*/
                //}

                /*CheckBox chkAll = (CheckBox)GvApproveAll.HeaderRow.FindControl("chkApproveAll");

                if (chkAll.Checked == true)
                {
                    foreach (GridViewRow gvRow in GvApproveAll.Rows)
                    {
                        CheckBox chkSel = (CheckBox)gvRow.FindControl("YrChkBox_Approve");
                        chkSel.Checked = true;
                    }
                }
                else
                {
                    foreach (GridViewRow gvRow in GvApproveAll.Rows)
                    {
                        CheckBox chkSel = (CheckBox)gvRow.FindControl("YrChkBox_Approve");
                        chkSel.Checked = false;
                    }
                }*/

                
                //if (chk_db.Checked)
                //{
                //CultureInfo culture = new CultureInfo("en-US");
                //Thread.CurrentThread.CurrentCulture = culture;

                //DataSet dsContacts = (DataSet)ViewState["vsTemp_Emps"];

                /*foreach (DataRow dr in GvEmployeeTemp.Rows)
                {

                    if (dr["emp_code"].ToString() == lbemp_code.Text)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                        return;
                    }

                    chk_db.Checked = false;
                }*/

                //foreach (GridViewRow row in GvEmployeeTemp_add.Rows)
                //{
                //if (row.RowType == DataControlRowType.DataRow)
                //{
                //Label lbl_emp_code_temp = (Label)row.Cells[1].FindControl("emp_code");
                //fs.Text = lbl_emp_code_temp.Text.ToString();
                /*if (lbl_emp_code_temp.Text == lbemp_code.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }

                chk_db.Checked = false;*/
                //}
                //}

                /*DataRow drContacts = dsContacts.Tables["dsAddListTable_Topic"].NewRow();

                drContacts["m1_type_name"] = txttopic.Text;
                drContacts["m0_typeidx"] = int.Parse(ddltype.SelectedValue);
                drContacts["status_name"] = ddlstatus.SelectedItem.Text;
                drContacts["status"] = int.Parse(ddlstatus.SelectedValue);

                dsContacts.Tables["dsAddListTable_Topic"].Rows.Add(drContacts);
                ViewState["vsAddListTable_Topic"] = dsContacts;*/

                //setGridData(gvName, dsContacts.Tables["dsAddListTable_Topic"]);
                //gvName.Visible = true;
                /*}
                else
                {
                    //GvReferenceAdd.DataSource = null;
                    //GvReferenceAdd.DataBind();
                }*/
                break;

        }
    }
    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;
        DropDownList ddl_org_report = (DropDownList)FvSearch_Report.FindControl("ddl_org_report");
        DropDownList ddl_dep_report = (DropDownList)FvSearch_Report.FindControl("ddl_dep_report");
        DropDownList ddl_sec_report = (DropDownList)FvSearch_Report.FindControl("ddl_sec_report");
        DropDownList ddl_pos_report = (DropDownList)FvSearch_Report.FindControl("ddl_pos_report");
        //DropDownList ddl_month_report = (DropDownList)FvSearch_Report.FindControl("ddl_month_report");
        //DropDownList ddl_shift_report = (DropDownList)FvSearch_Report.FindControl("ddl_shift_report");

        switch (ddName.ID)
        {
            case "ddlIndexOrgSearch":
                DropDownList ddlIndexOrgSearch = (DropDownList)Fv_Search_Shift_Index.FindControl("ddlIndexOrgSearch");
                DropDownList ddlIndexDeptSearch = (DropDownList)Fv_Search_Shift_Index.FindControl("ddlIndexDeptSearch");

                select_dep(ddlIndexDeptSearch, int.Parse(ddlIndexOrgSearch.SelectedValue));
                break;
            case "ddlorg":
                select_dep(ddldep, int.Parse(ddlorg.SelectedValue));
                break;
            case "ddldep":
                select_sec(ddlsec, int.Parse(ddlorg.SelectedValue), int.Parse(ddldep.SelectedValue));
                break;
            case "ddlsec":
                //select_pos(ddlpos, int.Parse(ddlorg.SelectedValue), int.Parse(ddldep.SelectedValue), int.Parse(ddlsec.SelectedValue));
                ddlPos_Add_CreateAll(int.Parse(ddlsec.SelectedValue),ddlpos);
                break;
            case "ddlorg_add":
                select_dep(ddldep_add, int.Parse(ddlorg_add.SelectedValue));
                break;
            case "ddldep_add":
                select_sec(ddlsec_add, int.Parse(ddlorg_add.SelectedValue), int.Parse(ddldep_add.SelectedValue));
                break;
            case "ddlsec_add":
                //select_pos(ddlpos, int.Parse(ddlorg.SelectedValue), int.Parse(ddldep.SelectedValue), int.Parse(ddlsec.SelectedValue));
                ddlPos_Add_CreateAll(int.Parse(ddlsec.SelectedValue),ddlpos_add);
                break;
            case "ddl_org_report":
                select_dep(ddl_dep_report, int.Parse(ddl_org_report.SelectedValue));
                break;
            case "ddl_dep_report":
                select_sec(ddl_sec_report, int.Parse(ddl_org_report.SelectedValue), int.Parse(ddl_dep_report.SelectedValue));
                break;
            case "ddl_sec_report":
                ddlPos_Add_CreateAll(int.Parse(ddl_sec_report.SelectedValue), ddl_pos_report);
                break;
            /*case "ddl_month_report":
                //Select_shift_month_report(ddl_shift_report,ddl_month_report.SelectedValue);
                break;*/
        }
    }

    #endregion

    #region BTN
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "btnIndex":              
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewIndex);
                break;
            case "btn_search_index":
                Select_Employee_index();             
                break;
            case "btn_search_report":
                Select_Employee_Report();
                break;
            case "Cmdsearch_clear":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnshift":
                Menu_Color(int.Parse(cmdArg));              
                MvMaster.SetActiveView(ViewShift);
                select_parttime(ddlparttime);
                Menu_Color_Page_Add(1);
                Menu_Color_Page_Add(3);

                if (ViewState["permission_check"].ToString() == "0")
                {
                    txt_search_name_mount.Text = ViewState["FullName"].ToString();
                    txt_search_empcode_mount.Text = ViewState["EmpCode"].ToString();
                    txt_search_name_mount.Enabled = false;
                    txt_search_empcode_mount.Enabled = false;
                }

                Session["u0_empshift_idx"] = 0;
                if (int.Parse(cmdArg) == 2)
                {
                    ViewState["emptype_search"] = 1; //รายวัน
                }
                else
                {
                    ViewState["emptype_search"] = 2; //รายเดือน
                }
                break;
            case "btnshiftchange":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewShiftchange);
                break;
            case "btnshiftholidayswap":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewShiftholidayswap);
                break;
            case "btnwaiting":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewWaiting);
                break;
            case "btnapprove":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewApprove);
                Select_Employee_Approve();
                boxGvApprove.Visible = true;
                boxviewApprove.Visible = false;
                break;
            case "btnhr":
                Menu_Color(int.Parse(cmdArg));
                MvMaster.SetActiveView(ViewHr);
                break;
            case "btnsearchtranfer":
                SelectGvGroupNameList(GvGroupName , txtgroupname.Text, int.Parse(ddlgroupname.SelectedValue));
                break;
            case "btnsearchtranfer_add":
                SelectGvGroupNameList(GvGroupName_add, txtgroupname_add.Text, int.Parse(ddlgroupname_add.SelectedValue));
                break;
            case "btnsearchtranfer_mount":
                if (txt_search_empcode_mount.Text != null)
                {
                    ViewState["empcode_search"] = txt_search_empcode_mount.Text;
                }
                else
                {
                    ViewState["empcode_search"] = "";
                }

                if (txt_search_name_mount.Text != null)
                {
                    ViewState["name_th_search"] = txt_search_name_mount.Text;
                }
                else
                {
                    ViewState["name_th_search"] = "";
                }

                //ViewState["org_idx_search"] = ddlorg.SelectedValue;          
                ViewState["rsec_idx_search"] = ddlsec.SelectedValue;
                ViewState["rpos_idx_search"] = ddlpos.SelectedValue;
                var GvEmployeeDB = (GridView)ViewShift.FindControl("GvEmployeeDB");

                if (ViewState["permission_check"].ToString() == "1")
                {
                    if (ddlsec.SelectedValue != "0")
                    {
                        Select_Employee_DB(GvEmployeeDB);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('*** กรุณาเลือกแผนก')", true);
                    }
                }
                else
                {
                    if (txt_search_empcode_mount.Text.ToString() != "" || txt_search_name_mount.Text.ToString() != "")                   
                    {
                        Select_Employee_DB(GvEmployeeDB);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('*** กรุณากรอกข้อมูลให้ครบถ้วน')", true);
                    }
                }
                break;
            case "btnsearchtranfer_mount_add":
                if (txt_search_empcode_mount_add.Text != null)
                {
                    ViewState["empcode_search"] = txt_search_empcode_mount_add.Text;
                }
                else
                {
                    ViewState["empcode_search"] = "";
                }

                if (txt_search_name_mount_add.Text != null)
                {
                    ViewState["name_th_search"] = txt_search_name_mount_add.Text;
                }
                else
                {
                    ViewState["name_th_search"] = "";
                }

                //ViewState["org_idx_search"] = ddlorg_add.SelectedValue;          
                ViewState["rsec_idx_search"] = ddlsec_add.SelectedValue;
                ViewState["rpos_idx_search"] = ddlpos_add.SelectedValue;
                //ViewState["emptype_search"] = 1; //รายวัน
                var GvEmployeeDB_add = (GridView)ViewIndex.FindControl("GvEmployeeDB_add");

                if (ViewState["permission_check"].ToString() == "1")
                {
                    if (ddlsec_add.SelectedValue != "0")
                    {
                        Select_Employee_DB(GvEmployeeDB_add);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('*** กรุณาเลือกแผนก')", true);
                    }
                }
                else
                {
                    if (txt_search_empcode_mount_add.Text.ToString() != "" || txt_search_name_mount_add.Text.ToString() != "")
                    {
                        Select_Employee_DB(GvEmployeeDB_add);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('*** กรุณากรอกข้อมูลให้ครบถ้วน')", true);
                    }
                }
                break;
            case "btnList_DB":
                Menu_Color_Page_Add(int.Parse(cmdArg));
                break;
            case "btnList_Temp":
                Menu_Color_Page_Add(int.Parse(cmdArg));
                break;
            case "btnList_DB_edit":
                Menu_Color_Page_Add(int.Parse(cmdArg));
                break;
            case "btnList_Temp_edit":
                Menu_Color_Page_Add(int.Parse(cmdArg));
                break;
            case "btnList_Temp_add":
                Menu_Color_Page_Add(int.Parse(cmdArg));
                break;
            case "btnList_Temp_approve":
                Menu_Color_Page_Add(int.Parse(cmdArg));
                break;
            case "btnselect":
                GridView GvGroupName_Temp = (GridView)ViewShift.FindControl("GvGroupName_Temp");
                GridView GvGroupName_Database = (GridView)ViewShift.FindControl("GvGroupName");
                string[] arg_sw = new string[3];
                arg_sw = e.CommandArgument.ToString().Split(';');
                int rsec_idx_ref = int.Parse(arg_sw[0]);
                int m0_group_diary_idx = int.Parse(arg_sw[1]);
                string group_diary_name = arg_sw[2];

                var dsDevice_sw = (DataSet)ViewState["vsTemp_ODSP"];
                var drDevice_sw = dsDevice_sw.Tables[0].NewRow();
                drDevice_sw["rsec_idx_ref"] = rsec_idx_ref;
                drDevice_sw["m0_group_diary_idx"] = m0_group_diary_idx;
                drDevice_sw["group_diary_name"] = group_diary_name;

                dsDevice_sw.Tables[0].Rows.Add(drDevice_sw);

                ViewState["vsTemp_ODSP"] = dsDevice_sw;
                GvGroupName_Temp.DataSource = dsDevice_sw.Tables[0];
                GvGroupName_Temp.DataBind();

                break;
            case "btnselect_add":
                GridView GvGroupName_Temp_add = (GridView)ViewIndex.FindControl("GvGroupName_Temp_add");
                GridView GvGroupName_Database_add = (GridView)ViewIndex.FindControl("GvGroupName_add");
                string[] arg_sw_add = new string[3];
                arg_sw_add = e.CommandArgument.ToString().Split(';');
                int rsec_idx_ref_add = int.Parse(arg_sw_add[0]);
                int m0_group_diary_idx_add = int.Parse(arg_sw_add[1]);
                string group_diary_name_add = arg_sw_add[2];

                var dsDevice_sw_add = (DataSet)ViewState["vsTemp_ODSP_add"];
                var drDevice_sw_add = dsDevice_sw_add.Tables[0].NewRow();
                drDevice_sw_add["rsec_idx_ref"] = rsec_idx_ref_add;
                drDevice_sw_add["m0_group_diary_idx"] = m0_group_diary_idx_add;
                drDevice_sw_add["group_diary_name"] = group_diary_name_add;

                dsDevice_sw_add.Tables[0].Rows.Add(drDevice_sw_add);

                ViewState["vsTemp_ODSP_add"] = dsDevice_sw_add;
                GvGroupName_Temp_add.DataSource = dsDevice_sw_add.Tables[0];
                GvGroupName_Temp_add.DataBind();

                break;
            case "btnselect_mount":
                GridView GvEmployeeDB_mount = (GridView)ViewShift.FindControl("GvEmployeeDB");
                GridView GvEmployeeTemp_mount = (GridView)ViewShift.FindControl("GvEmployeeTemp");
                GridView GvEmployeeTemp_mount_insert = (GridView)ViewShift.FindControl("GvEmployeeTemp_Insert");

                /*string[] ar_sw = new string[3];
                ar_sw = e.CommandArgument.ToString().Split(';');
                int emp_idx_ = int.Parse(ar_sw[0]);
                string emp_code_ = ar_sw[1];
                string emp_name_th_ = ar_sw[2];*/

                /*var ds_sw_ = (DataSet)ViewState["vsTemp_Emps"];

                var ds_sw = ds_sw_.Tables[0].NewRow();
                ds_sw["emp_idx"] = emp_idx_;
                ds_sw["emp_code"] = emp_code_;
                ds_sw["emp_name_th"] = emp_name_th_.ToString();

                ds_sw_.Tables[0].Rows.Add(ds_sw);
                ViewState["vsTemp_Emps"] = ds_sw_;
                GvEmployeeTemp_mount.DataSource = ds_sw_.Tables[0];
                GvEmployeeTemp_mount.DataBind();*/

                string emp_idx_db = "0";
                string emp_code_db = "";
                int _chk_row = 0;

                try
                {

                CheckBox ChkBoxHeader = (CheckBox)GvEmployeeDB_mount.HeaderRow.FindControl("checkAll");

                foreach (GridViewRow row in GvEmployeeDB_mount.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkRow1 = (CheckBox)row.Cells[0].FindControl("CheckBox1");

                        if (chkRow1.Checked == true)
                        {
                           
                            emp_idx_db = (row.Cells[1].FindControl("lbemp_idx") as Label).Text;
                            Label lbemp_code = (Label)row.Cells[1].FindControl("lbemp_code");
                            Label lbfm_name = (Label)row.Cells[2].FindControl("lbfm_name");

                            if (emp_idx_db != "")
                            {
                                var ds_sw_ = (DataSet)ViewState["vsTemp_Emps"];
                                var ds_sw = ds_sw_.Tables[0].NewRow();
                                ds_sw["emp_idx"] = int.Parse(emp_idx_db.ToString());
                                ds_sw["emp_code"] = lbemp_code.Text.ToString();
                                ds_sw["emp_name_th"] = lbfm_name.Text.ToString();

                                ds_sw_.Tables[0].Rows.Add(ds_sw);
                                ViewState["vsTemp_Emps"] = ds_sw_;

                                _chk_row = 0;

                            }
                            else
                            {
                                _chk_row = 1;
                            }
                        }
                    }
                }
                        
                if (_chk_row == 0) //มีข้อมูล
                {
                    var ds_sw_ = (DataSet)ViewState["vsTemp_Emps"];

                    var AddPur1 = new empshift[ds_sw_.Tables[0].Rows.Count];
                    int i1 = 0;

                    foreach (DataRow dr1 in ds_sw_.Tables[0].Rows)
                    {
                        AddPur1[i1] = new empshift();
                        AddPur1[i1].emp_idx = int.Parse(dr1["emp_idx"].ToString());
                        AddPur1[i1].emp_code = dr1["emp_code"].ToString();
                        AddPur1[i1].emp_name_th = dr1["emp_name_th"].ToString();

                        i1++;

                        _dataEmps.emps_empshift_action = AddPur1;
                    }


                    //ViewState["vsTemp_Emps"] = _dataEmps.emps_empshift_action;

                    /*foreach (GridViewRow row in GvEmployeeTemp.Rows)
                    {
                        if (row.RowType == DataControlRowType.DataRow)
                        {
                            Label lbl_emp_idx = (Label)row.Cells[0].FindControl("lbl_emp_idx");
                            //ViewState["emp_idx_insert"] += lbl_emp_idx.Text + ",";
                        }
                    }*/

                    /*var _linqTotalsex = (from data in _dataEmps.emps_empshift_action
                                         where count_row == data.emp_idx
                                         select new
                                         {
                                             data.emp_idx,
                                             data.emp_code,
                                             data.emp_name_th,

                                         }).ToList();*/

                    GvEmployeeTemp_mount.DataSource = AddPur1;
                    GvEmployeeTemp_mount.DataBind();

                    GvEmployeeTemp_mount_insert.DataSource = AddPur1;
                    GvEmployeeTemp_mount_insert.DataBind();

                    ViewState["vsTemp_Emps_1"] = AddPur1;

                }
                else
                {
                    //funcWeb.ShowAlert(this, "กรุณาเลือกกรอกข้อมูลให้ครบถ้วน");
                }

                _chk_row = 0;

                }
                catch (Exception ex)
                {

                }

                break;

            case "btnselect_mount_add":
                GridView GvEmployeeDB_mount_add = (GridView)ViewIndex.FindControl("GvEmployeeDB_add");
                GridView GvEmployeeTemp_mount_add = (GridView)ViewIndex.FindControl("GvEmployeeTemp_add");
                GridView GvEmployeeTemp_add_Edit_ = (GridView)ViewIndex.FindControl("GvEmployeeTemp_add_Edit");

                string[] ar_sw_add = new string[3];
                ar_sw_add = e.CommandArgument.ToString().Split(';');
                int emp_idx_add = int.Parse(ar_sw_add[0]);
                string emp_code_add = ar_sw_add[1];
                string emp_name_th_add = ar_sw_add[2];

                var ds_sw_add = (DataSet)ViewState["vsTemp_Emps_add"];

                var ds_sw_add_ = ds_sw_add.Tables[0].NewRow();
                ds_sw_add_["emp_idx"] = emp_idx_add;
                ds_sw_add_["emp_code"] = emp_code_add;
                ds_sw_add_["emp_name_th"] = emp_name_th_add.ToString();

                ds_sw_add.Tables[0].Rows.Add(ds_sw_add_);
                ViewState["vsTemp_Emps_add"] = ds_sw_add;
                GvEmployeeTemp_mount_add.DataSource = ds_sw_add.Tables[0];
                GvEmployeeTemp_mount_add.DataBind();

                GvEmployeeTemp_add_Edit_.DataSource = ds_sw_add.Tables[0];
                GvEmployeeTemp_add_Edit_.DataBind();
                break;

            case "btnselect_mount_edit":
                GridView GvEmployeeDB_add_ = (GridView)ViewIndex.FindControl("GvEmployeeDB_add");
                GridView GvEmployeeTemp_add_ = (GridView)ViewIndex.FindControl("GvEmployeeTemp_add");
                GridView GvEmployeeTemp_add_Edit = (GridView)ViewIndex.FindControl("GvEmployeeTemp_add_Edit");

                string emp_idx_db_ = "0";
                string emp_code_db_ = "";
                int _chk_row_ = 0;

                try
                {

                CheckBox ChkBoxHeader_ = (CheckBox)GvEmployeeDB_add_.HeaderRow.FindControl("checkAll_edit");

                foreach (GridViewRow row in GvEmployeeDB_add_.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        CheckBox chkRow1 = (CheckBox)row.Cells[0].FindControl("CheckBox1_edit");

                        if (chkRow1.Checked == true)
                        {

                            emp_idx_db_ = (row.Cells[1].FindControl("lbemp_idx1") as Label).Text;
                            Label lbemp_code1 = (Label)row.Cells[1].FindControl("lbemp_code1");
                            Label lbfm_name1 = (Label)row.Cells[2].FindControl("lbfm_name1");

                            if (emp_idx_db_ != "")
                            {
                                var ds_sw_ = (DataSet)ViewState["vsTemp_Emps_add"];
                                var ds_sw = ds_sw_.Tables[0].NewRow();
                                ds_sw["emp_idx"] = int.Parse(emp_idx_db_.ToString());
                                ds_sw["emp_code"] = lbemp_code1.Text.ToString();
                                ds_sw["emp_name_th"] = lbfm_name1.Text.ToString();

                                ds_sw_.Tables[0].Rows.Add(ds_sw);
                                ViewState["vsTemp_Emps_add"] = ds_sw_;

                                _chk_row_ = 0;

                            }
                            else
                            {
                                _chk_row_ = 1;
                            }
                        }
                    }
                }

                if (_chk_row_ == 0) //มีข้อมูล
                {
                    var ds_sw_ = (DataSet)ViewState["vsTemp_Emps_add"];

                    var AddPur1 = new empshift[ds_sw_.Tables[0].Rows.Count];
                    int i1 = 0;

                    foreach (DataRow dr1 in ds_sw_.Tables[0].Rows)
                    {
                        AddPur1[i1] = new empshift();
                        AddPur1[i1].emp_idx = int.Parse(dr1["emp_idx"].ToString());
                        AddPur1[i1].emp_code = dr1["emp_code"].ToString();
                        AddPur1[i1].emp_name_th = dr1["emp_name_th"].ToString();

                        i1++;

                        _dataEmps.emps_empshift_action = AddPur1;
                    }

                    GvEmployeeTemp_add_.DataSource = AddPur1;
                    GvEmployeeTemp_add_.DataBind();

                    GvEmployeeTemp_add_Edit.DataSource = AddPur1;
                    GvEmployeeTemp_add_Edit.DataBind();

                    ViewState["vsTemp_Emps_1"] = AddPur1;

                }
                else
                {
                    //funcWeb.ShowAlert(this, "กรุณาเลือกกรอกข้อมูลให้ครบถ้วน");
                }

                _chk_row = 0;

                }
                catch (Exception ex)
                {

                }

                break;

            case "btndel":
                string m0_group_diary_idx_1 = cmdArg;
                GridView GvGroupName_Temp_ = (GridView)ViewShift.FindControl("GvGroupName_Temp");
                var dsDevice_sw_1 = (DataSet)ViewState["vsTemp_ODSP"];
                ViewState["m0_group_diary_idx"] = m0_group_diary_idx_1;

                for (int counter = 0; counter < dsDevice_sw_1.Tables[0].Rows.Count; counter++)
                {
                    if (dsDevice_sw_1.Tables[0].Rows[counter]["m0_group_diary_idx"].ToString() == ViewState["m0_group_diary_idx"].ToString())
                    {
                        dsDevice_sw_1.Tables[0].Rows[counter].Delete();
                        break;
                    }
                }

                GvGroupName_Temp_.DataSource = (DataSet)ViewState["vsTemp_ODSP"];
                GvGroupName_Temp_.DataBind();
                break;
            case "btndel_add":
                string m0_group_diary_idx_1_add = cmdArg;
                GridView GvGroupName_Temp_add_ = (GridView)ViewShift.FindControl("GvGroupName_Temp_add");
                var dsDevice_sw_1_add = (DataSet)ViewState["vsTemp_ODSP_add"];
                ViewState["m0_group_diary_idx_add"] = m0_group_diary_idx_1_add;

                for (int counter = 0; counter < dsDevice_sw_1_add.Tables[0].Rows.Count; counter++)
                {
                    if (dsDevice_sw_1_add.Tables[0].Rows[counter]["m0_group_diary_idx"].ToString() == ViewState["m0_group_diary_idx_add"].ToString())
                    {
                        dsDevice_sw_1_add.Tables[0].Rows[counter].Delete();
                        break;
                    }
                }

                GvGroupName_Temp_add_.DataSource = (DataSet)ViewState["vsTemp_ODSP_add"];
                GvGroupName_Temp_add_.DataBind();
                break;
            case "btndel_mount":
                string emp_idx_1 = cmdArg;
                GridView GvEmployeeTemp_mount_ = (GridView)ViewShift.FindControl("GvEmployeeTemp");
                var dsD_sw_1 = (DataSet)ViewState["vsTemp_Emps"];
                ViewState["emp_idx_mount"] = emp_idx_1;

                for (int counter = 0; counter < dsD_sw_1.Tables[0].Rows.Count; counter++)
                {
                    if (dsD_sw_1.Tables[0].Rows[counter]["emp_idx"].ToString() == ViewState["emp_idx_mount"].ToString())
                    {
                        dsD_sw_1.Tables[0].Rows[counter].Delete();
                        break;
                    }
                }

                GvEmployeeTemp_mount_.DataSource = (DataSet)ViewState["vsTemp_Emps"];
                GvEmployeeTemp_mount_.DataBind();
                break;
            case "btndel_ListDB_edit":
                string[] arg_edit = new string[4];
                arg_edit = e.CommandArgument.ToString().Split(';');
                int u0_empshift_idx_edit = int.Parse(arg_edit[0]);
                int emp_idx_edit = int.Parse(arg_edit[1]);

                Delete_GvListDB(u0_empshift_idx_edit , emp_idx_edit);
                SelectGvListDB_edit(GvListDB_edit, u0_empshift_idx_edit);
                break;
            case "btndel_GroupDB_edit":
                string[] arg_edit_ = new string[4];
                arg_edit_ = e.CommandArgument.ToString().Split(';');
                int u0_empshift_idx_edit_ = int.Parse(arg_edit_[0]);
                string u0_group_idx = arg_edit_[1];

                Delete_GvGroupDB(u0_empshift_idx_edit_, u0_group_idx);
                SelectGvGroupDB_edit(GvGroupDB_edit, u0_empshift_idx_edit_);
                SelectGvListDB_edit(GvListDB_edit, u0_empshift_idx_edit_);
                break;
            case "btndel_mount_add":
                string emp_idx_1_add = cmdArg;
                GridView GvEmployeeTemp_mount_add_ = (GridView)ViewShift.FindControl("GvEmployeeTemp_add");
                var dsD_sw_1_add = (DataSet)ViewState["vsTemp_Emps_add"];
                ViewState["emp_idx_mount_add"] = emp_idx_1_add;

                for (int counter = 0; counter < dsD_sw_1_add.Tables[0].Rows.Count; counter++)
                {
                    if (dsD_sw_1_add.Tables[0].Rows[counter]["emp_idx"].ToString() == ViewState["emp_idx_mount_add"].ToString())
                    {
                        dsD_sw_1_add.Tables[0].Rows[counter].Delete();
                        break;
                    }
                }

                GvEmployeeTemp_mount_add_.DataSource = (DataSet)ViewState["vsTemp_Emps_add"];
                GvEmployeeTemp_mount_add_.DataBind();
                break;
            case "btnSave":
                Insert_create();
                
                break;
            case "btnSave_edit":
                Insert_edit(int.Parse(cmdArg));
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnapprove_detail":
                string[] arg1 = new string[4];
                arg1 = e.CommandArgument.ToString().Split(';');
                int u0_empshift_idx = int.Parse(arg1[0]);
                int u0_unidx = int.Parse(arg1[1]);
                int u0_acidx = int.Parse(arg1[2]);
                int u0_doc_decision = int.Parse(arg1[3]);

                ViewState["u0_empshift_idx_approve"] = u0_empshift_idx.ToString();
                ViewState["u0_unidx_approve"] = u0_unidx.ToString();
                ViewState["u0_acidx_approve"] = u0_acidx.ToString();
                ViewState["u0_doc_decision_approve"] = u0_doc_decision.ToString();
                Session["u0_empshift_idx"] = u0_empshift_idx;

                FvViewApprove.ChangeMode(FormViewMode.Insert);
                boxGvApprove.Visible = false;
                boxviewApprove.Visible = true;
                Select_RptLog_Detail(u0_empshift_idx);
                SelectGvListDB_edit(GvListDB_approve , u0_empshift_idx);
                SelectGvGroupDB_edit(GvGroupDB_approve , u0_empshift_idx);
                Menu_Color_Page_Add(11);
                break;
            case "btn_Approve":
                Update_Approve();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btn_Approve_back":
                boxGvApprove.Visible = true;
                boxviewApprove.Visible = false;
                break;

            case "btnedit_detail":
                string[] arg2 = new string[4];
                arg2 = e.CommandArgument.ToString().Split(';');
                int u0_empshift_idx_ = int.Parse(arg2[0]);

                FvEdit_Detail.ChangeMode(FormViewMode.Edit);
                Select_FvEdit_Detail(u0_empshift_idx_);
                SelectGvListDB_edit(GvListDB_edit , u0_empshift_idx_);
                SelectGvGroupDB_edit(GvGroupDB_edit , u0_empshift_idx_);

                boxgv_shiftime.Visible = false;
                boxsearch_shiftime.Visible = false;
                boxedit_shiftime.Visible = true;

                Menu_Color_Page_Add(5);
                Menu_Color_Page_Add(7);
                Menu_Color_Page_Add(9);
                select_org(ddlorg_add);
                select_groupname(ddlgroupname_add);
                Session["u0_empshift_idx"] = u0_empshift_idx_;
                ViewState["u0_empshift_idx_edit"] = u0_empshift_idx_;

                if (ViewState["permission_check"].ToString() == "0")
                {
                    txt_search_name_mount_add.Text = ViewState["FullName"].ToString();
                    txt_search_empcode_mount_add.Text = ViewState["EmpCode"].ToString();
                    txt_search_name_mount_add.Enabled = false;
                    txt_search_empcode_mount_add.Enabled = false;
                }
                break;

            case "btnCancel_edit":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnview_gridview_show":
                Box_viewhr_gridview.Visible = true;
                btnview_gridview_show.Visible = false;
                btnview_gridview_hide.Visible = true;
                break;
            case "btnview_gridview_hide":
                Box_viewhr_gridview.Visible = false;
                btnview_gridview_show.Visible = true;
                btnview_gridview_hide.Visible = false;
                break;
            case "btnview_calendar_show":
                Box_viewhr_calendar.Visible = true;
                btnview_calendar_show.Visible = false;
                btnview_calendar_hide.Visible = true;
                break;
            case "btnview_calendar_hide":
                Box_viewhr_calendar.Visible = false;
                btnview_calendar_show.Visible = true;
                btnview_calendar_hide.Visible = false;
                break;

            case "_divMenuBtnToDivManual":
                Response.Write("<script>window.open('https://docs.google.com/document/d/1vALu0Cpy_Ckc4wKEmOCXXulDyIPy5HfNNbl3qoxUjIo/edit?usp=sharing','_blank');</script>");

                break;
            case "btnexport_general":
                //Select_Employee_Report();
                if (ViewState["Box_dataEmployee_Report"] != null)
                {
                    GvEmployee_Report.AllowPaging = false;
                    GvEmployee_Report.DataSource = ViewState["Box_dataEmployee_Report"];
                    GvEmployee_Report.DataBind();

                    Response.Clear();
                    Response.Buffer = true;
                    Response.AddHeader("content-disposition", "attachment;filename=EmployeeExport.xls");
                    // Response.Charset = ""; set character 
                    Response.Charset = "utf-8";
                    Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                    Response.ContentType = "application/vnd.ms-excel";

                    Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                    Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
                    //Response.Write(@"<style>.number {mso-number-format:0\.00; } </style>");

                    StringWriter sw_4 = new StringWriter();
                    HtmlTextWriter hw_4 = new HtmlTextWriter(sw_4);

                    /*for (int i = 0; i < GvEmployee_Report.Rows.Count; i++)
                    {
                        //Apply text style to each Row
                        GvEmployee_Report.Rows[i].Attributes.Add("class", "number2");
                    }*/
                    GvEmployee_Report.RenderControl(hw_4);

                    //Amount is displayed in number format with 2 decimals
                    Response.Write(@"<style>.number2 {mso-number-format:0; } </style>");

                    Response.Output.Write(sw_4.ToString());
                    Response.Flush();
                    Response.End();
                }

                break;
        }
    }
    #endregion
}