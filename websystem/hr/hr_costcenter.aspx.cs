﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_hr_costcenter : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    service_execute _serviceExecute = new service_execute();

    string _localXml = String.Empty;
    string _localString = String.Empty;
    string _localJson = String.Empty;
    int _actionType = 0;
    int _emp_idx = 0;
    int _default_int = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetCostcenterOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetCostcenterOld"];
    static string _urlGetInsertCostcenter = _serviceUrl + ConfigurationManager.AppSettings["urlGetInsertCostcenter"];
    static string _urlGetUpdateCostcenter = _serviceUrl + ConfigurationManager.AppSettings["urlGetUpdateCostcenter"];
    static string _urlGetDeleteCostcenter = _serviceUrl + ConfigurationManager.AppSettings["urlGetDeleteCostcenter"];

    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();
        }

    }
    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Insert_Status()
    {
        _dataEmployee.CostCenterDetail = new CostCenter[1];
        CostCenter dtemployee = new CostCenter();

        dtemployee.CostName = txtcusname.Text;
        dtemployee.CostNo = txtcusno.Text;
        dtemployee.LocIDX = int.Parse(ddlorg.SelectedValue);
        dtemployee.CostStatus = int.Parse(ddl_status.SelectedValue);
        
        _dataEmployee.CostCenterDetail[0] = dtemployee;
        //text.Text = dtemployee.ToString();
        _dataEmployee = callService(_urlGetInsertCostcenter, _dataEmployee);
    }

    protected void SelectMasterList()
    {
        _dataEmployee.CostCenterDetail = new CostCenter[1];
        CostCenter dtemployee = new CostCenter();

        _dataEmployee.CostCenterDetail[0] = dtemployee;

        _dataEmployee = callService(_urlGetCostcenterOld, _dataEmployee);
        //text.Text = _dataEmployee.ToString();
        setGridData(GvMaster, _dataEmployee.CostCenterDetail);
    }

    protected data_employee callService(string _cmdUrl, data_employee _dtmaster)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmaster);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtmaster = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);
        
        return _dtmaster;
    }

    protected void Update_Master_List()
    {
        _dataEmployee.CostCenterDetail = new CostCenter[1];
        CostCenter dtemployee = new CostCenter();

        dtemployee.CostIDX = int.Parse(ViewState["CostIDX_update"].ToString());
        dtemployee.CostName = ViewState["txtcusname_update"].ToString();
        dtemployee.CostNo = ViewState["txtcusno_update"].ToString();
        dtemployee.LocIDX = int.Parse(ViewState["ddlorg_update"].ToString());
        dtemployee.CostStatus = int.Parse(ViewState["ddStatus_update"].ToString());

        _dataEmployee.CostCenterDetail[0] = dtemployee;
        //text.Text = dtemployee.ToString();
        _dataEmployee = callService(_urlGetUpdateCostcenter, _dataEmployee);
    }

    protected void Delete_Master_List()
    {
        _dataEmployee.CostCenterDetail = new CostCenter[1];
        CostCenter dtemployee = new CostCenter();

        dtemployee.CostIDX = int.Parse(ViewState["CostIDX_delete"].ToString());

        _dataEmployee.CostCenterDetail[0] = dtemployee;
        //text.Text = dtemployee.ToString();
        _dataEmployee = callService(_urlGetDeleteCostcenter, _dataEmployee);
    }

    protected void select_org(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServiceEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("เลือกองค์กร....", "0"));
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);
        return _dataEmployee;
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        var ddlorg_edit = (DropDownList)e.Row.FindControl("ddlorg_edit");
                        var lblorg_edit = (Label)e.Row.FindControl("lblorg_edit");
                        
                        /*var txtstatus_edit = (TextBox)e.Row.FindControl("txtstatus_edit");
                        var ddl_downtime_edit = (DropDownList)e.Row.FindControl("ddl_downtime_edit");
                        var lbDTIDX = (Label)e.Row.FindControl("lbDTIDX");

                        ddl_downtime_edit.AppendDataBoundItems = true;

                        _dtsupport.BoxStatusSAP = new StatusSAP[1];
                        StatusSAP dtsupport = new StatusSAP();

                        _dtsupport.BoxStatusSAP[0] = dtsupport;

                        _dtsupport = callServiceDevices(_urlGetDowntime, _dtsupport);


                        ddl_downtime_edit.DataSource = _dtsupport.BoxStatusSAP;
                        ddl_downtime_edit.DataTextField = "downtime_name";
                        ddl_downtime_edit.DataValueField = "dtidx";
                        ddl_downtime_edit.DataBind();
                        ddl_downtime_edit.SelectedValue = lbDTIDX.Text;*/
                        select_org(ddlorg_edit);
                        ddlorg_edit.SelectedValue = lblorg_edit.Text;

                    }

                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();
                

                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int CostIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtcusno_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtcusno_update");
                var txtcusname_update = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtcusname_update");
                var ddStatus_update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatus_update");
                var ddlorg_update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlorg_edit");

                GvMaster.EditIndex = -1;

                ViewState["CostIDX_update"] = CostIDX;
                ViewState["txtcusno_update"] = txtcusno_update.Text;
                ViewState["txtcusname_update"] = txtcusname_update.Text;
                ViewState["ddStatus_update"] = ddStatus_update.SelectedValue;
                ViewState["ddlorg_update"] = ddlorg_update.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion


    #endregion


    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAddHolder":
                btnaddholder.Visible = false;
                Panel_Add.Visible = true;
                select_org(ddlorg);
                break;

            case "btnCancel":
                btnaddholder.Visible = true;
                Panel_Add.Visible = false;
                break;

            case "btnAdd":
                Insert_Status();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "CmdDel":
                int CostIDX = int.Parse(cmdArg);
                ViewState["CostIDX_delete"] = CostIDX;
                Delete_Master_List();
                SelectMasterList();
                break;
        }



    }
    #endregion

}
