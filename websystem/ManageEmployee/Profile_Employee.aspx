﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="Profile_Employee.aspx.cs" Inherits="websystem_ManageEmployee_Profile_Employee" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="check_XML" runat="server"></asp:Literal>
    <div class="mainbody container-fluid">
        <div class="row">
            <div class="navbar-wrapper">
                <div class="container-fluid">
                    <div class="navbar navbar-light navbar-static-top" style="background-color: #1199c2;" role="navigation">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span><span
                                        class="icon-bar"></span><span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand">
                                    <small>
                                        <font color="#daebe8"><i class="glyphicon glyphicon-user"></i>&nbsp; Employee Profile &nbsp;(ข้อมูลส่วนตัว)</font>
                                    </small>
                                    <%-- #87b308--%>
                                </a>
                            </div>
                            <div class="navbar-collapse collapse">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--  <div style="padding-top: 20px;"></div>--%>

            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <asp:GridView ID="gvpicture" runat="server"  HeaderStyle-BorderStyle="None" GridLines="None"
                            BorderStyle="None" CssClass="table" BorderColor="White"
                            ShowHeader="false" border-color="#FFFFFF"
                            AutoGenerateColumns="false" Visible="true"
                            OnRowDataBound="Master_RowDataBound">
                            <Columns>
                                <asp:TemplateField ControlStyle-BorderColor="White">
                                    <ItemTemplate>
                                        <asp:Repeater ID="Repeater_pictureProfile" runat="server">
                                            <ItemTemplate>
                                                <div class="row">
                                                    <center><asp:Image ID="ImagProfile" runat="server" 
                                                         ImageUrl='<%# retrunpatProfile((string)Eval("emp_code"))%>'
                                            Style="width: 200px; height: 200px" class="thumbnail img-responsive">
                                             </asp:Image></center>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <asp:TextBox ID="txtemp_code"
                                            Text='<%# Eval("emp_code") %>' runat="server"
                                            CssClass="form-control left" Visible="false">
                                        </asp:TextBox>
                                        <asp:Panel ID="ImagProfile_default" runat="server" Visible="false">
                                            <img class="media-object img-thumbnail"
                                                src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/user-default.png")%>'
                                                width="200px" height="200px" style="margin-right: 8px; margin-top: -5px;">
                                        </asp:Panel>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                        <asp:Panel ID="showalert" runat="server" Visible="true">
                            <asp:Repeater ID="repeater_contact" runat="server">
                                <ItemTemplate>
                                    <div class="media">
                                        <div align="center">
                                            <asp:Label ID="Lb_emp_code" runat="server"></asp:Label>
                                        </div>
                                        <div class="media-body">
                                            <hr>
                                            <h4><strong>ที่อยู่ติดต่อปัจจุบัน</strong></h4>
                                            <span><i class="glyphicon glyphicon-home"></i>&nbsp;<%# Eval("emp_address") %>ต.<%# Eval("dist_name") %>
                                        อ.<%# Eval("amp_name") %>จ.<%# Eval("prov_name") %><%# Eval("post_code") %> ประเทศ <%# Eval("country_name") %>
                                            </span>
                                            <hr>
                                            <h4><strong>เบอร์โทรศัพท์</strong></h4>
                                            <span><i class="glyphicon glyphicon-phone"></i>&nbsp;<%# Eval("emp_mobile_no") %></span>
                                            <hr>
                                            <h4><strong>E-mail</strong></h4>
                                            <span><i class="glyphicon glyphicon-envelope"></i>&nbsp;<%# Eval("emp_email") %></span>
                                            <hr>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </asp:Panel>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <asp:Panel ID="Panel_social_security" runat="server" Visible="true">
                            <h4><strong>ประกันสังคม</strong></h4>
                            <asp:Panel ID="showalert_social_security" runat="server" Visible="false">
                                <div class="form-group">
                                    <div class="alert alert-warning" role="alert">
                                        <strong></strong><small>ยังไม่มีข้อมูลประกันสังคมของท่าน</small>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Repeater ID="value_social_security" runat="server">
                                <ItemTemplate>
                                    <asp:TextBox ID="lable_socIDX" runat="server" Visible="false" Text='<%# Eval("SocHosIDX") %>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:Repeater ID="repeater_social_security" runat="server">
                                <ItemTemplate>
                                    <div class="media-body">
                                        <hr />
                                        <h5>เลขประกันสังคม</h5>
                                        <span><i class="glyphicon glyphicon-list-alt"></i>&nbsp;<%# Eval("SocNo") %>
                                        </span>
                                        <h5>โรงพยาบาล</h5>
                                        <span><i class="glyphicon glyphicon-list-alt"></i>&nbsp;<%# Eval("SocHosName") %>
                                        </span>
                                        <h5>วันหมดอายุบัตร</h5>
                                        <span><i class="glyphicon glyphicon-calendar"></i>&nbsp;<%# Eval("SocDateExpired") %>
                                        </span>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </asp:Panel>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <span>
                            <h4 class="panel-title pull-left"><strong>ข้อมูลส่วนตัวพนักงาน</strong></h4>
                            <div class="dropdown pull-right">
                                <asp:LinkButton ID="btnviewdata" runat="server"
                                    CssClass="btn btn-default btn-sm" data-toggle="tooltip"
                                    title="การแจ้งเตือน" CommandName="viewdataEng" OnCommand="btnCommand">
                                    <i class="glyphicon glyphicon-list-alt"></i> การแจ้งเตือน
                                </asp:LinkButton>&nbsp;
                                <asp:LinkButton ID="btn_change_password" runat="server"
                                    CssClass="btn btn-warning btn-sm" data-toggle="tooltip"
                                    title="เปลี่ยนรหัสผ่าน" CommandName="change_password" OnCommand="btnCommand">
                                    <i class="glyphicon glyphicon-cog"></i> เปลี่ยนรหัสผ่าน
                                </asp:LinkButton>
                            </div>
                        </span>
                        <br>
                        <hr>
                        <span class="pull-left">
                            <asp:Panel ID="personal" runat="server" Visible="true">
                                <asp:Repeater ID="repeater_personal" runat="server">
                                    <ItemTemplate>
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <font color="#1A5276"><small class="list-group-item-heading text_left"><b>ชื่อไทย:</b></small></font>
                                            </div>
                                            <div class="col-sm-9">
                                                <span><%# Eval("prefix_th") %>&nbsp;<%# Eval("emp_firstname_th") %>&nbsp;<%# Eval("emp_lastname_th") %></span>
                                            </div>
                                            <div class="col-sm-2">
                                                <font color="#1A5276"><small class="list-group-item-heading text_left"><b>ชื่ออังกฤษ:</b></small></font>
                                            </div>
                                            <div class="col-sm-9">
                                                <span><%# Eval("prefix_en") %>&nbsp;<%# Eval("emp_firstname_en") %>&nbsp;<%# Eval("emp_lastname_en") %></span>
                                            </div>
                                            <div class="col-sm-2">
                                                <font color="#1A5276"><small class="list-group-item-heading text_left"><b>ชื่อเล่นไทย</b></small></font>
                                            </div>
                                            <div class="col-sm-9">
                                                <span><%# Eval("emp_nickname_th") %>&nbsp;</span>
                                            </div>
                                            <div class="col-sm-2">
                                                <font color="#1A5276"><small class="list-group-item-heading text_left"><b>ชื่อเล่น:</b></small></font>
                                            </div>
                                            <div class="col-sm-9">
                                                <span><%# Eval("emp_nickname_en") %>&nbsp;</span>
                                            </div>
                                            <div class="col-sm-2">
                                                <font color="#1A5276"><small class="list-group-item-heading text_left"><b>เพศ:</b></small></font>
                                            </div>
                                            <div class="col-sm-9">
                                                <span><%# Eval("sex_name_th") %>&nbsp;</span>
                                            </div>
                                            <div class="col-sm-2">
                                                <font color="#1A5276"><small class="list-group-item-heading text_left"><b>วันเกิด:</b></small></font>
                                            </div>
                                            <div class="col-sm-9">
                                                <span><%# Eval("emp_birthday") %>&nbsp;</span>
                                            </div>
                                            <div class="col-sm-2">
                                                <font color="#1A5276"><small class="list-group-item-heading text_left"><b>ศาสนา:</b></small></font>
                                            </div>
                                            <div class="col-sm-9">
                                                <span><%# Eval("rel_name") %>&nbsp;</span>
                                            </div>
                                            <div class="col-sm-2">
                                                <font color="#1A5276"><small class="list-group-item-heading text_left"><b>สัญชาติ:</b></small></font>
                                            </div>
                                            <div class="col-sm-9">
                                                <span><%# Eval("nat_name") %>&nbsp;</span>
                                            </div>
                                            <div class="col-sm-2">
                                                <font color="#1A5276"><small class="list-group-item-heading text_left"><b>เชื้อชาติ:</b></small></font>
                                            </div>
                                            <div class="col-sm-9">
                                                <span><%# Eval("race_name") %>&nbsp;</span>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                        </span>
                    </div>
                </div>
                <%--      <hr>--%>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <span>
                            <h4 class="panel-title pull-left"><strong>พนักงานสังกัด</strong></h4>
                        </span>
                        <br>
                        <br>
                        <%--    <hr>--%>
                        <span class="pull-left">
                            <asp:Repeater ID="repeater_employee" runat="server">
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_left"><b>บริษัท:</b></small></font>
                                        </div>
                                        <div class="col-sm-10">
                                            <span><%# Eval("org_name_th") %>&nbsp;</span>
                                        </div>
                                        <%--    </div>
                                    <div class="row">--%>
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_left"><b>ฝ่าย:</b></small></font>
                                        </div>
                                        <div class="col-sm-10">
                                            <span><%# Eval("dept_name_th") %>&nbsp;</span>
                                        </div>
                                        <%--   </div>
                                    <div class="row">--%>
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_left"><b>แผนก:</b></small></font>
                                        </div>
                                        <div class="col-sm-10">
                                            <span><%# Eval("sec_name_th") %>&nbsp;</span>
                                        </div>
                                        <%--   </div>
                                    <div class="row">--%>
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_left"><b>ตำแหน่ง:</b></small></font>
                                        </div>
                                        <div class="col-sm-10">
                                            <span><%# Eval("pos_name_th") %>&nbsp;</span>
                                        </div>
                                        <%--  </div>
                                    <div class="row">--%>
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_left"><b>รหัสพนักงาน:</b></small></font>
                                        </div>
                                        <div class="col-sm-10">
                                            <span><%# Eval("emp_code") %>&nbsp;</span>
                                        </div>
                                        <%-- </div>
                                    <div class="row">--%>
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_left"><b>Jobgrade:</b></small></font>
                                        </div>
                                        <div class="col-sm-10">
                                            <span><%# Eval("jobgrade_level") %>&nbsp;</span>
                                        </div>
                                        <%--  </div>
                                    <div class="row">--%>
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_left"><b>ประเภทพนักงาน:</b></small></font>
                                        </div>
                                        <div class="col-sm-10">
                                            <span><%# Eval("emp_type_name") %>&nbsp;</span>
                                        </div>
                                        <%--   </div>
                                    <div class="row">--%>
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_left"><b>ข้อมูลเงินเดือน:</b></small></font>
                                        </div>
                                        <div class="col-sm-10">
                                            <span>xx,xxx</span>
                                            <asp:LinkButton runat="server" ID="btndownload"
                                                OnCommand="btnCommand" CommandName="view_sarary"
                                                data-original-title="ดูข้อมูลเงินเดือนของท่าน">ดูข้อมูลเงินเดือนของท่าน
                                            </asp:LinkButton><%--<a href="" data-toggle="modal" data-target="#login-modal">ดูข้อมูลเงินเดือนของท่าน</a>--%>
                                        </div>
                                    </div>
                                    <%--  data-toggle="modal" data-target="#login-modal"--%>
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:Repeater ID="repeater_sarary" runat="server">
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <font color="#1A5276"><small class="list-group-item-heading text_left"><b>เงินเดือนของท่านคือ</b></small></font>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="alert alert-success" role="alert">
                                                <span><i class="glyphicon glyphicon-usd"></i>&nbsp;<%# Eval("sarary") %></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <asp:LinkButton runat="server" ID="btnhiddenView"
                                                OnCommand="btnCommand" ForeColor="Red" CommandName="hiddenViewSarary"
                                                data-original-title="ปิดการดู"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;ปิดการดู
                                            </asp:LinkButton>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                            <asp:Panel ID="repeater_sarary_empty" runat="server" Visible="false">

                                <div class="row">
                                    <div class="col-sm-12">
                                        <font color="#1A5276"><small class="list-group-item-heading text_left"><b>เงินเดือนของท่านคือ</b></small></font>
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-sm-12">
                                         <div class="alert alert-danger" role="alert">
                                            <strong>ไม่มีข้อมูลเงินเดือนของท่าน</strong>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <asp:LinkButton runat="server" ID="btnhiddenView1"
                                            OnCommand="btnCommand" ForeColor="Red" CommandName="hiddenViewSarary"
                                            data-original-title="ปิดการดู"><i class="glyphicon glyphicon-remove-circle"></i>&nbsp;ปิดการดู
                                        </asp:LinkButton>
                                    </div>
                                </div>
                               <%-- <div class="panel-body">
                                    <div class="col-sm-2">
                                        <font color="#1A5276"><small class="list-group-item-heading text_left"><b>เงินเดือนของท่านคือ</b></small></font>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="alert alert-danger" role="alert">
                                            <strong>ไม่มีข้อมูลเงินเดือนของท่าน</strong>
                                        </div>
                                    </div>
                                </div>--%>
                            </asp:Panel>
                        </span>
                    </div>
                </div>
                <asp:Panel ID="panel_login" runat="server" Visible="false">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="col-lg-5">
                                <fieldset>
                                    <span>
                                        <h4 class="panel-title pull-left"><strong>เข้าสู่ระบบเพื่อดูเงินเดือน</strong></h4>
                                    </span>
                                    <br />
                                    <br />
                                    <div class="form-group">
                                        <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control"
                                            placeholder="รหัสพนักงาน" MaxLength="8" ValidationGroup="formLogin"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvEmpCode" ValidationGroup="formLogin"
                                            runat="server" Display="None" SetFocusOnError="true" ControlToValidate="tbEmpCode"
                                            ErrorMessage="กรุณากรอกรหัสพนักงาน" />
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceEmpCode"
                                            TargetControlID="rfvEmpCode" HighlightCssClass="validatorCalloutHighlight" />
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="tbEmpPass" runat="server" CssClass="form-control"
                                            placeholder="รหัสผ่าน" MaxLength="20" TextMode="Password"
                                            ValidationGroup="formLogin"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvEmpPass" ValidationGroup="formLogin"
                                            runat="server" Display="None" SetFocusOnError="true" ControlToValidate="tbEmpPass"
                                            ErrorMessage="กรุณากรอกรหัสผ่าน" />
                                        <ajaxToolkit:ValidatorCalloutExtender runat="Server" ID="vceEmpPass"
                                            TargetControlID="rfvEmpPass" HighlightCssClass="validatorCalloutHighlight" />
                                    </div>
                                    <asp:Button ID="btnLogin" CssClass="btn btn-primary btn-block" runat="server"
                                        data-original-title="เข้าสู่ระบบ" data-toggle="tooltip" OnCommand="btnCommand"
                                        CommandName="cmdLogin" Text="เข้าสู่ระบบ" ValidationGroup="formLogin" />
                                </fieldset>
                                <br />
                                <div id="divShowError" runat="server" class="alert alert-danger" role="alert">
                                    <strong>Error
                                        <asp:Literal ID="litErrorCode" runat="server"></asp:Literal>
                                        : </strong>
                                    <small>รหัสพนักงาน หรือ รหัสผ่าน ไม่ถูกต้อง</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

                <div class="panel panel-default">
                    <div class="panel-body">
                        <span>
                            <h4 class="panel-title pull-left"><strong>ผู้อนุมัติใบลา</strong></h4>
                        </span>
                        <br />
                        <br />
                        <asp:Repeater ID="repeater_approve" runat="server">
                            <ItemTemplate>
                                <div class="post-content">
                                    <div class="panel panel-default">
                                        <div class="panel-body">

                                            <div class="pull-left">
                                                <a href="#">
                                                    <img class="media-object img-thumbnail"
                                                        src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/user-default.png")%>'
                                                        width="50px" height="50px" style="margin-right: 8px; margin-top: -5px;">
                                                </a>
                                            </div>
                                            <div class="post-content">
                                                &nbsp;&nbsp;&nbsp;    
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_left"><b>ผู้อนุมัติคนที่ 1:</b></small></font>
                                        </div>
                                                <div class="col-sm-10">
                                                    <span><%# Eval("emp_approve1") %>&nbsp;</span>
                                                    <%--<asp:Label ID="Label_emp_approve1" runat="server">ffffffffffffff</asp:Label>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="pull-left">
                                                <a href="#">
                                                    <img class="media-object img-thumbnail"
                                                        src='<%=ResolveUrl("~/uploadfiles/profile_picture/default/user-default.png")%>'
                                                        width="50px" height="50px" style="margin-right: 8px; margin-top: -5px;">
                                                </a><%--class="thumbnail img-responsive">--%>
                                            </div>
                                            <div class="post-content">
                                                &nbsp;&nbsp;&nbsp;    
                                        <div class="col-sm-2">
                                            <font color="#1A5276"><small class="list-group-item-heading text_left"><b>ผู้อนุมัติคนที่ 2:</b></small></font>
                                        </div>
                                                <div class="col-sm-10">
                                                    <span><%# Eval("emp_approve2") %>&nbsp;</span>
                                                    <%-- <span><%# Eval("prefix_th") %>&nbsp;<%# Eval("emp_firstname_th") %>&nbsp;<%# Eval("emp_lastname_th") %></span>--%>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>

