﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="cims_register_tools.aspx.cs" Inherits="websystem_qa_cims_register_tools" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <!--tab menu-->

    <asp:TextBox ID="tbPlaceIDXInDocument" Visible="false" runat="server"></asp:TextBox>
    <asp:TextBox ID="tbPlaceNameInDocument" Visible="false" runat="server"></asp:TextBox>

    <asp:TextBox ID="tbLabIDXInDocument" Visible="false" runat="server"></asp:TextBox>
    <asp:TextBox ID="tbLabNameInDocument" Visible="false" runat="server"></asp:TextBox>


    <asp:Label ID="lblDateTime" Visible="false" runat="server"></asp:Label>
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>
    <asp:TextBox ID="tbValuePlacecims" Visible="false" runat="server" CssClass="form-control"></asp:TextBox>
    <asp:TextBox ID="tb_PlacecimsName" Visible="false" runat="server" CssClass="form-control"></asp:TextBox>
    <asp:Literal ID="show_test" Visible="false" runat="server"></asp:Literal>

    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbCreate" runat="server" Visible="false" CommandName="cmdCreate" OnCommand="navCommand" CommandArgument="docCreate"> แจ้งขอบริการสอบเทียบ</asp:LinkButton>
                        </li>

                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbDetail" runat="server" Visible="false" CommandName="cmdDetailList" OnCommand="navCommand" CommandArgument="docDetailList"> รายการสอบเทียบ</asp:LinkButton>
                        </li>


                        <li id="li2" runat="server" class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ทะเบียนเครื่องมือ <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <% if (ViewState["rdept_permission"].ToString() == "27")
                                    { %>
                                <li>

                                    <asp:LinkButton ID="btnaddregis" runat="server" OnCommand="btnCommand" CommandName="cmdAddRegis" CommandArgument="1" Text="เพิ่มทะเบียนเครื่องมือ">
                                    </asp:LinkButton>
                                </li>

                                <li role="separator" class="divider"></li>
                                <% } %>
                                <li>
                                    <asp:LinkButton ID="_divMenuBtnToDivIndexPermTemp" runat="server" CommandName="cmdRegistration" OnCommand="navCommand" Text="ทะเบียนเครื่องมือ" CommandArgument="docRegistration" />
                                </li>
                            </ul>
                        </li>

                        <li id="li3" runat="server">
                            <asp:LinkButton ID="lbListDocument" runat="server" CommandName="cmdListDocument" OnCommand="navCommand" CommandArgument="docList"> รายการทั่วไป</asp:LinkButton>
                        </li>

                        <% if (ViewState["rdept_permission"].ToString() == "27" || Session["emp_idx"].ToString().ToString() == "1413")
                            { %>
                        <li id="li4" runat="server" class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">รายงาน <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <asp:LinkButton ID="btnreport_devices_online" runat="server" OnCommand="btnCommand" CommandName="cmdReportDevices" CommandArgument="1" Text="รายงานเครื่องมือที่ใช้งาน">
                                    </asp:LinkButton>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <asp:LinkButton ID="btnreport_devices_offline" runat="server" OnCommand="btnCommand" CommandName="cmdReportDevices" CommandArgument="2" Text="รายงานเครื่องมือที่ไม่ถูกใช้งาน">
                                    </asp:LinkButton>
                                </li>
                                <li role="separator" class="divider"></li>
                                <li>
                                    <asp:LinkButton ID="btnreport_devices_cutoff" runat="server" OnCommand="btnCommand" CommandName="cmdReportDevices" CommandArgument="3" Text="รายงานเครื่องมือที่ตัดเสีย">
                                    </asp:LinkButton>
                                </li>
                            </ul>
                            <%-- <asp:LinkButton ID="LinkButton3" runat="server" CommandName="cmdListDocument" OnCommand="navCommand" CommandArgument="docReport"> รายงาน</asp:LinkButton>--%>
                        </li>
                        <% } %>

                        <% if (ViewState["rpos_permission"].ToString() == "114" || Session["emp_idx"].ToString().ToString() == "1413")
                            { %>
                        <li id="li5" runat="server">
                            <asp:LinkButton ID="btnDocmanage" runat="server" CommandName="cmdListDocument" OnCommand="navCommand" CommandArgument="docManagement"> จัดการสิทธิ์</asp:LinkButton>
                        </li>
                        <% } %>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    <!--tab menu-->
    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        <asp:Literal ID="Testing" runat="server"></asp:Literal>
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">

        <asp:View ID="docCreate" runat="server">
            <div id="divCreate" runat="server" class="col-md-12">
                <!--รายละเอียดผู้ใช้งาน-->
                <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดข้อมูลผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                        <%-- <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>--%>
                                        <label class="col-sm-2 control-label">ฝ่าย :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </ItemTemplate>

                </asp:FormView>

                <asp:FormView ID="fvDetailsDocument" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดรายการ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">ผู้ทำรายการ :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbDetailsEmpCreate" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbDetailsDeptCreate" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbDetailsSecCreate" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>

                                        <label class="col-sm-2 control-label">ตำแหน่ง :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbDetailsPosCreate" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนกผู้ถือครอง :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbDetailsSectionHolder" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ประเภท :</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbDetailsEquipmentType" runat="server" CssClass="form-control" Text='<%# Eval("equipment_type_name") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbtypeEquipment" runat="server" CssClass="form-control" Text='<%# Eval("equipment_type") %>' Enabled="false" />
                                            <asp:TextBox ID="tbplaceidx" runat="server" CssClass="form-control" Text='<%# Eval("place_idx") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocCancelDoc" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdDocCancel"><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </ItemTemplate>

                </asp:FormView>

                <!--รายการเครื่องมือสอบเทียบ"  -->
                <asp:FormView ID="fvCalDocumentList" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายการเครื่องมือที่สอบเทียบ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <asp:GridView ID="gvCalibrationList"
                                        runat="server"
                                        AutoGenerateColumns="false" HeaderStyle-CssClass="info"
                                        OnRowDeleting="gvRowDeleted"
                                        OnRowDataBound="gvRowDataBound"
                                        RowStyle-Wrap="true"
                                        CssClass="table table-striped table-bordered table-responsive">
                                        <HeaderStyle CssClass="info" Height="30px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="No." HeaderStyle-CssClass="text-left" HeaderStyle-Width="1%" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblU1DocCal" Visible="false" runat="server" Text='<%# Eval("u1_cal_idx")%>'></asp:Label>
                                                    <asp:Label ID="lblm0NodeIDX" Visible="true" runat="server" Text='<%# Eval("m0_node_idx")%>'></asp:Label>
                                                    <%# (Container.DataItemIndex +1) %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Machine Name" HeaderStyle-Width="9%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblequipment_name" runat="server" Text='<%# Eval("equipment_name")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ID No." HeaderStyle-Width="5%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbldevice_id_no" runat="server" Text='<%# Eval("device_id_no")%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Serial No." HeaderStyle-Width="10%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbldevice_serial" runat="server" Text='<%# Eval("device_serial")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Last Cal Date" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblLastCalDate" runat="server" Text='<%# Eval("received_calibration_date")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Received Date" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblreceived_calibration_date" runat="server" Text='<%# Eval("received_calibration_date")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Cal Date" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Lab" HeaderStyle-Width="12%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>

                                                    <asp:Panel ID="pnlChooseLocationCal" runat="server" Visible="true" Width="100%">
                                                        <div class="panel-body">
                                                            <div class="form-horizontal" role="form">
                                                                <div class="form-group">
                                                                    <div class="col-sm-12">
                                                                        <asp:DropDownList ID="ddlChooseLabType" runat="server" CssClass="form-control"
                                                                            OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                                                        <asp:RequiredFieldValidator ID="RequiredddlChooseLabType" runat="server" InitialValue="0"
                                                                            ControlToValidate="ddlChooseLabType" Display="None" SetFocusOnError="true"
                                                                            ErrorMessage="*กรุณาเลือกประเภทการสอบเทียบ" ValidationGroup="SelectTypegroup" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorddlChooseLabType" runat="Server"
                                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlChooseLabType" Width="250" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">

                                                                    <div class="col-sm-12">
                                                                        <asp:DropDownList ID="ddlChooseLocation" Visible="true" Enabled="true" CssClass="form-control" runat="server" />
                                                                        <asp:RequiredFieldValidator ID="RequiredddlChooseLocation" runat="server" InitialValue="0"
                                                                            ControlToValidate="ddlChooseLocation" Display="None" SetFocusOnError="true"
                                                                            ErrorMessage="*กรุณาเลือกประเภทแลปสอบเทียบ" ValidationGroup="SelectTypegroup" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorddlChooseLocation" runat="Server"
                                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlChooseLocation" Width="250" />
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">

                                                                    <div class="col-sm-12">
                                                                        <asp:TextBox ID="txtCommentBeforeExtanalLab" Visible="false" CssClass="col-sm-12 form-control"
                                                                            placeholder="กรอกความคิดเห็น ..." runat="server" />
                                                                        <asp:TextBox ID="tet" Visible="false" CssClass="col-sm-12 form-control"
                                                                            placeholder="กรอกความคิดเห็น ..." runat="server" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </asp:Panel>


                                                    <asp:Label ID="lblM0Lab" runat="server" Visible="false" Text='<%# Eval("m0_lab_idx")%>'></asp:Label>
                                                    <asp:Label ID="lblM0LabName" runat="server" Text='<%# Eval("lab_name")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="certificate" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcertificate" runat="server" Text='<%# Eval("certificate")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstaidx" runat="server" Text='<%# Eval("staidx")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnSaveCertificate" Visible="false" CssClass="btn-sm btn-success" runat="server" data-original-title="Record Certificate" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdView"
                                                        CommandArgument='<%# Eval("u0_cal_idx") + "," + "2" + "," + Eval("equipment_type") + "," + Eval("certificate") + "," +  Eval("u1_cal_idx") %>'><i class="fa fa-pencil" aria-hidden="true"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnViewDocCal" Visible="false" CssClass="btn-sm btn-info" runat="server" data-original-title="View" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdView"
                                                        CommandArgument='<%# Eval("u0_cal_idx") + "," + "2" + "," + Eval("equipment_type") + "," + Eval("certificate") + "," +  Eval("u1_cal_idx") %>'><i class="fa fa-file-text-o"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Panel ID="pnlAction" runat="server" CssClass="pull-right" Visible="true">
                                        <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" runat="server" data-original-title="Save" CommandArgument='<%# Eval("m0_node_idx")%>' data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" ValidationGroup="SelectTypegroup" CommandName="cmdDocSave"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdDocCancel"></asp:LinkButton>
                                    </asp:Panel>

                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>

            </div>

            <!--รายการเครื่องมือ OnTextChanged="txtchange"  -->
            <div id="divSelectTypeEquipment" runat="server" class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">รายการเครื่องมือ</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <div class="col-sm-2 text_right small">
                                    <b>Place</b>
                                    <p class="list-group-item-text">เลือกสถานที่</p>
                                </div>
                                <div class="col-sm-4">
                                    <asp:DropDownList ID="ddlPlaceCreate" CssClass="form-control" runat="server"></asp:DropDownList>
                                </div>
                                <div class="col-sm-6"></div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <asp:RadioButtonList ID="rblTypeEquipment" CellPadding="3" Font-Size="10" CellSpacing="2" Width="50%" AutoPostBack="true" RepeatColumns="2" RepeatLayout="Table"
                                        RepeatDirection="Vertical" OnSelectedIndexChanged="radioCheckChange" runat="server">
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <asp:Panel ID="pnEquipmentOld" runat="server" Visible="true">
                                <div class="form-group">
                                    <div class="col-sm-2 text_right small">
                                        <b>Search ID No.</b>
                                        <p class="list-group-item-text">ค้นหาจากรหัสเครื่องมือ</p>
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="tbsearchingToolID" runat="server" OnTextChanged="TextBoxChanged" placeholder="รหัสเครื่อง.. (ID No.)" Visible="true"
                                            CssClass="form-control" AutoPostBack="true" />
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="pull-left">
                                            <asp:LinkButton ID="btnSearchToolID" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchIndex"
                                                OnCommand="btnCommand" CommandName="cmdSearchTool" Font-Size="Small" data-toggle="tooltip" title="ค้นหา"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                            </asp:Panel>

                            <asp:Panel ID="pnEquipmentNew" runat="server" Visible="false">
                                <hr />
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:RadioButtonList ID="rd_certificate1" CellPadding="3" CellSpacing="2" Width="54%" AutoPostBack="true" RepeatColumns="2" Font-Size="10" RepeatLayout="Table"
                                                RepeatDirection="Vertical" OnSelectedIndexChanged="radioCheckChange" runat="server">
                                                <asp:ListItem Value="4" Selected="True">มีใบรับรอง</asp:ListItem>
                                                <asp:ListItem Value="3">ไม่มีใบรับรอง</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                        <%--  <div class="col-sm-6"></div>--%>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Name</b>
                                            <p class="list-group-item-text">ชื่อเครื่องมือ/อุปกรณ์</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtinput_machine_name" runat="server" placeholder="กรอกชื่อเครื่องมือ.." Visible="false" CssClass="form-control" />
                                            <asp:DropDownList ID="ddl_machine_name" runat="server" CssClass="form-control"></asp:DropDownList>
                                            <asp:CheckBox ID="chk_other_name_equipment" runat="server" Font-Bold="true" CssClass="small" Text="อื่นๆ (Other)" AutoPostBack="true" OnCheckedChanged="CheckboxChanged" RepeatDirection="Vertical" />
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Brand</b>
                                            <p class="list-group-item-text">ยี่ห้อ/รุ่น</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtinput_machine_brand" runat="server" placeholder="กรอกชื่อยี่ห้อ.." Visible="false" CssClass="form-control" />
                                            <asp:DropDownList ID="ddl_machine_brand" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>ID No.</b>
                                            <p class="list-group-item-text">รหัสเครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbIDNo" runat="server" placeholder="กรอกรหัสเครื่อง.." CssClass="form-control" />
                                        </div>

                                        <div class="col-sm-2 text_right small">
                                            <b>Serial No.</b>
                                            <p class="list-group-item-text">หมายเลขเครื่อง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbserailnumber" runat="server" placeholder="กรอกหมายเลขเครื่อง.." CssClass="form-control" />
                                        </div>

                                    </div>
                                    <div class="form-group">

                                        <div class="col-sm-2 text_right small">
                                            <b>Calibration Type</b>
                                            <p class="list-group-item-text">ประเภทการสอบเทียบ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlCalibrationType" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Type</b>
                                            <p class="list-group-item-text">ประเภทเครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlmachineType" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Range of use</b>
                                            <p class="list-group-item-text">ช่วงการใช้งาน</p>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="tbRangeOfUseMin" runat="server" placeholder="เริ่มต้น.." CssClass="form-control" />
                                        </div>

                                        <div class="col-sm-1">
                                            <asp:TextBox ID="tbRangeOfUseMax" runat="server" placeholder="สิ้นสุด.." CssClass="form-control" />
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddl_range_of_use_unit" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Detail</b>
                                            <p class="list-group-item-text">รายละเอียดเครื่อง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbdetails_equipment" runat="server" TextMode="MultiLine" Rows="2" placeholder="กรอกรายละเอียดเครื่อง.." CssClass="form-control multiline-no-resize" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Measuring Range</b>
                                            <p class="list-group-item-text">ช่วงการวัด/พิสัย</p>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="tbMeasuringRangeMin" runat="server" placeholder="เริ่มต้น.." CssClass="form-control" />
                                        </div>

                                        <div class="col-sm-1">
                                            <asp:TextBox ID="tbMeasuringRangeMax" runat="server" placeholder="สิ้นสุด.." CssClass="form-control" />
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddl_measuring_range_unit" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Resolution</b>
                                            <p class="list-group-item-text">ค่าอ่านละเอียด</p>
                                        </div>
                                        <div class="col-sm-2">
                                            <div style="text-align: center;">
                                                <asp:TextBox ID="tbResolution" runat="server" placeholder="กรอกค่าละเอียด.." CssClass="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddl_resolution_unit" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Acceptance Criteria</b>
                                            <p class="list-group-item-text">เกณฑ์การยอมรับ</p>
                                        </div>
                                        <div class="col-sm-1">
                                            <div style="text-align: center;">
                                                <asp:TextBox ID="tbAcceptanceCriteriaMin" runat="server" placeholder="เริ่มต้น.." Enabled="false" Text="+-" CssClass="form-control text-center" />
                                            </div>
                                        </div>

                                        <div class="col-sm-1">
                                            <asp:TextBox ID="tbAcceptanceCriteria" runat="server" placeholder=".." CssClass="form-control" />
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddl_acceptance_criteria_unit" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>

                                        <div class="col-sm-2 text_right small">
                                            <b>Calibration Frequency</b>
                                            <p class="list-group-item-text">ความถี่ในการสอบเทียบ</p>
                                        </div>
                                        <div class="col-sm-2">
                                            <div style="text-align: center;">
                                                <asp:TextBox ID="tbCalibrationFrequency" runat="server" placeholder="กรอกความถี่ในการสอบเทียบ.." CssClass="form-control" />
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="tbCalibrationFrequencyUnit" runat="server" Enabled="false" placeholder="กรอกความถี่.." Text="เดือน/ครั้ง" CssClass="form-control text-center" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Calibration Point</b>
                                            <p class="list-group-item-text">จุดสอบเทียบ</p>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="tbcalibration_point" runat="server" placeholder="กรอกค่าจุดสอบเทียบ.." Enabled="true" CssClass="form-control" />
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddl_calibration_point_unit" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:LinkButton ID="btnAddCalibrationPoint" CssClass="btn btn-primary" Font-Size="Small" runat="server" CommandName="cmdAddCalibrationPoint" data-original-title="เพิ่มค่าจุดสอบเทียบ" data-toggle="tooltip" OnCommand="btnCommand" Text="เพิ่ม" ValidationGroup="Saveinsert"></asp:LinkButton>
                                        </div>
                                        <div class="col-sm-4"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>cal. point</b>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:GridView ID="gvCalibrationPoint"
                                                runat="server" RowStyle-Font-Size="9" RowStyle-Height="2" HeaderStyle-Font-Size="Small"
                                                AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet" HeaderStyle-CssClass="info"
                                                OnRowDeleting="gvRowDeleted"
                                                OnRowDataBound="gvRowDataBound"
                                                RowStyle-Wrap="true"
                                                CssClass="table table-striped table-bordered table-responsive"
                                                HeaderStyle-Height="20px"
                                                ShowHeaderWhenEmpty="True"
                                                ShowFooter="False"
                                                BorderStyle="None"
                                                CellSpacing="2">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ค่าจุดสอบเทียบ" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCalibrationPoint" runat="server" Text='<%# Eval("CalibrationPoint")%>'></asp:Label>
                                                            <asp:Label ID="lblIDNumber" runat="server" Text='<%# Eval("IDNUMBER")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="หน่วย" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCalibrationPointUnitIDX" Visible="false" runat="server" Text='<%# Eval("CalibrationPointUnitIDX")%>'></asp:Label>
                                                            <asp:Label ID="lblCalibrationPointUnit" runat="server" Text='<%# Eval("CalibrationPointUnit")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="XX-Small" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:LinkButton ID="btnDeleteCalibrationPoint" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="DeletePoint" CommandArgument='<%# Eval("IDNUMBER") + "," + "0" + "," + Eval("CalibrationPoint") %>' OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:GridView ID="gvCalPointInsert" Visible="true"
                                                runat="server" RowStyle-Font-Size="9" RowStyle-Height="2" HeaderStyle-Font-Size="Small"
                                                AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet" HeaderStyle-CssClass="info"
                                                OnRowDeleting="gvRowDeleted"
                                                OnRowDataBound="gvRowDataBound"
                                                RowStyle-Wrap="true"
                                                CssClass="table table-striped table-bordered table-responsive"
                                                HeaderStyle-Height="20px"
                                                ShowHeaderWhenEmpty="True"
                                                ShowFooter="False"
                                                BorderStyle="None"
                                                CellSpacing="2">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ค่าจุดสอบเทียบ" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCalibrationPoint1" runat="server" Text='<%# Eval("CalibrationPoint_insert")%>'></asp:Label>
                                                            <asp:Label ID="lblIDNumber1" runat="server" Text='<%# Eval("IDNUMBER_insert")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="หน่วย" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblCalibrationPointUnitIDX1" Visible="false" runat="server" Text='<%# Eval("CalibrationPointUnitIDX_insert")%>'></asp:Label>
                                                            <asp:Label ID="lblCalibrationPointUnit1" runat="server" Text='<%# Eval("CalibrationPointUnit_insert")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="XX-Small" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:LinkButton ID="btnDeleteCalibrationPoint1" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small"></div>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="btnInsertDetail" Visible="false" CssClass="btn btn-primary pull-left" runat="server"
                                                CommandName="cmdInserEquipmentNew" OnCommand="btnCommand" ValidationGroup="Saveinsertdetail"
                                                title="เพิ่มรายการ">เพิ่มรายการ</asp:LinkButton>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:GridView ID="gvListEquipmentCalNew"
                                                runat="server" RowStyle-Font-Size="9" RowStyle-Height="2" HeaderStyle-Font-Size="Small"
                                                AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet" HeaderStyle-CssClass="info"
                                                OnRowDeleting="gvRowDeleted"
                                                OnRowDataBound="gvRowDataBound"
                                                RowStyle-Wrap="true"
                                                CssClass="table table-striped table-bordered table-responsive"
                                                HeaderStyle-Height="25px"
                                                ShowHeaderWhenEmpty="True"
                                                ShowFooter="False"
                                                BorderStyle="None"
                                                CellSpacing="2">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="รหัสเครื่องมือ" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblIDNo" runat="server" Text='<%# Eval("IDCodeNumber_New")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ชื่อเครื่องมือ/อุปกรณ์" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEquipmentNameNew" runat="server" Text='<%# Eval("EquipmentName_New")%>'></asp:Label>
                                                            <asp:Label ID="lblEquipmentNameIDX" runat="server" Visible="false" Text='<%# Eval("M0DeviceIDX_New")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ประเภทเครื่องมือ" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblEquipmentTypeIDXNew" Visible="false" runat="server" Text='<%# Eval("EquipmentTypeIDX_New")%>'></asp:Label>
                                                            <asp:Label ID="lblEquipmentTypeNew" runat="server" Text='<%# Eval("EquipmentType_New")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ยี่ห้อ/รุ่น" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblM0BrandIDXNew" Visible="false" runat="server" Text='<%# Eval("M0BrandIDX_New")%>'></asp:Label>
                                                                <asp:Label ID="lblBrandNameNew" runat="server" Text='<%# Eval("BrandName_New")%>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="หมายเลขเครื่อง" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblSerialNumberNew" runat="server" Text='<%# Eval("SerialNumber_New")%>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ช่วงการใช้งาน" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblRangeOfUseMin" Visible="false" runat="server" Text='<%# Eval("RangeOfUseMin")%>'></asp:Label>
                                                                <asp:Label ID="lblRangeOfUseMax" Visible="false" runat="server" Text='<%# Eval("RangeOfUseMax")%>'></asp:Label>
                                                                <asp:Label ID="lblRangeOfUse" runat="server" Text='<%# Eval("RangeOfUseMin") + "-" + Eval("RangeOfUseMax") + "  " + Eval("RangeOfUseUnitName") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ช่วงการวัด" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblMeasuringRangeMin" Visible="false" runat="server" Text='<%# Eval("Measuring_Range_Min")%>'></asp:Label>
                                                                <asp:Label ID="lblMeasuringRangeMax" Visible="false" runat="server" Text='<%# Eval("Measuring_Range_Max")%>'></asp:Label>
                                                                <asp:Label ID="lblMeasuringRange" runat="server" Text='<%# Eval("Measuring_Range_Min") + "-" + Eval("Measuring_Range_Max") + "  " + Eval("Measuring_Range_UnitName") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ค่าอ่านละเอียด" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblResolution" Visible="false" runat="server" Text='<%# Eval("Resolution")%>'></asp:Label>
                                                                <asp:Label ID="lblResolutionShow" runat="server" Text='<%# Eval("Resolution") + " " + Eval("Resolution_UnitName") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="เกณฑ์การยอมรับ" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblAcceptanceCriteria" Visible="false" runat="server" Text='<%# Eval("Acceptance_Criteria")%>'></asp:Label>
                                                                <asp:Label ID="lblAcceptanceCriteriaName" runat="server" Text='<%# Eval("Acceptance_Criteria") + "  " + Eval("Acceptance_Criteria_UnitName") %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ความถี่ในการสอบเทียบ" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblCalibrationFrequency" Visible="false" runat="server" Text='<%# Eval("Calibration_Frequency")%>'></asp:Label>
                                                                <asp:Label ID="lblCalibrationFrequencyName" runat="server" Text='<%# Eval("Calibration_Frequency") + "  " +  "เดือน/ครั้ง" %>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="จุดสอบเทียบ" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblCalibrationPointList" runat="server" Text='<%# Eval("CalibrationPointList") %>'></asp:Label>
                                                                <asp:Label ID="lblCalibrationPointListIDX" runat="server" Text='<%# Eval("CalibrationPointList_IDX")%>'></asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ใบรับรอง" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:Label ID="lblCerticicate" runat="server" Visible="true" Text='<%# Eval("Certificate_insert") %>'></asp:Label>
                                                                <asp:Label ID="lblHasCerticicate" Visible="true" runat="server">
                                                                <i class="fa fa-check-circle" style="color:darkgreen;" aria-hidden="true"></i>
                                                                </asp:Label>
                                                                <asp:Label ID="lblNotHasCerticicate" Visible="false" runat="server">
                                                                <i class="fa fa-circle" style="color: red; font-size: 10px;" aria-hidden="true"></i>
                                                                </asp:Label>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-left" ControlStyle-Font-Size="XX-Small" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <small>
                                                                <asp:LinkButton ID="btnDeleteEquipmentNew" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="DeletePoint" CommandArgument='<%# Eval("IDCodeNumber_New") + "," + "1" + "," + Eval("M0DeviceIDX_New") %>' OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                            </small>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>

                            <div class="alert alert-warning" id="divAlertSearchEquipmentOld" runat="server" visible="false">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong>ไม่พบรายการเครื่องมือที่ค้นหา ! กรุณาทำการกรอกรหัสเครื่องมือใหม่อีกครั้งค่ะ </strong>
                            </div>
                            <!--รายการเครื่องมือเก่าที่ค้นหา-->
                            <asp:FormView ID="fvShowDetailsSearchTool" runat="server" Width="100%">
                                <ItemTemplate>
                                    <div class="form-horizontal" role="form">
                                        <%--  <div class="form-group">
                                            <div class="col-sm-2 text_right small"></div>
                                            <div class="col-sm-10 text_left">
                                                 <b>รายละเอียดเครื่องมือ</b>
                                            </div>
                                         </div>--%>
                                        <div class="form-group">
                                            <asp:TextBox ID="tbM0DeviceIDX" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("m0_device_idx") %>' Enabled="false"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 text_right small">
                                                <b>ID No.</b>
                                                <p class="list-group-item-text">รหัสเครื่องมือ</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbIDCodeEquipment" runat="server" CssClass="form-control" Text='<%# Eval("device_id_no") %>' Enabled="false"></asp:TextBox>

                                            </div>
                                            <div class="col-sm-2 text_right small">
                                                <b>Machine Type</b>
                                                <p class="list-group-item-text">ประเภทเครื่องมือ</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEquipmentType" runat="server" Text='<%# Eval("equipment_type_name") %>' CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 text_right small">
                                                <b>Machine Name</b>
                                                <p class="list-group-item-text">ชื่อเครื่องมือ/อุปกรณ์</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEquipmentName" runat="server" Text='<%# Eval("equipment_name") %>' CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-2 text_right small">
                                                <b>Brand</b>
                                                <p class="list-group-item-text">ยี่ห้อ/รุ่น</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEquipmentBrandName" runat="server" Text='<%# Eval("brand_name") %>' CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 text_right small">
                                                <b>Serial No.</b>
                                                <p class="list-group-item-text">หมายเลขเครื่อง </p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbSerailNumber" runat="server" CssClass="form-control" Text='<%# Eval("device_serial") %>' Enabled="false"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-2 text_right small">
                                                <b>Section Holder</b>
                                                <p class="list-group-item-text">แผนกผู้ถือครอง</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbSectionHolder" runat="server" CssClass="form-control" Text='<%# Eval("device_rsec") %>' Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 text_right small">
                                                <b>Cal Date</b>
                                                <p class="list-group-item-text">วันที่สอบเทียบล่าสุด </p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbDeviceCalDate" runat="server" CssClass="form-control" Text='<%# Eval("device_cal_date") %>' Enabled="false"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-2 text_right small">
                                                <b>Due date</b>
                                                <p class="list-group-item-text">วันที่สอบเทียบครั้งถัดไป</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbDeviceDueDate" runat="server" CssClass="form-control" Text='<%# Eval("device_due_date") %>' Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-offset-2 col-sm-10">
                                                <asp:LinkButton ID="btnInsertDetail" CssClass="btn btn-default" runat="server"
                                                    CommandName="cmdInsertDetail" OnCommand="btnCommand" ValidationGroup="Saveinsertdetail"
                                                    title="เพิ่มรายการ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:FormView>

                            <asp:GridView ID="gvListEquipmentCalOld"
                                runat="server" RowStyle-Font-Size="11" HeaderStyle-Font-Size="Small"
                                AutoGenerateColumns="false" RowStyle-VerticalAlign="NotSet" HeaderStyle-CssClass="info"
                                OnRowDeleting="gvRowDeleted"
                                OnRowDataBound="gvRowDataBound"
                                RowStyle-Wrap="true"
                                CssClass="table table-striped table-bordered table-responsive"
                                HeaderStyle-Height="25px"
                                ShowHeaderWhenEmpty="True"
                                ShowFooter="False"
                                BorderStyle="None"
                                CellSpacing="2">
                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="รหัสเครื่องมือ" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblIDCodeNumber" runat="server" Text='<%# Eval("IDCodeNumber")%>'></asp:Label>
                                                <asp:Label ID="lblM0DeviceIdx" runat="server" Text='<%# Eval("M0DeviceIDX")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ชื่อเครื่องมือ/อุปกรณ์" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblEquipmentName" runat="server" Text='<%# Eval("EquipmentName")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ประเภทเครื่องมือ" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblEquipmentType" runat="server" Text='<%# Eval("EquipmentType")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ยี่ห้อ/รุ่น" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblBrandname" runat="server" Text='<%# Eval("BrandName")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="หมายเลขเครื่อง" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblSerialNumber" runat="server" Text='<%# Eval("SerialNumber")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ผู้ถือครอง" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblSectionHolder" runat="server" Text='<%# Eval("Holder")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="วันที่สอบเทียบล่าสุด" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblcalDate" runat="server" Text='<%# Eval("calDate")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="วันที่สอบเทียบครั้งถัดไป" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:Label ID="lblDueDate" runat="server" Text='<%# Eval("DueDate")%>'></asp:Label>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <small>
                                                <asp:LinkButton ID="btnDeleteEquipmentOld" runat="server" title="delete" Text="" Font-Size="1px" CssClass="btn btn-danger btn-sm" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                            <div id="divActionSaveCreateDocument" runat="server" visible="false" class="pull-right">
                                <asp:LinkButton ID="btnSaveDocCal" CssClass="btn btn-success" Font-Size="Small" runat="server" CommandName="cmdSaveDocCal" data-original-title="บันทึก" data-toggle="tooltip" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="Saveinsert"></asp:LinkButton>
                                <asp:LinkButton ID="btnCancelDocCal" CssClass="btn btn-danger" Font-Size="Small" runat="server" CommandName="cmdCancelDocCal" data-original-title="ยกเลิก" data-toggle="tooltip" OnCommand="btnCommand" Text="ยกเลิก" ValidationGroup="Saveinsert"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="docDetailList" runat="server">
            <div id="divDocumentCalibration" runat="server" class="col-md-12">

                <asp:GridView ID="gvCalibrationDocument"
                    runat="server"
                    AutoGenerateColumns="false" HeaderStyle-CssClass="info"
                    OnRowDeleting="gvRowDeleted"
                    OnRowDataBound="gvRowDataBound"
                    RowStyle-Wrap="true"
                    CssClass="table table-striped table-bordered table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging"
                    AllowPaging="True"
                    PageSize="10">
                    <HeaderStyle CssClass="info" Height="30px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-left" ControlStyle-Width="1%" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblU0DocCal" Visible="false" runat="server" Text='<%# Eval("u0_cal_idx")%>'></asp:Label>
                                <%# (Container.DataItemIndex +1) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="วันที่สร้างรายการ" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblCreateDateDocument" runat="server" Text='<%# Eval("create_date")%>'></asp:Label>
                                <%-- <asp:Label ID="lblPlaceDocIDX" Visible="false" runat="server" Text='<%# Eval("place_idx")%>'></asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="เครื่องมือประเภท" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblequipmentType" runat="server" Text='<%# Eval("equipment_type_name")%>'></asp:Label>

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สถานะ" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblstaidxDoc" runat="server" Text='<%# Eval("staidx")%>'></asp:Label>
                                <asp:Label ID="lblstatus_name" runat="server" Text='<%# Eval("status_name")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-left" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnViewDocCal" CssClass="btn-sm btn-info" runat="server" CommandName="cmdView" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("u0_cal_idx") + "," + "1" + "," + Eval("equipment_type") + "," + "0" + "," + Eval("u1_cal_idx") %>' data-toggle="tooltip" title="View"><i class="fa fa-file-text-o"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>

        <asp:View ID="docRegistration" runat="server">
            <!--รายการทะเบียนเครื่องมือ (โอนย้าย)"  -->
            <div id="divTranferRegistration" runat="server" class="col-md-12">
                <!--รายละเอียดผู้ใช้งาน หน้าการจัดการทะเบียนเครื่องมือ -->
                <asp:FormView ID="fvDetailUserInPageRegistration" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfEmpOrgIDXTranfer" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDXTranfer" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDXTranfer" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดข้อมูลผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCodeRegistration" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpNameRegistration" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrgRegistration" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDeptRegistration" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSecRegistration" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPosRegistration" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </ItemTemplate>
                </asp:FormView>
            </div>
            <!--ฟอร์มเพิ่มแก้ไขรายการทะเบียนเครื่องมือ"  -->
            <div id="divFVAddRegistration" runat="server" class="col-md-12">
                <asp:FormView ID="fvInsertCIMS" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">เพิ่มทะเบียนเครื่องมือ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Location</b>
                                            <p class="list-group-item-text">สถานที่ของเครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlPlaceMachine" ValidationGroup="saveFormRegis" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="ReddlPlaceMachine" runat="server" InitialValue="0"
                                                ControlToValidate="ddlPlaceMachine" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกสถานที่ของเครื่องมือ" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorddlPlaceMachine" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlPlaceMachine" Width="200" />
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Calibration Type</b>
                                            <p class="list-group-item-text">ประเภทการสอบเทียบ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:RequiredFieldValidator ID="Redllcaltype" runat="server" InitialValue="0"
                                                ControlToValidate="dllcaltype" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกประเภทการสอบเทียบ" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valdllcaltype" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Redllcaltype" Width="200" />
                                            <asp:DropDownList ID="dllcaltype" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Name</b>
                                            <p class="list-group-item-text">ชื่อเครื่องมือ/อุปกรณ์</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:RequiredFieldValidator ID="ReddlEquipmentName" runat="server" InitialValue="0"
                                                ControlToValidate="ddlEquipmentName" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกชื่อเครื่องมือ/อุปกรณ์" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlEquipmentName" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlEquipmentName" Width="200" />
                                            <asp:RequiredFieldValidator ID="Retxtinput_machine_name" runat="server"
                                                ControlToValidate="txtinput_machine_name" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อเครื่องมือ/อุปกรณ์" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valtxtinput_machine_name" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtinput_machine_name" Width="200" />
                                            <asp:TextBox ID="txtinput_machine_name" runat="server" placeholder="กรอกชื่อเครื่องมือ/อุปกรณ์.." Visible="false" CssClass="form-control" />
                                            <asp:DropDownList ID="ddlEquipmentName" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                            <asp:CheckBox ID="chk_other_name" runat="server" Font-Bold="true" CssClass="small" Text="ชื่อเครื่องมืออื่นๆ (Other Machine Name)" AutoPostBack="true" OnCheckedChanged="CheckboxChanged" RepeatDirection="Vertical" />
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Brand</b>
                                            <p class="list-group-item-text">ยี่ห้อ/รุ่น</p>
                                        </div>
                                        <div class="col-sm-4">

                                            <asp:TextBox ID="txtinput_brand" runat="server" placeholder="กรอกยี่ห้อ/รุ่น.." Visible="false" CssClass="form-control" />
                                            <asp:DropDownList ID="ddlBrand" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="ReddlBrand" runat="server" InitialValue="0"
                                                ControlToValidate="ddlBrand" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกยี่ห้อ/รุ่น" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlBrand" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlBrand" Width="180" />
                                            <asp:RequiredFieldValidator ID="Retxtinput_brand" runat="server"
                                                ControlToValidate="txtinput_brand" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกยี่ห้อ/รุ่น" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valtxtinput_brand" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtinput_brand" Width="180" />
                                            <asp:CheckBox ID="chk_other_brand" runat="server" Font-Bold="true" CssClass="small" Text="ยี่ห้อ/รุ่นอื่นๆ (Other Brand)" AutoPostBack="true" OnCheckedChanged="CheckboxChanged" RepeatDirection="Vertical" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine No.</b>
                                            <p class="list-group-item-text">รหัสเครื่อง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtequipmentNo" runat="server" placeholder="กรอกรหัสเครื่อง.." AutoPostBack="true" OnTextChanged="TextBoxChanged" CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RetxtequipmentNo" runat="server"
                                                ControlToValidate="txtequipmentNo" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกรหัสเครื่อง" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtequipmentNo" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtequipmentNo" Width="200" />
                                            <asp:Label ID="lbltxtShowIDnumber" runat="server" Font-Size="9"></asp:Label>
                                        </div>

                                        <div class="col-sm-2 text_right small">
                                            <b>Serial No.</b>
                                            <p class="list-group-item-text">หมายเลขเครื่อง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtserialno" runat="server" placeholder="กรอกหมายเลขเครื่อง.." CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="Retxtserialno" runat="server"
                                                ControlToValidate="txtserialno" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกหมายเลขเครื่อง" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valtxtserialno" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtserialno" Width="200" />
                                            <asp:Label ID="lbltxtShowSerialNumber" runat="server" Font-Size="9"></asp:Label>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Location</b>
                                            <p class="list-group-item-text">ที่ตั้งเครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddl_location" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" Enabled="true">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Requiredddl_location" runat="server" ControlToValidate="ddl_location" Display="None" SetFocusOnError="true" ErrorMessage="*กรุณาเลือกที่ตั้งเครื่องมือ" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorRequiredddl_location" runat="Server" PopupPosition="BottomRight" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddl_location" Width="200" />

                                        </div>

                                        <div class="col-sm-2 text_right small">
                                            <b>Zone</b>
                                            <p class="list-group-item-text">ตำแหน่งที่อยู่เครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddl_Zone" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                                                ControlToValidate="txtserialno" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกหมายเลขเครื่อง" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtserialno" Width="200" />
                                            <asp:Label ID="Label4" runat="server" Font-Size="9"></asp:Label>--%>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Type</b>
                                            <p class="list-group-item-text">ประเภทเครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:RequiredFieldValidator ID="ReddlEquipType" runat="server" InitialValue="0"
                                                ControlToValidate="ddlEquipType" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกประเภทเครื่องมือ" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlEquipType" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlEquipType" Width="190" />
                                            <asp:DropDownList ID="ddlEquipType" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Detail</b>
                                            <p class="list-group-item-text">รายละเอียดเครื่อง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtequipmentdetail" runat="server" TextMode="MultiLine" Rows="3" placeholder="กรอกรายละเอียดเครื่อง.." CssClass="form-control multiline-no-resize" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Organization</b>
                                            <p class="list-group-item-text">องค์กรที่ถือครอง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlOrg_excel" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="saveFormRegis" />
                                            <asp:RequiredFieldValidator ID="RequiredddlOrg_excel" ValidationGroup="saveFormRegis" runat="server" Display="None"
                                                ControlToValidate="ddlOrg_excel" Font-Size="11" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกองค์กร"
                                                ValidationExpression="*กรุณาเลือกองค์กร" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlOrg_excel" Width="160" PopupPosition="BottomRight" />
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Department</b>
                                            <p class="list-group-item-text">ฝ่ายที่ถือครอง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlDept_excel" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="saveFormRegis" />
                                            <asp:RequiredFieldValidator ID="RequiredddlDept_excel" ValidationGroup="saveFormRegis" runat="server" Display="None"
                                                ControlToValidate="ddlDept_excel" Font-Size="11"
                                                ErrorMessage="ฝ่าย"
                                                ValidationExpression="*กรุณาเลือกฝ่าย" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlDept_excel" Width="160" PopupPosition="BottomRight" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Section</b>
                                            <p class="list-group-item-text">แผนกที่ถือครอง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlSec_excel" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="saveFormRegis" />
                                            <asp:RequiredFieldValidator ID="RequiredddlSec_excel" ValidationGroup="saveFormRegis" runat="server" Display="None"
                                                ControlToValidate="ddlSec_excel" Font-Size="11"
                                                ErrorMessage="*กรุณาเลือกแผนก"
                                                ValidationExpression="*กรุณาเลือกแผนก" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlSec_excel" Width="160" PopupPosition="BottomRight" />
                                        </div>
                                        <div class="col-sm-6 text_right small"></div>
                                        <%--<div class="col-sm-2 text_right small">
                                            <b>Status</b>
                                            <p class="list-group-item-text">สถานะ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlFormStatus" runat="server" CssClass="form-control" Enabled="true">
                                                <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                                <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Calibration Date</b>
                                            <p class="list-group-item-text">วันที่สอบเทียบครั้งล่าสุด</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:HiddenField ID="HiddenCalDate" runat="server" />
                                                <asp:TextBox ID="txt_cal_date" placeholder="เลือกวันที่สอบเทียบครั้งล่าสุด..." runat="server" CssClass="form-control datetimepicker-filter-perm-from cursor-pointer">
                                                </asp:TextBox>
                                                <span class="input-group-addon show-filter-perm-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                                <asp:RequiredFieldValidator ID="Reqtxt_cal_date" runat="server"
                                                    ControlToValidate="txt_cal_date" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกวันที่สอบเทียบครั้งล่าสุด" ValidationGroup="saveFormRegis" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="Valtxt_cal_date" runat="Server" PopupPosition="BottomRight"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqtxt_cal_date" Width="200" />
                                            </div>
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Due Date</b>
                                            <p class="list-group-item-text">วันที่สอบเทียบครั้งถัดไป</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:HiddenField ID="HiddenDueDate" runat="server" />
                                                <asp:TextBox ID="txt_due_date" placeholder="เลือกวันที่สอบเทียบครั้งถัดไป..." runat="server" CssClass="form-control datetimepicker-filter-perm-to cursor-pointer">
                                                </asp:TextBox>
                                                <span class="input-group-addon show-filter-perm-to-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                                <asp:RequiredFieldValidator ID="Reqtxt_due_date" runat="server"
                                                    ControlToValidate="txt_due_date" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกวันที่สอบเทียบครั้งถัดไป" ValidationGroup="saveFormRegis" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="Valtxt_due_date" runat="Server" PopupPosition="BottomRight"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Reqtxt_due_date" Width="200" />
                                            </div>
                                        </div>
                                    </div>
                                    <hr />

                                    <div class="form-group">

                                        <div class="col-sm-2 text_right small">
                                            <b>Receive Date</b>
                                            <p class="list-group-item-text">วันที่รับเข้าเครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtReceiveDevices" runat="server" CssClass="form-control datetimepicker-filter cursor-pointer" Enabled="true" AutoPostBack="true" ValidationGroup="saveFormRegis">

                                                </asp:TextBox>
                                                <span class="input-group-addon show-filter"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                            </div>

                                            <asp:RequiredFieldValidator ID="RequiredFieldtxtReceiveDevices" runat="server"
                                                ControlToValidate="txtReceiveDevices" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกวันที่รับเข้าเครื่องมือ" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldtxtReceiveDevices" Width="200" />
                                        </div>


                                        <div class="col-sm-2 text_right small">
                                            <b>Calibration Frequency</b>
                                            <p class="list-group-item-text">ความถี่ในการสอบเทียบ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlCalFrequency" runat="server" CssClass="form-control" Enabled="true" AutoPostBack="true" ValidationGroup="saveFormRegis"></asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredFieldddlCalFrequency" ValidationGroup="saveFormRegis" runat="server" Display="None"
                                                ControlToValidate="ddlCalFrequency" Font-Size="11"
                                                ErrorMessage="*กรุณาเลือกความถี่ในการสอบเทียบ"
                                                ValidationExpression="*กรุณาเลือกความถี่ในการสอบเทียบ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldddlCalFrequency" Width="160" PopupPosition="BottomRight" />

                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="btnAddCalFrequency" Visible="false" CssClass="btn btn-default" data-toggle="tooltip" runat="server" CommandName="btnAddCalFrequency" OnCommand="btnCommand" title="เพิ่มความถี่ในการสอบเทียบ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4">
                                            <asp:GridView ID="gvFrequency"
                                                runat="server"
                                                AutoGenerateColumns="false" Visible="false"
                                                CssClass="table table-striped table-bordered"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Font-Size="Small"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                BorderStyle="None"
                                                CellSpacing="2"
                                                OnRowDeleting="gvRowDeleted">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลความถี่ในการสอบเทียบ</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ความถี่ในการสอบเทียบ" HeaderStyle-CssClass="text-center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblFrequency" runat="server" Text='<%# Eval("Frequency")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Right">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDeleteFrequency" data-toggle="tooltip" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Resolution</b>
                                            <p class="list-group-item-text">ค่าอ่านละเอียด</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlResolution" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>



                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="btnAddResolution" CssClass="btn btn-default" data-toggle="tooltip" runat="server" CommandName="btnAddResolution" OnCommand="btnCommand" title="เพิ่มค่าอ่านละเอียด"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="gvResolution"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                BorderStyle="None"
                                                OnRowDeleting="gvRowDeleted">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลค่าอ่านละเอียด</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ค่าอ่านละเอียด" HeaderStyle-Width="85%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblResolution" runat="server" Text='<%# Eval("Resolution")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />

                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ลบ" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDeleteCalibrationPoint1" data-toggle="tooltip" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />

                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Range of use</b>
                                            <p class="list-group-item-text">ช่วงการใช้งาน</p>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtROUstart" runat="server" placeholder="เริ่มต้น.." CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RetxtROUstart" runat="server"
                                                ControlToValidate="txtROUstart" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกค่าเริ่มต้นของช่วงการใช้งาน" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtROUstart" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtROUstart" Width="200" />
                                        </div>

                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtROUend" runat="server" placeholder="สิ้นสุด.." CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RetxtROUend" runat="server"
                                                ControlToValidate="txtROUend" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกค่าสิ้นสุดของช่วงการใช้งาน" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtROUend" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtROUend" Width="200" />
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:RequiredFieldValidator ID="ReddlUnitROU" runat="server" InitialValue="0"
                                                ControlToValidate="ddlUnitROU" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกหน่วยวัด" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlUnitROU" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlUnitROU" Width="180" />
                                            <asp:DropDownList ID="ddlUnitROU" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="btnAddRangeOfUse" CssClass="btn btn-default" data-toggle="tooltip" runat="server" CommandName="btnAddRangeOfUse"
                                                OnCommand="btnCommand" title="เพิ่มช่วงการใช้งาน"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="gvRange"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                BorderStyle="None"
                                                CellSpacing="2"
                                                OnRowDeleting="gvRowDeleted">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลช่วงการใช้งาน</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ช่วงการใช้งานเริ่มต้น" HeaderStyle-Width="28%" HeaderStyle-CssClass="text-center" Visible="true" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRangeMin" runat="server" Text='<%# Eval("RangeOfUseMin")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ช่วงการใช้งานสูงสุด" HeaderStyle-Width="28%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRangeMax" runat="server" Text='<%# Eval("RangeOfUseMax")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="หน่วยวัด" HeaderStyle-Width="29%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblRangeUnit" runat="server" Text='<%# Eval("RangeOfUseUnit")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ลบ" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDeleteRange" data-toggle="tooltip" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Measuring Range</b>
                                            <p class="list-group-item-text">ช่วงการวัด/พิสัย</p>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtMRstart" runat="server" placeholder="เริ่มต้น.." CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RetxtMRstart" runat="server"
                                                ControlToValidate="txtMRstart" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกค่าเริ่มต้นของช่วงการวัด/พิสัย" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtMRstart" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtMRstart" Width="200" />
                                        </div>

                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtMRend" runat="server" placeholder="สิ้นสุด.." CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RetxtMRend" runat="server"
                                                ControlToValidate="txtMRend" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกค่าสิ้นสุดของช่วงการวัด/พิสัย" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtMRend" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtMRend" Width="200" />
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:RequiredFieldValidator ID="ReddlUnitMR" runat="server" InitialValue="0"
                                                ControlToValidate="ddlUnitMR" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกหน่วยวัด" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlUnitMR" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlUnitMR" Width="180" />
                                            <asp:DropDownList ID="ddlUnitMR" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="btnAddMeasuring" CssClass="btn btn-default" data-toggle="tooltip" runat="server" CommandName="btnAddMeasuring" OnCommand="btnCommand" title="เพิ่มช่วงการวัด/พิสัย"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="gvMeasuring"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                BorderStyle="None"
                                                CellSpacing="2"
                                                OnRowDeleting="gvRowDeleted">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลช่วงการวัด/พิสัย</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ช่วงการวัดเริ่มต้น" HeaderStyle-Width="28%" HeaderStyle-CssClass="text-center" Visible="true" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMeasuringMin" runat="server" Text='<%# Eval("MeasuringMin")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ช่วงการวัดสูงสุด" HeaderStyle-Width="28%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMeasuringMax" runat="server" Text='<%# Eval("MeasuringMax")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="หน่วยวัด" HeaderStyle-Width="29%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblMeasuringUnit" runat="server" Text='<%# Eval("MeasuringUnit")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ลบ" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDeleteMeasuring" data-toggle="tooltip" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Acceptance Criteria</b>
                                            <p class="list-group-item-text">เกณฑ์การยอมรับ</p>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtdefaultvalueAC" runat="server" Text="+-" Style="text-align: center" Enabled="false" CssClass="form-control" />
                                        </div>

                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtAC" runat="server" placeholder="ใส่ค่า.." CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RetxtAC" runat="server"
                                                ControlToValidate="txtAC" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกค่าเกณฑ์การยอมรับ" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtAC" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtAC" Width="200" />
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:RequiredFieldValidator ID="ReddlUnitAC" runat="server" InitialValue="0"
                                                ControlToValidate="ddlUnitAC" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกหน่วยวัด" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlUnitAC" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlUnitAC" Width="180" />
                                            <asp:DropDownList ID="ddlUnitAC" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="btnAddAcceptance" CssClass="btn btn-default" data-toggle="tooltip" runat="server" CommandName="btnAddAcceptance" OnCommand="btnCommand" title="เพิ่มค่าเกณฑ์การยอมรับ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="gvAcceptance"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                BorderStyle="None"
                                                OnRowDeleting="gvRowDeleted">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลเกณฑ์การยอมรับ</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="เกณฑ์การยอมรับ" HeaderStyle-Width="45%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAcceptance" runat="server" Text='<%# "+-" + " " + Eval("Acceptance")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="หน่วยวัด" HeaderStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAcceptanceUnit" runat="server" Text='<%# Eval("AcceptanceUnit")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ลบ" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDeleteAcceptance" data-toggle="tooltip" runat="server" title="delete" Text="" CssClass="btn-danger btn-sm small" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Calibration Point</b>
                                            <p class="list-group-item-text">จุดสอบเทียบ</p>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtcalpoint" runat="server" placeholder="ใส่ค่า.." CssClass="form-control" />

                                        </div>
                                        <div class="col-sm-2">

                                            <asp:DropDownList ID="ddlUnitCP" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>

                                        </div>
                                        <div class="col-sm-1">
                                            <%--ปุ่ม--%>

                                            <asp:LinkButton ID="btnInsertDeptExcel" CssClass="btn btn-default" data-toggle="tooltip" runat="server" ValidationGroup="saveInsertPoint" CommandName="btnInsertDeptExcel" OnCommand="btnCommand" title="เพิ่มจุดสอบเทียบ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                        </div>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="GvDeptExcel"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                BorderStyle="None"
                                                OnRowDeleting="gvRowDeleted">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลจุดสอบเทียบ</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="#" Visible="false">
                                                        <ItemTemplate>

                                                            <%# (Container.DataItemIndex +1) %>
                                                            <%--<asp:Label ID="lbl_RSecIDXExcel" runat="server" Visible="true" Text='<%# Eval("calibration_point_Excel")%>'></asp:Label>--%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="ค่าจุดสอบเทียบ" HeaderStyle-Width="45%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_orgnameexcel" runat="server" Text='<%# Eval("calibration_point")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="หน่วยวัด" HeaderStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_deptnameexcel" runat="server" Text='<%# Eval("unit_symbol_en")%>'></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ลบ" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btn_delete_departmentexcel" data-toggle="tooltip" runat="server" Text="" CssClass="btn-sm btn-danger" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <asp:UpdatePanel ID="updateFilecertificate" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <div class="col-sm-2 text_right small">
                                                    <b>Cetificate</b>
                                                    <p class="list-group-item-text">ใบรับรอง</p>
                                                </div>
                                                <div class="col-sm-4">
                                                    <asp:RadioButtonList ID="rd_certificate" CellPadding="3" CellSpacing="2" Width="54%" AutoPostBack="true" RepeatColumns="2" Font-Size="10" RepeatLayout="Table"
                                                        RepeatDirection="Vertical" OnSelectedIndexChanged="radioCheckChange" runat="server">
                                                        <asp:ListItem Value="1">มีใบรับรอง</asp:ListItem>
                                                        <asp:ListItem Value="0">ไม่มีใบรับรอง</asp:ListItem>
                                                    </asp:RadioButtonList>

                                                    <asp:RequiredFieldValidator ID="Requiredrd_certificate" runat="server"
                                                        ControlToValidate="rd_certificate" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกใบรับรอง" ValidationGroup="saveFormRegis" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" PopupPosition="BottomRight"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredrd_certificate" Width="200" />

                                                </div>

                                            </div>

                                            <asp:Panel class="form-group" ID="div_file_certificate" runat="server" Visible="false">
                                                <div class="form-group">
                                                    <div class="col-sm-2 col-sm-offset-2">

                                                        <asp:FileUpload ID="File_certificate" ViewStateMode="Enabled" runat="server" accept="jpg|pdf|png|jpeg" />

                                                        <asp:RequiredFieldValidator ID="RequiredFile_certificate" runat="server"
                                                            ControlToValidate="File_certificate" Display="None" SetFocusOnError="true"
                                                            ErrorMessage="*กรุณาแนบไฟล์ certificate" ValidationGroup="saveFormRegis" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" PopupPosition="BottomRight"
                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFile_certificate" Width="200" />

                                                    </div>
                                                </div>

                                            </asp:Panel>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <asp:LinkButton ID="Lbtn_submit_cal_type" ValidationGroup="saveFormRegis" CssClass="btn btn-success" Text="บันทึก" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="cmdSave" OnCommand="btnCommand"></asp:LinkButton>
                                                    <asp:LinkButton ID="Lbtn_cancel_regis" CssClass="btn btn-danger" runat="server" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancelInsertRegis" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="rd_certificate" />
                                            <asp:PostBackTrigger ControlID="Lbtn_submit_cal_type" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>

                    </InsertItemTemplate>
                    <EditItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">แก้ไขทะเบียนเครื่องมือ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Location</b>
                                            <p class="list-group-item-text">สถานที่ของเครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbplaceMachineEdit" Visible="false" Text='<%# Eval("place_idx") %>' runat="server" CssClass="form-control" />
                                            <asp:DropDownList ID="ddlPlaceMachineEdit" ValidationGroup="saveFormRegis" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="ReddlPlaceMachineEdit" runat="server" InitialValue="0"
                                                ControlToValidate="ddlPlaceMachineEdit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกสถานที่ของเครื่องมือ" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlPlaceMachineEdit" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlPlaceMachineEdit" Width="200" />
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Calibration Type</b>
                                            <p class="list-group-item-text">ประเภทการสอบเทียบ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbcaltype_Edit" runat="server" Visible="false" Text='<%# Eval("cal_type_idx") %>' CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="Redllcaltype" runat="server" InitialValue="-1"
                                                ControlToValidate="dllcaltypeEdit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกประเภทการสอบเทียบ" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valdllcaltype" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Redllcaltype" Width="200" />
                                            <asp:DropDownList ID="dllcaltypeEdit" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Name</b>
                                            <p class="list-group-item-text">ชื่อเครื่องมือ/อุปกรณ์</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEquipmentName_Edit" Visible="false" Text='<%# Eval("equipment_idx") %>' runat="server" CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="ReddlEquipmentName" runat="server" InitialValue="-1"
                                                ControlToValidate="ddlEquipmentNameEdit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกชื่อเครื่องมือ/อุปกรณ์" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlEquipmentName" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlEquipmentName" Width="200" />
                                            <asp:DropDownList ID="ddlEquipmentNameEdit" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Brand</b>
                                            <p class="list-group-item-text">ยี่ห้อ/รุ่น</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbBrand_Edit" Visible="false" Text='<%# Eval("brand_idx") %>' runat="server" CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="ReddlBrand" runat="server" InitialValue="-1"
                                                ControlToValidate="ddlBrandEdit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกยี่ห้อ/รุ่น" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlBrand" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlBrand" Width="180" />
                                            <asp:DropDownList ID="ddlBrandEdit" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine No.</b>
                                            <p class="list-group-item-text">รหัสเครื่อง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtequipmentNoEdit" runat="server" placeholder="กรอกรหัสเครื่อง.." OnTextChanged="TextBoxChanged" AutoPostBack="true" Text='<%# Eval("device_id_no") %>' CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="RetxtequipmentNo" runat="server"
                                                ControlToValidate="txtequipmentNoEdit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกรหัสเครื่อง" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtequipmentNo" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtequipmentNo" Width="200" />
                                            <asp:Label ID="lbltxtEditShowIDnumber" runat="server" Font-Size="9"></asp:Label>
                                            <asp:Label ID="lblOldIDNo" runat="server" Visible="false" Text='<%# Eval("device_id_no") %>' Font-Size="9"></asp:Label>
                                        </div>

                                        <div class="col-sm-2 text_right small">
                                            <b>Serial No.</b>
                                            <p class="list-group-item-text">หมายเลขเครื่อง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtserialnoEdit" runat="server" placeholder="กรอกหมายเลขเครื่อง.." Text='<%# Eval("device_serial") %>' CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="Retxtserialno" runat="server"
                                                ControlToValidate="txtserialnoEdit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกหมายเลขเครื่อง" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valtxtserialno" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtserialno" Width="200" />
                                            <asp:Label ID="lbltxtEditShowSerialnumber" runat="server" Font-Size="9"></asp:Label>
                                            <asp:Label ID="lblOldSerialNo" runat="server" Visible="false" Text='<%# Eval("device_serial") %>' Font-Size="9"></asp:Label>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Location</b>
                                            <p class="list-group-item-text">ที่ตั้งเครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_location_edit" Visible="false" Text='<%# Eval("m0_location_idx") %>' runat="server" CssClass="form-control" />

                                            <asp:DropDownList ID="ddl_location" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="Requiredddl_location_edit" runat="server" InitialValue="0"
                                                ControlToValidate="ddl_location" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกที่ตั้งเครื่องมือ" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredddl_location_edit" Width="200" />
                                        </div>

                                        <div class="col-sm-2 text_right small">
                                            <b>Zone</b>
                                            <p class="list-group-item-text">ตำแหน่งที่อยู่เครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_zone_edit" Visible="false" Text='<%# Eval("m1_location_idx") %>' runat="server" CssClass="form-control" />
                                            <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" InitialValue="-1"
                                                ControlToValidate="ddlEquipTypeEdit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกประเภทเครื่องมือ" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlEquipType" Width="200" />--%>
                                            <asp:DropDownList ID="ddl_Zone" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Type</b>
                                            <p class="list-group-item-text">ประเภทเครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEquipType_Edit" Visible="false" Text='<%# Eval("equipment_type_idx") %>' runat="server" CssClass="form-control" />
                                            <asp:RequiredFieldValidator ID="ReddlEquipType" runat="server" InitialValue="-1"
                                                ControlToValidate="ddlEquipTypeEdit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกประเภทเครื่องมือ" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlEquipType" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlEquipType" Width="200" />
                                            <asp:DropDownList ID="ddlEquipTypeEdit" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Detail</b>
                                            <p class="list-group-item-text">รายละเอียดเครื่อง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtequipmentdetailEdit" runat="server" TextMode="MultiLine" Rows="2" placeholder="กรอกรายละเอียดเครื่อง.." Text='<%# Eval("device_details") %>' CssClass="form-control multiline-no-resize" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Organization</b>
                                            <p class="list-group-item-text">องค์กรที่ถือครอง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbOrg_excel_Edit" runat="server" Visible="false" Text='<%# Eval("device_org_idx") %>' CssClass="form-control" />
                                            <asp:DropDownList ID="ddlOrg_excelEdit" runat="server" Enabled="false" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="saveFormRegis" />
                                            <asp:RequiredFieldValidator ID="RequiredddlOrg_excel" ValidationGroup="saveFormRegis" runat="server" Display="None"
                                                ControlToValidate="ddlOrg_excelEdit" Font-Size="11" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกองค์กรที่ถือครอง"
                                                ValidationExpression="*กรุณาเลือกองค์กร" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlOrg_excel" Width="160" PopupPosition="BottomRight" />
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Department</b>
                                            <p class="list-group-item-text">ฝ่ายที่ถือครอง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbDept_excel_Edit" runat="server" Visible="false" Text='<%# Eval("device_rdept_idx") %>' CssClass="form-control" />
                                            <asp:DropDownList ID="ddlDept_excelEdit" runat="server" Enabled="false" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="saveFormRegis" />
                                            <asp:RequiredFieldValidator ID="RequiredddlDept_excel" ValidationGroup="saveFormRegis" runat="server" Display="None"
                                                ControlToValidate="ddlDept_excelEdit" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกฝ่ายที่ถือครอง"
                                                ValidationExpression="*กรุณาเลือกฝ่าย" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlDept_excel" Width="160" PopupPosition="BottomRight" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Section</b>
                                            <p class="list-group-item-text">แผนกที่ถือครอง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbSec_excel_Edit" runat="server" Visible="false" Text='<%# Eval("device_rsec_idx") %>' CssClass="form-control" />
                                            <asp:DropDownList ID="ddlSec_excelEdit" runat="server" Enabled="false" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" ValidationGroup="saveFormRegis" />
                                            <asp:RequiredFieldValidator ID="RequiredddlSec_excel" ValidationGroup="saveFormRegis" runat="server" Display="None"
                                                ControlToValidate="ddlSec_excelEdit" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกแผนกที่ถือครอง"
                                                ValidationExpression="*กรุณาเลือกแผนก" InitialValue="-1" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlSec_excel" Width="160" PopupPosition="BottomRight" />
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Status</b>
                                            <p class="list-group-item-text">สถานะ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbFormStatusidx_Edit" runat="server" Visible="false" Text='<%# Eval("m0_status_idx") %>' CssClass="form-control" />
                                            <asp:DropDownList ID="ddlFormStatusEdit" runat="server" CssClass="form-control" Enabled="true">
                                                <%-- <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                                <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>--%>
                                            </asp:DropDownList>

                                            <asp:RequiredFieldValidator ID="RequiredddlFormStatusEdit" ValidationGroup="saveFormRegis" runat="server" Display="None"
                                                ControlToValidate="ddlFormStatusEdit" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกสถานะ"
                                                ValidationExpression="*กรุณาเลือกสถานะ" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlFormStatusEdit" Width="160" PopupPosition="BottomRight" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Calibration Date</b>
                                            <p class="list-group-item-text">วันที่สอบเทียบครั้งล่าสุด</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:HiddenField ID="HiddenCalDateEdit" runat="server" />
                                                <asp:TextBox ID="txt_cal_date" placeholder="เลือกวันที่สอบเทียบครั้งล่าสุด..." runat="server" Text='<%# Eval("device_cal_date") %>' CssClass="form-control datetimepicker-filter-perm-from cursor-pointer">
                                                </asp:TextBox>
                                                <span class="input-group-addon show-filter-perm-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Due Date</b>
                                            <p class="list-group-item-text">วันที่สอบเทียบครั้งถัดไป</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:HiddenField ID="HiddenDueDateEdit" runat="server" />
                                                <asp:TextBox ID="txt_due_date" placeholder="เลือกวันที่สอบเทียบครั้งถัดไป..." runat="server" Text='<%# Eval("device_due_date") %>' CssClass="form-control datetimepicker-filter-perm-to cursor-pointer">
                                                </asp:TextBox>
                                                <span class="input-group-addon show-filter-perm-to-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Certificate</b>
                                            <p class="list-group-item-text">ใบรับรอง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_certificateView" runat="server" Text='<%# Eval("certificate_detail") %>' CssClass="form-control" Enabled="false" />
                                        </div>

                                        <div class="col-sm-2 text_right small">
                                            <b>File Attachment</b>
                                            <p class="list-group-item-text">ไฟล์แนบ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:HyperLink runat="server" ID="btnViewFileAttachment" CssClass="pull-letf" data-toggle="tooltip" title="view" data-original-title="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>

                                            <asp:TextBox ID="txt_detailnotfile" runat="server" Text="-" CssClass="form-control" Enabled="false" Visible="false" />

                                        </div>
                                    </div>


                                    <hr />

                                    <div class="form-group">

                                        <div class="col-sm-2 text_right small">
                                            <b>Receive Date</b>
                                            <p class="list-group-item-text">วันที่รับเข้าเครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="txtreceive_devices_update" placeholder="วันที่รับเข้าเครื่องมือ..." runat="server" Text='<%# Eval("receive_devices") %>' CssClass="form-control datetimepicker-filter cursor-pointer">
                                                </asp:TextBox>
                                                <span class="input-group-addon show-filter">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>


                                        <div class="col-sm-2 text_right small">
                                            <b>Calibration Frequency</b>
                                            <p class="list-group-item-text">ความถี่ในการสอบเทียบ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:Label ID="lbl_m3_device_frequency_idx" Visible="false" runat="server" Text='<%# Eval("m3_device_frequency_idx")%>'></asp:Label>
                                            <asp:Label ID="lblFrequencyidx_update" Visible="false" runat="server" Text='<%# Eval("equipment_frequency_idx")%>'></asp:Label>
                                            <asp:DropDownList ID="ddlFrequencyUpdate" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                        </div>


                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Resolution</b>
                                            <p class="list-group-item-text">ค่าอ่านละเอียด</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <%--<asp:Label ID="lbl_equipment_resolution_idx" Visible="true" runat="server" Text='<%# Eval("equipment_resolution_idx")%>'></asp:Label>--%>
                                            <asp:DropDownList ID="ddlResolutionUpdate" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>

                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="btnAddResolutionUpdate" CssClass="btn btn-default" data-toggle="tooltip" runat="server" CommandName="btnAddResolutionUpdate" OnCommand="btnCommand" title="เพิ่มค่าอ่านละเอียด"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                        </div>

                                        <%--<div class="col-sm-1"></div>--%>
                                        <div class="col-sm-5 small">
                                            <asp:GridView ID="gvResolutionUpdate"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                OnRowDataBound="gvRowDataBound"
                                                OnRowEditing="gvRowEditing"
                                                OnRowCancelingEdit="gvRowCancelingEdit"
                                                OnRowUpdating="gvRowUpdating"
                                                BorderStyle="None">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลค่าอ่านละเอียด</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ค่าอ่านละเอียด" HeaderStyle-Width="80%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <div style="padding-top: 5px">
                                                                <%--<asp:Label ID="lblResolution_detail" runat="server" Text='<%# Eval("resolution_value")%>'></asp:Label>--%>

                                                                <asp:Label ID="lbl_unit_idx_resolution_edit" Visible="false" runat="server" Text='<%# Eval("unit_idx")%>'></asp:Label>
                                                                <asp:Label ID="lblResolution_edit" runat="server" Text='<%# Eval("resolution_name")%>'></asp:Label>
                                                                <asp:Label ID="lblunit_symbol_en_resolution_edit" runat="server" Text='<%# Eval("unit_symbol_en")%>'></asp:Label>


                                                                <%-- <asp:Label ID="Label2" runat="server" Text='<%# Eval("resolution_name")%>'></asp:Label>

                                                                <asp:Label ID="lbl_unit_idxresolutionupdate" Visible="true" runat="server" Text='<%# Eval("unit_idx")%>'></asp:Label>
                                                                <asp:Label ID="lblunit_symbol_enresolutionupdate" runat="server" Text='<%# Eval("unit_symbol_en")%>'></asp:Label>--%>
                                                            </div>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>

                                                            <div class="panel-body">
                                                                <div class="form-horizontal" role="form">

                                                                    <div class="form-group">
                                                                        <asp:Label ID="lbnameResolution" CssClass="col-sm-4 control-label" runat="server" Text="ค่าอ่านละเอียด" />
                                                                        <asp:Label ID="lblResolution_idx" runat="server" Visible="false" Text='<%# Eval("equipment_resolution_idx")%>'></asp:Label>
                                                                        <asp:Label ID="lbm0_device_idx_update" Visible="false" runat="server" Text='<%# Eval("m2_device_resolution_idx")%>'></asp:Label>
                                                                        <div class="col-sm-8">

                                                                            <asp:DropDownList ID="ddlResolutionForUpdate" runat="server" CssClass="form-control" Enabled="true">
                                                                            </asp:DropDownList>

                                                                        </div>

                                                                    </div>
                                                                </div>
                                                                <div class="form-group">

                                                                    <div class="col-sm-8 col-sm-offset-4 text-left">
                                                                        <%--<div class="col-sm-8">--%>
                                                                        <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" title="บันทึก" runat="server" data-toggle="tooltip" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default" data-toggle="tooltip" title="ยกเลิก" runat="server" CommandName="Cancel"><i class="fa fa-times"></i>

                                                                        </asp:LinkButton>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </EditItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>

                                                            <div style="padding-top: 5px">
                                                                <asp:LinkButton ID="btnEditResolution" CssClass="text-edit" runat="server" CommandName="Edit"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit"><i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                                                <asp:LinkButton ID="btnDelResolution" CssClass="text-trash" runat="server" CommandName="cmdDelete"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบช่วงการใช้งานนี้ใช่หรือไม่ ?')"
                                                                    CommandArgument=' <%# "Resolution" + "," + Eval("m2_device_resolution_idx")%> ' title="delete"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                                            </div>


                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Range of use</b>
                                            <p class="list-group-item-text">ช่วงการใช้งาน</p>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="tbRangeOfUseMinUpdate" runat="server" placeholder="เริ่มต้น.." CssClass="form-control" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="tbRangeOfUseMaxUpdate" runat="server" placeholder="สิ้นสุด.." CssClass="form-control" />
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddl_range_of_use_unitUpdate" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="btnAddRangeUpdate" CssClass="btn btn-default" data-toggle="tooltip" runat="server" CommandName="btnAddRangeUpdate" OnCommand="btnCommand" title="เพิ่มช่วงการใช้งาน"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                        </div>
                                        <%--<div class="col-sm-1"></div>--%>
                                        <div class="col-sm-5 small">
                                            <asp:GridView ID="gvRangeUpdate"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                OnRowDataBound="gvRowDataBound"
                                                OnRowEditing="gvRowEditing"
                                                OnRowCancelingEdit="gvRowCancelingEdit"
                                                OnRowUpdating="gvRowUpdating"
                                                BorderStyle="None">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลช่วงการใช้งาน</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ช่วงการใช้งานเริ่มต้น" HeaderStyle-Width="30%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <div style="padding-top: 5px;">
                                                                <asp:Label ID="lblrange_startupdate" runat="server" Text='<%# Eval("range_start")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <div class="panel-body">
                                                                <div class="form-horizontal" role="form">

                                                                    <div class="form-group">
                                                                        <asp:Label ID="lbupdates_range_start" CssClass="col-sm-5 control-label" runat="server" Text="ช่วงการใช้งานเริ่มต้น" />
                                                                        <div class="col-sm-6">
                                                                            <asp:Label ID="lbm4_device_range_idx_update" Visible="false" runat="server" Text='<%# Eval("m4_device_range_idx")%>'></asp:Label>
                                                                            <asp:TextBox ID="txt_range_start_update" runat="server" CssClass="form-control " Text='<%# Eval("range_start") %>'></asp:TextBox>
                                                                            <%-- <asp:RequiredFieldValidator ID="ReqTxbcalibration_point" runat="server"
                                                                                ControlToValidate="Txbcalibration_point" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*กรุณากรอกชื่อสถานที่" ValidationGroup="EditCP" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValTxbcalibration_point" runat="Server"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqTxbcalibration_point" Width="220" />--%>
                                                                        </div>
                                                                        <div class="col-sm-1"></div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <asp:Label ID="lbl_range_endupdate" CssClass="col-sm-5 control-label" runat="server" Text="ช่วงการใช้งานสิ้นสุด" />
                                                                        <div class="col-sm-6">
                                                                            <asp:TextBox ID="txt_range_endupdate" runat="server" CssClass="form-control " Text='<%# Eval("range_end") %>'></asp:TextBox>
                                                                            <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                                                ControlToValidate="Txbcalibration_point" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*กรุณากรอกชื่อสถานที่" ValidationGroup="EditCP" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqTxbcalibration_point" Width="220" />--%>
                                                                        </div>
                                                                        <div class="col-sm-1"></div>
                                                                    </div>


                                                                    <div class="form-group">
                                                                        <asp:Label ID="lbupdates_unit" CssClass="col-sm-5 control-label" runat="server" Text="หน่วยวัด" />
                                                                        <div class="col-sm-6">
                                                                            <asp:TextBox ID="txt_unit_idxeditrange" runat="server" Visible="false" CssClass="form-control " Text='<%# Eval("unit_idx") %>'></asp:TextBox>
                                                                            <asp:DropDownList ID="ddlrange_editrange" runat="server" CssClass="form-control" Enabled="true">
                                                                            </asp:DropDownList>
                                                                            <%--<asp:RequiredFieldValidator ID="ReqddlUnitCP_Edit" runat="server" InitialValue="-1"
                                                                                ControlToValidate="ddlUnitCP_Edit" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*กรุณากรอกชื่อสถานที่ย่อ(en)" ValidationGroup="EditCP" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlUnitCP_Edit" runat="Server"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqddlUnitCP_Edit" Width="220" />--%>
                                                                        </div>
                                                                        <div class="col-sm-1"></div>
                                                                    </div>


                                                                    <div class="form-group">
                                                                        <div class="col-sm-6 col-sm-offset-5 text-left">
                                                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" title="บันทึก" runat="server" data-toggle="tooltip" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default" data-toggle="tooltip" title="ยกเลิก" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                                        </div>

                                                                        <div class="col-sm-1"></div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ช่วงการใช้งานสิ้นสุด" HeaderStyle-Width="30%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <div style="padding-top: 5px;">
                                                                <asp:Label ID="lblrange_endupdate" runat="server" Text='<%# Eval("range_end")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="หน่วยวัด" HeaderStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <div style="padding-top: 5px;">
                                                                <asp:Label ID="lbl_unit_idxupdate" Visible="false" runat="server" Text='<%# Eval("unit_idx")%>'></asp:Label>
                                                                <asp:Label ID="lblunit_symbol_enupdate" runat="server" Text='<%# Eval("unit_symbol_en")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>

                                                            <div style="padding-top: 5px">
                                                                <asp:LinkButton ID="btnEditRangeUpdate" CssClass="text-edit" runat="server" CommandName="Edit"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit"><i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                                                <asp:LinkButton ID="btnDelRangeUpdate" CssClass="text-trash" runat="server" CommandName="btnDelRangeUpdate"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบช่วงการใช้งานนี้ใช่หรือไม่ ?')"
                                                                    CommandArgument='<%#Eval("m4_device_range_idx") %>' title="delete"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                                            </div>

                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Measuring Range</b>
                                            <p class="list-group-item-text">ช่วงการวัด/พิสัย</p>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txt_Measuringstart_update" runat="server" placeholder="เริ่มต้น.." CssClass="form-control" />
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txt_Measuringend_update" runat="server" placeholder="สิ้นสุด.." CssClass="form-control" />
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlMeasuring_Update" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="btnAddMeasuringUpdate" CssClass="btn btn-default" data-toggle="tooltip" runat="server" CommandName="btnAddMeasuringUpdate" OnCommand="btnCommand" title="เพิ่มช่วงการวัด/พิสัย"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                        </div>
                                        <%--<div class="col-sm-1"></div>--%>
                                        <div class="col-sm-5 small">
                                            <asp:GridView ID="gvMeasuringUpdate"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                OnRowDataBound="gvRowDataBound"
                                                OnRowEditing="gvRowEditing"
                                                OnRowCancelingEdit="gvRowCancelingEdit"
                                                OnRowUpdating="gvRowUpdating"
                                                BorderStyle="None">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลช่วงการวัด/พิสัย</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ช่วงการวัด/พิสัย เริ่มต้น" HeaderStyle-Width="30%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <div style="padding-top: 5px">
                                                                <asp:Label ID="lblmeasuring_startupdate" runat="server" Text='<%# Eval("measuring_start")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <div class="panel-body">
                                                                <div class="form-horizontal" role="form">

                                                                    <div class="form-group">
                                                                        <asp:Label ID="lbupdates_calibration_point" CssClass="col-sm-5 control-label" runat="server" Text="ช่วงการวัด/พิสัย เริ่มต้น" />
                                                                        <div class="col-sm-6">
                                                                            <asp:TextBox ID="txt_measuring_start_edit" runat="server" CssClass="form-control " Text='<%# Eval("measuring_start") %>'></asp:TextBox>
                                                                            <%-- <asp:RequiredFieldValidator ID="ReqTxbcalibration_point" runat="server"
                                                                                ControlToValidate="Txbcalibration_point" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*กรุณากรอกชื่อสถานที่" ValidationGroup="EditCP" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValTxbcalibration_point" runat="Server"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqTxbcalibration_point" Width="220" />--%>
                                                                        </div>
                                                                        <div class="col-sm-1"></div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <asp:Label ID="Label3" CssClass="col-sm-5 control-label" runat="server" Text="ช่วงการวัด/พิสัย สิ้นสุด" />
                                                                        <div class="col-sm-6">
                                                                            <asp:TextBox ID="txt_measuring_endedit" runat="server" CssClass="form-control " Text='<%# Eval("measuring_end") %>'></asp:TextBox>
                                                                            <%-- <asp:RequiredFieldValidator ID="ReqTxbcalibration_point" runat="server"
                                                                                ControlToValidate="Txbcalibration_point" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*กรุณากรอกชื่อสถานที่" ValidationGroup="EditCP" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValTxbcalibration_point" runat="Server"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqTxbcalibration_point" Width="220" />--%>
                                                                        </div>
                                                                        <div class="col-sm-1"></div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <asp:Label ID="Label1" CssClass="col-sm-5 control-label" runat="server" Text="หน่วยวัด" />
                                                                        <asp:Label ID="lbl_measuringunit_idxedit" runat="server" Visible="false" Text='<%# Eval("unit_idx")%>'></asp:Label>
                                                                        <asp:Label ID="lbl_m5_device_measuring_idxedit" Visible="false" runat="server" Text='<%# Eval("m5_device_measuring_idx")%>'></asp:Label>
                                                                        <div class="col-sm-6">

                                                                            <asp:DropDownList ID="ddlmeasuring_editunit" runat="server" CssClass="form-control" Enabled="true">
                                                                            </asp:DropDownList>
                                                                            <%--   <asp:RequiredFieldValidator ID="ReqddlUnitCP_Edit" runat="server" InitialValue="-1"
                                                                                ControlToValidate="ddlUnitCP_Edit" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*กรุณากรอกชื่อสถานที่ย่อ(en)" ValidationGroup="EditCP" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlUnitCP_Edit" runat="Server"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqddlUnitCP_Edit" Width="220" />--%>
                                                                        </div>

                                                                    </div>



                                                                    <div class="form-group">
                                                                        <div class="col-sm-6 col-sm-offset-5 text-left">
                                                                            <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success" title="บันทึก" runat="server" data-toggle="tooltip" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="LinkButton2" CssClass="btn btn-default" data-toggle="tooltip" title="ยกเลิก" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                                        </div>

                                                                        <%--<div class="col-sm-2"></div>--%>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ช่วงการวัด/พิสัย สิ้นสุด" HeaderStyle-Width="30%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <div style="padding-top: 5px">
                                                                <asp:Label ID="lblmeasuring_endupdate" runat="server" Text='<%# Eval("measuring_end")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="หน่วยวัด" HeaderStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <div style="padding-top: 5px;">
                                                                <asp:Label ID="lbl_unit_idxupdate_measuring" Visible="false" runat="server" Text='<%# Eval("unit_idx")%>'></asp:Label>
                                                                <asp:Label ID="lblunit_symbol_enupdate_measuring" runat="server" Text='<%# Eval("unit_symbol_en")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>

                                                            <div style="padding-top: 5px">
                                                                <asp:LinkButton ID="btnEditmeasuringUpdate" CssClass="text-edit" runat="server" CommandName="Edit"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit"><i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                                                <asp:LinkButton ID="btnDelmeasuringUpdate" CssClass="text-trash" runat="server" CommandName="btnDelmeasuringUpdate"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบช่วงการใช้งานนี้ใช่หรือไม่ ?')"
                                                                    CommandArgument='<%#Eval("m5_device_measuring_idx") %>' title="delete"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                                            </div>

                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Acceptance Criteria</b>
                                            <p class="list-group-item-text">เกณฑ์การยอมรับ</p>
                                        </div>
                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txtdefaultvalueAC" runat="server" Text="+-" Style="text-align: center" Enabled="false" CssClass="form-control" />
                                        </div>

                                        <div class="col-sm-1">
                                            <asp:TextBox ID="txt_AcceptanceUpdate" runat="server" placeholder="ใส่ค่า.." CssClass="form-control" />
                                            <%--<asp:RequiredFieldValidator ID="RetxtAC" runat="server"
                                                ControlToValidate="txtAC" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกค่าเกณฑ์การยอมรับ" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtAC" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtAC" Width="200" />--%>
                                        </div>
                                        <div class="col-sm-2">
                                            <%-- <asp:RequiredFieldValidator ID="ReddlUnitAC" runat="server" InitialValue="0"
                                                ControlToValidate="ddlUnitAC" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกหน่วยวัด" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlUnitAC" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlUnitAC" Width="180" />--%>
                                            <asp:DropDownList ID="ddlAcceptance_Update" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                        </div>

                                        <div class="col-sm-1">
                                            <asp:LinkButton ID="btnAddAcceptanceUpdate" CssClass="btn btn-default" data-toggle="tooltip" runat="server" CommandName="btnAddAcceptanceUpdate" OnCommand="btnCommand" title="เพิ่มค่าเกณฑ์การยอมรับ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                        </div>

                                        <%--<div class="col-sm-1"></div>--%>
                                        <div class="col-sm-5 small">
                                            <asp:GridView ID="gvAcceptanceUpdate"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                OnRowDataBound="gvRowDataBound"
                                                OnRowEditing="gvRowEditing"
                                                OnRowCancelingEdit="gvRowCancelingEdit"
                                                OnRowUpdating="gvRowUpdating"
                                                BorderStyle="None">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลเกณฑ์การยอมรับ</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="เกณฑ์การยอมรับ" HeaderStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <div style="padding-top: 5px">
                                                                <asp:Label ID="lblResolution_detail" runat="server" Text='<%# "+-" + " " + Eval("acceptance_criteria")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <div class="panel-body">
                                                                <div class="form-horizontal" role="form">

                                                                    <div class="form-group">
                                                                        <asp:Label ID="lbupdates_calibration_point" CssClass="col-sm-5 control-label" runat="server" Text="เกณฑ์การยอมรับ" />

                                                                        <div class="col-sm-2">
                                                                            <asp:TextBox ID="txtdefaultvalueACEdit" runat="server" Text="+-" Style="text-align: center" Enabled="false" CssClass="form-control" />
                                                                        </div>

                                                                        <div class="col-sm-4">
                                                                            <asp:TextBox ID="txt_acceptance_criteria_edit" runat="server" CssClass="form-control " Text='<%# Eval("acceptance_criteria") %>'></asp:TextBox>
                                                                            <%-- <asp:RequiredFieldValidator ID="ReqTxbcalibration_point" runat="server"
                                                                                ControlToValidate="Txbcalibration_point" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*กรุณากรอกชื่อสถานที่" ValidationGroup="EditCP" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValTxbcalibration_point" runat="Server"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqTxbcalibration_point" Width="220" />--%>
                                                                        </div>
                                                                        <%--<div class="col-sm-1"></div>--%>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <asp:Label ID="Label1" CssClass="col-sm-5 control-label" runat="server" Text="หน่วยวัด" />
                                                                        <asp:Label ID="lbl_acceptance_criteriadit" runat="server" Visible="false" Text='<%# Eval("unit_idx")%>'></asp:Label>
                                                                        <asp:Label ID="lbl_m6_device_acceptance_idxdit" Visible="false" runat="server" Text='<%# Eval("m6_device_acceptance_idx")%>'></asp:Label>
                                                                        <div class="col-sm-6">

                                                                            <asp:DropDownList ID="ddlacceptance_criteriaedit" runat="server" CssClass="form-control" Enabled="true">
                                                                            </asp:DropDownList>
                                                                            <%--   <asp:RequiredFieldValidator ID="ReqddlUnitCP_Edit" runat="server" InitialValue="-1"
                                                                                ControlToValidate="ddlUnitCP_Edit" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*กรุณากรอกชื่อสถานที่ย่อ(en)" ValidationGroup="EditCP" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlUnitCP_Edit" runat="Server"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqddlUnitCP_Edit" Width="220" />--%>
                                                                        </div>

                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-sm-6 col-sm-offset-5 text-left">
                                                                            <asp:LinkButton ID="btnAcceptanceUpdate" CssClass="btn btn-success" title="บันทึก" runat="server" data-toggle="tooltip" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="CancelbtnAcceptanceUpdate" CssClass="btn btn-default" data-toggle="tooltip" title="ยกเลิก" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                                        </div>

                                                                        <%--<div class="col-sm-2"></div>--%>
                                                                    </div>

                                                                    <%--<div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" title="บันทึก" runat="server" data-toggle="tooltip" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default" data-toggle="tooltip" title="ยกเลิก" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                                        </div>
                                                                    </div>--%>
                                                                </div>
                                                            </div>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="หน่วยวัด" HeaderStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <div style="padding-top: 5px">
                                                                <asp:Label ID="lblunit_idxacceptanceupdate" Visible="false" runat="server" Text='<%# Eval("unit_idx")%>'></asp:Label>
                                                                <asp:Label ID="lblsymbol_acceptanceupdate" runat="server" Text='<%# Eval("unit_symbol_en")%>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <div style="padding-top: 5px">

                                                                <asp:LinkButton ID="btnEditacceptanceUpdate" CssClass="text-edit" runat="server" CommandName="Edit"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit"><i class="glyphicon glyphicon-edit"></i></asp:LinkButton>

                                                                <asp:LinkButton ID="btnDelacceptanceUpdate" CssClass="text-trash" runat="server" CommandName="btnDelacceptanceUpdate"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบเกณฑ์การยอมรับนี้ใช่หรือไม่ ?')"
                                                                    CommandArgument='<%#Eval("m6_device_acceptance_idx") %>' title="delete"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                                <%--<asp:LinkButton ID="btnEditResolution" CssClass="btn-sm btn-warning small" runat="server" CommandName="Edit"
                                                                OnCommand="btnCommand" data-toggle="tooltip" title="แก้ไข"><i class="fa fa-edit" aria-hidden="true"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="btnDelResolution" runat="server" title="ลบ" Text=""
                                                                CssClass="btn-danger btn-sm small" data-toggle="tooltip" OnCommand="btnCommand" CommandName="Delete" OnClientClick="return confirm('Do you want delete this item?')"><span class="fa fa-times"></span></asp:LinkButton>--%>
                                                            </div>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Calibration Point</b>
                                            <p class="list-group-item-text">จุดสอบเทียบ</p>
                                        </div>
                                        <div class="col-sm-2">
                                            <asp:TextBox ID="txtcalpointEdit" runat="server" placeholder="ใส่ค่า.." CssClass="form-control" />
                                            <%--<asp:RequiredFieldValidator ID="Retxtcalpoint" runat="server"
                                                ControlToValidate="txtcalpointEdit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกค่าจุดสอบเทียบ" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valtxtcalpoint" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtcalpoint" Width="200" />--%>
                                        </div>
                                        <div class="col-sm-2">
                                            <%--<asp:RequiredFieldValidator ID="ReddlUnitCP" runat="server" InitialValue="0"
                                                ControlToValidate="ddlUnitCPEdit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกหน่วยวัด" ValidationGroup="saveFormRegis" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlUnitCP" runat="Server" PopupPosition="BottomRight"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlUnitCP" Width="180" />--%>
                                            <asp:DropDownList ID="ddlUnitCPEdit" runat="server" CssClass="form-control" Enabled="true">
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-1">
                                            <%--ปุ่ม--%>
                                            <asp:LinkButton ID="btnInsertCalPointEdit" data-toggle="tooltip" CssClass="btn btn-default" runat="server" CommandName="btnInsertCalpointEdit" OnCommand="btnCommand" title="เพิ่มจุดสอบเทียบ"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่ม</asp:LinkButton>
                                        </div>
                                        <%--<div class="col-sm-2 text_right small">
                                            <b>Calibration Point Table</b>
                                            <p class="list-group-item-text">ตารางจุดสอบเทียบ</p>
                                        </div>--%>
                                        <div class="col-sm-1"></div>
                                        <div class="col-sm-5 small">
                                            <asp:GridView ID="GvCalPointEdit"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                OnRowDataBound="gvRowDataBound"
                                                OnRowEditing="gvRowEditing"
                                                OnRowCancelingEdit="gvRowCancelingEdit"
                                                OnRowUpdating="gvRowUpdating"
                                                BorderStyle="None">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="ค่าจุดสอบเทียบ" HeaderStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>

                                                            <div style="padding-top: 5px;">
                                                                <asp:Label ID="lbcalibration_point" runat="server"
                                                                    Text='<%# Eval("calibration_point") %>'></asp:Label>
                                                            </div>

                                                        </ItemTemplate>
                                                        <EditItemTemplate>

                                                            <div class="panel-body">
                                                                <div class="form-horizontal" role="form">
                                                                    <div class="form-group">
                                                                        <div class="col-sm-12">
                                                                            <asp:TextBox ID="ID_M1" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("m1_device_idx") %>'></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <asp:Label ID="lbupdates_calibration_point" CssClass="col-sm-5 control-label" runat="server" Text="ค่าจุดสอบเทียบ" />
                                                                        <div class="col-sm-6">
                                                                            <asp:TextBox ID="Txbcalibration_point" runat="server" CssClass="form-control " Text='<%# Eval("calibration_point") %>'></asp:TextBox>
                                                                            <asp:RequiredFieldValidator ID="ReqTxbcalibration_point" runat="server"
                                                                                ControlToValidate="Txbcalibration_point" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*กรุณากรอกชื่อสถานที่" ValidationGroup="EditCP" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValTxbcalibration_point" runat="Server"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqTxbcalibration_point" Width="220" />
                                                                        </div>
                                                                        <div class="col-sm-1"></div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <asp:Label ID="lbupdates_unit" CssClass="col-sm-5 control-label" runat="server" Text="หน่วยวัด" />
                                                                        <div class="col-sm-6">
                                                                            <asp:TextBox ID="Txbunit_idx" runat="server" Visible="false" CssClass="form-control " Text='<%# Eval("unit_idx") %>'></asp:TextBox>
                                                                            <asp:DropDownList ID="ddlUnitCP_Edit" runat="server" CssClass="form-control" Enabled="true">
                                                                            </asp:DropDownList>
                                                                            <asp:RequiredFieldValidator ID="ReqddlUnitCP_Edit" runat="server" InitialValue="-1"
                                                                                ControlToValidate="ddlUnitCP_Edit" Display="None" SetFocusOnError="true"
                                                                                ErrorMessage="*กรุณากรอกชื่อสถานที่ย่อ(en)" ValidationGroup="EditCP" />
                                                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValddlUnitCP_Edit" runat="Server"
                                                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqddlUnitCP_Edit" Width="220" />
                                                                        </div>
                                                                        <div class="col-sm-1"></div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <div class="col-sm-6 col-sm-offset-5 text-left">
                                                                            <asp:LinkButton ID="btncalibrationupdate" CssClass="btn btn-success" title="บันทึก" runat="server" data-toggle="tooltip" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                                            <asp:LinkButton ID="lbCmdCancelcalibration" CssClass="btn btn-default" data-toggle="tooltip" title="ยกเลิก" runat="server" CommandName="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                                        </div>

                                                                        <%--<div class="col-sm-2"></div>--%>
                                                                    </div>


                                                                    <%--                                                                    <div class="form-group">
                                                                        <div class="col-sm-5"></div>
                                                                        <div class="col-sm-6">
                                                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="EditCP" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                                                Text="Save" CommandName="Update"></asp:LinkButton>
                                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                                                        </div>
                                                                    </div>--%>
                                                                </div>
                                                        </EditItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="หน่วยวัด" HeaderStyle-Width="40%" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center">
                                                        <ItemTemplate>

                                                            <div style="padding-top: 5px;">
                                                                <asp:TextBox ID="Txbunit_idx_gv" runat="server" Visible="false" CssClass="form-control " Text='<%# Eval("unit_idx") %>'></asp:TextBox>
                                                                <asp:Label ID="lbunit_symbol_en" runat="server"
                                                                    Text='<%# Eval("unit_symbol_en") %>'></asp:Label>
                                                            </div>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>



                                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <div style="padding-top: 5px">
                                                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                                                <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="btnTodelete_CPUnit_Edit"
                                                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการจุดสอบเทียบนี้ใช่หรือไม่ ?')"
                                                                    CommandArgument='<%#Eval("m1_device_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <div class="col-sm-2 col-sm-offset-2">
                                            <asp:LinkButton ID="Lbtn_submit_edit_regisedit" ValidationGroup="saveFormRegis" CssClass="btn btn-success" Text="บันทึก" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="cmdUpdate" OnCommand="btnCommand"></asp:LinkButton>
                                            <asp:LinkButton ID="Lbtn_cancel_regisedit" CssClass="btn btn-danger" runat="server" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancelEditRegis" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </EditItemTemplate>
                </asp:FormView>
            </div>
            <!--ฟอร์มวิวรายการทะเบียนเครื่องมือ"  -->
            <div id="viewRegistration" runat="server" class="col-md-12">

                <asp:FormView ID="fvOwner" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">ผู้ถือครอง</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Organization</b>
                                            <p class="list-group-item-text">องค์กรที่ถือครอง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtOrgView" runat="server" Text='<%# Eval("device_org") %>' CssClass="form-control" Enabled="false" />
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Department</b>
                                            <p class="list-group-item-text">ฝ่ายที่ถือครอง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtDeptView" runat="server" Text='<%# Eval("device_rdept") %>' CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Section</b>
                                            <p class="list-group-item-text">แผนกที่ถือครอง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtSecView" runat="server" Text='<%# Eval("device_rsec") %>' CssClass="form-control" Enabled="false" />
                                        </div>
                                        <div class="col-sm-6"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small"></div>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="btnBack" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip" Visible="true" OnCommand="btnCommand" CommandName="cmdback" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>

                <asp:FormView ID="fvEquipmentDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดเครื่องมือ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine No.</b>
                                            <p class="list-group-item-text">รหัสเครื่อง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtEquipmentNoView" runat="server" Text='<%# Eval("device_id_no") %>' CssClass="form-control" Enabled="false" />
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Place</b>
                                            <p class="list-group-item-text">สถานที่เครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtEquipmentPlaceNameView" runat="server" Text='<%# Eval("place_name") %>' CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Name</b>
                                            <p class="list-group-item-text">ชื่อเครื่องมือ/อุปกรณ์</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtEquipmentNameView" runat="server" Text='<%# Eval("equipment_name") %>' CssClass="form-control" Enabled="false" />
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Serial No.</b>
                                            <p class="list-group-item-text">หมายเลขเครื่อง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtSerialNoView" runat="server" Text='<%# Eval("device_serial") %>' CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <div class="col-sm-2 text_right small">
                                            <b>Location</b>
                                            <p class="list-group-item-text">ที่ตั้งเครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtlocationView" runat="server" Text='<%# Eval("location_name") %>' CssClass="form-control" Enabled="false" />
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Zone</b>
                                            <p class="list-group-item-text">ตำแหน่งที่อยู่เครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtzoneView" runat="server" Text='<%# Eval("location_zone") %>' CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Brand</b>
                                            <p class="list-group-item-text">ยี่ห้อ/รุ่น</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtBrandView" runat="server" Text='<%# Eval("brand_name") %>' CssClass="form-control" Enabled="false" />
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Type</b>
                                            <p class="list-group-item-text">ประเภทเครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtEquipmentTypeView" runat="server" Text='<%# Eval("equipment_type_name") %>' CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Status</b>
                                            <p class="list-group-item-text">สถานะ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_status_nameView" runat="server" Text='<%# Eval("status_name") %>' CssClass="form-control" Enabled="false" />
                                        </div>
                                        <div class="col-sm-6 text_right small"></div>
                                        <%-- <div class="col-sm-2 text_right small">
                                            <b>Due Date</b>
                                            <p class="list-group-item-text">วันที่สอบเทียบครั้งถัดไป</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Eval("device_due_date") %>' CssClass="form-control" Enabled="false" />
                                        </div>--%>
                                    </div>


                                    <hr></hr>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Receive Date</b>
                                            <p class="list-group-item-text">วันที่รับเข้าเครื่องมือ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtReceiveDateView" runat="server" Text='<%# Eval("receive_devices") %>' CssClass="form-control" Enabled="false" />
                                        </div>

                                        <div class="col-sm-2 text_right small">
                                            <b>Calibration Frequency</b>
                                            <p class="list-group-item-text">ความถี่ในการสอบเทียบ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtCFView" runat="server" Text='<%# Eval("detail_frequency") %>' CssClass="form-control" Enabled="false" />
                                        </div>

                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Cal Date</b>
                                            <p class="list-group-item-text">วันที่สอบเทียบล่าสุด</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtCalDateView" runat="server" Text='<%# Eval("device_cal_date") %>' CssClass="form-control" Enabled="false" />
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Due Date</b>
                                            <p class="list-group-item-text">วันที่สอบเทียบครั้งถัดไป</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtDueDateView" runat="server" Text='<%# Eval("device_due_date") %>' CssClass="form-control" Enabled="false" />
                                        </div>
                                    </div>



                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Range of use</b>
                                            <p class="list-group-item-text">ช่วงการใช้งาน</p>
                                        </div>
                                        <div class="col-sm-4 small">

                                            <asp:GridView ID="gvRangePeview"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                OnRowDataBound="gvRowDataBound"
                                                BorderStyle="None">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลช่วงการใช้งาน</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ช่วงการใช้งาน" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <span>
                                                                <asp:Label ID="lblrange_preview" runat="server" Text='<%# Eval("range_start") + " - " + Eval("range_end")%>'></asp:Label>
                                                                <asp:Label ID="lblrange_preview_unit" runat="server" Text='<%# Eval("unit_symbol_en")%>'></asp:Label>
                                                            </span>
                                                            <asp:Label ID="lblrange_idx_preview" runat="server" Visible="false" Text='<%# Eval("unit_idx")%>'></asp:Label>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <div class="col-sm-2 text_right small">
                                            <b>Measuring Range</b>
                                            <p class="list-group-item-text">ช่วงการวัด/พิสัย</p>
                                        </div>
                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="gvMeasuringPreview"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                OnRowDataBound="gvRowDataBound"
                                                BorderStyle="None">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลช่วงการวัด/พิสัย</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ช่วงการวัด/พิสัย" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <span>
                                                                <asp:Label ID="lbl_unit_idx_measuring_preview" Visible="false" runat="server" Text='<%# Eval("unit_idx")%>'></asp:Label>
                                                                <asp:Label ID="lblmeasuring_preview" runat="server" Text='<%# Eval("measuring_start") + " - " + Eval("measuring_end")%>'></asp:Label>
                                                                <asp:Label ID="lblunit_symbol_en_measuring_preview" runat="server" Text='<%# Eval("unit_symbol_en")%>'></asp:Label>
                                                            </span>


                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Acceptance Criteria</b>
                                            <p class="list-group-item-text">เกณฑ์การยอมรับ</p>
                                        </div>
                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="gvAcceptancePreview"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                OnRowDataBound="gvRowDataBound"
                                                BorderStyle="None">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลเกณฑ์การยอมรับ</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="เกณฑ์การยอมรับ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <span>
                                                                <asp:Label ID="lblacceptance_preview" runat="server" Text='<%# "+-" + " " + Eval("acceptance_criteria")%>'></asp:Label>
                                                                <asp:Label ID="lblunit_idxacceptance_preview" Visible="false" runat="server" Text='<%# Eval("unit_idx")%>'></asp:Label>
                                                                <asp:Label ID="lblsymbol_acceptance_preview" runat="server" Text='<%# Eval("unit_symbol_en")%>'></asp:Label>
                                                            </span>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>

                                        <div class="col-sm-2 text_right small">
                                            <b>Resolution</b>
                                            <p class="list-group-item-text">ค่าอ่่านละเอียด</p>
                                        </div>
                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="gvResolutionPreview"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                OnRowDataBound="gvRowDataBound"
                                                BorderStyle="None">
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลค่าอ่านละเอียด</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ค่าอ่านละเอียด" HeaderStyle-Width="80%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <span>
                                                                <asp:Label ID="lbl_unit_idx_resolution_preview" Visible="false" runat="server" Text='<%# Eval("unit_idx")%>'></asp:Label>
                                                                <asp:Label ID="lblResolution_preview" runat="server" Text='<%# Eval("resolution_name")%>'></asp:Label>
                                                                <asp:Label ID="lblunit_symbol_en_resolution_preview" runat="server" Text='<%# Eval("unit_symbol_en")%>'></asp:Label>
                                                            </span>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>

                                        </div>


                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Calibration Point</b>
                                            <p class="list-group-item-text">จุดสอบเทียบ</p>
                                        </div>
                                        <div class="col-sm-4 small">
                                            <asp:GridView ID="GvCalPointPreview"
                                                runat="server" HeaderStyle-Font-Size="8.5"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="10px"
                                                ShowFooter="False"
                                                OnRowDataBound="gvRowDataBound"
                                                BorderStyle="None">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">ยังไม่มีข้อมูลค่าจุดสอบเทียบ</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="ค่าจุดสอบเทียบ" HeaderStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                                                        <ItemTemplate>
                                                            <span>
                                                                <asp:Label ID="lbl_unit_idx_calibration_point_preview" Visible="false" runat="server" Text='<%# Eval("unit_idx")%>'></asp:Label>
                                                                <asp:Label ID="lblcalibration_point_preview" runat="server" Text='<%# Eval("calibration_point")%>'></asp:Label>

                                                                <asp:Label ID="lblunit_symbol_en_calibration_point_preview" runat="server" Text='<%# Eval("unit_symbol_en")%>'></asp:Label>
                                                            </span>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>

                                        </div>
                                    </div>

                                    <hr></hr>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Certificate</b>
                                            <p class="list-group-item-text">ใบรับรอง</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txt_certificateView" runat="server" Text='<%# Eval("certificate_detail") %>' CssClass="form-control" Enabled="false" />
                                        </div>

                                        <div class="col-sm-2 text_right small">
                                            <b>File Attachment</b>
                                            <p class="list-group-item-text">ไฟล์แนบ</p>
                                        </div>
                                        <div class="col-sm-4">
                                            <asp:HyperLink runat="server" ID="btnViewFileAttachment" CssClass="pull-letf" data-toggle="tooltip" title="view" data-original-title="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>

                                            <asp:TextBox ID="txt_detailnotfile" runat="server" Text="-" CssClass="form-control" Enabled="false" Visible="false" />

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-2 text_right small">
                                            <b>Machine Detail</b>
                                            <p class="list-group-item-text">รายละเอียดเครื่อง</p>
                                        </div>
                                        <div class="col-sm-10">
                                            <asp:TextBox ID="txtEquipmentDetailView" runat="server" Text='<%# Eval("device_details") %>' TextMode="MultiLine" Rows="3" Enabled="false" CssClass="form-control multiline-no-resize" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>

                <div class="form-group" id="log_register" runat="server" visible="false">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Registration Device Log</b> (ประวัติการดำเนินการทะเบียนเครื่องมือ)
                        </div>
                        <div class="panel-body">
                            <asp:Repeater ID="history_registration_device_action" runat="server">
                                <HeaderTemplate>
                                    <div class="row">
                                        <label class="col-sm-3 control-label"><small>วันที่ดำเนินการ</small></label>
                                        <label class="col-sm-3 control-label"><small>เวลาที่ดำเนินการ</small></label>
                                        <label class="col-sm-3 control-label"><small>ผู้ดำเนินการ</small></label>
                                        <label class="col-sm-3 control-label"><small>ดำเนินการ</small></label>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <small><span><%# Eval("create_date") %></span></small>
                                        </div>
                                        <div class="col-sm-3">
                                            <small><span><%# Eval("create_time") %></span></small>
                                        </div>
                                        <div class="col-sm-3">
                                            <small><span><%# Eval("emp_name_th") %></span></small>
                                        </div>
                                        <div class="col-sm-3">
                                            <small><span><%# Eval("action_name") %></span></small>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
            <!--รายการทะเบียนเครื่องมือ"  -->
            <div class="form-group" id="div_SearchIndex_1" runat="server" visible="true">
                <div id="div_SearchIndex" class="row" runat="server" visible="true">
                    <div class="col-md-6">
                        <label>ประเภทการสอบเทียบ</label>
                        <asp:DropDownList ID="dllcaltype_search" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" />
                    </div>
                    <div class="col-md-6">
                        <label>สถานที่เครื่องมือ</label>
                        <asp:DropDownList ID="ddlPlaceMachine_search" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" />
                    </div>
                </div>
            </div>
            <div id="divRegistrationList" runat="server" class="col-md-12">
                <div class="form-group">
                    <%--<hr />--%>
                    <div id="fv_SearchIndex" class="row" runat="server" visible="false">

                        <asp:LinkButton ID="btnsearchshow" CssClass="btn btn-info" runat="server" data-original-title="ค้นหา" data-toggle="tooltip"
                            OnCommand="btnCommand" CommandName="cmdshowsearch_Registration"><i class="fa fa-search" aria-hidden="true"></i> ค้นหา</asp:LinkButton>

                        <asp:LinkButton ID="btnhiddensearch" Visible="false" CssClass="btn btn-danger" runat="server" data-original-title="ยกเลิกการค้นหา"
                            data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdhiddensearch_Registration" CommandArgument="3"><i class="fa fa-times" aria-hidden="true"></i> ยกเลิกค้นหา</asp:LinkButton>

                        <asp:LinkButton ID="btnResetSearchPage" CssClass="btn btn-default" Visible="false" runat="server" data-original-title="ล้างค่า" data-toggle="tooltip" CommandName="cmdResetSearchPage" OnCommand="btnCommand"><span class="glyphicon glyphicon-refresh"></span></asp:LinkButton>


                    </div>
                </div>


                <%--<div class="form-horizontal" role="form" id="show_textsearch" runat="server" visible="false">--%>
                <!--*** START Search Index ***-->
                <div id="TabSearch" runat="server" cssclass="form-group" visible="false">
                    <div class="panel panel-info">
                        <div class="panel-heading f-bold">ค้นหาเครื่องมือสอบเทียบ</div>
                        <div class="panel-body">
                            <%--      <div class="form-horizontal" role="form">--%>

                            <%-------------- Document No, Sample Code --------------%>
                            <div class="form-group col-sm-12">
                                <div class="col-sm-1"></div>
                                <%-- <div class="col-md-3">--%>
                                <div class="col-sm-4">
                                    <label class="f-s-13">ชื่อเครื่องมือ :</label>
                                    <%--<br />--%>
                                    <asp:TextBox ID="equipment_name_search" placeholder="ชื่อเครื่องมือ..." runat="server" CssClass="form-control">
                                    </asp:TextBox>
                                </div>
                                <div class="col-sm-3    ">
                                    <label class="f-s-13">รหัสเครื่องมือ :</label>
                                    <%--<br />--%>
                                    <asp:TextBox ID="device_id_no_search" placeholder="รหัสเครื่องมือ ..." runat="server" CssClass="form-control">                                           
                                    </asp:TextBox>
                                </div>
                                <div class="col-sm-3">
                                    <label class="f-s-13">หมายเลขเครื่อง :</label>
                                    <%--<br />--%>
                                    <asp:TextBox ID="device_serial_search" placeholder="หมายเลขเครื่อง ..." runat="server" CssClass="form-control">
                                    </asp:TextBox>
                                </div>
                                <%--   </div>--%>
                            </div>
                            <div class="form-group col-sm-12">
                                <div class="col-md-1"></div>
                                <div class="col-md-4">
                                    <label class="f-s-13">องค์กร :</label>
                                    <%--<br />--%>
                                    <asp:DropDownList ID="ddOrg_seachregis" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" />
                                </div>
                                <div class="col-md-3">
                                    <label class="f-s-13">ฝ่าย :</label>
                                    <%--<br />--%>
                                    <asp:DropDownList ID="ddDept_seachregis" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" />
                                </div>
                                <div class="col-md-3">
                                    <label class="f-s-13">แผนก :</label>
                                    <%--<br />--%>
                                    <asp:DropDownList ID="ddSec_seachregis" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" />
                                </div>
                            </div>
                            <div class="form-group col-sm-12">
                                <div class="col-md-1"></div>
                                <div class="col-sm-5">
                                    <label class="f-s-13">Cal Date :</label>
                                    <%-- <asp:Label ID="Label3" CssClass="col-sm-2 control-label" runat="server" Text="Cal Date : " />--%>
                                    <div class='input-group date from-date-datepicker'>
                                        <asp:HiddenField ID="HiddenCalDateSearch" runat="server" />
                                        <asp:TextBox ID="txt_caldate_search" placeholder="Cal Date ..." runat="server" CssClass="form-control" ReadOnly="true">
                                        </asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <label class="f-s-13">Due Date :</label>
                                    <%--   <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text=" : " />--%>
                                    <div class='input-group date from-date-datepicker'>
                                        <asp:HiddenField ID="HiddenDueDateSearch" runat="server" />
                                        <asp:TextBox ID="txt_duedate_search" placeholder="Due Date ..." runat="server" CssClass="form-control" ReadOnly="true">
                                        </asp:TextBox><span class="input-group-addon"><span data-icon-element="" class="glyphicon glyphicon-calendar"></span></span>
                                    </div>
                                </div>
                            </div>
                            <%-------------- Mat., เลขตู้ --------------%>

                            <%-------------- btnsearch --------------%>

                            <div class="form-group col-sm-12">
                                <div class="col-sm-1"></div>
                                <div class="col-sm-10">
                                    <%--<div class="col-sm-12"></div>--%>
                                    <div class="pull-left">

                                        <asp:LinkButton ID="btnSearchIndex1" CssClass="btn btn-primary" runat="server" ValidationGroup="SearchIndex" OnCommand="btnCommand" CommandArgument="0" CommandName="cmdSearch_dataRegistration" data-toggle="tooltip" title="ค้นหา"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>

                                        <asp:LinkButton ID="btnCancelIndex_First" CssClass="btn btn-default" runat="server" OnCommand="btnCommand" Text="ล้างค่า" CommandName="cmdReset_dataRegistration" CommandArgument="0" data-toggle="tooltip" title="ล้างค่า"><i class="fa fa-refresh"></i>&nbsp;ล้างค่า</asp:LinkButton>
                                    </div>

                                </div>
                            </div>
                            <%-------------- btnsearch --------------%>
                        </div>
                        <%--</div>--%>
                    </div>
                </div>
                <%-- </div>--%>

                <div class="form-group" id="show_place" runat="server" visible="false">
                    <asp:Repeater ID="rptBindPlaceForSelectDoc_cims" OnItemDataBound="rptOnRowDataBound" runat="server">
                        <ItemTemplate>
                            <asp:Label ID="lbcheck_coler_place" runat="server" Visible="false" Text='<%# Eval("place_idx") %>'></asp:Label>
                            <asp:LinkButton ID="btnDocumentPlaceLab_cims" CssClass="btn btn-primary" runat="server" data-original-title='<%# Eval("place_name") %>' data-toggle="tooltip" CommandArgument='<%# Eval("place_idx")+ ";" + Eval("place_name") %>' OnCommand="btnCommand" Text='<%# Eval("place_name") %>' CommandName="cmdDocumentPlacecims"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:Label ID="lbl_place_cimsName" runat="server" Visible="true"></asp:Label>
                </div>
                <div class="form-group">
                    <h5>
                        <asp:Label ID="lbl_detail_placecims_index" Font-Bold="true" runat="server" Visible="false">
                        </asp:Label>
                    </h5>
                </div>

                <div class="form-group" id="show_gvregistration" runat="server" visible="false">
                    <asp:GridView ID="gvRegistrationList" runat="server" BackColor="Transparent"
                        AutoGenerateColumns="False" HeaderStyle-Font-Size="9.5" Width="100%"
                        CssClass="table table-bordered table-responsive"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        AllowPaging="True"
                        PageSize="10"
                        DataKeyNames="m0_device_idx">
                        <HeaderStyle CssClass="info" Height="40px" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่พบข้อมูล</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="ลำดับ" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="1%" HeaderStyle-CssClass="text-center">
                                <ItemTemplate>
                                    <asp:Label ID="lblM0DeviceIDX" runat="server" Visible="false" Text='<%# Eval("m0_device_idx") %>'></asp:Label>
                                    <asp:Label ID="lblM0DevicePlaceIDX" runat="server" Visible="false" Text='<%# Eval("place_idx") %>'></asp:Label>
                                    <%# (Container.DataItemIndex +1) %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ประเภทเครื่องมือ" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center">
                                <ItemTemplate>
                                    <asp:Label ID="lblEquipmentTypeName" runat="server" Text='<%#Eval("equipment_type_name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ชื่อเครื่องมือ" HeaderStyle-Width="16%" HeaderStyle-CssClass="text-center">
                                <ItemTemplate>
                                    <asp:Label ID="lblEquipmentName" runat="server" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="รหัสเครื่องมือ" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center">
                                <ItemTemplate>
                                    <asp:Label ID="lblDeviceIDNo" runat="server" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="หมายเลขเครื่อง" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center">
                                <ItemTemplate>
                                    <asp:Label ID="lblDeviceSerial" runat="server" Text='<%# Eval("device_serial") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ผู้ถือครอง" HeaderStyle-Width="16%" HeaderStyle-CssClass="text-center">
                                <ItemTemplate>
                                    <asp:Label ID="lblDeviceRsec" runat="server" Text='<%# Eval("device_rsec") %>'></asp:Label>
                                    <asp:Label ID="lblDeviceRsecIDX" runat="server" Visible="false" Text='<%# Eval("device_rsec_idx") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สถานะ" HeaderStyle-Width="7%" HeaderStyle-CssClass="text-center">
                                <ItemTemplate>

                                    <asp:Label ID="lbl_status_idx_detail" runat="server" Visible="false" Text='<%# Eval("m0_status_idx") %>'></asp:Label>
                                    <asp:Label ID="lblDeviceStatus" runat="server" Visible="false" Text='<%# Eval("device_status") %>'></asp:Label>
                                    <asp:Label ID="lblDeviceStatusName" runat="server" Font-Size="9" Visible="true"></asp:Label>
                                    <asp:Label ID="lblStatusOnLine" Visible="false" runat="server">
                                        <i class="fa fa-circle" style="color: lime; font-size: 10px;" aria-hidden="true"></i>&nbsp;Online
                                    </asp:Label>
                                    <asp:Label ID="lblStatusOffLine" Visible="false" runat="server">
                                       <i class="fa fa-circle" style="color: red; font-size: 10px;" aria-hidden="true"></i>&nbsp;Offline
                                    </asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="สถานะดำเนินการอื่นๆ" Visible="false" HeaderStyle-Width="0%">
                                <ItemTemplate>
                                    <asp:Label ID="lblDocStatus" runat="server" Text='<%# Eval("status_name") %>'></asp:Label>
                                    <asp:Label ID="lstaidx" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center">
                                <ItemTemplate>
                                    <asp:UpdatePanel ID="updatebtn_action" runat="server">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="btnView" CssClass="btn-sm btn-info" runat="server" CommandName="cmdViewEquipment" OnCommand="btnCommand" CommandArgument='<%# Eval("m0_device_idx") + "," + Eval("device_id_no") %>' data-toggle="tooltip" title="ดูรายละเอียด"><i class="fa fa-file-text-o"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnEdit" CssClass="btn-sm btn-warning" runat="server" CommandName="cmdEditEquipment" OnCommand="btnCommand" CommandArgument='<%# Eval("m0_device_idx") + "," + Eval("device_id_no") %>' data-toggle="tooltip" title="แก้ไข"><i class="fa fa-edit" aria-hidden="true"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnTranfer" CssClass="btn-sm btn-primary" runat="server" CommandName="cmdTranferEquipment" OnCommand="btnCommand" CommandArgument='<%# Eval("device_rsec_idx") + "," + Eval("place_idx") %>' data-toggle="tooltip" title="โอนย้ายเครื่องมือ"><i class="glyphicon glyphicon-transfer" aria-hidden="true"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnCutOff" CssClass="btn-sm btn-danger" runat="server" CommandName="cmdCutOffEquipment" OnCommand="btnCommand" CommandArgument='<%# Eval("device_rsec_idx") + "," + Eval("place_idx") %>' data-toggle="tooltip" title="ตัดเสียเครื่องมือ"><i class="fa fa-cut" aria-hidden="true"></i></asp:LinkButton>
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnView" />
                                            <asp:PostBackTrigger ControlID="btnEdit" />
                                            <asp:PostBackTrigger ControlID="btnTranfer" />
                                            <asp:PostBackTrigger ControlID="btnCutOff" />

                                        </Triggers>
                                    </asp:UpdatePanel>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>
            </div>

            <!--รายทะเบียนเครื่องมือทั้งหมดที่ถือครอง รายการเครื่องมือที่ถือครองทั้งหมด (โอนย้าย)-->
            <div id="divTranferRegistrationList" runat="server" visible="false" class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">เลือกรายการเครื่องมือที่ต้องการโอนย้าย</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <%-- <label class="col-sm-2 control-label">ค้นหาเครื่องมือ</label>
                                <div class="col-sm-10">--%>
                                <div class="col-sm-2 text_right small">
                                    <b>Search ID No.</b>
                                    <p class="list-group-item-text">ค้นหาจากรหัสเครื่องมือ</p>
                                </div>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="tbSearchIDNoTranfer" runat="server" placeholder="รหัสเครื่อง.. (ID No.)" Visible="true" CssClass="form-control" />
                                </div>
                                <div class="col-sm-2">
                                    <div class="pull-left">
                                        <asp:LinkButton ID="btnSearchTool" CssClass="btn btn-primary" runat="server"
                                            OnCommand="btnCommand" CommandName="cmdSearchToolTranfer" Font-Size="Small" data-toggle="tooltip" title="ค้นหา"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <div class="alert alert-warning" id="divAlertTranferSeacrh" runat="server" visible="false">
                                        <button type="button" class="close" data-dismiss="alert">x</button>
                                        ไม่พบรายการเครื่องมือที่ค้นหา ! กรุณาทำการกรอกรหัสเครื่องมือใหม่อีกครั้งค่ะ
                                    </div>
                                </div>
                            </div>
                            <asp:FormView ID="fvDetailSearchTranfer" runat="server" Width="100%">
                                <ItemTemplate>
                                    <div class="form-horizontal" role="form">
                                        <%--  <div class="form-group">
                                            <div class="col-sm-2 text_right small"></div>
                                            <div class="col-sm-10 text_left">
                                                 <b>รายละเอียดเครื่องมือ</b>
                                            </div>
                                         </div>--%>
                                        <div class="form-group">
                                            <asp:TextBox ID="tbM0DeviceIDX" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("m0_device_idx") %>' Enabled="false"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 text_right small">
                                                <b>ID No.</b>
                                                <p class="list-group-item-text">รหัสเครื่องมือ</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbIDCodeEquipment" runat="server" CssClass="form-control" Text='<%# Eval("device_id_no") %>' Enabled="false"></asp:TextBox>

                                            </div>
                                            <div class="col-sm-2 text_right small">
                                                <b>Machine Name</b>
                                                <p class="list-group-item-text">ชื่อเครื่องมือ/อุปกรณ์</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEquipmentName" runat="server" Text='<%# Eval("equipment_name") %>' CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 text_right small">
                                                <b>Serial No.</b>
                                                <p class="list-group-item-text">หมายเลขเครื่อง </p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEquipmentSerial" runat="server" CssClass="form-control" Text='<%# Eval("device_serial") %>' Enabled="false"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-2 text_right small">
                                                <b>Owner</b>
                                                <p class="list-group-item-text">ผู้ถือครอง</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEquipmentRSec" runat="server" Text='<%# Eval("device_rsec") %>' CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-offset-2 col-sm-10">
                                                <asp:LinkButton ID="btnInsertDetail" CssClass="btn btn-default" runat="server"
                                                    CommandName="cmdCheckFromSearch" OnCommand="btnCommand"
                                                    title="เลือกรายการ"><i class="fa fa-plus-square" aria-hidden="true"></i> เลือก</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:FormView>

                            <%--  </div>--%>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <asp:CheckBox ID="ChkAll" runat="server" Text="เลือกทั้งหมด" OnCheckedChanged="checkindexchange" AutoPostBack="true" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">เครื่องมือที่ถือครอง</label>
                                <div class="col-sm-10">
                                    <asp:GridView ID="gvListEquipment" runat="server"
                                        AutoGenerateColumns="False"
                                        CssClass="table table-striped table-bordered table-responsive"
                                        OnPageIndexChanging="gvPageIndexChanging"
                                        OnRowDataBound="gvRowDataBound"
                                        AllowPaging="false"
                                        PageSize="2"
                                        DataKeyNames="m0_device_idx">
                                        <HeaderStyle CssClass="success" Height="30px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Width="2%">
                                                <%--     <HeaderTemplate>
                                                    <asp:CheckBox ID="ChkEquipmentAll" runat="server" Text="เลือกทั้งหมด" OnCheckedChanged="checkindexchange" AutoPostBack="true" />
                                                </HeaderTemplate>--%>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkEquipment" runat="server" OnCheckedChanged="checkindexchange" Text='<%# Container.DataItemIndex %>' Style="color: transparent;" AutoPostBack="true" />
                                                    <asp:Label ID="lblDeviceIDX" runat="server" Visible="false" Text='<%# Eval("m0_device_idx") %>'></asp:Label>
                                                    <asp:Label ID="lblDevicePlaceIDX" runat="server" Visible="false" Text='<%# Eval("place_idx") %>'></asp:Label>
                                                    <asp:HiddenField ID="hfSelected" Visible="false" runat="server" Value='<%# Eval("selected") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="รหัสเครื่อง" HeaderStyle-Width="10%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipment_IDNO" runat="server" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ชื่อเครื่องมือ" HeaderStyle-Width="14%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipment_Name" runat="server" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="หมายเลขเครื่อง" HeaderStyle-Width="14%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipmentSerail" runat="server" Text='<%# Eval("device_serial") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ผู้ถือครอง" HeaderStyle-Width="14%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipmentRsection" runat="server" Text='<%# Eval("device_rsec") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ช่วงการใช้งาน" HeaderStyle-Width="14%">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txt_Range_of_use" runat="server" placeholder="ช่วงการใช้งาน" CssClass="form-control" />
                                                    <%--<asp:Label ID="lblEquipmentRsection" runat="server" Text='<%# Eval("device_rsec") %>'></asp:Label>--%>
                                                    <%--  <asp:RequiredFieldValidator ID="Requiredtxt_Range_of_use" runat="server" 
                                                        ControlToValidate="txt_Range_of_use" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกช่วงการใช้งาน" ValidationGroup="TranferGroup" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxt_Range_of_use" runat="Server" PopupPosition="BottomLeft"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_Range_of_use" Width="250" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="จุดการใช้งาน" HeaderStyle-Width="14%">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txt_Poin_of_use" runat="server" placeholder="จุดการใช้งาน" CssClass="form-control" />
                                                    <%--<asp:Label ID="lblEquipmentRsection" runat="server" Text='<%# Eval("device_rsec") %>'></asp:Label>--%>
                                                    <%--   <asp:RequiredFieldValidator ID="Requiredtxt_Poin_of_use" runat="server" 
                                                        ControlToValidate="txt_Poin_of_use" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกจุดการใช้งาน" ValidationGroup="TranferGroup" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="Validatortxt_Poin_of_use" runat="Server" PopupPosition="BottomLeft"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_Poin_of_use" Width="250" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สถานะ" Visible="false" HeaderStyle-Width="5%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStatusTool" Visible="true" runat="server" Text='<%# Eval("device_status") %>'></asp:Label>
                                                    <asp:Label ID="lblStatus" runat="server" Font-Size="9" Text='<%# Eval("device_status") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--การดำเนินการโอนย้าย -->
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">การโอนย้ายเครื่องมือ</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <div class="form-group" id="div_show_placetranfer" runat="server" visible="true">


                                <p class="col-sm-2 control-label"></p>
                                <p class="col-sm-10 textleft text-primary"><b>**กรุณาเลือก สถานที่, องค์กร, ฝ่าย และ แผนก ที่ต้องการโอนย้ายเครื่องมือ</b></p>


                                <label class="col-sm-2 control-label">สถานที่</label>
                                <div class="col-sm-4 control-label textleft">
                                    <asp:DropDownList ID="ddlPlaceTranfer" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" Enabled="true"></asp:DropDownList>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" InitialValue="-1"
                                        ControlToValidate="ddlOrgtranfer" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณาเลือกองค์กร" ValidationGroup="TranferGroup" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" PopupPosition="BottomLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequireddlOrgtranfer" Width="250" />--%>
                                </div>

                            </div>

                            <div class="form-group">

                                <label class="col-sm-2 control-label">องค์กร</label>
                                <div class="col-sm-4 control-label textleft">
                                    <asp:DropDownList ID="ddlOrgtranfer" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" Enabled="true"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequireddlOrgtranfer" runat="server" InitialValue="-1"
                                        ControlToValidate="ddlOrgtranfer" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณาเลือกองค์กร" ValidationGroup="TranferGroup" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorddlOrgtranfer" runat="Server" PopupPosition="BottomLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequireddlOrgtranfer" Width="250" />
                                </div>
                                <label class="col-sm-2 control-label">ฝ่าย</label>
                                <div class="col-sm-4 control-label textleft">
                                    <asp:DropDownList ID="ddlRdepttranfer" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredddlRdepttranfer" runat="server" InitialValue="-1"
                                        ControlToValidate="ddlRdepttranfer" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณาเลือกฝ่าย" ValidationGroup="TranferGroup" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorddlRdepttranfer" runat="Server" PopupPosition="BottomLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlRdepttranfer" Width="250" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">แผนก</label>
                                <div class="col-sm-4 control-label textleft">
                                    <asp:DropDownList ID="ddlRsectranfer" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredddlRsectranfer" runat="server" InitialValue="-1"
                                        ControlToValidate="ddlRsectranfer" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณาเลือกแผนก" ValidationGroup="TranferGroup" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorddlRsectranfer" runat="Server" PopupPosition="BottomLeft"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlRsectranfer" Width="250" />
                                </div>
                                <label class="col-sm-2 control-label">สถานะการโอนย้าย</label>
                                <div class="col-sm-4 control-label textleft">
                                    <asp:DropDownList ID="ddltranferStatus" runat="server" AutoPostBack="true" CssClass="form-control" Enabled="true">
                                        <asp:ListItem>โอนย้ายเครื่องมือ</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">หมายเหตุ</label>
                                <div class="col-sm-10 control-label textleft">
                                    <asp:TextBox ID="txtcoment_tranfer" runat="server" TextMode="MultiLine" placeholder="กรอกหมายเหตุ.." Rows="3" CssClass="form-control multiline-no-resize" Enabled="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10 control-label textleft">
                                    <asp:Repeater ID="rptActionTranfer" runat="server">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnSaveTranfer" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandArgument='<%# Eval("nodidx") %>' CommandName="cmdSaveTranfer" ValidationGroup="TranferGroup" OnClientClick="return confirm('คุณต้องการสร้างรายการโอนย้ายเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="btnCancelTranfer" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancelTranfer"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--รายทะเบียนเครื่องมือทั้งหมดที่ถือครอง รายการเครื่องมือที่ถือครองทั้งหมด (ตัดเสีย)-->
            <div id="divCutoffRegistrationList" runat="server" visible="false" class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">เลือกรายการเครื่องมือที่ต้องการตัดเสีย</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <%-- <label class="col-sm-2 control-label">ค้นหาเครื่องมือ</label>
                                <div class="col-sm-10">--%>
                                <div class="col-sm-2 text_right small">
                                    <b>Search ID No.</b>
                                    <p class="list-group-item-text">ค้นหาจากรหัสเครื่องมือ</p>
                                </div>
                                <div class="col-sm-4">
                                    <asp:TextBox ID="tbSearchIDNoCutoff" runat="server" placeholder="รหัสเครื่อง.. (ID No.)" Visible="true" CssClass="form-control" />
                                </div>
                                <div class="col-sm-2">
                                    <div class="pull-left">
                                        <asp:LinkButton ID="btnSearchToolCutoff" CssClass="btn btn-primary" runat="server"
                                            OnCommand="btnCommand" CommandName="cmdSearchToolCutoff" Font-Size="Small" data-toggle="tooltip" title="ค้นหา"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <div class="alert alert-warning" id="divAlertCutoffSeacrh" runat="server" visible="false">
                                        <button type="button" class="close" data-dismiss="alert">x</button>
                                        ไม่พบรายการเครื่องมือที่ค้นหา ! กรุณาทำการกรอกรหัสเครื่องมือใหม่อีกครั้งค่ะ
                                    </div>
                                </div>
                            </div>

                            <asp:FormView ID="fvDetailSearchCutoff" runat="server" Width="100%">
                                <ItemTemplate>
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <asp:TextBox ID="tbM0DeviceIDX" runat="server" Visible="false" CssClass="form-control" Text='<%# Eval("m0_device_idx") %>' Enabled="false"></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 text_right small">
                                                <b>ID No.</b>
                                                <p class="list-group-item-text">รหัสเครื่องมือ</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbIDCodeEquipment" runat="server" CssClass="form-control" Text='<%# Eval("device_id_no") %>' Enabled="false"></asp:TextBox>

                                            </div>
                                            <div class="col-sm-2 text_right small">
                                                <b>Machine Name</b>
                                                <p class="list-group-item-text">ชื่อเครื่องมือ/อุปกรณ์</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEquipmentName" runat="server" Text='<%# Eval("equipment_name") %>' CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-2 text_right small">
                                                <b>Serial No.</b>
                                                <p class="list-group-item-text">หมายเลขเครื่อง </p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEquipmentSerial" runat="server" CssClass="form-control" Text='<%# Eval("device_serial") %>' Enabled="false"></asp:TextBox>
                                            </div>
                                            <div class="col-sm-2 text_right small">
                                                <b>Owner</b>
                                                <p class="list-group-item-text">ผู้ถือครอง</p>
                                            </div>
                                            <div class="col-sm-4">
                                                <asp:TextBox ID="tbEquipmentRSec" runat="server" Text='<%# Eval("device_rsec") %>' CssClass="form-control" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-lg-offset-2 col-sm-10">
                                                <asp:LinkButton ID="btnInsertDetail" CssClass="btn btn-default" runat="server"
                                                    CommandName="cmdCheckFromSearchCutoff" OnCommand="btnCommand"
                                                    title="เลือกรายการ"><i class="fa fa-plus-square" aria-hidden="true"></i> เลือก</asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:FormView>
                            <hr />
                            <%--  </div>--%>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <asp:UpdatePanel ID="updatecheckedall" runat="server">
                                        <ContentTemplate>
                                            <asp:CheckBox ID="ChkAllCutoff" runat="server" Text="เลือกทั้งหมด" OnCheckedChanged="checkindexchange" AutoPostBack="true" />
                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="ChkAllCutoff" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">เครื่องมือที่ถือครอง</label>
                                <div class="col-sm-10">
                                    <asp:GridView ID="gvListEquipmentCutoff" runat="server" BackColor="Transparent"
                                        AutoGenerateColumns="False" HeaderStyle-Font-Size="9"
                                        CssClass="table table-hover table-bordered table-condensed table-responsive"
                                        OnPageIndexChanging="gvPageIndexChanging"
                                        OnRowDataBound="gvRowDataBound"
                                        AllowPaging="false"
                                        PageSize="5"
                                        DataKeyNames="m0_device_idx">
                                        <HeaderStyle CssClass="default" Height="20px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Width="2%">
                                                <%--   <HeaderTemplate>
                                                    <asp:CheckBox ID="ChkEquipmentAll" runat="server" Text="เลือกทั้งหมด" OnCheckedChanged="checkindexchange" AutoPostBack="true" />
                                                </HeaderTemplate>--%>
                                                <ItemTemplate>
                                                    <asp:UpdatePanel ID="updatechecked" runat="server">
                                                        <ContentTemplate>
                                                            <asp:CheckBox ID="ChkEquipmentCutoff" runat="server" OnCheckedChanged="checkindexchange" AutoPostBack="true" />


                                                            <asp:Label ID="lblDeviceIDX" runat="server" Visible="false" Text='<%# Eval("m0_device_idx") %>'></asp:Label>
                                                            <asp:Label ID="lblDevicePlaceIDX" runat="server" Visible="false" Text='<%# Eval("place_idx") %>'></asp:Label>
                                                            <asp:HiddenField ID="hfSelected" Visible="false" runat="server" Value='<%# Eval("selected") %>' />
                                                        </ContentTemplate>
                                                        <Triggers>
                                                            <asp:PostBackTrigger ControlID="ChkEquipmentCutoff" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="รหัสเครื่อง" HeaderStyle-Width="10%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipment_IDNO" runat="server" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ชื่อเครื่องมือ" HeaderStyle-Width="14%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipment_Name" runat="server" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="หมายเลขเครื่อง" HeaderStyle-Width="14%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipmentSerail" runat="server" Text='<%# Eval("device_serial") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ผู้ถือครอง" HeaderStyle-Width="14%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipmentRsection" runat="server" Text='<%# Eval("device_rsec") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สถานะ" Visible="false" HeaderStyle-Width="5%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblStatusTool" Visible="true" runat="server" Text='<%# Eval("device_status") %>'></asp:Label>
                                                    <asp:Label ID="lblStatus" runat="server" Font-Size="9" Text='<%# Eval("device_status") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--การดำเนินการตัดเสีย -->
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">การตัดเสียเครื่องมือ</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <%--<p style="color: red;">** แนบเอกสารอ้างอิงการตัดชำรุดเครื่องมือ ขนาดไฟล์ ไม่เกิน 5 MB</p>--%>
                            <div class="form-group">
                                <asp:Repeater ID="RepeaterCutoff" runat="server">
                                    <HeaderTemplate>
                                        <div class="row">
                                            <label class="col-sm-2 control-label">รายการตัดเสียที่เลือก</label>
                                            <label class="col-sm-1 control-label textleft">ลำดับ</label>
                                            <label class="col-sm-2 control-label textleft">รหัสเครื่อง</label>
                                            <label class="col-sm-3 control-label textleft" style="color: red"><small>** แนบเอกสารอ้างอิง ขนาดไฟล์ ไม่เกิน 5 MB</small></label>
                                            <label class="col-sm-4 control-label textleft">หมายเหตุ</label>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="row">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-1 control-label textleft">
                                                <%# (Container.ItemIndex +1) %>
                                                <asp:Label ID="lbM0Cutoff" runat="server" Visible="false" Text='<%# Eval("m0_device_idx") %>'></asp:Label>
                                            </div>
                                            <div class="col-sm-2 control-label textleft">
                                                <asp:Label ID="lbIDNOCutoff" runat="server" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                            </div>
                                            <div class="col-sm-3 control-label textleft">
                                                <asp:FileUpload ID="FileUpload2" ViewStateMode="Enabled" runat="server" accept="jpg|pdf|png|jpeg" />
                                                <asp:RequiredFieldValidator ID="RequiredFileUpload2" runat="server"
                                                    ControlToValidate="FileUpload2" ErrorMessage="กรุณาอัพโหลดไฟล์ก่อนทำการบันทึกค่ะ" Display="None" ValidationGroup="saveCutoff">
                                                </asp:RequiredFieldValidator>
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorFileUpload2" runat="Server" PopupPosition="BottomRight"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFileUpload2" Width="250" />
                                            </div>
                                            <div class="col-sm-4 control-label textleft">
                                                <asp:TextBox ID="txtcomment_cutoff" runat="server" placeholder="กรอกหมายเหตุ.." CssClass="form-control" Enabled="true" />
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">สถานะการตัดชำรุด :</label>
                                <div class="col-sm-10 control-label textleft">
                                    <asp:DropDownList ID="ddlcutoff" runat="server" CssClass="form-control" Enabled="true">
                                        <asp:ListItem>ตัดชำรุด</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10 control-label textleft">
                                    <asp:Repeater ID="rptActionCutoff" runat="server">
                                        <ItemTemplate>
                                            <asp:UpdatePanel ID="updatebtnsaveFileCutoff" runat="server">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="btnSaveCutoff" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandArgument='<%# Eval("nodidx") %>' CommandName="cmdSaveCutoff" ValidationGroup="saveCutoff" OnClientClick="return confirm('คุณต้องการสร้างรายการตัดเสียเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                                    <asp:LinkButton ID="btnCancelCuttoff" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancelTranfer"></asp:LinkButton>
                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="btnSaveCutoff" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </asp:View>

        <asp:View ID="docList" runat="server">

            <div id="div_SearchDoc" class="form-group" runat="server">
                <div id="div_SearchDoc_1" runat="server" visible="true">
                    <div id="div2" class="row" runat="server" visible="true">
                        <div class="col-md-6">
                            <label>ประเภทการสอบเทียบ</label>
                            <asp:DropDownList ID="dllcaltype_searchDoc" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" />
                        </div>
                        <div class="col-md-6">
                            <label>สถานที่เครื่องมือ</label>
                            <asp:DropDownList ID="ddlPlaceMachine_searchDoc" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" />

                        </div>
                        <hr />
                    </div>

                </div>
            </div>

            <div id="divSearchDocument" runat="server" visible="false" class="col-md-12">
                <div class="form-horizontal" role="form" id="show_search_lab" runat="server">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="input-group col-sm-4">
                                <asp:TextBox ID="txtseach_samplecode_lab" runat="server" placeholder="Machine No." CssClass="form-control" />
                                <div class="input-group-btn">
                                    <asp:LinkButton ID="btnSearchDocument" CssClass="btn btn-primary" runat="server" data-original-title="ค้นหา.." data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdSearchDocument"><i class="glyphicon glyphicon-search"></i></asp:LinkButton>
                                    <asp:LinkButton ID="btnResetDocument" CssClass="btn btn-default" runat="server" data-original-title="ล้างค่า.." data-toggle="tooltip" OnCommand="btnCommand" CommandArgument='1' CommandName="cmdResetDocument"><i class="fa fa-refresh" aria-hidden="true"></i></asp:LinkButton>
                                </div>
                            </div>
                            <label class="col-sm-8 control-label"></label>
                        </div>
                        <!--OnItemDataBound="rptOnRowDataBound"  -->
                        <div class="form-group" id="placeofDocument" runat="server" visible="false">
                            <asp:Repeater ID="rp_place_document" runat="server">
                                <ItemTemplate>
                                    <asp:Label ID="lbcheck_coler" runat="server" Visible="false" Text='<%# Eval("place_idx") %>'></asp:Label>
                                    <asp:LinkButton ID="btnPlaceDoc" CssClass="btn btn-primary" runat="server" data-original-title='<%# Eval("place_name") %>' data-toggle="tooltip" CommandArgument='<%# Eval("place_idx") %>' OnCommand="btnCommand" Text='<%# Eval("place_name") %>' CommandName="cmdPlaceDocument"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
                <%-- <div class="form-group">
                    <h5>
                        <asp:Label ID="lblNamePlaceOnDocument" Visible="false" Font-Bold="true" runat="server"></asp:Label>
                    </h5>
                </div>--%>
            </div>

            <!--รายการโอนย้าย/ตัดเสีย-->
            <div id="divdocumentList" runat="server" class="col-md-12">
                <asp:GridView ID="gvDocumentList" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-responsive"
                    OnPageIndexChanging="gvPageIndexChanging" Width="100%"
                    OnRowDataBound="gvRowDataBound"
                    AllowPaging="True"
                    PageSize="5"
                    DataKeyNames="u0_device_idx">
                    <HeaderStyle CssClass="info" Height="40px" />
                    <%--  <RowStyle Font-Size="Small" />--%>
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="รหัสเอกสาร" Visible="false" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="lblSampleName" runat="server" Text='<%# Eval("document_code") %>'></asp:Label>
                                <asp:Label ID="lbU0DocIdx" runat="server" Visible="true" Text='<%# Eval("u0_device_idx") %>'></asp:Label>
                                <asp:Label ID="lbDecision" runat="server" Visible="true" Text='<%# Eval("m0_node_idx") %>'></asp:Label>
                                <asp:Label ID="lbActorIdx" runat="server" Visible="true" Text='<%# Eval("m0_actor_idx") %>'></asp:Label>
                                <asp:Label ID="lbrsec_idx_tranfer" runat="server" Visible="true" Text='<%# Eval("rsec_idx_tranfer") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สร้าง" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center">
                            <ItemTemplate>
                                <asp:Label ID="lbDecision1" runat="server" Visible="false" Text='<%# Eval("m0_node_idx") %>'></asp:Label>
                                <asp:Label ID="lbActorIdx1" runat="server" Visible="false" Text='<%# Eval("m0_actor_idx") %>'></asp:Label>

                                <asp:Label ID="lblCreateDate" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                <asp:Label ID="lbl_rsec_idx_create" runat="server" Text='<%# Eval("rsec_idx_create") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lbl_device_rsec_idx_index" runat="server" Text='<%# Eval("device_rsec_idx") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รหัสเครื่องมือ" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <table style="background-color: transparent; border: hidden; margin-top: 0;">
                                    <asp:Repeater ID="rptEquipmentList" OnItemDataBound="rptOnRowDataBound" runat="server">
                                        <ItemTemplate>
                                            <tr style="background-color: transparent; border: hidden;">
                                                <td style="background-color: transparent; margin-top: 0;"><%# " - " +  Eval("device_id_no") + "  " %>
                                                    <asp:Label ID="lblu1_staidx" runat="server" Text='<%# Eval("staidx") %>' Visible="false"></asp:Label>
                                                    <asp:Label ID="lbpic_status" data-toggle="tooltip" runat="server"></asp:Label>

                                                </td>
                                            </tr>

                                            <asp:Label ID="lbu1_device_id_no_select" Font-Size="Small" runat="server" Text='<%# Eval("device_id_no") %>' Visible="false"></asp:Label>

                                            <asp:Label ID="lbu1_device_idx_idx_select" runat="server" Text='<%# Eval("u1_device_idx") %>' Visible="false"></asp:Label>

                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สถานที่" HeaderStyle-Width="5%" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="lblPlaceMachine" runat="server" Text='<%# Eval("place_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ประเภทเอกสาร" HeaderStyle-Width="8%" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="lblDocumentTypeName" runat="server" Text='<%# Eval("document_type_name") %>'></asp:Label>
                                <asp:Label ID="lblDocumentTypeIDX" runat="server" Visible="false" Text='<%# Eval("document_type_idx") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="สถานะ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <asp:Label ID="lblDocumentStatus" ForeColor="#0C090A" runat="server">
                                    <p><%# Eval("status_name")%></p>
                                    <p>โดย</p>
                                     <p><%# Eval("actor_name")%></p>
                                </asp:Label>
                                <asp:Label ID="lblDocumentStatusIDX" runat="server" Visible="false" Text='<%# Eval("staidx") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="การจัดการ" Visible="false" HeaderStyle-Width="10%" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="lblDecisionHeadSection" Visible="false" runat="server"></asp:Label>
                                <asp:Label ID="lblDecisionIDXHeadSection2" Visible="false" runat="server"></asp:Label>
                                <asp:Label ID="lblDecisionIDXHeadSection" Visible="false" runat="server"></asp:Label>

                                <asp:LinkButton ID="btnHeadSectionApprove" CssClass="btn btn-md btn-success" runat="server" data-original-title='อนุมัติ' data-toggle="tooltip" Text='อนุมัติ' OnCommand="btnCommand" CommandArgument='<%# Eval("document_type_idx") + "," + "1" + "," + Eval("u0_device_idx") + "," + Eval("m0_node_idx") + "," + Eval("m0_actor_idx") + "," + Container.DisplayIndex %>' CommandName="cmdHeadSectionApprove" OnClientClick="return confirm('คุณต้องการอนุมัติรายการเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                <asp:LinkButton ID="btnHeadSectionNotApprove" CssClass="btn btn-md btn-danger" runat="server" data-original-title='ไม่อนุมัติ' data-toggle="tooltip" Text='ไม่อนุมัติ' OnCommand="btnCommand" CommandArgument='<%# Eval("document_type_idx") + "," + "0" + "," + Eval("u0_device_idx") + "," + Eval("m0_node_idx") + "," + Eval("m0_actor_idx") + "," + Container.DisplayIndex  %>' CommandName="cmdHeadSectionApprove" OnClientClick="return confirm('คุณต้องการไม่อนุมัติรายการเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>


                                <asp:Label ID="lblDecisionHeadQA" Visible="true" runat="server"></asp:Label>
                                <asp:Label ID="lblDecisionIDXHeadQA2" Visible="true" runat="server"></asp:Label>
                                <asp:Label ID="lblDecisionIDXHeadQA" Visible="true" runat="server"></asp:Label>



                                <asp:LinkButton ID="btnHeadQAApprove" CssClass="btn btn-md btn-success" runat="server" data-original-title='อนุมัติ' data-toggle="tooltip" Text='อนุมัติ' OnCommand="btnCommand" CommandArgument='<%# Eval("document_type_idx") + "," + "1" + "," + Eval("u0_device_idx") + "," + Eval("m0_node_idx") + "," + Eval("m0_actor_idx") + "," + Container.DisplayIndex  %>' CommandName="cmdHeadQAApprove" OnClientClick="return confirm('คุณต้องการอนุมัติรายการเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                <asp:LinkButton ID="btnHeadQANotApprove" CssClass="btn btn-md btn-danger" runat="server" data-original-title='ไม่อนุมัติ' data-toggle="tooltip" Text='ไม่อนุมัติ' OnCommand="btnCommand" CommandArgument='<%# Eval("document_type_idx") + "," + "0" + "," + Eval("u0_device_idx") + "," + Eval("m0_node_idx") + "," + Eval("m0_actor_idx") + "," + Container.DisplayIndex %>' CommandName="cmdHeadQAApprove" OnClientClick="return confirm('คุณต้องการไม่อนุมัติรายการเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>

                                <asp:Label ID="lblDecisionReceiver" Visible="false" runat="server"></asp:Label>
                                <asp:Label ID="lblDecisionIDXReceiver2" Visible="false" runat="server"></asp:Label>
                                <asp:Label ID="lblDecisionIDXReceiver" Visible="false" runat="server"></asp:Label>

                                <asp:LinkButton ID="btnReceiverApprove" CssClass="btn btn-md btn-success" runat="server" data-original-title='รับเครื่อง' data-toggle="tooltip" Text='รับ' OnCommand="btnCommand" CommandArgument='<%# Eval("document_type_idx") + "," + "1" + "," + Eval("u0_device_idx") + "," + Eval("m0_node_idx") + "," + Eval("m0_actor_idx") + "," + Container.DisplayIndex %>' CommandName="cmdReceiverApprove" OnClientClick="return confirm('คุณต้องการรับเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                <asp:LinkButton ID="btnReceiverNotApprove" CssClass="btn btn-md btn-danger" runat="server" data-original-title='ไม่รับเครื่อง' data-toggle="tooltip" Text='ไม่รับ' OnCommand="btnCommand" CommandArgument='<%# Eval("document_type_idx") + "," + "0" + "," + Eval("u0_device_idx") + "," + Eval("m0_node_idx") + "," + Eval("m0_actor_idx") + "," + Container.DisplayIndex %>' CommandName="cmdReceiverApprove" OnClientClick="return confirm('คุณต้องการไม่รับเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>

                                <asp:Label ID="lblDecisionQA2" Visible="false" runat="server"></asp:Label>
                                <asp:Label ID="lblDecisionQA" Visible="false" runat="server"></asp:Label>

                                <asp:Label ID="lblDecisionQA2_node14" Visible="false" runat="server"></asp:Label>
                                <asp:Label ID="lblDecisionQA_node14" Visible="false" runat="server"></asp:Label>

                                <asp:Label ID="lblDecisionQACutoff2" Visible="false" runat="server"></asp:Label>
                                <asp:Label ID="lblDecisionQACutoff" Visible="false" runat="server"></asp:Label>
                                <asp:Label ID="lblstaidxQACutoff2" Visible="false" runat="server"></asp:Label>
                                <asp:Label ID="lblstaidxQACutoff" Visible="false" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="การดำเนินการ" HeaderStyle-Width="7%" HeaderStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnViewDocListAllCutoff" CssClass="btn btn-sm btn-info" runat="server" CommandName="cmdViewOnlyCutoff" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_device_idx") + "," + Eval("staidx") + "," + Eval("device_rsec_idx") + "," + Container.DisplayIndex %>' data-toggle="tooltip" title="ดูรายละเอียด"><i class="fa fa-file-text-o"></i></asp:LinkButton>

                                <asp:LinkButton ID="btnViewDocListAllTranfer" CssClass="btn btn-sm btn-info" runat="server" CommandName="cmdViewOnlyTranfer" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_device_idx") + "," + Container.DisplayIndex + "," + Eval("staidx") + "," + Eval("rsec_idx_tranfer") + "," + Eval("rsec_idx_create") %>' data-toggle="tooltip" title="ดูรายละเอียด"><i class="fa fa-file-text-o"></i></asp:LinkButton>

                                <asp:LinkButton ID="btnViewDocList" CssClass="btn btn-sm btn-info" runat="server" CommandName="cmdViewQACutoff" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_device_idx") + "," + Container.DisplayIndex %>' data-toggle="tooltip" title="ดูรายละเอียด"><i class="fa fa-file-text-o"></i></asp:LinkButton>

                                <asp:LinkButton ID="btnViewDocListTranfer" CssClass="btn btn-sm btn-info" runat="server" CommandName="cmdViewQATranfer" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_device_idx") + "," + Container.DisplayIndex + "," + Eval("staidx") %>' data-toggle="tooltip" title="ดูรายละเอียด"><i class="fa fa-file-text-o"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <!--รายละเอียดผู้ใช้งาน"  -->
            <div id="divDocDetailUser" runat="server" class="col-md-12" visible="false">
                <div class="form-group">
                    <asp:LinkButton ID="btnBackToDocList" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip" Visible="false"
                        OnCommand="btnCommand" CommandName="cmdBackToDocList" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
                </div>
                <!--รายละเอียดผู้ใช้งาน-->
                <asp:FormView ID="fvDocDetailUser" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfEmpOrgIDXTranfer" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDXTranfer" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDXTranfer" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดข้อมูลผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCodeRegistration" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpNameRegistration" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrgRegistration" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDeptRegistration" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSecRegistration" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPosRegistration" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </ItemTemplate>
                </asp:FormView>

                <!--รายละเอียดผู้สร้าง-->
                <asp:FormView ID="fvDocDetailCreator" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfEmpOrgIDXTranfer" runat="server" Value='<%# Eval("org_idx_create") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDXTranfer" runat="server" Value='<%# Eval("rdept_idx_create") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDXTranfer" runat="server" Value='<%# Eval("rsec_idx_create") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดข้อมูลผู้สร้าง</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCodeRegistration" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpNameRegistration" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrgRegistration" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDeptRegistration" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSecRegistration" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPosRegistration" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </ItemTemplate>
                </asp:FormView>
            </div>
            <!--ฟอร์มวิว QA (ตัดเสีย)-->

            <div id="div_notapprove_cutoff" runat="server" visible="false" class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">รายการตัดเสียที่ไม่ถูกอนุมัติ</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <%--<div class="form-group">
                                <div class="col-sm-2">
                                </div>
                                <label class="col-sm-6 control-label textleft" style="color: red">** เครื่องที่ไม่ได้เลือก จะเป็นการเลือกตรงกันข้าม</label>
                            </div>--%>

                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <asp:GridView ID="gv_notapprove_Cutoff" runat="server" BackColor="Transparent"
                                        AutoGenerateColumns="False" HeaderStyle-Font-Size="9"
                                        CssClass="table table-hover table-bordered table-condensed table-responsive"
                                        OnRowDataBound="gvRowDataBound"
                                        DataKeyNames="u1_device_idx">
                                        <HeaderStyle CssClass="default" Height="20px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Width="20%">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkEquipmentQACutoff" runat="server" OnCheckedChanged="checkindexchange" Visible="false" AutoPostBack="true" />
                                                    <asp:RadioButtonList ID="rblQAApproveCutoff" CellPadding="3" Font-Size="10" CellSpacing="2" Width="50%" AutoPostBack="true"
                                                        RepeatDirection="Vertical" OnSelectedIndexChanged="radioCheckChange" runat="server">
                                                        <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                                        <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:Label ID="lblDeviceIDX" runat="server" Visible="true" Text='<%# Eval("u1_device_idx") %>'></asp:Label>
                                                    <asp:Label ID="lbl_status_" runat="server" Visible="true" Text='<%# Eval("status") %>'></asp:Label>
                                                    <asp:Label ID="lblu0_device_idx" runat="server" Visible="false" Text='<%# Eval("u0_device_idx") %>'></asp:Label>
                                                    <asp:Label ID="lblm0_actor_idx" runat="server" Visible="false" Text='<%# Eval("m0_actor_idx") %>'></asp:Label>
                                                    <asp:Label ID="lblm0_node_idx" runat="server" Visible="false" Text='<%# Eval("m0_node_idx") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="รายละเอียดเครื่องมือ" HeaderStyle-Width="30%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipment_IDNO" runat="server" Visible="false" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipment_Name" runat="server" Visible="false" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipmentSerail" runat="server" Visible="false" Text='<%# Eval("device_serial") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipmentRsection" runat="server" Visible="false" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                    <asp:Label ID="lbEquipmentDetail" runat="server" Text='<%#"<p><b>" + "รหัสเครื่องมือ : " + "</b>" + Eval("device_id_no")
                                                            + "</p><br>" + "<p><b>" + "ชื่อเครื่องมือ : " + "</b>" + Eval("equipment_name") + "</p><br>" + "<p><b>" +  "หมายเลขเครื่อง : "
                                                            + "</b>" + Eval("device_serial") + "</p><br>" + "<p><b>" + "ผู้ถือครอง : " + "</b>" + Eval("sec_name_th") + "</p>" %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="การดำเนินการ" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstatus_name_cut" runat="server" Text='<%# Eval("status_name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ไฟล์แนบ" HeaderStyle-Width="10%">
                                                <ItemTemplate>

                                                    <asp:HyperLink runat="server" ID="btnViewFile_Cutoff" CssClass="pull-letf" data-toggle="tooltip" title="view" data-original-title="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>

                                                    <%-- <asp:TextBox ID="txtcomment_qacutoff" runat="server" TextMode="MultiLine" placeholder="กรอกหมายเหตุ.." Rows="3" CssClass="form-control multiline-no-resize" Enabled="true" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>


            </div>

            <div id="div_Detail_CutSuccess" runat="server" visible="false" class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">การดำเนินการตัดเสีย</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">

                                <%--<label class="col-sm-2 control-label">อนุมัติการตัดเสีย</label>--%>
                                <div class="col-sm-12">

                                    <asp:GridView ID="Gv_CutOff_Success" runat="server" BackColor="Transparent"
                                        AutoGenerateColumns="False" HeaderStyle-Font-Size="9"
                                        CssClass="table table-hover table-bordered table-condensed table-responsive"
                                        OnRowDataBound="gvRowDataBound"
                                        DataKeyNames="u1_device_idx">
                                        <HeaderStyle CssClass="default" Height="20px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="รหัสเครื่อง" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipment_IDNO_Success" runat="server" Enabled="false" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                                    <asp:Label ID="lbl_u0_device_idx_Success" runat="server" Enabled="false" Text='<%# Eval("u0_device_idx") %>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ชื่อเครื่องมือ" HeaderStyle-Width="20%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipment_Name_Success" runat="server" Enabled="false" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="หมายเลขเครื่อง" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipmentSerail_Success" runat="server" Enabled="false" Text='<%# Eval("device_serial") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ผู้ถือครอง" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipmentRsection_Success" runat="server" Enabled="false" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ไฟล์อ้างอิง" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:HyperLink runat="server" ID="btnViewFile_Success" CssClass="pull-letf" data-toggle="tooltip" title="view" data-original-title="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="การอนุมัติ" HeaderStyle-Width="33%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstatus_idx_Success" runat="server" Visible="false" Enabled="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                                    <asp:Label ID="lblstatusname_Success" runat="server" Enabled="false" Text='<%# Eval("status_name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>

            <div id="divFVVQACutoff" runat="server" visible="false" class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">การดำเนินการตัดเสีย</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <%--<div class="form-group">
                                <div class="col-sm-2">
                                </div>
                                <label class="col-sm-6 control-label textleft" style="color: red">** เครื่องที่ไม่ได้เลือก จะเป็นการเลือกตรงกันข้าม</label>
                            </div>--%>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">เครื่องมือที่ตัดเสีย</label>
                                <div class="col-sm-10">
                                    <asp:CheckBox ID="CheckAllQACutoff" runat="server" Text="เลือกทั้งหมด" Visible="false" Width="100%" OnCheckedChanged="checkindexchange" AutoPostBack="true" />
                                    <asp:CheckBox ID="CheckAllQAApproveCutoff" runat="server" Text="อนุมัติทั้งหมด" Width="100%" OnCheckedChanged="checkindexchange" AutoPostBack="true" />
                                    <asp:CheckBox ID="CheckAllQANotApproveCutoff" runat="server" Text="ไม่อนุมัติทั้งหมด" Width="100%" OnCheckedChanged="checkindexchange" AutoPostBack="true" />
                                </div>
                                <div class="col-sm-2">
                                    <asp:Label ID="lblDecisionQACutoffView2" Visible="false" runat="server"></asp:Label>
                                    <asp:Label ID="lbstaidxQACutoffView2" Visible="false" runat="server"></asp:Label>
                                </div>
                                <div class="col-sm-2">
                                    <asp:Label ID="lblDecisionQACutoffView" Visible="false" runat="server"></asp:Label>
                                    <asp:Label ID="lbstaidxQACutoffView" Visible="false" runat="server"></asp:Label>
                                </div>
                                <div class="col-sm-2">
                                    <asp:Label ID="lbl_HeadUserNotapprove" Visible="false" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_HeadUserapprove" Visible="false" runat="server"></asp:Label>
                                </div>

                                <div class="col-sm-2">
                                    <asp:Label ID="lbl_HeadQANotapprove" Visible="false" runat="server"></asp:Label>
                                    <asp:Label ID="lbl_HeadQAapprove" Visible="false" runat="server"></asp:Label>
                                </div>



                            </div>
                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <asp:GridView ID="gvEquipmentQACutoff" runat="server" BackColor="Transparent"
                                        AutoGenerateColumns="False" HeaderStyle-Font-Size="10"
                                        CssClass="table table-striped table-bordered table-responsive"
                                        OnRowDataBound="gvRowDataBound"
                                        DataKeyNames="u1_device_idx">
                                        <HeaderStyle CssClass="success" Height="30px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Width="20%">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkEquipmentQACutoff" runat="server" OnCheckedChanged="checkindexchange" Visible="false" AutoPostBack="true" />
                                                    <asp:RadioButtonList ID="rblQAApproveCutoff" CellPadding="3" Font-Size="10" CellSpacing="2" Width="100%" AutoPostBack="true"
                                                        RepeatDirection="Vertical" OnSelectedIndexChanged="radioCheckChange" runat="server">
                                                        <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                                        <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:Label ID="lblDeviceIDX" runat="server" Visible="false" Text='<%# Eval("u1_device_idx") %>'></asp:Label>
                                                    <asp:Label ID="lbl_status_" runat="server" Visible="false" Text='<%# Eval("status") %>'></asp:Label>
                                                    <asp:Label ID="lblu0_device_idx" runat="server" Visible="false" Text='<%# Eval("u0_device_idx") %>'></asp:Label>
                                                    <asp:Label ID="lblm0_actor_idx" runat="server" Visible="false" Text='<%# Eval("m0_actor_idx") %>'></asp:Label>
                                                    <asp:Label ID="lblm0_node_idx" runat="server" Visible="false" Text='<%# Eval("m0_node_idx") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="รายละเอียดเครื่องมือ" HeaderStyle-Width="30%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipment_IDNO" runat="server" Visible="false" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipment_Name" runat="server" Visible="false" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipmentSerail" runat="server" Visible="false" Text='<%# Eval("device_serial") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipmentRsection" runat="server" Visible="false" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                    <%--<asp:Label ID="lbEquipmentDetail" runat="server" Text='<%#"<p><b>" + "รหัสเครื่องมือ : " + "</b>" + Eval("device_id_no")
                                                            + "</p><br>" + "<p><b>" + "ชื่อเครื่องมือ : " + "</b>" + Eval("equipment_name") + "</p><br>" + "<p><b>" +  "หมายเลขเครื่อง : "
                                                            + "</b>" + Eval("device_serial") + "</p><br>" + "<p><b>" + "ผู้ถือครอง : " + "</b>" + Eval("sec_name_th") + "</p>" %>'></asp:Label>--%>

                                                    <asp:Label ID="DetailDevices_decision_HeadUserApprove" runat="server"> 
					                                    <p><b>รหัสเครื่องมือ :</b> <%# Eval("device_id_no") %></p> 
					                                    <p><b>ชื่อเครื่องมือ :</b> <%# Eval("equipment_name") %></p>
					                                    <p><b>หมายเลขเครื่อง :</b> <%# Eval("device_serial") %></p>
					                                    <p><b>ผู้ถือครอง :</b> <%# Eval("sec_name_th") %></p>
					                                    <p><b>สถานที่ :</b> <%# Eval("place_name") %></p>
					                                   
                                                    </asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="การดำเนินการ" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstatus_name_cut" runat="server" Text='<%# Eval("status_name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ไฟล์แนบ" HeaderStyle-Width="10%">
                                                <ItemTemplate>

                                                    <asp:HyperLink runat="server" ID="btnViewFile_Cutoff" CssClass="pull-letf" data-toggle="tooltip" title="view" data-original-title="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>

                                                    <%-- <asp:TextBox ID="txtcomment_qacutoff" runat="server" TextMode="MultiLine" placeholder="กรอกหมายเหตุ.." Rows="3" CssClass="form-control multiline-no-resize" Enabled="true" />--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="ชื่อเครื่องมือ" HeaderStyle-Width="20%" Visible ="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipment_Name" runat="server" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="หมายเลขเครื่อง" HeaderStyle-Width="15%" Visible ="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipmentSerail" runat="server" Text='<%# Eval("device_serial") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ผู้ถือครอง" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipmentRsection" runat="server" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Width="25%">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtcomment_qacutoff" runat="server" TextMode="MultiLine" placeholder="กรอกหมายเหตุ.." Rows="3" CssClass="form-control multiline-no-resize" Enabled="true" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <%--<asp:LinkButton ID="btnXmlQACutoff" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandArgument='<%# Eval("nodidx") %>' CommandName="cmdXmlCutoff" OnClientClick="return confirm('คุณต้องการสร้างรายการตัดเสียเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>--%>
                                    <asp:Repeater ID="rptActionQACutoff" runat="server">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnSaveQACutoff" CssClass="btn btn-success" runat="server" data-original-title="อนุมัติ" data-toggle="tooltip" Text="อนุมัติ" OnCommand="btnCommand" CommandArgument='<%# Eval("decision_idx") %>' CommandName="cmdXmlCutoff" OnClientClick="return confirm('คุณต้องการอนุมัติรายการตัดเสียเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <%--<asp:LinkButton ID="btnCancelQACuttoff" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancelTranfer"></asp:LinkButton>--%>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:Repeater ID="rptActionQACutoffNotAppove" runat="server">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnCancelQACuttoff" CssClass="btn btn-danger" runat="server" data-original-title="ไม่อนุมัติ" data-toggle="tooltip" Text="ไม่อนุมัติ" OnCommand="btnCommand" CommandArgument='<%# Eval("decision_idx") %>' CommandName="cmdXmlCutoff" OnClientClick="return confirm('คุณต้องการไม่อนุมัติรายการตัดเสียเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <%--<asp:LinkButton ID="btnCancelQACuttoff" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancelTranfer"></asp:LinkButton>--%>
                                        </ItemTemplate>
                                    </asp:Repeater>


                                    <asp:LinkButton ID="btnSubmitHeadUserCutoff" CssClass="btn btn-success" runat="server" data-original-title="Submit" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandName="cmdHeadUserSubmitCutoff" OnClientClick="return confirm('คุณต้องการดำเนินรายการตัดเสียเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>

                                    <asp:LinkButton ID="btnSubmitQACutoff" CssClass="btn btn-success" runat="server" data-original-title="Submit" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandName="QASubmitCutoff" OnClientClick="return confirm('คุณต้องการดำเนินรายการตัดเสียเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Cutoff Device Log</b> (ประวัติการดำเนินการตัดเสียเครื่องมือ)
                        </div>
                        <div class="panel-body">
                            <asp:Repeater ID="rpt_cutqa" runat="server">
                                <HeaderTemplate>
                                    <div class="row">
                                        <label class="col-sm-2 control-label"><small>วันที่ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>เวลาที่ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>ผู้ดำเนินการ</small></label>
                                        <label class="col-sm-4 control-label"><small>ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>หมายเหตุ</small></label>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <small><span><%# Eval("create_date") %></span></small>
                                        </div>
                                        <div class="col-sm-2">
                                            <small><span><%# Eval("create_time") %></span></small>
                                        </div>
                                        <div class="col-sm-2">
                                            <small><span><%# Eval("cemp_name") %></span></small>
                                        </div>
                                        <div class="col-sm-4">
                                            <small><span><%# Eval("decision_desc") %></span></small>
                                        </div>
                                        <div class="col-sm-2">
                                            <small><span><%# Eval("comment") %></span></small>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>


            <div id="div_detailTranfer" runat="server" visible="false" class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">รายละเอียดรายการที่ไม่ถูกพิจารณา</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <div class="form-group">
                                <div class="col-sm-2 control-label">
                                    <asp:Label ID="Label8" runat="server" Visible="false" Text="รอการอนุมัติ" Font-Bold="true"></asp:Label>
                                    <asp:Label ID="Label9" runat="server" Visible="false" Text="รายการที่อนุมัติ" Font-Bold="true"></asp:Label>
                                </div>
                                <%--<label class="col-sm-2 control-label">เครื่องมือที่ต้องโอนย้าย</label>--%>
                                <%--<div class="col-sm-12">--%>
                                <%--<div class="col-sm-2"></div>--%>
                                <div class="col-sm-12">
                                    <asp:GridView ID="gvDetailTranfer" runat="server" BackColor="Transparent"
                                        AutoGenerateColumns="False" HeaderStyle-Font-Size="9"
                                        CssClass="table table-hover table-bordered table-condensed table-responsive"
                                        OnRowDataBound="gvRowDataBound"
                                        DataKeyNames="u1_device_idx">
                                        <HeaderStyle CssClass="default" Height="20px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="รายละเอียดเครื่องมือ" HeaderStyle-Width="25%">
                                                <ItemTemplate>

                                                    <asp:Label ID="DetailDevices_not_approve" runat="server"> 
                                                        <p><b>รหัสเครื่องมือ :</b> <%# Eval("device_id_no") %></p> 
                                                        <p><b>ชื่อเครื่องมือ :</b> <%# Eval("equipment_name") %></p>
                                                        <p><b>หมายเลขเครื่อง :</b> <%# Eval("device_serial") %></p>
                                                        <p><b>ผู้ถือครอง :</b> <%# Eval("sec_name_th") %></p>
                                                        <p><b>สถานที่ :</b> <%# Eval("place_name") %></p>
                                                        <p><b>ช่วงการใช้งาน :</b> <%# Eval("range_of_use") %></p>
                                                        <p><b>จุดการใช้งาน :</b> <%# Eval("point_of_use") %></p>
                                                    </asp:Label>

                                                    <%--                                                    <asp:Label ID="lblEquipment_IDNO_success" runat="server" Visible="false" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipment_Name_success" runat="server" Visible="false" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipmentSerail_success" runat="server" Visible="false" Text='<%# Eval("device_serial") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipmentRsection_success" runat="server" Visible="false" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                    <asp:Label ID="lblReceiverEquipmentRsection_success" runat="server" Visible="false" Text='<%# Eval("tranfer_name") %>'></asp:Label>
                                                    <asp:Label ID="lbEquipmentDetail_success" runat="server" Text='<%#"<p><b>" + "รหัสเครื่องมือ : " + "</b>" + Eval("device_id_no")
                                                            + "</p><br>" + "<p><b>" + "ชื่อเครื่องมือ : " + "</b>" + Eval("equipment_name") + "</p><br>" + "<p><b>" +  "หมายเลขเครื่อง : "
                                                            + "</b>" + Eval("device_serial") + "</p><br>" + "<p><b>" + "ผู้ถือครอง : " + "</b>" + Eval("sec_name_th")                                                           
                                                            + "</p><br>" + "<p><b>" + "ช่วงการใช้งาน : " + "</b>" + Eval("range_of_use") + "</p>"
                                                            + "</p><br>" + "<p><b>" + "จุดการใช้งาน : " + "</b>" + Eval("point_of_use") + "</p>"%>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ผู้รับ" HeaderStyle-Width="30%">
                                                <ItemTemplate>
                                                    <asp:Label ID="DetailProduction_not_approve" runat="server"> 
                                                        <p><b>ผู้รับ :</b> <%# Eval("tranfer_name") %></p> 
                                                        <p><b>สถานที่ :</b> <%# Eval("place_name_tranfer") %></p>
                                                    </asp:Label>
                                                    <%--<asp:Label ID="lbltranfer_name_success" runat="server" Enabled="false" Text='<%# Eval("tranfer_name") %>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="การดำเนินการ" HeaderStyle-Width="30%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstatus_idx_not_approve" runat="server" Visible="true" Enabled="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                                    <asp:Label ID="lblstatusname_not_approve" runat="server" Enabled="false" Text='<%# Eval("status_name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Width="33%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcomment_qacutoff" runat="server" Enabled="false" Text='<%# Eval("comment") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                        </Columns>
                                    </asp:GridView>
                                </div>



                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <!--ฟอร์มวิว QA (โอนย้าย)-->
            <div id="divFVVQATranfer" runat="server" visible="false" class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">พิจารณารายการโอนย้ายเครื่องมือ</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">เครื่องมือที่โอนย้าย</label>
                                <div class="col-sm-10">
                                    <asp:CheckBox ID="CheckAllQATranfer" runat="server" Text="เลือกทั้งหมด" Visible="false" Width="100%" OnCheckedChanged="checkindexchange" AutoPostBack="true" />
                                    <asp:CheckBox ID="CheckAllQAApproveTranfer" runat="server" Text="อนุมัติทั้งหมด" Width="100%" OnCheckedChanged="checkindexchange" AutoPostBack="true" />
                                    <asp:CheckBox ID="CheckAllQANotApproveTranfer" runat="server" Text="ไม่อนุมัติทั้งหมด" Width="100%" OnCheckedChanged="checkindexchange" AutoPostBack="true" />
                                </div>
                                <div class="col-sm-2">
                                    <asp:Label ID="lblDecisionQATranferView2" Visible="false" runat="server"></asp:Label>
                                </div>
                                <div class="col-sm-2">
                                    <asp:Label ID="lblDecisionQATranferView" Visible="false" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <asp:GridView ID="gvEquipmentQATranfer" runat="server"
                                        AutoGenerateColumns="False" HeaderStyle-Font-Size="10"
                                        CssClass="table table-striped table-bordered table-responsive"
                                        OnRowDataBound="gvRowDataBound"
                                        DataKeyNames="u1_device_idx">
                                        <HeaderStyle CssClass="success" Height="30px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="ChkEquipmentQATranfer" runat="server" Visible="false" OnCheckedChanged="checkindexchange" AutoPostBack="true" />
                                                    <asp:RadioButtonList ID="rblQAApproveTranfer" CellPadding="3" Font-Size="10" CellSpacing="2" Width="100%" AutoPostBack="true"
                                                        RepeatDirection="Vertical" OnSelectedIndexChanged="radioCheckChange" runat="server">
                                                        <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                                        <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:Label ID="lblDeviceIDX" runat="server" Visible="false" Text='<%# Eval("u1_device_idx") %>'></asp:Label>
                                                    <asp:Label ID="lblu0_device_idx" runat="server" Visible="false" Text='<%# Eval("u0_device_idx") %>'></asp:Label>
                                                    <asp:Label ID="lblm0_actor_idx" runat="server" Visible="false" Text='<%# Eval("m0_actor_idx") %>'></asp:Label>
                                                    <asp:Label ID="lblm0_node_idx" runat="server" Visible="false" Text='<%# Eval("m0_node_idx") %>'></asp:Label>
                                                    <asp:Label ID="lbl_status" runat="server" Visible="false" Text='<%# Eval("status") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="รายละเอียดเครื่องมือ" HeaderStyle-Width="30%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipment_IDNO" runat="server" Visible="false" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipment_Name" runat="server" Visible="false" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipmentSerail" runat="server" Visible="false" Text='<%# Eval("device_serial") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipmentRsection" runat="server" Visible="false" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                    <asp:Label ID="lblReceiverEquipmentRsection" runat="server" Visible="false" Text='<%# Eval("tranfer_name") %>'></asp:Label>
                                                    <asp:Label ID="lbEquipmentDetail" runat="server" Visible="false" Text='<%#"<p><b>" + "รหัสเครื่องมือ : " + "</b>" + Eval("device_id_no")
                                                            + "</p><br>" + "<p><b>" + "ชื่อเครื่องมือ : " + "</b>" + Eval("equipment_name") + "</p><br>" + "<p><b>" +  "หมายเลขเครื่อง : "
                                                            + "</b>" + Eval("device_serial") + "</p><br>" + "<p><b>" + "ผู้ถือครอง : " + "</b>" + Eval("sec_name_th") 
                                                            + "</p><br>"+ "<p><b>" + "ผู้รับ : " + "</b>" + Eval("tranfer_name") + "</p>" %>'></asp:Label>

                                                    <asp:Label ID="DetailDevices_decision" runat="server"> 
                                                        <p><b>รหัสเครื่องมือ :</b> <%# Eval("device_id_no") %></p> 
                                                        <p><b>ชื่อเครื่องมือ :</b> <%# Eval("equipment_name") %></p>
                                                        <p><b>หมายเลขเครื่อง :</b> <%# Eval("device_serial") %></p>
                                                        <p><b>ผู้ถือครอง :</b> <%# Eval("sec_name_th") %></p>
                                                        <p><b>สถานที่ :</b> <%# Eval("place_name") %></p>
                                                        <p><b>ผู้รับ :</b> <%# Eval("tranfer_name") %></p>
                                                        <p><b>สถานที่ผู้รับ :</b> <%# Eval("place_name_tranfer") %></p>
                                                        <p><b>ช่วงการใช้งาน :</b> <%# Eval("range_of_use") %></p>
                                                        <p><b>จุดการใช้งาน :</b> <%# Eval("point_of_use") %></p>
                                                    </asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Width="25%">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtcomment_qacutoff" runat="server" TextMode="MultiLine" placeholder="กรอกหมายเหตุ.." Rows="3" CssClass="form-control multiline-no-resize" Enabled="true" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="การดำเนินการ" HeaderStyle-Width="30%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_qa_status_name" runat="server" Text='<%# Eval("status_name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <%--<asp:LinkButton ID="btnXmlQACutoff" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="Save" OnCommand="btnCommand" CommandArgument='<%# Eval("nodidx") %>' CommandName="cmdXmlCutoff" OnClientClick="return confirm('คุณต้องการสร้างรายการตัดเสียเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>--%>
                                    <asp:Repeater ID="rptActionQATranfer" runat="server">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnSaveQATranfer" CssClass="btn btn-success" runat="server" data-original-title="อนุมัติ" data-toggle="tooltip" Text="อนุมัติ" OnCommand="btnCommand" CommandArgument='<%# Eval("decision_idx") %>' CommandName="cmdXmlTranfer" OnClientClick="return confirm('คุณต้องการอนุมัติรายการตัดเสียเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <%--<asp:LinkButton ID="btnCancelQACuttoff" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancelTranfer"></asp:LinkButton>--%>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:Repeater ID="rptActionQATranferNotApprove" runat="server">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnCancelQATranfer" CssClass="btn btn-danger" runat="server" data-original-title="ไม่อนุมัติ" data-toggle="tooltip" Text="ไม่อนุมัติ" OnCommand="btnCommand" CommandArgument='<%# Eval("decision_idx") %>' CommandName="cmdXmlTranfer" OnClientClick="return confirm('คุณต้องการไม่อนุมัติรายการตัดเสียเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <%--<asp:LinkButton ID="btnCancelQACuttoff" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancelTranfer"></asp:LinkButton>--%>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <asp:LinkButton ID="btnSubmitQATranfer" CssClass="btn btn-success" runat="server" data-original-title="Submit" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandName="QASubmitTranfer" OnClientClick="return confirm('คุณต้องการดำเนินรายการโอนย้ายเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Transfer Device Log</b> (ประวัติการดำเนินการโอนย้ายเครื่องมือ)
                        </div>
                        <div class="panel-body">
                            <asp:Repeater ID="rpt_qatranfer" runat="server">
                                <HeaderTemplate>
                                    <div class="row">
                                        <label class="col-sm-2 control-label"><small>วันที่ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>เวลาที่ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>ผู้ดำเนินการ</small></label>
                                        <label class="col-sm-4 control-label"><small>ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>หมายเหตุ</small></label>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <small><span><%# Eval("create_date") %></span></small>
                                        </div>
                                        <div class="col-sm-2">
                                            <small><span><%# Eval("create_time") %></span></small>
                                        </div>
                                        <div class="col-sm-2">
                                            <small><span><%# Eval("cemp_name") %></span></small>
                                        </div>
                                        <div class="col-sm-4">
                                            <small><span><%# Eval("decision_desc") %></span></small>
                                        </div>
                                        <div class="col-sm-2">
                                            <small><span><%# Eval("comment") %></span></small>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>

            </div>
            <!--ฟอร์มวิว ALL (ตัดเสีย)-->

            <div id="div_Log_DetailCut_Sucess" runat="server" visible="false" class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <b>Cutoff Device Log</b> (ประวัติการดำเนินการตัดเสียเครื่องมือ)
                    </div>
                    <div class="panel-body">
                        <asp:Repeater ID="rpt_sucess_cutoff" runat="server">
                            <HeaderTemplate>
                                <div class="row">
                                    <label class="col-sm-2 control-label"><small>วันที่ดำเนินการ</small></label>
                                    <label class="col-sm-2 control-label"><small>เวลาที่ดำเนินการ</small></label>
                                    <label class="col-sm-2 control-label"><small>ผู้ดำเนินการ</small></label>
                                    <label class="col-sm-4 control-label"><small>ดำเนินการ</small></label>
                                    <label class="col-sm-2 control-label"><small>หมายเหตุ</small></label>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <small><span><%# Eval("create_date") %></span></small>
                                    </div>
                                    <div class="col-sm-2">
                                        <small><span><%# Eval("create_time") %></span></small>
                                    </div>
                                    <div class="col-sm-2">
                                        <small><span><%# Eval("cemp_name") %></span></small>
                                    </div>
                                    <div class="col-sm-4">
                                        <small><span><%# Eval("decision_desc") %></span></small>
                                    </div>
                                    <div class="col-sm-2">
                                        <small><span><%# Eval("comment") %></span></small>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>

            <div id="divFVVAllCutoff" runat="server" visible="false" class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">การดำเนินการตัดเสีย</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <div class="col-sm-2 control-label">
                                    <asp:Label ID="lbWaiting" runat="server" Visible="false" Text="รอการอนุมัติ" Font-Bold="true"></asp:Label>
                                    <asp:Label ID="lbApprove" runat="server" Visible="false" Text="อนุมัติการตัดเสีย" Font-Bold="true"></asp:Label>
                                </div>
                                <%--<label class="col-sm-2 control-label">อนุมัติการตัดเสีย</label>--%>
                                <div class="col-sm-12">

                                    <asp:GridView ID="gvEquipmentAllCutoff" runat="server" BackColor="Transparent"
                                        AutoGenerateColumns="False" HeaderStyle-Font-Size="9"
                                        CssClass="table table-hover table-bordered table-condensed table-responsive"
                                        OnRowDataBound="gvRowDataBound"
                                        DataKeyNames="u1_device_idx">
                                        <HeaderStyle CssClass="default" Height="20px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="รหัสเครื่อง" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipment_IDNO" runat="server" Enabled="false" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ชื่อเครื่องมือ" HeaderStyle-Width="20%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipment_Name" runat="server" Enabled="false" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="หมายเลขเครื่อง" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipmentSerail" runat="server" Enabled="false" Text='<%# Eval("device_serial") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ผู้ถือครอง" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipmentRsection" runat="server" Enabled="false" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ไฟล์อ้างอิง" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:HyperLink runat="server" ID="btnViewFile" CssClass="pull-letf" data-toggle="tooltip" title="view" data-original-title="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="การอนุมัติ" HeaderStyle-Width="33%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstatus_idx" runat="server" Visible="false" Enabled="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                                    <asp:Label ID="lblstatusname" runat="server" Enabled="false" Text='<%# Eval("status_name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-2 control-label">
                                    <asp:Label ID="lbNotApprove" runat="server" Visible="false" Text="ไม่อนุมัติการตัดเสีย" Font-Bold="true"></asp:Label>
                                </div>
                                <div class="col-sm-10">
                                    <asp:GridView ID="gvEquipmentAllCutoffNA" runat="server" BackColor="Transparent" Visible="false"
                                        AutoGenerateColumns="False" HeaderStyle-Font-Size="9"
                                        CssClass="table table-hover table-bordered table-condensed table-responsive"
                                        OnRowDataBound="gvRowDataBound"
                                        DataKeyNames="u1_device_idx">
                                        <HeaderStyle CssClass="default" Height="20px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>
                                            <asp:TemplateField HeaderText="รหัสเครื่อง" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipmentIDNO" runat="server" Enabled="false" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ชื่อเครื่องมือ" HeaderStyle-Width="20%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipment_Name" runat="server" Enabled="false" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="หมายเลขเครื่อง" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipmentSerail" runat="server" Enabled="false" Text='<%# Eval("device_serial") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ผู้ถือครอง" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipmentRsection" runat="server" Enabled="false" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="ไฟล์อ้างอิง" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:HyperLink runat="server" ID="btnViewFiles" CssClass="pull-letf" data-toggle="tooltip" title="view" data-original-title="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Width="33%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcomment_qacutoff" runat="server" Enabled="false" Text='<%# Eval("comment") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <b>Cutoff Device Log</b> (ประวัติการดำเนินการตัดเสียเครื่องมือ)
                        </div>
                        <div class="panel-body">
                            <asp:Repeater ID="rptLogCutoff" runat="server">
                                <HeaderTemplate>
                                    <div class="row">
                                        <label class="col-sm-2 control-label"><small>วันที่ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>เวลาที่ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>ผู้ดำเนินการ</small></label>
                                        <label class="col-sm-4 control-label"><small>ดำเนินการ</small></label>
                                        <label class="col-sm-2 control-label"><small>หมายเหตุ</small></label>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <small><span><%# Eval("create_date") %></span></small>
                                        </div>
                                        <div class="col-sm-2">
                                            <small><span><%# Eval("create_time") %></span></small>
                                        </div>
                                        <div class="col-sm-2">
                                            <small><span><%# Eval("cemp_name") %></span></small>
                                        </div>
                                        <div class="col-sm-4">
                                            <small><span><%# Eval("decision_desc") %></span></small>
                                        </div>
                                        <div class="col-sm-2">
                                            <small><span><%# Eval("comment") %></span></small>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>




            <!--ฟอร์มวิว ALL (โอนย้าย)-->

            <div id="div_detailwaitTranfer" runat="server" visible="false" class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">รายละเอียดรายการที่รอการพิจารณา</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <div class="form-group">
                                <div class="col-sm-2 control-label">
                                    <asp:Label ID="Label4" runat="server" Visible="false" Text="รอการอนุมัติ" Font-Bold="true"></asp:Label>
                                    <asp:Label ID="Label5" runat="server" Visible="false" Text="รายการที่อนุมัติ" Font-Bold="true"></asp:Label>
                                </div>
                                <%--<label class="col-sm-2 control-label">เครื่องมือที่ต้องโอนย้าย</label>--%>
                                <%--<div class="col-sm-12">--%>
                                <%--<div class="col-sm-2"></div>--%>
                                <div class="col-sm-12">
                                    <asp:GridView ID="gvDetailWaitTranfer" runat="server"
                                        AutoGenerateColumns="False" HeaderStyle-Font-Size="10"
                                        CssClass="table table-striped table-bordered table-responsive"
                                        OnRowDataBound="gvRowDataBound"
                                        DataKeyNames="u1_device_idx">
                                        <HeaderStyle CssClass="default" Height="30px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="รายละเอียดเครื่องมือ" HeaderStyle-Width="25%">
                                                <ItemTemplate>

                                                    <asp:Label ID="DetailDevices_wait" runat="server"> 
                                                        <p><b>รหัสเครื่องมือ :</b> <%# Eval("device_id_no") %></p> 
                                                        <p><b>ชื่อเครื่องมือ :</b> <%# Eval("equipment_name") %></p>
                                                        <p><b>หมายเลขเครื่อง :</b> <%# Eval("device_serial") %></p>
                                                        <p><b>ผู้ถือครอง :</b> <%# Eval("sec_name_th") %></p>
                                                        <p><b>สถานที่ :</b> <%# Eval("place_name") %></p>
                                                        <p><b>ช่วงการใช้งาน :</b> <%# Eval("range_of_use") %></p>
                                                        <p><b>จุดการใช้งาน :</b> <%# Eval("point_of_use") %></p>
                                                    </asp:Label>

                                                    <%--                                                    <asp:Label ID="lblEquipment_IDNO_success" runat="server" Visible="false" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipment_Name_success" runat="server" Visible="false" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipmentSerail_success" runat="server" Visible="false" Text='<%# Eval("device_serial") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipmentRsection_success" runat="server" Visible="false" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                    <asp:Label ID="lblReceiverEquipmentRsection_success" runat="server" Visible="false" Text='<%# Eval("tranfer_name") %>'></asp:Label>
                                                    <asp:Label ID="lbEquipmentDetail_success" runat="server" Text='<%#"<p><b>" + "รหัสเครื่องมือ : " + "</b>" + Eval("device_id_no")
                                                            + "</p><br>" + "<p><b>" + "ชื่อเครื่องมือ : " + "</b>" + Eval("equipment_name") + "</p><br>" + "<p><b>" +  "หมายเลขเครื่อง : "
                                                            + "</b>" + Eval("device_serial") + "</p><br>" + "<p><b>" + "ผู้ถือครอง : " + "</b>" + Eval("sec_name_th")                                                           
                                                            + "</p><br>" + "<p><b>" + "ช่วงการใช้งาน : " + "</b>" + Eval("range_of_use") + "</p>"
                                                            + "</p><br>" + "<p><b>" + "จุดการใช้งาน : " + "</b>" + Eval("point_of_use") + "</p>"%>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ผู้รับ" HeaderStyle-Width="30%">
                                                <ItemTemplate>
                                                    <asp:Label ID="DetailProduction_wait" runat="server"> 
                                                        <p><b>ผู้รับ :</b> <%# Eval("tranfer_name") %></p>  
                                                        <p><b>สถานที่ :</b> <%# Eval("place_name_tranfer") %></p>
                                                    </asp:Label>
                                                    <%--<asp:Label ID="lbltranfer_name_success" runat="server" Enabled="false" Text='<%# Eval("tranfer_name") %>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="การดำเนินการ" HeaderStyle-Width="30%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstatus_idx_wait" runat="server" Visible="false" Enabled="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                                    <asp:Label ID="lblstatusname_wait" runat="server" Enabled="false" Text='<%# Eval("status_name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Width="33%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcomment_qacutoff" runat="server" Enabled="false" Text='<%# Eval("comment") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                        </Columns>
                                    </asp:GridView>
                                </div>



                            </div>
                        </div>
                    </div>

                </div>


            </div>

            <div id="divFVVAllTranfer" runat="server" visible="false" class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">พิจารณาการโอนย้าย</h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <div class="form-group" id="show_detail_devices_approve" runat="server">
                                <label class="col-sm-2 control-label">เครื่องมือที่โอนย้าย</label>
                                <div class="col-sm-10">
                                    <asp:CheckBox ID="CheckAll_HeadApproveTranfer" runat="server" Width="100%" Text="อนุมัติทั้งหมด" OnCheckedChanged="checkindexchange" AutoPostBack="true" />
                                    <asp:CheckBox ID="CheckAll_HeadNotApproveTranfer" runat="server" Text="ไม่อนุมัติทั้งหมด" Width="100%" OnCheckedChanged="checkindexchange" AutoPostBack="true" />
                                </div>
                                <div class="col-sm-2">
                                    <asp:Label ID="lblDecisionHeadTranferView_not" Visible="false" runat="server"></asp:Label>
                                </div>
                                <div class="col-sm-2">
                                    <asp:Label ID="lblDecisionHeadTranferView_approve" Visible="false" runat="server"></asp:Label>
                                </div>

                                <div class="col-sm-2">
                                    <asp:Label ID="lblDecisionHeadReceiveTranferView_not" Visible="false" runat="server"></asp:Label>
                                </div>
                                <div class="col-sm-2">
                                    <asp:Label ID="lblDecisionHeadReceiveTranferView_approve" Visible="false" runat="server"></asp:Label>
                                </div>
                            </div>
                            <div class="form-group" id="show_detail_devices_notapprove" visible="false" runat="server">
                                <%--   <label class="col-sm-2 control-label"></label>--%>
                                <%-- <div class="col-sm-2">--%>
                                <%-- <asp:CheckBox ID="CheckAll_HeadNotApproveTranfer" runat="server" Text="ไม่อนุมัติทั้งหมด" OnCheckedChanged="checkindexchange" AutoPostBack="true" />--%>
                                <%-- </div>--%>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-2 control-label">
                                    <asp:Label ID="lbtranferWaiting" runat="server" Visible="false" Text="รอการอนุมัติ" Font-Bold="true"></asp:Label>
                                    <asp:Label ID="lbltranferApprove" runat="server" Visible="false" Text="รายการที่อนุมัติ" Font-Bold="true"></asp:Label>
                                </div>
                                <%--<label class="col-sm-2 control-label">เครื่องมือที่ต้องโอนย้าย</label>--%>
                                <%--<div class="col-sm-12">--%>
                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <asp:GridView ID="gvEquipmentAllTranfer" runat="server"
                                        AutoGenerateColumns="False" HeaderStyle-Font-Size="10"
                                        CssClass="table table-striped table-bordered table-responsive"
                                        OnRowDataBound="gvRowDataBound"
                                        DataKeyNames="u1_device_idx">
                                        <HeaderStyle CssClass="success" Height="30px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderStyle-Width="15%">
                                                <ItemTemplate>

                                                    <asp:RadioButtonList ID="rbl_HeadApproveTranfer" CellPadding="3" Font-Size="10" CellSpacing="2" Width="100%" AutoPostBack="true"
                                                        RepeatDirection="Vertical" OnSelectedIndexChanged="radioCheckChange" runat="server">
                                                        <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                                        <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                                    </asp:RadioButtonList>
                                                    <asp:Label ID="lblDeviceIDX_headtranfer" runat="server" Visible="false" Text='<%# Eval("u1_device_idx") %>'></asp:Label>
                                                    <asp:Label ID="lblu0_device_idx_headtranfer" runat="server" Visible="false" Text='<%# Eval("u0_device_idx") %>'></asp:Label>
                                                    <asp:Label ID="lblm0_actor_idx_headtranfer" runat="server" Visible="false" Text='<%# Eval("m0_actor_idx") %>'></asp:Label>
                                                    <asp:Label ID="lblm0_node_idx_headtranfer" runat="server" Visible="false" Text='<%# Eval("m0_node_idx") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="รายละเอียดเครื่องมือ" HeaderStyle-Width="25%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEquipment_IDNO_headtranfer" runat="server" Visible="false" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipment_Name_headtranfer" runat="server" Visible="false" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipmentSerail_headtranfer" runat="server" Visible="false" Text='<%# Eval("device_serial") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipmentRsection_headtranfer" runat="server" Visible="false" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                    <asp:Label ID="lblReceiverEquipmentRsection_headtranfer" runat="server" Visible="false" Text='<%# Eval("tranfer_name") %>'></asp:Label>
                                                    <asp:Label ID="lbEquipmentDetail_headtranfer" runat="server" Visible="false" Text='<%#"<p><b>" + "รหัสเครื่องมือ : " + "</b>" + Eval("device_id_no")
                                                            + "</p><br>" + "<p><b>" + "ชื่อเครื่องมือ : " + "</b>" + Eval("equipment_name") + "</p><br>" + "<p><b>" +  "หมายเลขเครื่อง : "
                                                            + "</b>" + Eval("device_serial") + "</p><br>" + "<p><b>" + "ผู้ถือครอง : " + "</b>" + Eval("sec_name_th") 
                                                            + "</p><br>"+ "<p><b>" + "ผู้รับ : " + "</b>" + Eval("tranfer_name") + "</p>"
                                                            + "</p><br>" + "<p><b>" + "ช่วงการใช้งาน : " + "</b>" + Eval("range_of_use") + "</p>"
                                                            + "</p><br>" + "<p><b>" + "จุดการใช้งาน : " + "</b>" + Eval("point_of_use") + "</p>"%>'></asp:Label>


                                                    <asp:Label ID="Detail_DecisionApprove" runat="server"> 
                                                        <p><b>รหัสเครื่องมือ :</b> <%# Eval("device_id_no") %></p>
                                                        <p><b>ชื่อเครื่องมือ :</b> <%# Eval("equipment_name") %></p>
                                                        <p><b>หมายเลขเครื่อง :</b> <%# Eval("device_serial") %></p>
                                                        <p><b>ผู้ถือครอง :</b> <%# Eval("sec_name_th") %></p>
                                                        <p><b>สถานที่  :</b> <%# Eval("place_name") %></p>
                                                        <p><b>ช่วงการใช้งาน  :</b> <%# Eval("range_of_use") %></p>
                                                        <p><b>จุดการใช้งาน  :</b> <%# Eval("point_of_use") %></p>

                                                    </asp:Label>



                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ผู้รับ" HeaderStyle-Width="25%">
                                                <ItemTemplate>
                                                    <asp:Label ID="Detail_Receive_Tranfer" runat="server"> 
                                                        <p><b>ผู้รับ :</b> <%# Eval("tranfer_name") %></p>
                                                        <p><b>สถานที่  :</b> <%# Eval("place_name_tranfer") %></p>

                                                    </asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Width="20%">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtcomment_headtranfer" runat="server" TextMode="MultiLine" placeholder="กรอกหมายเหตุ.." Rows="3" CssClass="form-control multiline-no-resize" Enabled="true" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="การดำเนินการ" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstatus_idx_tran" runat="server" Visible="false" Enabled="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                                    <asp:Label ID="lblstatusname" runat="server" Enabled="false" Text='<%# Eval("status_name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Width="33%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcomment_qacutoff" runat="server" Enabled="false" Text='<%# Eval("comment") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                        </Columns>
                                    </asp:GridView>
                                </div>

                                <div class="col-sm-2"></div>
                                <div class="col-sm-10">
                                    <asp:LinkButton ID="btnHeadUserSubmitQATranfer" CssClass="btn btn-success" runat="server" data-original-title="Submit" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandName="cmdHeadSubmitTranfer" OnClientClick="return confirm('คุณต้องการดำเนินรายการโอนย้ายเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>

                                    <asp:LinkButton ID="btnHeadAceptTranfer" CssClass="btn btn-success" runat="server" data-original-title="Submit" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandName="cmdHeadAceptSubmitTranfer" OnClientClick="return confirm('คุณต้องการดำเนินรายการโอนย้ายเครื่องมือนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2 control-label">
                                        <asp:Label ID="lbltranferNotApprove" runat="server" Visible="false" Text="รายการที่ไม่อนุมัติ" Font-Bold="true"></asp:Label>
                                    </div>
                                    <%--<label class="col-sm-2 control-label">เครื่องมือที่ต้องโอนย้าย</label>--%>
                                    <div class="col-sm-10">
                                        <asp:GridView ID="gvEquipmentAllTranferNA" runat="server" BackColor="Transparent"
                                            AutoGenerateColumns="False" HeaderStyle-Font-Size="9"
                                            CssClass="table table-hover table-bordered table-condensed table-responsive"
                                            OnRowDataBound="gvRowDataBound"
                                            DataKeyNames="u1_device_idx">
                                            <HeaderStyle CssClass="default" Height="20px" Font-Size="Small" />
                                            <RowStyle Font-Size="Small" />
                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                            <EmptyDataTemplate>
                                                <div style="text-align: center">ไม่พบข้อมูล</div>
                                            </EmptyDataTemplate>
                                            <Columns>
                                                <asp:TemplateField HeaderText="รหัสเครื่อง" HeaderStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEquipment_IDNO" runat="server" Enabled="false" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ชื่อเครื่องมือ" HeaderStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEquipment_Name" runat="server" Enabled="false" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="หมายเลขเครื่อง" HeaderStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEquipmentSerail" runat="server" Enabled="false" Text='<%# Eval("device_serial") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ผู้ถือครอง" HeaderStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEquipmentRsection" runat="server" Enabled="false" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ผู้รับ" HeaderStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReceiverEquipmentRsection" runat="server" Enabled="false" Text='<%# Eval("tranfer_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--<asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Width="33%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcomment_qacutoff" runat="server" Enabled="false" Text='<%# Eval("comment") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="div_successTranfer" runat="server" visible="false" class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">รายละเอียดรายการโอนย้ายเครื่องมือ</h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <asp:GridView ID="gvSucessTranfer" runat="server"
                                        AutoGenerateColumns="False" HeaderStyle-Font-Size="10"
                                        CssClass="table table-striped table-bordered table-responsive"
                                        OnRowDataBound="gvRowDataBound"
                                        DataKeyNames="u1_device_idx">
                                        <HeaderStyle CssClass="success" Height="30px" Font-Size="Small" />
                                        <RowStyle Font-Size="Small" />
                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                        <EmptyDataTemplate>
                                            <div style="text-align: center">ไม่พบข้อมูล</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="รายละเอียดเครื่องมือ" HeaderStyle-Width="25%">
                                                <ItemTemplate>

                                                    <asp:Label ID="DetailDevices_Sucess" runat="server"> 
                                                        <p><b>รหัสเครื่องมือ :</b> <%# Eval("device_id_no") %></p> 
                                                        <p><b>ชื่อเครื่องมือ :</b> <%# Eval("equipment_name") %></p>
                                                        <p><b>หมายเลขเครื่อง :</b> <%# Eval("device_serial") %></p>
                                                        <p><b>ผู้ถือครอง :</b> <%# Eval("sec_name_th") %></p>
                                                        <p><b>สถานที่ :</b> <%# Eval("place_name") %></p>
                                                        <p><b>ช่วงการใช้งาน :</b> <%# Eval("range_of_use") %></p>
                                                        <p><b>จุดการใช้งาน :</b> <%# Eval("point_of_use") %></p>
                                                    </asp:Label>

                                                    <%--                                                    <asp:Label ID="lblEquipment_IDNO_success" runat="server" Visible="false" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipment_Name_success" runat="server" Visible="false" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipmentSerail_success" runat="server" Visible="false" Text='<%# Eval("device_serial") %>'></asp:Label>
                                                    <asp:Label ID="lblEquipmentRsection_success" runat="server" Visible="false" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                                                    <asp:Label ID="lblReceiverEquipmentRsection_success" runat="server" Visible="false" Text='<%# Eval("tranfer_name") %>'></asp:Label>
                                                    <asp:Label ID="lbEquipmentDetail_success" runat="server" Text='<%#"<p><b>" + "รหัสเครื่องมือ : " + "</b>" + Eval("device_id_no")
                                                            + "</p><br>" + "<p><b>" + "ชื่อเครื่องมือ : " + "</b>" + Eval("equipment_name") + "</p><br>" + "<p><b>" +  "หมายเลขเครื่อง : "
                                                            + "</b>" + Eval("device_serial") + "</p><br>" + "<p><b>" + "ผู้ถือครอง : " + "</b>" + Eval("sec_name_th")                                                           
                                                            + "</p><br>" + "<p><b>" + "ช่วงการใช้งาน : " + "</b>" + Eval("range_of_use") + "</p>"
                                                            + "</p><br>" + "<p><b>" + "จุดการใช้งาน : " + "</b>" + Eval("point_of_use") + "</p>"%>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ผู้รับ" HeaderStyle-Width="30%">
                                                <ItemTemplate>
                                                    <asp:Label ID="DetailProduction" runat="server"> 
                                                        <p><b>ผู้รับ :</b> <%# Eval("tranfer_name") %></p>  
                                                         <p><b>สถานที่ :</b> <%# Eval("place_name_tranfer") %></p>
                                                    </asp:Label>
                                                    <%--<asp:Label ID="lbltranfer_name_success" runat="server" Enabled="false" Text='<%# Eval("tranfer_name") %>'></asp:Label>--%>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="การดำเนินการ" HeaderStyle-Width="30%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblstatus_idx_success" runat="server" Visible="false" Enabled="false" Text='<%# Eval("staidx") %>'></asp:Label>
                                                    <asp:Label ID="lblstatusname_success" runat="server" Enabled="false" Text='<%# Eval("status_name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Width="33%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcomment_qacutoff" runat="server" Enabled="false" Text='<%# Eval("comment") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                        </Columns>
                                    </asp:GridView>

                                </div>

                            </div>


                        </div>
                    </div>

                </div>
            </div>


            <div id="div_LogTranfer_wait" runat="server" visible="false" class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <b>Transfer Device Log</b> (ประวัติการดำเนินการโอนย้ายเครื่องมือ)
                    </div>
                    <div class="panel-body">
                        <asp:Repeater ID="rptLogTranfer" runat="server">
                            <HeaderTemplate>
                                <div class="row">
                                    <label class="col-sm-2 control-label"><small>วันที่ดำเนินการ</small></label>
                                    <label class="col-sm-2 control-label"><small>เวลาที่ดำเนินการ</small></label>
                                    <label class="col-sm-2 control-label"><small>ผู้ดำเนินการ</small></label>
                                    <label class="col-sm-4 control-label"><small>ดำเนินการ</small></label>
                                    <label class="col-sm-2 control-label"><small>หมายเหตุ</small></label>
                                </div>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <small><span><%# Eval("create_date") %></span></small>
                                    </div>
                                    <div class="col-sm-2">
                                        <small><span><%# Eval("create_time") %></span></small>
                                    </div>
                                    <div class="col-sm-2">
                                        <small><span><%# Eval("cemp_name") %></span></small>
                                    </div>
                                    <div class="col-sm-4">
                                        <small><span><%# Eval("decision_desc") %></span></small>
                                    </div>
                                    <div class="col-sm-2">
                                        <small><span><%# Eval("comment") %></span></small>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>

            <!--ฝากค่า-->
            <div id="divPassU1" runat="server" visible="false" class="col-md-12">
                <asp:GridView ID="gvPassU1" runat="server" BackColor="Transparent"
                    AutoGenerateColumns="False" HeaderStyle-Font-Size="9"
                    CssClass="table table-hover table-bordered table-condensed table-responsive"
                    OnRowDataBound="gvRowDataBound"
                    DataKeyNames="u1_device_idx">
                    <HeaderStyle CssClass="default" Height="20px" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="U0IDX" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <asp:Label ID="lblEquipment_U0IDX" runat="server" Enabled="false" Text='<%# Eval("u0_device_idx") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="U1IDX" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <asp:Label ID="lblEquipment_U1IDX" runat="server" Enabled="false" Text='<%# Eval("u1_device_idx") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Staidx" HeaderStyle-Width="5%">
                            <ItemTemplate>
                                <asp:Label ID="lblEquipment_Staidx" runat="server" Enabled="false" Text='<%# Eval("staidx") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รหัสเครื่อง" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblEquipment_IDNO" runat="server" Enabled="false" Text='<%# Eval("device_id_no") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ชื่อเครื่องมือ" HeaderStyle-Width="20%">
                            <ItemTemplate>
                                <asp:Label ID="lblEquipment_Name" runat="server" Enabled="false" Text='<%# Eval("equipment_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="หมายเลขเครื่อง" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblEquipmentSerail" runat="server" Enabled="false" Text='<%# Eval("device_serial") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ผู้ถือครอง" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblEquipmentRsection" runat="server" Enabled="false" Text='<%# Eval("sec_name_th") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ผู้รับ" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="lblReceiverEquipmentRsection" runat="server" Enabled="false" Text='<%# Eval("tranfer_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-Width="33%">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcomment_qacutoff" runat="server" Enabled="false" Text='<%# Eval("comment") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>

        <asp:View ID="docReport" runat="server">

            <asp:Panel ID="div_SearchReportOnline" runat="server" class="col-md-12" Visible="false">
                <div class="form-group">
                    <div class="panel panel-success">
                        <div class="panel-heading f-bold">ค้นหารายการเครื่องมือที่ถูกใช้งาน</div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <%-- <div class="form-group">
                                    <label class="col-sm-2 control-label">รหัสเครื่องมือ :</label>
                                    <div class="col-sm-4">
                                       
                                        <asp:TextBox ID="txt_devices_no_report" placeholder="รหัสเครื่องมือ ..." AutoPostBack="true" runat="server" CssClass="form-control" ReadOnly="false">
                                        </asp:TextBox>
                                        <asp:RequiredFieldValidator ID="Requiredtxt_devices_no_report"
                                            runat="server" ValidationGroup="SearchReport"
                                            Display="None"
                                            SetFocusOnError="true"
                                            ControlToValidate="txt_devices_no_report"
                                            Font-Size="13px" ForeColor="Red"
                                            ErrorMessage="*กรุณากรอกรหัสเครื่องมือ" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatortxt_devices_no_report"
                                            TargetControlID="Requiredtxt_devices_no_report" Width="220" />
                                    </div>--%>

                                <%--  <label class="col-sm-2 control-label">สถานะเครื่องมือ :</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddl_statusdevices" CssClass="form-control" runat="server">
                                           
                                        </asp:DropDownList>
                                        
                                        <asp:RequiredFieldValidator ID="Requiredddl_statusdevices"
                                            runat="server" ValidationGroup="SearchReport"
                                            Display="None"
                                            SetFocusOnError="true" InitialValue="0"
                                            ControlToValidate="ddl_statusdevices"
                                            Font-Size="13px" ForeColor="Red"
                                            ErrorMessage="*กรุณาเลือกสถานะ" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRequiredddl_statusdevices" runat="Server" PopupPosition="BottomLeft" HighlightCssClass="validatorddl_statusdevices"
                                            TargetControlID="Requiredddl_statusdevices" Width="220" />
                                    </div>--%>

                                <%--</div>--%>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">องค์กรผู้ถือครอง :</label>
                                    <div class="col-sm-4">
                                        <%--<div class='input-group date from-date-datepicker'>--%>
                                        <%-- <asp:HiddenField ID="HiddenSearchDateRecive" runat="server" />--%>
                                        <asp:DropDownList ID="ddl_orgidx_report" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldddl_orgidx_report"
                                            runat="server" ValidationGroup="SearchReport"
                                            Display="None"
                                            InitialValue="0"
                                            SetFocusOnError="true"
                                            ControlToValidate="ddl_orgidx_report"
                                            Font-Size="13px" ForeColor="Red"
                                            ErrorMessage="*กรุณาเลือกองค์กรผู้ถือครอง" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatortxt_devices_no_report"
                                            TargetControlID="RequiredFieldddl_orgidx_report" Width="220" />
                                    </div>

                                    <label class="col-sm-2 control-label">ฝ่ายผู้ถือครอง :</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddl_rdeptidx_report" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="Requiredddl_rdeptidx_report"
                                            runat="server" ValidationGroup="SearchReport"
                                            Display="None"
                                            SetFocusOnError="true" InitialValue="0"
                                            ControlToValidate="ddl_rdeptidx_report"
                                            Font-Size="13px" ForeColor="Red"
                                            ErrorMessage="*กรุณาเลือกฝ่ายผู้ถือครอง" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" PopupPosition="BottomLeft" HighlightCssClass="validatorddl_rdeptidx_report"
                                            TargetControlID="Requiredddl_rdeptidx_report" Width="220" />
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แผนกผู้ถือครอง :</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddl_rsecidx_report" CssClass="form-control" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="Requiredddl_rsecidx_report"
                                            runat="server"
                                            ValidationGroup="SearchReport"
                                            Display="None"
                                            SetFocusOnError="true" InitialValue="0"
                                            ControlToValidate="ddl_rsecidx_report"
                                            Font-Size="13px" ForeColor="Red"
                                            ErrorMessage="*กรุณาเลือกแผนกผู้ถือครอง" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorddl_rsecidx_report" TargetControlID="Requiredddl_rsecidx_report" Width="220" />
                                    </div>
                                    <label class="col-sm-6 control-label"></label>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                        <asp:LinkButton ID="btnSearchReport" CssClass="btn btn-primary" runat="server" ValidationGroup="searchprint"
                                            OnCommand="btnCommand" CommandName="cmdSearchReport" CommandArgument="1" Font-Size="Small" data-toggle="tooltip" title="Search"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                                        <asp:LinkButton ID="btnSearchCancel" CssClass="btn btn-default" runat="server" CommandArgument="1"
                                            OnCommand="btnCommand" CommandName="cmdSearchCancel" Font-Size="Small" data-toggle="tooltip" title="Reset"><span class="glyphicon glyphicon-refresh"></span></asp:LinkButton>
                                    </div>
                                    <label class="col-sm-6 control-label"></label>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <asp:GridView ID="gvReportDevicesOnline"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                        AllowPaging="false"
                        PageSize="10"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound">
                        <RowStyle Font-Size="Small" />
                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">Data Cannot Be Found</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <small>
                                        <%# (Container.DataItemIndex +1) %>
                                       
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานที่" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_placename_online" runat="server" Text='<%# Eval("place_name") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนรายการเครื่องมือที่ใช้งาน" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_countdevices_online" runat="server" Text='<%# Eval("count_devices") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnViewReportOnline" CssClass="btn-sm btn-info" runat="server" CommandName="cmdReportDevicesOnline" OnCommand="btnCommand" CommandArgument='<%# Eval("place_idx") + ";" + "1" %>' data-toggle="tooltip" title="Report"><i class="fa fa-file"></i></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>

                </div>

            </asp:Panel>

            <asp:Panel ID="div_ReportDetail_Online" runat="server" class="col-md-12" Visible="false">

                <asp:UpdatePanel ID="updatepanelbuttonSearch" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="panelBtnExportExcelOnline" runat="server" CssClass="pull-right m-b-10 hidden-print">
                            <asp:LinkButton ID="btnExportExcelOnline" CssClass="btn btn-primary" runat="server"
                                Text="Export To Excel" OnCommand="btnCommand" CommandName="cmdExportExcelOnline" />
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExportExcelOnline" />

                    </Triggers>
                </asp:UpdatePanel>

                <asp:LinkButton ID="btnBackToReportOnline" runat="server" CssClass="btn btn-danger"
                    Text='<i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ' OnCommand="btnCommand" CommandName="cmdBackToReport" CommandArgument="1" />

                <div class="panel panel-default m-t-10">
                    <div class="panel-body">
                        <h3 class="text-center">
                            <asp:Label ID="lblDateOrderSale" Text="รายงานเครื่องมือที่ใช้งาน" runat="server" /></h3>
                        <asp:GridView ID="gvReportDetailOnline"
                            runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                            AllowPaging="false"
                            PageSize="10"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound">
                            <RowStyle Font-Size="Small" />
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <small>
                                            <%# (Container.DataItemIndex +1) %>
                                       
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ชื่อเครื่องมือ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_equipment_name_reportonline" runat="server" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รหัสเครื่องมือ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_device_id_no_reportonline" runat="server" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="หมายเลขเครื่อง" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_device_serial_reportonline" runat="server" Text='<%# Eval("device_serial") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ผู้ถือครอง" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_device_rsec_reportonline" runat="server" Text='<%# Eval("device_rsec") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="วันที่สอบเทียบครั้งล่าสุด" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_device_cal_date_reportonline" runat="server" Text='<%# Eval("device_cal_date") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="วันที่สอบเทียบครั้งถัดไป" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_device_due_date_reportonline" runat="server" Text='<%# Eval("device_due_date") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%--<asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnViewReportOnline" CssClass="btn-sm btn-info" runat="server" CommandName="cmdReportDevicesOnline" OnCommand="btnCommand" CommandArgument='<%# Eval("place_idx") + ";" + "1" %>' data-toggle="tooltip" title="Report"><i class="fa fa-file"></i></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            </Columns>

                        </asp:GridView>
                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="div_SearchReportOffline" runat="server" class="col-md-12" Visible="false">
                <div class="form-group">
                    <div class="panel panel-success">
                        <div class="panel-heading f-bold">ค้นหารายการเครื่องมือที่ไม่ถูกใช้งาน</div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">องค์กรผู้ถือครอง :</label>
                                    <div class="col-sm-4">
                                        <%--<div class='input-group date from-date-datepicker'>--%>
                                        <%-- <asp:HiddenField ID="HiddenSearchDateRecive" runat="server" />--%>
                                        <asp:DropDownList ID="ddl_orgidx_report_offline" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldddl_orgidx_report_offline"
                                            runat="server" ValidationGroup="SearchReport"
                                            Display="None"
                                            InitialValue="0"
                                            SetFocusOnError="true"
                                            ControlToValidate="ddl_orgidx_report_offline"
                                            Font-Size="13px" ForeColor="Red"
                                            ErrorMessage="*กรุณาเลือกองค์กรผู้ถือครอง" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatortxt_devices_no_report"
                                            TargetControlID="RequiredFieldddl_orgidx_report_offline" Width="220" />
                                    </div>

                                    <label class="col-sm-2 control-label">ฝ่ายผู้ถือครอง :</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddl_rdeptidx_report_offline" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldddl_rdeptidx_report_offline"
                                            runat="server" ValidationGroup="SearchReport"
                                            Display="None"
                                            SetFocusOnError="true" InitialValue="0"
                                            ControlToValidate="ddl_rdeptidx_report_offline"
                                            Font-Size="13px" ForeColor="Red"
                                            ErrorMessage="*กรุณาเลือกฝ่ายผู้ถือครอง" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" PopupPosition="BottomLeft" HighlightCssClass="validatorddl_rdeptidx_report"
                                            TargetControlID="RequiredFieldddl_rdeptidx_report_offline" Width="220" />
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แผนกผู้ถือครอง :</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddl_rsecidx_report_offline" CssClass="form-control" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFielddl_rsecidx_report_offline"
                                            runat="server"
                                            ValidationGroup="SearchReport"
                                            Display="None"
                                            SetFocusOnError="true" InitialValue="0"
                                            ControlToValidate="ddl_rsecidx_report_offline"
                                            Font-Size="13px" ForeColor="Red"
                                            ErrorMessage="*กรุณาเลือกแผนกผู้ถือครอง" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorddl_rsecidx_report" TargetControlID="RequiredFielddl_rsecidx_report_offline" Width="220" />
                                    </div>
                                    <label class="col-sm-6 control-label"></label>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                        <asp:LinkButton ID="btnSearchDevicesOffline" CssClass="btn btn-primary" runat="server" ValidationGroup="searchprint"
                                            OnCommand="btnCommand" CommandName="cmdSearchReport" CommandArgument="2" Font-Size="Small" data-toggle="tooltip" title="Search"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                                        <asp:LinkButton ID="btnSearchCancelOffline" CssClass="btn btn-default" runat="server"
                                            OnCommand="btnCommand" CommandName="cmdSearchCancel" Font-Size="Small" CommandArgument="2" data-toggle="tooltip" title="Reset"><span class="glyphicon glyphicon-refresh"></span></asp:LinkButton>
                                    </div>
                                    <label class="col-sm-6 control-label"></label>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <asp:GridView ID="gvReportDevicesOffline"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                        AllowPaging="false"
                        PageSize="10"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound">
                        <RowStyle Font-Size="Small" />
                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">Data Cannot Be Found</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <small>
                                        <%# (Container.DataItemIndex +1) %>
                                       
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานที่" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_placename_offline" runat="server" Text='<%# Eval("place_name") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนรายการเครื่องมือที่ไม่ถูกใช้งาน" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_countdevices_offline" runat="server" Text='<%# Eval("count_devices") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnViewReportOffline" CssClass="btn-sm btn-info" runat="server" CommandName="cmdReportDevicesOnline" OnCommand="btnCommand" CommandArgument='<%# Eval("place_idx") + ";" + "2" %>' data-toggle="tooltip" title="Report"><i class="fa fa-file"></i></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>

                </div>

            </asp:Panel>

            <asp:Panel ID="div_ReportDetail_Offline" runat="server" class="col-md-12" Visible="false">

                <asp:UpdatePanel ID="updatepanelbuttonoffline" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="panelBtnExportExcelOffline" runat="server" CssClass="pull-right m-b-10 hidden-print">
                            <asp:LinkButton ID="btnExportExcelOffline" CssClass="btn btn-primary" runat="server"
                                Text="Export To Excel" OnCommand="btnCommand" CommandName="cmdExportExcelOffline" />
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExportExcelOffline" />

                    </Triggers>
                </asp:UpdatePanel>

                <asp:LinkButton ID="btnBackToReportOffline" runat="server" CssClass="btn btn-danger"
                    Text='<i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ' OnCommand="btnCommand" CommandName="cmdBackToReport" CommandArgument="2" />

                <div class="panel panel-default m-t-10">
                    <div class="panel-body">
                        <h3 class="text-center">
                            <asp:Label ID="Label2" Text="รายงานเครื่องมือที่ไม่ถูกใช้งาน" runat="server" /></h3>

                        <asp:GridView ID="gvReportDetailOffline"
                            runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                            AllowPaging="false"
                            PageSize="10"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound">
                            <RowStyle Font-Size="Small" />
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <small>
                                            <%# (Container.DataItemIndex +1) %>
                                       
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ชื่อเครื่องมือ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_equipment_name_reportoffline" runat="server" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รหัสเครื่องมือ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_device_id_no_reportoffline" runat="server" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="หมายเลขเครื่อง" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_device_serial_reportoffline" runat="server" Text='<%# Eval("device_serial") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ผู้ถือครอง" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_device_rsec_reportoffline" runat="server" Text='<%# Eval("device_rsec") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="วันที่สอบเทียบครั้งล่าสุด" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_device_cal_date_reportoffline" runat="server" Text='<%# Eval("device_cal_date") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="วันที่สอบเทียบครั้งถัดไป" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_device_due_date_reportoffline" runat="server" Text='<%# Eval("device_due_date") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%--<asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnViewReportOnline" CssClass="btn-sm btn-info" runat="server" CommandName="cmdReportDevicesOnline" OnCommand="btnCommand" CommandArgument='<%# Eval("place_idx") + ";" + "1" %>' data-toggle="tooltip" title="Report"><i class="fa fa-file"></i></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            </Columns>

                        </asp:GridView>


                    </div>
                </div>
            </asp:Panel>

            <asp:Panel ID="div_SearchReportCutoff" runat="server" class="col-md-12" Visible="false">
                <div class="form-group">
                    <div class="panel panel-success">
                        <div class="panel-heading f-bold">ค้นหารายการเครื่องมือที่ตัดเสีย</div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">องค์กรผู้ถือครอง :</label>
                                    <div class="col-sm-4">
                                        <%--<div class='input-group date from-date-datepicker'>--%>
                                        <%-- <asp:HiddenField ID="HiddenSearchDateRecive" runat="server" />--%>
                                        <asp:DropDownList ID="ddl_orgidx_report_cutoff" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="Requiredddl_orgidx_report_cutoff"
                                            runat="server" ValidationGroup="SearchReport"
                                            Display="None"
                                            InitialValue="0"
                                            SetFocusOnError="true"
                                            ControlToValidate="ddl_orgidx_report_cutoff"
                                            Font-Size="13px" ForeColor="Red"
                                            ErrorMessage="*กรุณาเลือกองค์กรผู้ถือครอง" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatortxt_devices_no_report"
                                            TargetControlID="Requiredddl_orgidx_report_cutoff" Width="220" />
                                    </div>

                                    <label class="col-sm-2 control-label">ฝ่ายผู้ถือครอง :</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddl_rdeptidx_report_cutoff" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldddl_rdeptidx_report_cutoff"
                                            runat="server" ValidationGroup="SearchReport"
                                            Display="None"
                                            SetFocusOnError="true" InitialValue="0"
                                            ControlToValidate="ddl_rdeptidx_report_cutoff"
                                            Font-Size="13px" ForeColor="Red"
                                            ErrorMessage="*กรุณาเลือกฝ่ายผู้ถือครอง" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server" PopupPosition="BottomLeft" HighlightCssClass="validatorddl_rdeptidx_report"
                                            TargetControlID="RequiredFieldddl_rdeptidx_report_cutoff" Width="220" />
                                    </div>

                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">แผนกผู้ถือครอง :</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddl_rsecidx_report_cutoff" CssClass="form-control" runat="server"></asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldddl_rsecidx_report_cutoff"
                                            runat="server"
                                            ValidationGroup="SearchReport"
                                            Display="None"
                                            SetFocusOnError="true" InitialValue="0"
                                            ControlToValidate="ddl_rsecidx_report_cutoff"
                                            Font-Size="13px" ForeColor="Red"
                                            ErrorMessage="*กรุณาเลือกแผนกผู้ถือครอง" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorddl_rsecidx_report" TargetControlID="RequiredFieldddl_rsecidx_report_cutoff" Width="220" />
                                    </div>
                                    <label class="col-sm-6 control-label"></label>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-4">
                                        <asp:LinkButton ID="btnSearchDevicesCutoff" CssClass="btn btn-primary" CommandArgument="3" runat="server" ValidationGroup="searchprint"
                                            OnCommand="btnCommand" CommandName="cmdSearchReport" Font-Size="Small" data-toggle="tooltip" title="Search"><span class="glyphicon glyphicon-search"></span></asp:LinkButton>
                                        <asp:LinkButton ID="btnSearchCancelCutoff" CssClass="btn btn-default" runat="server" CommandArgument="3"
                                            OnCommand="btnCommand" CommandName="cmdSearchCancel" Font-Size="Small" data-toggle="tooltip" title="Reset"><span class="glyphicon glyphicon-refresh"></span></asp:LinkButton>
                                    </div>
                                    <label class="col-sm-6 control-label"></label>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <asp:GridView ID="gvReportDevicesCutoff"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                        AllowPaging="false"
                        PageSize="10"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound">
                        <RowStyle Font-Size="Small" />
                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">Data Cannot Be Found</div>
                        </EmptyDataTemplate>
                        <Columns>
                            <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <small>
                                        <%# (Container.DataItemIndex +1) %>
                                       
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานที่" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_placename_cutoff" runat="server" Text='<%# Eval("place_name") %>'></asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวนรายการเครื่องมือที่ตัดเสีย" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_countdevices_cutoff" runat="server" Text='<%# Eval("count_devices") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnViewReportCutoff" CssClass="btn-sm btn-info" runat="server" CommandName="cmdReportDevicesOnline" OnCommand="btnCommand" CommandArgument='<%# Eval("place_idx") + ";" + "3" %>' data-toggle="tooltip" title="Report"><i class="fa fa-file"></i></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>

                </div>

            </asp:Panel>

            <asp:Panel ID="div_ReportDetail_Cutoff" runat="server" class="col-md-12" Visible="false">

                <asp:UpdatePanel ID="updatepanelbuttoncutoff" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="panelBtnExportExcelCutoff" runat="server" CssClass="pull-right m-b-10 hidden-print">
                            <asp:LinkButton ID="btnExportExcelCutoff" CssClass="btn btn-primary" runat="server"
                                Text="Export To Excel" OnCommand="btnCommand" CommandName="cmdExportExcelCutoff" />
                        </asp:Panel>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExportExcelCutoff" />

                    </Triggers>
                </asp:UpdatePanel>

                <asp:LinkButton ID="btnBackToReportCutoff" runat="server" CssClass="btn btn-danger"
                    Text='<i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ' OnCommand="btnCommand" CommandName="cmdBackToReport" CommandArgument="3" />

                <div class="panel panel-default m-t-10">
                    <div class="panel-body">
                        <h3 class="text-center">
                            <asp:Label ID="lbl_report_cutoff" Text="รายงานเครื่องมือที่ตัดเสีย" runat="server" /></h3>

                        <asp:GridView ID="gvReportDetailCutOff"
                            runat="server"
                            AutoGenerateColumns="false"
                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                            AllowPaging="false"
                            PageSize="10"
                            OnPageIndexChanging="gvPageIndexChanging"
                            OnRowDataBound="gvRowDataBound">
                            <RowStyle Font-Size="Small" />
                            <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                            <EmptyDataTemplate>
                                <div style="text-align: center">Data Cannot Be Found</div>
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <small>
                                            <%# (Container.DataItemIndex +1) %>
                                       
                                        </small>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ชื่อเครื่องมือ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_equipment_name_reportcutoff" runat="server" Text='<%# Eval("equipment_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="รหัสเครื่องมือ" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_device_id_no_reportcutoff" runat="server" Text='<%# Eval("device_id_no") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="หมายเลขเครื่อง" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_device_serial_reportcutoff" runat="server" Text='<%# Eval("device_serial") %>'></asp:Label>

                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="ผู้ถือครอง" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_device_rsec_reportcutoff" runat="server" Text='<%# Eval("device_rsec") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="วันที่สอบเทียบครั้งล่าสุด" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_device_cal_date_reportcutoff" runat="server" Text='<%# Eval("device_cal_date") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="วันที่สอบเทียบครั้งถัดไป" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Label ID="lbl_device_due_date_reportcutoff" runat="server" Text='<%# Eval("device_due_date") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <%--<asp:TemplateField HeaderText="Action" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:LinkButton ID="btnViewReportOnline" CssClass="btn-sm btn-info" runat="server" CommandName="cmdReportDevicesOnline" OnCommand="btnCommand" CommandArgument='<%# Eval("place_idx") + ";" + "1" %>' data-toggle="tooltip" title="Report"><i class="fa fa-file"></i></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            </Columns>

                        </asp:GridView>


                    </div>
                </div>
            </asp:Panel>

        </asp:View>

        <asp:View ID="docManagement" runat="server">

            <asp:Panel ID="PanelInertPerManageQA" runat="server" Visible="false" class="col-md-12">
                <asp:LinkButton ID="btnAddPermissionQA" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่มสิทธิ์" data-toggle="tooltip" title="เพิ่มสิทธิ์" runat="server"
                    CommandName="cmdAddPermissionQA" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มสิทธิ์</asp:LinkButton>
            </asp:Panel>

            <div class="form-group col-md-12" id="div_btncancel" runat="server" visible="false">
                <asp:LinkButton ID="btnCancelPer" runat="server" CssClass="btn btn-danger" CommandName="btnCancel" OnCommand="btnCommand"
                    Text="<i class='fa fa-angle-left fa-lg'></i> ย้อนกลับ" />
            </div>

            <div class="col-md-12 m-t-10">
                <asp:GridView ID="GvPerManagement"
                    runat="server"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive col-md-12"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    OnRowDataBound="gvRowDataBound"
                    OnPageIndexChanging="gvPageIndexChanging"
                    PageSize="10">
                    <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">No result</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex + 1) %>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:Label ID="lblm0_management_idx" runat="server" Text='<%# Eval("m0_management_idx") %>' Visible="false" />

                                <p><b>องค์กร :</b> <%# Eval("org_name_th_per") %></p>
                                <p><b>ฝ่าย :</b> <%# Eval("dept_name_th_per") %></p>
                                <p><b>แผนก :</b> <%# Eval("sec_name_th_per") %></p>
                                <p><b>ตำแหน่ง :</b> <%# Eval("pos_name_th_per") %></p>


                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียดสถานที่" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>

                                    <%--<div class="col-md-12 m-t-20">--%>
                                    <%-- <asp:Label ID="lblFoodNameType" runat="server" CssClass="f-bold f-s-14 m-b-10">
                              ประเภท :: <%# Eval("food_type_name_th") %>
                                        </asp:Label>--%>
                                    <table class="table table-striped f-s-12 table-empshift-responsive">
                                        <asp:Repeater ID="rptPlaceList" runat="server">
                                            <%-- <HeaderTemplate>
                                                    <tr>
                                                        <th>สถานที่</th>
                                                        
                                                    </tr>
                                                </HeaderTemplate>--%>
                                            <ItemTemplate>
                                                <tr>
                                                    <td data-th="สถานที่"><%# Eval("place_name") %></td>

                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                    <%-- </div>--%>
                                    <%-- <%# Eval("food_sell_price") %>--%>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="รายละเอียดสิทธิ์" ItemStyle-HorizontalAlign="Right" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>

                                    <%--<div class="col-md-12 m-t-20">--%>
                                    <%-- <asp:Label ID="lblFoodNameType" runat="server" CssClass="f-bold f-s-14 m-b-10">
                              ประเภท :: <%# Eval("food_type_name_th") %>
                                        </asp:Label>--%>
                                    <table class="table table-striped f-s-12 table-empshift-responsive">
                                        <asp:Repeater ID="rptPerList" runat="server">
                                            <%-- <HeaderTemplate>
                                                    <tr>
                                                        <th>สิทธิ์</th>
                                                        
                                                    </tr>
                                                </HeaderTemplate>--%>
                                            <ItemTemplate>
                                                <tr>
                                                    <td data-th="สิทธิ์"><%# Eval("per_management_name") %></td>

                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                    <%-- </div>--%>
                                    <%-- <%# Eval("food_sell_price") %>--%>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" HeaderStyle-Width="8%" ItemStyle-CssClass="f-bold">
                            <ItemTemplate>
                                <small>
                                    <%# getStatus((int)Eval("m0_management_status")) %>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small"
                            HeaderStyle-Width="10%">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnUpdatePer" CssClass="btn btn-warning btn-sm" runat="server" CommandName="cmdToUpdatePer" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("m0_management_idx") %>'>
                           <i class="fa fa-pencil"></i>
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnDelPerList" CssClass="btn btn-danger btn-sm" runat="server"
                                    CommandName="cmdDelPerList" OnCommand="btnCommand" CommandArgument='<%# Eval("m0_management_idx") %>'
                                    OnClientClick="return confirm('คุณต้องการลบข้อมูลนี้ใช่หรือไม่')">
                           <i class="fa fa-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>

            <div class="form-group">
                <asp:FormView ID="fvInsertPerQA" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                    <InsertItemTemplate>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading f-bold">สร้างรายการสิทธิ์การใช้งานทะเบียนเครื่องมือของ "เจ้าหน้าที่ห้องปฏิบัติการ"</div>
                                    <div class="panel-body">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>องค์กร</label>
                                                    <asp:DropDownList ID="ddlorg_idx" runat="server" CssClass="form-control" Enabled="false">
                                                    </asp:DropDownList>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                        ValidationGroup="btnSave" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlorg_idx"
                                                        Font-Size="12px" ForeColor="Red"
                                                        ErrorMessage="กรุณาเลือกองค์กร" />--%>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ฝ่าย</label>
                                                    <asp:DropDownList ID="ddlrdept_idx" runat="server" CssClass="form-control" Enabled="false" />
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                        ValidationGroup="btnSave" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlorg_idx"
                                                        Font-Size="12px" ForeColor="Red"
                                                        ErrorMessage="กรุณาเลือกองค์กร" />--%>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>แผนก</label>
                                                    <asp:DropDownList ID="ddlrsec_idx" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                        ValidationGroup="btnSave" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlorg_idx"
                                                        Font-Size="12px" ForeColor="Red"
                                                        ErrorMessage="กรุณาเลือกองค์กร" />--%>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ตำแหน่ง</label>
                                                    <asp:DropDownList ID="ddlrpos_idx" runat="server" CssClass="form-control" />
                                                    <asp:RequiredFieldValidator ID="requiredDdlrsec_idx"
                                                        ValidationGroup="btnSave" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlrpos_idx"
                                                        Font-Size="12px" ForeColor="Red"
                                                        ErrorMessage="กรุณาเลือกตำแหน่ง" />
                                                </div>
                                            </div>

                                            <asp:UpdatePanel ID="updatePlaceInsert" runat="server">
                                                <ContentTemplate>
                                                    <div class="col-md-12">
                                                        <div id="div_DetailPlace" class="panel panel-default" runat="server">
                                                            <div class="panel-body">
                                                                <div class="col-md-10">
                                                                    <div class="form-group">
                                                                        <label>เลือกสถานที่</label>
                                                                        <asp:DropDownList ID="ddl_Placeidx" runat="server" CssClass="form-control" />
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldDdl_Placeidx"
                                                                            runat="server"
                                                                            InitialValue="0"
                                                                            ControlToValidate="ddl_Placeidx" Display="None" SetFocusOnError="true"
                                                                            ErrorMessage="*กรุณาเลือกสถานที่"
                                                                            ValidationGroup="addPlacePerList" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server"
                                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDdl_Placeidx" Width="250" />
                                                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                                            ValidationGroup="addPlacePerList"
                                                                            ControlToValidate="ddl_Placeidx"
                                                                            Display="Dynamic"
                                                                            SetFocusOnError="true"
                                                                            Font-Size="13px" ForeColor="Red"
                                                                            ErrorMessage="กรุณาเลือกสถานที่" />--%>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label>&nbsp;</label>
                                                                        <div class="clearfix"></div>
                                                                        <asp:LinkButton ID="btnAddPlaceTolist" runat="server"
                                                                            CssClass="btn btn-primary col-md-12"
                                                                            Text="เพิ่มสถานที่" OnCommand="btnCommand" CommandName="cmdAddPlacePerList"
                                                                            ValidationGroup="addPlacePerList" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <asp:GridView ID="gvPlaceList"
                                                                        runat="server"
                                                                        CssClass="table table-striped table-responsive"
                                                                        GridLines="None"
                                                                        OnRowCommand="onRowCommand"
                                                                        AutoGenerateColumns="false">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-CssClass="text-center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                                <ItemTemplate>
                                                                                    <%# (Container.DataItemIndex + 1) %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:BoundField DataField="drPlacePerText" HeaderText="สถานที่" ItemStyle-CssClass="text-center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-CssClass="text-center">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnRemovePlacePer" runat="server"
                                                                                        CssClass="btn btn-danger btn-xs"
                                                                                        OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')"
                                                                                        CommandName="btnRemovePlacePer"><i class="fa fa-times"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                            <asp:UpdatePanel ID="updatePanelInsertPer" runat="server">
                                                <ContentTemplate>
                                                    <div class="col-md-12">
                                                        <div id="panelPerQA" class="panel panel-default" runat="server">
                                                            <div class="panel-body">
                                                                <div class="col-md-10">
                                                                    <div class="form-group">
                                                                        <label>เลือกสิทธิ์</label>
                                                                        <asp:DropDownList ID="ddl_Permanagement" runat="server" CssClass="form-control" />
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldDdl_Permanagement"
                                                                            runat="server"
                                                                            InitialValue="0"
                                                                            ControlToValidate="ddl_Permanagement" Display="None" SetFocusOnError="true"
                                                                            ErrorMessage="*กรุณาเลือกสิทธิ์"
                                                                            ValidationGroup="addPermanageList" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server"
                                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDdl_Permanagement" Width="250" />
                                                                        <%--<asp:RequiredFieldValidator ID="requiredDdlFoodMaterial" runat="server"
                                                                            ValidationGroup="addPermanageList"
                                                                            ControlToValidate="ddl_Permanagement"
                                                                            Display="Dynamic"
                                                                            SetFocusOnError="true"
                                                                            Font-Size="13px" ForeColor="Red"
                                                                            ErrorMessage="กรุณาเลือกสิทธิ์" />--%>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label>&nbsp;</label>
                                                                        <div class="clearfix"></div>
                                                                        <asp:LinkButton ID="btnAddPermissionManage" runat="server"
                                                                            CssClass="btn btn-primary col-md-12"
                                                                            Text="เพิ่มสิทธิ์" OnCommand="btnCommand" CommandName="cmdAddPermissionManage"
                                                                            ValidationGroup="addPermanageList" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <asp:GridView ID="gvPerManageList" runat="server"
                                                                        CssClass="table table-striped table-responsive"
                                                                        GridLines="None" OnRowCommand="onRowCommand"
                                                                        AutoGenerateColumns="false">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-CssClass="text-center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller"
                                                                                HeaderStyle-Width="10%">
                                                                                <ItemTemplate>
                                                                                    <%# (Container.DataItemIndex + 1) %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="drPerText" HeaderText="ชื่อสิทธิ์" ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" />

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="center"
                                                                                HeaderStyle-CssClass="text-center">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnRemovePerManage" runat="server"
                                                                                        CssClass="btn btn-danger btn-xs"
                                                                                        OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')"
                                                                                        CommandName="btnRemovePerManage">
                                                               <i class="fa fa-times"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:LinkButton ID="btnInsertPerManagement" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                                        CommandName="cmdInsertPerManagement" Text="บันทึก" ValidationGroup="btnSave"
                                                        OnClientClick="return confirm('ยืนยันการบันทึก')" />
                                                    <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand"
                                                        CommandName="btnCancel" Text="ยกเลิก" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>

                    <EditItemTemplate>
                        <asp:Label ID="lblm0_management_idx_edit" runat="server" Text='<%# Eval("m0_management_idx") %>' Visible="false" />
                        <asp:Label ID="lblorg_idx_edit" runat="server" Text='<%# Eval("org_idx") %>' Visible="false" />
                        <asp:Label ID="lbl_rdept_idx_edit" runat="server" Text='<%# Eval("rdept_idx") %>' Visible="false" />
                        <asp:Label ID="lbl_rsec_idx_edit" runat="server" Text='<%# Eval("rsec_idx") %>' Visible="false" />
                        <asp:Label ID="lbl_rpos_idx_edit" runat="server" Text='<%# Eval("rpos_idx") %>' Visible="false" />

                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-info">
                                    <div class="panel-heading f-bold">แก้ไขรายการสิทธิ์การใช้งานทะเบียนเครื่องมือของ "เจ้าหน้าที่ห้องปฏิบัติการ"</div>
                                    <div class="panel-body">
                                        <div class="col-md-10 col-md-offset-1">

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>องค์กร</label>
                                                    <asp:DropDownList ID="ddlorg_idxupdate" runat="server" CssClass="form-control" Enabled="false">
                                                    </asp:DropDownList>
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                        ValidationGroup="btnSave" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlorg_idx"
                                                        Font-Size="12px" ForeColor="Red"
                                                        ErrorMessage="กรุณาเลือกองค์กร" />--%>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ฝ่าย</label>
                                                    <asp:DropDownList ID="ddlrdept_idxupdate" runat="server" CssClass="form-control" Enabled="false" />
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                        ValidationGroup="btnSave" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlorg_idx"
                                                        Font-Size="12px" ForeColor="Red"
                                                        ErrorMessage="กรุณาเลือกองค์กร" />--%>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>แผนก</label>
                                                    <asp:DropDownList ID="ddlrsec_idxupdate" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldDdlrsec_idxupdate"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlrsec_idxupdate" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกแผนก"
                                                        ValidationGroup="cmdSaveUpdatePer" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDdlrsec_idxupdate" Width="250" />
                                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                                                        ValidationGroup="btnSave" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlorg_idx"
                                                        Font-Size="12px" ForeColor="Red"
                                                        ErrorMessage="กรุณาเลือกองค์กร" />--%>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>ตำแหน่ง</label>
                                                    <asp:DropDownList ID="ddlrpos_idxupdate" runat="server" CssClass="form-control" />
                                                    <asp:RequiredFieldValidator ID="RequiredFieldDdlrpos_idxupdate"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlrpos_idxupdate" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกตำแหน่ง"
                                                        ValidationGroup="cmdSaveUpdatePer" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDdlrpos_idxupdate" Width="250" />
                                                    <%--<asp:RequiredFieldValidator ID="requiredDdlrsec_idx"
                                                        ValidationGroup="btnSaveUpdate" runat="server"
                                                        Display="Dynamic"
                                                        SetFocusOnError="true"
                                                        ControlToValidate="ddlrpos_idxupdate"
                                                        Font-Size="12px" ForeColor="Red"
                                                        ErrorMessage="กรุณาเลือกตำแหน่ง" />--%>
                                                </div>
                                            </div>

                                            <div class="clearfix"></div>
                                            <asp:UpdatePanel ID="updatePanelGetPlaceUpdate" runat="server">
                                                <ContentTemplate>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-12">
                                                        <div id="panelPlaceUpdate" class="panel panel-default" runat="server">
                                                            <div class="panel-body">

                                                                <div class="col-md-10">
                                                                    <div class="form-group">
                                                                        <label>เลือกสถานที่</label>
                                                                        <asp:DropDownList ID="ddl_PlaceidxUpdate" runat="server" CssClass="form-control" />
                                                                        <asp:RequiredFieldValidator ID="RequiredFieldDdl_PlaceidxUpdate"
                                                                            runat="server"
                                                                            InitialValue="0"
                                                                            ControlToValidate="ddl_PlaceidxUpdate" Display="None" SetFocusOnError="true"
                                                                            ErrorMessage="*กรุณาเลือกสถานที่"
                                                                            ValidationGroup="addPlacePerListUpdate" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19444" runat="Server"
                                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldDdl_PlaceidxUpdate" Width="250" />
                                                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldddl_PlaceidxUpdate" runat="server"
                                                                            
                                                                            ValidationGroup="addPlacePerListUpdate"
                                                                            ControlToValidate="ddl_PlaceidxUpdate"
                                                                            Display="Dynamic"
                                                                            SetFocusOnError="true"
                                                                            Font-Size="13px" ForeColor="Red"
                                                                            ErrorMessage="กรุณาเลือกสถานที่" />--%>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label>&nbsp;</label>
                                                                        <div class="clearfix"></div>
                                                                        <asp:LinkButton ID="btnAddPlacePerUpdate" runat="server" CssClass="btn btn-primary col-md-12"
                                                                            Text="เพิ่มสถานที่" OnCommand="btnCommand" CommandName="cmdAddPlacePerUpdate"
                                                                            ValidationGroup="addPlacePerListUpdate" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <asp:GridView ID="gvPlacePerListUpdate" runat="server"
                                                                        CssClass="table table-striped table-responsive"
                                                                        GridLines="None"
                                                                        OnRowCommand="onRowCommand"
                                                                        AutoGenerateColumns="false">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                                <ItemTemplate>
                                                                                    <%# (Container.DataItemIndex + 1) %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="drPlaceTextUpdate" HeaderText="สถานที่" HeaderStyle-Font-Size="Smaller"
                                                                                ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" />

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnRemovePlacePerUpdate" runat="server" CssClass="btn btn-danger btn-xs"
                                                                                        OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')" CommandName="btnRemovePlacePerUpdate">
                                                               <i class="fa fa-times"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                            <asp:UpdatePanel ID="updatePanelGetPerUpdate" runat="server">
                                                <ContentTemplate>
                                                    <div class="clearfix"></div>
                                                    <div class="col-md-12">
                                                        <div id="Div1" class="panel panel-default" runat="server">
                                                            <div class="panel-body">

                                                                <div class="col-md-10">
                                                                    <div class="form-group">
                                                                        <label>เลือกสิทธิ์</label>
                                                                        <asp:DropDownList ID="ddl_PerManageUpdate" runat="server" CssClass="form-control" />
                                                                        <asp:RequiredFieldValidator ID="RequiredDdl_PerManageUpdate"
                                                                            runat="server"
                                                                            InitialValue="0"
                                                                            ControlToValidate="ddl_PerManageUpdate" Display="None" SetFocusOnError="true"
                                                                            ErrorMessage="*กรุณาเลือกสิทธิ์"
                                                                            ValidationGroup="addPerListUpdate" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorddlChooseLabType" runat="Server"
                                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredDdl_PerManageUpdate" Width="250" />


                                                                        <%-- <asp:DropDownList ID="ddlChooseLabType" runat="server" CssClass="form-control"
                                                                            OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" />
                                                                        <asp:RequiredFieldValidator ID="RequiredddlChooseLabType" runat="server" InitialValue="0"
                                                                            ControlToValidate="ddlChooseLabType" Display="None" SetFocusOnError="true"
                                                                            ErrorMessage="*กรุณาเลือกประเภทการสอบเทียบ" ValidationGroup="SelectTypegroup" />
                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorddlChooseLabType" runat="Server"
                                                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlChooseLabType" Width="250" />--%>



                                                                        <%--<asp:RequiredFieldValidator ID="RequiredDdl_PerManageUpdate" runat="server"
                                                                            ValidationGroup="addPlacePerListUpdate"
                                                                            ControlToValidate="ddl_PerManageUpdate"
                                                                            Display="Dynamic"
                                                                            SetFocusOnError="true"
                                                                            Font-Size="13px" ForeColor="Red"
                                                                            ErrorMessage="กรุณาเลือกสิทธิ์" />--%>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-2">
                                                                    <div class="form-group">
                                                                        <label>&nbsp;</label>
                                                                        <div class="clearfix"></div>
                                                                        <asp:LinkButton ID="btnPerPerUpdate" runat="server" CssClass="btn btn-primary col-md-12"
                                                                            Text="เพิ่มสิทธิ์" OnCommand="btnCommand" CommandName="cmdAddPerPerUpdate"
                                                                            ValidationGroup="addPerListUpdate" />
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <asp:GridView ID="gvPerPerListUpdate" runat="server"
                                                                        CssClass="table table-striped table-responsive"
                                                                        GridLines="None"
                                                                        OnRowCommand="onRowCommand"
                                                                        AutoGenerateColumns="false">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center"
                                                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller">
                                                                                <ItemTemplate>
                                                                                    <%# (Container.DataItemIndex + 1) %>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="drPerTextUpdate" HeaderText="สิทธิ์" HeaderStyle-Font-Size="Smaller"
                                                                                ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" />

                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton ID="btnRemovePerPerUpdate" runat="server" CssClass="btn btn-danger btn-xs"
                                                                                        OnClientClick="return confirm('คุณต้องการลบรายการนี้ใชหรือไม่ ?')" CommandName="btnRemovePerPerUpdate">
                                                               <i class="fa fa-times"></i>
                                                                                    </asp:LinkButton>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>สถานะ</label>
                                                    <asp:DropDownList ID="ddlm0_management_statusUpdate" runat="server" CssClass="form-control"
                                                        SelectedValue='<%# Eval("m0_management_status") %>'>
                                                        <asp:ListItem Value="1" Text="ใช้งาน" />
                                                        <asp:ListItem Value="0" Text="ยกเลิกใช้งาน" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <asp:LinkButton ID="btnSaveUpdatePer" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                                        OnClientClick="return confirmWithOutValidated('ยืนยันการบันทึกการเปลี่ยนแปลง', 'btnSaveUpdate')"
                                                        CommandName="cmdSaveUpdatePer" CommandArgument='<%# Eval("m0_management_idx") %>' Text="บันทึกการแก้ไข"
                                                        ValidationGroup="cmdSaveUpdatePer" />

                                                    <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand"
                                                        CommandName="btnCancel" Text="ยกเลิก" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </EditItemTemplate>
                </asp:FormView>
            </div>

        </asp:View>
    </asp:MultiView>

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
        });


        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY',
                    ignoreReadonly: true
                });
            });

            /* START Filter Datetimepicker Permission Permanent */
            $('.datetimepicker-filter-perm-from').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.datetimepicker-filter-perm-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-perm-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-perm-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
           <%-- $('#<%= HiddenCalDate.ClientID %>').val($('.datetimepicker-filter-perm-from').val());--%>
            });
            $('.show-filter-perm-from-onclick').click(function () {
                $('.datetimepicker-filter-perm-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-to-onclick').click(function () {
                $('.datetimepicker-filter-perm-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-perm-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-perm-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                 <%--$('#<%= txtFilterIndexPermPermToHidden.ClientID %>').val($('.datetimepicker-filter-perm-to').val());--%>
            });

            $('.show-filter').click(function () {
                $('.datetimepicker-filter').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });

            /* END Filter Datetimepicker Permission Permanent */
        });


        /* START Filter Datetimepicker Permission Permanent */
        $('.datetimepicker-filter-perm-from').datetimepicker({
            format: 'DD/MM/YYYY',
            ignoreReadonly: true
        });
        $('.datetimepicker-filter-perm-from').on('dp.change', function (e) {
            var dateTo = $('.datetimepicker-filter-perm-to').data("DateTimePicker").date();
            if (e.date > dateTo) {
                $('.datetimepicker-filter-perm-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
            }
           <%-- $('#<%= HiddenCalDate.ClientID %>').val($('.datetimepicker-filter-perm-from').val());--%>
        });
        $('.show-filter-perm-from-onclick').click(function () {
            $('.datetimepicker-filter-perm-from').data("DateTimePicker").show();
        });
        $('.datetimepicker-filter-perm-to').datetimepicker({
            format: 'DD/MM/YYYY',
            ignoreReadonly: true
        });
        $('.show-filter-perm-to-onclick').click(function () {
            $('.datetimepicker-filter-perm-to').data("DateTimePicker").show();
        });
        $('.datetimepicker-filter-perm-to').on('dp.change', function (e) {
            var dateFrom = $('.datetimepicker-filter-perm-from').data("DateTimePicker").date();
            if (e.date < dateFrom) {
                $('.datetimepicker-filter-perm-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
            }
             <%--$('#<%= txtFilterIndexPermPermToHidden.ClientID %>').val($('.datetimepicker-filter-perm-to').val());--%>
        });
        $('.show-filter').click(function () {
            $('.datetimepicker-filter').data("DateTimePicker").show();
        });
        $('.datetimepicker-filter').datetimepicker({
            format: 'DD/MM/YYYY',
            ignoreReadonly: true
        });
        /* END Filter Datetimepicker Permission Permanent */


    </script>

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>
</asp:Content>



