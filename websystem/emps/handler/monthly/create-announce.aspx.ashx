﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

public class Handler : IHttpHandler
{
   public void ProcessRequest(HttpContext context)
   {
      int creator = 0;
      int empIdx = 0;
      string callback = context.Request.QueryString["callback"];
      string announceStartDate = String.Empty;
      string announceEndDate = String.Empty;
      int.TryParse(context.Request.QueryString["empIdx"], out empIdx);
      int.TryParse(context.Request.QueryString["creator"], out creator);
      announceStartDate = context.Request.QueryString["announceStartDate"];
      announceEndDate = context.Request.QueryString["announceEndDate"];
      string json = this.createAnnounce(creator, empIdx, announceStartDate, announceEndDate);
      if (!string.IsNullOrEmpty(callback))
      {
         json = string.Format("{0}({1});", callback, json);
      }
      context.Response.ContentType = "text/json";
      context.Response.Write(json);
   }

   private string createAnnounce(int creator, int empIdx, string announceStartDate, string announceEndDate)
   {
      List<object> emps = new List<object>();
      using (SqlConnection conn = new SqlConnection())
      {
         conn.ConnectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;
         using (SqlCommand cmd = new SqlCommand())
         {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            var now = DateTime.Now.ToString("yyyy-MM-dd H:m:s");
            int statusOnline = 1;
            cmd.Connection = conn;
            cmd.CommandText = "INSERT INTO emps_u0_announce (u0_node_idx_ref, emp_idx_ref, announce_start_date, announce_end_date, announce_status, announce_created_at, announce_created_by, announce_updated_at, announce_updated_by) OUTPUT Inserted.u0_announce_idx VALUES (1, @empIdx, @announceStartDate, @announceEndDate, " + statusOnline + ", '" + now + "', @creator, '" + now + "', @creator)";
            cmd.Parameters.AddWithValue("@announceStartDate", announceStartDate);
            cmd.Parameters.AddWithValue("@announceEndDate", announceEndDate);
            cmd.Parameters.AddWithValue("@creator", creator);
            cmd.Parameters.AddWithValue("@empIdx", empIdx);
            conn.Open();
            var insertedId = cmd.ExecuteScalar();
            emps.Add(new
            {
               announceIdx = insertedId
            });
            conn.Close();
         }
         return (new JavaScriptSerializer().Serialize(emps));
      }
   }

   public bool IsReusable
   {
      get
      {
         return false;
      }
   }
}
