﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true"
    CodeFile="hr_news_Demo.aspx.cs" Inherits="websystem_hr_hr_news" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>

    <asp:TextBox ID="txtTinyMCE" runat="server" TextMode="MultiLine" CssClass="tinymce"></asp:TextBox>
<script type="text/javascript">
    tinymce.init({ selector: 'textarea', width: 300 });
   
</script>
<script type="text/javascript">
    tinymce.init({
        mode: "specific_textareas",
        editor_selector: "tinymce",
        encoding: "xml",
        theme: "modern",
        menubar: false,
        resize: false,
        statusbar: false,
        plugins: ["advlist autolink lists charmap preview hr anchor",
            "pagebreak code nonbreaking table contextmenu directionality paste"],
        toolbar1: "styleselect | bold italic underline | undo redo",
        toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
        setup: function (editor) {
            editor.on('change', function () { tinymce.triggerSave(); });
        }
    });

    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_endRequest(function () {
        tinymce.remove(".tinymce");

        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "tinymce",
            encoding: "xml",
            theme: "modern",
            menubar: false,
            resize: false,
            statusbar: false,
            plugins: ["advlist autolink lists charmap preview hr anchor",
                "pagebreak code nonbreaking table contextmenu directionality paste"],
            toolbar1: "styleselect | bold italic underline | undo redo",
            toolbar2: "alignleft aligncenter alignright alignjustify | bullist numlist outdent indent",
            setup: function (editor) {
                editor.on('change', function () { tinymce.triggerSave(); });
            }
        });

    })
</script> 

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" hidden="hidden">
		  <div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-body">
					<img src='<%= ResolveUrl("~/images/preload/RamaX.jpg") %>' class="img-responsive">
                  
				</div>
			</div>
		  </div>
		</div>

		<script type="text/javascript">
            $(window).on('load', function () {
                $('#myModal').modal('show');
            });
        </script>
    <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="sub-navbar">
                    <a class="navbar-brand" href="#"><b>Menu</b></a>
                </div>
            </div>

            <!--Collect the nav links, forms, and other content for toggling-->
            <div class="collapse navbar-collapse" id="menu-bar">
                <ul class="nav navbar-nav" id="uiNav" runat="server">
                    <li id="li0" runat="server">
                        <asp:LinkButton ID="lbtab1" runat="server" CommandName="cmdtab0" OnCommand="navCommand" CommandArgument="tab0"> รายการทั่วไป</asp:LinkButton>
                    </li>
                    <li id="li2" runat="server">
                        <asp:LinkButton ID="lbtab2" runat="server" CommandName="cmdtab2" OnCommand="navCommand" CommandArgument="tab2"> สร้างประเภท</asp:LinkButton>
                    </li>
                    <li id="li1" runat="server">
                        <asp:LinkButton ID="LinkButtonli1" runat="server" CommandName="cmdtab1" OnCommand="navCommand" CommandArgument="tab1"> สร้างรายการ</asp:LinkButton>
                    </li>
                    <li id="li3" runat="server">
                       <%-- <asp:LinkButton ID="LinkButtonli3" runat="server" CommandName="cmdtab3" OnCommand="navCommand" CommandArgument="tab3"> สร้างสไลด์</asp:LinkButton>--%>
                        <asp:LinkButton ID="lbtab3" runat="server" CommandName="cmdtab3" OnCommand="navCommand" class="dropdown-toggle" data-toggle="dropdown"
                            role="button" aria-haspopup="true" aria-expanded="false" CommandArgument="tab3"> อัพโหลดรูป <span class="caret"></span></asp:LinkButton>
                         <ul class="dropdown-menu">
                            <li>
                                <asp:LinkButton ID="btnview1" runat="server" data-original-title="" data-toggle="tooltip" OnCommand="navCommand"
                                    CommandArgument="tab3" CommandName="cmdView"> อัพโหลดรูปสไลด์</asp:LinkButton>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li>
                                <asp:LinkButton ID="btnview2" runat="server" data-original-title="" data-toggle="tooltip" OnCommand="navCommand"
                                    CommandArgument="tab4" CommandName="cmdView"> อัพโหลดรูปหน้าแรก</asp:LinkButton>
                            </li>

                        </ul>
                    </li>


                </ul>


            </div>
        </div>
    </nav>







    <asp:FormView ID="FvInsertEdit" runat="server" Width="100%">
        <InsertItemTemplate>
            <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">เพิ่ม</h3>
                    
                </div>
                <div class="panel-body">
                    <div class="panel-heading">

                        <div class="form-horizontal" role="form">

                            <div class="panel-heading">
                                
                                <div class="form-group">
                                    <%--<asp:Label ID="lbnameth" runat="server" Text="ชื่อองค์กร (ภาษาไทย)" CssClass="col-sm-3 control-label"></asp:Label>--%>
                                    <asp:Label ID="labelinsert1" runat="server" class="col-sm-3 control-label">ชื่อประเภทข่าวสาร
                                            <span class="text-danger">*</span></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="tb_JobGrade" runat="server" CssClass="form-control" placeholder="กรอกชื่อประเภทข่าวสาร" MaxLength="250">

                                        </asp:TextBox>

                                    </div>
                                    <div class="col-sm-3">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <asp:Label ID="lbstatus" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddStatus" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1" Text="Online" />
                                            <asp:ListItem Value="0" Text="Offline" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0">
                                                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save</asp:LinkButton>

                                        <asp:LinkButton ID="LinkButton2" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="cmdCancel" CausesValidation="false">
                                                    <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancel
                                        </asp:LinkButton>
                                    </div>
                                    <label class="col-sm-3 control-label"></label>
                                </div>
                                    </div>
                            
                        </div>
                    </div>
                </div>
            </div>
                </div>
        </InsertItemTemplate>
    </asp:FormView>

    <asp:FormView ID="Fvtab1" runat="server" Width="100%">
        <InsertItemTemplate>
            <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">เพิ่ม</h3>

                </div>
               


                
                

                <div class="panel-body">
                    <div class="panel-heading">

                        <div class="form-horizontal" role="form">

                            <div class="panel-heading">

                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="ประเภทข่าวสาร" CssClass="col-sm-3 control-label">ชื่อประเภทข่าวสาร
                                            <span class="text-danger">*</span></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="0" Text="--กรุณาเลือก--" />
                                            <asp:ListItem Value="1" Text="กฎหมาย" />
                                            <asp:ListItem Value="2" Text="โครงสร้างองค์กร" />
                                            <asp:ListItem Value="3" Text="ข้อมูลติดต่อสื่อสาร" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <%--<asp:Label ID="lbnameth" runat="server" Text="ชื่อองค์กร (ภาษาไทย)" CssClass="col-sm-3 control-label"></asp:Label>--%>
                                    <asp:Label ID="labelinsert1" runat="server" class="col-sm-3 control-label">หัวข้อข่าวสาร
                                            <span class="text-danger">*</span></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="tb_JobGrade" runat="server" CssClass="form-control" placeholder="กรอกหัวข้อข่าวสาร" MaxLength="250">

                                        </asp:TextBox>
                                 <%--<script type="text/javascript">

                                     tinyMCE.init({

                                         mode: "textarea"

                                     });

                                 </script>--%>
                                       <script type="text/javascript">
                                           tinymce.init({ selector: 'textarea', width: 300 });
                                       </script>
                                                    
                                        <%--<asp:TextBox ID="textareas" TextMode="MultiLine" runat="server"></asp:TextBox>--%>
                                        <asp:TextBox ID="txtTinyMCE" runat="server" TextMode="MultiLine"></asp:TextBox>
                                        
                                    
                                    </div>
                                </div>

                                <div class="form-group">
                                    <%--<asp:Label ID="lbnameth" runat="server" Text="ชื่อองค์กร (ภาษาไทย)" CssClass="col-sm-3 control-label"></asp:Label>--%>
                                    <asp:Label ID="label2" runat="server" class="col-sm-3 control-label">รายละเอียดข่าวสาร
                                            <span class="text-danger">*</span></asp:Label>
                                    <div class="col-sm-6">
    
                                        
                                    </div>
                                    <div class="col-sm-3">
                                    </div>
                                </div>

                                 <div class="form-group">
                                     <asp:Label ID="label3" runat="server" class="col-sm-3 control-label">ไฟล์แนบ
                                            <span class="text-danger"></span></asp:Label>
                                    <div class="col-sm-6">
                        
                                        <asp:FileUpload ID="upload_file_material" runat="server" />
                                     </div>
                                     <div class="col-sm-3">
                                    </div>
                                 </div>

                                <div class="form-group">
                                    <asp:Label ID="lbstatus" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddStatus" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1" Text="Online" />
                                            <asp:ListItem Value="0" Text="Offline" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <asp:LinkButton ID="lbCreatetab1" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSaveFvtab1" CommandArgument="0">
                                                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save</asp:LinkButton>

                                        <asp:LinkButton ID="LinkButton2" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="cmdCancel" CausesValidation="false">
                                                    <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancel
                                        </asp:LinkButton>
                                    </div>
                                    <label class="col-sm-3 control-label"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                 </div>
        </InsertItemTemplate>
    </asp:FormView>


        <asp:FormView ID="Fvimg" runat="server" Width="100%">
        <InsertItemTemplate>
            <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">เพิ่ม</h3>

                </div>
                <div class="panel-body">
                    <div class="panel-heading">

                        <div class="form-horizontal" role="form">

                            <div class="panel-heading">

                                

                                 <div class="form-group">
                                     <asp:Label ID="label3" runat="server" class="col-sm-3 control-label">รูปภาพ
                                            <span class="text-danger"></span></asp:Label>
                                    <div class="col-sm-6">
                        
                                        <asp:FileUpload ID="upload_file_material" runat="server" />
                                     </div>
                                     <div class="col-sm-3">
                                    </div>
                                 </div>

                                <div class="form-group">
                                    <asp:Label ID="lbstatus" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddStatus" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1" Text="Online" />
                                            <asp:ListItem Value="0" Text="Offline" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-success" OnCommand="navCommand" CommandName="tab3" CommandArgument="tab3">
                                                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save</asp:LinkButton>

                                        <asp:LinkButton ID="LinkButton2" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="cmdCancel" CausesValidation="false">
                                                    <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancel
                                        </asp:LinkButton>
                                    </div>
                                    <label class="col-sm-3 control-label"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                </div>
        </InsertItemTemplate>
    </asp:FormView>

            <asp:FormView ID="uploadimgMain" runat="server" Width="100%">
        <InsertItemTemplate>
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">เพิ่ม</h3>

                </div>
                <div class="panel-body">
                    <div class="panel-heading">

                        <div class="form-horizontal" role="form">

                            <div class="panel-heading">

                                

                                 <div class="form-group">
                                     <asp:Label ID="label3" runat="server" class="col-sm-3 control-label">รูปภาพ
                                            <span class="text-danger"></span></asp:Label>
                                    <div class="col-sm-6">
                        
                                        <asp:FileUpload ID="upload_file_material" runat="server" />
                                     </div>
                                     <div class="col-sm-3">
                                    </div>
                                 </div>

                                <div class="form-group">
                                    <asp:Label ID="lbstatus" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddStatus" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1" Text="Online" />
                                            <asp:ListItem Value="0" Text="Offline" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-success" OnCommand="navCommand" CommandName="tab4" CommandArgument="tab4">
                                                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save</asp:LinkButton>

                                        <asp:LinkButton ID="LinkButton2" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="cmdCancel" CausesValidation="false">
                                                    <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancel
                                        </asp:LinkButton>
                                    </div>
                                    <label class="col-sm-3 control-label"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </InsertItemTemplate>
    </asp:FormView>

    <%--    <div class="form-group">
                <asp:LinkButton ID="lbCreate" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdCreate" Visible="false"><i class="fa fa-plus" aria-hidden="true"></i></asp:LinkButton>
            </div>--%>

     

<%-- upload--%>
<%--            <div id="master_meterail" runat="server">
                <div class="form-horizontal" role="form">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <asp:FileUpload ID="upload_file_material" runat="server" />
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            
                            <div class="pull-left">
                                <asp:LinkButton ID="btnupload" CssClass="btn btn-success" runat="server" ValidationGroup="Import" OnCommand="btnCommand" CommandName="btnuploadimg" data-toggle="tooltip" title="Import" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')"><span class="glyphicon glyphicon-file"></span> Upload</asp:LinkButton>
                            </div>

                        </div>
                    </div>
                </div>
            </div>--%>


          
    <%--multiview--%>
    <asp:MultiView ID="mvSystem" runat="server">
        <asp:View ID="tab2" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="lbCreate" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdCreate"><i class="fa fa-plus" aria-hidden="true"></i></asp:LinkButton>
            </div>
            <asp:FormView ID="table" runat="server" Width="100%">
                <InsertItemTemplate>
                                            <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="Table1" runat="server" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 20%;">#</th>
                                <th scope="col" style="width: 40%;">ชื่อประเภทข่าวสาร</th>
                                <th scope="col" style="width: 20%;">สถานะ</th>
                                <th scope="col" style="width: 20%;">การจัดการ</th>

                            </tr>
                            <tr style="font-size: Small;">
                                <td>1</td>
                                <td>กฎหมาย</td>
                                <td>online</td>
                                <td>
                                <asp:LinkButton ID="LinkButton21" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton288" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>

                                </td>

                            </tr>
                            <tr style="font-size: Small;">
                                <td>2</td>
                                <td>โครงสร้างองค์กร</td>
                                <td>online</td>
                                                            <td>
                                <asp:LinkButton ID="LinkButton4" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton5" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>

                            </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>3</td>
                                <td>ข้อมูลติดต่อสื่อสาร</td>
                                <td>online</td>
                                                            <td>
                                <asp:LinkButton ID="LinkButton6" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton7" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>

                            </td>

                            </tr>

                        </tbody>
                    </table>
                    </div>
                            </div>
                        
                </InsertItemTemplate>
            </asp:FormView>
        </asp:View>

        <asp:View ID="tab1" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="lbCreatetab1" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdCreate2"><i class="fa fa-plus" aria-hidden="true"></i></asp:LinkButton>
            </div>
            <asp:FormView ID="tablelist" runat="server" Width="100%">
                <InsertItemTemplate>

                                        <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="Table1" runat="server" style="border-collapse: collapse;">

                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 10%;">#</th>
                                <th scope="col" style="width: 10%;">ชื่อประเภทข่าวสาร</th>
                                <th scope="col" style="width: 10%;">หัวข้อข่าวสาร</th>
                                <th scope="col" style="width: 20%;">รายละเอียดข่าวสาร</th>
                                <th scope="col" style="width: 40%;">ไฟล์แนบ</th>
                                <th scope="col" style="width: 10%;">สถานะ</th>
                                <th scope="col" style="width: 20%;">การจัดการ</th>

                            </tr>
                            <tr style="font-size: Small;">
                                <td>1</td>
                                <td>กฎหมาย</td>
                                <td>กฎหมายแรงงานปี 2564</td>
                                <td>กฎหมายแรงงานปี 2564 XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</td>
                                <td><img src='<%= ResolveUrl("~/images/carousel/christmas-01.jpg") %>' width="100%" alt="Car Booking"/></td>
                                <td>online</td>
                                <td>
                                <asp:LinkButton ID="LinkButton21" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton288" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>

                                </td>

                            </tr>
                            <tr style="font-size: Small;">
                                <td>2</td>
                                <td>โครงสร้างองค์กร</td>
                                <td>โครงสร้างองค์กรปี 2562</td>
                                <td>โครงสร้างองค์กรปี 2562 ตั้งแต่วันที่ XXXXXXXXXXXXXXXXXXXXXX</td>
                                 <td><img src='<%= ResolveUrl("~/images/carousel/christmas-01.jpg") %>' class="img-fluid" width="100%" alt="Car Booking"/></td>
                                <td>online</td>
                                <td>
                                <asp:LinkButton ID="LinkButton8" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton9" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>

                                </td>

                            </tr>
                                                        <tr style="font-size: Small;">
                                <td>2</td>
                                <td>โครงสร้างองค์กร</td>
                                <td>โครงสร้างองค์กรปี 2561</td>
                                <td>โครงสร้างองค์กรปี 2561 ตั้งแต่วันที่ XXXXXXXXXXXXXXXXXXXXXX</td>
                                 <td><img src='<%= ResolveUrl("~/images/carousel/christmas-01.jpg") %>' class="img-fluid" width="100%" alt="Car Booking"/></td>
                                <td>online</td>
                                <td>
                                <asp:LinkButton ID="LinkButton16" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton17" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>

                                </td>

                            </tr>
                            <tr style="font-size: Small;">
                                <td>3</td>
                                <td>ข้อมูลติดต่อสื่อสาร</td>
                                <td>เบอร์ติดต่อ ฝ่าย XXX</td>
                                <td>เบอร์ติดต่อ ฝ่าย XXX เริ่มตั้งแต่วันที่ XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</td>
                                 <td><img src='<%= ResolveUrl("~/images/carousel/christmas-01.jpg") %>' class="img-fluid" width="100%" alt="Car Booking"/></td>
                                <td>online</td>
                                <td>
                                <asp:LinkButton ID="LinkButton10" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton11" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>

                                </td>

                            </tr>

                        </tbody>
                    </table>
                       </div>
                                             </div>
                </InsertItemTemplate>
            </asp:FormView>
        </asp:View>


        <asp:View ID="tab0" runat="server">

            <asp:FormView ID="tablenews" runat="server" Width="100%">
                <InsertItemTemplate>

                    
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                <!-- <li data-target="#carousel-example-generic" data-slide-to="4"></li> -->
                                <!-- <li data-target="#carousel-example-generic" data-slide-to="5"></li> -->
                                <!-- <li data-target="#carousel-example-generic" data-slide-to="6"></li> -->
                            </ol>
                            <!-- Wrapper for slides -->

                            <div class="carousel-inner" role="listbox">

                                <div class="item active">
                                    <div class="col-md-10 col-md-offset-2">
                                        <img src='<%= ResolveUrl("~/images/carousel/christmas-01.jpg") %>' class="img-fluid" width="75%"
                                            alt="Christmas" />
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-10 col-md-offset-2">
                                        <img src='<%= ResolveUrl("~/images/carousel/car-booking-02.jpg") %>' class="img-fluid" width="75%"
                                            alt="Car Booking" />
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-10 col-md-offset-2">
                                        <img src='<%= ResolveUrl("~/images/carousel/reset-password-03.jpg") %>' class="img-fluid"
                                            width="75%" alt="Reset Password" />
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-10 col-md-offset-2">
                                        <img src='<%= ResolveUrl("~/images/carousel/room-booking-3.jpg") %>' class="img-fluid" width="75%"
                                            alt="Room Booking" />
                                    </div>
                                </div>
                            </div>



                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>

                    </div>
                        
                       </div>


                 
                    <div class="col-md-10 col-md-offset-1">
                      <div class="col-md-4 col-md-offset-8" style="padding-right:0px">

                                     <div class="col-md-9" style="padding-right:0px">
                                         <div class="form-group">
                                     <asp:TextBox ID="tb_JobGrade" runat="server" CssClass="form-control" placeholder="ค้นหา..." MaxLength="5"></asp:TextBox>
                                            </div>
                                         </div>
                                         <div class="col-md-3" style="padding-right:0px">
                                         <div class="form-group">
                                <asp:LinkButton ID="lbCreatetab1" runat="server" CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdre"><i class="fas fa-search"></i></asp:LinkButton>
                                        </div>
                                    </div>
                               </div>
                          </div>
                                      
                     

                                       
                                       
                                      
          

                    <div class="form-group">
                    <div class="col-md-3 col-md-offset-1"> 
                        <div class="list-group"> 
                            <a href="#" class="list-group-item list-group-item-info">ประเภทข่าวสาร</a>
                            <asp:LinkButton ID="tab0" runat="server" OnCommand="btnCommand" CommandName="tab0onclick"
                            class="list-group-item">กฎหมาย
                            </asp:LinkButton>
                            <a href="#" class="list-group-item">โครงสร้างองค์กร</a>
                            <a href="#" class="list-group-item">ข้อมูลติดต่อสื่อสาร</a>
                        </div>
                          </div> 
                    <div class="col-md-7" width: 58.55555%>
                        <a href="#" class="list-group-item list-group-item-info">หัวข้อข่าว</a>
                            <a href="#" class="list-group-item">กฎหมายแรงงานปี 2564 </a>
                            <a href="#" class="list-group-item">โครงสร้างองค์กรปี 2562</a>
                            <a href="#" class="list-group-item">โครงสร้างองค์กรปี 2561</a>
                            <a href="#" class="list-group-item">เบอร์ติดต่อ ฝ่าย XXX</a>
                        </div>
                    </div>
                   
                   

                    <%--<div class="list-group">
                        <div class="col-sm-3">
                        <asp:LinkButton ID="LinkButton1" runat="server" BackColor="#58ACFA" Font-Bold="true" ForeColor="White" CssClass="list-group-item">ประเภทข่าวสาร</asp:LinkButton>
                        <asp:Repeater ID="rplistnews" runat="server">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnmenuNewsPR" runat="server" data-toggle="tooltip" data-placement="top" title=""
                                    CommandName="cmdReadMore" OnCommand="btnCommand"
                                    CommandArgument='0' BorderColor="SkyBlue" CssClass="list-group-item btn-success">
                               <p class="card-title" style="font-size: 1.2em"><i class="fa fa-tags" aria-hidden="true"></i> XXX</h5>
                                  <p class="list-group-item-text">&nbsp; &nbsp; &nbsp; AAA</p>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:Repeater>


                    </div>
                        </div>--%>
                </InsertItemTemplate>
            </asp:FormView>
        </asp:View>

            <asp:View ID="tab0onclick" runat="server">
            <asp:FormView ID="Viewtab0" runat="server" Width="100%">
                <InsertItemTemplate>

                    
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                <!-- <li data-target="#carousel-example-generic" data-slide-to="4"></li> -->
                                <!-- <li data-target="#carousel-example-generic" data-slide-to="5"></li> -->
                                <!-- <li data-target="#carousel-example-generic" data-slide-to="6"></li> -->
                            </ol>
                            <!-- Wrapper for slides -->

                            <div class="carousel-inner" role="listbox">

                                <div class="item active">
                                    <div class="col-md-10 col-md-offset-2">
                                        <img src='<%= ResolveUrl("~/images/carousel/christmas-01.jpg") %>' class="img-fluid" width="75%"
                                            alt="Christmas" />
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-10 col-md-offset-2">
                                        <img src='<%= ResolveUrl("~/images/carousel/car-booking-02.jpg") %>' class="img-fluid" width="75%"
                                            alt="Car Booking" />
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-10 col-md-offset-2">
                                        <img src='<%= ResolveUrl("~/images/carousel/reset-password-03.jpg") %>' class="img-fluid"
                                            width="75%" alt="Reset Password" />
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-10 col-md-offset-2">
                                        <img src='<%= ResolveUrl("~/images/carousel/room-booking-3.jpg") %>' class="img-fluid" width="75%"
                                            alt="Room Booking" />
                                    </div>
                                </div>
                            </div>



                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>

                    </div>
                        
                       </div>

                    <div class="col-md-10 col-md-offset-1">
                                   <div class="col-md-4" style="padding-left: 0px;"></div>
                                   <div class="col-md-1 col-md-offset-11" style="padding-right: 0px;">

                                   <div class="form-group">
                <asp:LinkButton ID="lbCreatetab1" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdreset"><i class="fas fa-long-arrow-alt-left"></i></asp:LinkButton>
                                       </div>
                                       </div>
            </div>

                    <div class="form-group">
                    <div class="col-md-3 col-md-offset-1"> 
                        <div class="list-group"> 
                            <a href="#" class="list-group-item list-group-item-info">ประเภทข่าวสาร</a>
                            <asp:LinkButton ID="tab0" runat="server" OnCommand="btnCommand" CommandName="tab0onclick"
                            class="list-group-item">กฎหมาย
                            </asp:LinkButton>
                            <a href="#" class="list-group-item">โครงสร้างองค์กร</a>
                            <a href="#" class="list-group-item">ข้อมูลติดต่อสื่อสาร</a>
                        </div>
                          </div> 
                    <div class="col-md-7" width: 58.55555%>
                        <a href="#" class="list-group-item list-group-item-info">หัวข้อข่าว</a>
                            <asp:LinkButton ID="LinkButton3" runat="server" OnCommand="btnCommand" CommandName="tab0onclick2"
                            class="list-group-item">กฎหมายแรงงานปี 2564
                            </asp:LinkButton>
                        </div>
                  
                    </div>
                   

                    <%--<div class="list-group">
                        <div class="col-sm-3">
                        <asp:LinkButton ID="LinkButton1" runat="server" BackColor="#58ACFA" Font-Bold="true" ForeColor="White" CssClass="list-group-item">ประเภทข่าวสาร</asp:LinkButton>
                        <asp:Repeater ID="rplistnews" runat="server">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnmenuNewsPR" runat="server" data-toggle="tooltip" data-placement="top" title=""
                                    CommandName="cmdReadMore" OnCommand="btnCommand"
                                    CommandArgument='0' BorderColor="SkyBlue" CssClass="list-group-item btn-success">
                               <p class="card-title" style="font-size: 1.2em"><i class="fa fa-tags" aria-hidden="true"></i> XXX</h5>
                                  <p class="list-group-item-text">&nbsp; &nbsp; &nbsp; AAA</p>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:Repeater>


                    </div>
                        </div>--%>
                </InsertItemTemplate>
            </asp:FormView>
        </asp:View>


            <asp:View ID="tab0onclick2" runat="server">
            <asp:FormView ID="Viewtab02" runat="server" Width="100%">
                <InsertItemTemplate>

                    
                    <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                <!-- <li data-target="#carousel-example-generic" data-slide-to="4"></li> -->
                                <!-- <li data-target="#carousel-example-generic" data-slide-to="5"></li> -->
                                <!-- <li data-target="#carousel-example-generic" data-slide-to="6"></li> -->
                            </ol>
                            <!-- Wrapper for slides -->

                            <div class="carousel-inner" role="listbox">

                                <div class="item active">
                                    <div class="col-md-10 col-md-offset-2">
                                        <img src='<%= ResolveUrl("~/images/carousel/christmas-01.jpg") %>' class="img-fluid" width="75%"
                                            alt="Christmas" />
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-10 col-md-offset-2">
                                        <img src='<%= ResolveUrl("~/images/carousel/car-booking-02.jpg") %>' class="img-fluid" width="75%"
                                            alt="Car Booking" />
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-10 col-md-offset-2">
                                        <img src='<%= ResolveUrl("~/images/carousel/reset-password-03.jpg") %>' class="img-fluid"
                                            width="75%" alt="Reset Password" />
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-10 col-md-offset-2">
                                        <img src='<%= ResolveUrl("~/images/carousel/room-booking-3.jpg") %>' class="img-fluid" width="75%"
                                            alt="Room Booking" />
                                    </div>
                                </div>
                            </div>



                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>

                    </div>
                        
                       </div>



                    <div class="form-group">
                    <div class="col-md-3 col-md-offset-1"> 
                        <div class="list-group"> 
                            <a href="#" class="list-group-item list-group-item-info">ประเภทข่าวสาร</a>
                            <asp:LinkButton ID="tab0" runat="server" OnCommand="btnCommand" CommandName="tab0onclick"
                            class="list-group-item">กฎหมาย
                            </asp:LinkButton>
                            <a href="#" class="list-group-item">โครงสร้างองค์กร</a>
                            <a href="#" class="list-group-item">ข้อมูลติดต่อสื่อสาร</a>
                        </div>
                          </div> 
                       
                       
                    <div class="col-md-7">
                        
                        <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="Table1" runat="server" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 10%;">ชื่อประเภทข่าวสาร</th>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>กฎหมายแรงงานปี 2564 ..........................................................................................................................................................................................................................................</td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>โครงสร้างองค์กรปี 2562 เริ่มตั้งแต่วันที่ .................................................................................................................................................................................................................................................................................................................</td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>เบอร์ติดต่อ ฝ่าย XXX เริ่มตั้งแต่วันที่ .........................................................................................................................................................................................</td>
                            </tr>

                        </tbody>
                    </table>
                            
                        </div>
                      </div>
                    
                    
                   

                    <%--<div class="list-group">
                        <div class="col-sm-3">
                        <asp:LinkButton ID="LinkButton1" runat="server" BackColor="#58ACFA" Font-Bold="true" ForeColor="White" CssClass="list-group-item">ประเภทข่าวสาร</asp:LinkButton>
                        <asp:Repeater ID="rplistnews" runat="server">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnmenuNewsPR" runat="server" data-toggle="tooltip" data-placement="top" title=""
                                    CommandName="cmdReadMore" OnCommand="btnCommand"
                                    CommandArgument='0' BorderColor="SkyBlue" CssClass="list-group-item btn-success">
                               <p class="card-title" style="font-size: 1.2em"><i class="fa fa-tags" aria-hidden="true"></i> XXX</h5>
                                  <p class="list-group-item-text">&nbsp; &nbsp; &nbsp; AAA</p>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:Repeater>


                    </div>
                        </div>--%>
                </InsertItemTemplate>
            </asp:FormView>


        </asp:View>

        <asp:View ID="viewtest" runat="server">

            <asp:FormView ID="fvtest" runat="server">
                <InsertItemTemplate>
                        <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                <!-- <li data-target="#carousel-example-generic" data-slide-to="4"></li> -->
                                <!-- <li data-target="#carousel-example-generic" data-slide-to="5"></li> -->
                                <!-- <li data-target="#carousel-example-generic" data-slide-to="6"></li> -->
                            </ol>
                            <!-- Wrapper for slides -->

                            <div class="carousel-inner" role="listbox">

                                <div class="item active">
                                    <div class="col-md-10 col-md-offset-2">
                                        <img src='<%= ResolveUrl("~/images/carousel/christmas-01.jpg") %>' class="img-fluid" width="75%"
                                            alt="Christmas" />
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-10 col-md-offset-2">
                                        <img src='<%= ResolveUrl("~/images/carousel/car-booking-02.jpg") %>' class="img-fluid" width="75%"
                                            alt="Car Booking" />
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-10 col-md-offset-2">
                                        <img src='<%= ResolveUrl("~/images/carousel/reset-password-03.jpg") %>' class="img-fluid"
                                            width="75%" alt="Reset Password" />
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="col-md-10 col-md-offset-2">
                                        <img src='<%= ResolveUrl("~/images/carousel/room-booking-3.jpg") %>' class="img-fluid" width="75%"
                                            alt="Room Booking" />
                                    </div>
                                </div>
                            </div>



                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>

                    </div>
                        
                       </div>
                    <label class="col-md-1"></label>

                               <div class="col-md-10 col-md-offset-1">
                                   <div class="col-md-4" style="padding-left: 0px;"></div>
                                   <div class="col-md-1 col-md-offset-11" style="padding-right: 0px;">

                                   <div class="form-group">
                <asp:LinkButton ID="lbCreatetab1" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdre"><i class="fas fa-long-arrow-alt-left"></i></asp:LinkButton>
                                       </div>
                                       </div>
            </div>


                    <div class="col-md-10 col-md-offset-1">
                        <div class="col-md-4" style="padding-left: 0px;">
                        <div class="list-group"> 
                            <a href="#" class="list-group-item list-group-item-info">ประเภทข่าวสาร</a>
                            <asp:LinkButton ID="tab0" runat="server" OnCommand="btnCommand" CommandName="tab0onclick"
                            class="list-group-item">กฎหมาย
                            </asp:LinkButton>
                            <a href="#" class="list-group-item">โครงสร้างองค์กร</a>
                            <a href="#" class="list-group-item">ข้อมูลติดต่อสื่อสาร</a>
                        </div>
                             </div>
                        <div class="col-md-8" style="padding-right: 0px;">

   <%--                         <div class="list-group"> 
                            <a class="list-group-item list-group-item-info">กฎหมายแรงงานปี 2564</a>
                        </div>--%>
                            <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">กฎหมายแรงงานปี 2564</h3>
                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">

                                <div class="form-horizontal" role="form">

                                    <div class="panel-heading">
                            <asp:Label ID="lbtext" runat="server">
                                กฎหมายแรงงานปี 2564  XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX XXXXX 
                                <br>
                                <center>
                                <img src='<%= ResolveUrl("~/images/carousel/christmas-01.jpg") %>' class="img-fluid" width="75%"
                                            alt="Room Booking" />
                                    </center>
                            </asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                    </div>
                        <%--<div class="col-md-1"></div>--%>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>
        </asp:View>


            <asp:View ID="tab3" runat="server">
                <asp:FormView ID="uploadimg" runat="server">
                <InsertItemTemplate>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                    <h3 class="panel-title">เพิ่ม</h3>

                </div>

                <div class="panel-body">
                    <div class="panel-heading">

                        <div class="form-horizontal" role="form">

                            <div class="panel-heading">

                                
 

                                 <div class="form-group">
                                     <asp:Label ID="label3" runat="server" class="col-sm-3 control-label">รูปภาพ
                                            <span class="text-danger"></span></asp:Label>
                                    <div class="col-sm-6">
                        
                                        <asp:FileUpload ID="FileUpload1" runat="server" />
                                     </div>
                                     <div class="col-sm-3">
                                    </div>
                                 </div>

                                <div class="form-group">
                                    <asp:Label ID="lbstatus" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddStatus" CssClass="form-control" runat="server">
                                            <asp:ListItem Value="1" Text="Online" />
                                            <asp:ListItem Value="0" Text="Offline" />
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0">
                                                <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;Save</asp:LinkButton>

                                        <asp:LinkButton ID="LinkButton2" CssClass="btn btn-danger" runat="server" OnCommand="btnCommand" CommandName="cmdCancel" CausesValidation="false">
                                                    <i class="fa fa-times" aria-hidden="true"></i>&nbsp;Cancel
                                        </asp:LinkButton>
                                    </div>
                                    <label class="col-sm-3 control-label"></label>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

<%-- upload--%>
<%--            <div id="master_meterail" runat="server">
                <div class="form-horizontal" role="form">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <asp:FileUpload ID="upload_file_material" runat="server" />
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            
                            <div class="pull-left">
                                <asp:LinkButton ID="btnupload" CssClass="btn btn-success" runat="server" ValidationGroup="Import" OnCommand="btnCommand" CommandName="btnuploadimg" data-toggle="tooltip" title="Import" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')"><span class="glyphicon glyphicon-file"></span> Upload</asp:LinkButton>
                            </div>

                        </div>
                    </div>
                </div>
            </div>--%>
                        
                    </InsertItemTemplate>
                </asp:FormView>
             </asp:View>


         <asp:View ID="tabuploadimg" runat="server">

                         <div class="form-group">
                <asp:LinkButton ID="LinkButton14" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdCreateimg"><i class="fa fa-plus" aria-hidden="true"></i></asp:LinkButton>
            </div>

            <asp:FormView ID="upload" runat="server" Width="100%">
                <InsertItemTemplate>
                        <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="Table1" runat="server" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 10%;">#</th>
                                <%--<th scope="col" style="width: 20%;">ชื่อรูปภาพ</th>--%>
                                <th scope="col" style="width: 30%;">รูป</th>
                                <th scope="col" style="width: 20%;">สถานะ</th>
                                <th scope="col" style="width: 20%;">การจัดการ</th>

                            </tr>
                            <tr style="font-size: Small;">
                                <td>1</td>
                                <%--<td>กฎหมาย</td>--%>
                                <td><img src='<%= ResolveUrl("~/images/carousel/christmas-01.jpg") %>' width="100%" alt="Car Booking"/></td>
                                <td>online</td>
                                <td>
                                <asp:LinkButton ID="LinkButton21" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton288" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>

                                </td>

                            </tr>
                            <tr style="font-size: Small;">
                                <td>2</td>
                                <%--<td>โครงสร้างองค์กร</td>--%>
                                <td><img src='<%= ResolveUrl("~/images/carousel/car-booking-02.jpg") %>' width="100%" alt="Car Booking"/></td>
                                <td>online</td>
                                                            <td>
                                <asp:LinkButton ID="LinkButton4" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton5" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>

                            </td>
                            </tr>
                            <tr style="font-size: Small;">
                                <td>3</td>
                                <%--<td>ข้อมูลติดต่อสื่อสาร</td>--%>
                                <td><img src='<%= ResolveUrl("~/images/carousel/reset-password-03.jpg") %>' width="100%" alt="Car Booking"/></td>
                                <td>online</td>
                                                            <td>
                                <asp:LinkButton ID="LinkButton6" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton7" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>

                            </td>

                            </tr>

                            <tr style="font-size: Small;">
                                <td>4</td>
                                <%--<td>ข้อมูลติดต่อสื่อสาร</td>--%>
                                <td><img src='<%= ResolveUrl("~/images/carousel/room-booking-3.jpg") %>' width="100%" alt="Car Booking"/></td>
                                <td>online</td>
                                                            <td>
                                <asp:LinkButton ID="LinkButton12" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton13" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>

                            </td>

                            </tr>

                        </tbody>
                    </table>
                    </div>
                            </div>
                        
                </InsertItemTemplate>
            </asp:FormView>
        </asp:View>

                   <%-- <asp:View ID="tab4" runat="server">
                <asp:FormView ID="uploadimgMain" runat="server">
                <InsertItemTemplate>

            <div id="master_meterail" runat="server">
                <div class="form-horizontal" role="form">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <asp:FileUpload ID="upload_file_material" runat="server" />
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <div class="col-sm-12"></div>
                            <div class="pull-left">
                                <asp:LinkButton ID="btnupload" CssClass="btn btn-success" runat="server" ValidationGroup="Import" OnCommand="btnCommand" CommandName="uploadimgMain" data-toggle="tooltip" title="Import" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')"><span class="glyphicon glyphicon-file"></span> Upload</asp:LinkButton>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

                    </InsertItemTemplate>
                </asp:FormView>
             </asp:View>--%>


                 <asp:View ID="tabuploadimgMain" runat="server">
                                 <div class="form-group">
                <asp:LinkButton ID="LinkButton15" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="uploadimgMain"><i class="fa fa-plus" aria-hidden="true"></i></asp:LinkButton>
            </div>
            <asp:FormView ID="uploadMain" runat="server" Width="100%">

                <InsertItemTemplate>
                        <div class="col-md-10 col-md-offset-1">
                        <div class="form-group">
                    <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" rules="all" border="1" id="Table1" runat="server" style="border-collapse: collapse;">
                        <tbody>
                            <tr class="info" style="font-size: Small; height: 40px;">
                                <th scope="col" style="width: 10%;">#</th>
                                <%--<th scope="col" style="width: 20%;">ชื่อรูปภาพ</th>--%>
                                <th scope="col" style="width: 30%;">รูป</th>
                                <th scope="col" style="width: 20%;">สถานะ</th>
                                <th scope="col" style="width: 20%;">การจัดการ</th>

                            </tr>
                            <tr style="font-size: Small;">
                                <td>1</td>
                                <%--<td>กฎหมาย</td>--%>
                                <td><img src='<%= ResolveUrl("~/images/preload/RamaX.jpg") %>' width="100%" alt="Car Booking"/></td>
                                <td>online</td>
                                <td>
                                <asp:LinkButton ID="LinkButton21" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton288" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>

                                </td>

                            </tr>
                            <tr style="font-size: Small;">
                                <td>2</td>
                                <%--<td>โครงสร้างองค์กร</td>--%>
                                <td><img src='<%= ResolveUrl("~/images/carousel/car-booking-02.jpg") %>' width="100%" alt="Car Booking"/></td>
                                <td>offline</td>
                                                            <td>
                                <asp:LinkButton ID="LinkButton4" CssClass="btn-sm btn-default" runat="server" data-original-title="Edit" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></asp:LinkButton>

                                <asp:LinkButton ID="LinkButton5" CssClass="btn-sm btn-default" runat="server" data-original-title="Delete" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSave" CommandArgument=""><i class="glyphicon glyphicon-trash" aria-hidden="true"></i></asp:LinkButton>

                            </td>
                            </tr>


                        </tbody>
                    </table>
                    </div>
                            </div>
                        
                </InsertItemTemplate>
            </asp:FormView>
        </asp:View>


    </asp:MultiView>

</asp:Content>

