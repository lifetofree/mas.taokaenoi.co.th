﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="labis_details.aspx.cs" Inherits="websystem_qa_labis_details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
     <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:FormView ID="fvSampleCode" runat="server" Width="100%">
        <ItemTemplate>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">รายละเอียดรายการที่ตรวจ</h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Sample Name</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="tbmaterial_name_view" runat="server" Visible="true" CssClass="form-control" Text='<%# Eval("material_name") %>' Enabled="false" />
                                <asp:TextBox ID="tbu0_qalab_idx_view" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("u0_qalab_idx") %>' Enabled="false" />
                                <asp:TextBox ID="tbu1_qalab_idx_view" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("u1_qalab_idx") %>' Enabled="false" />
                            </div>
                            <label class="col-sm-2 control-label">Sample Code</label>
                            <div class="col-sm-4">
                                <asp:TextBox ID="tbsample_code" runat="server" CssClass="form-control" Text='<%# Eval("sample_code") %>' Enabled="false" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">รายการที่สั่งตรวจ</label>
                            <div class="col-sm-4">
                                <div class="panel-body">
                                    <table class="table">
                                        <asp:Repeater ID="rp_testdetail_lab_view" runat="server">
                                            <HeaderTemplate>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# (Container.ItemIndex + 1) %></td>

                                                    <td><%# Eval("test_detail_name") %></td>
                                                    <asp:Label ID="u2_labtest_detailidx_view" runat="server" Text='<%# Eval("test_detail_idx") %>' Visible="false"></asp:Label>

                                                    <asp:Label ID="u2_lab_detail_view" runat="server" Text='<%# Eval("u2_qalab_idx") %>' Visible="false"></asp:Label>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </div>
                            <label class="col-sm-6 control-label"></label>
                        </div>
                    </div>
                </div>
            </div>
        </ItemTemplate>
    </asp:FormView>


</asp:Content>

