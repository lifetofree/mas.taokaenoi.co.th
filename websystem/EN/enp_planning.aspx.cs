﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_EN_enp_planning : System.Web.UI.Page
{

    #region Connect

    service_execute serviceexcute = new service_execute();
    data_employee _dtEmployee = new data_employee();
    function_tool _funcTool = new function_tool();
    data_en_planning _dtenplan = new data_en_planning();
    DataMachine _dtmachine = new DataMachine();
    data_employee _dataEmployee = new data_employee();
    function_dmu _func_dmu = new function_dmu();


    string _localJson = String.Empty;

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    int[] secit = { 210 };
    int[] depten = { 11, 134, 1146 };
    string u0idx;
    string _link;
    int para = 1;
    int i = 1;
    static string keyenrepair = ConfigurationManager.AppSettings["keyenrepair"];


    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlSelect_TypeMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeMachineItem"];
    static string _urlSelect_GroupCodeMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_GroupCodeMachine"];
    static string _urlSelect_Group = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Group_Machine"];
    static string urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Location"];
    static string urlSelect_Building = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Building"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlSelect_Room = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Room"];
    static string _urlSelectLocationFormResult = _serviceUrl + ConfigurationManager.AppSettings["urlSelectLocationFormResult"];
    static string _urlSelectDoc_Machine = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDoc_Machine"];
    static string _urlSelect_Time_Machine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Time_Machine"];
    static string _urlSelectDoc_PM = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDoc_PM"];
    static string _urlSelectFormtoAddingResult = _serviceUrl + ConfigurationManager.AppSettings["urlSelectFormtoAddingResult"];
    static string _urlSelectStatusFormtoAddingResult = _serviceUrl + ConfigurationManager.AppSettings["urlSelectStatusFormtoAddingResult"];
    static string _urlSelectMachineFormResult = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMachineFormResult"];
    static string _urlAddingResult = _serviceUrl + ConfigurationManager.AppSettings["urlAddingResult"];
    static string _urlSelectIndexManagePlan = _serviceUrl + ConfigurationManager.AppSettings["urlSelectIndexManagePlan"];
    static string _urlSelectEditPlan = _serviceUrl + ConfigurationManager.AppSettings["urlSelectEditPlan"];
    static string _urlEditPlan = _serviceUrl + ConfigurationManager.AppSettings["urlEditPlan"];
    static string _urlDeletePlan = _serviceUrl + ConfigurationManager.AppSettings["urlDeletePlan"];

    

    // plan
    static string _urlInserttDoc_Planning = _serviceUrl + ConfigurationManager.AppSettings["urlInserttDoc_Planning"];
    static string _urlGeten_report = _serviceUrl + ConfigurationManager.AppSettings["urlGeten_report"];



    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            select_empIdx_present();
            SetDefaultIndex();
        }
        linkBtnTrigger(_divMenuBtnToDivIndex);
        linkBtnTrigger(_divMenuBtnToDivMange);
        linkBtnTrigger(_divMenuBtnToDivAdd);
        linkBtnTrigger(_divMenuBtnToDivApprove);
        linkBtnTrigger(_divMenuBtnToDivAddResult);

    }
    #endregion

    #region Planning By P'cake

    public void getTitle()
    {
        string sName = "";
        data_en_planning dataenplanning = new data_en_planning();
        dataenplanning = _func_dmu.zENLookupDataList(dataenplanning, "org_en_plan");
        litTitle1.Text = "";
        if (dataenplanning.en_lookup_action != null)
        {
            litTitle1.Text = dataenplanning.en_lookup_action[0].zName;
        }
        litTitle2.Text = "แบบฟอร์มใบบันทึก แผนการทำ PM เครื่องจักรประจำปี " + (_func_dmu.zStringToInt(ddlYearSearch_plan_pm.SelectedValue) + 543).ToString();
        if (ddlbuilding_search.SelectedValue != "0")
        {
            sName = ddlbuilding_search.Items[ddlbuilding_search.SelectedIndex].Text;
        }
        litTitle3.Text = "(Preventive Maintenance Program) เครื่องจักรในกระบวนการผลิต" + sName;

        string sLine = "...............................";
        string sDine_date = "วันที่...../...../.....";

        litapp_t1_1.Text = "ผู้จัดทำ" + sLine;
        litapp_t1_2.Text = "ผู้จัดการแผนกซ่อมบำรุง";
        litapp_t1_3.Text = sDine_date;

        litapp_t2_1.Text = "ผู้ตรวจสอบ" + sLine;
        litapp_t2_2.Text = "ผู้จัดการฝ่ายวิศวกรรม";
        litapp_t2_3.Text = sDine_date;

        litapp_t3_1.Text = "ผู้ตรวจสอบ" + sLine;
        litapp_t3_2.Text = "ผู้อำนวยการฝ่ายผลิต";
        litapp_t3_3.Text = sDine_date;

        litapp_t4_1.Text = "ผู้อนุมัติ" + sLine;
        litapp_t4_2.Text = "ประธานเจ้าหน้าที่ฝ่ายปฎิบัติการ";
        litapp_t4_3.Text = sDine_date;

        dataenplanning = new data_en_planning();
        dataenplanning = _func_dmu.zENLookupDataList(dataenplanning, "iso_plan_year");
        litiso.Text = "";
        if (dataenplanning.en_lookup_action != null)
        {
            litiso.Text = dataenplanning.en_lookup_action[0].zName;
        }


    }

    private void Page_Init(object sender, EventArgs e)
    {
        ClearApplicationCache();
    }

    #region Action

    public void ClearApplicationCache()
    {
        List<string> keys = new List<string>();
        // retrieve application Cache enumerator
        IDictionaryEnumerator enumerator = Cache.GetEnumerator();
        // copy all keys that currently exist in Cache
        while (enumerator.MoveNext())
        {
            keys.Add(enumerator.Key.ToString());
        }

        // delete every key from cache
        for (int i = 0; i < keys.Count; i++)
        {
            Cache.Remove(keys[i]);
        }
    }

    #endregion Action

    #region zMonth title
    public string zMonth(int id)
    {
        return _func_dmu.zMonthEN(id);
    }
    #endregion

    public string zMonthEN(int AMonth)
    {

        return _func_dmu.zMonthEN(AMonth);
    }
    public string getHtml_RptPlanPM(string sEmpty = "")
    {
        getTitle();

        string sHtml = getzMC();
        return sHtml;
    }

    public string getzMC()
    {
        // getTitle();
        data_en_planning dataenplanning = new data_en_planning();
        dataenplanning.en_report_action = new en_report[1];
        en_report obj_rpt = new en_report();
        //obj_trn_plan.filter_keyword = txtFilterKeyword_rpt_SCHED.Text;

        obj_rpt.zyear = _func_dmu.zStringToInt(ddlYearSearch_plan_pm.SelectedValue);
        obj_rpt.LocIDX = _func_dmu.zStringToInt(ddllocate_search.SelectedValue);
        obj_rpt.TmcIDX = _func_dmu.zStringToInt(ddltypemachine_search.SelectedValue);
        obj_rpt.TCIDX = _func_dmu.zStringToInt(ddltypecode_search.SelectedValue);
        obj_rpt.BuildingIDX = _func_dmu.zStringToInt(ddlbuilding_search.SelectedValue);

        obj_rpt.operation_status_id = "plan_year_mc";
        dataenplanning.en_report_action[0] = obj_rpt;
        //  txt.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataenplanning));
        dataenplanning = _func_dmu.zCallServicePostENPlanning(_urlGeten_report, dataenplanning);
        string sMCCode = "";
        string sHtml = "", sRow = "";
        int ic = 0;
        int irowspan = 0;
        if (dataenplanning.en_report_action != null)
        {
            pnl_alert.Visible = false;
            pnl_print_plan.Visible = true;
            foreach (var item in dataenplanning.en_report_action)
            {

                if (sMCCode != item.zMCCode)
                {
                    irowspan = 0;
                    ic++;
                }
                sRow = getHtml(item.zMCCode, item.idx, item.TCIDX, ic, item.zCount, irowspan);

                sHtml = sHtml + sRow;
                sMCCode = item.zMCCode;
                irowspan++;
            }
        }
        else
        {
            pnl_alert.Visible = true;
            pnl_print_plan.Visible = false;
        }

        return sHtml;
    }
    public string getHtml(string zMCCode, int _im0tidx, int _tcidx, int _icount, int _irowspan, int _irow)
    {
        data_en_planning dataenplanning = new data_en_planning();
        dataenplanning.en_report_action = new en_report[1];
        en_report obj_rpt = new en_report();
        obj_rpt.zyear = _func_dmu.zStringToInt(ddlYearSearch_plan_pm.SelectedValue);
        obj_rpt.LocIDX = _func_dmu.zStringToInt(ddllocate_search.SelectedValue);
        obj_rpt.TmcIDX = _func_dmu.zStringToInt(ddltypemachine_search.SelectedValue);
        // obj_rpt.TCIDX = _func_dmu.zStringToInt(ddltypecode_search.SelectedValue);
        obj_rpt.TCIDX = _tcidx;
        obj_rpt.BuildingIDX = _func_dmu.zStringToInt(ddlbuilding_search.SelectedValue);
        obj_rpt.zMCCode = zMCCode;
        obj_rpt.idx = _im0tidx;
        obj_rpt.operation_status_id = "plan_year";
        dataenplanning.en_report_action[0] = obj_rpt;
        // litDebug.Text =  HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataenplanning));
        dataenplanning = _func_dmu.zCallServicePostENPlanning(_urlGeten_report, dataenplanning);
        string sName = "", sHtml = "", sHtml1 = "", sHtml2 = "", sHtml3 = "", sRow;
        int ic = 0, iMCCode = 0;
        string sClone = "", sMCCode = "", sMCNameTH = "";

        if (dataenplanning.en_report_action != null)
        {
            foreach (var item in dataenplanning.en_report_action)
            {

                ic++;
                sMCNameTH = item.MCNameTH;
                /*
                if (ic == 1)
                {
                    if((item.MCCode != null) && (item.MCCode != ""))
                    {
                        sMCCode = item.MCCode;
                    }
                    
                }
                else
                {
                    if(sMCCode != "")
                    {
                        sMCCode = sMCCode + " , " + item.MCCode;
                    }
                    else
                    {
                        sMCCode = item.MCCode;
                    }
                }


                iMCCode = item.MCIDX;
                */

            }
            /*
            if ((_irowspan > 0) && (_irow == 0))
            {
                sHtml1 = "<td rowspan = " + _irowspan.ToString() + " style=\"font-size: 12px;font-family:serif;padding: 1px 0px;border: 1px solid #ddd;\" ><center> " + _icount.ToString() + " </center></td>";
                 sHtml1 = sHtml1 + "<td rowspan = " + _irowspan.ToString() + " style=\"font-size: 12px;font-family:serif;padding: 1px 0px;text-align: left;border: 1px solid #ddd;\" > " + sMCNameTH +" </td>";
               // sHtml1 = sHtml1 + "<td style=\"font-size: 12px;font-family:serif;padding: 1px 0px;text-align: left;border: 1px solid #ddd;\" > " + sMCNameTH + " </td>";
                sHtml1 = sHtml1 + "<td style=\"font-size: 11px;font-family:serif;padding: 1px 0px;text-align: left;border: 1px solid #ddd;\" > 1= " + sMCCode +  " </td>";

            }
            else if ((_irowspan > 0) && (_irow > 0))
            {
                sHtml1 = "<td style=\"font-size: 11px;font-family:serif;padding: 1px 0px;text-align: left;border: 1px solid #ddd;\" > 2=" + sMCCode + " </td>";

            }
            else
            {

                sHtml1 = "<td style=\"font-size: 12px;font-family:serif;padding: 1px 0px;border: 1px solid #ddd;\" ><center> " + _icount.ToString() + " </center></td>";
                sHtml1 = sHtml1 + "<td style=\"font-size: 12px;font-family:serif;padding: 1px 0px;text-align: left;border: 1px solid #ddd;\" > " + sMCNameTH + " </td>";
                sHtml1 = sHtml1 + "<td style=\"font-size: 11px;font-family:serif;padding: 1px 0px;text-align: left;border: 1px solid #ddd;\" > 3= " + sMCCode + " </td>";

            }
            */
            
            if(_irow == 0) // row แรก ของ กลุ่มเครื่องจักร
            {
                if(_irowspan > 0) // มีมากกว่า 1 แผน ให้ rowspan
                {
                    sHtml1 = "<td rowspan = " + _irowspan.ToString() + " style=\"font-size: 12px;font-family:serif;padding: 1px 0px;border: 1px solid #ddd;\" ><center> " + _icount.ToString() + " </center></td>";
                    sHtml1 = sHtml1 + "<td rowspan = " + _irowspan.ToString() + " style=\"font-size: 12px;font-family:serif;padding: 1px 0px;text-align: left;border: 1px solid #ddd;\" > " + sMCNameTH + " </td>";

                }
                else
                {
                    sHtml1 = "<td style=\"font-size: 12px;font-family:serif;padding: 1px 0px;border: 1px solid #ddd;\" ><center> " + _icount.ToString() + " </center></td>";
                    sHtml1 = sHtml1 + "<td style=\"font-size: 12px;font-family:serif;padding: 1px 0px;text-align: left;border: 1px solid #ddd;\" > " + sMCNameTH + " </td>";
                }
            }
            else // ไม่ใช่ row แรก ของ กลุ่มเครื่องจักร
            {
                if (_irowspan > 0) // มีมากกว่า 1 แผน ให้ rowspan
                {
                    sHtml1 = "";
                    sHtml1 = sHtml1 + "";
                }
                else
                {
                    sHtml1 = "<td style=\"font-size: 12px;font-family:serif;padding: 1px 0px;border: 1px solid #ddd;\" ><center> " + _icount.ToString() + " </center></td>";
                    sHtml1 = sHtml1 + "<td style=\"font-size: 12px;font-family:serif;padding: 1px 0px;text-align: left;border: 1px solid #ddd;\" > " + sMCNameTH + " </td>";
                }

            }

            // sHtml1 = sHtml1 + "<td style=\"font-size: 11px;font-family:serif;padding: 1px 0px;text-align: left;border: 1px solid #ddd;\" > 1= " + sMCCode + " </td>";


            sHtml2 = "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;border: 1px solid #ddd;\" ><center>  </center></td>";
            sHtml2 = sHtml2 + "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;border: 1px solid #ddd;\" >  </td>";
            // sHtml2 = sHtml2 + "<td style=\"font-size: 12px;font-family:serif;padding: 5px 0px;border: 1px solid #ddd;\" > 4= </td>";

            sRow = getu0idx(iMCCode, _im0tidx, _tcidx, sHtml1, sHtml2, _irowspan);
            sHtml = sHtml + sRow;
        }

        return sHtml;
    }

    public string getCollum(int id, int iMCIDX, int _tcidx, string sName)
    {
        StringBuilder _StringBuilder = new StringBuilder();
        _StringBuilder.Clear();

        data_en_planning dataenplanning = new data_en_planning();
        dataenplanning.en_report_action = new en_report[1];
        en_report obj_rpt = new en_report();
        obj_rpt.zyear = _func_dmu.zStringToInt(ddlYearSearch_plan_pm.SelectedValue);
        obj_rpt.LocIDX = _func_dmu.zStringToInt(ddllocate_search.SelectedValue);
        obj_rpt.TmcIDX = _func_dmu.zStringToInt(ddltypemachine_search.SelectedValue);
        // obj_rpt.TCIDX = _func_dmu.zStringToInt(ddltypecode_search.SelectedValue);
        obj_rpt.TCIDX = _tcidx;
        obj_rpt.BuildingIDX = _func_dmu.zStringToInt(ddlbuilding_search.SelectedValue);
        obj_rpt.idx = id;
        obj_rpt.MCIDX = iMCIDX;
        obj_rpt.operation_status_id = "plan_year_detail";
        dataenplanning.en_report_action[0] = obj_rpt;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataenplanning));
        dataenplanning = _func_dmu.zCallServicePostENPlanning(_urlGeten_report, dataenplanning);
        //dataenplanning.en_report_action != null
        if (dataenplanning.en_report_action != null)
        {

            string January_1 = "", January_2 = "", January_3 = "", January_4 = ""; //1
            string February_1 = "", February_2 = "", February_3 = "", February_4 = "";//2
            string March_1 = "", March_2 = "", March_3 = "", March_4 = "";//3
            string April_1 = "", April_2 = "", April_3 = "", April_4 = "";//4
            string May_1 = "", May_2 = "", May_3 = "", May_4 = "";//5
            string June_1 = "", June_2 = "", June_3 = "", June_4 = "";//6
            string July_1 = "", July_2 = "", July_3 = "", July_4 = "";//7
            string August_1 = "", August_2 = "", August_3 = "", August_4 = "";//8
            string September_1 = "", September_2 = "", September_3 = "", September_4 = "";//9
            string October_1 = "", October_2 = "", October_3 = "", October_4 = "";//10
            string November_1 = "", November_2 = "", November_3 = "", November_4 = "";//11
            string December_1 = "", December_2 = "", December_3 = "", December_4 = "";//12

            foreach (var item in dataenplanning.en_report_action)
            {
                string sField = item.zmonth.ToString() + "_" + item.zweek.ToString();
                string sData = sName;// item.zName;
                if (sField == "1_1")//1
                {
                    January_1 = sData;
                }
                else if (sField == "1_2")
                {
                    January_2 = sData;
                }
                else if (sField == "1_3")
                {
                    January_3 = sData;
                }
                else if (sField == "1_4")
                {
                    January_4 = sData;
                }//2
                else if (sField == "2_1")
                {
                    February_1 = sData;
                }
                else if (sField == "2_2")
                {
                    February_2 = sData;
                }
                else if (sField == "2_3")
                {
                    February_3 = sData;
                }
                else if (sField == "2_4")
                {
                    February_4 = sData;
                }//3
                else if (sField == "3_1")
                {
                    March_1 = sData;
                }
                else if (sField == "3_2")
                {
                    March_2 = sData;
                }
                else if (sField == "3_3")
                {
                    March_3 = sData;
                }
                else if (sField == "3_4")
                {
                    March_4 = sData;
                }//4
                else if (sField == "4_1")
                {
                    April_1 = sData;
                }
                else if (sField == "4_2")
                {
                    April_2 = sData;
                }
                else if (sField == "4_3")
                {
                    April_3 = sData;
                }
                else if (sField == "4_4")
                {
                    April_4 = sData;
                }//5
                else if (sField == "5_1")
                {
                    May_1 = sData;
                }
                else if (sField == "5_2")
                {
                    May_2 = sData;
                }
                else if (sField == "5_3")
                {
                    May_3 = sData;
                }
                else if (sField == "5_4")
                {
                    May_4 = sData;
                }//6
                else if (sField == "6_1")
                {
                    June_1 = sData;
                }
                else if (sField == "6_2")
                {
                    June_2 = sData;
                }
                else if (sField == "6_3")
                {
                    June_3 = sData;
                }
                else if (sField == "6_4")
                {
                    June_4 = sData;
                }//7
                else if (sField == "7_1")
                {
                    July_1 = sData;
                }
                else if (sField == "7_2")
                {
                    July_2 = sData;
                }
                else if (sField == "7_3")
                {
                    July_3 = sData;
                }
                else if (sField == "7_4")
                {
                    July_4 = sData;
                }//8
                else if (sField == "8_1")
                {
                    August_1 = sData;
                }
                else if (sField == "8_2")
                {
                    August_2 = sData;
                }
                else if (sField == "8_3")
                {
                    August_3 = sData;
                }
                else if (sField == "8_4")
                {
                    August_4 = sData;
                }//9
                else if (sField == "9_1")
                {
                    September_1 = sData;
                }
                else if (sField == "9_2")
                {
                    September_2 = sData;
                }
                else if (sField == "9_3")
                {
                    September_3 = sData;
                }
                else if (sField == "9_4")
                {
                    September_4 = sData;
                }//10
                else if (sField == "10_1")
                {
                    October_1 = sData;
                }
                else if (sField == "10_2")
                {
                    October_2 = sData;
                }
                else if (sField == "10_3")
                {
                    October_3 = sData;
                }
                else if (sField == "10_4")
                {
                    October_4 = sData;
                }//11
                else if (sField == "11_1")
                {
                    November_1 = sData;
                }
                else if (sField == "11_2")
                {
                    November_2 = sData;
                }
                else if (sField == "11_3")
                {
                    November_3 = sData;
                }
                else if (sField == "11_4")
                {
                    November_4 = sData;
                }//12
                else if (sField == "12_1")
                {
                    December_1 = sData;
                }
                else if (sField == "12_2")
                {
                    December_2 = sData;
                }
                else if (sField == "12_3")
                {
                    December_3 = sData;
                }
                else if (sField == "12_4")
                {
                    December_4 = sData;
                }
            }
            int imonth = 0, icount = 0;
            string sDataWeek = "", sDataWeekAll = "";
            for (int i = 1; i <= 48; i++)
            {
                icount++;
                if (icount == 1)
                {
                    imonth++;
                }
                if (imonth == 1)
                {
                    sDataWeek = getWeek(icount, January_1, January_2, January_3, January_4);
                }
                else if (imonth == 2)
                {
                    sDataWeek = getWeek(icount, February_1, February_2, February_3, February_4);
                }
                else if (imonth == 3)
                {
                    sDataWeek = getWeek(icount, March_1, March_2, March_3, March_4);
                }
                else if (imonth == 4)
                {
                    sDataWeek = getWeek(icount, April_1, April_2, April_3, April_4);
                }
                else if (imonth == 5)
                {
                    sDataWeek = getWeek(icount, May_1, May_2, May_3, May_4);
                }
                else if (imonth == 6)
                {
                    sDataWeek = getWeek(icount, June_1, June_2, June_3, June_4);
                }
                else if (imonth == 7)
                {
                    sDataWeek = getWeek(icount, July_1, July_2, July_3, July_4);
                }
                else if (imonth == 8)
                {
                    sDataWeek = getWeek(icount, August_1, August_2, August_3, August_4);
                }
                else if (imonth == 9)
                {
                    sDataWeek = getWeek(icount, September_1, September_2, September_3, September_4);
                }
                else if (imonth == 10)
                {
                    sDataWeek = getWeek(icount, October_1, October_2, October_3, October_4);
                }
                else if (imonth == 11)
                {
                    sDataWeek = getWeek(icount, November_1, November_2, November_3, November_4);
                }
                else if (imonth == 12)
                {
                    sDataWeek = getWeek(icount, December_1, December_2, December_3, December_4);
                }
                _StringBuilder.AppendLine(sDataWeek);
                if (icount == 4)
                {
                    icount = 0;
                }
            }



        }
        else
        {
            for (int i = 1; i <= 48; i++)
            {
                _StringBuilder.AppendLine("<td style=\"font-family:serif;padding: 0px 0px;border: 1px solid #ddd;\" > ");
                _StringBuilder.AppendLine("<center> </center>");
                _StringBuilder.AppendLine("</td>");
            }
        }

        return _StringBuilder.ToString();
    }

    public string getWeek(int iWeek, string Week_1, string Week_2, string Week_3, string Week_4)
    {
        string sData = "";
        if (iWeek == 1)
        {
            sData = Week_1;
        }
        else if (iWeek == 2)
        {
            sData = Week_2;
        }
        else if (iWeek == 3)
        {
            sData = Week_3;
        }
        else if (iWeek == 4)
        {
            sData = Week_4;
        }

        StringBuilder _StringBuilder = new StringBuilder();
        _StringBuilder.Clear();
        _StringBuilder.AppendLine("<td style=\"font-size: 11px;font-family:serif;padding: 5px 0px;border: 1px solid #ddd;\" > ");
        _StringBuilder.AppendLine("<center> " + sData + " </center>");
        _StringBuilder.AppendLine("</td>");

        return _StringBuilder.ToString();
    }

    public string getu0idx(int iMCIDX, int _m0tidx, int _tcidx, string sHTML1, string sHTML2, int _irowspan)
    {
        StringBuilder _StringBuilder = new StringBuilder();
        _StringBuilder.Clear();

        data_en_planning dataenplanning = new data_en_planning();
        dataenplanning.en_report_action = new en_report[1];
        en_report obj_rpt = new en_report();
        obj_rpt.zyear = _func_dmu.zStringToInt(ddlYearSearch_plan_pm.SelectedValue);
        obj_rpt.LocIDX = _func_dmu.zStringToInt(ddllocate_search.SelectedValue);
        obj_rpt.TmcIDX = _func_dmu.zStringToInt(ddltypemachine_search.SelectedValue);
        // obj_rpt.TCIDX = _func_dmu.zStringToInt(ddltypecode_search.SelectedValue);
        obj_rpt.TCIDX = _tcidx;
        obj_rpt.BuildingIDX = _func_dmu.zStringToInt(ddlbuilding_search.SelectedValue);
        obj_rpt.MCIDX = iMCIDX;
        obj_rpt.idx = _m0tidx;
        obj_rpt.operation_status_id = "plan_year_u0idx";
        dataenplanning.en_report_action[0] = obj_rpt;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(dataenplanning));
        //return "";

        dataenplanning = _func_dmu.zCallServicePostENPlanning(_urlGeten_report, dataenplanning);
        int ic = 0;
        if (dataenplanning.en_report_action != null)
        {
            foreach (var item in dataenplanning.en_report_action)
            {
                string _sql, _MC_Code = "", _HTML = "";

                _MC_Code = item.MCCode;
                _HTML = "<td style=\"font-size: 11px;font-family:serif;padding: 1px 0px;text-align: left;border: 1px solid #ddd;\" > " + _MC_Code + " </td>";

                _sql = getCollum(item.idx, iMCIDX, _tcidx, item.zName);
                ic++;

                if (ic == 1)
                {
                    _StringBuilder.AppendLine("<tr>" + sHTML1 + _HTML + _sql + "</tr>");

                }
                else
                {
                    // _StringBuilder.AppendLine("<tr>" + sHTML2 + _sql + "</tr>");
                    if (_irowspan > 0)
                    {
                        _StringBuilder.AppendLine("<tr>" + _HTML + _sql + "</tr>");
                    }
                    else
                    {
                        _StringBuilder.AppendLine("<tr>" + sHTML2 + _sql + "</tr>");
                    }

                }
            }
        }
        else
        {
            _StringBuilder.AppendLine("<tr>");
            _StringBuilder.AppendLine(sHTML1);
            for (int i = 1; i <= 48; i++)
            {

                _StringBuilder.AppendLine("<td style=\"font-family:serif;padding: 0px 0px;border: 1px solid #ddd;\" > ");
                _StringBuilder.AppendLine("<center> </center>");
                _StringBuilder.AppendLine("</td>");
            }
            _StringBuilder.AppendLine("</tr>");
        }

        return _StringBuilder.ToString();

    }
    #endregion

    #region Select
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

    }

    protected void select_typecodemachine(DropDownList ddlName, int tmcidx)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_TypeCodeList = new TypeCode_Detail[1];
        TypeCode_Detail groupmachine = new TypeCode_Detail();
        groupmachine.TmcIDX = tmcidx;

        _dtenplan.BoxEN_TypeCodeList[0] = groupmachine;

        _dtenplan = callServicePostENPlanning(_urlSelect_Group, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_TypeCodeList, "NameTypecode", "TCIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกรหัสกลุ่มเครื่องจักร...", "0"));

    }

    protected void select_groupmachine(DropDownList ddlName, int tcidx)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_GroupCodeList = new GroupCode_Detail[1];
        GroupCode_Detail groupmachine = new GroupCode_Detail();
        groupmachine.TCIDX = tcidx;

        _dtenplan.BoxEN_GroupCodeList[0] = groupmachine;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtenplan));
        _dtenplan = callServicePostENPlanning(_urlSelect_GroupCodeMachine, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_GroupCodeList, "NameTypecode", "GCIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกรหัสกลุ่ม...", "0"));

    }

    protected void select_typemachine(DropDownList ddlName)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_TypeItemList = new TypeItem_Detail[1];
        TypeItem_Detail typemachine = new TypeItem_Detail();
        _dtenplan.BoxEN_TypeItemList[0] = typemachine;
        //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtenplan));
        _dtenplan = callServicePostENPlanning(_urlSelect_TypeMachine, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_TypeItemList, "NameEN", "TmcIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภทเครื่องจักร...", "0"));

    }

    protected void Select_Location(DropDownList ddlName)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail locate = new RepairMachineDetail();
        _dtmachine.BoxRepairMachine[0] = locate;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));

        _dtmachine = callServicePostENRepair(urlSelect_Location, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxRepairMachine, "LocName", "LocIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสถานที่...", "0"));
    }

    protected void Select_Building(DropDownList ddlName, int LocIDX)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail building = new RepairMachineDetail();

        building.LocIDX = LocIDX;
        _dtmachine.BoxRepairMachine[0] = building;

        _dtmachine = callServicePostENRepair(urlSelect_Building, _dtmachine);

        setDdlData(ddlName, _dtmachine.BoxRepairMachine, "BuildingName", "BuildingIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกอาคาร...", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกฝ่าย...", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกแผนก...", "0"));
    }

    protected void Select_Room(DropDownList ddlName, int BuildIDX)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail room = new RepairMachineDetail();

        room.BuildingIDX = BuildIDX;
        _dtmachine.BoxRepairMachine[0] = room;

        _dtmachine = callServicePostENRepair(urlSelect_Room, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxRepairMachine, "Roomname", "RoomIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกห้อง...", "0"));
    }

    protected void select_machine(CheckBoxList chkName, int TmcIDX, string code, int LocIDX, int RoomIDX, int Building)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_u0docform = new u0docform_Detail[1];
        u0docform_Detail qtymachine = new u0docform_Detail();
        qtymachine.TmcIDX = TmcIDX;
        qtymachine.LocIDX = LocIDX;
        qtymachine.RoomIDX = RoomIDX;
        qtymachine.BuildingIDX = Building;
        qtymachine.code = code;
        _dtenplan.BoxEN_u0docform[0] = qtymachine;
        //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtenplan));
        _dtenplan = callServicePostENPlanning(_urlSelectDoc_Machine, _dtenplan);
        setChkData(chkName, _dtenplan.BoxEN_u0docform, "MCCode", "MCIDX");

    }

    protected void select_time(DropDownList ddlName)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_TimeList = new Timing_Detail[1];
        Timing_Detail time = new Timing_Detail();

        _dtenplan.BoxEN_TimeList[0] = time;

        _dtenplan = callServicePostENPlanning(_urlSelect_Time_Machine, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_TimeList, "time_desc", "m0tidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกความถี่ในการดำเนินการ...", "0"));

    }

    protected void Select_FormResultIndex()
    {

        _dtmachine = new DataMachine();

        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail index = new RepairMachineDetail();

        if (depten.Contains(int.Parse(ViewState["rdept_idx"].ToString())) || secit.Contains(int.Parse(ViewState["Sec_idx"].ToString())))
        {
            index.RDeptIDX = 0;
            index.RSecIDX = 0;
            index.TmcIDX = int.Parse(ddltype_result.SelectedValue);
            index.TCIDX = int.Parse(ddltypecode_result.SelectedValue);
            index.GCIDX = int.Parse(ddlgroupcode_result.SelectedValue);
            index.LocIDX = int.Parse(ddllocate_result.SelectedValue);
            index.BuildingIDX = int.Parse(ddlbuilding_result.SelectedValue);
            index.RoomIDX = int.Parse(ddlroom_result.SelectedValue);
            index.m0tidx = int.Parse(ddlplan_result.SelectedValue);
        }
        else
        {
            index.RDeptIDX = int.Parse(ViewState["rdept_idx"].ToString());
            index.RSecIDX = int.Parse(ViewState["Sec_idx"].ToString());
            index.TmcIDX = 0;
            index.TCIDX = 0;
            index.GCIDX = 0;
            index.LocIDX = 0;
            index.BuildingIDX = 0;
            index.RoomIDX = 0;
            index.m0tidx = 0;
        }

        _dtmachine.BoxRepairMachine[0] = index;

        _dtmachine = callServicePostENRepair(_urlSelectDoc_PM, _dtmachine);
        setGridData(GvViewENRepair, _dtmachine.BoxRepairMachine);
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));

    }

    protected void SelectFormtoAddingResult(GridView GvName)
    {

        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_u0docform = new u0docform_Detail[1];
        u0docform_Detail index = new u0docform_Detail();

        index.RPIDX = int.Parse(ViewState["RPIDX"].ToString());

        _dtenplan.BoxEN_u0docform[0] = index;

        _dtenplan = callServicePostENPlanning(_urlSelectFormtoAddingResult, _dtenplan);
        setGridData(GvName, _dtenplan.BoxEN_u0formList);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));

    }

    protected void SelectStatusFormtoAddingResult(DropDownList ddlName)
    {

        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_u0docform = new u0docform_Detail[1];
        u0docform_Detail index = new u0docform_Detail();

        _dtenplan.BoxEN_u0docform[0] = index;

        _dtenplan = callServicePostENPlanning(_urlSelectStatusFormtoAddingResult, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_u0docform, "status_name", "stidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสถานะดำเนินการ...", "0"));

    }

    protected void SelectMachineFormResult()
    {

        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_u0docform = new u0docform_Detail[1];
        u0docform_Detail index = new u0docform_Detail();

        index.RPIDX = int.Parse(ViewState["RPIDX"].ToString());

        _dtenplan.BoxEN_u0docform[0] = index;
        //   txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtenplan));

        _dtenplan = callServicePostENPlanning(_urlSelectMachineFormResult, _dtenplan);
        setFormViewData(FvBindMachine, _dtenplan.BoxEN_u0docform);

    }

    protected void SelectIndexManagePlan(GridView GvName)
    {

        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_u0docform = new u0docform_Detail[1];
        u0docform_Detail index = new u0docform_Detail();

        index.TmcIDX = int.Parse(ddltypemanage_search.SelectedValue);
        index.TCIDX = int.Parse(ddltypecodemanage_search.SelectedValue);
        index.GCIDX = int.Parse(ddlgroupmachinemanage_search.SelectedValue);
        index.LocIDX = int.Parse(ddllocatemanage_search.SelectedValue);
        index.BuildingIDX = int.Parse(ddlbuildingmanage_search.SelectedValue);
        index.RoomIDX = int.Parse(ddlroommanage_search.SelectedValue);
        index.m0tidx = int.Parse(ddlplanmange_search.SelectedValue);
        index.Year = int.Parse(ddlyearmanage_plan.SelectedValue);


        _dtenplan.BoxEN_u0docform[0] = index;

        _dtenplan = callServicePostENPlanning(_urlSelectIndexManagePlan, _dtenplan);
        setGridData(GvName, _dtenplan.BoxEN_u0docform);

    }

    protected void SelectEditPlan(GridView GvName, FormView FvName, FormView FvDetail)
    {

        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_u0docform = new u0docform_Detail[1];
        u0docform_Detail index = new u0docform_Detail();

        index.u0idx = int.Parse(ViewState["u0idx_edit"].ToString());

        _dtenplan.BoxEN_u0docform[0] = index;

        _dtenplan = callServicePostENPlanning(_urlSelectEditPlan, _dtenplan);
        setFormViewData(FvName, _dtenplan.BoxEN_u0docform);
        setFormViewData(FvDetail, _dtenplan.BoxEN_u0docform);
        setGridData(GvName, _dtenplan.BoxEN_u0docform);




    }


    #endregion

    #region CallService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_en_planning callServicePostENPlanning(string _cmdUrl, data_en_planning _dtenplan)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtenplan);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtenplan = (data_en_planning)_funcTool.convertJsonToObject(typeof(data_en_planning), _localJson);

        return _dtenplan;
    }

    protected DataMachine callServicePostENRepair(string _cmdUrl, DataMachine _dtmachine)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmachine);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtmachine = (DataMachine)_funcTool.convertJsonToObject(typeof(DataMachine), _localJson);




        return _dtmachine;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //  convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }
    #endregion

    #region Set Default View
    protected void SetDefaultIndex()
    {
        if (Page.RouteData.Values["link"] != null)
        {
            _link = Page.RouteData.Values["link"].ToString().ToLower();
            ViewState["link"] = _link;
            string[] urldecry = (_funcTool.getDecryptRC4(_link, keyenrepair)).Split('|');


            if (ViewState["link"].ToString() != "0" && i == para)
            {
                ViewState["RPIDX"] = urldecry[0];
                ViewState["m0_node"] = urldecry[1];
                ViewState["m0_actor"] = urldecry[2];

                MvMaster.SetActiveView(ViewDetailResult);
                setFormData();

                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewAddResult.Attributes.Add("class", "active");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewManage.Attributes.Remove("class");

                if (depten.Contains(int.Parse(ViewState["rdept_idx"].ToString())) || secit.Contains(int.Parse(ViewState["Sec_idx"].ToString())))
                {
                    _divMenuLiToViewAdd.Visible = true;
                    _divMenuLiToViewIndex.Visible = true;
                    _divMenuLiToViewManage.Visible = true;
                }
                else
                {
                    _divMenuLiToViewAdd.Visible = false;
                    _divMenuLiToViewIndex.Visible = false;
                    _divMenuLiToViewManage.Visible = false;
                }

            }
            else
            {
                _divMenuLiToViewIndex.Attributes.Add("class", "active");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                _divMenuLiToViewAddResult.Attributes.Remove("class");
                _divMenuLiToViewApprove.Attributes.Remove("class");
                _divMenuLiToViewManage.Attributes.Remove("class");


                hddf_empty.Value = "full";
                hddf_empty.Value = "";
                _func_dmu.zENDropDownList(ddlYearSearch_plan_pm,
                                       "",
                                       "zyear",
                                       "zyear",
                                       "0",
                                       DateTime.Now.ToString("yyyy", CultureInfo.InstalledUICulture),
                                       "plan_year"
                                       );
                Select_Location(ddllocate_search);
                select_typemachine(ddltypemachine_search);
                getHtml_RptPlanPM();
                getTitle();

                if (depten.Contains(int.Parse(ViewState["rdept_idx"].ToString())) || secit.Contains(int.Parse(ViewState["Sec_idx"].ToString())))
                {
                    _divMenuLiToViewAdd.Visible = true;
                    _divMenuLiToViewIndex.Visible = true;
                    _divMenuLiToViewManage.Visible = true;
                    MvMaster.SetActiveView(ViewIndex);
                }
                else
                {
                    _divMenuLiToViewAdd.Visible = false;
                    _divMenuLiToViewIndex.Visible = false;
                    _divMenuLiToViewManage.Visible = false;
                    SetDefaultAddResult();
                }



            }

        }
    }

    protected void SetDefaultAdd()
    {
        _divMenuLiToViewAdd.Attributes.Add("class", "active");
        _divMenuLiToViewIndex.Attributes.Remove("class");
        _divMenuLiToViewAddResult.Attributes.Remove("class");
        _divMenuLiToViewManage.Attributes.Remove("class");
        _divMenuLiToViewApprove.Attributes.Remove("class");
        MvMaster.SetActiveView(ViewAdd);
        FvDetailUser.ChangeMode(FormViewMode.Insert);
        FvDetailUser.DataBind();
        FvInsert.ChangeMode(FormViewMode.Insert);
        FvInsert.DataBind();

        var ddltypemachine = (DropDownList)FvInsert.FindControl("ddltypemachine");
        var ddllocate = (DropDownList)FvInsert.FindControl("ddllocate");
        var ddldep = (DropDownList)FvInsert.FindControl("ddldep");
        var ddlplan = (DropDownList)FvInsert.FindControl("ddlplan");
        var GvReportAdd = (GridView)FvInsert.FindControl("GvReportAdd");

        select_typemachine(ddltypemachine);
        Select_Location(ddllocate);
        getDepartmentList(ddldep, 1);
        select_time(ddlplan);
        CleardataSetFormList();
        GvReportAdd.Visible = false;
    }

    protected void SetDefaultAddResult()
    {
        _divMenuLiToViewIndex.Attributes.Remove("class");
        _divMenuLiToViewAdd.Attributes.Remove("class");
        _divMenuLiToViewAddResult.Attributes.Add("class", "active");
        _divMenuLiToViewManage.Attributes.Remove("class");
        _divMenuLiToViewApprove.Attributes.Remove("class");
        MvMaster.SetActiveView(ViewAddResult);
        select_typemachine(ddltype_result);
        Select_Location(ddllocate_result);
        getDepartmentList(ddldept_result, 1);
        select_time(ddlplan_result);
        //GenerateddlYear(ddlyear_result);
        //ddlyear_result.Items.Add(new ListItem("กรุณาเลือกปี ....", "0"));
        ddllocate_result.SelectedValue = "0";
        ddlbuilding_result.SelectedValue = "0";
        ddlroom_result.SelectedValue = "0";
        ddltype_result.SelectedValue = "0";
        ddltypecode_result.SelectedValue = "0";
        ddlgroupcode_result.SelectedValue = "0";
        ddlplan_result.SelectedValue = "0";
        //ddlyear_result.SelectedValue = "0";
        ddldept_result.SelectedValue = "0";
        ddlsec_result.SelectedValue = "0";

        if (depten.Contains(int.Parse(ViewState["rdept_idx"].ToString())) || secit.Contains(int.Parse(ViewState["Sec_idx"].ToString())))
        {
            div_searchresult.Visible = true;
        }
        else
        {
            div_searchresult.Visible = false;
        }


        Select_FormResultIndex();
    }

    protected void SetDefaultManage()
    {
        _divMenuLiToViewIndex.Attributes.Remove("class");
        _divMenuLiToViewAdd.Attributes.Remove("class");
        _divMenuLiToViewAddResult.Attributes.Remove("class");
        _divMenuLiToViewManage.Attributes.Add("class", "active");
        _divMenuLiToViewApprove.Attributes.Remove("class");
        MvMaster.SetActiveView(ViewManage);

        Select_Location(ddllocatemanage_search);
        select_typemachine(ddltypemanage_search);
        GenerateddlYear(ddlyearmanage_plan);
        select_time(ddlplanmange_search);


        SelectIndexManagePlan(GvManagePlan);
        mergeCell(GvManagePlan);

        ddlbuildingmanage_search.SelectedValue = "0";
        ddlroommanage_search.SelectedValue = "0";
        ddltypemanage_search.SelectedValue = "0";
        ddltypecodemanage_search.SelectedValue = "0";
        ddlgroupmachinemanage_search.SelectedValue = "0";
        ddlplanmange_search.SelectedValue = "0";
        ddlyearmanage_plan.Items.Add(new ListItem("2019", "2019"));

    }

    protected void SetDefaultApprove()
    {
        _divMenuLiToViewIndex.Attributes.Remove("class");
        _divMenuLiToViewAdd.Attributes.Remove("class");
        _divMenuLiToViewAddResult.Attributes.Remove("class");
        _divMenuLiToViewManage.Attributes.Remove("class");
        _divMenuLiToViewApprove.Attributes.Add("class", "active");
        MvMaster.SetActiveView(ViewApprove);
    }



    #endregion

    #region SetDefault Form

    protected void SetViewState_Form()
    {

        DataSet dsFoodMaterialList = new DataSet();
        dsFoodMaterialList.Tables.Add("dsAddListTable");

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("custom", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("customidx", typeof(int));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("LocName", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("LocIDX", typeof(int));


        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("NameEN", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("TmcIDX", typeof(int));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("NameTypecode", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("TCIDX", typeof(int));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("NameGroupcode", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("GCIDX", typeof(int));


        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("BuildingName", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("BuildingIDX", typeof(int));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("RoomName", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("RoomIDX", typeof(int));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("RDeptName", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("RDeptIDX", typeof(int));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("RSecName", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("RSecIDX", typeof(int));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("MCCode", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("MCIDX", typeof(String));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("time_desc", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("m0tidx", typeof(String));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("datestart", typeof(String));

        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("countdate", typeof(int));


        ViewState["vsBuyequipment"] = dsFoodMaterialList;

    }

    protected void setAddList_Form()
    {

        if (ViewState["vsBuyequipment"] != null)
        {
            DropDownList ddlcustom = (DropDownList)FvInsert.FindControl("ddlcustom");
            DropDownList ddllocate = (DropDownList)FvInsert.FindControl("ddllocate");
            DropDownList ddltypemachine = (DropDownList)FvInsert.FindControl("ddltypemachine");
            DropDownList ddltypecode = (DropDownList)FvInsert.FindControl("ddltypecode");
            DropDownList ddlgroupmachine = (DropDownList)FvInsert.FindControl("ddlgroupmachine");
            DropDownList ddlbuilding = (DropDownList)FvInsert.FindControl("ddlbuilding");
            DropDownList ddlroom = (DropDownList)FvInsert.FindControl("ddlroom");
            DropDownList ddldep = (DropDownList)FvInsert.FindControl("ddldep");
            DropDownList ddlsec = (DropDownList)FvInsert.FindControl("ddlsec");
            DropDownList ddlplan = (DropDownList)FvInsert.FindControl("ddlplan");
            TextBox AddStartdate = (TextBox)FvInsert.FindControl("AddStartdate");
            TextBox txtcount = (TextBox)FvInsert.FindControl("txtcount");
            CheckBoxList chkmachine = (CheckBoxList)FvInsert.FindControl("chkmachine");
            GridView GvReportAdd = (GridView)FvInsert.FindControl("GvReportAdd");


            int sumcheck_create = 0;
            string test_detail = "";
            string test_detail_idx = "";

            List<String> AddoingList_checklist = new List<string>();
            foreach (ListItem chkLis_create in chkmachine.Items)
            {
                if (chkLis_create.Selected)
                {

                    sumcheck_create = sumcheck_create + 1;


                    test_detail += chkLis_create.Text + ',';
                    test_detail_idx += chkLis_create.Value + ',';
                    sumcheck_create++;
                }
            }

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable"].Rows)
            {

                if (dr["customidx"].ToString() == ddlcustom.SelectedValue &&
                    int.Parse(dr["LocIDX"].ToString()) == int.Parse(ddllocate.SelectedValue) &&
                    int.Parse(dr["TmcIDX"].ToString()) == int.Parse(ddltypemachine.SelectedValue) &&
                    int.Parse(dr["TCIDX"].ToString()) == int.Parse(ddltypecode.SelectedValue) &&
                    int.Parse(dr["GCIDX"].ToString()) == int.Parse(ddlgroupmachine.SelectedValue) &&
                    int.Parse(dr["BuildingIDX"].ToString()) == int.Parse(ddlbuilding.SelectedValue) &&
                    int.Parse(dr["RoomIDX"].ToString()) == int.Parse(ddlroom.SelectedValue) &&
                    int.Parse(dr["RDeptIDX"].ToString()) == int.Parse(ddldep.SelectedValue) &&
                    int.Parse(dr["RSecIDX"].ToString()) == int.Parse(ddlsec.SelectedValue) &&
                    int.Parse(dr["m0tidx"].ToString()) == int.Parse(ddlplan.SelectedValue) &&
                     dr["datestart"].ToString() == AddStartdate.Text &&
                     dr["countdate"].ToString() == txtcount.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }
            }

            DataRow drContacts = dsContacts.Tables["dsAddListTable"].NewRow();

            drContacts["custom"] = ddlcustom.SelectedItem.Text;
            drContacts["customidx"] = ddlcustom.SelectedValue;
            drContacts["LocName"] = ddllocate.SelectedItem.Text;
            drContacts["LocIDX"] = ddllocate.SelectedValue;
            drContacts["NameEN"] = ddltypemachine.SelectedItem.Text;
            drContacts["TmcIDX"] = ddltypemachine.SelectedValue;
            drContacts["NameTypecode"] = ddltypecode.SelectedItem.Text;
            drContacts["TCIDX"] = ddltypecode.SelectedValue;
            drContacts["NameGroupcode"] = ddlgroupmachine.SelectedItem.Text;
            drContacts["GCIDX"] = ddlgroupmachine.SelectedValue;
            drContacts["BuildingName"] = ddlbuilding.SelectedItem.Text;
            drContacts["BuildingIDX"] = ddlbuilding.SelectedValue;
            if (ddlroom.SelectedValue == "0")
            {
                drContacts["RoomName"] = "-";
                drContacts["RoomIDX"] = 0;

            }
            else
            {
                drContacts["RoomName"] = ddlroom.SelectedItem.Text;
                drContacts["RoomIDX"] = ddlroom.SelectedValue;

            }
            drContacts["RDeptName"] = ddldep.SelectedItem.Text;
            drContacts["RDeptIDX"] = ddldep.SelectedValue;
            drContacts["RSecName"] = ddlsec.SelectedItem.Text;
            drContacts["RSecIDX"] = ddlsec.SelectedValue;


            if (test_detail != "")
            {
                drContacts["MCCode"] = test_detail;
                drContacts["MCIDX"] = test_detail_idx;
            }
            else
            {
                drContacts["MCCode"] = "-";
                drContacts["MCIDX"] = "0";
            }

            drContacts["time_desc"] = ddlplan.SelectedItem.Text;
            drContacts["m0tidx"] = ddlplan.SelectedValue;
            drContacts["datestart"] = AddStartdate.Text;
            drContacts["countdate"] = txtcount.Text;


            dsContacts.Tables["dsAddListTable"].Rows.Add(drContacts);
            ViewState["vsBuyequipment"] = dsContacts;
            // GvReportAdd.Visible = true;
            setGridData(GvReportAdd, dsContacts.Tables["dsAddListTable"]);
            GvReportAdd.Visible = true;

        }

    }

    protected void CleardataSetFormList()
    {
        var GvReportAdd = (GridView)FvInsert.FindControl("GvReportAdd");
        ViewState["vsBuyequipment"] = null;
        GvReportAdd.DataSource = ViewState["vsBuyequipment"];
        GvReportAdd.DataBind();
        SetViewState_Form();
    }

    #endregion

    #region GridView
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {

            case "GvViewENRepair":

                GvViewENRepair.PageIndex = e.NewPageIndex;
                Select_FormResultIndex();

                break;
            case "GvManagePlan":

                GvManagePlan.PageIndex = e.NewPageIndex;
                SelectIndexManagePlan(GvManagePlan);
                mergeCell(GvManagePlan);

                break;

        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvViewENRepair":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvViewENRepair.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var ltstatus = ((Literal)e.Row.FindControl("ltstatus"));
                        var listate = ((Label)e.Row.FindControl("listate"));


                        if (ltstatus.Text != "11")
                        {
                            listate.Text = "ยังไม่ได้ทำการกรอกข้อมูล";
                            listate.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                            listate.Style["font-weight"] = "bold";
                        }
                        else
                        {
                            listate.Text = "เสร็จสมบูรณ์";
                            listate.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                            listate.Style["font-weight"] = "bold";
                        }

                    }
                }

                break;

            case "GvItem":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }



                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    if (GvItem.EditIndex != e.Row.RowIndex) //to overlook header row
                    {

                        var ddlstatus = ((DropDownList)e.Row.FindControl("ddlstatus"));
                        var div_addstatus = ((Control)e.Row.FindControl("div_addstatus"));
                        var div_addcomment = ((Control)e.Row.FindControl("div_addcomment"));
                        var div_bindcomment = ((Control)e.Row.FindControl("div_bindcomment"));
                        var div_bindstatus = ((Control)e.Row.FindControl("div_bindstatus"));

                        SelectStatusFormtoAddingResult(ddlstatus);


                        if (ViewState["m0_node"].ToString() != "9")
                        {
                            div_addstatus.Visible = true;
                            div_addcomment.Visible = true;
                            div_bindcomment.Visible = false;
                            div_bindstatus.Visible = false;
                        }
                        else
                        {
                            div_addstatus.Visible = false;
                            div_addcomment.Visible = false;
                            div_bindcomment.Visible = true;
                            div_bindstatus.Visible = true;
                        }
                    }
                }
                break;

            case "GvManagePlan":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                break;

            case "GvShowPlan":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvShowPlan.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var ltstatus = ((Label)e.Row.FindControl("ltstatus"));
                        var listate = ((Label)e.Row.FindControl("listate"));
                        var lbViewEN = ((LinkButton)e.Row.FindControl("lbViewEN"));
                        var lbdatemanage = ((Label)e.Row.FindControl("lbdatemanage"));

                        if (ltstatus.Text != "11")
                        {
                            listate.Text = "ยังไม่ได้ทำการกรอกข้อมูล";
                            listate.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                            listate.Style["font-weight"] = "bold";

                        }
                        else
                        {
                            listate.Text = "เสร็จสมบูรณ์";
                            listate.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                            listate.Style["font-weight"] = "bold";

                        }

                        if (lbdatemanage.Text == String.Empty)
                        {
                            lbViewEN.Visible = false;
                        }
                        else
                        {
                            lbViewEN.Visible = true;

                        }


                    }
                }
                break;

            case "gvFile":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;


        }
    }
    #endregion

    #region Directories_File URL

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt1.Rows.Add(file.Name);
                    dt1.Rows[i][1] = f[0];
                    i++;
                }
            }

            GridView gvFile = (GridView)FvDetailEditPlan.FindControl("gvFile");
            // Panel gvFileLo1 = (Panel)FvDetailUser.FindControl("gvFileLo1");

            if (dt1.Rows.Count > 0)
            {

                ds1.Tables.Add(dt1);
                gvFile.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
                gvFile.DataBind();
                ds1.Dispose();
            }
            else
            {

                gvFile.DataSource = null;
                gvFile.DataBind();

            }
        }
        catch
        {
            //ViewState["CheckFile"] = "0";
            //  checkfile = "11";
        }
    }

    protected void SearchFile (string u0idx)
    {
        try
        {

            string getPathLotus = ConfigurationSettings.AppSettings["path_en_planning"];

            string filePathLotus = Server.MapPath(getPathLotus + u0idx);
            DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

            SearchDirectories(myDirLotus, u0idx);

            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + myDirLotus + "');", true);
        }
        catch
        {

        }
    }


    #endregion


    #region MergeCell
    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {
            case "GvItem":
                for (int rowIndex = GvItem.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvItem.Rows[rowIndex];
                    GridViewRow previousRow = GvItem.Rows[rowIndex + 1];

                    if (((Literal)currentRow.Cells[0].FindControl("lbtype")).Text == ((Literal)previousRow.Cells[0].FindControl("lbtype")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }
                }
                break;

            case "GvManagePlan":
                for (int rowIndex = GvManagePlan.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvManagePlan.Rows[rowIndex];
                    GridViewRow previousRow = GvManagePlan.Rows[rowIndex + 1];

                    if (((Literal)currentRow.Cells[0].FindControl("ltyear")).Text == ((Literal)previousRow.Cells[0].FindControl("ltyear")).Text &&
                        ((Literal)currentRow.Cells[1].FindControl("ltlocname")).Text == ((Literal)previousRow.Cells[1].FindControl("ltlocname")).Text &&
                        ((Literal)currentRow.Cells[1].FindControl("ltbuilding")).Text == ((Literal)previousRow.Cells[1].FindControl("ltbuilding")).Text &&
                        ((Literal)currentRow.Cells[1].FindControl("ltroom")).Text == ((Literal)previousRow.Cells[1].FindControl("ltroom")).Text &&
                        ((Literal)currentRow.Cells[2].FindControl("lttype")).Text == ((Literal)previousRow.Cells[2].FindControl("lttype")).Text &&
                        ((Literal)currentRow.Cells[3].FindControl("lttypecode")).Text == ((Literal)previousRow.Cells[3].FindControl("lttypecode")).Text &&
                        ((Literal)currentRow.Cells[4].FindControl("ltgroupcode")).Text == ((Literal)previousRow.Cells[4].FindControl("ltgroupcode")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;
                            currentRow.Cells[1].RowSpan = 2;
                            currentRow.Cells[2].RowSpan = 2;
                            currentRow.Cells[3].RowSpan = 2;
                            currentRow.Cells[4].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;
                            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;
                            currentRow.Cells[2].RowSpan = previousRow.Cells[2].RowSpan + 1;
                            currentRow.Cells[3].RowSpan = previousRow.Cells[3].RowSpan + 1;
                            currentRow.Cells[4].RowSpan = previousRow.Cells[4].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                        previousRow.Cells[1].Visible = false;
                        previousRow.Cells[2].Visible = false;
                        previousRow.Cells[3].Visible = false;
                        previousRow.Cells[4].Visible = false;
                    }
                }

                break;

            case "GvShowPlan":
                for (int rowIndex = GvShowPlan.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvShowPlan.Rows[rowIndex];
                    GridViewRow previousRow = GvShowPlan.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[0].FindControl("lbldateplanstart")).Text == ((Label)previousRow.Cells[0].FindControl("lbldateplanstart")).Text &&
                        ((Label)currentRow.Cells[1].FindControl("lbldateplanend")).Text == ((Label)previousRow.Cells[1].FindControl("lbldateplanend")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;
                            currentRow.Cells[1].RowSpan = 2;
                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;
                            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                        previousRow.Cells[1].Visible = false;
                    }
                }

                break;
        }
    }

    #endregion

    #region bind Node

    protected void setFormData()
    {


        int m0_node = int.Parse(ViewState["m0_node"].ToString());


        switch (m0_node)
        {


            case 1: // สร้าง/แก้ไข
            case 2: // พิจารณาผลโดยผู้สร้าง
            case 3: // ดำเนินการโดยเจ้าหน้าที่
            case 4: // พิจารณาผลโดยเจ้าหน้าที่(รับงาน)
            case 5: // ปิดงานโดยเจ้าหน้าที่ 
                setFormDataActor();

                break;

            case 9: // จบการดำเนินการ

                setFormDataActor();

                break;

        }

    }

    #endregion

    #region bind Actor

    protected void setFormDataActor()
    {


        int m0_actor = int.Parse(ViewState["m0_actor"].ToString());
        //txt.Text = Convert.ToString(m0_actor) + "," + ViewState["m0_node"].ToString() + "," + ViewState["RPIDX"].ToString();
        switch (m0_actor)
        {

            case 1:

                FvBindMachine.ChangeMode(FormViewMode.Edit);
                FvBindMachine.DataBind();
                SelectMachineFormResult();
                SelectFormtoAddingResult(GvItem);
                mergeCell(GvItem);


                if (depten.Contains(int.Parse(ViewState["rdept_idx"].ToString())) || secit.Contains(int.Parse(ViewState["Sec_idx"].ToString())))
                {
                    div_save.Visible = true;
                    GvItem.Enabled = true;
                    div_cancel.Visible = false;
                }
                else
                {
                    GvItem.Enabled = false;
                    div_save.Visible = false;
                    div_cancel.Visible = true;
                }

                break;

            case 2: // เจ้าหน้าที่ EN

                if (ViewState["m0_node"].ToString() != "9")
                {
                    FvBindMachine.ChangeMode(FormViewMode.Edit);
                    FvBindMachine.DataBind();
                    SelectMachineFormResult();

                    if (depten.Contains(int.Parse(ViewState["rdept_idx"].ToString())) || secit.Contains(int.Parse(ViewState["Sec_idx"].ToString())))
                    {
                        div_save.Visible = true;
                        GvItem.Enabled = true;
                        div_cancel.Visible = false;
                    }
                    else
                    {
                        GvItem.Enabled = false;
                        div_save.Visible = false;
                        div_cancel.Visible = true;
                    }

                }
                else
                {
                    FvBindMachine.ChangeMode(FormViewMode.Edit);
                    FvBindMachine.DataBind();
                    SelectMachineFormResult();
                    div_cancel.Visible = true;
                    div_save.Visible = false;
                    btnsaveuser.Visible = false;
                }

                SelectFormtoAddingResult(GvItem);
                mergeCell(GvItem);
                break;

        }

    }

    #endregion

    #region onRowCommand

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "bnDeleteList":
                    GridView GvReportAdd = (GridView)FvInsert.FindControl("GvReportAdd");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];
                    dsContacts.Tables["dsAddListTable"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(GvReportAdd, dsContacts.Tables["dsAddListTable"]);
                    if (dsContacts.Tables["dsAddListTable"].Rows.Count < 1)
                    {
                        GvReportAdd.Visible = false;
                    }
                    break;

            }
        }
    }

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;
        var ddltypecode = (DropDownList)FvInsert.FindControl("ddltypecode");
        var ddltypemachine = (DropDownList)FvInsert.FindControl("ddltypemachine");
        var ddlgroupmachine = (DropDownList)FvInsert.FindControl("ddlgroupmachine");
        var ddllocate = (DropDownList)FvInsert.FindControl("ddllocate");
        var ddlbuilding = (DropDownList)FvInsert.FindControl("ddlbuilding");
        var ddlroom = (DropDownList)FvInsert.FindControl("ddlroom");
        var chkmachine = (CheckBoxList)FvInsert.FindControl("chkmachine");
        var idchecklist = (Control)FvInsert.FindControl("idchecklist");
        var ddldep = (DropDownList)FvInsert.FindControl("ddldep");
        var ddlsec = (DropDownList)FvInsert.FindControl("ddlsec");

        switch (ddName.ID)
        {
            case "ddltypemachine":

                select_typecodemachine(ddltypecode, int.Parse(ddltypemachine.SelectedValue));

                break;

            case "ddltypecode":
                select_groupmachine(ddlgroupmachine, int.Parse(ddltypecode.SelectedValue));


                break;

            case "ddllocate":
                Select_Building(ddlbuilding, int.Parse(ddllocate.SelectedValue));

                break;

            case "ddlbuilding":
                string typecode = ddltypecode.SelectedItem.Text;
                string groupcode = ddlgroupmachine.SelectedItem.Text;
                string subtype = typecode.Substring(0, typecode.IndexOf("-"));
                string subgroup = groupcode.Substring(0, groupcode.IndexOf("-"));
                string total = subtype + "-" + subgroup;
                total = total.Replace(" ", "");


                Select_Room(ddlroom, int.Parse(ddlbuilding.SelectedValue));
                select_machine(chkmachine, int.Parse(ddltypemachine.SelectedValue), total, int.Parse(ddllocate.SelectedValue), int.Parse(ddlroom.SelectedValue), int.Parse(ddlbuilding.SelectedValue));
                idchecklist.Visible = true;
                break;

            case "ddlroom":
                string typecode1 = ddltypecode.SelectedItem.Text;
                string groupcode1 = ddlgroupmachine.SelectedItem.Text;
                string subtype1 = typecode1.Substring(0, typecode1.IndexOf("-"));
                string subgroup1 = groupcode1.Substring(0, groupcode1.IndexOf("-"));
                string total1 = subtype1 + "-" + subgroup1;
                total1 = total1.Replace(" ", "");

                select_machine(chkmachine, int.Parse(ddltypemachine.SelectedValue), total1, int.Parse(ddllocate.SelectedValue), int.Parse(ddlroom.SelectedValue), int.Parse(ddlbuilding.SelectedValue));
                break;

            case "ddldep":
                getSectionList(ddlsec, 1, int.Parse(ddldep.SelectedValue));
                break;


            case "ddltypemachine_search":

                //   _func_dmu.zENtypecodemachine(ddltypecode_search, int.Parse(ddltypemachine_search.SelectedValue));
                select_typecodemachine(ddltypecode_search, int.Parse(ddltypemachine_search.SelectedValue));

                break;
            case "ddllocate_search":
                //  _func_dmu.zENBuilding(ddlbuilding_search, int.Parse(ddllocate_search.SelectedValue));
                Select_Building(ddlbuilding_search, int.Parse(ddllocate_search.SelectedValue));


                break;

            case "ddllocatemanage_search":
                Select_Building(ddlbuildingmanage_search, int.Parse(ddllocatemanage_search.SelectedValue));
                break;

            case "ddlbuildingmanage_search":
                Select_Room(ddlroommanage_search, int.Parse(ddlbuildingmanage_search.SelectedValue));

                break;

            case "ddltypemanage_search":
                select_typecodemachine(ddltypecodemanage_search, int.Parse(ddltypemanage_search.SelectedValue));

                break;

            case "ddltypecodemanage_search":
                select_groupmachine(ddlgroupmachinemanage_search, int.Parse(ddltypecodemanage_search.SelectedValue));
                break;

            case "ddllocate_result":
                //_func_dmu.zENBuilding(ddlbuilding_result, int.Parse(ddllocate_result.SelectedValue));
                Select_Building(ddlbuilding_result, int.Parse(ddllocate_result.SelectedValue));

                break;

            case "ddlbuilding_result":
                Select_Room(ddlroom_result, int.Parse(ddlbuilding_result.SelectedValue));

                break;

            case "ddltype_result":
                select_typecodemachine(ddltypecode_result, int.Parse(ddltype_result.SelectedValue));

                break;

            case "ddltypecode_result":
                select_groupmachine(ddlgroupcode_result, int.Parse(ddltypecode_result.SelectedValue));
                break;

            case "ddldept_result":
                getSectionList(ddlsec_result, 1, int.Parse(ddldept_result.SelectedValue));
                break;

        }
    }

    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setChkData(CheckBoxList chkName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        chkName.Items.Clear();
        // bind items
        chkName.DataSource = obj;
        chkName.DataTextField = _data_text;
        chkName.DataValueField = _data_value;
        chkName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void GenerateddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 3; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }

        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    #endregion

    #region FormView

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvDetailUser":
                FormView FvDetailUser = (FormView)ViewAdd.FindControl("FvDetailUser");

                if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();
                }
                break;


        }
    }


    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        Control divsave = (Control)FvInsert.FindControl("divsave");
        DropDownList ddlcustom = (DropDownList)FvInsert.FindControl("ddlcustom");
        DropDownList ddllocate = (DropDownList)FvInsert.FindControl("ddllocate");
        DropDownList ddltypemachine = (DropDownList)FvInsert.FindControl("ddltypemachine");
        DropDownList ddltypecode = (DropDownList)FvInsert.FindControl("ddltypecode");
        DropDownList ddlgroupmachine = (DropDownList)FvInsert.FindControl("ddlgroupmachine");
        DropDownList ddlbuilding = (DropDownList)FvInsert.FindControl("ddlbuilding");
        DropDownList ddlroom = (DropDownList)FvInsert.FindControl("ddlroom");
        DropDownList ddldep = (DropDownList)FvInsert.FindControl("ddldep");
        DropDownList ddlsec = (DropDownList)FvInsert.FindControl("ddlsec");
        CheckBoxList chkmachine = (CheckBoxList)FvInsert.FindControl("chkmachine");
        DropDownList ddlplan = (DropDownList)FvInsert.FindControl("ddlplan");
        TextBox AddStartdate = (TextBox)FvInsert.FindControl("AddStartdate");
        TextBox txtcount = (TextBox)FvInsert.FindControl("txtcount");
        Control idchecklist = (Control)FvInsert.FindControl("idchecklist");

        _dtenplan.BoxEN_u0docform = new u0docform_Detail[1];
        u0docform_Detail _document = new u0docform_Detail();
        u2docform_Detail _u2document = new u2docform_Detail();

        switch (cmdName)
        {

            case "_divMenuBtnToDivIndex":
                para = 0;
                SetDefaultIndex();

                break;

            case "_divMenuBtnToDivMange":
                SetDefaultManage();
                break;

            case "_divMenuBtnToDivAdd":
                SetDefaultAdd();

                break;

            case "_divMenuBtnToDivAddResult":
                SetDefaultAddResult();
                break;

            case "_divMenuBtnToDivApprove":
                SetDefaultApprove();
                break;

            case "CmdAddPlan":
                setAddList_Form();
                divsave.Visible = true;
                //ddlcustom.SelectedValue = "0";
                //ddllocate.SelectedValue = "0";
                //ddltypemachine.SelectedValue = "0";
                //ddltypecode.SelectedValue = "0";
                //ddlgroupmachine.SelectedValue = "0";
                //ddlbuilding.SelectedValue = "0";
                //ddlroom.SelectedValue = "0";
                //ddldep.SelectedValue = "0";
                //ddlsec.SelectedValue = "0";
                ddlplan.SelectedValue = "0";
                AddStartdate.Text = String.Empty;
                txtcount.Text = String.Empty;
                //idchecklist.Visible = false;

                foreach (ListItem chkLis_create in chkmachine.Items)
                {
                    chkLis_create.Selected = false;

                }

                break;
            case "CmdInsertForm":
                FileUpload UploadImages = (FileUpload)FvInsert.FindControl("UploadImages");

                int i = 0;
                var ds_udoc1_insert = (DataSet)ViewState["vsBuyequipment"];
                var _document1 = new u0docform_Detail[ds_udoc1_insert.Tables[0].Rows.Count];

                foreach (DataRow dtrow in ds_udoc1_insert.Tables[0].Rows)
                {
                    _document1[i] = new u0docform_Detail();
                    _dtenplan.BoxEN_u0docform = new u0docform_Detail[1];

                    _document1[i].TmcIDX = int.Parse(dtrow["TmcIDX"].ToString());
                    _document1[i].TCIDX = int.Parse(dtrow["TCIDX"].ToString());
                    _document1[i].GCIDX = int.Parse(dtrow["GCIDX"].ToString());
                    _document1[i].m0tidx = int.Parse(dtrow["m0tidx"].ToString());
                    _document1[i].customplan = dtrow["custom"].ToString();
                    _document1[i].LocIDX = int.Parse(dtrow["LocIDX"].ToString());
                    _document1[i].RdeptIDX = int.Parse(dtrow["RDeptIDX"].ToString());
                    _document1[i].RSecIDX = int.Parse(dtrow["RSecIDX"].ToString());
                    _document1[i].BuildingIDX = int.Parse(dtrow["BuildingIDX"].ToString());
                    _document1[i].RoomIDX = int.Parse(dtrow["RoomIDX"].ToString());
                    _document1[i].m1idx = 0;
                    _document1[i].CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
                    _document1[i].dateplanstart = dtrow["datestart"].ToString();
                    _document1[i].countplan = int.Parse(dtrow["countdate"].ToString());
                    _document1[i].MCCode_comma = dtrow["MCIDX"].ToString();

                    i++;
                }
                _dtenplan.BoxEN_u0docform = _document1;
                //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtenplan));
                _dtenplan = callServicePostENPlanning(_urlInserttDoc_Planning, _dtenplan);

                u0idx = _dtenplan.return_code.ToString();

                string[] idxu0 = u0idx.Split(',');
                foreach (string rtu0idx in idxu0)
                {
                    if (rtu0idx != String.Empty && rtu0idx != "0")
                    {

                        if (UploadImages.HasFile)
                        {
                            string getPathfile = ConfigurationManager.AppSettings["path_en_planning"];
                            string fileName_upload = rtu0idx;
                            string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                            if (!Directory.Exists(filePath_upload))
                            {
                                Directory.CreateDirectory(filePath_upload);
                            }
                            string extension = Path.GetExtension(UploadImages.FileName);

                            UploadImages.SaveAs(Server.MapPath(getPathfile + rtu0idx) + "\\" + fileName_upload + extension);

                        }
                    }
                }

                Page.Response.Redirect(Page.Request.Url.ToString(), true);


                break;

            case "btnFilter_plan_pm":
                hddf_empty.Value = "full";
                getHtml_RptPlanPM();
                hddf_empty.Value = "";
                break;

            case "ViewFormResult":
                string[] arg_detail = new string[4];
                arg_detail = e.CommandArgument.ToString().Split(';');
                ViewState["RPIDX"] = arg_detail[0];
                ViewState["m0_node"] = arg_detail[1];
                ViewState["m0_actor"] = arg_detail[2];
                ViewState["mo_status"] = arg_detail[3];

                MvMaster.SetActiveView(ViewDetailResult);
                setFormData();


                break;

            case "CmdAddResult":

                TextBox txtRPIDX = (TextBox)FvBindMachine.FindControl("txtRPIDX");
                TextBox txtu1idx_planning = (TextBox)FvBindMachine.FindControl("txtu1idx_planning");
                TextBox txtu0idx = (TextBox)FvBindMachine.FindControl("txtu0idx");
                TextBox txtdatemanage = (TextBox)FvBindMachine.FindControl("txtdatemanage");


                _dtenplan.BoxEN_u0docform = new u0docform_Detail[1];
                _document.RPIDX = int.Parse(txtRPIDX.Text);
                _document.u1idx_planning = int.Parse(txtu1idx_planning.Text);
                _document.u0idx = int.Parse(txtu0idx.Text);
                _document.DateManage = txtdatemanage.Text;
                _document.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

                if (depten.Contains(int.Parse(ViewState["rdept_idx"].ToString())) || secit.Contains(int.Parse(ViewState["Sec_idx"].ToString())))
                {
                    _document.condition = 1;
                }
                else
                {
                    _document.condition = 2;
                }

                _dtenplan.BoxEN_u0docform[0] = _document;


                var count_alert_u1excel = GvItem.Rows.Count;
                var u2_doc = new u2docform_Detail[GvItem.Rows.Count];
                int adding = 0;
                foreach (GridViewRow row in GvItem.Rows)
                {
                    DropDownList ddlstatus = (DropDownList)row.FindControl("ddlstatus");
                    TextBox txtremark = (TextBox)row.FindControl("txtremark");
                    Literal lbm2idx = (Literal)row.FindControl("lbm2idx");

                    u2_doc[adding] = new u2docform_Detail();
                    u2_doc[adding].m2idx = int.Parse(lbm2idx.Text);
                    u2_doc[adding].reult_value = int.Parse(ddlstatus.SelectedValue);
                    u2_doc[adding].result_comment = txtremark.Text;

                    adding++;
                }

                _dtenplan.BoxEN_u2docform = u2_doc;
                //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtenplan));

                _dtenplan = callServicePostENPlanning(_urlAddingResult, _dtenplan);
                SetDefaultAddResult();

                break;

            case "btnbackresult":
                SetDefaultAddResult();
                break;

            case "btnsearch":
                SelectIndexManagePlan(GvManagePlan);
                mergeCell(GvManagePlan);
                break;

            case "ViewEditPlan":
                string[] arg_edit = new string[1];
                arg_edit = e.CommandArgument.ToString().Split(';');
                ViewState["u0idx_edit"] = arg_edit[0];
                MvMaster.SetActiveView(ViewEditPlan);

                SelectEditPlan(GvShowPlan, FvDetailEmpPan, FvDetailEditPlan);
                FvDetailEditPlan.ChangeMode(FormViewMode.ReadOnly);
                FvDetailEditPlan.DataBind();
                SearchFile(ViewState["u0idx_edit"].ToString());
                //try
                //{

                //    string getPathLotus = ConfigurationSettings.AppSettings["path_en_planning"];

                //    string filePathLotus = Server.MapPath(getPathLotus + ViewState["u0idx_edit"].ToString());
                //    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                //    SearchDirectories(myDirLotus, ViewState["u0idx_edit"].ToString());

                //    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + myDirLotus + "');", true);
                //}
                //catch
                //{

                //}

                mergeCell(GvShowPlan);
                break;

            case "btneditplan":


                SelectEditPlan(GvShowPlan, FvDetailEmpPan, FvDetailEditPlan);
                FvDetailEditPlan.ChangeMode(FormViewMode.Edit);
                FvDetailEditPlan.DataBind();
                mergeCell(GvShowPlan);
                DropDownList ddlplan_edit = (DropDownList)FvDetailEditPlan.FindControl("ddlplan_edit");
                TextBox Lbplan_edit = (TextBox)FvDetailEditPlan.FindControl("Lbplan_edit");
                TextBox txttypecode = (TextBox)FvDetailEditPlan.FindControl("txttypecode");
                TextBox txtgroupcode = (TextBox)FvDetailEditPlan.FindControl("txtgroupcode");
                TextBox txtLocIDX = (TextBox)FvDetailEditPlan.FindControl("txtLocIDX");
                TextBox txtBuildingIDX = (TextBox)FvDetailEditPlan.FindControl("txtBuildingIDX");
                TextBox txtRoomIDX = (TextBox)FvDetailEditPlan.FindControl("txtRoomIDX");
                TextBox txtTmcIDX = (TextBox)FvDetailEditPlan.FindControl("txtTmcIDX");
                CheckBoxList chkmachine_edit = (CheckBoxList)FvDetailEditPlan.FindControl("chkmachine_edit");
                TextBox txtchk_mcidx = (TextBox)FvDetailEditPlan.FindControl("txtchk_mcidx");

                string typecode = txttypecode.Text;
                string groupcode = txtgroupcode.Text;
                string subtype = typecode.Substring(0, typecode.IndexOf("-"));
                string subgroup = groupcode.Substring(0, groupcode.IndexOf("-"));
                string total = subtype + "-" + subgroup;
                total = total.Replace(" ", "");
                select_time(ddlplan_edit);
                ddlplan_edit.SelectedValue = Lbplan_edit.Text;

                select_machine(chkmachine_edit, int.Parse(txtTmcIDX.Text), total, int.Parse(txtLocIDX.Text), int.Parse(txtRoomIDX.Text), int.Parse(txtBuildingIDX.Text));

                string[] ToId = txtchk_mcidx.Text.Split(',');

                foreach (ListItem li in chkmachine_edit.Items)
                {
                    foreach (string To_check in ToId)
                    {

                        if (li.Value == To_check)
                        {
                            li.Selected = true;
                            break;
                        }
                    }
                }
                //  txt.Text = total;

                break;

            case "btnsaveedit":
                DropDownList ddlplan_edit1 = (DropDownList)FvDetailEditPlan.FindControl("ddlplan_edit");
                TextBox txtcount_edit = (TextBox)FvDetailEditPlan.FindControl("txtcount_edit");
                CheckBoxList chktooling_edit1 = (CheckBoxList)FvDetailEditPlan.FindControl("chkmachine_edit");
                TextBox Lbplan_edit1 = (TextBox)FvDetailEditPlan.FindControl("Lbplan_edit");

                _dtenplan.BoxEN_u0docform = new u0docform_Detail[1];

                _document.u0idx = int.Parse(ViewState["u0idx_edit"].ToString());
                _document.m0tidx = int.Parse(ddlplan_edit1.SelectedValue);
                _document.countplan = int.Parse(txtcount_edit.Text);
                _document.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

                if (Lbplan_edit1.Text == ddlplan_edit1.SelectedValue)
                {
                    _document.condition = 1;
                }
                else
                {
                    _document.condition = 2;
                }


                foreach (ListItem li in chktooling_edit1.Items)
                {

                    if (li.Selected == true)
                    {

                        _document.MCIDX_Comma += li.Value + ",";

                    }

                }




                _dtenplan.BoxEN_u0docform[0] = _document;
                //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtenplan));

                _dtenplan = callServicePostENPlanning(_urlEditPlan, _dtenplan);

                SelectEditPlan(GvShowPlan, FvDetailEmpPan, FvDetailEditPlan);
                FvDetailEditPlan.ChangeMode(FormViewMode.ReadOnly);
                FvDetailEditPlan.DataBind();
                SearchFile(ViewState["u0idx_edit"].ToString());
                mergeCell(GvShowPlan);
                break;

            case "btncanceledit":
                SelectEditPlan(GvShowPlan, FvDetailEmpPan, FvDetailEditPlan);
                FvDetailEditPlan.ChangeMode(FormViewMode.ReadOnly);
                FvDetailEditPlan.DataBind();
                SearchFile(ViewState["u0idx_edit"].ToString());
                mergeCell(GvShowPlan);
                break;

            case "btnsearch_result":
                Select_FormResultIndex();
                break;

            case "CmdDeletePlan":
                string[] arg_delete = new string[1];
                arg_delete = e.CommandArgument.ToString().Split(';');
                ViewState["u0idx_delete"] = arg_delete[0];

                _dtenplan.BoxEN_u0docform = new u0docform_Detail[1];

                _document.u0idx = int.Parse(ViewState["u0idx_delete"].ToString());
                _dtenplan.BoxEN_u0docform[0] = _document;

                // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtenplan));

                _dtenplan = callServicePostENPlanning(_urlDeletePlan, _dtenplan);

                var rtcode = _dtenplan.return_code.ToString();

                if (rtcode == "1")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ขออภัยค่ะ!!! ไม่สามารถลบข้อมูลได้ เนื่องจากมีการดำเนินรายการอยู่');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('ดำเนินการลบข้อมูลเรียบร้อยแล้ว!!!');", true);
                }

                SetDefaultManage();

                break;
        }
    }
    #endregion


}
