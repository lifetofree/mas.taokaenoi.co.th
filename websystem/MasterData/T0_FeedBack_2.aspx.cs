﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_masterdata_T0_FeedBack_2 : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();

    data_FeedBack _data_FeedBack = new data_FeedBack();
    

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetT0FeedBack2 = _serviceUrl + ConfigurationManager.AppSettings["urlGetT0FeedBack2"];
    static string _urlSetT0FeedBack2 = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsT0FeedBack2"];
    static string _urlSetUpdT0FeedBack2 = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdT0FeedBack2"];
    static string _urlDeleteT0FeedBack2 = _serviceUrl + ConfigurationManager.AppSettings["urlDeleteT0FeedBack2"];



    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;
    #endregion initial function/data


    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
            actionIndex();

        }
    }

    #region selected   
    protected void actionIndex()
    {

        data_FeedBack data_FeedBack = new data_FeedBack();
        data_FeedBack.T0_FeedBack_2_list = new T0_FeedBack_2_detail[1];

        T0_FeedBack_2_detail _T0_FeedBack_2_detailindex = new T0_FeedBack_2_detail();

        _T0_FeedBack_2_detailindex.TFBIDX2 = 0;

        data_FeedBack.T0_FeedBack_2_list[0] = _T0_FeedBack_2_detailindex;
        //litDebug.Text = _funcTool.convertObjectToXml(data_FeedBack);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_FeedBack));
        data_FeedBack = callServiceNetwork(_urlGetT0FeedBack2, data_FeedBack);

         setGridData(GvMaster, data_FeedBack.T0_FeedBack_2_list);

    }
    #endregion selected 

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _TFBIDX2;
        string _txtTopic;
        int _cemp_idx;

        T0_FeedBack_2_detail objT0_FeedBack_2 = new T0_FeedBack_2_detail();

        switch (cmdName)
        {

            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);
                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnInsert":
                _txtTopic = ((TextBox)ViewInsert.FindControl("txtTopic")).Text.Trim();
                DropDownList _ddlTFB2Status = (DropDownList)ViewInsert.FindControl("ddlTFB2Status");
                _cemp_idx = emp_idx;
                T0_FeedBack_2_detail obj = new T0_FeedBack_2_detail();
                _data_FeedBack.T0_FeedBack_2_list = new T0_FeedBack_2_detail[1];
                obj.TFBIDX2 = 0;//_type_idx; 
                obj.Topic = _txtTopic;
                obj.TFB2Status = int.Parse(_ddlTFB2Status.SelectedValue);
                obj.CEmpIDX = _cemp_idx;

                _data_FeedBack.T0_FeedBack_2_list[0] = obj;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_FeedBack));
               
                _data_FeedBack = callServiceNetwork(_urlSetT0FeedBack2, _data_FeedBack);

                if (_data_FeedBack.return_code == 0)
                {
                    actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    setError(_data_FeedBack.return_code.ToString() + " - " + _data_FeedBack.return_msg);
                }

                //actionCreate();
                //Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnDelete":

                _TFBIDX2 = int.Parse(cmdArg);
                _cemp_idx = emp_idx;


                _data_FeedBack.T0_FeedBack_2_list = new T0_FeedBack_2_detail[1];
                objT0_FeedBack_2.TFBIDX2 = _TFBIDX2;
                objT0_FeedBack_2.CEmpIDX = _cemp_idx;

                _data_FeedBack.T0_FeedBack_2_list[0] = objT0_FeedBack_2;

               // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_FeedBack));

                _data_FeedBack = callServiceNetwork(_urlDeleteT0FeedBack2, _data_FeedBack);


                if (_data_FeedBack.return_code == 0)
                {

                    //actionIndex();
                    //MvMaster.SetActiveView(ViewIndex);
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);


                }
                else
                {
                    setError(_data_FeedBack.return_code.ToString() + " - " + _data_FeedBack.return_msg);
                }

                break;




        }
    }
    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                actionIndex();
                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {
                        
                    }
                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                actionIndex();
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                int TFBIDX2_update = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtTopicUpdate = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtTopicUpdate");
                var ddlTFB2StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlTFB2StatusUpdate");

                GvMaster.EditIndex = -1;

                _data_FeedBack.T0_FeedBack_2_list = new T0_FeedBack_2_detail[1];
                T0_FeedBack_2_detail _FeedBack_2_detail = new T0_FeedBack_2_detail();

                _FeedBack_2_detail.TFBIDX2 = TFBIDX2_update;
                _FeedBack_2_detail.Topic = txtTopicUpdate.Text;
                _FeedBack_2_detail.TFB2Status = int.Parse(ddlTFB2StatusUpdate.SelectedValue);
                _FeedBack_2_detail.CEmpIDX = emp_idx;

                _data_FeedBack.T0_FeedBack_2_list[0] = _FeedBack_2_detail;

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_FeedBack));
                _data_FeedBack = callServiceNetwork(_urlSetUpdT0FeedBack2, _data_FeedBack);

                if (_data_FeedBack.return_code == 0)
                {
                    //initPage();
                    //setDataList(dtlMenu, _dataMenu.T0_menu_list);  
                    actionIndex();


                }
                else
                {
                    setError(_data_FeedBack.return_code.ToString() + " - " + _data_FeedBack.return_msg);
                }


                break;
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj, string sysidx_menu, string menuidx_per, string role_per)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        //switch (fvName.ID)
        //{



        //}
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvPermission, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected data_FeedBack callServiceNetwork(string _cmdUrl, data_FeedBack _data_FeedBack)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_FeedBack);
        // litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _localJson);

        // convert json to object
        _data_FeedBack = (data_FeedBack)_funcTool.convertJsonToObject(typeof(data_FeedBack), _localJson);

        return _data_FeedBack;
    }


    #endregion reuse

}