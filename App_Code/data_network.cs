﻿using System;
using System.Xml.Serialization;

/// <summary>
/// Summary description for data_networkdevices
/// </summary>
/// 

[Serializable]
[XmlRoot("data_network")]
public class data_network
{
    [XmlElement("return_code")]
    public int return_code { get; set; }
    [XmlElement("return_msg")]
    public string return_msg { get; set; }

   
    [XmlElement("empinsert_list")]
    public empinsert_detail[] empinsert_list { get; set; }

    [XmlElement("network_list")]
    public network_detail[] network_list { get; set; }

    [XmlElement("bindnetwork_list")]
    public bindnetwork_detail[] bindnetwork_list { get; set; }

    [XmlElement("lognetwork_list")]
    public lognetwork_detail[] lognetwork_list { get; set; }

    [XmlElement("repairnetwork_list")]
    public repairnetwork_detail[] repairnetwork_list { get; set; }

    [XmlElement("logrepairnetwork_list")]
    public logrepairnetwork_detail[] logrepairnetwork_list { get; set; }

    [XmlElement("manetwork_list")]
    public manetwork_detail[] manetwork_list { get; set; }

    [XmlElement("logmanetwork_list")]
    public logmanetwork_detail[] logmanetwork_list { get; set; }

    [XmlElement("bindcutnetwork_list")]
    public bindcutnetwork_detail[] bindcutnetwork_list { get; set; }

    [XmlElement("logrepaircutnetwork_list")]
    public logrepaircutnetwork_detail[] logrepaircutnetwork_list { get; set; }

    [XmlElement("logmacutnetwork_list")]
    public logmacutnetwork_detail[] logmacutnetwork_list { get; set; }

    [XmlElement("cutnetwork_list")]
    public cutnetwork_detail[] cutnetwork_list { get; set; }

    [XmlElement("bindmovenetwork_list")]
    public bindmovenetwork_detail[] bindmovenetwork_list { get; set; }

    [XmlElement("movenetwork_list")]
    public movenetwork_detail[] movenetwork_list { get; set; }

    [XmlElement("exportnetwork_list")]
    public exportnetwork_detail[] exportnetwork_list { get; set; }

    [XmlElement("inexportnetwork_list")]
    public inexportnetwork_detail[] inexportnetwork_list { get; set; }

    [XmlElement("editnetwork_list")]
    public editnetwork_detail[] editnetwork_list { get; set; }

}

[Serializable]
public class empinsert_detail
{
    [XmlElement("emp_idx")]
    public int emp_idx { get; set; }


    // employee name profile
    [XmlElement("emp_name_en")]
    public string emp_name_en { get; set; }
    [XmlElement("emp_name_th")]
    public string emp_name_th { get; set; }

    [XmlElement("emp_firstname_en")]
    public string emp_firstname_en { get; set; }
    [XmlElement("emp_lastname_en")]
    public string emp_lastname_en { get; set; }
    [XmlElement("emp_firstname_th")]
    public string emp_firstname_th { get; set; }
    [XmlElement("emp_lastname_th")]
    public string emp_lastname_th { get; set; }

    [XmlElement("emp_nickname_en")]
    public string emp_nickname_en { get; set; }
    [XmlElement("emp_nickname_th")]
    public string emp_nickname_th { get; set; }
    [XmlElement("emp_fullname_th")]
    public string emp_fullname_th { get; set; }
    // employee name profile

    // employee type
    [XmlElement("emp_type_idx")]
    public int emp_type_idx { get; set; }
    [XmlElement("emp_type_name")]
    public string emp_type_name { get; set; }
    // employee type

    // employee organization profile
    [XmlElement("org_idx")]
    public int org_idx { get; set; }
    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }
    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }
    [XmlElement("rpos_idx")]
    public int rpos_idx { get; set; }

    [XmlElement("org_name_en")]
    public string org_name_en { get; set; }
    [XmlElement("dept_name_en")]
    public string dept_name_en { get; set; }
    [XmlElement("sec_name_en")]
    public string sec_name_en { get; set; }
    [XmlElement("pos_name_en")]
    public string pos_name_en { get; set; }

    [XmlElement("org_name_th")]
    public string org_name_th { get; set; }
    [XmlElement("dept_name_th")]
    public string dept_name_th { get; set; }
    [XmlElement("sec_name_th")]
    public string sec_name_th { get; set; }
    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("jobgrade_idx")]
    public int jobgrade_idx { get; set; }
    [XmlElement("jobgrade_level")]
    public int jobgrade_level { get; set; }

    [XmlElement("costcenter_idx")]
    public int costcenter_idx { get; set; }
    [XmlElement("costcenter_no")]
    public string costcenter_no { get; set; }
    [XmlElement("costcenter_name")]
    public string costcenter_name { get; set; }
    // employee organization profile

    // employee start date, probation date and status
    [XmlElement("emp_start_date")]
    public string emp_start_date { get; set; }
    [XmlElement("emp_probation_status")]
    public int emp_probation_status { get; set; }
    [XmlElement("emp_probation_date")]
    public string emp_probation_date { get; set; }
    [XmlElement("emp_end_date")]
    public string emp_end_date { get; set; }
    // employee start date, probation date and status

    // employee profile date and status
    [XmlElement("emp_createdate")]
    public string emp_createdate { get; set; }
    [XmlElement("emp_updatedate")]
    public string emp_updatedate { get; set; }
    [XmlElement("emp_status")]
    public int emp_status { get; set; }
    // employee profile date and status

    // employee update password profile
    [XmlElement("new_password")]
    public string new_password { get; set; }
    [XmlElement("type_reset")]
    public int type_reset { get; set; }
    // employee update password profile

    // vacation approver profile
    [XmlElement("emp_idx_approve1")]
    public int emp_idx_approve1 { get; set; }
    [XmlElement("emp_idx_approve2")]
    public int emp_idx_approve2 { get; set; }
    [XmlElement("emp_approve1")]
    public string emp_approve1 { get; set; }
    [XmlElement("emp_approve2")]
    public string emp_approve2 { get; set; }
    // vacation approver profile

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("emp_mobile_no")]
    public string emp_mobile_no { get; set; }

    [XmlElement("emp_email")]
    public string emp_email { get; set; }

}

[Serializable]
public class network_detail
{
    [XmlElement("u0_devicenetwork_idx")]
    public int u0_devicenetwork_idx { get; set; }

    [XmlElement("u0_doc_code")]
    public string u0_doc_code { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("category_idx")]
    public int category_idx { get; set; }

    [XmlElement("category_name")]
    public string category_name { get; set; }

    [XmlElement("generation_name")]
    public string generation_name { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("serial_number")]
    public string serial_number { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }


    [XmlElement("room_idx")]
    public int room_idx { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }

    [XmlElement("place_new_name")]
    public string place_new_name { get; set; }

    [XmlElement("room_new_name")]
    public string room_new_name { get; set; }


    [XmlElement("status_deviecs")]
    public int status_deviecs { get; set; }

    [XmlElement("status_move")]
    public int status_move { get; set; }

    [XmlElement("user_name")]
    public string user_name { get; set; }

    [XmlElement("pass_word")]
    public string pass_word { get; set; }

    [XmlElement("detail_devices")]
    public string detail_devices { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("register_number")]
    public string register_number { get; set; }

    [XmlElement("cemp_idx_create")]
    public int cemp_idx_create { get; set; }

    [XmlElement("cemp_idx_approve")]
    public int cemp_idx_approve { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("name_approve")]
    public string name_approve { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("status_approve")]
    public string status_approve { get; set; }

    [XmlElement("date_purchase_new")]
    public string date_purchase_new { get; set; }

    [XmlElement("date_expire_new")]
    public string date_expire_new { get; set; }

    [XmlElement("status_ma")]
    public int status_ma { get; set; }

    [XmlElement("detail_ma")]
    public string detail_ma { get; set; }

    [XmlElement("company_new_idx")]
    public int company_new_idx { get; set; }

    [XmlElement("company_name_new")]
    public string company_name_new { get; set; }

    [XmlElement("brand_name_edit")]
    public string brand_name_edit { get; set; }

    [XmlElement("generation_name_edit")]
    public string generation_name_edit { get; set; }

    [XmlElement("type_idx_edit")]
    public int type_idx_edit { get; set; }

    [XmlElement("category_idx_edit")]
    public int category_idx_edit { get; set; }

    [XmlElement("ip_address_edit")]
    public string ip_address_edit { get; set; }

    [XmlElement("serial_number_edit")]
    public string serial_number_edit { get; set; }

    [XmlElement("place_idx_edit")]
    public int place_idx_edit { get; set; }

    [XmlElement("room_idx_edit")]
    public int room_idx_edit { get; set; }

    [XmlElement("user_name_edit")]
    public string user_name_edit { get; set; }

    [XmlElement("asset_code_edit")]
    public string asset_code_edit { get; set; }

    [XmlElement("pass_word_edit")]
    public string pass_word_edit { get; set; }

}

[Serializable]
public class bindnetwork_detail
{

    [XmlElement("if_date")]
    public int if_date { get; set; }

    [XmlElement("u0_devicenetwork_idx")]
    public int u0_devicenetwork_idx { get; set; }

    [XmlElement("u0_doc_code")]
    public string u0_doc_code { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("category_idx")]
    public int category_idx { get; set; }

    [XmlElement("category_name")]
    public string category_name { get; set; }

    [XmlElement("generation_name")]
    public string generation_name { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("serial_number")]
    public string serial_number { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("room_idx")]
    public int room_idx { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }


    [XmlElement("place_new_idx")]
    public int place_new_idx { get; set; }

    [XmlElement("place_name_new")]
    public string place_name_new { get; set; }

    [XmlElement("room_new_idx")]
    public int room_new_idx { get; set; }

    [XmlElement("room_name_new")]
    public string room_name_new { get; set; }

    [XmlElement("status_deviecs")]
    public int status_deviecs { get; set; }

    [XmlElement("user_name")]
    public string user_name { get; set; }

    [XmlElement("pass_word")]
    public string pass_word { get; set; }

    [XmlElement("detail_devices")]
    public string detail_devices { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("register_number")]
    public string register_number { get; set; }

    [XmlElement("cemp_idx_create")]
    public int cemp_idx_create { get; set; }

    [XmlElement("cemp_idx_approve")]
    public int cemp_idx_approve { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("name_approve")]
    public string name_approve { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("status_move")]
    public int status_move { get; set; }

    [XmlElement("date_purchase_new")]
    public string date_purchase_new { get; set; }

    [XmlElement("date_expire_new")]
    public string date_expire_new { get; set; }

    [XmlElement("status_ma")]
    public int status_ma { get; set; }

    [XmlElement("detail_ma")]
    public string detail_ma { get; set; }

    [XmlElement("company_new_idx")]
    public int company_new_idx { get; set; }

    [XmlElement("company_name_new")]
    public string company_name_new { get; set; }


}

[Serializable]
public class lognetwork_detail
{
    [XmlElement("u0_devicenetwork_idx")]
    public int u0_devicenetwork_idx { get; set; }

    [XmlElement("u1_devicenetwork_idx")]
    public int u1_devicenetwork_idx { get; set; }

    [XmlElement("u0_doc_code")]
    public string u0_doc_code { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("category_idx")]
    public int category_idx { get; set; }

    [XmlElement("category_name")]
    public string category_name { get; set; }

    [XmlElement("generation_name")]
    public string generation_name { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("serial_number")]
    public string serial_number { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("room_idx")]
    public int room_idx { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }

    [XmlElement("status_deviecs")]
    public int status_deviecs { get; set; }

    [XmlElement("user_name")]
    public string user_name { get; set; }

    [XmlElement("pass_word")]
    public string pass_word { get; set; }

    [XmlElement("detail_devices")]
    public string detail_devices { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("register_number")]
    public string register_number { get; set; }

    [XmlElement("cemp_idx_create")]
    public int cemp_idx_create { get; set; }

    [XmlElement("cemp_idx_approve")]
    public int cemp_idx_approve { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("actor_des")]
    public string actor_des { get; set; }

    [XmlElement("node_name")]
    public string node_name { get; set; }

    [XmlElement("status_name")]
    public string status_name { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("name_approve")]
    public string name_approve { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }
}

[Serializable]
public class repairnetwork_detail
{
    [XmlElement("l0_devicerepair_idx")]
    public int l0_devicerepair_idx { get; set; }

    [XmlElement("u0_devicenetwork_idx")]
    public int u0_devicenetwork_idx { get; set; }

    [XmlElement("u0_doc_code")]
    public string u0_doc_code { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("category_idx")]
    public int category_idx { get; set; }

    [XmlElement("category_name")]
    public string category_name { get; set; }

    [XmlElement("generation_name")]
    public string generation_name { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("serial_number")]
    public string serial_number { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }


    [XmlElement("room_idx")]
    public int room_idx { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }


    [XmlElement("status_deviecs")]
    public int status_deviecs { get; set; }

    [XmlElement("user_name")]
    public string user_name { get; set; }

    [XmlElement("pass_word")]
    public string pass_word { get; set; }

    [XmlElement("detail_devices")]
    public string detail_devices { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("register_number")]
    public string register_number { get; set; }

    [XmlElement("cemp_idx_create")]
    public int cemp_idx_create { get; set; }

    [XmlElement("cemp_idx_approve")]
    public int cemp_idx_approve { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("name_approve")]
    public string name_approve { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("date_repair")]
    public string date_repair { get; set; }

}

[Serializable]
public class logrepairnetwork_detail
{
    [XmlElement("l0_devicerepair_idx")]
    public int l0_devicerepair_idx { get; set; }

    [XmlElement("u0_devicenetwork_idx")]
    public int u0_devicenetwork_idx { get; set; }

    [XmlElement("u0_doc_code")]
    public string u0_doc_code { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("category_idx")]
    public int category_idx { get; set; }

    [XmlElement("category_name")]
    public string category_name { get; set; }

    [XmlElement("generation_name")]
    public string generation_name { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("serial_number")]
    public string serial_number { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }


    [XmlElement("room_idx")]
    public int room_idx { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }


    [XmlElement("status_deviecs")]
    public int status_deviecs { get; set; }

    [XmlElement("user_name")]
    public string user_name { get; set; }

    [XmlElement("pass_word")]
    public string pass_word { get; set; }

    [XmlElement("detail_devices")]
    public string detail_devices { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("register_number")]
    public string register_number { get; set; }

    [XmlElement("cemp_idx_create")]
    public int cemp_idx_create { get; set; }

    [XmlElement("cemp_idx_approve")]
    public int cemp_idx_approve { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("name_approve")]
    public string name_approve { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("date_repair")]
    public string date_repair { get; set; }

}

[Serializable]
public class manetwork_detail
{
    [XmlElement("l0_devicema_idx")]
    public int l0_devicema_idx { get; set; }

    [XmlElement("u0_devicenetwork_idx")]
    public int u0_devicenetwork_idx { get; set; }

    [XmlElement("u0_doc_code")]
    public string u0_doc_code { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("category_idx")]
    public int category_idx { get; set; }

    [XmlElement("category_name")]
    public string category_name { get; set; }

    [XmlElement("generation_name")]
    public string generation_name { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("serial_number")]
    public string serial_number { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("company_name_old")]
    public string company_name_old { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_purchase_new")]
    public string date_purchase_new { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("date_expire_old")]
    public string date_expire_old { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }


    [XmlElement("room_idx")]
    public int room_idx { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }


    [XmlElement("status_deviecs")]
    public int status_deviecs { get; set; }

    [XmlElement("user_name")]
    public string user_name { get; set; }

    [XmlElement("pass_word")]
    public string pass_word { get; set; }

    [XmlElement("detail_devices")]
    public string detail_devices { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("register_number")]
    public string register_number { get; set; }

    [XmlElement("cemp_idx_create")]
    public int cemp_idx_create { get; set; }

    [XmlElement("cemp_idx_approve")]
    public int cemp_idx_approve { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("name_approve")]
    public string name_approve { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("date_start")]
    public string date_start { get; set; }

    [XmlElement("date_end")]
    public string date_end { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

}

[Serializable]
public class logmanetwork_detail
{
    [XmlElement("l0_devicescompany_idx")]
    public int l0_devicescompany_idx { get; set; }

    [XmlElement("u0_devicenetwork_idx")]
    public int u0_devicenetwork_idx { get; set; }

    [XmlElement("u0_doc_code")]
    public string u0_doc_code { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("category_idx")]
    public int category_idx { get; set; }

    [XmlElement("category_name")]
    public string category_name { get; set; }

    [XmlElement("generation_name")]
    public string generation_name { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("serial_number")]
    public string serial_number { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }


    [XmlElement("room_idx")]
    public int room_idx { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }


    [XmlElement("status_deviecs")]
    public int status_deviecs { get; set; }

    [XmlElement("user_name")]
    public string user_name { get; set; }

    [XmlElement("pass_word")]
    public string pass_word { get; set; }

    [XmlElement("detail_devices")]
    public string detail_devices { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("register_number")]
    public string register_number { get; set; }

    [XmlElement("cemp_idx_create")]
    public int cemp_idx_create { get; set; }

    [XmlElement("cemp_idx_approve")]
    public int cemp_idx_approve { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("name_approve")]
    public string name_approve { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("date_start")]
    public string date_start { get; set; }

    [XmlElement("date_end")]
    public string date_end { get; set; }

}

[Serializable]
public class bindcutnetwork_detail
{

    [XmlElement("diff_date")]
    public int diff_date { get; set; }

    [XmlElement("if_date")]
    public int if_date { get; set; }

    [XmlElement("u0_devicenetwork_idx")]
    public int u0_devicenetwork_idx { get; set; }

    [XmlElement("u0_doc_code")]
    public string u0_doc_code { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("category_idx")]
    public int category_idx { get; set; }

    [XmlElement("category_name")]
    public string category_name { get; set; }

    [XmlElement("generation_name")]
    public string generation_name { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("serial_number")]
    public string serial_number { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("room_idx")]
    public int room_idx { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }

    [XmlElement("status_deviecs")]
    public int status_deviecs { get; set; }

    [XmlElement("user_name")]
    public string user_name { get; set; }

    [XmlElement("pass_word")]
    public string pass_word { get; set; }

    [XmlElement("detail_devices")]
    public string detail_devices { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("register_number")]
    public string register_number { get; set; }

    [XmlElement("cemp_idx_create")]
    public int cemp_idx_create { get; set; }

    [XmlElement("cemp_idx_approve")]
    public int cemp_idx_approve { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("name_approve")]
    public string name_approve { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }


}

[Serializable]
public class logrepaircutnetwork_detail
{
    [XmlElement("l0_devicerepair_idx")]
    public int l0_devicerepair_idx { get; set; }

    [XmlElement("u0_devicenetwork_idx")]
    public int u0_devicenetwork_idx { get; set; }

    [XmlElement("u0_doc_code")]
    public string u0_doc_code { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("category_idx")]
    public int category_idx { get; set; }

    [XmlElement("category_name")]
    public string category_name { get; set; }

    [XmlElement("generation_name")]
    public string generation_name { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("serial_number")]
    public string serial_number { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }


    [XmlElement("room_idx")]
    public int room_idx { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }


    [XmlElement("status_deviecs")]
    public int status_deviecs { get; set; }

    [XmlElement("user_name")]
    public string user_name { get; set; }

    [XmlElement("pass_word")]
    public string pass_word { get; set; }

    [XmlElement("detail_devices")]
    public string detail_devices { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("register_number")]
    public string register_number { get; set; }

    [XmlElement("cemp_idx_create")]
    public int cemp_idx_create { get; set; }

    [XmlElement("cemp_idx_approve")]
    public int cemp_idx_approve { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("name_approve")]
    public string name_approve { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("date_repair")]
    public string date_repair { get; set; }

}

[Serializable]
public class logmacutnetwork_detail
{
    [XmlElement("l0_devicescompany_idx")]
    public int l0_devicescompany_idx { get; set; }

    [XmlElement("u0_devicenetwork_idx")]
    public int u0_devicenetwork_idx { get; set; }

    [XmlElement("u0_doc_code")]
    public string u0_doc_code { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("category_idx")]
    public int category_idx { get; set; }

    [XmlElement("category_name")]
    public string category_name { get; set; }

    [XmlElement("generation_name")]
    public string generation_name { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("serial_number")]
    public string serial_number { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }


    [XmlElement("room_idx")]
    public int room_idx { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }


    [XmlElement("status_deviecs")]
    public int status_deviecs { get; set; }

    [XmlElement("user_name")]
    public string user_name { get; set; }

    [XmlElement("pass_word")]
    public string pass_word { get; set; }

    [XmlElement("detail_devices")]
    public string detail_devices { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("register_number")]
    public string register_number { get; set; }

    [XmlElement("cemp_idx_create")]
    public int cemp_idx_create { get; set; }

    [XmlElement("cemp_idx_approve")]
    public int cemp_idx_approve { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("name_approve")]
    public string name_approve { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("comment")]
    public string comment { get; set; }

    [XmlElement("date_start")]
    public string date_start { get; set; }

    [XmlElement("date_end")]
    public string date_end { get; set; }

}

[Serializable]
public class cutnetwork_detail
{
    [XmlElement("u0_devicenetwork_idx")]
    public int u0_devicenetwork_idx { get; set; }

    [XmlElement("diff_date")]
    public string diff_date { get; set; }

    [XmlElement("u0_doc_code")]
    public string u0_doc_code { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("category_idx")]
    public int category_idx { get; set; }

    [XmlElement("category_name")]
    public string category_name { get; set; }

    [XmlElement("generation_name")]
    public string generation_name { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("serial_number")]
    public string serial_number { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }


    [XmlElement("room_idx")]
    public int room_idx { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }


    [XmlElement("status_deviecs")]
    public int status_deviecs { get; set; }

    [XmlElement("user_name")]
    public string user_name { get; set; }

    [XmlElement("pass_word")]
    public string pass_word { get; set; }

    [XmlElement("detail_devices")]
    public string detail_devices { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("register_number")]
    public string register_number { get; set; }

    [XmlElement("cemp_idx_create")]
    public int cemp_idx_create { get; set; }

    [XmlElement("cemp_idx_approve")]
    public int cemp_idx_approve { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("name_approve")]
    public string name_approve { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }
}

[Serializable]
public class bindmovenetwork_detail
{

    [XmlElement("diff_date")]
    public int diff_date { get; set; }

    [XmlElement("if_date")]
    public int if_date { get; set; }

    [XmlElement("u0_devicenetwork_idx")]
    public int u0_devicenetwork_idx { get; set; }

    [XmlElement("u0_doc_code")]
    public string u0_doc_code { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("category_idx")]
    public int category_idx { get; set; }

    [XmlElement("category_name")]
    public string category_name { get; set; }

    [XmlElement("generation_name")]
    public string generation_name { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("serial_number")]
    public string serial_number { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("room_idx")]
    public int room_idx { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }

    [XmlElement("status_deviecs")]
    public int status_deviecs { get; set; }

    [XmlElement("user_name")]
    public string user_name { get; set; }

    [XmlElement("pass_word")]
    public string pass_word { get; set; }

    [XmlElement("detail_devices")]
    public string detail_devices { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("register_number")]
    public string register_number { get; set; }

    [XmlElement("cemp_idx_create")]
    public int cemp_idx_create { get; set; }

    [XmlElement("cemp_idx_approve")]
    public int cemp_idx_approve { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("name_approve")]
    public string name_approve { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }


}

[Serializable]
public class movenetwork_detail
{
    [XmlElement("u0_devicenetwork_idx")]
    public int u0_devicenetwork_idx { get; set; }

    [XmlElement("u0_doc_code")]
    public string u0_doc_code { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("category_idx")]
    public int category_idx { get; set; }

    [XmlElement("category_name")]
    public string category_name { get; set; }

    [XmlElement("generation_name")]
    public string generation_name { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("serial_number")]
    public string serial_number { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("room_idx")]
    public int room_idx { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }

    [XmlElement("place_new_idx")]
    public int place_new_idx { get; set; }

    [XmlElement("place_new_name")]
    public string place_new_name { get; set; }

    [XmlElement("room_new_idx")]
    public int room_new_idx { get; set; }

    [XmlElement("room_new_name")]
    public string room_new_name { get; set; }

    [XmlElement("status_deviecs")]
    public int status_deviecs { get; set; }

    [XmlElement("user_name")]
    public string user_name { get; set; }

    [XmlElement("pass_word")]
    public string pass_word { get; set; }

    [XmlElement("detail_devices")]
    public string detail_devices { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("register_number")]
    public string register_number { get; set; }

    [XmlElement("cemp_idx_create")]
    public int cemp_idx_create { get; set; }

    [XmlElement("cemp_idx_approve")]
    public int cemp_idx_approve { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("name_approve")]
    public string name_approve { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }



}

[Serializable]
public class exportnetwork_detail
{
   
    [XmlElement("เลขทะเบียนอุปกรณ์")]
    public string เลขทะเบียนอุปกรณ์ { get; set; }

    [XmlElement("สถานที่ติดตั้ง")]
    public string สถานที่ติดตั้ง { get; set; }

    [XmlElement("อาคาร")]
    public string อาคาร { get; set; }

    [XmlElement("ประเภทอุปกรณ์")]
    public string ประเภทอุปกรณ์ { get; set; }

    [XmlElement("ชนิดอุปกรณ์")]
    public string ชนิดอุปกรณ์ { get; set; }

    [XmlElement("ยี่ห้อ")]
    public string ยี่ห้อ { get; set; }

    [XmlElement("รุ่น")]
    public string รุ่น { get; set; }

    [XmlElement("บริษัทประกัน")]
    public string บริษัทประกัน { get; set; }

    [XmlElement("วันที่เริ่มประกัน")]
    public string วันที่เริ่มประกัน { get; set; }

    [XmlElement("วันที่หมดประกัน")]
    public string วันที่หมดประกัน { get; set; }

    [XmlElement("ชื่อผู้สร้างรายการ")]
    public string ชื่อผู้สร้างรายการ { get; set; }

    [XmlElement("ชื่อผู้อนุมัติ")]
    public string ชื่อผู้อนุมัติ { get; set; }

    [XmlElement("รายละเอียดอุปกรณ์")]
    public string รายละเอียดอุปกรณ์ { get; set; }

}

[Serializable]
public class inexportnetwork_detail
{

    [XmlElement("if_date")]
    public int if_date { get; set; }

    [XmlElement("u0_devicenetwork_idx")]
    public int u0_devicenetwork_idx { get; set; }

    [XmlElement("u0_doc_code")]
    public string u0_doc_code { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("category_idx")]
    public int category_idx { get; set; }

    [XmlElement("category_name")]
    public string category_name { get; set; }

    [XmlElement("generation_name")]
    public string generation_name { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("serial_number")]
    public string serial_number { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_name")]
    public string place_name { get; set; }

    [XmlElement("room_idx")]
    public int room_idx { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }


    [XmlElement("place_new_idx")]
    public int place_new_idx { get; set; }

    [XmlElement("place_name_new")]
    public string place_name_new { get; set; }

    [XmlElement("room_new_idx")]
    public int room_new_idx { get; set; }

    [XmlElement("room_name_new")]
    public string room_name_new { get; set; }

    [XmlElement("status_deviecs")]
    public int status_deviecs { get; set; }

    [XmlElement("user_name")]
    public string user_name { get; set; }

    [XmlElement("pass_word")]
    public string pass_word { get; set; }

    [XmlElement("detail_devices")]
    public string detail_devices { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("register_number")]
    public string register_number { get; set; }

    [XmlElement("cemp_idx_create")]
    public int cemp_idx_create { get; set; }

    [XmlElement("cemp_idx_approve")]
    public int cemp_idx_approve { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("name_approve")]
    public string name_approve { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("status_move")]
    public int status_move { get; set; }

    [XmlElement("date_purchase_new")]
    public string date_purchase_new { get; set; }

    [XmlElement("date_expire_new")]
    public string date_expire_new { get; set; }

    [XmlElement("status_ma")]
    public int status_ma { get; set; }

    [XmlElement("detail_ma")]
    public string detail_ma { get; set; }

    [XmlElement("company_new_idx")]
    public int company_new_idx { get; set; }

    [XmlElement("company_name_new")]
    public string company_name_new { get; set; }


}

[Serializable]
public class editnetwork_detail
{

    [XmlElement("if_date")]
    public int if_date { get; set; }

    [XmlElement("u0_devicenetwork_idx")]
    public int u0_devicenetwork_idx { get; set; }

    [XmlElement("u0_doc_code")]
    public string u0_doc_code { get; set; }

    [XmlElement("brand_name")]
    public string brand_name { get; set; }

    [XmlElement("type_idx")]
    public int type_idx { get; set; }

    [XmlElement("type_name")]
    public string type_name { get; set; }

    [XmlElement("category_idx")]
    public int category_idx { get; set; }

    [XmlElement("category_name")]
    public string category_name { get; set; }

    [XmlElement("generation_name")]
    public string generation_name { get; set; }

    [XmlElement("ip_address")]
    public string ip_address { get; set; }

    [XmlElement("serial_number")]
    public string serial_number { get; set; }

    [XmlElement("company_idx")]
    public int company_idx { get; set; }

    [XmlElement("company_name")]
    public string company_name { get; set; }

    [XmlElement("date_purchase")]
    public string date_purchase { get; set; }

    [XmlElement("date_expire")]
    public string date_expire { get; set; }

    [XmlElement("place_idx")]
    public int place_idx { get; set; }

    [XmlElement("place_nameedit")]
    public string place_nameedit { get; set; }

    [XmlElement("room_idx")]
    public int room_idx { get; set; }

    [XmlElement("room_name")]
    public string room_name { get; set; }


    [XmlElement("place_new_idx")]
    public int place_new_idx { get; set; }

    [XmlElement("place_name_new")]
    public string place_name_new { get; set; }

    [XmlElement("room_new_idx")]
    public int room_new_idx { get; set; }

    [XmlElement("room_name_new")]
    public string room_name_new { get; set; }

    [XmlElement("status_deviecs")]
    public int status_deviecs { get; set; }

    [XmlElement("user_name")]
    public string user_name { get; set; }

    [XmlElement("pass_word")]
    public string pass_word { get; set; }

    [XmlElement("detail_devices")]
    public string detail_devices { get; set; }

    [XmlElement("asset_code")]
    public string asset_code { get; set; }

    [XmlElement("register_number")]
    public string register_number { get; set; }

    [XmlElement("cemp_idx_create")]
    public int cemp_idx_create { get; set; }

    [XmlElement("cemp_idx_approve")]
    public int cemp_idx_approve { get; set; }

    [XmlElement("create_date")]
    public string create_date { get; set; }

    [XmlElement("update_date")]
    public string update_date { get; set; }

    [XmlElement("m0_node_idx")]
    public int m0_node_idx { get; set; }

    [XmlElement("m0_actor_idx")]
    public int m0_actor_idx { get; set; }

    [XmlElement("doc_decision")]
    public int doc_decision { get; set; }

    [XmlElement("org_idx")]
    public int org_idx { get; set; }

    [XmlElement("rdept_idx")]
    public int rdept_idx { get; set; }

    [XmlElement("rsec_idx")]
    public int rsec_idx { get; set; }

    [XmlElement("cemp_idx")]
    public int cemp_idx { get; set; }

    [XmlElement("status_desc")]
    public string status_desc { get; set; }

    [XmlElement("name_create")]
    public string name_create { get; set; }

    [XmlElement("name_approve")]
    public string name_approve { get; set; }

    [XmlElement("date_create")]
    public string date_create { get; set; }

    [XmlElement("pos_name_th")]
    public string pos_name_th { get; set; }

    [XmlElement("status_move")]
    public int status_move { get; set; }

    [XmlElement("date_purchase_new")]
    public string date_purchase_new { get; set; }

    [XmlElement("date_expire_new")]
    public string date_expire_new { get; set; }

    [XmlElement("status_ma")]
    public int status_ma { get; set; }

    [XmlElement("detail_ma")]
    public string detail_ma { get; set; }

    [XmlElement("company_new_idx")]
    public int company_new_idx { get; set; }

    [XmlElement("company_name_new")]
    public string company_name_new { get; set; }


}
