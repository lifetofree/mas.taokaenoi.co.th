﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_cims_m0_lab.aspx.cs" Inherits="websystem_MasterData_qa_cims_m0_lab" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <div class="col-sm-12">
        <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
        <asp:Literal ID="test_lab" runat="server"></asp:Literal>
    </div>

    <asp:MultiView ID="MvMaster_lab" runat="server">
        <asp:View ID="view_Genaral" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btn_addlab" Visible="true" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มข้อมูลแลป" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdAddlab" CommandArgument="0" title="เพิ่มข้อมูลแลป"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มข้อมูลแลปทดสอบ</asp:LinkButton>
            </div>

            <%--  DIV ADD--%>
            <asp:FormView ID="Fv_Insert_Result" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มข้อมูลแลปทดสอบ</h3>

                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">
                                        <div class="form-group">
                                            <asp:Label ID="la_namelab" runat="server" Text="ชื่อของแลปที่ส่งตรวจ" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtlab_names" runat="server" CssClass="form-control" placeholder="กรอกชื่อแลปที่ส่งตรวจ ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorLabname" runat="server"
                                                    ControlToValidate="txtlab_names" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อของแลปที่ส่งตรวจ" ValidationGroup="saveM0Lab" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenderlabTname" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorLabname" Width="250" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Lalab_type" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทของแลปที่ส่งตรวจ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="DDcal_type" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="ReqDDlab" runat="server"
                                                    ControlToValidate="DDcal_type" Display="None" InitialValue="0" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกประเภทแลปที่ส่งตรวจ" ValidationGroup="saveM0Lab" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValDDlab" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqDDlab" Width="250" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="สถานที่ส่งตรวจ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="DD_place" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="ReqDDplace" runat="server"
                                                    ControlToValidate="DD_place" Display="None" InitialValue="0" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกสถานที่ส่งตรวจ" ValidationGroup="saveM0Lab" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValDDplace" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReqDDplace" Width="250" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="la_statuslab_type" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="DDlab_status" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label"></label>
                                            <div class="col-sm-9">
                                                <asp:LinkButton ID="Lbtn_submit_lab" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                    Text="Save" OnCommand="btnCommand" CommandName="cmdSave" ValidationGroup="saveM0Lab" CommandArgument="0"></asp:LinkButton>
                                                <asp:LinkButton ID="Lbtn_cancel_lab" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                    Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0"></asp:LinkButton>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <hr />
                                <%--  /DIV END ADD--%>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>
            <%--select lab--%>
            <asp:GridView ID="Gv_select_lab" runat="server"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>ไม่มีข้อมูลของแลปที่ตรวจ</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="8%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="text-align: center; padding-top: 5px;">
                                <asp:Label ID="lab_idx" runat="server" Visible="false" Text='<%# Eval("m0_lab_idx") %>' />
                                <%# (Container.DataItemIndex +1) %>
                            </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="ID_lab" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("m0_lab_idx") %>'></asp:TextBox>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="La_labname" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อแลปที่ส่งตรวจ" />
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="Name_lab" runat="server" CssClass="form-control " Text='<%# Eval("lab_name") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Re_Name_lab" runat="server"
                                                    ControlToValidate="Name_lab" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อของแลปที่ส่งตรวจ" ValidationGroup="EditM0Lab" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_labTname" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_Name_lab" Width="250" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lab_typename" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทของแลปทดสอบ" />
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_IDcaltype" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("cal_type_idx") %>'></asp:TextBox>
                                                <asp:DropDownList ID="DD_caltype" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorEditDD1" runat="server"
                                                    ControlToValidate="DD_caltype" InitialValue="0" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกประเภทของแลปทดสอบ" ValidationGroup="EditM0Lab" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenderEditDD1" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorEditDD1" Width="250" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="lab_place" CssClass="col-sm-3 control-label" runat="server" Text="สถานที่ส่งตรวจ" />
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_IDlabplace" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("place_idx") %>'></asp:TextBox>
                                                <asp:DropDownList ID="DD_labplace" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorEditDD2" runat="server"
                                                    ControlToValidate="DD_labplace" InitialValue="0" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกสถานที่ส่งตรวจ" ValidationGroup="EditM0Lab" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenderEditDD2" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorEditDD2" Width="250" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>


                                        <div class="form-group">
                                            <asp:Label ID="lbupdates_lab" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddEdit_lab" Text='<%# Eval("lab_status") %>'
                                                    CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1">Online</asp:ListItem>
                                                    <asp:ListItem Value="0">Offline</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-9">
                                                <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="EditM0Lab" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                    Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อแลปทดสอบ" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="30%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lab_name" runat="server"
                                        Text='<%# Eval("lab_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ประเภทของแลปทดสอบ" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lblab_type" Visible="true" runat="server"
                                        Text='<%# Eval("cal_type_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานที่ส่งตรวจ" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lb_place" Visible="true" runat="server"
                                        Text='<%# Eval("place_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lblab_status" Visible="false" runat="server"
                                        Text='<%# Eval("lab_status") %>'></asp:Label>
                                    <asp:Label ID="lab_statusOpen" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="แลปนี้ดำเดินการอยู่">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="lab_statusClose" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="แลปนี้จบการทำงานแล้ว" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="btnview_exa" CssClass="text-read" runat="server" CommandName="cmdview_exa"
                                    data-toggle="tooltip" OnCommand="btnCommand"
                                    CommandArgument='<%#Eval("m0_lab_idx")%>' title="view">
                                        <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i></asp:LinkButton>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="btnTodelete_lab"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบแลปนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("m0_lab_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </asp:View>

        <asp:View ID="view_CreateRoot" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnBack" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdback" title=""><i class="fa fa-reply" aria-hidden="true"></i> กลับ</asp:LinkButton>
                <asp:LinkButton ID="btnAdd" CssClass="btn btn-primary" Visible="true" runat="server" data-original-title="เพิ่มรายการที่ตรวจสอบ" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdAddlab" CommandArgument="1" title=""><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มรายการที่ตรวจสอบ</asp:LinkButton>
            </div>
            <!--formview insert Level 2-->
            <asp:FormView ID="fvform_Insert_Root" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">เพิ่มรายการที่ตรวจ</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ชื่อแลปทดสอบ</label>
                                    <div class="col-sm-4">
                                        <asp:TextBox ID="txt_IDtestDetail" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("m0_lab_idx") %>'></asp:TextBox>
                                        <asp:TextBox ID="tbSetLabList" runat="server" CssClass="form-control"
                                            placeholder="กรอกชื่อแลป ..." Enabled="false" />

                                    </div>
                                    <label class="col-sm-1 control-label">สถานะ</label>
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddlRootSetStatus" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ชื่ออุปกรณ์</label>
                                    <div class="col-sm-8">
                                        <%-- <asp:TextBox ID="txt_IDtestDetail" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("test_detail_idx") %>'></asp:TextBox>--%>
                                        <asp:DropDownList ID="DD_EquipmentName" CssClass="form-control" runat="server">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorDD_EquipmentName" runat="server"
                                            ControlToValidate="DD_EquipmentName" InitialValue="0" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือกชื่ออุปกรณ์" ValidationGroup="SaveM1Lab" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" PopupPosition="BottomRight"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorDD_EquipmentName" Width="250" />
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ใบรับรอง</label>
                                    <div class="col-sm-4">
                                        <asp:RadioButtonList ID="rb_cer" runat="server" CssClass="radioButtonList">
                                            <asp:ListItem Text="มีใบรับรอง" Value="1" />
                                            <asp:ListItem Text="ไม่มีใบรับรอง" Value="0" />
                                        </asp:RadioButtonList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorlabcerm1" runat="server"
                                            ControlToValidate="rb_cer" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณาเลือกใบรับรอง" ValidationGroup="SaveM1Lab" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenderlabm1" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorlabcerm1" Width="200" />

                                    </div>
                                    <div class="col-sm-5"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label"></label>
                                    <div class="col-sm-9">
                                        <asp:LinkButton ID="lnkbtnSave" ValidationGroup="SaveM1Lab" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="1"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="1"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>
            <%--select lab M1--%>
            <asp:GridView ID="Gv_selectLabM1" runat="server"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnRowDataBound="Master_RowDataBound"
                OnRowUpdating="Master_RowUpdating"
                OnRowEditing="Master_RowEditing"
                OnRowCancelingEdit="Master_RowCancelingEdit"
                OnPageIndexChanging="Master_PageIndexChanging"
                AutoPostBack="false">

                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center; color: red"><b>ไม่มีข้อมูลของแลปที่ตรวจ</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="8%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                              <div style="text-align: center; padding-top: 5px;">
                            <asp:Label ID="m1Lab_idx" runat="server" Visible="false" Text='<%# Eval("m1_lab_idx") %>' />
                            <%# (Container.DataItemIndex +1) %>
                             </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="ID_labM1" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("m1_lab_idx") %>'></asp:TextBox>
                                            <asp:TextBox ID="txt_IDM0Edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("m0_lab_idx") %>'></asp:TextBox>
                                        </div>
                                        <%--<div class="form-group">
                                            <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อแลปทดสอบ" />
                                            <div class="col-sm-7">--%>

                                        <%--<asp:TextBox ID="tbM0Edit" runat="server" CssClass="form-control" Text='<%# Eval("lab_name") %>'
                                                    placeholder="ชื่อแลป ..." Enabled="false" />--%>
                                        <%--   </div>
                                            <div class="col-sm-2"></div>
                                        </div>--%>
                                        <div class="form-group">
                                            <asp:Label ID="lb_testdetail" CssClass="col-sm-3 control-label" runat="server" Text="ชื่ออุปกรณ์" />
                                            <div class="col-sm-7">
                                                <asp:TextBox ID="txt_equipmentname" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("equipment_idx") %>'></asp:TextBox>
                                                <asp:DropDownList ID="DD_equipmentname" CssClass="form-control" runat="server">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorEditlabm1" runat="server"
                                                    ControlToValidate="DD_equipmentname" InitialValue="0" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกชื่ออุปกรณ์" ValidationGroup="EditM1Lab" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenderlabm1" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorEditlabm1" Width="200" />
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lbupdates_lab" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-7">
                                                <asp:DropDownList ID="ddEdit_statusLab" Text='<%# Eval("status") %>'
                                                    CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1">Online</asp:ListItem>
                                                    <asp:ListItem Value="0">Offline</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lb_certi" CssClass="col-sm-3 control-label" runat="server" Text="ใบรับรอง" />
                                            <div class="col-sm-2">
                                                <asp:TextBox ID="text_Certificate" Visible="false" runat="server" CssClass="form-control" Text='<%# Eval("certificate") %>'></asp:TextBox>
                                                <asp:RadioButtonList ID="Ra_Certificate" runat="server">
                                                    <asp:ListItem Value="1">มีใบรับรอง</asp:ListItem>
                                                    <asp:ListItem Value="0">ไม่มีใบรับรอง</asp:ListItem>
                                                </asp:RadioButtonList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorEditcerm1" runat="server"
                                                    ControlToValidate="Ra_Certificate" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกใบรับรอง" ValidationGroup="EditM1Lab" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorEditcerm1" Width="200" />
                                            </div>
                                            <div class="col-sm-7"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-4">
                                                <asp:LinkButton ID="btnupdate" ValidationGroup="EditM1Lab" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                    Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่ออุปกรณ์" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="30%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="m2Lab_idx" runat="server" Visible="false" Text='<%# Eval("equipment_idx") %>' />
                                    <asp:Label ID="lb_nametestDtail" Visible="true" runat="server"
                                        Text='<%# Eval("equipment_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbtestDtail_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("status") %>'></asp:Label>
                                    <asp:Label ID="testDtail_statusOpen" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="testDtail_statusClose" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ใบรับรอง" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lb_certificate" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("certificate") %>'></asp:Label>
                                    <asp:Label ID="Lbhas_certificate" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="มีใบรับรอง"
                                        CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>มีใบรับรอง</b></span>
                                    </asp:Label>
                                    <asp:Label ID="Lbnohas_certificate" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="ไม่มีใบรับรอง" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>ไม่มีใบรับรอง</b></span>
                                    </asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <div style="padding-top: 5px">
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="btnTodelete_labM1"
                                    data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการที่ตรวจนี้ใช่หรือไม่ ?')"
                                    CommandArgument='<%#Eval("m1_lab_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>

        </asp:View>
    </asp:MultiView>
</asp:Content>

