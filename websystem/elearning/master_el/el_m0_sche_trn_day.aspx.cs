﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_el_m0_sche_trn_day : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_elearning _data_elearning = new data_elearning();
    function_dmu _func_dmu = new function_dmu();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetel_m0_sche_trn_day = _serviceUrl + ConfigurationManager.AppSettings["urlGetel_m0_sche_trn_day"];
    static string _urlSetInsel_m0_sche_trn_day = _serviceUrl + ConfigurationManager.AppSettings["urlSetInsel_m0_sche_trn_day"];
    static string _urlSetUpdel_m0_sche_trn_day = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdel_m0_sche_trn_day"];
    static string _urlDelel_m0_sche_trn_day = _serviceUrl + ConfigurationManager.AppSettings["urlDelel_m0_sche_trn_day"];
    static string _urlGetErrorel_m0_sche_trn_day = _serviceUrl + ConfigurationManager.AppSettings["urlGetErrorel_m0_sche_trn_day"];
    static string _urlsendEmail_OpenTraning = _serviceUrl + ConfigurationManager.AppSettings["urlsendEmail_OpenTraning"];

    string _localJson = "";
    int _tempInt = 0;

    string _defaultDdlText;
    string _defaultDdlValue;

    int emp_idx = 0;
    #endregion initial function/data


    protected void Page_Load(object sender, EventArgs e)
    {
        emp_idx = int.Parse(Session["emp_idx"].ToString());

        if (!IsPostBack)
        {
            initPage();
            actionIndex();

        }
    }

    #region selected   
    protected void actionIndex()
    {
        pnlsearch.Visible = true;
        data_elearning data_elearning = new data_elearning();
        data_elearning.el_m0_sche_trn_day_action = new m0_sche_trn_day[1];

        m0_sche_trn_day obj = new m0_sche_trn_day();

        obj.m0_sche_trn_day_idx = 0;
        if (txtFilterKeyword.Text != "")
        {
            obj.filter_keyword = _func_dmu.zDateToDB(txtFilterKeyword.Text);
        }
        data_elearning.el_m0_sche_trn_day_action[0] = obj;
        //litDebug.Text = _funcTool.convertObjectToXml(data_elearning);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_elearning));
        //  litDebug.Text = _func_dmu.zJson(_urlGetel_m0_sche_trn_day, data_elearning);
        data_elearning = _func_dmu.zCallServiceNetwork(_urlGetel_m0_sche_trn_day, data_elearning);

        setGridData(GvMaster, data_elearning.el_m0_sche_trn_day_action);

    }
    #endregion selected 


    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        int _m0_sche_trn_day_idx;
        string _txtsche_trn_day_from, _txtsche_trn_day_to, _txtsche_trn_day_remark;
        int _cemp_idx;

        m0_sche_trn_day objM0_ProductType = new m0_sche_trn_day();

        switch (cmdName)
        {

            case "btnToInsert":
                MvMaster.SetActiveView(ViewInsert);
                break;
            case "btnCancel":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "btnInsert":
                _txtsche_trn_day_from = ((TextBox)ViewInsert.FindControl("txtsche_trn_day_from")).Text.Trim();
                _txtsche_trn_day_to = ((TextBox)ViewInsert.FindControl("txtsche_trn_day_to")).Text.Trim();
                _txtsche_trn_day_remark = ((TextBox)ViewInsert.FindControl("txtsche_trn_day_remark")).Text.Trim();
                DropDownList _ddlsche_trn_day_status = (DropDownList)ViewInsert.FindControl("ddlsche_trn_day_status");
                _cemp_idx = emp_idx;

                m0_sche_trn_day obj = new m0_sche_trn_day();
                _data_elearning.el_m0_sche_trn_day_action = new m0_sche_trn_day[1];
                obj.m0_sche_trn_day_idx = 0;//_type_idx; 
                obj.sche_trn_day_from = _func_dmu.zDateToDB(_txtsche_trn_day_from);
                obj.sche_trn_day_to = _func_dmu.zDateToDB(_txtsche_trn_day_to);
                obj.sche_trn_day_remark = _txtsche_trn_day_remark;
                obj.sche_trn_day_status = int.Parse(_ddlsche_trn_day_status.SelectedValue);
                obj.sche_trn_day_created_by = _cemp_idx;

                _data_elearning.el_m0_sche_trn_day_action[0] = obj;

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
                if (CheckErrorDate(_txtsche_trn_day_from, _txtsche_trn_day_to) == false)
                {
                    _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetInsel_m0_sche_trn_day, _data_elearning);

                    if (_data_elearning.return_code == 0)
                    {
                        sendEmail(int.Parse(_ddlsche_trn_day_status.SelectedValue));
                        actionIndex();
                        Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                    else
                    {
                        setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('วันที่เริ่มมากกว่าวันที่สิ้นสุด ไม่สามารถเพิ่มได้');", true);
                }

                break;
            case "btnDelete":

                _m0_sche_trn_day_idx = int.Parse(cmdArg);
                _cemp_idx = emp_idx;
                _data_elearning.el_m0_sche_trn_day_action = new m0_sche_trn_day[1];
                objM0_ProductType.m0_sche_trn_day_idx = _m0_sche_trn_day_idx;
                _data_elearning.el_m0_sche_trn_day_action[0] = objM0_ProductType;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));

                _data_elearning = _func_dmu.zCallServiceNetwork(_urlDelel_m0_sche_trn_day, _data_elearning);
                if (_data_elearning.return_code == 0)
                {
                    Page.Response.Redirect(Page.Request.Url.ToString(), true);
                }
                else
                {
                    setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                }

                break;
            case "btnFilter":
                actionIndex();
                break;



        }
    }
    #endregion btnCommand

    #region bind data
    protected void getGridData(string _cmdName, int _actionType)
    {
        // setGridData(gvBookingType, ViewState["listData"]);
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                actionIndex();
                break;
        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    pnlsearch.Visible = false;
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex) //to overlook header row
                    {

                    }
                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                actionIndex();
                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = -1;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
        }
    }

    private Boolean CheckErrorDate(string date_from, string date_to)
    {
        Boolean bError = false;
        if ((date_from != "") && (date_to != ""))
        {
            date_from = _func_dmu.zDateToDB(date_from);
            date_to = _func_dmu.zDateToDB(date_to);
            if ((date_from != "") && (date_to != ""))
            {
                if (DateTime.Parse(date_from) > DateTime.Parse(date_to))
                {
                    bError = true;
                }
            }
        }

        return bError;
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMaster":


                int m0_sche_trn_day_idx_update = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtsche_trn_day_from = ((TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtsche_trn_day_fromUpdate")).Text.Trim();
                var txtsche_trn_day_to = ((TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtsche_trn_day_toUpdate")).Text.Trim();
                var txtsche_trn_day_remark = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtsche_trn_day_remarkUpdate");
                var ddlsche_trn_day_status = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlsche_trn_day_statusUpdate");

                //if (checkCodeError(m0_sche_trn_day_idx_update, txtsche_trn_day_code.Text.Trim()) == true)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('รหัสนี้มีในระบบแล้ว ไม่สามารถเพิ่มได้');", true);
                //    return;
                //}

                GvMaster.EditIndex = -1;

                _data_elearning.el_m0_sche_trn_day_action = new m0_sche_trn_day[1];
                m0_sche_trn_day obj = new m0_sche_trn_day();

                obj.m0_sche_trn_day_idx = m0_sche_trn_day_idx_update;
                obj.sche_trn_day_from = _func_dmu.zDateToDB(txtsche_trn_day_from);
                obj.sche_trn_day_to = _func_dmu.zDateToDB(txtsche_trn_day_to);
                obj.sche_trn_day_remark = txtsche_trn_day_remark.Text;
                obj.sche_trn_day_status = int.Parse(ddlsche_trn_day_status.SelectedValue);
                obj.sche_trn_day_updated_by = emp_idx;

                _data_elearning.el_m0_sche_trn_day_action[0] = obj;
                if (CheckErrorDate(txtsche_trn_day_from, txtsche_trn_day_to) == false)
                {
                    // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));
                    _data_elearning = _func_dmu.zCallServicePostNetwork(_urlSetUpdel_m0_sche_trn_day, _data_elearning);
                    if (_data_elearning.return_code == 0)
                    {
                        sendEmail(int.Parse(ddlsche_trn_day_status.SelectedValue));
                        actionIndex();
                    }
                    else
                    {
                        setError(_data_elearning.return_code.ToString() + " - " + _data_elearning.return_msg);
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('วันที่เริ่มมากกว่าวันที่สิ้นสุด ไม่สามารถเพิ่มได้');", true);
                }
                break;
        }
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj, string sysidx_menu, string menuidx_per, string role_per)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();

        //switch (fvName.ID)
        //{



        //}
    }

    protected void setDataList(DataList dtlName, Object obj)
    {
        dtlName.DataSource = obj;
        dtlName.DataBind();
    }

    #endregion bind data

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvMenu, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void initPagePermission()
    {
        clearSession();
        clearViewState();

        // divAction.Visible = true;
        // gvBookingType.Visible = true;
        // fvBookingType.Visible = false;

        // getGridData("booking_type", 20);
        //setFormData(fvPermission, FormViewMode.Insert, null, "0", "0", "0");


    }

    protected void setVisible()
    {
        // divAction.Visible = !divAction.Visible;
        // gvBookingType.Visible = !gvBookingType.Visible;
        // fvBookingType.Visible = !fvBookingType.Visible;
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        // ViewState["listData"] = null;
    }

    protected void setError(string _errorText)
    {
        // divShowError.Visible = !divShowError.Visible;
        // litErrorCode.Text = _dataEmployee.return_code.ToString();
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    public Boolean checkCodeError(int _m0_sche_trn_day_idx, string _sche_trn_day_code)
    {
        Boolean error = false;
        data_elearning data_elearning = new data_elearning();
        data_elearning.el_m0_sche_trn_day_action = new m0_sche_trn_day[1];

        m0_sche_trn_day obj = new m0_sche_trn_day();

        obj.m0_sche_trn_day_idx = _m0_sche_trn_day_idx;
        // obj.sche_trn_day_code = _sche_trn_day_code;
        data_elearning.el_m0_sche_trn_day_action[0] = obj;
        data_elearning = _func_dmu.zCallServiceNetwork(_urlGetErrorel_m0_sche_trn_day, data_elearning);
        if (data_elearning.el_m0_sche_trn_day_action != null)
        {
            //obj = data_elearning.el_m0_sche_trn_day_action[0];
            //if (obj.sche_trn_day_code.ToString().Trim()== _sche_trn_day_code.Trim())
            //{

            //}
            error = true;

        }
        return error;
    }

    #endregion reuse

    private void sendEmail(int id)
    {
        if (id != 1)
        {
            return;
        }
        trainingLoolup obj_Loolup = new trainingLoolup();
        _data_elearning.trainingLoolup_action = new trainingLoolup[1];
        obj_Loolup.idx = id;
        obj_Loolup.operation_status_id = "SEND-E_MAIL";
        _data_elearning.trainingLoolup_action[0] = obj_Loolup;
        // litDebug.Text = _func_dmu.zJson(_urlsendEmail_Traning, _data_elearning);
        _func_dmu.zCallServiceNetwork(_urlsendEmail_OpenTraning, _data_elearning);
    }

}