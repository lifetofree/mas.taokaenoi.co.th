﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="Network-Layout.aspx.cs" Inherits="websystem_ITServices_Network_Layout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <style type="text/css">
        .GridHeader {
            height: 40px;
            color: white;
        }
    </style>

    <script type="text/javascript">
        function openModal() {
            $('#ordine').modal('show');

        }

    </script>


    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>


    <asp:Literal ID="text" runat="server"></asp:Literal>

    <!--*** START Menu ***-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav" runat="server">
                    <li id="liToIndexList" runat="server">
                        <asp:LinkButton ID="lbindex" runat="server"
                            CommandName="btnIndex"
                            OnCommand="btnCommand" Text="ข้อมูลทั่วไป" />
                    </li>

                    <li id="li1" runat="server">
                        <asp:LinkButton ID="lbreport" runat="server"
                            CommandName="btnReport"
                            OnCommand="btnCommand" Text="รายงาน" />
                    </li>


                </ul>
            </div>
        </nav>
    </div>
    <!--*** END Menu ***-->

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">


        <%-------- Index Start----------%>
        <asp:View ID="ViewIndex" runat="server">

            <%-------------- BoxSearch Start--------------%>

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="col-lg-12" runat="server" id="div_search">
                        <div id="BoxButtonSearchShow" runat="server">
                            <asp:LinkButton ID="BtnHideSETBoxAllSearchShow" CssClass="btn btn-sm btn-info" data-toggle="tooltip" title="Search" runat="server" CommandName="BtnHideSETBoxAllSearchShow" OnCommand="btnCommand"><i class="fa fa-search"></i> ShowSearch</asp:LinkButton>
                        </div>

                        <div id="BoxButtonSearchHide" runat="server">
                            <asp:LinkButton ID="BtnHideSETBoxAllSearchHide" CssClass="btn btn-sm btn-info" data-toggle="tooltip" title="Search" runat="server" CommandName="BtnHideSETBoxAllSearchHide" OnCommand="btnCommand"><i class="fa fa-search"></i> HideSearch</asp:LinkButton>
                        </div>
                        <br />
                        <div class="col-md-12" runat="server" id="BoxSearch">

                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ค้นหาข้อมูล</strong></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <%------------ Search Start----------------%>
                                            <div id="SETBoxAllSearch2" runat="server">

                                                <%-------------- ประเภทการค้นหา --------------%>
                                                <%--  <div class="form-group">
                                                    <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทการค้นหา : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddltypesearch" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Value="0">เลือกประเภทการค้นหา</asp:ListItem>
                                                            <asp:ListItem Value="1">ระบุตำแหน่งอุปกรณ์</asp:ListItem>
                                                            <asp:ListItem Value="2">ชนิดอุปกรณ์/ประเภทอุปกรณ์</asp:ListItem>
                                                        </asp:DropDownList>

                                                    </div>


                                                </div>--%>
                                                <%-------------- ประเภทการค้นหา --------------%>

                                                <%--  <asp:Panel ID="panel_position" runat="server" Visible="false">
                                                    <%-------------- สถานที่,อาคาร --------------
                                                    <div class="form-group">
                                                        <asp:Label ID="lbplacesearch" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ติดตั้ง : " />
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddlplace" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                <asp:ListItem Value="0" Text="กรุณาเลือกชื่อสถานที่ติดตั้งอุปกรณ์"></asp:ListItem>
                                                            </asp:DropDownList>

                                                        </div>

                                                        <asp:Label ID="lbroomsearch" CssClass="col-sm-2 control-label" runat="server" Text="อาคารที่ติดตั้ง : " />
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddlbuild" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                <asp:ListItem Text="กรุณาเลือกอาคาร ...." Value="0"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>

                                                    </div>
                                                    <%-------------- สถานที่,อาคาร --------------


                                                    <%-------------- ชั้น,ห้อง --------------
                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" CssClass="col-sm-2 control-label" runat="server" Text="ชั้นที่ติดตั้ง : " />
                                                        <div class="col-sm-3">
                                                            <asp:DropDownList ID="ddlfloor" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>
                                                        </div>

                                                        <%-- <asp:Label ID="Label3" CssClass="col-sm-2 control-label" runat="server" Text="ห้องที่ติดตั้ง : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlroom" AutoPostBack="true" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="กรุณาเลือกอาคาร ...." Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>--%>
                                            </div>
                                            <%-------------- สถานที่,อาคาร --------------
                                                </asp:Panel>--%>


                                            <asp:Panel ID="panel_type" runat="server">

                                                <%-------------- ชนิด,ประเภท --------------%>
                                                <div class="form-group">
                                                    <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="ชนิดอุปกรณ์ : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlcate" AutoPostBack="true" runat="server" CssClass="form-control">
                                                            <asp:ListItem Text="กรุณาเลือกชนิดอุปกรณ์ ...." Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                    <asp:Label ID="Label5" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddltype" AutoPostBack="true" runat="server" CssClass="form-control">
                                                            <asp:ListItem Text="กรุณาเลือกประเภทอุปกรณ์ ...." Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                </div>
                                                <%-------------- ชนิด,ประเภท --------------%>
                                            </asp:Panel>


                                            <div class="form-group">
                                                <div class="col-sm-5 col-sm-offset-1">
                                                    <asp:LinkButton ID="btnsearch" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch" ValidationGroup="btnsearch" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                                    <asp:LinkButton ID="btnRefresh" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Refresh" runat="server" CommandName="BtnBack" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="BtnHideSETBoxAllSearchShow" />
                    <asp:PostBackTrigger ControlID="BtnHideSETBoxAllSearchHide" />
                    <asp:PostBackTrigger ControlID="btnsearch" />
                    <asp:PostBackTrigger ControlID="btnRefresh" />

                    <asp:PostBackTrigger ControlID="ddlcate" />
                    <asp:PostBackTrigger ControlID="ddltype" />


                </Triggers>
            </asp:UpdatePanel>

            <asp:Panel ID="panel_chart" runat="server">
                <div class="form-group">
                    <div class="col-lg-6">
                        <div class="panel panel-warning" id="grant" runat="server">
                            <div id="div_npw" class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>&nbsp; สถานที่ :
                                        <asp:LinkButton OnCommand="btnCommand" CommandArgument="2" CommandName="btnhide" runat="server" ID="LinkButton5">นพวงศ์</asp:LinkButton></strong></h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <asp:GridView ID="GvNPW"
                                        runat="server"
                                        AutoGenerateColumns="false"
                                        DataKeyNames="u0idx"
                                        CssClass="table table-striped table-bordered table-hover"
                                        HeaderStyle-CssClass="danger"
                                        AllowPaging="true"
                                        OnPageIndexChanging="Master_PageIndexChanging"
                                        OnRowDataBound="Master_RowDataBound">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center;">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>


                                        <Columns>

                                            <asp:TemplateField HeaderText="ชนิดอุปกรณ์" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lbu0idx" Visible="false" runat="server" Text='<%# Eval("u0idx") %>' />
                                                        <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("category_name") %>' /></small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ชื่ออุปกรณ์" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <small>
                                                        <%-- <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("no_regis") %>' />--%>
                                                        <asp:LinkButton OnCommand="btnCommand" CommandArgument='<%# Eval("u0idx")%>' CommandName="btndevice" runat="server" ID="lbtnpw_noregis" Text='<%# Eval("no_regis") %>'></asp:LinkButton>


                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="อาคาร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lblroom_name" runat="server" Text='<%# Eval("room_name") %>'></asp:Label>
                                                    </small>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ชั้น" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lblFloor_name" runat="server" Text='<%# Eval("Floor_name") %>'></asp:Label>
                                                    </small>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="วัน/เดือน/ปี" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lblDateDown" runat="server" Text='<%# Eval("DateDown") %>'></asp:Label>
                                                    </small>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="IP Address" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <small>

                                                        <asp:LinkButton OnCommand="btnCommand" CommandArgument='<%# Eval("u0idx")%>' CommandName="btndevice" runat="server" ID="lbtnpw_ip" Text='<%# Eval("ip_address") %>'></asp:LinkButton>
                                                    </small>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                <ItemTemplate>

                                                    <asp:LinkButton ID="Edit_Log" CssClass="btn btn-primary  btn-sm" runat="server" data-toggle="tooltip" OnCommand="btnCommand" CommandArgument='<%# Eval("u0idx") %>' CommandName="ViewLog" title="View Log"><i class="glyphicon glyphicon-eye-open"></i></asp:LinkButton>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>


                                        </Columns>

                                    </asp:GridView>


                                    <div class="col-lg-6">
                                        <asp:Literal ID="litReportChart" runat="server" />
                                    </div>
                                    <div class="col-lg-6">
                                        <asp:Literal ID="litReportChart1" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-warning" id="Div3" runat="server">
                            <div id="div_rjn" class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>&nbsp; สถานที่ :
                                        <asp:LinkButton OnCommand="btnCommand" CommandArgument="15" CommandName="btnhide" runat="server" ID="btnrjn">โรจนะ</asp:LinkButton></strong></h3>
                            </div>
                            <div class="panel-body">



                                <div class="form-horizontal" role="form">

                                    <asp:GridView ID="GvRJN"
                                        runat="server"
                                        AutoGenerateColumns="false"
                                        DataKeyNames="u0idx"
                                        CssClass="table table-striped table-bordered table-hover"
                                        HeaderStyle-CssClass="danger"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="true"
                                        OnPageIndexChanging="Master_PageIndexChanging">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center;">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>


                                        <Columns>

                                            <asp:TemplateField HeaderText="ชนิดอุปกรณ์" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("category_name") %>' /></small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ชื่ออุปกรณ์" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:LinkButton OnCommand="btnCommand" CommandArgument='<%# Eval("u0idx")%>' CommandName="btndevice" runat="server" ID="lbtrjn_regis" Text='<%# Eval("no_regis") %>'></asp:LinkButton>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="อาคาร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lblroom_name" runat="server" Text='<%# Eval("room_name") %>'></asp:Label>
                                                    </small>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ชั้น" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lblFloor_name" runat="server" Text='<%# Eval("Floor_name") %>'></asp:Label>
                                                    </small>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="วัน/เดือน/ปี" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lblDateDown" runat="server" Text='<%# Eval("DateDown") %>'></asp:Label>
                                                    </small>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="IP Address" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:LinkButton OnCommand="btnCommand" CommandArgument='<%# Eval("u0idx")%>' CommandName="btndevice" runat="server" ID="lbtrjn_ip" Text='<%# Eval("ip_address") %>'></asp:LinkButton>
                                                    </small>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                <ItemTemplate>

                                                    <asp:LinkButton ID="Edit_Log2" CssClass="btn btn-primary  btn-sm" runat="server" data-toggle="tooltip" OnCommand="btnCommand" CommandArgument='<%# Eval("u0idx") %>' CommandName="ViewLog" title="View Log"><i class="glyphicon glyphicon-eye-open"></i></asp:LinkButton>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>
                                        </Columns>

                                    </asp:GridView>


                                    <div class="col-lg-6">
                                        <asp:Literal ID="litReportChart4" runat="server" />
                                    </div>
                                    <div class="col-lg-6">
                                        <asp:Literal ID="litReportChart5" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

                    <div class="col-lg-6">
                        <div class="panel panel-warning" id="Div2" runat="server">
                            <div id="div_idc" class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>&nbsp; สถานที่ :
                                        <asp:LinkButton OnCommand="btnCommand" CommandArgument="18" CommandName="btnhide" runat="server" ID="LinkButton4">Data Center</asp:LinkButton></strong></h3>
                            </div>
                            <div class="panel-body">



                                <div class="form-horizontal" role="form">

                                    <asp:GridView ID="GVIDC"
                                        runat="server"
                                        AutoGenerateColumns="false"
                                        DataKeyNames="u0idx"
                                        CssClass="table table-striped table-bordered table-hover"
                                        HeaderStyle-CssClass="danger"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="true"
                                        OnPageIndexChanging="Master_PageIndexChanging">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center;">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>


                                        <Columns>
                                            <%--    <asp:TemplateField HeaderText="#" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                                    </small>
                                                </ItemTemplate>

                                            </asp:TemplateField>--%>

                                            <asp:TemplateField HeaderText="ชนิดอุปกรณ์" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("category_name") %>' /></small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ชื่ออุปกรณ์" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:LinkButton OnCommand="btnCommand" CommandArgument='<%# Eval("u0idx")%>' CommandName="btndevice" runat="server" ID="lbtidc_regis" Text='<%# Eval("no_regis") %>'></asp:LinkButton>
                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="อาคาร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lblroom_name" runat="server" Text='<%# Eval("room_name") %>'></asp:Label>
                                                    </small>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ชั้น" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lblFloor_name" runat="server" Text='<%# Eval("Floor_name") %>'></asp:Label>
                                                    </small>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="วัน/เดือน/ปี" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lblstaname" runat="server" Text='<%# Eval("DateDown") %>'></asp:Label>
                                                    </small>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="IP Address" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:LinkButton OnCommand="btnCommand" CommandArgument='<%# Eval("u0idx")%>' CommandName="btndevice" runat="server" ID="lbtidc_ip" Text='<%# Eval("ip_address") %>'></asp:LinkButton>
                                                    </small>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                <ItemTemplate>

                                                    <asp:LinkButton ID="Edit_Log1" CssClass="btn btn-primary  btn-sm" runat="server" data-toggle="tooltip" OnCommand="btnCommand" CommandArgument='<%# Eval("u0idx") %>' CommandName="ViewLog" title="View Log"><i class="glyphicon glyphicon-eye-open"></i></asp:LinkButton>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                        </Columns>

                                    </asp:GridView>


                                    <div class="col-lg-6">
                                        <asp:Literal ID="litReportChart2" runat="server" />
                                    </div>
                                    <div class="col-lg-6">
                                        <asp:Literal ID="litReportChart3" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-warning" id="Div4" runat="server">
                            <div id="div_mtt" class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i><strong>&nbsp; สถานที่ :
                                        <asp:LinkButton OnCommand="btnCommand" CommandArgument="1" CommandName="btnhide" runat="server" ID="LinkButton3">เมืองทอง</asp:LinkButton></strong></h3>
                            </div>
                            <div class="panel-body">



                                <div class="form-horizontal" role="form">

                                    <asp:GridView ID="GvMTT"
                                        runat="server"
                                        AutoGenerateColumns="false"
                                        DataKeyNames="u0idx"
                                        CssClass="table table-striped table-bordered table-hover"
                                        HeaderStyle-CssClass="danger"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="true"
                                        OnPageIndexChanging="Master_PageIndexChanging">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center;">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>


                                        <Columns>

                                            <asp:TemplateField HeaderText="ชนิดอุปกรณ์" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Literal ID="ltSys1" runat="server" Text='<%# Eval("category_name") %>' /></small>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ชื่ออุปกรณ์" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:LinkButton OnCommand="btnCommand" CommandArgument='<%# Eval("u0idx")%>' CommandName="btndevice" runat="server" ID="lbtmtt_regis" Text='<%# Eval("no_regis") %>'></asp:LinkButton>

                                                    </small>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="อาคาร" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lblroom_name" runat="server" Text='<%# Eval("room_name") %>'></asp:Label>
                                                    </small>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ชั้น" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lblFloor_name" runat="server" Text='<%# Eval("Floor_name") %>'></asp:Label>
                                                    </small>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="วัน/เดือน/ปี" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:Label ID="lblDateDown" runat="server" Text='<%# Eval("DateDown") %>'></asp:Label>
                                                    </small>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="IP Address" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                                <ItemTemplate>
                                                    <small>
                                                        <asp:LinkButton OnCommand="btnCommand" CommandArgument='<%# Eval("u0idx")%>' CommandName="btndevice" runat="server" ID="lbtmtt_ip" Text='<%# Eval("ip_address") %>'></asp:LinkButton>
                                                    </small>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>



                                            <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                <ItemTemplate>

                                                    <asp:LinkButton ID="Edit_Log3" CssClass="btn btn-primary  btn-sm" runat="server" data-toggle="tooltip" OnCommand="btnCommand" CommandArgument='<%# Eval("u0idx") %>' CommandName="ViewLog" title="View Log"><i class="glyphicon glyphicon-eye-open"></i></asp:LinkButton>
                                                </ItemTemplate>

                                                <EditItemTemplate />
                                                <FooterTemplate />
                                            </asp:TemplateField>
                                        </Columns>

                                    </asp:GridView>


                                    <div class="col-lg-6">
                                        <asp:Literal ID="litReportChart6" runat="server" />
                                    </div>
                                    <div class="col-lg-6">
                                        <asp:Literal ID="litReportChart7" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="col-lg-12" runat="server" id="Div_Log">
                    <div id="ordine" class="modal open" role="dialog">
                        <div class="modal-dialog" style="width: 70%;">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title">ประวัติผู้ถือครอง</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <asp:Label ID="lblUIDX" runat="server" Visible="false" Text='<%# Eval("u0idx") %>' />
                                            <asp:Repeater ID="rptLog" runat="server">
                                                <HeaderTemplate>
                                                    <div class="form-group">
                                                        <label class="col-sm-4"><small>วันที่อุปกรณ์ Down</small></label>
                                                        <label class="col-sm-4"><small>เวลาที่อุปกรณ์ Down</small></label>
                                                        <label class="col-sm-4"><small>สาเหตุที่อุปกรณ์ Down</small></label>
                                                    </div>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <div class="row">
                                                        <div class="col-sm-4">
                                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small><%#Eval("DateDown")%></small></span>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small>(<%# Eval("TimeDown") %>)</small></span>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small>(<%# Eval("down_desciption") %>)</small></span>
                                                        </div>

                                                    </div>
                                                </ItemTemplate>
                                            </asp:Repeater>

                                            <br />

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-10">
                                                    <%--<asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" OnCommand="btnCommand" CommandName="btnCancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                               <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>--%>
                                                    <asp:LinkButton ID="Cancel" class="btn btn-default btn-sm" data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal" CommandName="Cancel">&nbsp;Close</asp:LinkButton>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </asp:View>

        <asp:View ID="ViewPosition" runat="server">


            <div class="col-lg-12" runat="server">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; เลือกชั้น</strong></h3>
                    </div>

                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <asp:Label ID="Label6" CssClass="col-sm-2 control-label" runat="server" Text="กรุณาเลือกอาคาร : " />
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlbuildsearch" AutoPostBack="true"  runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                          <asp:ListItem Value="0">กรุณาเลือกอาคารตั้งอุปกรณ์</asp:ListItem>
                                      </asp:DropDownList>

                                </div>
                                <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="กรุณาเลือกชั้น : " />
                                <div class="col-sm-3">
                                    <asp:DropDownList ID="ddlfloorsearch" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                        <asp:ListItem Value="0">กรุณาเลือกชั้นตั้งอุปกรณ์</asp:ListItem>
                                    </asp:DropDownList>

                                </div>


                                <div class="col-sm-1">

                                    <asp:LinkButton ID="btnback_position" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Back" runat="server" CommandName="BtnBack" OnCommand="btnCommand"><i class="glyphicon glyphicon-arrow-left"></i></asp:LinkButton>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-lg-12" runat="server" id="div_plan" visible="false">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; แผนผังภาพข้อมูล</strong></h3>
                    </div>



                    <div id="div_gridview" runat="server" style="z-index: 1; position: absolute; width: 100%;" visible="true">
                        <asp:GridView ID="GvMaster" runat="server" ShowHeader="true"
                            AutoGenerateColumns="true"
                            CssClass="col-lg-12"
                            GridLines="None"
                            OnRowDataBound="Master_RowDataBound">

                            <PagerStyle CssClass="pageCustom" />

                            <EmptyDataTemplate>
                            </EmptyDataTemplate>
                            <HeaderStyle CssClass="GridHeader" />
                        </asp:GridView>
                    </div>

                    <div id="div_image" runat="server" style="z-index: unset; width: 100%;">
                        <asp:Image ID="images" runat="server" Width="100%" />
                    </div>


                </div>
            </div>

        </asp:View>

        <asp:View ID="ViewDevices" runat="server">

            <div class="col-lg-12" runat="server" id="div_only" visible="true">
                <div class="form-horizontal" role="form">

                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; รายละเอียดอุปกรณ์</strong></h3>
                        </div>
                        <div class="panel-body">

                            <div id="gridviewindex" runat="server">
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <asp:GridView ID="GvViewDevices"
                                            runat="server"
                                            AutoGenerateColumns="false"
                                            DataKeyNames="REIDX"
                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                            HeaderStyle-CssClass="info"
                                            HeaderStyle-Height="40px"
                                            OnRowDataBound="Master_RowDataBound"
                                            AllowPaging="true"
                                            PageSize="10"
                                            OnPageIndexChanging="Master_PageIndexChanging">

                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                            <EmptyDataTemplate>
                                                <div style="text-align: center;">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>

                                            <Columns>

                                                <asp:TemplateField HeaderText="รูปภาพ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Image ID="image" runat="server" Width="100%"></asp:Image>

                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="รหัสทะเบียนอุปกรณ์" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lblregister_number" runat="server" Text='<%# Eval("register_number") %>' /></small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="ประเภทอุปกรณ์" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <small>

                                                            <asp:Label ID="Literal5" runat="server" Text='<%# Eval("type_name") %>' /></small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="ชนิดอุปกรณ์" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="ltSystemName" runat="server" Text='<%# Eval("category_name") %>' /></small>


                                                    </ItemTemplate>

                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="IP Address" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="Literal4" runat="server" Text='<%# Eval("ip_address") %>' />

                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="ชื่ออุปกรณ์ " HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-Wrap="True">

                                                    <ItemTemplate>
                                                        <small>

                                                            <asp:Label ID="DetailProblem" runat="server" Style="break-word;" Text='<%# Eval("no_regis") %>' /></p>

                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="ยี่ห้อ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <small>

                                                            <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("brand_name") %>' />
                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="รุ่น" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <small>

                                                            <asp:Label ID="lbStatusComment" runat="server" Text='<%#Eval("generation_name") %>'></asp:Label>


                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="Serial Number" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lblserial_number" runat="server" Text='<%# Eval("serial_number")%>'></asp:Label>


                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="สถานะอุปกรณ์ " HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Statusactive" Visible="false" runat="server" Text='<%# Eval("status_active") %>' />
                                                        <asp:Label ID="lbstate_ok" Visible="false" ToolTip="ทำงานปกติ" runat="server" Text=""> <i class="glyphicon glyphicon-ok-circle"></i></asp:Label>
                                                        <asp:Label ID="lbstate_where" ToolTip="ไม่พบ IP Address" Visible="false" runat="server" Text=""> <i class="glyphicon glyphicon-off"></i></asp:Label>
                                                        <asp:Label ID="lbstate_no" Visible="false" ToolTip="ทำงานผิดพลาด" runat="server" Text=""> <i class="glyphicon glyphicon-exclamation-sign"></i></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                            </Columns>

                                        </asp:GridView>

                                        <div class="form-group">
                                            <div class="col-sm-1 col-sm-offset-11">

                                                <asp:LinkButton ID="btnback_viewdevice" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Back" runat="server" CommandName="BtnBack" OnCommand="btnCommand"><i class="glyphicon glyphicon-arrow-left"></i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

        </asp:View>

        <asp:View ID="ViewFVDevice" runat="server">
            <div class="col-lg-12" runat="server" id="div5">
                <div class="form-horizontal" role="form">

                    <div class="panel panel-warning">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; ข้อมูลรายละเอียดอุปกรณ์</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="col-lg-2">

                                <asp:Image ID="img_devices" Width="100%" runat="server" />
                            </div>

                            <div class="col-lg-10">
                                <asp:FormView ID="FvDetailDevices" DefaultMode="ReadOnly" runat="server" Width="100%">
                                    <ItemTemplate>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <asp:Label ID="Label2" runat="server" Visible="false" Text='<%# Eval("u0idx") %>' />
                                                <asp:Label ID="Label15" CssClass="col-md-3 control-labelnotop text_right" runat="server" Text="เลขทะเบียนอุปกรณ์ :" />
                                                <asp:Label ID="lblu0idx" CssClass="control-labelnotop  col-md-3" runat="server" Text='<%# Eval("register_number") %>' />
                                                <asp:Label ID="Label9" CssClass="col-md-3 control-labelnotop text_right" runat="server" Text="ประเภทอุปกรณ์ :" />
                                                <asp:Label ID="lblno_regis" CssClass=" control-labelnotop col-xs-3 " runat="server" Text='<%# Eval("type_name") %>' />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label14" CssClass="col-md-3 control-labelnotop text_right" runat="server" Text="ชนิดอุปกรณ์ :" />
                                                <asp:Label ID="lblcate" CssClass="col-md-3 control-labelnotop" runat="server" Text='<%# Eval("category_name") %>' />
                                                <asp:Label ID="Label77" CssClass="col-md-3 control-labelnotop text_right" runat="server" Text="IP Address :" />
                                                <asp:Label ID="lbltype" CssClass="col-md-3  control-labelnotop" runat="server" Text='<%# Eval("ip_address") %>' />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label13" CssClass="col-md-3 control-labelnotop text_right" runat="server" Text="เลขอุปกรณ์ :" />
                                                <asp:Label ID="Label16" CssClass="col-md-3 control-labelnotop" runat="server" Text='<%# Eval("no_regis") %>' />
                                                <asp:Label ID="Label17" CssClass="col-md-3 control-labelnotop text_right" runat="server" Text="Serial Number :" />
                                                <asp:Label ID="Label18" CssClass="col-md-3 control-labelnotop" runat="server" Text='<%# Eval("serial_number") %>' />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label19" CssClass="col-md-3 control-labelnotop text_right" runat="server" Text="ยี่ห้อ :" />
                                                <asp:Label ID="Label20" CssClass="col-md-3 control-labelnotop" runat="server" Text='<%# Eval("brand_name") %>' />
                                                <asp:Label ID="Label21" CssClass="col-md-3 control-labelnotop text_right" runat="server" Text="รุ่น :" />
                                                <asp:Label ID="Label22" CssClass="col-md-3  control-labelnotop" runat="server" Text='<%# Eval("generation_name") %>' />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label23" CssClass="col-md-3 control-labelnotop text_right" runat="server" Text="สถานที่ :" />
                                                <asp:Label ID="Label24" CssClass="col-md-3 control-labelnotop" runat="server" Text='<%# Eval("place_name") %>' />
                                                <asp:Label ID="Label25" CssClass="col-md-3 control-labelnotop text_right" runat="server" Text="อาคาร :" />
                                                <asp:Label ID="Label26" CssClass="col-md-3  control-labelnotop" runat="server" Text='<%# Eval("room_name") %>' />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <asp:Label ID="Label27" CssClass="col-md-3 control-labelnotop text_right" runat="server" Text="ชั้น :" />
                                                <asp:Label ID="Label28" CssClass="col-md-3 control-labelnotop" runat="server" Text='<%# Eval("Floor_name") %>' />
                                                <asp:Label ID="Label29" CssClass="col-md-3 control-labelnotop text_right" runat="server" Text="สถานะทำงาน :" />
                                                <asp:Label ID="lbl_status" Visible="false" runat="server" Text='<%# Eval("status_active") %>' />
                                                <asp:Label ID="lbstate_ok" ToolTip="ทำงานปกติ" Visible="false" CssClass="col-md-3 control-labelnotop" runat="server" Text=""> <i class="glyphicon glyphicon-ok-circle"></i></asp:Label>
                                                <asp:Label ID="lbstate_where" ToolTip="ไม่พบ IP Address" Visible="false" CssClass="col-md-3 control-labelnotop" runat="server" Text=""> <i class="glyphicon glyphicon-off"></i></asp:Label>
                                                <asp:Label ID="lbstate_no" ToolTip="ทำงานผิดพลาด" Visible="false" CssClass="col-md-3 control-labelnotop" runat="server" Text=""> <i class="glyphicon glyphicon-exclamation-sign"></i></asp:Label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-1 col-sm-offset-11">

                                                <asp:LinkButton ID="LinkButton6" CssClass="btn btn-info btn-sm" data-toggle="tooltip" title="Back" runat="server" CommandName="BtnBack" OnCommand="btnCommand"><i class="glyphicon glyphicon-arrow-left"></i></asp:LinkButton>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:FormView>

                            </div>
                        </div>
                    </div>


                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-exclamation-sign"></i><strong>&nbsp; ประวัติการทำงานผิดพลาดอุปกรณ์</strong></h3>
                        </div>
                        <div class="panel-body">

                            <asp:Repeater ID="Repeater_Log" runat="server">
                                <HeaderTemplate>
                                    <div class="form-group">
                                        <label class="col-sm-4"><small>วันที่อุปกรณ์ Down</small></label>
                                        <label class="col-sm-4"><small>เวลาที่อุปกรณ์ Down</small></label>
                                        <label class="col-sm-4"><small>สาเหตุที่อุปกรณ์ Down</small></label>
                                    </div>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small><%#Eval("DateDown")%></small></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small>(<%# Eval("TimeDown") %>)</small></span>
                                        </div>
                                        <div class="col-sm-4">
                                            <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small>(<%# Eval("down_desciption") %>)</small></span>
                                        </div>

                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>





                        </div>
                    </div>

                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewReport" runat="server">
            <div class="col-md-12" runat="server">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; สรุปรายงาน</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <%------------ Search Start----------------%>

                                    <%-------------- ประเภทการค้นหา --------------%>
                                    <div class="form-group">
                                        <asp:Label ID="Label3" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทการค้นหา : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddltypereport" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">เลือกประเภทรายงาน</asp:ListItem>
                                                <asp:ListItem Value="1">รายงานรูปแบบตาราง</asp:ListItem>
                                                <asp:ListItem Value="2">รายงานรูปแบบกราฟ</asp:ListItem>

                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="SearchReport" runat="server" Display="None"
                                                ControlToValidate="ddltypereport" Font-Size="11"
                                                ErrorMessage="กรุณาเลือกประเภทการค้นหา"
                                                ValidationExpression="กรุณาเลือกประเภทการค้นหา" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />

                                        </div>

                                        <div id="div_table" runat="server" visible="false">
                                            <asp:Label ID="Label8" CssClass="col-sm-2 control-label" runat="server" Text="เลือกรายการค้นหา : " />

                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlsearch" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    <asp:ListItem Value="0">เลือกรายการค้นหา....</asp:ListItem>
                                                    <asp:ListItem Value="1">สถานที่</asp:ListItem>
                                                    <asp:ListItem Value="2">ชนิดอุปกรณ์/ข้อมูลอุปกรณ์</asp:ListItem>

                                                </asp:DropDownList>

                                            </div>
                                        </div>


                                        <div id="div_report" runat="server" visible="false">
                                            <asp:Label ID="Label30" CssClass="col-sm-2 control-label" runat="server" Text="เลือกรายการค้นหา : " />

                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddl_chart" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Value="0">เลือกรายการค้นหา....</asp:ListItem>
                                                    <asp:ListItem Value="1">ชื่ออุปกรณ์ / จำนวนครั้ง</asp:ListItem>
                                                    <asp:ListItem Value="2">IP Address / จำนวนครั้ง</asp:ListItem>
                                                    <asp:ListItem Value="3">เลขทะเบียนอุปกรณ์ / จำนวนครั้ง</asp:ListItem>
                                                    <asp:ListItem Value="4">ชนิดอุปกรณ์ / จำนวนครั้ง</asp:ListItem>
                                                    <asp:ListItem Value="5">สถานที่ / จำนวนครั้ง</asp:ListItem>

                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SearchReport" runat="server" Display="None"
                                                    ControlToValidate="ddl_chart" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกรายการการค้นหา"
                                                    ValidationExpression="กรุณาเลือกรายการการค้นหา" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group" id="panel_searchdate" runat="server">
                                        <asp:Label ID="Label9" runat="server" Text="ระบุวันค้นหา :" CssClass="col-sm-2 control-label"></asp:Label>
                                        <div class="col-sm-2">
                                            <asp:DropDownList ID="ddlSearchDate" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                <asp:ListItem Value="0">เลือกเงื่อนไข</asp:ListItem>
                                                <asp:ListItem Value="1">มากกว่า  ></asp:ListItem>
                                                <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                                <asp:ListItem Value="3">ระหว่าง  <></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3 ">

                                            <div class='input-group date'>
                                                <asp:TextBox ID="AddStartdate" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SearchReport" runat="server" Display="None"
                                                    ControlToValidate="AddStartdate" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                    ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class='input-group date'>
                                                <asp:TextBox ID="AddEndDate" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            </div>
                                        </div>
                                        <p class="help-block"><font color="red">**</font></p>
                                    </div>

                                    <asp:Panel ID="panel_only" runat="server" Visible="false">
                                        <%-------------- ชนิด,ประเภท --------------%>
                                        <div class="form-group">
                                            <div id="div_ddlre_cate" runat="server">
                                                <asp:Label ID="lblcate" CssClass="col-sm-2 control-label" runat="server" Text="ชนิดอุปกรณ์ : " />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlre_cate" AutoPostBack="true" runat="server" CssClass="form-control">
                                                        <asp:ListItem Text="กรุณาเลือกชนิดอุปกรณ์ ...." Value="0"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div id="div_txtdevices" runat="server">

                                                <asp:Label ID="lbldevice" CssClass="col-sm-2 control-label" runat="server" Text="ข้อมูลอุปกรณ์ : " />
                                                <div class="col-sm-4">
                                                    <asp:TextBox ID="txtdevices" PlaceHolder="กรอก IP Address / ชื่ออุปกรณ์ / เลขทะเบียน ...." runat="server" CssClass="form-control" />
                                                </div>
                                            </div>
                                        </div>

                                    </asp:Panel>

                                    <asp:Panel ID="panel_searchposition" runat="server" Visible="false">
                                        <%-------------- สถานที่,อาคาร --------------%>
                                        <div class="form-group">
                                            <asp:Label ID="Label10" CssClass="col-sm-2 control-label" runat="server" Text="สถานที่ติดตั้ง : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlre_place" AutoPostBack="true" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="กรุณาเลือกชื่อสถานที่ติดตั้งอุปกรณ์"></asp:ListItem>
                                                </asp:DropDownList>

                                            </div>

                                            <asp:Label ID="Label11" CssClass="col-sm-2 control-label" runat="server" Text="อาคารที่ติดตั้ง : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlre_build" AutoPostBack="true" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="กรุณาเลือกอาคาร ...." Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                        </div>
                                        <%-------------- สถานที่,อาคาร --------------%>


                                        <%-------------- ชั้น,ห้อง --------------%>
                                        <div class="form-group">
                                            <asp:Label ID="Label12" CssClass="col-sm-2 control-label" runat="server" Text="ชั้นที่ติดตั้ง : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddlre_floor" AutoPostBack="true" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>


                                        </div>
                                    </asp:Panel>

                                    <div class="form-group">
                                        <div class="col-sm-5 col-sm-offset-1">
                                            <asp:LinkButton ID="btnsearch_report" CssClass="btn btn-primary btn-sm" data-toggle="tooltip" title="Search" runat="server" CommandName="btnsearch_report" ValidationGroup="SearchReport" OnCommand="btnCommand"><i class="fa fa-search"></i> </asp:LinkButton>
                                            <asp:LinkButton ID="btnexport" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Export Excel" runat="server" CommandName="btnexport" ValidationGroup="SearchReport" OnCommand="btnCommand"><i class="glyphicon glyphicon-export"></i> </asp:LinkButton>
                                            <asp:LinkButton ID="LinkButton2" CssClass="btn btn-warning btn-sm" data-toggle="tooltip" title="Refresh" runat="server" CommandName="BtnRefresh" OnCommand="btnCommand"><i class="fa fa-refresh"></i></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-warning" id="div_datagrid" runat="server" visible="false">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ข้อมูลรายงาน</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div id="div_gridreport" runat="server" visible="false">
                                        <asp:GridView ID="GvReport"
                                            runat="server"
                                            AutoGenerateColumns="false"
                                            DataKeyNames="u0idx"
                                            CssClass="table table-striped table-bordered table-hover"
                                            HeaderStyle-CssClass="danger"
                                            HeaderStyle-Height="40px"
                                            OnRowDataBound="Master_RowDataBound"
                                            AllowPaging="true"
                                            PageSize="10"
                                            OnPageIndexChanging="Master_PageIndexChanging">

                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                            <EmptyDataTemplate>
                                                <div style="text-align: center;">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>

                                            <Columns>



                                                <asp:TemplateField HeaderText="รหัสทะเบียนอุปกรณ์" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lblregister_number" runat="server" Text='<%# Eval("register_number") %>' /></small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="สถานที่" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <small>

                                                            <asp:Label ID="Literal5" runat="server" Text='<%# Eval("place_name") %>' /></small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="ชนิดอุปกรณ์" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="ltSystemName" runat="server" Text='<%# Eval("category_name") %>' /></small>


                                                    </ItemTemplate>

                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="วันที่ Down" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <small>

                                                            <asp:Literal ID="Literal7" runat="server" Text='<%# Eval("DateDown") %>' />
                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="เวลาที่ Down" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <small>

                                                            <asp:Label ID="lbStatusComment" runat="server" Text='<%#Eval("TimeDown") %>'></asp:Label>


                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="IP Address" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="Literal4" runat="server" Text='<%# Eval("ip_address") %>' />

                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="ชื่ออุปกรณ์ " HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-Wrap="True">

                                                    <ItemTemplate>
                                                        <small>

                                                            <asp:Label ID="DetailProblem" runat="server" Style="break-word;" Text='<%# Eval("no_regis") %>' /></p>

                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>





                                                <asp:TemplateField HeaderText="Serial Number" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                                                    <ItemTemplate>
                                                        <small>
                                                            <asp:Label ID="lblserial_number" runat="server" Text='<%# Eval("serial_number")%>'></asp:Label>


                                                        </small>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%--  <asp:TemplateField HeaderText="สถานะอุปกรณ์ " HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" ItemStyle-CssClass="text-center">
                            <ItemTemplate>
                                <asp:Label ID="Statusactive" Visible="false" runat="server" Text='<%# Eval("status_active") %>' />
                                <asp:Label ID="lbstate_ok" Visible="false" ToolTip="ทำงานปกติ" runat="server" Text=""> <i class="glyphicon glyphicon-ok-circle"></i></asp:Label>
                                <asp:Label ID="lbstate_where" ToolTip="ไม่พบ IP Address" Visible="false" runat="server" Text=""> <i class="glyphicon glyphicon-off"></i></asp:Label>
                                <asp:Label ID="lbstate_no" Visible="false" ToolTip="ทำงานผิดพลาด" runat="server" Text=""> <i class="glyphicon glyphicon-exclamation-sign"></i></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                                            </Columns>

                                        </asp:GridView>
                                    </div>
                                    <div id="div_chartreport" runat="server" visible="false">
                                        <asp:Literal ID="litReportChart8" runat="server" />
                                    </div>

                                </div>
                            </div>
                        </div>

                        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="true">
                            <Columns>
                                <asp:TemplateField HeaderText="ลำดับที่" ItemStyle-Width="100">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                    </ContentTemplate>

                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnsearch_report" />
                        <asp:PostBackTrigger ControlID="btnexport" />
                    </Triggers>
                </asp:UpdatePanel>

            </div>

        </asp:View>
    </asp:MultiView>


</asp:Content>

