﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="law_trademark_permise.aspx.cs" Inherits="websystem_laws_law_trademark_permise" %>



<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">

                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbCreate" runat="server" CommandName="cmdCreate" OnCommand="navCommand" CommandArgument="docCreate"> สร้างรายการ</asp:LinkButton>
                        </li>

                        <li id="li1" runat="server">
                            <asp:LinkButton ID="lbDetail" runat="server" CommandName="cmdDetail" OnCommand="navCommand" CommandArgument="docDetail"> รายการทั่วไป</asp:LinkButton>
                        </li>

                        <li id="li2" runat="server">
                            <asp:LinkButton ID="lbApprove" runat="server" CommandName="cmdApprove" OnCommand="navCommand" CommandArgument="docApprove"> รายการรออนุมัติ</asp:LinkButton>
                        </li>

                        <li id="li3" runat="server">
                            <asp:LinkButton ID="lbPromise" runat="server" CommandName="cmdPromise" OnCommand="navCommand" CommandArgument="docPromise"> ไฟล์สัญญา/ หนังสือมอบอำนาจ</asp:LinkButton>
                        </li>

                    </ul>

                    <ul class="nav navbar-nav navbar-right" runat="server">

                        <li id="_divFlowLiToDocument" runat="server">

                            <asp:HyperLink ID="hplFlow" NavigateUrl="https://drive.google.com/file/d/1AV9pbnMrKzqRJm2lcRemXub3zn2SVKbf/view?usp=sharing" Target="_blank" runat="server" CommandName="cmdFlow" OnCommand="btnCommand"><i class="fas fa-book"></i> Flow การทำงาน</asp:HyperLink>


                        </li>

                        <li id="_divFlowLiToDocument1" runat="server">
                            <asp:HyperLink ID="HyperLink1" NavigateUrl="https://docs.google.com/document/d/1YEr66py8hryQ05nR-rXKwm-PDHFkqwlIaLiu1peTCl4/edit?usp=sharing" Target="_blank" runat="server" CommandName="cmdBookSystem" OnCommand="btnCommand"><i class="fas fa-book"></i> คู่มือการใช้งานระบบ</asp:HyperLink>
                        </li>

                    </ul>

                </div>
                <!--Collect the nav links, forms, and other content for toggling-->

            </div>
        </nav>
    </div>
    <!--tab menu-->

    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <asp:Literal ID="litDebug1" runat="server"></asp:Literal>
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">

        <!--View Create-->
        <asp:View ID="docCreate" runat="server">
            <div class="col-md-12">

                <div class="col-md-12" id="div_fileshow" runat="server" style="color: transparent;">
                    <asp:FileUpload ID="FileShowUpload" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|png|pdf" />
                </div>

                <!-- Form Detail Employee Create -->
                <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>

                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:FormView>
                <!-- Form Detail Employee Create -->

                <!-- Form Create -->
                <asp:FormView ID="fvCreate" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>

                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">สร้างใบคำขอ(สัญญา/ หนังสือมอบอำนาจ)</h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">ประเภทเอกสาร</label>
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="ddlTypeDocument" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-md-2 control-label">รูปแบบ</label>
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="ddlSubTypeDocument" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <%--<asp:TextBox ID="txt_subtype_document" runat="server" CssClass="form-control" placeholder="รูปแบบ ..." />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">เลขที่เอกสารอ้างอิง</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_docreference" runat="server" CssClass="form-control" placeholder="เลขที่เอกสารอ้างอิง ..." />
                                        </div>
                                        <label class="col-md-2 control-label">คู่สัญญา</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_contract_parties" runat="server" CssClass="form-control" placeholder="คู่สัญญา ..." />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">มูลค่า</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_value" runat="server" CssClass="form-control" placeholder="มูลค่า ..." />
                                        </div>
                                        <label class="col-md-2 control-label">องค์กร</label>
                                        <div class="col-md-4">
                                            <asp:DropDownList ID="ddlOrg" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <label class="col-md-2 control-label">หัวข้อ</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_topic" runat="server" CssClass="form-control" placeholder="หัวข้อ ..." />
                                        </div>

                                        <label class="col-md-2 control-label">รายละเอียด</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_detail" runat="server" CssClass="form-control" placeholder="รายละเอียด ..." />
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="col-md-2 control-label">วันที่เริ่มต้น</label>
                                        <div class="col-md-4">
                                            <div class="input-group date">

                                                <asp:TextBox ID="txt_date_start" runat="server" CssClass="form-control datetimepicker-from cursor-pointer" placeholder="วันที่เริ่มต้น ..." />
                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <label class="col-md-2 control-label">วันที่สิ้นสุด</label>
                                        <div class="col-md-4">

                                            <div class="input-group date">
                                                <asp:TextBox ID="txt_date_end" runat="server" CssClass="form-control datetimepicker-to cursor-pointer" placeholder="วันที่สิ้นสุด ..." />
                                                <span class="input-group-addon show-to-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="col-md-2 control-label">ผู้ติดต่อ</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_contract_person" runat="server" CssClass="form-control" placeholder="ผู้ติดต่อ ..." />
                                        </div>

                                        <label class="col-md-2 control-label">เบอร์โทร</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_phone_number" runat="server" MaxLength="10" CssClass="form-control" placeholder="เบอร์โทร ..." />
                                        </div>

                                    </div>


                                    <asp:UpdatePanel ID="UpdatePanel_FvCreate" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group">

                                                <label class="col-md-2 control-label">ไฟล์แนบ</label>
                                                <div class="col-md-4">
                                                    <asp:FileUpload ID="UploadFile" ViewStateMode="Enabled" AutoPostBack="true" CssClass="control-label UploadFile multi max-1 accept-pdf maxsize-1024 with-preview" runat="server" />

                                                    <small>
                                                        <p class="help-block"><font color="red">**เฉพาะไฟล์นามสกุล pdf เท่านั้น</font></p>
                                                    </small>

                                                </div>

                                                <label class="col-md-6 control-label"></label>

                                            </div>


                                            <div class="form-group pull-right">

                                                <div class="col-md-12">
                                                    <asp:LinkButton ID="btnSave" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึก" OnCommand="btnCommand" CommandName="cmdSave" ValidationGroup="SaveCreate"></asp:LinkButton>

                                                    <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancel"></asp:LinkButton>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSave" />
                                        </Triggers>
                                    </asp:UpdatePanel>

                                </div>
                            </div>

                        </div>

                    </InsertItemTemplate>

                    <ItemTemplate>
                        <asp:HiddenField ID="hfu0_document_idx" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("noidx") %>' />
                        <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("acidx") %>' />
                        <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("staidx") %>' />

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดรายการใบคำขอ(สัญญา/ หนังสือมอบอำนาจ)</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">รหัสเอกสาร</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_document_code" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("document_code")%>' />

                                        </div>
                                        <label class="col-md-6 control-label"></label>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">ประเภทเอกสาร</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_type_document_idx" runat="server" Visible="false" CssClass="form-control" Enabled="false" Text='<%# Eval("type_document_idx")%>' />
                                            <asp:DropDownList ID="ddlTypeDocument" runat="server" CssClass="form-control" Enabled="false" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-md-2 control-label">รูปแบบ</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_subtype_document_idx" runat="server" Visible="false" CssClass="form-control" Enabled="false" Text='<%# Eval("subtype_document_idx")%>' />
                                            <asp:DropDownList ID="ddlSubTypeDocument" runat="server" CssClass="form-control" Enabled="false" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <%--<asp:TextBox ID="txt_subtype_document" runat="server" CssClass="form-control" placeholder="รูปแบบ ..." />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">เลขที่เอกสารอ้างอิง</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_u0_doc_idx" runat="server" Visible="false" CssClass="form-control" Enabled="false" Text='<%# Eval("u0_doc_idx")%>' />
                                            <asp:TextBox ID="txt_docreference" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("doc_reference")%>' placeholder="เลขที่เอกสารอ้างอิง ..." />
                                        </div>
                                        <label class="col-md-2 control-label">คู่สัญญา</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_contract_parties" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("contract_parties")%>' placeholder="คู่สัญญา ..." />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">มูลค่า</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_value" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("value")%>' placeholder="มูลค่า ..." />
                                        </div>
                                        <label class="col-md-2 control-label">องค์กร</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_org_nameth" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("OrgNameTH")%>' placeholder="องค์กร ..." />
                                        </div>
                                    </div>

                                    <div class="form-group">

                                        <label class="col-md-2 control-label">หัวข้อ</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_topic" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("topic")%>' placeholder="หัวข้อ ..." />
                                        </div>

                                        <label class="col-md-2 control-label">รายละเอียด</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_detail" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("detail")%>' placeholder="รายละเอียด ..." />
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="col-md-2 control-label">วันที่เริ่มต้น</label>
                                        <div class="col-md-4">
                                            <div class="input-group date">

                                                <asp:TextBox ID="txt_date_start" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("date_start")%>' placeholder="วันที่เริ่มต้น ..." />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <label class="col-md-2 control-label">วันที่สิ้นสุด</label>
                                        <div class="col-md-4">

                                            <div class="input-group date">
                                                <asp:TextBox ID="txt_date_end" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("date_end")%>' placeholder="วันที่สิ้นสุด ..." />
                                                <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="col-md-2 control-label">ผู้ติดต่อ</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_contract_person" runat="server" Enabled="false" CssClass="form-control" Text='<%# Eval("contact_person")%>' placeholder="ผู้ติดต่อ ..." />
                                        </div>

                                        <label class="col-md-2 control-label">เบอร์โทร</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_phone_number" runat="server" MaxLength="10" Enabled="false" CssClass="form-control" Text='<%# Eval("phone_number")%>' placeholder="เบอร์โทร ..." />
                                        </div>

                                    </div>


                                    <asp:Panel ID="UpdatePanel_gvFileUserCreate" runat="server">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ไฟล์แนบ(ผู้สร้าง)</label>
                                            <div class="col-md-10">
                                                <asp:GridView ID="gvFileUserCreate"
                                                    runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                    HeaderStyle-CssClass="default"
                                                    AllowPaging="true"
                                                    OnRowDataBound="gvRowDataBound"
                                                    OnPageIndexChanging="gvPageIndexChanging"
                                                    PageSize="10">
                                                    <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">No result</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <%# (Container.DataItemIndex + 1) %>
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ไฟล์แนบสร้างรายการ(ผู้สร้าง)">
                                                            <ItemTemplate>
                                                                <div class="col-lg-10">
                                                                    <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                    <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                                </div>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>

                                    </asp:Panel>

                                    <asp:Panel ID="UpdatePanel_gvFileUserCheckPromise" runat="server">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ไฟล์แนบตรวจสอบสัญญา(ผู้สร้าง)</label>
                                            <div class="col-md-10">
                                                <asp:GridView ID="gvFileUserCheckPromise"
                                                    runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                    HeaderStyle-CssClass="default"
                                                    AllowPaging="true"
                                                    OnRowDataBound="gvRowDataBound"
                                                    OnPageIndexChanging="gvPageIndexChanging"
                                                    PageSize="10">
                                                    <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">No result</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>

                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ไฟล์แนบตรวจสอบสัญญา(ผู้สร้าง)">
                                                            <ItemTemplate>
                                                                <div class="col-lg-10">
                                                                    <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                    <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                                </div>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel ID="UpdatePanel_gvFileUserCheckScript" runat="server">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ไฟล์แนบตรวจสอบต้นฉบับ(ผู้สร้าง)</label>
                                            <div class="col-md-10">
                                                <asp:GridView ID="gvFileUserCheckScript"
                                                    runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                    HeaderStyle-CssClass="default"
                                                    AllowPaging="true"
                                                    OnRowDataBound="gvRowDataBound"
                                                    OnPageIndexChanging="gvPageIndexChanging"
                                                    PageSize="10">
                                                    <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">No result</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>

                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ไฟล์แนบตรวจสอบต้นฉบับ(ผู้สร้าง)">
                                                            <ItemTemplate>
                                                                <div class="col-lg-10">
                                                                    <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                    <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                                </div>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </asp:Panel>


                                    <asp:Panel ID="UpdatePanel_gvDetailLawOfficer" runat="server">
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">ผู้ที่รับผิดชอบ</label>
                                            <div class="col-sm-10">
                                                <asp:GridView ID="gvDetailLawOfficer"
                                                    runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                    HeaderStyle-CssClass="default"
                                                    AllowPaging="true"
                                                    OnRowDataBound="gvRowDataBound"
                                                    OnPageIndexChanging="gvPageIndexChanging"
                                                    PageSize="10">
                                                    <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">No result</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>

                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblu2_doc_idx_view" runat="server" Text='<%# Eval("u2_doc_idx") %>' Visible="false" />
                                                                <asp:Label ID="lbl_emp_code_view" runat="server" Text='<%# Eval("emp_code") %>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ชื่อ-สกุล" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>

                                                                <asp:Label ID="lbl_emp_name_th_view" runat="server" Text='<%# Eval("emp_name_th") %>' />

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <%--  <asp:TemplateField HeaderText="Position(EN)" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblrpos_idx_view" runat="server" Text='<%# Eval("rpos_idx") %>' Visible="false" />
                                                            <asp:Label ID="lbl_pos_name_en_view" runat="server" Text='<%# Eval("pos_name_en") %>' />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </asp:Panel>



                                    <asp:Panel ID="UpdatePanel_gvFileLawCreatePromise" runat="server">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ไฟล์แนบร่างสัญญา(กฏหมาย)</label>
                                            <div class="col-md-10">
                                                <asp:GridView ID="gvFileLawCreatePromise"
                                                    runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                    HeaderStyle-CssClass="default"
                                                    AllowPaging="true"
                                                    OnRowDataBound="gvRowDataBound"
                                                    OnPageIndexChanging="gvPageIndexChanging"
                                                    PageSize="10">
                                                    <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">No result</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>

                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ไฟล์แนบร่างสัญญา(กฏหมาย)">
                                                            <ItemTemplate>
                                                                <div class="col-lg-10">
                                                                    <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                    <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                                </div>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel ID="UpdatePanel_gvFileLawCreateScript" runat="server">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ไฟล์แนบตรวจสอบ/ ออกต้นฉบับ(กฏหมาย)</label>
                                            <div class="col-md-10">
                                                <asp:GridView ID="gvFileLawCreateScript"
                                                    runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                    HeaderStyle-CssClass="default"
                                                    AllowPaging="true"
                                                    OnRowDataBound="gvRowDataBound"
                                                    OnPageIndexChanging="gvPageIndexChanging"
                                                    PageSize="10">
                                                    <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">No result</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>

                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ไฟล์แนบตรวจสอบ/ ออกต้นฉบับ(กฏหมาย)">
                                                            <ItemTemplate>
                                                                <div class="col-lg-10">
                                                                    <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                    <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                                </div>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel ID="UpdatePanel_gvFileLawSavePromise" runat="server">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">ไฟล์แนบจัดทำสัญญา(กฏหมาย)</label>
                                            <div class="col-md-10">
                                                <asp:GridView ID="gvFileLawSavePromise"
                                                    runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                    HeaderStyle-CssClass="default"
                                                    AllowPaging="true"
                                                    OnRowDataBound="gvRowDataBound"
                                                    OnPageIndexChanging="gvPageIndexChanging"
                                                    PageSize="10">
                                                    <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">No result</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                            HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>

                                                                <%# (Container.DataItemIndex + 1) %>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ไฟล์แนบจัดทำสัญญา(กฏหมาย)">
                                                            <ItemTemplate>
                                                                <div class="col-lg-10">
                                                                    <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                    <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                                </div>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-default" runat="server" data-original-title="Back" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdCancel"><i class="fa fa-reply" aria-hidden="true"></i> Back</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </ItemTemplate>

                    <EditItemTemplate>
                        <asp:HiddenField ID="hfu0_document_idx" runat="server" Value='<%# Eval("u0_doc_idx") %>' />
                        <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("noidx") %>' />
                        <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("acidx") %>' />
                        <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("staidx") %>' />

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">แก้ไขรายละเอียดรายการใบคำขอ(สัญญา/ หนังสือมอบอำนาจ)</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">รหัสเอกสาร</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_document_code" runat="server" CssClass="form-control" Enabled="false" Text='<%# Eval("document_code")%>' />

                                        </div>
                                        <label class="col-md-6 control-label"></label>

                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">ประเภทเอกสาร</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_type_document_idx" runat="server" Visible="false" CssClass="form-control" Enabled="false" Text='<%# Eval("type_document_idx")%>' />
                                            <asp:DropDownList ID="ddlTypeDocument" runat="server" Enabled="false" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </div>
                                        <label class="col-md-2 control-label">รูปแบบ</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_subtype_document_idx" runat="server" Visible="false" CssClass="form-control" Enabled="false" Text='<%# Eval("subtype_document_idx")%>' />
                                            <asp:DropDownList ID="ddlSubTypeDocument" runat="server" Enabled="false" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>
                                            <%--<asp:TextBox ID="txt_subtype_document" runat="server" CssClass="form-control" placeholder="รูปแบบ ..." />--%>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">เลขที่เอกสารอ้างอิง</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_u0_doc_idx" runat="server" Visible="false" CssClass="form-control" Enabled="false" Text='<%# Eval("u0_doc_idx")%>' />
                                            <asp:TextBox ID="txt_docreference" runat="server" CssClass="form-control" Text='<%# Eval("doc_reference")%>' placeholder="เลขที่เอกสารอ้างอิง ..." />
                                        </div>
                                        <label class="col-md-2 control-label">คู่สัญญา</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_contract_parties" runat="server" CssClass="form-control" Text='<%# Eval("contract_parties")%>' placeholder="คู่สัญญา ..." />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">มูลค่า</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_value" runat="server" CssClass="form-control" Text='<%# Eval("value")%>' placeholder="มูลค่า ..." />
                                        </div>
                                        <label class="col-md-6 control-label"></label>
                                    </div>

                                    <div class="form-group">

                                        <label class="col-md-2 control-label">หัวข้อ</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_topic" runat="server" CssClass="form-control" Text='<%# Eval("topic")%>' placeholder="หัวข้อ ..." />
                                        </div>

                                        <label class="col-md-2 control-label">รายละเอียด</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_detail" runat="server" CssClass="form-control" Text='<%# Eval("detail")%>' placeholder="รายละเอียด ..." />
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="col-md-2 control-label">วันที่เริ่มต้น</label>
                                        <div class="col-md-4">
                                            <div class="input-group date">

                                                <asp:TextBox ID="txt_date_start" runat="server" CssClass="form-control datetimepicker-from cursor-pointer" Text='<%# Eval("date_start")%>' placeholder="วันที่เริ่มต้น ..." />
                                                <span class="input-group-addon show-from-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                            </div>
                                        </div>

                                        <label class="col-md-2 control-label">วันที่สิ้นสุด</label>
                                        <div class="col-md-4">

                                            <div class="input-group date">
                                                <asp:TextBox ID="txt_date_end" runat="server" CssClass="form-control datetimepicker-to cursor-pointer" Text='<%# Eval("date_end")%>' placeholder="วันที่สิ้นสุด ..." />
                                                <span class="input-group-addon show-to-onclick">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="col-md-2 control-label">ผู้ติดต่อ</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_contract_person" runat="server" CssClass="form-control" Text='<%# Eval("contact_person")%>' placeholder="ผู้ติดต่อ ..." />
                                        </div>

                                        <label class="col-md-2 control-label">เบอร์โทร</label>
                                        <div class="col-md-4">
                                            <asp:TextBox ID="txt_phone_number" runat="server" MaxLength="10" CssClass="form-control" Text='<%# Eval("phone_number")%>' placeholder="เบอร์โทร ..." />
                                        </div>

                                    </div>

                                    <asp:UpdatePanel ID="UpdatePanel_FvCreateEdit" runat="server">
                                        <ContentTemplate>

                                            <div class="form-group">

                                                <label class="col-md-2 control-label">ไฟล์แนบ</label>
                                                <div class="col-md-4">
                                                    <asp:FileUpload ID="UploadFileEdit" ViewStateMode="Enabled" AutoPostBack="true" CssClass="control-label UploadFileEdit multi max-1 accept-pdf maxsize-1024 with-preview" runat="server" />
                                                </div>

                                                <label class="col-md-6 control-label"></label>

                                            </div>


                                            <asp:Panel ID="Pn_gvFileUserCreate" runat="server">
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">ไฟล์แนบ(ผู้สร้าง)</label>
                                                    <div class="col-md-10">
                                                        <asp:GridView ID="gvFileUserCreate"
                                                            runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                            HeaderStyle-CssClass="default"
                                                            AllowPaging="true"
                                                            OnRowDataBound="gvRowDataBound"
                                                            OnPageIndexChanging="gvPageIndexChanging"
                                                            PageSize="10">
                                                            <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">No result</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>

                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ไฟล์แนบสร้างรายการ(ผู้สร้าง)">
                                                                    <ItemTemplate>
                                                                        <div class="col-lg-10">
                                                                            <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                                        </div>
                                                                        <div class="col-lg-2">
                                                                            <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                            <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                                        </div>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="Panel_gvFileUserCheckPromise" runat="server">
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">ไฟล์แนบตรวจสอบสัญญา(ผู้สร้าง)</label>
                                                    <div class="col-md-10">
                                                        <asp:GridView ID="gvFileUserCheckPromise"
                                                            runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                            HeaderStyle-CssClass="default"
                                                            AllowPaging="true"
                                                            OnRowDataBound="gvRowDataBound"
                                                            OnPageIndexChanging="gvPageIndexChanging"
                                                            PageSize="10">
                                                            <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">No result</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>

                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ไฟล์แนบตรวจสอบสัญญา(ผู้สร้าง)">
                                                                    <ItemTemplate>
                                                                        <div class="col-lg-10">
                                                                            <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                                        </div>
                                                                        <div class="col-lg-2">
                                                                            <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                            <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                                        </div>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="Panel_gvFileUserCheckScript" runat="server">
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">ไฟล์แนบตรวจสอบต้นฉบับ(ผู้สร้าง)</label>
                                                    <div class="col-md-10">
                                                        <asp:GridView ID="gvFileUserCheckScript"
                                                            runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                            HeaderStyle-CssClass="default"
                                                            AllowPaging="true"
                                                            OnRowDataBound="gvRowDataBound"
                                                            OnPageIndexChanging="gvPageIndexChanging"
                                                            PageSize="10">
                                                            <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">No result</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>

                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ไฟล์แนบตรวจสอบต้นฉบับ(ผู้สร้าง)">
                                                                    <ItemTemplate>
                                                                        <div class="col-lg-10">
                                                                            <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                                        </div>
                                                                        <div class="col-lg-2">
                                                                            <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                            <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                                        </div>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </asp:Panel>



                                            <asp:Panel ID="Panel_gvDetailLawOfficer" runat="server">
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">ผู้ที่รับผิดชอบ</label>
                                                    <div class="col-md-10">
                                                        <asp:GridView ID="gvDetailLawOfficer"
                                                            runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                            HeaderStyle-CssClass="default"
                                                            AllowPaging="true"
                                                            OnRowDataBound="gvRowDataBound"
                                                            OnPageIndexChanging="gvPageIndexChanging"
                                                            PageSize="10">
                                                            <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">No result</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>

                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="รหัสพนักงาน" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblu2_doc_idx_view" runat="server" Text='<%# Eval("u2_doc_idx") %>' Visible="false" />
                                                                        <asp:Label ID="lbl_emp_code_view" runat="server" Text='<%# Eval("emp_code") %>' />

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ชื่อ-สกุล" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>

                                                                        <asp:Label ID="lbl_emp_name_th_view" runat="server" Text='<%# Eval("emp_name_th") %>' />

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <%--  <asp:TemplateField HeaderText="Position(EN)" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center"
                                                        HeaderStyle-Font-Size="Small">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblrpos_idx_view" runat="server" Text='<%# Eval("rpos_idx") %>' Visible="false" />
                                                            <asp:Label ID="lbl_pos_name_en_view" runat="server" Text='<%# Eval("pos_name_en") %>' />

                                                        </ItemTemplate>
                                                    </asp:TemplateField>--%>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </asp:Panel>


                                            <asp:Panel ID="Panel_gvFileLawCreatePromise" runat="server">
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">ไฟล์แนบ(กฏหมาย)</label>
                                                    <div class="col-md-10">
                                                        <asp:GridView ID="gvFileLawCreatePromise"
                                                            runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                            HeaderStyle-CssClass="default"
                                                            AllowPaging="true"
                                                            OnRowDataBound="gvRowDataBound"
                                                            OnPageIndexChanging="gvPageIndexChanging"
                                                            PageSize="10">
                                                            <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">No result</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>

                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ไฟล์แนบร่างสัญญา(กฏหมาย)">
                                                                    <ItemTemplate>
                                                                        <div class="col-lg-10">
                                                                            <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                                        </div>
                                                                        <div class="col-lg-2">
                                                                            <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                            <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                                        </div>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="Panel_gvFileLawCreateScript" runat="server">

                                                <label class="col-md-2 control-label">ไฟล์แนบตรวจสอบ/ ออกต้นฉบับ(กฏหมาย)</label>
                                                <div class="col-md-10">
                                                    <asp:GridView ID="gvFileLawCreateScript"
                                                        runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                        HeaderStyle-CssClass="default"
                                                        AllowPaging="true"
                                                        OnRowDataBound="gvRowDataBound"
                                                        OnPageIndexChanging="gvPageIndexChanging"
                                                        PageSize="10">
                                                        <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">No result</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>

                                                                    <%# (Container.DataItemIndex + 1) %>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ไฟล์แนบตรวจสอบ/ ออกต้นฉบับ(กฏหมาย)">
                                                                <ItemTemplate>
                                                                    <div class="col-lg-10">
                                                                        <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                                    </div>
                                                                    <div class="col-lg-2">
                                                                        <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                        <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                                    </div>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                </div>

                                            </asp:Panel>

                                            <asp:Panel ID="Panel_gvFileLawSavePromise" runat="server">
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">ไฟล์แนบจัดทำสัญญา(กฏหมาย)</label>
                                                    <div class="col-md-10">
                                                        <asp:GridView ID="gvFileLawSavePromise"
                                                            runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-responsive col-md-12"
                                                            HeaderStyle-CssClass="default"
                                                            AllowPaging="true"
                                                            OnRowDataBound="gvRowDataBound"
                                                            OnPageIndexChanging="gvPageIndexChanging"
                                                            PageSize="10">
                                                            <PagerStyle CssClass="pageCustom" HorizontalAlign="Left" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">No result</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>

                                                                <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center"
                                                                    HeaderStyle-Font-Size="Small">
                                                                    <ItemTemplate>

                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ไฟล์แนบจัดทำสัญญา(กฏหมาย)">
                                                                    <ItemTemplate>
                                                                        <div class="col-lg-10">
                                                                            <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                                        </div>
                                                                        <div class="col-lg-2">
                                                                            <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank">
                                                                    <i class="fa fa-download"></i></asp:HyperLink>
                                                                            <asp:HiddenField runat="server" ID="hidFile" Value='<%# Eval("Download") %>' />
                                                                        </div>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <div class="form-group pull-right">

                                                <div class="col-md-12">
                                                    <asp:LinkButton ID="btnSaveEdit" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" Text="บันทึกการแก้ไข" OnCommand="btnCommand" CommandName="cmdSaveEdit" ValidationGroup="SaveCreate"></asp:LinkButton>

                                                    <%-- <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancel"></asp:LinkButton>--%>
                                                </div>
                                            </div>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSaveEdit" />
                                        </Triggers>


                                    </asp:UpdatePanel>

                                </div>
                            </div>
                        </div>

                    </EditItemTemplate>

                </asp:FormView>
                <!-- Form Create -->

                <!-- Form Dir/mgr Approve -->
                <asp:FormView ID="fvDirectorUser" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">พิจารณารายการ By Director/ Manager User</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlDirectorUserApprove" runat="server" CssClass="form-control" ValidationGroup="SaveDirectorUserApprove" />
                                            <asp:RequiredFieldValidator ID="Req_ddlDirectorUserApprove" ValidationGroup="SaveDirectorUserApprove" runat="server" Display="None"
                                                ControlToValidate="ddlDirectorUserApprove" Font-Size="11"
                                                ErrorMessage="*Please Select Status"
                                                ValidationExpression="*Please Select Status" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlDirectorUserApprove" Width="200" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_comment_DirectorUserApprove" TextMode="MultiLine" Rows="2" runat="server" placeHolder="Comment ..." Style="overflow: auto" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocSaveDirectorUserApprove" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSaveApprove" ValidationGroup="SaveDirectorUserApprove"><i class="fas fa-save"></i> Save</asp:LinkButton>
                                            <asp:LinkButton ID="lbDocCancelDirectorUserApprove" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fas fa-times"></i> Cancel</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form Dir/mgr Approve -->

                <!-- Form MgLaw Approve -->
                <asp:FormView ID="fvMgLaw" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">พิจารณาผล By Manager Law</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlMgLawApprove" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" ValidationGroup="SaveddlMgLawApproveApprove" />
                                            <asp:RequiredFieldValidator ID="Req_ddlMgLawApprove" ValidationGroup="SaveddlMgLawApproveApprove" runat="server" Display="None"
                                                ControlToValidate="ddlMgLawApprove" Font-Size="11"
                                                ErrorMessage="*Please Select Status"
                                                ValidationExpression="*Please Select Status" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlMgLawApprove" Width="200" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_comment_MgLawApprove" TextMode="MultiLine" Rows="2" runat="server" placeHolder="Comment ..." Style="overflow: auto" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>

                                    <asp:UpdatePanel ID="_Panel_selectofficer_law" runat="server" Visible="false">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">เลือกผู้รับผิดชอบ</label>
                                                <div class="col-sm-6">
                                                    <asp:CheckBoxList ID="chkLawOfficer" runat="server" CellPadding="5" CellSpacing="5" RepeatColumns="3" Width="100%" Font-Bold="false" RepeatDirection="Vertical" RepeatLayout="Table" TextAlign="Right" Style="overflow: auto;" />

                                                    <%-- <asp:TextBox ID="TextBox1" TextMode="MultiLine" Rows="2" runat="server" placeHolder="Comment ..." Style="overflow: auto" CssClass="form-control" />--%>
                                                </div>
                                                <label class="col-sm-4"></label>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocSaveApprove" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSaveApprove" ValidationGroup="SaveddlMgLawApproveApprove"><i class="fas fa-save"></i> Save</asp:LinkButton>
                                            <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fas fa-times"></i> Cancel</asp:LinkButton>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form MgLaw Approve -->

                <!-- Form fvLaw Approve -->
                <asp:FormView ID="fvLaw" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">ร่างสัญญา แนบไฟล์ By Law Officer</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlLawApprove" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" ValidationGroup="SaveddlLawApprove" />
                                            <asp:RequiredFieldValidator ID="Req_ddlLawApprove" ValidationGroup="SaveddlLawApprove" runat="server" Display="None"
                                                ControlToValidate="ddlLawApprove" Font-Size="11"
                                                ErrorMessage="*Please Select Status"
                                                ValidationExpression="*Please Select Status" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1666" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlLawApprove" Width="200" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_comment_LawApprove" TextMode="MultiLine" Rows="2" runat="server" placeHolder="Comment ..." Style="overflow: auto" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>

                                    <asp:UpdatePanel ID="_Panel_File_permise" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group" id="div_lawfile" runat="server">
                                                <label class="col-sm-2 control-label">แนบไฟล์</label>
                                                <div class="col-sm-6">
                                                    <asp:FileUpload ID="UploadFilePromise" CssClass="control-label UploadFilePromise multi max-1 accept-pdf maxsize-1024 with-preview" runat="server" />

                                                    <%-- <asp:RequiredFieldValidator ID="Req_UploadFileMemo" ValidationGroup="SaveddlLawApprove" runat="server" Display="None"
                                                        ControlToValidate="UploadFileMemo" Font-Size="11"
                                                        ErrorMessage="*Please Attrach File memo"
                                                        ValidationExpression="*Please Attrach File memo" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_UploadFileMemo" Width="200" PopupPosition="BottomLeft" />--%>
                                                </div>
                                                <label class="col-sm-4"></label>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <asp:LinkButton ID="lbDocSaveApprove" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSaveApprove" ValidationGroup="SaveddlLawApprove"><i class="fas fa-save"></i> Save</asp:LinkButton>
                                                    <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fas fa-times"></i> Cancel</asp:LinkButton>
                                                </div>
                                            </div>
                                        </ContentTemplate>

                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbDocSaveApprove" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                        </div>

                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form fvLaw Approve -->

                <!-- Form Fv user check file Approve -->
                <asp:FormView ID="fvUserCheckfilepromise" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">ตรวจสอบสัญญา By User</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlUserCheckfile" runat="server" CssClass="form-control" ValidationGroup="SaveUserCheckFile" />
                                            <asp:RequiredFieldValidator ID="Req_ddlUserCheckfile" ValidationGroup="SaveUserCheckFile" runat="server" Display="None"
                                                ControlToValidate="ddlUserCheckfile" Font-Size="11"
                                                ErrorMessage="*Please Select Status"
                                                ValidationExpression="*Please Select Status" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlUserCheckfile" Width="200" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_comment_UserCheckFile" TextMode="MultiLine" Rows="2" runat="server" placeHolder="Comment ..." Style="overflow: auto" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-6"></label>
                                    </div>

                                    <asp:UpdatePanel ID="_Panel_CheckFile_permise" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group" id="div_usercheckfile" runat="server">
                                                <label class="col-sm-2 control-label">แนบไฟล์</label>
                                                <div class="col-sm-6">
                                                    <asp:FileUpload ID="UploadFilePromiseUserCheck" CssClass="control-label UploadFilePromiseUserCheck multi max-1 accept-pdf maxsize-1024 with-preview" runat="server" />

                                                    <%-- <asp:RequiredFieldValidator ID="Req_UploadFileMemo" ValidationGroup="SaveddlLawApprove" runat="server" Display="None"
                                                        ControlToValidate="UploadFileMemo" Font-Size="11"
                                                        ErrorMessage="*Please Attrach File memo"
                                                        ValidationExpression="*Please Attrach File memo" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_UploadFileMemo" Width="200" PopupPosition="BottomLeft" />--%>
                                                </div>
                                                <label class="col-sm-4"></label>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <asp:LinkButton ID="lbDocuserSaveApprove" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSaveApprove" ValidationGroup="SaveUserCheckFile"><i class="fas fa-save"></i> Save</asp:LinkButton>
                                                    <asp:LinkButton ID="lbDocuserCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fas fa-times"></i> Cancel</asp:LinkButton>
                                                </div>
                                            </div>
                                        </ContentTemplate>

                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbDocuserSaveApprove" />
                                        </Triggers>
                                    </asp:UpdatePanel>



                                    <%-- <div class="form-group">
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            <asp:LinkButton ID="lbDocSaveApprove" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSaveApprove" ValidationGroup="SaveUserCheckFile"><i class="fas fa-save"></i> Save</asp:LinkButton>
                                            <asp:LinkButton ID="lbDocCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fas fa-times"></i> Cancel</asp:LinkButton>
                                        </div>
                                    </div>--%>
                                </div>
                            </div>

                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form Fv user check file Approve -->

                <!-- Form fvLaw File Script -->
                <asp:FormView ID="fvLawFileScript" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">ตรวจสอบ/ออกต้นฉบับ By Law Officer</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlStatusScript" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" ValidationGroup="SaveddlStatusScript" />
                                            <asp:RequiredFieldValidator ID="Req_ddlStatusScript" ValidationGroup="SaveddlStatusScript" runat="server" Display="None"
                                                ControlToValidate="ddlStatusScript" Font-Size="11"
                                                ErrorMessage="*Please Select Status"
                                                ValidationExpression="*Please Select Status" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1666" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlStatusScript" Width="200" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_comment_LawScript" TextMode="MultiLine" Rows="2" runat="server" placeHolder="Comment ..." Style="overflow: auto" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>

                                    <asp:UpdatePanel ID="_Panel_FileScript_permise" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group" id="div_lawfilescript" runat="server">
                                                <label class="col-sm-2 control-label">แนบไฟล์</label>
                                                <div class="col-sm-6">
                                                    <asp:FileUpload ID="UploadFileScriptPromise" CssClass="control-label UploadFileScriptPromise multi max-1 accept-pdf maxsize-1024 with-preview" runat="server" />

                                                    <%-- <asp:RequiredFieldValidator ID="Req_UploadFileMemo" ValidationGroup="SaveddlLawApprove" runat="server" Display="None"
                                                        ControlToValidate="UploadFileMemo" Font-Size="11"
                                                        ErrorMessage="*Please Attrach File memo"
                                                        ValidationExpression="*Please Attrach File memo" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_UploadFileMemo" Width="200" PopupPosition="BottomLeft" />--%>
                                                </div>
                                                <label class="col-sm-4"></label>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <asp:LinkButton ID="lbDocSaveLawScript" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSaveApprove" ValidationGroup="SaveddlStatusScript"><i class="fas fa-save"></i> Save</asp:LinkButton>
                                                    <asp:LinkButton ID="lbDocCancelLawScript" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fas fa-times"></i> Cancel</asp:LinkButton>
                                                </div>
                                            </div>
                                        </ContentTemplate>

                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbDocSaveLawScript" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                        </div>

                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form fvLaw File Script -->

                <!-- Form fvUser checkScript -->
                <asp:FormView ID="fvUsercheckScript" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">ตรวจสอบต้นฉบับ/ ติดต่อซัพ By User</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlUsercheckScript" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" ValidationGroup="SaveddlUsercheckScript" />
                                            <asp:RequiredFieldValidator ID="Req_ddlUsercheckScript" ValidationGroup="SaveddlUsercheckScript" runat="server" Display="None"
                                                ControlToValidate="ddlUsercheckScript" Font-Size="11"
                                                ErrorMessage="*Please Select Status"
                                                ValidationExpression="*Please Select Status" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1666" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlUsercheckScript" Width="200" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_comment_UsercheckScript" TextMode="MultiLine" Rows="2" runat="server" placeHolder="Comment ..." Style="overflow: auto" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>

                                    <asp:UpdatePanel ID="_Panel_UsercheckScript" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group" id="div_UsercheckScript" runat="server">
                                                <label class="col-sm-2 control-label">แนบไฟล์</label>
                                                <div class="col-sm-6">
                                                    <asp:FileUpload ID="UploadFileUsercheckScript" CssClass="control-label UploadFileUsercheckScript multi max-1 accept-pdf maxsize-1024 with-preview" runat="server" />

                                                    <asp:RequiredFieldValidator ID="Req_UploadFileUsercheckScript" ValidationGroup="SaveddlUsercheckScript" runat="server" Display="None"
                                                        ControlToValidate="UploadFileUsercheckScript" Font-Size="11"
                                                        ErrorMessage="*Please Attrach File"
                                                        ValidationExpression="*Please Attrach File" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_UploadFileUsercheckScript" Width="200" PopupPosition="BottomLeft" />
                                                </div>
                                                <label class="col-sm-4"></label>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <asp:LinkButton ID="lbDocSaveUsercheckScript" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSaveApprove" ValidationGroup="SaveddlUsercheckScript"><i class="fas fa-save"></i> Save</asp:LinkButton>
                                                    <asp:LinkButton ID="lbDocCancelUsercheckScript" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fas fa-times"></i> Cancel</asp:LinkButton>
                                                </div>
                                            </div>
                                        </ContentTemplate>

                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbDocSaveUsercheckScript" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                        </div>

                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form fvUser checkScript -->

                <!-- Form fvLaw save promise -->
                <asp:FormView ID="fvSavePromise" runat="server" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">จัดทำสัญญา/ แจ้งผู้ที่เกี่ยวข้อง By Law Officer</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlLawSavePromise" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" ValidationGroup="SaveddlLawSavePromise" />
                                            <asp:RequiredFieldValidator ID="Req_ddlLawSavePromise" ValidationGroup="SaveddlLawSavePromise" runat="server" Display="None"
                                                ControlToValidate="ddlLawSavePromise" Font-Size="11"
                                                ErrorMessage="*Please Select Status"
                                                ValidationExpression="*Please Select Status" InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1666" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlLawSavePromise" Width="200" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Comment</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_comment_ddlLawSavePromise" TextMode="MultiLine" Rows="2" runat="server" placeHolder="Comment ..." Style="overflow: auto" CssClass="form-control" />
                                        </div>
                                        <label class="col-sm-4"></label>
                                    </div>

                                    <asp:UpdatePanel ID="_Panel_LawSavePromise" runat="server">
                                        <ContentTemplate>
                                            <div class="form-group" id="div_LawSavePromise" runat="server">
                                                <label class="col-sm-2 control-label">แนบไฟล์</label>
                                                <div class="col-sm-6">
                                                    <asp:FileUpload ID="UploadFileLawSavePromise" CssClass="control-label UploadFileLawSavePromise multi max-1 accept-pdf maxsize-1024 with-preview" runat="server" />

                                                    <%-- <asp:RequiredFieldValidator ID="Req_UploadFileMemo" ValidationGroup="SaveddlLawApprove" runat="server" Display="None"
                                                        ControlToValidate="UploadFileMemo" Font-Size="11"
                                                        ErrorMessage="*Please Attrach File memo"
                                                        ValidationExpression="*Please Attrach File memo" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_UploadFileMemo" Width="200" PopupPosition="BottomLeft" />--%>
                                                </div>
                                                <label class="col-sm-4"></label>
                                            </div>


                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-10">
                                                    <asp:LinkButton ID="lbDocLawSavePromise" CssClass="btn btn-success" runat="server" data-original-title="Save" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocSaveApprove" ValidationGroup="SaveddlLawSavePromise"><i class="fas fa-save"></i> Save</asp:LinkButton>
                                                    <asp:LinkButton ID="lbDocCancelLawSavePromise" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" OnCommand="btnCommand" CommandName="cmdDocCancel"><i class="fas fa-times"></i> Cancel</asp:LinkButton>
                                                </div>
                                            </div>
                                        </ContentTemplate>

                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="lbDocLawSavePromise" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                        </div>

                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form fvLaw File Script -->

                <!-- Log Detail OT Mont -->
                <div class="panel panel-info" id="div_Log" runat="server" visible="false">
                    <div class="panel-heading">ประวัติการดำเนินการ</div>
                    <table class="table table-striped f-s-12 table-empshift-responsive">
                        <asp:Repeater ID="rptLog" runat="server">
                            <HeaderTemplate>
                                <tr>
                                    <th>วัน / เวลา</th>
                                    <th>ผู้ดำเนินการ</th>
                                    <th>ดำเนินการ</th>
                                    <%--<th>ผลการดำเนินการ</th>--%>
                                    <th>เหตุผล</th>
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td data-th="วัน / เวลา"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                    <td data-th="ผู้ดำเนินการ"><%# Eval("emp_name_th") %> ( <%# Eval("current_artor") %> )</td>
                                    <td data-th="ดำเนินการ"><%# Eval("current_decision") %></td>
                                    <td data-th="เหตุผล"><%# Eval("comment") %></td>
                                    <%--<td data-th="ผลการดำเนินการ"><%# Eval("current_status_name") %></td>--%>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <div class="m-t-10 m-b-10"></div>
                </div>
                <!-- Log Detail OT Mont -->

            </div>
        </asp:View>
        <!--View Create-->

        <!--View Detail-->
        <asp:View ID="docDetail" runat="server">
            <div class="col-md-12">

                <asp:UpdatePanel ID="_PanelSearchDetail" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหารายการ</h3>
                            </div>

                            <div class="panel-body">

                                <div class="col-md-10 col-md-offset-1">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ประเภทเอกสาร</label>
                                            <asp:DropDownList ID="ddlTypeDocumentSearch" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>


                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>รูปแบบ</label>
                                            <asp:DropDownList ID="ddlSubTypeDocumentSearch" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>รหัสเอกสาร</label>
                                            <asp:TextBox ID="txtDocumentSearch" runat="server" placeholder="รหัสเอกสาร" CssClass="form-control">
                                            </asp:TextBox>

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <%--<label>&nbsp;</label>--%>
                                            <div class="clearfix"></div>
                                            <asp:LinkButton ID="btnSearchDetail" runat="server" CssClass="btn btn-primary" Text="ค้นหา" OnCommand="btnCommand" CommandName="cmdSearchDetail" />

                                            <asp:LinkButton ID="btnClearSearchDetail" runat="server" CssClass="btn btn-default" Text="ล้างค่า" OnCommand="btnCommand" CommandName="cmdClearSearchDetail" />
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>


                <div id="div_GvDetail" style="overflow-x: auto; width: 100%" runat="server">
                    <asp:GridView ID="GvDetail" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        ShowHeaderWhenEmpty="False"
                        AllowPaging="True"
                        PageSize="10"
                        BorderStyle="None"
                        CellSpacing="2">
                        <HeaderStyle CssClass="success" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                                <ItemTemplate>
                                    <p>
                                        <asp:Label ID="lbl_document_code_detail" runat="server" Text='<%# Eval("document_code") %>'></asp:Label>
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่ทำรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                                <ItemTemplate>
                                    <p>
                                        <asp:Label ID="lbl_create_date_detail" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียดผู้สร้าง" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="left">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_u0_document_idx_detail" runat="server" Visible="false" Text='<%# Eval("u0_doc_idx") %>'></asp:Label>

                                    <p>
                                        <b>ชื่อ-สกุลผู้สร้าง:</b>
                                        <asp:Label ID="lbl_cemp_idx_detail" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                        <asp:Label ID="lbl_emp_name_th_detail" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                    </p>
                                    <p>
                                        <b>องค์กร:</b>
                                        <asp:Label ID="lbl_org_name_th_detail" runat="server" Text='<%# Eval("org_name_th") %>' />
                                    </p>
                                    <p>
                                        <b>ฝ่าย:</b>
                                        <asp:Label ID="lbl_dept_name_th_detail" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียดรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" ItemStyle-CssClass="left">
                                <ItemTemplate>

                                    <p>
                                        <b>ประเภทเอกสาร:</b>

                                        <asp:Label ID="lbl_type_document_en_detail" runat="server" Text='<%# Eval("type_document_en") %>' />
                                    </p>
                                    <p>
                                        <b>รูปแบบ:</b>
                                        <asp:Label ID="lbl_subtype_document_en_detail" runat="server" Text='<%# Eval("subtype_document_en") %>' />
                                    </p>
                                    <p>
                                        <b>หัวข้อ:</b>
                                        <asp:Label ID="lbl_topic_detail" runat="server" Text='<%# Eval("topic") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" ItemStyle-CssClass="text-center">

                                <ItemTemplate>

                                    <asp:Label ID="lbl_staidx_detail" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                    <asp:Label ID="lbl_status_name_detail" runat="server" Text='<%# Eval("status_name") %>' />
                                    <asp:Label ID="lbl_m0_node_idx_detail" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                    <asp:Label ID="lbl_node_name_detail" runat="server" Text='<%# Eval("node_name") %>' />
                                    <br />
                                    โดย<br />
                                    <asp:Label ID="lbl_m0_actor_idx_detail" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                    <asp:Label ID="lbl_actor_name_detail" runat="server" Text='<%# Eval("actor_name") %>' />


                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" ItemStyle-CssClass="text-center">
                                <ItemTemplate>

                                    <asp:UpdatePanel ID="Update_PnbtnViewDetail" runat="server">
                                        <ContentTemplate>

                                            <asp:LinkButton ID="btnViewDetail" CssClass="btn-sm btn-info" target="" runat="server" CommandName="cmdView" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_doc_idx")+ ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx")%>'
                                                data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>

                                        </ContentTemplate>

                                    </asp:UpdatePanel>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>

            </div>
        </asp:View>
        <!--View Detail-->

        <!-- View Approve -->
        <asp:View ID="docApprove" runat="server">
            <div class="col-md-12">

                <div class="col-md-12" id="div_fileupload" runat="server" style="color: transparent;">
                    <asp:FileUpload ID="FileUploadTestShow" CssClass="btn btn-warning hidden" ClientIDMode="Static" runat="server" accept="jpg|png|pdf" />
                </div>

                <div id="div_Approve" style="overflow-x: auto; width: 100%" runat="server">
                    <asp:GridView ID="gvApprove" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        ShowHeaderWhenEmpty="True"
                        AllowPaging="True"
                        PageSize="10"
                        BorderStyle="None"
                        CellSpacing="2">
                        <HeaderStyle CssClass="default" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="text-center">
                                <ItemTemplate>
                                    <p>
                                        <asp:Label ID="lbl_document_code_approve" runat="server" Text='<%# Eval("document_code") %>'></asp:Label>
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="วันที่ทำรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                                <ItemTemplate>
                                    <p>
                                        <asp:Label ID="lbl_create_date_approve" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียดผู้สร้าง" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="left">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_u0_document_idx_approve" runat="server" Visible="false" Text='<%# Eval("u0_doc_idx") %>'></asp:Label>

                                    <p>
                                        <b>ชื่อ-สกุลผู้สร้าง:</b>
                                        <asp:Label ID="lbl_cemp_idx_approve" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                        <asp:Label ID="lbl_emp_name_th_approve" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                    </p>
                                    <p>
                                        <b>องค์กร:</b>
                                        <asp:Label ID="lbl_org_name_th_approve" runat="server" Text='<%# Eval("org_name_th") %>' />
                                    </p>
                                    <p>
                                        <b>ฝ่าย:</b>
                                        <asp:Label ID="lbl_dept_name_th_approve" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียดรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="left">
                                <ItemTemplate>

                                    <p>
                                        <b>ประเภทเอกสาร:</b>

                                        <asp:Label ID="lbl_type_document_en_approve" runat="server" Text='<%# Eval("type_document_en") %>' />
                                    </p>
                                    <p>
                                        <b>รูปแบบ:</b>
                                        <asp:Label ID="lbl_subtype_document_en_approve" runat="server" Text='<%# Eval("subtype_document_en") %>' />
                                    </p>
                                    <p>
                                        <b>หัวข้อ:</b>
                                        <asp:Label ID="lbl_topic_approve" runat="server" Text='<%# Eval("topic") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="สถานะรายการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" ItemStyle-CssClass="text-center">

                                <ItemTemplate>

                                    <asp:Label ID="lbl_staidx_approve" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                    <asp:Label ID="lbl_status_name_approve" runat="server" Text='<%# Eval("status_name") %>' />
                                    <asp:Label ID="lbl_m0_node_idx_approve" runat="server" Text='<%# Eval("noidx") %>' Visible="false" />
                                    <asp:Label ID="lbl_node_name_approve" runat="server" Text='<%# Eval("node_name") %>' />
                                    <br />
                                    โดย<br />
                                    <asp:Label ID="lbl_m0_actor_idx_approve" runat="server" Text='<%# Eval("acidx") %>' Visible="false" />
                                    <asp:Label ID="lbl_actor_name_approve" runat="server" Text='<%# Eval("actor_name") %>' />


                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" ItemStyle-CssClass="text-center">
                                <ItemTemplate>

                                    <asp:UpdatePanel ID="Update_Approve" runat="server">
                                        <ContentTemplate>

                                            <asp:LinkButton ID="btnViewApprove" CssClass="btn-sm btn-info" target="" runat="server" CommandName="cmdViewApprove" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_doc_idx")+ ";" + Eval("noidx") + ";" + Eval("acidx") + ";" + Eval("staidx") + ";" + Eval("cemp_idx")%>'
                                                data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>

                                        </ContentTemplate>

                                    </asp:UpdatePanel>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>

            </div>
        </asp:View>
        <!-- View Approve -->

        <!-- View Promise -->
        <asp:View ID="docPromise" runat="server">
            <div class="col-md-12">

                <asp:UpdatePanel ID="PanelFilePromise" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหาเอกสารสัญญา/ หนังสือมอบอำนาจ</h3>
                            </div>

                            <div class="panel-body">

                                <div class="col-md-10 col-md-offset-1">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>ประเภทเอกสาร</label>
                                            <asp:DropDownList ID="ddlTypeDocumentSearchFile" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>


                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>รูปแบบ</label>
                                            <asp:DropDownList ID="ddlSubTypeDocumentSearchFile" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                            </asp:DropDownList>

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>รหัสเอกสาร</label>
                                            <asp:TextBox ID="txtDocumentSearchFile" runat="server" placeholder="รหัสเอกสาร" CssClass="form-control">
                                            </asp:TextBox>

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <%--<label>&nbsp;</label>--%>
                                            <div class="clearfix"></div>
                                            <asp:LinkButton ID="btnSearchDetailFile" runat="server" CssClass="btn btn-primary" Text="ค้นหา" OnCommand="btnCommand" CommandName="cmdSearchDetailFile" />

                                            <asp:LinkButton ID="btnClearSearchDetailFile" runat="server" CssClass="btn btn-default" Text="ล้างค่า" OnCommand="btnCommand" CommandName="cmdClearSearchDetailFile" />
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">รายละเอียดเอกสารสัญญา/ หนังสือมอบอำนาจ</h3>
                    </div>
                    <div id="div_Promise" style="overflow-x: auto; width: 100%" runat="server">
                        <div class="panel-body">
                            <asp:GridView ID="GvDocumentPromise" runat="server"
                                AutoGenerateColumns="False"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                OnPageIndexChanging="gvPageIndexChanging"
                                OnRowDataBound="gvRowDataBound"
                                ShowHeaderWhenEmpty="True"
                                AllowPaging="True"
                                PageSize="10"
                                BorderStyle="None"
                                CellSpacing="2">
                                <HeaderStyle CssClass="default" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>
                                            <p>
                                                <%# (Container.DataItemIndex +1) %>
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รหัสเอกสาร" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-left">
                                        <ItemTemplate>
                                            <p>
                                                <asp:Label ID="lbl_u0_doc_idx_file" Visible="false" runat="server" Text='<%# Eval("u0_doc_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_document_code_file" runat="server" Text='<%# Eval("document_code") %>' />
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center" ItemStyle-Width="20%" ItemStyle-CssClass="text-left">
                                        <ItemTemplate>
                                            <p>
                                                <b>ประเภทเอกสาร:</b>
                                                <asp:Label ID="lbl_type_document_en_file" runat="server" Text='<%# Eval("type_document_en") %>' />
                                            </p>
                                            <p>
                                                <b>รูปแบบ:</b>
                                                <asp:Label ID="lbl_subtype_document_en_file" runat="server" Text='<%# Eval("subtype_document_en") %>' />
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="วันที่เริ่มต้น" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="left">
                                        <ItemTemplate>
                                            <p>
                                                <asp:Label ID="lbl_date_start_file" runat="server" Text='<%# Eval("date_start") %>'></asp:Label>
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="วันที่สิ้นสุด" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="left">
                                        <ItemTemplate>
                                            <p>
                                                <asp:Label ID="lbl_date_end_file" runat="server" Text='<%# Eval("date_end") %>'></asp:Label>
                                            </p>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-center">
                                        <ItemTemplate>

                                            <asp:UpdatePanel ID="Update_File" runat="server">
                                                <ContentTemplate>

                                                    <asp:HyperLink runat="server" ID="btnDLX" CssClass="btn btn-default btn-sm" data-toggle="tooltip" title="เอกสาร" Target="_blank"><i class="fa fa-folder-open"></i></asp:HyperLink>

                                                </ContentTemplate>

                                            </asp:UpdatePanel>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                </div>

            </div>
        </asp:View>
        <!-- View Promise -->

    </asp:MultiView>
    <!--multiview-->

    <!--script-->
    <script src='<%=ResolveUrl("~/Scripts/jquery.MultiFileNew.js")%>'></script>
    <!--script-->

    <script>
        $(function () {
            $('.UploadFilePromise').MultiFile({

            });

        });

        $(function () {
            $('.UploadFile').MultiFile({

            });

        });

        $(function () {
            $('.UploadFilePromiseUserCheck').MultiFile({

            });

        });

        $(function () {
            $('.UploadFileScriptPromise').MultiFile({

            });

        });

        $(function () {
            $('.UploadFileUsercheckScript').MultiFile({

            });

        });

        $(function () {
            $('.UploadFileLawSavePromise').MultiFile({

            });

        });

        $(function () {
            $('.UploadFileEdit').MultiFile({

            });

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.UploadFilePromise').MultiFile({

                });

            });

            $(function () {
                $('.UploadFile').MultiFile({

                });

            });

            $(function () {
                $('.UploadFilePromiseUserCheck').MultiFile({

                });

            });

            $(function () {
                $('.UploadFileScriptPromise').MultiFile({

                });

            });

            $(function () {
                $('.UploadFileUsercheckScript').MultiFile({

                });

            });

            $(function () {
                $('.UploadFileLawSavePromise').MultiFile({

                });

            });

            $(function () {
                $('.UploadFileEdit').MultiFile({

                });

            });

        });

        //})
    </script>

    <script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            //var dateToday = new Date();
            //currentTime = new Date();
            $('.datetimepicker-from').datetimepicker({
                format: 'DD/MM/YYYY',
                //minDate: moment().add(-1, 'day'),
                //maxDate: moment().add(60, 'day'),
                //ignoreReadonly: true

            });
            $('.datetimepicker-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                //}

            });

            $('.show-from-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').datetimepicker({
                format: 'DD/MM/YYYY',
                //minDate: moment().add(-1, 'day'),
                //maxDate: moment().add(60, 'day'),
                //ignoreReadonly: true
            });
            $('.show-to-onclick').click(function () {
                $('.datetimepicker-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }

            });


        });
    </script>

</asp:Content>





