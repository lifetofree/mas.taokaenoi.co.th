﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

public class Handler : IHttpHandler
{
   public void ProcessRequest(HttpContext context)
   {
      int creator = 0;
      int announceIdxRef = 0;
      string callback = context.Request.QueryString["callback"];
      string changeAnnounceStartDate = String.Empty;
      string changeAnnounceEndDate = String.Empty;
      int.TryParse(context.Request.QueryString["announceIdxRef"], out announceIdxRef);
      int.TryParse(context.Request.QueryString["creator"], out creator);
      changeAnnounceStartDate = context.Request.QueryString["changeAnnounceStartDate"];
      changeAnnounceEndDate = context.Request.QueryString["changeAnnounceEndDate"];
      string json = this.createChangeAnnounce(creator, announceIdxRef, changeAnnounceStartDate, changeAnnounceEndDate);
      if (!string.IsNullOrEmpty(callback))
      {
         json = string.Format("{0}({1});", callback, json);
      }
      context.Response.ContentType = "text/json";
      context.Response.Write(json);
   }

   private string createChangeAnnounce(int creator, int announceIdxRef, string changeAnnounceStartDate, string changeAnnounceEndDate)
   {
      List<object> emps = new List<object>();
      using (SqlConnection conn = new SqlConnection())
      {
         conn.ConnectionString = ConfigurationManager.ConnectionStrings["conn_mas"].ConnectionString;
         using (SqlCommand cmd = new SqlCommand())
         {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;
            var now = DateTime.Now.ToString("yyyy-MM-dd H:m:s");
            int statusOnline = 1;
            cmd.Connection = conn;
            cmd.CommandText = "INSERT INTO emps_u0_change_announce (u0_announce_idx_ref, u0_node_idx_ref, change_announce_date_start, change_announce_date_end, change_announce_status, change_announce_created_at, change_announce_created_by, change_announce_updated_at, change_announce_updated_by) OUTPUT Inserted.u0_change_announce_idx VALUES (@announceIdxRef, 1, @changeAnnounceStartDate, @changeAnnounceEndDate, " + statusOnline + ", '" + now + "', @creator, '" + now + "', @creator)";
            cmd.Parameters.AddWithValue("@changeAnnounceStartDate", changeAnnounceStartDate);
            cmd.Parameters.AddWithValue("@changeAnnounceEndDate", changeAnnounceEndDate);
            cmd.Parameters.AddWithValue("@creator", creator);
            cmd.Parameters.AddWithValue("@announceIdxRef", announceIdxRef);
            conn.Open();
            var insertedId = cmd.ExecuteScalar();
            emps.Add(new
            {
               changeAnnounceIdx = insertedId
            });
            conn.Close();
         }
         return (new JavaScriptSerializer().Serialize(emps));
      }
   }

   public bool IsReusable
   {
      get
      {
         return false;
      }
   }
}
