﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="DeviceRegister.aspx.cs" Inherits="websystem_ITServices_DeviceRegister" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadImages_Cutoff").MultiFile();
            }
        })
    </script>

    <script type="text/javascript">
        function GridSelectAllColumn(spanChk) {
            var oItem = spanChk.children;
            var theBox = (spanChk.type == "checkbox") ? spanChk : spanChk.children.item[0]; xState = theBox.checked;
            elm = theBox.form.elements;

            for (i = 0; i < elm.length; i++) {
                if (elm[i].type === 'checkbox' && elm[i].checked != xState)
                    elm[i].click();
            }
        }
        function GridSelectAllColumn_ram(spanChk) {
            var oItem = spanChk.children;
            var theBox = (spanChk.type == "checkbox") ? spanChk : spanChk.children.item[0]; xState = theBox.checked;
            elm = theBox.form.elements;

            for (i = 0; i < elm.length; i++) {
                if (elm[i].type === 'checkbox' && elm[i].checked != xState)
                    elm[i].click();
            }
        }
    </script>

    <script type="text/javascript">
        function CheckAll() {
            var intIndex = 0;
            var rowCount = document.getElementById('CheckBox_free').getElementsByTagName("input").length;
            for (i = 0; i < rowCount; i++) {
                if (document.getElementById('chkAll').checked == true) {
                    if (document.getElementById("CheckBox_free" + "_" + i)) {
                        if (document.getElementById("CheckBox_free" + "_" + i).disabled != true)
                            document.getElementById("CheckBox_free" + "_" + i).checked = true;
                    }
                }
                else {
                    if (document.getElementById("CheckBox_free" + "_" + i)) {
                        if (document.getElementById("CheckBox_free" + "_" + i).disabled != true)
                            document.getElementById("CheckBox_free" + "_" + i).checked = false;
                    }
                }
            }
        }

        function UnCheckAll() {
            var intIndex = 0;
            var flag = 0;
            var rowCount = document.getElementById('CheckBox_free').getElementsByTagName("input").length;
            for (i = 0; i < rowCount; i++) {
                if (document.getElementById("CheckBox_free" + "_" + i)) {
                    if (document.getElementById("CheckBox_free" + "_" + i).checked == true) {
                        flag = 1;
                    }
                    else {
                        flag = 0;
                        break;
                    }
                }
            }
            if (flag == 0)
                document.getElementById('chkAll').checked = false;
            else
                document.getElementById('chkAll').checked = true;

        }
    </script>




    <%----------- Menu Tab Start---------------%>
    <div id="BoxTabMenuIndex" runat="server">
        <div class="form-group">
            <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#Menu1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="sub-navbar">
                            <a class="navbar-brand " href="#"><b>Menu</b></a>
                        </div>
                    </div>

                    <div class="collapse navbar-collapse" id="Menu1">
                        <asp:LinkButton ID="lbindex" CssClass="btn_menulist" runat="server" CommandName="btnIndex" OnCommand="btnCommand" CommandArgument="1">รายการทั่วไป</asp:LinkButton>
                        <asp:LinkButton ID="lbadd" CssClass="btn_menulist" runat="server" CommandName="btnadd" OnCommand="btnCommand" CommandArgument="2">เพิ่มอุปกรณ์</asp:LinkButton>
                        <asp:LinkButton ID="lbapproveadddevice" CssClass="btn_menulist" runat="server" CommandName="btnapproveadddevice" OnCommand="btnCommand" CommandArgument="7">รายการอนุมัติเพิ่มอุปกรณ์</asp:LinkButton>
                        <asp:LinkButton ID="lbapprovetransfer" CssClass="btn_menulist" runat="server" CommandName="btnapprovetransfer" OnCommand="btnCommand" CommandArgument="8">รายการอนุมัติเปลี่ยนแปลงอุปกรณ์</asp:LinkButton>
                        <asp:LinkButton ID="lbapprovecutoff" CssClass="btn_menulist" runat="server" CommandName="btnapprovecutoff" OnCommand="btnCommand" Visible="false" CommandArgument="6">รายการอนุมัติตัดชำรุด</asp:LinkButton>
                        <asp:LinkButton ID="lbaddmasterdata" CssClass="btn_menulist" runat="server" CommandName="btnaddmasterdata" OnCommand="btnCommand" Visible="true" CommandArgument="5">Master Data</asp:LinkButton>
                    </div>
                </div>
                <!-- /.container-fluid -->
            </nav>
        </div>
    </div>
    <%-------------- Menu Tab End--------------%>

    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <%-------- Index Start----------%>
        <asp:View ID="ViewIndex" runat="server">
            <div class="col-lg-12">
                <div class="form-group">
                    <div class="col-lg-9"></div>
                    <div class="col-lg-3">
                        <asp:LinkButton ID="lb_guidebook" class="btn btn-success" runat="server" Text="คู่มือ" CommandName="cmd_guidebook" OnCommand="btnCommand" />
                        <asp:LinkButton ID="lb_flow" class="btn btn-success" runat="server" Text="Flow Process" CommandName="cmd_flow" OnCommand="btnCommand" />
                    </div>
                </div>
                <hr />
            </div>

            <div class="col-lg-12">
                <asp:Label ID="fs" runat="server"></asp:Label>
                <asp:Label ID="fs_2" runat="server"></asp:Label>
                <asp:Label ID="fs_3" runat="server"></asp:Label>
                <asp:Label ID="fs_4" runat="server"></asp:Label>
                <asp:Label ID="fds" runat="server"></asp:Label>
                <asp:HyperLink runat="server" ID="txtfocus" />
                <asp:Panel ID="boxindex" runat="server" Visible="true">
                    <%-------------- BoxSearch Start--------------%>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; ค้นหาข้อมูล</strong></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" id="BoxSearch" runat="server">
                                <asp:FormView ID="Fv_Search_Device_Index" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                    <InsertItemTemplate>
                                        <div class="form-group">
                                            <asp:Label ID="Label65" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ :" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddl_search_typedevice_index" AutoPostBack="true" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label23" CssClass="col-sm-2 control-label" runat="server" Text="สถานะอุปกรณ์ :" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddl_search_statusdevice_index" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                            <asp:Label ID="Label44" CssClass="control-label col-sm-2" runat="server" Text="รหัสอุปกรณ์ :" />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_search_code_index" CssClass="form-control" runat="server" placeholder="Ex. PC00000001"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label67" runat="server" Text="Asset Code :" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_search_assetcode_index" CssClass="form-control" runat="server" placeholder="Ex. 00000000"></asp:TextBox>
                                            </div>
                                            <asp:Label ID="Label48" runat="server" Text="SerialNumber :" CssClass="col-sm-2 control-label"></asp:Label>
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_search_serial_index" CssClass="form-control" runat="server" placeholder="Ex. 00000000"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Lab3el4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddl_add_org_Search" runat="server" AutoPostBack="true" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-6">
                                                <asp:Button ID="btnsearch_index" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btn_search_index" OnCommand="btnCommand" />
                                                <asp:Button ID="btnclearsearch_index" class="btn btn-primary" runat="server" Text="Clear" data-original-title="search" data-toggle="tooltip" CommandName="Cmdsearch_clear" OnCommand="btnCommand" />
                                            </div>
                                        </div>
                                    </InsertItemTemplate>
                                </asp:FormView>
                            </div>
                        </div>
                    </div>
                    <%-------------- BoxSearch End--------------%>
                    <asp:GridView ID="GvDevice"
                        runat="server"
                        AutoGenerateColumns="false"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        HeaderStyle-CssClass="table_headCenter"
                        HeaderStyle-Height="40px"
                        AllowPaging="True"
                        PageSize="15"
                        DataKeyNames="u0_didx"
                        OnPageIndexChanging="Master_PageIndexChanging"
                        OnRowDataBound="Master_RowDataBound">

                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                        <EmptyDataTemplate>
                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                        </EmptyDataTemplate>

                        <Columns>

                            <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="7%">
                                <ItemTemplate>
                                    <small>
                                        <b>
                                            <asp:Label ID="fsde" runat="server" Text="รหัสอุปกรณ์ : " /></b><asp:Label ID="lbDeviceIDX" runat="server" Text='<%# Eval("u0_code") %>' />
                                        </p>
                                        <b>
                                            <asp:Label ID="lbl_code_main_1" runat="server" Text="รหัสอุปกรณ์หลัก : " /></b><asp:Label ID="lbl_code_main_2" runat="server" Text='<%# Eval("u0_code_main") %>'></asp:Label>
                                        <asp:Label ID="lblu0_referent_code" runat="server" Text='<%# Eval("u0_referent_code") %>' Visible="false" />
                                        <asp:Label ID="lbl_u0_didx_main" runat="server" Text='<%# Eval("u0_didx") %>' Visible="false" />
                                        </p>
                                        <b>
                                            <asp:Label ID="frse" runat="server" Text="ผู้ถือครอง : " /></b><asp:Label ID="lblAsse4ta" runat="server" Text='<%# Eval("FullNameTH_HD") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                <ItemTemplate>
                                    <small>
                                        <b>
                                            <asp:Label ID="fse" runat="server" Text="เลขที่ Acc : " /></b><asp:Label ID="lblAsseta" runat="server" Text='<%# Eval("u0_acc") %>'></asp:Label>
                                        </p>
                                        <b>
                                            <asp:Label ID="Label53" runat="server" Text="เลขที่ Po : " /></b><asp:Label ID="Label59" runat="server" Text='<%# Eval("u0_po") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Detail Device" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="7%">
                                <ItemTemplate>
                                    <small>
                                        <asp:Label ID="lb_m0_tdidx" runat="server" Text='<%# Eval("m0_tdidx") %>' Visible="false" />
                                        <b>
                                            <asp:Label ID="Label533" runat="server" Text="ประเภทอุปกรณ์ : " /></b><asp:Label ID="lblAssetaq" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                        </p>
                                        <b>
                                            <asp:Label ID="Label60" runat="server" Text="ยี่ห้อ : " /></b><asp:Label ID="lblAssetsaq" runat="server" Text='<%# Eval("name_m0_band") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Status Device" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="9%">
                                <ItemTemplate>
                                    <small>
                                        <b>
                                            <asp:Label ID="lbl_m0_sidx_status" runat="server" Visible="false" Text='<%# Eval("m0_sidx") %>'></asp:Label>
                                            <asp:Label ID="fs" runat="server" Text="สถานะอุปกรณ์ : " /></b><asp:Label ID="lblAssetsueaq" runat="server" Text='<%# Eval("name_m0_status") %>'></asp:Label>
                                        </p>
                                        <asp:Label ID="lblstatusdoc" runat="server" Text='<%# Eval("doc_status") %>' Visible="false"></asp:Label>
                                        <b>
                                            <asp:Label ID="Label52" runat="server" Text="สถานะรายการ : " /></b><asp:Label ID="Label30" runat="server" Text='<%# Eval("name_doc_status") %>'></asp:Label>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="3%">
                                <ItemTemplate>
                                    <small>
                                        <asp:LinkButton ID="btnView" CssClass="btn btn-primary" runat="server" CommandName="btnView_GvDevice" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_didx") %>' data-toggle="tooltip" title="รายละเอียด"><i class="fa fa-edit"></i></asp:LinkButton>

                                        <%--<asp:LinkButton ID="btnGvapprove" CssClass="btn btn-warning" runat="server" CommandName="btnView_Gv_Approve" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_didx") %>' data-toggle="tooltip" title="อนุมัติเพิ่มอุปกรณ์"><i class="fa fa-check-square-o"></i></asp:LinkButton>--%>

                                        <%--<asp:LinkButton ID="btnGvapprove_tranfer" CssClass="btn btn-success" runat="server" CommandName="btnView_Gv_Approve_tranfer" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_didx") %>' data-toggle="tooltip" title="อนุมัติเปลี่ยนแปลงอุปกรณ์"><i class="fa fa-check-square-o"></i></asp:LinkButton>--%>
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                    <asp:Panel ID="Box_Temp_Transfer_All" runat="server" Visible="false">
                        <asp:GridView ID="GvTemp_Device_Transfer_all"
                            DataKeyNames="u0_didx"
                            runat="server"
                            AutoGenerateColumns="false">
                            <Columns>
                                <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <div class="panel-heading">
                                            <asp:Label ID="lbl_u0_didx_approve_transfer" runat="server" Text='<%# Eval("u0_didx")%>'></asp:Label>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </asp:Panel>

                <asp:Panel ID="boxviewdetail" runat="server" Visible="false">
                    <asp:FormView ID="FvViewDetailDevice" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <EditItemTemplate>
                            <%--<asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("u0_unidx") %>' />
                            <asp:HiddenField ID="hfM0ActorIDX" runat="server" Value='<%# Eval("u0_acidx") %>' />--%>
                            <div class="panel panel-success class">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียดทะเบียนคอมพิวเตอร์</strong></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <asp:Panel ID="box_edit_detail" runat="server" Visible="true">
                                            <div class="form-group">
                                                <asp:Label ID="Label13" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lbl_view_u0_didx" runat="server" Visible="false" Text='<%# Eval("u0_didx") %>'></asp:Label>
                                                    <asp:Label ID="lbl_view_typedevice" runat="server" Visible="false" Text='<%# Eval("m0_tdidx") %>'></asp:Label>
                                                    <asp:TextBox ID="txt_view_typedevice" CssClass="form-control" runat="server" Text='<%# Eval("name_m0_typedevice") %>' Enabled="false"></asp:TextBox>
                                                    <asp:Label ID="lbl_hfM0NodeIDX" runat="server" Visible="false" Text='<%# Eval("u0_unidx") %>'></asp:Label>
                                                    <asp:Label ID="lbl_hfM0ActorIDX" runat="server" Visible="false" Text='<%# Eval("u0_acidx") %>'></asp:Label>
                                                    <asp:Label ID="lbl_u0_software_licen" runat="server" Visible="false" Text='<%# Eval("u0_software_licen") %>'></asp:Label>
                                                    <asp:Label ID="lbl_u0_software_free" runat="server" Visible="false" Text='<%# Eval("u0_software_free") %>'></asp:Label>
                                                    <%--<asp:Label ID="lbl_HDEmpIDX" runat="server" Visible="false" Text='<%# Eval("HDEmpIDX") %>'></asp:Label>--%>
                                                    <asp:Label ID="lbl_m0_orgidx_view" runat="server" Visible="false" Text='<%# Eval("m0_orgidx") %>'></asp:Label>
                                                    <asp:Label ID="lbl_R0_depidx_view" runat="server" Visible="false" Text='<%# Eval("R0_depidx") %>'></asp:Label>
                                                    <asp:Label ID="lbl_R0_secidx_view" runat="server" Visible="false" Text='<%# Eval("R0_secidx") %>'></asp:Label>
                                                    <asp:Label ID="lbl_u0_relation_didx_view" runat="server" Visible="false" Text='<%# Eval("u0_relation_didx") %>'></asp:Label>

                                                </div>
                                                <asp:Label ID="Label74" CssClass="col-sm-3 control-label" runat="server" Text="รหัสเก่า : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txt_view_code_old" CssClass="form-control" Text='<%# Eval("u0_code_old") %>' runat="server"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label64" CssClass="col-sm-2 control-label" runat="server" Text="รหัสอุปกรณ์ : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txt_view_code" CssClass="form-control" Text='<%# Eval("u0_code") %>' runat="server" Enabled="false"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label77" class="col-sm-3 control-label" runat="server" Text="รหัสอุปกรณ์หลัก : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txt_view_code_main" CssClass="form-control" Text='<%# Eval("u0_code_main") %>' Enabled="false" runat="server"></asp:TextBox>
                                                </div>
                                            </div>

                                            <asp:Panel ID="boxview_cpu" runat="server" Visible="false">
                                                <div class="form-group">
                                                    <asp:Label ID="Label79" CssClass="col-sm-2 control-label" runat="server" Text="รายละเอียด Cpu : " />
                                                    <div class="col-sm-3">
                                                        <asp:Label ID="lbl_view_cpu" runat="server" Visible="false" Text='<%# Eval("m0_cidx") %>'></asp:Label>
                                                        <asp:DropDownList ID="ddl_view_cpu" runat="server" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="boxview_ram" runat="server" Visible="false">
                                                <div class="form-group">
                                                    <asp:Label ID="Label76" CssClass="col-sm-2 control-label" runat="server" Text="รายละเอียด Ram : " />
                                                    <div class="col-sm-3">
                                                        <asp:Label ID="lbl_view_ram" runat="server" Visible="false" Text='<%# Eval("m0_ridx") %>'></asp:Label>
                                                        <asp:DropDownList ID="ddl_view_ram" runat="server" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="boxview_hdd" runat="server" Visible="false">
                                                <div class="form-group">
                                                    <asp:Label ID="Label78" CssClass="col-sm-2 control-label" runat="server" Text="รายละเอียด Hdd : " />
                                                    <div class="col-sm-3">
                                                        <asp:Label ID="lbl_view_hdd" runat="server" Visible="false" Text='<%# Eval("m0_hidx") %>'></asp:Label>
                                                        <asp:DropDownList ID="ddl_view_hdd" runat="server" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="boxview_vga" runat="server" Visible="false">
                                                <div class="form-group">
                                                    <asp:Label ID="Label80" CssClass="col-sm-2 control-label" runat="server" Text="รายละเอียด Vga : " />
                                                    <div class="col-sm-3">
                                                        <asp:Label ID="lbl_view_vga" runat="server" Visible="false" Text='<%# Eval("m0_vidx") %>'></asp:Label>
                                                        <asp:DropDownList ID="ddl_view_vga" runat="server" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <asp:Panel ID="boxview_moniter" runat="server" Visible="false">
                                                <div class="form-group">
                                                    <asp:Label ID="Label81" CssClass="col-sm-2 control-label" runat="server" Text="รายละเอียด Moniter : " />
                                                    <div class="col-sm-3">
                                                        <asp:Label ID="lbl_view_moniter" runat="server" Visible="false" Text='<%# Eval("m0_midx") %>'></asp:Label>
                                                        <asp:DropDownList ID="ddl_view_moniter" runat="server" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>


                                            </asp:Panel>

                                            <asp:Panel ID="boxview_printer" runat="server" Visible="false">
                                                <div class="form-group">
                                                    <asp:Label ID="Label82" CssClass="col-sm-2 control-label" runat="server" Text="รายละเอียด Printer : " />
                                                    <div class="col-sm-3">
                                                        <asp:Label ID="lbl_view_printer" runat="server" Visible="false" Text='<%# Eval("m0_pidx") %>'></asp:Label>
                                                        <asp:DropDownList ID="ddl_view_printer" runat="server" CssClass="form-control"></asp:DropDownList>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <div class="form-group">
                                                <asp:Label ID="Label12" class="col-sm-2 control-label" runat="server" Text="เลขที่ Acc : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txt_view_acc" CssClass="form-control" Text='<%# Eval("u0_acc") %>' Enabled="true" runat="server"></asp:TextBox>
                                                   
<%--                                                    <asp:RequiredFieldValidator ID="Requir23edFiewldValidator1" ValidationGroup="SaveView" runat="server" Display="None"
                                                        ControlToValidate="txt_view_acc" Font-Size="11"
                                                        ErrorMessage="Plese input acc" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requir23edFiewldValidator1" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RetxwtUnit" runat="server" ValidationGroup="SaveView" Display="None"
                                                        ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                        ControlToValidate="txt_view_acc"
                                                        ValidationExpression="^[0-9]{1,10}$" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxwtUnit" Width="160" />
                                                --%>
                                                </div>

                                                <asp:Label ID="Label18" CssClass="col-sm-3 control-label" runat="server" Text="เลขที่ PO : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txt_view_po" CssClass="form-control" Text='<%# Eval("u0_po") %>' Enabled="true" runat="server"></asp:TextBox>
                                                    
<%--                                                    <asp:RequiredFieldValidator ID="Req2uired3FieldValidator1" ValidationGroup="SaveView" runat="server" Display="None"
                                                        ControlToValidate="txt_view_po" Font-Size="11"
                                                        ErrorMessage="Plese input po" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req2uired3FieldValidator1" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularEwxpressionValidator1" runat="server" ValidationGroup="SaveView" Display="None"
                                                        ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                        ControlToValidate="txt_view_po"
                                                        ValidationExpression="^[0-9]{1,10}$" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularEwxpressionValidator1" Width="160" />
                                               --%>
                                                    </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทประกัน : " />
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lbl_view_Insurance" runat="server" Visible="false" Text='<%# Eval("m0_iridx") %>'></asp:Label>
                                                    <asp:DropDownList ID="ddl_view_Insurance" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>

                                                <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ยี่ห้อ : " />
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lbl_view_band" runat="server" Visible="false" Text='<%# Eval("m0_bidx") %>'></asp:Label>
                                                    <asp:DropDownList ID="ddl_view_band" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFi54e4ldValidator1" ValidationGroup="SaveView" runat="server" Display="None"
                                                        ControlToValidate="ddl_view_band" Font-Size="11"
                                                        ErrorMessage="เลือกยี่ห้อ ...."
                                                        ValidationExpression="เลือกยี่ห้อ ...." InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFi54e4ldValidator1" Width="160" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label2" class="col-sm-2 control-label" runat="server" Text="วันที่ซื้อ : " />
                                                <div class="col-sm-3">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="txt_view_buy" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" Text='<%# Eval("u0_purchase") %>'></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="RqDocC32wreate" ValidationGroup="SaveView" runat="server" Display="None"
                                                        ControlToValidate="txt_view_buy" Font-Size="11"
                                                        ErrorMessage="Plese date buy" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqDocC32wreate" Width="160" />
                                                </div>
                                                <asp:Label ID="Label3" CssClass="col-sm-3 control-label" runat="server" Text="วันที่หมดประกัน : " />
                                                <div class="col-sm-3">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="txt_view_expire" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" Text='<%# Eval("u0_expDate") %>'></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                    <asp:RequiredFieldValidator ID="Required43FieldValidator1" ValidationGroup="SaveView" runat="server" Display="None"
                                                        ControlToValidate="txt_view_expire" Font-Size="11"
                                                        ErrorMessage="Plese date buy" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required43FieldValidator1" Width="160" />
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lbl_view_org" runat="server" Visible="false" Text='<%# Eval("m0_orgidx") %>'></asp:Label>
                                                    <asp:DropDownList ID="ddl_view_org" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="Required22FieldValidator1" ValidationGroup="SaveView" runat="server" Display="None"
                                                        ControlToValidate="ddl_view_org" Font-Size="11"
                                                        ErrorMessage="เลือกองค์กร ...."
                                                        ValidationExpression="เลือกองค์กร ...." InitialValue="-1" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required22FieldValidator1" Width="160" />
                                                </div>

                                                <asp:Panel ID="Box_view_dept" runat="server">
                                                    <asp:Label ID="Label5" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                                    <div class="col-sm-3">
                                                        <asp:Label ID="lbl_view_dept" runat="server" Visible="false" Text='<%# Eval("R0_depidx") %>'></asp:Label>
                                                        <asp:DropDownList ID="ddl_view_dept" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                            <asp:ListItem Text="เลือกฝ่าย ......." Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="Re333quiredFieldValidator1" ValidationGroup="SaveView" runat="server" Display="None"
                                                            ControlToValidate="ddl_view_dept" Font-Size="11"
                                                            ErrorMessage="เลือกฝ่าย ...."
                                                            ValidationExpression="เลือกฝ่าย ...." InitialValue="-1" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re333quiredFieldValidator1" Width="160" />
                                                    </div>
                                                </asp:Panel>
                                            </div>

                                            <asp:Panel ID="Box_view_sec" runat="server">
                                                <div class="form-group">

                                                    <asp:Label ID="Label61" CssClass="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                                    <div class="col-sm-3">
                                                        <asp:Label ID="lbl_view_sec" runat="server" Visible="false" Text='<%# Eval("R0_secidx") %>'></asp:Label>
                                                        <asp:DropDownList ID="ddl_view_sec" runat="server" CssClass="form-control">
                                                            <asp:ListItem Text="เลือกแผนก ......." Value="0"></asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="Requ77iredFieldValidator1" ValidationGroup="SaveView" runat="server" Display="None"
                                                            ControlToValidate="ddl_view_sec" Font-Size="11"
                                                            ErrorMessage="เลือกแผนก ...."
                                                            ValidationExpression="เลือกแผนก ...." InitialValue="-1" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requ77iredFieldValidator1" Width="160" />
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <div class="form-group">
                                                <asp:Label ID="Label6" class="col-sm-2 control-label" runat="server" Text="Serial number : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txt_view_serial" CssClass="form-control" Text='<%# Eval("u0_serial") %>' Enabled="true" runat="server"></asp:TextBox>
                                                </div>
                                                <asp:Panel ID="boxcomputer" runat="server" Visible="false">
                                                    <asp:Label ID="Label14" CssClass="col-sm-3 control-label" runat="server" Text="ระดับ : " />
                                                    <div class="col-sm-3">
                                                        <asp:Label ID="lbl_view_level" runat="server" Visible="false" Text='<%# Eval("m0_lvidx") %>'></asp:Label>
                                                        <asp:DropDownList ID="ddl_view_level" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>
                                                </asp:Panel>
                                                <asp:Panel ID="boxslot" runat="server" Visible="false">
                                                    <asp:Label ID="Label57" CssClass="col-sm-3 control-label" runat="server" Text="Slot : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddl_add_slot" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("u0_slotram") %>'>
                                                            <asp:ListItem Value="0">กรุณาเลือก slot....</asp:ListItem>
                                                            <asp:ListItem Value="1">1</asp:ListItem>
                                                            <asp:ListItem Value="2">2</asp:ListItem>
                                                            <asp:ListItem Value="3">3</asp:ListItem>
                                                            <asp:ListItem Value="4">4</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </asp:Panel>


                                            </div>

                                            <asp:Panel ID="Box_viewdetail" runat="server" Visible="true">
                                                <div class="form-group">
                                                    <asp:Label ID="Label84" CssClass="col-sm-2 control-label" runat="server" Text="CPU : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddl_viewdetail_cpu" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>

                                                    <asp:Label ID="Label86" CssClass="col-sm-3 control-label" runat="server" Text="RAM : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddl_viewdetail_ram" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label85" CssClass="col-sm-2 control-label" runat="server" Text="HDD : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddl_viewdetail_hdd" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>

                                                    <asp:Label ID="Label87" CssClass="col-sm-3 control-label" runat="server" Text="VGA : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddl_viewdetail_vga" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label88" CssClass="col-sm-2 control-label" runat="server" Text="ETC : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddl_viewdetail_etc" runat="server" CssClass="form-control">
                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                            <div class="form-group">
                                                <asp:Label ID="Label69" CssClass="col-sm-2 control-label" runat="server" Text="สถานะอุปกรณ์ : " />
                                                <div class="col-sm-3">
                                                    <asp:Label ID="lbl_view_statusdevice" runat="server" Visible="false" Text='<%# Eval("m0_sidx") %>'></asp:Label>
                                                    <asp:DropDownList ID="ddl_view_status" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="Requi32redFieldVali2dator1" ValidationGroup="SaveView" runat="server" Display="None"
                                                        ControlToValidate="ddl_view_status" Font-Size="11"
                                                        ErrorMessage="เลือกสถานะ ...."
                                                        ValidationExpression="เลือกสถานะ ...." InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender25" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requi32redFieldVali2dator1" Width="160" />
                                                </div>

                                                <asp:Panel ID="boxtype" runat="server" Visible="true">
                                                    <asp:Label ID="Label83" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทอุปกรณ์ย่อย : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddl_typedetail_view" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("u0_typedeviceetc_idx") %>'>
                                                            <asp:ListItem Value="0">กรุณาเลือก ประเภทอุปกรณ์ย่อย....</asp:ListItem>
                                                            <asp:ListItem Value="1">Notebook</asp:ListItem>
                                                            <asp:ListItem Value="2">Pc</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                </asp:Panel>
                                            </div>


                                            <div class="form-group">
                                                <hr />
                                                <div class="col-sm-12">
                                                    <asp:LinkButton ID="btnViewdata" CssClass="btn btn-success" runat="server" CommandName="btnView_Device" OnCommand="btnCommand" ValidationGroup="SaveView" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"> บันทึกข้อมูล</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnSaveCutoff" CssClass="btn btn-success" runat="server" CommandName="btnSave_Cutoff" OnCommand="btnCommand" Visible="false" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"> บันทึกข้อมูล</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnViewadddata_show" CssClass="btn btn-primary" runat="server" CommandName="btnView_addDevice_show" OnCommand="btnCommand" title="Save"><i class="fa fa-plus"> เพิ่ม/ผูกอุปกรณ์</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnViewadddata_hide" CssClass="btn btn-warning" runat="server" Visible="false" CommandName="btnView_addDevice_hide" OnCommand="btnCommand" title="Save"><i class="fa fa-minus"> เพิ่ม/ผูกอุปกรณ์</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnvieweditdevice_show" CssClass="btn btn-primary" runat="server" CommandName="btnView_editDevice_show" OnCommand="btnCommand" title="Save"><i class="fa fa-plus"> เปลี่ยนแปลงอุปกรณ์</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnvieweditdevice_hide" CssClass="btn btn-warning" runat="server" Visible="false" CommandName="btnView_editDevice_hide" OnCommand="btnCommand" title="Save"><i class="fa fa-minus"> เปลี่ยนแปลงอุปกรณ์</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnView_software_show" CssClass="btn btn-primary" runat="server" CommandName="btnView_editDevice_software_show" OnCommand="btnCommand" title="Save"><i class="fa fa-plus"> เพิ่ม/แก้ไข Software</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnView_software_hide" CssClass="btn btn-warning" runat="server" Visible="false" CommandName="btnView_editDevice_software_hide" OnCommand="btnCommand" title="Save"><i class="fa fa-minus"> เพิ่ม/แก้ไข Software</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnView_cut_show" CssClass="btn btn-primary" runat="server" CommandName="btnView_editDevice_cut_show" Visible="false" OnCommand="btnCommand" title="Save"><i class="fa fa-plus"> ตัดชำรุด</i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnView_cut_hide" CssClass="btn btn-warning" runat="server" Visible="false" CommandName="btnView_editDevice_cut_hide" OnCommand="btnCommand" title="Save"><i class="fa fa-minus"> ตัดชำรุด</i></asp:LinkButton>
                                                    <asp:LinkButton ID="ViewCancel" CssClass="btn btn-default" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" CommandArgument="1" title="Cancel"><i class="fa fa-times"> ยกเลิก</i></asp:LinkButton>
                                                </div>
                                            </div>

                                            <asp:Panel ID="boxviewApprove_all" runat="server" Visible="false">
                                                <hr />
                                                <asp:FormView ID="FvViewApproveDevice" runat="server" OnDataBound="FvDetail_DataBound">
                                                    <InsertItemTemplate>
                                                        <div class="panel-heading">
                                                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; Approve เปลี่ยนแปลงอุปกรณ์</strong></h3>
                                                        </div>
                                                        <asp:Panel ID="boxviewapprove_cpu" runat="server" Visible="true">
                                                        </asp:Panel>
                                                        <div class="form-group">
                                                            <asp:Label ID="Labe2l13" CssClass="col-sm-2 control-label" runat="server" Text="ผลอนุมัติ : " />
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddl_view_Approve" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">กรุณาเลือกผลการอนุมัติ....</asp:ListItem>
                                                                    <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                                                    <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:Label ID="Labe2l30" CssClass="col-sm-2 control-label" runat="server" Text="Comment : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="tbDocRemark" runat="server" CssClass="form-control" TextMode="multiline" Width="563pt" Rows="5"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-4 col-sm-offset-8">
                                                                <asp:LinkButton ID="btnApprove_new_device_" CssClass="btn btn-success" runat="server" CommandName="btnView_Approve_Device_transfer" OnCommand="btnCommand" ValidationGroup="SaveView" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"> บันทึกข้อมูล</i></asp:LinkButton>
                                                                <asp:LinkButton ID="ViewCancel" CssClass="btn btn-default" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" CommandArgument="1" title="Cancel"><i class="fa fa-times"> ยกเลิก</i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:FormView>
                                            </asp:Panel>
                                        </asp:Panel>

                                        <asp:Panel ID="box_view_addsoftware" runat="server" Visible="false">
                                            <hr />
                                            <div class="form-group">
                                                <div class="col-lg-12">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <div class="panel-heading">
                                                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; Software License </strong></h3>
                                                            </div>
                                                            <asp:GridView ID="Gv_detail_software" runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-5"
                                                                HeaderStyle-CssClass="primary"
                                                                HeaderStyle-Height="40px"
                                                                AllowPaging="true"
                                                                DataKeyNames="u0_swidx"
                                                                PageSize="10"
                                                                OnPageIndexChanging="Master_PageIndexChanging"
                                                                OnRowDataBound="Master_RowDataBound">
                                                                <HeaderStyle CssClass="success" />

                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="CheckBox_licence" runat="server" />
                                                                            <asp:Label ID="lbl_software_idx" runat="server" Text='<%# Eval("software_idx") %>' Visible="false"></asp:Label>
                                                                            <%--<asp:Label ID="lbl_name_software" runat="server" Text='<%# Eval("name_software") %>'></asp:Label>--%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="software_name" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_name_software" runat="server" Text='<%# Eval("name_software") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ประเภท Software" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_type_name" runat="server" Text='<%# Eval("status_name") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-5 col-lg-offset-1">
                                                        <div class="form-group">
                                                            <div class="panel-heading">
                                                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; Software Free</strong></h3>
                                                            </div>
                                                            <asp:GridView ID="GvSoftware_free" runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-5"
                                                                HeaderStyle-CssClass="primary"
                                                                HeaderStyle-Height="40px"
                                                                AllowPaging="true"
                                                                DataKeyNames="software_idx"
                                                                PageSize="10"
                                                                OnPageIndexChanging="Master_PageIndexChanging"
                                                                OnRowDataBound="Master_RowDataBound">
                                                                <HeaderStyle CssClass="success" />

                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>

                                                                    <asp:TemplateField>
                                                                        <%--<HeaderTemplate>
                                                                            <asp:CheckBox ID="chkAll" runat="server" onclick="CheckAll(this);" />
                                                                        </HeaderTemplate>--%>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="CheckBox_free" runat="server" />
                                                                            <asp:Label ID="lbl_u0_software_idx" runat="server" Text='<%# Eval("software_idx")%>' Visible="false"></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="software_name" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_name_software" runat="server" Text='<%# Eval("name_software") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_status_name" runat="server" Text='<%# Eval("status_name") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <hr />
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <div class="panel-heading">
                                                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; Add Software License </strong></h3>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label49" CssClass="col-sm-4 control-label" runat="server" Text="ประเภท Software :" />
                                                                <div class="col-sm-6">
                                                                    <asp:DropDownList ID="ddl_type_software" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Value="0">กรุณาเลือกประเภท Software....</asp:ListItem>
                                                                        <asp:ListItem Value="1">Software(รายปี)</asp:ListItem>
                                                                        <asp:ListItem Value="2">Software(ซื้อขาด)</asp:ListItem>
                                                                        <asp:ListItem Value="3">Software(ฟรี)</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <asp:Label ID="Label50" CssClass="col-sm-4 control-label" runat="server" Text="บริษัทผู้ผลิต :" />
                                                                <div class="col-sm-6">
                                                                    <asp:DropDownList ID="ddl_org_software" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                        <asp:ListItem Value="0">กรุณาเลือกบริษัทผู้ผลิต....</asp:ListItem>
                                                                        <asp:ListItem Value="1">Software(รายปี)</asp:ListItem>
                                                                        <asp:ListItem Value="2">Software(ซื้อขาด)</asp:ListItem>
                                                                        <asp:ListItem Value="3">Software(ฟรี)</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <asp:Label ID="Label51" CssClass="control-label col-sm-4" runat="server" Text="รหัส Software" />
                                                                <div class="col-sm-6">
                                                                    <asp:TextBox ID="txt_softwarecode" CssClass="form-control" runat="server" placeholder="Ex. PC00000001"></asp:TextBox>
                                                                </div>
                                                            </div>

                                                            <div class="form-group">
                                                                <div class="col-sm-1 col-sm-offset-3">
                                                                    <asp:Button ID="Button1" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btnsearch_software" OnCommand="btnCommand" />
                                                                </div>
                                                            </div>
                                                            <hr />
                                                            <asp:GridView ID="GvDeviceDatabase_software" runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-6"
                                                                HeaderStyle-CssClass="primary"
                                                                HeaderStyle-Height="40px"
                                                                AllowPaging="true"
                                                                DataKeyNames="software_idx"
                                                                PageSize="10"
                                                                OnPageIndexChanging="Master_PageIndexChanging">
                                                                <HeaderStyle CssClass="success" />

                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="เลือก" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="LinkButton10" CssClass="btn btn-success btn-xs" data-toggle="tooltip" title="Select" runat="server" CommandName="btnselect_software" OnCommand="btnCommand"
                                                                                CommandArgument='<%# Eval("software_idx") + ";" + Eval("name_software") + ";" + Eval("type_name")%>'><i class="fa fa-plus-square"></i></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="software_name" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_name_software" runat="server" Text='<%# Eval("name_software") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="ประเภท Software" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_type_name" runat="server" Text='<%# Eval("type_name") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-5 col-lg-offset-1">
                                                        <div class="form-group">
                                                            <asp:GridView ID="GvTemp_sotfware" runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-5"
                                                                HeaderStyle-CssClass="primary"
                                                                HeaderStyle-Height="40px"
                                                                AllowPaging="true"
                                                                DataKeyNames="software_idx"
                                                                PageSize="17"
                                                                OnPageIndexChanging="Master_PageIndexChanging">

                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <%#(Container.DataItemIndex + 1) %>
                                                                            <asp:Label ID="lbl_software_idx" runat="server" Visible="false" Text='<%# Eval("software_idx") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="software_name" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_name_software" runat="server" Text='<%# Eval("name_software") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="ประเภท Software" ItemStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lbl_type_name" runat="server" Text='<%# Eval("type_name") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                                        <ItemTemplate>
                                                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-xs" runat="server" CommandName="CmdDel_sw" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" CommandArgument='<%# Eval("software_idx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                                        </ItemTemplate>
                                                                        <EditItemTemplate />
                                                                        <FooterTemplate />
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel ID="box_view_adddevice" runat="server" Visible="false">
                                            <div class="form-group">
                                                <div class="col-lg-12">
                                                    <hr />
                                                </div>
                                                <div class="panel-heading">
                                                    <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; เพิ่มอุปกรณ์ให้ PC,NoteBook</strong></h3>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group" runat="server">
                                                        <div class="row">
                                                            <div class="form-horizontal">
                                                                <div class="form-group">
                                                                    <asp:Label ID="Label65" CssClass="col-sm-4 control-label" runat="server" Text="ประเภทอุปกรณ์ :" />
                                                                    <div class="col-sm-6">
                                                                        <asp:DropDownList ID="ddl_view_search_typedevice" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                            <asp:ListItem Value="0">กรุณาเลือกประเภทอุปกรณ์....</asp:ListItem>
                                                                            <asp:ListItem Value="6">CPU</asp:ListItem>
                                                                            <asp:ListItem Value="7">RAM</asp:ListItem>
                                                                            <asp:ListItem Value="8">HDD</asp:ListItem>
                                                                            <asp:ListItem Value="9">VGA</asp:ListItem>
                                                                            <asp:ListItem Value="10">OTHER</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-horizontal">
                                                                <div class="form-group">
                                                                    <asp:Label ID="Label23" CssClass="col-sm-4 control-label" runat="server" Text="สถานะอุปกรณ์" />
                                                                    <div class="col-sm-6">
                                                                        <asp:DropDownList ID="ddl_view_search_statusdevice" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-horizontal">
                                                                <div class="form-group">
                                                                    <asp:Label ID="Label66" CssClass="control-label col-sm-4" runat="server" Text="รหัสอุปกรณ์" />
                                                                    <div class="col-sm-6">
                                                                        <asp:TextBox ID="txt_view_search_u0_code" CssClass="form-control" runat="server" placeholder="Ex. PC00000001"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-horizontal">
                                                                <div class="form-group">
                                                                    <asp:Label ID="Label67" runat="server" Text="Asset Code" CssClass="col-sm-4 control-label"></asp:Label>
                                                                    <div class="col-sm-6">
                                                                        <asp:TextBox ID="txt_view_search_assetcode" CssClass="form-control" runat="server" placeholder="Ex. 00000000"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-horizontal">
                                                                <div class="form-group">
                                                                    <asp:Label ID="Label68" runat="server" Text="SerialNumber" CssClass="col-sm-4 control-label"></asp:Label>
                                                                    <div class="col-sm-6">
                                                                        <asp:TextBox ID="txt_view_search_seria" CssClass="form-control" runat="server" placeholder="Ex. 00000000"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-horizontal">
                                                            <div class="form-group">
                                                                <div class="col-sm-1 col-sm-offset-3">
                                                                    <asp:Button ID="btnsearch" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btnsearchtranfer" OnCommand="btnCommand" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <hr />
                                                    </div>
                                                    <asp:GridView ID="GvDeviceDatabase" runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-5"
                                                        HeaderStyle-CssClass="primary"
                                                        HeaderStyle-Height="40px"
                                                        AllowPaging="true"
                                                        DataKeyNames="u0_didx"
                                                        PageSize="10"
                                                        OnPageIndexChanging="Master_PageIndexChanging">
                                                        <HeaderStyle CssClass="success" />

                                                        <PagerStyle CssClass="pageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="เลือก" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnSelect" CssClass="btn btn-success btn-xs" data-toggle="tooltip" title="Select" runat="server" CommandName="btnselect" OnCommand="btnCommand"
                                                                        CommandArgument='<%# Eval("u0_didx") + ";" + Eval("u0_code") + ";" + Eval("name_m0_typedevice") + ";" + Eval("m0_tdidx") + ";" +  Eval("u0_acc") + ";" + Eval("u0_serial")%>'><i class="fa fa-plus-square"></i></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="รหัสอุปกรณ์" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbfm_empcode" runat="server" Text='<%# Eval("u0_code") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ประเภทอุปกรณ์" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbfm_name" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <div class="col-lg-6">
                                                    <asp:GridView ID="GvEmpTemp" runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-5"
                                                        HeaderStyle-CssClass="primary"
                                                        HeaderStyle-Height="40px"
                                                        AllowPaging="true"
                                                        DataKeyNames="u0_didx"
                                                        PageSize="17"
                                                        OnPageIndexChanging="Master_PageIndexChanging">

                                                        <PagerStyle CssClass="pageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="#" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <%#(Container.DataItemIndex + 1) %>
                                                                    <asp:Label ID="lbfm_dempcode" runat="server" Visible="false" Text='<%# Eval("u0_didx") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="รหัสอุปกรณ์" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbfm_empcode" runat="server" Text='<%# Eval("u0_code") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ประเภทอุปกรณ์" ItemStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbfm_name" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-xs" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" CommandArgument='<%# Eval("u0_didx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                                </ItemTemplate>

                                                                <EditItemTemplate />
                                                                <FooterTemplate />
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </asp:Panel>

                                        <asp:Panel ID="box_edit" runat="server" Visible="false">
                                            <%--รายละเอียดก่อน Approve--%>
                                            <hr />
                                            <div class="form-group">
                                                <div class="col-sm-6" runat="server">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียดอุปกรณ์ - ปัจจุบัน</strong></h3>
                                                    </div>
                                                    <asp:GridView ID="GvDetail_CPU"
                                                        DataKeyNames="u0_didx"
                                                        runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover"
                                                        HeaderStyle-CssClass="table_headCenter"
                                                        HeaderStyle-Height="40px"
                                                        AllowPaging="True"
                                                        PageSize="5"
                                                        BorderStyle="None"
                                                        CellSpacing="2"
                                                        OnRowDataBound="Master_RowDataBound"
                                                        OnRowEditing="Master_RowEditing"
                                                        OnRowUpdating="Master_RowUpdating"
                                                        OnRowCancelingEdit="Master_RowCancelingEdit"
                                                        OnPageIndexChanging="Master_PageIndexChanging">

                                                        <PagerStyle CssClass="PageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="#">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <%# (Container.DataItemIndex +1) %>
                                                                        <asp:Label ID="lblu0_didx_tranfer" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียด CPU ปัจจุบัน</strong></h3>
                                                                    </div>
                                                                    <hr />
                                                                    <div class="form-group">
                                                                        <asp:Label ID="Label27" class="col-sm-2 control-label" runat="server" Text="รหัส CPU : " />
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="TextBox1" CssClass="form-control" Text='<%# Eval("u0_code") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <asp:Label ID="Label16" class="col-sm-2 control-label" runat="server" Text="ยี่ห้อ cpu : " />
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txt_view_bandcpdu" CssClass="form-control" Text='<%# Eval("name_m0_band") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                        <asp:Label ID="Label17" CssClass="col-sm-3 control-label" runat="server" Text="รุ่น cpu : " />
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txt_view_seriecpdu" CssClass="form-control" Text='<%# Eval("detail_m0_band") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <div class="col-lg-offset-10">
                                                                                <div class="control-label" style="text-align: center;">
                                                                                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Back" CssClass="btn btn-danger" CommandName="Cancel" ValidationGroup="SaveUpdate" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="รหัส">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="lblu0_code_tranfer" runat="server" Text='<%# Eval("u0_code")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EditItemTemplate />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ประเภท">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="Labele69" runat="server" Text='<%# Eval("name_m0_typedevice")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EditItemTemplate />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="สถานะ">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="Label54" runat="server" Text='<%# Eval("name_doc_status")%>'></asp:Label>
                                                                        <asp:Label ID="lbl_doc_status_cpu" runat="server" Text='<%# Eval("doc_status")%>' Visible="false"></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EditItemTemplate />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="จัดการ">
                                                                <ItemTemplate>
                                                                    <%--<asp:LinkButton ID="LinkButton32" runat="server" Text="View" CssClass="btn btn-info" CommandName="Edit"><i class="fa fa-file-text-o"></i></asp:LinkButton>--%>
                                                                    <asp:LinkButton ID="btn_selecr_tranfer_cpu" runat="server" Text="Tranfer" CssClass="btn btn-success" CommandName="btn_SelectTranfer_device" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_didx") %>'><i class="fa fa-sign-in"></i></asp:LinkButton>
                                                                    <%--<asp:LinkButton ID="btndeleteGv_cpu" runat="server" CommandName="DeleteGv" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_didx") %>' Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('คุณต้องการถอดอุปกรณ์?')"><i class="fa fa-save"></i></asp:LinkButton>--%>
                                                                </ItemTemplate>
                                                                <EditItemTemplate />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <div class="col-sm-6" runat="server">
                                                    <asp:FormView ID="FvView_Detail_cpu" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                                        <ItemTemplate>
                                                            <%--<div class="panel-heading">
                                                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียดอุปกรณ์ - ปัจจุบัน</strong></h3>
                                                            </div>--%>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label27" class="col-sm-3 control-label" runat="server" Text="รหัส CPU : " />
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="lbl_f" CssClass="form-control" Text='<%# Eval("u0_code") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                    <asp:TextBox ID="lbl_u0_didx_old" CssClass="form-control" Text='<%# Eval("u0_didx") %>' runat="server" Visible="false"></asp:TextBox>
                                                                    <asp:Label ID="lbl_didx_tranfer_cpu" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label16" class="col-sm-3 control-label" runat="server" Text="ยี่ห้อ cpu : " />
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_view_bandcpdu" CssClass="form-control" Text='<%# Eval("name_m0_band") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label17" CssClass="col-sm-3 control-label" runat="server" Text="รุ่น cpu : " />
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_view_seriecpdu" CssClass="form-control" Text='<%# Eval("detail_m0_band") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <hr />
                                                        </ItemTemplate>
                                                    </asp:FormView>
                                                    <asp:FormView ID="FvViewTranferDevice_cpu" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                                        <InsertItemTemplate>
                                                            <div class="panel-heading">
                                                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียดอุปกรณ์ - ใหม่</strong></h3>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label23" CssClass="col-sm-3 control-label" runat="server" Text="สถานะอุปกรณ์ :" />
                                                                <div class="col-sm-5">
                                                                    <asp:DropDownList ID="ddl_Edit_search_statusdevice" runat="server" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label66" CssClass="control-label col-sm-3" runat="server" Text="รหัสอุปกรณ์ :" />
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_edit_search_u0_code" CssClass="form-control" runat="server" placeholder="Ex. PC00000001"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label67" runat="server" Text="Asset Code :" CssClass="col-sm-3 control-label"></asp:Label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_edit_search_assetcode" CssClass="form-control" runat="server" placeholder="Ex. 00000000"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label68" runat="server" Text="SerialNumber :" CssClass="col-sm-3 control-label"></asp:Label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_edit_search_seria" CssClass="form-control" runat="server" placeholder="Ex. 00000000"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-lg-offset-6">
                                                                    <div class="control-label" style="text-align: center;">
                                                                        <asp:Button ID="btnsearch_tranfer_cpu" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btnsearchtranfer_cpu" OnCommand="btnCommand" />
                                                                        <asp:Button ID="btnclearsearch" class="btn btn-primary" runat="server" Text="ยกเลิก" data-original-title="search" data-toggle="tooltip" CommandName="Cancel_cpu" OnCommand="btnCommand" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </InsertItemTemplate>
                                                    </asp:FormView>
                                                    <asp:GridView ID="GvDevice_Detail_Search_cpu"
                                                        runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                                        HeaderStyle-CssClass="table_headCenter"
                                                        HeaderStyle-Height="40px"
                                                        AllowPaging="True"
                                                        PageSize="15"
                                                        DataKeyNames="u0_didx"
                                                        OnPageIndexChanging="Master_PageIndexChanging"
                                                        OnRowDataBound="Master_RowDataBound">

                                                        <PagerStyle CssClass="PageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                                        </EmptyDataTemplate>

                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbDeviceIDX" runat="server" Text='<%# Eval("u0_code") %>' />
                                                                    <asp:Label ID="lbl_u0_didx_old" CssClass="form-control" Text='<%# Eval("u0_didx") %>' runat="server" Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAsseta" runat="server" Text='<%# Eval("u0_acc") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Type" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lb_m0_tdidx" runat="server" Text='<%# Eval("m0_tdidx") %>' Visible="false" />
                                                                    <asp:Label ID="lblAssetaq" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Band" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAssetsaq" runat="server" Text='<%# Eval("name_m0_band") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnsearch_tr_cpu" runat="server" Text="บันทึก" ValidationGroup="SaveUpdate" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="btnsave_tranfer_cpu" CommandArgument='<%# Eval("u0_didx") %>' OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <asp:Panel ID="boxviewapprove_cpu" runat="server" Visible="true">
                                                    <div class="col-sm-6" runat="server">
                                                        <div class="panel-heading">
                                                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียด - ใหม่</strong></h3>
                                                        </div>
                                                        <asp:GridView ID="GvDetail_CPU_Tranfer"
                                                            DataKeyNames="u0_didx"
                                                            runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover"
                                                            HeaderStyle-CssClass="table_headCenter"
                                                            HeaderStyle-Height="40px"
                                                            AllowPaging="True"
                                                            PageSize="20"
                                                            BorderStyle="None"
                                                            CellSpacing="2">

                                                            <PagerStyle CssClass="PageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="#">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:CheckBox ID="CheckBox1" runat="server" />
                                                                            <asp:Label ID="lbl_u0_didx_approve" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="รหัส">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Labe2l69" runat="server" Text='<%# Eval("u0_code")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ประเภท">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Labedfl69" runat="server" Text='<%# Eval("name_m0_typedevice")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="อ้างอิง">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Label56" runat="server" Text='<%# Eval("u0_referent_code")%>'></asp:Label>
                                                                            <asp:Label ID="lbl_u0_referent_approve" runat="server" Text='<%# Eval("u0_referent")%>' Visible="false"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </asp:Panel>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-6" runat="server">
                                                    <asp:GridView ID="GvDetail_RAM"
                                                        DataKeyNames="u0_didx"
                                                        runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover"
                                                        HeaderStyle-CssClass="table_headCenter"
                                                        HeaderStyle-Height="40px"
                                                        AllowPaging="True"
                                                        PageSize="5"
                                                        BorderStyle="None"
                                                        CellSpacing="2"
                                                        OnRowDataBound="Master_RowDataBound"
                                                        OnRowEditing="Master_RowEditing"
                                                        OnRowUpdating="Master_RowUpdating"
                                                        OnRowCancelingEdit="Master_RowCancelingEdit"
                                                        OnPageIndexChanging="Master_PageIndexChanging">

                                                        <PagerStyle CssClass="PageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="#">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <%# (Container.DataItemIndex +1) %>
                                                                        <asp:Label ID="lblu0_didx_tranfer_ram" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>

                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียด Ram ปัจจุบัน</strong></h3>
                                                                    </div>
                                                                    <hr />
                                                                    <div class="form-group">
                                                                        <asp:Label ID="Label220" class="col-sm-2 control-label" runat="server" Text="ยี่ห้อ ram : " />
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txt_view_edit_bandram" CssClass="form-control" Text='<%# Eval("band_ram") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                        </div>

                                                                        <asp:Label ID="Label321" CssClass="col-sm-3 control-label" runat="server" Text="ประเภท ram : " />
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txt_view_edit_typeram" CssClass="form-control" Text='<%# Eval("type_ram") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <asp:Label ID="Labe2l22" class="col-sm-2 control-label" runat="server" Text="ความจุ ram : " />
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txt_view_sizeram" CssClass="form-control" Text='<%# Eval("capacity_ram") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <div class="col-lg-offset-10">
                                                                                <div class="control-label" style="text-align: center;">
                                                                                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Back" CssClass="btn btn-danger" CommandName="Cancel" ValidationGroup="SaveUpdate" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="รหัส">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="Labelw69" runat="server" Text='<%# Eval("u0_code")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ประเภท">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="Label369" runat="server" Text='<%# Eval("name_m0_typedevice")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="สถานะ">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="Label55" runat="server" Text='<%# Eval("name_doc_status")%>'></asp:Label>
                                                                        <asp:Label ID="lbl_doc_status_ram" runat="server" Text='<%# Eval("doc_status")%>' Visible="false"></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EditItemTemplate />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="จัดการ">
                                                                <ItemTemplate>
                                                                    <%--<asp:LinkButton ID="LinkButton3" runat="server" ValidationGroup="fromgroup" Text="View" CssClass="btn btn-info" CommandName="Edit"><i class="fa fa-file-text-o"></i></asp:LinkButton>--%>
                                                                    <asp:LinkButton ID="btn_selecr_tranfer_ram" runat="server" ValidationGroup="fromgroup" Text="Tranfer" CssClass="btn btn-success" CommandName="btn_SelectTranfer_ram" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_didx") %>'><i class="fa fa-sign-in"></i></asp:LinkButton>
                                                                    <%--<asp:LinkButton ID="btndeleteGv_ram" runat="server" CommandName="DeleteGv" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_didx") %>' Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('คุณต้องการถอดอุปกรณ์?')"><i class="fa fa-save"></i></asp:LinkButton>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <div class="col-sm-6" runat="server">
                                                    <asp:FormView ID="FvView_Detail_ram" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label27" class="col-sm-3 control-label" runat="server" Text="รหัส ram : " />
                                                                <div class="col-sm-5">
                                                                    <asp:Label ID="lbl_didx_tranfer_ram" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                    <asp:TextBox ID="TextBox1" CssClass="form-control" Text='<%# Eval("u0_code") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label16" class="col-sm-3 control-label" runat="server" Text="ยี่ห้อ ram : " />
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_view_bandcpdu" CssClass="form-control" Text='<%# Eval("name_m0_band") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label17" CssClass="col-sm-3 control-label" runat="server" Text="รุ่น ram : " />
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_view_seriecpdu" CssClass="form-control" Text='<%# Eval("detail_m0_band") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <hr />
                                                        </ItemTemplate>
                                                    </asp:FormView>
                                                    <asp:FormView ID="FvViewTranferDevice_ram" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                                        <InsertItemTemplate>
                                                            <%--<div class="panel-heading">
                                                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียด ram ต้องการเปลี่ยน</strong></h3>
                                                            </div>--%>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label23" CssClass="col-sm-3 control-label" runat="server" Text="สถานะอุปกรณ์ :" />
                                                                <div class="col-sm-5">
                                                                    <asp:DropDownList ID="ddl_Edit_search_statusdevice_ram" runat="server" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label66" CssClass="control-label col-sm-3" runat="server" Text="รหัสอุปกรณ์ :" />
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_edit_search_u0_code_ram" CssClass="form-control" runat="server" placeholder="Ex. PC00000001"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label67" runat="server" Text="Asset Code :" CssClass="col-sm-3 control-label"></asp:Label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_edit_search_assetcode_ram" CssClass="form-control" runat="server" placeholder="Ex. 00000000"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label68" runat="server" Text="SerialNumber :" CssClass="col-sm-3 control-label"></asp:Label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_edit_search_seria_ram" CssClass="form-control" runat="server" placeholder="Ex. 00000000"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-lg-offset-6">
                                                                    <div class="control-label" style="text-align: center;">
                                                                        <asp:Button ID="btnsearch_tranfer_ram" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btnsearchtranfer_ram" OnCommand="btnCommand" />
                                                                        <asp:Button ID="btnclearsearch_ram" class="btn btn-primary" runat="server" Text="ยกเลิก" data-original-title="search" data-toggle="tooltip" CommandName="Cancel_ram" OnCommand="btnCommand" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </InsertItemTemplate>
                                                    </asp:FormView>
                                                    <asp:GridView ID="GvDevice_Detail_Search_ram"
                                                        runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                                        HeaderStyle-CssClass="table_headCenter"
                                                        HeaderStyle-Height="40px"
                                                        AllowPaging="True"
                                                        PageSize="15"
                                                        DataKeyNames="u0_didx"
                                                        OnPageIndexChanging="Master_PageIndexChanging"
                                                        OnRowDataBound="Master_RowDataBound">

                                                        <PagerStyle CssClass="PageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                                        </EmptyDataTemplate>

                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbDeviceIDX" runat="server" Text='<%# Eval("u0_code") %>' />
                                                                    <asp:Label ID="lbl_u0_didx_old" CssClass="form-control" Text='<%# Eval("u0_didx") %>' runat="server" Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAsseta" runat="server" Text='<%# Eval("u0_acc") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Type" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lb_m0_tdidx" runat="server" Text='<%# Eval("m0_tdidx") %>' Visible="false" />
                                                                    <asp:Label ID="lblAssetaq" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Band" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAssetsaq" runat="server" Text='<%# Eval("name_m0_band") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnsearch_tr_ram" runat="server" Text="บันทึก" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="btnsave_tranfer_ram" CommandArgument='<%# Eval("u0_didx") %>' OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <asp:Panel ID="boxviewapprove_ram" runat="server" Visible="true">
                                                    <div class="col-sm-6" runat="server">
                                                        <asp:GridView ID="GvDetail_RAM_Tranfer"
                                                            DataKeyNames="u0_didx"
                                                            runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover"
                                                            HeaderStyle-CssClass="table_headCenter"
                                                            HeaderStyle-Height="40px"
                                                            AllowPaging="True"
                                                            PageSize="5"
                                                            BorderStyle="None"
                                                            CellSpacing="2">

                                                            <PagerStyle CssClass="PageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="#">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:CheckBox ID="CheckBox1" runat="server" />
                                                                            <asp:Label ID="lbl_u0_didx_approve" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="รหัส">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Labe2l69" runat="server" Text='<%# Eval("u0_code")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ประเภท">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Lasabefl69" runat="server" Text='<%# Eval("name_m0_typedevice")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="อ้างอิง">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Label56" runat="server" Text='<%# Eval("u0_referent_code")%>'></asp:Label>
                                                                            <asp:Label ID="lbl_u0_referent_approve" runat="server" Text='<%# Eval("u0_referent")%>' Visible="false"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </asp:Panel>
                                            </div>

                                            <div class="form-group">

                                                <div class="col-sm-6" runat="server">
                                                    <%--<div class="panel-heading">
                                                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียด HDD - เก่า</strong></h3>
                                                    </div>--%>
                                                    <asp:GridView ID="GvDetail_HDD"
                                                        DataKeyNames="u0_didx"
                                                        runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover"
                                                        HeaderStyle-CssClass="table_headCenter"
                                                        HeaderStyle-Height="40px"
                                                        AllowPaging="True"
                                                        PageSize="5"
                                                        BorderStyle="None"
                                                        CellSpacing="2"
                                                        OnRowDataBound="Master_RowDataBound"
                                                        OnRowEditing="Master_RowEditing"
                                                        OnRowUpdating="Master_RowUpdating"
                                                        OnRowCancelingEdit="Master_RowCancelingEdit"
                                                        OnPageIndexChanging="Master_PageIndexChanging">

                                                        <PagerStyle CssClass="PageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="#">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <%# (Container.DataItemIndex +1) %>
                                                                        <asp:Label ID="lblu0_didx_tranfer_hdd" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>

                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียด Hdd ปัจจุบัน</strong></h3>
                                                                    </div>
                                                                    <hr />
                                                                    <div class="form-group">
                                                                        <asp:Label ID="Label24" class="col-sm-2 control-label" runat="server" Text="ยี่ห้อ hdd : " />
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txt_view_bandhdd" CssClass="form-control" runat="server" Text='<%# Eval("band_hdd") %>' Enabled="false"></asp:TextBox>
                                                                        </div>

                                                                        <asp:Label ID="Label25" CssClass="col-sm-3 control-label" runat="server" Text="ประเภท hdd : " />
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txt_view_typehdd" CssClass="form-control" runat="server" Text='<%# Eval("type_hdd") %>' Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <asp:Label ID="Label26" class="col-sm-2 control-label" runat="server" Text="ความจุ hdd : " />
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txt_view_sizehdd" CssClass="form-control" runat="server" Text='<%# Eval("capacity_hdd") %>' Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <div class="col-lg-offset-10">
                                                                                <div class="control-label" style="text-align: center;">
                                                                                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Back" CssClass="btn btn-danger" CommandName="Cancel" ValidationGroup="SaveUpdate" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="รหัส">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="Label629" runat="server" Text='<%# Eval("u0_code")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ประเภท">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="Labeel69" runat="server" Text='<%# Eval("name_m0_typedevice")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="สถานะ">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="Label55" runat="server" Text='<%# Eval("name_doc_status")%>'></asp:Label>
                                                                        <asp:Label ID="lbl_doc_status_hdd" runat="server" Text='<%# Eval("doc_status")%>' Visible="false"></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EditItemTemplate />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="จัดการ">
                                                                <ItemTemplate>
                                                                    <%--<asp:LinkButton ID="LinkButton3" runat="server" ValidationGroup="fromgroup" Text="View" CssClass="btn btn-info" CommandName="Edit"><i class="fa fa-file-text-o"></i></asp:LinkButton>--%>
                                                                    <asp:LinkButton ID="btn_selecr_tranfer_hdd" runat="server" ValidationGroup="fromgroup" Text="Tranfer" CssClass="btn btn-success" CommandName="btn_SelectTranfer_hdd" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_didx") %>'><i class="fa fa-sign-in"></i></asp:LinkButton>
                                                                    <%--<asp:LinkButton ID="btndeleteGv_hdd" runat="server" CommandName="DeleteGv" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_didx") %>' Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('คุณต้องการถอดอุปกรณ์?')"><i class="fa fa-save"></i></asp:LinkButton>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <div class="col-sm-6" runat="server">
                                                    <asp:FormView ID="FvView_Detail_hdd" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label27" class="col-sm-3 control-label" runat="server" Text="รหัส hdd : " />
                                                                <div class="col-sm-5">
                                                                    <asp:Label ID="lbl_didx_tranfer_hdd" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                    <asp:TextBox ID="TextBox1" CssClass="form-control" Text='<%# Eval("u0_code") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label16" class="col-sm-3 control-label" runat="server" Text="ยี่ห้อ hdd : " />
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_view_bandcpdu" CssClass="form-control" Text='<%# Eval("name_m0_band") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label17" CssClass="col-sm-3 control-label" runat="server" Text="รุ่น hdd : " />
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_view_seriecpdu" CssClass="form-control" Text='<%# Eval("detail_m0_band") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <hr />
                                                        </ItemTemplate>
                                                    </asp:FormView>
                                                    <asp:FormView ID="FvViewTranferDevice_hdd" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                                        <InsertItemTemplate>
                                                            <%--<div class="panel-heading">
                                                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียด hdd ต้องการเปลี่ยน</strong></h3>
                                                            </div>--%>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label23" CssClass="col-sm-3 control-label" runat="server" Text="สถานะอุปกรณ์ :" />
                                                                <div class="col-sm-5">
                                                                    <asp:DropDownList ID="ddl_Edit_search_statusdevice_hdd" runat="server" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label66" CssClass="control-label col-sm-3" runat="server" Text="รหัสอุปกรณ์ :" />
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_edit_search_u0_code_hdd" CssClass="form-control" runat="server" placeholder="Ex. PC00000001"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label67" runat="server" Text="Asset Code :" CssClass="col-sm-3 control-label"></asp:Label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_edit_search_assetcode_hdd" CssClass="form-control" runat="server" placeholder="Ex. 00000000"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label68" runat="server" Text="SerialNumber :" CssClass="col-sm-3 control-label"></asp:Label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_edit_search_seria_hdd" CssClass="form-control" runat="server" placeholder="Ex. 00000000"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-lg-offset-6">
                                                                    <div class="control-label" style="text-align: center;">
                                                                        <asp:Button ID="btnsearch_tranfer_ram" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btnsearchtranfer_hdd" OnCommand="btnCommand" />
                                                                        <asp:Button ID="btnclearsearch_ram" class="btn btn-primary" runat="server" Text="ยกเลิก" data-original-title="search" data-toggle="tooltip" CommandName="Cancel_hdd" OnCommand="btnCommand" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </InsertItemTemplate>
                                                    </asp:FormView>
                                                    <asp:GridView ID="GvDevice_Detail_Search_hdd"
                                                        runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                                        HeaderStyle-CssClass="table_headCenter"
                                                        HeaderStyle-Height="40px"
                                                        AllowPaging="True"
                                                        PageSize="15"
                                                        DataKeyNames="u0_didx"
                                                        OnPageIndexChanging="Master_PageIndexChanging"
                                                        OnRowDataBound="Master_RowDataBound">

                                                        <PagerStyle CssClass="PageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                                        </EmptyDataTemplate>

                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbDeviceIDX" runat="server" Text='<%# Eval("u0_code") %>' />
                                                                    <asp:Label ID="lbl_u0_didx_old" CssClass="form-control" Text='<%# Eval("u0_didx") %>' runat="server" Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAsseta" runat="server" Text='<%# Eval("u0_acc") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Type" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lb_m0_tdidx" runat="server" Text='<%# Eval("m0_tdidx") %>' Visible="false" />
                                                                    <asp:Label ID="lblAssetaq" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Band" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAssetsaq" runat="server" Text='<%# Eval("name_m0_band") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnsearch_tr_hdd" runat="server" Text="บันทึก" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="btnsave_tranfer_hdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("u0_didx") %>' />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <asp:Panel ID="boxviewapprove_hdd" runat="server" Visible="true">
                                                    <div class="col-sm-6" runat="server">
                                                        <%--<div class="panel-heading">
                                                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียด HDD - ใหม่</strong></h3>
                                                        </div>--%>
                                                        <asp:GridView ID="GvDetail_HDD_Tranfer"
                                                            DataKeyNames="u0_didx"
                                                            runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover"
                                                            HeaderStyle-CssClass="table_headCenter"
                                                            HeaderStyle-Height="40px"
                                                            AllowPaging="True"
                                                            PageSize="5"
                                                            BorderStyle="None"
                                                            CellSpacing="2">

                                                            <PagerStyle CssClass="PageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="#">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:CheckBox ID="CheckBox1" runat="server" />
                                                                            <asp:Label ID="lbl_u0_didx_approve" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="รหัส">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Labe2l69" runat="server" Text='<%# Eval("u0_code")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ประเภท">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Lasbel69" runat="server" Text='<%# Eval("name_m0_typedevice")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="อ้างอิง">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Label56" runat="server" Text='<%# Eval("u0_referent_code")%>'></asp:Label>
                                                                            <asp:Label ID="lbl_u0_referent_approve" runat="server" Text='<%# Eval("u0_referent")%>' Visible="false"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                </asp:TemplateField>
                                                                <%--<asp:TemplateField HeaderText="จัดการ">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:LinkButton ID="btndeleteGvPosition" runat="server" CommandName="DeleteGvDetailPosition" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_didx") %>' Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('Do you want delete this item?')" />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                </asp:TemplateField>--%>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </asp:Panel>
                                            </div>

                                            <div class="form-group">

                                                <div class="col-sm-6" runat="server">
                                                    <asp:GridView ID="GvDetail_VGA"
                                                        DataKeyNames="u0_didx"
                                                        runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover"
                                                        HeaderStyle-CssClass="table_headCenter"
                                                        HeaderStyle-Height="40px"
                                                        AllowPaging="True"
                                                        PageSize="5"
                                                        BorderStyle="None"
                                                        CellSpacing="2"
                                                        OnRowDataBound="Master_RowDataBound"
                                                        OnRowEditing="Master_RowEditing"
                                                        OnRowUpdating="Master_RowUpdating"
                                                        OnRowCancelingEdit="Master_RowCancelingEdit"
                                                        OnPageIndexChanging="Master_PageIndexChanging">

                                                        <PagerStyle CssClass="PageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="#">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <%# (Container.DataItemIndex +1) %>
                                                                        <asp:Label ID="lblu0_didx_tranfer_vga" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EditItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียด Vga ปัจจุบัน</strong></h3>
                                                                    </div>
                                                                    <hr />
                                                                    <div class="form-group">
                                                                        <asp:Label ID="Label28" class="col-sm-2 control-label" runat="server" Text="รุ่น vga : " />
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txt_view_serievga" CssClass="form-control" MaxLength="10" runat="server" Text='<%# Eval("type_vga") %>' Enabled="false"></asp:TextBox>
                                                                        </div>

                                                                        <asp:Label ID="Label29" CssClass="col-sm-3 control-label" runat="server" Text="ประเภท vga : " />
                                                                        <div class="col-sm-3">
                                                                            <asp:TextBox ID="txt_view_typevga" CssClass="form-control" MaxLength="10" runat="server" Text='<%# Eval("generation_vga") %>' Enabled="false"></asp:TextBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12">
                                                                        <div class="form-group">
                                                                            <div class="col-lg-offset-10">
                                                                                <div class="control-label" style="text-align: center;">
                                                                                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Back" CssClass="btn btn-danger" CommandName="Cancel" ValidationGroup="SaveUpdate" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </EditItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="รหัส">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="Label269" runat="server" Text='<%# Eval("u0_code")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ประเภท">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="Labe2l69" runat="server" Text='<%# Eval("name_m0_typedevice")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="สถานะ">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="Label55" runat="server" Text='<%# Eval("name_doc_status")%>'></asp:Label>
                                                                        <asp:Label ID="lbl_doc_status_vga" runat="server" Text='<%# Eval("doc_status")%>' Visible="false"></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EditItemTemplate />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="จัดการ">
                                                                <ItemTemplate>
                                                                    <%--<asp:LinkButton ID="LinkButton3" runat="server" ValidationGroup="fromgroup" Text="Back" CssClass="btn btn-info" CommandName="Edit"><i class="fa fa-file-text-o"></i></asp:LinkButton>--%>
                                                                    <asp:LinkButton ID="btn_selecr_tranfer_vga" runat="server" ValidationGroup="fromgroup" Text="Tranfer" CssClass="btn btn-success" CommandName="btn_SelectTranfer_vga" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_didx") %>'><i class="fa fa-sign-in"></i></asp:LinkButton>
                                                                    <%--<asp:LinkButton ID="btndeleteGv_vga" runat="server" CommandName="DeleteGv" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_didx") %>' Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('คุณต้องการถอดอุปกรณ์?')"><i class="fa fa-save"></i></asp:LinkButton>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <div class="col-sm-6" runat="server">
                                                    <asp:FormView ID="FvView_Detail_vga" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label27" class="col-sm-3 control-label" runat="server" Text="รุ่น vga : " />
                                                                <div class="col-sm-5">
                                                                    <asp:Label ID="lbl_didx_tranfer_vga" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                    <asp:TextBox ID="TextBox1" CssClass="form-control" Text='<%# Eval("type_vga") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label16" class="col-sm-3 control-label" runat="server" Text="ประเภท vga : " />
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_view_bandcpdu" CssClass="form-control" Text='<%# Eval("generation_vga") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <hr />
                                                        </ItemTemplate>
                                                    </asp:FormView>
                                                    <asp:FormView ID="FvViewTranferDevice_vga" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                                        <InsertItemTemplate>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label23" CssClass="col-sm-3 control-label" runat="server" Text="สถานะอุปกรณ์ :" />
                                                                <div class="col-sm-5">
                                                                    <asp:DropDownList ID="ddl_Edit_search_statusdevice_vga" runat="server" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label66" CssClass="control-label col-sm-3" runat="server" Text="รหัสอุปกรณ์ :" />
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_edit_search_u0_code_vga" CssClass="form-control" runat="server" placeholder="Ex. PC00000001"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label67" runat="server" Text="Asset Code :" CssClass="col-sm-3 control-label"></asp:Label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_edit_search_assetcode_vga" CssClass="form-control" runat="server" placeholder="Ex. 00000000"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label68" runat="server" Text="SerialNumber :" CssClass="col-sm-3 control-label"></asp:Label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_edit_search_seria_vga" CssClass="form-control" runat="server" placeholder="Ex. 00000000"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-lg-offset-6">
                                                                    <div class="control-label" style="text-align: center;">
                                                                        <asp:Button ID="btnsearch_tranfer_ram" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btnsearchtranfer_vga" OnCommand="btnCommand" />
                                                                        <asp:Button ID="btnclearsearch_ram" class="btn btn-primary" runat="server" Text="ยกเลิก" data-original-title="search" data-toggle="tooltip" CommandName="Cancel_vga" OnCommand="btnCommand" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </InsertItemTemplate>
                                                    </asp:FormView>
                                                    <asp:GridView ID="GvDevice_Detail_Search_vga"
                                                        runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                                        HeaderStyle-CssClass="table_headCenter"
                                                        HeaderStyle-Height="40px"
                                                        AllowPaging="True"
                                                        PageSize="15"
                                                        DataKeyNames="u0_didx"
                                                        OnPageIndexChanging="Master_PageIndexChanging"
                                                        OnRowDataBound="Master_RowDataBound">

                                                        <PagerStyle CssClass="PageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                                        </EmptyDataTemplate>

                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbDeviceIDX" runat="server" Text='<%# Eval("u0_code") %>' />
                                                                    <asp:Label ID="lbl_u0_didx_old" CssClass="form-control" Text='<%# Eval("u0_didx") %>' runat="server" Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAsseta" runat="server" Text='<%# Eval("u0_acc") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Type" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lb_m0_tdidx" runat="server" Text='<%# Eval("m0_tdidx") %>' Visible="false" />
                                                                    <asp:Label ID="lblAssetaq" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="รุ่น" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAssetsaq" runat="server" Text='<%# Eval("type_vga") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnsearch_tr_vga" runat="server" Text="บันทึก" ValidationGroup="SaveUpdate" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="btnsave_tranfer_vga" CommandArgument='<%# Eval("u0_didx") %>' OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <asp:Panel ID="boxviewapprove_vga" runat="server" Visible="true">
                                                    <div class="col-sm-6" runat="server">
                                                        <asp:GridView ID="GvDetail_VGA_Tranfer"
                                                            DataKeyNames="u0_didx"
                                                            runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover"
                                                            HeaderStyle-CssClass="table_headCenter"
                                                            HeaderStyle-Height="40px"
                                                            AllowPaging="True"
                                                            PageSize="5"
                                                            BorderStyle="None"
                                                            CellSpacing="2">

                                                            <PagerStyle CssClass="PageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="#">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:CheckBox ID="CheckBox1" runat="server" />
                                                                            <asp:Label ID="lbl_u0_didx_approve" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="รหัส">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Labe2l69" runat="server" Text='<%# Eval("u0_code")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ประเภท">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Labfwel69" runat="server" Text='<%# Eval("name_m0_typedevice")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="อ้างอิง">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Label56" runat="server" Text='<%# Eval("u0_referent_code")%>'></asp:Label>
                                                                            <asp:Label ID="lbl_u0_referent_approve" runat="server" Text='<%# Eval("u0_referent")%>' Visible="false"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </asp:Panel>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-6" runat="server">
                                                    <asp:GridView ID="GvDetail_ETC"
                                                        DataKeyNames="u0_didx"
                                                        runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover"
                                                        HeaderStyle-CssClass="table_headCenter"
                                                        HeaderStyle-Height="40px"
                                                        AllowPaging="True"
                                                        PageSize="5"
                                                        BorderStyle="None"
                                                        CellSpacing="2"
                                                        OnRowDataBound="Master_RowDataBound"
                                                        OnRowEditing="Master_RowEditing"
                                                        OnRowUpdating="Master_RowUpdating"
                                                        OnRowCancelingEdit="Master_RowCancelingEdit"
                                                        OnPageIndexChanging="Master_PageIndexChanging">

                                                        <PagerStyle CssClass="PageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">Data Cannot Be Found</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="#">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <%# (Container.DataItemIndex +1) %>
                                                                        <asp:Label ID="lblu0_didx_tranfer_etc" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="รหัส">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="Labelw69" runat="server" Text='<%# Eval("u0_code")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ประเภท">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="Label369" runat="server" Text='<%# Eval("name_m0_typedevice")%>'></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="สถานะ">
                                                                <ItemTemplate>
                                                                    <div class="panel-heading">
                                                                        <asp:Label ID="Label55" runat="server" Text='<%# Eval("name_doc_status")%>'></asp:Label>
                                                                        <asp:Label ID="lbl_doc_status_etc" runat="server" Text='<%# Eval("doc_status")%>' Visible="false"></asp:Label>
                                                                    </div>
                                                                </ItemTemplate>
                                                                <EditItemTemplate />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="จัดการ">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btn_selecr_tranfer_etc" runat="server" ValidationGroup="fromgroup" Text="Tranfer" CssClass="btn btn-success" CommandName="btn_SelectTranfer_etc" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_didx") %>'><i class="fa fa-sign-in"></i></asp:LinkButton>
                                                                    <%--<asp:LinkButton ID="btndeleteGv_etc" runat="server" CommandName="DeleteGv" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_didx") %>' Text="Delete" CssClass="btn btn-danger" OnClientClick="return confirm('คุณต้องการถอดอุปกรณ์?')"><i class="fa fa-save"></i></asp:LinkButton>--%>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <div class="col-sm-6" runat="server">
                                                    <asp:FormView ID="FvView_Detail_etc" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                                        <ItemTemplate>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label27" class="col-sm-3 control-label" runat="server" Text="รหัส etc : " />
                                                                <div class="col-sm-5">
                                                                    <asp:Label ID="lbl_didx_tranfer_etc" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                    <asp:TextBox ID="TextBox1" CssClass="form-control" Text='<%# Eval("u0_code") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label16" class="col-sm-3 control-label" runat="server" Text="ยี่ห้อ etc : " />
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_view_bandetc" CssClass="form-control" Text='<%# Eval("name_m0_band") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label17" CssClass="col-sm-3 control-label" runat="server" Text="รุ่น etc : " />
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_view_serieetc" CssClass="form-control" Text='<%# Eval("detail_m0_band") %>' runat="server" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <hr />
                                                        </ItemTemplate>
                                                    </asp:FormView>
                                                    <asp:FormView ID="FvViewTranferDevice_etc" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                                        <InsertItemTemplate>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label23" CssClass="col-sm-3 control-label" runat="server" Text="สถานะอุปกรณ์ :" />
                                                                <div class="col-sm-5">
                                                                    <asp:DropDownList ID="ddl_Edit_search_statusdevice_etc" runat="server" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label66" CssClass="control-label col-sm-3" runat="server" Text="รหัสอุปกรณ์ :" />
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_edit_search_u0_code_etc" CssClass="form-control" runat="server" placeholder="Ex. PC00000001"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label67" runat="server" Text="Asset Code :" CssClass="col-sm-3 control-label"></asp:Label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_edit_search_assetcode_etc" CssClass="form-control" runat="server" placeholder="Ex. 00000000"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:Label ID="Label68" runat="server" Text="SerialNumber :" CssClass="col-sm-3 control-label"></asp:Label>
                                                                <div class="col-sm-5">
                                                                    <asp:TextBox ID="txt_edit_search_seria_etc" CssClass="form-control" runat="server" placeholder="Ex. 00000000"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-lg-offset-6">
                                                                    <div class="control-label" style="text-align: center;">
                                                                        <asp:Button ID="btnsearch_tranfer_etc" class="btn btn-primary" ValidationGroup="Search" runat="server" Text="ค้นหา" data-original-title="search" data-toggle="tooltip" CommandName="btnsearchtranfer_etc" OnCommand="btnCommand" />
                                                                        <asp:Button ID="btnclearsearch_etc" class="btn btn-primary" runat="server" Text="ยกเลิก" data-original-title="search" data-toggle="tooltip" CommandName="Cancel_etc" OnCommand="btnCommand" />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </InsertItemTemplate>
                                                    </asp:FormView>
                                                    <asp:GridView ID="GvDevice_Detail_Search_etc"
                                                        runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                                        HeaderStyle-CssClass="table_headCenter"
                                                        HeaderStyle-Height="40px"
                                                        AllowPaging="True"
                                                        PageSize="15"
                                                        DataKeyNames="u0_didx"
                                                        OnPageIndexChanging="Master_PageIndexChanging"
                                                        OnRowDataBound="Master_RowDataBound">

                                                        <PagerStyle CssClass="PageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                                        </EmptyDataTemplate>

                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbDeviceIDX" runat="server" Text='<%# Eval("u0_code") %>' />
                                                                    <asp:Label ID="lbl_u0_didx_old" CssClass="form-control" Text='<%# Eval("u0_didx") %>' runat="server" Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAsseta" runat="server" Text='<%# Eval("u0_acc") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Type" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lb_m0_tdidx" runat="server" Text='<%# Eval("m0_tdidx") %>' Visible="false" />
                                                                    <asp:Label ID="lblAss3etaq" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Band" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblAssetsaq" runat="server" Text='<%# Eval("name_m0_band") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="15%">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnsearch_tr_etc" runat="server" Text="บันทึก" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="btnsave_tranfer_etc" CommandArgument='<%# Eval("u0_didx") %>' OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                                <asp:Panel ID="boxviewapprove_etc" runat="server" Visible="true">
                                                    <div class="col-sm-6" runat="server">
                                                        <asp:GridView ID="GvDetail_ETC_Tranfer"
                                                            DataKeyNames="u0_didx"
                                                            runat="server"
                                                            AutoGenerateColumns="false"
                                                            CssClass="table table-striped table-bordered table-hover"
                                                            HeaderStyle-CssClass="table_headCenter"
                                                            HeaderStyle-Height="40px"
                                                            AllowPaging="True"
                                                            PageSize="5"
                                                            BorderStyle="None"
                                                            CellSpacing="2">

                                                            <PagerStyle CssClass="PageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="#">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:CheckBox ID="CheckBox1" runat="server" />
                                                                            <asp:Label ID="lbl_u0_didx_approve" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="รหัส">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Labe2l69" runat="server" Text='<%# Eval("u0_code")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ประเภท">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Lasabefl69" runat="server" Text='<%# Eval("name_m0_typedevice")%>'></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="อ้างอิง">
                                                                    <ItemTemplate>
                                                                        <div class="panel-heading">
                                                                            <asp:Label ID="Label56" runat="server" Text='<%# Eval("u0_referent_code")%>'></asp:Label>
                                                                            <asp:Label ID="lbl_u0_referent_approve" runat="server" Text='<%# Eval("u0_referent")%>' Visible="false"></asp:Label>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <EditItemTemplate />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </asp:Panel>

                                            </div>



                                        </asp:Panel>

                                        <asp:Panel ID="box_cutoff_device" runat="server" Visible="false">
                                            <hr />
                                            <div class="form-group">
                                                <div class="col-lg-12">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ตัดชำรุดอุปกรณ์ </strong></h3>
                                                    </div>

                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <div class="col-sm-10">
                                                                <label class="radio-inline">
                                                                    <asp:RadioButtonList ID="Rdtype_cutoff" runat="server" AutoPostBack="true" OnSelectedIndexChanged="Cutoff_SelectedIndexChanged">
                                                                        <asp:ListItem Value="3" Selected="True">ทั้งเครื่อง</asp:ListItem>
                                                                        <asp:ListItem Value="6">เฉพาะอุปกรณ์</asp:ListItem>
                                                                    </asp:RadioButtonList>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-6">
                                                        <div class="form-group">
                                                            <asp:Panel ID="box_cutoff_detail" runat="server" Visible="false">
                                                                <asp:GridView ID="Gv_cutoff_detail" runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                                                    HeaderStyle-CssClass="primary"
                                                                    HeaderStyle-Height="40px"
                                                                    AllowPaging="true"
                                                                    DataKeyNames="u0_didx"
                                                                    PageSize="20"
                                                                    OnPageIndexChanging="Master_PageIndexChanging">
                                                                    <HeaderStyle CssClass="success" />

                                                                    <PagerStyle CssClass="pageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>
                                                                    <Columns>

                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="CheckBox_u0_didx" runat="server" />
                                                                                <asp:Label ID="lbl_u0_didx_cutoff" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                                <asp:Label ID="lbl_u0_unidx_cutoff" runat="server" Text='<%# Eval("u0_unidx")%>' Visible="false"></asp:Label>
                                                                                <asp:Label ID="lbl_u0_acidx_cutoff" runat="server" Text='<%# Eval("u0_acidx")%>' Visible="false"></asp:Label>
                                                                                <asp:Label ID="lbl_u0_doc_decision_cutoff" runat="server" Text='<%# Eval("u0_doc_decision")%>' Visible="false"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="รหัสอุปกรณ์" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbl_u0code_cutoff" runat="server" Text='<%# Eval("u0_code") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Type Device" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbl_typedevice_cutoff" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="Status Device" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbl_statusidx_cutoff" runat="server" Visible="false" Text='<%# Eval("doc_status") %>'></asp:Label>
                                                                                <asp:Label ID="lbl_statusdevice_cutoff" runat="server" Text='<%# Eval("name_doc_status") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>
                                                                </asp:GridView>
                                                            </asp:Panel>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-12">
                                                        <div class="form-group">
                                                            <asp:Label ID="Labe2l30" CssClass="col-sm-1 control-label" runat="server" Text="Comment: " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="tbDocRemark_cutoff" runat="server" CssClass="form-control" TextMode="multiline" Width="350pt" Rows="5"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </asp:Panel>

                                    </div>
                                </div>
                            </div>
                        </EditItemTemplate>
                    </asp:FormView>
                </asp:Panel>

                <asp:Panel ID="box_log" runat="server" Visible="false">
                    <div class="form-group">
                        <div class="row panel panel-default font_text" runat="server" id="panel_history">
                            <div class="panel panel-info">
                                <div class="panel-heading f-bold">ประวัติข้อมูล Device</div>
                            </div>
                            <div class="panel-body">
                                <asp:Repeater ID="rptLog" runat="server">
                                    <HeaderTemplate>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"><small>วัน / เวลา</small></label>
                                            <label class="col-sm-2 control-label"><small>ผู้ดำเนินการ</small></label>
                                            <label class="col-sm-2 control-label"><small>ดำเนินการ</small></label>
                                            <label class="col-sm-2 control-label"><small>ผลการดำเนินการ</small></label>
                                            <label class="col-sm-2 control-label"><small>อ้างอิง</small></label>
                                            <label class="col-sm-2 control-label"><small>ความคิดเห็น</small></label>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <span><small><%#Eval("u0_createdate")%></small></span>
                                            </div>
                                            <div class="col-sm-2">
                                                <span><small><%# Eval("EmpName") %>(<%# Eval("actor_des") %>)</small></span>
                                            </div>
                                            <div class="col-sm-2">
                                                <span><small><%# Eval("node_name") %></small></span>
                                            </div>
                                            <div class="col-sm-2">
                                                <span><small><%# Eval("status_name") %></small></span>
                                            </div>
                                            <div class="col-sm-2">
                                                <span><small><%# Eval("u0_code") %></small></span>
                                            </div>
                                            <div class="col-sm-2">
                                                <span><small><%# Eval("comment") %></small></span>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                </asp:Panel>

            </div>
        </asp:View>

        <asp:View ID="ViewAdd" runat="server">
            <asp:Label ID="fss" runat="server"></asp:Label>
            <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value="2" />
            <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value="2" />
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; เพิ่มทะเบียนคอมพิวเตอร์</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal" role="form">

                        <asp:FormView ID="FvInsertDevice" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>

                                <div class="form-group">
                                    <asp:Label ID="Label13" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddl_add_typedevice" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" ValidationGroup="SaveAdd">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" ValidationGroup="SaveAdd" runat="server" Display="None"
                                            ControlToValidate="ddl_add_typedevice" Font-Size="11"
                                            ErrorMessage="เลือกประเภทอุปกรณ์ ...."
                                            ValidationExpression="เลือกประเภทอุปกรณ์ ...." InitialValue="0" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExten3der3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator12" Width="160" />
                                    </div>

                                    <asp:Label ID="Label75" CssClass="col-sm-3 control-label" runat="server" Text="รหัสเก่า : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txt_add_code_old" CssClass="form-control" MaxLength="15" runat="server"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label12" class="col-sm-2 control-label" runat="server" Text="เลขที่ Acc : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txt_add_acc" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                        
<%--                                        <asp:RequiredFieldValidator ID="RequiredFiewldValidator1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                            ControlToValidate="txt_add_acc" Font-Size="11"
                                            ErrorMessage="Plese input acc" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFiewldValidator1" Width="160" />
                                        <asp:RegularExpressionValidator ID="RetxtUnit" runat="server" ValidationGroup="SaveAdd" Display="None"
                                            ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                            ControlToValidate="txt_add_acc"
                                            ValidationExpression="^[0-9]{1,10}$" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RetxtUnit" Width="160" />--%>

                                    </div>

                                    <asp:Label ID="Label18" CssClass="col-sm-3 control-label" runat="server" Text="เลขที่ PO : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txt_add_po" CssClass="form-control" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                        
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldVal2idator1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                            ControlToValidate="txt_add_po" Font-Size="11"
                                            ErrorMessage="Plese input po" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldVal2idator1" Width="160" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionVdalidator1" runat="server" ValidationGroup="SaveAdd" Display="None"
                                            ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                            ControlToValidate="txt_add_po"
                                            ValidationExpression="^[0-9]{1,10}$" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionVdalidator1" Width="160" />
                                    --%>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทประกัน : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddl_add_Insurance" runat="server" CssClass="form-control">
                                        </asp:DropDownList>
                                    </div>

                                    <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ยี่ห้อ : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddl_add_band" runat="server" CssClass="form-control" ValidationGroup="SaveAdd">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFie4ldValidator1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                            ControlToValidate="ddl_add_band" Font-Size="11"
                                            ErrorMessage="เลือกยี่ห้อ ...."
                                            ValidationExpression="เลือกยี่ห้อ ...." InitialValue="0" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFie4ldValidator1" Width="160" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label2" class="col-sm-2 control-label" runat="server" Text="วันที่ซื้อ : " />
                                    <div class="col-sm-3">
                                        <div class='input-group date'>
                                            <asp:TextBox ID="txt_add_buy" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            <asp:RequiredFieldValidator ID="RqDocCwreate" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="txt_add_buy" Font-Size="11"
                                                ErrorMessage="Plese date buy" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqDocCwreate" Width="160" />
                                        </div>
                                    </div>
                                    <asp:Label ID="Label3" CssClass="col-sm-3 control-label" runat="server" Text="วันที่หมดประกัน : " />
                                    <div class="col-sm-3">
                                        <div class='input-group date'>
                                            <asp:TextBox ID="txt_add_expire" CssClass="form-control from-date-datepicker" MaxLengh="100%" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                            <asp:RequiredFieldValidator ID="Required2FieldValidator1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="txt_add_expire" Font-Size="11"
                                                ErrorMessage="Plese date expire" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required2FieldValidator1" Width="160" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Lab3el4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddl_add_org" runat="server" AutoPostBack="true" CssClass="form-control" ValidationGroup="SaveAdd" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldVdalidator1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                            ControlToValidate="ddl_add_org" Font-Size="11"
                                            ErrorMessage="เลือกบริษัท ...."
                                            ValidationExpression="เลือกบริษัท ...." InitialValue="-1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldVdalidator1" Width="160" />
                                    </div>

                                    <asp:Label ID="Label5" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddl_add_dept" runat="server" CssClass="form-control" AutoPostBack="true" ValidationGroup="SaveAdd" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            <asp:ListItem Text="เลือกฝ่าย ......." Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFcieldsVsalidator21" ValidationGroup="SaveAdd" runat="server" Display="None"
                                            ControlToValidate="ddl_add_dept" Font-Size="11"
                                            ErrorMessage="เลือกฝ่าย ...."
                                            ValidationExpression="เลือกฝ่าย ...." InitialValue="-1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFcieldsVsalidator21" Width="160" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label61" CssClass="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddl_add_sec" runat="server" CssClass="form-control" ValidationGroup="SaveAdd">
                                            <asp:ListItem Text="เลือกแผนก ......." Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidat3or1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                            ControlToValidate="ddl_add_sec" Font-Size="11"
                                            ErrorMessage="เลือกแผนก ...."
                                            ValidationExpression="เลือกแผนก ...." InitialValue="-1" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidat3or1" Width="160" />
                                    </div>

                                    <asp:Label ID="Label62" CssClass="col-sm-3 control-label" runat="server" Text="สถานะอุปกรณ์ : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddl_add_status" runat="server" CssClass="form-control" ValidationGroup="SaveAdd">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="SaveAdd" runat="server" Display="None"
                                            ControlToValidate="ddl_add_status" Font-Size="11"
                                            ErrorMessage="เลือกสถานะอุปกรณ์ ...."
                                            ValidationExpression="เลือกสถานะอุปกรณ์ ...." InitialValue="0" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label6" class="col-sm-2 control-label" runat="server" Text="Serial number : " />
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txt_add_serial" CssClass="form-control" MaxLength="10" runat="server" ValidationGroup="SaveAdd"></asp:TextBox>
                                        <%--<asp:RequiredFieldValidator ID="RqDowcCreate" ValidationGroup="SaveAdd" runat="server" Display="None"
                                            ControlToValidate="txt_add_serial" Font-Size="11"
                                            ErrorMessage="Plese Serial" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqDowcCreate" Width="160" />--%>
                                    </div>

                                    <asp:Panel ID="boxcomputer" runat="server" Visible="false">
                                        <asp:Label ID="Label14" CssClass="col-sm-3 control-label" runat="server" Text="ระดับ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_add_level" runat="server" CssClass="form-control" ValidationGroup="SaveAdd">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldsValidator1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddl_add_level" Font-Size="11"
                                                ErrorMessage="เลือกระดับ ...."
                                                ValidationExpression="เลือกระดับ ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldsValidator1" Width="160" />
                                        </div>
                                    </asp:Panel>

                                    <asp:Panel ID="boxslot" runat="server" Visible="false">
                                        <asp:Label ID="Label57" CssClass="col-sm-3 control-label" runat="server" Text="Slot : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_add_slot" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือก slot....</asp:ListItem>
                                                <asp:ListItem Value="1">1</asp:ListItem>
                                                <asp:ListItem Value="2">2</asp:ListItem>
                                                <asp:ListItem Value="3">3</asp:ListItem>
                                                <asp:ListItem Value="4">4</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </asp:Panel>
                                </div>

                                <div class="form-group">
                                    <asp:Panel ID="boxtype" runat="server" Visible="false">
                                        <asp:Label ID="Label83" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ย่อย : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_add_type_detail" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="0">กรุณาเลือก ประเภทอุปกรณ์ย่อย....</asp:ListItem>
                                                <asp:ListItem Value="1">Notebook</asp:ListItem>
                                                <asp:ListItem Value="2">Pc</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </asp:Panel>
                                </div>

                                <asp:Panel ID="boxcpu" runat="server" Visible="false">

                                    <div class="form-group">
                                        <asp:Label ID="Label15" CssClass="col-sm-2 control-label" runat="server" Text="cpu : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_add_cpu" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFie2ldValidator1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddl_add_cpu" Font-Size="11"
                                                ErrorMessage="เลือก cpu ...."
                                                ValidationExpression="เลือก cpu ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFie2ldValidator1" Width="160" />
                                        </div>
                                    </div>

                                    <asp:Panel ID="boxpc_note_cpu" runat="server" Visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label16" class="col-sm-2 control-label" runat="server" Text="ยี่ห้อ cpu : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_detail_bandcpu" CssClass="form-control" MaxLength="10" runat="server" Enabled="false"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label17" CssClass="col-sm-3 control-label" runat="server" Text="รุ่น cpu : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_detail_seriecpu" CssClass="form-control" MaxLength="10" runat="server" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </asp:Panel>

                                </asp:Panel>

                                <asp:Panel ID="boxram" runat="server" Visible="false">
                                    <div class="form-group">
                                        <asp:Label ID="Label19" CssClass="col-sm-2 control-label" runat="server" Text="ram : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_add_ram" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFiereldValidator3" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddl_add_ram" Font-Size="11"
                                                ErrorMessage="เลือก ram ...."
                                                ValidationExpression="เลือก ram ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFiereldValidator3" Width="160" />
                                        </div>
                                    </div>

                                    <asp:Panel ID="boxpc_note_ram" runat="server" Visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label20" class="col-sm-2 control-label" runat="server" Text="ยี่ห้อ ram : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_detail_bandram" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label21" CssClass="col-sm-3 control-label" runat="server" Text="ประเภท ram : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_detail_typeram" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label22" class="col-sm-2 control-label" runat="server" Text="ความจุ ram : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_detail_sizeram" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </asp:Panel>

                                <asp:Panel ID="boxhdd" runat="server" Visible="false">
                                    <div class="form-group">
                                        <asp:Label ID="Label23" CssClass="col-sm-2 control-label" runat="server" Text="hdd : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_add_hdd" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldVal2ida3tor1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddl_add_hdd" Font-Size="11"
                                                ErrorMessage="เลือก hdd ...."
                                                ValidationExpression="เลือก hdd ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldVal2ida3tor1" Width="160" />
                                        </div>
                                    </div>

                                    <asp:Panel ID="boxpc_note_hdd" runat="server" Visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label24" class="col-sm-2 control-label" runat="server" Text="ยี่ห้อ hdd : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_detail_bandhdd" CssClass="form-control" MaxLength="10" runat="server" Enabled="false"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label25" CssClass="col-sm-3 control-label" runat="server" Text="ประเภท hdd : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_detail_typehdd" CssClass="form-control" MaxLength="10" runat="server" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label26" class="col-sm-2 control-label" runat="server" Text="ความจุ hdd : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_detail_sizehdd" CssClass="form-control" MaxLength="10" runat="server" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </asp:Panel>

                                <asp:Panel ID="boxvga" runat="server" Visible="false">
                                    <div class="form-group">
                                        <asp:Label ID="Label58" CssClass="col-sm-2 control-label" runat="server" Text="vga : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_add_vga" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFweieldValidator1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddl_add_vga" Font-Size="11"
                                                ErrorMessage="เลือก vga ...."
                                                ValidationExpression="เลือก vga ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFweieldValidator1" Width="160" />
                                        </div>
                                    </div>

                                    <asp:Panel ID="boxpc_note_vga" runat="server" Visible="false">
                                        <div class="form-group">
                                            <asp:Label ID="Label28" class="col-sm-2 control-label" runat="server" Text="รุ่น vga : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_detail_serievga" CssClass="form-control" MaxLength="10" runat="server" Enabled="false"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label29" CssClass="col-sm-3 control-label" runat="server" Text="ประเภท vga : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_detail_typevga" CssClass="form-control" MaxLength="10" runat="server" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                </asp:Panel>

                                <asp:Panel ID="boxmonitor" runat="server" Visible="false">
                                    <div class="form-group">
                                        <asp:Label ID="Labe2l8" CssClass="col-sm-2 control-label" runat="server" Text="รหัส moniter : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_add_monitor" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequirweredFieldValidator1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddl_add_monitor" Font-Size="11"
                                                ErrorMessage="เลือก monitor ...."
                                                ValidationExpression="เลือก monitor ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequirweredFieldValidator1" Width="160" />
                                        </div>
                                    </div>

                                    <asp:Panel ID="boxmoniter_detail" runat="server" Visible="false">

                                        <div class="form-group">
                                            <asp:Label ID="Label46" class="col-sm-2 control-label" runat="server" Text="ยี่ห้อ moniter : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_detail_bandmoniter" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label47" CssClass="col-sm-3 control-label" runat="server" Text="ขนาด moniter : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_detail_sizemoniter" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>

                                    </asp:Panel>
                                </asp:Panel>

                                <asp:Panel ID="boxprinter" runat="server" Visible="false">

                                    <div class="form-group">
                                        <asp:Label ID="Label10" CssClass="col-sm-2 control-label" runat="server" Text="รหัส Printer : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_add_printer" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorww1" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                ControlToValidate="ddl_add_printer" Font-Size="11"
                                                ErrorMessage="เลือก printer ...."
                                                ValidationExpression="เลือก printer ...." InitialValue="0" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidatorww1" Width="160" />
                                        </div>
                                    </div>

                                    <asp:Panel ID="boxprinter_detail" runat="server" Visible="false">

                                        <div class="form-group">
                                            <asp:Label ID="Label8" class="col-sm-2 control-label" runat="server" Text="ยี่ห้อ printer : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_detail_bandprinter" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label11" CssClass="col-sm-3 control-label" runat="server" Text="ประเภท printer : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_detail_typeprinter" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="Label42" class="col-sm-2 control-label" runat="server" Text="ประเภทหมึก : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_detail_typeink" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>
                                            </div>

                                            <asp:Label ID="Label45" CssClass="col-sm-3 control-label" runat="server" Text="รุ่น printer : " />
                                            <div class="col-sm-3">
                                                <asp:TextBox ID="txt_detail_serizeprinter" CssClass="form-control" runat="server" Enabled="false"></asp:TextBox>
                                            </div>
                                        </div>

                                    </asp:Panel>

                                </asp:Panel>

                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-10">
                                        <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="btnAdd_Device" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                        <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                    </div>
                                </div>

                            </InsertItemTemplate>
                        </asp:FormView>

                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="Viewapprovecutoff" runat="server">
            <asp:Label ID="fsd" runat="server"></asp:Label>
            <div class="panel-body">
                <div class="form-horizontal" role="form">

                    <asp:Panel ID="pn_gv_cutoff" runat="server">
                        <asp:UpdatePanel ID="ud_GvCutoff" runat="server">
                            <ContentTemplate>

                                <asp:GridView ID="GvDetail_Cutoff_List"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                    HeaderStyle-CssClass="table_headCenter"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="True"
                                    PageSize="20"
                                    DataKeyNames="u0_didx"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                    </EmptyDataTemplate>

                                    <Columns>

                                        <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <b>
                                                        <asp:Label ID="fsde" runat="server" Text="รหัสอุปกรณ์ : " /></b><asp:Label ID="lbDeviceIDX" runat="server" Text='<%# Eval("u0_code") %>' />
                                                    <asp:Label ID="lblu0_referent_code" runat="server" Text='<%# Eval("u0_referent_code") %>' Visible="false" />
                                                    <asp:Label ID="lbl_R0_depidx" runat="server" Text='<%# Eval("R0_depidx") %>' Visible="false" />
                                                    <asp:Label ID="lbl_R0_secidx" runat="server" Text='<%# Eval("R0_secidx") %>' Visible="false" />
                                                    <asp:Label ID="lbl_HDEmpIDX" runat="server" Text='<%# Eval("HDEmpIDX") %>' Visible="false" />
                                                    <asp:Label ID="lbl_HD_R0_depidx" runat="server" Text='<%# Eval("HD_R0_depidx") %>' Visible="false" />
                                                    <asp:Label ID="lbl_HD_R0_secidx" runat="server" Text='<%# Eval("HD_R0_secidx") %>' Visible="false" />
                                                    <asp:Label ID="lbl_HD_Level" runat="server" Text='<%# Eval("HD_Level") %>' Visible="false" />

                                                    <asp:Label ID="lbl_u0_unidx" runat="server" Text='<%# Eval("u0_unidx") %>' Visible="false" />
                                                    <asp:Label ID="lbl_u0_acidx" runat="server" Text='<%# Eval("u0_acidx") %>' Visible="false" />
                                                    <asp:Label ID="lbl_u0_doc_decision" runat="server" Text='<%# Eval("u0_doc_decision") %>' Visible="false" />
                                                    </p>
                                        <b>
                                            <asp:Label ID="frse" runat="server" Text="ผู้ถือครอง : " /></b><asp:Label ID="lblAsse4ta" runat="server" Text='<%# Eval("FullNameTH_HD") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <b>
                                                        <asp:Label ID="fsse" runat="server" Text="Code : " /></b><asp:Label ID="lblAsscxeta" runat="server" Text='<%# Eval("u0_code_cutoff") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <b>
                                                        <asp:Label ID="fse" runat="server" Text="เลขที่ Acc : " /></b><asp:Label ID="lblAsseta" runat="server" Text='<%# Eval("u0_acc") %>'></asp:Label>
                                                    </p>
                                        <b>
                                            <asp:Label ID="Label53" runat="server" Text="เลขที่ Po : " /></b><asp:Label ID="Label59" runat="server" Text='<%# Eval("u0_po") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Detail Device" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="7%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lb_m0_tdidx" runat="server" Text='<%# Eval("m0_tdidx") %>' Visible="false" />
                                                    <b>
                                                        <asp:Label ID="Label533" runat="server" Text="ประเภทอุปกรณ์ : " /></b><asp:Label ID="lblAssetaq" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                                    </p>
                                        <b>
                                            <asp:Label ID="Label60" runat="server" Text="ยี่ห้อ : " /></b><asp:Label ID="lblAssetsaq" runat="server" Text='<%# Eval("name_m0_band") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status Device" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="8%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_statusdevice_cutoff_app" runat="server" Text='<%# Eval("StatusDoc") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:LinkButton ID="btnView_cutoff" CssClass="btn btn-primary" runat="server" CommandName="btnView_GvDevice_Cutoff" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_didx") %>' data-toggle="tooltip" title="Edit"><i class="fa fa-edit"> รายละเอียด</i></asp:LinkButton>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>


                                <asp:GridView ID="GvDetail_Cutoff_List_one"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                    HeaderStyle-CssClass="table_headCenter"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="True"
                                    PageSize="20"
                                    DataKeyNames="u0_didx"
                                    OnPageIndexChanging="Master_PageIndexChanging"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                    </EmptyDataTemplate>

                                    <Columns>

                                        <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <b>
                                                        <asp:Label ID="fsde_1" runat="server" Text="รหัสอุปกรณ์ : " /></b><asp:Label ID="lbDeviceIDX_1" runat="server" Text='<%# Eval("u0_code") %>' />
                                                    <asp:Label ID="lblu0_referent_code_1" runat="server" Text='<%# Eval("u0_referent_code") %>' Visible="false" />
                                                    <asp:Label ID="lbl_R0_depidx_one" runat="server" Text='<%# Eval("R0_depidx") %>' Visible="false" />
                                                    <asp:Label ID="lbl_R0_secidx_one" runat="server" Text='<%# Eval("R0_secidx") %>' Visible="false" />
                                                    <asp:Label ID="lbl_HDEmpIDX_one" runat="server" Text='<%# Eval("HDEmpIDX") %>' Visible="false" />
                                                    <asp:Label ID="lbl_HD_R0_depidx_one" runat="server" Text='<%# Eval("HD_R0_depidx") %>' Visible="false" />
                                                    <asp:Label ID="lbl_HD_R0_secidx_one" runat="server" Text='<%# Eval("HD_R0_secidx") %>' Visible="false" />
                                                    <asp:Label ID="lbl_HD_Level_one" runat="server" Text='<%# Eval("HD_Level") %>' Visible="false" />

                                                    <asp:Label ID="lbl_u0_unidx_one" runat="server" Text='<%# Eval("u0_unidx") %>' Visible="false" />
                                                    <asp:Label ID="lbl_u0_acidx_one" runat="server" Text='<%# Eval("u0_acidx") %>' Visible="false" />
                                                    <asp:Label ID="lbl_u0_doc_decision_one" runat="server" Text='<%# Eval("u0_doc_decision") %>' Visible="false" />
                                                    </p>
                                                    <b>
                                                        <asp:Label ID="frse_1" runat="server" Text="ผู้ถือครอง : " /></b><asp:Label ID="lblAsse4tea" runat="server" Text='<%# Eval("FullNameTH_HD") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <b>
                                                        <asp:Label ID="fssese" runat="server" Text="Code : " /></b><asp:Label ID="lblAssdcxeta" runat="server" Text='<%# Eval("u0_code_cutoff") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="6%">
                                            <ItemTemplate>
                                                <small>
                                                    <b>
                                                        <asp:Label ID="fsdse" runat="server" Text="เลขที่ Acc : " /></b><asp:Label ID="lblAssfseta" runat="server" Text='<%# Eval("u0_acc") %>'></asp:Label>
                                                    </p>
                                        <b>
                                            <asp:Label ID="Label5sf3" runat="server" Text="เลขที่ Po : " /></b><asp:Label ID="Labefsl59" runat="server" Text='<%# Eval("u0_po") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Detail Device" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="7%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lb_m0_tdidxf_1" runat="server" Text='<%# Eval("m0_tdidx") %>' Visible="false" />
                                                    <b>
                                                        <asp:Label ID="Label5f33" runat="server" Text="ประเภทอุปกรณ์ : " /></b><asp:Label ID="lblAssetaq" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                                    </p>
                                        <b>
                                            <asp:Label ID="Labesl60" runat="server" Text="ยี่ห้อ : " /></b><asp:Label ID="lblAssasetsaq" runat="server" Text='<%# Eval("name_m0_band") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status Device" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="8%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:Label ID="lbl_statusdevice_cutoff_app_1" runat="server" Text='<%# Eval("StatusDoc") %>'></asp:Label>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%">
                                            <ItemTemplate>
                                                <small>
                                                    <asp:LinkButton ID="btnView_cutoff_1" CssClass="btn btn-primary" runat="server" CommandName="btnView_GvDevice_Cutoff" OnCommand="btnCommand" CommandArgument='<%# Eval("u0_didx") %>' data-toggle="tooltip" title="Edit"><i class="fa fa-edit"> รายละเอียด</i></asp:LinkButton>
                                                </small>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>

                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>

                    <asp:Panel ID="pn_fv_cutoff" runat="server" Visible="false">
                        <asp:FormView ID="FvViewDetailDevice_Cutoff" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <ItemTemplate>
                                <div class="panel panel-success class">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; รายละเอียดทะเบียนคอมพิวเตอร์</strong></h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">
                                            <asp:Panel ID="box_edit_detail" runat="server" Visible="true">
                                                <div class="form-group">
                                                    <asp:Label ID="Label1c3" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                                    <div class="col-sm-3">
                                                        <asp:Label ID="lbl_view_u0_didx_c" runat="server" Visible="false" Text='<%# Eval("u0_didx") %>'></asp:Label>
                                                        <asp:Label ID="lbl_view_typedevice_c" runat="server" Visible="false" Text='<%# Eval("m0_tdidx") %>'></asp:Label>
                                                        <asp:TextBox ID="txt_view_typedevice_c" CssClass="form-control" runat="server" Text='<%# Eval("name_m0_typedevice") %>' Enabled="false"></asp:TextBox>
                                                        <asp:Label ID="lbl_HDEmpIDX_c" runat="server" Visible="false" Text='<%# Eval("HDEmpIDX") %>'></asp:Label>
                                                        <asp:Label ID="lbl_m0_orgidx_view_c" runat="server" Visible="false" Text='<%# Eval("m0_orgidx") %>'></asp:Label>
                                                        <asp:Label ID="lbl_R0_depidx_view_c" runat="server" Visible="false" Text='<%# Eval("R0_depidx") %>'></asp:Label>
                                                        <asp:Label ID="lbl_R0_secidx_view_c" runat="server" Visible="false" Text='<%# Eval("R0_secidx") %>'></asp:Label>
                                                        <asp:Label ID="lbl_hfM0NodeIDX_view_c" runat="server" Visible="false" Text='<%# Eval("u0_unidx") %>'></asp:Label>
                                                        <asp:Label ID="lbl_hfM0ActorIDX_view_c" runat="server" Visible="false" Text='<%# Eval("u0_acidx") %>'></asp:Label>
                                                    </div>

                                                    <asp:Label ID="Lab4el70" CssClass="col-sm-3 control-label" runat="server" Text="Code : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txt_view_code_cutoff" CssClass="form-control" Text='<%# Eval("u0_code_cutoff") %>' runat="server" Enabled="false"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label64" CssClass="col-sm-2 control-label" runat="server" Text="รหัสอุปกรณ์ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txt_view_code_c" CssClass="form-control" Text='<%# Eval("u0_code") %>' runat="server" Enabled="false"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <asp:Panel ID="boxview_cpu_c" runat="server" Visible="false">
                                                    <div class="form-group">
                                                        <asp:Label ID="Label16" class="col-sm-2 control-label" runat="server" Text="ยี่ห้อ cpu : " />
                                                        <div class="col-sm-3">
                                                            <asp:TextBox ID="txt_view_bandcpu_c" CssClass="form-control" Text='<%# Eval("name_m0_band") %>' runat="server" Enabled="false"></asp:TextBox>
                                                        </div>

                                                        <asp:Label ID="Label17" CssClass="col-sm-3 control-label" runat="server" Text="รุ่น cpu : " />
                                                        <div class="col-sm-3">
                                                            <asp:TextBox ID="txt_view_seriecpu_c" CssClass="form-control" Text='<%# Eval("detail_m0_band") %>' runat="server" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </asp:Panel>

                                                <asp:Panel ID="boxview_ram_c" runat="server" Visible="false">
                                                    <div class="form-group">
                                                        <asp:Label ID="Labeld20" class="col-sm-2 control-label" runat="server" Text="ยี่ห้อ ram : " />
                                                        <div class="col-sm-3">
                                                            <asp:TextBox ID="txt_view_bandram_c" CssClass="form-control" Text='<%# Eval("band_ram") %>' runat="server" Enabled="false"></asp:TextBox>
                                                        </div>

                                                        <asp:Label ID="Label21" CssClass="col-sm-3 control-label" runat="server" Text="ประเภท ram : " />
                                                        <div class="col-sm-3">
                                                            <asp:TextBox ID="txt_view_typeram_c" CssClass="form-control" Text='<%# Eval("type_ram") %>' runat="server" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:Label ID="Labsel22" class="col-sm-2 control-label" runat="server" Text="ความจุ ram : " />
                                                        <div class="col-sm-3">
                                                            <asp:TextBox ID="txt_view_sizeram_c" CssClass="form-control" Text='<%# Eval("capacity_ram") %>' runat="server" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </asp:Panel>

                                                <asp:Panel ID="boxview_hdd_c" runat="server" Visible="false">
                                                    <div class="form-group">
                                                        <asp:Label ID="Labessl24" class="col-sm-2 control-label" runat="server" Text="ยี่ห้อ hdd : " />
                                                        <div class="col-sm-3">
                                                            <asp:TextBox ID="txt_view_bandhdd_c" CssClass="form-control" runat="server" Text='<%# Eval("band_hdd") %>' Enabled="false"></asp:TextBox>
                                                        </div>

                                                        <asp:Label ID="Ldabel25" CssClass="col-sm-3 control-label" runat="server" Text="ประเภท hdd : " />
                                                        <div class="col-sm-3">
                                                            <asp:TextBox ID="txt_view_typehdd_c" CssClass="form-control" runat="server" Text='<%# Eval("type_hdd") %>' Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:Label ID="Labdel26" class="col-sm-2 control-label" runat="server" Text="ความจุ hdd : " />
                                                        <div class="col-sm-3">
                                                            <asp:TextBox ID="txt_view_sizehdd_c" CssClass="form-control" runat="server" Text='<%# Eval("capacity_hdd") %>' Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </asp:Panel>

                                                <asp:Panel ID="boxview_vga_c" runat="server" Visible="false">
                                                    <div class="form-group">
                                                        <asp:Label ID="Labessdl28" class="col-sm-2 control-label" runat="server" Text="รุ่น vga : " />
                                                        <div class="col-sm-3">
                                                            <asp:TextBox ID="txt_view_serievga_c" CssClass="form-control" MaxLength="10" runat="server" Text='<%# Eval("type_vga") %>' Enabled="false"></asp:TextBox>
                                                        </div>

                                                        <asp:Label ID="Lsabel29" CssClass="col-sm-3 control-label" runat="server" Text="ประเภท vga : " />
                                                        <div class="col-sm-3">
                                                            <asp:TextBox ID="txt_view_typevga_c" CssClass="form-control" MaxLength="10" runat="server" Text='<%# Eval("generation_vga") %>' Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                </asp:Panel>

                                                <div class="form-group">
                                                    <asp:Label ID="Labedl12" class="col-sm-2 control-label" runat="server" Text="เลขที่ Acc : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txt_view_acc_c" CssClass="form-control" Text='<%# Eval("u0_acc") %>' Enabled="false" runat="server"></asp:TextBox>
                                                    </div>

                                                    <asp:Label ID="Label18d" CssClass="col-sm-3 control-label" runat="server" Text="เลขที่ PO : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txt_view_po_c" CssClass="form-control" Text='<%# Eval("u0_po") %>' Enabled="false" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Lasbel7" CssClass="col-sm-2 control-label" runat="server" Text="บริษัทประกัน : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txt_view_insurance_c" CssClass="form-control" Text='<%# Eval("name_m0_insurance") %>' Enabled="false" runat="server"></asp:TextBox>
                                                    </div>

                                                    <asp:Label ID="Lafbel9" CssClass="col-sm-3 control-label" runat="server" Text="ยี่ห้อ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txt_view_band_c" CssClass="form-control" Text='<%# Eval("name_m0_band") %>' Enabled="false" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Labeld2" class="col-sm-2 control-label" runat="server" Text="วันที่ซื้อ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txt_view_buy_c" CssClass="form-control" runat="server" Text='<%# Eval("u0_purchase") %>' Enabled="false"></asp:TextBox>
                                                    </div>
                                                    <asp:Label ID="Labegl3" CssClass="col-sm-3 control-label" runat="server" Text="วันที่หมดประกัน : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txt_view_expire_c" CssClass="form-control" runat="server" Text='<%# Eval("u0_expDate") %>' Enabled="false"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label4t" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txt_view_org_c" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" runat="server"></asp:TextBox>
                                                    </div>

                                                    <asp:Label ID="Labhel5" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txt_view_dept_c" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Lahbel61" CssClass="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txt_view_sec_c" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" runat="server"></asp:TextBox>
                                                    </div>

                                                    <asp:Label ID="Ldabel69" CssClass="col-sm-3 control-label" runat="server" Text="สถานะอุปกรณ์ : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txt_view_status_c" CssClass="form-control" Text='<%# Eval("name_m0_status") %>' Enabled="false" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Labjel6" class="col-sm-2 control-label" runat="server" Text="Serial number : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txt_view_serial_c" CssClass="form-control" Text='<%# Eval("u0_serial") %>' Enabled="false" runat="server"></asp:TextBox>
                                                    </div>
                                                    <asp:Panel ID="boxcomputer_c" runat="server" Visible="false">
                                                        <asp:Label ID="Labefl14" CssClass="col-sm-3 control-label" runat="server" Text="ระดับ : " />
                                                        <div class="col-sm-3">
                                                            <asp:TextBox ID="txt_view_level_c" CssClass="form-control" Text='<%# Eval("name_m0_level") %>' Enabled="false" runat="server"></asp:TextBox>
                                                        </div>
                                                    </asp:Panel>
                                                    <asp:Panel ID="boxslot_c" runat="server" Visible="false">
                                                        <asp:Label ID="Lcabel57" CssClass="col-sm-3 control-label" runat="server" Text="Slot : " />
                                                        <div class="col-sm-3">
                                                            <asp:TextBox ID="txt_add_slot_c" CssClass="form-control" Text='<%# Eval("u0_slotram") %>' Enabled="false" runat="server"></asp:TextBox>
                                                        </div>
                                                    </asp:Panel>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-6">
                                                        <asp:LinkButton ID="btnbackcut" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>

                                                <asp:GridView ID="gvFileCutoff" Visible="true" runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                                    HeaderStyle-CssClass="warning"
                                                    OnRowDataBound="Master_RowDataBound"
                                                    BorderStyle="None"
                                                    CellSpacing="2"
                                                    Font-Size="Small">

                                                    <Columns>
                                                        <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                                            <ItemTemplate>
                                                                <div class="col-lg-10">
                                                                    <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                                </div>
                                                                <div class="col-lg-2">
                                                                    <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                                    <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                                </div>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>

                                            </asp:Panel>
                                        </div>
                                    </div>
                                </div>



                            </ItemTemplate>
                        </asp:FormView>
                    </asp:Panel>


                    <asp:Panel ID="pn_gv_approve_cutoff" runat="server" Visible="false">
                        <div class="col-lg-12">
                            <div class="col-lg-6">
                                <asp:Label ID="Labe2l13" CssClass="col-lg-4 control-label" runat="server" Text="ทั้งเครื่องที่ตัดชำรุด : " />
                                <asp:GridView ID="Gv_Cutoff_Approve" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                    HeaderStyle-CssClass="primary"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    DataKeyNames="u0_didx"
                                    PageSize="10"
                                    OnPageIndexChanging="Master_PageIndexChanging">
                                    <HeaderStyle CssClass="success" />

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox_u0_didx" runat="server" />
                                                <asp:Label ID="lbl_u0_didx_cutoff_approve" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_u0code_cutoff_approve" runat="server" Text='<%# Eval("u0_code") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Type Device" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_typedevice_cutoff_approve" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Referent" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_referent_cutoff_approve" runat="server" Text='<%# Eval("u0_referent_code") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status Device" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_m0_sidx_cutoff" runat="server" Visible="false" Text='<%# Eval("m0_sidx") %>'></asp:Label>
                                                <asp:Label ID="lbl_statusidx_cutoff_approve" runat="server" Visible="false" Text='<%# Eval("doc_status") %>'></asp:Label>
                                                <asp:Label ID="lbl_statusdevice_cutoff_approve" runat="server" Text='<%# Eval("StatusDoc") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="col-lg-6">
                                <asp:Label ID="Label63" CssClass="col-lg-4 control-label" runat="server" Text="เฉพาะอุปกรณ์ที่ตัดชำรุด : " />
                                <asp:GridView ID="Gv_Cutoff_Approve_specific" runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                    HeaderStyle-CssClass="primary"
                                    HeaderStyle-Height="40px"
                                    AllowPaging="true"
                                    DataKeyNames="u0_didx"
                                    PageSize="10"
                                    OnPageIndexChanging="Master_PageIndexChanging">
                                    <HeaderStyle CssClass="success" />

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="7" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                        <div style="text-align: center">Data Cannot Be Found</div>
                                    </EmptyDataTemplate>
                                    <Columns>

                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="CheckBox_u0_didx" runat="server" />
                                                <asp:Label ID="lbl_u0_didx_cutoff_approve" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_u0code_cutoff_approve" runat="server" Text='<%# Eval("u0_code") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Type Device" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_typedevice_cutoff_approve" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Referent" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_referent_cutoff_approve" runat="server" Text='<%# Eval("u0_referent_code") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Status Device" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_m0_sidx_cutoff" runat="server" Visible="false" Text='<%# Eval("m0_sidx") %>'></asp:Label>
                                                <asp:Label ID="lbl_statusidx_cutoff_approve" runat="server" Visible="false" Text='<%# Eval("doc_status") %>'></asp:Label>
                                                <asp:Label ID="lbl_statusdevice_cutoff_approve" runat="server" Text='<%# Eval("StatusDoc") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:Panel>

                    <asp:Panel ID="pn_fv_approve_cutoff" runat="server" Visible="false">
                        <div class="panel panel-success class">
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <asp:FormView ID="FvViewApprove_cutoff" runat="server" OnDataBound="FvDetail_DataBound">
                                        <InsertItemTemplate>

                                            <div class="form-group">
                                                <asp:Label ID="Labe2l13" CssClass="col-sm-2 control-label" runat="server" Text="ผลอนุมัติ : " />
                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddl_view_Approve" AutoPostBack="true" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">กรุณาเลือกผลการอนุมัติ....</asp:ListItem>
                                                        <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                                        <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Labe2l30" CssClass="col-sm-2 control-label" runat="server" Text="Comment : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="tbDocRemark_cutoff" runat="server" CssClass="form-control" TextMode="multiline" Width="563pt" Rows="5"></asp:TextBox>
                                                </div>
                                            </div>

                                            <asp:Panel ID="Box_fileupload_cutoff" Visible="false" runat="server">
                                                <asp:UpdatePanel ID="ud_fileupload" runat="server">
                                                    <ContentTemplate>
                                                        <div class="form-group">
                                                            <label class="col-sm-2 control-label"></label>
                                                            <div class="col-sm-10">
                                                                <asp:FileUpload ID="UploadImages_Cutoff" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-sm multi" accept="gif|jpg|pdf|png|pdf" />
                                                                <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png</font></p>
                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnApprove_cutoff_device" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </asp:Panel>

                                            <div class="form-group">
                                                <div class="col-sm-4 col-sm-offset-8">
                                                    <asp:LinkButton ID="btnApprove_cutoff_device" CssClass="btn btn-success" runat="server" CommandName="btn_Approve_cutoff" OnCommand="btnCommand" ValidationGroup="SaveView" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"> บันทึกข้อมูล</i></asp:LinkButton>
                                                    <asp:LinkButton ID="ViewCancel" CssClass="btn btn-default" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" CommandArgument="1" title="Cancel"><i class="fa fa-times"> ยกเลิก</i></asp:LinkButton>
                                                </div>
                                            </div>

                                        </InsertItemTemplate>
                                    </asp:FormView>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>

                    <asp:Panel ID="pn_rp_log_approve" runat="server" Visible="false">

                        <div class="row panel panel-default font_text" runat="server" id="Div1">
                            <div class="panel panel-info">
                                <div class="panel-heading f-bold">ประวัติข้อมูล Device</div>
                            </div>
                            <div class="panel-body">
                                <asp:Repeater ID="rplog_cutoff" runat="server">
                                    <ItemTemplate>
                                        <div class="form-horizontal">
                                            <div class="form-group">
                                                <div class="col-sm-2 control-label">
                                                    <span>ผู้ดำเนินการ</span>
                                                </div>
                                                <div class="col-sm-4 control-label textleft">
                                                    <span><%# Eval("EmpName") %>(<%# Eval("actor_des") %>)</span>
                                                </div>
                                                <div class="col-sm-2 control-label">
                                                    <span>วัน / เวลา</span>
                                                </div>
                                                <div class="col-sm-4 control-label textleft">
                                                    <span><%# Eval("u0_createdate") %></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2 control-label">
                                                    <span>ดำเนินการ</span>
                                                </div>
                                                <div class="col-sm-4 control-label textleft">
                                                    <span><%# Eval("node_name") %></span>
                                                </div>
                                                <div class="col-sm-2 control-label">
                                                    <span>ผลการดำเนินการ</span>
                                                </div>
                                                <div class="col-sm-4 control-label textleft">
                                                    <span><%# Eval("status_name") %></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2 control-label">
                                                    <span>ความคิดเห็น</span>
                                                </div>
                                                <div class="col-sm-4 control-label textleft">
                                                    <span><%# Eval("comment") %></span>
                                                </div>
                                            </div>
                                            <hr />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>

                    </asp:Panel>

                </div>
            </div>
        </asp:View>

        <asp:View ID="ViewaddMasterdata" runat="server">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; เพิ่ม Master Data ทะเบียนคอมพิวเตอร์</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="form-horizontal" role="form">

                        <asp:FormView ID="FvInsertMasterDevice" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>

                                <div class="form-group">
                                    <asp:Label ID="Labe2l13" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                    <div class="col-sm-3">
                                        <asp:DropDownList ID="ddl_addmaster_typedevice" AutoPostBack="true" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                            <asp:ListItem Value="0">กรุณาเลือกประเภทอุปกรณ์....</asp:ListItem>
                                            <asp:ListItem Value="6">CPU</asp:ListItem>
                                            <asp:ListItem Value="7">RAM</asp:ListItem>
                                            <asp:ListItem Value="8">HDD</asp:ListItem>
                                            <asp:ListItem Value="9">VGA</asp:ListItem>
                                            <asp:ListItem Value="3">PRINTER</asp:ListItem>
                                            <asp:ListItem Value="4">MONITER</asp:ListItem>
                                            <%--<asp:ListItem Value="10">ประเภทอุปกรณ์</asp:ListItem>--%>
                                            <asp:ListItem Value="11">ยี่ห้อ</asp:ListItem>
                                            <%--<asp:ListItem Value="12">ระดับ</asp:ListItem>--%>
                                            <%--<asp:ListItem Value="13">สถานะอุปกรณ์</asp:ListItem>--%>
                                            <asp:ListItem Value="14">บริษัทประกัน</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>

                                <asp:Panel ID="boxmaster_cpu" runat="server" Visible="false">
                                    <div class="form-group">
                                        <asp:Label ID="Label16" class="col-sm-2 control-label" runat="server" Text="ยี่ห้อ CPU : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_cpu_band" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label17" CssClass="col-sm-2 control-label" runat="server" Text="รุ่น CPU : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_cpu_generation" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label31" class="col-sm-2 control-label" runat="server" Text="สถานะ CPU : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_addmaster_cpu_status" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="1">ใช้งาน</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่ใช้งาน</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="boxmaster_ram" runat="server" Visible="false">
                                    <div class="form-group">
                                        <asp:Label ID="Label32" class="col-sm-2 control-label" runat="server" Text="ยี่ห้อ ram : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_ram_band" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label33" CssClass="col-sm-2 control-label" runat="server" Text="ประเภท ram : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_ram_type" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label35" class="col-sm-2 control-label" runat="server" Text="ความจุ ram : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_ram_size" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label34" class="col-sm-2 control-label" runat="server" Text="สถานะ ram : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_addmaster_ram_status" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="1">ใช้งาน</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่ใช้งาน</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="boxmaster_hdd" runat="server" Visible="false">
                                    <div class="form-group">
                                        <asp:Label ID="Label36" class="col-sm-2 control-label" runat="server" Text="ยี่ห้อ hdd : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_hdd_band" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label37" CssClass="col-sm-2 control-label" runat="server" Text="ประเภท hdd : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txxt_addmaster_hdd_type" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label38" class="col-sm-2 control-label" runat="server" Text="ความจุ hdd : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_hdd_size" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label39" class="col-sm-2 control-label" runat="server" Text="สถานะ hdd : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_addmaster_hdd_status" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="1">ใช้งาน</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่ใช้งาน</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="boxmaster_vga" runat="server" Visible="false">
                                    <div class="form-group">
                                        <asp:Label ID="Label41" CssClass="col-sm-2 control-label" runat="server" Text="ประเภท vga : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_vga_type" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label40" class="col-sm-2 control-label" runat="server" Text="รุ่น vga : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_vga_gen" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label43" class="col-sm-2 control-label" runat="server" Text="สถานะ vga : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_addmaster_vga_status" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="1">ใช้งาน</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่ใช้งาน</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="boxmaster_printer" runat="server" Visible="false">                                 
                                    <div class="form-group">
                                        <asp:Label ID="Label94" CssClass="col-sm-2 control-label" runat="server" Text="ยี่ห้อ printer : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_band_print" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>       
                                    <div class="form-group">
                                        <asp:Label ID="Label91" CssClass="col-sm-2 control-label" runat="server" Text="ประเภท printer : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_printer_type" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <asp:Label ID="Label95" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทหมึก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_ink_print" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label92" class="col-sm-2 control-label" runat="server" Text="รุ่น printer : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_gen_print" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label93" class="col-sm-2 control-label" runat="server" Text="สถานะ printer : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_addmaster_printer_status" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="1">ใช้งาน</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่ใช้งาน</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="boxmaster_moniter" runat="server" Visible="false">                                   
                                    <div class="form-group">
                                        <asp:Label ID="Label99" class="col-sm-2 control-label" runat="server" Text="รุ่น moniter : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_band_moniter" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label98" CssClass="col-sm-2 control-label" runat="server" Text="ขนาดจอ moniter : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_type_moniter" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label100" class="col-sm-2 control-label" runat="server" Text="สถานะ moniter : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_addmaster_moniter_status" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="1">ใช้งาน</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่ใช้งาน</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="boxmaster_insurance" runat="server" Visible="false">                                   
                                    <div class="form-group">
                                        <asp:Label ID="Label101" class="col-sm-2 control-label" runat="server" Text="ชื่อบริษัทประกัน : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_name_insurance" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label102" CssClass="col-sm-2 control-label" runat="server" Text="รายละเอียด : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_detail_insurance" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label103" class="col-sm-2 control-label" runat="server" Text="สถานะบริษัทประกัน : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_addmaster_insurance_status" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="1">ใช้งาน</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่ใช้งาน</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <asp:Panel ID="boxmaster_band" runat="server" Visible="false">
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-2 control-label" runat="server" Text="ชื่อปะเภท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txt_addmaster_name_band" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                    </div>
   
                                    <div class="form-group">
                                        <asp:Label ID="Label105" class="col-sm-2 control-label" runat="server" Text="สถานะ : " />
                                        <div class="col-sm-3">
                                            <asp:DropDownList ID="ddl_addmaster_band_status" AutoPostBack="true" runat="server" CssClass="form-control">
                                                <asp:ListItem Value="1">ใช้งาน</asp:ListItem>
                                                <asp:ListItem Value="2">ไม่ใช้งาน</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                </asp:Panel>

                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-10">
                                        <asp:LinkButton ID="btnAddMaster" CssClass="btn btn-success btn-sm" runat="server" CommandName="btninsert_masterdata" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                        <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                    </div>
                                </div>

                            </InsertItemTemplate>
                        </asp:FormView>

                        <asp:Panel ID="box_master_cpu" runat="server" Visible="false">
                            <asp:GridView ID="Gv_Master_cpu"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="table_headCenter"
                                HeaderStyle-Height="40px"
                                AllowPaging="True"
                                PageSize="10"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowEditing="Master_RowEditing"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="PageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_idx" runat="server" Text='<%# Eval("m0_cidx") %>' />
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_master_m0_cidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_cidx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ยี่ห้อ CPU" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_band_cpu" runat="server" CssClass="form-control" Text='<%# Eval("band_cpu")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" Text="รุ่น CPU" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_gen_cpu" runat="server" CssClass="form-control" Text='<%# Eval("generation_cpu")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="Status" />

                                                        <div class="col-sm-5">
                                                            <asp:DropDownList ID="ddStatus_update" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status_m0_cpu") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-4 col-sm-offset-6">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </EditItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ยี่ห้อ CPU" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_band_cpu" runat="server" Text='<%# Eval("band_cpu") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รุ่น CPU" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_gen_cpu" runat="server" Text='<%# Eval("generation_cpu") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <div style="text-align: center; padding-top: 10px;">
                                        <span class='<%# Eval("status_m0_cpu").ToString() == "1" ? "fa fa-check" : "fa fa-times" %>'></span>
                                    </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel_master_cpu" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_cidx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>

                        <asp:Panel ID="box_master_ram" runat="server" Visible="false">
                            <asp:GridView ID="Gv_Master_ram"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="table_headCenter"
                                HeaderStyle-Height="40px"
                                AllowPaging="True"
                                PageSize="10"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowEditing="Master_RowEditing"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="PageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_idx_ram" runat="server" Text='<%# Eval("m0_ridx") %>' />
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_master_idx_ram" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_ridx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ยี่ห้อ RAM" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_band_ram" runat="server" CssClass="form-control" Text='<%# Eval("band_ram")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" Text="ประเภท RAM" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_gen_ram" runat="server" CssClass="form-control" Text='<%# Eval("type_ram")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label90" runat="server" Text="ความจุ RAM" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_capa_ram" runat="server" CssClass="form-control" Text='<%# Eval("capacity_ram")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="Status" />

                                                        <div class="col-sm-5">
                                                            <asp:DropDownList ID="ddStatus_update" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status_m0_ram") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-4 col-sm-offset-6">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </EditItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ยี่ห้อ RAM" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_band_ram" runat="server" Text='<%# Eval("band_ram") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ประเภท RAM" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_gen_ram" runat="server" Text='<%# Eval("type_ram") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ความจุ RAM" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_capa_ram" runat="server" Text='<%# Eval("capacity_ram") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <div style="text-align: center; padding-top: 10px;">
                                        <span class='<%# Eval("status_m0_ram").ToString() == "1" ? "fa fa-check" : "fa fa-times" %>'></span>
                                    </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel_master_ram" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_ridx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>

                        <asp:Panel ID="box_master_hdd" runat="server" Visible="false">
                            <asp:GridView ID="Gv_Master_hdd"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="table_headCenter"
                                HeaderStyle-Height="40px"
                                AllowPaging="True"
                                PageSize="10"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowEditing="Master_RowEditing"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="PageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_idx_hdd" runat="server" Text='<%# Eval("m0_hidx") %>' />
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_master_idx_hdd" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_hidx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label89" runat="server" Text="ยี่ห้อ HDD" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_band_hdd" runat="server" CssClass="form-control" Text='<%# Eval("name_band_hdd")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ประเภท HDD" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_gen_hdd" runat="server" CssClass="form-control" Text='<%# Eval("type_hdd")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" Text="ความจุ HDD" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_capa_hdd" runat="server" CssClass="form-control" Text='<%# Eval("capacity_hdd")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="Status" />

                                                        <div class="col-sm-5">
                                                            <asp:DropDownList ID="ddStatus_update" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status_m0_hdd") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-4 col-sm-offset-6">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </EditItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ยี่ห้อ HDD" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_band_hdd" runat="server" Text='<%# Eval("name_band_hdd") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ประเภท HDD" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_gen_hdd" runat="server" Text='<%# Eval("type_hdd") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ความจุ HDD" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_capa_hdd" runat="server" Text='<%# Eval("capacity_hdd") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <div style="text-align: center; padding-top: 10px;">
                                        <span class='<%# Eval("status_m0_hdd").ToString() == "1" ? "fa fa-check" : "fa fa-times" %>'></span>
                                    </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel_master_hdd" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_hidx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>

                        <asp:Panel ID="box_master_vga" runat="server" Visible="false">
                            <asp:GridView ID="Gv_Master_vga"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="table_headCenter"
                                HeaderStyle-Height="40px"
                                AllowPaging="True"
                                PageSize="10"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowEditing="Master_RowEditing"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="PageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_idx_vga" runat="server" Text='<%# Eval("m0_vidx") %>' />
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_master_idx_vga" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_vidx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label89" runat="server" Text="ประเภทการ์ดจอ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_gen_vga" runat="server" CssClass="form-control" Text='<%# Eval("name_type_vga")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="รุ่นการ์ดจอ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_capa_vga" runat="server" CssClass="form-control" Text='<%# Eval("generation_vga")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="Status" />

                                                        <div class="col-sm-5">
                                                            <asp:DropDownList ID="ddStatus_update" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status_m0_vga") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-4 col-sm-offset-6">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </EditItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ประเภทการ์ดจอ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_gen_vga" runat="server" Text='<%# Eval("name_type_vga") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รุ่นการ์ดจอ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_capa_vga" runat="server" Text='<%# Eval("generation_vga") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <div style="text-align: center; padding-top: 10px;">
                                        <span class='<%# Eval("status_m0_vga").ToString() == "1" ? "fa fa-check" : "fa fa-times" %>'></span>
                                    </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel_master_vga" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_vidx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>

                        <asp:Panel ID="box_master_printer" runat="server" Visible="false">
                            <asp:GridView ID="Gv_Master_printer"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="table_headCenter"
                                HeaderStyle-Height="40px"
                                AllowPaging="True"
                                PageSize="10"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowEditing="Master_RowEditing"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="PageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_idx_printer" runat="server" Text='<%# Eval("m0_pidx") %>' />
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_master_idx_poid" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_pidx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label89" runat="server" Text="ยี่ห้อปริ้นเตอร์" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_addmaster_band_print" runat="server" CssClass="form-control" Text='<%# Eval("name_band_print")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ประเภทเครื่องปริ้น" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_type_print" runat="server" CssClass="form-control" Text='<%# Eval("type_print")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label96" runat="server" Text="ประเภทหมึก" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_ink_print" runat="server" CssClass="form-control" Text='<%# Eval("type_ink")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label97" runat="server" Text="รุ่นปริ้นเตอร์" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_gen_print" runat="server" CssClass="form-control" Text='<%# Eval("generation_print")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="Status" />

                                                        <div class="col-sm-5">
                                                            <asp:DropDownList ID="ddStatus_update" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status_m0_printer") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-4 col-sm-offset-6">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </EditItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ยี่ห้อปริ้นเตอร์" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_band_print" runat="server" Text='<%# Eval("name_band_print") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ประเภทเครื่องปริ้น" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_type_print" runat="server" Text='<%# Eval("type_print") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ประเภทหมึก" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_ink_print" runat="server" Text='<%# Eval("type_ink") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รุ่นปริ้นเตอร์" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_gen_print" runat="server" Text='<%# Eval("generation_print") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <div style="text-align: center; padding-top: 10px;">
                                        <span class='<%# Eval("status_m0_printer").ToString() == "1" ? "fa fa-check" : "fa fa-times" %>'></span>
                                    </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel_master_printer" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_pidx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>

                        <asp:Panel ID="box_master_moniter" runat="server" Visible="false">
                            <asp:GridView ID="Gv_Master_moniter"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="table_headCenter"
                                HeaderStyle-Height="40px"
                                AllowPaging="True"
                                PageSize="10"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowEditing="Master_RowEditing"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="PageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_idx_moniter" runat="server" Text='<%# Eval("m0_midx") %>' />
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_master_idx_moniter" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_midx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ยี่ห้อหน้าจอ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_band_moniter" runat="server" CssClass="form-control" Text='<%# Eval("name_band_moniter")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" Text="ขนาดหน้าจอ" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_type_moniter" runat="server" CssClass="form-control" Text='<%# Eval("size_moniter")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="Status" />

                                                        <div class="col-sm-5">
                                                            <asp:DropDownList ID="ddStatus_update" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status_m0_moniter") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-4 col-sm-offset-6">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </EditItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ยี่ห้อหน้าจอ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_band_moniter" runat="server" Text='<%# Eval("name_band_moniter") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ขนาดหน้าจอ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_type_moniter" runat="server" Text='<%# Eval("size_moniter") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <div style="text-align: center; padding-top: 10px;">
                                        <span class='<%# Eval("status_m0_moniter").ToString() == "1" ? "fa fa-check" : "fa fa-times" %>'></span>
                                    </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel_master_moniter" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_midx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>                    
                       
                        <asp:Panel ID="box_master_insurance" runat="server" Visible="false">
                            <asp:GridView ID="Gv_Master_insurance"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="table_headCenter"
                                HeaderStyle-Height="40px"
                                AllowPaging="True"
                                PageSize="10"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowEditing="Master_RowEditing"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="PageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("m0_iridx") %>' />
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_master_m0_iridx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_iridx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ชื่อบริษัทประกัน" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_name_insurance" runat="server" CssClass="form-control" Text='<%# Eval("name_m0_insurance")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label2" runat="server" Text="รายละเอียด" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_detail_insurance" runat="server" CssClass="form-control" Text='<%# Eval("detail_m0_insurance")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="Status" />

                                                        <div class="col-sm-5">
                                                            <asp:DropDownList ID="ddStatus_update" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status_m0_insurance") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-4 col-sm-offset-6">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </EditItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชื่อบริษัทประกัน" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_name_insurance" runat="server" Text='<%# Eval("name_m0_insurance") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_detail_insurance" runat="server" Text='<%# Eval("detail_m0_insurance") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <div style="text-align: center; padding-top: 10px;">
                                        <span class='<%# Eval("status_m0_insurance").ToString() == "1" ? "fa fa-check" : "fa fa-times" %>'></span>
                                    </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel_master_insurance" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_iridx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>

                        <asp:Panel ID="box_master_band" runat="server" Visible="false">
                            <asp:GridView ID="Gv_Master_band"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="table_headCenter"
                                HeaderStyle-Height="40px"
                                AllowPaging="True"
                                PageSize="10"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowEditing="Master_RowEditing"
                                OnRowUpdating="Master_RowUpdating"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnRowDataBound="Master_RowDataBound">

                                <PagerStyle CssClass="PageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_idx_band" runat="server" Text='<%# Eval("m0_bidx") %>' />
                                        </ItemTemplate>

                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txt_master_m0_bidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m0_bidx")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="ชื่อปะเภท" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-5">
                                                            <asp:TextBox ID="txt_master_name_band" runat="server" CssClass="form-control" Text='<%# Eval("name_m0_band")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatus" CssClass="col-sm-3 control-label" runat="server" Text="Status" />

                                                        <div class="col-sm-5">
                                                            <asp:DropDownList ID="ddStatus_update" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("status_m0_band") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="0" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-4 col-sm-offset-6">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server" ValidationGroup="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>

                                        </EditItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชื่อปะเภท" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_name_band" runat="server" Text='<%# Eval("name_m0_band") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <div style="text-align: center; padding-top: 10px;">
                                        <span class='<%# Eval("status_m0_band").ToString() == "1" ? "fa fa-check" : "fa fa-times" %>'></span>
                                    </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel_master_band" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("m0_bidx") %>'><i class="fa fa-trash-o"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>


                        <asp:Panel ID="box_master_status" runat="server" Visible="false">
                            <asp:GridView ID="Gv_Master_status"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="table_headCenter"
                                HeaderStyle-Height="40px"
                                AllowPaging="True"
                                PageSize="10"
                                OnPageIndexChanging="Master_PageIndexChanging">

                                <PagerStyle CssClass="PageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("m0_sidx") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชื่อสถานะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_name_status" runat="server" Text='<%# Eval("name_m0_status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_detail_status" runat="server" Text='<%# Eval("detail_m0_status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_status_status" runat="server" Text='<%# Eval("status_m0_status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton8" CssClass="btn btn-primary" runat="server" CommandName="btnView_GvDevice" OnCommand="btnCommand" CommandArgument='<%# Eval("m0_sidx") %>' data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>
                        
                        <asp:Panel ID="box_master_typedevice" runat="server" Visible="false">
                            <asp:GridView ID="Gv_Master_typedevice"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="table_headCenter"
                                HeaderStyle-Height="40px"
                                AllowPaging="True"
                                PageSize="10"
                                OnPageIndexChanging="Master_PageIndexChanging">

                                <PagerStyle CssClass="PageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_idx_typedevice" runat="server" Text='<%# Eval("m0_tdidx") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชื่อปะเภท" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_name_typedevice" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_status_typedevice" runat="server" Text='<%# Eval("status_m0_typedevice") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton5" CssClass="btn btn-primary" runat="server" CommandName="btnView_GvDevice" OnCommand="btnCommand" CommandArgument='<%# Eval("m0_tdidx") %>' data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>

                        <asp:Panel ID="box_master_level" runat="server" Visible="false">
                            <asp:GridView ID="Gv_Master_level"
                                runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                HeaderStyle-CssClass="table_headCenter"
                                HeaderStyle-Height="40px"
                                AllowPaging="True"
                                PageSize="10"
                                OnPageIndexChanging="Master_PageIndexChanging">

                                <PagerStyle CssClass="PageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่มีข้อมูลในขณะนี้</div>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("m0_lvidx") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ชื่อปะเภท" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_name_level" runat="server" Text='<%# Eval("name_m0_level") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_detail_level" runat="server" Text='<%# Eval("detail_m0_level") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_master_status_level" runat="server" Text='<%# Eval("status_m0_level") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="LinkButton7" CssClass="btn btn-primary" runat="server" CommandName="btnView_GvDevice" OnCommand="btnCommand" CommandArgument='<%# Eval("m0_lvidx") %>' data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </asp:Panel>

                    </div>
                </div>
            </div>
        </asp:View>

        <asp:View ID="Viewapproveadddevice" runat="server">
            <asp:Label ID="daaq" runat="server"></asp:Label>
            <asp:Label ID="fssq" runat="server"></asp:Label>
            <div class="panel-body">
                <div class="form-horizontal" role="form">
                    <asp:Panel ID="boxviewapprove" runat="server" Visible="true">
                        <asp:FormView ID="FvViewApproveDevice" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>
                                <div class="panel panel-success class">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; Approve เพิ่มอุปกรณ์คอมพิวเตอร์</strong></h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <asp:GridView ID="GvDetail_Approve_New"
                                                DataKeyNames="u0_didx"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover"
                                                HeaderStyle-CssClass="table_headCenter"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="True"
                                                PageSize="10"
                                                BorderStyle="None"
                                                CellSpacing="2"
                                                OnPageIndexChanging="Master_PageIndexChanging">

                                                <PagerStyle CssClass="PageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lbDeviceIDX" runat="server" Text='<%# Eval("u0_code") %>' />
                                                                <asp:Label ID="lbl_u0_didx_approve" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lbl_u0_relation_didx_new" runat="server" Text='<%# Eval("u0_relation_didx")%>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lbl_doc_status_new" runat="server" Text='<%# Eval("doc_status")%>' Visible="false"></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lblAsseta" runat="server" Text='<%# Eval("u0_acc") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Type" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lb_m0_tdidx" runat="server" Text='<%# Eval("m0_tdidx") %>' Visible="false" />
                                                                <asp:Label ID="lblAssetaq" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Band" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lblAssetsaq" runat="server" Text='<%# Eval("name_m0_band") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Management">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnView_approve_new" CssClass="btn btn-primary" runat="server" CommandName="btnView_approve_new" OnCommand="btnCommand" CommandArgument='<%#Eval("u0_didx") + ";" + Eval("u0_relation_didx") %>' data-toggle="tooltip" title="รายละเอียด"><i class="fa fa-edit"></i></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>

                                            <asp:Panel ID="Box_Approve_NewDevice" runat="server" Visible="false">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title"><strong>&nbsp; รายละเอียดอุปกรณ์หลัก</strong></h3>
                                                </div>

                                                <asp:FormView ID="FvView_Detail_NewDevice" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                                    <ItemTemplate>
                                                        <div class="form-group">
                                                            <asp:Label ID="Label13" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbl_view_u0_didx" runat="server" Visible="false" Text='<%# Eval("u0_didx") %>'></asp:Label>
                                                                <asp:Label ID="lbl_view_typedevice" runat="server" Visible="false" Text='<%# Eval("m0_tdidx") %>'></asp:Label>
                                                                <asp:TextBox ID="txt_view_typedevice" CssClass="form-control" runat="server" Text='<%# Eval("name_m0_typedevice") %>' Enabled="false"></asp:TextBox>
                                                            </div>

                                                            <asp:Label ID="Label70" CssClass="col-sm-3 control-label" runat="server" Text="รหัสอุปกรณ์ : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="TextBox3" CssClass="form-control" Text='<%# Eval("u0_code") %>' runat="server" Enabled="false"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label12" class="col-sm-2 control-label" runat="server" Text="เลขที่ Acc : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txt_view_acc" CssClass="form-control" Text='<%# Eval("u0_acc") %>' Enabled="false" runat="server"></asp:TextBox>
                                                            </div>

                                                            <asp:Label ID="Label18" CssClass="col-sm-3 control-label" runat="server" Text="เลขที่ PO : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txt_view_po" CssClass="form-control" Text='<%# Eval("u0_po") %>' Enabled="false" runat="server"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:FormView>

                                                <div class="panel-heading">
                                                    <h3 class="panel-title"><strong>&nbsp; รายละเอียดอุปกรณ์ที่เพิ่ม</strong></h3>
                                                </div>

                                                <asp:FormView ID="FvView_Detail_NewDevice_new" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                                    <ItemTemplate>
                                                        <div class="form-group">
                                                            <asp:Label ID="Label13" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbl_view_u0_didx" runat="server" Visible="false" Text='<%# Eval("u0_didx") %>'></asp:Label>
                                                                <asp:Label ID="lbl_view_relation_didx_new" runat="server" Text='<%# Eval("u0_relation_didx")%>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lbl_view_typedevice" runat="server" Visible="false" Text='<%# Eval("m0_tdidx") %>'></asp:Label>
                                                                <asp:TextBox ID="txt_view_typedevice" CssClass="form-control" runat="server" Text='<%# Eval("name_m0_typedevice") %>' Enabled="false"></asp:TextBox>
                                                            </div>

                                                            <asp:Label ID="Label70" CssClass="col-sm-3 control-label" runat="server" Text="รหัสอุปกรณ์ : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="TextBox3" CssClass="form-control" Text='<%# Eval("u0_code") %>' runat="server" Enabled="false"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label12" class="col-sm-2 control-label" runat="server" Text="เลขที่ Acc : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txt_view_acc" CssClass="form-control" Text='<%# Eval("u0_acc") %>' Enabled="false" runat="server"></asp:TextBox>
                                                            </div>

                                                            <asp:Label ID="Label18" CssClass="col-sm-3 control-label" runat="server" Text="เลขที่ PO : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txt_view_po" CssClass="form-control" Text='<%# Eval("u0_po") %>' Enabled="false" runat="server"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:FormView>

                                                <hr />
                                                <div class="form-group">
                                                    <asp:Label ID="Labe2l13" CssClass="col-sm-2 control-label" runat="server" Text="ผลอนุมัติ : " />
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddl_view_Approve2" AutoPostBack="true" runat="server" CssClass="form-control" ValidationGroup="Approve_device_new">
                                                            <asp:ListItem Value="0">กรุณาเลือกผลการอนุมัติ....</asp:ListItem>
                                                            <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                                            <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFiweldValidator12" ValidationGroup="Approve_device_new" runat="server" Display="None"
                                                            ControlToValidate="ddl_view_Approve2" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกผลการอนุมัติ ...."
                                                            ValidationExpression="กรุณาเลือกผลการอนุมัติ ...." InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExten3der3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFiweldValidator12" Width="160" />
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Labe2l30" CssClass="col-sm-2 control-label" runat="server" Text="Comment : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="tbDocRemark" runat="server" CssClass="form-control" TextMode="multiline" Width="563pt" Rows="5"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-4 col-sm-offset-8">
                                                        <asp:LinkButton ID="btnViewdata" CssClass="btn btn-success" runat="server" CommandName="btnView_Approve_Device" OnCommand="btnCommand" ValidationGroup="Approve_device_new" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"> บันทึกข้อมูล</i></asp:LinkButton>
                                                        <asp:LinkButton ID="ViewCancel" CssClass="btn btn-default" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" CommandArgument="1" title="Cancel"><i class="fa fa-times"> ยกเลิก</i></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </asp:Panel>

                                        </div>
                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </asp:Panel>
                </div>
            </div>
        </asp:View>

        <asp:View ID="Viewapprovetransfer" runat="server">
            <asp:Label ID="Label71" runat="server"></asp:Label>
            <asp:Label ID="Label72" runat="server"></asp:Label>
            <asp:Label ID="Label73" runat="server"></asp:Label>
            <div class="panel-body">
                <div class="form-horizontal" role="form">
                    <asp:Panel ID="boxviewapprove_transfer" runat="server" Visible="false">
                        <asp:FormView ID="FvViewApproveDevice_transfer" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                            <InsertItemTemplate>
                                <div class="panel panel-success class">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; Approve เปลี่ยนแปลงอุปกรณ์คอมพิวเตอร์</strong></h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <asp:GridView ID="GvDetail_Approve_Transfer"
                                                DataKeyNames="u0_didx"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover"
                                                HeaderStyle-CssClass="table_headCenter"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="True"
                                                PageSize="10"
                                                BorderStyle="None"
                                                CellSpacing="2"
                                                OnPageIndexChanging="Master_PageIndexChanging">

                                                <PagerStyle CssClass="PageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ID" ItemStyle-HorizontalAlign="Center">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lbDeviceIDX" runat="server" Text='<%# Eval("u0_code") %>' />
                                                                <asp:Label ID="lbl_u0_didx_approve" runat="server" Text='<%# Eval("u0_didx")%>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lbl_u0_relation_didx_new" runat="server" Text='<%# Eval("u0_relation_didx")%>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lbl_doc_status_new" runat="server" Text='<%# Eval("doc_status")%>' Visible="false"></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Asset Code" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lblAsseta" runat="server" Text='<%# Eval("u0_acc") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Type" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lb_m0_tdidx" runat="server" Text='<%# Eval("m0_tdidx") %>' Visible="false" />
                                                                <asp:Label ID="lblAssetaq" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Band" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <div class="panel-heading">
                                                                <asp:Label ID="lblAssetsaq" runat="server" Text='<%# Eval("name_m0_band") %>'></asp:Label>
                                                            </div>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Management">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnView_approve_transfer" CssClass="btn btn-primary" runat="server" CommandName="btnView_approve_transfer" OnCommand="btnCommand" CommandArgument='<%#Eval("u0_didx") + ";" + Eval("u0_referent") %>' data-toggle="tooltip" title="รายละเอียด"><i class="fa fa-edit"></i></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>


                                            <asp:Panel ID="Box_Approve_Transfer" runat="server" Visible="false">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title"><strong>&nbsp; รายละเอียดอุปกรณ์ปัจจุบัน</strong></h3>
                                                </div>

                                                <asp:FormView ID="FvView_Detail_Transfer" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                                    <ItemTemplate>
                                                        <div class="form-group">
                                                            <asp:Label ID="Label13" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbl_view_u0_didx" runat="server" Visible="false" Text='<%# Eval("u0_didx") %>'></asp:Label>
                                                                <asp:Label ID="lbl_view_typedevice" runat="server" Visible="false" Text='<%# Eval("m0_tdidx") %>'></asp:Label>
                                                                <asp:Label ID="lbl_u0_relation_didx_main" runat="server" Text='<%# Eval("u0_relation_didx")%>' Visible="false"></asp:Label>
                                                                <asp:TextBox ID="txt_view_typedevice" CssClass="form-control" runat="server" Text='<%# Eval("name_m0_typedevice") %>' Enabled="false"></asp:TextBox>
                                                            </div>

                                                            <asp:Label ID="Label70" CssClass="col-sm-3 control-label" runat="server" Text="รหัสอุปกรณ์ : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="TextBox3" CssClass="form-control" Text='<%# Eval("u0_code") %>' runat="server" Enabled="false"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label12" class="col-sm-2 control-label" runat="server" Text="เลขที่ Acc : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txt_view_acc" CssClass="form-control" Text='<%# Eval("u0_acc") %>' Enabled="false" runat="server"></asp:TextBox>
                                                            </div>

                                                            <asp:Label ID="Label18" CssClass="col-sm-3 control-label" runat="server" Text="เลขที่ PO : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txt_view_po" CssClass="form-control" Text='<%# Eval("u0_po") %>' Enabled="false" runat="server"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:FormView>

                                                <div class="panel-heading">
                                                    <h3 class="panel-title"><strong>&nbsp; รายละเอียดอุปกรณ์ใหม่</strong></h3>
                                                </div>

                                                <asp:FormView ID="FvView_Detail_Transfer_new" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                                    <ItemTemplate>
                                                        <div class="form-group">
                                                            <asp:Label ID="Label13" CssClass="col-sm-2 control-label" runat="server" Text="ประเภทอุปกรณ์ : " />
                                                            <div class="col-sm-3">
                                                                <asp:Label ID="lbl_view_u0_didx_new" runat="server" Visible="false" Text='<%# Eval("u0_didx") %>'></asp:Label>
                                                                <asp:Label ID="lbl_view_u0_referent" runat="server" Text='<%# Eval("u0_referent")%>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lbl_view_typedevice" runat="server" Visible="false" Text='<%# Eval("m0_tdidx") %>'></asp:Label>
                                                                <asp:TextBox ID="txt_view_typedevice" CssClass="form-control" runat="server" Text='<%# Eval("name_m0_typedevice") %>' Enabled="false"></asp:TextBox>
                                                            </div>

                                                            <asp:Label ID="Label70" CssClass="col-sm-3 control-label" runat="server" Text="รหัสอุปกรณ์ : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="TextBox3" CssClass="form-control" Text='<%# Eval("u0_code") %>' runat="server" Enabled="false"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <asp:Label ID="Label12" class="col-sm-2 control-label" runat="server" Text="เลขที่ Acc : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txt_view_acc" CssClass="form-control" Text='<%# Eval("u0_acc") %>' Enabled="false" runat="server"></asp:TextBox>
                                                            </div>

                                                            <asp:Label ID="Label18" CssClass="col-sm-3 control-label" runat="server" Text="เลขที่ PO : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="txt_view_po" CssClass="form-control" Text='<%# Eval("u0_po") %>' Enabled="false" runat="server"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:FormView>

                                                <hr />
                                                <asp:FormView ID="FvViewApprove_transfer" runat="server" OnDataBound="FvDetail_DataBound">
                                                    <InsertItemTemplate>
                                                        <div class="panel-heading">
                                                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; Approve เปลี่ยนแปลงอุปกรณ์</strong></h3>
                                                        </div>
                                                        <asp:Panel ID="boxviewapprove_cpu" runat="server" Visible="true">
                                                        </asp:Panel>
                                                        <div class="form-group">
                                                            <asp:Label ID="Labe2l13" CssClass="col-sm-2 control-label" runat="server" Text="ผลอนุมัติ : " />
                                                            <div class="col-sm-3">
                                                                <asp:DropDownList ID="ddl_view_Approve" AutoPostBack="true" runat="server" CssClass="form-control">
                                                                    <asp:ListItem Value="0">กรุณาเลือกผลการอนุมัติ....</asp:ListItem>
                                                                    <asp:ListItem Value="1">อนุมัติ</asp:ListItem>
                                                                    <asp:ListItem Value="2">ไม่อนุมัติ</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:Label ID="Labe2l30" CssClass="col-sm-2 control-label" runat="server" Text="Comment : " />
                                                            <div class="col-sm-3">
                                                                <asp:TextBox ID="tbDocRemark" runat="server" CssClass="form-control" TextMode="multiline" Width="563pt" Rows="5"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-sm-4 col-sm-offset-8">
                                                                <asp:LinkButton ID="btnApprove_new_device_" CssClass="btn btn-success" runat="server" CommandName="btnView_Approve_Device_transfer" OnCommand="btnCommand" ValidationGroup="SaveView" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"> บันทึกข้อมูล</i></asp:LinkButton>
                                                                <asp:LinkButton ID="ViewCancel" CssClass="btn btn-default" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" CommandArgument="1" title="Cancel"><i class="fa fa-times"> ยกเลิก</i></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                    </InsertItemTemplate>
                                                </asp:FormView>
                                            </asp:Panel>

                                        </div>
                                    </div>
                                </div>
                            </InsertItemTemplate>
                        </asp:FormView>
                    </asp:Panel>
                </div>
            </div>
        </asp:View>

        <%-------- Index End----------%>
    </asp:MultiView>



</asp:Content>

