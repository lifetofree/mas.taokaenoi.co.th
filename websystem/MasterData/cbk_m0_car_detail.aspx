﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="cbk_m0_car_detail.aspx.cs" Inherits="websystem_MasterData_cbk_m0_car_detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>
    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbDetailCar" runat="server" CommandName="cmdDetailCar" OnCommand="navCommand" CommandArgument="docDetailCar"> รายการข้อมูลรถ</asp:LinkButton>
                        </li>


                        <li id="li3" runat="server">
                            <asp:LinkButton ID="lbDetailCarMA" runat="server" CommandName="cmdDetailCar" OnCommand="navCommand" CommandArgument="docDetailCarMA"> รายการ MA</asp:LinkButton>
                        </li>


                        <li id="li1" runat="server" class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">สร้างรายการ <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li id="liCreateDetail" runat="server">
                                    <asp:LinkButton ID="lbCreateDetail" runat="server" CommandName="cmdDetailCar" OnCommand="btnCommand" CommandArgument="1"> เพิ่มข้อมูลรถ</asp:LinkButton>
                                </li>


                                <li role="separator" class="divider"></li>

                                <li id="liCreateCar_MA" runat="server">
                                    <asp:LinkButton ID="lbCreateCar_MA" runat="server" CommandName="cmdCreateCarMA" OnCommand="btnCommand" CommandArgument="2"> เพิ่มรายการ MA</asp:LinkButton>
                                </li>

                            </ul>
                        </li>

                        <li id="li4" runat="server">
                            <asp:LinkButton ID="lbReport" runat="server" CommandName="cmdReport" OnCommand="navCommand" CommandArgument="docReport"> รายงาน</asp:LinkButton>
                        </li>


                        <li id="li2" runat="server" class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Master Data MA <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li id="lilblCreateTopicMA" runat="server">
                                    <asp:LinkButton ID="lblCreateTopicMA" runat="server" CommandName="cmdMasterTopicCar" OnCommand="btnCommand" CommandArgument="1"> หัวข้อ MA</asp:LinkButton>
                                </li>
                                <%--<li role="separator" class="divider"></li>--%>
                            </ul>
                        </li>

                    </ul>

                    <ul class="nav navbar-nav navbar-right" runat="server">

                        <li id="_divMenuLiToDocument" runat="server">
                            <asp:HyperLink ID="_divMenuBtnToDocument" runat="server"
                                NavigateUrl="https://docs.google.com/document/d/1F-Z6qprH8TKCfKuKbK5AlhITjDJT7CFQj9-OSBc5YMc/edit?usp=sharing" Target="_blank" Text="คู่มือการใช้งาน" />
                        </li>
                    </ul>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->

            </div>
        </nav>
    </div>
    <!--tab menu-->

    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <asp:Literal ID="litDebug1" runat="server"></asp:Literal>
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">

        <!--View Detail Car-->
        <asp:View ID="docDetailCar" runat="server">
            <div class="col-md-12">

                <div id="div_GvCarDetail" style="overflow-x: scroll; width: 100%" runat="server">
                    <asp:GridView ID="GvCarDetail" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        ShowHeaderWhenEmpty="True"
                        AllowPaging="True" PageSize="5">
                        <HeaderStyle CssClass="success" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                        </EmptyDataTemplate>
                        <Columns>


                            <asp:TemplateField HeaderText="รูปรถ" ItemStyle-HorizontalAlign="center" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                <ItemTemplate>

                                    <asp:HyperLink runat="server" ID="btnViewFileCar" CssClass="pull-letf" data-toggle="tooltip" title="View" data-original-title="" Target="_blank">
                                    <i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียดรถ" HeaderStyle-CssClass="left" ItemStyle-Width="35%" ItemStyle-CssClass="text-left">

                                <HeaderTemplate>
                                    <%-- <label class="col-sm-4 control-label">สถานะรายการ : </label>
                                <div class="col-sm-6">--%>
                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>ประเภทรถ</label>
                                                <asp:UpdatePanel ID="UpdatePanel_Header" runat="server">
                                                    <ContentTemplate>
                                                        <small>
                                                            <asp:DropDownList ID="ddlTypeCarFilter" runat="server" CssClass="form-control" Font-Size="Small"
                                                                OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </small>

                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>
                                    </div>
                                    <%-- </div>
                                <label class="col-sm-2 control-label"></label>--%>
                                </HeaderTemplate>

                                <ItemTemplate>

                                    <p>
                                        <b>ประเภทรถ:</b>
                                        <asp:Label ID="lbl_m0_car_idx_detail" Visible="false" runat="server" Text='<%# Eval("m0_car_idx") %>' />
                                        <asp:Label ID="lbl_type_car_idx_detail" Visible="false" runat="server" Text='<%# Eval("type_car_idx") %>' />
                                        <asp:Label ID="lbl_type_car_name_detail" runat="server" Text='<%# Eval("type_car_name") %>' />
                                    </p>
                                    <p>
                                        <b>รายละเอียดประเภทรถ:</b>
                                        <asp:Label ID="lbl_detailtype_car_idx_detail" Visible="false" runat="server" Text='<%# Eval("detailtype_car_idx") %>' />
                                        <asp:Label ID="lbl_detailtype_car_name_detail" runat="server" Text='<%# Eval("detailtype_car_name") %>' />
                                    </p>

                                    <p>
                                        <b>ทะเบียนรถ:</b>
                                        <asp:Label ID="lbl_car_register_detail" runat="server" Text='<%# Eval("car_register") %>' />
                                    </p>
                                    <p>
                                        <b>จังหวัด:</b>
                                        <asp:Label ID="lbl_car_province_idx_detail" Visible="false" runat="server" Text='<%# Eval("car_province_idx") %>' />
                                        <asp:Label ID="lbl_ProvName_detail" runat="server" Text='<%# Eval("ProvName") %>' />
                                    </p>

                                    <p>
                                        <b>วันครบกำหนดเสียภาษี:</b>
                                        <asp:Label ID="lbl_m1_car_idx_detail" Visible="false" runat="server" Text='<%# Eval("m1_car_idx") %>' />
                                        <asp:Label ID="lbl_tax_deadline_detail" runat="server" Text='<%# Eval("tax_deadline") %>' />
                                    </p>
                                    <p>
                                        <b>ค่าภาษี บาท/สต.:</b>
                                        <asp:Label ID="lbl_price_tax_detail" runat="server" Text='<%# Eval("price_tax") %>' />
                                    </p>

                                    <p>
                                        <b>วันครบกำหนดประกันภัยคุ้มครองผู้ประสบภัยจากรถ:</b>
                                        <asp:Label ID="lbl_date_insurance_term_detail" runat="server" Text='<%# Eval("date_insurance_term") %>' />
                                    </p>
                                    <p>
                                        <b>วันครบกำหนดประกันภัยรถยนต์:</b>
                                        <asp:Label ID="lbl_date_car_insurance_deadline_detail" runat="server" Text='<%# Eval("date_car_insurance_deadline") %>' />
                                    </p>

                                </ItemTemplate>

                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียดผู้ดูแลรถ" HeaderStyle-CssClass="text-center" ItemStyle-Width="30%" ItemStyle-CssClass="text-left">
                                <ItemTemplate>
                                    <p>
                                        <b>ผู้ดูแล:</b>
                                        <asp:Label ID="lbl_admin_idx_detail" Visible="false" runat="server" Text='<%# Eval("admin_idx") %>' />
                                        <asp:Label ID="lbl_admin_emp_name_th_detail" runat="server" Text='<%# Eval("admin_emp_name_th") %>' />
                                    </p>
                                    <p>
                                        <b>ฝ่าย:</b>
                                        <asp:Label ID="lbl_admin_rdept_idx_detail" Visible="false" runat="server" Text='<%# Eval("admin_rdept_idx") %>' />
                                        <asp:Label ID="lbl_admin_dept_name_th_detail" runat="server" Text='<%# Eval("admin_dept_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>แผนก:</b>
                                        <asp:Label ID="lbl_admin_rsec_idx_detail" Visible="false" runat="server" Text='<%# Eval("admin_rsec_idx") %>' />
                                        <asp:Label ID="lbl_admin_sec_name_th_detail" runat="server" Text='<%# Eval("admin_sec_name_th") %>' />
                                    </p>
                                    <p>
                                        <b>Cost Center:</b>
                                        <asp:Label ID="lbl_costcenter_idx_detail" Visible="false" runat="server" Text='<%# Eval("costcenter_idx") %>' />
                                        <asp:Label ID="lbl_CostNo_detail" runat="server" Text='<%# Eval("CostNo") %>' />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <%--<asp:TemplateField HeaderText="รายละเอียดประกัน" HeaderStyle-CssClass="text-center" ItemStyle-Width="25%" ItemStyle-CssClass="text-left">
                            <ItemTemplate>
                                <p>
                                    <b>วันครบกำหนดเสียภาษี:</b>
                                    <asp:Label ID="lbl_m1_car_idx_detail" Visible="false" runat="server" Text='<%# Eval("m1_car_idx") %>' />
                                    <asp:Label ID="lbl_tax_deadline_detail" runat="server" Text='<%# Eval("tax_deadline") %>' />
                                </p>
                                <p>
                                    <b>ค่าภาษี บาท/สต.:</b>
                                    <asp:Label ID="lbl_price_tax_detail" runat="server" Text='<%# Eval("price_tax") %>' />
                                </p>

                                <p>
                                    <b>วันครบกำหนดประกันภัยคุ้มครองผู้ประสบภัยจากรถ:</b>
                                    <asp:Label ID="lbl_date_insurance_term_detail" runat="server" Text='<%# Eval("date_insurance_term") %>' />
                                </p>
                                <p>
                                    <b>วันครบกำหนดประกันภัยรถยนต์:</b>
                                    <asp:Label ID="lbl_date_car_insurance_deadline_detail" runat="server" Text='<%# Eval("date_car_insurance_deadline") %>' />
                                </p>
                            </ItemTemplate>
                        </asp:TemplateField>--%>

                            <asp:TemplateField HeaderText="สถานะ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-left" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_m0_car_status" Visible="false" runat="server"
                                        CssClass="col-sm-12" Text='<%# Eval("m0_car_status") %>'></asp:Label>
                                    <asp:Label ID="m0_car_statusOnline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Online"
                                        CssClass="col-sm-12"><div style="text-align: center; color:green;"><span><b>Online</b></span>
                                    </asp:Label>
                                    <asp:Label ID="m0_car_statusOffline" runat="server" Visible="false"
                                        data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12"><div style="text-align: center; color:red;"><span><b>Offline</b></span>
                                    </asp:Label>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:UpdatePanel ID="UpdatePnFileUploadEdit" runat="server">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="btnViewCarDetail" CssClass="btn btn-info btn-xs" target="" runat="server" CommandName="cmdViewCarDetail" OnCommand="btnCommand"
                                                CommandArgument='<%# Eval("m0_car_idx")%>'
                                                data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>

                                            <asp:LinkButton ID="btnDeleteCarDetail" CssClass="btn btn-danger btn-xs" target="" runat="server" CommandName="cmdDeleteCarDetail" OnCommand="btnCommand"
                                                CommandArgument='<%# Eval("m0_car_idx")%>' OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                                data-toggle="tooltip" title="ลบข้อมูล"><i class="fa fa-trash"></i></asp:LinkButton>
                                        </ContentTemplate>

                                    </asp:UpdatePanel>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>

                <!-- Form Detail Car -->
                <asp:FormView ID="FvDetailCar" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                    <ItemTemplate>
                        <asp:UpdatePanel ID="UpdatePnFileUploadEdit_Car" runat="server">
                            <ContentTemplate>
                                <asp:LinkButton ID="btnBackToDetailIndexCar" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand"
                                    CommandName="cmdBackToDetailIndexCar"><i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ</asp:LinkButton>

                                <div class="form-group pull-right">
                                    <asp:LinkButton ID="btnEditDetailCar" CssClass="btn btn-default" runat="server" CommandName="cmdEditDetailCar" OnCommand="btnCommand" CommandArgument='<%# Eval("m0_car_idx")%>' Text="แก้ไขข้อมูลรถ" />
                                    <asp:LinkButton ID="btnInsertTaxCar" CssClass="btn btn-success" runat="server" CommandName="cmdInsertTaxCar" CommandArgument='<%# Eval("m0_car_idx")%>' OnCommand="btnCommand" Text="เพิ่มวันเสียภาษีรอบใหม่" />
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnEditDetailCar" />
                            </Triggers>
                        </asp:UpdatePanel>

                        <label>&nbsp;</label>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="panel panel-info">

                                <div class="panel-heading f-bold">รายละเอียดข้อมูลรถ</div>

                                <div class="panel-body">
                                    <div class="col-md-12">

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lbltype_car_name" runat="server" CssClass="f-s-13 f-bold" Text="ประเภทรถ :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblm0_car_idx_detail" runat="server" Visible="false" Text='<%# Eval("m0_car_idx") %>' CssClass="f-s-13" />
                                            <asp:Label ID="lbltype_car_name_detail" runat="server" Text='<%# Eval("type_car_name") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lbldetailtype_car_name" runat="server" CssClass="f-s-13 f-bold" Text="รายละเอียดประเภทรถ :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lbldetailtype_car_name_detail" runat="server" Text='<%# Eval("detailtype_car_name") %>' CssClass="f-s-13" />
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblcar_register" runat="server" CssClass="f-s-13 f-bold" Text="ทะเบียนรถ :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblcar_register_detail" runat="server" Text='<%# Eval("car_register") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblProvName" runat="server" CssClass="f-s-13 f-bold" Text="จังหวัด :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblProvName_detail" runat="server" Text='<%# Eval("ProvName") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lbladmin_emp_name_th" runat="server" CssClass="f-s-13 f-bold" Text="ผู้ดูแล :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lbladmin_emp_name_th_detail" runat="server" Text='<%# Eval("admin_emp_name_th") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblCostNo" runat="server" CssClass="f-s-13 f-bold" Text="CostCenter :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblCostNo_detail" runat="server" Text='<%# Eval("CostNo") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblasset_number" runat="server" CssClass="f-s-13 f-bold" Text="Asset :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblasset_number_detail" runat="server" Text='<%# Eval("asset_number") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lbltype_name" runat="server" CssClass="f-s-13 f-bold" Text="ประเภท :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lbltype_name_detail" runat="server" Text='<%# Eval("type_name") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblcar_look" runat="server" CssClass="f-s-13 f-bold" Text="ลักษณะ :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblcar_look_detail" runat="server" Text='<%# Eval("car_look") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblbrand_car_name" runat="server" CssClass="f-s-13 f-bold" Text="ยี่ห้อรถ :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblbrand_car_name_detail" runat="server" Text='<%# Eval("brand_car_name") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblcar_model" runat="server" CssClass="f-s-13 f-bold" Text="แบบ :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblcar_model_detail" runat="server" Text='<%# Eval("car_model") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblcar_genaration" runat="server" CssClass="f-s-13 f-bold" Text="รุ่น ปี ค.ศ :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblcar_genaration_detail" runat="server" Text='<%# Eval("car_genaration") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblcar_color" runat="server" CssClass="f-s-13 f-bold" Text="สี :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblcar_color_detail" runat="server" Text='<%# Eval("car_color") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblcar_number" runat="server" CssClass="f-s-13 f-bold" Text="เลขตัวรถ :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblcar_number_detail" runat="server" Text='<%# Eval("car_number") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblcar_number_located" runat="server" CssClass="f-s-13 f-bold" Text="อยู่ที่ :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblcar_number_located_detail" runat="server" Text='<%# Eval("car_number_located") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblengine_brand_name" runat="server" CssClass="f-s-13 f-bold" Text="ยี่ห้อเครื่องยนต์ :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblengine_brand_name_detail" runat="server" Text='<%# Eval("engine_brand_name") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblengine_number" runat="server" CssClass="f-s-13 f-bold" Text="เลขที่เครื่องยนต์ :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblengine_number_detail" runat="server" Text='<%# Eval("engine_number") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblengine_number_located" runat="server" CssClass="f-s-13 f-bold" Text="อยู่ที่ :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblengine_number_located_detail" runat="server" Text='<%# Eval("engine_number_located") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblfuel_name" runat="server" CssClass="f-s-13 f-bold" Text="เชื้อเพลิง :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblfuel_name_detail" runat="server" Text='<%# Eval("fuel_name") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblgas_cylinder" runat="server" CssClass="f-s-13 f-bold" Text="เลขถังแก๊ส :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblgas_cylinder_detail" runat="server" Text='<%# Eval("gas_cylinder") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblcount_pump" runat="server" CssClass="f-s-13 f-bold" Text="จำนวน(สูบ) :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblcount_pump_detail" runat="server" Text='<%# Eval("count_pump") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblcc" runat="server" CssClass="f-s-13 f-bold" Text="ซีซี(cc) :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblcc_detail" runat="server" Text='<%# Eval("cc") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblhorse_power" runat="server" CssClass="f-s-13 f-bold" Text="แรงม้า :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblhorse_power_detail" runat="server" Text='<%# Eval("horse_power") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblcar_weight" runat="server" CssClass="f-s-13 f-bold" Text="นำหนักรถ(กก.) :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblcar_weight_detail" runat="server" Text='<%# Eval("car_weight") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblcar_load" runat="server" CssClass="f-s-13 f-bold" Text="น้ำหนักบรรทุก/น้ำหนักลงเพลา :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblcar_load_detail" runat="server" Text='<%# Eval("car_load") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lbltotal_weight" runat="server" CssClass="f-s-13 f-bold" Text="นำหนักรวม :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lbltotal_weight_detail" runat="server" Text='<%# Eval("total_weight") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblcount_seat" runat="server" CssClass="f-s-13 f-bold" Text="จำนวนที่นั่ง :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblcount_seat_detail" runat="server" Text='<%# Eval("count_seat") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblm0_car_status_name" runat="server" CssClass="f-s-13 f-bold" Text="สถานะ :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblm0_car_status_name_detail" runat="server" Text='<%# Eval("m0_car_status_name") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lbl_m1_car_idx" runat="server" Visible="false" CssClass="f-s-13 f-bold" Text='<%# Eval("m1_car_idx") %>' />
                                            <asp:Label ID="lbltax_deadline" runat="server" CssClass="f-s-13 f-bold" Text="วันครบกำหนดเสียภาษี :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lbltax_deadline_detail" runat="server" Text='<%# Eval("tax_deadline") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lblprice_tax" runat="server" CssClass="f-s-13 f-bold" Text="ค่าภาษี บาท/สต. :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lblprice_tax_detail" runat="server" Text='<%# Eval("price_tax") %>' CssClass="f-s-13" />
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lbldate_insurance_term" runat="server" CssClass="f-s-13 f-bold" Text="วันครบกำหนดประกันภัยคุ้มครองผู้ประสบภัยจากรถ :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lbldate_insurance_term_detail" runat="server" Text='<%# Eval("date_insurance_term") %>' CssClass="f-s-13" />
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="lbldate_car_insurance_deadline" runat="server" CssClass="f-s-13 f-bold" Text="วันครบกำหนดประกันภัยรถยนต์ :" />
                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:Label ID="lbldate_car_insurance_deadline_detail" runat="server" Text='<%# Eval("date_car_insurance_deadline") %>' CssClass="f-s-13" />
                                        </div>


                                        <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="Label1" runat="server" CssClass="f-s-13 f-bold" Text="รายละเอียดไฟล์แนบ :" />

                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:HyperLink runat="server" ID="btnViewCarDetail" CssClass="f-s-13" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </ItemTemplate>

                    <EditItemTemplate>
                        <div class="row">
                            <div class="panel panel-info">
                                <div class="panel-heading f-bold">แก้ไขข้อมูลรถ</div>

                                <div class="panel-body">

                                    <div class="col-md-12">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>เลือกประเภทรถ</label>
                                                <asp:Label ID="lbl_m0_car_idx_edit" runat="server" Visible="false" Text='<%# Eval("m0_car_idx") %>' CssClass="f-s-13" />
                                                <asp:TextBox ID="txt_type_car_idx" runat="server" CssClass="form-control" Text='<%# Eval("type_car_idx") %>' Enabled="false" Visible="false">
                                                </asp:TextBox>
                                                <asp:DropDownList ID="ddlTypeCar_edit" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Re_ddlTypeCar_edit"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddlTypeCar_edit" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกประเภทรถ"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender66" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlTypeCar_edit" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>เลือกรายละเอียดประเภทรถ</label>
                                                <asp:TextBox ID="txt_detailtype_car_idx" runat="server" CssClass="form-control" Text='<%# Eval("detailtype_car_idx") %>' Visible="false"></asp:TextBox>
                                                <asp:DropDownList ID="ddlDetailTypeCar_edit" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="--- เลือกรายละเอียดประเภทรถ ---" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ทะเบียนรถ</label>
                                                <asp:TextBox ID="txt_car_register" runat="server" CssClass="form-control" Text='<%# Eval("car_register") %>' placeHolder="กรอกทะเบียนรถ">
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_car_register"
                                                    runat="server"
                                                    ControlToValidate="txt_car_register" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกทะเบียนรถ"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender766" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_register" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>จังหวัด</label>
                                                <asp:TextBox ID="txt_car_province_idx" runat="server" CssClass="form-control" Text='<%# Eval("car_province_idx") %>' Visible="false"></asp:TextBox>
                                                <asp:DropDownList ID="ddlcar_province" runat="server" CssClass="form-control">
                                                    <%-- <asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Re_ddlcar_province"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddlcar_province" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกจังหวัด"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender166" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlcar_province" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ผู้ดูแล</label>
                                                <asp:TextBox ID="txt_admin_idx" runat="server" CssClass="form-control" Text='<%# Eval("admin_idx") %>' Visible="false"></asp:TextBox>
                                                <asp:DropDownList ID="ddladmin_idx" runat="server" CssClass="form-control">
                                                    <%-- <asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Re_ddladmin_idx"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddladmin_idx" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกผู้ดูแล"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender266" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddladmin_idx" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>CostCenter</label>
                                                <asp:TextBox ID="txt_costcenter_idx" runat="server" CssClass="form-control" Text='<%# Eval("costcenter_idx") %>' Visible="false"></asp:TextBox>
                                                <asp:DropDownList ID="ddlcostcenter_idx" runat="server" CssClass="form-control">
                                                    <%-- <asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Re_ddlcostcenter_idx"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddlcostcenter_idx" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือก CostCenter"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender333" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlcostcenter_idx" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Asset</label>
                                                <asp:TextBox ID="txt_asset" runat="server" CssClass="form-control" placeHolder="กรอกเลขที่สินทรัพย์" Text='<%# Eval("asset_number") %>'>
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_asset"
                                                    runat="server"
                                                    ControlToValidate="txt_asset" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอก Asset"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender466" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_asset" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ประเภท</label>
                                                <asp:TextBox ID="txt_type_idx" runat="server" CssClass="form-control" Text='<%# Eval("type_idx") %>' Visible="false"></asp:TextBox>
                                                <asp:DropDownList ID="ddltype_idx" runat="server" CssClass="form-control">
                                                    <%-- <asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Re_ddltype_idx"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddltype_idx" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกประเภท"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender566" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddltype_idx" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ลักษณะ</label>
                                                <asp:TextBox ID="txt_car_look" runat="server" CssClass="form-control" placeHolder="กรอกลักษณะรถ" Text='<%# Eval("car_look") %>'>
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_car_look"
                                                    runat="server"
                                                    ControlToValidate="txt_car_look" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกลักษณะ"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6333" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_look" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ยี่ห้อรถ</label>
                                                <asp:TextBox ID="txt_brand_car_idx" runat="server" CssClass="form-control" Text='<%# Eval("brand_car_idx") %>' Visible="false"></asp:TextBox>
                                                <asp:DropDownList ID="ddlbrand_car_idx" runat="server" CssClass="form-control">
                                                    <%-- <asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Re_ddlbrand_car_idx"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddlbrand_car_idx" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกยี่ห้อรถ"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8666" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlbrand_car_idx" Width="200" PopupPosition="BottomLeft" />


                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>แบบ</label>
                                                <asp:TextBox ID="txt_carmodel" runat="server" CssClass="form-control" placeHolder="กรอกแบบรถ" Text='<%# Eval("car_model") %>'>
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_carmodel"
                                                    runat="server"
                                                    ControlToValidate="txt_carmodel" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกแบบ"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9666" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_carmodel" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>รุ่น ปี ค.ศ</label>
                                                <asp:TextBox ID="txt_car_genaration" runat="server" CssClass="form-control" placeHolder="กรอกรุ่น ปี ค.ศ." Text='<%# Eval("car_genaration") %>'>
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_car_genaration"
                                                    runat="server"
                                                    ControlToValidate="txt_car_genaration" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอก รุ่น ปี ค.ศ"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1055" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_genaration" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>สี</label>
                                                <asp:TextBox ID="txt_car_color" runat="server" CssClass="form-control" placeHolder="กรอกสีรถ" Text='<%# Eval("car_color") %>'>
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_car_color"
                                                    runat="server"
                                                    ControlToValidate="txt_car_color" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกสี"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1155" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_color" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>เลขตัวรถ</label>
                                                <asp:TextBox ID="txt_car_number" runat="server" CssClass="form-control" placeHolder="กรอกเลขตัวรถ" Text='<%# Eval("car_number") %>'>
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_car_number"
                                                    runat="server"
                                                    ControlToValidate="txt_car_number" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกเลขตัวรถ"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1255" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_number" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>อยู่ที่</label>
                                                <asp:TextBox ID="txt_car_number_located" runat="server" CssClass="form-control" placeHolder="อยู่ที่" Text='<%# Eval("car_number_located") %>'>
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_car_number_located"
                                                    runat="server"
                                                    ControlToValidate="txt_car_number_located" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกอยู่ที่"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_number_located" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <asp:TextBox ID="txt_engine_brand_idx" runat="server" CssClass="form-control" Text='<%# Eval("engine_brand_idx") %>' Visible="false"></asp:TextBox>
                                                <label>ยี่ห้อเครื่องยนต์</label>
                                                <asp:DropDownList ID="ddl_engine_brand_idx_edit" runat="server" CssClass="form-control">
                                                    <%-- <asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Re_ddl_engine_brand_idx_edit"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddl_engine_brand_idx_edit" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกยี่ห้อเครื่องยนต์"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1444" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddl_engine_brand_idx_edit" Width="200" PopupPosition="BottomLeft" />


                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>เลขที่เครื่องยนต์</label>
                                                <asp:TextBox ID="txt_engine_brand_edit" runat="server" CssClass="form-control" placeHolder="กรอกเลขที่เครื่องยนต์" Text='<%# Eval("engine_number") %>'>
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_engine_brand_edit"
                                                    runat="server"
                                                    ControlToValidate="txt_engine_brand_edit" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกเลขที่เครื่องยนต์"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1544" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_engine_brand_edit" Width="200" PopupPosition="BottomLeft" />


                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>อยู่ที่</label>
                                                <asp:TextBox ID="txt_engine_brand_located_edit" runat="server" CssClass="form-control" placeHolder="อยู่ที่" Text='<%# Eval("engine_number_located") %>'>
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_engine_brand_located_edit"
                                                    runat="server"
                                                    ControlToValidate="txt_engine_brand_located_edit" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกอยู่ที่"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1611" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_engine_brand_located_edit" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <asp:TextBox ID="txt_fuel_idx" runat="server" CssClass="form-control" Text='<%# Eval("fuel_idx") %>' Visible="false"></asp:TextBox>
                                                <label>เชื้อเพลิง</label>
                                                <asp:DropDownList ID="ddlfuel_idx_edit" runat="server" CssClass="form-control">
                                                    <%-- <asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Re_ddlfuel_idx_edit"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddlfuel_idx_edit" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกเชื้อเพลิง"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1711" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlfuel_idx_edit" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>เลขถังแก๊ส</label>
                                                <asp:TextBox ID="txt_gas_cylinder" runat="server" CssClass="form-control" placeHolder="กรอกเลขถังแก๊ส" Text='<%# Eval("gas_cylinder") %>'>
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_gas_cylinder"
                                                    runat="server"
                                                    ControlToValidate="txt_gas_cylinder" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกเลขถังแก๊ส"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1866" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_gas_cylinder" Width="200" PopupPosition="BottomLeft" />


                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>จำนวน(สูบ)</label>
                                                <asp:TextBox ID="txt_count_pump" runat="server" CssClass="form-control" placeHolder="กรอกจำนวน(สูบ)" Text='<%# Eval("count_pump") %>'>
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_count_pump"
                                                    runat="server"
                                                    ControlToValidate="txt_count_pump" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกจำนวน(สูบ)"
                                                    ValidationGroup="SaveEdit" />


                                                <asp:RegularExpressionValidator ID="Re_txt_count_pump1" runat="server" ValidationGroup="SaveEdit" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txt_count_pump"
                                                    ValidationExpression="^\d+" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1933" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_count_pump" Width="200" PopupPosition="BottomLeft" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3033" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_count_pump1" Width="200" PopupPosition="BottomLeft" />


                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ซีซี(cc)</label>
                                                <asp:TextBox ID="txt_cc" runat="server" CssClass="form-control" placeHolder="กรอกซีซี" Text='<%# Eval("cc") %>'>
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_cc"
                                                    runat="server"
                                                    ControlToValidate="txt_cc" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกซีซี(cc)"
                                                    ValidationGroup="SaveEdit" />

                                                <asp:RegularExpressionValidator ID="Re_txt_cc1" runat="server" ValidationGroup="SaveEdit" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txt_cc"
                                                    ValidationExpression="^\d+" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2033" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_cc" Width="200" PopupPosition="BottomLeft" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3133" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_cc1" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>แรงม้า</label>
                                                <asp:TextBox ID="txt_horse_power" runat="server" CssClass="form-control" placeHolder="กรอกค่าแรงม้า" Text='<%# Eval("horse_power") %>'>
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_horse_power"
                                                    runat="server"
                                                    ControlToValidate="txt_horse_power" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกแรงม้า"
                                                    ValidationGroup="SaveEdit" />

                                                <asp:RegularExpressionValidator ID="Re_txt_horse_power1" runat="server" ValidationGroup="SaveEdit" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txt_horse_power"
                                                    ValidationExpression="^\d+" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2122" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_horse_power" Width="200" PopupPosition="BottomLeft" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3222" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_horse_power1" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>นำหนักรถ(กก.)</label>
                                                <asp:TextBox ID="txt_car_weight" runat="server" CssClass="form-control" placeHolder="กรอกนำหนักรถ(กก.)" Text='<%# Eval("car_weight") %>'>
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_car_weight"
                                                    runat="server"
                                                    ControlToValidate="txt_car_weight" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกนำหนักรถ(กก.)"
                                                    ValidationGroup="SaveEdit" />

                                                <asp:RegularExpressionValidator ID="Re_txt_car_weight1" runat="server" ValidationGroup="SaveEdit" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txt_car_weight"
                                                    ValidationExpression="^\d+" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2244" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_weight" Width="200" PopupPosition="BottomLeft" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3344" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_weight1" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>น้ำหนักบรรทุก/น้ำหนักลงเพลา</label>
                                                <asp:TextBox ID="txt_car_load" runat="server" CssClass="form-control" placeHolder="กรอกน้ำหนักบรรทุก/น้ำหนักลงเพลา" Text='<%# Eval("car_load") %>'>
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_car_load"
                                                    runat="server"
                                                    ControlToValidate="txt_car_load" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกน้ำหนักบรรทุก/น้ำหนักลงเพลา"
                                                    ValidationGroup="SaveEdit" />

                                                <asp:RegularExpressionValidator ID="Re_txt_car_load1" runat="server" ValidationGroup="SaveEdit" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txt_car_load"
                                                    ValidationExpression="^\d+" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender235" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_load" Width="200" PopupPosition="BottomLeft" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender345" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_load1" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>นำหนักรวม</label>
                                                <asp:TextBox ID="txt_total_weight" runat="server" CssClass="form-control" placeHolder="กรอกนำหนักรวม" Text='<%# Eval("total_weight") %>'>
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_total_weight"
                                                    runat="server"
                                                    ControlToValidate="txt_total_weight" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกนำหนักรวม"
                                                    ValidationGroup="SaveEdit" />

                                                <asp:RegularExpressionValidator ID="Re_txt_total_weight1" runat="server" ValidationGroup="SaveEdit" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txt_total_weight"
                                                    ValidationExpression="^\d+" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender242222" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_total_weight" Width="200" PopupPosition="BottomLeft" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender352222" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_total_weight1" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>จำนวนที่นั่ง</label>
                                                <asp:TextBox ID="txt_count_seat" runat="server" CssClass="form-control" placeHolder="กรอกจำนวนที่นั่ง" Text='<%# Eval("count_seat") %>'>
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_count_seat"
                                                    runat="server"
                                                    ControlToValidate="txt_count_seat" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกจำนวนที่นั่ง"
                                                    ValidationGroup="SaveEdit" />

                                                <asp:RegularExpressionValidator ID="Re_txt_count_seat1" runat="server" ValidationGroup="SaveEdit" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txt_count_seat"
                                                    ValidationExpression="^\d+" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2599" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_count_seat" Width="200" PopupPosition="BottomLeft" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3699" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_count_seat1" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>สถานะ</label>
                                                <asp:DropDownList ID="ddlm0_car_status_edit" runat="server" CssClass="form-control" SelectedValue='<%# Eval("m0_car_status") %>'>
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>วันครบกำหนดเสียภาษี</label>
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txt_m1_car_idx_edit" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("m1_car_idx") %>'>
                                                    </asp:TextBox>
                                                    <asp:TextBox ID="txt_tax_deadline" runat="server" CssClass="form-control from-deadline-datepicker"
                                                        placeHolder="กรอกวันครบกำหนดเสียภาษี" Text='<%# Eval("tax_deadline") %>'></asp:TextBox>
                                                    <span class="input-group-addon showdeadline-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>

                                                    <asp:RequiredFieldValidator ID="Re_txt_tax_deadline"
                                                        runat="server"
                                                        ControlToValidate="txt_tax_deadline" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกวันครบกำหนดเสียภาษี"
                                                        ValidationGroup="SaveEdit" />


                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender26888" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_tax_deadline" Width="200" PopupPosition="BottomLeft" />



                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ค่าภาษี บาท/สต.</label>
                                                <asp:TextBox ID="txt_price_tax" runat="server" CssClass="form-control"
                                                    placeHolder="กรอกค่าภาษี บาท/สต." Text='<%# Eval("price_tax") %>'></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_price_tax"
                                                    runat="server"
                                                    ControlToValidate="txt_price_tax" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกค่าภาษี บาท/สต."
                                                    ValidationGroup="SaveEdit" />

                                                <asp:RegularExpressionValidator ID="Re_txt_price_tax1" runat="server" ValidationGroup="SaveEdit" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txt_price_tax"
                                                    ValidationExpression="^\d+" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender27999" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_price_tax" Width="200" PopupPosition="BottomLeft" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender37999" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_price_tax1" Width="200" PopupPosition="BottomLeft" />


                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>วันครบกำหนดประกันภัยคุ้มครองผู้ประสบภัยจากรถ</label>
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txt_date_insurance_term" runat="server" CssClass="form-control from-dateinsurance-datepicker" Text='<%# Eval("date_insurance_term") %>'
                                                        placeHolder="กรอกวันครบกำหนดประกันภัยคุ้มครองผู้ประสบภัยจากรถ"></asp:TextBox>
                                                    <span class="input-group-addon showinsurance-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>


                                                    <asp:RequiredFieldValidator ID="Re_txt_date_insurance_term"
                                                        runat="server"
                                                        ControlToValidate="txt_date_insurance_term" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกวันครบกำหนดประกันภัยคุ้มครองผู้ประสบภัยจากรถ"
                                                        ValidationGroup="SaveEdit" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender28999" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_date_insurance_term" Width="200" PopupPosition="BottomLeft" />

                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>วันครบกำหนดประกันภัยรถยนต์</label>
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txt_date_car_insurance_deadline" runat="server" CssClass="form-control from-date-datepicker" Text='<%# Eval("date_car_insurance_deadline") %>'
                                                        placeHolder="กรอกวันครบกำหนดประกันภัยรถยนต์"></asp:TextBox>
                                                    <span class="input-group-addon show-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>

                                                    <asp:RequiredFieldValidator ID="Re_txt_date_car_insurance_deadline"
                                                        runat="server"
                                                        ControlToValidate="txt_date_car_insurance_deadline" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกวันครบกำหนดประกันภัยรถยนต์"
                                                        ValidationGroup="SaveEdit" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender29" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_date_car_insurance_deadline" Width="200" PopupPosition="BottomLeft" />

                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>รายละเอียดไฟล์</label>
                                                <%--<asp:HyperLink runat="server" ID="btnViewFileRoomEdit" CssClass="" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>--%>
                                                <asp:HyperLink runat="server" ID="btnViewCarDetailEdit" CssClass="" Target="_blank"><i class="fa fa-file"></i> ดูรายละเอียด</asp:HyperLink>


                                            </div>
                                        </div>


                                        <%-- <div class="col-md-3 m-b-5 col-md-offset-2">
                                            <asp:Label ID="Label1" runat="server" CssClass="f-s-13 f-bold" Text="รายละเอียดไฟล์แนบ :" />

                                        </div>
                                        <div class="col-md-7 m-b-5">
                                            <asp:HyperLink runat="server" ID="btnViewCarDetail" CssClass="f-s-13" Target="_blank"><i class="fa fa-file-text-o"></i> ดูรายละเอียด</asp:HyperLink>
                                        </div>--%>


                                        <asp:UpdatePanel ID="UpdatePnFileUpload_View" runat="server">
                                            <ContentTemplate>
                                                <div class="col-md-12">
                                                    <asp:Panel ID="pnUploadfileCar_View" runat="server">
                                                        <div class="form-group">
                                                            <label>แนบไฟล์</label>
                                                            <%--<div class="col-sm-10">--%> <%--CssClass="form-control from-date-datepicker"--%>
                                                            <asp:FileUpload ID="UploadFileCarEdit" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                                CssClass="control-label UploadFileCarEdit multi max-1" accept="jpg|pdf|png" />
                                                            <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpg|pdf|png</font></p>

                                                            <%-- <asp:FileUpload ID="FileUpload1" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                                CssClass="control-label multi max-1" accept="jpg|pdf|png" />
                                                            <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpg|pdf|png</font></p>--%>
                                                        </div>
                                                    </asp:Panel>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="pull-right">
                                                            <asp:LinkButton ID="btnSaveUpdate" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                                                CommandName="cmdSaveEdit" CommandArgument='<%# Eval("m0_car_idx") %>' Text="บันทึกการเปลี่ยนแปลง" ValidationGroup="SaveEdit"
                                                                OnClientClick="return confirm('ยืนยันการบันทึก')" />
                                                            <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand"
                                                                CommandName="CmdCancel" Text="ยกเลิก" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnSaveUpdate" />
                                            </Triggers>
                                        </asp:UpdatePanel>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </EditItemTemplate>

                </asp:FormView>
                <!-- Form Detail Car -->

                <!-- Form Tax Insert -->
                <asp:FormView ID="FvInsertTax" runat="server" Width="100%">
                    <InsertItemTemplate>

                        <div class="panel panel-info">
                            <div class="panel-heading f-bold">เพิ่มวันเสียภาษีรอบใหม่</div>
                            <div class="panel-body">
                                <%--<div class="col-md-12">--%>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>วันครบกำหนดเสียภาษี</label>
                                        <div class='input-group date'>
                                            <asp:TextBox ID="txt_tax_deadline_add" runat="server" CssClass="form-control from-deadline-datepicker"
                                                placeHolder="กรอกวันครบกำหนดเสียภาษี" ValidationGroup="SaveTax"></asp:TextBox>
                                            <span class="input-group-addon showdeadline-from-onclick">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>

                                            <asp:RequiredFieldValidator ID="Re_txt_tax_deadline_add"
                                                runat="server"
                                                ControlToValidate="txt_tax_deadline_add" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรอกวันครบกำหนดเสียภาษี"
                                                ValidationGroup="SaveTax" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_tax_deadline_add" Width="200" PopupPosition="BottomLeft" />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>ค่าภาษี บาท/สต.</label>
                                        <asp:TextBox ID="txt_price_tax_add" runat="server" CssClass="form-control" placeHolder="กรอกค่าภาษี บาท/สต." ValidationGroup="SaveTax" />

                                        <asp:RequiredFieldValidator ID="Re_txt_price_tax_add"
                                            runat="server"
                                            ControlToValidate="txt_price_tax_add" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรอกค่าภาษี บาท/สต."
                                            ValidationGroup="SaveTax" />

                                        <asp:RegularExpressionValidator ID="Re_txt_price_tax_add1" runat="server" ValidationGroup="SaveCreate" Display="None"
                                            ErrorMessage="กรุณากรอกเฉพาะตัวเลข หรือทศนิยมเท่านั้น"
                                            ControlToValidate="txt_price_tax_add"
                                            ValidationExpression="^\d+\d*\.?\d*$" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender38" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_price_tax_add" Width="200" PopupPosition="BottomLeft" />

                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender39" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_price_tax_add1" Width="200" PopupPosition="BottomLeft" />
                                    </div>
                                </div>

                                <%-- <div class="clearfix"></div>--%>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>วันครบกำหนดประกันภัยคุ้มครองผู้ประสบภัยจากรถ</label>
                                        <div class='input-group date'>
                                            <asp:TextBox ID="txt_date_insurance_term_add" runat="server" CssClass="form-control from-dateinsurance-datepicker"
                                                placeHolder="กรอกวันครบกำหนดประกันภัยคุ้มครองผู้ประสบภัยจากรถ" ValidationGroup="SaveTax"></asp:TextBox>
                                            <span class="input-group-addon showinsurance-from-onclick">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>

                                            <asp:RequiredFieldValidator ID="Re_txt_date_insurance_term_add"
                                                runat="server"
                                                ControlToValidate="txt_date_insurance_term_add" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรอกวันครบกำหนดประกันภัยคุ้มครองผู้ประสบภัยจากรถ"
                                                ValidationGroup="SaveTax" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender40" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_date_insurance_term_add" Width="200" PopupPosition="BottomLeft" />
                                        </div>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>วันครบกำหนดประกันภัยรถยนต์</label>
                                        <div class='input-group date'>
                                            <asp:TextBox ID="txt_date_car_insurance_deadline_add" runat="server" CssClass="form-control from-date-datepicker"
                                                placeHolder="กรอกวันครบกำหนดประกันภัยรถยนต์" ValidationGroup="SaveTax"></asp:TextBox>
                                            <span class="input-group-addon show-from-onclick">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>

                                            <asp:RequiredFieldValidator ID="Re_txt_date_car_insurance_deadline_add"
                                                runat="server"
                                                ControlToValidate="txt_date_car_insurance_deadline_add" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรอกวันครบกำหนดประกันภัยรถยนต์"
                                                ValidationGroup="SaveTax" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender41" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_date_car_insurance_deadline_add" Width="200" PopupPosition="BottomLeft" />

                                        </div>

                                    </div>
                                </div>

                                <asp:UpdatePanel ID="UpdatePn_InsertTax" runat="server">
                                    <ContentTemplate>

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="pull-right">
                                                    <asp:LinkButton ID="btnSaveTax" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                                        CommandName="CmdSaveTax" Text="บันทึก" ValidationGroup="SaveTax" />
                                                    <%-- <asp:LinkButton ID="btnSave" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                                                CommandName="CmdSave" Text="บันทึก" ValidationGroup="SaveDetailCar"
                                                                OnClientClick="return confirm('ยืนยันการบันทึก')" />--%>
                                                    <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand"
                                                        CommandName="CmdCancel" Text="ยกเลิก" />
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="btnSaveTax" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <%-- </div>--%>
                            </div>
                        </div>


                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form Tax Insert -->

                <div class="panel panel-info" id="div_LogTax" runat="server" visible="false">
                    <div class="panel-heading">ประวัติการเสียภาษีและต่อประกัน</div>
                    <table class="table table-striped f-s-12 table-empshift-responsive">
                        <asp:Repeater ID="rptLogTax" runat="server">
                            <HeaderTemplate>
                                <tr>
                                    <th>วันครบกำหนดเสียภาษา</th>
                                    <th>วันครบกำหนดประกันภัยคุ้มครองผู้ประสบภัยจากรถ</th>
                                    <th>วันครบกำหนดประกันภัยรถยนต์</th>
                                    <th>วัน-เวลาที่สร้าง</th>
                                    <th>ผู้สร้าง</th>
                                    <th>วัน-เวลาที่แก้ไข</th>
                                    <th>ผู้แก้ไข</th>
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td data-th="วันครบกำหนดเสียภาษา"><%# Eval("tax_deadline") %></td>
                                    <td data-th="วันครบกำหนดประกันภัยคุ้มครองผู้ประสบภัยจากรถ"><%# Eval("date_insurance_term") %></td>
                                    <td data-th="วันครบกำหนดประกันภัยรถยนต์"><%# Eval("date_car_insurance_deadline") %></td>
                                    <td data-th="วัน-เวลาที่สร้าง"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                    <td data-th="ผู้สร้าง"><%# Eval("emp_name_th") %></td>
                                    <td data-th="วัน-เวลาที่แก้ไข"><%# Eval("update_date") %> <%# Eval("time_update") %></td>
                                    <td data-th="ผู้แก้ไข"><%# Eval("emp_name_th_update") %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <div class="m-t-10 m-b-10"></div>
                </div>

                <div class="panel panel-default" runat="server" id="Panel_LogTax" visible="false">
                    <div class="panel-heading f-bold">ประวัติการเสียภาษีและต่อประกัน</div>
                    <div class="panel-body">
                        <div id="GvLogTax_scroll" style="overflow-x: scroll; width: 100%" runat="server">
                            <asp:GridView ID="GvLogTax" runat="server"
                                AutoGenerateColumns="False"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                OnPageIndexChanging="gvPageIndexChanging"
                                OnRowDataBound="gvRowDataBound"
                                AllowPaging="True" PageSize="10">
                                <HeaderStyle CssClass="default" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="วันครบกำหนดเสียภาษา" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:Label ID="lbl_tax_deadline_log" runat="server" Text='<%# Eval("tax_deadline") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="วันครบกำหนดประกันภัยคุ้มครองผู้ประสบภัยจากรถ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>

                                            <asp:Label ID="lbl_date_insurance_term_log" runat="server" Text='<%# Eval("date_insurance_term") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="วันครบกำหนดประกันภัยรถยนต์" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_date_car_insurance_deadline_log" runat="server" Text='<%# Eval("date_car_insurance_deadline") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <p>
                                                <b>วันที่สร้าง:</b>
                                                <asp:Label ID="lbl_create_date_log" runat="server" Text='<%# Eval("create_date") %> ' />
                                                <b>เวลา:</b>
                                                <asp:Label ID="lbl_time_create_log" runat="server" Text='<%# Eval("time_create") %> ' />
                                                <b>น.</b>
                                            </p>
                                            <p>
                                                <b>ชื่อ-สกุลผู้สร้าง:</b>
                                                <asp:Label ID="lbl_emp_name_th_log" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                            </p>
                                            <p>
                                                <b>วันที่แก้ไข:</b>
                                                <asp:Label ID="lbl_update_date_log" runat="server" Text='<%# Eval("update_date") %> ' />
                                                <b>เวลา:</b>
                                                <asp:Label ID="lbl_time_update_log" runat="server" Text='<%# Eval("time_update") %> ' />
                                                <b>น.</b>
                                            </p>
                                            <p>
                                                <b>ชื่อ-สกุลผู้แก้ไข:</b>
                                                <asp:Label ID="lbl_emp_name_th_update_log" runat="server" Text='<%# Eval("emp_name_th_update") %>' />
                                            </p>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbl_m1_car_status_name_log" runat="server" Text='<%# Eval("m1_car_status_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                </div>

            </div>
        </asp:View>
        <!--View Detail Car-->

        <!--View DocDetail MA-->
        <asp:View ID="docDetailCarMA" runat="server">

            <div class="col-md-12">

                <div class="panel panel-primary" id="Div_DetailCarMa" runat="server" visible="false">
                    <div class="panel-heading">
                        <h3 class="panel-title">รายการ MA รถ</h3>
                    </div>
                </div>
                <div id="divGVDetailCarMA_scroll_x" style="overflow-x: scroll; width: 100%" runat="server">
                    <asp:GridView ID="GVDetailCarMA" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        ShowHeaderWhenEmpty="True"
                        AllowPaging="True" PageSize="10">
                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="วันที่ทำรายการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" ItemStyle-Width="15%">
                                <ItemTemplate>
                                    <p>
                                        <asp:Label ID="lbl_u0_document_ma_idx_ma" runat="server" Visible="false" Text='<%# Eval("u0_document_ma_idx") %>'></asp:Label>
                                        <asp:Label ID="lbl_create_date_detailma" runat="server" Text='<%# Eval("create_date") %>'></asp:Label>
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_time_create_date_detailma" runat="server" Text='<%# Eval("time_create_date") %>'></asp:Label>
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="เลขทะเบียนรถ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" ItemStyle-Width="20%">
                                <ItemTemplate>

                                    <p>
                                        <asp:Label ID="lbl_car_register_detailma" runat="server" Text='<%# Eval("car_register") %>' />
                                    </p>
                                    <p>
                                        <asp:Label ID="lbl_ProvName_detailma" runat="server" Text='<%# Eval("ProvName") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียดรายการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-left" ItemStyle-Width="25%">

                                <ItemTemplate>

                                    <p>
                                        <b>ผู้บันทึกรายการ :</b>
                                        <asp:Label ID="lbl_cemp_idx_detailma" Visible="false" runat="server" Text='<%# Eval("cemp_idx") %>' />
                                        <asp:Label ID="lbl_emp_name_th_detailma" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                    </p>
                                    <p>
                                        <b>ฝ่าย :</b>
                                        <asp:Label ID="lbl_rdept_idx_detailma" Visible="false" runat="server" Text='<%# Eval("rdept_idx") %>' />
                                        <asp:Label ID="lbl_dept_name_th_detailma" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                    </p>

                                    <p>
                                        <b>แผนก :</b>
                                        <asp:Label ID="lbl_rsec_idx_detailma" Visible="false" runat="server" Text='<%# Eval("rsec_idx") %>' />
                                        <asp:Label ID="lbl_sec_name_th_detailma" runat="server" Text='<%# Eval("sec_name_th") %>' />
                                    </p>
                                    <p>
                                        <b>CostCenter :</b>
                                        <asp:Label ID="lbl_costcenter_idx_detailma" Visible="false" runat="server" Text='<%# Eval("costcenter_idx") %>' />
                                        <asp:Label ID="lbl_costcenter_no_detailma" runat="server" Text='<%# Eval("costcenter_no") %>' />
                                    </p>
                                </ItemTemplate>

                            </asp:TemplateField>

                            <%-- <asp:TemplateField HeaderText="รายละเอียดผู้ดูแลรถ" HeaderStyle-CssClass="text-center" Visible="false" ItemStyle-CssClass="text-left" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <p>
                                    <b>ผู้ดูแล :</b>

                                    <asp:Label ID="lbl_emp_name_th_driver_detailma" runat="server" Text='<%# Eval("emp_name_th_driver") %>' />
                                </p>
                                <p>
                                    <b>ฝ่าย :</b>
                                    <asp:Label ID="lbl_dept_name_th_driver_detailma" runat="server" Text='<%# Eval("dept_name_th_driver") %>' />
                                </p>

                                <p>
                                    <b>แผนก :</b>
                                    <asp:Label ID="lbl_admin_sec_name_th_detail" runat="server" Text='<%# Eval("sec_name_th_driver") %>' />
                                </p>
                                <p>
                                    <b>Cost Center :</b>
                                    <asp:Label ID="lbl_costcenter_idx_driver_detailma" Visible="false" runat="server" Text='<%# Eval("costcenter_idx_driver") %>' />
                                    <asp:Label ID="lbl_costcenter_no_driver_detailma" runat="server" Text='<%# Eval("costcenter_no_driver") %>' />
                                </p>

                            </ItemTemplate>
                        </asp:TemplateField>--%>

                            <asp:TemplateField HeaderText="สถานะรายการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" ItemStyle-Width="25%">
                                <ItemTemplate>

                                    <asp:Label ID="lbl_staidx_detail" runat="server" Text='<%# Eval("staidx") %>' Visible="false" />
                                    <asp:Label ID="lbl_status_name_detail" runat="server" Text='<%# Eval("status_name") %>' />
                                    <asp:Label ID="lbl_m0_node_idx_detail" runat="server" Text='<%# Eval("m0_node_idx") %>' Visible="false" />
                                    <asp:Label ID="lbl_node_name_detail" runat="server" Text='<%# Eval("node_name") %>' />
                                    <br />
                                    โดย<br />
                                    <asp:Label ID="lbl_m0_actor_idx_detail" runat="server" Text='<%# Eval("m0_actor_idx") %>' Visible="false" />
                                    <asp:Label ID="lbl_actor_name_detail" runat="server" Text='<%# Eval("actor_name") %>' />

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" ItemStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:UpdatePanel ID="UpdatePnFileUploadEdit" runat="server">
                                        <ContentTemplate>
                                            <asp:LinkButton ID="btnViewDetailCarMA" CssClass="btn btn-info btn-xs" target="" runat="server" CommandName="cmdViewDetailCarMA" OnCommand="btnCommand"
                                                CommandArgument='<%# Eval("u0_document_ma_idx")%>'
                                                data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>
                                        </ContentTemplate>

                                    </asp:UpdatePanel>
                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>

                <!-- Back To Detail Room Booking -->
                <asp:UpdatePanel ID="Update_BackToDetailCarMa" runat="server" Visible="false">
                    <ContentTemplate>
                        <div class="form-group">
                            <asp:LinkButton ID="btnBackToDetailCarMa" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                CommandName="cmdBackToDetailCarMa" OnCommand="btnCommand">< ย้อนกลับ</asp:LinkButton>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Back To Detail Room Booking -->

                <asp:FormView ID="FvViewDetailCarMa" runat="server" OnDataBound="FvDetail_DataBound" Width="100%">
                    <ItemTemplate>
                        <asp:HiddenField ID="hfu0_document_idx_ma" runat="server" Value='<%# Eval("u0_document_ma_idx") %>' />
                        <asp:HiddenField ID="hfM0NodeIDX_ma" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                        <asp:HiddenField ID="hfM0ActoreIDX_ma" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                        <asp:HiddenField ID="hfM0StatusIDX_ma" runat="server" Value='<%# Eval("staidx") %>' />

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดรายการ MA รถ</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">

                                        <asp:Label ID="lbl_emp_code_view_ma" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_u0_document_ma_idx_viewma" Visible="false" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("u0_document_ma_idx") %>' />
                                            <asp:Label ID="lbl_emp_code_viewma" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("emp_code") %>' />
                                            <asp:Label ID="lbl_cemp_idx_viewma" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("cemp_idx")%>' />
                                            <asp:Label ID="lbl_m0_car_idx_viewma" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("m0_car_idx")%>' />

                                        </div>

                                        <asp:Label ID="lbl_emp_name_th_ma" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_emp_name_th_viewma" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label3" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อผู้ดูแลรถ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_emp_name_th_driver_viewma" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("emp_name_th_driver")%>' />
                                            <asp:Label ID="lbl_emp_idx_viewma" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("emp_idx")%>' />
                                        </div>

                                        <asp:Label ID="Label5" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ฝ่าย : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_dept_name_th_driver_viewma" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("dept_name_th_driver")%>' />

                                        </div>


                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="แผนก : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_sec_name_th_driver_viewma" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("sec_name_th_driver")%>' />

                                        </div>

                                        <asp:Label ID="Label21" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="CostCenter : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_costcenter_no_driver_viewma" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("costcenter_no_driver")%>' />
                                            <asp:Label ID="lbl_costcenter_idx_driver_viewma" runat="server" CssClass="control-label" Visible="false" Text='<%# Eval("costcenter_idx_driver")%>' />
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เลขทะเบียน : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_car_register_viewma" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("car_register")%>' />

                                        </div>

                                        <asp:Label ID="lbl_dept_name_thma" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ลักษณะรถ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_car_look_viewma" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("car_look")%>' />

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label2" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เลขไมค์ครบระยะกำหนด : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_miles_start_viewma" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("miles_start")%>' />

                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="เลขไมค์ล่าสุด : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_miles_current_viewma" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("miles_current")%>' />

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label8" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัทที่เข้ารับบริการ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_company_detail_viewma" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("company_detail")%>' />

                                        </div>

                                        <asp:Label ID="Label10" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รายละเอียดเพิ่มเติม : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_note_detail_viewma" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("note_detail")%>' />

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label9" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="PR Code : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_pr_code_viewma" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("pr_code")%>' />

                                        </div>

                                        <asp:Label ID="Label11" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="PO Code : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_po_code_viewma" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("po_code")%>' />

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label12" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่เข้าซ่อม : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_date_start_repair_viewma" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("date_start_repair")%>' />

                                        </div>

                                        <asp:Label ID="Label14" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่ซ่อมเสร็จ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_date_end_repair_viewma" runat="server" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("date_end_repair")%>' />

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label13" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ราคารวม : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_price_total_repair_viewma" runat="server" Visible="false" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("price_total_repair")%>' />
                                            <asp:Label ID="lbl_sum_price_total_repair_decimal_value" runat="server" CssClass="control-labelnotop col-xs-8" />
                                            <asp:Label ID="lbl_sum_price_total_repair_decimal_unit" runat="server" CssClass="control-labelnotop col-xs-4" Text="บาท" />

                                        </div>

                                        <asp:Label ID="Label16" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ส่วนลด : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_discount_viewma" runat="server" CssClass="control-labelnotop col-xs-8" Text='<%# Eval("discount")%>' />
                                            <asp:Label ID="lbl_discount_viewma_unit" runat="server" CssClass="control-labelnotop col-xs-4" Text="บาท" />
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="Label15" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ภาษีมูลค่าเพิ่ม : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_vat_viewma" runat="server" Visible="false" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("vat")%>' />
                                            <asp:Label ID="lbl_sum_vat_decimal_value" runat="server" CssClass="control-labelnotop col-xs-8" />
                                            <asp:Label ID="lbl_sum_vat_decimal_value_unit" runat="server" CssClass="control-labelnotop col-xs-4" Text="บาท" />

                                        </div>

                                        <asp:Label ID="Label18" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ราคาสุทธิ : " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="lbl_price_net_viewma" runat="server" Visible="false" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("price_net")%>' />
                                            <asp:Label ID="lbl_sum_price_net_decimal_value" runat="server" CssClass="control-labelnotop col-xs-8" />
                                            <asp:Label ID="lbl_sum_price_net_decimal_value_unit" runat="server" CssClass="control-labelnotop col-xs-4" Text="บาท" />
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <asp:Label ID="lbl_fileactrach" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ไฟล์แนบ : " />
                                        <div class="col-xs-4">

                                            <asp:HyperLink runat="server" ID="btnViewFileCarMa" CssClass="f-s-13" Target="_blank"><i class="fa fa-file"></i> ดูข้อมูลไฟล์แนบ</asp:HyperLink>
                                            <%--<asp:Label ID="Label19" runat="server" Visible="false" CssClass="control-labelnotop col-xs-12" Text='<%# Eval("vat")%>' />
                                            <asp:Label ID="Label20" runat="server" CssClass="control-labelnotop col-xs-12" />--%>
                                        </div>
                                        <div class="col-xs-6">
                                        </div>

                                    </div>

                                    <asp:UpdatePanel ID="Update_DetailTopicCarMa" runat="server">
                                        <ContentTemplate>

                                            <div class="col-md-12">

                                                <asp:GridView ID="GvTopicDetailCarMa" runat="server"
                                                    AutoGenerateColumns="False"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive"
                                                    OnPageIndexChanging="gvPageIndexChanging"
                                                    OnRowDataBound="gvRowDataBound"
                                                    ShowFooter="true">
                                                    <HeaderStyle CssClass="success" Height="40px" Font-Size="Small" />
                                                    <RowStyle Font-Size="Small" />
                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>

                                                        <asp:TemplateField HeaderText="รายการหัวข้อ MA" HeaderStyle-CssClass="text-center" ItemStyle-Width="70%" ItemStyle-CssClass="text-left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_topic_ma_idx_ma" runat="server" Visible="false" Text='<%# Eval("topic_ma_idx") %>'></asp:Label>
                                                                <asp:Label ID="lbl_topic_ma_name_ma" runat="server" Text='<%# Eval("topic_ma_name") %>'></asp:Label>

                                                            </ItemTemplate>


                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="จำนวน" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_quantity_ma" runat="server" Text='<%# Eval("quantity") %>'></asp:Label>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ราคา/หน่วย" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_price_unit_ma" runat="server" Visible="false" Text='<%# Eval("sum_price_unit_decimal") %>'></asp:Label>
                                                                <asp:Label ID="lbl_sum_price_unit_decimal_value" runat="server"></asp:Label>
                                                            </ItemTemplate>

                                                            <FooterTemplate>
                                                                <div style="text-align: right;">
                                                                    <asp:Literal ID="lit_total" runat="server" Text="ราคารวม :"></asp:Literal>
                                                                </div>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ราคารวม" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbl_price_total_ma" runat="server" Visible="false" Text='<%# Eval("sum_price_total_decimal") %>'></asp:Label>
                                                                <asp:Label ID="lbl_sum_price_total_decimal_value" runat="server"></asp:Label>

                                                            </ItemTemplate>

                                                            <FooterTemplate>
                                                                <div style="text-align: right; background-color: chartreuse;">
                                                                    <asp:Label ID="lit_total_price" runat="server" Font-Bold="true"></asp:Label>

                                                                </div>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>

                                            </div>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:FormView>

                <!-- Log Detail CarMA -->
                <div class="panel panel-info" id="div_LogViewDetailCarMA" runat="server">
                    <div class="panel-heading">ประวัติการดำเนินการ</div>
                    <table class="table table-striped f-s-12 table-empshift-responsive">
                        <asp:Repeater ID="rptLogDetailCarMA" runat="server">
                            <HeaderTemplate>
                                <tr>
                                    <th>วัน / เวลา</th>
                                    <th>ผู้ดำเนินการ</th>
                                    <th>ดำเนินการ</th>
                                    <%--<th>ผลการดำเนินการ</th>--%>
                                    <th>เหตุผล</th>
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td data-th="วัน / เวลา"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                    <td data-th="ผู้ดำเนินการ"><%# Eval("emp_name_th") %> ( <%# Eval("current_artor") %> )</td>
                                    <td data-th="ดำเนินการ"><%# Eval("current_decision") %></td>
                                    <td data-th="เหตุผล"><%# Eval("comment") %></td>
                                    <%--<td data-th="ผลการดำเนินการ"><%# Eval("current_status_name") %></td>--%>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                    </table>
                    <div class="m-t-10 m-b-10"></div>
                </div>
                <!-- Log Detail CarMA -->
            </div>
        </asp:View>

        <!--View DocDetail MA-->

        <!--View Create Detail Car-->
        <asp:View ID="docCreateDetailCar" runat="server">
            <div class="col-md-12">
                <!-- Form Detail Employee Create -->
                <asp:FormView ID="fvEmpDetail" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <ItemTemplate>

                        <asp:HiddenField ID="hfEmpOrgIDX" runat="server" Value='<%# Eval("org_idx") %>' />
                        <asp:HiddenField ID="hfEmpRdeptIDX" runat="server" Value='<%# Eval("rdept_idx") %>' />
                        <asp:HiddenField ID="hfEmpRsecIDX" runat="server" Value='<%# Eval("rsec_idx") %>' />
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">รายละเอียดผู้ใช้งาน</h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">รหัสพนักงาน</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpCode" runat="server" CssClass="form-control" Text='<%# Eval("emp_code") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ชื่อ - นามสกุล</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpName" runat="server" CssClass="form-control" Text='<%# Eval("emp_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">องค์กร</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpOrg" runat="server" CssClass="form-control" Text='<%# Eval("org_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ฝ่าย</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpDept" runat="server" CssClass="form-control" Text='<%# Eval("dept_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">แผนก</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpSec" runat="server" CssClass="form-control" Text='<%# Eval("sec_name_th") %>' Enabled="false" />
                                        </div>
                                        <label class="col-sm-2 control-label">ตำแหน่ง</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="tbEmpPos" runat="server" CssClass="form-control" Text='<%# Eval("pos_name_th") %>' Enabled="false" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </ItemTemplate>
                </asp:FormView>
                <!-- Form Detail Employee Create -->

                <!-- Form Create -->
                <asp:FormView ID="FvInsertDetailCar" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>
                        <div class="row">

                            <div class="panel panel-info">
                                <div class="panel-heading f-bold">เพิ่มข้อมูลรถ</div>
                                <div class="panel-body">
                                    <div class="col-md-12">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>เลือกประเภทรถ</label>
                                                <asp:DropDownList ID="ddlTypeCar" runat="server" CssClass="form-control" ValidationGroup="SaveDetailCar" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    <%-- <asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Re_ddlTypeCar"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddlTypeCar" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกประเภทรถ"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19444" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlTypeCar" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>เลือกรายละเอียดประเภทรถ</label>
                                                <asp:DropDownList ID="ddlDetailTypeCar" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0" Text="--- เลือกรายละเอียดประเภทรถ ---" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ทะเบียนรถ</label>
                                                <asp:TextBox ID="txt_car_register_insert" runat="server" ValidationGroup="SaveDetailCar" CssClass="form-control" placeHolder="กรอกทะเบียนรถ">
                                                </asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Re_txt_car_register_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_car_register_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกทะเบียนรถ"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_register_insert" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>จังหวัด</label>
                                                <asp:DropDownList ID="ddlcar_province_insert" runat="server" ValidationGroup="SaveDetailCar" CssClass="form-control">
                                                    <%-- <asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Re_ddlcar_province_insert"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddlcar_province_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกจังหวัด"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlcar_province_insert" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ผู้ดูแล</label>
                                                <asp:DropDownList ID="ddladmin_idx_insert" runat="server" ValidationGroup="SaveDetailCar" CssClass="form-control">
                                                    <%-- <asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Re_ddladmin_idx_insert"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddladmin_idx_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกผู้ดูแล"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddladmin_idx_insert" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>CostCenter</label>
                                                <asp:DropDownList ID="ddlcostcenter_idx_insert" runat="server" ValidationGroup="SaveDetailCar" CssClass="form-control">
                                                    <%-- <asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Re_ddlcostcenter_idx_insert"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddlcostcenter_idx_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือก CostCenter"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlcostcenter_idx_insert" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Asset</label>
                                                <asp:TextBox ID="txt_asset_insert" runat="server" CssClass="form-control" ValidationGroup="SaveDetailCar" placeHolder="กรอกเลขที่สินทรัพย์">
                                                </asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Re_txt_asset_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_asset_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอก Asset"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_asset_insert" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ประเภท</label>
                                                <asp:DropDownList ID="ddltype_idx_insert" runat="server" ValidationGroup="SaveDetailCar" CssClass="form-control">
                                                    <%-- <asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Re_ddltype_idx_insert"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddltype_idx_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกประเภท"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddltype_idx_insert" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ลักษณะ</label>
                                                <asp:TextBox ID="txt_car_look_insert" runat="server" CssClass="form-control" ValidationGroup="SaveDetailCar" placeHolder="กรอกลักษณะรถ">
                                                </asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Re_txt_car_look_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_car_look_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกลักษณะ"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_asset_insert" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ยี่ห้อรถ</label>
                                                <asp:DropDownList ID="ddlbrand_car_idx_insert" runat="server" ValidationGroup="SaveDetailCar" CssClass="form-control">
                                                    <%-- <asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Re_ddlbrand_car_idx_insert"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddlbrand_car_idx_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกยี่ห้อรถ"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlbrand_car_idx_insert" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>แบบ</label>
                                                <asp:TextBox ID="txt_carmodel_insert" runat="server" CssClass="form-control" ValidationGroup="SaveDetailCar" placeHolder="กรอกแบบรถ">
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_carmodel_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_carmodel_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกแบบ"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender9" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_carmodel_insert" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>รุ่น ปี ค.ศ</label>
                                                <asp:TextBox ID="txt_car_genaration_insert" runat="server" CssClass="form-control" ValidationGroup="SaveDetailCar" placeHolder="กรอกรุ่น ปี ค.ศ.">
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_car_genaration_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_car_genaration_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอก รุ่น ปี ค.ศ"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender10" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_genaration_insert" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>สี</label>
                                                <asp:TextBox ID="txt_car_color_insert" runat="server" CssClass="form-control" ValidationGroup="SaveDetailCar" placeHolder="กรอกสีรถ">
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_car_color_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_car_color_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกสี"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender11" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_color_insert" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>เลขตัวรถ</label>
                                                <asp:TextBox ID="txt_car_number_insert" runat="server" CssClass="form-control" ValidationGroup="SaveDetailCar" placeHolder="กรอกเลขตัวรถ">
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_car_number_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_car_number_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกเลขตัวรถ"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_number_insert" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>อยู่ที่</label>
                                                <asp:TextBox ID="txt_car_number_located_insert" runat="server" CssClass="form-control" ValidationGroup="SaveDetailCar" placeHolder="อยู่ที่">
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_car_number_located_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_car_number_located_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกอยู่ที่"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender13" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_number_located_insert" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>ยี่ห้อเครื่องยนต์</label>
                                                <asp:DropDownList ID="ddl_engine_brand_idx_insert" runat="server" ValidationGroup="SaveDetailCar" CssClass="form-control">
                                                    <%-- <asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Re_ddl_engine_brand_idx_insert"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddl_engine_brand_idx_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกยี่ห้อเครื่องยนต์"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender14" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddl_engine_brand_idx_insert" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>เลขที่เครื่องยนต์</label>
                                                <asp:TextBox ID="txt_engine_brand_insert" runat="server" CssClass="form-control" ValidationGroup="SaveDetailCar" placeHolder="กรอกเลขที่เครื่องยนต์">
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_engine_brand_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_engine_brand_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกเลขที่เครื่องยนต์"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender15" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_engine_brand_insert" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>อยู่ที่</label>
                                                <asp:TextBox ID="txt_engine_brand_located_insert" runat="server" CssClass="form-control" ValidationGroup="SaveDetailCar" placeHolder="อยู่ที่">
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_engine_brand_located_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_engine_brand_located_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกอยู่ที่"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_engine_brand_located_insert" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>เชื้อเพลิง</label>
                                                <asp:DropDownList ID="ddlfuel_idx_insert" runat="server" ValidationGroup="SaveDetailCar" CssClass="form-control">
                                                    <%-- <asp:ListItem Value="0" Text="ไม่เร่งด่วน" />
                                                <asp:ListItem Value="1" Text="เร่งด่วน" />--%>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Re_ddlfuel_idx_insert"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddlfuel_idx_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกเชื้อเพลิง"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender17" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlfuel_idx_insert" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>เลขถังแก๊ส</label>
                                                <asp:TextBox ID="txt_gas_cylinder_insert" runat="server" CssClass="form-control" ValidationGroup="SaveDetailCar" placeHolder="กรอกเลขถังแก๊ส">
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_gas_cylinder_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_gas_cylinder_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกเลขถังแก๊ส"
                                                    ValidationGroup="SaveDetailCar" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender18" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_gas_cylinder_insert" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>จำนวน(สูบ)</label>
                                                <asp:TextBox ID="txt_count_pump_insert" runat="server" CssClass="form-control" ValidationGroup="SaveDetailCar" placeHolder="กรอกจำนวน(สูบ)">
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_count_pump_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_count_pump_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกจำนวน(สูบ)"
                                                    ValidationGroup="SaveDetailCar" />


                                                <asp:RegularExpressionValidator ID="Re_txt_count_pump_insert1" runat="server" ValidationGroup="SaveDetailCar" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txt_count_pump_insert"
                                                    ValidationExpression="^\d+" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender19" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_count_pump_insert" Width="200" PopupPosition="BottomLeft" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender30" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_count_pump_insert1" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ซีซี(cc)</label>
                                                <asp:TextBox ID="txt_cc_insert" runat="server" CssClass="form-control" ValidationGroup="SaveDetailCar" placeHolder="กรอกซีซี">
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_cc_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_cc_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกซีซี(cc)"
                                                    ValidationGroup="SaveDetailCar" />

                                                <asp:RegularExpressionValidator ID="Re_txt_cc_insert1" runat="server" ValidationGroup="SaveDetailCar" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txt_cc_insert"
                                                    ValidationExpression="^\d+" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender20" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_cc_insert" Width="200" PopupPosition="BottomLeft" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender31" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_cc_insert1" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>แรงม้า</label>
                                                <asp:TextBox ID="txt_horse_power_insert" runat="server" CssClass="form-control" ValidationGroup="SaveDetailCar" placeHolder="กรอกค่าแรงม้า">
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_horse_power_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_horse_power_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกแรงม้า"
                                                    ValidationGroup="SaveDetailCar" />

                                                <asp:RegularExpressionValidator ID="Re_txt_horse_power_insert1" runat="server" ValidationGroup="SaveDetailCar" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txt_horse_power_insert"
                                                    ValidationExpression="^\d+" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender21" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_horse_power_insert" Width="200" PopupPosition="BottomLeft" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender32" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_horse_power_insert1" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>


                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>นำหนักรถ(กก.)</label>
                                                <asp:TextBox ID="txt_car_weight_insert" runat="server" CssClass="form-control" ValidationGroup="SaveDetailCar" placeHolder="กรอกนำหนักรถ(กก.)">
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_car_weight_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_car_weight_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกนำหนักรถ(กก.)"
                                                    ValidationGroup="SaveDetailCar" />

                                                <asp:RegularExpressionValidator ID="Re_txt_car_weight_insert11" runat="server" ValidationGroup="SaveDetailCar" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txt_car_weight_insert"
                                                    ValidationExpression="^\d+" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender22" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_weight_insert" Width="200" PopupPosition="BottomLeft" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender33" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_weight_insert11" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>น้ำหนักบรรทุก/น้ำหนักลงเพลา</label>
                                                <asp:TextBox ID="txt_car_load_insert" runat="server" CssClass="form-control" ValidationGroup="SaveDetailCar" placeHolder="กรอกน้ำหนักบรรทุก/น้ำหนักลงเพลา">
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_car_load_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_car_load_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกน้ำหนักบรรทุก/น้ำหนักลงเพลา"
                                                    ValidationGroup="SaveDetailCar" />

                                                <asp:RegularExpressionValidator ID="Re_txt_car_load_insert1" runat="server" ValidationGroup="SaveDetailCar" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txt_car_load_insert"
                                                    ValidationExpression="^\d+" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender23" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_load_insert" Width="200" PopupPosition="BottomLeft" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender34" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_car_load_insert1" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>นำหนักรวม</label>
                                                <asp:TextBox ID="txt_total_weight_insert" runat="server" ValidationGroup="SaveDetailCar" CssClass="form-control" placeHolder="กรอกนำหนักรวม">
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_total_weight_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_total_weight_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกนำหนักรวม"
                                                    ValidationGroup="SaveDetailCar" />

                                                <asp:RegularExpressionValidator ID="Re_txt_total_weight_insert1" runat="server" ValidationGroup="SaveDetailCar" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txt_total_weight_insert"
                                                    ValidationExpression="^\d+" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender24" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_total_weight_insert" Width="200" PopupPosition="BottomLeft" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender35" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_total_weight_insert1" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>จำนวนที่นั่ง</label>
                                                <asp:TextBox ID="txt_count_seat_insert" runat="server" ValidationGroup="SaveDetailCar" CssClass="form-control" MaxLength="2" placeHolder="กรอกจำนวนที่นั่ง">
                                                </asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_count_seat_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_count_seat_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกจำนวนที่นั่ง"
                                                    ValidationGroup="SaveDetailCar" />

                                                <asp:RegularExpressionValidator ID="Re_txt_count_seat_insert1" runat="server" ValidationGroup="SaveDetailCar" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txt_count_seat_insert"
                                                    ValidationExpression="^\d+" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender25" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_count_seat_insert" Width="200" PopupPosition="BottomLeft" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender36" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_count_seat_insert1" Width="200" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>สถานะ</label>
                                                <asp:DropDownList ID="ddlm0_car_status" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="1" Text="Online" />
                                                    <asp:ListItem Value="0" Text="Offline" />
                                                </asp:DropDownList>
                                            </div>
                                        </div>


                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>วันครบกำหนดเสียภาษี</label>
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txt_tax_deadline_insert" runat="server" ValidationGroup="SaveDetailCar" CssClass="form-control from-deadline-datepicker"
                                                        placeHolder="กรอกวันครบกำหนดเสียภาษี"></asp:TextBox>
                                                    <span class="input-group-addon showdeadline-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>

                                                    <asp:RequiredFieldValidator ID="Re_txt_tax_deadline_insert"
                                                        runat="server"
                                                        ControlToValidate="txt_tax_deadline_insert" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกวันครบกำหนดเสียภาษี"
                                                        ValidationGroup="SaveDetailCar" />


                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender26" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_tax_deadline_insert" Width="200" PopupPosition="BottomLeft" />


                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ค่าภาษี บาท/สต.</label>
                                                <asp:TextBox ID="txt_price_tax_insert" runat="server" ValidationGroup="SaveDetailCar" CssClass="form-control" MaxLength="5"
                                                    placeHolder="กรอกค่าภาษี บาท/สต."></asp:TextBox>

                                                <asp:RequiredFieldValidator ID="Re_txt_price_tax_insert"
                                                    runat="server"
                                                    ControlToValidate="txt_price_tax_insert" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกค่าภาษี บาท/สต."
                                                    ValidationGroup="SaveDetailCar" />

                                                <asp:RegularExpressionValidator ID="Re_txt_price_tax_insert1" runat="server" ValidationGroup="SaveDetailCar" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข" Font-Size="11"
                                                    ControlToValidate="txt_price_tax_insert"
                                                    ValidationExpression="^\d+" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender27" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_price_tax_insert" Width="200" PopupPosition="BottomLeft" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender37" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_price_tax_insert1" Width="200" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="clearfix"></div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>วันครบกำหนดประกันภัยคุ้มครองผู้ประสบภัยจากรถ</label>
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txt_date_insurance_term_insert" ValidationGroup="SaveDetailCar" runat="server" CssClass="form-control from-dateinsurance-datepicker"
                                                        placeHolder="กรอกวันครบกำหนดประกันภัยคุ้มครองผู้ประสบภัยจากรถ"></asp:TextBox>
                                                    <span class="input-group-addon showinsurance-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>

                                                    <asp:RequiredFieldValidator ID="Re_txt_date_insurance_term_insert"
                                                        runat="server"
                                                        ControlToValidate="txt_date_insurance_term_insert" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกวันครบกำหนดประกันภัยคุ้มครองผู้ประสบภัยจากรถ"
                                                        ValidationGroup="SaveDetailCar" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender28" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_date_insurance_term_insert" Width="200" PopupPosition="BottomLeft" />
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>วันครบกำหนดประกันภัยรถยนต์</label>
                                                <div class='input-group date'>
                                                    <asp:TextBox ID="txt_date_car_insurance_deadline_insert" ValidationGroup="SaveDetailCar" runat="server" CssClass="form-control from-date-datepicker"
                                                        placeHolder="กรอกวันครบกำหนดประกันภัยรถยนต์"></asp:TextBox>
                                                    <span class="input-group-addon show-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>

                                                    <asp:RequiredFieldValidator ID="Re_txt_date_car_insurance_deadline_insert"
                                                        runat="server"
                                                        ControlToValidate="txt_date_car_insurance_deadline_insert" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกวันครบกำหนดประกันภัยรถยนต์"
                                                        ValidationGroup="SaveDetailCar" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender29" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_date_car_insurance_deadline_insert" Width="200" PopupPosition="BottomLeft" />
                                                </div>

                                            </div>
                                        </div>

                                        <asp:UpdatePanel ID="UpdatePnFileUpload" runat="server">
                                            <ContentTemplate>
                                                <div class="col-md-12">
                                                    <asp:Panel ID="pnUploadfileCar" runat="server">
                                                        <div class="form-group">
                                                            <label>แนบไฟล์</label>
                                                            <%--<div class="col-sm-10">--%>

                                                            <%--  <asp:FileUpload ID="UploadFileCarEdit" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                                CssClass="control-label multi max-1" accept="jpg|pdf|png" />
                                                            <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpg|pdf|png</font></p>--%>

                                                            <asp:FileUpload ID="UploadFileCar" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                                CssClass="control-label UploadFileCar multi max-1" accept="jpg|pdf|png" />
                                                            <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpg|pdf|png</font></p>



                                                        </div>
                                                    </asp:Panel>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="pull-right">
                                                            <asp:LinkButton ID="btnSave" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                                                CommandName="CmdSave" Text="บันทึก" ValidationGroup="SaveDetailCar"
                                                                OnClientClick="return confirm('ยืนยันการบันทึก')" />
                                                            <asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand"
                                                                CommandName="CmdCancel" Text="ยกเลิก" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnSave" />
                                            </Triggers>
                                        </asp:UpdatePanel>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </InsertItemTemplate>
                </asp:FormView>
                <!-- Form Create -->

                <!-- Form Create MA -->
                <asp:UpdatePanel ID="Panel_FormCreateMA" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <asp:FormView ID="FvInsertCarMA" runat="server" DefaultMode="ReadOnly" Width="100%">
                            <InsertItemTemplate>

                                <div class="row">

                                    <div class="panel panel-info">
                                        <div class="panel-heading f-bold">สร้างรายการ MA</div>

                                        <div class="panel-body">
                                            <%--<div class="col-md-12">--%>
                                            <div class="col-md-10 col-md-offset-1">

                                                <asp:UpdatePanel ID="update_test" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>เลือกประเภทรถ</label>
                                                                <asp:DropDownList ID="ddlTypeCar_MA" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ID="Req_ddlTypeCar_MA"
                                                                    runat="server"
                                                                    InitialValue="0"
                                                                    ControlToValidate="ddlTypeCar_MA" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณาเลือกประเภทรถ"
                                                                    ValidationGroup="SaveInsertCarMa" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender60" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlTypeCar_MA" Width="200" PopupPosition="BottomLeft" />
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>เลือกทะเบียนรถ</label>
                                                                <asp:DropDownList ID="ddlCarregister_MA" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                                    <asp:ListItem Value="0">--- เลือกทะเบียนรถ ---</asp:ListItem>
                                                                </asp:DropDownList>

                                                                <asp:RequiredFieldValidator ID="Req_ddlCarregister_MA"
                                                                    runat="server"
                                                                    InitialValue="0"
                                                                    ControlToValidate="ddlCarregister_MA" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณาเลือกทะเบียนรถ"
                                                                    ValidationGroup="SaveInsertCarMa" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender59" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlCarregister_MA" Width="200" PopupPosition="BottomLeft" />
                                                            </div>
                                                        </div>

                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="ddlTypeCar_MA" EventName="SelectedIndexChanged" />
                                                        <asp:AsyncPostBackTrigger ControlID="ddlCarregister_MA" EventName="SelectedIndexChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>

                                                <asp:UpdatePanel ID="Panel_CreateMA" runat="server" UpdateMode="Always">
                                                    <ContentTemplate>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>รายละเอียดประเภทรถ</label>
                                                                <asp:TextBox ID="txt_DetailTypeCar_ma" placeholder="รายละเอียดประเภทรถ..." runat="server" CssClass="form-control" Enabled="false" />
                                                                <%-- <asp:RequiredFieldValidator ID="Re_ddlApproveStatus" Text='<%# Eval("emp_code") %>' 
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddlApproveStatus" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกสถานะการอนุมัติ"
                                                    ValidationGroup="SaveApproveHeaduser" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="Vali_Re_ddlApproveStatus" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlApproveStatus" Width="200" PopupPosition="BottomLeft" />--%>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>CostCenter</label>
                                                                <asp:TextBox ID="txt_CostCenter_detail_ma" runat="server" CssClass="form-control" placeholder="CostCenter..." Enabled="false" />

                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>เลขไมค์ครบระยะกำหนด</label>
                                                                <asp:TextBox ID="txt_miles_muturity_ma" placeholder="เลขไมค์ครบระยะกำหนด..." ValidationGroup="SaveInsertCarMa" runat="server" CssClass="form-control" />

                                                                <asp:RequiredFieldValidator ID="Re_txt_miles_muturity_ma"
                                                                    runat="server"
                                                                    ControlToValidate="txt_miles_muturity_ma" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณากรอกเลขไมค์ครบระยะกำหนด"
                                                                    ValidationGroup="SaveInsertCarMa" />

                                                                <asp:RegularExpressionValidator ID="Re_txt_miles_muturity_ma1" runat="server" ValidationGroup="SaveCreate" Display="None"
                                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข หรือทศนิยมเท่านั้น"
                                                                    ControlToValidate="txt_miles_muturity_ma"
                                                                    ValidationExpression="^\d+\d*\.?\d*$" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="Vali_Re_ddlApproveStatus" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_miles_muturity_ma" Width="200" PopupPosition="BottomLeft" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender53" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_miles_muturity_ma1" Width="200" PopupPosition="BottomLeft" />
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>เลขไมค์ล่าสุด</label>
                                                                <asp:TextBox ID="txt_miles_current_ma" runat="server" ValidationGroup="SaveInsertCarMa" placeholder="เลขไมค์ล่าสุด..." CssClass="form-control" />

                                                                <asp:RequiredFieldValidator ID="Re_txt_miles_current_ma"
                                                                    runat="server"
                                                                    ControlToValidate="txt_miles_current_ma" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณากรอกเลขไมค์ล่าสุด"
                                                                    ValidationGroup="SaveInsertCarMa" />

                                                                <asp:RegularExpressionValidator ID="Re_txt_miles_current_ma1" runat="server" ValidationGroup="SaveCreate" Display="None"
                                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข หรือทศนิยมเท่านั้น"
                                                                    ControlToValidate="txt_miles_current_ma"
                                                                    ValidationExpression="^\d+\d*\.?\d*$" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender42" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_miles_current_ma" Width="200" PopupPosition="BottomLeft" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender54" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_miles_current_ma1" Width="200" PopupPosition="BottomLeft" />
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>บริษัทที่เข้ารับบริการ</label>
                                                                <asp:TextBox ID="txt_company_repair_ma" runat="server" ValidationGroup="SaveInsertCarMa" CssClass="form-control" TextMode="multiline" Rows="3" placeholder="รายละเอียดบริษัทที่เข้ารับบริการ ..." />
                                                                <asp:RequiredFieldValidator ID="Re_txt_company_repair_ma"
                                                                    runat="server"
                                                                    ControlToValidate="txt_company_repair_ma" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณากรอกบริษัทที่เข้ารับบริการ"
                                                                    ValidationGroup="SaveInsertCarMa" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender43" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_company_repair_ma" Width="200" PopupPosition="BottomLeft" />
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>รายละเอียดเพิ่มเติม</label>
                                                                <asp:TextBox ID="txt_comment_detailcreate_ma" runat="server" CssClass="form-control" ValidationGroup="SaveInsertCarMa" TextMode="multiline" Rows="3" placeholder="รายละเอียดเพิ่มเติม ..." />
                                                                <asp:RequiredFieldValidator ID="Re_txt_comment_detailcreate_ma"
                                                                    runat="server"
                                                                    ControlToValidate="txt_comment_detailcreate_ma" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณากรอกรายละเอียดเพิ่มเติม"
                                                                    ValidationGroup="SaveInsertCarMa" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender44" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_comment_detailcreate_ma" Width="200" PopupPosition="BottomLeft" />
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>PR Code</label>
                                                                <asp:TextBox ID="txt_prcode_ma" runat="server" ValidationGroup="SaveInsertCarMa" placeholder="PR Code ..." CssClass="form-control" />
                                                                <asp:RequiredFieldValidator ID="Re_txt_prcode_ma"
                                                                    runat="server"
                                                                    ControlToValidate="txt_prcode_ma" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณากรอก PR Code"
                                                                    ValidationGroup="SaveInsertCarMa" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender45" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_prcode_ma" Width="200" PopupPosition="BottomLeft" />
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>PO Code</label>
                                                                <asp:TextBox ID="txt_pocode_ma" runat="server" ValidationGroup="SaveInsertCarMa" placeholder="PO Code ..." CssClass="form-control" />
                                                                <asp:RequiredFieldValidator ID="Re_txt_pocode_ma"
                                                                    runat="server"
                                                                    ControlToValidate="txt_pocode_ma" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณากรอก PO Code"
                                                                    ValidationGroup="SaveInsertCarMa" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender46" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_pocode_ma" Width="200" PopupPosition="BottomLeft" />
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>วันที่เข้าซ่อม</label>

                                                                <div class="input-group date">
                                                                    <asp:TextBox ID="txt_create_repair_ma" runat="server" ValidationGroup="SaveInsertCarMa" placeholder="วันที่เข้าซ่อม..." CssClass="form-control datetimepicker-from cursor-pointer" />

                                                                    <span class="input-group-addon show-fromto-onclick">
                                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                                    <asp:RequiredFieldValidator ID="Re_txt_create_repair_ma"
                                                                        runat="server"
                                                                        ControlToValidate="txt_create_repair_ma" Display="None" SetFocusOnError="true"
                                                                        ErrorMessage="*กรุณาเลือกวันที่เข้าซ่อม"
                                                                        ValidationGroup="SaveInsertCarMa" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender47" runat="Server"
                                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_create_repair_ma" Width="200" PopupPosition="BottomLeft" />


                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>วันที่ซ่อมเสร็จ</label>
                                                                <div class="input-group date">
                                                                    <asp:TextBox ID="txt_create_repair_success_ma" runat="server" ValidationGroup="SaveInsertCarMa" placeholder="วันที่ซ่อมเสร็จ..." CssClass="form-control datetimepicker-to cursor-pointer" />

                                                                    <span class="input-group-addon show-toto-onclick">
                                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>

                                                                    <asp:RequiredFieldValidator ID="Re_txt_create_repair_success_ma"
                                                                        runat="server"
                                                                        ControlToValidate="txt_create_repair_success_ma" Display="None" SetFocusOnError="true"
                                                                        ErrorMessage="*กรุณาเลือกวันที่ซ่อมเสร็จ"
                                                                        ValidationGroup="SaveInsertCarMa" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender48" runat="Server"
                                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_create_repair_success_ma" Width="200" PopupPosition="BottomLeft" />


                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ราคารวม</label>
                                                                <asp:TextBox ID="txt_count_total_ma" runat="server" ValidationGroup="SaveInsertCarMa" placeholder="ราคารวม..." CssClass="form-control" />

                                                                <asp:RequiredFieldValidator ID="Re_txt_count_total_ma"
                                                                    runat="server"
                                                                    ControlToValidate="txt_count_total_ma" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณากรอกราคารวม"
                                                                    ValidationGroup="SaveInsertCarMa" />

                                                                <asp:RegularExpressionValidator ID="Re_txt_count_total_ma1" runat="server" ValidationGroup="SaveCreate" Display="None"
                                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข หรือทศนิยมเท่านั้น"
                                                                    ControlToValidate="txt_count_total_ma"
                                                                    ValidationExpression="^\d+\d*\.?\d*$" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender49" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_count_total_ma" Width="200" PopupPosition="BottomLeft" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender55" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_count_total_ma1" Width="200" PopupPosition="BottomLeft" />

                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ส่วนลด</label>
                                                                <asp:TextBox ID="txt_discount_ma" runat="server" ValidationGroup="SaveInsertCarMa" placeholder="ส่วนลด..." CssClass="form-control" />
                                                                <asp:RequiredFieldValidator ID="Re_txt_discount_ma"
                                                                    runat="server"
                                                                    ControlToValidate="txt_discount_ma" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณากรอกส่วนลด"
                                                                    ValidationGroup="SaveInsertCarMa" />

                                                                <asp:RegularExpressionValidator ID="Re_txt_discount_ma1" runat="server" ValidationGroup="SaveCreate" Display="None"
                                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข หรือทศนิยมเท่านั้น"
                                                                    ControlToValidate="txt_discount_ma"
                                                                    ValidationExpression="^\d+\d*\.?\d*$" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender50" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_discount_ma" Width="200" PopupPosition="BottomLeft" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender56" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_discount_ma1" Width="200" PopupPosition="BottomLeft" />
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ภาษีมูลค่าเพิ่ม</label>
                                                                <asp:TextBox ID="txt_price_tax_ma" runat="server" ValidationGroup="SaveInsertCarMa" placeholder="ภาษีมูลค่าเพิ่ม..." CssClass="form-control" />
                                                                <asp:RequiredFieldValidator ID="Re_txt_price_tax_ma"
                                                                    runat="server"
                                                                    ControlToValidate="txt_price_tax_ma" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณากรอกภาษีมูลค่าเพิ่ม"
                                                                    ValidationGroup="SaveInsertCarMa" />

                                                                <asp:RegularExpressionValidator ID="Re_txt_price_tax_ma1" runat="server" ValidationGroup="SaveCreate" Display="None"
                                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข หรือทศนิยมเท่านั้น"
                                                                    ControlToValidate="txt_price_tax_ma"
                                                                    ValidationExpression="^\d+\d*\.?\d*$" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender51" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_price_tax_ma" Width="200" PopupPosition="BottomLeft" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender57" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_price_tax_ma1" Width="200" PopupPosition="BottomLeft" />
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label>ราคาสุทธิ</label>
                                                                <asp:TextBox ID="txt_net_price_ma" runat="server" ValidationGroup="SaveInsertCarMa" placeholder="ราคาสุทธิ..." CssClass="form-control" />

                                                                <asp:RequiredFieldValidator ID="Re_txt_net_price_ma"
                                                                    runat="server"
                                                                    ControlToValidate="txt_net_price_ma" Display="None" SetFocusOnError="true"
                                                                    ErrorMessage="*กรุณากรอกราคาสุทธิ"
                                                                    ValidationGroup="SaveInsertCarMa" />

                                                                <asp:RegularExpressionValidator ID="Re_txt_net_price_ma1" runat="server" ValidationGroup="SaveCreate" Display="None"
                                                                    ErrorMessage="กรุณากรอกเฉพาะตัวเลข หรือทศนิยมเท่านั้น"
                                                                    ControlToValidate="txt_net_price_ma"
                                                                    ValidationExpression="^\d+\d*\.?\d*$" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender52" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_net_price_ma" Width="200" PopupPosition="BottomLeft" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender58" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_net_price_ma1" Width="200" PopupPosition="BottomLeft" />

                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div id="divGvTopicDetailMa_scroll_x" style="overflow-x: scroll; width: 100%" runat="server">
                                                                    <div id="divGvTopicDetailMa_scroll_y" style="overflow-y: scroll; width: auto; height: 300px" runat="server">
                                                                        <asp:GridView ID="GvTopicDetailMa"
                                                                            runat="server"
                                                                            AutoGenerateColumns="false"
                                                                            DataKeyNames="topic_ma_idx"
                                                                            CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                                            HeaderStyle-CssClass="info"
                                                                            HeaderStyle-Height="40px"
                                                                            OnPageIndexChanging="Master_PageIndexChanging"
                                                                            OnRowDataBound="Master_RowDataBound">

                                                                            <PagerStyle CssClass="pageCustom" />
                                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="First" LastPageText="Last" />

                                                                            <EmptyDataTemplate>
                                                                                <div style="text-align: center;">Data Cannot Be Found</div>
                                                                            </EmptyDataTemplate>


                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="เลือกรายการ" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                                                                    <ItemTemplate>
                                                                                        <asp:UpdatePanel ID="update_test11" runat="server" UpdateMode="Always">
                                                                                            <ContentTemplate>
                                                                                                <asp:CheckBox ID="chk_detailTopic_ma" runat="server" OnCheckedChanged="onSelectedCheckChanged" AutoPostBack="true" />

                                                                                            </ContentTemplate>
                                                                                            <Triggers>
                                                                                                <asp:AsyncPostBackTrigger ControlID="chk_detailTopic_ma" EventName="CheckedChanged" />
                                                                                                <%--<asp:AsyncPostBackTrigger ControlID="ddlCarregister_MA" EventName="SelectedIndexChanged" />--%>
                                                                                            </Triggers>
                                                                                        </asp:UpdatePanel>

                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="หัวข้อ MA." HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-left">
                                                                                    <ItemTemplate>
                                                                                        <small>
                                                                                            <asp:Label ID="lbl_topic_ma_idx" Visible="false" runat="server" Text='<%# Eval("topic_ma_idx") %>' />
                                                                                            <asp:Label ID="lbl_topic_ma_name" runat="server" Text='<%# Eval("topic_ma_name") %>' /></small>

                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="จำนวน" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                                                                    <ItemTemplate>
                                                                                        <small>

                                                                                            <asp:UpdatePanel ID="update_test112" runat="server" UpdateMode="Always">
                                                                                                <ContentTemplate>

                                                                                                    <asp:TextBox ID="txt_Quantity" runat="server" Visible="false" placeholder="จำนวน..." OnTextChanged="TextBoxChanged" AutoPostBack="true" Enabled="false" CssClass="form-control" />


                                                                                                    <asp:RequiredFieldValidator ID="Re_txt_Quantity"
                                                                                                        runat="server"
                                                                                                        ControlToValidate="txt_Quantity" Display="None" SetFocusOnError="true"
                                                                                                        ErrorMessage="*กรุณากรอกจำนวน"
                                                                                                        ValidationGroup="SaveInsertCarMa" />

                                                                                                    <asp:RegularExpressionValidator ID="Re_txt_Quantity1" runat="server" ValidationGroup="SaveCreate" Display="None"
                                                                                                        ErrorMessage="กรุณากรอกเฉพาะตัวเลข หรือทศนิยมเท่านั้น"
                                                                                                        ControlToValidate="txt_Quantity"
                                                                                                        ValidationExpression="^\d+\d*\.?\d*$" />

                                                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender521" runat="Server"
                                                                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_Quantity" Width="200" PopupPosition="BottomLeft" />

                                                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender591" runat="Server"
                                                                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_Quantity1" Width="200" PopupPosition="BottomLeft" />

                                                                                                </ContentTemplate>
                                                                                                <Triggers>
                                                                                                    <asp:AsyncPostBackTrigger ControlID="txt_Quantity" EventName="TextChanged" />
                                                                                                    <%--<asp:AsyncPostBackTrigger ControlID="ddlCarregister_MA" EventName="SelectedIndexChanged" />--%>
                                                                                                </Triggers>
                                                                                            </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="ราคา/หน่วย" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                                                                    <ItemTemplate>
                                                                                        <small>

                                                                                            <asp:UpdatePanel ID="update_test2211" runat="server" UpdateMode="Always">
                                                                                                <ContentTemplate>

                                                                                                    <asp:TextBox ID="txt_price_uit" runat="server" Visible="false" OnTextChanged="TextBoxChanged" AutoPostBack="true" placeholder="ราคา/หน่วย..." Enabled="false" CssClass="form-control" />


                                                                                                    <asp:RequiredFieldValidator ID="Re_txt_price_uit"
                                                                                                        runat="server"
                                                                                                        ControlToValidate="txt_price_uit" Display="None" SetFocusOnError="true"
                                                                                                        ErrorMessage="*กรุณากรอกราคา/หน่วย"
                                                                                                        ValidationGroup="SaveInsertCarMa" />

                                                                                                    <asp:RegularExpressionValidator ID="Re_txt_price_uit1" runat="server" ValidationGroup="SaveCreate" Display="None"
                                                                                                        ErrorMessage="กรุณากรอกเฉพาะตัวเลข หรือทศนิยมเท่านั้น"
                                                                                                        ControlToValidate="txt_price_uit"
                                                                                                        ValidationExpression="^\d+\d*\.?\d*$" />

                                                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5213" runat="Server"
                                                                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_price_uit" Width="200" PopupPosition="BottomLeft" />

                                                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5913" runat="Server"
                                                                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_price_uit1" Width="200" PopupPosition="BottomLeft" />

                                                                                                </ContentTemplate>
                                                                                                <Triggers>
                                                                                                    <asp:AsyncPostBackTrigger ControlID="txt_price_uit" EventName="TextChanged" />
                                                                                                    <%--<asp:AsyncPostBackTrigger ControlID="ddlCarregister_MA" EventName="SelectedIndexChanged" />--%>
                                                                                                </Triggers>
                                                                                            </asp:UpdatePanel>
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="ราคารวม" HeaderStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%" ItemStyle-CssClass="text-center">
                                                                                    <ItemTemplate>
                                                                                        <small>
                                                                                            <asp:TextBox ID="txt_price_total" runat="server" placeholder="ราคารวม..." Visible="false" Enabled="false" CssClass="form-control" />
                                                                                    </ItemTemplate>

                                                                                    <%--<FooterTemplate>
                                                                                        <div style="text-align: right;">
                                                                                            <asp:Label ID="lb_sum_counttotal" runat="server"></asp:Label>
                                                                                        </div>
                                                                                    </FooterTemplate>--%>


                                                                                </asp:TemplateField>

                                                                            </Columns>

                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                                            <ContentTemplate>
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label>แนบไฟล์</label>
                                                                        <asp:FileUpload ID="UploadFileCarma" CssClass="btn btn-warning" ClientIDMode="Static" runat="server" accept="jpg|png|pdf" />
                                                                        <p class="help-block"><font color="red">**เฉพาะนามสกุล jpg, png, pdf</font></p>
                                                                        <%--<asp:RequiredFieldValidator ID="required_UploadFile_UseCarOut"
                                                                    runat="server" ValidationGroup="SaveCarUseHrOut"
                                                                    Display="None"
                                                                    SetFocusOnError="true"
                                                                    ControlToValidate="UploadFile_UseCarOut"
                                                                    Font-Size="13px" ForeColor="Red"
                                                                    ErrorMessage="กรุณาเลือกไฟล์" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="toolkitRequiredTxtEmpVisitorReason" runat="Server" HighlightCssClass="validatorCalloutHighlight"
                                                                    TargetControlID="required_UploadFile_UseCarOut" Width="200" PopupPosition="BottomRight" />    --%>
                                                                    </div>
                                                                </div>

                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <div class="form-group pull-right">
                                                                            <asp:LinkButton ID="btnSaveDetailMA" CssClass="btn btn-success" runat="server" ValidationGroup="SaveInsertCarMa" data-original-title="Save" data-toggle="tooltip" Text="บันทึกรายการ MA" OnCommand="btnCommand" CommandName="cmdSaveDetailMA"></asp:LinkButton>

                                                                            <asp:LinkButton ID="btnCancelDetailMA" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip" Text="ยกเลิก" OnCommand="btnCommand" CommandName="cmdCancelDetailMA"></asp:LinkButton>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </ContentTemplate>

                                                            <Triggers>
                                                                <asp:PostBackTrigger ControlID="btnSaveDetailMA" />

                                                            </Triggers>

                                                        </asp:UpdatePanel>

                                                    </ContentTemplate>

                                                    <%-- <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnSaveDetailMA" />

                                                    </Triggers>--%>
                                                </asp:UpdatePanel>

                                            </div>
                                        </div>

                                    </div>

                                </div>


                            </InsertItemTemplate>
                        </asp:FormView>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Form Create MA -->

            </div>
        </asp:View>
        <!--View Create Detail Car-->

        <!--View Report Detail Car-->
        <asp:View ID="docReport" runat="server">

            <div class="col-md-12">
                <asp:UpdatePanel ID="Update_PanelSearchReport" runat="server">
                    <ContentTemplate>
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหารายงาน </h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%--<div class="form-group">
                                        <label class="col-sm-2 control-label">ประเภทรายงาน</label>
                                        <div class="col-sm-4">
                                            <asp:DropDownList ID="ddlTypeReport" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                                <asp:ListItem Value="0">--- เลือกประเภทรายงาน ---</asp:ListItem>
                                                <asp:ListItem Value="1">ตาราง</asp:ListItem>
                                                <asp:ListItem Value="2">กราฟ</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="Re_ddlTypeReport"
                                                runat="server"
                                                InitialValue="0"
                                                ControlToValidate="ddlTypeReport" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกประเภทรายงาน"
                                                ValidationGroup="SearchReportDetail" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1233" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlTypeReport" Width="200" PopupPosition="BottomLeft" />
                                        </div>
                                        <label class="col-sm-6 control-label"></label>
                                    </div>--%>

                                    <!-- START Report Table -->
                                    <asp:UpdatePanel ID="Update_PnTableReportDetail" runat="server" Visible="true">
                                        <ContentTemplate>
                                            <%-- <div class="form-group">
                                                <label class="col-sm-2 control-label">ค้นหาจากสถานะ</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlStatusCarBooking" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกประเภทการค้นหา ---</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>
                                                <label class="col-sm-6 control-label"></label>
                                            </div>--%>

                                            <div class="form-group">
                                                <%-- <asp:Label ID="Label9" runat="server" Text="ระบุวันค้นหา" CssClass="col-sm-2 control-label"></asp:Label>--%>
                                                <label class="col-sm-2 control-label">ระบุวันค้นหา</label>
                                                <div class="col-sm-2">
                                                    <asp:DropDownList ID="ddlSearchDateReport" AutoPostBack="true" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                        <asp:ListItem Value="0">--- เลือกเงื่อนไข ---</asp:ListItem>
                                                        <asp:ListItem Value="1">มากกว่า ></asp:ListItem>
                                                        <asp:ListItem Value="2">น้อยกว่า <</asp:ListItem>
                                                        <asp:ListItem Value="3">ระหว่าง <></asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <div class="col-sm-4 ">

                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="txtAddStartdateReport" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                        <asp:RequiredFieldValidator ID="Required_txtAddStartdateReport" ValidationGroup="SearchReportDetail" runat="server" Display="None"
                                                            ControlToValidate="txtAddStartdateReport" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                            ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5133" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Required_txtAddStartdateReport" Width="160" />
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="txtAddEndDateReport" runat="server" CssClass="form-control from-date-datepicker" Enabled="false" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">ค้นหาจากองค์กร</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlorgidx" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกองค์กร ---</asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>
                                                <label class="col-sm-2 control-label">ค้นหาจากฝ่าย</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlrdeptidx" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกฝ่าย ---</asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">เลือกประเภทรถ</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlTypeCarMA_report" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                    <%--<asp:RequiredFieldValidator ID="Re_ddlPlaceSearchDetail"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlPlaceSearchDetail" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกสถานที่"
                                                        ValidationGroup="SearchDetail" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8444" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlPlaceSearchDetail" Width="200" PopupPosition="BottomLeft" />--%>
                                                </div>
                                                <label class="col-sm-2 control-label">เลือกทะเบียนรถ</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlCarregisterMa_report" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                        <asp:ListItem Value="0">--- เลือกทะเบียนรถ ---</asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-4">
                                                    <asp:LinkButton ID="btnSearchReportCarDetail" runat="server"
                                                        CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearchReportCarDetail" data-original-title="search" data-toggle="tooltip" ValidationGroup="SearchReportDetail"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>

                                                    <asp:LinkButton ID="btnRefreshReport" runat="server"
                                                        CssClass="btn btn-default" OnCommand="btnCommand" CommandName="cmdRefreshReport" data-original-title="refresh" data-toggle="tooltip"><span class="glyphicon glyphicon-refresh"></span> refresh</asp:LinkButton>

                                                </div>
                                                <label class="col-sm-6 control-label"></label>
                                            </div>

                                            <%--<p class="help-block"><font color="red">**</font></p>--%>
                                        </ContentTemplate>
                                        <%-- <Triggers>
                                            <asp:PostBackTrigger ControlID="btnExportReportCarDetail" />
                                        </Triggers>--%>
                                    </asp:UpdatePanel>
                                    <!-- START Report Table -->

                                    <!-- START Report Graph -->
                                    <%--<asp:UpdatePanel ID="Update_PnGraphReportDetail" runat="server" Visible="false" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">ประเภทกราฟ</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlTypeGraph" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกประเภทกราฟ ---</asp:ListItem>
                                                        <asp:ListItem Value="1">จำนวนการจองรถ/หน่วยงาน</asp:ListItem>
                                                        <asp:ListItem Value="2">จำนวนการยกเลิก/หน่วยงาน</asp:ListItem>
                                                        <asp:ListItem Value="3">จำนวนค่าใช้จ่าย/หน่วยงาน</asp:ListItem>
                                                        <asp:ListItem Value="4">จำนวนครั้งที่ไม่ใช้รถ/หน่วยงาน</asp:ListItem>
                                                    </asp:DropDownList>

                                                    <asp:RequiredFieldValidator ID="Re_ddlTypeGraph"
                                                        runat="server"
                                                        InitialValue="0"
                                                        ControlToValidate="ddlTypeGraph" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาเลือกประเภทกราฟ"
                                                        ValidationGroup="SearchReportDetailGraph" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender512212" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_ddlTypeGraph" Width="200" PopupPosition="BottomLeft" />


                                                </div>
                                                <label class="col-sm-2 control-label" runat="server" id="status_report" visible="false">ค้นหาจากสถานะ</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlStatusBooking" runat="server" CssClass="form-control" Visible="false">
                                                        <asp:ListItem Value="0">--- เลือกสถานะเอกสาร ---</asp:ListItem>
                                                        
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label">เดือน</label>
                                                <div class="col-sm-4">
                                                    <asp:DropDownList ID="ddlmonth" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">--- เลือกเดือน ---</asp:ListItem>
                                                        <asp:ListItem Value="1">มกราคม</asp:ListItem>
                                                        <asp:ListItem Value="2">กุมภาพันธ์</asp:ListItem>
                                                        <asp:ListItem Value="3">มีนาคม</asp:ListItem>
                                                        <asp:ListItem Value="4">เมษายน</asp:ListItem>
                                                        <asp:ListItem Value="5">พฤษภาคม</asp:ListItem>
                                                        <asp:ListItem Value="6">มิถุนายน</asp:ListItem>
                                                        <asp:ListItem Value="7">กรกฎาคม</asp:ListItem>
                                                        <asp:ListItem Value="8">สิงหาคม</asp:ListItem>
                                                        <asp:ListItem Value="9">กันยายน</asp:ListItem>
                                                        <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                                        <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                                        <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <label class="col-sm-2 control-label">ปี</label>
                                                <div class="col-sm-4">

                                                    <asp:DropDownList ID="ddlyear" CssClass="form-control" runat="server">
                                                    </asp:DropDownList>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"></label>
                                                <div class="col-sm-4">
                                                    <asp:LinkButton ID="btnSearchGraph" runat="server"
                                                        CssClass="btn btn-primary" OnCommand="btnCommand" CommandName="cmdSearchReportGraph" data-original-title="ค้นหา" data-toggle="tooltip" ValidationGroup="SearchReportDetailGraph"><span class="glyphicon glyphicon-search"></span> ค้นหา</asp:LinkButton>
                                                </div>
                                                <label class="col-sm-6 control-label"></label>
                                            </div>


                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnSearchGraph" />
                                            <asp:AsyncPostBackTrigger ControlID="ddlTypeGraph" EventName="SelectedIndexChanged" />
                                        </Triggers>

                                    </asp:UpdatePanel>--%>
                                    <!-- START Report Graph -->


                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <!-- Detail Search Report CarMA -->
                <asp:UpdatePanel ID="Update_PanelReportTable" runat="server" Visible="false">
                    <ContentTemplate>

                        <div id="divGvTopicDetailMareportGvOut_scroll_x" style="overflow-x: scroll; width: 100%" runat="server">

                            <asp:GridView ID="GvDetailTableReport" runat="server"
                                AutoGenerateColumns="False"
                                CssClass="table table-striped table-bordered table-hover table-responsive"
                                OnPageIndexChanging="gvPageIndexChanging"
                                OnRowDataBound="gvRowDataBound"
                                AllowPaging="True" PageSize="5"
                                ShowFooter="true">
                                <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                                <RowStyle Font-Size="Small" />
                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="left" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <p>
                                                <b>ชื่อผู้บันทึกรายการ :</b>
                                                <asp:Label ID="lbl_u0_document_ma_idx_reportma" Visible="false" runat="server" Text='<%# Eval("u0_document_ma_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_cemp_idx_reportma" runat="server" Visible="false" Text='<%# Eval("cemp_idx") %>'></asp:Label>
                                                <asp:Label ID="lbl_emp_name_th_reportma" runat="server" Text='<%# Eval("emp_name_th") %>'></asp:Label>

                                            </p>
                                            <p>
                                                <b>ฝ่ายผู้บันทึกรายการ :</b>
                                                <asp:Label ID="lbl_dept_name_th_reportma" runat="server" Text='<%# Eval("dept_name_th") %>'></asp:Label>

                                            </p>

                                            <p>
                                                <b>วันที่บันทึกรายการ :</b>
                                                <asp:Label ID="lbl_create_date_reportma" runat="server" Text='<%# Eval("create_date") %>' />
                                                <b>เวลา:</b>
                                                <asp:Label ID="lbl_time_create_date_reportma" runat="server" Text='<%# Eval("time_create_date") %>' />
                                                <b>น.</b>
                                            </p>

                                            <p>
                                                <b>เลขทะเบียน :</b>
                                                <asp:Label ID="lbl_car_register_reportma" runat="server" Text='<%# Eval("car_register") %>'></asp:Label>
                                                <%--<asp:Label ID="lbl_ProvName_reportma" runat="server" Text='<%# Eval("ProvName") %>'></asp:Label>--%>
                                            </p>
                                            <p>
                                                <b>รายละเอียดประเภทรถ :</b>
                                                <asp:Label ID="lbl_detailtype_car_name_reportma" runat="server" Text='<%# Eval("detailtype_car_name") %>'></asp:Label>
                                            </p>
                                            <p>
                                                <b>เลขที่ PR :</b>
                                                <asp:Label ID="lbl_pr_code_reportma" runat="server" Text='<%# Eval("pr_code") %>'></asp:Label>
                                            </p>

                                            <p>
                                                <b>เลขที่ PO :</b>
                                                <asp:Label ID="lbl_po_code_reportma" runat="server" Text='<%# Eval("po_code") %>'></asp:Label>
                                            </p>

                                            <p>
                                                <b>วันที่เข้าซ่อม</b>
                                                <asp:Label ID="lbl_date_start_repair_reportma" runat="server" Text='<%# Eval("date_start_repair") %>' />
                                            </p>
                                            <p>
                                                <b>วันที่ซ่อมเสร็จ</b>
                                                <asp:Label ID="lbl_date_end_repair_reportma" runat="server" Text='<%# Eval("date_end_repair") %>' />
                                            </p>

                                        </ItemTemplate>

                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียดการซ่อม" ItemStyle-HorizontalAlign="left" ItemStyle-Width="30%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <p>
                                                <b>เลขไมค์ครบระยะกำหนด :</b>
                                                <asp:Label ID="lbl_miles_start_reportma" runat="server" Text='<%# Eval("miles_start") %>'></asp:Label>
                                            </p>
                                            <p>
                                                <b>เลขไมค์ล่าสุด :</b>
                                                <asp:Label ID="lbl_miles_current_reportma" runat="server" Text='<%# Eval("miles_current") %>'></asp:Label>
                                            </p>
                                            <p>
                                                <b>บริษัทที่เข้ารับบริการ :</b>
                                                <asp:Label ID="lbl_company_detail_reportma" runat="server" Text='<%# Eval("company_detail") %>'></asp:Label>
                                            </p>
                                            <p>
                                                <b>รายละเอียดเพิ่มเติม :</b>
                                                <asp:Label ID="lbl_note_detail_reportma" runat="server" Text='<%# Eval("note_detail") %>'></asp:Label>
                                            </p>
                                            <p>
                                                <b>ราคารวม :</b>
                                                <asp:Label ID="lbl_price_total_repair_reportma" runat="server" Visible="false" Text='<%# Eval("price_total_repair") %>'></asp:Label>
                                                <asp:Label ID="lbl_price_total_repair_reportma_dicimal" runat="server"></asp:Label>
                                                <b>บาท</b>
                                            </p>
                                            <p>
                                                <b>ส่วนลด :</b>
                                                <asp:Label ID="lbl_discount_reportma" runat="server" Visible="false" Text='<%# Eval("discount") %>'></asp:Label>
                                                <asp:Label ID="lbl_discount_reportma_decimal" runat="server"></asp:Label>
                                                <b>บาท</b>
                                            </p>
                                            <p>
                                                <b>ภาษีมูลค่าเพิ่ม :</b>
                                                <asp:Label ID="lbl_vat_reportma" runat="server" Visible="false" Text='<%# Eval("vat") %>'></asp:Label>
                                                <asp:Label ID="lbl_vat_reportma_decimal" runat="server"></asp:Label>
                                                <b>บาท</b>
                                            </p>
                                            <p>
                                                <b>ราคาสุทธิ :</b>
                                                <asp:Label ID="lbl_price_net_reportma" runat="server" Visible="false" Text='<%# Eval("price_net") %>'></asp:Label>
                                                <asp:Label ID="lbl_price_net_reportma_decimal" runat="server"></asp:Label>
                                                <b>บาท</b>
                                            </p>

                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right;">
                                                <p>
                                                    <asp:Literal ID="lit_price_total_report_" runat="server" Text="ราคารวม :"></asp:Literal>
                                                </p>
                                                <p>
                                                    <asp:Literal ID="lit_discount_report_" runat="server" Text="ส่วนลด :"></asp:Literal>
                                                </p>
                                                <p>
                                                    <asp:Literal ID="lit_vat_report_" runat="server" Text="ภาษีมูลค่าเพิ่ม :"></asp:Literal>
                                                </p>
                                                <p>
                                                    <asp:Literal ID="lit_price_net_report_" runat="server" Text="ราคาสุทธิ :"></asp:Literal>
                                                </p>
                                            </div>
                                        </FooterTemplate>


                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายการหัวข้อ MA" ItemStyle-HorizontalAlign="left" ItemStyle-Width="35%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <div id="divGvTopicDetailMareport_scroll_x" style="overflow-x: scroll; width: 100%" runat="server">
                                                <div id="divGvTopicDetailMareport_scroll_y" style="overflow-y: scroll; width: auto; height: 150px" runat="server">
                                                    <asp:GridView ID="GvDetailTopicReport" runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                                        PageSize="5"
                                                        AllowPaging="false"
                                                        OnRowDataBound="gvRowDataBound"
                                                        ShowFooter="true">
                                                        <HeaderStyle CssClass="success" Height="40px" Font-Size="Small" />
                                                        <RowStyle Font-Size="Small" />
                                                        <PagerStyle CssClass="pageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                                                        </EmptyDataTemplate>
                                                        <Columns>

                                                            <asp:TemplateField HeaderText="รายการหัวข้อ MA" ItemStyle-CssClass="left" ItemStyle-Width="40%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRowNumber" Visible="false" Text='<%# Container.DataItemIndex + 1 %>' runat="server" />
                                                                    <asp:Label ID="lbl_u0_document_ma_idx_mareport" Visible="false" runat="server" Text='<%# Eval("u0_document_ma_idx") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_topic_ma_idx_mareport" runat="server" Visible="false" Text='<%# Eval("topic_ma_idx") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_topic_ma_name_mareport" runat="server" Text='<%# Eval("topic_ma_name") %>'></asp:Label>

                                                                    <asp:Label ID="lbl_test" runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="จำนวน" ItemStyle-CssClass="right" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" ItemStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_quantity_mareport" runat="server" Text='<%# Eval("quantity") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ราคา/หน่วย	" ItemStyle-CssClass="right" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" ItemStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_price_unit_mareport" runat="server" Visible="false" Text='<%# Eval("sum_price_unit_decimal") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_sum_price_unit_decimal_valuereport" runat="server"></asp:Label>
                                                                </ItemTemplate>

                                                                <FooterTemplate>
                                                                    <div style="text-align: right;">
                                                                        <asp:Literal ID="lit_total_report" runat="server" Text="ราคารวม :"></asp:Literal>
                                                                    </div>
                                                                </FooterTemplate>


                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ราคารวม" ItemStyle-CssClass="right" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="right" ItemStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbl_price_total_mareport" runat="server" Visible="false" Text='<%# Eval("sum_price_total_decimal") %>'></asp:Label>
                                                                    <asp:Label ID="lbl_sum_price_total_decimal_valuereport" runat="server"></asp:Label>
                                                                </ItemTemplate>

                                                                <FooterTemplate>
                                                                    <div style="text-align: right; background-color: chartreuse;">

                                                                        <asp:Label ID="lit_total_price_report" runat="server" Font-Bold="true"></asp:Label>
                                                                    </div>
                                                                </FooterTemplate>

                                                            </asp:TemplateField>


                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                        </ItemTemplate>

                                        <FooterTemplate>
                                            <div style="text-align: right; background-color: chartreuse;">
                                                <p>
                                                    <asp:Label ID="lit_price_total_report" runat="server" Font-Bold="true"></asp:Label>

                                                </p>
                                                <p>
                                                    <asp:Label ID="lit_discount_report" runat="server" Font-Bold="true"></asp:Label>

                                                </p>
                                                <p>
                                                    <asp:Label ID="lit_vat_report" runat="server" Font-Bold="true"></asp:Label>

                                                </p>
                                                <p>
                                                    <asp:Label ID="lit_price_net_report" runat="server" Font-Bold="true"></asp:Label>

                                                </p>

                                            </div>
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ไฟล์แนบ" ItemStyle-HorizontalAlign="left" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:HyperLink runat="server" ID="btnViewFileCarmaReport" CssClass="pull-letf" data-toggle="tooltip" title="view" data-original-title="" Target="_blank"><i class="fa fa-file"></i> ดูข้อมูลไฟล์แนบ</asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- <asp:TemplateField HeaderText="ดูข้อมูล" ItemStyle-HorizontalAlign="center" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                                    <ItemTemplate>



                                        <asp:LinkButton ID="btnViewDetailReportTable" CssClass="btn-sm btn-info" runat="server" CommandName="cmdViewDetailReportTable"
                                            OnCommand="btnCommand" CommandArgument='<%# Eval("costcenter_idx") + ";" + Eval("rdept_idx")%>'
                                            data-toggle="tooltip" title="ดูรายละเอียด"><i class="fa fa-list"></i></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Detail Search Report CarMA -->

            </div>
        </asp:View>
        <!--View Report Detail Car-->

        <!--View Master Data MA-->
        <asp:View ID="docMasterMA" runat="server">
            <div class="col-md-12">
                <div class="form-group">
                    <asp:LinkButton ID="btn_AddTopicMA" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่มหัวข้อ MA" data-toggle="tooltip" title="เพิ่มหัวข้อ MA" runat="server"
                        CommandName="cmdAddTopicMA" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มหัวข้อ MA</asp:LinkButton>
                </div>

                <asp:FormView ID="FvInsert" runat="server" DefaultMode="ReadOnly" Width="100%">
                    <InsertItemTemplate>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title">เพิ่มรายละเอียดหัวข้อ MA</h3>
                            </div>
                            <div class="panel-body">
                                <div class="panel-heading">

                                    <div class="form-horizontal" role="form">

                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="lbltopic_name" runat="server" Text="ชื่อหัวข้อ MA" CssClass="col-sm-3 control-label"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="txt_topic_name" runat="server" CssClass="form-control" placeholder="กรอกชื่อหัวข้อ MA ..." Enabled="true" />

                                                    <asp:RequiredFieldValidator ID="Re_txt_topic_name"
                                                        runat="server"
                                                        ControlToValidate="txt_topic_name"
                                                        Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณากรอกชื่อหัวข้อ MA"
                                                        ValidationGroup="Save" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="aj_pname" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_topic_name" Width="220" />
                                                </div>
                                                <div class="col-sm-3"></div>
                                            </div>

                                            <%--<div class="form-group">
                                            <asp:Label ID="lbl_detailtopic_name" runat="server" Text="รายละเอียดเพิ่มเติม" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_detailtopic_name" runat="server" CssClass="form-control" TextMode="multiline" Rows="3" placeholder="รายละเอียดเพิ่มเติม ..." Enabled="true" />

                                               
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>--%>

                                            <%-- <div class="form-group">
                                            <asp:Label ID="lblplace_code" runat="server" Text="ชื่อสถานที่ย่อ(en)" CssClass="col-sm-3 control-label"></asp:Label>
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txtplace_code" runat="server" CssClass="form-control" placeholder="กรอกชื่อสถานที่ย่อ(en) ..." Enabled="true" />
                                                <asp:RequiredFieldValidator ID="Re_place_code" runat="server"
                                                    ControlToValidate="txtplace_code" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อสถานที่ย่อ(en)" ValidationGroup="Save" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="aj_pcode" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_place_code" Width="220" />
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>--%>

                                            <div class="form-group">
                                                <asp:Label ID="lbl_status_topic" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlStatusTopic" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="0" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-sm-3"></div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-3">
                                                    <asp:LinkButton ID="btnSave" ValidationGroup="Save" CssClass="btn btn-success" Text="Save" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="CmdSaveTopicMA" OnCommand="btnCommand"></asp:LinkButton>
                                                    <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </InsertItemTemplate>
                </asp:FormView>

                <%--select Place--%>
                <asp:GridView ID="GvDetail" runat="server" Visible="true"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                    HeaderStyle-CssClass="success"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowDataBound="Master_RowDataBound"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowEditing="Master_RowEditing"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    AutoPostBack="false">

                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style="text-align: center; padding-top: 5px;">
                                        <asp:Label ID="lbl_topic_ma_idx" runat="server" Visible="false" Text='<%# Eval("topic_ma_idx") %>' />
                                        <%# (Container.DataItemIndex +1) %>
                                    </div>
                                </small>

                            </ItemTemplate>
                            <EditItemTemplate>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <asp:TextBox ID="txt_topic_ma_idx_edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("topic_ma_idx") %>'></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <asp:Label ID="lbltopic_ma_name_edit" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อหัวข้อ MA" />
                                            <div class="col-sm-6">
                                                <asp:TextBox ID="txt_topic_ma_name_edit" runat="server" CssClass="form-control " Text='<%# Eval("topic_ma_name") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="Re_txt_topic_ma_name_edit" runat="server"
                                                    ControlToValidate="txt_topic_ma_name_edit" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณากรอกชื่อหัวข้อ MA" ValidationGroup="Editpname" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender_pname" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_topic_ma_name_edit" Width="220" />
                                            </div>
                                            <div class="col-sm-2"></div>
                                        </div>

                                        <%--<div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อหัวข้อ MA" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_detail_ma_name_edit" runat="server" CssClass="form-control " TextMode="multiline" Rows="3" placeholder="กรอกรายละเอียดเพิ่มเติม ..." Text='<%# Eval("detail_ma_name") %>'></asp:TextBox>
                                            
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>--%>

                                        <div class="form-group">
                                            <asp:Label ID="lbl_topic_ma_status_edit" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                            <div class="col-sm-6">
                                                <asp:DropDownList ID="ddltopic_ma_status_edit" Text='<%# Eval("topic_ma_status") %>'
                                                    CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="1">Online</asp:ListItem>
                                                    <asp:ListItem Value="0">Offline</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-sm-3"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-3"></div>
                                            <div class="col-sm-9">
                                                <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editpname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                    Text="Save" CommandName="Update"></asp:LinkButton>
                                                <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                    Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อหัวข้อ MA" ItemStyle-HorizontalAlign="left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style="padding-top: 5px;">
                                        <asp:Label ID="lbl_topic_ma_name_detail" runat="server"
                                            Text='<%# Eval("topic_ma_name") %>'></asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <%--<asp:TemplateField HeaderText="รายละเอียดเพิ่มเติม" ItemStyle-HorizontalAlign="left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <div style="padding-top: 5px;">
                                    <asp:Label ID="lbl_detail_ma_name_detail" runat="server"
                                        Text='<%# Eval("detail_ma_name") %>'></asp:Label>
                                </div>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>--%>

                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">

                            <ItemTemplate>
                                <small>
                                    <div style="padding-top: 5px;">
                                        <asp:Label ID="lbl_topic_ma_status" Visible="false" runat="server"
                                            CssClass="col-sm-12" Text='<%# Eval("topic_ma_status") %>'></asp:Label>
                                        <asp:Label ID="topic_ma_statusOnline" runat="server" Visible="false"
                                            data-toggle="tooltip" data-placement="top" title="Online"
                                            CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                        </asp:Label>
                                        <asp:Label ID="topic_ma_statusOffline" runat="server" Visible="false"
                                            data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                        </asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="15%" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <div style="padding-top: 5px">
                                    <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                        data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                                    <asp:LinkButton ID="btnTodelete" CssClass="text-trash" runat="server" CommandName="cmdDelete"
                                        data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบสถานที่นี้ใช่หรือไม่ ?')"
                                        CommandArgument='<%#Eval("topic_ma_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>
        <!--View Master Data MA-->

    </asp:MultiView>

    <script type="text/javascript">
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.from-deadline-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
            $('.from-dateinsurance-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });

        $('.show-from-onclick').click(function () {
            $('.from-date-datepicker').data("DateTimePicker").show();
        });
        $('.showdeadline-from-onclick').click(function () {
            $('.from-deadline-datepicker').data("DateTimePicker").show();
        });
        $('.showinsurance-from-onclick').click(function () {
            $('.from-dateinsurance-datepicker').data("DateTimePicker").show();
        });



        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('.from-deadline-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
                $('.from-dateinsurance-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });

            $('.show-from-onclick').click(function () {
                $('.from-date-datepicker').data("DateTimePicker").show();
            });
            $('.showdeadline-from-onclick').click(function () {
                $('.from-deadline-datepicker').data("DateTimePicker").show();
            });
            $('.showinsurance-from-onclick').click(function () {
                $('.from-dateinsurance-datepicker').data("DateTimePicker").show();
            });
        });

    </script>

    <script>
        $(function () {
            $('.UploadFileCar').MultiFile({

            });

        });
        $(function () {
            $('.UploadFileCarEdit').MultiFile({

            });

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.UploadFileCar').MultiFile({

                });

            });

            $(function () {
                $('.UploadFileCarEdit').MultiFile({

                });

            });
        });



        //$(document).ready(function () { UploadFileCarEdit
        //    window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

        //    function pageLoad() {
        //        $("#UploadFileCar").MultiFile();
        //    }
        //})
    </script>

    <script type="text/javascript">
        function confirmWithOutValidated(message, validateGroup) {
            if (validateGroup != '') {
                var validated = Page_ClientValidate(validateGroup);
                if (validated) {
                    return confirm(message);
                }
            } else {
                return confirm(message);
            }
        }
        $(function () {
            $('.datetimepicker-from').datetimepicker({

                format: 'DD/MM/YYYY',
                //minDate: moment().add(-1, 'day'),
                //maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true

            });
            $('.datetimepicker-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                //}

            });
            $('.show-fromto-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').datetimepicker({
                format: 'DD/MM/YYYY',
                //minDate: moment().add(-1, 'day'),
                //maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true
            });
            $('.show-toto-onclick').click(function () {
                $('.datetimepicker-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));

                //}
                //$('.txtSearchToHidden').val($('.datetimepicker-to').val());
                <%--$('#<%= txtSearchToHidden.ClientID %>').val($('.datetimepicker-to').val());--%>
            });
        });
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            //var dateToday = new Date();
            //currentTime = new Date();
            $('.datetimepicker-from').datetimepicker({

                format: 'DD/MM/YYYY',
                //minDate: moment().add(-1, 'day'),
                //maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true

            });
            $('.datetimepicker-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                //}

            });
            $('.show-fromto-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').datetimepicker({
                format: 'DD/MM/YYYY',
                //minDate: moment().add(-1, 'day'),
                //maxDate: moment().add(30, 'day'),
                //ignoreReadonly: true
            });
            $('.show-toto-onclick').click(function () {
                $('.datetimepicker-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));

                //}
                //$('.txtSearchToHidden').val($('.datetimepicker-to').val());
                <%--$('#<%= txtSearchToHidden.ClientID %>').val($('.datetimepicker-to').val());--%>
            });

            /* START Filter DatetimePicker Permission Temporary */
            $('.datetimepicker-filter-from').datetimepicker({
                format: 'DD/MM/YYYY',
                //ignoreReadonly: true
            });
            $('.datetimepicker-filter-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
               <%-- $('#<%= txtFilterIndexPermTempFromHidden.ClientID %>').val($('.datetimepicker-filter-from').val());--%>
            });
            $('.show-filter-from-onclick').click(function () {
                $('.datetimepicker-filter-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-to').datetimepicker({
                format: 'DD/MM/YYYY',
                //ignoreReadonly: true
            });
            $('.show-filter-to-onclick').click(function () {
                $('.datetimepicker-filter-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermTempToHidden.ClientID %>').val($('.datetimepicker-filter-to').val());--%>
            });
            $('.datetimepicker-filter-only').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-only-onclick').click(function () {
                $('.datetimepicker-filter-only').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-only').on('dp.change', function (e) {
                <%--$('#<%= txtFilterIndexPermTempOnlyHidden.ClientID %>').val($('.datetimepicker-filter-only').val());--%>
            });
            /* END Filter Datetimepicker Permission Temporary */
            /* START Filter Datetimepicker Permission Permanent */
            $('.datetimepicker-filter-perm-from').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.datetimepicker-filter-perm-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-filter-perm-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-filter-perm-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermPermFromHidden.ClientID %>').val($('.datetimepicker-filter-perm-from').val());--%>
            });
            $('.show-filter-perm-from-onclick').click(function () {
                $('.datetimepicker-filter-perm-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-to-onclick').click(function () {
                $('.datetimepicker-filter-perm-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-filter-perm-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-filter-perm-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                <%--$('#<%= txtFilterIndexPermPermToHidden.ClientID %>').val($('.datetimepicker-filter-perm-to').val());--%>
            });
            $('.datetimepicker-filter-perm-only').datetimepicker({
                format: 'DD/MM/YYYY',
                ignoreReadonly: true
            });
            $('.show-filter-perm-only-onclick').click(function () {
                $('.datetimepicker-filter-perm-only').data("DateTimePicker").show();
            });
            $('.datetimepicker-filter-perm-only').on('dp.change', function (e) {
                <%--$('#<%= txtFilterIndexPermPermOnlyHidden.ClientID %>').val($('.datetimepicker-filter-perm-only').val());--%>
            });
            /* END Filter Datetimepicker Permission Permanent */
        });
    </script>

</asp:Content>

