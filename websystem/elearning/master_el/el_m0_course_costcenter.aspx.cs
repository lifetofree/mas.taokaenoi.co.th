﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_elearning_master_el_el_m0_course_costcenter : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    data_elearning _data_elearning = new data_elearning();
    service_execute _serviceExecute = new service_execute();

    string _localXml = String.Empty;
    string _localString = String.Empty;
    string _localJson = String.Empty;
    int _actionType = 0;
    int _emp_idx = 0;
    int _default_int = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlget_course_costcenter = _serviceUrl + ConfigurationManager.AppSettings["urlget_course_costcenter"];
    static string _urlGetCostcenterOld = _serviceUrl + ConfigurationManager.AppSettings["urlGetCostcenterOld"];
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = Session["emp_idx"].ToString();
            ViewState["ddl_cost_center_search"] = 0;

            SetDefaultpage(1);

        }


        linkBtnTrigger(btnaddholder);
        linkBtnTrigger(btnAdddata_import);

    }
    #endregion

    #region INSERT&SELECT&UPDATE

    protected void Insert_Status()
    {
        _data_elearning.m0_course_cost_action = new m0_course_cost[1];
        m0_course_cost dtelearning = new m0_course_cost();

        //dtelearning.CCIDX = txtcusname.Text;
        dtelearning.cost_idx = int.Parse(ddlcost_center.SelectedValue);
        dtelearning.course_nub = txtcourse_nub.Text;
        // dtelearning.course_detail = txtcourse_detail.Text;
        dtelearning.year_cost = int.Parse(ddlyear.SelectedValue);
        //dtelearning.course_status = int.Parse(ddl_status.SelectedValue);
        dtelearning.create_idx = int.Parse(ViewState["EmpIDX"].ToString());
        dtelearning.type_select_course = 1; //insert

        _data_elearning.m0_course_cost_action[0] = dtelearning;
        //text.Text = dtemployee.ToString();
        _data_elearning = callServiceElearning(_urlget_course_costcenter, _data_elearning);
    }

    protected void SelectMasterList()
    {
        _data_elearning.m0_course_cost_action = new m0_course_cost[1];
        m0_course_cost dtelearning = new m0_course_cost();

        dtelearning.cost_idx = int.Parse(ViewState["ddl_cost_center_search"].ToString()); //select
        dtelearning.type_select_course = 2; //select

        _data_elearning.m0_course_cost_action[0] = dtelearning;

        _data_elearning = callServiceElearning(_urlget_course_costcenter, _data_elearning);
        //text.Text = _dataEmployee.ToString();
        setGridData(GvMaster, _data_elearning.m0_course_cost_action);
    }

    protected void SelectDetailList()
    {
        _data_elearning.m0_course_cost_action = new m0_course_cost[1];
        m0_course_cost dtelearning = new m0_course_cost();

        dtelearning.CCIDX = int.Parse(ViewState["CCIDX_Detail"].ToString()); //select
        dtelearning.type_select_course = 6; //select

        _data_elearning.m0_course_cost_action[0] = dtelearning;

        _data_elearning = callServiceElearning(_urlget_course_costcenter, _data_elearning);
        //text.Text = _dataEmployee.ToString();
        setGridData(GvHistory, _data_elearning.m0_course_cost_action);
    }

    protected data_employee callService(string _cmdUrl, data_employee _dtmaster)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmaster);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //text.Text = _localJson.ToString();
        _dtmaster = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtmaster;
    }

    protected void Update_Master_List()
    {
        _data_elearning.m0_course_cost_action = new m0_course_cost[1];
        m0_course_cost dtelearning = new m0_course_cost();

        dtelearning.CCIDX = int.Parse(ViewState["CostIDX_update"].ToString());
        dtelearning.cost_idx = int.Parse(ViewState["ddlcost_center_edit"].ToString());
        dtelearning.course_nub = ViewState["txtcourse_nub_edit"].ToString();
        dtelearning.course_detail = ViewState["txtcourse_detail_edit"].ToString();
        //dtelearning.course_status = int.Parse(ViewState["ddStatus_update"].ToString());
        dtelearning.create_idx = int.Parse(ViewState["EmpIDX"].ToString());
        dtelearning.type_select_course = 3; //update

        _data_elearning.m0_course_cost_action[0] = dtelearning;
        //text.Text = dtemployee.ToString();
        _data_elearning = callServiceElearning(_urlget_course_costcenter, _data_elearning);
    }

    protected void Delete_Master_List()
    {
        _data_elearning.m0_course_cost_action = new m0_course_cost[1];
        m0_course_cost dtelearning = new m0_course_cost();

        dtelearning.CCIDX = int.Parse(ViewState["CCIDX_delete"].ToString());
        dtelearning.type_select_course = 4; //select

        _data_elearning.m0_course_cost_action[0] = dtelearning;

        _data_elearning = callServiceElearning(_urlget_course_costcenter, _data_elearning);
        //text.Text = _dataEmployee.ToString();
        setGridData(GvMaster, _data_elearning.m0_course_cost_action);
    }

    protected void Select_Coscenter(DropDownList ddlName)
    {
        ddlName.Items.Clear();
        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Add(new ListItem("เลือก CostCenter...", "0"));

        _dataEmployee.CostCenterDetail = new CostCenter[1];
        CostCenter dtemployee = new CostCenter();

        _dataEmployee.CostCenterDetail[0] = dtemployee;

        _dataEmployee = callServiceEmployee(_urlGetCostcenterOld, _dataEmployee);

        ddlName.DataSource = _dataEmployee.CostCenterDetail;
        ddlName.DataTextField = "costcenter_name";
        ddlName.DataValueField = "CostIDX";
        ddlName.DataBind();
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);
        return _dataEmployee;
    }

    protected data_elearning callServiceElearning(string _cmdUrl, data_elearning _data_elearning)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_elearning);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _data_elearning = (data_elearning)_funcTool.convertJsonToObject(typeof(data_elearning), _localJson);
        return _data_elearning;
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    #endregion

    #region Gridview

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        var ddlcost_center_edit = (DropDownList)e.Row.FindControl("ddlcost_center_edit");
                        var lblcost_center_edit = (Label)e.Row.FindControl("lblcost_center_edit");
                        
                        Select_Coscenter(ddlcost_center_edit);
                        ddlcost_center_edit.SelectedValue = lblcost_center_edit.Text;
                    }
                    else
                    {
                        var lblcourse_nub = (Label)e.Row.FindControl("lblcourse_nub");
                        var lbprice = (Label)e.Row.FindControl("lbprice");
                        var lblcost_balance = (Label)e.Row.FindControl("lblcost_balance");
                        var lbprice_balance = (Label)e.Row.FindControl("lbprice_balance");

                        lbprice.Text = string.Format("{0:N2}", Convert.ToDecimal(lblcourse_nub.Text));
                        lbprice_balance.Text = string.Format("{0:N2}", Convert.ToDecimal(lblcost_balance.Text));

                    }

                }

                break;

            case "GvHistory":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex != e.Row.RowIndex)
                    {
                        var lbcost_price = (Label)e.Row.FindControl("lbcost_price");
                        var lbprice = (Label)e.Row.FindControl("lbprice");

                        lbprice.Text = string.Format("{0:N2}", Convert.ToDecimal(lbcost_price.Text));
                    }
                   
                }

                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();
                break;
        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":
                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();
                btnaddholder.Visible = false;
                div_search.Visible = false;
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                btnaddholder.Visible = true;
                div_search.Visible = true;
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int CCIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtcourse_nub_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtcourse_nub_edit");
                var txtcourse_detail_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtcourse_detail_edit");
                var ddlcost_center_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlcost_center_edit");
                var ddStatus_update = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatus_update");

                GvMaster.EditIndex = -1;

                ViewState["CostIDX_update"] = CCIDX;
                ViewState["txtcourse_nub_edit"] = txtcourse_nub_edit.Text;
                ViewState["txtcourse_detail_edit"] = txtcourse_detail_edit.Text;
                ViewState["ddlcost_center_edit"] = ddlcost_center_edit.SelectedValue;
                //ViewState["ddStatus_update"] = ddStatus_update.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion


    #endregion


    #region SetDefault
    protected void SetDefaultpage(int page)
    {

        switch (page)
        {

            case 1:
                MvMaster.SetActiveView(ViewIndex);
                SelectMasterList();
                Select_Coscenter(ddl_cost_center_search);
                btnaddholder.Visible = true;
                Panel_Add.Visible = false;
                div_search.Visible = true;
                GenerateddlYear(ddlyear);
                ddlchoose_addcost.SelectedValue = "0";
                div_addmanual.Visible = false;
                div_addimport.Visible = false;
                linkBtnTrigger(btnAdddata_import);
                break;

            case 2:
                MvMaster.SetActiveView(ViewDetail);
                SelectDetailList();
                break;

        }
    }


    protected void GenerateddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 1; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }

        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected void ImportFileNumber(string filePath, string Extension, string isHDR, string fileName, int cempidx, int condition)
    {
        string conStr = String.Empty;
        conStr = ConfigurationManager.ConnectionStrings["Excel03ConString"].ConnectionString;
        conStr = String.Format(conStr, filePath, "Yes");
        OleDbConnection connExcel = new OleDbConnection(conStr);
        OleDbCommand cmdExcel = new OleDbCommand();
        OleDbDataAdapter oda = new OleDbDataAdapter();
        DataTable dt = new DataTable();

        cmdExcel.Connection = connExcel;
        connExcel.Open();
        DataTable dtExcelSchema;
        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
        connExcel.Close();
        connExcel.Open();
        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
        oda.SelectCommand = cmdExcel;
        oda.Fill(dt);
        connExcel.Close();

        _data_elearning = new data_elearning();

        string idxCreated = String.Empty;
        for (var i = 0; i <= dt.Rows.Count - 1;)
        {
            if (dt.Rows[i][0].ToString().Trim() != "")
            {
                M1_ImportCost import = new M1_ImportCost();
                _data_elearning.Boxm1_ImportCost = new M1_ImportCost[1];

                import.year_import = int.Parse(dt.Rows[i][0].ToString().Trim());
                import.cost_name = dt.Rows[i][1].ToString().Trim();
                import.cost_price = dt.Rows[i][2].ToString().Trim();
                import.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
                _data_elearning.Boxm1_ImportCost[0] = import;

                _data_elearning.m0_course_cost_action = new m0_course_cost[1];
                m0_course_cost _select = new m0_course_cost();

                _select.type_select_course = condition;

                _data_elearning.m0_course_cost_action[0] = _select;

             //   text.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_elearning));

                _data_elearning = callServiceElearning(_urlget_course_costcenter, _data_elearning);
            }
            i++;
        }
    }


    #endregion


    #region SelectedIndexChanged
    protected void SelectedIndexChanged(object sender, EventArgs e)
    {
        if (sender is DropDownList)
        {
            DropDownList ddlName = (DropDownList)sender;

            switch (ddlName.ID)
            {

                case "ddlchoose_addcost":
                    if (ddlchoose_addcost.SelectedValue == "1")
                    {
                        div_addmanual.Visible = true;
                        div_addimport.Visible = false;
                    }
                    else if (ddlchoose_addcost.SelectedValue == "2")
                    {
                        div_addmanual.Visible = false;
                        div_addimport.Visible = true;

                    }
                    break;

            }
        }
    }
    #endregion



    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAddHolder":
                btnaddholder.Visible = false;
                Panel_Add.Visible = true;
                Select_Coscenter(ddlcost_center);
                div_search.Visible = false;
                linkBtnTrigger(btnAdddata_import);

                break;

            case "btnCancel":
                SetDefaultpage(1);
                break;

            case "btnAdd":
                Insert_Status();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "CmdSearch":
                ViewState["ddl_cost_center_search"] = ddl_cost_center_search.SelectedValue;
                SelectMasterList();
                break;

            case "CmdRefresh":
                ddl_cost_center_search.SelectedValue = "0";
                ViewState["ddl_cost_center_search"] = ddl_cost_center_search.SelectedValue;
                SelectMasterList();
                break;

            case "CmdDetail":
                int CCIDX_detail = int.Parse(cmdArg);
                ViewState["CCIDX_Detail"] = CCIDX_detail;
                SetDefaultpage(2);
                break;

            case "BtnBack":
                SetDefaultpage(1);
                break;

            case "CmdImportMaster":
                if (UploadFile.HasFile)
                {
                    string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff_import");
                    string FileName = Path.GetFileName(UploadFile.PostedFile.FileName);
                    string extension = Path.GetExtension(UploadFile.PostedFile.FileName);
                    string newFileName = datetimeNow + extension.ToLower();
                    string folderPath = ConfigurationManager.AppSettings["path_costcenter_elearning_import"];
                    string filePath = Server.MapPath(folderPath + newFileName);
                    if (extension.ToLower() == ".xls" || extension.ToLower() == ".xlsx" || extension.ToLower() == ".csv" || extension.ToLower() == ".ods")
                    {
                        UploadFile.SaveAs(filePath);
                        ImportFileNumber(filePath, extension, "Yes", FileName, int.Parse(ViewState["EmpIDX"].ToString()), 7);
                        File.Delete(filePath);
                        SetDefaultpage(1);
                    }
                    else
                    {
                        _funcTool.showAlert(this, "Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls หรือ .xlsx) เท่านั้น");
                    }
                }
                break;

        }

    }
    #endregion
}