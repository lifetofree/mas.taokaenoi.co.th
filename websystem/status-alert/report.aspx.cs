﻿using System;
using System.Web.UI.WebControls;
using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Helpers;
using System.Web.UI;

public partial class websystem_statal_ipaddress : System.Web.UI.Page
{
   #region Init
   data_statal dataStatal = new data_statal();
   service_execute servExec = new service_execute();
   function_tool _funcTool = new function_tool();
   string misConn = "conn_mis";
   string misConnReal = "conn_mis_real";
   string statalIPAddressService = "statalIPAddressService";
   string statalLogIPAddressService = "statalLogIPAddressService";
   string _local_xml = String.Empty;
   #endregion Init

   #region Constant
   public static class Constants
   {
      #region for region Action
      public const int SELECT_ALL = 20;
      public const int CREATE = 10;
      public const int UPDATE = 11;
      public const int SELECT_WHERE = 21;
      public const int SELECT_WHERE_CHART = 22;
      public const int BAN = 30;
      public const int UNBAN = 31;
      public const int SELECT_WHERE_EXISTS_IPADDRESS_NAME = 28;
      public const int SELECT_WHERE_EXISTS_IPADDRESS_NAME_UPDATE = 29;
      public const string VALID_FALSE = "101";
      #endregion for region Action
   }
   #endregion Constant

   #region Page Load
   protected void Page_Load(object sender, EventArgs e)
   {
      if (!IsPostBack)
      {
         ViewState["searchClick"] = "0";
         ViewState["keywordSearch"] = String.Empty;
         ViewState["txtSearchFrom"] = String.Empty;
         ViewState["txtSearchTo"] = String.Empty;
         visibleReport("table");
         actionIndex();
      }
   }
   #endregion Page Load

   #region Action
   protected void actionIndex()
   {
      log_ipaddress objStatal = new log_ipaddress();
      dataStatal.statal_log_ipaddress_action = new log_ipaddress[1];
      dataStatal.statal_log_ipaddress_action[0] = objStatal;
      _local_xml = servExec.actionExec(misConn, "data_statal", statalLogIPAddressService, dataStatal, Constants.SELECT_WHERE);
      dataStatal = (data_statal)_funcTool.convertXmlToObject(typeof(data_statal), _local_xml);
      setGridData(gvPingableReport, dataStatal.statal_log_ipaddress_action);
   }

   protected void actionIndexSearch(string keywordSearch = "", string from = "", string to = "")
   {
      log_ipaddress objStatal = new log_ipaddress();
      dataStatal.statal_log_ipaddress_action = new log_ipaddress[1];
      objStatal.keyword_search = keywordSearch.Trim();
      objStatal.search_from_date_time = convertDateToDB(from);
      objStatal.search_to_date_time = convertDateToDB(to);
      dataStatal.statal_log_ipaddress_action[0] = objStatal;
      _local_xml = servExec.actionExec(misConn, "data_statal", statalLogIPAddressService, dataStatal, Constants.SELECT_WHERE);
      dataStatal = (data_statal)_funcTool.convertXmlToObject(typeof(data_statal), _local_xml);
      setGridData(gvPingableReport, dataStatal.statal_log_ipaddress_action);
   }

   protected void actionIndexChart(string keywordSearch = "", string from = "", string to = "")
   {
      log_ipaddress objStatal = new log_ipaddress();
      dataStatal.statal_log_ipaddress_action = new log_ipaddress[1];
      objStatal.keyword_search = keywordSearch.Trim();
      objStatal.search_from_date_time = from;
      objStatal.search_to_date_time = to;
      dataStatal.statal_log_ipaddress_action[0] = objStatal;
      _local_xml = servExec.actionExec(misConn, "data_statal", statalLogIPAddressService, dataStatal, Constants.SELECT_WHERE_CHART);
      dataStatal = (data_statal)_funcTool.convertXmlToObject(typeof(data_statal), _local_xml);
      if (dataStatal.return_code == 0)
      {
         int count = dataStatal.statal_log_ipaddress_action.Length;
         string[] ipaddressName = new string[count];
         object[] ipaddressCount = new object[count];
         int i = 0;
         foreach (var data in dataStatal.statal_log_ipaddress_action)
         {
            ipaddressName[i] = data.ipaddress_name.ToString();
            ipaddressCount[i] = data.count_ipaddress_name.ToString();
            i++;
         }
         Highcharts chart = new Highcharts("chart");
         chart.SetTitle(new Title { Text = "" });
         chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
         chart.SetXAxis(new XAxis { Categories = ipaddressName });
         chart.SetSeries(
             new Series { Name = "IP Address", Data = new Data(ipaddressCount) }
         );
         litReportChart.Text = chart.ToHtmlString();
      }
      else
      {
         litReportChart.Text = "<p class='bg-warning'>No result.</p>";
      }
   }

   protected void actionIndexChartSearch(string keywordSearch = "", string from = "", string to = "")
   {
      log_ipaddress objStatal = new log_ipaddress();
      dataStatal.statal_log_ipaddress_action = new log_ipaddress[1];
      objStatal.keyword_search = keywordSearch.Trim();
      objStatal.search_from_date_time = convertDateToDB(from);
      objStatal.search_to_date_time = convertDateToDB(to);
      dataStatal.statal_log_ipaddress_action[0] = objStatal;
      _local_xml = servExec.actionExec(misConn, "data_statal", statalLogIPAddressService, dataStatal, Constants.SELECT_WHERE_CHART);
      dataStatal = (data_statal)_funcTool.convertXmlToObject(typeof(data_statal), _local_xml);
      if (dataStatal.return_code == 0)
      {
         int count = dataStatal.statal_log_ipaddress_action.Length;
         string[] ipaddressName = new string[count];
         object[] ipaddressCount = new object[count];
         int i = 0;
         foreach (var data in dataStatal.statal_log_ipaddress_action)
         {
            ipaddressName[i] = data.ipaddress_name.ToString();
            ipaddressCount[i] = data.count_ipaddress_name.ToString();
            i++;
         }
         Highcharts chart = new Highcharts("chart");
         chart.SetTitle(new Title { Text = "" });
         chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
         chart.SetXAxis(new XAxis { Categories = ipaddressName });
         chart.SetSeries(
             new Series { Name = "IP Address", Data = new Data(ipaddressCount) }
         );
         litReportChart.Text = chart.ToHtmlString();
      }
      else
      {
         litReportChart.Text = "<p class='bg-warning'>No result.</p>";
      }
   }
   #endregion Action

   #region btnCommand
   protected void btnCommand(object sender, CommandEventArgs e)
   {
      string cmdName = e.CommandName.ToString();
      string cmdArg = e.CommandArgument.ToString();
      switch (cmdName)
      {
         case "btnSearchReport":
            ViewState["searchClick"] = "1";
            ViewState["keywordSearch"] = keywordSearch.Text.Trim();
            ViewState["txtSearchFrom"] = txtSearchFromHidden.Value;
            ViewState["txtSearchTo"] = txtSearchToHidden.Value;
            actionIndexSearch(ViewState["keywordSearch"].ToString(), ViewState["txtSearchFrom"].ToString(), ViewState["txtSearchTo"].ToString());
            actionIndexChartSearch(ViewState["keywordSearch"].ToString(), ViewState["txtSearchFrom"].ToString(), ViewState["txtSearchTo"].ToString());
            keywordSearch.Text = ViewState["keywordSearch"].ToString();
            txtSearchFrom.Text = ViewState["txtSearchFrom"].ToString();
            txtSearchTo.Text = ViewState["txtSearchTo"].ToString();
            txtSearchFromHidden.Value = ViewState["txtSearchFrom"].ToString();
            txtSearchToHidden.Value = ViewState["txtSearchTo"].ToString();
            break;

         case "btnToReportTable":
            if (ViewState["viewReportType"].ToString() != "table")
            {
               visibleReport("table");
               if (ViewState["keywordSearch"].ToString() != String.Empty || ViewState["txtSearchFrom"].ToString() != String.Empty || ViewState["txtSearchTo"].ToString() != String.Empty)
               {
                  actionIndexSearch(ViewState["keywordSearch"].ToString(), ViewState["txtSearchFrom"].ToString(), ViewState["txtSearchTo"].ToString());
               }
               else
               {
                  actionIndex();
               }
            }
            break;

         case "btnToReportChart":
            if (ViewState["viewReportType"].ToString() != "chart")
            {
               visibleReport("chart");
               if (ViewState["keywordSearch"].ToString() != String.Empty || ViewState["txtSearchFrom"].ToString() != String.Empty || ViewState["txtSearchTo"].ToString() != String.Empty)
               {
                  actionIndexChartSearch(ViewState["keywordSearch"].ToString(), ViewState["txtSearchFrom"].ToString(), ViewState["txtSearchTo"].ToString());
               }
               else
               {
                  actionIndexChart();
               }
            }
            break;
      }
   }
   #endregion btnCommand

   #region Custom Functions
   protected void setGridData(GridView gvName, Object obj)
   {
      gvName.DataSource = obj;
      gvName.DataBind();
   }

   protected void visibleReport(string viewStateName)
   {
      ViewState["viewReportType"] = viewStateName;
      if (ViewState["viewReportType"].ToString() == "table")
      {
         divReportTable.Visible = true;
         divReportChart.Visible = false;
      }
      else if (ViewState["viewReportType"].ToString() == "chart")
      {
         divReportTable.Visible = false;
         divReportChart.Visible = true;
      }
      else
      {
         divReportTable.Visible = false;
         divReportChart.Visible = false;
      }
   }

   protected string convertDateToDB(string datetimeStr)
   {
      if (datetimeStr != String.Empty)
      {
         var datetime = datetimeStr.Split(' ');
         var date = datetime[0].Split('/');
         var dateCv = date[2] + "-" + date[1] + "-" + date[0];
         return dateCv + " " + datetime[1];
      }
      return String.Empty;
   }

   protected void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
   {
      var gvName = (GridView)sender;
      switch (gvName.ID)
      {
         case "gvPingableReport":
            gvName.PageIndex = e.NewPageIndex;
            if (ViewState["searchClick"].ToString() == "0")
            {
               actionIndex();
            }
            else
            {
               actionIndexSearch(ViewState["keywordSearch"].ToString(), ViewState["txtSearchFrom"].ToString(), ViewState["txtSearchTo"].ToString());
            }
            break;
      }
   }

   protected string getStatus(int status)
   {
      if (status == 1)
      {
         return "<span class='status-online' data-toggle='tooltip' title='ONLINE'><i class='fa fa-check-circle fa-lg'></i></span>";
      }
      else
      {
         return "<span class='status-offline' data-toggle='tooltip' title='OFFLINE'><i class='fa fa-times-circle fa-lg'></i></span>";
      }
   }
   #endregion Custom Functions
}
