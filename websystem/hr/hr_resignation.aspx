﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_resignation.aspx.cs" Inherits="websystem_hr_hr_resignation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">



    <script>
        $(document).ready(function () {
            window.Sys.WebForms.PageRequestManager.getInstance().add_endRequest(pageLoad);

            function pageLoad() {
                $("#UploadImages").MultiFile();
            }
        })
    </script>

    <style type="text/css">
        .bg-template-resign {
            background-image: url('../../masterpage/images/hr/Cover_Head02.png');
            background-size: 100% 100%;
            width: 100%;
            height: 100%;
            text-align: center;
        }
    </style>
    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand">Menu</a>
            </div>
            <div class="collapse navbar-collapse p-l-10" style="overflow-x: hidden; background-color: blanchedalmond" id="bs-example-navbar-collapse-1">
                <%--lemonchiffon--%>
                <ul class="nav navbar-nav navbar-left" runat="server">
                    <li id="_divMenuLiToViewIndex" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivIndex" runat="server"
                            CommandName="_divMenuBtnToDivIndex"
                            OnCommand="btnCommand" Text="รายการทั่วไป" />
                    </li>


                    <li id="_divMenuLiToViewAdd" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivAdd" runat="server"
                            CommandName="_divMenuBtnToDivAdd"
                            OnCommand="btnCommand" Text="ทำรายการ" />
                    </li>

                    <li id="_divMenuLiToViewApprove" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivApprove" runat="server"
                            CommandName="_divMenuBtnToDivApprove"
                            OnCommand="btnCommand" Text="รายการรออนุมัติ">
                            <asp:Label ID="nav_approve" Font-Bold="true" runat="server"></asp:Label>
                        </asp:LinkButton>

                    </li>

                </ul>

                <ul class="nav navbar-nav navbar-right" runat="server">

                    <li id="_divMenuLiToViewManual" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivManual" runat="server"
                            CommandName="_divMenuBtnToDivManual"
                            OnCommand="btnCommand" Text="คู่มือการใช้งาน" />
                    </li>
                    <li id="_divMenuLiToViewFlowChart" runat="server">
                        <asp:LinkButton ID="_divMenuBtnToDivFlowChart" runat="server"
                            CommandName="_divMenuBtnToDivFlowChart"
                            OnCommand="btnCommand" Text="Flow Chart" />
                    </li>

                </ul>
            </div>
        </nav>
    </div>



    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">

            <asp:UpdatePanel ID="update_viewindex" runat="server">
                <ContentTemplate>
                    <div class="col-lg-12 bg-template-resign">
                        <div class="col-lg-12">
                            <asp:Image ID="imghead" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/hr_resignation/Cover_Head_ระบบลาออก_02-05.png" Style="height: 100%; width: 100%;" />
                        </div>

                        <div class="col-lg-12">

                            <div class="alert-message alert-message-success">
                                <blockquote class="danger" style="font-size: small; background-color: gold; text-align: left">
                                    <h3><b>ประวัติการลาออก</b></h3>
                                </blockquote>

                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <asp:GridView ID="GvListEX" runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                        HeaderStyle-CssClass="info small"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="false"
                                        OnRowDataBound="Master_RowDataBound"
                                        DataKeyNames="u0_docidx">

                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>
                                        <Columns>

                                            <asp:TemplateField HeaderText="ระบบ" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                                    <asp:Label ID="lbm0_tidx" Visible="false" runat="server" Text='<%# Eval("m0_tidx") %>'></asp:Label>
                                                    <asp:Label ID="lblunidx_ex" Visible="false" runat="server" Text='<%# Eval("unidx_ex") %>'></asp:Label>
                                                    <asp:Label ID="lbltype_menu" Font-Bold="true" runat="server" Text='<%# Eval("type_menu") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="วันที่ทำรายการ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbname" runat="server" Text='<%# Eval("createdate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="วันที่ต้องการออกจากงาน" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbdate_resign" runat="server" Text='<%# Eval("date_resign") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:Literal ID="ltunidx" Visible="false" runat="server" Text='<%# Eval("unidx") %>' /></small>
                                                    <asp:Literal ID="ltstaidx" Visible="false" runat="server" Text='<%# Eval("staidx") %>' /></small>
                                                   
                                                 <strong>
                                                     <asp:Label ID="Label15" runat="server">สถานะรายการ: </asp:Label></strong>
                                                    <asp:Label ID="lblstasus" Font-Bold="true" runat="server" Text='<%# Eval("StatusDoc") %>'></asp:Label>
                                                    </p>
                                                    <div id="divstatus_ex" runat="server" visible="false">
                                                        <strong>
                                                            <asp:Label ID="Label24" runat="server">สถานะส่งมอบงาน: </asp:Label></strong>
                                                        <asp:Label ID="lblstasus_ex" Font-Bold="true" runat="server" Text='<%# Eval("StatusDoc_Ex") %>'></asp:Label>
                                                        </p>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnlinkquest" CssClass="btn btn-primary btn-sm" runat="server" OnCommand="btnCommand" CommandName="CmdDetail" CommandArgument='<%#  Eval("u0_docidx") + ";" + Eval("CEmpIDX")+ ";" + Eval("m0_tidx")+ ";" + "1"  %>'><i class="	fa fa-book"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnexchange" CssClass="btn btn-success btn-sm" runat="server" OnCommand="btnCommand" CommandName="CmdDetail" CommandArgument='<%#  Eval("u0_docidx") + ";" + Eval("CEmpIDX")+ ";" + Eval("m0_tidx") + ";"+ "2"%>'><i class="	fa fa-envelope"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="btnremove" CssClass="btn btn-danger btn-sm" runat="server" OnCommand="btnCommand" CommandName="CmdDetail" CommandArgument='<%#  Eval("u0_docidx") + ";" + Eval("CEmpIDX")+ ";" + Eval("m0_tidx") + ";"+ "3"%>'><i class="fa fa-trash"></i></asp:LinkButton>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>

                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>

        <asp:View ID="ViewInsert" runat="server">
            <asp:UpdatePanel ID="updatepic" runat="server" UpdateMode="Always">
                <ContentTemplate>
                    <div class="col-lg-12">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                            </div>
                            <asp:FormView ID="FvDetailUser" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                            </div>


                                            <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                            </div>


                                            <%-------------- แผนก,ตำแหน่ง --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                            </div>

                                            <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                            <div class="form-group">

                                                <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                                <div class="col-sm-3">
                                                    <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                                    <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                                <div class="col-sm-3">
                                                    <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                                    <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </InsertItemTemplate>
                            </asp:FormView>


                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="	fa fa-bookmark"></i><strong>&nbsp; เลือกระบบที่ต้องการ</strong></h3>
                            </div>
                            <div class="form-horizontal" role="form">
                                <div class="panel-body">

                                    <div class="col-lg-12">
                                        <div class="col-sm-4 col-sm-offset-2">

                                            <asp:ImageButton ID="imgcreate" CssClass="img-responsive" runat="server" ToolTip="ลาออก" Width="70%" CommandName="btnsystem" CommandArgument="1" OnCommand="btnCommand" ImageUrl="~/images/hr_resignation/Icon_ปุ่มระบบลาออก-01.png" />
                                        </div>

                                        <div class="col-sm-4">

                                            <asp:ImageButton ID="imgedit" CssClass="img-responsive" ToolTip="เปลี่ยนแปลงรายการ" runat="server" Width="70%" CommandName="btnsystem" CommandArgument="2" OnCommand="btnCommand" ImageUrl="~/images/hr_resignation/Icon_ปุ่มระบบลาออก-09.png" />
                                        </div>
                                        <%-- <div class="col-sm-4">
                                            <asp:ImageButton ID="imgchange" CssClass="img-responsive" ToolTip="ยกเลิกรายการ" runat="server" Width="50%" CommandName="btnsystem" CommandArgument="3" OnCommand="btnCommand" ImageUrl="~/images/hr_resignation/Icon_ปุ่มระบบลาออก-05.png" />
                                        </div>--%>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div id="div_create_quit" runat="server" visible="false">
                        <div class="col-lg-12">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-laptop"></i><strong>&nbsp; ข้อมูลส่งมอบทรัพย์สิน</strong></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="col-lg-12">
                                            <div class="alert-message alert-message-success">
                                                <blockquote class="danger" style="font-size: small; background-color: palevioletred; text-align: left">
                                                    <h4 style="color: whitesmoke"><b>ส่งมอบทรัพย์สินอุปกรณ์สำนักงาน</b></h4>
                                                </blockquote>

                                            </div>

                                            <asp:GridView ID="GvAsset" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table  table-bordered table-hover table-responsive col-lg-3"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="true"
                                                DataKeyNames="u0_asidx">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">

                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chklist" Enabled="false" Checked="true" runat="server" />
                                                            <asp:Label ID="lbu0_asidx" Visible="false" runat="server" Text='<%# Eval("u0_asidx") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ประเภทอุปกรณ์" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("as_name") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="เลข ASSET" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbassname" runat="server" Text='<%# Eval("u0_asset_no") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbu0_number" runat="server" Text='<%# Eval("u0_number") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbcode" runat="server" Text='<%# Eval("u0_detail") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <hr />
                                        <div class="col-lg-12">
                                            <div class="alert-message alert-message-success">
                                                <blockquote class="danger" style="font-size: small; background-color: palevioletred; text-align: left">
                                                    <h4 style="color: whitesmoke"><b>ส่งมอบทรัพย์สินอุปกรณ์คอมพิวเตอร์</b></h4>
                                                </blockquote>

                                            </div>

                                            <asp:GridView ID="GvHolder" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table  table-bordered table-hover table-responsive col-lg-3"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="true"
                                                DataKeyNames="u0_didx"
                                                OnRowDataBound="Master_RowDataBound">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">

                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chklist" Enabled="false" Checked="true" runat="server" />
                                                            <asp:Label ID="lbu0_didx" Visible="false" runat="server" Text='<%# Eval("u0_didx") %>'></asp:Label>
                                                        </ItemTemplate>

                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="ประเภทอุปกรณ์" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="เลข ASSET" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbassname" runat="server" Text='<%# Eval("u0_acc") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="อุปกรณ์ถือครอง" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbu0code" runat="server" Text='<%# Eval("u0_code") %>'></asp:Label>

                                                            <asp:Label ID="btnsoftware" runat="server" CssClass="btn btn-xs"><i class="glyphicon glyphicon-folder-open"></i></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="วันที่ถือครอง" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbcode" runat="server" Text='<%# Eval("HolderDate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-frown-o"></i><strong>&nbsp; กรอกข้อมูลลาออก</strong></h3>
                                </div>
                                <asp:FormView ID="fv_insert" runat="server" DefaultMode="Insert" Width="100%" OnDataBound="FvDetail_DataBound">
                                    <InsertItemTemplate>

                                        <div class="panel-body">
                                            <div class="form-horizontal" role="form">

                                                <div class="alert-message alert-message-success">
                                                    <blockquote class="danger" style="font-size: small; background-color: darkseagreen; text-align: left">
                                                        <h4 style="color: whitesmoke"><b>ระบุข้อมูลลาออก</b></h4>
                                                    </blockquote>

                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label3" runat="server" Text="เริ่มงานวันที่" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtstartdate" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <asp:TextBox ID="txtin" Visible="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>

                                                    <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="อายุงาน : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtyear" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label1" runat="server" Text="วันที่สิ้นสุดการทำงาน" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Always">
                                                            <ContentTemplate>
                                                                <div class='input-group date'>
                                                                    <asp:TextBox ID="dateexit" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                                    <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="SaveAdd" runat="server" Display="None"
                                                                        ControlToValidate="dateexit" Font-Size="11"
                                                                        ErrorMessage="กรุณาเลือกวันที่ต้องการ"
                                                                        ValidationExpression="กรุณาเลือกวันที่ต้องการ" />
                                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />
                                                                </div>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>

                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <asp:Label ID="Label10" runat="server" Text="สาเหตุการลาออก" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-6">
                                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Always">
                                                            <ContentTemplate>
                                                                <asp:RadioButtonList ID="rdochoice" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Horizontal">
                                                                </asp:RadioButtonList>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                                                    ValidationGroup="SaveAdd" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                                    Display="None" SetFocusOnError="true" ControlToValidate="rdochoice" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160"
                                                                    PopupPosition="BottomLeft" />
                                                            </ContentTemplate>
                                                            <Triggers>
                                                                <asp:AsyncPostBackTrigger ControlID="rdochoice" EventName="SelectedIndexChanged" />
                                                            </Triggers>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>

                                                <div class="form-group" id="divrdoreason" runat="server" visible="false">
                                                    <asp:Label ID="Label11" CssClass="col-sm-2 control-label" runat="server" Text="ระบุเหตุผลในการลาออก : " />
                                                    <div class="col-sm-3">
                                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                                            <ContentTemplate>
                                                                <asp:TextBox ID="txtrdo" TextMode="multiline" Rows="2" CssClass="form-control" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtrdo" Font-Size="11"
                                                                    ErrorMessage="กรุณาระบุเหตุผลในการลาออก"
                                                                    ValidationExpression="กรุณาระบุเหตุผลในการลาออก"
                                                                    SetFocusOnError="true" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidatxor1" runat="server"
                                                                    ValidationGroup="SaveAdd" Display="None"
                                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                    ControlToValidate="txtrdo"
                                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                                    SetFocusOnError="true" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidatxor1" Width="160" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>

                                                </div>
                                                <div class="form-group">
                                                    <asp:Label ID="Label2" runat="server" Text="รายละเอียด" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-9">
                                                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                                            <ContentTemplate>
                                                                <asp:TextBox ID="txtremark" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidsator8" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtremark" Font-Size="11"
                                                                    ErrorMessage="กรุณากรอกความคิดเห็น"
                                                                    ValidationExpression="กรุณากรอกความคิดเห็น"
                                                                    SetFocusOnError="true" />
                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallodutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidsator8" Width="160" />
                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                                    ValidationGroup="SaveAdd" Display="None"
                                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                    ControlToValidate="txtremark"
                                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                                    SetFocusOnError="true" />

                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutEqxtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </div>
                                                </div>

                                                <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <div class="form-group">

                                                            <asp:Label ID="Labelf12" class="col-sm-2 control-label" runat="server" Text="Upload File : " />
                                                            <div class="col-sm-7">
                                                                <asp:FileUpload ID="UploadImages" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="btn btn-primary btn-sm multi" accept="gif|jpg|pdf|png|pdf" />
                                                                <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล gif|jpg|pdf|png</font></p>

                                                            </div>
                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnAdddata" />
                                                    </Triggers>
                                                </asp:UpdatePanel>

                                                <div class="form-group" style="font-weight: bold; color: red; text-decoration-line: overline">
                                                    <div class="form-group">
                                                        <asp:Label ID="Label17" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                                                        <div class="col-sm-9">
                                                            <asp:Label ID="Label13" class="control-label" runat="server" Text="พร้อมกันนี้ข้าพเจ้าได้ทราบระเบียบเกี่ยวกับการลาออกจากงานของบริษัทฯ เป็นที่เข้าใจแล้ว คือ " />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:Label ID="Label18" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                                                        <div class="col-sm-9">
                                                            <asp:Label ID="Label14" class="control-label" runat="server" Text="1. ข้าพเจ้ายินดีชดใช้ค่าเสียหาย หรือ หนี้สินที่ยังค้างชำระต่อบริษัทฯ ทั้งหมด" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:Label ID="Label19" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                                                        <div class="col-sm-9">
                                                            <asp:Label ID="Label15" class="control-label" runat="server" Text="2. ข้าพเจ้ายินดีปฏิบัติตามกฎระเบียบและเงื่อนไขแห่งการลาออกของบริษีทฯ ทุกประการ" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <asp:Label ID="Label20" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                                                        <div class="col-sm-9">
                                                            <asp:Label ID="Label16" class="control-label" runat="server" Text="3. ข้าพเจ้ายินดีลาออกด้วยตนเอง และจำไม่เรียกร้องสิทธิใดๆ ทั้งสิน" />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label21" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                                                        <div class="col-sm-9">
                                                            <asp:Label ID="Label22" class="control-label" runat="server" Text="พร้อมกันนี้ข้าพเจ้าได้ส่งคืนทรัพย์สินที่อยู่ในความดูแลและครอบครองของข้าพเจ้าคืนแก่บริษัทฯ ด้วย" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="color: black; font-weight: normal">
                                                        <asp:Label ID="Label23" runat="server" Text="" CssClass="col-sm-2 control-label"></asp:Label>
                                                        <div class="col-sm-9">
                                                            <asp:CheckBox ID="chkagree" runat="server" Text="ยืนยันการรับทราบ" />

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-2 col-sm-offset-10">
                                                        <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                    </InsertItemTemplate>
                                </asp:FormView>
                            </div>
                        </div>

                    </div>

                    <div id="div_edit_quit" runat="server" visible="false">
                        <div class="col-lg-12">
                            <div class="panel panel-warning">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-laptop"></i><strong>&nbsp; ข้อมูลส่งมอบทรัพย์สิน</strong></h3>
                                </div>
                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <div class="col-lg-12">
                                            <div class="alert-message alert-message-success">
                                                <blockquote class="danger" style="font-size: small; background-color: palevioletred; text-align: left">
                                                    <h4 style="color: whitesmoke"><b>ส่งมอบทรัพย์สินอุปกรณ์สำนักงาน</b></h4>
                                                </blockquote>

                                            </div>

                                            <asp:GridView ID="GvAsset_edit" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table  table-bordered table-hover table-responsive col-lg-3"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="true"
                                                DataKeyNames="u0_asidx">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>



                                                    <asp:TemplateField HeaderText="ประเภทอุปกรณ์" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("as_name") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="เลข ASSET" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbassname" runat="server" Text='<%# Eval("u0_asset_no") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbu0_number" runat="server" Text='<%# Eval("u0_number") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbcode" runat="server" Text='<%# Eval("u0_detail") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <hr />
                                        <div class="col-lg-12">
                                            <div class="alert-message alert-message-success">
                                                <blockquote class="danger" style="font-size: small; background-color: palevioletred; text-align: left">
                                                    <h4 style="color: whitesmoke"><b>ส่งมอบทรัพย์สินอุปกรณ์คอมพิวเตอร์</b></h4>
                                                </blockquote>

                                            </div>

                                            <asp:GridView ID="GvHolder_edit" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table  table-bordered table-hover table-responsive col-lg-3"
                                                HeaderStyle-CssClass="info"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="true"
                                                DataKeyNames="u0_didx"
                                                OnRowDataBound="Master_RowDataBound">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>

                                                    <%--    <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">

                                                <ItemTemplate>
                                                    <%--<asp:CheckBox ID="chklist" Enabled="false" Checked="true" runat="server" />

                                                </ItemTemplate>

                                            </asp:TemplateField>--%>

                                                    <asp:TemplateField HeaderText="ประเภทอุปกรณ์" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbu0_didx" Visible="false" runat="server" Text='<%# Eval("u0_asidx") %>'></asp:Label>
                                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="เลข ASSET" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbassname" runat="server" Text='<%# Eval("u0_acc") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="อุปกรณ์ถือครอง" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbu0code" runat="server" Text='<%# Eval("u0_code") %>'></asp:Label>

                                                            <asp:Label ID="btnsoftware" runat="server" CssClass="btn btn-xs"><i class="glyphicon glyphicon-folder-open"></i></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="วันที่ถือครอง" ItemStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbcode" runat="server" Text='<%# Eval("HolderDate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-frown-o"></i><strong>&nbsp; กรอกข้อมูลลาออก</strong></h3>
                                </div>
                                <asp:FormView ID="fv_edit" runat="server" DefaultMode="Insert" Width="100%" OnDataBound="FvDetail_DataBound">
                                    <EditItemTemplate>
                                        <asp:HiddenField ID="hfu0_docidx" runat="server" Value='<%# Eval("u0_docidx") %>' />

                                        <div class="panel-body">
                                            <div class="form-horizontal" role="form">

                                                <div class="alert-message alert-message-success">
                                                    <blockquote class="danger" style="font-size: small; background-color: darkseagreen; text-align: left">
                                                        <h4 style="color: whitesmoke"><b>ระบุข้อมูลลาออก</b></h4>
                                                    </blockquote>

                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label3" runat="server" Text="เริ่มงานวันที่" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtstartdate_edit" Text='<%# Eval("EmpIN_Ex") %>' Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <asp:TextBox ID="txtin_edit" Visible="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </div>

                                                    <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="อายุงาน : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtyear_edit" Text='<%# Eval("dateexp") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <asp:Label ID="Label1" runat="server" Text="วันที่สิ้นสุดการทำงาน" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <div class='input-group date'>
                                                            <asp:TextBox ID="dateexit_edit" runat="server" Text='<%# Eval("date_resign") %>' CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-group">
                                                    <asp:Label ID="Label10" runat="server" Text="สาเหตุการลาออก" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-6">
                                                        <asp:TextBox ID="lblm0_rsidx_edit" Visible="false" Text='<%# Eval("m0_rsidx") %>' CssClass="form-control" runat="server"></asp:TextBox>
                                                        <asp:RadioButtonList ID="rdochoice_edit" runat="server" Enabled="false" CssClass="radio-list-inline-emps" RepeatDirection="Horizontal">
                                                        </asp:RadioButtonList>

                                                    </div>
                                                </div>

                                                <div class="form-group" id="divrdoreason_edit" runat="server" visible="false">
                                                    <asp:Label ID="Label11" CssClass="col-sm-2 control-label" runat="server" Text="ระบุเหตุผลในการลาออก : " />
                                                    <div class="col-sm-3">
                                                        <asp:TextBox ID="txtrdo_edit" Text='<%# Eval("comment_m0rsidx") %>' Enabled="false" TextMode="multiline" Rows="2" CssClass="form-control" runat="server"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="SaveAdd_Edit" runat="server" Display="None" ControlToValidate="txtrdo_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาระบุเหตุผลในการลาออก"
                                                            ValidationExpression="กรุณาระบุเหตุผลในการลาออก"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressioanValidator1" runat="server"
                                                            ValidationGroup="SaveAdd_Edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtrdo_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressioanValidator1" Width="160" />
                                                    </div>

                                                </div>
                                                <div class="form-group">
                                                    <asp:Label ID="Label2" runat="server" Text="รายละเอียด" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-9">
                                                        <asp:TextBox ID="txtremark_edit" Text='<%# Eval("reason_resign") %>' TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                                        <asp:RequiredFieldValidator ID="RequiredFieldVaslidator8" ValidationGroup="SaveAdd_Edit" runat="server" Display="None" ControlToValidate="txtremark_edit" Font-Size="11"
                                                            ErrorMessage="กรุณากรอกความคิดเห็น"
                                                            ValidationExpression="กรุณากรอกความคิดเห็น"
                                                            SetFocusOnError="true" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloustExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldVaslidator8" Width="160" />
                                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                            ValidationGroup="SaveAdd_Edit" Display="None"
                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                            ControlToValidate="txtremark_edit"
                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                            SetFocusOnError="true" />

                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExgtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />
                                                    </div>
                                                </div>




                                                <div class="form-group">
                                                    <div class="col-sm-2 col-sm-offset-10">
                                                        <asp:LinkButton ID="btnAdddata" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdInsert_Edit" OnCommand="btnCommand" ValidationGroup="SaveAdd_Edit" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                                    </div>
                                                </div>
                                    </EditItemTemplate>
                                </asp:FormView>
                            </div>
                        </div>
                    </div>

                    <div id="div_cancel_quit" runat="server" visible="false">
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>

        <asp:View ID="ViewExitinterview" runat="server">

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="col-lg-12 bg-template-resign">
                        <div class="col-lg-12">
                            <asp:Image ID="Image1" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/hr_resignation/Cover_Head_ระบบลาออก_02-05.png" Style="height: 100%; width: 100%;" />
                        </div>

                        <div class="col-lg-12">

                            <div class="alert-message alert-message-success">
                                <blockquote class="danger" style="font-size: small; background-color: gold; text-align: left">
                                    <h3><b>แบบฟอร์ม Exit Interview</b></h3>
                                </blockquote>

                            </div>
                        </div>

                        <div class="col-lg-12">
                            <asp:UpdatePanel ID="UpdatePanel9" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">
                                            <asp:GridView ID="GvTypeAns" runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                HeaderStyle-Height="40px"
                                                AllowPaging="false"
                                                DataKeyNames="m0_taidx"
                                                OnRowDataBound="Master_RowDataBound">

                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                <EmptyDataTemplate>
                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                </EmptyDataTemplate>
                                                <Columns>


                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="" ItemStyle-CssClass="text-left">
                                                        <ItemTemplate>

                                                            <div class="alert-message alert-message-success">
                                                                <blockquote class="danger" style="background-color: #cce6ff;">
                                                                    <h4><b>
                                                                        <i class="fa fa-edit"></i>&nbsp;
                                            <asp:Label ID="Label1" runat="server" Font-Bold="true" Text='<%# Eval("type_ans") %>'></asp:Label>
                                                                    </b></h4>
                                                                </blockquote>
                                                            </div>

                                                            <asp:Label ID="lblm0_taidx" runat="server" CssClass="col-sm-10" Visible="false" Text='<%# Eval("m0_taidx") %>'></asp:Label>

                                                            <asp:GridView ID="GvQuestion" runat="server"
                                                                AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                                HeaderStyle-CssClass="danger small"
                                                                HeaderStyle-Height="40px"
                                                                AllowPaging="false"
                                                                DataKeyNames="m0_quidx"
                                                                OnRowDataBound="Master_RowDataBound">


                                                                <PagerStyle CssClass="pageCustom" />
                                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                <EmptyDataTemplate>
                                                                    <div style="text-align: center">Data Cannot Be Found</div>
                                                                </EmptyDataTemplate>
                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="ข้อที่" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" Visible="true" ItemStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblm0_toidx" Visible="false" runat="server" Text='<%# Eval("m0_toidx") %>' />
                                                                            <asp:Label ID="lblm0_tqidx" Visible="false" runat="server" Text='<%# Eval("m0_tqidx") %>' />
                                                                            <asp:Label ID="lblm0_taidx" Visible="false" runat="server" Text='<%# Eval("m0_taidx") %>' />
                                                                            <asp:Label ID="lblno_choice" runat="server" Text='<%# Eval("no_choice") %>' />
                                                                        </ItemTemplate>

                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="คำถาม" ItemStyle-CssClass="text-left" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblm0_quidx" Visible="false" runat="server" Text='<%# Eval("m0_quidx") %>' />
                                                                            <asp:Label ID="lblchoice" runat="server" Text='<%# Eval("quest_name") %>'></asp:Label>

                                                                            <asp:Panel runat="server" ID="panel_choice_rdo" Visible="false">
                                                                                <br />
                                                                                <asp:RadioButtonList ID="rdochoice_exit" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                                                    <%-- <asp:ListItem Value="Single" Text="SingleDate">Single Date: <input id="txtSingleDate"  class="form-control" runat="server"  type="text" /></asp:ListItem>--%>
                                                                                </asp:RadioButtonList>
                                                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6"
                                                                                    ValidationGroup="AddExit" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                                                    Display="None" SetFocusOnError="true" ControlToValidate="rdochoice_exit" />
                                                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenxder7" runat="Server"
                                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160"
                                                                                    PopupPosition="BottomLeft" />
                                                                                &nbsp;

                                                                            </asp:Panel>

                                                                            <asp:Panel runat="server" ID="panel_choice_chk" Visible="false">
                                                                                <br />
                                                                                <asp:CheckBoxList ID="chkchoice_exit" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                                                </asp:CheckBoxList>
                                                                                <%--   <asp:RequiredFieldValidator ID="RequiredFieffldValidator1"
                                                                                    ValidationGroup="AddExit" runat="server" Font-Size="13px" ForeColor="Red" ErrorMessage="กรุณาเลือกคำตอบให้ครบถ้วน"
                                                                                    Display="None" SetFocusOnError="true" ControlToValidate="chkchoice_exit" />
                                                                              <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieffldValidator1" Width="160"
                                                                                    PopupPosition="BottomLeft" />--%>
                                                                                &nbsp;
                                                                            </asp:Panel>

                                                                            <asp:Panel runat="server" ID="panel_comment" Visible="false">
                                                                                <div class="form-group">
                                                                                    <div class="col-sm-10">
                                                                                        <asp:TextBox ID="txtquest" ValidationGroup="saveanswer" row="20" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="AddExit" runat="server" Display="None" ControlToValidate="txtquest" Font-Size="11"
                                                                                            ErrorMessage="กรุณากรอกคำตอบ"
                                                                                            ValidationExpression="กรุณากรอกคำตอบ"
                                                                                            SetFocusOnError="true" />
                                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                                                                        <asp:RegularExpressionValidator ID="RegularExprezssionValidator1" runat="server"
                                                                                            ValidationGroup="AddExit" Display="None"
                                                                                            ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                                                            ControlToValidate="txtquest"
                                                                                            ValidationExpression="^[\s\S]{0,1000}$"
                                                                                            SetFocusOnError="true" />

                                                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExprezssionValidator1" Width="160" />
                                                                                    </div>
                                                                            </asp:Panel>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                </Columns>
                                            </asp:GridView>


                                            <div class="form-group">
                                                <div class="col-sm-1 col-sm-offset-11">
                                                    <asp:LinkButton ID="LinkButton1" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="AddExit" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" CommandName="CmdSaveList" data-toggle="tooltip" title="Save"><i class="fa fa-check-square"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="LinkButton2" CssClass="btn btn-primary btn-sm" runat="server" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Back"><i class="fa fa-reply"></i></asp:LinkButton>
                                                </div>
                                            </div>

                                            <%--         <div class="form-group">
                                                <asp:Label ID="Label67" CssClass="col-sm-5 control-label" runat="server" Text="ตั้งค่าความเป็นส่วนตัว :" />

                                                <div class="col-sm-3">
                                                    <asp:DropDownList ID="ddlpermission" runat="server" CssClass="form-control" ValidationGroup="AddExit">
                                                        <asp:ListItem Value="0" Text="กรุณาเลือกความเป็นส่วนตัว"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="อนุญาตให้เผยแพร่ข้อมูล"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="ไม่อนุญาตให้เผยแพร่ข้อมูล"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="AddExit" runat="server" Display="None"
                                                        ControlToValidate="ddlpermission" Font-Size="11"
                                                        ErrorMessage="กรุณาเลือกความเป็นส่วนตัว"
                                                        ValidationExpression="กรุณาเลือกความเป็นส่วนตัว" InitialValue="0" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />

                                                </div>

                                               
                                            </div>--%>
                                        </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:View>

        <asp:View ID="ViewDetail" runat="server">
            <div class="col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัวผู้ทำรายการ</strong></h3>
                    </div>
                    <asp:FormView ID="Fvdetailusercreate" runat="server" Width="100%" OnDataBound="FvDetail_DataBound">
                        <InsertItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label7" CssClass="col-sm-2 control-label" runat="server" Text="รหัสผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtempcode" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อผู้ทำรายการ : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesname" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- ชื่อผู้ทำรายการ,ฝ่าย --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label4" CssClass="col-sm-2 control-label" runat="server" Text="บริษัท : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtorg" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label34" CssClass="col-sm-3 control-label" runat="server" Text="ฝ่าย : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtrequesdept" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>


                                    <%-------------- แผนก,ตำแหน่ง --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Labedl1" class="col-sm-2 control-label" runat="server" Text="แผนก : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label8" CssClass="col-sm-3 control-label" runat="server" Text="ตำแหน่ง : " />
                                        <div class="col-sm-3">
                                            <asp:TextBox ID="txtpos" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                    <%-------------- เบอร์ติดต่อ,E-mail --------------%>
                                    <div class="form-group">

                                        <asp:Label ID="Label5" class="col-sm-2 control-label" runat="server" Text="เบอร์ติดต่อ : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtsec" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txttel" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="E-mail : " />
                                        <div class="col-sm-3">
                                            <%--<asp:TextBox ID="txtdep" Enabled="false" CssClass="form-control" runat="server" Text='<%# Eval("PosNameTH") %>'></asp:TextBox>--%>
                                            <asp:TextBox ID="txtemail" Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </InsertItemTemplate>
                    </asp:FormView>
                </div>
            </div>


            <div id="div_detail_create" runat="server" visible="false">
                <div class="col-lg-12">
                    <div class="container">
                        <ul class="nav nav-tabs">
                            <li id="_divMenuLiToViewDetail_resign" runat="server" class="active">

                                <asp:LinkButton ID="btndata_resign" CssClass="btn btn-default  btn-sm" runat="server" CommandName="cmddata_resign" OnCommand="btnCommand" title="ข้อมูลหลัก"><h4> ข้อมูลหลัก</h4></asp:LinkButton>
                            </li>
                            <li id="_divMenuLiToViewDetail_exitinterivew" runat="server">
                                <asp:LinkButton ID="btndata_exitinterivew" CssClass="btn btn-default  btn-sm" runat="server" CommandName="cmddata_exitinterview" OnCommand="btnCommand" title="Exit Interview"><h4>Exit Interview</h4></asp:LinkButton>
                            </li>
                        </ul>
                    </div>

                </div>

                <div id="div_detailasset" runat="server">
                    <div class="col-lg-12" id="div_selectasset" runat="server">
                        <div class="panel panel-warning">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-laptop"></i><strong>&nbsp; ข้อมูลส่งมอบทรัพย์สิน</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="col-lg-12">
                                        <div class="alert-message alert-message-success">
                                            <blockquote class="danger" style="font-size: small; background-color: palevioletred; text-align: left">
                                                <h4 style="color: whitesmoke"><b>ส่งมอบทรัพย์สินอุปกรณ์สำนักงาน</b></h4>
                                            </blockquote>

                                        </div>

                                        <asp:GridView ID="GvAsset_detail" runat="server"
                                            AutoGenerateColumns="false"
                                            CssClass="table  table-bordered table-hover table-responsive col-lg-3"
                                            HeaderStyle-CssClass="info"
                                            HeaderStyle-Height="40px"
                                            AllowPaging="true"
                                            DataKeyNames="u0_asidx">

                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>

                                                <%--  <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">

                                                <ItemTemplate>
                                                    <%--<asp:CheckBox ID="chklist" Enabled="false" Checked="true" runat="server" />
                                                    <asp:Label ID="lbu0_asidx" Visible="false" runat="server" Text='<%# Eval("u0_asidx") %>'></asp:Label>
                                                </ItemTemplate>

                                            </asp:TemplateField>--%>

                                                <asp:TemplateField HeaderText="ประเภทอุปกรณ์" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbname" runat="server" Text='<%# Eval("as_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="เลข ASSET" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbassname" runat="server" Text='<%# Eval("u0_asset_no") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="จำนวน" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbu0_number" runat="server" Text='<%# Eval("u0_number") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbcode" runat="server" Text='<%# Eval("u0_detail") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                    <hr />
                                    <div class="col-lg-12">
                                        <div class="alert-message alert-message-success">
                                            <blockquote class="danger" style="font-size: small; background-color: palevioletred; text-align: left">
                                                <h4 style="color: whitesmoke"><b>ส่งมอบทรัพย์สินอุปกรณ์คอมพิวเตอร์</b></h4>
                                            </blockquote>

                                        </div>

                                        <asp:GridView ID="GvHolder_detail" runat="server"
                                            AutoGenerateColumns="false"
                                            CssClass="table  table-bordered table-hover table-responsive col-lg-3"
                                            HeaderStyle-CssClass="info"
                                            HeaderStyle-Height="40px"
                                            AllowPaging="true"
                                            DataKeyNames="u0_didx"
                                            OnRowDataBound="Master_RowDataBound">

                                            <PagerStyle CssClass="pageCustom" />
                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                            <EmptyDataTemplate>
                                                <div style="text-align: center">Data Cannot Be Found</div>
                                            </EmptyDataTemplate>
                                            <Columns>



                                                <asp:TemplateField HeaderText="ประเภทอุปกรณ์" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbu0_didx" Visible="false" runat="server" Text='<%# Eval("u0_asidx") %>'></asp:Label>
                                                        <asp:Label ID="lbname" runat="server" Text='<%# Eval("name_m0_typedevice") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="เลข ASSET" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbassname" runat="server" Text='<%# Eval("u0_acc") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="อุปกรณ์ถือครอง" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbu0code" runat="server" Text='<%# Eval("u0_code") %>'></asp:Label>

                                                        <asp:Label ID="btnsoftware" runat="server" CssClass="btn btn-xs"><i class="glyphicon glyphicon-folder-open"></i></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="วันที่ถือครอง" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbcode" runat="server" Text='<%# Eval("HolderDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-frown-o"></i><strong>&nbsp; กรอกข้อมูลลาออก</strong></h3>
                            </div>
                            <asp:FormView ID="fvinsert_detail" runat="server" DefaultMode="Insert" Width="100%" OnDataBound="FvDetail_DataBound">
                                <EditItemTemplate>
                                    <asp:HiddenField ID="hfM0NodeIDX" runat="server" Value='<%# Eval("unidx") %>' />
                                    <asp:HiddenField ID="hfM0ActoreIDX" runat="server" Value='<%# Eval("acidx") %>' />
                                    <asp:HiddenField ID="hfM0StatusIDX" runat="server" Value='<%# Eval("staidx") %>' />
                                    <asp:HiddenField ID="hfM0CEmpIDX" runat="server" Value='<%# Eval("CEmpIDX") %>' />
                                    <asp:HiddenField ID="hftype_jobgrade_flow" runat="server" Value='<%# Eval("type_jobgrade_flow") %>' />
                                    <asp:HiddenField ID="hfm0_tidx" runat="server" Value='<%# Eval("m0_tidx") %>' />
                                    <asp:HiddenField ID="hfdocidx_ref" runat="server" Value='<%# Eval("docidx_ref") %>' />
                                    <asp:HiddenField ID="hfm0_toidx" runat="server" Value='<%# Eval("m0_toidx") %>' />

                                    <asp:HiddenField ID="hfM0NodeIDX_EX" runat="server" Value='<%# Eval("unidx_ex") %>' />
                                    <asp:HiddenField ID="hfM0ActoreIDX_EX" runat="server" Value='<%# Eval("acidx_ex") %>' />
                                    <asp:HiddenField ID="hfM0StatusIDX_EX" runat="server" Value='<%# Eval("staidx_ex") %>' />
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="alert-message alert-message-success">
                                                <blockquote class="danger" style="font-size: small; background-color: darkseagreen; text-align: left">
                                                    <h4 style="color: whitesmoke"><b>ระบุข้อมูลลาออก</b></h4>
                                                </blockquote>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label31" runat="server" Text="ระบบ" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txttypemenu" Text='<%# Eval("type_menu") %>' Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>

                                                <%--  <asp:Label ID="Label32" CssClass="col-sm-3 control-label" runat="server" Text="เลขเอกสารอ้างอิง : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="TextBox3" Text='<%# Eval("dateexp") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>--%>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="เริ่มงานวันที่" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtstartdate_detail" Text='<%# Eval("EmpIN_Ex") %>' Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                    <asp:TextBox ID="txtin_detail" Visible="false" runat="server" CssClass="form-control"></asp:TextBox>
                                                </div>

                                                <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="อายุงาน : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtyear_detail" Text='<%# Eval("dateexp") %>' Enabled="false" CssClass="form-control" runat="server"></asp:TextBox>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="วันที่สิ้นสุดการทำงาน" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="dateexit_detail" Enabled="false" runat="server" Text='<%# Eval("date_resign") %>' CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="Label10" runat="server" Text="สาเหตุการลาออก" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:TextBox ID="lblm0_rsidx" Visible="false" Text='<%# Eval("m0_rsidx") %>' CssClass="form-control" runat="server"></asp:TextBox>
                                                    <asp:RadioButtonList ID="rdochoice_detail" runat="server" Enabled="false" CssClass="radio-list-inline-emps" RepeatDirection="Horizontal">
                                                    </asp:RadioButtonList>

                                                </div>
                                            </div>

                                            <div class="form-group" id="divrdoreason_detail" runat="server" visible="false">
                                                <asp:Label ID="Label11" CssClass="col-sm-2 control-label" runat="server" Text="ระบุเหตุผลในการลาออก : " />
                                                <div class="col-sm-3">
                                                    <asp:TextBox ID="txtrdo" Text='<%# Eval("comment_m0rsidx") %>' Enabled="false" TextMode="multiline" Rows="2" CssClass="form-control" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtrdo" Font-Size="11"
                                                        ErrorMessage="กรุณาระบุเหตุผลในการลาออก"
                                                        ValidationExpression="กรุณาระบุเหตุผลในการลาออก"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionVsalidator1" runat="server"
                                                        ValidationGroup="SaveAdd" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtrdo"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionVsalidator1" Width="160" />
                                                </div>

                                            </div>
                                            <div class="form-group">
                                                <asp:Label ID="Label2" runat="server" Text="รายละเอียด" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-9">
                                                    <asp:TextBox ID="txtremark_detail" Enabled="false" Text='<%# Eval("reason_resign") %>' TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValiddator8" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtremark_detail" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกความคิดเห็น"
                                                        ValidationExpression="กรุณากรอกความคิดเห็น"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloautExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValiddator8" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                        ValidationGroup="SaveAdd" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtremark_detail"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtenvder12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />
                                                </div>
                                            </div>
                                            <%--                                           <div class="form-group">
                                                <asp:Label ID="Label12" runat="server" Text="ความเป็นส่วนตัว" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">

                                                    <asp:DropDownList ID="ddlpermission_detail" Enabled="false" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="1" Text="อนุญาตให้เผยแพร่ข้อมูล"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="ไม่อนุญาตให้เผยแพร่ข้อมูล"></asp:ListItem>
                                                    </asp:DropDownList>

                                                </div>
                                            </div>--%>


                                            <div class="form-group">
                                                <asp:Label ID="Label17" CssClass="col-sm-2 control-label" runat="server" />

                                                <div class="col-lg-9">
                                                    <asp:GridView ID="gvFile" Visible="true" runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover table-responsive"
                                                        HeaderStyle-CssClass="warning"
                                                        OnRowDataBound="Master_RowDataBound"
                                                        BorderStyle="None"
                                                        CellSpacing="2"
                                                        Font-Size="Small">

                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ไฟล์เอกสารเพิ่มเติม">
                                                                <ItemTemplate>
                                                                    <div class="col-lg-10">
                                                                        <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                                    </div>
                                                                    <div class="col-lg-2">
                                                                        <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                                        <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                                    </div>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                    <h4></h4>
                                                    </label>
                                                </div>
                                            </div>


                                            <div class="form-group" id="div_cancelquit" runat="server" visible="false">
                                                <asp:Label ID="Label30" runat="server" Text="ระบุสาเหตุที่ยกเลิกรายการ" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-9">
                                                    <asp:TextBox ID="txtremrk_cancel" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtremrk_cancel" Font-Size="11"
                                                        ErrorMessage="าะบุสาเหตุที่ยกเลิกรายการ"
                                                        ValidationExpression="าะบุสาเหตุที่ยกเลิกรายการ"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                                                        ValidationGroup="SaveAdd" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtremrk_cancel"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator3" Width="160" />
                                                </div>


                                            </div>

                                            <div class="form-group">
                                                <%--id="divbtn" runat="server"--%>
                                                <div class="col-sm-2 col-sm-offset-10">
                                                    <asp:LinkButton ID="btnAdddata" Visible="false" CssClass="btn btn-success btn-sm" runat="server" CommandName="CmdEdit" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="AddCancel" CssClass="btn btn-default  btn-sm" runat="server" ValidationGroup="Cancel" CommandName="BtnBack" OnCommand="btnCommand" title="Cancel" OnClientClick="return confirm('คุณต้องการยกเลิกรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                </EditItemTemplate>
                            </asp:FormView>


                        </div>
                    </div>

                    <div class="col-lg-12" id="divexchange" runat="server" visible="false">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-bookmark"></i><strong>&nbsp; ข้อมูลหน้าที่รับผิดชอบ</strong></h3>
                            </div>
                            <asp:FormView ID="fvexchange" runat="server" DefaultMode="Insert" Width="100%" OnDataBound="FvDetail_DataBound">
                                <InsertItemTemplate>
                                    <div class="panel-body">
                                        <div class="form-horizontal" role="form">

                                            <div class="alert-message alert-message-success">
                                                <blockquote class="danger" style="font-size: small; background-color: burlywood; text-align: left">
                                                    <h4 style="color: whitesmoke"><b>ส่งมอบงาน</b></h4>
                                                </blockquote>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="วันที่ส่งมอบงาน" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">
                                                    <div class='input-group date'>
                                                        <asp:TextBox ID="dateexchange" runat="server" CssClass="form-control from-date-datepicker" placeholder="กรุณาเลือกวันที่" MaxLengh="100%"></asp:TextBox>
                                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label27" runat="server" Text="งานหน่วยงาน" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-3">

                                                    <asp:RadioButtonList ID="rdotype" runat="server" OnSelectedIndexChanged="SelectedIndexChanged" AutoPostBack="true" CssClass="radio-list-inline-emps" RepeatDirection="Horizontal">
                                                        <asp:ListItem Value="1" Text="ทุกหน่วยงาน"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="เฉพาะหน่วยงาน"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </div>
                                            </div>

                                            <div id="div_exchange" runat="server" visible="false">
                                                <div class="form-group">
                                                    <asp:Label ID="Label12" runat="server" Text="องค์กร" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlorg_exchange" runat="server" AutoPostBack="true" OnSelectedIndexChanged="SelectedIndexChanged" CssClass="form-control">
                                                            <asp:ListItem Value="0">องค์กร....</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <asp:Label ID="Label28" runat="server" Text="ฝ่าย" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlrdept_exchange" runat="server" AutoPostBack="true" OnSelectedIndexChanged="SelectedIndexChanged" CssClass="form-control">
                                                            <asp:ListItem Value="0">ฝ่าย....</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>

                                                </div>
                                                <div class="form-group">
                                                    <asp:Label ID="Label29" runat="server" Text="แผนก" CssClass="col-sm-2 control-label"></asp:Label>
                                                    <div class="col-sm-3">
                                                        <asp:DropDownList ID="ddlrsec_exchange" runat="server" AutoPostBack="true" OnSelectedIndexChanged="SelectedIndexChanged" CssClass="form-control">
                                                            <asp:ListItem Value="0">แผนก....</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </div>


                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <asp:Label ID="Label2" runat="server" Text="รายละเอียด" CssClass="col-sm-2 control-label"></asp:Label>
                                                <div class="col-sm-9">
                                                    <asp:TextBox ID="txtremark_exchange" TextMode="multiline" Rows="5" CssClass="form-control" runat="server"></asp:TextBox>

                                                    <asp:RequiredFieldValidator ID="RequiredFieldVadlidator8" ValidationGroup="SaveAdd" runat="server" Display="None" ControlToValidate="txtremark_exchange" Font-Size="11"
                                                        ErrorMessage="กรุณากรอกความคิดเห็น"
                                                        ValidationExpression="กรุณากรอกความคิดเห็น"
                                                        SetFocusOnError="true" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatogrCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldVadlidator8" Width="160" />
                                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                                        ValidationGroup="SaveAdd" Display="None"
                                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                        ControlToValidate="txtremark_exchange"
                                                        ValidationExpression="^[\s\S]{0,1000}$"
                                                        SetFocusOnError="true" />

                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator2" Width="160" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <asp:LinkButton ID="btnAdddata_exchange" CssClass="btn btn-warning btn-sm" runat="server" CommandName="CmdAdd_Exchange" OnCommand="btnCommand" ValidationGroup="SaveAdd" OnClientClick="return confirm('คุณต้องการบันทึกรายการนี้ใช่หรือไม่ ?')" title="Save">ADD +</asp:LinkButton>
                                                </div>

                                            </div>



                                            <div class="col-sm-1">
                                            </div>
                                            <div class="col-sm-10">
                                                <asp:UpdatePanel ID="update1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:GridView ID="GvExchange"
                                                            runat="server"
                                                            CssClass="table table-striped table-responsive info"
                                                            GridLines="None"
                                                            AllowPaging="false"
                                                            OnRowDataBound="Master_RowDataBound"
                                                            Visible="false"
                                                            OnRowCommand="onRowCommand"
                                                            AutoGenerateColumns="false">


                                                            <PagerStyle CssClass="pageCustom" />
                                                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                            <EmptyDataTemplate>
                                                                <div style="text-align: center">Data Cannot Be Found</div>
                                                            </EmptyDataTemplate>

                                                            <Columns>


                                                                <asp:TemplateField HeaderText="วันที่ส่งมอบงาน" ItemStyle-CssClass="text-center" ItemStyle-Width="5%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:Label ID="lbldate_exchange" runat="server" Text='<%# Eval("date_exchange") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="งานหน่วยงาน" ItemStyle-CssClass="text-center" ItemStyle-Width="15%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:Label ID="lbltype_jobidx" Visible="false" runat="server" Text='<%# Eval("type_jobidx") %>' />
                                                                            <asp:Label ID="lbltype_job" runat="server" Text='<%# Eval("type_job") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="องค์กร" ItemStyle-CssClass="text-center" ItemStyle-Width="15%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:Label ID="lblOrgIDX" Visible="false" runat="server" Text='<%# Eval("OrgIDX_exchange") %>' />
                                                                            <asp:Label ID="lblOrgNameTH" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-CssClass="text-center" ItemStyle-Width="15%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:Label ID="lblRDeptIDX" Visible="false" runat="server" Text='<%# Eval("RDeptIDX_exchange") %>' />
                                                                            <asp:Label ID="lblDeptNameTH" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="แผนก" ItemStyle-CssClass="text-center" ItemStyle-Width="15%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:Label ID="lblRSecIDX" Visible="false" runat="server" Text='<%# Eval("RSecIDX_exchange") %>' />
                                                                            <asp:Label ID="lblSecNameTH" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-CssClass="text-center" ItemStyle-Width="20%"
                                                                    HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <div class="word-wrap">
                                                                            <asp:Label ID="lbldetail_exchange" runat="server" Text='<%# Eval("detail_exchange") %>' />
                                                                        </div>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="จัดการ" HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" ItemStyle-Width="3%" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="btndelete" runat="server" Text="Delete" CssClass="btn btn-danger btn-xs" OnClientClick="return confirm('คุณต้องการลบข้อมูลใช่หรือไม่?')" CommandName="bnDeleteList"><i class="glyphicon glyphicon-trash"></i></asp:LinkButton>

                                                                    </ItemTemplate>

                                                                    <EditItemTemplate />
                                                                    <FooterTemplate />
                                                                </asp:TemplateField>

                                                            </Columns>

                                                        </asp:GridView>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="col-sm-1">
                                            </div>

                                            <div class="form-group" id="div_save" runat="server" visible="false">
                                                <div class="col-sm-2 col-sm-offset-2">
                                                    <asp:LinkButton ID="lbladd" ValidationGroup="SaveTopic" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd_Exchange" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                </div>
                                            </div>
                                        </div>
                                </InsertItemTemplate>
                            </asp:FormView>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">


                                    <asp:GridView ID="GvSelectExchange" runat="server"
                                        AutoGenerateColumns="false"
                                        CssClass="table  table-bordered table-hover table-responsive col-lg-3"
                                        HeaderStyle-CssClass="info"
                                        HeaderStyle-Height="40px"
                                        AllowPaging="false"
                                        DataKeyNames="u2_docidx"
                                        OnRowDataBound="Master_RowDataBound">


                                        <PagerStyle CssClass="pageCustom" />
                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                        <EmptyDataTemplate>
                                            <div style="text-align: center">Data Cannot Be Found</div>
                                        </EmptyDataTemplate>

                                        <Columns>


                                            <asp:TemplateField HeaderText="วันที่ส่งมอบงาน" ItemStyle-CssClass="text-center" ItemStyle-Width="5%"
                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <div class="word-wrap">
                                                        <asp:Label ID="lbldate_exchange" runat="server" Text='<%# Eval("date_exchange") %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="องค์กร" ItemStyle-CssClass="text-center" ItemStyle-Width="15%"
                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <div class="word-wrap">
                                                        <asp:Label ID="lblOrgIDX" Visible="false" runat="server" Text='<%# Eval("orgidx") %>' />
                                                        <asp:Label ID="lblOrgNameTH" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="ฝ่าย" ItemStyle-CssClass="text-center" ItemStyle-Width="15%"
                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <div class="word-wrap">
                                                        <asp:Label ID="lblRDeptIDX" Visible="false" runat="server" Text='<%# Eval("rdeptidx") %>' />
                                                        <asp:Label ID="lblDeptNameTH" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="แผนก" ItemStyle-CssClass="text-center" ItemStyle-Width="15%"
                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <div class="word-wrap">
                                                        <asp:Label ID="lblRSecIDX" Visible="false" runat="server" Text='<%# Eval("rsecidx") %>' />
                                                        <asp:Label ID="lblSecNameTH" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-CssClass="text-center" ItemStyle-Width="20%"
                                                HeaderStyle-Font-Size="Smaller" ItemStyle-Font-Size="Smaller" HeaderStyle-CssClass="text-center">
                                                <ItemTemplate>
                                                    <div class="word-wrap">
                                                        <asp:Label ID="lbldetail_exchange" runat="server" Text='<%# Eval("detail_exchange") %>' />
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>

                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="col-lg-12">
                        <asp:Panel ID="panel_approveholder" runat="server" Visible="false">

                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-blackboard"></i><strong>&nbsp; ผลการอนุมัติ</strong></h3>
                                </div>

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">

                                        <div class="form-group">

                                            <asp:Label ID="Label33" CssClass="col-sm-2 control-label text_right" runat="server" Text="สถานะอนุมัติ" />
                                            <div class="col-sm-3">
                                                <asp:DropDownList ID="ddl_approve" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">เลือกสถานะอนุมัติ</asp:ListItem>
                                                    <asp:ListItem Value="2">อนุมัติ</asp:ListItem>
                                                    <asp:ListItem Value="3">ไม่อนุมัติ </asp:ListItem>
                                                    <asp:ListItem Value="4">ส่งกลับแก้ไขรายการ </asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator15" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddl_approve" Font-Size="11"
                                                    ErrorMessage="เลือกสถานะอนุมัติ"
                                                    ValidationExpression="เลือกสถานะอนุมัติ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender16" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator15" Width="160" />

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <asp:Label ID="Label26" runat="server" Text="ความคิดเห็นเพิ่มเติม" CssClass="col-sm-2 control-label text_right"></asp:Label>
                                            <div class="col-sm-9">
                                                <asp:TextBox ID="txtremark_approve" TextMode="multiline" Width="510pt" Rows="5" runat="server" CssClass="form-control" PlaceHoldaer="........" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ValidationGroup="Save" runat="server" Display="None" ControlToValidate="txtremark_approve" Font-Size="11"
                                                    ErrorMessage="กรุณากรอกความคิดเห็น"
                                                    ValidationExpression="กรุณากรอกความคิดเห็น"
                                                    SetFocusOnError="true" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator8" Width="160" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                                    ValidationGroup="Save" Display="None"
                                                    ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                                    ControlToValidate="txtremark_approve"
                                                    ValidationExpression="^[\s\S]{0,1000}$"
                                                    SetFocusOnError="true" />

                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorsCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />

                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-9">
                                                <asp:LinkButton ID="LinkButton3" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="CmdUpdateApprove" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                <asp:LinkButton ID="LinkButton4" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </asp:Panel>
                    </div>

                    <div class="col-lg-12">

                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ประวัติการอนุมัติ</strong></h3>
                            </div>

                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <asp:Repeater ID="rpLog" runat="server">
                                        <HeaderTemplate>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label"><small>วัน / เวลา</small></label>
                                                <label class="col-sm-3 control-label"><small>ผู้ดำเนินการ</small></label>
                                                <label class="col-sm-2 control-label"><small>ดำเนินการ</small></label>
                                                <label class="col-sm-2 control-label"><small>ผลการดำเนินการ</small></label>
                                                <label class="col-sm-2 control-label"><small>ความคิดเห็น</small></label>
                                            </div>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <%-- <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small><%#Eval("createdate")%></small></span>--%>
                                                    <div class="col-sm-2">
                                                    </div>
                                                    <div class="col-sm-8">
                                                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small><%#Eval("createdate")%></small></span>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <span>&nbsp;&nbsp;&nbsp;<small><%# Eval("FullNameTH") %>&nbsp;&nbsp;&nbsp;(<%# Eval("actor_name") %>)</small></span>
                                                </div>
                                                <div class="col-sm-2">
                                                    <span><small><%# Eval("node_name") %></small></span>
                                                </div>
                                                <div class="col-sm-2">
                                                    <span><small><%# Eval("statue_name") %></small></span>
                                                </div>
                                                <div class="col-sm-2">
                                                    <span><small><%# Eval("coment") %></small></span>
                                                </div>
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div id="div_exitinterview" runat="server" visible="false">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-envelope"></i><strong>&nbsp; ข้อมูล Exit Interview</strong></h3>
                            </div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="col-lg-12">
                                        <div class="alert-message alert-message-success">
                                            <blockquote class="danger" style="font-size: small; background-color: forestgreen; text-align: left">
                                                <h4 style="color: whitesmoke"><b>แบบฟอร์มลาออก</b></h4>
                                            </blockquote>
                                        </div>


                                        <div class="panel-body">
                                            <div class="form-horizontal" role="form">
                                                <asp:GridView ID="GvTypeAns_detail" runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                    HeaderStyle-Height="40px"
                                                    AllowPaging="false"
                                                    DataKeyNames="m0_taidx"
                                                    OnRowDataBound="Master_RowDataBound">

                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                    </EmptyDataTemplate>
                                                    <Columns>


                                                        <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="" ItemStyle-CssClass="text-left">
                                                            <ItemTemplate>

                                                                <div class="alert-message alert-message-success">
                                                                    <blockquote class="danger" style="background-color: #cce6ff;">
                                                                        <h4><b>
                                                                            <i class="fa fa-edit"></i>&nbsp;
                                            <asp:Label ID="Label1" runat="server" Font-Bold="true" Text='<%# Eval("type_ans") %>'></asp:Label>
                                                                        </b></h4>
                                                                    </blockquote>
                                                                </div>

                                                                <asp:Label ID="lblm0_taidx" runat="server" CssClass="col-sm-10" Visible="false" Text='<%# Eval("m0_taidx") %>'></asp:Label>

                                                                <asp:GridView ID="GvQuestion_detail" runat="server"
                                                                    AutoGenerateColumns="false"
                                                                    CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                                                    HeaderStyle-CssClass="danger small"
                                                                    HeaderStyle-Height="40px"
                                                                    AllowPaging="false"
                                                                    DataKeyNames="m0_quidx"
                                                                    OnRowDataBound="Master_RowDataBound">


                                                                    <PagerStyle CssClass="pageCustom" />
                                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                                                    <EmptyDataTemplate>
                                                                        <div style="text-align: center">Data Cannot Be Found</div>
                                                                    </EmptyDataTemplate>
                                                                    <Columns>

                                                                        <asp:TemplateField HeaderText="ข้อที่" ItemStyle-CssClass="text-center" HeaderStyle-CssClass="text-center" ItemStyle-Width="5%" Visible="true" ItemStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblm0_toidx" Visible="false" runat="server" Text='<%# Eval("m0_toidx") %>' />
                                                                                <asp:Label ID="lblm0_tqidx" Visible="false" runat="server" Text='<%# Eval("m0_tqidx") %>' />
                                                                                <asp:Label ID="lblm0_taidx" Visible="false" runat="server" Text='<%# Eval("m0_taidx") %>' />
                                                                                <asp:Label ID="lblno_choice" runat="server" Text='<%# Eval("no_choice") %>' />
                                                                                <asp:Label ID="lblanswer" runat="server" Visible="false" Text='<%# Eval("answer") %>' />
                                                                                <asp:Label ID="lblremark" runat="server" Visible="false" Text='<%# Eval("remark") %>' />
                                                                            </ItemTemplate>

                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="คำถาม" ItemStyle-CssClass="text-left" ItemStyle-Width="20%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblm0_quidx" Visible="false" runat="server" Text='<%# Eval("m0_quidx") %>' />
                                                                                <asp:Label ID="lblchoice" runat="server" Text='<%# Eval("quest_name") %>'></asp:Label>

                                                                                <asp:Panel runat="server" ID="panel_choice_rdo" Visible="false">
                                                                                    <br />
                                                                                    <asp:RadioButtonList ID="rdochoice_exit" Enabled="false" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                                                    </asp:RadioButtonList>

                                                                                </asp:Panel>

                                                                                <asp:Panel runat="server" ID="panel_choice_chk" Visible="false">
                                                                                    <br />
                                                                                    <asp:CheckBoxList ID="chkchoice_exit" Enabled="false" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical">
                                                                                    </asp:CheckBoxList>
                                                                                </asp:Panel>

                                                                                <asp:Panel runat="server" ID="panel_comment" Visible="false">
                                                                                    <div class="form-group">
                                                                                        <div class="col-sm-10">
                                                                                            <asp:TextBox ID="txtquest" Text='<%# Eval("answer") %>' Enabled="false" row="20" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
                                                                                        </div>
                                                                                </asp:Panel>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                    </Columns>
                                                                </asp:GridView>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </asp:View>

        <asp:View ID="ViewAproove" runat="server">
            <div class="col-lg-12 bg-template-resign">
                <div class="col-lg-12">
                    <asp:Image ID="Image2" runat="server" class="img-responsive img-fluid" ImageUrl="~/images/hr_resignation/Cover_Head_ระบบลาออก_02-05.png" Style="height: 100%; width: 100%;" />
                </div>

                <div class="col-lg-12">

                    <div class="alert-message alert-message-success">
                        <blockquote class="danger" style="font-size: small; background-color: darkkhaki; text-align: left">
                            <h3><b>รายการรออนุมัติ</b></h3>
                        </blockquote>

                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">
                            <asp:GridView ID="GvApprove" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="info small"
                                HeaderStyle-Height="40px"
                                AllowPaging="false"
                                OnRowDataBound="Master_RowDataBound"
                                DataKeyNames="u0_docidx">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="ระบบ" ItemStyle-Width="10%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbu0_docidx" runat="server" Visible="false" Text='<%# Eval("u0_docidx") %>'></asp:Label>
                                            <asp:Label ID="lbm0_tidx" Visible="false" runat="server" Text='<%# Eval("m0_tidx") %>'></asp:Label>
                                            <asp:Label ID="lbltype_menu" Font-Bold="true" runat="server" Text='<%# Eval("type_menu") %>'></asp:Label>
                                            <asp:Label ID="lblunidx_ex" runat="server" Visible="false" Text='<%# Eval("unidx_ex") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="ข้อมูลผู้ทำรายการ" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <small>
                                                <strong>
                                                    <asp:Label ID="Label15" runat="server">องค์กร: </asp:Label></strong>
                                                <asp:Literal ID="Literal3" runat="server" Text='<%# Eval("OrgNameTH") %>' />
                                                </p>
                                                 <strong>
                                                     <asp:Label ID="Label24" runat="server">ฝ่าย: </asp:Label></strong>
                                                <asp:Literal ID="Literal1" runat="server" Text='<%# Eval("DeptNameTH") %>' />
                                                </p>
                                                 <strong>
                                                     <asp:Label ID="Label25" runat="server">แผนก: </asp:Label></strong>
                                                <asp:Literal ID="Literal2" runat="server" Text='<%# Eval("SecNameTH") %>' />
                                                </p>
                                                 <strong>
                                                     <asp:Label ID="Label26" runat="server">ชื่อ-สกุล: </asp:Label></strong>
                                                <asp:Literal ID="Literal4" runat="server" Text='<%# Eval("FullNameTH") %>' />
                                                </p>
                                            </small>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="วันที่ทำรายการ" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbname" runat="server" Text='<%# Eval("createdate") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="วันที่ต้องการออกจากงาน" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Label ID="lbuse_year" runat="server" Text='<%# Eval("date_resign") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะรายการ" ItemStyle-Width="25%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltunidx" Visible="false" runat="server" Text='<%# Eval("unidx") %>' /></small>
                                            <asp:Literal ID="ltstaidx" Visible="false" runat="server" Text='<%# Eval("staidx") %>' /></small>
                                                    <strong>
                                                        <asp:Label ID="Label1d5" runat="server">สถานะรายการ: </asp:Label></strong>
                                            <asp:Label ID="lblstasus" Font-Bold="true" runat="server" Text='<%# Eval("StatusDoc") %>'></asp:Label>
                                            </p>
                                                    <div id="divstatus_ex" runat="server" visible="false">
                                                        <strong>
                                                            <asp:Label ID="Labels24" runat="server">สถานะส่งมอบงาน: </asp:Label></strong>
                                                        <asp:Label ID="lblstasus_ex" Font-Bold="true" runat="server" Text='<%# Eval("StatusDoc_Ex") %>'></asp:Label>
                                                        </p>
                                                    
                                                    </div>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-Width="15%" HeaderStyle-CssClass="text-center" ItemStyle-HorizontalAlign="center" ItemStyle-Font-Size="Small">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnlinkquest" CssClass="btn btn-primary btn-sm" runat="server" OnCommand="btnCommand" CommandName="CmdDetail" CommandArgument='<%#  Eval("u0_docidx") + ";" + Eval("CEmpIDX")+ ";" + Eval("m0_tidx")+ ";" + "1"  %>'><i class="	fa fa-book"></i></asp:LinkButton>
                                            <asp:LinkButton ID="btnexchange" Visible="false" CssClass="btn btn-success btn-sm" runat="server" OnCommand="btnCommand" CommandName="CmdDetail" CommandArgument='<%#  Eval("u0_docidx") + ";" + Eval("CEmpIDX")+ ";" + Eval("m0_tidx") + ";"+ "2"%>'><i class="	fa fa-envelope"></i></asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>


                        </div>
                    </div>
                </div>
            </div>


        </asp:View>
    </asp:MultiView>

    <script>

        $(function () {
            var string = $(".from-date-datepicker").val();
            //alert(string);
            if (string == '') {
                ////alert(11);
                $('.from-date-datepicker').datetimepicker({

                    minDate: moment().add(+1, 'day'),
                    format: 'DD/MM/YYYY',
                    ignoreReadonly: true,

                });

            }
            else {
                //alert(12);
                $('.from-date-datepicker').datetimepicker({

                    minDate: moment().add(+1, 'day'),
                    format: 'DD/MM/YYYY', //$('.datetimepicker-fromsearch').data("DateTimePicker").date(e.date.format("DD/MM/YYYY")),//'DD/MM/YYYY',
                    //ignoreReadonly: true,

                }).val(string);
            }
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                var string = $(".from-date-datepicker").val();
                //alert(string);
                if (string == '') {
                    //alert(1);
                    $('.from-date-datepicker').datetimepicker({

                        minDate: moment().add(+1, 'day'),
                        format: 'DD/MM/YYYY',
                        ignoreReadonly: true,

                    });

                }
                else {
                    //alert(2);
                    $('.from-date-datepicker').datetimepicker({

                        minDate: moment().add(+1, 'day'),
                        format: 'DD/MM/YYYY', //$('.datetimepicker-fromsearch').data("DateTimePicker").date(e.date.format("DD/MM/YYYY")),//'DD/MM/YYYY',
                        //ignoreReadonly: true,

                    }).val(string);
                }
            });
        });
    </script>


   <%-- <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY',
                minDate: moment().add(+1, 'day')
            });
        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY',
                    minDate: moment().add(+1, 'day')
                });
            });
        });

    </script>--%>
</asp:Content>

