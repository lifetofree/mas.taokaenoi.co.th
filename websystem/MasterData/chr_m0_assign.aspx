﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="chr_m0_assign.aspx.cs" Inherits="websystem_MasterData_chr_m0_assign" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="txt" runat="server"></asp:Literal>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

                <asp:View ID="ViewIndex" runat="server">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-user"></i><strong>&nbsp; Assign (หนังสือมอบอำนาจ)</strong></h3>
                        </div>

                        <div class="panel-body">
                            <div class="form-group">

                                <asp:LinkButton ID="btnshow" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Add User SAP" runat="server" CommandName="CmdAdd" OnCommand="btnCommand"><i class="fa fa-plus"></i></asp:LinkButton>
                            </div>

                            <asp:Panel ID="Panel_Add" runat="server" Visible="false">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><i class="glyphicon glyphicon-plus"></i><strong>&nbsp; Add Assign (หนังสือมอบอำนาจ) </strong></h4>
                                    <div class="form-horizontal" role="form">
                                        <div class="panel-heading">

                                            <div class="form-group">
                                                <asp:Label ID="Label1" runat="server" Text="System" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlsystem" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">กรุณาเลือกระบบ...</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidddator8" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddlsystem" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกระบบ"
                                                    ValidationExpression="กรุณาเลือกระบบ" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidddator8" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label2" runat="server" Text="Organization" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlorgidx" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">กรุณาเลือกองค์กร...</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddlorgidx" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกองค์กร"
                                                    ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label3" runat="server" Text="Department" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlrdeptidx" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">กรุณาเลือกฝ่าย...</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddlrdeptidx" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือกฝ่าย"
                                                    ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label4" runat="server" Text="Section" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlsecidx" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">กรุณาเลือก Section...</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddlsecidx" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือก Section"
                                                    ValidationExpression="กรุณาเลือก Section" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator3" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="Label26" runat="server" Text="Assign Name" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddlassign" runat="server" CssClass="form-control">
                                                        <asp:ListItem Value="0">กรุณาเลือก Assign Name...</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>

                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save" runat="server" Display="None"
                                                    ControlToValidate="ddlassign" Font-Size="11"
                                                    ErrorMessage="กรุณาเลือก Assign Name"
                                                    ValidationExpression="กรุณาเลือก Assign Name" InitialValue="0" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />

                                            </div>

                                            <div class="form-group">
                                                <asp:Label ID="lbl" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                <div class="col-sm-6">
                                                    <asp:DropDownList ID="ddStatusadd" CssClass="form-control" runat="server">
                                                        <asp:ListItem Value="1" Text="Online" />
                                                        <asp:ListItem Value="2" Text="Offline" />
                                                    </asp:DropDownList>
                                                </div>
                                            </div>

                                            <asp:UpdatePanel ID="UpdatePnFileUpload" runat="server">
                                                <ContentTemplate>
                                                    <div class="form-group">
                                                        <asp:Label ID="Label6" CssClass="col-sm-3 control-label" runat="server" Text="แนบไฟล์" />

                                                        <div class="col-md-6">
                                                            <asp:FileUpload ID="UploadFileRoom" Font-Size="small" ViewStateMode="Enabled" runat="server"
                                                                CssClass="control-label" accept="jpg|pdf|png" />
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-3">
                                                            <asp:LinkButton ID="lbladd" ValidationGroup="Save" CssClass="btn btn-success btn-sm" data-toggle="tooltip" title="Save" runat="server" CommandName="btnAdd" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการยืนยันการทำรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-save"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lblcancel" CssClass="btn btn-default btn-sm" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="btnCancel" data-toggle="tooltip" title="Close"><i class="fa fa-times"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </ContentTemplate>
                                                <Triggers>
                                                    <asp:PostBackTrigger ControlID="lbladd" />
                                                </Triggers>
                                            </asp:UpdatePanel>

                                        </div>
                                    </div>
                                    <hr />
                                </div>
                            </asp:Panel>

                            <asp:GridView ID="GvMaster" runat="server"
                                AutoGenerateColumns="false"
                                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                                HeaderStyle-CssClass="primary"
                                HeaderStyle-Height="40px"
                                AllowPaging="true"
                                DataKeyNames="AssignIDX"
                                PageSize="10"
                                OnRowDataBound="Master_RowDataBound"
                                OnRowEditing="Master_RowEditing"
                                OnRowCancelingEdit="Master_RowCancelingEdit"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                OnRowUpdating="Master_RowUpdating">

                                <PagerStyle CssClass="pageCustom" />
                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                <EmptyDataTemplate>
                                    <div style="text-align: center">Data Cannot Be Found</div>
                                </EmptyDataTemplate>
                                <Columns>

                                    <asp:TemplateField HeaderText="#">

                                        <ItemTemplate>
                                            <asp:Label ID="lblstidx" runat="server" Visible="false" Text='<%# Eval("AssignIDX") %>' />
                                            <%# (Container.DataItemIndex +1) %>
                                        </ItemTemplate>


                                        <EditItemTemplate>
                                            <div class="form-horizontal" role="form">
                                                <div class="panel-heading">
                                                    <div class="form-group">
                                                        <div class="col-sm-2">
                                                            <asp:TextBox ID="txtSysidx" runat="server" CssClass="form-control" Visible="false" Text='<%# Eval("AssignIDX")%>' />
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="System" />
                                                        <div class="col-sm-8">
                                                            <asp:Label ID="lbl_SysIDX_edit" runat="server" Text='<%# Bind("Sysidx") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlsysidx_edit" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="ReddlSysIDXedit" ValidationGroup="Save_edit" runat="server" Display="None"
                                                            ControlToValidate="ddlsysidx_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกระบบ"
                                                            ValidationExpression="กรุณาเลือกระบบ" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="ReddlSysIDXedit" Width="160" />


                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label5" CssClass="col-sm-3 control-label" runat="server" Text="Organization" />
                                                        <div class="col-sm-8">
                                                            <asp:Label ID="lbl_OrgIDX_edit" runat="server" Text='<%# Bind("OrgIDX") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlorgidx_edit" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="Save_edit" runat="server" Display="None"
                                                            ControlToValidate="ddlorgidx_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกองค์กร"
                                                            ValidationExpression="กรุณาเลือกองค์กร" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator4" Width="160" />


                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label7" CssClass="col-sm-3 control-label" runat="server" Text="Department" />
                                                        <div class="col-sm-8">
                                                            <asp:Label ID="lbl_RDeptIDX_edit" runat="server" Text='<%# Bind("RDeptIDX") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlrdeptidx_edit"  OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ValidationGroup="Save_edit" runat="server" Display="None"
                                                            ControlToValidate="ddlrdeptidx_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกฝ่าย"
                                                            ValidationExpression="กรุณาเลือกฝ่าย" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender6" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator5" Width="160" />

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label9" CssClass="col-sm-3 control-label" runat="server" Text="Section" />
                                                        <div class="col-sm-8">
                                                            <asp:Label ID="lbl_RSecIDX_edit" runat="server" Text='<%# Bind("RSecIDX") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlrsecidx_edit" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ValidationGroup="Save_edit" runat="server" Display="None"
                                                            ControlToValidate="ddlrsecidx_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกแผนก"
                                                            ValidationExpression="กรุณาเลือกแผนก" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender8" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator6" Width="160" />

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="Label26" runat="server" Text="Assign Name" CssClass="col-sm-3 control-label text_right"></asp:Label>
                                                        <div class="col-sm-8">
                                                            <asp:Label ID="lbl_EmpIDX_edit" runat="server" Text='<%# Bind("EmpIDX") %>' Visible="false" />
                                                            <asp:DropDownList ID="ddlempidx_edit" AutoPostBack="true" CssClass="form-control" runat="server">
                                                            </asp:DropDownList>

                                                        </div>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Save_edit" runat="server" Display="None"
                                                            ControlToValidate="ddlempidx_edit" Font-Size="11"
                                                            ErrorMessage="กรุณาเลือกรายชื่อ"
                                                            ValidationExpression="กรุณาเลือกรายชื่อ" InitialValue="0" />
                                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator7" Width="160" />

                                                    </div>

                                                    <div class="form-group">
                                                        <asp:Label ID="lbstatate" CssClass="col-sm-3 control-label" runat="server" Text="Status" />
                                                        <div class="col-sm-8">
                                                            <asp:DropDownList ID="ddStatusUpdate" runat="server" CssClass="form-control" SelectedValue='<%# (int)Eval("Assign_Status") %>'>
                                                                <asp:ListItem Value="1" Text="Online" />
                                                                <asp:ListItem Value="2" Text="Offline" />
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <div class="col-sm-2 col-sm-offset-10">
                                                            <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success btn-sm" runat="server" ValidationGroup="Save_edit" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"><i class="fa fa-check"></i></asp:LinkButton>
                                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-default btn-sm" runat="server" CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>


                                        </EditItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="System Name" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbsysname" runat="server" Text='<%# Eval("System_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Organization" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lborgname" runat="server" Text='<%# Eval("OrgNameTH") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Department" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbdepname" runat="server" Text='<%# Eval("DeptNameTH") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Section" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbcostname" runat="server" Text='<%# Eval("SecNameTH") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Assign Name" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbusername" runat="server" Text='<%# Eval("FullNameTH") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lbstatus" runat="server" Text='<%# Eval("Assign_StatusDetail") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="File Document" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                            <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>
                                            <asp:HiddenField runat="server" ID="hidFile11" Value='<%#  Eval("AssignIDX") %>' />

                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>



                                    <asp:TemplateField HeaderText="Management" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="15%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="Edit" CssClass="btn btn-primary btn-sm" runat="server" CommandName="Edit" data-toggle="tooltip" title="Edit"><i class="fa fa-edit"></i></asp:LinkButton>
                                            <asp:LinkButton ID="Delete" CssClass="btn btn-danger btn-sm" runat="server" CommandName="CmdDel" OnCommand="btnCommand" data-toggle="tooltip" title="Delete" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')" CommandArgument='<%# Eval("AssignIDX") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                        </ItemTemplate>

                                        <EditItemTemplate />
                                        <FooterTemplate />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

