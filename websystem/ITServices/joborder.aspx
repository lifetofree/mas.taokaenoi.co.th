﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="joborder.aspx.cs" Inherits="websystem_ITServices_Default" %>

    <asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
    </asp:Content>
    <asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
        <style>
            .box_css3_raduis {
                border: solid 1px green;
                -webkit-border-radius: 10px;
                -moz-border-radius: 10px;
                border-radius: 10px;
                background: #009900;
            }

            #glad {
                border-radius: 10px 10px 10px 10px;
                -moz-border-radius: 10px 10px 10px 10px;
                -webkit-border-radius: 10px 10px 10px 10px;
                border: 7px solid #179c19;
            }

            h1 {
                font-size: small;
                color: red;
            }

            .date {
                width: 12%;
            }

            .day {
                width: 5%;
            }

            .a {
                display: block;
                border: solid 1px green;
                -webkit-border-radius: 10px;
                -moz-border-radius: 10px;
                border-radius: 10px;
                background: #009900;
            }
        </style>
        <script>
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });

            });
            //});

            var dat = Sys.WebForms.PageRequestManager.getInstance();
            dat.add_endRequest(function () {
                $(function () {
                    $('.from-date-datepicker').datetimepicker({
                        format: 'DD/MM/YYYY'
                    });
                });
            });
        </script>
        <script>
            $(function () {

                $('.from-date-datepickerexpire').datetimepicker({
                    format: 'DD/MM/YYYY',
                    minDate: new Date()

                });
            });


            var dat = Sys.WebForms.PageRequestManager.getInstance();
            dat.add_endRequest(function () {
                $(function () {
                    $('.from-date-datepickerexpire').datetimepicker({
                        format: 'DD/MM/YYYY',
                        minDate: new Date()

                    });
                });
            });
        </script>
        <script type="text/javascript">
            function openModal() {
                $('#opmodal').modal('show');
                $('#sentmodal').modal('show');

            }
        </script>
        <script type="text/javascript">
            window.scrollTo(0, 0);
        </script>
        <script type="text/javascript">
            $('#date').datepicker();
            $('#date').datepicker('setDate', '04/23/2014');
        </script>
        <div>
            <asp:Literal id="p1" runat="server" />
            <asp:MultiView ID="FirstMultiview" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">
                    <div class="w3-container" style="background-color: #f0f0f0; padding: 2% 5% 2% 5%;">
                        <div class="box_css3_raduis " style="background-color: #ffffff; padding: 2% 2% 2% 2%;">

                            <div class="w3-container">
                                <div class=".col-md-1">
                                    <center>
                                        <asp:ImageButton width="50%" ID="homebut" ImageUrl="~/images/joborder/title.png" runat="server" CommandName="submithome"
                                            OnCommand="btnuser" data-toggle="tooltip" title="หน้าหลัก" />
                                        <!--<asp:Image ImageUrl="~/images/joborder/title.png" width="50%" id="r1" runat="server" />-->
                                    </center>
                                </div>
                                <asp:Label style="font-size:20px;margin-left:20%;color:dodgerblue " id="e1" runat="server">
                                    <center>กรุณาเลือกสถานะ ที่ต้องการใช้</center>
                                </asp:Label><br />

                                <div class=".col-md-4">
                                    <center><br />&emsp;&emsp;
                                        <asp:ImageButton width="22%" ID="userbut" ImageUrl="~/images/joborder/user.png" runat="server" CommandName="submit1" OnCommand="btnuser"
                                        />
                                        <asp:ImageButton width="22%" ID="headbut" ImageUrl="~/images/joborder/head.png" runat="server" CommandName="submit2" OnCommand="btnuser"
                                        />
                                        <asp:ImageButton width="22%" ID="misbut" ImageUrl="~/images/joborder/mis.png" runat="server" CommandName="submit3" OnCommand="btnuser"
                                        />
                                        <asp:ImageButton width="22%" ID="mishbut" ImageUrl="~/images/joborder/mishead.png" runat="server" CommandName="submit4" OnCommand="btnuser"
                                        />

                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="col-md-5">
                        <asp:LinkButton ID="create" style="margin-right:80%; margin-top:2" class="btn btn-primary" runat="server" visible="false"
                            CommandName="click" OnCommand="btnuser" Text="+ คลิกเพื่อสร้างใบjoborder" />
                        <br><br/>
                        <asp:LinkButton ID="searchbutton" style="margin-right:80%; margin-top:2" class="btn btn-primary m-b-10 f-s-14" runat="server"
                            visible="false" CommandName="showsearch" OnCommand="btnuser"><i class="glyphicon glyphicon-search"></i> แสดงเครื่องมือค้นหา</asp:LinkButton>
                        <asp:LinkButton ID="searchhidden" style="margin-right:80%; margin-top:2" class="btn btn-danger m-b-10 f-s-14" runat="server"
                            visible="false" CommandName="showsearch1" OnCommand="btnuser" Text="ซ่อนเครื่องมือค้นหา" />
                    </div>
                    <br/><br/><br/><br/>
                    <br/>
                    <div id="_divsearch" runat="server" visible="false">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="f-s-13">รหัสเอกสาร</label>
                                        <asp:TextBox ID="TempCodeId" runat="server" CssClass="form-control" placeholder="รหัสเอกสาร" />
                                       
                                    </div>
                                    <div class="form-group">
                                         <div class="col-md-12">
                                            <div class="pull-left">
                                                <asp:LinkButton ID="btnSearch"  runat="server" CssClass="btn btn-success"
                                                    OnCommand="btnuser" CommandName="btncommandsearch" ><i class="glyphicon glyphicon-search"></i> </asp:LinkButton>
                                                <asp:LinkButton ID="btnReset"  runat="server"
                                                    CssClass="btn btn-warning f-s-14"><i class="glyphicon glyphicon-repeat"></i> </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <asp:Panel ID="remoteuser" visible="false" runat="server">
                        <div class="panel panel-primary" runat="server" id="insert" visible="true">
                            <div class="panel-heading" style="background-color:cornflowerblue">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-file"></i><strong>&nbsp; ระบบส่งใบ Job order</strong></h3>
                            </div>
                            <div class="panel-body">

                                <div class="form-group">

                                    <div class="col-sm-10">
                                        <!--<asp:Label ID="node_idx" runat="server" Visible="false" Text='<%# Eval("m0_node_idx") %>' />
                                        <asp:Label ID="actor_idx" runat="server" Visible="false" Text='<%# Eval("m0_actor_idx") %>' />-->
                                        <asp:Label ID="reinput1" CssClass="col-sm-2 control-label" runat="server" Text="ระบุหัวข้อความต้องการ: " />

                                        <asp:Textbox runat="server" ID="titlename" placeholder="กรุณากรอกหัวข้อ">
                                        </asp:Textbox>
                                        <asp:RequiredFieldValidator ID="requiredtitle" ValidationGroup="Update" runat="server" Display="Dynamic" SetFocusOnError="true"
                                            ControlToValidate="titlename" Font-Size="0.8em" ForeColor="Red" ErrorMessage="*กรุณากรอกข้อมูลให้ครบ"
                                        />


                                    </div>

                                    <div class="col-sm-10">
                                        <asp:Label ID="reinput2" CssClass="col-sm-2 control-label" runat="server" Text="ระบุรายละเอียดความต้องการ: " />
                                        <br>
                                        <div class="col-sm-9">
                                            <asp:TextBox ID="require" runat="server" TextMode="MultiLine" style="margin-left:20%;margin-top:1%" height="200px" Width="650px"
                                                placeholder="กรุณากรอกรายละเอียดความต้องการ"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="requiredreq" ValidationGroup="Update" runat="server" Display="Dynamic" SetFocusOnError="true"
                                                ControlToValidate="require" Font-Size="0.8em" ForeColor="Red" ErrorMessage="*กรุณากรอกข้อมูลให้ครบ"
                                            />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <asp:LinkButton ID="trueadd" style="margin-left:100%; margin-top:2.5%" class="btn btn-success" runat="server" visible="true"
                                                data-toggle="tooltip" title="add" CommandName="add" OnCommand="btnuser" ValidationGroup="Update"
                                                OnClientClick="return confirm('คุณต้องการ สร้างใบJOB ใช่หรือไม่')">
                                                <i class="glyphicon glyphicon-plus"></i>
                                            </asp:LinkButton>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:LinkButton ID="reset" style="margin-left:5%; margin-top:5%" class="btn btn-info" runat="server" visible="true" CommandName="reset"
                                                data-toggle="tooltip" title="reset" OnCommand="btnuser">
                                                <i class="glyphicon glyphicon-repeat"></i>
                                            </asp:LinkButton>
                                        </div>
                                        <div class="col-md-3">
                                            <asp:LinkButton ID="closejob" style="margin-right:30%; margin-top:5%" class="btn btn-danger" runat="server" visible="true"
                                                data-toggle="tooltip" title="close" CommandName="close" OnCommand="btnuser">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </asp:LinkButton>
                                        </div>

                                    </div>
                                    <!--<div class="row">
                                        <div class="col-md-5">
                                            <asp:LinkButton ID="clickgrid" style="margin-left:70%; margin-top:2px" class="btn btn-success" runat="server" visible="true"
                                                CommandName="click" OnCommand="btnuser" Text="คลิกเพื่อดูประวัติใบ joborder"
                                            />
                                        </div>
                                        <div class="col-md-7">
                                            <asp:LinkButton ID="closegrid" style="margin-right:30%; margin-top:2px" class="btn btn-danger" runat="server" visible="true"
                                                CommandName="close" OnCommand="btnuser" Text="คลิกเพื่อปิดประวัติใบ joborder"
                                            />
                                        </div>
                                    </div>-->


                                </div>

                            </div>

                        </div>

                    </asp:Panel>
        </div>
        <asp:Panel ID="grid" visible="false" runat="server">
            <asp:GridView ID="jogrid" runat="server" AutoGenerateColumns="false" DataKeyNames="u0_jo_idx" CssClass="table table-striped table-bordered table-responsive col-md-12 footable"
                HeaderStyle-CssClass="info" OnRowDataBound="Master_RowDataBound" AllowPaging="true" PageSize="10" AutoPostBack="False"
                onpageindexchanging="Master_PageIndexChanging" OnRowEditing="Master_RowEditing" OnRowCancelingEdit="Master_RowCancelingEdit"
                OnRowUpdating="Master_RowUpdating">
                <PagerStyle CssClass="pageCustom" HorizontalAlign="Center" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />


                <EmptyDataTemplate>
                    <div style="text-align: center;">Data Cannot Be Found</div>
                </EmptyDataTemplate>



                <Columns>

                    <asp:TemplateField HeaderText="รหัสเอกสาร" ItemStyle-CssClass="f-s-13 text-left" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="14%">
                        <ItemTemplate>
                            <small>
                              <asp:Label ID="gencode" runat="server"   Text='<%# Eval("no_invoice") %>' />
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ID" ItemStyle-CssClass="f-s-13 text-left" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="14%"
                        visible="false">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="u0" runat="server"  visible ="false" Text='<%# Eval("u0_jo_idx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                            </small>
                        </ItemTemplate>




                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ผู้สร้าง" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <asp:Label ID="emp" runat="server" Text='<%# Eval("AdminMain") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="TITLE" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <asp:Label ID="title" runat="server" Text='<%# Eval("title_jo") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="REQUIREMENT" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <asp:Label ID="req" runat="server" Text='<%# Eval("require_jo") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="DATE/MONTH/YEAR" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <asp:Label ID="dat" runat="server" Text='<%# Eval("day_created") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="STATUS" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="15%">
                        <ItemTemplate>
                            <asp:Label ID="_actorm0" runat="server" visible="false" Text='<%# Eval("m0_actor_idx") %>' />
                            <asp:Label ID="_nodem0" runat="server" visible="false" Text='<%# Eval("m0_node_idx") %>' />
                            <asp:Label ID="_idlog" runat="server" visible="false" Text='<%# Eval("jo_idx_ref") %>' />
                            <asp:Label ID="_idstate" runat="server" visible="false" Text='<%# Eval("id_statework") %>' />





                            <asp:Label ID="showact" runat="server" Text='<%# Eval("StatusDoc") %>' />
                            <!--<asp:Label ID="act" runat="server" Text='<%# Eval("name_actor") %>' />-->
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                        HeaderStyle-Width="15%">

                        <ItemTemplate>
                            <div class="col-sm-3">

                                

                            </div>

                            <div class="col-sm-3">

                                <asp:LinkButton id="addafter" style="margin-left:10%; margin-top:1px" class="btn btn-primary" runat="server" visible="false"
                                    CommandName="addaf" OnCommand="btnuser" data-toggle="tooltip" title="ส่งใบ JOB" CommandArgument='<%# Eval("u0_jo_idx") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("id_statework") %>'
                                    OnClientClick="return confirm('คุณต้องการ ส่งใบ JOB ORDER ใช่หรือไม่')">
                                    <i class="glyphicon glyphicon-export"></i>
                                </asp:LinkButton>

                                <asp:LinkButton id="sentaccept" style="margin-left:10%; margin-top:2%" class="btn btn-success" runat="server" visible="false"
                                    CommandName="showsent" OnCommand="btnuser" data-toggle="tooltip" title="เซ็นยอมรับ" CommandArgument='<%# Eval("u0_jo_idx") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("id_statework") %>'>
                                    <i class="glyphicon glyphicon-list-alt"></i>
                                </asp:LinkButton>
                                
                                <asp:LinkButton ID="sentrup" style="margin-left:10%; margin-top:2%" class="btn btn-default" runat="server" visible="false"
                                data-toggle="tooltip" title="เซ็นรับงาน" CommandArgument='<%# Eval("u0_jo_idx") %>'>
                                <i class="glyphicon glyphicon-save-file"></i>
                                </asp:LinkButton>
                            </div>
                            <div class="col-sm-3">



                                <asp:LinkButton ID="edit" style="margin-left:10%; margin-top:2%" class="btn btn-warning" runat="server" visible="true" CommandName="edit"
                                    OnCommand="btnuser" data-toggle="tooltip" title="detail" CommandArgument='<%# Eval("u0_jo_idx") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("id_statework")+ ";" + Eval("jo_idx_ref") %>'>
                                    <i class="glyphicon glyphicon-envelope"></i>
                                </asp:LinkButton>

                               

                            

                            </div>

                            <div class="col-sm-2">
                                <asp:LinkButton ID="delete" style="margin-right:200px; margin-top:2%" class="btn btn-danger" runat="server" visible="true"
                                    data-toggle="tooltip" title="delete" CommandName="del" OnCommand="btnuser" CommandArgument='<%# Eval("u0_jo_idx") %>'
                                    OnClientClick="return confirm('คุณต้องการ ลบข้อมูลใช่หรือไม่')">
                                    <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            </div>

                        </ItemTemplate>
                    </asp:TemplateField>













                </Columns>
            </asp:GridView>

        </asp:Panel>

        <div class="col-lg-12" runat="server" id="div_sentaccept">
            <div id="sentmodal" class="modal open" role="dialog">
                <div class="modal-dialog" style="width:45%">
                    <!--Modal content-->
                    <div class="modal-content" style="background:whitesmoke">
                        <div class="modal-header" style="color:#ffffff ;background:forestgreen">

                            <h4 class="modal-title">เอกสารเซ็นยอมรับ</h4>
                        </div>
                        <div class="modal-body">

                            <div class="panel-body">
                                <asp:FormView ID="DetailUsersent" DefaultMode="ReadOnly" runat="server" Width="100%" OnDataBound="DetailUsersent_DataBound">
                                    <ItemTemplate>
                                        <div class="form-horizontal" role="form">
                                            <div class="form-group">
                                                <asp:Label ID="lsdate" CssClass="col-sm-2 control-label" runat="server" Text="วันที่เริ่มงาน : " />

                                                <div class="col-sm-2">
                                                    <asp:TextBox ID="sdate" runat="server" CssClass="form-control" Maxlengh="100%" Enabled="false" style="width:94%" Text='<%# Eval("day_start") %>'></asp:TextBox>
                                                </div>
                                                <asp:Label ID="lfdate" CssClass="col-sm-2 control-label" runat="server" Text="วันที่สิ้นสุดงาน : " />

                                                <div class="col-sm-2">
                                                    <asp:TextBox ID="fdate" runat="server" CssClass="form-control" Maxlengh="100%" Enabled="false" style="width:94%" Text='<%# Eval("day_finish") %>'></asp:TextBox>
                                                </div>

                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:FormView>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <asp:LinkButton ID="yomrup" style="margin-left:100%; margin-top:2px" class="btn btn-success" runat="server" visible="true"
                                            CommandName="acceptsent" OnCommand="btnuser" Text="ยอมรับ" OnClientClick="return confirm('คุณต้องการ เซ็นยอมรับใช่หรือไม่')"
                                        />
                                    </div>
                                    <div class="col-sm-4">
                                        <asp:LinkButton ID="maiyomrup" style="margin-left:30%; margin-top:2px" class="btn btn-warning" runat="server" visible="true"
                                            CommandName="closemishead2" OnCommand="btnuser" Text="ไม่ยอมรับ" OnClientClick="return confirm('คุณต้องการ ไม่ยอมรับใช่หรือไม่')"
                                        />
                                    </div>
                                </div>
                                <div class="col-sm-2 col-sm-offset-10">
                                    <asp:LinkButton ID="Cancel1" class="btn btn-danger" data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal"
                                        CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

        </asp:View>

        <asp:View id="View2" runat="server">
            <div class="w3-container">
                <div class="row">
                    <div class="col-xs-6">
                        <asp:LinkButton ID="home" style="margin-right:80%; margin-top:1%" class="btn btn-primary" runat="server" visible="true" CommandName="home"
                            data-toggle="tooltip" title="หน้าหลัก" OnCommand="btnuser">
                            <i class="glyphicon glyphicon-home"></i>
                        </asp:LinkButton>
                    </div>
                </div>
            </div>
            <br>
            <asp:Panel ID="grid2" visible="true" runat="server">
                <asp:GridView ID="jogrid2" runat="server" AutoGenerateColumns="false" DataKeyNames="u0_jo_idx" CssClass="table table-striped table-bordered table-responsive col-md-12 footable"
                    HeaderStyle-CssClass="info" OnRowDataBound="Master_RowDataBound" AllowPaging="true" PageSize="10" AutoPostBack="False"
                    onpageindexchanging="Master_PageIndexChanging" OnRowEditing="Master_RowEditing" OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnRowUpdating="Master_RowUpdating">
                    <PagerStyle CssClass="pageCustom" HorizontalAlign="Center" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align:center; color:red"><b>ไม่มีข้อมูล !! ที่เข้ามาถึง ส่วน MIS HEAD</b> </div>
                    </EmptyDataTemplate>

                    <Columns>

                        <asp:TemplateField HeaderText="ID" ItemStyle-CssClass="f-s-13 text-left" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="14%">
                            <ItemTemplate>
                                <small>
                                <asp:Label ID="u0" runat="server"  visible = "false" Text='<%# Eval("u0_jo_idx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                    
                            </small>
                                <asp:HiddenField ID="nodehf" runat="server" Value='<%# Eval("m0_node_idx") %>' />
                                <asp:HiddenField ID="actorhf" runat="server" Value='<%# Eval("m0_actor_idx") %>' />
                                <asp:HiddenField ID="statehf" runat="server" Value='<%# Eval("id_statework") %>' />
                                <asp:HiddenField ID="log" runat="server" Value='<%# Eval("jo_idx_ref") %>' />
                                <asp:Label ID="dstart" runat="server" visible="false" Text='<%# Eval("day_start") %>' />
                                <asp:Label ID="dfinish" runat="server" visible="false" Text='<%# Eval("day_finish") %>' />
                            </ItemTemplate>

                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="TITLE" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="title" runat="server" Text='<%# Eval("title_jo") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="REQUIREMENT" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                            HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="req" runat="server" Text='<%# Eval("require_jo") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="statusmishead" runat="server" Text='<%# Eval("StatusDoc") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่เริ่มต้น" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                            HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="dst" runat="server" Text='<%# Eval("day_start") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่สิ้นสุด" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                            HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="dfn" runat="server" Text='<%# Eval("day_finish") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>






                        <asp:TemplateField HeaderText="รายละเอียด" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                            HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <div class="col-xs-6">
                                    <asp:LinkButton ID="detail" style="margin-left:68%; margin-top:2%" class="btn btn-info" runat="server" visible="true" CommandName="detail"
                                        data-toggle="tooltip" title="detail" OnCommand="btnuser" CommandArgument='<%# Eval("u0_jo_idx") + ";" + Eval("m0_node_idx") + ";" + Eval("m0_actor_idx") + ";" + Eval("id_statework")  + ";" + Eval("day_start") + ";" + Eval("day_finish") + ";" + Eval("jo_idx_ref")  %>'>
                                        <i class="glyphicon glyphicon-new-window"></i>
                                    </asp:LinkButton>
                                </div>

                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </asp:Panel>
        </asp:View>

        <asp:View id="View3" runat="server">

            <!--<asp:Label ID="oaklog" runat="server" CssClass="form-control" visible="false" Text='<%# Eval("oakja") %>' />-->

            <div class="col-xs-6">
                <asp:LinkButton ID="homy" style="margin-right:80%; margin-top:1%" class="btn btn-primary" runat="server" visible="true" CommandName="home"
                    data-toggle="tooltip" title="หน้าหลัก" OnCommand="btnuser">
                    <i class="glyphicon glyphicon-home"></i>
                </asp:LinkButton>

            </div>
            <br><br/><br>
            <div class="form-horizontal" role="form">

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัว USER : </strong></h3>
                    </div>
                    <div class="panel-body">

                        <asp:FormView ID="DetailUser" runat="server" OnDataBound="Detail_DataBound" Width="100%">


                            <ItemTemplate>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <asp:Label ID="Label2" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="หัวข้อความต้องการ : " />
                                        <div class="col-xs-3">
                                            <asp:Label ID="titlecode" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("title_jo") %>' />

                                        </div>

                                        <asp:Label ID="Label3" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รายละเอียดความต้องการ: " />
                                        <div class="col-xs-4">
                                            <asp:Label ID="reqcode" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("require_jo") %>' />

                                        </div>
                                    </div>
                                </div>

                            </ItemTemplate>
                        </asp:FormView>
                    </div>
                </div>
                <div class="panel panel-warning" style="color:darkred ; background:peachpuff">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; MIS HEAD:</strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-horizontal" role="form">

                            <div class="form-group">


                                <asp:Label ID="addstart" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่เริ่มงาน : " />
                                <div class="row">
                                    <div class="col-sm-10">
                                        <div class='input-group date from-date-datepickerexpire'>
                                            <asp:TextBox ID="startdate" runat="server" CssClass="form-control" Maxlengh="100%" style="width:300px" placeholder="กรุณาเลือกวันที่"></asp:TextBox>
                                            <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>

                                        </div>


                                        <asp:RequiredFieldValidator ID="requiredsdate" ValidationGroup="dategroup" runat="server" Display="Dynamic" ControlToValidate="startdate"
                                            Font-Size="0.87em" ForeColor="Red" ErrorMessage="*กรุณากรอกข้อมูลให้ครบ" />
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label ID="addfinal" CssClass="col-sm-2 control-label" runat="server" Text="ระบุวันที่สิ้นสุดงาน : " />
                            <div class="row">
                                <div class="col-sm-10">

                                    <div id="show_date" class='input-group date from-date-datepickerexpire' runat="server">
                                        <asp:TextBox ID="finaldate" runat="server" CssClass="form-control" placeholder="กรุณาเลือกวันที่" Maxlengh="100%" style="width:300px"></asp:TextBox>
                                        <span class="input-group-addon"><span data-icon-element="" class="fa fa-calendar"></span></span>
                                    </div>




                                    <asp:RequiredFieldValidator ID="requiredfdate" ValidationGroup="dategroup" runat="server" Display="Dynamic" ControlToValidate="finaldate"
                                        Font-Size="0.87em" ForeColor="Red" ErrorMessage="*กรุณากรอกข้อมูลให้ครบ" />


                                </div>
                            </div>
                        </div>

                        <!--<div class="form-group">
                            <asp:Label ID="ld" CssClass="col-sm-2 control-label" runat="server" Text="ระยะเวลาดำเนินงาน : " />

                            <div class="col-sm-3">
                                <asp:TextBox ID="lengthdate" runat="server" CssClass="form-control" Maxlengh="10%" style="width:50%" placeholder="/วัน"></asp:TextBox>
                            </div>

                        </div>
                        <div class="form-group">
                            <asp:Label ID="misheadla" CssClass="col-sm-2 control-label" runat="server" Text="ผู้เซ็นอนุมัติ : " />

                            <div class="col-sm-3">
                                <asp:TextBox ID="linecenmh" runat="server" CssClass="form-control" Maxlengh="10%" style="width:50%"></asp:TextBox>
                            </div>

                        </div>-->

                        <br><br/><br><br/>
                        <div class="row">
                            <div class="col-sm-4">
                                <asp:LinkButton ID="accept" style="margin-left:100%; margin-top:2px" class="btn btn-success" runat="server" visible="true"
                                    CommandName="accept" OnCommand="btnuser" Text="อนุมัติ" ValidationGroup="dategroup" OnClientClick="return confirm('คุณต้องการ อนุมัติใช่หรือไม่')"
                                />
                            </div>
                            <div class="col-sm-4">
                                <asp:LinkButton ID="decline" style="margin-left:10%; margin-top:2px" class="btn btn-danger" runat="server" visible="true"
                                    CommandName="closemishead" OnCommand="btnuser" Text="ไม่อนุมัติ" CommandArgument='<%# Eval("u0_jo_idx") %>'
                                    OnClientClick="return confirm('คุณต้องการ ไม่อนุมัติใช่หรือไม่')" />
                            </div>
                        </div>

                    </div>

                </div>
                <div ID="sd" class="alert alert-success" role="alert" visible="false" runat="server">
                    <strong>อนุมัติสำเร็จ !!!</strong>
                </div>
            </div>
            </div>
            <div class="w3-container">
                <div class="row">
                    <div class="col-xs-6">
                        <asp:LinkButton ID="btnback" style="margin-right:80%; margin-top:1%" class="btn btn-primary" runat="server" visible="true"
                            data-toggle="tooltip" title="back" CommandName="back" OnCommand="btnuser">
                            <i class="glyphicon glyphicon-arrow-left"></i>
                        </asp:LinkButton>
                    </div>
                </div>
            </div>
        </asp:View>
        <asp:View id="View4" runat="server">
            <div class="form-horizontal" role="form">
                <asp:TextBox ID="u0_idx" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("u0_jo_idx")%>' />


                <asp:Panel ID="userpanel2" visible="true" runat="server">
                    <div class="panel panel-info" style="color:darkslateblue; background:whitesmoke">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-user"></i><strong>&nbsp; ข้อมูลส่วนตัว USER : </strong></h3>
                        </div>
                        <div class="panel-body">
                            <asp:FormView ID="DetailUserpv" runat="server" DefaultMode="ReadOnly" OnDataBound="Detail_DataBound" Width="100%">
                                <ItemTemplate>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Labelnu0" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รหัสพนักงาน: " />
                                            <div class="col-xs-3">
                                                <asp:TextBox ID="ce" CssClass="form-control" runat="server" Text='<%# Eval("Codeemp") %>' Enabled="false" />

                                            </div>
                                            <asp:Label ID="Labelnu1" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ชื่อ-สกุล: " />
                                            <div class="col-xs-3">
                                                <asp:TextBox ID="nu" CssClass="form-control" runat="server" Text='<%# Eval("AdminMain") %>' Enabled="false" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">

                                            <asp:Label ID="Labelnu2" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="บริษัท: " />
                                            <div class="col-xs-3">
                                                <asp:TextBox ID="org" CssClass="form-control" runat="server" Text='<%# Eval("Organize") %>' Enabled="false" />

                                            </div>
                                            <asp:Label ID="Labelnu3" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ฝ่าย: " />
                                            <div class="col-xs-3">
                                                <asp:TextBox ID="dept" CssClass="form-control" runat="server" Text='<%# Eval("Deptname") %>' Enabled="false" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">

                                            <asp:Label ID="Labelnu4" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="แผนก: " />
                                            <div class="col-xs-3">
                                                <asp:TextBox ID="sec" CssClass="form-control" runat="server" Text='<%# Eval("Secname") %>' Enabled="false" />
                                            </div>
                                            <asp:Label ID="Labelnu5" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ตำแหน่ง: " />
                                            <div class="col-xs-3">
                                                <asp:TextBox ID="pos" CssClass="form-control" runat="server" Text='<%# Eval("Posname") %>' Enabled="false" />
                                            </div>
                                        </div>
                                    </div>

                                </ItemTemplate>
                            </asp:FormView>
                        </div>
                    </div>
                </asp:Panel>


                <asp:Panel ID="userpanel" visible="true" runat="server">
                    <div class="panel panel-info" style="color:darkslateblue; background:lightcyan">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="glyphicon glyphicon-folder-open"></i><strong>&nbsp; ข้อมูลเอกสาร USER : </strong></h3>
                        </div>
                        <div class="panel-body">

                            <asp:FormView ID="DetaileditUser" runat="server" DefaultMode="ReadOnly" OnDataBound="Detail_DataBound" Width="100%">


                                <ItemTemplate>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label22" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="หัวข้อความต้องการ : " />
                                            <div class="col-xs-3">
                                                <asp:Label ID="titlecode2" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("title_jo") %>' />

                                            </div>

                                            <asp:Label ID="Label32" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รายละเอียดความต้องการ: " />
                                            <div class="col-xs-4">
                                                <asp:Label ID="reqcode2" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("require_jo") %>' />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label42" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="ผู้อนุมัติ: " />
                                            <div class="col-xs-3">
                                                <asp:Label ID="actorcode2" CssClass="control-labelnotop col-xs-5" runat="server" Text='<%# Eval("name_actor") %>' />
                                            </div>


                                            <asp:Label ID="Label52" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="วันที่สร้าง: " />
                                            <div class="col-xs-4">
                                                <asp:Label ID="datecode2" CssClass="control-labelnotop col-xs-12" runat="server" Text='<%# Eval("day_created") %>' />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                           <div class="col-sm-8">
                                                <asp:LinkButton ID="btnedit" style="margin-left:90%; margin-top:2px" class="btn btn-warning" runat="server" visible="false"
                                                    CommandName="edit2" OnCommand="btnuser" data-toggle="tooltip" title="แก้ไข"
                                                    CommandArgument='<%# Eval("u0_jo_idx") %>'>
                                                    <i class="glyphicon glyphicon-pencil"></i>
                                                </asp:LinkButton>
                                           </div>
                                            <div class="col-sm-4">
                                                <asp:LinkButton ID="showlog" style="margin-right:80%; margin-top:2px" class="btn btn-info" runat="server" visible="true"
                                                    CommandName="showlog" OnCommand="btnuser" CommandArgument='<%# Eval("u0_jo_idx") %>'
                                                    data-toggle="tooltip" title="ประวัติการสร้าง/แก้ไข">
                                                    <i class="glyphicon glyphicon-eye-open"></i>
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <asp:Label ID="Label23" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="หัวข้อความต้องการ : " />
                                            <div class="col-xs-3">
                                                <asp:textbox runat="server" ID="titledit" required Text='<%# Eval("title_jo") %>' Enabled="false" style="width: 200px">
                                                </asp:textbox>
                                                <asp:RequiredFieldValidator ID="requiredtitledit" ValidationGroup="editUpdate" runat="server" Display="Dynamic" SetFocusOnError="true"
                                                    ControlToValidate="titledit" Font-Size="0.5em" ForeColor="Red" ErrorMessage="*กรุณากรอกข้อมูล"
                                                />

                                            </div>

                                            <asp:Label ID="Label33" CssClass="col-xs-2 control-labelnotop text_right" runat="server" Text="รายละเอียดความต้องการ: " />
                                            <div class="col-xs-4">
                                                <asp:textbox runat="server" ID="reqedit" TextMode="MultiLine" style="width: 500px ; height: 200px" Text='<%# Eval("require_jo") %>'>
                                                </asp:textbox>
                                                <asp:RequiredFieldValidator ID="requiredreqedit" ValidationGroup="editUpdate" runat="server" Display="Dynamic" SetFocusOnError="true"
                                                    ControlToValidate="reqedit" Font-Size="0.5em" ForeColor="Red" ErrorMessage="*กรุณากรอกข้อมูล"
                                                />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">
                                            <asp:LinkButton ID="truebut" style="margin-left:100%; margin-top:2.5%" class="btn btn-success" runat="server" visible="true"
                                                CommandName="Updateedit" OnCommand="btnuser" ValidationGroup="editUpdate" data-toggle="tooltip"
                                                title="update" CommandArgument='<%# Eval("u0_jo_idx") %>'>
                                                <i class="glyphicon glyphicon-ok"></i>
                                            </asp:LinkButton>



                                        </div>
                                        <div class="col-md-4">
                                            <asp:LinkButton ID="cmdcancel" style="margin-left:5%; margin-top:5%" class="btn btn-danger" runat="server" visible="true"
                                                data-toggle="tooltip" title="cancel" CommandName="cancel" OnCommand="btnuser">
                                                <i class="glyphicon glyphicon-remove"></i>
                                            </asp:LinkButton>
                                        </div>
                                    </div>

                                </EditItemTemplate>

                            </asp:FormView>
                        </div>
                    </div>
                    <br/><br/>
                </asp:Panel>

            </div>

            <asp:Panel ID="gridshowu1" visible="true" runat="server">
                <asp:GridView ID="u1grid" runat="server" AutoGenerateColumns="false" DataKeyNames="u1_jo_idx" CssClass="table table-striped table-bordered table-responsive col-md-12 footable"
                    HeaderStyle-CssClass="info" OnRowDataBound="Master_RowDataBound" AllowPaging="true" PageSize="10" AutoPostBack="False"
                    onpageindexchanging="Master_PageIndexChanging">
                    <PagerStyle CssClass="pageCustom" HorizontalAlign="Center" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align:center; color:red"><b>ไม่มีข้อมูล !! ส่วนรายละเอียดงาน</b> </div>
                    </EmptyDataTemplate>

                    <Columns>
                        <asp:TemplateField HeaderText="EMPLOYEE"     ItemStyle-CssClass="f-s-13 text-center"         HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="u1employee" runat="server" Text='<%# Eval("u1_emp_receive") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="หัวข้อรายละเอียดงาน" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="u1title" runat="server" Text='<%# Eval("u1_title") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField HeaderText="วันที่ส่งงาน" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="u1daysent" runat="server" Text='<%# Eval("u1_day_sent") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="วันที่รับงาน" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="u1dayre" runat="server" Text='<%# Eval("u1_day_receive") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                       
                    </Columns>
                </asp:GridView>
            </asp:Panel>






            <div class="row">
                <div class="col-xs-6">
                    <asp:LinkButton ID="btnback2" style="margin-right:80%; margin-top:1%" class="btn btn-primary" runat="server" visible="true"
                        data-toggle="tooltip" title="back" CommandName="back2" OnCommand="btnuser">
                        <i class="glyphicon glyphicon-arrow-left"></i>
                    </asp:LinkButton>
                </div>
            </div>


            <div class="col-lg-12" runat="server" id="div_log">
                <div id="opmodal" class="modal open" role="dialog">
                    <div class="modal-dialog" style="width:70%">
                        <!--Modal content-->
                        <div class="modal-content" style="background:whitesmoke">
                            <div class="modal-header" style="color:#ffffff ;background:lightseagreen">

                                <h4 class="modal-title">ประวัติการแก้ไขใบ joborder</h4>
                            </div>
                            <div class="modal-body">

                                <div class="panel-body">
                                    <div class="form-horizontal" role="form">
                                        <asp:Label ID="u0log_idx" runat="server" Visible="false" Text='<%# Eval("u0_jo_idx") %>' />
                                        <asp:Repeater ID="rplog" runat="server">
                                            <HeaderTemplate>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label"><small>วัน / เวลา</small></label>
                                                    <label class="col-sm-3 control-label"><small>ผู้ดำเนินการ(from_ac)</small></label>
                                                    <label class="col-sm-2 control-label"><small>ดำเนินการ(from_node)</small></label>
                                                    <label class="col-sm-2 control-label"><small>ผลการดำเนินการ(status)</small></label>
                                                </div>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <div class="row">
                                                    <div class="col-sm-3">
                                                        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small><%#Eval("day_created")%></small></span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <span><small>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%# Eval("fromac")%></small></span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <span><small><%# Eval("node_name") %></small></span>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <span><small><%# Eval("statenode_name") %></small></span>
                                                    </div>

                                                </div>
                                            </ItemTemplate>

                                        </asp:Repeater>
                                        <div class="form-group">
                                            <div class="col-sm-2 col-sm-offset-10">
                                                <asp:LinkButton ID="Cancel" class="btn btn-danger" data-toggle="tooltip" title="Close" runat="server" data-dismiss="modal"
                                                    CommandName="Cancel"><i class="glyphicon glyphicon-remove"></i></asp:LinkButton>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>








        </asp:View>

        <asp:View id="ViewHome" runat="server">
            <div class="row">
                <div class="col-xs-6">
                    <asp:LinkButton ID="home1" style="margin-right:80%; margin-top:1%" class="btn btn-primary" runat="server" visible="true"
                        CommandName="home" data-toggle="tooltip" title="หน้าหลัก" OnCommand="btnuser">
                        <i class="glyphicon glyphicon-home"></i>
                    </asp:LinkButton>
                </div>
            </div>
            <br></br>
            <asp:Panel ID="gridhome" visible="false" runat="server">
                <asp:GridView ID="homegrid" runat="server" AutoGenerateColumns="false" DataKeyNames="u0_jo_idx" CssClass="table table-striped table-bordered table-responsive col-md-12 footable"
                    HeaderStyle-CssClass="info" OnRowDataBound="Master_RowDataBound" AllowPaging="true" PageSize="10" AutoPostBack="False"
                    onpageindexchanging="Master_PageIndexChanging" OnRowEditing="Master_RowEditing" OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnRowUpdating="Master_RowUpdating">
                    <PagerStyle CssClass="pageCustom" HorizontalAlign="Center" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center;">Data Cannot Be Found</div>
                    </EmptyDataTemplate>

                    <Columns>
                        <asp:TemplateField HeaderText="เลขใบกำกับเอกสาร" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                            HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="gencode" runat="server" Text='<%# Eval("no_invoice") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ID" ItemStyle-CssClass="f-s-13 text-left" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="14%"
                            visible="false">
                            <ItemTemplate>
                                <small>
                                <asp:Label ID="u0" runat="server"  visible ="false" Text='<%# Eval("u0_jo_idx") %>' />
                                    <%# (Container.DataItemIndex +1) %>
                                    
                        </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="TITLE" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center" HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="title" runat="server" Text='<%# Eval("title_jo") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="REQUIREMENT" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                            HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="req" runat="server" Text='<%# Eval("require_jo") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="DATE/MONTH/YEAR" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                            HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="dat" runat="server" Text='<%# Eval("day_created") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="STATUS" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                            HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="_actorm0" runat="server" visible="false" Text='<%# Eval("m0_actor_idx") %>' />
                                <asp:Label ID="_nodem0" runat="server" visible="false" Text='<%# Eval("m0_node_idx") %>' />
                                <asp:Label ID="_idlog" runat="server" visible="false" Text='<%# Eval("jo_idx_ref") %>' />
                                <asp:Label ID="_idstate" runat="server" visible="false" Text='<%# Eval("id_statework") %>' />


                                <asp:Label ID="showact" runat="server" Text='<%# Eval("StatusDoc") %>' />
                                <!--<asp:Label ID="act" runat="server" Text='<%# Eval("name_actor") %>' />-->
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="วันที่เริ่มงาน" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                            HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="datst" runat="server" Text='<%# Eval("day_start") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="วันที่สิ้นสุดงาน" ItemStyle-CssClass="f-s-13 text-center" HeaderStyle-CssClass="f-s-13 text-center"
                            HeaderStyle-Width="15%">
                            <ItemTemplate>
                                <asp:Label ID="datfn" runat="server" Text='<%# Eval("day_finish") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>
        </asp:View>

        </asp:MultiView>


    </asp:Content>