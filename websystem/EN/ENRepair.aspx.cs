﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Helpers;
using System.Drawing;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using DotNet.Highcharts.Enums;

public partial class websystem_EN_ENRepair : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    service_mail servicemail = new service_mail();

    function_tool _funcTool = new function_tool();
    function_dmu _func_dmu = new function_dmu();
    data_employee _dtEmployee = new data_employee();
    DataMachine _dtmachine = new DataMachine();
    data_en_planning _dtenplan = new data_en_planning();
    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _ret_val = "";
    string _localJson = "";
    public string checkfile;
    int secit = 210;
    int depten = 11;
    string nameupload;
    static string keyenrepair = ConfigurationManager.AppSettings["keyenrepair"];
    string linkquest = "http://mas.taokaenoi.co.th/en-planning/";//"http://localhost/mas.taokaenoi.co.th/en-planning/";
    #endregion

    #region URL

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];

    #region Master Data
    static string urlSelect_TypeMachineList = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeMachineList"];
    static string urlInsert_TypeMachineList = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_TypeMachineList"];
    static string urlDelete_TypeMachineList = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_TypeMachineList"];
    static string urlSelect_DowntimeList = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_DowntimeList"];
    static string urlInsert_StatusList = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_StatusList"];
    static string urlDelete_StatuseList = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_StatuseList"];
    static string urlSelect_StatusListMACHINE = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_StatusListMACHINE"];

    #endregion

    #region Select For Add System
    static string urlSelect_TypeMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_TypeMachine"];
    static string urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Location"];
    static string urlSelect_Building = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Building"];
    static string urlSelect_Room = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Room"];
    static string urlSelect_NameMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_NameMachine"];
    static string urlSelect_Index = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Index"];
    static string urlSelect_DetailUserIndex = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_DetailUserIndex"];
    static string urlSelect_Comment = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Comment"];
    static string urlSelect_Admin = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Admin"];
    static string urlSelect_SearchIndex = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_SearchIndex"];
    static string urlSelect_StatusSearch = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_StatusSearch"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string urlSelect_Machine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Machine"];
    static string _urlSelect_Time_Machine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Time_Machine"];
    static string _urlSelect_GroupCodeMachine = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_GroupCodeMachine"];
    static string _urlSelect_Group = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Group_Machine"];
    static string _urlSelectDoc_LenghtDate = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDoc_LenghtDate"];
    static string _urlSelectDoc_MachineEN = _serviceUrl + ConfigurationManager.AppSettings["urlSelectDoc_MachineEN"];




    #endregion

    #region Insert and Update For System
    static string urlInsert_System = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_System"];
    static string urlUploadfile_System = _serviceUrl + ConfigurationManager.AppSettings["urlUploadfile_System"];
    static string urlAdminGetJob_System = _serviceUrl + ConfigurationManager.AppSettings["urlAdminGetJob_System"];
    static string urlInsertComment_System = _serviceUrl + ConfigurationManager.AppSettings["urlInsertComment_System"];
    static string urlUpdateComment_System = _serviceUrl + ConfigurationManager.AppSettings["urlUpdateComment_System"];
    static string urlSelect_History = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_History"];

    #endregion

    #region Report

    static string urlReport_Table = _serviceUrl + ConfigurationManager.AppSettings["urlReport_Table"];
    static string urlReport_ExportData = _serviceUrl + ConfigurationManager.AppSettings["urlReport_ExportData"];

    #endregion

    #region Questionair
    static string urlSelect_Question = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Question"];
    static string urlInsert_Questionair = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Questionair"];
    static string urlInsert_Questionair_Sub = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Questionair_Sub"];


    #endregion


    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            select_empIdx_present();
            litoindex.Attributes.Add("class", "active");
            Select_Index();
            Page.Form.Attributes.Add("enctype", "multipart/form-data");
            SetViewIndex();


        }


        if (int.Parse(ViewState["Sec_idx"].ToString()) == secit)
        {
            litoreport.Visible = true;
            litoMaster.Visible = true;
            listatus.Visible = true;
        }
        else if (int.Parse(ViewState["rdept_idx"].ToString()) == depten || int.Parse(ViewState["rdept_idx"].ToString()) == 134)
        {
            litoreport.Visible = true;
            litoMaster.Visible = true;
            listatus.Visible = false;
        }
        else
        {
            litoreport.Visible = false;
            litoMaster.Visible = false;
        }

        

        linkBtnTrigger(lblindex);
        linkBtnTrigger(lblinsert);
        linkBtnTrigger(lblreport);
        linkBtnTrigger(lblreportquestion);
        linkBtnTrigger(lblmastertypemachine);
        linkBtnTrigger(lblmasterstatus);
        linkBtnTrigger(lblmanual);
        linkBtnTrigger(lblflow);
        linkBtnTrigger(btnlink);
        linkBtnTrigger(btnsearch_from);
        linkBtnTrigger(btnclearsearch_from);
        linkBtnTrigger(btnprint_plan);


    }
    #endregion

    #region CallService


    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected DataMachine callServicePostENRepair(string _cmdUrl, DataMachine _dtmachine)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtmachine);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtmachine = (DataMachine)_funcTool.convertJsonToObject(typeof(DataMachine), _localJson);




        return _dtmachine;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dtEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //  convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_en_planning callServicePostENPlanning(string _cmdUrl, data_en_planning _dtenplan)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtenplan);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtenplan = (data_en_planning)_funcTool.convertJsonToObject(typeof(data_en_planning), _localJson);

        return _dtenplan;
    }

    #endregion

    #region Select
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;


    }

    protected void SelectMaster_TypeMachine()
    {


        _dtmachine = new DataMachine();
        _dtmachine.BoxTypeMachine = new TypeMachineDetail[1];

        _dtmachine = callServicePostENRepair(urlSelect_TypeMachineList, _dtmachine);
        setGridData(GvMasterTypeMachine, _dtmachine.BoxTypeMachine);
    }

    protected void SelectMaster_Status()
    {


        _dtmachine = new DataMachine();
        _dtmachine.BoxStatusList = new StatusListDetail[1];

        _dtmachine = callServicePostENRepair(urlSelect_StatusListMACHINE, _dtmachine);
        setGridData(GvMasterStatus, _dtmachine.BoxStatusList);
    }

    protected void Select_Admin(DropDownList ddlName)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];

        _dtmachine = callServicePostENRepair(urlSelect_Admin, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxRepairMachine, "FullNameTH", "EmpIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกผู้ปิดงาน...", "0"));
    }

    protected void Select_Status(DropDownList ddlName)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxStatusList = new StatusListDetail[1];

        _dtmachine = callServicePostENRepair(urlSelect_StatusSearch, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxStatusList, "status_name", "stidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสถานะดำเนินการ...", "0"));
    }

    protected void Select_Downtime(DropDownList ddlName)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxStatusList = new StatusListDetail[1];

        _dtmachine = callServicePostENRepair(urlSelect_DowntimeList, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxStatusList, "downtime_name", "dtidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกสถานะดาวไทม์...", "0"));
    }

    protected void Select_TypeMachine(DropDownList ddlName)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxTypeMachine = new TypeMachineDetail[1];

        _dtmachine = callServicePostENRepair(urlSelect_TypeMachine, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxTypeMachine, "NameTH", "TmcIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกประเภทเครื่องจักร...", "0"));
    }

    protected void Select_Location(DropDownList ddlName)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];

        _dtmachine = callServicePostENRepair(urlSelect_Location, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxRepairMachine, "LocName", "LocIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกสถานที่แจ้งซ่อม...", "0"));
    }

    protected void Select_Building(DropDownList ddlName, int LocIDX)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail building = new RepairMachineDetail();

        building.LocIDX = LocIDX;
        _dtmachine.BoxRepairMachine[0] = building;

        _dtmachine = callServicePostENRepair(urlSelect_Building, _dtmachine);

        setDdlData(ddlName, _dtmachine.BoxRepairMachine, "BuildingName", "BuildingIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกอาคาร...", "0"));
    }

    protected void Select_Room(DropDownList ddlName, int BuildIDX)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail room = new RepairMachineDetail();

        room.BuildingIDX = BuildIDX;
        _dtmachine.BoxRepairMachine[0] = room;

        _dtmachine = callServicePostENRepair(urlSelect_Room, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxRepairMachine, "Roomname", "RoomIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกห้อง...", "0"));
    }

    protected void Select_NameMachine(DropDownList ddlName, int BuildIDX, int RoomIDX, int TMCIDX)
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail machine = new RepairMachineDetail();

        machine.BuildingIDX = BuildIDX;
        machine.RoomIDX = RoomIDX;
        machine.TmcIDX = TMCIDX;
        _dtmachine.BoxRepairMachine[0] = machine;

        _dtmachine = callServicePostENRepair(urlSelect_NameMachine, _dtmachine);
        setDdlData(ddlName, _dtmachine.BoxRepairMachine, "NameMachine", "MCIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกชื่ออุปกรณ์...", "0"));
    }


    protected void Select_Index()
    {

        if (ViewState["BoxSearchIndex"] == null)
        {
            _dtmachine = new DataMachine();
            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));
            _dtmachine = callServicePostENRepair(urlSelect_Index, _dtmachine);
            setGridData(GvViewENRepair, _dtmachine.BoxRepairMachine);
        }
        else
        {
            _dtmachine = callServicePostENRepair(urlSelect_SearchIndex, (DataMachine)ViewState["BoxSearchIndex"]);
            setGridData(GvViewENRepair, _dtmachine.BoxRepairMachine);
        }


    }

    protected void Select_History()
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail history = new RepairMachineDetail();

        history.MCIDX = int.Parse(ViewState["IDX"].ToString());

        _dtmachine.BoxRepairMachine[0] = history;

        _dtmachine = callServicePostENRepair(urlSelect_History, _dtmachine);
        setGridData(GvHistory, _dtmachine.BoxRepairMachine);
    }

    protected void Select_DetailUserIndex()
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail selectdetailuser = new RepairMachineDetail();

        selectdetailuser.RPIDX = int.Parse(ViewState["IDX"].ToString());

        _dtmachine.BoxRepairMachine[0] = selectdetailuser;

        _dtmachine = callServicePostENRepair(urlSelect_DetailUserIndex, _dtmachine);
        setFormViewData(FvDetailUserRepair, _dtmachine.BoxRepairMachine);
        setFormViewData(FvDetailRepair, _dtmachine.BoxRepairMachine);

        ViewState["RECode"] = _dtmachine.BoxRepairMachine[0].RECode;
        ViewState["system"] = _dtmachine.BoxRepairMachine[0].SysIDX;



    }



    protected void Select_Comment()
    {

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail selectcomment = new RepairMachineDetail();

        selectcomment.RPIDX = int.Parse(ViewState["IDX"].ToString());

        _dtmachine.BoxRepairMachine[0] = selectcomment;

        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));

        _dtmachine = callServicePostENRepair(urlSelect_Comment, _dtmachine);
        setGridData(GvComment, _dtmachine.BoxRepairMachine);
    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dtEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dtEmployee.organization_list[0] = _orgList;

        _dtEmployee = callServicePostEmployee(_urlGetOrganizationList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร...", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dtEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dtEmployee.department_list[0] = _deptList;

        _dtEmployee = callServicePostEmployee(_urlGetDepartmentList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกฝ่าย...", "0"));
    }

    protected void Select_ReportTable()
    {
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail _repairreport = new RepairMachineDetail();

        _repairreport.STAppIDX = int.Parse(ddlReportStatus.SelectedValue);
        _repairreport.OrgIDX = int.Parse(ddlReportOrg.SelectedValue);
        _repairreport.RDeptIDX = int.Parse(ddlReporthDep.SelectedValue);
        _repairreport.EmpCode = txtempname.Text;
        _repairreport.IFSearch = int.Parse(ddlTypeSearchDate_Report.SelectedValue);
        _repairreport.IFSearchbetween = int.Parse(ddlSearchDate_Report.SelectedValue);
        _repairreport.DategetJobSearch = AddStartDate_Report.Text;
        _repairreport.DatecloseJobSearch = AddEndDate_Report.Text;
        _repairreport.LocIDX = int.Parse(ddllocate_report.SelectedValue);
        _repairreport.BuildingIDX = int.Parse(ddlbuilding_report.SelectedValue);
        _repairreport.RoomIDX = int.Parse(ddlroom_report.SelectedValue);
        _repairreport.AdminReciveEmpIDX = int.Parse(ddladminreceive_report.SelectedValue);
        _repairreport.AdminEmpIDX = int.Parse(ddladminaccept_report.SelectedValue);
        _repairreport.TmcIDX = int.Parse(ddltype_report.SelectedValue);
        _repairreport.MCIDX = int.Parse(ddlmachine_report.SelectedValue);
        _repairreport.SysIDX = int.Parse(ddlSysIDX.SelectedValue);

        _dtmachine.BoxRepairMachine[0] = _repairreport;

        _dtmachine = callServicePostENRepair(urlReport_Table, _dtmachine);
        setGridData(GvReport, _dtmachine.BoxRepairMachine);
    }

    protected void Select_Questionnair()
    {
        _dtmachine = new DataMachine();
        _dtmachine.BoxQuestionMachine = new QuestionDetail[1];

        _dtmachine = callServicePostENRepair(urlSelect_Question, _dtmachine);
        setGridData(GvQuestionEN, _dtmachine.BoxQuestionMachine);

        ViewState["TIDX"] = _dtmachine.BoxQuestionMachine[0].TIDX;
        ViewState["TFBIDX"] = _dtmachine.BoxQuestionMachine[0].TFBIDX;
        ViewState["TFBIDX2"] = _dtmachine.BoxQuestionMachine[0].TFBIDX2;
    }


    #endregion

    #region PMMS
    protected void select_time(DropDownList ddlName)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_TimeList = new Timing_Detail[1];
        Timing_Detail time = new Timing_Detail();

        _dtenplan.BoxEN_TimeList[0] = time;

        _dtenplan = callServicePostENPlanning(_urlSelect_Time_Machine, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_TimeList, "time_desc", "m0tidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกความถี่ในการดำเนินการ...", "0"));

    }

    protected void SelectDoc_LenghtDate(DropDownList ddlName, int TmcIDX, int TCIDX, int GCIDX, int LocIDX, int Building, int RoomIDX, int m0tidx)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_u0docform = new u0docform_Detail[1];
        u0docform_Detail time = new u0docform_Detail();

        time.TmcIDX = TmcIDX;
        time.TCIDX = TCIDX;
        time.GCIDX = GCIDX;
        time.LocIDX = LocIDX;
        time.BuildingIDX = Building;
        time.RoomIDX = RoomIDX;
        time.m0tidx = m0tidx;

        _dtenplan.BoxEN_u0docform[0] = time;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtenplan));

        _dtenplan = callServicePostENPlanning(_urlSelectDoc_LenghtDate, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_u0docform, "datestart", "u1idx_planning");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกช่วงวันที่ดำเนินการ...", "0"));

    }

    protected void select_typecodemachine(DropDownList ddlName, int tmcidx)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_TypeCodeList = new TypeCode_Detail[1];
        TypeCode_Detail groupmachine = new TypeCode_Detail();
        groupmachine.TmcIDX = tmcidx;

        _dtenplan.BoxEN_TypeCodeList[0] = groupmachine;

        _dtenplan = callServicePostENPlanning(_urlSelect_Group, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_TypeCodeList, "NameTypecode", "TCIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกรหัสกลุ่มเครื่องจักร...", "0"));

    }

    protected void select_groupmachine(DropDownList ddlName, int tcidx)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_GroupCodeList = new GroupCode_Detail[1];
        GroupCode_Detail groupmachine = new GroupCode_Detail();
        groupmachine.TCIDX = tcidx;

        _dtenplan.BoxEN_GroupCodeList[0] = groupmachine;

        _dtenplan = callServicePostENPlanning(_urlSelect_GroupCodeMachine, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_GroupCodeList, "NameTypecode", "GCIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกรหัสกลุ่ม...", "0"));

    }

    protected void SelectDoc_MachineEN(DropDownList ddlName, int TmcIDX, int TCIDX, int GCIDX, int LocIDX, int Building, int RoomIDX, int m0tidx)
    {
        _dtenplan = new data_en_planning();

        _dtenplan.BoxEN_u0docform = new u0docform_Detail[1];
        u0docform_Detail time = new u0docform_Detail();

        time.TmcIDX = TmcIDX;
        time.TCIDX = TCIDX;
        time.GCIDX = GCIDX;
        time.LocIDX = LocIDX;
        time.BuildingIDX = Building;
        time.RoomIDX = RoomIDX;
        time.m0tidx = m0tidx;

        _dtenplan.BoxEN_u0docform[0] = time;

        //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtenplan));

        _dtenplan = callServicePostENPlanning(_urlSelectDoc_MachineEN, _dtenplan);
        setDdlData(ddlName, _dtenplan.BoxEN_u0docform, "MCCode", "MCIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกชื่ออุปกรณ์...", "0"));

    }



    #endregion

    #region Select Chart

    protected void SelectChartLV1()
    {
        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail lv2 = new RepairMachineDetail();

        lv2.LocIDX = int.Parse(ddllocate_chart.SelectedValue);
        lv2.BuildingIDX = int.Parse(ddlbuilding_chart.SelectedValue);
        lv2.IFSearchbetween = int.Parse(ddlSearchDate_Chart.SelectedValue);
        lv2.DC_Month = ddlmonth.SelectedValue;
        lv2.DC_YEAR = ddlyear.SelectedValue;
        lv2.SysIDX = int.Parse(ddlSysIDX_chart.SelectedValue);

        _dtmachine.BoxRepairMachine[0] = lv2;
        //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));
        _local_xml = serviceexcute.actionExec("conn_EN", "DataMachine", "service_enrepair", _dtmachine, 219);
        _dtmachine = (DataMachine)_funcTool.convertXmlToObject(typeof(DataMachine), _local_xml);

        string sLabel = getSysName(int.Parse(ddlSysIDX_chart.SelectedValue));
        if (_dtmachine.ReturnCode == "0")
        {
            int count = _dtmachine.BoxRepairMachine.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;

            foreach (var data in _dtmachine.BoxRepairMachine)
            {
                lv1code[i] = data.DateNotify.ToString();
                lv1count[i] = data.countbuild.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "จำนวนครั้ง" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "วันที่ทำการ" + sLabel,
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();


            DataTable dt = new DataTable();
            dt.Columns.Add("วันที่ทำการ" + sLabel);
            dt.Columns.Add("จำนวนครั้ง");

            for (i = 0; i < lv1code.Length; i++)
            {
                DataRow row = dt.NewRow();
                row[0] = lv1code[i];
                row[1] = lv1count[i];
                dt.Rows.Add(row);


            }


            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
            GridView1.DataSource = null;
            GridView1.DataBind();
        }


    }

    protected void SelectChartLV2()
    {
        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail lv2 = new RepairMachineDetail();

        lv2.LocIDX = int.Parse(ddllocate_chart.SelectedValue);
        lv2.BuildingIDX = int.Parse(ddlbuilding_chart.SelectedValue);
        lv2.IFSearchbetween = int.Parse(ddlSearchDate_Chart.SelectedValue);
        lv2.DategetJobSearch = AddStartdate_Chart.Text;
        lv2.DatecloseJobSearch = AddEndDate_Chart.Text;
        lv2.SysIDX = int.Parse(ddlSysIDX_chart.SelectedValue);

        _dtmachine.BoxRepairMachine[0] = lv2;
        //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));
        _local_xml = serviceexcute.actionExec("conn_EN", "DataMachine", "service_enrepair", _dtmachine, 218);
        _dtmachine = (DataMachine)_funcTool.convertXmlToObject(typeof(DataMachine), _local_xml);

        string sLabel = getSysName(int.Parse(ddlSysIDX_chart.SelectedValue));
        if (_dtmachine.ReturnCode == "0")
        {
            int count = _dtmachine.BoxRepairMachine.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;

            foreach (var data in _dtmachine.BoxRepairMachine)
            {
                lv1code[i] = data.MCNameTH.ToString();
                lv1count[i] = data.countbuild.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "จำนวนครั้ง" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "รายการเครื่องจักร" + sLabel,
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();


            DataTable dt = new DataTable();
            dt.Columns.Add("รายการเครื่องจักร" + sLabel);
            dt.Columns.Add("จำนวนครั้ง");

            for (i = 0; i < lv1code.Length; i++)
            {
                DataRow row = dt.NewRow();
                row[0] = lv1code[i];
                row[1] = lv1count[i];
                dt.Rows.Add(row);


            }


            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
            GridView1.DataSource = null;
            GridView1.DataBind();
        }


    }

    protected void SelectChartLV3()
    {
        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail lv2 = new RepairMachineDetail();

        lv2.LocIDX = int.Parse(ddllocate_chart.SelectedValue);
        lv2.IFSearchbetween = int.Parse(ddlSearchDate_Chart.SelectedValue);
        lv2.DategetJobSearch = AddStartdate_Chart.Text;
        lv2.DatecloseJobSearch = AddEndDate_Chart.Text;
        lv2.SysIDX = int.Parse(ddlSysIDX_chart.SelectedValue);

        _dtmachine.BoxRepairMachine[0] = lv2;
        //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));
        _local_xml = serviceexcute.actionExec("conn_EN", "DataMachine", "service_enrepair", _dtmachine, 220);
        _dtmachine = (DataMachine)_funcTool.convertXmlToObject(typeof(DataMachine), _local_xml);

        string sLabel = getSysName(int.Parse(ddlSysIDX_chart.SelectedValue));
        if (_dtmachine.ReturnCode == "0")
        {
            int count = _dtmachine.BoxRepairMachine.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;

            foreach (var data in _dtmachine.BoxRepairMachine)
            {
                lv1code[i] = data.BuildingName.ToString();
                lv1count[i] = data.countbuild.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "จำนวนครั้ง" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "อาคาร" + sLabel,
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();

            DataTable dt = new DataTable();
            dt.Columns.Add("อาคาร" + sLabel);
            dt.Columns.Add("จำนวนครั้ง");
            for (i = 0; i < lv1code.Length; i++)
            {
                DataRow row = dt.NewRow();
                row[0] = lv1code[i];
                row[1] = lv1count[i];
                dt.Rows.Add(row);


            }


            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";

            GridView1.DataSource = null;
            GridView1.DataBind();
        }


    }

    protected void SelectChartLV4()
    {
        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail lv2 = new RepairMachineDetail();

        lv2.LocIDX = int.Parse(ddllocate_chart.SelectedValue);
        lv2.BuildingIDX = int.Parse(ddlbuilding_chart.SelectedValue);
        lv2.IFSearchbetween = int.Parse(ddlSearchDate_Chart.SelectedValue);
        lv2.DC_YEAR = ddlyear.SelectedValue;
        lv2.DC_Month = ddlmonth.SelectedValue;
        lv2.SysIDX = int.Parse(ddlSysIDX_chart.SelectedValue);

        _dtmachine.BoxRepairMachine[0] = lv2;
        //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));
        _local_xml = serviceexcute.actionExec("conn_EN", "DataMachine", "service_enrepair", _dtmachine, 221);
        _dtmachine = (DataMachine)_funcTool.convertXmlToObject(typeof(DataMachine), _local_xml);

        string sLabel = getSysName(int.Parse(ddlSysIDX_chart.SelectedValue));
        if (_dtmachine.ReturnCode == "0")
        {
            int count = _dtmachine.BoxRepairMachine.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            int i = 0;

            foreach (var data in _dtmachine.BoxRepairMachine)
            {
                lv1code[i] = data.DateNotify.ToString();
                lv1count[i] = data.countbuild.ToString();

                for (int ii = 0; ii < lv1code[i].Length; ii++)
                {
                    switch (lv1code[i])
                    {
                        case "1":
                            lv1code[i] = "มกราคม";

                            break;
                        case "2":
                            lv1code[i] = "กุมภาพันธ์";

                            break;
                        case "3":
                            lv1code[i] = "มีนาคม";

                            break;
                        case "4":
                            lv1code[i] = "เมษายน";

                            break;
                        case "5":
                            lv1code[i] = "พฤษภาคม";

                            break;
                        case "6":
                            lv1code[i] = "มิถุนายน";

                            break;
                        case "7":
                            lv1code[i] = "กรกฎาคม";

                            break;
                        case "8":
                            lv1code[i] = "สิงหาคม";

                            break;
                        case "9":
                            lv1code[i] = "กันยายน";

                            break;
                        case "10":
                            lv1code[i] = "ตุลาคม";

                            break;
                        case "11":
                            lv1code[i] = "พฤศจิกายน";

                            break;
                        case "12":
                            lv1code[i] = "ธันวาคม";

                            break;
                    }
                }

                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "จำนวนครั้ง" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "เดือนที่" + sLabel,
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();

            DataTable dt = new DataTable();
            dt.Columns.Add("เดือนที่" + sLabel);
            dt.Columns.Add("จำนวนครั้ง");
            for (i = 0; i < lv1code.Length; i++)
            {
                DataRow row = dt.NewRow();
                row[0] = lv1code[i];
                row[1] = lv1count[i];
                dt.Rows.Add(row);


            }


            GridView1.DataSource = dt;
            GridView1.DataBind();

        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";
            GridView1.DataSource = null;
            GridView1.DataBind();
        }


    }

    protected void SelectChartLV5()
    {
        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail lv2 = new RepairMachineDetail();

        lv2.LocIDX = int.Parse(ddllocate_chart.SelectedValue);
        lv2.IFSearchbetween = int.Parse(ddlSearchDate_Chart.SelectedValue);
        lv2.DategetJobSearch = AddStartdate_Chart.Text;
        lv2.DatecloseJobSearch = AddEndDate_Chart.Text;
        lv2.SysIDX = int.Parse(ddlSysIDX_chart.SelectedValue);

        _dtmachine.BoxRepairMachine[0] = lv2;
        //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));
        _local_xml = serviceexcute.actionExec("conn_EN", "DataMachine", "service_enrepair", _dtmachine, 222);
        _dtmachine = (DataMachine)_funcTool.convertXmlToObject(typeof(DataMachine), _local_xml);

        string sLabel = getSysName(int.Parse(ddlSysIDX_chart.SelectedValue));
        if (_dtmachine.ReturnCode == "0")
        {
            int count = _dtmachine.BoxRepairMachine.Length;
            string[] lv1code = new string[count];
            object[] lv1count = new object[count];
            object[] lv1downtime = new object[count];
            int i = 0;

            foreach (var data in _dtmachine.BoxRepairMachine)
            {
                lv1code[i] = data.BuildingName.ToString();
                lv1count[i] = data.countbuild.ToString();
                lv1downtime[i] = data.downtime.ToString();


                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "จำนวน Downtimes" } } });
            chart.SetXAxis(new XAxis { Categories = lv1code });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "อาคาร" + sLabel,
                    Data = new Data(lv1count)
                }
            );
            litReportChart.Text = chart.ToHtmlString();

            DataTable dt = new DataTable();
            dt.Columns.Add("อาคาร" + sLabel);
            dt.Columns.Add("จำนวน Downtimes");
            for (i = 0; i < lv1code.Length; i++)
            {
                DataRow row = dt.NewRow();
                row[0] = lv1code[i];
                row[1] = lv1downtime[i];
                dt.Rows.Add(row);


            }


            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        else
        {
            litReportChart.Text = "<p class='bg-danger'>No result.</p>";

            GridView1.DataSource = null;
            GridView1.DataBind();
        }


    }
    #endregion

    #region Insert AND Update For System

    protected void InsertMaster_TypeMachine()
    {
        _dtmachine = new DataMachine();

        _dtmachine.BoxTypeMachine = new TypeMachineDetail[1];
        TypeMachineDetail insertmaster = new TypeMachineDetail();



        insertmaster.CodeTM = txtaddcode.Text;
        insertmaster.NameTH = txtaddnameth.Text;
        insertmaster.NameEN = txtaddnameen.Text;
        insertmaster.TStatus = int.Parse(ddlStatus.SelectedValue);
        insertmaster.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dtmachine.BoxTypeMachine[0] = insertmaster;

        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));
        _dtmachine = callServicePostENRepair(urlInsert_TypeMachineList, _dtmachine);

        if (_dtmachine.ReturnCode == "0")
        {
            setGridData(GvMasterTypeMachine, _dtmachine.BoxTypeMachine);
        }
        else
        {
            setError(_dtmachine.ReturnCode.ToString() + " - " + _dtmachine.ReturnMsg);
        }

    }

    protected void InsertMaster_Status()
    {
        _dtmachine = new DataMachine();

        _dtmachine.BoxStatusList = new StatusListDetail[1];
        StatusListDetail insertmaster = new StatusListDetail();



        insertmaster.status_name = txtaddstatusname.Text;
        insertmaster.dtidx = int.Parse(ddl_downtime.SelectedValue);
        insertmaster.status_state = int.Parse(ddlStatus_master.SelectedValue);
        insertmaster.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dtmachine.BoxStatusList[0] = insertmaster;

        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));
        _dtmachine = callServicePostENRepair(urlInsert_StatusList, _dtmachine);

        if (_dtmachine.ReturnCode == "0")
        {
            setGridData(GvMasterStatus, _dtmachine.BoxStatusList);
        }
        else
        {
            setError(_dtmachine.ReturnCode.ToString() + " - " + _dtmachine.ReturnMsg);
        }

    }

    protected void Insert_Repair()
    {
        DropDownList ddltype = (DropDownList)FvInsertRepair.FindControl("ddltype");
        DropDownList ddllocate = (DropDownList)FvInsertRepair.FindControl("ddllocate");
        DropDownList ddlbuilding = (DropDownList)FvInsertRepair.FindControl("ddlbuilding");
        DropDownList ddlroom = (DropDownList)FvInsertRepair.FindControl("ddlroom");
        DropDownList ddlmachine = (DropDownList)FvInsertRepair.FindControl("ddlmachine");
        DropDownList ddlmachine_pm = (DropDownList)FvInsertRepair.FindControl("ddlmachine_pm");
        DropDownList ddlsystem = (DropDownList)FvInsertRepair.FindControl("ddlsystem");
        DropDownList ddldate = (DropDownList)FvInsertRepair.FindControl("ddldate");

        TextBox AddStartdate = (TextBox)FvInsertRepair.FindControl("AddStartdate");
        TextBox txtremark = (TextBox)FvInsertRepair.FindControl("txtremark");

        HttpFileCollection hfcit2 = Request.Files;

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail insert = new RepairMachineDetail();

        insert.TmcIDX = int.Parse(ddltype.SelectedValue);
        insert.LocIDX = int.Parse(ddllocate.SelectedValue);
        insert.BuildingIDX = int.Parse(ddlbuilding.SelectedValue);
        insert.RoomIDX = int.Parse(ddlroom.SelectedValue);

        insert.OrgIDX = int.Parse(ViewState["Org_idx"].ToString());
        insert.RDeptIDX = int.Parse(ViewState["rdept_idx"].ToString());
        insert.RSecIDX = int.Parse(ViewState["Sec_idx"].ToString());
        insert.RPosIDX = int.Parse(ViewState["Pos_idx"].ToString());
        insert.CommentUser = txtremark.Text;
        insert.FileUser = 0;
        insert.UserEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        insert.SysIDX = int.Parse(ddlsystem.SelectedValue);
        insert.ACIDX = 1;
        if (ddlsystem.SelectedValue == "11")
        {
            insert.DateManage = System.DateTime.Now.ToString("dd/MM/yyyy");
            insert.MCIDX = int.Parse(ddlmachine.SelectedValue);
        }
        else
        {
            insert.DateManage = AddStartdate.Text;
            insert.MCIDX = int.Parse(ddlmachine_pm.SelectedValue);
        }


        insert.u1idx_planning = int.Parse(ddldate.SelectedValue);

        _dtmachine.BoxRepairMachine[0] = insert;
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));

        _dtmachine = callServicePostENRepair(urlInsert_System, _dtmachine);

        int RPIDX = _dtmachine.BoxRepairMachine[0].RPIDX;
        string recode = _dtmachine.BoxRepairMachine[0].RECode;

        hfcit2 = Request.Files;
        if (hfcit2.Count > 0)
        {
            for (int ii = 0; ii < hfcit2.Count; ii++)
            {
                HttpPostedFile hpfLo = hfcit2[ii];
                if (hpfLo.ContentLength > 1)
                {
                    string getPath = ConfigurationSettings.AppSettings["PathFile_EN"];
                    //  string RECode1 = ViewState["ReCode"].ToString();
                    string fileName1 = recode + ii;
                    string filePath1 = Server.MapPath(getPath + recode);
                    if (!Directory.Exists(filePath1))
                    {
                        Directory.CreateDirectory(filePath1);
                    }
                    string extension = Path.GetExtension(hpfLo.FileName);

                    hpfLo.SaveAs(Server.MapPath(getPath + recode) + "\\" + fileName1 + extension);
                }
            }
        }
        else
        {

        }


        if (hfcit2.Count != 0)
        {
            _dtmachine = new DataMachine();

            _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
            RepairMachineDetail updatefile = new RepairMachineDetail();


            updatefile.FileUser = 1;
            updatefile.RPIDX = RPIDX;

            _dtmachine.BoxRepairMachine[0] = updatefile;
            _dtmachine = callServicePostENRepair(urlUploadfile_System, _dtmachine);

        }


    }

    protected void AdminGetJob_System()
    {
        HiddenField hfM0NodeIDX = (HiddenField)FvDetailRepair.FindControl("hfM0NodeIDX");
        HiddenField hfM0ActoreIDX = (HiddenField)FvDetailRepair.FindControl("hfM0ActoreIDX");
        DropDownList ddl_admin_getjob = (DropDownList)panel_admin_getjob.FindControl("ddl_admin_getjob");
        TextBox txtcommentreceive = (TextBox)panel_admin_getjob.FindControl("txtcommentreceive");
        Label lblRPIDX = (Label)FvDetailRepair.FindControl("lblRPIDX");

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail admingetjob = new RepairMachineDetail();

        admingetjob.nodeidx = int.Parse(hfM0NodeIDX.Value);
        admingetjob.actoridx = int.Parse(hfM0ActoreIDX.Value);
        admingetjob.STAppIDX = int.Parse(ddl_admin_getjob.SelectedValue);
        admingetjob.CommentReciveAdmin = txtcommentreceive.Text;
        admingetjob.AdminReciveEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        admingetjob.RPIDX = int.Parse(lblRPIDX.Text);
        _dtmachine.BoxRepairMachine[0] = admingetjob;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));

        _dtmachine = callServicePostENRepair(urlAdminGetJob_System, _dtmachine);

    }

    protected void AdminCloseJob_System()
    {
        HiddenField hfM0NodeIDX = (HiddenField)FvDetailRepair.FindControl("hfM0NodeIDX");
        HiddenField hfM0ActoreIDX = (HiddenField)FvDetailRepair.FindControl("hfM0ActoreIDX");
        DropDownList ddl_admin_closejob = (DropDownList)panel_admin_closejob.FindControl("ddl_admin_closejob");
        Label lblRPIDX = (Label)FvDetailRepair.FindControl("lblRPIDX");
        TextBox txtprice = (TextBox)FvCloseJob.FindControl("txtprice");
        TextBox CommentAdmin = (TextBox)FvCloseJob.FindControl("CommentAdmin");

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail admingetjob = new RepairMachineDetail();

        admingetjob.nodeidx = int.Parse(hfM0NodeIDX.Value);
        admingetjob.actoridx = int.Parse(hfM0ActoreIDX.Value);
        admingetjob.STAppIDX = int.Parse(ddl_admin_closejob.SelectedValue);
        admingetjob.AdminEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        admingetjob.RPIDX = int.Parse(lblRPIDX.Text);

        if (int.Parse(ddl_admin_closejob.SelectedValue) == 7 || ViewState["m0_node"].ToString() == "5")
        {
            admingetjob.Price = 0;// int.Parse(txtprice.Text);
            admingetjob.CommentAdmin = CommentAdmin.Text;
        }

        _dtmachine.BoxRepairMachine[0] = admingetjob;

        _dtmachine = callServicePostENRepair(urlAdminGetJob_System, _dtmachine);
    }

    protected void UserCloseJob_System()
    {
        HiddenField hfM0NodeIDX = (HiddenField)FvDetailRepair.FindControl("hfM0NodeIDX");
        HiddenField hfM0ActoreIDX = (HiddenField)FvDetailRepair.FindControl("hfM0ActoreIDX");
        Label lblRPIDX = (Label)FvDetailRepair.FindControl("lblRPIDX");
        CheckBox chkapprove = (CheckBox)FvApproveUser.FindControl("chkapprove");
        TextBox CommentUser = (TextBox)FvApproveUser.FindControl("CommentUser");

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail userclosejob = new RepairMachineDetail();

        userclosejob.nodeidx = int.Parse(hfM0NodeIDX.Value);
        userclosejob.actoridx = int.Parse(hfM0ActoreIDX.Value);
        userclosejob.CommentApprove = CommentUser.Text;

        if (chkapprove.Checked)
        {
            userclosejob.STAppIDX = 8;
        }
        else
        {
            userclosejob.STAppIDX = 9;
        }

        userclosejob.RPIDX = int.Parse(lblRPIDX.Text);

        _dtmachine.BoxRepairMachine[0] = userclosejob;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));

        _dtmachine = callServicePostENRepair(urlAdminGetJob_System, _dtmachine);
    }

    protected void InsertComment_System()
    {
        Label lblRPIDX = (Label)FvDetailRepair.FindControl("lblRPIDX");

        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail comment = new RepairMachineDetail();

        if (txtcomment.Text != "")
        {
            comment.CommentApprove = txtcomment.Text;
        }
        else
        {
            comment.CommentApprove = "-";
        }
        comment.RPIDX = int.Parse(lblRPIDX.Text);
        comment.UserEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtmachine.BoxRepairMachine[0] = comment;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));

        _dtmachine = callServicePostENRepair(urlInsertComment_System, _dtmachine);
    }

    protected void Insert_File()
    {
        Label lblRPIDX = (Label)FvDetailRepair.FindControl("lblRPIDX");


        _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail comment = new RepairMachineDetail();

        comment.FileUser = 1;
        comment.RPIDX = int.Parse(lblRPIDX.Text);
        comment.RDeptIDX = int.Parse(ViewState["rdept_idx"].ToString());

        _dtmachine.BoxRepairMachine[0] = comment;


        _dtmachine = callServicePostENRepair(urlUploadfile_System, _dtmachine);


    }

    protected void Insert_GvQuestionEN_List()
    {
        int[] subSet = new int[30]; //ค่าประเมิน

        GridView GvQuestionEN = (GridView)ViewQuestion.FindControl("GvQuestionEN");
        TextBox txt = (TextBox)ViewQuestion.FindControl("txtother");

        _dtmachine = new DataMachine();
        _dtmachine.BoxQuestionMachine = new QuestionDetail[1];
        QuestionDetail questionair = new QuestionDetail();


        //code percen
        for (int i = 0; i <= GvQuestionEN.Rows.Count - 1; i++)
        {
            RadioButtonList rdbChoice = (RadioButtonList)GvQuestionEN.Rows[i].FindControl("rdbChoice");
            string rd = rdbChoice.SelectedValue.ToString();

            subSet[i] = int.Parse(rd);

        }



        //ชุดแบบสอบถาม
        int[] numSet = new int[] { 1 }; //{ 1 }; //เลขชุดข้อมูลGvQuestionEN.Rows.Count
        int[] valueSet = new int[] { GvQuestionEN.Rows.Count }; // { 4 }; //จำนวนข้อย่อยของแต่ละชุด
        //int[] subSet = new int[] { 1, 1, 1, 1, 1, 1, 1, 1 }; //ค่าประเมิน

        double[] subSet2 = new double[30];//รับค่าข้อย่อย
        double[] subSet3 = new double[30];//รัปค่าเปอร์เซ็นข้อย่อย
                                          //คะแนน
        double[] fullScore = new double[30];
        double[] score = new double[30];
        //แยกจำนวนข้อของแต่ละชุด
        int pointSet = 0;
        //หาเปอร์เซ็น
        double setPercen = 0.0;
        setPercen += 100 / numSet.Length;

        Double setPercen2 = 0.0;
        //ผลรวมทั้งหมด
        double summary = 0.0;
        //จำช่อง array จะได้ไม่วนค่าซ้ำ
        int arrayPoint = 0;

        for (int num = 0; num < numSet.Length; num++)
        {
            //เปอร์เซ็นที่ 2
            setPercen2 = setPercen / valueSet[num];
            //รับเลขจำนวนข้อย่อย
            pointSet = valueSet[num];
            for (int num2 = 0; num2 < pointSet; num2++)
            {
                //คะแนนเต็ม
                fullScore[num] += 4;
                //รวมคะแนนข้อย่อย
                score[num] += subSet[arrayPoint];
                //เปอร์เซ็นข้อย่อย
                subSet2[arrayPoint] = subSet[arrayPoint];
                subSet3[arrayPoint] = (subSet2[arrayPoint] / pointSet) * setPercen2;
                //บวกเพิ่มครั้งละ1
                arrayPoint += 1;
            }
        }

        //หาเปอร์เซ็น
        for (int num3 = 0; num3 < numSet.Length; num3++)
        {
            summary += (score[num3] / fullScore[num3]) * setPercen;
        }
        int sumInt = Convert.ToInt32(summary);
        //แสดงค่า
        for (int num4 = 0; num4 < numSet.Length; num4++)
        {
            pointSet = valueSet[num4];
            for (int num5 = 0; num5 < pointSet; num5++)
            {
                //Response.Write(subSet3[num5] + "%"+"<br />");                
            }

        }

        questionair.TIDX = int.Parse(ViewState["TIDX"].ToString());
        questionair.RPIDX = int.Parse(ViewState["RPIDX"].ToString());

        if (txt.Text != "")
        {
            questionair.Other = txt.Text;
        }
        else
        {
            questionair.Other = "-";
        }

        questionair.SumPoint = sumInt;//Convert.ToInt32(summary);
        questionair.WFIDX = 1;
        questionair.STAIDX = 1;

        _dtmachine.BoxQuestionMachine[0] = questionair;
        _dtmachine = callServicePostENRepair(urlInsert_Questionair, _dtmachine);

        ViewState["ReturnCode_fb"] = _dtmachine.ReturnCode;

        if (int.Parse(ViewState["ReturnCode_fb"].ToString()) == 1)
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('แบบสอบถามนี้ถูกประเมินไปแล้ว');", true);
        }

        else
        {
            Insert_GvQuestionEN1_List(numSet, valueSet, pointSet, subSet3, summary);
        }



    }

    protected void Insert_GvQuestionEN1_List(int[] numSet, int[] valueSet, int pointSet, double[] subSet3, double summary)
    {
        int[] subSet = new int[30]; //ค่าประเมิน

        GridView GvQuestionEN = (GridView)ViewQuestion.FindControl("GvQuestionEN");

        for (int i = 0; i <= GvQuestionEN.Rows.Count - 1; i++)
        {
            RadioButtonList rdbChoice = (RadioButtonList)GvQuestionEN.Rows[i].FindControl("rdbChoice");
            string rd = rdbChoice.SelectedValue;


            _dtmachine = new DataMachine();
            _dtmachine.BoxQuestionMachine = new QuestionDetail[1];
            QuestionDetail questionair = new QuestionDetail();

            questionair.TFBIDX = int.Parse(ViewState["TFBIDX"].ToString());

            questionair.PO_TFBIDX2 = Convert.ToInt32(subSet3[i]);

            questionair.POIDX = int.Parse(rd);

            _dtmachine.BoxQuestionMachine[0] = questionair;
            _dtmachine = callServicePostENRepair(urlInsert_Questionair_Sub, _dtmachine);


        }

    }
    #endregion

    #region GridView
    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMasterTypeMachine":

                GvMasterTypeMachine.PageIndex = e.NewPageIndex;
                SelectMaster_TypeMachine();

                break;

            case "GvMasterStatus":

                GvMasterStatus.PageIndex = e.NewPageIndex;
                SelectMaster_Status();

                break;


            case "GvViewENRepair":

                GvViewENRepair.PageIndex = e.NewPageIndex;
                Select_Index();

                break;

            case "GvComment":

                GvComment.PageIndex = e.NewPageIndex;
                Select_Comment();

                break;

            case "GvHistory":
                GvHistory.PageIndex = e.NewPageIndex;
                Select_History();
                break;

            case "GvReport":
                GvReport.PageIndex = e.NewPageIndex;
                Select_ReportTable();
                break;

        }
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMasterTypeMachine":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }


                break;

            case "GvComment":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    if (GvComment.EditIndex != e.Row.RowIndex)
                    {

                        var btnedit = ((LinkButton)e.Row.FindControl("Edit"));
                        var lblIDXCMSAP = ((Label)e.Row.FindControl("lblIDXCMSAP"));
                        int emp = int.Parse(lblIDXCMSAP.Text);

                        if (emp == int.Parse(ViewState["EmpIDX"].ToString()))
                        {
                            btnedit.Visible = true;
                        }
                        else
                        {
                            btnedit.Visible = false;
                        }
                    }

                }

                break;

            case "GvMasterStatus":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMasterStatus.EditIndex == e.Row.RowIndex)
                    {
                        var ddl_downtime_edit = (DropDownList)e.Row.FindControl("ddl_downtime_edit");
                        var lbDTIDX = (Label)e.Row.FindControl("lbDTIDX");

                        Select_Downtime(ddl_downtime_edit);
                        ddl_downtime_edit.SelectedValue = lbDTIDX.Text;


                    }

                }


                break;

            case "GvViewENRepair":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvViewENRepair.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lblIDApprove = ((Label)e.Row.FindControl("lblIDApprove"));
                        var lblnameApprove = ((Label)e.Row.FindControl("lblnameApprove"));
                        var BoxDate = ((Panel)e.Row.FindControl("BoxDate"));
                        var lbdaterecive = ((Literal)e.Row.FindControl("lbdaterecive"));
                        var lbtimerecive = ((Literal)e.Row.FindControl("lbtimerecive"));
                        var lbViewHistory = ((LinkButton)e.Row.FindControl("lbViewHistory"));
                        var lbViewEN = ((LinkButton)e.Row.FindControl("lbViewEN"));
                        var litnode = ((Literal)e.Row.FindControl("litnode"));
                        var liactor = ((Literal)e.Row.FindControl("liactor"));
                        var linode_status = ((Literal)e.Row.FindControl("linode_status"));
                        var liquest = ((Literal)e.Row.FindControl("liquest"));
                        var RepairEmpIDX = ((Label)e.Row.FindControl("RepairEmpIDX"));
                        var lbQuestionEN = ((LinkButton)e.Row.FindControl("lbQuestionEN"));
                        var litma = ((Label)e.Row.FindControl("litma"));
                        var litsys = ((Literal)e.Row.FindControl("litsys"));
                        var lisystem = ((Label)e.Row.FindControl("lisystem"));


                        if (litsys.Text == "11")
                        {
                            lisystem.ForeColor = System.Drawing.ColorTranslator.FromHtml("#b300b3");

                            if (linode_status.Text != "8" && linode_status.Text != "11" && linode_status.Text != "12")
                            {
                                litma.Text = "ยังไม่ได้รับการทำความสะอาด";
                                litma.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                                litma.Style["font-weight"] = "bold";
                            }
                            else
                            {
                                litma.Text = "ได้รับการทำความสะอาดเรียบร้อยแล้ว";
                                litma.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                                litma.Style["font-weight"] = "bold";
                            }
                        }
                        else
                        {
                            lisystem.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff4d88"); //#86b300

                            if (linode_status.Text != "12")
                            {
                                litma.Text = "ยังไม่ได้ทำการกรอกข้อมูล";
                                litma.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff8000");
                                litma.Style["font-weight"] = "bold";
                            }
                            else
                            {
                                litma.Text = "ทำการกรอกข้อมูลเรียบร้อยแล้ว";
                                litma.ForeColor = System.Drawing.ColorTranslator.FromHtml("#3333ff");
                                litma.Style["font-weight"] = "bold";
                            }
                        }
                        lisystem.Style["font-weight"] = "bold";

                        linkBtnTrigger(lbViewEN);
                        linkBtnTrigger(btn_savecomment);

                        if (litnode.Text == "2" && liactor.Text == "1" && RepairEmpIDX.Text == ViewState["EmpIDX"].ToString() && liquest.Text == "0" ||
                            litnode.Text == "5" && liactor.Text == "2" && RepairEmpIDX.Text == ViewState["EmpIDX"].ToString() && liquest.Text == "0" ||
                            litnode.Text == "9" && liactor.Text == "2" && RepairEmpIDX.Text == ViewState["EmpIDX"].ToString() && liquest.Text == "0")
                        {

                            lbQuestionEN.Visible = true;


                        }
                        else
                        {
                            lbQuestionEN.Visible = false;
                        }

                        if (int.Parse(lblIDApprove.Text) == 11)  // เสร็จสมบูรณ์
                        {
                            lblnameApprove.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                            lblnameApprove.Style["font-weight"] = "bold";

                            lbdaterecive.Visible = true;
                            lbtimerecive.Visible = true;
                            BoxDate.Visible = true;
                        }
                        else if (int.Parse(lblIDApprove.Text) == 1)  // ยังไม่รับงาน
                        {
                            lblnameApprove.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                            lblnameApprove.Style["font-weight"] = "bold";

                            lbdaterecive.Visible = false;
                            lbtimerecive.Visible = false;
                            BoxDate.Visible = false;
                        }
                        else if (int.Parse(lblIDApprove.Text) == 4 || int.Parse(lblIDApprove.Text) == 5
                            || int.Parse(lblIDApprove.Text) == 6)  // ส่งซัพ
                        {
                            lblnameApprove.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff6600");
                            lblnameApprove.Style["font-weight"] = "bold";

                            lbdaterecive.Visible = false;
                            lbtimerecive.Visible = false;
                            BoxDate.Visible = false;
                        }
                        else if (int.Parse(lblIDApprove.Text) == 2) //  กำลังดำเนินการ
                        {
                            lblnameApprove.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                            lblnameApprove.Style["font-weight"] = "bold";

                            lbdaterecive.Visible = false;
                            lbtimerecive.Visible = false;
                            BoxDate.Visible = false;
                        }
                        else if (int.Parse(lblIDApprove.Text) == 7) //  กำลังตรวจสอบ
                        {
                            lblnameApprove.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0000FF");
                            lblnameApprove.Style["font-weight"] = "bold";

                            lbdaterecive.Visible = true;
                            lbtimerecive.Visible = true;
                            BoxDate.Visible = true;
                        }
                        else if (int.Parse(lblIDApprove.Text) == 12)  // เสร็จสมบูรณ์
                        {
                            lblnameApprove.ForeColor = System.Drawing.ColorTranslator.FromHtml("#6600cc");
                            lblnameApprove.Style["font-weight"] = "bold";

                            lbdaterecive.Visible = true;
                            lbtimerecive.Visible = true;
                            BoxDate.Visible = true;
                        }

                        if (int.Parse(ViewState["Sec_idx"].ToString()) == secit || int.Parse(ViewState["rdept_idx"].ToString()) == depten || int.Parse(ViewState["rdept_idx"].ToString()) == 134)
                        {
                            lbViewHistory.Visible = true;
                        }
                        else
                        {
                            lbViewHistory.Visible = false;
                        }

                    }
                    else
                    {
                        linkBtnTrigger(btn_savecomment);

                    }

                }

                break;


            case "GvHistory":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvHistory.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var lblIDApprove = ((Label)e.Row.FindControl("lblIDApprove"));
                        var lblnameApprove = ((Label)e.Row.FindControl("lblnameApprove"));
                        var BoxDate = ((Panel)e.Row.FindControl("BoxDate"));
                        var lbdaterecive = ((Literal)e.Row.FindControl("lbdaterecive"));
                        var lbtimerecive = ((Literal)e.Row.FindControl("lbtimerecive"));
                        var lbViewEN = ((LinkButton)e.Row.FindControl("lbViewEN"));

                        linkBtnTrigger(lbViewEN);
                        linkBtnTrigger(btn_savecomment);


                        if (int.Parse(lblIDApprove.Text) == 11)  // เสร็จสมบูรณ์
                        {
                            lblnameApprove.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                            lblnameApprove.Style["font-weight"] = "bold";

                            lbdaterecive.Visible = true;
                            lbtimerecive.Visible = true;
                            BoxDate.Visible = true;
                        }
                        else if (int.Parse(lblIDApprove.Text) == 1)  // ยังไม่รับงาน
                        {
                            lblnameApprove.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                            lblnameApprove.Style["font-weight"] = "bold";

                            lbdaterecive.Visible = false;
                            lbtimerecive.Visible = false;
                            BoxDate.Visible = false;
                        }
                        else if (int.Parse(lblIDApprove.Text) == 4 || int.Parse(lblIDApprove.Text) == 5
                            || int.Parse(lblIDApprove.Text) == 6)  // ส่งซัพ
                        {
                            lblnameApprove.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff6600");
                            lblnameApprove.Style["font-weight"] = "bold";

                            lbdaterecive.Visible = false;
                            lbtimerecive.Visible = false;
                            BoxDate.Visible = false;
                        }
                        else if (int.Parse(lblIDApprove.Text) == 2) //  กำลังดำเนินการ
                        {
                            lblnameApprove.ForeColor = System.Drawing.ColorTranslator.FromHtml("#000000");
                            lblnameApprove.Style["font-weight"] = "bold";

                            lbdaterecive.Visible = false;
                            lbtimerecive.Visible = false;
                            BoxDate.Visible = false;
                        }
                        else if (int.Parse(lblIDApprove.Text) == 7) //  กำลังตรวจสอบ
                        {
                            lblnameApprove.ForeColor = System.Drawing.ColorTranslator.FromHtml("#0000FF");
                            lblnameApprove.Style["font-weight"] = "bold";

                            lbdaterecive.Visible = true;
                            lbtimerecive.Visible = true;
                            BoxDate.Visible = true;
                        }
                        else if (int.Parse(lblIDApprove.Text) == 12)  // เสร็จสมบูรณ์
                        {
                            lblnameApprove.ForeColor = System.Drawing.ColorTranslator.FromHtml("#6600cc");
                            lblnameApprove.Style["font-weight"] = "bold";

                            lbdaterecive.Visible = true;
                            lbtimerecive.Visible = true;
                            BoxDate.Visible = true;
                        }
                    }
                    else
                    {
                        linkBtnTrigger(btn_savecomment);

                    }

                }

                break;
            case "gvFile":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);
                }

                break;

            case "GvReport":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvReport.EditIndex != e.Row.RowIndex) //to overlook header row
                    {
                        var ltSysIDXreport = ((Label)e.Row.FindControl("ltSysIDXreport"));
                        var ltzSysIDX_name = ((Label)e.Row.FindControl("ltzSysIDX_name"));
                        var lblnameApprove1 = ((Label)e.Row.FindControl("lblnameApprove1"));
                        var lblnode = ((Label)e.Row.FindControl("lblnode"));

                        if (ltSysIDXreport.Text == "11")
                        {
                            ltzSysIDX_name.ForeColor = System.Drawing.ColorTranslator.FromHtml("#b300b3");

                            if (lblnode.Text != "8" && lblnode.Text != "11" && lblnode.Text != "12")
                            {
                                lblnameApprove1.Text = "ยังไม่ได้รับการทำความสะอาด";
                                lblnameApprove1.ForeColor = System.Drawing.ColorTranslator.FromHtml("#FF0000");
                                lblnameApprove1.Style["font-weight"] = "bold";
                            }
                            else
                            {
                                lblnameApprove1.Text = "ได้รับการทำความสะอาดเรียบร้อยแล้ว";
                                lblnameApprove1.ForeColor = System.Drawing.ColorTranslator.FromHtml("#00CC00");
                                lblnameApprove1.Style["font-weight"] = "bold";
                            }
                        }
                        else
                        {
                            ltzSysIDX_name.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff4d88"); //#86b300

                            if (lblnode.Text != "12")
                            {
                                lblnameApprove1.Text = "ยังไม่ได้ทำการกรอกข้อมูล";
                                lblnameApprove1.ForeColor = System.Drawing.ColorTranslator.FromHtml("#ff8000");
                                lblnameApprove1.Style["font-weight"] = "bold";
                            }
                            else
                            {
                                lblnameApprove1.Text = "ทำการกรอกข้อมูลเรียบร้อยแล้ว";
                                lblnameApprove1.ForeColor = System.Drawing.ColorTranslator.FromHtml("#3333ff");
                                lblnameApprove1.Style["font-weight"] = "bold";
                            }
                        }
                        ltzSysIDX_name.Style["font-weight"] = "bold";



                    }
                }


                break;

            case "GvQuestionEN":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }
                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                break;
        }
    }

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMasterTypeMachine":

                GvMasterTypeMachine.EditIndex = e.NewEditIndex;
                SelectMaster_TypeMachine();

                break;

            case "GvMasterStatus":

                GvMasterStatus.EditIndex = e.NewEditIndex;
                SelectMaster_Status();

                break;

            case "GvComment":
                GvComment.EditIndex = e.NewEditIndex;
                Select_Comment();


                break;
        }
    }

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMasterTypeMachine":


                int TmcIDX = Convert.ToInt32(GvMasterTypeMachine.DataKeys[e.RowIndex].Values[0].ToString());
                var txtcodeupdate = (TextBox)GvMasterTypeMachine.Rows[e.RowIndex].FindControl("txtcodeupdate");
                var txtNameENupdate = (TextBox)GvMasterTypeMachine.Rows[e.RowIndex].FindControl("txtNameENupdate");
                var txtNameTHupdate = (TextBox)GvMasterTypeMachine.Rows[e.RowIndex].FindControl("txtNameTHupdate");
                var ddlStatusUpdate = (DropDownList)GvMasterTypeMachine.Rows[e.RowIndex].FindControl("ddlStatusUpdate");

                GvMasterTypeMachine.EditIndex = -1;

                _dtmachine = new DataMachine();

                _dtmachine.BoxTypeMachine = new TypeMachineDetail[1];
                TypeMachineDetail insertmaster = new TypeMachineDetail();


                insertmaster.TmcIDX = TmcIDX;
                insertmaster.CodeTM = txtcodeupdate.Text;
                insertmaster.NameEN = txtNameENupdate.Text;
                insertmaster.NameTH = txtNameTHupdate.Text;
                insertmaster.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
                insertmaster.TStatus = int.Parse(ddlStatusUpdate.SelectedValue);

                _dtmachine.BoxTypeMachine[0] = insertmaster;

                //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));
                _dtmachine = callServicePostENRepair(urlInsert_TypeMachineList, _dtmachine);

                if (_dtmachine.ReturnCode == "0")
                {

                    SelectMaster_TypeMachine();

                }
                else
                {
                    setError(_dtmachine.ReturnCode.ToString() + " - " + _dtmachine.ReturnMsg);
                }


                break;

            case "GvMasterStatus":


                int stidx = Convert.ToInt32(GvMasterStatus.DataKeys[e.RowIndex].Values[0].ToString());
                var txtstatusupdate = (TextBox)GvMasterStatus.Rows[e.RowIndex].FindControl("txtstatusupdate");
                var ddl_downtime_edit = (DropDownList)GvMasterStatus.Rows[e.RowIndex].FindControl("ddl_downtime_edit");
                var ddlStatusUpdate_master = (DropDownList)GvMasterStatus.Rows[e.RowIndex].FindControl("ddlStatusUpdate_master");

                GvMasterStatus.EditIndex = -1;

                _dtmachine = new DataMachine();

                _dtmachine.BoxStatusList = new StatusListDetail[1];
                StatusListDetail masterstatus = new StatusListDetail();


                masterstatus.stidx = stidx;
                masterstatus.status_name = txtstatusupdate.Text;
                masterstatus.dtidx = int.Parse(ddl_downtime_edit.SelectedValue);
                masterstatus.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
                masterstatus.status_state = int.Parse(ddlStatusUpdate_master.SelectedValue);

                _dtmachine.BoxStatusList[0] = masterstatus;

                //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));
                _dtmachine = callServicePostENRepair(urlInsert_StatusList, _dtmachine);

                if (_dtmachine.ReturnCode == "0")
                {

                    SelectMaster_Status();

                }
                else
                {
                    setError(_dtmachine.ReturnCode.ToString() + " - " + _dtmachine.ReturnMsg);
                }


                break;

            case "GvComment":

                int CMIDX = Convert.ToInt32(GvComment.DataKeys[e.RowIndex].Values[0].ToString());
                var txtNameTypecodeEdit = (TextBox)GvComment.Rows[e.RowIndex].FindControl("txtNameTypecodeEdit");
                var txtEmpIDX = (TextBox)GvComment.Rows[e.RowIndex].FindControl("txtEmpIDX");

                GvComment.EditIndex = -1;

                _dtmachine = new DataMachine();

                _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
                RepairMachineDetail comment = new RepairMachineDetail();


                comment.CMIDX = CMIDX;
                comment.CommentApprove = txtNameTypecodeEdit.Text;

                _dtmachine.BoxRepairMachine[0] = comment;

                //  litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_datama));
                _dtmachine = callServicePostENRepair(urlUpdateComment_System, _dtmachine);

                Select_Comment();

                break;
        }
    }

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        switch (GvName.ID)
        {
            case "GvMasterTypeMachine":
                GvMasterTypeMachine.EditIndex = -1;
                SelectMaster_TypeMachine();
                break;

            case "GvMasterStatus":
                GvMasterStatus.EditIndex = -1;
                SelectMaster_Status();
                break;

            case "GvComment":
                GvComment.EditIndex = -1;
                Select_Comment();
                break;
        }
    }
    #endregion

    #region FvDetail_DataBound

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvDetailUser":
                FormView FvDetailUser = (FormView)ViewInsert.FindControl("FvDetailUser");

                if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();



                }
                break;

            case "FvInsertRepair":
                if (FvInsertRepair.CurrentMode == FormViewMode.Insert)
                {
                    DropDownList ddltype = (DropDownList)FvInsertRepair.FindControl("ddltype");
                    DropDownList ddllocate = (DropDownList)FvInsertRepair.FindControl("ddllocate");

                    Select_TypeMachine(ddltype);
                    Select_Location(ddllocate);

                }
                break;

            case "FvDetailUserRepair":

                FormView FvDetailUserRepair = (FormView)ViewIndex.FindControl("FvDetailUserRepair");
                if (FvDetailUserRepair.CurrentMode == FormViewMode.ReadOnly)
                {

                }
                break;

            case "FvApproveUser":

                FormView FvApproveUser = (FormView)ViewIndex.FindControl("FvApproveUser");
                if (FvApproveUser.CurrentMode == FormViewMode.Insert)
                {

                }
                break;

            case "FvCloseJob":
                UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
                if (FvCloseJob.CurrentMode == FormViewMode.Insert)
                {

                    linkBtnTrigger(btnAdminCloseJob);
                }
                break;
        }
    }

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;
        DropDownList ddltype = (DropDownList)FvInsertRepair.FindControl("ddltype");
        DropDownList ddllocate = (DropDownList)FvInsertRepair.FindControl("ddllocate");
        DropDownList ddlbuilding = (DropDownList)FvInsertRepair.FindControl("ddlbuilding");
        DropDownList ddlroom = (DropDownList)FvInsertRepair.FindControl("ddlroom");
        DropDownList ddlmachine = (DropDownList)FvInsertRepair.FindControl("ddlmachine");
        DropDownList ddlsystem = (DropDownList)FvInsertRepair.FindControl("ddlsystem");
        DropDownList ddltypecode = (DropDownList)FvInsertRepair.FindControl("ddltypecode");
        DropDownList ddlplan = (DropDownList)FvInsertRepair.FindControl("ddlplan");
        DropDownList ddlgroupmachine = (DropDownList)FvInsertRepair.FindControl("ddlgroupmachine");
        DropDownList ddldate = (DropDownList)FvInsertRepair.FindControl("ddldate");
        DropDownList ddlmachine_pm = (DropDownList)FvInsertRepair.FindControl("ddlmachine_pm");


        Control divpmms_add = (Control)FvInsertRepair.FindControl("divpmms_add");
        Control divtypecode_planning = (Control)FvInsertRepair.FindControl("divtypecode_planning");
        Control divenrepair_machine = (Control)FvInsertRepair.FindControl("divenrepair_machine");

        Label lblcommentadminclosejob = (Label)FvDetailRepair.FindControl("lblcommentadminclosejob");
        Label lblprice = (Label)FvDetailRepair.FindControl("lblprice");
        TextBox txtprice = (TextBox)FvCloseJob.FindControl("txtprice");
        TextBox CommentAdmin = (TextBox)FvCloseJob.FindControl("CommentAdmin");





        switch (ddName.ID)
        {

            case "ddllocate":
                Select_Building(ddlbuilding, int.Parse(ddllocate.SelectedValue));

                break;

            case "ddlsystem":
                if (ddlsystem.SelectedValue == "11")
                {
                    divpmms_add.Visible = false;
                    divtypecode_planning.Visible = false;
                    divenrepair_machine.Visible = true;
                    SetDefaultAdd();
                }
                else
                {
                    divpmms_add.Visible = true;
                    divtypecode_planning.Visible = true;
                    divenrepair_machine.Visible = false;
                    SetDefaultAdd();
                }
                break;

            case "ddllocate_search":
                Select_Building(ddlbuilding_search, int.Parse(ddllocate_search.SelectedValue));

                break;

            case "ddlbuilding":

                Select_Room(ddlroom, int.Parse(ddlbuilding.SelectedValue));
                Select_NameMachine(ddlmachine, int.Parse(ddlbuilding.SelectedValue), int.Parse(ddlroom.SelectedValue), int.Parse(ddltype.SelectedValue));

                break;

            case "ddlbuilding_search":
                Select_Room(ddlroom_search, int.Parse(ddlbuilding_search.SelectedValue));
                Select_NameMachine(ddlmachine_search, int.Parse(ddlbuilding_search.SelectedValue), int.Parse(ddlroom_search.SelectedValue), int.Parse(ddltype_search.SelectedValue));
                break;

            case "ddltype":
                select_typecodemachine(ddltypecode, int.Parse(ddltype.SelectedValue));

                break;
            case "ddlroom":

                Select_NameMachine(ddlmachine, int.Parse(ddlbuilding.SelectedValue), int.Parse(ddlroom.SelectedValue), int.Parse(ddltype.SelectedValue));
                break;

            case "ddltypecode":
                select_groupmachine(ddlgroupmachine, int.Parse(ddltypecode.SelectedValue));
                break;

            case "ddlplan":

                SelectDoc_LenghtDate(ddldate, int.Parse(ddltype.SelectedValue), int.Parse(ddltypecode.SelectedValue), int.Parse(ddlgroupmachine.SelectedValue),
                    int.Parse(ddllocate.SelectedValue), int.Parse(ddlbuilding.SelectedValue), int.Parse(ddlroom.SelectedValue), int.Parse(ddlplan.SelectedValue));

                SelectDoc_MachineEN(ddlmachine_pm, int.Parse(ddltype.SelectedValue), int.Parse(ddltypecode.SelectedValue), int.Parse(ddlgroupmachine.SelectedValue),
                    int.Parse(ddllocate.SelectedValue), int.Parse(ddlbuilding.SelectedValue), int.Parse(ddlroom.SelectedValue), int.Parse(ddlplan.SelectedValue));

                break;



            case "ddltype_search":
            case "ddlroom_search":
                Select_NameMachine(ddlmachine_search, int.Parse(ddlbuilding_search.SelectedValue), int.Parse(ddlroom_search.SelectedValue), int.Parse(ddltype_search.SelectedValue));

                break;

            case "ddlSearchOrg":
                getDepartmentList(ddlSearchDep, int.Parse(ddlSearchOrg.SelectedValue));
                break;


            case "ddl_admin_closejob":

                if (ddl_admin_closejob.SelectedValue == "7")
                {
                    AdminAcceptCloseJob.Visible = true;
                    linkBtnAsynTrigger(btnAdminCloseJob);

                    if (lblcommentadminclosejob.Text != "" && lblcommentadminclosejob.Text != "ยังไม่ได้ปิดงาน")
                    {
                        //  txtprice.Text = "0";// lblprice.Text;
                        CommentAdmin.Text = lblcommentadminclosejob.Text;
                    }
                    else
                    {
                        // txtprice.Text = "";
                        CommentAdmin.Text = "";
                    }
                }
                else
                {
                    AdminAcceptCloseJob.Visible = false;
                }
                break;

            case "ddlTypeSearchDate":

                if (int.Parse(ddlTypeSearchDate.SelectedValue) != 00)
                {
                    BoxSearch.Visible = true;
                }
                else
                {
                    AddStartdate.Text = string.Empty;
                    AddEndDate.Text = string.Empty;

                    ddlSearchDate.AppendDataBoundItems = true;
                    ddlSearchDate.Items.Clear();
                    ddlSearchDate.Items.Add(new ListItem("เลือกเงื่อนไข ....", "00"));
                    ddlSearchDate.Items.Add(new ListItem("มากกว่า >", "1"));
                    ddlSearchDate.Items.Add(new ListItem("น้อยกว่า <", "2"));
                    ddlSearchDate.Items.Add(new ListItem("ระหว่าง <>", "3"));
                }

                break;

            case "ddlSearchDate":

                if (int.Parse(ddlSearchDate.SelectedValue) == 3)
                {
                    AddEndDate.Enabled = true;
                }
                else
                {
                    AddEndDate.Enabled = false;
                    AddEndDate.Text = string.Empty;
                }

                break;

            case "ddltypereport":
                gridreport.Visible = false;
                grant.Visible = false;
              //  gridreport_from.Visible = false;

                if (int.Parse(ddltypereport.SelectedValue) == 1)
                {
                    Panel_Table.Visible = true;
                    Panel_Chart.Visible = false;
                    Panel_from.Visible = false;

                    SetDefault_ReportTable();
                }
                else if (int.Parse(ddltypereport.SelectedValue) == 2)
                {
                    Panel_Table.Visible = false;
                    Panel_Chart.Visible = true;
                    Panel_from.Visible = false;
                }
                else if (int.Parse(ddltypereport.SelectedValue) == 3)
                {
                    Panel_Table.Visible = false;
                    Panel_Chart.Visible = false;
                    Panel_from.Visible = true;
                    Select_Location(ddllocate_from);
                    Select_Building(ddlbuilding_from, int.Parse(ddllocate_from.SelectedValue));
                    Select_Admin(ddladminreceive_from);
                    // SetDefault_Reportfrom();
                }
                else
                {
                    Panel_Table.Visible = false;
                    Panel_Chart.Visible = false;
                    Panel_from.Visible = false;
                    ddltypereport.SelectedValue = "0";
                    SetDefault_ReportTable();
                }

                break;

            case "ddlTypeSearchDate_Report":


                if (ddlTypeSearchDate_Report.SelectedValue == "0")
                {
                    AddStartDate_Report.Text = string.Empty;
                    AddEndDate_Report.Text = string.Empty;

                    ddlSearchDate_Report.AppendDataBoundItems = true;
                    ddlSearchDate_Report.Items.Clear();
                    ddlSearchDate_Report.Items.Add(new ListItem("เลือกเงื่อนไข ....", "0"));
                    ddlSearchDate_Report.Items.Add(new ListItem("มากกว่า >", "1"));
                    ddlSearchDate_Report.Items.Add(new ListItem("น้อยกว่า <", "2"));
                    ddlSearchDate_Report.Items.Add(new ListItem("ระหว่าง <>", "3"));
                }

                break;

            case "ddlSearchDate_Report":

                if (int.Parse(ddlSearchDate_Report.SelectedValue) == 3)
                {
                    AddEndDate_Report.Enabled = true;
                }
                else
                {
                    AddEndDate_Report.Enabled = false;
                    AddEndDate_Report.Text = string.Empty;
                }

                break;

            case "ddllocate_report":
                Select_Building(ddlbuilding_report, int.Parse(ddllocate_report.SelectedValue));

                break;

            case "ddlbuilding_report":
                Select_Room(ddlroom_report, int.Parse(ddlbuilding_report.SelectedValue));

                break;

            case "ddltype_report":
            case "ddlroom_report":
                Select_NameMachine(ddlmachine_report, int.Parse(ddlbuilding_report.SelectedValue), int.Parse(ddlroom_report.SelectedValue), int.Parse(ddltype_report.SelectedValue));

                break;
            case "ddlReportOrg":

                getDepartmentList(ddlSearchDep, int.Parse(ddlSearchOrg.SelectedValue));

                break;

            case "ddllocate_chart":
                Select_Building(ddlbuilding_chart, int.Parse(ddllocate_chart.SelectedValue));

                break;

            case "ddltypechart":
                //   div_machine.Visible = false;
                if (ddltypechart.SelectedValue == "1" || ddltypechart.SelectedValue == "4")
                {
                    div_year.Visible = true;
                    div_lenght.Visible = false;
                    divbuild.Visible = true;
                }
                else if (ddltypechart.SelectedValue == "2" || ddltypechart.SelectedValue == "3" || ddltypechart.SelectedValue == "5")
                {
                    div_year.Visible = false;
                    div_lenght.Visible = true;

                    //if (ddltypechart.SelectedValue == "2")
                    //{
                    //    div_machine.Visible = true;
                    //    Select_Checkbox_Machine();
                    //}
                    //else
                    //{
                    //    div_machine.Visible = false;
                    //}

                }
                else
                {
                    div_year.Visible = false;
                    div_lenght.Visible = false;
                }


                if (ddltypechart.SelectedValue == "3" || ddltypechart.SelectedValue == "5")
                {
                    divbuild.Visible = false;
                }
                else

                {
                    divbuild.Visible = true;
                }

                if (ddltypechart.SelectedValue == "5")
                {
                    ddlSysIDX_chart.SelectedValue = "11";
                    ddlSysIDX_chart.Enabled = false;
                }
                else
                {
                    ddlSysIDX_chart.Enabled = true;
                }
                break;

            case "ddlSearchDate_Chart":
                if (int.Parse(ddlSearchDate_Chart.SelectedValue) == 3)
                {
                    AddEndDate_Chart.Enabled = true;
                }
                else
                {
                    AddEndDate_Chart.Enabled = false;
                    AddEndDate_Chart.Text = string.Empty;
                }
                break;

            case "ddllocate_from":
                Select_Building(ddlbuilding_from, int.Parse(ddllocate_from.SelectedValue));
                break;

        }
    }
    #endregion

    #region SetDefault

    protected void SetViewIndex()
    {
        litoindex.Attributes.Add("class", "active");
        litoreport.Attributes.Add("class", "non-active");
        litoinsert.Attributes.Add("class", "non-active");
        litoreportrepair.Attributes.Add("class", "non-active");
        litoreportquestion.Attributes.Add("class", "non-active");
        litoMaster.Attributes.Add("class", "non-active");
        lblmasterstatus.Attributes.Add("class", "non-active");
        lblmastertypemachine.Attributes.Add("class", "non-active");
        MvMaster.SetActiveView(ViewIndex);

        SETBoxAllSearch.Visible = false;
        BoxButtonSearchShow_search.Visible = true;
        BoxButtonSearchHide_search.Visible = false;

        Select_TypeMachine(ddltype_search);
        Select_Location(ddllocate_search);
        getOrganizationList(ddlSearchOrg);
        Select_Status(ddlSearchStatus);
        Select_Admin(ddladmin);
        Set_Defult_TextSearch();
    }

    protected void Set_Defult_TextSearch()
    {
        ddlTypeSearchDate.AppendDataBoundItems = true;
        ddlTypeSearchDate.Items.Clear();
        ddlTypeSearchDate.Items.Insert(0, new ListItem("เลือกประเภทการค้นหา ........", "0"));
        ddlTypeSearchDate.Items.Insert(1, new ListItem("วันที่แจ้งงาน", "1"));
        ddlTypeSearchDate.Items.Insert(2, new ListItem("วันที่รับงาน", "2"));
        ddlTypeSearchDate.Items.Insert(3, new ListItem("วันที่ปิดงาน", "3"));


        ddlSearchDate.AppendDataBoundItems = true;
        ddlSearchDate.Items.Clear();
        ddlSearchDate.Items.Insert(0, new ListItem("เลือกเงื่อนไข ....", "0"));
        ddlSearchDate.Items.Insert(1, new ListItem("มากกว่า >", "1"));
        ddlSearchDate.Items.Insert(2, new ListItem("น้อยกว่า <", "2"));
        ddlSearchDate.Items.Insert(3, new ListItem("ระหว่าง <>", "3"));


        ddlmachine_search.SelectedValue = "0";
        ddlbuilding_search.SelectedValue = "0";
        ddlroom_search.SelectedValue = "0";
        ddlSearchDep.SelectedValue = "0";

        AddEndDate.Enabled = false;
        AddStartdate.Text = string.Empty;
        AddEndDate.Text = string.Empty;
        txtEmpIDX.Text = string.Empty;

    }

    protected void SetViewInsert()
    {
        litoinsert.Attributes.Add("class", "active");
        litoindex.Attributes.Add("class", "non-active");
        litoreport.Attributes.Add("class", "non-active");
        litoreportrepair.Attributes.Add("class", "non-active");
        litoreportquestion.Attributes.Add("class", "non-active");
        litoMaster.Attributes.Add("class", "non-active");
        lblmasterstatus.Attributes.Add("class", "non-active");
        lblmastertypemachine.Attributes.Add("class", "non-active");

        MvMaster.SetActiveView(ViewInsert);

        FvDetailUser.ChangeMode(FormViewMode.Insert);
        FvDetailUser.DataBind();

        FvInsertRepair.ChangeMode(FormViewMode.Insert);
        FvInsertRepair.DataBind();
        var ddlplan = (DropDownList)FvInsertRepair.FindControl("ddlplan");
        select_time(ddlplan);



    }

    protected void SetDefaultAdd()
    {
        DropDownList ddltype = (DropDownList)FvInsertRepair.FindControl("ddltype");
        DropDownList ddllocate = (DropDownList)FvInsertRepair.FindControl("ddllocate");
        DropDownList ddlbuilding = (DropDownList)FvInsertRepair.FindControl("ddlbuilding");
        DropDownList ddlroom = (DropDownList)FvInsertRepair.FindControl("ddlroom");
        DropDownList ddlmachine = (DropDownList)FvInsertRepair.FindControl("ddlmachine");
        DropDownList ddlsystem = (DropDownList)FvInsertRepair.FindControl("ddlsystem");
        DropDownList ddltypecode = (DropDownList)FvInsertRepair.FindControl("ddltypecode");
        DropDownList ddlgroupmachine = (DropDownList)FvInsertRepair.FindControl("ddlgroupmachine");
        DropDownList ddlplan = (DropDownList)FvInsertRepair.FindControl("ddlplan");
        DropDownList ddldate = (DropDownList)FvInsertRepair.FindControl("ddldate");
        DropDownList ddlmachine_pm = (DropDownList)FvInsertRepair.FindControl("ddlmachine_pm");

        TextBox txtremark = (TextBox)FvInsertRepair.FindControl("txtremark");

        ddltype.SelectedValue = "0";
        ddllocate.SelectedValue = "0";
        ddltypecode.SelectedValue = "0";
        ddlgroupmachine.SelectedValue = "0";
        ddlbuilding.SelectedValue = "0";
        ddlroom.SelectedValue = "0";
        ddlmachine.SelectedValue = "0";
        ddlplan.SelectedValue = "0";
        ddldate.SelectedValue = "0";
        ddlmachine_pm.SelectedValue = "0";
        AddStartdate.Text = String.Empty;
        txtremark.Text = String.Empty;
    }

    protected void SetViewReportEN()
    {
        litoreport.Attributes.Add("class", "active");
        litoreportrepair.Attributes.Add("class", "active");
        litoindex.Attributes.Add("class", "non-active");
        litoinsert.Attributes.Add("class", "non-active");
        litoreportquestion.Attributes.Add("class", "non-active");
        litoMaster.Attributes.Add("class", "non-active");
        lblmasterstatus.Attributes.Add("class", "non-active");
        lblmastertypemachine.Attributes.Add("class", "non-active");
        MvMaster.SetActiveView(ViewReport);

        ddltypereport.SelectedValue = "0";
        ddlSearchDate_Report.SelectedValue = "0";
        ddlTypeSearchDate_Report.SelectedValue = "0";
        AddStartDate_Report.Text = String.Empty;
        AddEndDate_Report.Text = String.Empty;
        txtempname.Text = String.Empty;

        Select_TypeMachine(ddltype_report);
        Select_Location(ddllocate_report);
        getOrganizationList(ddlReportOrg);
        Select_Status(ddlReportStatus);
        Select_Admin(ddladminreceive_report);
        Select_Admin(ddladminaccept_report);

        ddltypechart.SelectedValue = "0";
        ddlmonth.SelectedValue = "0";

        Select_Location(ddllocate_chart);
        GenerateddlYear(ddlyear);
        div_year.Visible = false;
        div_lenght.Visible = false;

        pnl_print_plan.Visible = false;
        pnl_alert.Visible = false;

    }

    protected void SetViewReportQuestion()
    {
        litoreport.Attributes.Add("class", "active");
        litoreportquestion.Attributes.Add("class", "active");
        litoreportrepair.Attributes.Add("class", "non-active");
        litoindex.Attributes.Add("class", "non-active");
        litoinsert.Attributes.Add("class", "non-active");
        litoMaster.Attributes.Add("class", "non-active");
        lblmasterstatus.Attributes.Add("class", "non-active");
        lblmastertypemachine.Attributes.Add("class", "non-active");

        MvMaster.SetActiveView(ViewQuestion);
    }

    protected void SetViewMasterTypeMachine()
    {
        litoMaster.Attributes.Add("class", "active");
        listatus.Attributes.Add("class", "non-active");
        limastertypemachine.Attributes.Add("class", "active");
        litoreport.Attributes.Add("class", "non-active");
        litoreportrepair.Attributes.Add("class", "non-active");
        litoindex.Attributes.Add("class", "non-active");
        litoinsert.Attributes.Add("class", "non-active");
        litoreportquestion.Attributes.Add("class", "non-active");

        MvMaster.SetActiveView(ViewMasterTypeMachine);
        txtaddcode.Text = String.Empty;
        txtaddnameen.Text = String.Empty;
        txtaddnameth.Text = String.Empty;

        SelectMaster_TypeMachine();
        BoxButtonSearchShow.Visible = true;
        BoxButtonSearchHide.Visible = false;

        Panel_Add.Visible = false;
    }

    protected void SetViewMasterStatus()
    {
        litoMaster.Attributes.Add("class", "active");
        listatus.Attributes.Add("class", "active");
        limastertypemachine.Attributes.Add("class", "non-active");
        litoreport.Attributes.Add("class", "non-active");
        litoreportrepair.Attributes.Add("class", "non-active");
        litoindex.Attributes.Add("class", "non-active");
        litoinsert.Attributes.Add("class", "non-active");
        litoreportquestion.Attributes.Add("class", "non-active");
        limastertypemachine.Attributes.Add("class", "non-active");

        MvMaster.SetActiveView(ViewMasterStatus);

        BoxButtonSearchShowStatus.Visible = true;
        BoxButtonSearchHideStatus.Visible = false;

        SelectMaster_Status();

        txtaddstatusname.Text = String.Empty;

        Panel_AddStatus.Visible = false;
    }

    protected void SetDefault_ReportTable()
    {
        ddlSysIDX_chart.Enabled = true;
        ddlSysIDX_chart.SelectedValue = "0";
        ddlSearchDate_Report.SelectedValue = "0";
        ddlTypeSearchDate_Report.SelectedValue = "0";
        AddStartDate_Report.Text = String.Empty;
        AddEndDate_Report.Text = String.Empty;
        txtempname.Text = String.Empty;
        gridreport.Visible = false;
        Select_TypeMachine(ddltype_report);
        Select_Location(ddllocate_report);
        getOrganizationList(ddlReportOrg);
        Select_Status(ddlReportStatus);
        Select_Admin(ddladminreceive_report);
        Select_Admin(ddladminaccept_report);
    }




    #endregion

    #region Function
    protected void setError(string _errorText)
    {
        if (_errorText != "")
        {
            litDebug.Text = "error : " + _errorText;
        }
        else
        {
            litDebug.Text = String.Empty;
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }

    protected void linkBtnAsynTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger asynctrigger1 = new AsyncPostBackTrigger();
        asynctrigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(asynctrigger1);
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        ddlName.Items.Clear();
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    protected void GenerateddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 5; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }

        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }

    #region Directories_File URL

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt1.Rows.Add(file.Name);
                    dt1.Rows[i][1] = f[0];
                    i++;
                }
            }

            GridView gvFile = (GridView)FvDetailRepair.FindControl("gvFile");
            // Panel gvFileLo1 = (Panel)FvDetailUser.FindControl("gvFileLo1");

            if (dt1.Rows.Count > 0)
            {

                ds1.Tables.Add(dt1);
                gvFile.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
                gvFile.DataBind();
                ds1.Dispose();
            }
            else
            {

                gvFile.DataSource = null;
                gvFile.DataBind();

            }
        }
        catch
        {
            //ViewState["CheckFile"] = "0";
            checkfile = "11";
        }
    }

    protected void Uploadfile()
    {
        Label DocCode = (Label)FvDetailRepair.FindControl("DocCode");


        if (int.Parse(ViewState["rdept_idx"].ToString()) == depten || int.Parse(ViewState["rdept_idx"].ToString()) == 134)
        {
            nameupload = "Admin-";
        }
        else
        {
            nameupload = "User-";
        }

        string getPath = ConfigurationSettings.AppSettings["PathFile_EN"];
        string filePath = Server.MapPath(getPath + DocCode.Text);
        DirectoryInfo dir = new DirectoryInfo(filePath);
        HttpFileCollection hfc = Request.Files;

        SearchDirectories(dir, DocCode.Text);




        if (checkfile != "11")
        {
            hfc = Request.Files;
            int ocount = dir.GetFiles().Length; //นับจำนวน file ใน folder

            //upload files
            if (hfc.Count > 0)
            {
                for (int i = ocount; i < (hfc.Count + ocount); i++)
                {
                    HttpPostedFile hpfLo_comment = hfc[i - ocount];
                    if (hpfLo_comment.ContentLength > 1)
                    {
                        string getPath_ = ConfigurationSettings.AppSettings["PathFile_EN"];
                        string RECode1 = DocCode.Text;
                        string fileName1 = nameupload + RECode1 + i;
                        string filePath1 = Server.MapPath(getPath + RECode1);
                        if (!Directory.Exists(filePath1))
                        {
                            Directory.CreateDirectory(filePath1);
                        }
                        string extension = Path.GetExtension(hpfLo_comment.FileName);

                        hpfLo_comment.SaveAs(Server.MapPath(getPath + RECode1) + "\\" + fileName1 + extension);
                    }
                }
                Insert_File();
            }
        }
        else
        {
            hfc = Request.Files;
            if (hfc.Count > 0)
            {
                for (int ii = 0; ii < hfc.Count; ii++)
                {
                    HttpPostedFile hpfLo = hfc[ii];
                    if (hpfLo.ContentLength > 1)
                    {
                        string _getPath = ConfigurationSettings.AppSettings["PathFile_EN"];
                        string RECode1 = DocCode.Text;
                        string fileName1 = nameupload + RECode1 + ii;
                        string filePath1 = Server.MapPath(getPath + RECode1);
                        if (!Directory.Exists(filePath1))
                        {
                            Directory.CreateDirectory(filePath1);
                        }
                        string extension = Path.GetExtension(hpfLo.FileName);

                        hpfLo.SaveAs(Server.MapPath(getPath + RECode1) + "\\" + fileName1 + extension);
                    }
                }
                //text.Text = "hasfile";
                Insert_File();
            }
        }

        SearchDirectories(dir, DocCode.Text);
    }

    #endregion

    #endregion

    #region bind Node

    protected void setFormData()
    {

        HiddenField hfM0NodeIDX = (HiddenField)FvDetailRepair.FindControl("hfM0NodeIDX");
        ViewState["m0_node"] = hfM0NodeIDX.Value;

        int m0_node = int.Parse(ViewState["m0_node"].ToString());


        switch (m0_node)
        {
            case 1: // สร้าง/แก้ไข

                setFormDataActor();

                break;
            case 2: // พิจารณาผลโดยผู้สร้าง
            case 3: // ดำเนินการโดยเจ้าหน้าที่
            case 4: // พิจารณาผลโดยเจ้าหน้าที่(รับงาน)
            case 5: // ปิดงานโดยเจ้าหน้าที่ 
                setFormDataActor();

                break;

            case 9: // จบการดำเนินการ

                setFormDataActor();

                break;

        }

    }

    #endregion

    #region bind Actor

    protected void setFormDataActor()
    {

        HiddenField hfM0ActoreIDX = (HiddenField)FvDetailRepair.FindControl("hfM0ActoreIDX");
        HiddenField hfM0StatusIDX = (HiddenField)FvDetailRepair.FindControl("hfM0StatusIDX");
        Label LbEmpIDX = (Label)FvDetailUserRepair.FindControl("LbEmpIDX");
        Label LbRDeptIDX = (Label)FvDetailUserRepair.FindControl("LbRDeptIDX");
        Control BoxAdmin = (Control)FvDetailRepair.FindControl("BoxAdmin");
        Control BoxAdminReceive = (Control)FvDetailRepair.FindControl("BoxAdminReceive");
        Control BoxAdminCloseJob = (Control)FvDetailRepair.FindControl("BoxAdminCloseJob");
        Control BoxUserCloseJob = (Control)FvDetailRepair.FindControl("BoxUserCloseJob");
        Label lblcommentadminclosejob = (Label)FvDetailRepair.FindControl("lblcommentadminclosejob");
        Label lblprice = (Label)FvDetailRepair.FindControl("lblprice");
        TextBox txtprice = (TextBox)FvCloseJob.FindControl("txtprice");
        TextBox CommentAdmin = (TextBox)FvCloseJob.FindControl("CommentAdmin");
        TextBox CommentUser = (TextBox)FvApproveUser.FindControl("CommentUser");


        ViewState["m0_actor"] = hfM0ActoreIDX.Value;
        int m0_actor = int.Parse(hfM0ActoreIDX.Value);

        //litDebug.Text = ViewState["m0_node"].ToString() + "," + ViewState["m0_actor"].ToString(); // + "," + ViewState["Returncode_closejobsap"].ToString() + "," + ViewState["SysIDX"].ToString();


        switch (m0_actor)
        {

            case 1: // ผู้สร้าง


                if (ViewState["m0_node"].ToString().ToString() == "2" && LbRDeptIDX.Text == ViewState["rdept_idx"].ToString())
                {
                    panel_user_closejob.Visible = true;
                    CommentUser.Text = String.Empty;
                }
                else
                {
                    panel_user_closejob.Visible = false;
                }
                BoxAdmin.Visible = true;
                BoxAdminReceive.Visible = true;
                BoxAdminCloseJob.Visible = true;
                BoxUserCloseJob.Visible = false;
                panel_admin_getjob.Visible = false;
                panel_admin_closejob.Visible = false;

                if (ViewState["SysIDX"].ToString() == "11")
                {
                    panel_linkpmms.Visible = false;
                }
                else
                {
                    panel_linkpmms.Visible = true;
                }

                break;

            case 2: // เจ้าหน้าที่ 

                if (int.Parse(ViewState["Sec_idx"].ToString()) == secit || int.Parse(ViewState["rdept_idx"].ToString()) == depten || int.Parse(ViewState["rdept_idx"].ToString()) == 134)
                {
                    if (ViewState["m0_node"].ToString().ToString() == "4")
                    {
                        if (ViewState["SysIDX"].ToString() == "11")
                        {
                            panel_admin_getjob.Visible = true;
                            panel_linkpmms.Visible = false;
                        }
                        else
                        {
                            panel_admin_getjob.Visible = false;
                            panel_linkpmms.Visible = true;
                        }

                        panel_admin_closejob.Visible = false;
                        BoxAdmin.Visible = false;
                        txtcommentreceive.Text = String.Empty;
                    }
                    else if (ViewState["m0_node"].ToString().ToString() == "3")
                    {
                        panel_admin_getjob.Visible = false;
                        panel_admin_closejob.Visible = true;
                        BoxAdmin.Visible = true;
                        BoxAdminReceive.Visible = true;
                        BoxAdminCloseJob.Visible = false;
                        BoxUserCloseJob.Visible = false;
                        BoxChosseCloseJob.Visible = true;

                        if (ViewState["SysIDX"].ToString() == "11")
                        {
                            panel_admin_closejob.Visible = true;
                            panel_linkpmms.Visible = false;
                        }
                        else
                        {
                            panel_admin_closejob.Visible = false;
                            panel_linkpmms.Visible = true;
                        }

                        if (hfM0StatusIDX.Value == "9")
                        {
                            BoxUserCloseJob.Visible = true;
                        }
                    }
                    else if (ViewState["m0_node"].ToString().ToString() == "5")
                    {
                        panel_admin_getjob.Visible = false;
                        panel_admin_closejob.Visible = true;
                        BoxAdmin.Visible = true;
                        BoxAdminReceive.Visible = true;
                        BoxAdminCloseJob.Visible = true;
                        AdminAcceptCloseJob.Visible = true;
                        BoxUserCloseJob.Visible = true;
                        BoxChosseCloseJob.Visible = false;

                        if (ViewState["SysIDX"].ToString() == "11")
                        {
                            panel_admin_closejob.Visible = true;
                            panel_linkpmms.Visible = false;
                        }
                        else
                        {
                            panel_admin_closejob.Visible = false;
                            panel_linkpmms.Visible = true;
                        }
                        //  txtprice.Text = lblprice.Text;
                        CommentAdmin.Text = lblcommentadminclosejob.Text;

                    }
                }
                else
                {
                    panel_admin_getjob.Visible = false;
                    panel_admin_closejob.Visible = false;
                    BoxAdmin.Visible = true;
                    BoxAdminReceive.Visible = true;
                    BoxAdminCloseJob.Visible = false;
                    BoxUserCloseJob.Visible = false;

                    if (ViewState["m0_node"].ToString().ToString() == "4")
                    {
                        BoxAdminCloseJob.Visible = true;
                        BoxAdmin.Visible = false;

                        if (ViewState["SysIDX"].ToString() == "11")
                        {
                            panel_linkpmms.Visible = false;
                        }
                        else
                        {
                            panel_linkpmms.Visible = true;
                        }

                    }
                    else if (ViewState["m0_node"].ToString().ToString() == "3" && LbEmpIDX.Text == ViewState["EmpIDX"].ToString() && hfM0StatusIDX.Value == "9")
                    {
                        BoxUserCloseJob.Visible = true;
                    }
                    else if (ViewState["m0_node"].ToString().ToString() == "5")
                    {
                        panel_admin_getjob.Visible = false;
                        panel_admin_closejob.Visible = false;
                        BoxAdmin.Visible = true;
                        BoxAdminReceive.Visible = true;
                        BoxAdminCloseJob.Visible = true;
                        AdminAcceptCloseJob.Visible = true;
                        BoxUserCloseJob.Visible = true;
                        BoxChosseCloseJob.Visible = false;

                        if (ViewState["SysIDX"].ToString() == "11")
                        {
                            panel_linkpmms.Visible = false;
                        }
                        else
                        {
                            panel_linkpmms.Visible = true;
                        }
                    }
                }
                if (ViewState["m0_node"].ToString().ToString() == "9")
                {
                    BoxUserCloseJob.Visible = true;
                    BoxAdmin.Visible = true;
                    BoxAdminReceive.Visible = true;
                    BoxAdminCloseJob.Visible = true;
                    panel_admin_getjob.Visible = false;
                    panel_admin_closejob.Visible = false;
                    panel_linkpmms.Visible = false;

                    if (ViewState["SysIDX"].ToString() == "11")
                    {
                        panel_linkpmms.Visible = false;
                    }
                    else
                    {
                        panel_linkpmms.Visible = true;
                    }
                }

                break;
        }

    }

    #endregion

    #region Link for Email
    protected string LinktoPMMS(int RPIDX, int node, int actor)
    {
        string urlendcry = linkquest + _funcTool.getEncryptRC4(RPIDX.ToString() + "|" + node.ToString() + "|" + actor.ToString(), keyenrepair);

        return urlendcry;
    } // สำหรับลิ้งแจ้ง email
    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        TypeMachineDetail _mastermachine = new TypeMachineDetail();
        StatusListDetail _masterstatus = new StatusListDetail();

        LinkButton btnAdddata = (LinkButton)FvInsertRepair.FindControl("btnAdddata");
        Label DocCode = (Label)FvDetailRepair.FindControl("DocCode");


        switch (cmdName)
        {

            case "btnindex":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);


                break;

            case "btninsert":
                SetViewInsert();
                break;

            case "btnreport":
                SetViewReportEN();

                break;

            case "btnreportquestion":
                // SetViewReportQuestion();

                string emp_fb = ViewState["EmpIDX"].ToString();
                string rdept_fb = ViewState["rdept_idx"].ToString();
                string url_fb = "http://www.taokaenoi.co.th/MAS/ReportTMMSFB";


                HttpResponse response_fb = HttpContext.Current.Response;
                response_fb.Clear();

                StringBuilder s_fb = new StringBuilder();
                s_fb.Append("<html>");
                s_fb.AppendFormat("<body onload='document.forms[\"form\"].submit()'>");
                s_fb.AppendFormat("<form name='form' action='{0}' method='post'>", url_fb);
                s_fb.Append("<input type='hidden' name='emp_idx' value=" + emp_fb + " />");
                s_fb.Append("<input type='hidden' name='rdept_idx' value=" + rdept_fb + " />");

                s_fb.Append("</form></body></html>");
                response_fb.Write(s_fb.ToString());
                response_fb.End();
                break;

            case "btnmastertypemachine":
                SetViewMasterTypeMachine();

                break;

            case "btnmasterstatus":
                SetViewMasterStatus();
                break;

            case "BtnHideSETBoxAllSearchShow":

                Panel_Add.Visible = true;
                BoxButtonSearchShow.Visible = false;
                BoxButtonSearchHide.Visible = true;
                litDebug.Text = String.Empty;

                break;

            case "BtnHideSETBoxAllSearchHide":

                Panel_Add.Visible = false;
                BoxButtonSearchShow.Visible = true;
                BoxButtonSearchHide.Visible = false;
                litDebug.Text = String.Empty;


                break;

            case "BtnHideSETBoxAllSearchShowStatus":

                Panel_AddStatus.Visible = true;
                BoxButtonSearchShowStatus.Visible = false;
                BoxButtonSearchHideStatus.Visible = true;
                litDebug.Text = String.Empty;
                Select_Downtime(ddl_downtime);

                break;

            case "BtnHideSETBoxAllSearchHideStatus":

                Panel_AddStatus.Visible = false;
                BoxButtonSearchShowStatus.Visible = true;
                BoxButtonSearchHideStatus.Visible = false;
                litDebug.Text = String.Empty;


                break;

            case "btnInsertmaster_typemachine":
                InsertMaster_TypeMachine();
                SelectMaster_TypeMachine();
                Panel_Add.Visible = false;
                txtaddcode.Text = String.Empty;
                txtaddnameen.Text = String.Empty;
                txtaddnameth.Text = String.Empty;
                BoxButtonSearchShow.Visible = true;
                BoxButtonSearchHide.Visible = false;

                break;

            case "btnInsertmaster_status":
                InsertMaster_Status();
                SelectMaster_Status();
                Panel_AddStatus.Visible = false;
                txtaddstatusname.Text = String.Empty;
                ddl_downtime.SelectedValue = "0";

                BoxButtonSearchShowStatus.Visible = true;
                BoxButtonSearchHideStatus.Visible = false;
                break;

            case "BtnBack":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnDelete_mastermachine":
                int TmcIDX = int.Parse(cmdArg);

                _dtmachine.BoxTypeMachine = new TypeMachineDetail[1];

                _mastermachine.TmcIDX = TmcIDX;
                _mastermachine.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

                _dtmachine.BoxTypeMachine[0] = _mastermachine;

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));

                _dtmachine = callServicePostENRepair(urlDelete_TypeMachineList, _dtmachine);

                SelectMaster_TypeMachine();
                break;

            case "btnDelete_masterstatus":
                int stidx = int.Parse(cmdArg);

                _dtmachine.BoxStatusList = new StatusListDetail[1];

                _masterstatus.stidx = stidx;
                _masterstatus.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

                _dtmachine.BoxStatusList[0] = _masterstatus;

                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));

                _dtmachine = callServicePostENRepair(urlDelete_StatuseList, _dtmachine);

                SelectMaster_Status();
                break;

            case "btncancelmasterstatus":
                SetViewMasterStatus();
                break;

            case "btncancelmaster":
                SetViewMasterTypeMachine();
                break;

            case "CmdInsert":
                linkBtnTrigger(btnAdddata);
                Insert_Repair();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "ViewENRepair":
                MvMaster.SetActiveView(ViewDetailIndex);

                string[] arg_detail = new string[2];
                arg_detail = e.CommandArgument.ToString().Split(';');
                ViewState["IDX"] = arg_detail[0];
                ViewState["SysIDX"] = arg_detail[1];

                Select_DetailUserIndex();

                Control divshowdatemange = (Control)FvDetailRepair.FindControl("divshowdatemange");

                if (ViewState["system"].ToString() == "11")
                {
                    divshowdatemange.Visible = false;
                }
                else
                {
                    divshowdatemange.Visible = true;
                }

                Select_Comment();
                setFormData();
                linkBtnTrigger(btnlink);

                try
                {

                    string getPathLotus = ConfigurationSettings.AppSettings["PathFile_EN"];

                    string filePathLotus = Server.MapPath(getPathLotus + ViewState["RECode"].ToString());
                    DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

                    SearchDirectories(myDirLotus, ViewState["RECode"].ToString());

                    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('" + myDirLotus + "');", true);



                }
                catch
                {

                }


                break;

            case "CmdAdminGetJob":

                AdminGetJob_System();
                MvMaster.SetActiveView(ViewIndex);
                SetViewIndex();
                Select_Index();
                //   Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "CmdAdminCloseJob":
                AdminCloseJob_System();

                string getPath = ConfigurationSettings.AppSettings["PathFile_EN"];
                string filePath = Server.MapPath(getPath + DocCode.Text);
                DirectoryInfo dir = new DirectoryInfo(filePath);
                HttpFileCollection hfc = Request.Files;

                SearchDirectories(dir, DocCode.Text);


                if (checkfile != "11")
                {
                    hfc = Request.Files;
                    int ocount = dir.GetFiles().Length; //นับจำนวน file ใน folder
                    string adminupload = "Admin-";
                    //upload files
                    if (hfc.Count > 0)
                    {
                        for (int i = ocount; i < (hfc.Count + ocount); i++)
                        {
                            HttpPostedFile hpfLo_comment = hfc[i - ocount];
                            if (hpfLo_comment.ContentLength > 1)
                            {
                                string getPath_ = ConfigurationSettings.AppSettings["PathFile_EN"];
                                string RECode1 = DocCode.Text;
                                string fileName1 = adminupload + RECode1 + i;
                                string filePath1 = Server.MapPath(getPath + RECode1);
                                if (!Directory.Exists(filePath1))
                                {
                                    Directory.CreateDirectory(filePath1);
                                }
                                string extension = Path.GetExtension(hpfLo_comment.FileName);

                                hpfLo_comment.SaveAs(Server.MapPath(getPath + RECode1) + "\\" + fileName1 + extension);
                            }
                        }
                        Insert_File();
                    }
                }
                MvMaster.SetActiveView(ViewIndex);
                SetViewIndex();
                Select_Index();

                //  Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "CmdUserCloseJob":
                UserCloseJob_System();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "CmdSaveComment":
                InsertComment_System();
                Uploadfile();
                Select_Comment();
                txtcomment.Text = String.Empty;
                break;

            case "ViewHistory":
                string[] arg_history = new string[1];
                arg_history = e.CommandArgument.ToString().Split(';');
                ViewState["IDX"] = arg_history[0];

                MvMaster.SetActiveView(ViewHistory);
                Select_History();
                break;

            case "btnsearch":
                _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
                RepairMachineDetail _repairsearch = new RepairMachineDetail();

                _repairsearch.STAppIDX = int.Parse(ddlSearchStatus.SelectedValue);
                _repairsearch.OrgIDX = int.Parse(ddlSearchOrg.SelectedValue);
                _repairsearch.RDeptIDX = int.Parse(ddlSearchDep.SelectedValue);
                _repairsearch.EmpCode = txtEmpIDX.Text;
                _repairsearch.IFSearch = int.Parse(ddlTypeSearchDate.SelectedValue);
                _repairsearch.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
                _repairsearch.DategetJobSearch = AddStartdate.Text;
                _repairsearch.DatecloseJobSearch = AddEndDate.Text;
                _repairsearch.LocIDX = int.Parse(ddllocate_search.SelectedValue);
                _repairsearch.BuildingIDX = int.Parse(ddlbuilding_search.SelectedValue);
                _repairsearch.RoomIDX = int.Parse(ddlroom_search.SelectedValue);
                _repairsearch.AdminEmpIDX = int.Parse(ddladmin.SelectedValue);
                _repairsearch.TmcIDX = int.Parse(ddltype_search.SelectedValue);
                _repairsearch.MCIDX = int.Parse(ddlmachine_search.SelectedValue);
                _repairsearch.SysIDX = int.Parse(ddlsystem.SelectedValue);
                _repairsearch.RECode = txtrecode.Text;
                _dtmachine.BoxRepairMachine[0] = _repairsearch;

                ViewState["BoxSearchIndex"] = _dtmachine;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(ViewState["BoxSearchIndex"]));
                Select_Index();
                break;

            case "BtnHideSETBoxAllSearchShow_Search":

                SETBoxAllSearch.Visible = true;
                BoxButtonSearchShow_search.Visible = false;
                BoxButtonSearchHide_search.Visible = true;
                break;

            case "BtnHideSETBoxAllSearchHide_Search":

                SETBoxAllSearch.Visible = false;
                BoxButtonSearchShow_search.Visible = true;
                BoxButtonSearchHide_search.Visible = false;

                break;

            case "btnmanual":


                Response.Write("<script>window.open('http://demo.taokaenoi.co.th/images/ENRepair/คู่มือระบบRepairMachine.pdf','_blank');</script>");

                break;


            case "btnflow":

                Response.Write("<script>window.open('http://demo.taokaenoi.co.th/images/ENRepair/FlowChart.jpg','_blank');</script>");

                break;

            case "btnsearch_report":
                gridreport.Visible = true;
                Select_ReportTable();
                break;


            case "CmdExport":

                _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
                RepairMachineDetail _repairreport = new RepairMachineDetail();

                _repairreport.STAppIDX = int.Parse(ddlReportStatus.SelectedValue);
                _repairreport.OrgIDX = int.Parse(ddlReportOrg.SelectedValue);
                _repairreport.RDeptIDX = int.Parse(ddlReporthDep.SelectedValue);
                _repairreport.EmpCode = txtempname.Text;
                _repairreport.IFSearch = int.Parse(ddlTypeSearchDate_Report.SelectedValue);
                _repairreport.IFSearchbetween = int.Parse(ddlSearchDate_Report.SelectedValue);
                _repairreport.DategetJobSearch = AddStartDate_Report.Text;
                _repairreport.DatecloseJobSearch = AddEndDate_Report.Text;
                _repairreport.LocIDX = int.Parse(ddllocate_report.SelectedValue);
                _repairreport.BuildingIDX = int.Parse(ddlbuilding_report.SelectedValue);
                _repairreport.RoomIDX = int.Parse(ddlroom_report.SelectedValue);
                _repairreport.AdminReciveEmpIDX = int.Parse(ddladminreceive_report.SelectedValue);
                _repairreport.AdminEmpIDX = int.Parse(ddladminaccept_report.SelectedValue);
                _repairreport.TmcIDX = int.Parse(ddltype_report.SelectedValue);
                _repairreport.MCIDX = int.Parse(ddlmachine_report.SelectedValue);
                _repairreport.SysIDX = int.Parse(ddlSysIDX.SelectedValue);

                _dtmachine.BoxRepairMachine[0] = _repairreport;
                // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));

                _dtmachine = callServicePostENRepair(urlReport_ExportData, _dtmachine);
                GridView2.DataSource = _dtmachine.BoxRepairMachine_Export;
                GridView2.DataBind();

                GridView2.AllowSorting = false;
                GridView2.AllowPaging = false;

                Response.Clear();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment;filename=GridViewExport.xls");
                // Response.Charset = ""; set character 
                Response.Charset = "utf-8";
                Response.ContentEncoding = System.Text.Encoding.GetEncoding("utf-8");
                Response.ContentType = "application/vnd.ms-excel";

                Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
                Response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");


                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);


                GridView2.Columns[0].Visible = true;
                GridView2.HeaderRow.BackColor = Color.White;

                foreach (TableCell cell in GridView2.HeaderRow.Cells)
                {
                    cell.BackColor = GridView2.HeaderStyle.BackColor;
                }

                foreach (GridViewRow row in GridView2.Rows)
                {
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = GridView2.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = GridView2.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }

                GridView2.RenderControl(hw);

                //style to format numbers to string
                string style = @"<style> .textmode { mso-number-format:\@; font-family: myFirstFont; } </style>";
                Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();


                break;

            case "ViewENRepairQuestion":
                string[] arg_quest = new string[1];
                arg_quest = e.CommandArgument.ToString().Split(';');
                ViewState["RPIDX"] = arg_quest[0];

                MvMaster.SetActiveView(ViewQuestion);
                Select_Questionnair();
                break;

            case "CmdInsertQuestion":
                Insert_GvQuestionEN_List();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnsearch_chart":
                gridreport.Visible = false;
                grant.Visible = true;

                switch (ddltypechart.SelectedValue)
                {
                    case "1":
                        SelectChartLV1();
                        break;

                    case "2":
                        SelectChartLV2();
                        break;

                    case "3":

                        SelectChartLV3();
                        break;

                    case "4":
                        SelectChartLV4();
                        break;

                    case "5":
                        SelectChartLV5();
                        break;
                }


                break;

            case "BtnBack_report":
                ddltypereport.SelectedValue = "0";
                Panel_Table.Visible = false;
                Panel_Chart.Visible = false;
                Panel_from.Visible = false;
                gridreport.Visible = false;
                grant.Visible = false;
              //  gridreport_from.Visible = false;
                ddlSysIDX_chart.Enabled = true;
                ddlSysIDX_chart.SelectedValue = "0";
                SetViewReportEN();
                break;

            case "btnlink":
                string urlendcry = LinktoPMMS(int.Parse(ViewState["IDX"].ToString()), int.Parse(ViewState["m0_node"].ToString()), int.Parse(ViewState["m0_actor"].ToString()));

                Response.Write("<script>window.open('" + urlendcry + "','_blank');</script>");


                break;
            case "btnsearch_from":
               // gridreport_from.Visible = true;
                SetDefault_Reportfrom();
                break;
            case "btnclearsearch_from":
                ddllocate_from.SelectedValue = "0";
                ddlbuilding_from.SelectedValue = "0";
                ddladminreceive_from.SelectedValue = "0";
                txtStartDate_from.Text = "";
                //gridreport_from.Visible = false;
                SetDefault_Reportfrom();
                pnl_print_plan.Visible = false;
                pnl_alert.Visible = false;
                break;
        }
    }
    #endregion

    #region get zSys Name

    public string getSysName(int iSysIDX)
    {
        string sReturn = "";
        if (iSysIDX == 11)
        {
            sReturn = "แจ้งซ่อม";
        }
        else if (iSysIDX == 27)
        {
            sReturn = "PM";
        }

        return sReturn;
    }

    #endregion

    public string SetDefault_Reportfrom()
    {
       
        data_en_planning dataenplanning = new data_en_planning();
        dataenplanning = _func_dmu.zENLookupDataList(dataenplanning, "org_en_plan");
        litTitle1.Text = "";
        if (dataenplanning.en_lookup_action != null)
        {
            litTitle1.Text = dataenplanning.en_lookup_action[0].zName;
        }
        litTitle2.Text = "แบบฟอร์มใบบันทึก รายงานประจำวัน";

        dataenplanning = new data_en_planning();
        dataenplanning = _func_dmu.zENLookupDataList(dataenplanning, "iso_plan_year",2);
        litiso.Text = "";
        if (dataenplanning.en_lookup_action != null)
        {
            litiso.Text = dataenplanning.en_lookup_action[0].zName;
        }


        DataMachine _dtmachine = new DataMachine();
        _dtmachine.BoxRepairMachine = new RepairMachineDetail[1];
        RepairMachineDetail _repairreport = new RepairMachineDetail();
        _repairreport.SysIDX = 11;
        _repairreport.IFSearch = 1;
        _repairreport.IFSearchbetween = 0;
        _repairreport.DategetJobSearch = txtStartDate_from.Text;
        _repairreport.LocIDX = _func_dmu.zStringToInt(ddllocate_from.SelectedValue);
        _repairreport.BuildingIDX = _func_dmu.zStringToInt(ddlbuilding_from.SelectedValue);
        _repairreport.AdminReciveEmpIDX = _func_dmu.zStringToInt(ddladminreceive_from.SelectedValue);

        _dtmachine.BoxRepairMachine[0] = _repairreport;

       // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtmachine));
        _dtmachine = callServicePostENRepair(urlReport_Table, _dtmachine);
        //setGridData(GvReport, _dtmachine.BoxRepairMachine);
        string _string = "";
        string _sTR = "";
        string _sTD = "";

        if (_dtmachine.BoxRepairMachine != null)
        {
            int ic = 0;
            foreach (var item in _dtmachine.BoxRepairMachine)
            {
                ic++;
                _sTD = " <td style=\"font-size: 13px; font-family: serif;border: 1px solid #ddd;\" ><center>" + ic.ToString() + "</center></td> " +
                       " <td style=\"font-size: 13px; font-family: serif;border: 1px solid #ddd;\" >" + item.DateReceive + "</td> " +
                       " <td style=\"font-size: 13px; font-family: serif;border: 1px solid #ddd;\" >" + item.DateClose + "</td> " +
                       " <td style=\"font-size: 13px; font-family: serif;border: 1px solid #ddd;\" >" + item.CommentUser + "</td> " +
                       " <td style=\"font-size: 13px; font-family: serif;border: 1px solid #ddd;\" >" + item.CommentAdmin + "</td> " +
                       " <td style=\"font-size: 13px; font-family: serif;border: 1px solid #ddd;\" >" + item.RECode + "</td> " +
                       " <td style=\"font-size: 13px; font-family: serif;border: 1px solid #ddd;\" >" + item.status_name + "</td> " +
                       " <td style=\"font-size: 13px; font-family: serif;border: 1px solid #ddd;\" >" + item.NameAdminRe + "</td> " +
                       " <td style=\"font-size: 13px; font-family: serif;border: 1px solid #ddd;\" >" + item.NameUser + "</td> ";
                _sTR = "<tr> " + _sTD + " </tr>";
                _string = _string + _sTR;
            }
            pnl_print_plan.Visible = true;
            pnl_alert.Visible = false;
          //  litDebug.Text = "1";
        }
        else
        {
            _sTD = " <td colspan=\"9\"  style=\"font-size: 16px; font-family: serif;border: 1px solid #ddd;\" ><center> ไม่พบข้อมูล </center></td> ";
            _sTR = "<tr> " + _sTD + " </tr>";
            _string = _string + _sTR;
            pnl_print_plan.Visible = false;
            pnl_alert.Visible = true;
           // litDebug.Text = "2";
        }

        return _string;
    }
    
    public string getHtml()
    {
        return SetDefault_Reportfrom();
    }
    public string getLocation_lb()
    {
        return _func_dmu.zGetDataDropDownList(ddlbuilding_from);
    }
    public string getname_lb()
    {
        return _func_dmu.zGetDataDropDownList(ddladminreceive_from);
    }
    public string getdate_lb()
    {
        return txtStartDate_from.Text;
    }

}