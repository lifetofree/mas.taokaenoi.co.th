﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_laws_law_trademark_permise : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();
    service_execute serviceexcute = new service_execute();
    data_employee _dataEmployee = new data_employee();

    data_law _data_law = new data_law();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];

    static string _urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlGetPlace"];

    //-- employee --//

    //-- promise --//
    static string _urlGetLawPMtypepromise = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawPMtypepromise"];
    static string _urlGetLawPMSubtypepromise = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawPMSubtypepromise"];

    static string _urlSetDocumentPromise = _serviceUrl + ConfigurationManager.AppSettings["urlSetDocumentPromise"];
    static string _urlGetLawPMDocDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawPMDocDetail"];
    static string _urlGetLawPMViewDocDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawPMViewDocDetail"];
    static string _urlGetLawPMLogViewDocDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawPMLogViewDocDetail"];
    static string _urlGetCountLawPMWaitApprove = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountLawPMWaitApprove"];
    static string _urlGetLawPMWaitApproveDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawPMWaitApproveDetail"];
    static string _urlGetLawStatusApprove = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawStatusApprove"];
    static string _urlSetApproveDocumentPromise = _serviceUrl + ConfigurationManager.AppSettings["urlSetApproveDocumentPromise"];
    static string _urlGetSelectLawInDocument = _serviceUrl + ConfigurationManager.AppSettings["urlGetSelectLawInDocument"];
    static string _urlGetLawPMLawOfficer = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawPMLawOfficer"];
    static string _urlGetLawPMFileDocument = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawPMFileDocument"];


    //-- file path --//
    static string _path_file_law_filepermise = ConfigurationManager.AppSettings["path_file_law_filepermise"];
    static string _path_file_law_usercreatefilepermise = ConfigurationManager.AppSettings["path_file_law_usercreatefilepermise"];
    static string _path_file_law_usercheckfilepermise = ConfigurationManager.AppSettings["path_file_law_usercheckfilepermise"];
    static string _path_file_law_lawscriptfilepermise = ConfigurationManager.AppSettings["path_file_law_lawscriptfilepermise"];
    static string _path_file_law_usercheckscriptfile = ConfigurationManager.AppSettings["path_file_law_usercheckscriptfile"];
    static string _path_file_law_lawsave_permise = ConfigurationManager.AppSettings["path_file_law_lawsave_permise"];
    

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;

    int returnResult = 0;
    int condition_ = 0;
    int _set_statusFilter = 0;
    string _sumhour = "";
    string _total_floor = "";
    string _total_time = "";

    //set rpos hr tab approve
    string set_rpos_idx_hr = "5903,5906";

    //ViewState["vs_ViewWaitApproveRoomBooking"]

    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());

        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

        ViewState["org_permission"] = _dataEmployee.employee_list[0].org_idx;
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["joblevel_permission"] = _dataEmployee.employee_list[0].jobgrade_level;
        ViewState["emp_type_permission"] = _dataEmployee.employee_list[0].emp_type_idx;

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
            initPageLoad();

            getCountWaitApprove();

            linkBtnTrigger(lbCreate);
            linkBtnTrigger(lbApprove);

        }
    }

    #region set/get bind data
    protected void getPMtypeDetail(DropDownList ddlName, int _type_idx)
    {

        data_law data_m0_type_detail = new data_law();
        pm_m0_typepromise_detail m0_type_detail = new pm_m0_typepromise_detail();
        data_m0_type_detail.pm_m0_typepromise_detail_list = new pm_m0_typepromise_detail[1];
        
        data_m0_type_detail.pm_m0_typepromise_detail_list[0] = m0_type_detail;

        data_m0_type_detail = callServicePostLaw(_urlGetLawPMtypepromise, data_m0_type_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_type_detail));
        setDdlData(ddlName, data_m0_type_detail.pm_m0_typepromise_detail_list, "type_document_en", "type_document_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกประเภทเอกสาร ---", "0"));

        if(_type_idx > 0)
        {
            ddlName.SelectedValue = _type_idx.ToString();
        }
        
      
    }

    protected void getPMSubtypeDetail(DropDownList ddlName, int _type_idx)
    {

        data_law data_m0_subtype_detail = new data_law();
        pm_m0_subtypepromise_detail m0_subtype_detail = new pm_m0_subtypepromise_detail();
        data_m0_subtype_detail.pm_m0_subtypepromise_detail_list = new pm_m0_subtypepromise_detail[1];

        data_m0_subtype_detail.pm_m0_subtypepromise_detail_list[0] = m0_subtype_detail;

        data_m0_subtype_detail = callServicePostLaw(_urlGetLawPMSubtypepromise, data_m0_subtype_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_type_detail));
        setDdlData(ddlName, data_m0_subtype_detail.pm_m0_subtypepromise_detail_list, "subtype_document_en", "subtype_document_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกรูปแบบเอกสาร ---", "0"));

        if(_type_idx > 0)
        {
            ddlName.SelectedValue = _type_idx.ToString();
        }
        

    }

    protected void getDetailPromise(GridView gvName, int _u0_docidx, int _cemp_idx)
    {

        data_law data_u0_detail = new data_law();
        pm_u0_document_detail u0_doc_detail = new pm_u0_document_detail();
        data_u0_detail.pm_u0_document_detail_list = new pm_u0_document_detail[1];

        u0_doc_detail.u0_doc_idx = _u0_docidx;
        u0_doc_detail.cemp_idx = _cemp_idx;
        
        data_u0_detail.pm_u0_document_detail_list[0] = u0_doc_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_detail));
        data_u0_detail = callServicePostLaw(_urlGetLawPMDocDetail, data_u0_detail);
       

        ViewState["Vs_DetailPromiseAll"] = data_u0_detail.pm_u0_document_detail_list;
        ViewState["Vs_DetailPromise"] = data_u0_detail.pm_u0_document_detail_list;
        setGridData(gvName, ViewState["Vs_DetailPromise"]);
       
    }

    protected void getViewDetailPromise(FormView fvName, int _u0_doc_idx, int _node_idx)
    {

        data_law data_u0_viewdetail = new data_law();
        pm_u0_document_detail u0_doc_viewdetail = new pm_u0_document_detail();
        data_u0_viewdetail.pm_u0_document_detail_list = new pm_u0_document_detail[1];

        u0_doc_viewdetail.u0_doc_idx = _u0_doc_idx;

        data_u0_viewdetail.pm_u0_document_detail_list[0] = u0_doc_viewdetail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_viewdetail));
        data_u0_viewdetail = callServicePostLaw(_urlGetLawPMViewDocDetail, data_u0_viewdetail);
        

        ViewState["Vs_ViewDetailPromise"] = data_u0_viewdetail.pm_u0_document_detail_list;
        
        if(_node_idx != 15)
        {
            setFormData(fvName, FormViewMode.ReadOnly, ViewState["Vs_ViewDetailPromise"]);
        }
        else
        {
            setFormData(fvName, FormViewMode.Edit, ViewState["Vs_ViewDetailPromise"]);
        }
        
    }

    protected void getLogViewDetail(Repeater rptName, int _u0_doc_idx)
    {

        data_law data_u0_logviewdetail = new data_law();
        pm_u1_document_detail u0_doc_logviewdetail = new pm_u1_document_detail();
        data_u0_logviewdetail.pm_u1_document_detail_list = new pm_u1_document_detail[1];

        u0_doc_logviewdetail.u0_doc_idx = _u0_doc_idx;

        data_u0_logviewdetail.pm_u1_document_detail_list[0] = u0_doc_logviewdetail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_viewdetail));
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_logviewdetail));
        data_u0_logviewdetail = callServicePostLaw(_urlGetLawPMLogViewDocDetail, data_u0_logviewdetail);
        

        ViewState["vs_LogDetail"] = data_u0_logviewdetail.pm_u1_document_detail_list;
        setRepeaterData(rptName, ViewState["vs_LogDetail"]);

    }

    protected void getCountWaitApprove()
    {
        data_law data_countwait = new data_law();
        pm_u0_document_detail u0_doc_countwait = new pm_u0_document_detail();
        data_countwait.pm_u0_document_detail_list = new pm_u0_document_detail[1];

        u0_doc_countwait.cemp_idx = _emp_idx;
        data_countwait.pm_u0_document_detail_list[0] = u0_doc_countwait;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_countwait));
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_countwait));
        data_countwait = callServicePostLaw(_urlGetCountLawPMWaitApprove, data_countwait);
        
        //setGridData(gvName, data_u0_document_wait.tcm_u0_document_list);

        if (data_countwait.pm_u0_document_detail_list[0].count_waitApprove > 0)
        {
            ViewState["vs_CountWaitApprove"] = data_countwait.pm_u0_document_detail_list[0].count_waitApprove; // count all wait approve
            lbApprove.Text = " รายการรออนุมัติ <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
            setActiveTab("docApprove", 0, 0, 0, 0, 0, 0, 0);

        }
        else
        {
            ViewState["vs_CountWaitApprove"] = 0; // count all wait approve
            lbApprove.Text = " รายการรออนุมัติ <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
            setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);

        }


    }

    protected void getWaitApproveDetailPromise(GridView gvName, int _cemp_idx)
    {

        data_law data_u0_waitapprove = new data_law();
        pm_u0_document_detail u0_doc_waitapprove = new pm_u0_document_detail();
        data_u0_waitapprove.pm_u0_document_detail_list = new pm_u0_document_detail[1];

        u0_doc_waitapprove.cemp_idx = _cemp_idx;

        data_u0_waitapprove.pm_u0_document_detail_list[0] = u0_doc_waitapprove;

        data_u0_waitapprove = callServicePostLaw(_urlGetLawPMWaitApproveDetail, data_u0_waitapprove);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_u0_detail));


        ViewState["Vs_WaitApprovePromise"] = data_u0_waitapprove.pm_u0_document_detail_list;
        setGridData(gvName, ViewState["Vs_WaitApprovePromise"]);

    }

    protected void getDecisionApprove(DropDownList ddlName, int _noidx)
    {

        data_law data_m0_decision_detail = new data_law();
        tcm_m0_decision_detail m0_decision_detail = new tcm_m0_decision_detail();
        data_m0_decision_detail.tcm_m0_decision_list = new tcm_m0_decision_detail[1];

        m0_decision_detail.noidx = _noidx;

        data_m0_decision_detail.tcm_m0_decision_list[0] = m0_decision_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_m0_decision_detail = callServicePostLaw(_urlGetLawStatusApprove, data_m0_decision_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_decision_detail.tcm_m0_decision_list, "decision_name", "decision_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Status ---", "0"));


    }

    protected void getLawOfficer(CheckBoxList chkName, int _emp_idx)
    {

        data_law data_u3_document_detail = new data_law();
        tcm_u3_document_detail u3_document_detail = new tcm_u3_document_detail();
        data_u3_document_detail.tcm_u3_document_list = new tcm_u3_document_detail[1];

        data_u3_document_detail.tcm_u3_document_list[0] = u3_document_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_u3_document_detail = callServicePostLaw(_urlGetSelectLawInDocument, data_u3_document_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setChkData(chkName, data_u3_document_detail.tcm_u3_document_list, "emp_name_th", "emp_idx");
        //chkName.Items.Insert(0, new ListItem("--- Select ---", "0"));

    }

    protected void getDetailLawOfficer(GridView gvName, int _u0_docidx, Panel pnName)
    {

        data_law data_u2_detaillaw = new data_law();
        pm_u2_document_detail u2_detaillaw = new pm_u2_document_detail();
        data_u2_detaillaw.pm_u2_document_detail_list = new pm_u2_document_detail[1];

        u2_detaillaw.u0_doc_idx = _u0_docidx;

        data_u2_detaillaw.pm_u2_document_detail_list[0] = u2_detaillaw;

        data_u2_detaillaw = callServicePostLaw(_urlGetLawPMLawOfficer, data_u2_detaillaw);
        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_u0_detail));

        if(data_u2_detaillaw.return_code == 0)
        {
            pnName.Visible = true;
            gvName.Visible = true;
            ViewState["Vs_DetailLawOfficer"] = data_u2_detaillaw.pm_u2_document_detail_list;
            setGridData(gvName, ViewState["Vs_DetailLawOfficer"]);
        }
        else
        {
            pnName.Visible = false;
            gvName.Visible = false;
            ViewState["Vs_DetailLawOfficer"] = null;
            setGridData(gvName, ViewState["Vs_DetailLawOfficer"]);
        }
        

    }

    protected void getDetailFileAttrach(GridView gvName)
    {

        data_law data_docfile_ = new data_law();
        pm_u0_document_detail udoc_file = new pm_u0_document_detail();
        data_docfile_.pm_u0_document_detail_list = new pm_u0_document_detail[1];

        udoc_file.u0_doc_idx = 0;
        udoc_file.cemp_idx = _emp_idx;

        data_docfile_.pm_u0_document_detail_list[0] = udoc_file;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_docfile_));
        data_docfile_ = callServicePostLaw(_urlGetLawPMFileDocument, data_docfile_);

        ViewState["Vs_DetailFileUploadAll"] = data_docfile_.pm_u0_document_detail_list;
        ViewState["Vs_DetailFileUpload"] = data_docfile_.pm_u0_document_detail_list;
        setGridData(gvName, ViewState["Vs_DetailFileUpload"]);

    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
    }

    #endregion set/get bind data

    #region SelectedIndexChanged
    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        switch (ddlName.ID)
        {

            case "ddlMgLawApprove":

                DropDownList ddlMgLawApprove = (DropDownList)fvMgLaw.FindControl("ddlMgLawApprove");
                UpdatePanel _Panel_selectofficer_law = (UpdatePanel)fvMgLaw.FindControl("_Panel_selectofficer_law");
                if (ddlMgLawApprove.SelectedValue == "25") //select officer law to document
                {
                    _Panel_selectofficer_law.Visible = true;
                    getLawOfficer(chkLawOfficer, 0);
                }
                else
                {
                    _Panel_selectofficer_law.Visible = false;
                    chkLawOfficer.ClearSelection();
                }

                break;


        }
    }

    protected void rdoSelectedIndexChanged(object sender, EventArgs e)
    {
        RadioButtonList rdoName = (RadioButtonList)sender;
        switch (rdoName.ID)
        {
            case "rdoStepAssignment":


                break;
        }

    }

    protected void chkSelectedIndexChanged(object sender, EventArgs e)
    {
        CheckBox chkName = (CheckBox)sender;
        switch (chkName.ID)
        {
            case "chkAddRenewalDate":

                

                break;
         
        }

    }


    #endregion SelectedIndexChanged

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, 0, 0, 0, 0);

    }

    protected void ClickPrint(object sender, EventArgs e)
    {
        //  litDebug.Text = "lals";
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        //linkBtnTrigger(lbDocSaveImport);

        switch (cmdName)
        {

            case "cmdDetail":

                setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);

                break;
            case "cmdCancel":

                getCountWaitApprove();
                //setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);
                break;

            case "cmdDocCancel":
                getCountWaitApprove();
                break;

            case "cmdBackToDetail":
                setActiveTab("docDetail", 0, 0, 0, 0, 2, 0, 0);
                break;

            case "cmdSave":

                DropDownList ddlTypeDocument = (DropDownList)fvCreate.FindControl("ddlTypeDocument");
                DropDownList ddlSubTypeDocument = (DropDownList)fvCreate.FindControl("ddlSubTypeDocument");
                DropDownList ddlOrg = (DropDownList)fvCreate.FindControl("ddlOrg");
                TextBox txt_docreference = (TextBox)fvCreate.FindControl("txt_docreference");
                TextBox txt_contract_parties = (TextBox)fvCreate.FindControl("txt_contract_parties");
                TextBox txt_value = (TextBox)fvCreate.FindControl("txt_value");
                TextBox txt_topic = (TextBox)fvCreate.FindControl("txt_topic");
                TextBox txt_detail = (TextBox)fvCreate.FindControl("txt_detail");
                TextBox txt_date_start = (TextBox)fvCreate.FindControl("txt_date_start");
                TextBox txt_date_end = (TextBox)fvCreate.FindControl("txt_date_end");
                TextBox txt_contract_person = (TextBox)fvCreate.FindControl("txt_contract_person");
                TextBox txt_phone_number = (TextBox)fvCreate.FindControl("txt_phone_number");

                UpdatePanel UpdatePanel_FvCreate = (UpdatePanel)fvCreate.FindControl("UpdatePanel_FvCreate");
                FileUpload UploadFile = (FileUpload)UpdatePanel_FvCreate.FindControl("UploadFile");


                data_law data_insertu0_pm = new data_law();

                //insert u0 document
                pm_u0_document_detail u0_insertu0_pm = new pm_u0_document_detail();
                data_insertu0_pm.pm_u0_document_detail_list = new pm_u0_document_detail[1];

                u0_insertu0_pm.cemp_idx = _emp_idx;
                u0_insertu0_pm.acidx = 1;
                u0_insertu0_pm.noidx = 15;
                u0_insertu0_pm.decision = 21;
                u0_insertu0_pm.type_document_idx = int.Parse(ddlTypeDocument.SelectedValue);
                u0_insertu0_pm.subtype_document_idx = int.Parse(ddlSubTypeDocument.SelectedValue);
                u0_insertu0_pm.org_idx = int.Parse(ddlOrg.SelectedValue);
                u0_insertu0_pm.doc_reference = txt_docreference.Text;
                u0_insertu0_pm.contract_parties = txt_contract_parties.Text;
                u0_insertu0_pm.value = txt_value.Text;
                u0_insertu0_pm.topic = txt_topic.Text;
                u0_insertu0_pm.detail = txt_detail.Text;
                u0_insertu0_pm.date_start = txt_date_start.Text;
                u0_insertu0_pm.date_end = txt_date_end.Text;
                u0_insertu0_pm.contact_person = txt_contract_person.Text;
                u0_insertu0_pm.phone_number = txt_phone_number.Text;

                data_insertu0_pm.pm_u0_document_detail_list[0] = u0_insertu0_pm;
                //insert u0 document 
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_insertu0_pm));
               
              
                if(int.Parse(ddlTypeDocument.SelectedValue) == 1) //promise NDA
                {

                    data_insertu0_pm = callServicePostLaw(_urlSetDocumentPromise, data_insertu0_pm);

                    if (UploadFile.HasFile)
                    {
                        //litDebug.Text = "1";
                        //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0doc_insert));
                        ////data_u0doc_insert = callServicePostLaw(_urlSetLawCreateDocument, data_u0doc_insert);

                        string filepath_usercreate = Server.MapPath(_path_file_law_usercreatefilepermise);
                        HttpFileCollection uploadedFiles_usercreate = Request.Files;

                        string filePath2 = "usercreatedoc" + "-" + data_insertu0_pm.pm_u0_document_detail_list[0].u0_doc_idx.ToString();//data_insertu0_pm.tcm_u0_document_list[0].docrequest_code.ToString();
                        string filePath1 = Server.MapPath(_path_file_law_usercreatefilepermise + filePath2);

                        for (int i = 0; i < uploadedFiles_usercreate.Count; i++)
                        {
                            HttpPostedFile userpost_createfile = uploadedFiles_usercreate[i];

                            try
                            {
                                if (userpost_createfile.ContentLength > 0)
                                {

                                    //litDebug.Text += "MA620001";


                                    string _filepathExtension = Path.GetExtension(userpost_createfile.FileName);

                                    ////litDebug1.Text += filepath_usercreate.ToString() + filePath2.ToString();

                                    if (!Directory.Exists(filePath1))
                                    {
                                        Directory.CreateDirectory(filePath1);
                                    }

                                    userpost_createfile.SaveAs(filepath_usercreate + filePath2 + "\\" + data_insertu0_pm.pm_u0_document_detail_list[0].u0_doc_idx.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());

                                }
                            }
                            catch (Exception Ex)
                            {
                                //litDebug.Text += "Error: <br>" + Ex.Message;
                            }
                        }

                       
                    }

                    setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);

                }
                else
                {
                    if (UploadFile.HasFile)
                    {
                        //litDebug.Text = "1";
                        //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0doc_insert));
                        ////data_u0doc_insert = callServicePostLaw(_urlSetLawCreateDocument, data_u0doc_insert);

                        data_insertu0_pm = callServicePostLaw(_urlSetDocumentPromise, data_insertu0_pm);

                        string filepath_usercreate = Server.MapPath(_path_file_law_usercreatefilepermise);
                        HttpFileCollection uploadedFiles_usercreate = Request.Files;

                        string filePath2 = "usercreatedoc" + "-" + data_insertu0_pm.pm_u0_document_detail_list[0].u0_doc_idx.ToString();//data_insertu0_pm.tcm_u0_document_list[0].docrequest_code.ToString();
                        string filePath1 = Server.MapPath(_path_file_law_usercreatefilepermise + filePath2);

                        for (int i = 0; i < uploadedFiles_usercreate.Count; i++)
                        {
                            HttpPostedFile userpost_createfile = uploadedFiles_usercreate[i];

                            try
                            {
                                if (userpost_createfile.ContentLength > 0)
                                {

                                    //litDebug.Text += "MA620001";


                                    string _filepathExtension = Path.GetExtension(userpost_createfile.FileName);

                                    ////litDebug1.Text += filepath_usercreate.ToString() + filePath2.ToString();

                                    if (!Directory.Exists(filePath1))
                                    {
                                        Directory.CreateDirectory(filePath1);
                                    }

                                    userpost_createfile.SaveAs(filepath_usercreate + filePath2 + "\\" + data_insertu0_pm.pm_u0_document_detail_list[0].u0_doc_idx.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());

                                }
                            }
                            catch (Exception Ex)
                            {
                                //litDebug.Text += "Error: <br>" + Ex.Message;
                            }
                        }

                        setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Please Attach File ---');", true);
                        break;
                    }

                }
             
                break;
            case "cmdSaveEdit":

                TextBox txt_u0_doc_idx = (TextBox)fvCreate.FindControl("txt_u0_doc_idx");
                DropDownList ddlTypeDocument_edit = (DropDownList)fvCreate.FindControl("ddlTypeDocument");
                DropDownList ddlSubTypeDocument_edit = (DropDownList)fvCreate.FindControl("ddlSubTypeDocument");
                TextBox txt_docreference_edit = (TextBox)fvCreate.FindControl("txt_docreference");
                TextBox txt_contract_parties_edit = (TextBox)fvCreate.FindControl("txt_contract_parties");
                TextBox txt_value_edit = (TextBox)fvCreate.FindControl("txt_value");
                TextBox txt_topic_edit = (TextBox)fvCreate.FindControl("txt_topic");
                TextBox txt_detail_edit = (TextBox)fvCreate.FindControl("txt_detail");
                TextBox txt_date_start_edit = (TextBox)fvCreate.FindControl("txt_date_start");
                TextBox txt_date_end_edit = (TextBox)fvCreate.FindControl("txt_date_end");
                TextBox txt_contract_person_edit = (TextBox)fvCreate.FindControl("txt_contract_person");
                TextBox txt_phone_number_edit = (TextBox)fvCreate.FindControl("txt_phone_number");

                UpdatePanel UpdatePanel_FvCreateEdit = (UpdatePanel)fvCreate.FindControl("UpdatePanel_FvCreateEdit");
                FileUpload UploadFileEdit = (FileUpload)UpdatePanel_FvCreateEdit.FindControl("UploadFileEdit");


                data_law data_insertu0_pm_edit = new data_law();

                //insert u0 document
                pm_u0_document_detail u0_insertu0_pm_edit = new pm_u0_document_detail();
                data_insertu0_pm_edit.pm_u0_document_detail_list = new pm_u0_document_detail[1];

                u0_insertu0_pm_edit.u0_doc_idx = int.Parse(txt_u0_doc_idx.Text);
                u0_insertu0_pm_edit.cemp_idx = _emp_idx;
                u0_insertu0_pm_edit.acidx = 1;
                u0_insertu0_pm_edit.noidx = 15;
                u0_insertu0_pm_edit.decision = 21;
                //u0_insertu0_pm_edit.type_document_idx = int.Parse(ddlTypeDocument_edit.SelectedValue);
                //u0_insertu0_pm_edit.subtype_document_idx = int.Parse(ddlSubTypeDocument_edit.SelectedValue);
                u0_insertu0_pm_edit.doc_reference = txt_docreference_edit.Text;
                u0_insertu0_pm_edit.contract_parties = txt_contract_parties_edit.Text;
                u0_insertu0_pm_edit.value = txt_value_edit.Text;
                u0_insertu0_pm_edit.topic = txt_topic_edit.Text;
                u0_insertu0_pm_edit.detail = txt_detail_edit.Text;
                u0_insertu0_pm_edit.date_start = txt_date_start_edit.Text;
                u0_insertu0_pm_edit.date_end = txt_date_end_edit.Text;
                u0_insertu0_pm_edit.contact_person = txt_contract_person_edit.Text;
                u0_insertu0_pm_edit.phone_number = txt_phone_number_edit.Text;

                data_insertu0_pm_edit.pm_u0_document_detail_list[0] = u0_insertu0_pm_edit;
                //insert u0 document 
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_insertu0_pm_edit));

                data_insertu0_pm_edit = callServicePostLaw(_urlSetApproveDocumentPromise, data_insertu0_pm_edit);

                //has file edit
                if (UploadFileEdit.HasFile)
                {
                    //litDebug.Text = "1";
                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0doc_insert));
                    ////data_u0doc_insert = callServicePostLaw(_urlSetLawCreateDocument, data_u0doc_insert);


                    string filepath_usercreate = Server.MapPath(_path_file_law_usercreatefilepermise);
                    HttpFileCollection uploadedFiles_usercreate = Request.Files;

                    string filePath2 = "usercreatedoc" + "-" + txt_u0_doc_idx.Text;//data_insertu0_pm.tcm_u0_document_list[0].docrequest_code.ToString();
                    string filePath1 = Server.MapPath(_path_file_law_usercreatefilepermise + filePath2);


                    //check file in directory in edit file memo
                    if (Directory.Exists(filePath1))
                    {
                        string[] filePaths = Directory.GetFiles(filePath1);
                        foreach (string filePath in filePaths)
                            File.Delete(filePath);
                    }


                    for (int i = 0; i < uploadedFiles_usercreate.Count; i++)
                    {
                        HttpPostedFile userpost_createfile = uploadedFiles_usercreate[i];

                        try
                        {
                            if (userpost_createfile.ContentLength > 0)
                            {

                                //litDebug.Text += "MA620001";


                                string _filepathExtension = Path.GetExtension(userpost_createfile.FileName);

                                ////litDebug1.Text += filepath_usercreate.ToString() + filePath2.ToString();

                                if (!Directory.Exists(filePath1))
                                {
                                    Directory.CreateDirectory(filePath1);
                                }

                                userpost_createfile.SaveAs(filepath_usercreate + filePath2 + "\\" + txt_u0_doc_idx.Text + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());

                            }
                        }
                        catch (Exception Ex)
                        {
                            //litDebug.Text += "Error: <br>" + Ex.Message;
                        }
                    }

                    ////setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0, 0);
                }
                //has file edit


                getCountWaitApprove();

                break;

            case "cmdView":

                string[] _viewdetail = new string[4];
                _viewdetail = e.CommandArgument.ToString().Split(';');
                int _u0_doc_idx_view = int.Parse(_viewdetail[0]);
                int _noidx_view = int.Parse(_viewdetail[1]);
                int _acidx_view = int.Parse(_viewdetail[2]);
                int _staidx_view = int.Parse(_viewdetail[3]);
                int _cemp_idx_view = int.Parse(_viewdetail[4]);


                setActiveTab("docCreate", _u0_doc_idx_view, _noidx_view, _staidx_view, _acidx_view, 1, _cemp_idx_view, 0);
                break;

            case "cmdViewApprove":

                string[] _viewApprove = new string[4];
                _viewApprove = e.CommandArgument.ToString().Split(';');
                int _u0_doc_idx_Approve = int.Parse(_viewApprove[0]);
                int _noidx_Approve = int.Parse(_viewApprove[1]);
                int _acidx_Approve = int.Parse(_viewApprove[2]);
                int _staidx_Approve = int.Parse(_viewApprove[3]);
                int _cemp_idx_Approve = int.Parse(_viewApprove[4]);


                setActiveTab("docCreate", _u0_doc_idx_Approve, _noidx_Approve, _staidx_Approve, _acidx_Approve, 2, _cemp_idx_Approve, 0);
                break;

            case "cmdDocSaveApprove":

                HiddenField hfU0IDX = (HiddenField)fvCreate.FindControl("hfu0_document_idx");
                HiddenField hfACIDX = (HiddenField)fvCreate.FindControl("hfM0ActoreIDX");
                HiddenField hfNOIDX = (HiddenField)fvCreate.FindControl("hfM0NodeIDX");
                HiddenField hfSTAIDX = (HiddenField)fvCreate.FindControl("hfM0StatusIDX");

                switch (hfNOIDX.Value)
                {
                    case "16"://director user approve

                        DropDownList ddlDirectorUserApprove = (DropDownList)fvDirectorUser.FindControl("ddlDirectorUserApprove");
                        TextBox txt_comment_DirectorUserApprove = (TextBox)fvDirectorUser.FindControl("txt_comment_DirectorUserApprove");

                        data_law data_doc_approve = new data_law();

                        //insert u0 document
                        pm_u0_document_detail u0doc_approve = new pm_u0_document_detail();
                        data_doc_approve.pm_u0_document_detail_list = new pm_u0_document_detail[1];

                        u0doc_approve.cemp_idx = _emp_idx;
                        u0doc_approve.u0_doc_idx = int.Parse(hfU0IDX.Value);
                        u0doc_approve.acidx = int.Parse(hfACIDX.Value);
                        u0doc_approve.noidx = int.Parse(hfNOIDX.Value);
                        u0doc_approve.decision = int.Parse(ddlDirectorUserApprove.SelectedValue);
                        u0doc_approve.comment = txt_comment_DirectorUserApprove.Text;

                        data_doc_approve.pm_u0_document_detail_list[0] = u0doc_approve;

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_doc_approve));

                        data_doc_approve = callServicePostLaw(_urlSetApproveDocumentPromise, data_doc_approve);
                        //////setActiveTab("docWaitApprove", 0, 0, 0, 0, 0, 0, 0);

                        getCountWaitApprove();
                        setOntop.Focus();
                        break;
                    case "17": //mg law receive and select law officer

                        DropDownList ddlMgLawApprove = (DropDownList)fvMgLaw.FindControl("ddlMgLawApprove");
                        TextBox txt_comment_MgLawApprove = (TextBox)fvMgLaw.FindControl("txt_comment_MgLawApprove");

                        data_law data_doc_mglawapprove = new data_law();

                        //insert u0 document
                        pm_u0_document_detail u0doc_mglawapprove = new pm_u0_document_detail();
                        data_doc_mglawapprove.pm_u0_document_detail_list = new pm_u0_document_detail[1];

                        u0doc_mglawapprove.cemp_idx = _emp_idx;
                        u0doc_mglawapprove.u0_doc_idx = int.Parse(hfU0IDX.Value);
                        u0doc_mglawapprove.acidx = int.Parse(hfACIDX.Value);
                        u0doc_mglawapprove.noidx = int.Parse(hfNOIDX.Value);
                        u0doc_mglawapprove.decision = int.Parse(ddlMgLawApprove.SelectedValue);
                        u0doc_mglawapprove.comment = txt_comment_MgLawApprove.Text;

                        data_doc_mglawapprove.pm_u0_document_detail_list[0] = u0doc_mglawapprove;

                        if(ddlMgLawApprove.SelectedValue == "25")
                        {
                            //insert u2 document
                            var _u2_doc_insert = new pm_u2_document_detail[chkLawOfficer.Items.Count];
                            int sumcheck_u2 = 0;
                            int count_u2 = 0;

                            List<String> Chk_u3list = new List<string>();
                            foreach (ListItem chklaw_list in chkLawOfficer.Items)
                            {
                                if (chklaw_list.Selected)
                                {
                                    _u2_doc_insert[count_u2] = new pm_u2_document_detail();
                                    _u2_doc_insert[count_u2].cemp_idx = _emp_idx;
                                    _u2_doc_insert[count_u2].emp_idx = int.Parse(chklaw_list.Value);

                                    sumcheck_u2 = sumcheck_u2 + 1;

                                    count_u2++;
                                }
                            }
                            //insert u3 document

                            data_doc_mglawapprove.pm_u2_document_detail_list = _u2_doc_insert;
                            //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_doc_mglawapprove));

                            if (count_u2 > 0)
                            {

                                data_doc_approve = callServicePostLaw(_urlSetApproveDocumentPromise, data_doc_mglawapprove);

                                getCountWaitApprove();
                                setOntop.Focus();
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณาเลือกผู้รับผิดชอบ ---');", true);
                                break;
                            }
                           
                        }
                        else
                        {

                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_doc_mglawapprove));
                            data_doc_approve = callServicePostLaw(_urlSetApproveDocumentPromise, data_doc_mglawapprove);

                            getCountWaitApprove();
                            setOntop.Focus();
                        }

                        break;
                    case "18": //law officer attrach file or not approve

                        DropDownList ddlLawApprove = (DropDownList)fvLaw.FindControl("ddlLawApprove");
                        TextBox txt_comment_LawApprove = (TextBox)fvLaw.FindControl("txt_comment_LawApprove");

                        UpdatePanel _Panel_File_permise = (UpdatePanel)fvLaw.FindControl("_Panel_File_permise");
                        FileUpload UploadFilePromise = (FileUpload)_Panel_File_permise.FindControl("UploadFilePromise");

                        data_law data_doc_lawapprove = new data_law();

                        //insert u0 document
                        pm_u0_document_detail u0doc_lawapprove = new pm_u0_document_detail();
                        data_doc_lawapprove.pm_u0_document_detail_list = new pm_u0_document_detail[1];

                        u0doc_lawapprove.cemp_idx = _emp_idx;
                        u0doc_lawapprove.u0_doc_idx = int.Parse(hfU0IDX.Value);
                        u0doc_lawapprove.acidx = int.Parse(hfACIDX.Value);
                        u0doc_lawapprove.noidx = int.Parse(hfNOIDX.Value);
                        u0doc_lawapprove.decision = int.Parse(ddlLawApprove.SelectedValue);
                        u0doc_lawapprove.comment = txt_comment_LawApprove.Text;

                        data_doc_lawapprove.pm_u0_document_detail_list[0] = u0doc_lawapprove;

                        if (ddlLawApprove.SelectedValue == "27")
                        {

                            //attract file permise
                            if (UploadFilePromise.HasFile)
                            {
                                //litDebug.Text = hfU0IDX.Value.ToString(); 
                                //data_doc_lawapprove = callServicePostLaw(_urlSetLawApproveDocument, data_doc_lawapprove);

                                //attrach file promise
                                data_doc_approve = callServicePostLaw(_urlSetApproveDocumentPromise, data_doc_lawapprove);

                                string filepath_lawpermise = Server.MapPath(_path_file_law_filepermise);
                                HttpFileCollection Upload_FilePromise = Request.Files;
                                string filePath2 = "law_permise" + "-" + hfU0IDX.Value.ToString();
                                string filePath1 = Server.MapPath(_path_file_law_filepermise + filePath2);

                                //check file in directory in edit file permise
                                if (Directory.Exists(filePath1))
                                {
                                    string[] filePaths = Directory.GetFiles(filePath1);
                                    foreach (string filePath in filePaths)
                                        File.Delete(filePath);
                                }

                                for (int i = 0; i < Upload_FilePromise.Count; i++)
                                {
                                    HttpPostedFile lawpermisepost_createfile = Upload_FilePromise[i];

                                    try
                                    {
                                        if (lawpermisepost_createfile.ContentLength > 0)
                                        {
                                            //litDebug.Text = "1";
                                           
                                            string _filepathExtension = Path.GetExtension(lawpermisepost_createfile.FileName);
                                            if (!Directory.Exists(filePath1))
                                            {
                                                Directory.CreateDirectory(filePath1);
                                                //litDebug.Text += "create file" + (i + 1) + "|" + "<br>";
                                            }
                                            else
                                            {

                                            }
        
                                            lawpermisepost_createfile.SaveAs(filepath_lawpermise + filePath2 + "\\" + hfU0IDX.Value.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());

                                        }
                                    }
                                    catch (Exception Ex)
                                    {
                                        //litDebug.Text += "Error: <br>" + Ex.Message;
                                    }
                                }

                                getCountWaitApprove();
                                setOntop.Focus();

                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Please Attach File ---');", true);
                                break;
                                //litDebug.Text = "2";
                            }
                           


                        }
                        else
                        {
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_doc_mglawapprove));
                            data_doc_approve = callServicePostLaw(_urlSetApproveDocumentPromise, data_doc_lawapprove);

                            getCountWaitApprove();
                            setOntop.Focus();
                        }

                        break;

                    case "19": //user check file promise

                        DropDownList ddlUserCheckfile = (DropDownList)fvUserCheckfilepromise.FindControl("ddlUserCheckfile");
                        TextBox txt_comment_UserCheckFile = (TextBox)fvUserCheckfilepromise.FindControl("txt_comment_UserCheckFile");

                        UpdatePanel _Panel_CheckFile_permise = (UpdatePanel)fvUserCheckfilepromise.FindControl("_Panel_CheckFile_permise");
                        FileUpload UploadFilePromiseUserCheck = (FileUpload)_Panel_CheckFile_permise.FindControl("UploadFilePromiseUserCheck");


                        data_law data_user_checkfile = new data_law();

                        //insert u0 document
                        pm_u0_document_detail u0doc_user_checkfile = new pm_u0_document_detail();
                        data_user_checkfile.pm_u0_document_detail_list = new pm_u0_document_detail[1];

                        u0doc_user_checkfile.cemp_idx = _emp_idx;
                        u0doc_user_checkfile.u0_doc_idx = int.Parse(hfU0IDX.Value);
                        u0doc_user_checkfile.acidx = int.Parse(hfACIDX.Value);
                        u0doc_user_checkfile.noidx = int.Parse(hfNOIDX.Value);
                        u0doc_user_checkfile.decision = int.Parse(ddlUserCheckfile.SelectedValue);
                        u0doc_user_checkfile.comment = txt_comment_UserCheckFile.Text;

                        data_user_checkfile.pm_u0_document_detail_list[0] = u0doc_user_checkfile;


                        


                        //attract file permise
                        if (UploadFilePromiseUserCheck.HasFile)
                        {
                            //litDebug.Text = hfU0IDX.Value.ToString(); 
                            //data_doc_lawapprove = callServicePostLaw(_urlSetLawApproveDocument, data_doc_lawapprove);

                            data_doc_approve = callServicePostLaw(_urlSetApproveDocumentPromise, data_user_checkfile);

                            string filepath_usercheck_permise = Server.MapPath(_path_file_law_usercheckfilepermise);
                            HttpFileCollection Upload_Fileusercheck_permise = Request.Files;
                            string filePath2 = "usercheck_permise" + "-" + hfU0IDX.Value.ToString();
                            string filePath1 = Server.MapPath(_path_file_law_usercheckfilepermise + filePath2);

                            //check file in directory in edit file permise
                            if (Directory.Exists(filePath1))
                            {
                                string[] filePaths = Directory.GetFiles(filePath1);
                                foreach (string filePath in filePaths)
                                    File.Delete(filePath);
                            }

                            for (int i = 0; i < Upload_Fileusercheck_permise.Count; i++)
                            {
                                HttpPostedFile lawpermisepost_usercheckfile = Upload_Fileusercheck_permise[i];

                                try
                                {
                                    if (lawpermisepost_usercheckfile.ContentLength > 0)
                                    {
                                        //litDebug.Text = "1";

                                        string _filepathExtension = Path.GetExtension(lawpermisepost_usercheckfile.FileName);
                                        if (!Directory.Exists(filePath1))
                                        {
                                            Directory.CreateDirectory(filePath1);
                                            //litDebug.Text += "create file" + (i + 1) + "|" + "<br>";
                                        }
                                        else
                                        {

                                        }

                                        lawpermisepost_usercheckfile.SaveAs(filepath_usercheck_permise + filePath2 + "\\" + hfU0IDX.Value.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());

                                    }
                                }
                                catch (Exception Ex)
                                {
                                    //litDebug.Text += "Error: <br>" + Ex.Message;
                                }
                            }

                            getCountWaitApprove();
                            setOntop.Focus();
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Please Attach File ---');", true);
                            break;
                        }

                        
                        break;
                    case "20": //law officer attrach file script

                        DropDownList ddlStatusScript = (DropDownList)fvLawFileScript.FindControl("ddlStatusScript");
                        TextBox txt_comment_LawScript = (TextBox)fvLawFileScript.FindControl("txt_comment_LawScript");

                        UpdatePanel _Panel_FileScript_permise = (UpdatePanel)fvLawFileScript.FindControl("_Panel_FileScript_permise");
                        FileUpload UploadFileScriptPromise = (FileUpload)_Panel_FileScript_permise.FindControl("UploadFileScriptPromise");


                        data_law data_lawscript = new data_law();

                        //insert u0 document
                        pm_u0_document_detail u0doc_lawscript = new pm_u0_document_detail();
                        data_lawscript.pm_u0_document_detail_list = new pm_u0_document_detail[1];

                        u0doc_lawscript.cemp_idx = _emp_idx;
                        u0doc_lawscript.u0_doc_idx = int.Parse(hfU0IDX.Value);
                        u0doc_lawscript.acidx = int.Parse(hfACIDX.Value);
                        u0doc_lawscript.noidx = int.Parse(hfNOIDX.Value);
                        u0doc_lawscript.decision = int.Parse(ddlStatusScript.SelectedValue);
                        u0doc_lawscript.comment = txt_comment_LawScript.Text;

                        data_lawscript.pm_u0_document_detail_list[0] = u0doc_lawscript;


                        

                        //attract file permise
                        if (UploadFileScriptPromise.HasFile)
                        {

                            data_lawscript = callServicePostLaw(_urlSetApproveDocumentPromise, data_lawscript);

                            //litDebug.Text = hfU0IDX.Value.ToString(); 
                            //data_doc_lawapprove = callServicePostLaw(_urlSetLawApproveDocument, data_doc_lawapprove);

                            string filepath_lawscript_permise = Server.MapPath(_path_file_law_lawscriptfilepermise);
                            HttpFileCollection Upload_Filelawscript_permise = Request.Files;
                            string filePath2 = "lawscript_permise" + "-" + hfU0IDX.Value.ToString();
                            string filePath1 = Server.MapPath(_path_file_law_lawscriptfilepermise + filePath2);

                            //check file in directory in edit file permise
                            if (Directory.Exists(filePath1))
                            {
                                string[] filePaths = Directory.GetFiles(filePath1);
                                foreach (string filePath in filePaths)
                                    File.Delete(filePath);
                            }

                            for (int i = 0; i < Upload_Filelawscript_permise.Count; i++)
                            {
                                HttpPostedFile lawpermisepost_lawscriptfile = Upload_Filelawscript_permise[i];

                                try
                                {
                                    if (lawpermisepost_lawscriptfile.ContentLength > 0)
                                    {
                                        //litDebug.Text = "1";

                                        string _filepathExtension = Path.GetExtension(lawpermisepost_lawscriptfile.FileName);
                                        if (!Directory.Exists(filePath1))
                                        {
                                            Directory.CreateDirectory(filePath1);
                                            //litDebug.Text += "create file" + (i + 1) + "|" + "<br>";
                                        }
                                        else
                                        {

                                        }

                                        lawpermisepost_lawscriptfile.SaveAs(filepath_lawscript_permise + filePath2 + "\\" + hfU0IDX.Value.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());

                                    }
                                }
                                catch (Exception Ex)
                                {
                                    //litDebug.Text += "Error: <br>" + Ex.Message;
                                }
                            }

                            getCountWaitApprove();
                            setOntop.Focus();
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Please Attach File ---');", true);
                            break;
                        }

                        break;
                    case "21": //user check filescript and contract sup

                        DropDownList ddlUsercheckScript = (DropDownList)fvUsercheckScript.FindControl("ddlUsercheckScript");
                        TextBox txt_comment_UsercheckScript = (TextBox)fvUsercheckScript.FindControl("txt_comment_UsercheckScript");

                        UpdatePanel _Panel_UsercheckScript = (UpdatePanel)fvUsercheckScript.FindControl("_Panel_UsercheckScript");
                        FileUpload UploadFileUsercheckScript = (FileUpload)_Panel_UsercheckScript.FindControl("UploadFileUsercheckScript");

                        data_law data_UsercheckScript = new data_law();

                        //insert u0 document
                        pm_u0_document_detail u0doc_UsercheckScript = new pm_u0_document_detail();
                        data_UsercheckScript.pm_u0_document_detail_list = new pm_u0_document_detail[1];

                        u0doc_UsercheckScript.cemp_idx = _emp_idx;
                        u0doc_UsercheckScript.u0_doc_idx = int.Parse(hfU0IDX.Value);
                        u0doc_UsercheckScript.acidx = int.Parse(hfACIDX.Value);
                        u0doc_UsercheckScript.noidx = int.Parse(hfNOIDX.Value);
                        u0doc_UsercheckScript.decision = int.Parse(ddlUsercheckScript.SelectedValue);
                        u0doc_UsercheckScript.comment = txt_comment_UsercheckScript.Text;

                        data_UsercheckScript.pm_u0_document_detail_list[0] = u0doc_UsercheckScript;

                        //attract file permise
                        if (UploadFileUsercheckScript.HasFile)
                        {
                            
                            data_UsercheckScript = callServicePostLaw(_urlSetApproveDocumentPromise, data_UsercheckScript);

                            //litDebug.Text = hfU0IDX.Value.ToString(); 
                            //data_doc_lawapprove = callServicePostLaw(_urlSetLawApproveDocument, data_doc_lawapprove);

                            string filepath_lawscript_permise = Server.MapPath(_path_file_law_usercheckscriptfile);
                            HttpFileCollection Upload_Fileusercheck_script = Request.Files;
                            string filePath2 = "usercheck_script" + "-" + hfU0IDX.Value.ToString();
                            string filePath1 = Server.MapPath(_path_file_law_usercheckscriptfile + filePath2);

                            //check file in directory in edit file permise
                            if (Directory.Exists(filePath1))
                            {
                                string[] filePaths = Directory.GetFiles(filePath1);
                                foreach (string filePath in filePaths)
                                File.Delete(filePath);
                            }

                            for (int i = 0; i < Upload_Fileusercheck_script.Count; i++)
                            {
                                HttpPostedFile lawpermisepost_usercheck_script = Upload_Fileusercheck_script[i];

                                try
                                {
                                    if (lawpermisepost_usercheck_script.ContentLength > 0)
                                    {
                                        //litDebug.Text = "1";

                                        string _filepathExtension = Path.GetExtension(lawpermisepost_usercheck_script.FileName);
                                        if (!Directory.Exists(filePath1))
                                        {
                                            Directory.CreateDirectory(filePath1);
                                            //litDebug.Text += "create file" + (i + 1) + "|" + "<br>";
                                        }
                                        else
                                        {

                                        }

                                        lawpermisepost_usercheck_script.SaveAs(filepath_lawscript_permise + filePath2 + "\\" + hfU0IDX.Value.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());

                                    }
                                }
                                catch (Exception Ex)
                                {
                                    //litDebug.Text += "Error: <br>" + Ex.Message;
                                }
                            }

                            getCountWaitApprove();
                            setOntop.Focus();


                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Please Attach File ---');", true);
                            break;
                        }


                        break;
                    case "22": //law save attract file promise

                        DropDownList ddlLawSavePromise = (DropDownList)fvSavePromise.FindControl("ddlLawSavePromise");
                        TextBox txt_comment_ddlLawSavePromise = (TextBox)fvSavePromise.FindControl("txt_comment_ddlLawSavePromise");

                        UpdatePanel _Panel_LawSavePromise = (UpdatePanel)fvSavePromise.FindControl("_Panel_LawSavePromise");
                        FileUpload UploadFileLawSavePromise = (FileUpload)_Panel_LawSavePromise.FindControl("UploadFileLawSavePromise");

                        data_law data_LawSavePromise = new data_law();

                        //insert u0 document
                        pm_u0_document_detail u0doc_LawSavePromise = new pm_u0_document_detail();
                        data_LawSavePromise.pm_u0_document_detail_list = new pm_u0_document_detail[1];

                        u0doc_LawSavePromise.cemp_idx = _emp_idx;
                        u0doc_LawSavePromise.u0_doc_idx = int.Parse(hfU0IDX.Value);
                        u0doc_LawSavePromise.acidx = int.Parse(hfACIDX.Value);
                        u0doc_LawSavePromise.noidx = int.Parse(hfNOIDX.Value);
                        u0doc_LawSavePromise.decision = int.Parse(ddlLawSavePromise.SelectedValue);
                        u0doc_LawSavePromise.comment = txt_comment_ddlLawSavePromise.Text;

                        data_LawSavePromise.pm_u0_document_detail_list[0] = u0doc_LawSavePromise;


                        //data_LawSavePromise = callServicePostLaw(_urlSetApproveDocumentPromise, data_LawSavePromise);

                        //attract file permise
                        if (UploadFileLawSavePromise.HasFile)
                        {

                            data_LawSavePromise = callServicePostLaw(_urlSetApproveDocumentPromise, data_LawSavePromise);

                            //litDebug.Text = hfU0IDX.Value.ToString(); 
                          

                            string filepath_lawsave_permise = Server.MapPath(_path_file_law_lawsave_permise);
                            HttpFileCollection Upload_Filelawsave_permise = Request.Files;
                            string filePath2 = "lawsave_permise" + "-" + hfU0IDX.Value.ToString();
                            string filePath1 = Server.MapPath(_path_file_law_lawsave_permise + filePath2);

                            //check file in directory in edit file permise
                            if (Directory.Exists(filePath1))
                            {
                                string[] filePaths = Directory.GetFiles(filePath1);
                                foreach (string filePath in filePaths)
                                    File.Delete(filePath);
                            }

                            for (int i = 0; i < Upload_Filelawsave_permise.Count; i++)
                            {
                                HttpPostedFile lawpermisepost_lawsave_permise = Upload_Filelawsave_permise[i];

                                try
                                {
                                    if (lawpermisepost_lawsave_permise.ContentLength > 0)
                                    {
                                        //litDebug.Text = "1";

                                        string _filepathExtension = Path.GetExtension(lawpermisepost_lawsave_permise.FileName);
                                        if (!Directory.Exists(filePath1))
                                        {
                                            Directory.CreateDirectory(filePath1);
                                            //litDebug.Text += "create file" + (i + 1) + "|" + "<br>";
                                        }
                                        else
                                        {

                                        }

                                        lawpermisepost_lawsave_permise.SaveAs(filepath_lawsave_permise + filePath2 + "\\" + hfU0IDX.Value.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());

                                    }
                                }
                                catch (Exception Ex)
                                {
                                    //litDebug.Text += "Error: <br>" + Ex.Message;
                                }
                            }

                            getCountWaitApprove();
                            setOntop.Focus();


                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Please Attach File ---');", true);
                            break;
                        }

                        break;

                }

                break;
            case "cmdSearchDetail":


                DropDownList ddlTypeDocumentSearch = (DropDownList)_PanelSearchDetail.FindControl("ddlTypeDocumentSearch");
                DropDownList ddlSubTypeDocumentSearch = (DropDownList)_PanelSearchDetail.FindControl("ddlSubTypeDocumentSearch");
                TextBox txtDocumentSearch = (TextBox)_PanelSearchDetail.FindControl("txtDocumentSearch");

                if(ViewState["Vs_DetailPromiseAll"] != null)
                {
                    pm_u0_document_detail[] _templist_DocumentDetail = (pm_u0_document_detail[])ViewState["Vs_DetailPromiseAll"];

                    var _linqDocumentDetail = (from dt in _templist_DocumentDetail

                                               where

                                               (int.Parse(ddlTypeDocumentSearch.SelectedValue) == 0 || dt.type_document_idx == int.Parse(ddlTypeDocumentSearch.SelectedValue))
                                               && (int.Parse(ddlSubTypeDocumentSearch.SelectedValue) == 0 || dt.subtype_document_idx == int.Parse(ddlSubTypeDocumentSearch.SelectedValue))
                                               && (txtDocumentSearch.Text == "" || dt.document_code.Contains(txtDocumentSearch.Text)) //where Like use Contains

                                               select dt

                                          ).Distinct().ToList();


                    ViewState["Vs_DetailPromise"] = _linqDocumentDetail.ToList();
                    setGridData(GvDetail, ViewState["Vs_DetailPromise"]);
                }
                else
                {
                    setGridData(GvDetail, ViewState["Vs_DetailPromise"]);
                }

                break;
            case "cmdClearSearchDetail":

                //getDetailPromise(GvDetail, 0, _emp_idx);

                setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);

                break;
            case "cmdSearchDetailFile":


                DropDownList ddlTypeDocumentSearchFile = (DropDownList)PanelFilePromise.FindControl("ddlTypeDocumentSearchFile");
                DropDownList ddlSubTypeDocumentSearchFile = (DropDownList)PanelFilePromise.FindControl("ddlSubTypeDocumentSearchFile");
                TextBox txtDocumentSearchFile = (TextBox)PanelFilePromise.FindControl("txtDocumentSearchFile");
                
                if(ViewState["Vs_DetailFileUploadAll"] != null)
                {
                    pm_u0_document_detail[] _templist_DocumentDetailFile = (pm_u0_document_detail[])ViewState["Vs_DetailFileUploadAll"];

                    var _linqDocumentDetailFile = (from dt in _templist_DocumentDetailFile

                                                   where

                                               (int.Parse(ddlTypeDocumentSearchFile.SelectedValue) == 0 || dt.type_document_idx == int.Parse(ddlTypeDocumentSearchFile.SelectedValue))
                                               && (int.Parse(ddlSubTypeDocumentSearchFile.SelectedValue) == 0 || dt.subtype_document_idx == int.Parse(ddlSubTypeDocumentSearchFile.SelectedValue))
                                               && (txtDocumentSearchFile.Text == "" || dt.document_code.Contains(txtDocumentSearchFile.Text)) //where Like use Contains

                                                   select dt

                                          ).Distinct().ToList();


                    ViewState["Vs_DetailFileUpload"] = _linqDocumentDetailFile.ToList();
                    setGridData(GvDocumentPromise, ViewState["Vs_DetailFileUpload"]);
                }
                else
                {
                   
                    setGridData(GvDocumentPromise, ViewState["Vs_DetailFileUpload"]);
                }

               

                break;
            case "cmdClearSearchDetailFile":

                setActiveTab("docPromise", 0, 0, 0, 0, 0, 0, 0);

                break;


        }

    }
    //endbtn

    #endregion event command

    #region gridview
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;


        switch (gvName.ID)
        {
            case "GvDetail":

                setGridData(GvDetail, ViewState["Vs_DetailPromise"]);

                break;

            case "GvDocumentPromise":

                setGridData(GvDocumentPromise, ViewState["Vs_DetailFileUpload"]);

                break;


        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvDetail":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                  

                }

                break;
            case "gvFileUserCreate":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.FindControl("btnDL11");
                    HiddenField hidFile = (HiddenField)e.Row.FindControl("hidFile");
                    string LinkHost = string.Format("http://{0}", Request.Url.Host);

                    //btnDL11.NavigateUrl = LinkHost11_permwait + MapURL(hidFile.Value);    
                    string path = HttpContext.Current.Request.Url.AbsolutePath;
                    string[] result_path = path.Split(new string[] { "/" }, StringSplitOptions.None);

                    if (LinkHost.ToString() == "http://localhost")
                    {
                        //litDebug1.Text = LinkHost11_permwait;
                        btnDL11.NavigateUrl = LinkHost + "/" + result_path[1].ToString() + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }
                    else
                    {
                        btnDL11.NavigateUrl = LinkHost + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }


                }
                break;
            case "gvFileLawCreatePromise":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.FindControl("btnDL11");
                    HiddenField hidFile = (HiddenField)e.Row.FindControl("hidFile");
                    string LinkHost = string.Format("http://{0}", Request.Url.Host);

                    //btnDL11.NavigateUrl = LinkHost11_permwait + MapURL(hidFile.Value);    
                    string path = HttpContext.Current.Request.Url.AbsolutePath;
                    string[] result_path = path.Split(new string[] { "/" }, StringSplitOptions.None);

                    if (LinkHost.ToString() == "http://localhost")
                    {
                        //litDebug1.Text = LinkHost11_permwait;
                        btnDL11.NavigateUrl = LinkHost + "/" + result_path[1].ToString() + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }
                    else
                    {
                        btnDL11.NavigateUrl = LinkHost + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }


                }
                break;
            case "gvFileLawCreateScript":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.FindControl("btnDL11");
                    HiddenField hidFile = (HiddenField)e.Row.FindControl("hidFile");
                    string LinkHost = string.Format("http://{0}", Request.Url.Host);

                    //btnDL11.NavigateUrl = LinkHost11_permwait + MapURL(hidFile.Value);    
                    string path = HttpContext.Current.Request.Url.AbsolutePath;
                    string[] result_path = path.Split(new string[] { "/" }, StringSplitOptions.None);

                    if (LinkHost.ToString() == "http://localhost")
                    {
                        //litDebug1.Text = LinkHost11_permwait;
                        btnDL11.NavigateUrl = LinkHost + "/" + result_path[1].ToString() + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }
                    else
                    {
                        btnDL11.NavigateUrl = LinkHost + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }


                }
                break;
            case "gvFileLawSavePromise":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.FindControl("btnDL11");
                    HiddenField hidFile = (HiddenField)e.Row.FindControl("hidFile");
                    string LinkHost = string.Format("http://{0}", Request.Url.Host);

                    //btnDL11.NavigateUrl = LinkHost11_permwait + MapURL(hidFile.Value);    
                    string path = HttpContext.Current.Request.Url.AbsolutePath;
                    string[] result_path = path.Split(new string[] { "/" }, StringSplitOptions.None);

                    if (LinkHost.ToString() == "http://localhost")
                    {
                        //litDebug1.Text = LinkHost11_permwait;
                        btnDL11.NavigateUrl = LinkHost + "/" + result_path[1].ToString() + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }
                    else
                    {
                        btnDL11.NavigateUrl = LinkHost + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }


                }
                break;
            case "gvFileUserCheckPromise":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.FindControl("btnDL11");
                    HiddenField hidFile = (HiddenField)e.Row.FindControl("hidFile");
                    string LinkHost = string.Format("http://{0}", Request.Url.Host);

                    //btnDL11.NavigateUrl = LinkHost11_permwait + MapURL(hidFile.Value);    
                    string path = HttpContext.Current.Request.Url.AbsolutePath;
                    string[] result_path = path.Split(new string[] { "/" }, StringSplitOptions.None);

                    if (LinkHost.ToString() == "http://localhost")
                    {
                        //litDebug1.Text = LinkHost11_permwait;
                        btnDL11.NavigateUrl = LinkHost + "/" + result_path[1].ToString() + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }
                    else
                    {
                        btnDL11.NavigateUrl = LinkHost + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }


                }
                break;
            case "gvFileUserCheckScript":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.FindControl("btnDL11");
                    HiddenField hidFile = (HiddenField)e.Row.FindControl("hidFile");
                    string LinkHost = string.Format("http://{0}", Request.Url.Host);

                    //btnDL11.NavigateUrl = LinkHost11_permwait + MapURL(hidFile.Value);    
                    string path = HttpContext.Current.Request.Url.AbsolutePath;
                    string[] result_path = path.Split(new string[] { "/" }, StringSplitOptions.None);

                    if (LinkHost.ToString() == "http://localhost")
                    {
                        //litDebug1.Text = LinkHost11_permwait;
                        btnDL11.NavigateUrl = LinkHost + "/" + result_path[1].ToString() + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }
                    else
                    {
                        btnDL11.NavigateUrl = LinkHost + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }


                }
                break;
            case "GvDocumentPromise":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lbl_u0_doc_idx_file = (Label)e.Row.Cells[0].FindControl("lbl_u0_doc_idx_file");
                    HyperLink btnDLX = (HyperLink)e.Row.Cells[0].FindControl("btnDLX");

                    string getPath_file_lawsave_permise = ConfigurationManager.AppSettings["path_file_law_lawsave_permise"];
                    string law_filesavescript = "lawsave_permise-" + lbl_u0_doc_idx_file.Text;

                    if (Directory.Exists(Server.MapPath(getPath_file_lawsave_permise + law_filesavescript)))
                    {

                        //litDebug1.Text += "1";
                        string[] filesPath = Directory.GetFiles(Server.MapPath(getPath_file_lawsave_permise + law_filesavescript));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string path in filesPath)
                        {
                            string getfiles = "";
                            getfiles = Path.GetFileName(path);
                            btnDLX.NavigateUrl = getPath_file_lawsave_permise + law_filesavescript + "/" + getfiles;

                        }
                        btnDLX.Visible = true;
                    }
                    else
                    {
                        //litDebug1.Text += "2";
                        btnDLX.Visible = false;
                        //btnViewFileRoom_.Text = "-";
                    }


                }
                break;
        }
    }

    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {
            case "GvViewDetailReport":
                //for (int rowIndex = GvViewDetailReport.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                //{
                //    GridViewRow currentRow = GvViewDetailReport.Rows[rowIndex];
                //    GridViewRow previousRow = GvViewDetailReport.Rows[rowIndex + 1];

                //    if (((Label)currentRow.Cells[0].FindControl("lbl_dept_name_th_reportview")).Text == ((Label)previousRow.Cells[0].FindControl("lbl_dept_name_th_reportview")).Text)
                //    {
                //        if (previousRow.Cells[0].RowSpan < 2)
                //        {
                //            currentRow.Cells[0].RowSpan = 2;

                //        }
                //        else
                //        {
                //            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                //        }
                //        previousRow.Cells[0].Visible = false;
                //    }


                //    if (((Label)currentRow.Cells[1].FindControl("lbl_costcenter_no_reportview")).Text == ((Label)previousRow.Cells[1].FindControl("lbl_costcenter_no_reportview")).Text)
                //    {
                //        if (previousRow.Cells[1].RowSpan < 2)
                //        {
                //            currentRow.Cells[1].RowSpan = 2;

                //        }
                //        else
                //        {
                //            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;

                //        }
                //        previousRow.Cells[1].Visible = false;
                //    }


                //}
                break;
        }
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;

            switch (cmdName)
            {
                case "cmdRemoveCar":
                    //GridView gvAddCarBooking = (GridView)FvHrApprove.FindControl("gvAddCarBooking");
                    //GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    //int rowIndex = rowSelect.RowIndex;
                    //DataSet dsContacts = (DataSet)ViewState["vsCarDetailList"];
                    //dsContacts.Tables["dsCarDetailTable"].Rows[rowIndex].Delete();
                    //dsContacts.AcceptChanges();
                    //setGridData(gvAddCarBooking, dsContacts.Tables["dsCarDetailTable"]);
                    //if (dsContacts.Tables["dsCarDetailTable"].Rows.Count < 1)
                    //{
                    //    gvAddCarBooking.Visible = false;
                    //}
                    break;

            }
        }
    }

    #endregion gridview

    #region pathfile
    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    protected void SelectPathFile(string _docrequest_code, string path, GridView gvName, Panel pnName)
    {
        try
        {
            string filePathView = Server.MapPath(path + _docrequest_code);
            DirectoryInfo myDirLotus = new DirectoryInfo(filePathView);
            SearchDirectories(myDirLotus, _docrequest_code, gvName);
            pnName.Visible = true;
            gvName.Visible = true;

        }
        catch (Exception ex)
        {
            pnName.Visible = false;
            gvName.Visible = false;
            //txt.Text = ex.ToString();
        }
    }

    public void SearchDirectories(DirectoryInfo dir, String target, GridView gvName)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");


        FileInfo[] files = dir.GetFiles();
        int i = 0;
        foreach (FileInfo file in files)
        {
            //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
            //{
            string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
            dt1.Rows.Add(file.Name);
            dt1.Rows[i][1] = f[0];
            i++;
            //}
        }

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            gvName.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvName.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvName.DataSource = null;
            gvName.DataBind();

        }


    }

    #endregion path file

    #region reuse

    protected void initPageLoad()
    {

    }

    protected void initPage()
    {

        setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);

    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setChkData(CheckBoxList chkName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        chkName.Items.Clear();
        // bind items
        chkName.DataSource = obj;
        chkName.DataTextField = _data_text;
        chkName.DataValueField = _data_value;
        chkName.DataBind();
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_law callServicePostLaw(string _cmdUrl, data_law _data_law)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_law);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_law = (data_law)_funcTool.convertJsonToObject(typeof(data_law), _localJson);

        return _data_law;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cempidx, int u1idx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));
        setFormData(fvEmpDetail, FormViewMode.ReadOnly, null);

        //set tab create
        setFormData(fvCreate, FormViewMode.ReadOnly, null);
        div_Log.Visible = false;

        setFormData(fvDirectorUser, FormViewMode.ReadOnly, null);
        setFormData(fvMgLaw, FormViewMode.ReadOnly, null);
        setFormData(fvLaw, FormViewMode.ReadOnly, null);
        setFormData(fvUserCheckfilepromise, FormViewMode.ReadOnly, null);
        setFormData(fvLawFileScript, FormViewMode.ReadOnly, null);
        setFormData(fvUsercheckScript, FormViewMode.ReadOnly, null);
        setFormData(fvSavePromise, FormViewMode.ReadOnly, null);

        //set tab detail
        GvDetail.Visible = false;
        setGridData(GvDetail, null);

        //set tab approve
        gvApprove.Visible = false;
        setGridData(gvApprove, null);

        //set file document
       
        GvDocumentPromise.Visible = false;
        setGridData(GvDocumentPromise, null);

        _PanelSearchDetail.Visible = false;

        PanelFilePromise.Visible = false;

        switch (activeTab)
        {
            case "docCreate":
                setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                
                switch (_chk_tab)
                {
                    case 0: //create


                        setFormData(fvCreate, FormViewMode.Insert, null);
                        DropDownList ddlTypeDocument = (DropDownList)fvCreate.FindControl("ddlTypeDocument");
                        DropDownList ddlSubTypeDocument = (DropDownList)fvCreate.FindControl("ddlSubTypeDocument");
                        DropDownList ddlOrg = (DropDownList)fvCreate.FindControl("ddlOrg");

                        getPMtypeDetail(ddlTypeDocument, 0);
                        getPMSubtypeDetail(ddlSubTypeDocument, 0);

                        getOrganizationList(ddlOrg);

                        break;
                    case 1: //view

                        // setFormData(fvCreate, FormViewMode.ReadOnly, null);
                        getViewDetailPromise(fvCreate, uidx, nodeidx);

                        DropDownList ddlTypeDocument_ = (DropDownList)fvCreate.FindControl("ddlTypeDocument");
                        DropDownList ddlSubTypeDocument_ = (DropDownList)fvCreate.FindControl("ddlSubTypeDocument");

                        TextBox txt_type_document_idx = (TextBox)fvCreate.FindControl("txt_type_document_idx");
                        TextBox txt_subtype_document_idx = (TextBox)fvCreate.FindControl("txt_subtype_document_idx");

                        getPMtypeDetail(ddlTypeDocument_, int.Parse(txt_type_document_idx.Text));
                        getPMSubtypeDetail(ddlSubTypeDocument_, int.Parse(txt_subtype_document_idx.Text));

                        Panel UpdatePanel_gvDetailLawOfficer = (Panel)fvCreate.FindControl("UpdatePanel_gvDetailLawOfficer");
                        GridView gvDetailLawOfficer_ = (GridView)fvCreate.FindControl("gvDetailLawOfficer");
                        //gvDetailLawOfficer_.Visible = true;
                        getDetailLawOfficer(gvDetailLawOfficer_, uidx, UpdatePanel_gvDetailLawOfficer);

                        div_Log.Visible = true;
                        getLogViewDetail(rptLog, uidx);


                        //select file
                        Panel UpdatePanel_gvFileUserCreate = (Panel)fvCreate.FindControl("UpdatePanel_gvFileUserCreate");
                        GridView gvFileUserCreate = (GridView)UpdatePanel_gvFileUserCreate.FindControl("gvFileUserCreate");

                        Panel UpdatePanel_gvFileUserCheckPromise = (Panel)fvCreate.FindControl("UpdatePanel_gvFileUserCheckPromise");
                        GridView gvFileUserCheckPromise = (GridView)UpdatePanel_gvFileUserCheckPromise.FindControl("gvFileUserCheckPromise");

                        Panel UpdatePanel_gvFileUserCheckScript = (Panel)fvCreate.FindControl("UpdatePanel_gvFileUserCheckScript");
                        GridView gvFileUserCheckScript = (GridView)UpdatePanel_gvFileUserCheckScript.FindControl("gvFileUserCheckScript");

                        Panel UpdatePanel_gvFileLawCreatePromise = (Panel)fvCreate.FindControl("UpdatePanel_gvFileLawCreatePromise");
                        GridView gvFileLawCreatePromise = (GridView)UpdatePanel_gvFileLawCreatePromise.FindControl("gvFileLawCreatePromise");

                        Panel UpdatePanel_gvFileLawCreateScript = (Panel)fvCreate.FindControl("UpdatePanel_gvFileLawCreateScript");
                        GridView gvFileLawCreateScript = (GridView)UpdatePanel_gvFileLawCreateScript.FindControl("gvFileLawCreateScript");

                        Panel UpdatePanel_gvFileLawSavePromise = (Panel)fvCreate.FindControl("UpdatePanel_gvFileLawSavePromise");
                        GridView gvFileLawSavePromise = (GridView)UpdatePanel_gvFileLawSavePromise.FindControl("gvFileLawSavePromise");


                        string getPath_file_usercreate = ConfigurationSettings.AppSettings["path_file_law_usercreatefilepermise"];
                        string getPath_file_law_filepermise = ConfigurationSettings.AppSettings["path_file_law_filepermise"];
                        string getPath_file_law_filescript = ConfigurationSettings.AppSettings["path_file_law_lawscriptfilepermise"];
                        string getPath_file_law_filesavescript = ConfigurationSettings.AppSettings["path_file_law_lawsave_permise"];

                        string getPath_file_usercheckpermise = ConfigurationSettings.AppSettings["path_file_law_usercheckfilepermise"];
                        string getPath_file_usercheckscript = ConfigurationSettings.AppSettings["path_file_law_usercheckscriptfile"];

                        string user_create = "usercreatedoc-" + uidx.ToString();
                        string usercheckpermise = "usercheck_permise-" + uidx.ToString();
                        string usercheckscript = "usercheck_script-" + uidx.ToString();

                        string law_filepermise = "law_permise-" + uidx.ToString();
                        string law_filescript = "lawscript_permise-" + uidx.ToString();
                        string law_filesavescript = "lawsave_permise-" + uidx.ToString();

                        

                        //select path show file
                        SelectPathFile(user_create.ToString(), getPath_file_usercreate, gvFileUserCreate, UpdatePanel_gvFileUserCreate);
                        SelectPathFile(law_filepermise.ToString(), getPath_file_law_filepermise, gvFileLawCreatePromise, UpdatePanel_gvFileLawCreatePromise);
                        SelectPathFile(law_filescript.ToString(), getPath_file_law_filescript, gvFileLawCreateScript, UpdatePanel_gvFileLawCreateScript);
                        SelectPathFile(law_filesavescript.ToString(), getPath_file_law_filesavescript, gvFileLawSavePromise, UpdatePanel_gvFileLawSavePromise);

                        SelectPathFile(usercheckpermise.ToString(), getPath_file_usercheckpermise, gvFileUserCheckPromise, UpdatePanel_gvFileUserCheckPromise);
                        SelectPathFile(usercheckscript.ToString(), getPath_file_usercheckscript, gvFileUserCheckScript, UpdatePanel_gvFileUserCheckScript);
                        break;

                    case 2: //approve

                        if(nodeidx == 15)
                        {

                            getViewDetailPromise(fvCreate, uidx, nodeidx);

                            DropDownList ddlTypeDocument_approve = (DropDownList)fvCreate.FindControl("ddlTypeDocument");
                            DropDownList ddlSubTypeDocument_approve = (DropDownList)fvCreate.FindControl("ddlSubTypeDocument");

                            TextBox txt_type_document_idx_approve = (TextBox)fvCreate.FindControl("txt_type_document_idx");
                            TextBox txt_subtype_document_idx_approve = (TextBox)fvCreate.FindControl("txt_subtype_document_idx");

                            getPMtypeDetail(ddlTypeDocument_approve, int.Parse(txt_type_document_idx_approve.Text));
                            getPMSubtypeDetail(ddlSubTypeDocument_approve, int.Parse(txt_subtype_document_idx_approve.Text));

                            div_Log.Visible = true;
                            getLogViewDetail(rptLog, uidx);

                            //////select file
                            ////GridView gvFileUserCreate_ = (GridView)fvCreate.FindControl("gvFileUserCreate");
                            ////GridView gvFileLawCreatePromise_ = (GridView)fvCreate.FindControl("gvFileLawCreatePromise");
                            ////GridView gvFileLawCreateScript_ = (GridView)fvCreate.FindControl("gvFileLawCreateScript");
                            ////GridView gvFileLawSavePromise_ = (GridView)fvCreate.FindControl("gvFileLawSavePromise");

                            ////GridView gvFileUserCheckPromise_ = (GridView)fvCreate.FindControl("gvFileUserCheckPromise");
                            ////GridView gvFileUserCheckScript_ = (GridView)fvCreate.FindControl("gvFileUserCheckScript");

                            //select file
                            Panel Pn_gvFileUserCreate = (Panel)fvCreate.FindControl("Pn_gvFileUserCreate");
                            GridView gvFileUserCreate_ = (GridView)Pn_gvFileUserCreate.FindControl("gvFileUserCreate");

                            Panel Panel_gvFileUserCheckPromise = (Panel)fvCreate.FindControl("Panel_gvFileUserCheckPromise");
                            GridView gvFileUserCheckPromise_ = (GridView)Panel_gvFileUserCheckPromise.FindControl("gvFileUserCheckPromise");

                            Panel Panel_gvFileUserCheckScript = (Panel)fvCreate.FindControl("Panel_gvFileUserCheckScript");
                            GridView gvFileUserCheckScript_ = (GridView)Panel_gvFileUserCheckScript.FindControl("gvFileUserCheckScript");

                            Panel Panel_gvFileLawCreatePromise = (Panel)fvCreate.FindControl("Panel_gvFileLawCreatePromise");
                            GridView gvFileLawCreatePromise_ = (GridView)Panel_gvFileLawCreatePromise.FindControl("gvFileLawCreatePromise");

                            Panel Panel_gvFileLawCreateScript = (Panel)fvCreate.FindControl("Panel_gvFileLawCreateScript");
                            GridView gvFileLawCreateScript_ = (GridView)Panel_gvFileLawCreateScript.FindControl("gvFileLawCreateScript");

                            Panel Panel_gvFileLawSavePromise = (Panel)fvCreate.FindControl("Panel_gvFileLawSavePromise");
                            GridView gvFileLawSavePromise_ = (GridView)Panel_gvFileLawSavePromise.FindControl("gvFileLawSavePromise");

                            string getPath_file_usercreate_ = ConfigurationSettings.AppSettings["path_file_law_usercreatefilepermise"];
                            string getPath_file_law_filepermise_ = ConfigurationSettings.AppSettings["path_file_law_filepermise"];
                            string getPath_file_law_filescript_ = ConfigurationSettings.AppSettings["path_file_law_lawscriptfilepermise"];
                            string getPath_file_law_filesavescript_ = ConfigurationSettings.AppSettings["path_file_law_lawsave_permise"];

                            string getPath_file_usercheckpermise_ = ConfigurationSettings.AppSettings["path_file_law_usercheckfilepermise"];
                            string getPath_file_usercheckscript_ = ConfigurationSettings.AppSettings["path_file_law_usercheckscriptfile"];

                            string user_create_ = "usercreatedoc-" + uidx.ToString();
                            string law_filepermise_ = "law_permise-" + uidx.ToString();
                            string law_filescript_ = "lawscript_permise-" + uidx.ToString();
                            string law_filesavescript_ = "lawsave_permise-" + uidx.ToString();

                            string usercheckpermise_ = "usercheck_permise-" + uidx.ToString();
                            string usercheckscript_ = "usercheck_script-" + uidx.ToString();

                            //select path show file
                            SelectPathFile(user_create_.ToString(), getPath_file_usercreate_, gvFileUserCreate_, Pn_gvFileUserCreate);
                            SelectPathFile(law_filepermise_.ToString(), getPath_file_law_filepermise_, gvFileLawCreatePromise_, Panel_gvFileLawCreatePromise);
                            SelectPathFile(law_filescript_.ToString(), getPath_file_law_filescript_, gvFileLawCreateScript_, Panel_gvFileLawCreateScript);
                            SelectPathFile(law_filesavescript_.ToString(), getPath_file_law_filesavescript_, gvFileLawSavePromise_, Panel_gvFileLawSavePromise);

                            SelectPathFile(usercheckpermise_.ToString(), getPath_file_usercheckpermise_, gvFileUserCheckPromise_, Panel_gvFileUserCheckPromise);
                            SelectPathFile(usercheckscript_.ToString(), getPath_file_usercheckscript_, gvFileUserCheckScript_, Panel_gvFileUserCheckScript);

                        }
                        else
                        {
                            getViewDetailPromise(fvCreate, uidx, nodeidx);

                            DropDownList ddlTypeDocument_approve = (DropDownList)fvCreate.FindControl("ddlTypeDocument");
                            DropDownList ddlSubTypeDocument_approve = (DropDownList)fvCreate.FindControl("ddlSubTypeDocument");

                            TextBox txt_type_document_idx_approve = (TextBox)fvCreate.FindControl("txt_type_document_idx");
                            TextBox txt_subtype_document_idx_approve = (TextBox)fvCreate.FindControl("txt_subtype_document_idx");

                            getPMtypeDetail(ddlTypeDocument_approve, int.Parse(txt_type_document_idx_approve.Text));
                            getPMSubtypeDetail(ddlSubTypeDocument_approve, int.Parse(txt_subtype_document_idx_approve.Text));

                            div_Log.Visible = true;
                            getLogViewDetail(rptLog, uidx);

                            Panel UpdatePanel_gvDetailLawOfficer_ = (Panel)fvCreate.FindControl("UpdatePanel_gvDetailLawOfficer");
                            GridView gvDetailLawOfficer = (GridView)UpdatePanel_gvDetailLawOfficer_.FindControl("gvDetailLawOfficer");

                            gvDetailLawOfficer.Visible = true;
                            getDetailLawOfficer(gvDetailLawOfficer, uidx, UpdatePanel_gvDetailLawOfficer_);


                            //gvDetailLawOfficer.Visible = false;
                            //setGridData(gvDetailLawOfficer, null);

                            //litDebug.Text = nodeidx.ToString();

                            ////select file
                            //GridView gvFileUserCreate_approve = (GridView)fvCreate.FindControl("gvFileUserCreate");
                            //GridView gvFileLawCreatePromise_approve = (GridView)fvCreate.FindControl("gvFileLawCreatePromise");
                            //GridView gvFileLawCreateScript_approve = (GridView)fvCreate.FindControl("gvFileLawCreateScript");
                            //GridView gvFileLawSavePromise_approve = (GridView)fvCreate.FindControl("gvFileLawSavePromise");

                            //GridView gvFileUserCheckPromise_approve = (GridView)fvCreate.FindControl("gvFileUserCheckPromise");
                            //GridView gvFileUserCheckScript_approve = (GridView)fvCreate.FindControl("gvFileUserCheckScript");

                            //select file
                            Panel UpdatePanel_gvFileUserCreate_ = (Panel)fvCreate.FindControl("UpdatePanel_gvFileUserCreate");
                            GridView gvFileUserCreate_ = (GridView)UpdatePanel_gvFileUserCreate_.FindControl("gvFileUserCreate");

                            Panel UpdatePanel_gvFileUserCheckPromise_ = (Panel)fvCreate.FindControl("UpdatePanel_gvFileUserCheckPromise");
                            GridView gvFileUserCheckPromise_ = (GridView)UpdatePanel_gvFileUserCheckPromise_.FindControl("gvFileUserCheckPromise");

                            Panel UpdatePanel_gvFileUserCheckScript_ = (Panel)fvCreate.FindControl("UpdatePanel_gvFileUserCheckScript");
                            GridView gvFileUserCheckScript_ = (GridView)UpdatePanel_gvFileUserCheckScript_.FindControl("gvFileUserCheckScript");

                            Panel UpdatePanel_gvFileLawCreatePromise_ = (Panel)fvCreate.FindControl("UpdatePanel_gvFileLawCreatePromise");
                            GridView gvFileLawCreatePromise_ = (GridView)UpdatePanel_gvFileLawCreatePromise_.FindControl("gvFileLawCreatePromise");

                            Panel UpdatePanel_gvFileLawCreateScript_ = (Panel)fvCreate.FindControl("UpdatePanel_gvFileLawCreateScript");
                            GridView gvFileLawCreateScript_ = (GridView)UpdatePanel_gvFileLawCreateScript_.FindControl("gvFileLawCreateScript");

                            Panel UpdatePanel_gvFileLawSavePromise_ = (Panel)fvCreate.FindControl("UpdatePanel_gvFileLawSavePromise");
                            GridView gvFileLawSavePromise_ = (GridView)UpdatePanel_gvFileLawSavePromise_.FindControl("gvFileLawSavePromise");


                            string getPath_file_usercreate_approve = ConfigurationSettings.AppSettings["path_file_law_usercreatefilepermise"];
                            string getPath_file_law_filepermise_approve = ConfigurationSettings.AppSettings["path_file_law_filepermise"];
                            string getPath_file_law_filescript_approve = ConfigurationSettings.AppSettings["path_file_law_lawscriptfilepermise"];
                            string getPath_file_law_filesavescript_approve = ConfigurationSettings.AppSettings["path_file_law_lawsave_permise"];

                            string getPath_file_usercheckpermise_approve = ConfigurationSettings.AppSettings["path_file_law_usercheckfilepermise"];
                            string getPath_file_usercheckscript_approve = ConfigurationSettings.AppSettings["path_file_law_usercheckscriptfile"];

                            string user_create_approve = "usercreatedoc-" + uidx.ToString();
                            string law_filepermise_approve = "law_permise-" + uidx.ToString();
                            string law_filescript_approve = "lawscript_permise-" + uidx.ToString();
                            string law_filesavescript_approve = "lawsave_permise-" + uidx.ToString();

                            string usercheckpermise_approve = "usercheck_permise-" + uidx.ToString();
                            string usercheckscript_approve = "usercheck_script-" + uidx.ToString();

                            //select path show file
                            ////SelectPathFile(user_create_approve.ToString(), getPath_file_usercreate_approve, gvFileUserCreate_approve);
                            ////SelectPathFile(law_filepermise_approve.ToString(), getPath_file_law_filepermise_approve, gvFileLawCreatePromise_approve);
                            ////SelectPathFile(law_filescript_approve.ToString(), getPath_file_law_filescript_approve, gvFileLawCreateScript_approve);
                            ////SelectPathFile(law_filesavescript_approve.ToString(), getPath_file_law_filesavescript_approve, gvFileLawSavePromise_approve);
                            ////SelectPathFile(usercheckpermise_approve.ToString(), getPath_file_usercheckpermise_approve, gvFileUserCheckPromise_approve);
                            ////SelectPathFile(usercheckscript_approve.ToString(), getPath_file_usercheckscript_approve, gvFileUserCheckScript_approve);


                            //select path show file
                            SelectPathFile(user_create_approve.ToString(), getPath_file_usercreate_approve, gvFileUserCreate_, UpdatePanel_gvFileUserCreate_);
                            SelectPathFile(law_filepermise_approve.ToString(), getPath_file_law_filepermise_approve, gvFileLawCreatePromise_, UpdatePanel_gvFileLawCreatePromise_);
                            SelectPathFile(law_filescript_approve.ToString(), getPath_file_law_filescript_approve, gvFileLawCreateScript_, UpdatePanel_gvFileLawCreateScript_);
                            SelectPathFile(law_filesavescript_approve.ToString(), getPath_file_law_filesavescript_approve, gvFileLawSavePromise_, UpdatePanel_gvFileLawSavePromise_);

                            SelectPathFile(usercheckpermise_approve.ToString(), getPath_file_usercheckpermise_approve, gvFileUserCheckPromise_, UpdatePanel_gvFileUserCheckPromise_);
                            SelectPathFile(usercheckscript_approve.ToString(), getPath_file_usercheckscript_approve, gvFileUserCheckScript_, UpdatePanel_gvFileUserCheckScript_);


                            switch (nodeidx)
                            {
                                case 16: //dir/mg approve

                                    setFormData(fvDirectorUser, FormViewMode.Insert, null);
                                    DropDownList ddlDirectorUserApprove = (DropDownList)fvDirectorUser.FindControl("ddlDirectorUserApprove");
                                    getDecisionApprove(ddlDirectorUserApprove, nodeidx);

                                    break;
                                case 17: //mg law select receive and select law officer

                                    setFormData(fvMgLaw, FormViewMode.Insert, null);
                                    DropDownList ddlMgLawApprove = (DropDownList)fvMgLaw.FindControl("ddlMgLawApprove");
                                    getDecisionApprove(ddlMgLawApprove, nodeidx);
                                    break;

                                case 18: //law officer attrach promise


                                    //gvDetailLawOfficer.Visible = true;
                                    //getDetailLawOfficer(gvDetailLawOfficer, uidx);

                                    setFormData(fvLaw, FormViewMode.Insert, null);
                                    DropDownList ddlLawApprove = (DropDownList)fvLaw.FindControl("ddlLawApprove");
                                    getDecisionApprove(ddlLawApprove, nodeidx);

                                    break;
                                case 19: //user check file or edit

                                    //gvDetailLawOfficer.Visible = true;
                                    //getDetailLawOfficer(gvDetailLawOfficer, uidx);

                                    setFormData(fvUserCheckfilepromise, FormViewMode.Insert, null);
                                    DropDownList ddlUserCheckfile = (DropDownList)fvUserCheckfilepromise.FindControl("ddlUserCheckfile");
                                    getDecisionApprove(ddlUserCheckfile, nodeidx);

                                    break;
                                case 20: //law check and attract file script

                                    //gvDetailLawOfficer.Visible = true;
                                    //getDetailLawOfficer(gvDetailLawOfficer, uidx);

                                    setFormData(fvLawFileScript, FormViewMode.Insert, null);
                                    DropDownList ddlStatusScript = (DropDownList)fvLawFileScript.FindControl("ddlStatusScript");
                                    getDecisionApprove(ddlStatusScript, nodeidx);

                                    break;
                                case 21: //user check file script

                                    //gvDetailLawOfficer.Visible = true;
                                    //getDetailLawOfficer(gvDetailLawOfficer, uidx);

                                    setFormData(fvUsercheckScript, FormViewMode.Insert, null);
                                    DropDownList ddlUsercheckScript = (DropDownList)fvUsercheckScript.FindControl("ddlUsercheckScript");
                                    getDecisionApprove(ddlUsercheckScript, nodeidx);



                                    break;
                                case 22: //law file promise success

                                    //gvDetailLawOfficer.Visible = true;
                                    //getDetailLawOfficer(gvDetailLawOfficer, uidx);

                                    setFormData(fvSavePromise, FormViewMode.Insert, null);
                                    DropDownList ddlLawSavePromise = (DropDownList)fvSavePromise.FindControl("ddlLawSavePromise");
                                    getDecisionApprove(ddlLawSavePromise, nodeidx);

                                    break;
                            }

                        }

                        break;

                }
                
                break;

            case "docDetail":

                switch (_chk_tab)
                {
                    case 0:

                        _PanelSearchDetail.Visible = true;
                     
                        getPMtypeDetail(ddlTypeDocumentSearch, 0);
                        getPMSubtypeDetail(ddlSubTypeDocumentSearch, 0);
                        txtDocumentSearch.Text = string.Empty;

                        GvDetail.Visible = true;
                        getDetailPromise(GvDetail, 0, _emp_idx);
                        

                        break;
                    case 1:

                        break;
                }

                setOntop.Focus();
                break;
            case "docApprove":

                switch (_chk_tab)
                {
                    case 0:

                        gvApprove.Visible = true;
                        //getDetailPromise(GvDetail, 0);

                        getWaitApproveDetailPromise(gvApprove, _emp_idx);

                        break;
                    case 1:

                        break;
                }
                setOntop.Focus();
                break;

            case "docPromise":

                PanelFilePromise.Visible = true;
                getPMtypeDetail(ddlTypeDocumentSearchFile, 0);
                getPMSubtypeDetail(ddlSubTypeDocumentSearchFile, 0);
                txtDocumentSearchFile.Text = string.Empty;

                GvDocumentPromise.Visible = true;
                getDetailFileAttrach(GvDocumentPromise);

                break;

        }
    }

    protected void setActiveTab(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cemp_idx, int u1idx)
    {
        setActiveView(activeTab, uidx, nodeidx, staidx, actor_idx, _chk_tab, _cemp_idx, u1idx);
        switch (activeTab)
        {
            case "docCreate":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");

                break;

            case "docDetail":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");

                break;
            case "docApprove":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                li3.Attributes.Add("class", "");

                break;
            case "docPromise":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
                break;



        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }

    protected string getStatus(int status)
    {
        if (status == 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Certificate'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='No Certificate'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        //specified ASP.NET server control at run time.
    }

    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;
        switch (rptName.ID)
        {

            case "rptBindbtnApprove":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    //Repeater rptBindbtnApprove = (Repeater)FvHeadUserApprove.FindControl("rptBindbtnApprove");
                    //var chk_coler = (Label)e.Item.FindControl("lbcheck_coler");
                    //var btnHeadUserSaveApprove = (LinkButton)e.Item.FindControl("btnHeadUserSaveApprove");

                    //for (int k = 0; k <= rptBindbtnApprove.Items.Count; k++)
                    //{
                    //    btnHeadUserSaveApprove.CssClass = ConfigureColors(k);
                    //    //Console.WriteLine(i);
                    //}


                }
                break;
            
        }
    }

    #endregion reuse
}