using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_form : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
    	if(!IsPostBack)
    	{
    		fvExample.DataSource = null;
    		fvExample.DataBind();
    	}
    }
}