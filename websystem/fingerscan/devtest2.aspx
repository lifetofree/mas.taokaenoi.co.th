<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="devtest2.aspx.cs" Inherits="websystem_fingerscan_devtest2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Connection &amp; Method</div>
                <div class="panel-body">
                    <div class="form-horizontal" role="form">
                        <%--<div class="form-group">
                            <label class="col-sm-4 control-label">template</label>
                            <div class="col-md-8">
                                <asp:DropDownList ID="ddlTemplate" runat="server" CssClass="form-control">
                                    <asp:ListItem Text="entity" Value="entity"></asp:ListItem>
                                    <asp:ListItem Text="events" Value="events"></asp:ListItem>
                                    <asp:ListItem Text="activealarms" Value="activealarms"></asp:ListItem>
                                    <asp:ListItem Text="action" Value="action"></asp:ListItem>
                                    <asp:ListItem Text="activemacros" Value="activemacros"></asp:ListItem>
                                    <asp:ListItem Text="report" Value="report"></asp:ListItem>
                                    <asp:ListItem Text="customField" Value="customField"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">sub-template</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="tbSubTemplate" runat="server" CssClass="form-control"
                                    placeholder="sub-template" MaxLength="50" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">query</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="tbQuery" runat="server" CssClass="form-control" placeholder="query" MaxLength="500" TextMode="Multiline" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-md-8">
                                <asp:LinkButton ID="lbGet" CssClass="btn btn-success" runat="server"
                                    data-original-title="Get" Text="GET" OnCommand="btnCommand" CommandName="cmdGet">
                                </asp:LinkButton>
                                <asp:LinkButton ID="lbPost" CssClass="btn btn-primary" runat="server"
                                    data-original-title="Post" Text="POST" OnCommand="btnCommand" CommandName="cmdPost">
                                </asp:LinkButton>
                                <asp:LinkButton ID="lbDelete" CssClass="btn btn-danger" runat="server"
                                    data-original-title="Delete" Text="DELETE" OnCommand="btnCommand" CommandName="cmdDelete" OnClientClick='return confirm("Do you want to delete this?")'>
                                </asp:LinkButton>
                                <asp:LinkButton ID="lbPut" CssClass="btn btn-warning" runat="server"
                                    data-original-title="Put" Text="PUT" OnCommand="btnCommand" CommandName="cmdPut">
                                </asp:LinkButton>
                            </div>
                        </div>--%>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">IP</label>
                            <div class="col-md-8">
                                <asp:TextBox ID="tbIp" runat="server" CssClass="form-control"
                                    placeholder="ip address" Text="192.168.120.17" MaxLength="50" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-md-8">
                                <asp:LinkButton ID="lbGetVersion" CssClass="btn btn-success" runat="server"
                                    data-original-title="Get Version" Text="Get Version" OnCommand="btnCommand" CommandName="cmdGetVersion">
                                </asp:LinkButton>
                                <asp:LinkButton ID="lbAllocate" CssClass="btn btn-success" runat="server"
                                    data-original-title="Allocate" Text="Allocate" OnCommand="btnCommand" CommandName="cmdAllocate">
                                </asp:LinkButton>
                                <asp:LinkButton ID="lbInitialize" CssClass="btn btn-success" runat="server"
                                    data-original-title="Initialize" Text="Initialize" OnCommand="btnCommand" CommandName="cmdInitialize">
                                </asp:LinkButton>
                                <asp:LinkButton ID="lbRelease" CssClass="btn btn-success" runat="server"
                                    data-original-title="Release" Text="Release" OnCommand="btnCommand" CommandName="cmdRelease">
                                </asp:LinkButton>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label"></label>
                            <div class="col-md-8">
                                <asp:LinkButton ID="lbConnect" CssClass="btn btn-success" runat="server"
                                    data-original-title="Connect" Text="Connect and Get Info" OnCommand="btnCommand" CommandName="cmdConnect">
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Result</div>
                <div class="panel-body">
                    <asp:Literal ID="litResult" runat="server"></asp:Literal>
                </div>
            </div>
        </div>
    </div>
    <asp:GridView ID="gvResult" runat="server" AutoGenerateColumns="True" CssClass="table table-striped table-bordered">
    </asp:GridView>
</asp:Content>