﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


public partial class websystem_hr_hr_blacklist : System.Web.UI.Page
{
    function_tool _funcTool = new function_tool();

    data_employee _dataEmployee = new data_employee();
    //data_testnew _data_testnew = new data_testnew();

    string _local_xml = String.Empty;
    string _localJson = String.Empty;
    int _emp_idx = 0;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetNationalityList = _serviceUrl + ConfigurationManager.AppSettings["urlGetNationalityList"];

    static string _urlGetblacklistM0 = _serviceUrl + ConfigurationManager.AppSettings["urlGetblacklistM0"];
    static string _urlSetblacklistU0 = _serviceUrl + ConfigurationManager.AppSettings["urlSetblacklistU0"];
    static string _urlGetblacklistU0 = _serviceUrl + ConfigurationManager.AppSettings["urlGetblacklistU0"];
    static string _urlGetblacklistU1 = _serviceUrl + ConfigurationManager.AppSettings["urlGetblacklistU1"];
    static string _urlSetblacklistU0del = _serviceUrl + ConfigurationManager.AppSettings["urlSetblacklistU0del"];
    static string _urlGetblacklistlog = _serviceUrl + ConfigurationManager.AppSettings["urlGetblacklistlog"];
    static string _urlGetSearchDetailBlacklist = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchDetailBlacklist"];

    public string checkfile;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {

            SelectblacklistDetail_U0();
            div_Logblacklist.Visible = false;


        }

        linkBtnTrigger(btnAdd);
        //linkBtnTrigger(btnViewblacklistDetail);

    }

    #region Trigger
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    #endregion

    #region Page_Init

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
    }
    #endregion

    #region getEmployeeProfile
    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }
    #endregion

    #region btnCommand
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdAdd":
                btnAdd.Visible = false;
                _PanelSearchBlacklist.Visible = false;

                setFormData(FvInsert, FormViewMode.Insert, null);
                DropDownList ddlnation = (DropDownList)FvInsert.FindControl("ddlnation");
                select_nationality(ddlnation);
                SelectblacklistDetail();
                Gvblacklistu0.Visible = false;
                FvInsert.Visible = true;
                div_Logblacklist.Visible = false;
                break;

            case "CmdSave":
                InsertblacklistDetail();
                div_Logblacklist.Visible = false;
                break;

            case "CmdCancel":
                btnAdd.Visible = true;
                _PanelSearchBlacklist.Visible = true;

                FvInsert.Visible = false;
                Gvblacklistu0.Visible = true;
                div_Logblacklist.Visible = false;

                break;

            case "CmdCancel_edit":
                FvDetailBlacklist.Visible = true;
                FvDetailBlacklist.ChangeMode(FormViewMode.ReadOnly);
                actionReadblacklistDetail(int.Parse(cmdArg));
                div_Logblacklist.Visible = true;
                break;

            case "cmdViewblacklistDetail":

                linkBtnTrigger(btnViewblacklistDetail);

                btnAdd.Visible = false;
                _PanelSearchBlacklist.Visible = false;

                FvDetailBlacklist.Visible = true;
                Gvblacklistu0.Visible = false;
                actionReadblacklistDetail(int.Parse(cmdArg));
                linkBtnTrigger(btnEditDetailBlacklist);
                div_Logblacklist.Visible = true;
                getLogDetailblacklist(int.Parse(cmdArg));

                break;
            case "cmdBackToDetailIndexblacklist":

                btnAdd.Visible = true;
                _PanelSearchBlacklist.Visible = true;

                FvInsert.Visible = false;
                Gvblacklistu0.Visible = true;
                FvDetailBlacklist.Visible = false;
                SelectblacklistDetail_U0();
                div_Logblacklist.Visible = false;
                break;

            case "cmdEditDetailBlacklist":

                FvDetailBlacklist.Visible = true;
                FvDetailBlacklist.ChangeMode(FormViewMode.Edit);
                actionEditblacklistDetail(int.Parse(cmdArg));
                div_Logblacklist.Visible = false;
                break;

            case "cmdSaveEdit":
                div_Logblacklist.Visible = true;
                Label lbl_u0_blacklist_idx_edit = (Label)FvDetailBlacklist.FindControl("lbl_u0_blacklist_idx_edit");
                TextBox txt_Idcard_edit = (TextBox)FvDetailBlacklist.FindControl("txt_Idcard_edit");
                TextBox txt_Passpost_edit = (TextBox)FvDetailBlacklist.FindControl("txt_Passpost_edit");
                DropDownList ddl_prefixth_edit = (DropDownList)FvDetailBlacklist.FindControl("ddl_prefixth_edit");
                TextBox txt_nameth_edit = (TextBox)FvDetailBlacklist.FindControl("txt_nameth_edit");
                TextBox txt_surnameth_edit = (TextBox)FvDetailBlacklist.FindControl("txt_surnameth_edit");
                DropDownList ddl_prefixen_edit = (DropDownList)FvDetailBlacklist.FindControl("ddl_prefixen_edit");
                TextBox txt_FirstnameEN_edit = (TextBox)FvDetailBlacklist.FindControl("txt_FirstnameEN_edit");
                TextBox txt_LastnameEN_edit = (TextBox)FvDetailBlacklist.FindControl("txt_LastnameEN_edit");
                DropDownList ddlnation_edit1 = (DropDownList)FvDetailBlacklist.FindControl("ddlnation_edit");
                DropDownList ddl_race_edit = (DropDownList)FvDetailBlacklist.FindControl("ddl_race_edit");
                TextBox txt_note_edit = (TextBox)FvDetailBlacklist.FindControl("txt_note_edit");

                if (txt_Idcard_edit.Text != "" && txt_Idcard_edit.Text.Length < 13)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "No Data", "alert('" + "คุณกรอกข้อมูลบัตรประชาชนผิด" + "')", true);
                }
                else
                {
                    data_blacklist data_u0_blacklist_edit = new data_blacklist();
                    blacklist_u0_detail u0_blacklist_edit = new blacklist_u0_detail();
                    data_u0_blacklist_edit.blacklist_u0_list = new blacklist_u0_detail[1];

                    u0_blacklist_edit.U0_idx = int.Parse(lbl_u0_blacklist_idx_edit.Text);
                    u0_blacklist_edit.Idcard = txt_Idcard_edit.Text.Trim();
                    u0_blacklist_edit.Passpost = txt_Idcard_edit.Text;
                    u0_blacklist_edit.PrefixIDXTH = int.Parse(ddl_prefixth_edit.SelectedValue);
                    u0_blacklist_edit.FirstnameTH = txt_nameth_edit.Text;
                    u0_blacklist_edit.LastnameTH = txt_surnameth_edit.Text;
                    u0_blacklist_edit.PrefixIDXEN = int.Parse(ddl_prefixen_edit.SelectedValue);
                    u0_blacklist_edit.FirstnameEN = txt_FirstnameEN_edit.Text;
                    u0_blacklist_edit.LastnameEN = txt_LastnameEN_edit.Text;
                    u0_blacklist_edit.NatIDX = int.Parse(ddlnation_edit1.SelectedValue);
                    u0_blacklist_edit.RaceIDX = int.Parse(ddl_race_edit.SelectedValue);
                    u0_blacklist_edit.U0_note = txt_note_edit.Text;
                    u0_blacklist_edit.cemp_idx = _emp_idx;

                    GridView Gvblacklist_edit = (GridView)FvDetailBlacklist.FindControl("Gvblacklist_edit");
                    var u1_insert = new blacklist_u1_detail[Gvblacklist_edit.Rows.Count];
                    var u1_insert_log = new blacklist_log_detail[Gvblacklist_edit.Rows.Count];
                    int i = 0;
                    int countchkedit = 0;

                    foreach (GridViewRow row in Gvblacklist_edit.Rows)
                    {
                        CheckBox chk_full = ((CheckBox)row.FindControl("chk_full_edit"));
                        Label lbl_idxu1_edit = ((Label)row.FindControl("lbl_idxu1_edit"));
                        Label idxU1_edit = ((Label)row.FindControl("idxU1_edit"));
                        TextBox blacklist_note_edit = ((TextBox)row.FindControl("blacklist_note_edit"));

                        if (chk_full != null && chk_full.Checked)
                        {
                            if (blacklist_note_edit.Text != null && blacklist_note_edit.Text != "")
                            {
                                u1_insert[i] = new blacklist_u1_detail();
                                u1_insert[i].U0_IDX = int.Parse(lbl_u0_blacklist_idx_edit.Text);
                                u1_insert[i].U1_IDX = int.Parse(lbl_idxu1_edit.Text);
                                u1_insert[i].BlacklistIDX = int.Parse(idxU1_edit.Text);
                                u1_insert[i].Note = blacklist_note_edit.Text;
                                u1_insert[i].cemp_idx = _emp_idx;
                                u1_insert[i].status = 1;

                                u1_insert_log[i] = new blacklist_log_detail();
                                u1_insert_log[i].u0_idx = int.Parse(lbl_u0_blacklist_idx_edit.Text);
                                u1_insert_log[i].u1_idx = int.Parse(lbl_idxu1_edit.Text);
                                u1_insert_log[i].cemp_idx = _emp_idx;
                                u1_insert_log[i].status = 1;
                                countchkedit += 1;
                                i++;
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ผลการบันทึก", "alert('" + "ผลการทำงาน : " + "กรุณากรอกข้อมูลช่องหมายเหตุ" + "')", true);
                                return;
                            }
                        }
                        else
                        {
                            if (int.Parse(lbl_idxu1_edit.Text) != 0)
                            {
                                u1_insert[i] = new blacklist_u1_detail();
                                u1_insert[i].U0_IDX = int.Parse(lbl_u0_blacklist_idx_edit.Text);
                                u1_insert[i].U1_IDX = int.Parse(lbl_idxu1_edit.Text);
                                u1_insert[i].BlacklistIDX = int.Parse(idxU1_edit.Text);
                                u1_insert[i].Note = blacklist_note_edit.Text;
                                u1_insert[i].cemp_idx = _emp_idx;
                                u1_insert[i].status = 9;

                                u1_insert_log[i] = new blacklist_log_detail();
                                u1_insert_log[i].u0_idx = int.Parse(lbl_u0_blacklist_idx_edit.Text);
                                u1_insert_log[i].u1_idx = int.Parse(lbl_idxu1_edit.Text);
                                u1_insert_log[i].cemp_idx = _emp_idx;
                                u1_insert_log[i].status = 9;
                                i++;

                            }
                        }
                    }
                    if (countchkedit > 0)
                    {
                        data_u0_blacklist_edit.blacklist_u0_list[0] = u0_blacklist_edit;
                        data_u0_blacklist_edit.blacklist_u1_list = u1_insert;
                        data_u0_blacklist_edit.blacklist_log_list = u1_insert_log;
                        //litdebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_u0_blacklist));
                        //litdebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_blacklist_edit));
                        data_u0_blacklist_edit = callServicePostblacklist(_urlSetblacklistU0, data_u0_blacklist_edit);
                        //litdebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_blacklist_edit));

                        if (UploadFileblacklist_edit.HasFile)
                        {
                            string getPathfile_edit = ConfigurationManager.AppSettings["path_flie_blacklist"];
                            string directoryName_edit_old = txt_Idcard_edit.Text;
                            string directoryName_edit_new = txt_Idcard_edit.Text;
                            string filePath_old = Server.MapPath(getPathfile_edit + directoryName_edit_old);

                            if (!Directory.Exists(filePath_old))
                            {
                                HttpFileCollection UploadFile_new = Request.Files;
                                for (int ii = 0; ii < UploadFile_new.Count; ii++)
                                {
                                    HttpPostedFile userpost_createfilenew = UploadFile_new[ii];

                                    if (userpost_createfilenew.ContentLength > 0)
                                    {
                                        string getPath = ConfigurationSettings.AppSettings["path_flie_blacklist"];
                                        string fileName1 = txt_Idcard_edit.Text + ii;
                                        string fileName_upload = txt_Idcard_edit.Text;
                                        string filePath_new = Server.MapPath(getPathfile_edit + directoryName_edit_new);

                                        if (!Directory.Exists(filePath_new))
                                        {
                                            Directory.CreateDirectory(filePath_new);
                                        }
                                        string extension = Path.GetExtension(userpost_createfilenew.FileName);
                                        userpost_createfilenew.SaveAs(Server.MapPath(getPath + fileName_upload) + "\\" + fileName1 + extension);
                                    }
                                }
                            }
                            else
                            {
                                string path_old = string.Empty;
                                List<string> files = new List<string>();
                                DirectoryInfo dirListRoom = new DirectoryInfo(filePath_old);
                                int chk = 0;
                                foreach (FileInfo file in dirListRoom.GetFiles())
                                {
                                    path_old = ResolveUrl(filePath_old + "\\" + file.Name);
                                    chk += 1;
                                }
                                HttpFileCollection UploadFile_edit = Request.Files;
                                for (int ii = 0; ii < UploadFile_edit.Count; ii++)
                                {
                                    chk += 1;
                                    HttpPostedFile userpost_createfile = UploadFile_edit[ii];
                                    if (userpost_createfile.ContentLength > 0)
                                    {
                                        string getPath = ConfigurationSettings.AppSettings["path_flie_blacklist"];
                                        string fileName1 = txt_Idcard_edit.Text + chk.ToString();
                                        string fileName_upload = txt_Idcard_edit.Text;
                                        string extension = Path.GetExtension(userpost_createfile.FileName);
                                        string fileNamechk = txt_Idcard_edit.Text + chk.ToString() + extension;
                                        foreach (FileInfo file in dirListRoom.GetFiles())
                                        {
                                            path_old = file.Name;
                                            if (fileNamechk.ToString() == path_old.ToString())
                                            {
                                                chk += 1;
                                                fileName1 = txt_Idcard_edit.Text + chk.ToString();
                                            }
                                        }
                                        userpost_createfile.SaveAs(Server.MapPath(getPath + fileName_upload) + "\\" + fileName1 + extension);
                                    }
                                }
                            }
                        }
                        else
                        {
                        }

                        FvDetailBlacklist.Visible = true;
                        FvDetailBlacklist.ChangeMode(FormViewMode.ReadOnly);
                        actionReadblacklistDetail(int.Parse(cmdArg));
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ผลการบันทึก", "alert('" + "ผลการทำงาน : " + "กรุณาเลือกข้อมูล Blacklist" + "')", true);
                    }
                }

                break;

            case "Delete_updatefile":
                uploadfile_del(cmdArg);
                break;

            case "cmdDeleteblacklistDetail":
                data_blacklist data_u0_blacklist_del = new data_blacklist();
                blacklist_u0_detail u0_blacklist_del = new blacklist_u0_detail();
                data_u0_blacklist_del.blacklist_u0_list = new blacklist_u0_detail[1];

                u0_blacklist_del.U0_idx = int.Parse(cmdArg);
                u0_blacklist_del.cemp_idx = _emp_idx;
                data_u0_blacklist_del.blacklist_u0_list[0] = u0_blacklist_del;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_blacklist_del));
                data_u0_blacklist_del = callServicePostblacklist(_urlSetblacklistU0del, data_u0_blacklist_del);

                data_blacklist data_u1_blacklist_del = new data_blacklist();
                blacklist_u1_detail u1_blacklist_del = new blacklist_u1_detail();
                data_u1_blacklist_del.blacklist_u1_list = new blacklist_u1_detail[1];

                u1_blacklist_del.U0_IDX = int.Parse(cmdArg);
                u1_blacklist_del.cemp_idx = _emp_idx;
                data_u1_blacklist_del.blacklist_u1_list[0] = u1_blacklist_del;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1_blacklist_del));
                data_u1_blacklist_del = callServicePostblacklist(_urlSetblacklistU0del, data_u1_blacklist_del);

                SelectblacklistDetail_U0();

                break;

            case "Cmdsearch":

                data_blacklist data_m0_blacklist_detail_searchu0 = new data_blacklist();
                blacklist_u0_detail u0_blacklist_detail_search = new blacklist_u0_detail();
                data_m0_blacklist_detail_searchu0.blacklist_u0_list = new blacklist_u0_detail[1];

                u0_blacklist_detail_search.Idcard = txt_idcard_search.Text;
                u0_blacklist_detail_search.Passpost = txt_passport_search.Text;

                data_m0_blacklist_detail_searchu0.blacklist_u0_list[0] = u0_blacklist_detail_search;

                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0_blacklist_detail_searchu0));
                data_m0_blacklist_detail_searchu0 = callServicePostblacklist(_urlGetSearchDetailBlacklist, data_m0_blacklist_detail_searchu0);
                //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_blacklist_detail_searchu0));

                if (data_m0_blacklist_detail_searchu0.return_code == 0)
                {
                    //litdebug.Text = "1";
                    ViewState["Vs_DetailBlacklist"] = data_m0_blacklist_detail_searchu0.blacklist_u0_list;
                    setGridData(Gvblacklistu0, ViewState["Vs_DetailBlacklist"]);
                }
                else
                {
                    //litdebug.Text = "2";
                    ViewState["Vs_DetailBlacklist"] = null;
                    setGridData(Gvblacklistu0, ViewState["Vs_DetailBlacklist"]);
                }


                break;
            case "Cmdclear":

                txt_idcard_search.Text = string.Empty;
                txt_passport_search.Text = string.Empty;

                SelectblacklistDetail_U0();


                break;



        }
    }
    #endregion

    #region uploadfile_del
    protected void uploadfile_del(string FileName)
    {
        string getPathfile_edit = ConfigurationManager.AppSettings["path_flie_blacklist"];
        string directoryName_edit_old = FileName;
        var txt_Idcard_edit = (TextBox)FvDetailBlacklist.FindControl("txt_Idcard_edit");
        string filename = txt_Idcard_edit.Text;
        string filePath_old = Server.MapPath(getPathfile_edit + filename + "\\" + directoryName_edit_old);
        File.Delete(filePath_old);

        try
        {

            //var txt_Idcard_edit = (TextBox)FvDetailBlacklist.FindControl("txt_Idcard_edit");
            string getPathLotus = ConfigurationSettings.AppSettings["path_flie_blacklist"];
            string fileName_upload = txt_Idcard_edit.Text;
            string filePathLotus = Server.MapPath(getPathLotus + fileName_upload);
            DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

            SearchDirectories_edit(myDirLotus, fileName_upload);

        }
        catch
        {

        }
    }
    #endregion

    #region RowIndexChangedBlacklist
    protected void RowIndexChangedBlacklist(object sender, EventArgs e)
    {


        var cbName = (CheckBox)sender;

        //var chkName = (CheckBoxList)sender;

        switch (cbName.ID)
        {
            case "chk_full":

                int selRowIndex = ((GridViewRow)(((CheckBox)sender).Parent.Parent)).RowIndex;

                GridView Gvblacklist = (GridView)FvInsert.FindControl("Gvblacklist");
                CheckBox chk_full = (CheckBox)Gvblacklist.Rows[selRowIndex].FindControl("chk_full");
                TextBox blacklist_note = (TextBox)Gvblacklist.Rows[selRowIndex].FindControl("blacklist_note");

                if (chk_full.Checked)
                {
                    //litdebug.Text = "1111";
                    blacklist_note.Visible = true;
                }
                else
                {
                    blacklist_note.Visible = false;
                    blacklist_note.Text = "";
                }
                break;
        }

    }
    protected void RowIndexChangedBlacklist_edit(object sender, EventArgs e)
    {
        int selRowIndex = ((GridViewRow)(((CheckBox)sender).Parent.Parent)).RowIndex;
        GridView Gvblacklist_edit = (GridView)FvDetailBlacklist.FindControl("Gvblacklist_edit");
        CheckBox chk_full_edit = (CheckBox)Gvblacklist_edit.Rows[selRowIndex].FindControl("chk_full_edit");
        TextBox blacklist_note_edit = (TextBox)Gvblacklist_edit.Rows[selRowIndex].FindControl("blacklist_note_edit");

        if (chk_full_edit.Checked) { }
        else
        {
            blacklist_note_edit.Text = "";
        }
    }
    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        DropDownList ddl_prefix_th = (DropDownList)FvInsert.FindControl("ddl_prefix_th");
        DropDownList ddl_prefix_en = (DropDownList)FvInsert.FindControl("ddl_prefix_en");

        DropDownList ddl_prefixth_edit = (DropDownList)FvDetailBlacklist.FindControl("ddl_prefixth_edit");
        DropDownList ddl_prefixen_edit = (DropDownList)FvDetailBlacklist.FindControl("ddl_prefixen_edit");

        switch (ddName.ID)
        {
            case "ddl_prefix_th":
                if (ddl_prefix_th.SelectedValue == "1") //นาย
                {
                    ddl_prefix_en.SelectedValue = "1";
                }
                else if (ddl_prefix_th.SelectedValue == "2") //นางสาว
                {
                    ddl_prefix_en.SelectedValue = "2";
                }
                else if (ddl_prefix_th.SelectedValue == "3") //นาง
                {
                    ddl_prefix_en.SelectedValue = "3";
                }
                break;
            case "ddl_prefix_en":
                if (ddl_prefix_en.SelectedValue == "1") //นาย
                {
                    ddl_prefix_th.SelectedValue = "1";
                }
                else if (ddl_prefix_en.SelectedValue == "2") //นางสาว
                {
                    ddl_prefix_th.SelectedValue = "2";
                }
                else if (ddl_prefix_en.SelectedValue == "3") //นาง
                {
                    ddl_prefix_th.SelectedValue = "3";
                }
                break;

            case "ddl_prefixth_edit":
                if (ddl_prefixth_edit.SelectedValue == "1") //นาย
                {
                    ddl_prefixen_edit.SelectedValue = "1";
                }
                else if (ddl_prefixth_edit.SelectedValue == "2") //นางสาว
                {
                    ddl_prefixen_edit.SelectedValue = "2";
                }
                else if (ddl_prefixth_edit.SelectedValue == "3") //นาง
                {
                    ddl_prefixen_edit.SelectedValue = "3";
                }
                break;

            case "ddl_prefixen_edit":
                if (ddl_prefixen_edit.SelectedValue == "1") //นาย
                {
                    ddl_prefixth_edit.SelectedValue = "1";
                }
                else if (ddl_prefixen_edit.SelectedValue == "2") //นางสาว
                {
                    ddl_prefixth_edit.SelectedValue = "2";
                }
                else if (ddl_prefixen_edit.SelectedValue == "3") //นาง
                {
                    ddl_prefixth_edit.SelectedValue = "3";
                }
                break;
        }
    }
    #endregion

    #region actionRead&&edit
    protected void actionReadblacklistDetail(int _U0_idx)
    {
        LinkButton btnViewblacklistDetail = (LinkButton)FvInsert.FindControl("btnViewblacklistDetail");
        data_blacklist data_u0_blacklist_view = new data_blacklist();
        blacklist_u0_detail u0_blacklist_view = new blacklist_u0_detail();
        data_u0_blacklist_view.blacklist_u0_list = new blacklist_u0_detail[1];

        u0_blacklist_view.condition = 0;
        u0_blacklist_view.U0_idx = _U0_idx;

        data_u0_blacklist_view.blacklist_u0_list[0] = u0_blacklist_view;

        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0_test_detail));
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_blacklist_view));
        data_u0_blacklist_view = callServicePostblacklist(_urlGetblacklistU0, data_u0_blacklist_view);

        setFormData(FvDetailBlacklist, FormViewMode.ReadOnly, data_u0_blacklist_view.blacklist_u0_list);

        GridView GvDetailblacklist = (GridView)FvDetailBlacklist.FindControl("GvDetailblacklist");
        setGridData(GvDetailblacklist, data_u0_blacklist_view.blacklist_u0_list);

        try
        {
            var lbl_Idcard_detail = (Label)FvDetailBlacklist.FindControl("lbl_Idcard_detail");
            string getPathLotus = ConfigurationSettings.AppSettings["path_flie_blacklist"];
            string fileName_upload = lbl_Idcard_detail.Text;
            string filePathLotus = Server.MapPath(getPathLotus + fileName_upload);
            DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

            SearchDirectories(myDirLotus, fileName_upload);

        }
        catch
        {

        }

      
    }

    protected void getLogDetailblacklist(int _u0_idx_log)
    {

        data_blacklist data_log_blacklist_view = new data_blacklist();
        blacklist_log_view log_blacklist_view = new blacklist_log_view();
        data_log_blacklist_view.blacklist_log_listview = new blacklist_log_view[1];

        log_blacklist_view.u0_idx = _u0_idx_log;

        data_log_blacklist_view.blacklist_log_listview[0] = log_blacklist_view;
        data_log_blacklist_view = callServicePostblacklist(_urlGetblacklistlog, data_log_blacklist_view);


        //ViewState["vs_LogDetailblacklist"] = data_log_blacklist_view.blacklist_log_listview;
        //Repeater rptLogblacklist = (Repeater)FormView1.FindControl("rptLogblacklist");
        setRepeaterData(rptLogblacklist, data_log_blacklist_view.blacklist_log_listview);

    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void actionEditblacklistDetail(int _U0_idx)
    {
        data_blacklist data_u0_blacklist_view = new data_blacklist();
        blacklist_u0_detail u0_blacklist_view = new blacklist_u0_detail();
        data_u0_blacklist_view.blacklist_u0_list = new blacklist_u0_detail[1];

        u0_blacklist_view.condition = 0;
        u0_blacklist_view.U0_idx = _U0_idx;

        data_u0_blacklist_view.blacklist_u0_list[0] = u0_blacklist_view;

        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0_test_detail));
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_blacklist_view));
        data_u0_blacklist_view = callServicePostblacklist(_urlGetblacklistU0, data_u0_blacklist_view);
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_blacklist_view));
        setFormData(FvDetailBlacklist, FormViewMode.Edit, data_u0_blacklist_view.blacklist_u0_list);


        var ddlnation_edit = (DropDownList)FvDetailBlacklist.FindControl("ddlnation_edit");
        var txt_nat_edit = (Label)FvDetailBlacklist.FindControl("txt_nat_edit");
        ddlnation_edit.AppendDataBoundItems = true;
        ddlnation_edit.Items.Clear();
        data_employee _dtEmployee = new data_employee();
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();

        _dataEmployee.R0Address_list[0] = _addresslist;
        _dataEmployee = callServiceEmployee(_urlGetNationalityList, _dataEmployee);
        ddlnation_edit.DataSource = _dataEmployee.R0Address_list;
        ddlnation_edit.DataTextField = "NatName";
        ddlnation_edit.DataValueField = "NatIDX";
        ddlnation_edit.DataBind();
        ddlnation_edit.Items.Insert(0, new ListItem("เลือกสัญชาติ....", "0"));
        ddlnation_edit.SelectedValue = txt_nat_edit.Text;

        GridView Gvblacklist_edit = (GridView)FvDetailBlacklist.FindControl("Gvblacklist_edit");
        data_blacklist data_u1_blacklist_view = new data_blacklist();
        blacklist_u1_detail u1_blacklist_view = new blacklist_u1_detail();
        data_u1_blacklist_view.blacklist_u1_list = new blacklist_u1_detail[1];

        u1_blacklist_view.condition = 0;
        u1_blacklist_view.U0_IDX = _U0_idx;

        data_u1_blacklist_view.blacklist_u1_list[0] = u1_blacklist_view;
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1_blacklist_view));
        data_u1_blacklist_view = callServicePostblacklist(_urlGetblacklistU1, data_u1_blacklist_view);
        setGridData(Gvblacklist_edit, data_u1_blacklist_view.blacklist_u1_list);

        try
        {

            var txt_Idcard_edit = (TextBox)FvDetailBlacklist.FindControl("txt_Idcard_edit");
            string getPathLotus = ConfigurationSettings.AppSettings["path_flie_blacklist"];
            string fileName_upload = txt_Idcard_edit.Text;
            string filePathLotus = Server.MapPath(getPathLotus + fileName_upload);
            DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);

            SearchDirectories_edit(myDirLotus, fileName_upload);

        }
        catch
        {

        }

    }
    #endregion

    #region setForm && setGrid

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    private void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region PageIndexChanging
    protected void U0_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView GvName = (GridView)sender;
        GvName.PageIndex = e.NewPageIndex;

        switch (GvName.ID)
        {
            case "Gvblacklistu0":
                //Gvblacklistu0.PageIndex = e.NewPageIndex;

                setGridData(Gvblacklistu0, ViewState["Vs_DetailBlacklist"]);

                //Gvblacklistu0.DataBind();
                //SelectblacklistDetail_U0();
                break;
        }
    }
    #endregion

    #region InsertblacklistDetail
    protected void InsertblacklistDetail()
    {
        LinkButton btnSave = (LinkButton)FvInsert.FindControl("btnSave");
        TextBox Idcard = (TextBox)FvInsert.FindControl("Idcard");
        TextBox Passpost = (TextBox)FvInsert.FindControl("Passpost");
        DropDownList ddl_prefix_th = (DropDownList)FvInsert.FindControl("ddl_prefix_th");
        TextBox txt_name_th = (TextBox)FvInsert.FindControl("txt_name_th");
        TextBox txt_surname_th = (TextBox)FvInsert.FindControl("txt_surname_th");
        DropDownList ddl_prefix_en = (DropDownList)FvInsert.FindControl("ddl_prefix_en");
        TextBox FirstnameEN = (TextBox)FvInsert.FindControl("FirstnameEN");
        TextBox LastnameEN = (TextBox)FvInsert.FindControl("LastnameEN");
        DropDownList ddlnation = (DropDownList)FvInsert.FindControl("ddlnation");
        DropDownList ddlrace = (DropDownList)FvInsert.FindControl("ddlrace");
        TextBox txtnote = (TextBox)FvInsert.FindControl("txtnote");

        if (Idcard.Text != "" && Idcard.Text.Length < 13)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "No Data", "alert('" + "คุณกรอกข้อมูลบัตรประชาชนผิด" + "')", true);
        }
        else
        {
            data_blacklist data_u0_blacklist = new data_blacklist();
            blacklist_u0_detail u0_blacklist_insert = new blacklist_u0_detail();
            data_u0_blacklist.blacklist_u0_list = new blacklist_u0_detail[1];

            u0_blacklist_insert.U0_idx = 0;
            u0_blacklist_insert.Idcard = Idcard.Text.Trim();
            u0_blacklist_insert.Passpost = Passpost.Text;
            u0_blacklist_insert.PrefixIDXTH = int.Parse(ddl_prefix_th.SelectedValue);
            u0_blacklist_insert.FirstnameTH = txt_name_th.Text;
            u0_blacklist_insert.LastnameTH = txt_surname_th.Text;
            u0_blacklist_insert.PrefixIDXEN = int.Parse(ddl_prefix_en.SelectedValue);
            u0_blacklist_insert.FirstnameEN = FirstnameEN.Text;
            u0_blacklist_insert.LastnameEN = LastnameEN.Text;
            u0_blacklist_insert.NatIDX = int.Parse(ddlnation.SelectedValue);
            u0_blacklist_insert.RaceIDX = int.Parse(ddlrace.SelectedValue);
            u0_blacklist_insert.U0_note = txtnote.Text;
            u0_blacklist_insert.cemp_idx = _emp_idx;

            //data_blacklist data_log_blacklist = new data_blacklist();
            //blacklist_log_detail log_blacklist_insert = new blacklist_log_detail();
            //data_log_blacklist.blacklist_log_list = new blacklist_log_detail[1];

            GridView Gvblacklist = (GridView)FvInsert.FindControl("Gvblacklist");
            var count_alert_u1excel = Gvblacklist.Rows.Count;
            var u1_insert = new blacklist_u1_detail[Gvblacklist.Rows.Count];
            var u1_insert_log = new blacklist_log_detail[Gvblacklist.Rows.Count];
            int i = 0;
            int countchk = 0;


            foreach (GridViewRow row in Gvblacklist.Rows)
            {
                CheckBox chk_full = ((CheckBox)row.FindControl("chk_full"));
                if (chk_full != null && chk_full.Checked)
                {
                    Label idxU1 = ((Label)row.FindControl("idxU1"));
                    TextBox blacklist_note = ((TextBox)row.FindControl("blacklist_note"));
                    if (blacklist_note.Text != "" && blacklist_note.Text != null)
                    {
                        u1_insert[i] = new blacklist_u1_detail();
                        u1_insert[i].BlacklistIDX = int.Parse(idxU1.Text);
                        u1_insert[i].Note = blacklist_note.Text;
                        u1_insert[i].cemp_idx = _emp_idx;

                        u1_insert_log[i] = new blacklist_log_detail();

                        //u1_insert_log[i].u0_idx = int.Parse(idxU1.Text);
                        //u1_insert_log[i].u1_idx = int.Parse(idxU1.Text);
                        u1_insert_log[i].cemp_idx = _emp_idx;
                        //u1_insert_log[i].status = 1;
                        countchk += 1;
                        i++;
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ผลการบันทึก", "alert('" + "ผลการทำงาน : " + "กรุณากรอกข้อมูลช่องหมายเหตุ" + "')", true);
                        return;
                    }
                }
            }
            if (countchk > 0)
            {
                data_u0_blacklist.blacklist_u0_list[0] = u0_blacklist_insert;
                data_u0_blacklist.blacklist_u1_list = u1_insert;
                data_u0_blacklist.blacklist_log_list = u1_insert_log;
                //litdebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_u0_blacklist));
                //litdebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_blacklist));
                data_u0_blacklist = callServicePostblacklist(_urlSetblacklistU0, data_u0_blacklist);

                //if (data_u0_blacklist.return_code == 1)
                //{
                //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ข้อมูลซ้ำ", "alert('" + "เลขบัตรประชาชนซ้ำ : " + "กรุณาแก้ไขใหม่อีกครั้ง" + "')", true);
                //}
                //else
                //{

                if (UploadFileblacklist.HasFile)
                {
                    HttpFileCollection uploadedFiles_blacklist = Request.Files;
                    for (int ii = 0; ii < uploadedFiles_blacklist.Count; ii++)
                    {
                        HttpPostedFile userpost_createfile = uploadedFiles_blacklist[ii];
                        if (userpost_createfile.ContentLength > 0)
                        {
                            string getPath = ConfigurationSettings.AppSettings["path_flie_blacklist"];
                            if (Idcard.Text != "")
                            {
                                var txtidcard_upload = (TextBox)FvInsert.FindControl("Idcard");
                                string fileName1 = txtidcard_upload.Text + ii;
                                string fileName_upload = txtidcard_upload.Text;
                                string filePath1 = Server.MapPath(getPath + fileName_upload);
                                if (!Directory.Exists(filePath1))
                                {
                                    Directory.CreateDirectory(filePath1);
                                }
                                string extension = Path.GetExtension(userpost_createfile.FileName);

                                userpost_createfile.SaveAs(Server.MapPath(getPath + fileName_upload) + "\\" + fileName1 + extension);
                            }
                            else
                            {
                                var Passpost_upload = (TextBox)FvInsert.FindControl("Passpost");

                                string fileName1 = Passpost_upload.Text + ii;
                                string fileName_upload = Passpost_upload.Text;
                                string filePath1 = Server.MapPath(getPath + fileName_upload);
                                if (!Directory.Exists(filePath1))
                                {
                                    Directory.CreateDirectory(filePath1);
                                }
                                string extension = Path.GetExtension(userpost_createfile.FileName);

                                userpost_createfile.SaveAs(Server.MapPath(getPath + fileName_upload) + "\\" + fileName1 + extension);
                            }

                        }
                    }
                }
                else
                {
                }

                SelectblacklistDetail_U0();
                btnAdd.Visible = true;
                _PanelSearchBlacklist.Visible = true;
                FvInsert.Visible = false;
                Gvblacklistu0.Visible = true;
                _PanelSearchBlacklist.Visible = true;

                //}
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ผลการบันทึก", "alert('" + "ผลการทำงาน : " + "กรุณาเลือกข้อมูล Blacklist" + "')", true);
            }
        }
    }
    #endregion

    #region callService
    protected data_blacklist callServicePostblacklist(string _cmdUrl, data_blacklist _data_blacklist)
    {
        _localJson = _funcTool.convertObjectToJson(_data_blacklist);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _data_blacklist = (data_blacklist)_funcTool.convertJsonToObject(typeof(data_blacklist), _localJson);
        return _data_blacklist;
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }
    #endregion

    #region nationality
    protected void select_nationality(DropDownList ddlName)
    {
        data_employee _dtEmployee = new data_employee();
        _dataEmployee.R0Address_list = new Address_List[1];
        Address_List _addresslist = new Address_List();

        _dataEmployee.R0Address_list[0] = _addresslist;

        _dataEmployee = callServiceEmployee(_urlGetNationalityList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.R0Address_list, "NatName", "NatIDX");
        ddlName.Items.Insert(0, new ListItem("เลือกสัญชาติ....", "0"));
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        ddlName.Items.Clear();
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }
    #endregion

    #region Select
    protected void SelectblacklistDetail()
    {
        data_blacklist data_m0_blacklist_detail = new data_blacklist();
        blacklist_m0_detail m0_blacklist_detail = new blacklist_m0_detail();
        data_m0_blacklist_detail.blacklist_m0_list = new blacklist_m0_detail[1];

        m0_blacklist_detail.condition = 0;

        data_m0_blacklist_detail.blacklist_m0_list[0] = m0_blacklist_detail;

        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0_test_detail));
        data_m0_blacklist_detail = callServicePostblacklist(_urlGetblacklistM0, data_m0_blacklist_detail);
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_test_detail));
        GridView Gvblacklist = (GridView)FvInsert.FindControl("Gvblacklist");
        setGridData(Gvblacklist, data_m0_blacklist_detail.blacklist_m0_list);
    }

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvFileblacklist":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);
                    //string LinkHost111 = string.Format("http://" + Request.Url.Authority);
                    //string baseUrl = "http://" + Request.Url.Authority + Request.Url.Segments[0] + Request.Url.Segments[1];

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);

                }
                break;
            case "gvFileblacklist_edit":

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.Cells[0].FindControl("btnDL11");
                    HiddenField hidFile11 = (HiddenField)e.Row.Cells[0].FindControl("hidFile11");
                    // Display the company name in italics.
                    string LinkHost11 = string.Format("http://{0}", Request.Url.Host);
                    //string LinkHost111 = string.Format("http://" + Request.Url.Authority);

                    btnDL11.NavigateUrl = LinkHost11 + MapURL(hidFile11.Value);

                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                }
                break;

            case "Gvblacklistu0":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    btnAdd.Visible = true;
                    _PanelSearchBlacklist.Visible = true;
                    FvInsert.Visible = false;

                }
                break;

            case "Gvbacklistlog":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //GridView Gvbacklistlog = (GridView)FvDetailBlacklist.FindControl("Gvbacklistlog");
                    //Label lbl_status = (Label)e.Row.Cells[3].FindControl("lbl_status");
                    //Label test_statusOnline = (Label)e.Row.Cells[3].FindControl("test_statusOnline");
                    //Label test_statusOffline = (Label)e.Row.Cells[3].FindControl("test_statusOffline");

                    //ViewState["vs_status"] = lbl_status.Text;

                    //if (ViewState["vs_status"].ToString() == "1")
                    //{
                    //    test_statusOnline.Visible = true;
                    //}
                    //else if (ViewState["vs_status"].ToString() == "0")
                    //{
                    //    test_statusOffline.Visible = true;
                    //}

                }
                break;


        }
    }

    #region Directories_File URL

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt1.Rows.Add(file.Name);
                    dt1.Rows[i][1] = f[0];
                    i++;
                }
            }

            GridView gvFileblacklist = (GridView)FvDetailBlacklist.FindControl("gvFileblacklist");

            if (dt1.Rows.Count > 0)
            {

                ds1.Tables.Add(dt1);
                gvFileblacklist.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
                gvFileblacklist.DataBind();
                ds1.Dispose();
            }
            else
            {

                gvFileblacklist.DataSource = null;
                gvFileblacklist.DataBind();

            }
        }
        catch
        {
            checkfile = "11";
        }
    }

    public void SearchDirectories_edit(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        try
        {
            FileInfo[] files = dir.GetFiles();
            int i = 0;
            foreach (FileInfo file in files)
            {
                if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
                {
                    string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                    dt1.Rows.Add(file.Name);
                    dt1.Rows[i][1] = f[0];
                    i++;
                }
            }

            GridView gvFileblacklist_edit = (GridView)FvDetailBlacklist.FindControl("gvFileblacklist_edit");

            if (dt1.Rows.Count > 0)
            {
                ds1.Tables.Add(dt1);
                gvFileblacklist_edit.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
                gvFileblacklist_edit.DataBind();
                ds1.Dispose();
            }
            else
            {

                gvFileblacklist_edit.DataSource = null;
                gvFileblacklist_edit.DataBind();

            }
        }
        catch
        {
            checkfile = "11";
        }
    }
    #endregion


    protected void SelectblacklistDetail_U0()
    {
        data_blacklist data_m0_blacklist_detail_u0 = new data_blacklist();
        blacklist_u0_detail u0_blacklist_detail = new blacklist_u0_detail();
        data_m0_blacklist_detail_u0.blacklist_u0_list = new blacklist_u0_detail[1];

        u0_blacklist_detail.condition = 0;

        data_m0_blacklist_detail_u0.blacklist_u0_list[0] = u0_blacklist_detail;

        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0_test_detail));
        data_m0_blacklist_detail_u0 = callServicePostblacklist(_urlGetblacklistU0, data_m0_blacklist_detail_u0);
        //litdebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_blacklist_detail_u0));

        if (data_m0_blacklist_detail_u0.return_code == 0)
        {
            //litdebug.Text = "1";
            
           // ViewState["Vs_DetailBlacklist"] = data_m0_blacklist_detail_u0.blacklist_u0_list;

            ViewState["Vs_DetailBlacklist"] = data_m0_blacklist_detail_u0.blacklist_u0_list;
            setGridData(Gvblacklistu0, ViewState["Vs_DetailBlacklist"]);
        }
        else
        {
            //litdebug.Text = "2";
            ViewState["Vs_DetailBlacklist"] = null;
            setGridData(Gvblacklistu0, ViewState["Vs_DetailBlacklist"]);
        }

    }

  
    #endregion


}