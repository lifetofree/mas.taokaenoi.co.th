﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_overtime_report.aspx.cs" Inherits="websystem_hr_hr_overtime_report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setOntop" runat="server"></asp:HyperLink>

    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">

                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbReport" runat="server" CommandName="cmdReport" OnCommand="navCommand" CommandArgument="docReport"> รายงาน Overtime</asp:LinkButton>
                        </li>

                        <%-- <li id="li1" runat="server">
                            <asp:LinkButton ID="lbCreate" runat="server" CommandName="cmdCreate" OnCommand="navCommand" CommandArgument="docCreate"> Admin สร้างรายการ</asp:LinkButton>
                        </li>--%>
                    </ul>

                </div>
                <!--Collect the nav links, forms, and other content for toggling-->

            </div>
        </nav>
    </div>
    <!--tab menu-->

    <div class="col-md-12">
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
        <asp:Literal ID="litDebug1" runat="server"></asp:Literal>
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvSystem" runat="server">

        <!--View Report-->
        <asp:View ID="docReport" runat="server">

            <div class="col-md-12">

                <asp:UpdatePanel ID="_PanelReportOTMonth" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">ค้นหารายงานระบบ Overtime</h3>
                            </div>

                            <div class="panel-body">

                                <div class="col-md-10 col-md-offset-1">

                                    <div class="col-md-12">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ประเภทกะการทำงาน <span class="text-danger">*</span></label>
                                                <asp:DropDownList ID="ddlM0Shift" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Req_ddlM0Shift"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddlM0Shift" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกประเภทกะการทำงาน"
                                                    ValidationGroup="SearchReportOTmonth" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlM0Shift" Width="160" PopupPosition="BottomLeft" />

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-12" id="div_reportshift_fix" runat="server" visible="false">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>เดือน <span class="text-danger">*</span></label>
                                                <asp:DropDownList ID="ddlMonthReport" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">--- เลือกเดือน ---</asp:ListItem>
                                                    <asp:ListItem Value="01">มกราคม</asp:ListItem>
                                                    <asp:ListItem Value="02">กุมภาพันธ์</asp:ListItem>
                                                    <asp:ListItem Value="03">มีนาคม</asp:ListItem>
                                                    <asp:ListItem Value="04">เมษายน</asp:ListItem>
                                                    <asp:ListItem Value="05">พฤษภาคม</asp:ListItem>
                                                    <asp:ListItem Value="06">มิถุนายน</asp:ListItem>
                                                    <asp:ListItem Value="07">กรกฎาคม</asp:ListItem>
                                                    <asp:ListItem Value="08">สิงหาคม</asp:ListItem>
                                                    <asp:ListItem Value="09">กันยายน</asp:ListItem>
                                                    <asp:ListItem Value="10">ตุลาคม</asp:ListItem>
                                                    <asp:ListItem Value="11">พฤศจิกายน</asp:ListItem>
                                                    <asp:ListItem Value="12">ธันวาคม</asp:ListItem>
                                                </asp:DropDownList>

                                                <asp:RequiredFieldValidator ID="Req_ddlMonthReport"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddlMonthReport" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกเดือน"
                                                    ValidationGroup="SearchReportOTmonth" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5111" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlMonthReport" Width="160" PopupPosition="BottomLeft" />
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ปี</label>
                                                <asp:DropDownList ID="ddlYearReport" runat="server" CssClass="form-control">
                                                </asp:DropDownList>

                                                <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1"
                                                runat="server"
                                                ControlToValidate="txt_set_datestart" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกวันที่เริ่ม"
                                                ValidationGroup="HeadSaveSetOTMonth" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Re_txt_set_datestart" Width="160" PopupPosition="BottomLeft" />--%>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-12" id="div_reportshift_rotate" runat="server" visible="false">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ระบุช่วงวันที่ตัดรอบ <span class="text-danger">*</span></label>

                                                <div class="input-group date">
                                                    <asp:TextBox ID="txt_start_search" placeholder="วันที่เริ่มต้น" runat="server" CssClass="form-control datetimepicker-from cursor-pointer" />
                                                    <span class="input-group-addon show-from-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>

                                                    <asp:RequiredFieldValidator ID="Req_txt_start_search"
                                                        runat="server" ControlToValidate="txt_start_search" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาระบุช่วงวันที่ตัดรอบ"
                                                        ValidationGroup="SearchReportOTmonth" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_txt_start_search" Width="160" PopupPosition="BottomLeft" />

                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ถึงวันที่ <span class="text-danger">*</span></label>
                                                <div class="input-group date">
                                                    <asp:TextBox ID="txt_end_search" placeholder="วันที่สิ้นสุด" runat="server" CssClass="form-control datetimepicker-to cursor-pointer" />
                                                    <span class="input-group-addon show-to-onclick">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>

                                                    <asp:RequiredFieldValidator ID="Req_txt_end_search"
                                                        runat="server" ControlToValidate="txt_end_search" Display="None" SetFocusOnError="true"
                                                        ErrorMessage="*กรุณาระบุถึงวันที่ตัดรอบ"
                                                        ValidationGroup="SearchReportOTmonth" />
                                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server"
                                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_txt_end_search" Width="160" PopupPosition="BottomLeft" />
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-12">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>องค์กร <span class="text-danger">*</span></label>
                                                <asp:DropDownList ID="ddlorgReport" runat="server" Enabled="false" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Req_ddlorgReport"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddlorgReport" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกองค์กร"
                                                    ValidationGroup="SearchReportOTmonth" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlorgReport" Width="160" PopupPosition="BottomLeft" />
                                            </div>

                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ฝ่าย <span class="text-danger">*</span></label>
                                                <asp:DropDownList ID="ddlrdeptReport" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="Req_ddlrdeptReport"
                                                    runat="server"
                                                    InitialValue="0"
                                                    ControlToValidate="ddlrdeptReport" Display="None" SetFocusOnError="true"
                                                    ErrorMessage="*กรุณาเลือกฝ่าย"
                                                    ValidationGroup="SearchReportOTmonth" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender5" runat="Server"
                                                    HighlightCssClass="validatorCalloutHighlight" TargetControlID="Req_ddlrdeptReport" Width="160" PopupPosition="BottomLeft" />
                                            </div>

                                        </div>

                                    </div>

                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>แผนก</label>
                                                <asp:DropDownList ID="ddlrsecReport" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true">
                                                </asp:DropDownList>

                                            </div>

                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>ชื่อพนักงาน</label>
                                                <asp:DropDownList ID="ddlempReport" runat="server" CssClass="form-control">
                                                    <asp:ListItem Text="--- เลือกพนักงาน ---" Value="0"></asp:ListItem>
                                                </asp:DropDownList>

                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-md-12">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Coscenter</label>
                                                <asp:TextBox ID="txtCostcenterReport" MaxLength="10" placeholder="Coscenter" runat="server" CssClass="form-control"> </asp:TextBox>

                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>&nbsp;</label>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-12">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <asp:LinkButton ID="btnSearchReportMonthx0" CssClass="btn btn-default" ValidationGroup="SearchReportOTmonth" data-toggle="tooltip" title="ot x1" runat="server" CommandArgument="0" CommandName="cmdSearchReport"
                                                    OnCommand="btnCommand">ค่าล่วงเวลา 1 เท่า</asp:LinkButton>

                                                <asp:LinkButton ID="btnSearchReportMonthx1" CssClass="btn btn-default" ValidationGroup="SearchReportOTmonth" data-toggle="tooltip" title="ot x1.5" runat="server" CommandArgument="1" CommandName="cmdSearchReport"
                                                    OnCommand="btnCommand">ค่าล่วงเวลา 1.5 เท่า</asp:LinkButton>

                                                <asp:LinkButton ID="btnSearchReportMonthx2" CssClass="btn btn-default" ValidationGroup="SearchReportOTmonth" data-toggle="tooltip" title="ot x2" runat="server" CommandArgument="2" CommandName="cmdSearchReport"
                                                    OnCommand="btnCommand">ค่าล่วงเวลา 2 เท่า</asp:LinkButton>

                                                <asp:LinkButton ID="btnSearchReportMonthx3" CssClass="btn btn-default" ValidationGroup="SearchReportOTmonth" data-toggle="tooltip" title="ot x3" runat="server" CommandArgument="3" CommandName="cmdSearchReport"
                                                    OnCommand="btnCommand">ค่าล่วงเวลา 3 เท่า</asp:LinkButton>

                                                <asp:LinkButton ID="btnSearchTotalReport" CssClass="btn btn-default" ValidationGroup="SearchReportOTmonth" data-toggle="tooltip" title="สรุปค่าล่วงเวลา" runat="server" CommandArgument="4" CommandName="cmdSearchReport" OnCommand="btnCommand">สรุปค่าล่วงเวลา</asp:LinkButton>

                                                <asp:LinkButton ID="btnResetSearchReport" CssClass="btn btn-default" runat="server" data-original-title="ล้างค่า" data-toggle="tooltip" CommandName="cmdResetSearchReport" OnCommand="btnCommand"><span class="glyphicon glyphicon-refresh"></span> ล้างค่า</asp:LinkButton>

                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <!-- Export Excel Report Month -->
                <asp:UpdatePanel ID="_PanelExportExcelReportMonth" runat="server">

                    <ContentTemplate>
                        <div class="form-group">
                            <asp:LinkButton ID="btnExportExcelReportMonth" CssClass="btn btn-success" data-toggle="tooltip" title="Export Excel" runat="server"
                                CommandName="cmdExportExcelReportMonth" OnCommand="btnCommand"><span class="glyphicon glyphicon-export"></span> Export Excel</asp:LinkButton>
                        </div>
                    </ContentTemplate>

                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExportExcelReportMonth" />
                    </Triggers>

                </asp:UpdatePanel>
                <!-- Export Excel Report Month -->

                <!-- Export Excel Total Report Month -->
                <asp:UpdatePanel ID="_PanelExportExcelTotalReportMonth" runat="server">

                    <ContentTemplate>
                        <div class="form-group">
                            <asp:LinkButton ID="btnExportExcelTotalReportMonth" CssClass="btn btn-success" data-toggle="tooltip" title="Export Excel" runat="server"
                                CommandName="cmdExportExcelTotalReportMonth" OnCommand="btnCommand"><span class="glyphicon glyphicon-export"></span> Export Excel</asp:LinkButton>
                        </div>
                    </ContentTemplate>

                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnExportExcelTotalReportMonth" />
                    </Triggers>

                </asp:UpdatePanel>
                <!-- Export Excel Total Report Month -->

                <div id="GvReportOvertime_scroll" style="overflow-x: auto; width: 100%" runat="server">
                    <asp:GridView ID="GvReportOvertime" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        ShowHeaderWhenEmpty="False"
                        AllowPaging="False"
                        PageSize="5"
                        BorderStyle="None"
                        CellSpacing="2">
                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <small>
                                        <%# (Container.DataItemIndex +1) %>
                                       
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="รหัสพนักงาน" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="left">
                                <ItemTemplate>
                                    <p>
                                        <%-- <b>ชื่อ-สกุลผู้สร้าง:</b>--%>
                                        <%--<asp:Label ID="lbl_u0_doc_idx_reportmonth" Visible="false" runat="server" Text='<%# Eval("u0_doc_idx") %>' />--%>
                                        <asp:Label ID="lbl_emp_code_reportmonth" runat="server" Text='<%# Eval("emp_code") %>' />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ประเภทเงินได้" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="left">
                                <ItemTemplate>
                                    <p>
                                        <asp:Label ID="lbl_condition_otmonth_reportmonth" runat="server" Text='<%# Eval("condition_otmonth") %>' />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน ot ก่อน(ชั่วโมง)" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <p>
                                        <%--<b>โรงงาน:</b>--%>
                                        <asp:Label ID="lbl_ot_before_reportmonth" Visible="true" runat="server" Text='<%# Eval("ot_before") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน ot หลัง(ชั่วโมง)" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <p>
                                        <%--<b>โรงงาน:</b>--%>
                                        <asp:Label ID="lbl_ot_after_reportmonth" Visible="true" runat="server" Text='<%# Eval("ot_after") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน ot วันหยุด(ชั่วโมง)" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <p>
                                        <%--<b>โรงงาน:</b>--%>
                                        <asp:Label ID="lbl_ot_holiday_reportmonth" Visible="true" runat="server" Text='<%# Eval("ot_holiday") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="จำนวน ot รวม (ชั่วโมง)" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>

                                    <asp:Label ID="lbl_ot_total_reportmonth" Visible="true" runat="server" Text='<%# Eval("ot_total")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="หมายเหตุ" HeaderStyle-CssClass="text-center" ItemStyle-Width="30%" ItemStyle-CssClass="text-left">
                                <ItemTemplate>

                                    <asp:Label ID="lbl_remark_otmonth_reportmonth" Visible="true" runat="server" Text='<%# Eval("remark_otmonth")%>' />

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>
                </div>

                <div id="GvReportTotalOvertime_scroll" style="overflow-x: auto; width: 100%" runat="server">
                    <asp:GridView ID="GvReportTotalOvertime" runat="server"
                        AutoGenerateColumns="False"
                        CssClass="table table-striped table-bordered table-hover table-responsive"
                        OnPageIndexChanging="gvPageIndexChanging"
                        OnRowDataBound="gvRowDataBound"
                        ShowHeaderWhenEmpty="False"
                        AllowPaging="False"
                        PageSize="5"
                        BorderStyle="None"
                        CellSpacing="2">
                        <HeaderStyle CssClass="info" Height="40px" Font-Size="Small" />
                        <RowStyle Font-Size="Small" />
                        <PagerStyle CssClass="pageCustom" />
                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />
                        <EmptyDataTemplate>
                            <div style="text-align: center">--- ไม่พบข้อมูล ---</div>
                        </EmptyDataTemplate>
                        <Columns>

                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <small>
                                        <%# (Container.DataItemIndex +1) %>
                                       
                                    </small>
                                </ItemTemplate>
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="รหัสพนักงาน" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="left">
                                <ItemTemplate>
                                    <p>
                                        <%-- <b>ชื่อ-สกุลผู้สร้าง:</b>--%>
                                        <%--<asp:Label ID="lbl_u0_doc_idx_reportmonth" Visible="false" runat="server" Text='<%# Eval("u0_doc_idx") %>' />--%>
                                        <asp:Label ID="lbl_emp_code_reportmonth" runat="server" Text='<%# Eval("emp_code") %>' />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="ชื่อพนักงาน" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="left">
                                <ItemTemplate>
                                    <p>
                                        <asp:Label ID="lbl_emp_name_th_reportmonth" runat="server" Text='<%# Eval("emp_name_th") %>' />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="แผนก" HeaderStyle-CssClass="text-center" ItemStyle-Width="15%" ItemStyle-CssClass="left">
                                <ItemTemplate>
                                    <p>
                                        <asp:Label ID="lbl_dept_name_th_reportmonth" runat="server" Text='<%# Eval("dept_name_th") %>' />
                                    </p>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="OTx1.5" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <p>

                                        <asp:Label ID="lbl_ot_x15_reportmonth" Visible="true" runat="server" Text='<%# Eval("ot_x15") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="OTx1" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <p>

                                        <asp:Label ID="lbl_ot_x1_reportmonth" Visible="true" runat="server" Text='<%# Eval("ot_x1") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="OTx2" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <p>

                                        <asp:Label ID="lbl_ot_x2_reportmonth" Visible="true" runat="server" Text='<%# Eval("ot_x2") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="OTx3" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <p>

                                        <asp:Label ID="lbl_ot_x3_reportmonth" Visible="true" runat="server" Text='<%# Eval("ot_x3") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รวมชั่วโมงOT" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <p>

                                        <asp:Label ID="lbl_total_hours_reportmonth" Visible="true" runat="server" Text='<%# Eval("total_hours") %>' />
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="รายละเอียด" HeaderStyle-CssClass="text-center" ItemStyle-Width="10%" ItemStyle-CssClass="text-right">
                                <ItemTemplate>
                                    <asp:Label ID="lbl_u0_doc1_idx_reportmonth" Visible="false" runat="server" Text='<%# Eval("u0_doc1_idx_value") %>' />
                                    <asp:LinkButton ID="btnViewTotalReportOvt" CssClass="btn btn-sm btn-info" target="" runat="server" CommandName="cmdViewTotalReportOvt" OnCommand="btnCommand"
                                        CommandArgument='<%# Eval("u0_doc1_idx_value")%>' data-toggle="tooltip" title="ดูข้อมูล"><i class="far fa-file-alt"> ดูข้อมูล</i></asp:LinkButton>

                                </ItemTemplate>
                            </asp:TemplateField>

                        </Columns>
                    </asp:GridView>

                </div>

                <!-- Div Print Report -->
                <div id="div_PrintReport" runat="server">

                    <!-- Back To Search Report -->
                    <asp:UpdatePanel ID="Update_BackToSearchReport" runat="server">
                        <ContentTemplate>
                            <div class="form-group">
                                <asp:LinkButton ID="btnBackToSearchReport" CssClass="btn btn-danger" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                                    CommandName="cmdBackToSearchReport" CommandArgument="1" OnCommand="btnCommand"><i class="fa fa-reply" aria-hidden="true"></i> ย้อนกลับ</asp:LinkButton>

                                <asp:LinkButton ID="btnPrintReport" CssClass="btn btn-primary" runat="server" Text="<i class='fa fa-print'></i> พิมพ์" OnClientClick="return printDiv('printableAreaReport');" />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <!-- Back To Search Report -->

                    <div id="printableAreaReport">

                        <div class="panel panel-default">

                            <table class="table f-s-12 m-t-10" style="font-family: 'Angsana New'; border: 0px">

                                <tr>
                                    <td style="text-align: left; border: 0px;">
                                        <h3 style="text-align: left;">รายละเอียดค่าล่วงเวลา</h3>
                                    </td>
                                </tr>

                                <tr>

                                    <td style="text-align: center; border: 0px">
                                        <%--<h3 style="text-align: center;">บริษัท เถ้าแก่น้อย ฟู๊ดแอนด์มาร์เก็ตติ้ง จำกัด(มหาชน)</h3>--%>
                                        <h3 style="text-align: center;"><asp:Label ID="lbl_org_name_th_viewreport" class="control-labelnotop" runat="server"> </asp:Label></h3>
                                        <h3 style="text-align: center;">รายงานการอนุมัติการทำงานล่วงเวลา</h3>
                                        <%--<asp:Label ID="lbl_datetime_otstart_viewreport" runat="server" Text='<%# Eval("datetime_otstart") %>' />--%>
                                    </td>

                                </tr>

                            </table>

                            <table class="table f-s-12 m-t-10" style="font-family: 'Angsana New'; border: 0px">
                                <tr>
                                    <td style="text-align: left; border: 0px; vertical-align: middle; width: 480px;">
                                        <h3>กะการทำงาน  
                                        <asp:Label ID="lbl_parttime_name_th_viewreport" class="control-labelnotop" runat="server"> </asp:Label>

                                        </h3>

                                    </td>

                                    <td style="text-align: left; border: 0px; vertical-align: middle; width: 480px;">
                                        <div id="div_shiftfix" runat="server">
                                            <h3>ประจำเดือน   
                                                <asp:Label ID="lbl_shift_fixprint_month" class="control-labelnotop" runat="server"> </asp:Label>
                                                ปี
                                                <asp:Label ID="lbl_shift_fixprint_year" class="control-labelnotop" runat="server"> </asp:Label>

                                            </h3>
                                        </div>

                                        <div id="div_shiftrotate" runat="server">
                                            <h3>วันที่    
                                                <asp:Label ID="lbl_shiftstart_rotateprint" class="control-labelnotop" runat="server"> </asp:Label>
                                                -
                                                <asp:Label ID="lbl_shiftend_rotateprint" class="control-labelnotop" runat="server"> </asp:Label>
                                            </h3>
                                        </div>

                                    </td>

                                </tr>
                            </table>

                            <table class="table f-s-12 m-t-10" style="font-family: 'Angsana New'; border: 0px">
                                <tr>
                                    <td style="text-align: left; border: 0px;">
                                        <h3>รหัส   
                                        <asp:Label ID="lbl_emp_code_viewreport" class="control-labelnotop" runat="server"> </asp:Label>
                                        </h3>
                                    </td>

                                    <td style="text-align: left; border: 0px;">
                                        <h3>ชื่อ-สกุล   
                                     <asp:Label ID="lbl_emp_name_th_viewreport" class="control-labelnotop" runat="server"> </asp:Label>
                                        </h3>
                                    </td>

                                    <td style="text-align: left; border: 0px;">
                                        <h3>ตำแหน่ง   
                                     <asp:Label ID="lbl_pos_name_th_viewreport" class="control-labelnotop" runat="server"> </asp:Label>
                                        </h3>
                                    </td>

                                    <td style="text-align: left; border: 0px;">
                                        <h3>ตำแหน่ง   
                                     <asp:Label ID="lbl_dept_name_th_viewreport" class="control-labelnotop" runat="server"> </asp:Label>
                                        </h3>
                                    </td>

                                </tr>
                            </table>

                            <table class="table f-s-12 m-t-10" style="font-family: 'Angsana New'; border: 0px">
                                <tr>
                                    <td style="text-align: left; border: 0px">
                                        <h4>
                                            <asp:GridView ID="gvPrintReport"
                                                runat="server"
                                                AutoGenerateColumns="false"
                                                CssClass="table table-striped table-bordered table-responsive"
                                                HeaderStyle-CssClass="info"
                                                AllowPaging="true"
                                                PageSize="31"
                                                ShowFooter="true"
                                                OnPageIndexChanging="gvPageIndexChanging"
                                                OnRowDataBound="gvRowDataBound">
                                                <PagerStyle CssClass="pageCustom" />
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                                                <EmptyDataTemplate>
                                                    <div class="text-center">-- ไม่พบข้อมูล --</div>
                                                </EmptyDataTemplate>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="วันที่" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_datetime_otstart_viewreport" runat="server" Text='<%# Eval("datetime_otstart") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="งานที่ปฏิบัติ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_detail_job_viewreport" runat="server" Text='<%# Eval("detail_job") %>' />


                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="เวลาเริ่มทำ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                        <ItemTemplate>

                                                            <asp:Label ID="lbl_datetime_start_viewreport" runat="server" Text='<%# Eval("datetime_start") %>' />


                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="เวลาเลิกทำ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_datetime_end_viewreport" runat="server" Text='<%# Eval("datetime_end") %>' />
                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate>
                                                            <div style="text-align: right;">
                                                                <asp:Literal ID="lit_total_hour" runat="server" Text="เวลารวม :"></asp:Literal>
                                                            </div>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="OTx1.5" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_ot_x15_viewreport" runat="server" Text='<%# Eval("ot_x15") %>' />

                                                        </ItemTemplate>
                                                        <EditItemTemplate />

                                                        <FooterTemplate>
                                                            <div style="text-align: right; background-color: chartreuse;">
                                                                <asp:Label ID="lit_total_hoursot_x15" runat="server" Font-Bold="true"></asp:Label>

                                                            </div>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="OTx1" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_ot_x1_viewreport" runat="server" Text='<%# Eval("ot_x1") %>' />

                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate>
                                                            <div style="text-align: right; background-color: chartreuse;">
                                                                <asp:Label ID="lit_total_hoursot_x1" runat="server" Font-Bold="true"></asp:Label>

                                                            </div>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="OTx2" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_ot_x2_viewreport" runat="server" Text='<%# Eval("ot_x2") %>' />

                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate>
                                                            <div style="text-align: right; background-color: chartreuse;">
                                                                <asp:Label ID="lit_total_hoursot_x2" runat="server" Font-Bold="true"></asp:Label>

                                                            </div>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="OTx3" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_ot_x3_viewreport" runat="server" Text='<%# Eval("ot_x3") %>' />

                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate>
                                                            <div style="text-align: right; background-color: chartreuse;">
                                                                <asp:Label ID="lit_total_hoursot_x3" runat="server" Font-Bold="true"></asp:Label>

                                                            </div>
                                                        </FooterTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbl_remark_admin_viewreport" runat="server" Text='<%# Eval("remark_admin") %>' />
                                                            <asp:Label ID="lbl_comment_viewreport" runat="server" Text='<%# Eval("comment") %>' />

                                                        </ItemTemplate>
                                                        <EditItemTemplate />
                                                        <FooterTemplate />
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </h4>

                                    </td>

                                </tr>
                            </table>

                            <table class="table f-s-12 m-t-10" style="font-family: 'Angsana New'; border: 0px">
                                <tr>

                                    <td style="text-align: left; border: 0px">
                                        <h3 style="text-align: left;">สรุปค่าล่วงเวลารวมทั้งสิ้น :
                                            <asp:Label ID="lbl_total_hourovertime" class="control-labelnotop" runat="server"> </asp:Label>
                                            ชั่วโมง</h3>
                                    </td>

                                </tr>

                            </table>

                            <br />
                            <br />

                            <table class="table f-s-12 m-t-10" style="font-family: 'Angsana New'; border: 0px">
                                <tr>
                                    <td style="text-align: left; border: 0px; width: 320px;">
                                        <h3 style="text-align: center;">
                                            <asp:Label ID="lbl_emp_name_th_print" class="control-labelnotop" runat="server"> </asp:Label>
                                            ผู้จัดทำ
                                        </h3>
                                        <h3 style="text-align: center;">พนักงาน</h3>
                                    </td>

                                    <td style="text-align: left; border: 0px; width: 320px;">
                                        <h3 style="text-align: center;">
                                            <asp:Label ID="lbl_emp_approve1_print" class="control-labelnotop" runat="server"> </asp:Label>
                                            ผู้ตรวจสอบ
                                        </h3>
                                        <h3 style="text-align: center;">หัวหน้าฝ่าย / ผู้ที่ได้รับมอบหมาย</h3>
                                    </td>

                                    <td style="text-align: left; border: 0px; width: 320px;">
                                        <h3 style="text-align: center;">
                                            <asp:Label ID="lbl_emp_approve2_print" class="control-labelnotop" runat="server"> </asp:Label>
                                            ผู้อนุมัติ
                                        </h3>
                                        <h3 style="text-align: center;">ผู้จัดการฝ่าย / ผู้ที่ได้รับมอบหมาย</h3>
                                    </td>

                                </tr>
                            </table>

                            <table class="table f-s-12 m-t-10" style="font-family: 'Angsana New'; border: 0px">
                                <tr>

                                    <td style="text-align: right; border: 0px">
                                        <h3 style="text-align: right;">FM-HR-CB-006/05 Rev.00(01/11/2016)</h3>
                                    </td>

                                </tr>

                            </table>

                        </div>

                    </div>
                </div>
                <!-- Div Print Report -->

            </div>
        </asp:View>
        <!--View Report-->

    </asp:MultiView>
    <!--multiview-->

    <script type="text/javascript">

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            //var dateToday = new Date();
            //currentTime = new Date();
            $('.datetimepicker-from').datetimepicker({

                format: 'DD/MM/YYYY',
                //minDate: moment().add(-1, 'day'),
                //maxDate: moment().add(60, 'day'),
                //ignoreReadonly: true

            });
            $('.datetimepicker-from').on('dp.change', function (e) {
                var dateTo = $('.datetimepicker-to').data("DateTimePicker").date();
                if (e.date > dateTo) {
                    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }
                //else {
                //    $('.datetimepicker-to').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                //}

            });
            $('.show-from-onclick').click(function () {
                $('.datetimepicker-from').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').datetimepicker({
                format: 'DD/MM/YYYY',
                //minDate: moment().add(-1, 'day'),
                //maxDate: moment().add(60, 'day'),
                //ignoreReadonly: true
            });
            $('.show-to-onclick').click(function () {
                $('.datetimepicker-to').data("DateTimePicker").show();
            });
            $('.datetimepicker-to').on('dp.change', function (e) {
                var dateFrom = $('.datetimepicker-from').data("DateTimePicker").date();
                if (e.date < dateFrom) {
                    $('.datetimepicker-from').data("DateTimePicker").date(e.date.format("DD/MM/YYYY"));
                }

            });


        });

    </script>

    <script type="text/javascript">
        function printDiv(divName) {
            var panel = document.getElementById(divName);


            //var gvname_ = document.getElementById(gvname);
            var printWindow = window.open('', '', 'height=1000,width=800');
            printWindow.document.write('<html><head><title></title></head>');
            printWindow.document.write('<body style="font-size:12px;">');
            printWindow.document.write('<style>table{width:100%;color:#000000;font-size:10px;};</style>');
            printWindow.document.write(panel.innerHTML);
            printWindow.document.write('</body></html>');
            //document.getElementById(gvname).Enable = true;
            printWindow.document.close();
            setTimeout(function () {
                printWindow.print();
            }, 500);
            //document.getElementById('gvWork_scroll').Visible = true;
            //return false;
        }
    </script>


</asp:Content>

