﻿<%@ Page Title="Security Board" Language="C#" MasterPageFile="~/masterpage/master_blank.master" AutoEventWireup="true" CodeFile="security_board.aspx.cs" Inherits="websystem_visitors_security_board" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
    <asp:Literal ID="Litdebug" runat="server" Text=""></asp:Literal>

    <%--<form id="wrapper" runat="server">--%>
    <div class="body_vm_board">
        <div class="wrapper_vm_board">
            <div id="wrapper1" runat="server">
                <div class="col-md-12" style="border-color: #B4E1E4; background: #136915; color: #fff;"
                    runat="server" id="_header">
                    <div class="row">

                        <div class="container text-center">
                            <asp:Image ID="imglogo" ImageUrl="~/images/logotkn.png"
                                runat="server" Width="150" Height="150" />
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <asp:Label ID="lbname" CssClass="h3" Style="font-weight: bold" Text="รายการโหลดสินค้า" runat="server" />
                            </div>
                            <div class="col-md-5">
                            </div>
                            <div class="col-md-3">
                                <b>
                                    <asp:Label ID="demo_time" CssClass="h3" Style="font-weight: bold" runat="server"></asp:Label>
                                </b>

                                <script>

                                    function myTimer() {
                                        var d = new Date();
                                        document.getElementById("demo_time").innerHTML = "Date : " + d.format("dd/MM/yyyy");
                                    }
                                </script>

                            </div>
                        </div>
                    </div>

                </div>


                <asp:Timer ID="Timer1" runat="server" OnTick="Timer1_Tick" Enabled="true" Interval="40000"></asp:Timer>
                
                <%--<div class="body_vm_board-content">--%>
                <div class="body_vm_board-content col-md-12">
                    <div class="col-md-12">
                        <div class="row">

                            <asp:GridView ID="GvMaster"
                                runat="server" Visible="true"
                                AutoGenerateColumns="false"
                                DataKeyNames="u0_idx"
                                CssClass="table table-hover table-striped table-responsive word-wrap col-md-12 m-t-10"
                                HeaderStyle-CssClass="bg-success"
                                AllowPaging="true"
                                GridLines="None"
                                OnRowDataBound="gvRowDataBound"
                                OnPageIndexChanging="Master_PageIndexChanging"
                                HeaderStyle-BackColor="#BEBEBE"
                                PageSize="3"
                                Width="100%" Font-Names="Verdana,Helvetica,sans-serif">
                                <%-- <RowStyle BackColor="#f5f5f5" />--%>
                                <HeaderStyle BackColor="#BEBEBE" Font-Bold="True" ForeColor="White" CssClass="text-left" />
                                <RowStyle Height="100" />
                                <AlternatingRowStyle BackColor="#f5f5f5" />
                                <Columns>

                                    <asp:TemplateField HeaderText="ทะเบียนรถ" ItemStyle-HorizontalAlign="Center"
                                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="14">
                                        <ItemTemplate>
                                            <asp:Label ID="lb_status_flag" runat="server" Text='<%# Eval("status_flag") %>' Visible="false" />
                                              
                                                <h4 class="title">
                                                    <asp:Label ID="lbvehicle_plate_no" runat="server" Text='<%# Eval("vehicle_plate_no").ToString() != "" ? Eval("vehicle_plate_no") : "-" %>' Font-Size="24" />

                                                </h4>
                                           
                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />

                                        <HeaderStyle CssClass="text-center" Font-Size="14"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>


                                    <asp:TemplateField HeaderText="ตั้งแต่" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="14">
                                        <ItemTemplate>
                                            <small>
                                                <h4 class="title">
                                                    <asp:Label ID="lbtime_start" runat="server" Text='<%# Eval("zvisit_time") %>' Font-Size="24" />
                                                </h4>
                                            </small>
                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />

                                        <HeaderStyle CssClass="text-center" Font-Size="14"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Left"
                                        HeaderStyle-CssClass="text-left" HeaderStyle-Font-Size="14">
                                        <ItemTemplate>
                                            <small>
                                                <h4 class="title">
                                                    <asp:Label ID="lb_zstatus" runat="server" Text='<%# Eval("zstatus") %>' Font-Size="24" />
                                                </h4>
                                            </small>
                                        </ItemTemplate>
                                        <EditItemTemplate />
                                        <FooterTemplate />

                                        <HeaderStyle CssClass="text-center" Font-Size="14"></HeaderStyle>

                                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                                    </asp:TemplateField>

                                </Columns>

                                <EmptyDataTemplate>
                                    <div style="text-align: center">ไม่พบข้อมูล</div>
                                </EmptyDataTemplate>

                                <HeaderStyle CssClass="bg-success" Font-Bold="True" BackColor="#BEBEBE" Font-Names="Verdana,Helvetica,sans-serif"></HeaderStyle>

                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="10" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" Visible="false" />

                                <PagerStyle CssClass="pageCustom" />

                            </asp:GridView>

                        </div>

                    </div>
                </div>

                <%--</div>--%>

                <div class="col-md-12">
                    <div class="row">

                        <div class="footer_vm_board">
                            <marquee scrollamount="5"><p><h3>
                                <asp:Label ID="lbmarquee" Text="รายการรถที่รอโหลดสินค้า" runat="server" CssClass="h3">
                                 </asp:Label>
                                </h3> </p>
                            </marquee>
                        </div>

                    </div>
                </div>

                <asp:HiddenField ID="hddf_item" runat="server" />
            </div>
        </div>
    </div>
    <%-- </form>--%>

    <style type="text/css">
        .body_vm_board {
            margin: 0;
            padding: 0;
            height: 100%;
        }

        .wrapper_vm_board {
            min-height: 100%;
            position: relative;
        }

        .body_vm_board-content {
            padding-left: 0px; /* 15px */
            padding-right: 0px; /* 15px */
            padding-top: 15px; /* Needed for fixed headers/menu */
            padding-bottom: 60px; /* Height of the footer */
        }

        .footer_vm_board {
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 60px; /* Height of the footer */
            border-color: #B4E1E4;
            background: #136915;
            color: #fff;
        }
    </style>
    <style>
        .full-height {
            height: 100%;
            background: yellow;
        }
    </style>
</asp:Content>

