﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_ITServices_it_asset : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    service_mail servicemail = new service_mail();
    function_tool _funcTool = new function_tool();
    data_employee _dtEmployee = new data_employee();
    data_itasset _dtitseet = new data_itasset();

    string _localJson = String.Empty;

    #region URL

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlSelectSystem = _serviceUrl + ConfigurationManager.AppSettings["urlSelectSystem"];
    static string _urlInsertClass = _serviceUrl + ConfigurationManager.AppSettings["urlnsert_Master_Class"];
    static string _urlSelectClass = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_Class"];
    static string _urlDeleteClass = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_Master_Class"];
    static string _urlInsertAssetBuy = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Master_AssetBuy"];
    static string _urlSelectAssetBuy = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_AssetBuy"];



    #endregion

    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            select_empIdx_present();
            _divMenuLiToDivIndex.Attributes.Add("class", "active");
            SetViewState_BuyNew();
            SetViewState_BuyReplace();

        }
        linkBtnTrigger(_divMenuBtnToDivDataAsset);
        linkBtnTrigger(CmdAdd_Import);

    }
    #endregion

    #region CallService


    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }

    protected data_itasset callServicePostITAsset(string _cmdUrl, data_itasset _dtitseet)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtitseet);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtitseet = (data_itasset)_funcTool.convertJsonToObject(typeof(data_itasset), _localJson);




        return _dtitseet;
    }


    #endregion

    #region Select
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());


        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

    }

    protected void select_system(DropDownList ddlName)
    {

        _dtitseet = new data_itasset();

        _dtitseet.boxdata_u0document_itasset = new u0_document_itasset[1];
        u0_document_itasset search = new u0_document_itasset();

        _dtitseet.boxdata_u0document_itasset[0] = search;

        _dtitseet = callServicePostITAsset(_urlSelectSystem, _dtitseet);
        setDdlData(ddlName, _dtitseet.boxdata_u0document_itasset, "SysNameEN", "SysIDX");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกประเภททรัพย์สิน...", "0"));
    }

    protected void SelectMasterList_Class()
    {
        _dtitseet = new data_itasset();
        _dtitseet.boxmaster_class = new class_itasset[1];
        class_itasset dtasset = new class_itasset();

        _dtitseet.boxmaster_class[0] = dtasset;
        //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        //string _localJson1 = _funcTool.convertObjectToJson(_dtitseet);
        //txt.Text = _localJson1;
        _dtitseet = callServicePostITAsset(_urlSelectClass, _dtitseet);
        setGridData(GvMaster, _dtitseet.boxmaster_class);
    }

    protected void SelectMasterList_AssetBuy()
    {
        _dtitseet = new data_itasset();
        _dtitseet.boxmaster_asset = new asset_itasset[1];
        asset_itasset dtasset = new asset_itasset();

        _dtitseet.boxmaster_asset[0] = dtasset;
        //   string _localJson1 = _funcTool.convertObjectToJson(_dtitseet);
        //  txt.Text = _localJson1;
        _dtitseet = callServicePostITAsset(_urlSelectAssetBuy, _dtitseet);
      //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));

        setGridData(GvAsset_Buy, _dtitseet.boxmaster_asset);
    }
    #endregion

    #region Insert
    protected void Insert_Master()
    {
        string text = String.Empty;
        text = txtname.Text.Replace('&', ' ');

        _dtitseet.boxmaster_class = new class_itasset[1];
        class_itasset insert = new class_itasset();

        insert.code_class = txtcode.Text;
        insert.code_desc = text;
        insert.code_status = int.Parse(ddStatusadd.SelectedValue);
        insert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtitseet.boxmaster_class[0] = insert;

        //string _localJson1 = _funcTool.convertObjectToJson(_dtitseet);
        //txt.Text = _localJson1;// HttpUtility.HtmlEncode(_funcTool.(_localJson1));
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
        _dtitseet = callServicePostITAsset(_urlInsertClass, _dtitseet);

        if (_dtitseet.ReturnCode == "102")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้ในระบบแล้ว');", true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลสำเร็จ');", true);
        }

    }

    protected void ImportFileBuyDevices(string filePath, string Extension, string isHDR, string fileName)
    {
        string conStr = String.Empty;
        conStr = ConfigurationManager.ConnectionStrings["Excel07ConString"].ConnectionString;
        conStr = String.Format(conStr, filePath, "Yes");
        OleDbConnection connExcel = new OleDbConnection(conStr);
        OleDbCommand cmdExcel = new OleDbCommand();
        OleDbDataAdapter oda = new OleDbDataAdapter();
        DataTable dt = new DataTable();

        cmdExcel.Connection = connExcel;
        connExcel.Open();
        DataTable dtExcelSchema;
        dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        string SheetName = dtExcelSchema.Rows[0]["TABLE_NAME"].ToString();
        connExcel.Close();
        connExcel.Open();
        cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
        oda.SelectCommand = cmdExcel;
        oda.Fill(dt);
        connExcel.Close();


        string idxCreated = String.Empty;
        int j = 1;
        for (var i = 0; i < dt.Rows.Count - 1; i++)
        {
            if (dt.Rows[j][5].ToString().Trim() != "Asset description")
            {
                asset_itasset import = new asset_itasset();
                _dtitseet.boxmaster_asset = new asset_itasset[1];

                import.Asset = dt.Rows[j][0].ToString().Trim();
                import.SNo = dt.Rows[j][1].ToString().Trim();
                import.CostCenter = dt.Rows[j][2].ToString().Trim();
                import.LocIDX = int.Parse(dt.Rows[j][3].ToString()); 
                import.CapDate = dt.Rows[j][4].ToString().Trim();
                import.Asset_Description = dt.Rows[j][5].ToString().Trim();
                import.AcquisVal = dt.Rows[j][6].ToString();
                import.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

                _dtitseet.boxmaster_asset[0] = import;
                //string _localJson1 = _funcTool.convertObjectToJson(_dtitseet);
                //txt.Text = _localJson1;// HttpUtility.HtmlEncode(_funcTool.(_localJson1));
                //txt.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtitseet));
                _dtitseet = callServicePostITAsset(_urlInsertAssetBuy, _dtitseet);


            }
            j++;
        }








    }
    #endregion

    #region Update
    protected void Update_Master_List()
    {
        _dtitseet.boxmaster_class = new class_itasset[1];
        class_itasset update = new class_itasset();

        update.code_class = ViewState["txtname_edit"].ToString();
        update.code_desc = ViewState["txtcode_edit"].ToString();
        update.code_status = int.Parse(ViewState["StatusUpdate_Update"].ToString());
        update.clidx = int.Parse(ViewState["clidx"].ToString());
        update.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtitseet.boxmaster_class[0] = update;

        _dtitseet = callServicePostITAsset(_urlInsertClass, _dtitseet);



    }

    protected void Delete_Master_List()
    {
        _dtitseet.boxmaster_class = new class_itasset[1];
        class_itasset delete = new class_itasset();

        delete.clidx = int.Parse(ViewState["clidx"].ToString());
        delete.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        _dtitseet.boxmaster_class[0] = delete;

        _dtitseet = callServicePostITAsset(_urlDeleteClass, _dtitseet);
    }

    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void SetDefaultAdd()
    {
        txtname.Text = String.Empty;
        txtcode.Text = String.Empty;
        ddStatusadd.SelectedValue = "1";
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }


    #region SetDefault BuyNew
    protected void SetViewState_BuyNew()
    {

        DataSet dsFoodMaterialList = new DataSet();
        dsFoodMaterialList.Tables.Add("dsAddListTable");
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("list_buynew", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("qty_buynew", typeof(int));
        dsFoodMaterialList.Tables["dsAddListTable"].Columns.Add("price_buynew", typeof(int));
        ViewState["vsBuyequipment"] = dsFoodMaterialList;

    }
    protected void setAddList_BuyNew()
    {
        if (ViewState["vsBuyequipment"] != null)
        {
            TextBox txtlist_buynew = (TextBox)FvInsertBuyNew.FindControl("txtlist_buynew");
            TextBox txtqty_buynew = (TextBox)FvInsertBuyNew.FindControl("txtqty_buynew");
            TextBox txtprice_buynew = (TextBox)FvInsertBuyNew.FindControl("txtprice_buynew");
            GridView GvReportAdd = (GridView)FvInsertBuyNew.FindControl("GvReportAdd");


            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable"].Rows)
            {
                if (dr["list_buynew"].ToString() == txtlist_buynew.Text &&
                       dr["qty_buynew"].ToString() == txtqty_buynew.Text &&
                       dr["price_buynew"].ToString() == txtprice_buynew.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsAddListTable"].NewRow();
            drContacts["list_buynew"] = txtlist_buynew.Text;
            drContacts["qty_buynew"] = int.Parse(txtqty_buynew.Text);
            drContacts["price_buynew"] = int.Parse(txtprice_buynew.Text);

            dsContacts.Tables["dsAddListTable"].Rows.Add(drContacts);
            ViewState["vsBuyequipment"] = dsContacts;
            GvReportAdd.Visible = true;
            setGridData(GvReportAdd, dsContacts.Tables["dsAddListTable"]);

        }
    }
    protected void CleardataSetBuyNewList()
    {
        var GvReportAdd = (GridView)FvInsertBuyNew.FindControl("GvReportAdd");
        ViewState["vsBuyequipment"] = null;
        GvReportAdd.DataSource = ViewState["vsBuyequipment"];
        GvReportAdd.DataBind();
        SetViewState_BuyNew();
    }

    #endregion

    #region SetDefault BuyReplace
    protected void SetViewState_BuyReplace()
    {

        DataSet dsFoodMaterialList = new DataSet();
        dsFoodMaterialList.Tables.Add("dsAddListTable_BuyReplace");
        dsFoodMaterialList.Tables["dsAddListTable_BuyReplace"].Columns.Add("list_buyreplace", typeof(String));
        dsFoodMaterialList.Tables["dsAddListTable_BuyReplace"].Columns.Add("qty_buyreplace", typeof(int));
        dsFoodMaterialList.Tables["dsAddListTable_BuyReplace"].Columns.Add("price_buyreplace", typeof(int));
        ViewState["vsBuyequipment_BuyReplace"] = dsFoodMaterialList;

    }
    protected void setAddList_BuyReplace()
    {
        if (ViewState["vsBuyequipment_BuyReplace"] != null)
        {
            TextBox txtlist_buyreplace = (TextBox)FvInsertBuyReplace.FindControl("txtlist_buyreplace");
            TextBox txtqty_buyreplace = (TextBox)FvInsertBuyReplace.FindControl("txtqty_buyreplace");
            TextBox txtprice_buyreplace = (TextBox)FvInsertBuyReplace.FindControl("txtprice_buyreplace");
            GridView GvReportAdd_BuyReplace = (GridView)FvInsertBuyReplace.FindControl("GvReportAdd_BuyReplace");


            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsBuyequipment_BuyReplace"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable_BuyReplace"].Rows)
            {
                if (dr["list_buyreplace"].ToString() == txtlist_buyreplace.Text &&
                       dr["qty_buyreplace"].ToString() == txtqty_buyreplace.Text &&
                       dr["price_buyreplace"].ToString() == txtprice_buyreplace.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsAddListTable_BuyReplace"].NewRow();
            drContacts["list_buyreplace"] = txtlist_buyreplace.Text;
            drContacts["qty_buyreplace"] = int.Parse(txtqty_buyreplace.Text);
            drContacts["price_buyreplace"] = int.Parse(txtprice_buyreplace.Text);

            dsContacts.Tables["dsAddListTable_BuyReplace"].Rows.Add(drContacts);
            ViewState["vsBuyequipment_BuyReplace"] = dsContacts;
            GvReportAdd_BuyReplace.Visible = true;
            setGridData(GvReportAdd_BuyReplace, dsContacts.Tables["dsAddListTable_BuyReplace"]);

        }
    }
    protected void CleardataSetBuyReplaceList()
    {
        var GvReportAdd_BuyReplace = (GridView)FvInsertBuyReplace.FindControl("GvReportAdd_BuyReplace");
        ViewState["vsBuyequipment_BuyReplace"] = null;
        GvReportAdd_BuyReplace.DataSource = ViewState["vsBuyequipment_BuyReplace"];
        GvReportAdd_BuyReplace.DataBind();
        SetViewState_BuyReplace();
    }

    #endregion

    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }



                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList_Class();

                break;

            case "GvAsset_Buy":
                GvAsset_Buy.PageIndex = e.NewPageIndex;
                GvAsset_Buy.DataBind();
                SelectMasterList_AssetBuy();
                break;



        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList_Class();
                btnshow.Visible = false;
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList_Class();
                btnshow.Visible = true;
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int clidx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtname_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtname_edit");
                var txtcode_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtcode_edit");
                var StatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster.EditIndex = -1;

                ViewState["clidx"] = clidx;
                ViewState["txtname_edit"] = txtname_edit.Text;
                ViewState["txtcode_edit"] = txtcode_edit.Text;
                ViewState["StatusUpdate_Update"] = StatusUpdate.SelectedValue;

                Update_Master_List();
                SelectMasterList_Class();

                break;
        }
    }

    #endregion

    #region FormView

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvDetailUser":
                FormView FvDetailUser = (FormView)ViewBuy.FindControl("FvDetailUser");

                if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();
                }
                break;
            case "FvInsertBuyNew":
                FormView FvInsertBuyNew = (FormView)ViewBuy.FindControl("FvInsertBuyNew");
                DropDownList ddlsystem = (DropDownList)FvInsertBuyNew.FindControl("ddlsystem");
                if (FvInsertBuyNew.CurrentMode == FormViewMode.Insert)
                {
                    select_system(ddlsystem);
                }
                break;

            case "FvInsertBuyReplace":
                FormView FvInsertBuyReplace = (FormView)ViewBuy.FindControl("FvInsertBuyReplace");
                DropDownList ddlsystem_replace = (DropDownList)FvInsertBuyReplace.FindControl("ddlsystem_replace");
                if (FvInsertBuyReplace.CurrentMode == FormViewMode.Insert)
                {
                    select_system(ddlsystem_replace);
                }
                break;

            case "FvInsertCutDevices":
                FormView FvInsertCutDevices = (FormView)ViewBuy.FindControl("FvInsertCutDevices");
                DropDownList ddlsystem_cutdevices = (DropDownList)FvInsertCutDevices.FindControl("ddlsystem_cutdevices");
                if (FvInsertCutDevices.CurrentMode == FormViewMode.Insert)
                {
                    select_system(ddlsystem_cutdevices);
                }
                break;
            case "FvDetailUser_Approve":
                if (FvDetailUser_Approve.CurrentMode == FormViewMode.Insert)
                {

                }
                break;

            case "FvDetailUser_Budget":
                if (FvDetailUser_Budget.CurrentMode == FormViewMode.Insert)
                {

                }
                break;

            case "FvDetail_ShowBudget":
                if (FvDetail_ShowBudget.CurrentMode == FormViewMode.Insert)
                {

                }
                break;

            case "FvDetailUser_Asset":
                if (FvDetailUser_Asset.CurrentMode == FormViewMode.Insert)
                {

                }
                break;

            case "FvDetail_ShowAsset":
                if (FvDetail_ShowAsset.CurrentMode == FormViewMode.Insert)
                {

                }
                break;

                //case "FvDetailUser_CutDevieces":
                //    FormView FvDetailUser_CutDevieces = (FormView)ViewCutDevices.FindControl("FvDetailUser_CutDevieces");

                //    if (FvDetailUser_CutDevieces.CurrentMode == FormViewMode.Insert)
                //    {
                //        var txtempcode = ((TextBox)FvDetailUser_CutDevieces.FindControl("txtempcode"));
                //        var txtrequesname = ((TextBox)FvDetailUser_CutDevieces.FindControl("txtrequesname"));
                //        var txtrequesdept = ((TextBox)FvDetailUser_CutDevieces.FindControl("txtrequesdept"));
                //        var txtsec = ((TextBox)FvDetailUser_CutDevieces.FindControl("txtsec"));
                //        var txtpos = ((TextBox)FvDetailUser_CutDevieces.FindControl("txtpos"));
                //        var txtemail = ((TextBox)FvDetailUser_CutDevieces.FindControl("txtemail"));
                //        var txttel = ((TextBox)FvDetailUser_CutDevieces.FindControl("txttel"));
                //        var txtorg = ((TextBox)FvDetailUser_CutDevieces.FindControl("txtorg"));

                //        txtempcode.Text = ViewState["EmpCode"].ToString();
                //        txtrequesname.Text = ViewState["FullName"].ToString();
                //        txtorg.Text = ViewState["Org_name"].ToString();
                //        txtrequesdept.Text = ViewState["rdept_name"].ToString();
                //        txtsec.Text = ViewState["Secname"].ToString();
                //        txtpos.Text = ViewState["Positname"].ToString();
                //        txttel.Text = ViewState["Tel"].ToString();
                //        txtemail.Text = ViewState["Email"].ToString();
                //    }
                //    break;

        }
    }


    #endregion

    #region onRowCommand

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "bnDeleteList":
                    GridView GvReportAdd = (GridView)FvInsertBuyNew.FindControl("GvReportAdd");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsBuyequipment"];
                    dsContacts.Tables["dsAddListTable"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(GvReportAdd, dsContacts.Tables["dsAddListTable"]);
                    if (dsContacts.Tables["dsAddListTable"].Rows.Count < 1)
                    {
                        GvReportAdd.Visible = false;
                    }
                    break;

            }
        }
    }

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;

        switch (ddName.ID)
        {
            case "ddlsystem_cutdevices":
                DropDownList ddlsystem_cutdevices = (DropDownList)FvInsertCutDevices.FindControl("ddlsystem_cutdevices");
                Control div_doccode = (Control)FvInsertCutDevices.FindControl("div_doccode");
                TextBox txtreason_cutdevices = (TextBox)FvInsertCutDevices.FindControl("txtreason_cutdevices");
                TextBox txtasset_cutdevices = (TextBox)FvInsertCutDevices.FindControl("txtasset_cutdevices");
                if (ddlsystem_cutdevices.SelectedValue != "9")
                {
                    div_doccode.Visible = true;
                    txtreason_cutdevices.Enabled = false;
                    txtasset_cutdevices.Enabled = false;

                }
                else
                {
                    div_doccode.Visible = false;
                    txtreason_cutdevices.Enabled = true;
                    txtasset_cutdevices.Enabled = true;
                }

                break;
        }
    }
    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "_divMenuBtnToDivIndex":
                _divMenuLiToDivIndex.Attributes.Add("class", "active");
                _divMenuLiToDivBuy.Attributes.Remove("class");
                _divMenuLiToDivBuyNew.Attributes.Remove("class");
                _divMenuLiToDivBuyReplace.Attributes.Remove("class");
                _divMenuLiToDivDevices.Attributes.Remove("class");
                _divMenuLiToDivChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_Buy.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_CutDevices.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivBudget.Attributes.Remove("class");
                _divMenuLiToDivAsset.Attributes.Remove("class");
                _divMenuLiToDivAsset_system.Attributes.Remove("class");
                _divMenuLiToDivAsset_master.Attributes.Remove("class");
                _divMenuLiToDivDataAsset.Attributes.Remove("class");
                _divMenuLiToDivDataBuyAsset.Attributes.Remove("class");
                _divMenuLiToDivDataGiveAsset.Attributes.Remove("class");
                MvMaster.SetActiveView(ViewIndex);

                break;
            case "_divMenuBtnToDivBuyNew":
                _divMenuLiToDivIndex.Attributes.Remove("class");
                _divMenuLiToDivBuy.Attributes.Add("class", "active");
                _divMenuLiToDivBuyNew.Attributes.Add("class", "active");
                _divMenuLiToDivBuyReplace.Attributes.Remove("class");
                _divMenuLiToDivDevices.Attributes.Remove("class");
                _divMenuLiToDivChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_Buy.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_CutDevices.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivBudget.Attributes.Remove("class");
                _divMenuLiToDivAsset.Attributes.Remove("class");
                _divMenuLiToDivAsset_system.Attributes.Remove("class");
                _divMenuLiToDivAsset_master.Attributes.Remove("class");
                _divMenuLiToDivDataAsset.Attributes.Remove("class");
                _divMenuLiToDivDataBuyAsset.Attributes.Remove("class");
                _divMenuLiToDivDataGiveAsset.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewBuy);
                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();
                div_createbuynew.Visible = true;
                div_createbuyreplace.Visible = false;
                div_createcutdevices.Visible = false;
                FvInsertBuyNew.ChangeMode(FormViewMode.Insert);
                FvInsertBuyNew.DataBind();
                CleardataSetBuyNewList();

                break;
            case "_divMenuBtnToDivBuyReplace":
                _divMenuLiToDivIndex.Attributes.Remove("class");
                _divMenuLiToDivBuy.Attributes.Add("class", "active");
                _divMenuLiToDivBuyReplace.Attributes.Add("class", "active");
                _divMenuLiToDivBuyNew.Attributes.Remove("class");
                _divMenuLiToDivDevices.Attributes.Remove("class");
                _divMenuLiToDivChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_Buy.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_CutDevices.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivBudget.Attributes.Remove("class");
                _divMenuLiToDivAsset.Attributes.Remove("class");
                _divMenuLiToDivAsset_system.Attributes.Remove("class");
                _divMenuLiToDivAsset_master.Attributes.Remove("class");
                _divMenuLiToDivDataAsset.Attributes.Remove("class");
                _divMenuLiToDivDataBuyAsset.Attributes.Remove("class");
                _divMenuLiToDivDataGiveAsset.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewBuy);
                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();
                div_createbuynew.Visible = false;
                div_createbuyreplace.Visible = true;
                div_createcutdevices.Visible = false;
                FvInsertBuyReplace.ChangeMode(FormViewMode.Insert);
                FvInsertBuyReplace.DataBind();
                CleardataSetBuyReplaceList();



                break;
            case "_divMenuBtnToDivCutDevices":
                _divMenuLiToDivIndex.Attributes.Remove("class");
                _divMenuLiToDivBuy.Attributes.Remove("class");
                _divMenuLiToDivBuyNew.Attributes.Remove("class");
                _divMenuLiToDivBuyReplace.Attributes.Remove("class");
                _divMenuLiToDivDevices.Attributes.Add("class", "active");
                _divMenuLiToDivChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_Buy.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_CutDevices.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivBudget.Attributes.Remove("class");
                _divMenuLiToDivAsset.Attributes.Remove("class");
                _divMenuLiToDivAsset_system.Attributes.Remove("class");
                _divMenuLiToDivAsset_master.Attributes.Remove("class");
                _divMenuLiToDivDataAsset.Attributes.Remove("class");
                _divMenuLiToDivDataBuyAsset.Attributes.Remove("class");
                _divMenuLiToDivDataGiveAsset.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewBuy);
                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();
                div_createbuynew.Visible = false;
                div_createbuyreplace.Visible = false;
                div_createcutdevices.Visible = true;

                FvInsertCutDevices.ChangeMode(FormViewMode.Insert);
                FvInsertCutDevices.DataBind();


                break;
            case "_divMenuBtnToDivChangeOwn":
                _divMenuLiToDivIndex.Attributes.Remove("class");
                _divMenuLiToDivBuy.Attributes.Remove("class");
                _divMenuLiToDivBuyNew.Attributes.Remove("class");
                _divMenuLiToDivBuyReplace.Attributes.Remove("class");
                _divMenuLiToDivDevices.Attributes.Remove("class");
                _divMenuLiToDivChangeOwn.Attributes.Add("class", "active");
                _divMenuLiToDivWaitApprove.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_Buy.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_CutDevices.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivBudget.Attributes.Remove("class");
                _divMenuLiToDivAsset.Attributes.Remove("class");
                _divMenuLiToDivAsset_system.Attributes.Remove("class");
                _divMenuLiToDivAsset_master.Attributes.Remove("class");
                _divMenuLiToDivDataAsset.Attributes.Remove("class");
                _divMenuLiToDivDataBuyAsset.Attributes.Remove("class");
                _divMenuLiToDivDataGiveAsset.Attributes.Remove("class");


                break;
            case "_divMenuBtnToDivWaitApprove_Buy":
                _divMenuLiToDivIndex.Attributes.Remove("class");
                _divMenuLiToDivBuy.Attributes.Remove("class");
                _divMenuLiToDivBuyNew.Attributes.Remove("class");
                _divMenuLiToDivBuyReplace.Attributes.Remove("class");
                _divMenuLiToDivDevices.Attributes.Remove("class");
                _divMenuLiToDivChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove.Attributes.Add("class", "active");
                _divMenuLiToDivWaitApprove_Buy.Attributes.Add("class", "active");
                _divMenuLiToDivWaitApprove_CutDevices.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivBudget.Attributes.Remove("class");
                _divMenuLiToDivAsset.Attributes.Remove("class");
                _divMenuLiToDivAsset_system.Attributes.Remove("class");
                _divMenuLiToDivAsset_master.Attributes.Remove("class");
                _divMenuLiToDivDataAsset.Attributes.Remove("class");
                _divMenuLiToDivDataBuyAsset.Attributes.Remove("class");
                _divMenuLiToDivDataGiveAsset.Attributes.Remove("class");

                ViewState["cmdArg_typesys"] = "Approve_Buy";

                MvMaster.SetActiveView(ViewApprove);
                gvapprove.Visible = true;
                detailapprove.Visible = false;

                break;

            case "_divMenuBtnToDivWaitApprove_CutDevices":
                _divMenuLiToDivIndex.Attributes.Remove("class");
                _divMenuLiToDivBuy.Attributes.Remove("class");
                _divMenuLiToDivBuyNew.Attributes.Remove("class");
                _divMenuLiToDivBuyReplace.Attributes.Remove("class");
                _divMenuLiToDivDevices.Attributes.Remove("class");
                _divMenuLiToDivChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove.Attributes.Add("class", "active");
                _divMenuLiToDivWaitApprove_Buy.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_CutDevices.Attributes.Add("class", "active");
                _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivBudget.Attributes.Remove("class");
                _divMenuLiToDivAsset.Attributes.Remove("class");
                _divMenuLiToDivAsset_system.Attributes.Remove("class");
                _divMenuLiToDivAsset_master.Attributes.Remove("class");
                _divMenuLiToDivDataAsset.Attributes.Remove("class");
                _divMenuLiToDivDataBuyAsset.Attributes.Remove("class");
                _divMenuLiToDivDataGiveAsset.Attributes.Remove("class");

                ViewState["cmdArg_typesys"] = "Approve_CutDevices";

                MvMaster.SetActiveView(ViewApprove);
                gvapprove.Visible = true;
                detailapprove.Visible = false;

                break;

            case "_divMenuBtnToDivWaitApprove_ChangeOwn":
                _divMenuLiToDivIndex.Attributes.Remove("class");
                _divMenuLiToDivBuy.Attributes.Remove("class");
                _divMenuLiToDivBuyNew.Attributes.Remove("class");
                _divMenuLiToDivBuyReplace.Attributes.Remove("class");
                _divMenuLiToDivDevices.Attributes.Remove("class");
                _divMenuLiToDivChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove.Attributes.Add("class", "active");
                _divMenuLiToDivWaitApprove_Buy.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_CutDevices.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Add("class", "active");
                _divMenuLiToDivBudget.Attributes.Remove("class");
                _divMenuLiToDivAsset.Attributes.Remove("class");
                _divMenuLiToDivAsset_system.Attributes.Remove("class");
                _divMenuLiToDivAsset_master.Attributes.Remove("class");
                _divMenuLiToDivDataAsset.Attributes.Remove("class");
                _divMenuLiToDivDataBuyAsset.Attributes.Remove("class");
                _divMenuLiToDivDataGiveAsset.Attributes.Remove("class");

                ViewState["cmdArg_typesys"] = "Approve_ChangeOwn";

                MvMaster.SetActiveView(ViewApprove);
                gvapprove.Visible = true;
                detailapprove.Visible = false;

                break;

            case "_divMenuBtnToDivBudget":
                _divMenuLiToDivIndex.Attributes.Remove("class");
                _divMenuLiToDivBuy.Attributes.Remove("class");
                _divMenuLiToDivBuyNew.Attributes.Remove("class");
                _divMenuLiToDivBuyReplace.Attributes.Remove("class");
                _divMenuLiToDivDevices.Attributes.Remove("class");
                _divMenuLiToDivChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_Buy.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_CutDevices.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivBudget.Attributes.Add("class", "active");
                _divMenuLiToDivAsset.Attributes.Remove("class");
                _divMenuLiToDivAsset_system.Attributes.Remove("class");
                _divMenuLiToDivAsset_master.Attributes.Remove("class");
                _divMenuLiToDivDataAsset.Attributes.Remove("class");
                _divMenuLiToDivDataBuyAsset.Attributes.Remove("class");
                _divMenuLiToDivDataGiveAsset.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewBudget);
                gvbudget.Visible = true;
                detailbudget.Visible = false;
                // GvListBudget.DataBind();

                break;

            case "_divMenuBtnToDivAsset_system":
                _divMenuLiToDivIndex.Attributes.Remove("class");
                _divMenuLiToDivBuy.Attributes.Remove("class");
                _divMenuLiToDivBuyNew.Attributes.Remove("class");
                _divMenuLiToDivBuyReplace.Attributes.Remove("class");
                _divMenuLiToDivDevices.Attributes.Remove("class");
                _divMenuLiToDivChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_Buy.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_CutDevices.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivBudget.Attributes.Remove("class");
                _divMenuLiToDivAsset.Attributes.Add("class", "active");
                _divMenuLiToDivAsset_system.Attributes.Add("class", "active");
                _divMenuLiToDivAsset_master.Attributes.Remove("class");
                _divMenuLiToDivDataAsset.Attributes.Remove("class");
                _divMenuLiToDivDataBuyAsset.Attributes.Remove("class");
                _divMenuLiToDivDataGiveAsset.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewAsset);
                gvasset.Visible = true;
                detailasset.Visible = false;
                break;

            case "_divMenuBtnToDivAsset_master":
                _divMenuLiToDivIndex.Attributes.Remove("class");
                _divMenuLiToDivBuy.Attributes.Remove("class");
                _divMenuLiToDivBuyNew.Attributes.Remove("class");
                _divMenuLiToDivBuyReplace.Attributes.Remove("class");
                _divMenuLiToDivDevices.Attributes.Remove("class");
                _divMenuLiToDivChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_Buy.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_CutDevices.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivBudget.Attributes.Remove("class");
                _divMenuLiToDivAsset.Attributes.Add("class", "active");
                _divMenuLiToDivAsset_system.Attributes.Remove("class");
                _divMenuLiToDivAsset_master.Attributes.Add("class", "active");
                _divMenuLiToDivDataAsset.Attributes.Remove("class");
                _divMenuLiToDivDataBuyAsset.Attributes.Remove("class");
                _divMenuLiToDivDataGiveAsset.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewAsset_Master);
                SelectMasterList_Class();
                break;

            case "_divMenuBtnToDivDataAsset":
                _divMenuLiToDivIndex.Attributes.Remove("class");
                _divMenuLiToDivBuy.Attributes.Remove("class");
                _divMenuLiToDivBuyNew.Attributes.Remove("class");
                _divMenuLiToDivBuyReplace.Attributes.Remove("class");
                _divMenuLiToDivDevices.Attributes.Remove("class");
                _divMenuLiToDivChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_Buy.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_CutDevices.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivBudget.Attributes.Remove("class");
                _divMenuLiToDivAsset.Attributes.Remove("class");
                _divMenuLiToDivAsset_system.Attributes.Remove("class");
                _divMenuLiToDivAsset_master.Attributes.Remove("class");
                _divMenuLiToDivDataAsset.Attributes.Add("class", "active");
                _divMenuLiToDivDataBuyAsset.Attributes.Add("class", "active");
                _divMenuLiToDivDataGiveAsset.Attributes.Remove("class");

                MvMaster.SetActiveView(ViewDataAsset);
                SelectMasterList_AssetBuy();
                break;

            case "_divMenuBtnToDivDataGiveAsset":
                _divMenuLiToDivIndex.Attributes.Remove("class");
                _divMenuLiToDivBuy.Attributes.Remove("class");
                _divMenuLiToDivBuyNew.Attributes.Remove("class");
                _divMenuLiToDivBuyReplace.Attributes.Remove("class");
                _divMenuLiToDivDevices.Attributes.Remove("class");
                _divMenuLiToDivChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_Buy.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_CutDevices.Attributes.Remove("class");
                _divMenuLiToDivWaitApprove_ChangeOwn.Attributes.Remove("class");
                _divMenuLiToDivBudget.Attributes.Remove("class");
                _divMenuLiToDivAsset.Attributes.Remove("class");
                _divMenuLiToDivAsset_system.Attributes.Remove("class");
                _divMenuLiToDivAsset_master.Attributes.Remove("class");
                _divMenuLiToDivDataAsset.Attributes.Add("class", "active");
                _divMenuLiToDivDataBuyAsset.Attributes.Remove("class");
                _divMenuLiToDivDataGiveAsset.Attributes.Add("class", "active");
                break;


            case "CmdAddBuy_Newlist":
                Control div_btn_buynew = (Control)FvInsertBuyNew.FindControl("div_btn_buynew");
                div_btn_buynew.Visible = true;
                setAddList_BuyNew();


                break;

            case "CmdAddBuy_Replacelist":
                Control div_btn_buyreplace = (Control)FvInsertBuyReplace.FindControl("div_btn_buyreplace");
                div_btn_buyreplace.Visible = true;
                setAddList_BuyReplace();
                break;

            case "BtnBack":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btndetailbudget":
                gvbudget.Visible = false;
                detailbudget.Visible = true;

                FvDetailUser_Budget.ChangeMode(FormViewMode.Insert);
                FvDetailUser_Budget.DataBind();

                FvDetail_ShowBudget.ChangeMode(FormViewMode.Insert);
                FvDetail_ShowBudget.DataBind();


                break;

            case "BtnBack_viewbudget":
                MvMaster.SetActiveView(ViewBudget);
                gvbudget.Visible = true;
                detailbudget.Visible = false;

                break;

            case "btndetailasset":
                gvasset.Visible = false;
                detailasset.Visible = true;

                FvDetailUser_Asset.ChangeMode(FormViewMode.Insert);
                FvDetailUser_Asset.DataBind();

                FvDetail_ShowAsset.ChangeMode(FormViewMode.Insert);
                FvDetail_ShowAsset.DataBind();

                break;

            case "BtnBack_viewasset":
                MvMaster.SetActiveView(ViewAsset);
                gvasset.Visible = true;
                detailasset.Visible = false;

                break;

            case "CmdAdd":
                btnshow.Visible = false;
                Panel_Add.Visible = true;
                SetDefaultAdd();
                break;

            case "btnCancel":
                btnshow.Visible = true;
                Panel_Add.Visible = false;
                break;

            case "CmdAddBuy_Cutlist":

                break;

            case "btndetailapprove":

                gvapprove.Visible = false;
                detailapprove.Visible = true;
                FvDetailUser_Approve.ChangeMode(FormViewMode.Insert);
                FvDetailUser_Approve.DataBind();

                FvDetail_ShowApprove.ChangeMode(FormViewMode.Insert);
                FvDetail_ShowApprove.DataBind();

                Control div_showreasoncut = (Control)FvDetail_ShowApprove.FindControl("div_showreasoncut");

                if (ViewState["cmdArg_typesys"].ToString() == "Approve_CutDevices")
                {
                    div_showreasoncut.Visible = true;
                }
                else
                {
                    div_showreasoncut.Visible = false;
                }

                break;

            case "BtnBack_viewapprove":
                MvMaster.SetActiveView(ViewApprove);
                gvapprove.Visible = true;
                detailapprove.Visible = false;
                break;

            case "btnAddMaster":
                Insert_Master();
                Panel_Add.Visible = false;
                btnshow.Visible = true;
                SelectMasterList_Class();
                break;

            case "CmdDel":
                int clidx = int.Parse(cmdArg);
                ViewState["clidx"] = clidx;
                Delete_Master_List();
                SelectMasterList_Class();
                break;

            case "CmdAdd_Import":
                Panel_ImportAdd.Visible = true;
                CmdAdd_Import.Visible = false;
                CmdAdd_Search.Visible = false;
                break;

            case "btnCancel_Import":
                Panel_ImportAdd.Visible = false;
                CmdAdd_Import.Visible = true;
                CmdAdd_Search.Visible = true;
                break;

            case "btnImport":
                if (upload.HasFile)
                {
                    string datetimeNow = DateTime.Now.ToString("yyyyMMdd_HHmmss_ffff");
                    string FileName = Path.GetFileName(upload.PostedFile.FileName);
                    string extension = Path.GetExtension(upload.PostedFile.FileName);
                    string newFileName = datetimeNow + extension.ToLower();
                    string folderPath = ConfigurationManager.AppSettings["path_asset_import"];
                    string filePath = Server.MapPath(folderPath + newFileName);
                    if (extension.ToLower() == ".xls" || extension.ToLower() == ".xlsx")
                    {
                        upload.SaveAs(filePath);
                        ImportFileBuyDevices(filePath, extension, "Yes", FileName);
                        File.Delete(filePath);
                        //  Page.Response.Redirect(Page.Request.Url.ToString(), true);
                    }
                    else
                    {
                        _funcTool.showAlert(this, "Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls หรือ .xlsx) เท่านั้น");
                    }
                }
                break;

            case "CmdAdd_Search":
                CmdAdd_Import.Visible = false;
                CmdAdd_Search.Visible = false;
                Panel_SearchImport.Visible = true;

                break;
        }
    }


    #endregion
}