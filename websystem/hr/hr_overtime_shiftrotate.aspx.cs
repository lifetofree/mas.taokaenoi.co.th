﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_hr_hr_overtime_shiftrotate : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();
    service_execute serviceexcute = new service_execute();
    data_employee _dataEmployee = new data_employee();
    data_overtime _data_overtime = new data_overtime();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string _urlGetPositionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetPositionList"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];
    static string _urlSelect_Location = _serviceUrl + ConfigurationManager.AppSettings["urlGetPlace"];

    static string _urlGetEmployee = _serviceUrl + ConfigurationManager.AppSettings["urlGetEmployee"];

    //-- employee --//

    //-- over time --//
    static string _urlGetDateOTMonth = _serviceUrl + ConfigurationManager.AppSettings["urlGetDateOTMonth"];
    static string _urlGetTimeScanOTMont = _serviceUrl + ConfigurationManager.AppSettings["urlGetTimeScanOTMont"];
    static string _urlGetDetailOT = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailOT"];
    static string _urlGetDetailOTView = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailOTView"];
    static string _urlGetDetailOTViewLog = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailOTViewLog"];
    static string _urlGetOTMontView = _serviceUrl + ConfigurationManager.AppSettings["urlGetOTMontView"];
    static string _urlGetDetailWaitApproveOT = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailWaitApproveOT"];
    static string _urlGetCountWaitApproveOT = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountWaitApproveOT"];
    static string _urlGetDecisionWaitApprove = _serviceUrl + ConfigurationManager.AppSettings["urlGetDecisionWaitApprove"];
    static string _urlGetDateScanOTMont = _serviceUrl + ConfigurationManager.AppSettings["urlGetDateScanOTMont"];
    static string _urlGetTimeStartScanOTMont = _serviceUrl + ConfigurationManager.AppSettings["urlGetTimeStartScanOTMont"];

    static string _urlSetCreateOTMonth = _serviceUrl + ConfigurationManager.AppSettings["urlSetCreateOTMonth"];
    static string _urlSetHeadUserApproveOTMonth = _serviceUrl + ConfigurationManager.AppSettings["urlSetHeadUserApproveOTMonth"];
    static string _urlSetUserEditOTMonth = _serviceUrl + ConfigurationManager.AppSettings["urlSetUserEditOTMonth"];
    static string _urlGetHeadUserSetOTMonth = _serviceUrl + ConfigurationManager.AppSettings["urlGetHeadUserSetOTMonth"];
    static string _urlGetEditViewScanOTMont = _serviceUrl + ConfigurationManager.AppSettings["urlGetEditViewScanOTMont"];
    static string _urlSetGroupOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlSetGroupOTDay"];
    static string _urlGetDetailGroupOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailGroupOTDay"];
    static string _urlGetViewDetailGroupOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewDetailGroupOTDay"];
    static string _urlSetDelGroupOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlSetDelGroupOTDay"];
    static string _urlGetTypeOTGroupOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetTypeOTGroupOTDay"];
    static string _urlSetCreateGroupOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlSetCreateGroupOTDay"];
    static string _urlSetOTMontHeadUser = _serviceUrl + ConfigurationManager.AppSettings["urlSetOTMontHeadUser"];
    static string _urlGetTypeDayWorkOT = _serviceUrl + ConfigurationManager.AppSettings["urlGetTypeDayWorkOT"];
    static string _urlGetTypeOTImport = _serviceUrl + ConfigurationManager.AppSettings["urlGetTypeOTImport"];
    static string _urlSetImportEmployeeOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlSetImportEmployeeOTDay"];
    static string _urlGetDetailEmployeeImportOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailEmployeeImportOTDay"];
    static string _urlGetViewDetailEmployeeImportOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewDetailEmployeeImportOTDay"];
    static string _urlGetLogViewDetailEmployeeImportOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetLogViewDetailEmployeeImportOTDay"];
    static string _urlGetSearchReportDetailEmpOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchReportDetailEmpOTDay"];
    static string _urlGetReportOTMonth = _serviceUrl + ConfigurationManager.AppSettings["urlGetReportOTMonth"];
    static string _urlGetSearchOTMonthIndex = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchOTMonthIndex"];

    static string _urlGetDetailOTDayAdmin = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailOTDayAdmin"];
    static string _urlGetTimeStartOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetTimeStartOTDay"];
    static string _urlGetPermissionAdminCreate = _serviceUrl + ConfigurationManager.AppSettings["urlGetPermissionAdminCreate"];
    static string _urlGetDetailAdminCreateOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailAdminCreateOTDay"];
    static string _urlGetViewDetailAdminCreateOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewDetailAdminCreateOTDay"];
    static string _urlGetViewDetailGvAdminOT = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewDetailGvAdminOT"];
    static string _urlGetDetailToEmployeeOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailToEmployeeOTDay"];
    static string _urlGetViewDetailToEmployeeOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewDetailToEmployeeOTDay"];
    static string _urlGetLogDetailEmployeeOTDay = _serviceUrl + ConfigurationManager.AppSettings["urlGetLogDetailEmployeeOTDay"];
    static string _urlGetWaitApproveHeadOTDayAdminCreate = _serviceUrl + ConfigurationManager.AppSettings["urlGetWaitApproveHeadOTDayAdminCreate"];
    static string _urlSetApproveOTAdminCreate = _serviceUrl + ConfigurationManager.AppSettings["urlSetApproveOTAdminCreate"];
    static string _urlGetCountWaitApproveAdminCreate = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountWaitApproveAdminCreate"];
    static string _urlSetCreateOTByAdmin = _serviceUrl + ConfigurationManager.AppSettings["urlSetCreateOTByAdmin"];
    static string _urlGetDocStatusOTMonth = _serviceUrl + ConfigurationManager.AppSettings["urlGetDocStatusOTMonth"];
    static string _urlGetSearchWaitApproveOTMonth = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchWaitApproveOTMonth"];

    //-- over time shift rotate --//
    static string _urlGetDetailOTCreateShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailOTCreateShiftRotate"];
    static string _urlSetCreateOTShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlSetCreateOTShiftRotate"];
    static string _urlGetDetailCreateToAdminShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailCreateToAdminShiftRotate"];
    static string _urlGetDetailCreateToUserShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailCreateToUserShiftRotate"];
    static string _urlGetLogDetailCreateToUserShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlGetLogDetailCreateToUserShiftRotate"];
    static string _urlGetDetailApproveOTShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailApproveOTShiftRotate"];
    static string _urlSetApproveOTShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlSetApproveOTShiftRotate"];
    static string _urlGetCountWaitApproveOTShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountWaitApproveOTShiftRotate"];
    static string _urlGetStatusSearchOTShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlGetStatusSearchOTShiftRotate"];
    static string _urlGetUserSearchOTShiftRotate = _serviceUrl + ConfigurationManager.AppSettings["urlGetUserSearchOTShiftRotate"];
    static string _urlGetDetailRotateAdminEdit = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailRotateAdminEdit"];
    static string _urlGetDataDetailRotateAdminEdit = _serviceUrl + ConfigurationManager.AppSettings["urlGetDataDetailRotateAdminEdit"];
    static string _urlGetDetailRotateAdminStartEdit = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailRotateAdminStartEdit"];
    static string _urlGetDetailRotateAdminEndEdit = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailRotateAdminEndEdit"];
    static string _urlGetDetailRotateAdminEndEdit1 = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailRotateAdminEndEdit1"];
    static string _urlGetDetailRotateAdminStartEdit1 = _serviceUrl + ConfigurationManager.AppSettings["urlGetDetailRotateAdminStartEdit1"];

    static string _path_file_otmonth = ConfigurationManager.AppSettings["path_file_otmonth"];
    //-- over time --//


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;

    string _otx1 = "x1";
    string _otx2 = "x1.5";
    string _otx3 = "x3";

    decimal tot_actual_otmonth = 0;
    decimal tot_actual_otday = 0;
    decimal tot_actualoutplan = 0;

    decimal tot_actual_otbefore = 0;
    decimal tot_actual_otafter = 0;
    decimal tot_actual_otholiday = 0;

    decimal totactual_otbefore = 0;
    decimal totactual_otafter = 0;
    decimal totactual_otholiday = 0;

    int returnResult = 0;
    int condition_ = 0;
    int _set_statusFilter = 0;
    string _sumhour = "";
    string _total_floor = "";
    string _total_time = "";
    //set rpos hr tab approve
    string set_rpos_idx_hr = "5903,5906";

    //set function cal 
    string _before = "before";
    string _after = "after";
    string _holiday = "holiday";




    //ViewState["vs_ViewWaitApproveRoomBooking"]

    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {




        _emp_idx = int.Parse(Session["emp_idx"].ToString());

        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

        ViewState["org_permission"] = _dataEmployee.employee_list[0].org_idx;
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["joblevel_permission"] = _dataEmployee.employee_list[0].jobgrade_level;
        ViewState["emp_type_permission"] = _dataEmployee.employee_list[0].emp_type_idx;
        ViewState["time_idx_permission"] = _dataEmployee.employee_list[0].ACIDX; // shift type

        //ViewState["Vs_check_tab_noidx"] = 0;



        ////Set Permission Tab Detail Admin
        //string[] setTabReportHR = set_rpos_idx_hr.Split(',');
        //for (int i = 0; i < setTabReportHR.Length; i++)
        //{
        //    if (setTabReportHR[i] == ViewState["rpos_permission"].ToString())
        //    {
        //        li4.Visible = true;

        //        break;
        //    }
        //    else
        //    {
        //        li4.Visible = false;

        //    }

        //}



    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {



            ViewState["vs_CountWaitApprove_"] = 0;
            ViewState["vs_CountWaitApprove_Head"] = 0;
            ViewState["vs_CountWaitApprove_Hr"] = 0;
            ViewState["vs_CountWaitApprove_AdminEdit"] = 0;

            ViewState["Vs_Setpermission_Admin"] = 0;


            getCountWaitDetailApproveOTAdminCreate();
            getCheckPermissionAdmin();
            initPageLoad();
            initPage();
        }


    }

    #region set/get bind data

    protected void getddl_Affiliation(DropDownList ddlName, int ACIDX)
    {
        
        //ddlName.Items.Clear();
        //ddlName.AppendDataBoundItems = true;
        //ddlName.Items.Add(new ListItem(" -- เลือก สังกัด --", "0"));

        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail dataEmployee = new employee_detail();

        dataEmployee.type_select_emp = 18; //Index     

        _dataEmployee.employee_list[0] = dataEmployee;

        
        _dataEmployee = callServiceEmployee(_urlGetEmployee, _dataEmployee);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));

        setDdlData(ddlName, _dataEmployee.employee_list, "name_Affiliation", "ACIDX");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสังกัด ---", "0"));

        ddlName.SelectedValue = ACIDX.ToString();
        


        //ddlName.Items.Insert(0, new ListItem("--- name_Affiliation ---", "0"));
        //ddlName.SelectedValue = ACIDX.ToString();

        /*
        ddlName.DataSource = _dataEmployee.employee_list;
        ddlName.DataTextField = "name_Affiliation";
        ddlName.DataValueField = "ACIDX";
        ddlName.DataBind();*/



    }

    protected void getCheckPermissionAdmin()
    {

        #region Set Permission Tab Detail Admin

        li3.Visible = false;
        li1.Visible = false;
        li0.Visible = false;


        data_overtime data_permission_admin = new data_overtime();
        ovt_permission_admin_detail permission_admin = new ovt_permission_admin_detail();
        data_permission_admin.ovt_permission_admin_list = new ovt_permission_admin_detail[1];

        permission_admin.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());

        data_permission_admin.ovt_permission_admin_list[0] = permission_admin;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b)); 
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_node));

        data_permission_admin = callServicePostOvertime(_urlGetPermissionAdminCreate, data_permission_admin);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_permission_admin));

        if (data_permission_admin.return_code == 0 || ViewState["time_idx_permission"].ToString() == "2") //admin
        {


            if (data_permission_admin.return_code == 0 && ViewState["time_idx_permission"].ToString() == "2")
            {
                ViewState["Vs_Setpermission_Admin"] = 1;
                li0.Visible = true;
                li1.Visible = true;
                li3.Visible = true;
            }
            else if (data_permission_admin.return_code == 0 && ViewState["time_idx_permission"].ToString() == "1")
            {
                ViewState["Vs_Setpermission_Admin"] = 1;
                li1.Visible = true;
                li3.Visible = true;
            }

            else if (data_permission_admin.return_code != 0 && ViewState["time_idx_permission"].ToString() == "2")
            {
                // litDebug1.Text = "3";

                ViewState["Vs_Setpermission_Admin"] = 0;

                li0.Visible = true;
                li1.Visible = true;

            }

        }
        else
        {
            ViewState["Vs_Setpermission_Admin"] = 0;
            li3.Visible = false;
            li1.Visible = false;
            li0.Visible = true;

        }

        #endregion Set Permission Tab Detail Admin

    }


    protected void getStatusSearchDocument(DropDownList ddlName, int staidx)
    {
        
        data_overtime data_m0_status_detail = new data_overtime();
        ovt_m0_status_detail m0_status_detail = new ovt_m0_status_detail();
        data_m0_status_detail.ovt_m0_status_list = new ovt_m0_status_detail[1];

        data_m0_status_detail.ovt_m0_status_list[0] = m0_status_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_m0_status_detail = callServicePostOvertime(_urlGetStatusSearchOTShiftRotate, data_m0_status_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0type_detail));

        setDdlData(ddlName, data_m0_status_detail.ovt_m0_status_list, "status_desc", "staidx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานะรายการ ---", "0"));
        ddlName.SelectedValue = staidx.ToString();
    }

    protected void getCountWaitDetailApproveOTAdminCreate()
    {

        data_overtime data_waitapprove_count = new data_overtime();
        ovt_u1doc_rotate_detail waitapprove_count = new ovt_u1doc_rotate_detail();
        data_waitapprove_count.ovt_u1doc_rotate_list = new ovt_u1doc_rotate_detail[1];

        waitapprove_count.emp_idx = _emp_idx;
        waitapprove_count.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        waitapprove_count.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        waitapprove_count.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
        waitapprove_count.joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

        data_waitapprove_count.ovt_u1doc_rotate_list[0] = waitapprove_count;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_waitapprove_count));

        data_waitapprove_count = callServicePostOvertime(_urlGetCountWaitApproveOTShiftRotate, data_waitapprove_count);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_waitapprove_count));

        if (data_waitapprove_count.return_code == 0)
        {
            //litDebug.Text = "1";
            //ViewState["vs_CountWaitApprove_"] = data_waitapprove_count.ovt_u1doc_rotate_list[0].count_all_waitapprove; // count all wait approve
            ViewState["vs_CountWaitApprove_Head"] = data_waitapprove_count.ovt_u1doc_rotate_list[0].count_head_waitapprove; // count all wait approve
            ViewState["vs_CountWaitApprove_Hr"] = data_waitapprove_count.ovt_u1doc_rotate_list[0].count_hr_waitapprove; // count all wait approve
            ViewState["vs_CountWaitApprove_AdminEdit"] = data_waitapprove_count.ovt_u1doc_rotate_list[0].count_admin_waitapprove; // count all wait approve

            ////lbApprove.Text = " รายการรออนุมัติ <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_"]) + "</span>";
            lbWaitApproveHeadUser.Text = " รายการที่รออนุมัติ <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_Head"]) + "</span>";
            lbWaitApproveHR.Text = " ส่วนของ HR <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_Hr"]) + "</span>";
            lbWaitApproveAdminEdit.Text = " รายการที่รอแก้ไข <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_AdminEdit"]) + "</span>";

            if (int.Parse(ViewState["vs_CountWaitApprove_Head"].ToString()) > 0) //head
            {
                setActiveTab("docApprove", 0, 12, 0, 0, 0, 0, 0);
            }
            else if (int.Parse(ViewState["vs_CountWaitApprove_Hr"].ToString()) > 0) //hr
            {
                //setActiveTab("docApprove", 0, 13, 0, 0, 0, 0, 0);

            }
            else
            {
                setActiveTab("docApprove", 0, 11, 0, 0, 0, 0, 0);
            }

        }
        else
        {

            //ViewState["vs_CountWaitApprove_"] = 0;
            ViewState["vs_CountWaitApprove_Head"] = 0;
            ViewState["vs_CountWaitApprove_Hr"] = 0;
            ViewState["vs_CountWaitApprove_AdminEdit"] = 0;

            ////lbApprove.Text = " รายการรออนุมัติ <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_"]) + "</span>";
            lbWaitApproveHeadUser.Text = " รายการที่รออนุมัติ <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_Head"]) + "</span>";
            lbWaitApproveHR.Text = " ส่วนของ HR <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_Hr"]) + "</span>";
            lbWaitApproveAdminEdit.Text = " รายการที่รอแก้ไข <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove_AdminEdit"]) + "</span>";


            //setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);

        }

    }

    protected void getSearchDetailWaitApprove()
    {
        IFormatProvider culture_ = new CultureInfo("en-US", true);

        var selectedEmpCode_ = new List<string>();
        string input_ = txt_empcodeApprove.Text;
        string[] recepempcode_ = input_.Split(',');
        foreach (string rempcode_ in recepempcode_)
        {
            if (rempcode_ != String.Empty)
            {
                //mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                selectedEmpCode_.Add(rempcode_);
            }
        }

        if (ViewState["Vs_WaitApproveDetailOTShiftRotateAll"] != null)
        {
            ovt_u1doc_rotate_detail[] _value_employee_waitapprove = (ovt_u1doc_rotate_detail[])ViewState["Vs_WaitApproveDetailOTShiftRotateAll"];

            var _linq_employee_waitapprove = (from data in _value_employee_waitapprove

                                              where
                                               (int.Parse(ddlemptypeApprove.SelectedValue) == 0 || data.emp_type_idx == int.Parse(ddlemptypeApprove.SelectedValue))
                                               && (int.Parse(ddlorgApprove.SelectedValue) == 0 || data.org_idx == int.Parse(ddlorgApprove.SelectedValue))
                                               && (int.Parse(ddlrdeptApprove.SelectedValue) == 0 || data.rdept_idx == int.Parse(ddlrdeptApprove.SelectedValue))
                                               && (int.Parse(ddlrsecApprove.SelectedValue) == 0 || data.rsec_idx == int.Parse(ddlrsecApprove.SelectedValue))
                                               && (int.Parse(ddlrposApprove.SelectedValue) == 0 || data.rpos_idx == int.Parse(ddlrposApprove.SelectedValue))
                                               && ((txt_dateStart_approve.Text == "") || (txt_dateEnd_approve.Text == "") || (
                                                    DateTime.ParseExact((data.datetime_otstart), "dd/MM/yyyy", culture_)
                                                    >= DateTime.ParseExact((txt_dateStart_approve.Text), "dd/MM/yyyy", culture_)
                                                    &&
                                                    DateTime.ParseExact((data.datetime_otstart), "dd/MM/yyyy", culture_)
                                                    <= DateTime.ParseExact((txt_dateEnd_approve.Text), "dd/MM/yyyy", culture_)
                                                    )
                                                  )
                                              && (selectedEmpCode_.Count == 0 || selectedEmpCode_.Contains(data.emp_code))
                                              && (int.Parse(ddl_Affiliation.SelectedValue) == 0 || data.ACIDX_Search == int.Parse(ddl_Affiliation.SelectedValue))

                                              select data
                                              ).Distinct().ToList();

            //&& (DateTime.ParseExact((dt.time_start), "dd/MM/yyyy HH:mm", culture_) > d_finish)
            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_linq_employee_waitapprove.ToList()));
            _PanelbtnApprove.Visible = true;
            ViewState["Vs_WaitApproveDetailOTShiftRotate"] = _linq_employee_waitapprove.ToList();
            setGridData(GvWaitApprove, ViewState["Vs_WaitApproveDetailOTShiftRotate"]);
        }
        else
        {
            _PanelbtnApprove.Visible = false;
            ViewState["Vs_WaitApproveDetailOTShiftRotateAll"] = null;
            setGridData(GvWaitApprove, ViewState["Vs_WaitApproveDetailOTShiftRotate"]);
        }

    }

    protected void getbtnDecisionApprove(Repeater rptName, int _m0_node_idx, int _decision_idx)
    {

        data_overtime data_u0_document_node = new data_overtime();
        ovt_m0_node_decision_detail u0_document_node = new ovt_m0_node_decision_detail();
        data_u0_document_node.ovt_m0_node_decision_list = new ovt_m0_node_decision_detail[1];
        u0_document_node.noidx = _m0_node_idx;

        data_u0_document_node.ovt_m0_node_decision_list[0] = u0_document_node;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b)); 
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_node));
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_node));
        data_u0_document_node = callServicePostOvertime(_urlGetDecisionWaitApprove, data_u0_document_node);

        setRepeaterData(rptName, data_u0_document_node.ovt_m0_node_decision_list);

    }

    protected void getWaitApproveDetailOTDayAdminCreate(GridView gvName, int m0_node_idx, int condition)
    {

        data_overtime data_u1doc_wait_detail = new data_overtime();
        ovt_u1doc_rotate_detail u1doc_wait_detail = new ovt_u1doc_rotate_detail();
        data_u1doc_wait_detail.ovt_u1doc_rotate_list = new ovt_u1doc_rotate_detail[1];

        u1doc_wait_detail.emp_idx = _emp_idx;
        u1doc_wait_detail.m0_node_idx = m0_node_idx;
        u1doc_wait_detail.condition = condition;
        u1doc_wait_detail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u1doc_wait_detail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        u1doc_wait_detail.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
        u1doc_wait_detail.joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

        data_u1doc_wait_detail.ovt_u1doc_rotate_list[0] = u1doc_wait_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1doc_otday_detail));
        data_u1doc_wait_detail = callServicePostOvertime(_urlGetDetailApproveOTShiftRotate, data_u1doc_wait_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1doc_otday_detail));

        if (data_u1doc_wait_detail.return_code == 0)
        {
            ViewState["Vs_WaitApproveDetailOTShiftRotateAll"] = data_u1doc_wait_detail.ovt_u1doc_rotate_list;
            ViewState["Vs_WaitApproveDetailOTShiftRotate"] = data_u1doc_wait_detail.ovt_u1doc_rotate_list;

            gvName.Visible = true;
            setGridData(gvName, ViewState["Vs_WaitApproveDetailOTShiftRotate"]);
        }
        else
        {
            ViewState["Vs_WaitApproveDetailOTShiftRotateAll"] = null;
            ViewState["Vs_WaitApproveDetailOTShiftRotate"] = null;

            gvName.Visible = true;
            setGridData(gvName, ViewState["Vs_WaitApproveDetailOTShiftRotate"]);
        }

    }

    protected void getWaitApproveDetailOTDayAdminEdit(GridView gvName, int m0_node_idx, int condition)
    {

        data_overtime data_u1doc_waitedit_detail = new data_overtime();
        ovt_shifttime_rotate_detail u1doc_waitedit_detail = new ovt_shifttime_rotate_detail();
        data_u1doc_waitedit_detail.ovt_shifttime_rotate_list = new ovt_shifttime_rotate_detail[1];

        u1doc_waitedit_detail.cemp_idx = _emp_idx;
        u1doc_waitedit_detail.rpos_idx_admim = int.Parse(ViewState["rpos_permission"].ToString());
        //u1doc_waitedit_detail.condition = condition;
        //u1doc_waitedit_detail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        //u1doc_waitedit_detail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        //u1doc_waitedit_detail.rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
        //u1doc_waitedit_detail.joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

        data_u1doc_waitedit_detail.ovt_shifttime_rotate_list[0] = u1doc_waitedit_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1doc_waitedit_detail));
        //data_u1doc_waitedit_detail = callServicePostOvertime(_urlGetDetailRotateAdminEdit, data_u1doc_waitedit_detail);
        data_u1doc_waitedit_detail = callServicePostOvertime(_urlGetDataDetailRotateAdminEdit, data_u1doc_waitedit_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1doc_waitedit_detail));

        if (data_u1doc_waitedit_detail.return_code == 0)
        {

            ViewState["Vs_WaitEditApproveDetailOTShiftRotateALL"] = data_u1doc_waitedit_detail.ovt_shifttime_rotate_list;

            ViewState["Vs_WaitEditApproveDetailOTShiftRotate"] = data_u1doc_waitedit_detail.ovt_shifttime_rotate_list;

            gvName.Visible = true;
            setGridData(gvName, ViewState["Vs_WaitEditApproveDetailOTShiftRotate"]);
        }
        else
        {
            ViewState["Vs_WaitEditApproveDetailOTShiftRotate"] = null;

            gvName.Visible = true;
            setGridData(gvName, ViewState["Vs_WaitEditApproveDetailOTShiftRotate"]);
        }

    }

    protected void getLogViewDetailOTShiftRotate(Repeater rptName, int _u0doc_idx, int _u1doc_idx)
    {
        data_overtime data_userlog_shiftrotate_detail = new data_overtime();
        ovt_u1doc_rotate_detail userlog_shiftrotate_detail = new ovt_u1doc_rotate_detail();

        data_userlog_shiftrotate_detail.ovt_u1doc_rotate_list = new ovt_u1doc_rotate_detail[1];
        userlog_shiftrotate_detail.u0_doc_idx = _u0doc_idx;
        userlog_shiftrotate_detail.u1_doc_idx = _u1doc_idx;
        userlog_shiftrotate_detail.emp_idx = _emp_idx;
        //timestart_otday_detail.d = 0;

        data_userlog_shiftrotate_detail.ovt_u1doc_rotate_list[0] = userlog_shiftrotate_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_userlog_shiftrotate_detail));
        data_userlog_shiftrotate_detail = callServicePostOvertime(_urlGetLogDetailCreateToUserShiftRotate, data_userlog_shiftrotate_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_user_shiftrotate_detail));

        ViewState["VsLog_DetailShiftRotateToUser"] = data_userlog_shiftrotate_detail.ovt_u1doc_rotate_list;

        setRepeaterData(rptName, ViewState["VsLog_DetailShiftRotateToUser"]);

        //gvName.Visible = true;
        //setGridData(gvName, ViewState["Vs_DetailShiftRotateToUser"]);

    }

    protected void getViewDetailShiftRotateUser(FormView fvName, int _u1doc_idx)
    {
        data_overtime data_user_viewshiftrotate_detail = new data_overtime();
        ovt_u1doc_rotate_detail user_viewshiftrotate_detail = new ovt_u1doc_rotate_detail();

        data_user_viewshiftrotate_detail.ovt_u1doc_rotate_list = new ovt_u1doc_rotate_detail[1];

        user_viewshiftrotate_detail.u1_doc_idx = _u1doc_idx;
        //user_viewshiftrotate_detail.emp_idx = _emp_idx;
        //timestart_otday_detail.d = 0;

        data_user_viewshiftrotate_detail.ovt_u1doc_rotate_list[0] = user_viewshiftrotate_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_user_viewshiftrotate_detail));
        data_user_viewshiftrotate_detail = callServicePostOvertime(_urlGetDetailCreateToUserShiftRotate, data_user_viewshiftrotate_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_user_shiftrotate_detail));

        ViewState["Vs_ViewDetailShiftRotateToUser"] = data_user_viewshiftrotate_detail.ovt_u1doc_rotate_list;

        setFormData(fvName, FormViewMode.ReadOnly, ViewState["Vs_ViewDetailShiftRotateToUser"]);

        //gvName.Visible = true;
        //setGridData(gvName, ViewState["Vs_DetailShiftRotateToUser"]);

    }

    protected void getDetailShiftRotateUser(GridView gvName, int _u1doc_idx)
    {
        data_overtime data_user_shiftrotate_detail = new data_overtime();
        ovt_u1doc_rotate_detail user_shiftrotate_detail = new ovt_u1doc_rotate_detail();

        data_user_shiftrotate_detail.ovt_u1doc_rotate_list = new ovt_u1doc_rotate_detail[1];

        user_shiftrotate_detail.u1_doc_idx = _u1doc_idx;
        user_shiftrotate_detail.emp_idx = _emp_idx;
        //timestart_otday_detail.d = 0;

        data_user_shiftrotate_detail.ovt_u1doc_rotate_list[0] = user_shiftrotate_detail;

        data_user_shiftrotate_detail = callServicePostOvertime(_urlGetDetailCreateToUserShiftRotate, data_user_shiftrotate_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_user_shiftrotate_detail));

        ViewState["Vs_DetailShiftRotateToUser"] = data_user_shiftrotate_detail.ovt_u1doc_rotate_list;

        gvName.Visible = true;
        setGridData(gvName, ViewState["Vs_DetailShiftRotateToUser"]);

    }

    protected void getTimeStartOTDay(string _datesearch)
    {
        data_overtime data_timestart_otday_detail = new data_overtime();
        ovt_timestart_otday_detail timestart_otday_detail = new ovt_timestart_otday_detail();
        data_timestart_otday_detail.ovt_timestart_otday_list = new ovt_timestart_otday_detail[1];
        timestart_otday_detail.condition = 0;
        timestart_otday_detail.datesearch = _datesearch.ToString();
        //timestart_otday_detail.d = 0;

        data_timestart_otday_detail.ovt_timestart_otday_list[0] = timestart_otday_detail;

        data_timestart_otday_detail = callServicePostOvertime(_urlGetTimeStartOTDay, data_timestart_otday_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_timestart_otday_detail));
        if (data_timestart_otday_detail.return_code == 0)
        {
            ViewState["Vs_TimeStartOTDay"] = data_timestart_otday_detail.ovt_timestart_otday_list;
        }
        else
        {
            ViewState["Vs_TimeStartOTDay"] = null;
        }


    }

    protected void getTimeEndOTDay(string _datesearch)
    {
        data_overtime data_timeend_otday_detail = new data_overtime();
        ovt_timestart_otday_detail timeend_otday_detail = new ovt_timestart_otday_detail();
        data_timeend_otday_detail.ovt_timestart_otday_list = new ovt_timestart_otday_detail[1];
        timeend_otday_detail.condition = 1;
        timeend_otday_detail.datesearch = _datesearch.ToString();

        data_timeend_otday_detail.ovt_timestart_otday_list[0] = timeend_otday_detail;

        data_timeend_otday_detail = callServicePostOvertime(_urlGetTimeStartOTDay, data_timeend_otday_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_timeend_otday_detail));
        if (data_timeend_otday_detail.return_code == 0)
        {
            ViewState["Vs_TimeEndOTDay"] = data_timeend_otday_detail.ovt_timestart_otday_list;
        }
        else
        {
            ViewState["Vs_TimeEndOTDay"] = null;
        }


    }

    protected void getTimeStartOTDay_Scan(string _datesearch)
    {
        data_overtime data_timestart_otdayscan_detail = new data_overtime();
        ovt_timestart_otday_detail timestart_otdayscan_detail = new ovt_timestart_otday_detail();
        data_timestart_otdayscan_detail.ovt_timestart_otday_list = new ovt_timestart_otday_detail[1];
        timestart_otdayscan_detail.condition = 2;
        timestart_otdayscan_detail.datesearch = _datesearch.ToString();

        data_timestart_otdayscan_detail.ovt_timestart_otday_list[0] = timestart_otdayscan_detail;

        data_timestart_otdayscan_detail = callServicePostOvertime(_urlGetTimeStartOTDay, data_timestart_otdayscan_detail);

        if (data_timestart_otdayscan_detail.return_code == 0)
        {
            ViewState["Vs_TimeStartOTDay_Scan"] = data_timestart_otdayscan_detail.ovt_timestart_otday_list;
        }
        else
        {
            ViewState["Vs_TimeStartOTDay_Scan"] = null;
        }


    }

    protected void getTimeEndOTDay_Scan(string _datesearch)
    {
        data_overtime data_timeend_otdayscan_detail = new data_overtime();
        ovt_timestart_otday_detail timeend_otdayscan_detail = new ovt_timestart_otday_detail();
        data_timeend_otdayscan_detail.ovt_timestart_otday_list = new ovt_timestart_otday_detail[1];
        timeend_otdayscan_detail.condition = 3;
        timeend_otdayscan_detail.datesearch = _datesearch.ToString();

        data_timeend_otdayscan_detail.ovt_timestart_otday_list[0] = timeend_otdayscan_detail;

        data_timeend_otdayscan_detail = callServicePostOvertime(_urlGetTimeStartOTDay, data_timeend_otdayscan_detail);

        if (data_timeend_otdayscan_detail.return_code == 0)
        {
            ViewState["Vs_TimeEndOTDay_Scan"] = data_timeend_otdayscan_detail.ovt_timestart_otday_list;
        }
        else
        {
            ViewState["Vs_TimeEndOTDay_Scan"] = null;
        }



    }

    protected void GetPathFile(string _u0_doc_idx, string path, GridView gvName)
    {
        try
        {
            string filePathLotus = Server.MapPath(path + _u0_doc_idx);
            DirectoryInfo myDirLotus = new DirectoryInfo(filePathLotus);
            SearchDirectories(myDirLotus, _u0_doc_idx, gvName);
            gvName.Visible = true;

            ViewState["Vs_CheckFileMemo"] = "yes";

        }
        catch (Exception ex)
        {
            gvName.Visible = false;
            ViewState["Vs_CheckFileMemo"] = "no";
            //txt.Text = ex.ToString();
        }
    }

    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    public void SearchDirectories(DirectoryInfo dir, String target, GridView gvName)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");


        FileInfo[] files = dir.GetFiles();
        int i = 0;
        foreach (FileInfo file in files)
        {
            //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
            //{
            string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
            dt1.Rows.Add(file.Name);
            dt1.Rows[i][1] = f[0];
            i++;
            //}
        }


        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            gvName.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvName.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvName.DataSource = null;
            gvName.DataBind();

        }


    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกองค์กร --", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        data_employee _dataEmployee_rdept = new data_employee();
        _dataEmployee_rdept.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee_rdept.department_list[0] = _deptList;

        _dataEmployee_rdept = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee_rdept);
        setDdlData(ddlName, _dataEmployee_rdept.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกฝ่าย --", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));
        //ddlName.SelectedValue = _rdept_idx.ToString();

    }

    protected void getPositionList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.position_list = new position_details[1];
        position_details _posList = new position_details();
        _posList.org_idx = _org_idx;
        _posList.rdept_idx = _rdept_idx;
        _posList.rsec_idx = _rsec_idx;
        _dataEmployee.position_list[0] = _posList;

        _dataEmployee = callServicePostEmployee(_urlGetPositionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.position_list, "pos_name_th", "rpos_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกตำแหน่ง --", "0"));
        //ddlName.SelectedValue = _rpos_idx.ToString();
    }

    protected void getEmployeeList(CheckBoxList chkName, int _org_idx, int _rdept_idx, int _rsec_idx, int _rpos_idx, UpdatePanel pnName)
    {
        chkName.Items.Clear();
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _employeeList = new employee_detail();
        _employeeList.org_idx = _org_idx;
        _employeeList.rdept_idx = _rdept_idx;
        _employeeList.rsec_idx = _rsec_idx;
        _employeeList.rpos_idx = _rpos_idx;
        _employeeList.emp_status = 1;
        _dataEmployee.employee_list[0] = _employeeList;

        _dataEmployee = callServicePostEmployee(urlGetAll, _dataEmployee);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dataEmployee));
        //litDebug.Text = _dataEmployee.employee_list.Length.ToString();

        if (_dataEmployee.return_code == "0")
        {

            // litDebug.Text = _dataEmployee.employee_list.Length.ToString();
            pnName.Visible = true;
            setChkData(chkName, _dataEmployee.employee_list, "emp_code", "emp_code");
        }
        else
        {
            pnName.Visible = false;
        }


    }



    #endregion set/get bind data  

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, 0, 0, 0, 0);

    }

    protected void ClickPrint(object sender, EventArgs e)
    {
        //  litDebug.Text = "lals";
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        //linkBtnTrigger(lbDocSaveImport);

        switch (cmdName)
        {

            case "cmdCancel":

                //setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);
                //litDebug.Text = ViewState["Vs_Setpermission_Admin"].ToString();
                if (ViewState["Vs_Setpermission_Admin"].ToString() == "1") //admin
                {
                    setActiveTab("docDetailAdmin", 0, 0, 0, 0, 0, 0, 0);
                }
                else
                {
                    setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);
                }
                // getCountWaitDetailApproveOTAdminCreate();
                break;

            case "cmdSearchDate":

                data_overtime data_search_date = new data_overtime();
                ovt_shifttime_rotate_detail u0_searchdate = new ovt_shifttime_rotate_detail();
                data_search_date.ovt_shifttime_rotate_list = new ovt_shifttime_rotate_detail[1];

                u0_searchdate.cemp_idx = _emp_idx;
                u0_searchdate.rpos_idx_admim = int.Parse(ViewState["rpos_permission"].ToString());
                u0_searchdate.search_date = txtDateStart_create.Text;

                int i = 0;
                string value_empcode = "";
                List<String> AddoingList_empcode = new List<string>();


                if (ddlrposCreate.SelectedValue != "0")
                {
                    foreach (ListItem chkList_empcode in chkempCreate.Items)
                    {
                        if (chkList_empcode.Selected)
                        {
                            // u0_searchdate[i_excel] = new ovt_shifttime_rotate_detail();
                            u0_searchdate.emp_idxvalue += chkList_empcode.Value + ","; //chkList_empcode.Value;
                            value_empcode += "," + chkList_empcode.Value;

                            //sumcheck_excel = sumcheck_excel + 1;

                            i++;
                        }
                    }

                }
                else
                {
                    u0_searchdate.emp_idxvalue = ""; //chkList_empcode.Value;
                    value_empcode = "";
                }

                data_search_date.ovt_shifttime_rotate_list[0] = u0_searchdate;


               
                data_search_date = callServicePostOvertime(_urlGetDetailOTCreateShiftRotate, data_search_date);

                //litDebug1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_date));
                if (data_search_date.return_code == 0)
                {

                    if (data_search_date.ovt_shifttime_rotatestart_list[0].emp_idx != 0 && data_search_date.ovt_shifttime_rotateend_list[0].emp_idx != 0)
                    {
                        ViewState["Vs_DateTimeToCreateOTALL"] = data_search_date.ovt_shifttime_rotate_list;
                        ViewState["Vs_DateTimeToCreateOT"] = data_search_date.ovt_shifttime_rotate_list;
                        ViewState["Vs_ScanIn_OT"] = data_search_date.ovt_shifttime_rotatestart_list;
                        ViewState["Vs_ScanOut_OT"] = data_search_date.ovt_shifttime_rotateend_list;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_date));


                        ///////////////////////////////

                        var selectedEmpCode = new List<string>();
                        ////for (int k = 0; k < chkempCreate.Items.Count; k++)
                        ////{
                        ////    if (chkempCreate.Items[k].Selected)
                        ////    {
                        ////        selectedEmpCode.Add(chkempCreate.Items[k].Value);
                        ////    }
                        ////}

                        string input = txt_empcodeCreate.Text;//"62000828,";
                        string[] recepempcode = input.Split(',');
                        foreach (string rempcode in recepempcode)
                        {
                            if (rempcode != String.Empty)
                            {
                                //mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                                selectedEmpCode.Add(rempcode);
                            }
                        }
                        //litDebug.Text = selectedEmpCode.Count().ToString();

                        ovt_shifttime_rotate_detail[] _value_employee_create = (ovt_shifttime_rotate_detail[])ViewState["Vs_DateTimeToCreateOTALL"];

                        var _linq_employee_createot = (from data in _value_employee_create

                                                       where
                                                       (int.Parse(ddlemptypeCreate.SelectedValue) == 0 || data.emp_type_idx == int.Parse(ddlemptypeCreate.SelectedValue))
                                                       && (data.org_idx == int.Parse(ddlorgCreate.SelectedValue))
                                                       && (int.Parse(ddlrdeptCreate.SelectedValue) == 0 || data.rdept_idx == int.Parse(ddlrdeptCreate.SelectedValue))
                                                       && (int.Parse(ddlrsecCreate.SelectedValue) == 0 || data.rsec_idx == int.Parse(ddlrsecCreate.SelectedValue))
                                                       && (int.Parse(ddlrposCreate.SelectedValue) == 0 || data.rpos_idx == int.Parse(ddlrposCreate.SelectedValue))
                                                       && (selectedEmpCode.Count == 0 || selectedEmpCode.Contains(data.emp_code))

                                                       select new
                                                       {
                                                           data.emp_idx,
                                                           data.emp_code,
                                                           data.emp_name_th,
                                                           data.pos_name_th,
                                                           data.date_ot,
                                                           data.parttime_name_th,
                                                           data.holiday_idx,
                                                           data.holiday_name,
                                                           data.day_off,
                                                           data.date_condition,
                                                           data.parttime_break_start_time,
                                                           data.parttime_break_end_time,
                                                           data.emp_type_idx,
                                                           data.STIDX,
                                                           data.m0_parttime_idx,
                                                           data.parttime_start_time,
                                                           data.parttime_end_time,
                                                           data.org_idx,
                                                           data.rdept_idx,
                                                           data.rsec_idx,
                                                           data.rpos_idx,

                                                       }
                                                   ).Distinct().ToList();



                        PanelDetailOTCreate.Visible = true;
                        setGridData(GvCreateShiftRotate, _linq_employee_createot.ToList());

                    }
                    else
                    {
                        ViewState["Vs_DateTimeToCreateOT"] = null;

                        setGridData(GvCreateShiftRotate, ViewState["Vs_DateTimeToCreateOT"]);
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                        break;
                    }

                    //litDebug.Text = GvCreateShiftRotate.Rows.Count.ToString();


                }
                else
                {

                    ViewState["Vs_DateTimeToCreateOT"] = null;

                    setGridData(GvCreateShiftRotate, ViewState["Vs_DateTimeToCreateOT"]);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                    break;
                }

                break;
            case "cmdClearSearch":

                setActiveTab("docCreate", 0, 0, 0, 0, 0, 0, 0);
                break;

            case "cmdSearchWaitApprove":

                getSearchDetailWaitApprove();

                //IFormatProvider culture_ = new CultureInfo("en-US", true);

                //var selectedEmpCode_ = new List<string>();
                //string input_ = txt_empcodeApprove.Text;
                //string[] recepempcode_ = input_.Split(',');
                //foreach (string rempcode_ in recepempcode_)
                //{
                //    if (rempcode_ != String.Empty)
                //    {
                //        //mailMessage.To.Add(new MailAddress(rEmail)); //Adding Multiple To email Id
                //        selectedEmpCode_.Add(rempcode_);
                //    }
                //}

                //ovt_u1doc_rotate_detail[] _value_employee_waitapprove= (ovt_u1doc_rotate_detail[])ViewState["Vs_WaitApproveDetailOTShiftRotateAll"];

                //var _linq_employee_waitapprove = (from data in _value_employee_waitapprove

                //                                  where
                //                                   (int.Parse(ddlemptypeApprove.SelectedValue) == 0 || data.emp_type_idx == int.Parse(ddlemptypeApprove.SelectedValue))
                //                                   && (data.org_idx == int.Parse(ddlorgApprove.SelectedValue))
                //                                   && (int.Parse(ddlrdeptApprove.SelectedValue) == 0 || data.rdept_idx == int.Parse(ddlrdeptApprove.SelectedValue))
                //                                   && (int.Parse(ddlrsecApprove.SelectedValue) == 0 || data.rsec_idx == int.Parse(ddlrsecApprove.SelectedValue))
                //                                   && (int.Parse(ddlrposApprove.SelectedValue) == 0 || data.rpos_idx == int.Parse(ddlrposApprove.SelectedValue))
                //                                   && ((
                //                                        DateTime.ParseExact((data.datetime_otstart), "dd/MM/yyyy", culture_) 
                //                                        >= DateTime.ParseExact((txt_dateStart_approve.Text), "dd/MM/yyyy", culture_) 
                //                                        &&
                //                                        DateTime.ParseExact((data.datetime_otstart), "dd/MM/yyyy", culture_)
                //                                        <= DateTime.ParseExact((txt_dateEnd_approve.Text), "dd/MM/yyyy", culture_)
                //                                        )
                //                                      )
                //                                  && (selectedEmpCode_.Count == 0 || selectedEmpCode_.Contains(data.emp_code))

                //                                  select data
                //                                  ).Distinct().ToList();

                ////&& (DateTime.ParseExact((dt.time_start), "dd/MM/yyyy HH:mm", culture_) > d_finish)
                ////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_linq_employee_waitapprove.ToList()));
                //ViewState["Vs_WaitApproveDetailOTShiftRotate"] = _linq_employee_waitapprove.ToList();
                //setGridData(GvWaitApprove, ViewState["Vs_WaitApproveDetailOTShiftRotate"]);

                break;

            case "cmdClearSearchWaitApprove":

                txt_dateStart_approve.Text = String.Empty;
                txt_dateEnd_approve.Text = String.Empty;
                ddlemptypeApprove.ClearSelection();
                txt_empcodeApprove.Text = String.Empty;


                _PanelWaitApprove.Visible = true;
                li5.Attributes.Add("class", "active");

                ViewState["Vs_check_tab_noidx"] = 13; //hr approve
                //divheadapprove_otday.Visible = true;

                getWaitApproveDetailOTDayAdminCreate(GvWaitApprove, 13, 2);

                if (ViewState["Vs_WaitApproveDetailOTShiftRotate"] != null)
                {
                    _PanelSearchWaitApprove.Visible = true;

                    getddl_Affiliation(ddl_Affiliation, 0);

                    getOrganizationList(ddlorgApprove);
                    getDepartmentList(ddlrdeptApprove, int.Parse(ddlorgApprove.SelectedValue));
                    getSectionList(ddlrsecApprove, int.Parse(ddlorgApprove.SelectedValue), int.Parse(ddlrdeptApprove.SelectedValue));
                    getPositionList(ddlrposApprove, int.Parse(ddlorgApprove.SelectedValue), int.Parse(ddlrdeptApprove.SelectedValue), int.Parse(ddlrsecApprove.SelectedValue));

                    _PanelbtnApprove.Visible = true;
                    getbtnDecisionApprove(rptBindbtnApprove, 13, 0);
                    
                }

                ////getOrganizationList(ddlorgApprove);
                ////getDepartmentList(ddlrdeptApprove, int.Parse(ddlorgApprove.SelectedValue));
                ////getSectionList(ddlrsecApprove, int.Parse(ddlorgApprove.SelectedValue), int.Parse(ddlrdeptApprove.SelectedValue));
                ////getPositionList(ddlrposApprove, int.Parse(ddlorgApprove.SelectedValue), int.Parse(ddlrdeptApprove.SelectedValue), int.Parse(ddlrsecApprove.SelectedValue));


                //setActiveTab("docApprove", 0, 13, 0, 0, 0, 0, 0);
                break;

            case "cmdSave":

                data_overtime data_create_admin = new data_overtime();

                //insert u0 document ot ratate by admin
                ovt_u0doc_rotate_detail u0_create_admin = new ovt_u0doc_rotate_detail();
                data_create_admin.ovt_u0doc_rotate_list = new ovt_u0doc_rotate_detail[1];

                u0_create_admin.cemp_idx = _emp_idx;
                u0_create_admin.cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                u0_create_admin.m0_actor_idx = 4;
                u0_create_admin.m0_node_idx = 11;
                u0_create_admin.decision = 17;

                data_create_admin.ovt_u0doc_rotate_list[0] = u0_create_admin;
                //insert u0 document ot ratate by admin

                //insert u1 document ot ratate
                var _u1_doc_idx = new ovt_u1doc_rotate_detail[GvCreateShiftRotate.Rows.Count];
                int sum_check = 0;
                int count_check = 0;
                //int sum_total_price = 0;

                foreach (GridViewRow gv_row in GvCreateShiftRotate.Rows)
                {

                    data_create_admin.ovt_u1doc_rotate_list = new ovt_u1doc_rotate_detail[1];

                    CheckBox chk_otday = (CheckBox)gv_row.FindControl("chk_otday");
                    Label lbl_emp_idx = (Label)gv_row.FindControl("lbl_emp_idx");

                    Label lbl_STIDX = (Label)gv_row.FindControl("lbl_STIDX");
                    Label lbl_m0_parttime_idx = (Label)gv_row.FindControl("lbl_m0_parttime_idx");

                    Label lbl_scan_in = (Label)gv_row.FindControl("lbl_scan_in");
                    Label lbl_scan_out = (Label)gv_row.FindControl("lbl_scan_out");

                    Label lbl_holiday_idx = (Label)gv_row.FindControl("lbl_holiday_idx");
                    Label lbl_day_off = (Label)gv_row.FindControl("lbl_day_off");
                    Label lbl_date_condition = (Label)gv_row.FindControl("lbl_date_condition");

                    Label lbl_parttime_start_time = (Label)gv_row.FindControl("lbl_parttime_start_time");
                    Label lbl_parttime_end_time = (Label)gv_row.FindControl("lbl_parttime_end_time");

                    DropDownList ddl_timebefore = (DropDownList)gv_row.FindControl("ddl_timebefore");
                    DropDownList ddl_timeafter = (DropDownList)gv_row.FindControl("ddl_timeafter");
                    DropDownList ddl_timeholiday = (DropDownList)gv_row.FindControl("ddl_timeholiday");

                    Label lbl_hour_otbefore = (Label)gv_row.FindControl("lbl_hour_otbefore");
                    Label lbl_hour_otafter = (Label)gv_row.FindControl("lbl_hour_otafter");
                    Label lbl_hour_otholiday = (Label)gv_row.FindControl("lbl_hour_otholiday");
                    TextBox txt_remark_shift_otholiday = (TextBox)gv_row.FindControl("txt_remark_shift_otholiday");

                    Label lbl_ot_before = (Label)gv_row.FindControl("lbl_ot_before");
                    Label lbl_ot_after = (Label)gv_row.FindControl("lbl_ot_after");
                    Label lbl_ot_holiday = (Label)gv_row.FindControl("lbl_ot_holiday");

                    DropDownList ddl_timestart = (DropDownList)gv_row.FindControl("ddl_timestart");
                    DropDownList ddl_timeend = (DropDownList)gv_row.FindControl("ddl_timeend");


                    double _ot_holiday_max;
                    double _ot_before_max;
                    double _ot_after_max;

                    if (chk_otday.Checked == true)
                    {

                        _u1_doc_idx[count_check] = new ovt_u1doc_rotate_detail();
                        _u1_doc_idx[count_check].cemp_idx = _emp_idx;
                        _u1_doc_idx[count_check].cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                        _u1_doc_idx[count_check].m0_actor_idx = 4;
                        _u1_doc_idx[count_check].m0_node_idx = 11;
                        _u1_doc_idx[count_check].decision = 17;
                        _u1_doc_idx[count_check].emp_idx = int.Parse(lbl_emp_idx.Text);
                        _u1_doc_idx[count_check].STIDX = int.Parse(lbl_STIDX.Text);
                        _u1_doc_idx[count_check].datetime_start = ddl_timestart.SelectedItem.ToString();//lbl_scan_in.Text;
                        _u1_doc_idx[count_check].datetime_end = ddl_timeend.SelectedItem.ToString();//lbl_scan_out.Text;
                        _u1_doc_idx[count_check].shift_start = lbl_parttime_start_time.Text;
                        _u1_doc_idx[count_check].shift_end = lbl_parttime_end_time.Text;
                        _u1_doc_idx[count_check].day_off = lbl_day_off.Text;
                        _u1_doc_idx[count_check].day_condition = int.Parse(lbl_date_condition.Text);
                        _u1_doc_idx[count_check].holiday_idx = int.Parse(lbl_holiday_idx.Text);
                        _u1_doc_idx[count_check].m0_parttime_idx = int.Parse(lbl_m0_parttime_idx.Text);

                        ////if (lbl_ot_before.Text != "")
                        ////{
                        ////    _ot_before_max = Convert.ToDouble(lbl_ot_before.Text);
                        ////}
                        ////else
                        ////{
                        ////    _ot_before_max = 0.0;
                        ////}

                        ////if (lbl_ot_after.Text != "")
                        ////{
                        ////    _ot_after_max = Convert.ToDouble(lbl_ot_after.Text);
                        ////}
                        ////else
                        ////{
                        ////    _ot_after_max = 0.0;
                        ////}

                        ////if (lbl_ot_holiday.Text != "")
                        ////{
                        ////    _ot_holiday_max = Convert.ToDouble(lbl_ot_holiday.Text);
                        ////}
                        ////else
                        ////{
                        ////    _ot_holiday_max = 0.0;
                        ////}

                        //////before
                        ////if (_ot_before_max >= 0.5)
                        ////{
                        ////    _u1_doc_idx[count_check].ot_before_max = lbl_ot_before.Text;
                        ////}
                        ////else
                        ////{
                        ////    _u1_doc_idx[count_check].ot_before_max = "";
                        ////}

                        //////after
                        ////if (_ot_after_max >= 0.5)
                        ////{
                        ////    _u1_doc_idx[count_check].ot_after_max = lbl_ot_after.Text;
                        ////}
                        ////else
                        ////{
                        ////    _u1_doc_idx[count_check].ot_after_max = "";
                        ////}

                        //////holiday
                        ////if (_ot_holiday_max >= 0.5)
                        ////{
                        ////    _u1_doc_idx[count_check].ot_holiday_max = lbl_ot_holiday.Text;
                        ////}
                        ////else
                        ////{
                        ////    _u1_doc_idx[count_check].ot_holiday_max = "";
                        ////}

                        if (ddl_timebefore.SelectedValue != "")
                        {
                            _u1_doc_idx[count_check].ot_before = ddl_timebefore.SelectedItem.Text;
                            _u1_doc_idx[count_check].hour_ot_before = lbl_hour_otbefore.Text;
                        }

                        if (ddl_timeafter.SelectedValue != "")
                        {
                            _u1_doc_idx[count_check].ot_after = ddl_timeafter.SelectedItem.Text;
                            _u1_doc_idx[count_check].hour_ot_after = lbl_hour_otafter.Text;
                        }

                        if (ddl_timeholiday.SelectedValue != "")
                        {
                            _u1_doc_idx[count_check].ot_holiday = ddl_timeholiday.SelectedItem.Text;
                            _u1_doc_idx[count_check].hour_ot_holiday = lbl_hour_otholiday.Text;
                        }

                        if (txt_remark_shift_otholiday.Text != "")
                        {
                            _u1_doc_idx[count_check].remark_admin = txt_remark_shift_otholiday.Text;
                        }
                        else
                        {
                            _u1_doc_idx[count_check].remark_admin = "-";
                        }

                        sum_check = sum_check + 1;
                        count_check++;

                    }

                }
                //insert u1 document ot month

                //check selected count ot
                if (sum_check == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert('--- กรุณาเลือกข้อมูล ก่อนทำการบันทึก ---');"), true);
                    break;
                }
                else
                {
                    data_create_admin.ovt_u0doc_rotate_list[0] = u0_create_admin;
                    data_create_admin.ovt_u1doc_rotate_list = _u1_doc_idx;
                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_create_admin));
                    data_create_admin = callServicePostOvertime(_urlSetCreateOTShiftRotate, data_create_admin);


                    if (ViewState["Vs_Setpermission_Admin"].ToString() == "1") //admin
                    {
                        setActiveTab("docDetailAdmin", 0, 0, 0, 0, 0, 0, 0);
                    }
                    else
                    {
                        setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);
                    }


                    //getCountWaitDetailApproveOTAdminCreate();

                }

                break;
            case "cmdViewAdminShiftRotate":
                setActiveTab("docDetailAdmin", int.Parse(cmdArg.ToString()), 0, 0, 0, 0, 0, 0);
                break;
            case "cmdBackToDetailAdmin":
                setActiveTab("docDetailAdmin", 0, 0, 0, 0, 0, 0, 0);
                break;
            case "cmdViewUserShiftRotate":

                string[] arg_viewuser = new string[1];
                arg_viewuser = e.CommandArgument.ToString().Split(';');
                int _u0_doc_idx_viewuser = int.Parse(arg_viewuser[0]);
                int _u1_doc_idx_viewuser = int.Parse(arg_viewuser[1]);
                // string _decision_name_saveotday = arg_viewuser[1];


                setActiveTab("docDetail", _u0_doc_idx_viewuser, 0, 0, 0, 0, 0, _u1_doc_idx_viewuser);
                break;
            
            case "cmdApproveOTShiftRotate":

                setActiveTab("docApprove", 0, int.Parse(cmdArg.ToString()), 0, 0, 0, 0, 0);

                break;

            case "cmdSaveAdminEdit":

                string[] arg_edit = new string[1];
                arg_edit = e.CommandArgument.ToString().Split(';');
                int _decision_idx_edit = int.Parse(arg_edit[0]);

                data_overtime data_ovt_u1doc_edit = new data_overtime();

                //update u1 document ot month
                var _u1doc_ot_edit = new ovt_u1doc_rotate_detail[GvWaitAdminEditDoc.Rows.Count];
                int sum_edit_ = 0;
                int count_edit_ = 0;
                string u0_sentmail = "";

                foreach (GridViewRow gv_row_edit in GvWaitAdminEditDoc.Rows)
                {

                    data_ovt_u1doc_edit.ovt_u1doc_rotate_list = new ovt_u1doc_rotate_detail[1];

                    CheckBox chk_edit = (CheckBox)gv_row_edit.FindControl("chk_edit");
                    Label lbl_u1_doc_idx_edit = (Label)gv_row_edit.FindControl("lbl_u1_doc_idx_edit");
                    Label lbl_u0_doc_idx_edit = (Label)gv_row_edit.FindControl("lbl_u0_doc_idx_edit");

                    Label lbl_staidx_edit = (Label)gv_row_edit.FindControl("lbl_staidx_edit");
                    Label lbl_m0_node_idx_edit = (Label)gv_row_edit.FindControl("lbl_m0_node_idx_edit");
                    Label lbl_m0_actor_idx_edit = (Label)gv_row_edit.FindControl("lbl_m0_actor_idx_edit");

                    TextBox txt_comment_head_edit = (TextBox)gv_row_edit.FindControl("txt_comment_head_approve");
                    TextBox txt_remark_admin_edit = (TextBox)gv_row_edit.FindControl("txt_remark_admin_edit");
                    TextBox txt_comment_hr_edit = (TextBox)gv_row_edit.FindControl("txt_comment_hr_check");

                    DropDownList ddl_timebefore_edit = (DropDownList)gv_row_edit.FindControl("ddl_timebefore_edit");
                    DropDownList ddl_timeafter_edit = (DropDownList)gv_row_edit.FindControl("ddl_timeafter_edit");
                    DropDownList ddl_timeholiday_edit = (DropDownList)gv_row_edit.FindControl("ddl_timeholiday_edit");

                    DropDownList ddl_timestart_edit = (DropDownList)gv_row_edit.FindControl("ddl_timestart_edit");
                    DropDownList ddl_timeend_edit = (DropDownList)gv_row_edit.FindControl("ddl_timeend_edit");

                    Label lbl_hour_otbefore_edit = (Label)gv_row_edit.FindControl("lbl_hour_otbefore_edit");
                    Label lbl_hour_otafter_edit = (Label)gv_row_edit.FindControl("lbl_hour_otafter_edit");
                    Label lbl_hour_otholiday_edit = (Label)gv_row_edit.FindControl("lbl_hour_otholiday_edit");

                    if (chk_edit.Checked == true)
                    {

                        _u1doc_ot_edit[count_edit_] = new ovt_u1doc_rotate_detail();
                        //_u1doc_otmonth_approve[count_approve].emp_code = tbEmpCode_.Text;
                        _u1doc_ot_edit[count_edit_].u0_doc_idx = int.Parse(lbl_u0_doc_idx_edit.Text);
                        _u1doc_ot_edit[count_edit_].u1_doc_idx = int.Parse(lbl_u1_doc_idx_edit.Text);
                        _u1doc_ot_edit[count_edit_].m0_node_idx = int.Parse(lbl_m0_node_idx_edit.Text);
                        _u1doc_ot_edit[count_edit_].m0_actor_idx = int.Parse(lbl_m0_actor_idx_edit.Text);
                        _u1doc_ot_edit[count_edit_].staidx = int.Parse(lbl_staidx_edit.Text);
                        _u1doc_ot_edit[count_edit_].decision = _decision_idx_edit;
                        _u1doc_ot_edit[count_edit_].cemp_idx = _emp_idx;
                        _u1doc_ot_edit[count_edit_].comment_head = txt_comment_head_edit.Text;
                        _u1doc_ot_edit[count_edit_].comment_hr = txt_comment_hr_edit.Text;
                        _u1doc_ot_edit[count_edit_].remark_admin = txt_remark_admin_edit.Text;

                        _u1doc_ot_edit[count_edit_].datetime_start = ddl_timestart_edit.SelectedItem.Text;
                        _u1doc_ot_edit[count_edit_].datetime_end = ddl_timeend_edit.SelectedItem.Text;

                        if (ddl_timebefore_edit.SelectedValue != "")
                        {
                            _u1doc_ot_edit[count_edit_].ot_before = ddl_timebefore_edit.SelectedItem.Text;
                            _u1doc_ot_edit[count_edit_].hour_ot_before = lbl_hour_otbefore_edit.Text;
                        }

                        if (ddl_timeafter_edit.SelectedValue != "")
                        {
                            _u1doc_ot_edit[count_edit_].ot_after = ddl_timeafter_edit.SelectedItem.Text;
                            _u1doc_ot_edit[count_edit_].hour_ot_after = lbl_hour_otafter_edit.Text;
                        }

                        if (ddl_timeholiday_edit.SelectedValue != "")
                        {
                            _u1doc_ot_edit[count_edit_].ot_holiday = ddl_timeholiday_edit.SelectedItem.Text;
                            _u1doc_ot_edit[count_edit_].hour_ot_holiday = lbl_hour_otholiday_edit.Text;
                        }


                        sum_edit_ = sum_edit_ + 1;
                        count_edit_++;

                    }

                }
                //update u1 document ot shift rotate
                if (sum_edit_ == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณาเลือกรายการ ก่อนทำการบันทึก ---');", true);
                    break;
                }
                else
                {


                    data_ovt_u1doc_edit.ovt_u1doc_rotate_list = _u1doc_ot_edit;
                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ovt_u1doc_edit));
                    data_ovt_u1doc_edit = callServicePostOvertime(_urlSetApproveOTShiftRotate, data_ovt_u1doc_edit);
                }


                getCountWaitDetailApproveOTAdminCreate();
                setActiveTab("docApprove", 0, int.Parse(ViewState["Vs_check_tab_noidx"].ToString()), 0, 0, 0, 0, 0);

                break;

            case "cmdSaveApprove":

                string[] arg_approve = new string[1];
                arg_approve = e.CommandArgument.ToString().Split(';');
                int _decision_idx_approve = int.Parse(arg_approve[0]);

                data_overtime data_ovt_u1doc_approve = new data_overtime();

                //update u1 document ot month
                var _u1doc_ot_approve = new ovt_u1doc_rotate_detail[GvWaitApprove.Rows.Count];
                int sum_approve_ = 0;
                int count_approve_ = 0;
                //string u0_sentmail = "";

                foreach (GridViewRow gv_row in GvWaitApprove.Rows)
                {

                    data_ovt_u1doc_approve.ovt_u1doc_rotate_list = new ovt_u1doc_rotate_detail[1];

                    CheckBox chk_ot_approve = (CheckBox)gv_row.FindControl("chk_ot_approve");
                    Label lbl_u1_doc_idx_approve = (Label)gv_row.FindControl("lbl_u1_doc_idx_approve");
                    Label lbl_u0_doc_idx_approve = (Label)gv_row.FindControl("lbl_u0_doc_idx_approve");

                    Label lbl_staidx_approve = (Label)gv_row.FindControl("lbl_staidx_approve");
                    Label lbl_m0_node_idx_approve = (Label)gv_row.FindControl("lbl_m0_node_idx_approve");
                    Label lbl_m0_actor_idx_approve = (Label)gv_row.FindControl("lbl_m0_actor_idx_approve");

                    TextBox txt_comment_head_approve = (TextBox)gv_row.FindControl("txt_comment_head_approve");
                    TextBox txt_remark_admin_edit = (TextBox)gv_row.FindControl("txt_remark_admin_edit");
                    TextBox txt_comment_hr_check = (TextBox)gv_row.FindControl("txt_comment_hr_check");

                    DropDownList ddl_timebefore_approve = (DropDownList)gv_row.FindControl("ddl_timebefore_approve");
                    DropDownList ddl_timeafter_approve = (DropDownList)gv_row.FindControl("ddl_timeafter_approve");
                    DropDownList ddl_timeholiday_approve = (DropDownList)gv_row.FindControl("ddl_timeholiday_approve");

                    Label lbl_hour_otbefore_approve = (Label)gv_row.FindControl("lbl_hour_otbefore_approve");
                    Label lbl_hour_otafter_approve = (Label)gv_row.FindControl("lbl_hour_otafter_approve");
                    Label lbl_hour_otholiday_approve = (Label)gv_row.FindControl("lbl_hour_otholiday_approve");

                    Label lbl_ddl_timestart_approve = (Label)gv_row.FindControl("lbl_ddl_timestart_approve");
                    Label lbl_ddl_timeend_approve = (Label)gv_row.FindControl("lbl_ddl_timeend_approve");


                    if (chk_ot_approve.Checked == true)
                    {

                        _u1doc_ot_approve[count_approve_] = new ovt_u1doc_rotate_detail();
                        //_u1doc_otmonth_approve[count_approve].emp_code = tbEmpCode_.Text;
                        _u1doc_ot_approve[count_approve_].u0_doc_idx = int.Parse(lbl_u0_doc_idx_approve.Text);
                        _u1doc_ot_approve[count_approve_].u1_doc_idx = int.Parse(lbl_u1_doc_idx_approve.Text);
                        _u1doc_ot_approve[count_approve_].m0_node_idx = int.Parse(lbl_m0_node_idx_approve.Text);
                        _u1doc_ot_approve[count_approve_].m0_actor_idx = int.Parse(lbl_m0_actor_idx_approve.Text);
                        _u1doc_ot_approve[count_approve_].staidx = int.Parse(lbl_staidx_approve.Text);
                        _u1doc_ot_approve[count_approve_].decision = _decision_idx_approve;
                        _u1doc_ot_approve[count_approve_].cemp_idx = _emp_idx;
                        _u1doc_ot_approve[count_approve_].comment_head = txt_comment_head_approve.Text;
                        _u1doc_ot_approve[count_approve_].comment_hr = txt_comment_hr_check.Text;
                        _u1doc_ot_approve[count_approve_].remark_admin = txt_remark_admin_edit.Text;

                        _u1doc_ot_approve[count_approve_].datetime_start = lbl_ddl_timestart_approve.Text;
                        _u1doc_ot_approve[count_approve_].datetime_end = lbl_ddl_timeend_approve.Text;

                        if (ddl_timebefore_approve.SelectedValue != "")
                        {
                            _u1doc_ot_approve[count_approve_].ot_before = ddl_timebefore_approve.SelectedItem.Text;
                            _u1doc_ot_approve[count_approve_].hour_ot_before = lbl_hour_otbefore_approve.Text;
                        }

                        if (ddl_timeafter_approve.SelectedValue != "")
                        {
                            _u1doc_ot_approve[count_approve_].ot_after = ddl_timeafter_approve.SelectedItem.Text;
                            _u1doc_ot_approve[count_approve_].hour_ot_after = lbl_hour_otafter_approve.Text;
                        }

                        if (ddl_timeholiday_approve.SelectedValue != "")
                        {
                            _u1doc_ot_approve[count_approve_].ot_holiday = ddl_timeholiday_approve.SelectedItem.Text;
                            _u1doc_ot_approve[count_approve_].hour_ot_holiday = lbl_hour_otholiday_approve.Text;
                        }


                        sum_approve_ = sum_approve_ + 1;
                        count_approve_++;

                    }

                }
                //update u1 document ot shift rotate
                if (sum_approve_ == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณาเลือกรายการ ก่อนทำการบันทึก ---');", true);
                    break;
                }
                else
                {
                    data_ovt_u1doc_approve.ovt_u1doc_rotate_list = _u1doc_ot_approve;
                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_ovt_u1doc_approve));
                    data_ovt_u1doc_approve = callServicePostOvertime(_urlSetApproveOTShiftRotate, data_ovt_u1doc_approve);
                }


                getCountWaitDetailApproveOTAdminCreate();

                if (int.Parse(ViewState["Vs_check_tab_noidx"].ToString()) == 13)
                {
                    //litDebug.Text = "2";
                    getWaitApproveDetailOTDayAdminCreate(GvWaitApprove, int.Parse(ViewState["Vs_check_tab_noidx"].ToString()), 2);
                    getSearchDetailWaitApprove();


                }
                else
                {
                    setActiveTab("docApprove", 0, int.Parse(ViewState["Vs_check_tab_noidx"].ToString()), 0, 0, 0, 0, 0);
                }

                //setActiveTab("docApprove", 0, int.Parse(ViewState["Vs_check_tab_noidx"].ToString()), 0, 0, 0, 0, 0);

                break;

            case "cmdUserSearch":

                TextBox txt_scanin_search = (TextBox)_PanelSearchDetailUser.FindControl("txt_scanin_search");
                DropDownList ddlStatus = (DropDownList)_PanelSearchDetailUser.FindControl("ddlStatus");

                data_overtime data_user_search_detail = new data_overtime();
                ovt_u1doc_rotate_detail user_search_detail = new ovt_u1doc_rotate_detail();
                data_user_search_detail.ovt_u1doc_rotate_list = new ovt_u1doc_rotate_detail[1];

                user_search_detail.emp_idx = _emp_idx;
                user_search_detail.datetime_start = txt_scanin_search.Text;
                user_search_detail.staidx = int.Parse(ddlStatus.SelectedValue);

                data_user_search_detail.ovt_u1doc_rotate_list[0] = user_search_detail;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_user_search_detail));
                data_user_search_detail = callServicePostOvertime(_urlGetUserSearchOTShiftRotate, data_user_search_detail);

                
                ViewState["Vs_DetailShiftRotateToUser"] = data_user_search_detail.ovt_u1doc_rotate_list;
                setGridData(GvDetailOTShiftRotate, ViewState["Vs_DetailShiftRotateToUser"]);

                break;
            case "cmdUserSearchRefresh":

                setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);

                break;
            case "cmdDocCancel":

                _PanelSearchDetailUser.Visible = true;

                TextBox txt_scanin_search_ = (TextBox)_PanelSearchDetailUser.FindControl("txt_scanin_search");
                DropDownList ddlStatus_ = (DropDownList)_PanelSearchDetailUser.FindControl("ddlStatus");


                //getStatusSearchDocument(ddlStatus_, int.Parse(ddlStatus_.SelectedValue));
                GvDetailOTShiftRotate.Visible = true;
                getDetailShiftRotateUser(GvDetailOTShiftRotate, 0);

                //set tab
                //set tab detail user
                setFormData(fvEmpCreateDetail, FormViewMode.ReadOnly, null);
                div_LogViewDetailOTShiftRotate.Visible = false;

                li4.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li5.Attributes.Add("class", "");

                //set tab


                data_overtime data_user_search_detail_ = new data_overtime();
                ovt_u1doc_rotate_detail user_search_detail_ = new ovt_u1doc_rotate_detail();
                data_user_search_detail_.ovt_u1doc_rotate_list = new ovt_u1doc_rotate_detail[1];

                user_search_detail_.emp_idx = _emp_idx;
                user_search_detail_.datetime_start = txt_scanin_search_.Text;
                user_search_detail_.staidx = int.Parse(ddlStatus_.SelectedValue);

                data_user_search_detail_.ovt_u1doc_rotate_list[0] = user_search_detail_;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_user_search_detail));
                data_user_search_detail_ = callServicePostOvertime(_urlGetUserSearchOTShiftRotate, data_user_search_detail_);


                ViewState["Vs_DetailShiftRotateToUser"] = data_user_search_detail_.ovt_u1doc_rotate_list;
                setGridData(GvDetailOTShiftRotate, ViewState["Vs_DetailShiftRotateToUser"]);



                //setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);
                break;

        }

    }
    //endbtn

    #endregion event command

    #region Changed
    protected void onSelectedCheckChanged(object sender, EventArgs e)
    {
        var cbName = (CheckBox)sender;

        //var chkName = (CheckBoxList)sender;

        switch (cbName.ID)
        {

            case "chkallApprove":
                int chack_all = 0;

                if (chkallApprove.Checked)
                {

                    foreach (GridViewRow row in GvWaitApprove.Rows)
                    {
                        CheckBox chk_ot_approve = (CheckBox)row.FindControl("chk_ot_approve");

                        chk_ot_approve.Checked = true;
                        chack_all++;
                    }
                }
                else
                {
                    foreach (GridViewRow row in GvWaitApprove.Rows)
                    {
                        CheckBox chk_ot_approve = (CheckBox)row.FindControl("chk_ot_approve");

                        chk_ot_approve.Checked = false;
                        chack_all++;
                    }
                }

                break;

            case "chkallCreate":

                int chkcreat_all = 0;

                if (chkallCreate.Checked)
                {

                    foreach (GridViewRow row in GvCreateShiftRotate.Rows)
                    {
                        CheckBox chk_otday = (CheckBox)row.FindControl("chk_otday");

                        chk_otday.Checked = true;
                        chkcreat_all++;
                    }
                }
                else
                {
                    foreach (GridViewRow row in GvCreateShiftRotate.Rows)
                    {
                        CheckBox chk_otday = (CheckBox)row.FindControl("chk_otday");

                        chk_otday.Checked = false;
                        chkcreat_all++;
                    }
                }

                break;
            case "chkall_edit":
                int chkalledit = 0;

                if (chkall_edit.Checked)
                {

                    foreach (GridViewRow row in GvWaitAdminEditDoc.Rows)
                    {
                        CheckBox chk_edit = (CheckBox)row.FindControl("chk_edit");

                        chk_edit.Checked = true;
                        chkalledit++;
                    }
                }
                else
                {
                    foreach (GridViewRow row in GvWaitAdminEditDoc.Rows)
                    {
                        CheckBox chk_edit = (CheckBox)row.FindControl("chk_edit");

                        chk_edit.Checked = false;
                        chkalledit++;
                    }
                }

                break;

        }
    }

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;
        //DropDownList ddlorg_day = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddlorg_rp");
        //DropDownList ddldep_day = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddldep_rp");
        //DropDownList ddlsec_day = (DropDownList)Fv_Search_Emp_Report_day.FindControl("ddlsec_rp");

        _Panel_Employee.Visible = false;

        switch (ddlName.ID)
        {

            case "ddl_timeholiday":

                GridView grid = sender as GridView;

                int RowIndex = ((GridViewRow)(((DropDownList)sender).Parent.Parent)).RowIndex;
                ////int RowIndex_Label = ((GridViewRow)(((Label)sender).Parent.Parent)).RowIndex;
                //// GridViewRow RowIndex = GvCreateShiftRotate.SelectedRow;

                DropDownList ddl_timeholiday = (DropDownList)GvCreateShiftRotate.Rows[RowIndex].FindControl("ddl_timeholiday");
                DropDownList ddl_timeafter = (DropDownList)GvCreateShiftRotate.Rows[RowIndex].FindControl("ddl_timeafter");
                DropDownList ddl_timebefore = (DropDownList)GvCreateShiftRotate.Rows[RowIndex].FindControl("ddl_timebefore");

                Label lbl_hour_otbefore = (Label)GvCreateShiftRotate.Rows[RowIndex].FindControl("lbl_hour_otbefore");
                Label lbl_hour_otafter = (Label)GvCreateShiftRotate.Rows[RowIndex].FindControl("lbl_hour_otafter");

                if (ddl_timeholiday.SelectedValue.ToString() == "8" && ddl_timeafter.SelectedValue != "")
                {
                    ddl_timeafter.Visible = true;
                    lbl_hour_otafter.Visible = true;

                }

                if (ddl_timeholiday.SelectedValue.ToString() == "8" && ddl_timebefore.SelectedValue != "")
                {

                    ddl_timebefore.Visible = true;
                    lbl_hour_otbefore.Visible = true;

                }


                break;

            case "ddlorgCreate":

                getDepartmentList(ddlrdeptCreate, int.Parse(ddlorgCreate.SelectedValue));

                ddlrsecCreate.Items.Clear();
                ddlrsecCreate.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));

                ddlrposCreate.Items.Clear();
                ddlrposCreate.Items.Insert(0, new ListItem("-- เลือกตำแหน่ง --", "0"));

                break;
            case "ddlrdeptCreate":

                getSectionList(ddlrsecCreate, int.Parse(ddlorgCreate.SelectedItem.Value), int.Parse(ddlrdeptCreate.SelectedItem.Value));

                ddlrposCreate.Items.Clear();
                ddlrposCreate.Items.Insert(0, new ListItem("-- เลือกตำแหน่ง --", "0"));

                break;
            case "ddlrsecCreate":

                getPositionList(ddlrposCreate, int.Parse(ddlorgCreate.SelectedItem.Value), int.Parse(ddlrdeptCreate.SelectedItem.Value), int.Parse(ddlrsecCreate.SelectedItem.Value));

                chkempCreate.Items.Clear();
                //ddlrposCreate.Items.Insert(0, new ListItem("-- เลือกตำแหน่ง --", "0"));

                break;

            case "ddlorgApprove":

                getDepartmentList(ddlrdeptApprove, int.Parse(ddlorgApprove.SelectedValue));

                ddlrsecApprove.Items.Clear();
                ddlrsecApprove.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));

                ddlrposApprove.Items.Clear();
                ddlrposApprove.Items.Insert(0, new ListItem("-- เลือกตำแหน่ง --", "0"));

                break;
            case "ddlrdeptApprove":

                getSectionList(ddlrsecApprove, int.Parse(ddlorgApprove.SelectedItem.Value), int.Parse(ddlrdeptApprove.SelectedItem.Value));

                ddlrposApprove.Items.Clear();
                ddlrposApprove.Items.Insert(0, new ListItem("-- เลือกตำแหน่ง --", "0"));

                break;
            case "ddlrsecApprove":

                getPositionList(ddlrposApprove, int.Parse(ddlorgApprove.SelectedItem.Value), int.Parse(ddlrdeptApprove.SelectedItem.Value), int.Parse(ddlrsecApprove.SelectedItem.Value));

                //chkempCreate.Items.Clear();
                //ddlrposCreate.Items.Insert(0, new ListItem("-- เลือกตำแหน่ง --", "0"));

                break;

            case "ddlrposCreate":

                //if (ddlrposCreate.SelectedValue != "0")
                //{

                //    getEmployeeList(chkempCreate, int.Parse(ddlorgCreate.SelectedItem.Value), int.Parse(ddlrdeptCreate.SelectedItem.Value), int.Parse(ddlrsecCreate.SelectedItem.Value), int.Parse(ddlrposCreate.SelectedItem.Value), _Panel_Employee);
                //}

                break;

            case "ddl_timestart":

                int count_ = 0;
                foreach (GridViewRow row in GvCreateShiftRotate.Rows)
                {
                    Label lbl_emp_code_otday = (Label)row.FindControl("lbl_emp_code_otday");
                    Label lbl_scan_in = (Label)row.FindControl("lbl_scan_in");
                    Label lbl_scan_out = (Label)row.FindControl("lbl_scan_out");

                    Label lbl_parttime_start_time = (Label)row.FindControl("lbl_parttime_start_time");
                    Label lbl_parttime_end_time = (Label)row.FindControl("lbl_parttime_end_time");

                    Label lbl_ot_before = (Label)row.FindControl("lbl_ot_before");
                    DropDownList ddl_timebefore_set = (DropDownList)row.FindControl("ddl_timebefore_set");
                    DropDownList ddl_timebefore_ = (DropDownList)row.FindControl("ddl_timebefore");

                    Label lbl_ot_after = (Label)row.FindControl("lbl_ot_after");
                    DropDownList ddl_timeafter_set = (DropDownList)row.FindControl("ddl_timeafter_set");
                    DropDownList ddl_timeafter_ = (DropDownList)row.FindControl("ddl_timeafter");

                    Label lbl_ot_holiday = (Label)row.FindControl("lbl_ot_holiday");
                    DropDownList ddl_timeholiday_set = (DropDownList)row.FindControl("ddl_timeholiday_set");
                    DropDownList ddl_timeholiday_ = (DropDownList)row.FindControl("ddl_timeholiday");

                    Label lbl_day_off = (Label)row.FindControl("lbl_day_off");
                    Label lbl_date_condition = (Label)row.FindControl("lbl_date_condition");
                    Label lbl_holiday_idx = (Label)row.FindControl("lbl_holiday_idx");

                    Label lbl_time_shiftstart = (Label)row.FindControl("lbl_time_shiftstart");
                    Label lbl_time_shiftend = (Label)row.FindControl("lbl_time_shiftend");

                    Label lbl_parttime_break_start_time = (Label)row.FindControl("lbl_parttime_break_start_time");
                    Label lbl_parttime_break_end_time = (Label)row.FindControl("lbl_parttime_break_end_time");

                    Label lbl_emp_type_idx = (Label)row.FindControl("lbl_emp_type_idx");

                    Label lbl_hour_otbefore_ = (Label)row.FindControl("lbl_hour_otbefore");
                    Label lbl_hour_otafter_ = (Label)row.FindControl("lbl_hour_otafter");
                    Label lbl_hour_otholiday = (Label)row.FindControl("lbl_hour_otholiday");

                    Label lbl_detailholiday = (Label)row.FindControl("lbl_detailholiday");
                    Label lbl_detaildayoff = (Label)row.FindControl("lbl_detaildayoff");

                    CheckBox chk_otday = (CheckBox)row.FindControl("chk_otday");
                    TextBox txt_remark_shift_otholiday = (TextBox)row.FindControl("txt_remark_shift_otholiday");

                    CheckBox chkallCreate = (CheckBox)GvCreateShiftRotate.HeaderRow.Cells[0].FindControl("chkallCreate");

                    DropDownList ddl_timestart = (DropDownList)row.FindControl("ddl_timestart");
                    DropDownList ddl_timeend = (DropDownList)row.FindControl("ddl_timeend");


                    IFormatProvider culture_ = new CultureInfo("en-US", true);


                    //day off employee
                    string day_off = lbl_day_off.Text;
                    string day_condition = lbl_date_condition.Text;
                    string[] set_test = day_off.Split(',');
                    string day_offemployee = "";

                    double value_otbefore;
                    double value_otafter;
                    DateTime d_finish_addhour_after = new DateTime();

                    var d_start = new DateTime();
                    var d_end = new DateTime();
                    var d_finish = new DateTime();
                    var d_workstart = new DateTime();

                    var DateTime_BreakStart = new DateTime();
                    var DateTime_BreakEnd = new DateTime();

                    double value_total_ot;
                    double value_total_otresult;

                    double value_total_ot_holiday;
                    double value_total_ot_before;
                    double value_total_ot_diff;

                    ////DateTime DateTime_BreakStart = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_start.Text, "dd/MM/yyyy HH:mm", culture);
                    ////DateTime DateTime_BreakEnd = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_end.Text, "dd/MM/yyyy HH:mm", culture);

                    d_workstart = DateTime.ParseExact((txtDateStart_create.Text + " " + lbl_parttime_start_time.Text), "dd/MM/yyyy HH:mm", culture_);
                    DateTime_BreakStart = DateTime.ParseExact((txtDateStart_create.Text + " " + lbl_parttime_break_start_time.Text), "dd/MM/yyyy HH:mm", culture_);
                    if (DateTime.ParseExact((lbl_parttime_start_time.Text), "HH:mm", culture_) < DateTime.ParseExact((lbl_parttime_end_time.Text), "HH:mm", culture_))
                    {

                        d_finish = DateTime.ParseExact((txtDateStart_create.Text + " " + lbl_parttime_end_time.Text), "dd/MM/yyyy HH:mm", culture_);
                        DateTime_BreakEnd = DateTime.ParseExact((txtDateStart_create.Text + " " + lbl_parttime_break_end_time.Text), "dd/MM/yyyy HH:mm", culture_);


                    }
                    else
                    {
                        //d_workstart = DateTime.ParseExact((DateTime.ParseExact((txtDateStart_create.Text), "dd/MM/yyyy", culture).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_start_time.Text), "dd/MM/yyyy HH:mm", culture);
                        d_finish = DateTime.ParseExact((DateTime.ParseExact((txtDateStart_create.Text), "dd/MM/yyyy", culture_).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_end_time.Text), "dd/MM/yyyy HH:mm", culture_);
                        DateTime_BreakEnd = DateTime.ParseExact(DateTime.ParseExact((txtDateStart_create.Text), "dd/MM/yyyy", culture_).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_break_end_time.Text, "dd/MM/yyyy HH:mm", culture_);

                    }

                    if (row.RowType == DataControlRowType.DataRow)
                    {

                        if (Array.IndexOf(set_test, day_condition.ToString()) > -1)
                        {
                            day_offemployee = "1"; //yes day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                                                   //litDebug.Text += "1" + "|";
                        }
                        else
                        {
                            day_offemployee = "2"; // no day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                        }
                        //day off employee

                        IFormatProvider culture = new CultureInfo("en-US", true);

                        //litDebug.Text += lbl_break_start.Text;

                        if (ddl_timeend.SelectedValue != "" && ddl_timestart.SelectedValue != "")
                        {


                            DateTime DateTime_ScanIn = DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_ScanOut = DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);


                            double value;
                            double _value_diffbreaktime;
                            double _value_timescan_late;
                            double _value_timescan_late_add;

                            double _value_otafter_holiday;

                            DateTime d_finish_addhour = new DateTime();
                            DateTime d_start_addhour = new DateTime();

                            DateTime d_finish_addhour_holiday = new DateTime();
                            double _value_diff_scanout;
                            /*
                            
                            litDebug1.Text += DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_).ToString("dd/MM/yyyy") + "|";
                            DateTime DateTime_BreakStart = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_finish.Text), "dd/MM/yyyy HH:mm", culture_);

                            */


                            //ot before
                            value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                            lbl_ot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                            //litDebug.Text = lbl_ot_before.Text;
                            getHourToOT(ddl_timebefore_, Convert.ToDouble(lbl_ot_before.Text), _before);

                            //ot after
                            if (DateTime_ScanOut > DateTime_ScanIn)
                            {

                                //litDebug.Text += DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");

                                d_finish_addhour_after = d_finish.AddMinutes(30);

                                //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                lbl_ot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                //litDebug.Text += value_otafter.ToString() + "|";

                                //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                getHourToOT(ddl_timeafter_, Convert.ToDouble(lbl_ot_after.Text), _after);
                            }
                            else
                            {
                                ddl_timeafter_.Visible = false;
                            }


                            //ot holiday
                            if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1")
                            {
                                //lbl_detailholiday.Visible = true;

                                //litDebug.Text = "test test";

                                //litDebug.Text += day_offemployee.ToString() + "|";
                                if (lbl_holiday_idx.Text != "0")
                                {
                                    lbl_detailholiday.Visible = true;
                                    lbl_detaildayoff.Visible = false;
                                }
                                else if (day_offemployee.ToString() == "1")
                                {
                                    lbl_detaildayoff.Visible = true;
                                    lbl_detailholiday.Visible = false;
                                }

                                //check case new
                                if ((DateTime_ScanIn < d_workstart || DateTime_ScanIn >= d_workstart) && DateTime_ScanOut >= d_finish)
                                {

                                    //_value_diffbreaktime = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;
                                    //_value_diff_scanout = DateTime_ScanOut.Subtract(DateTime_ShiftEnd).TotalMinutes;

                                    //ot before
                                    value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                                    lbl_ot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                    //litDebug.Text = lbl_ot_before.Text;
                                    getHourToOT(ddl_timebefore_, Convert.ToDouble(lbl_ot_before.Text), _before);

                                    //ot after
                                    if (DateTime_ScanOut > DateTime_ScanIn)
                                    {

                                        d_finish_addhour_after = d_finish.AddMinutes(30);

                                        //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                        //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                        value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                        lbl_ot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                                                                           //litDebug.Text += value_otafter.ToString() + "|";

                                        //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                        getHourToOT(ddl_timeafter_, Convert.ToDouble(lbl_ot_after.Text), _after);
                                    }
                                    else
                                    {
                                        ddl_timeafter_.Visible = false;
                                    }

                                    _value_diffbreaktime = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;
                                    _value_diff_scanout = DateTime_ScanOut.Subtract(d_finish).TotalMinutes;


                                    //litDebug1.Text = _value_diff_scanout.ToString() + "|";

                                    if (DateTime_ScanOut >= d_finish && DateTime_ScanIn < d_workstart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {
                                        value = Convert.ToDouble((Convert.ToDouble(d_finish.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50 - 1);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);

                                        if (ddl_timeholiday_.SelectedValue == "8")//check ot holiday have 8 hour
                                        {

                                            d_finish_addhour_holiday = d_finish.AddMinutes(30);

                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                            lbl_ot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter_, Convert.ToDouble(lbl_ot_after.Text), _after);


                                        }
                                    }
                                    else if (DateTime_ScanOut >= d_finish && DateTime_ScanIn > d_workstart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {

                                        //litDebug.Text = DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");

                                        _value_timescan_late = Convert.ToDouble((Convert.ToDouble(DateTime_ScanIn.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50);//DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes;
                                        _value_timescan_late_add = Convert.ToDouble(Math.Round(_value_timescan_late * 2, MidpointRounding.AwayFromZero) / 2);// _value_timescan_late.ToString();

                                        //litDebug.Text = _value_timescan_late_add.ToString();

                                        d_start_addhour = d_workstart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                        d_finish_addhour = d_finish.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                        // litDebug1.Text += d_start_addhour.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish_addhour.ToString("dd/MM/yyyy HH:mm");

                                        if (_value_diff_scanout >= 60)
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }
                                        else
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }

                                        //litDebug1.Text += value.ToString();
                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);

                                        if (ddl_timeholiday_.SelectedValue == "8")//check ot holiday have 8 hour
                                        {
                                            //litDebug.Text = d_finish_addhour_holiday.ToString();
                                            //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                            //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                            //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                            d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_).AddMinutes(30);
                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                            lbl_ot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter_, Convert.ToDouble(lbl_ot_after.Text), _after);

                                        }
                                        else
                                        {
                                            getHourToOT(ddl_timeafter_, Convert.ToDouble("0"), _after);
                                        }

                                    }
                                    else if (DateTime_ScanOut < d_finish && DateTime_ScanIn >= DateTime_BreakStart)
                                    {
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                    }
                                    else if (DateTime_ScanOut < d_finish && DateTime_ScanOut >= DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {

                                        //litDebug.Text = "32";
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        //value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                    }
                                    else //if (DateTime_ScanOut > DateTime_BreakEnd && DateTime_ScanOut < DateTime_ShiftEnd)
                                    {
                                        //litDebug.Text += DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|";
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                    }

                                }
                                else if (DateTime_ScanIn < d_workstart && DateTime_ScanOut < d_finish)
                                {

                                    //total ot
                                    value_total_ot = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 60));
                                    value_total_otresult = (Math.Floor(value_total_ot * 2) / 2);
                                    //total ot

                                    if (DateTime_ScanOut > DateTime_ScanIn)
                                    {
                                        //litDebug.Text = DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|" + DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");//DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_).ToString("dd/MM/yyyy") + "|";

                                        if (value_total_otresult >= 9.5) //set -1 breaktime
                                        {

                                            value_total_ot_diff = value_total_otresult - 1;


                                            value_total_ot_holiday = 8;
                                            value_total_ot_before = value_total_ot_diff - value_total_ot_holiday;

                                            //litDebug.Text = value_total_ot_before.ToString();
                                            getHourToOT(ddl_timeholiday_, Convert.ToDouble(value_total_ot_holiday.ToString()), _holiday);
                                            getHourToOT(ddl_timebefore_, Convert.ToDouble(value_total_ot_before.ToString()), _before);

                                        }
                                        else if (value_total_otresult == 9)
                                        {
                                            value_total_ot_diff = value_total_otresult - 1;
                                            value_total_ot_holiday = 8;
                                            getHourToOT(ddl_timeholiday_, Convert.ToDouble(value_total_ot_holiday.ToString()), _holiday);
                                        }
                                        else if (value_total_otresult >= 4 && value_total_otresult <= 5)
                                        {
                                            value_total_ot_before = 4;
                                            getHourToOT(ddl_timebefore_, Convert.ToDouble(value_total_ot_before.ToString()), _before);
                                        }
                                        else if (value_total_otresult > 5 && value_total_otresult <= 8)
                                        {
                                            value_total_ot_diff = value_total_otresult - 1;

                                            value_total_ot_before = value_total_ot_diff;
                                            getHourToOT(ddl_timebefore_, Convert.ToDouble(value_total_ot_before.ToString()), _before);
                                        }
                                    }

                                }
                                else if (DateTime_ScanOut <= DateTime_ScanIn)
                                {

                                    getHourToOT(ddl_timeholiday_, Convert.ToDouble("0"), _holiday);
                                    getHourToOT(ddl_timebefore_, Convert.ToDouble("0"), _before);

                                }
                            }
                            else
                            {
                                lbl_detailholiday.Visible = false;
                                lbl_detaildayoff.Visible = false;
                                ddl_timeholiday_.Visible = false;
                            }


                            //litDebug.Text += value_otbefore.ToString() + "|";
                        }

                    }

                    count_++;

                    //set ot x1, x1.5, x2, x3
                    //set ot before

                    if (ddl_timebefore_.SelectedValue != "" && (DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_) != DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_)))
                    {
                        //litDebug.Text = ddl_timeend.SelectedItem.ToString();


                        lbl_hour_otbefore_.Visible = true;
                        ddl_timebefore_.Visible = true;
                        if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                        {
                            if (ddl_timeholiday_.SelectedValue == "8")
                            {
                                lbl_hour_otbefore_.Text = "x3";
                            }
                            else
                            {
                                lbl_hour_otbefore_.Text = "x1";
                            }
                            //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                            //{
                            //    lbl_hour_otbefore.Text = "x1";
                            //}
                            //else
                            //{
                            //    lbl_hour_otbefore.Text = "x2";
                            //}
                        }
                        else
                        {
                            lbl_hour_otbefore_.Text = "x1.5";
                        }


                    }
                    else
                    {
                        lbl_hour_otbefore_.Visible = false;
                        ddl_timebefore_.Visible = false;
                    }

                    //set ot after
                    if (ddl_timeafter_.SelectedValue != "" && (DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_) != DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_)))
                    {


                        // litDebug.Text += ddl_timestart.SelectedItem.ToString() + "|" + ddl_timeend.SelectedItem.ToString();

                        lbl_hour_otafter_.Visible = true;
                        ddl_timeafter_.Visible = true;
                        if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                        {
                            if (ddl_timeholiday_.SelectedValue == "8")
                            {
                                lbl_hour_otafter_.Text = "x3";
                            }
                            else
                            {
                                lbl_hour_otafter_.Text = "x1";
                            }
                            //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                            //{
                            //    lbl_hour_otbefore.Text = "x1";
                            //}
                            //else
                            //{
                            //    lbl_hour_otbefore.Text = "x2";
                            //}
                        }
                        else
                        {
                            lbl_hour_otafter_.Text = "x1.5";
                        }



                    }
                    else
                    {
                        lbl_hour_otafter_.Visible = false;
                        ddl_timeafter_.Visible = false;
                    }

                    //set ot holiday
                    if (ddl_timeholiday_.SelectedValue != "" && (DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_) != DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_)))
                    {

                        lbl_hour_otholiday.Visible = true;
                        ddl_timeholiday_.Visible = true;
                        if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                        {
                            if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                            {
                                lbl_hour_otholiday.Text = "x1";
                            }
                            else
                            {
                                lbl_hour_otholiday.Text = "x2";
                            }
                        }

                    }
                    else
                    {
                        lbl_hour_otholiday.Visible = false;
                        ddl_timeholiday_.Visible = false;
                    }


                    //set condition checkbox
                    if (ddl_timebefore_.SelectedValue == "" && ddl_timeafter_.SelectedValue == "" && ddl_timeholiday_.SelectedValue == "")
                    {
                        chk_otday.Visible = false;
                        txt_remark_shift_otholiday.Visible = false;
                        chk_otday.Checked = false;
                    }
                    else
                    {
                        if ((DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_) != DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_))) //&& (ddl_timebefore.SelectedValue != "" && ddl_timeholiday.SelectedValue != "")
                        {
                            chk_otday.Visible = true;
                            txt_remark_shift_otholiday.Visible = true;
                            chk_otday.Checked = true;

                            chkallCreate.Enabled = true;
                            chkallCreate.Checked = true;

                        }
                        else
                        {
                            chk_otday.Visible = false;
                            txt_remark_shift_otholiday.Visible = false;
                            chk_otday.Checked = false;
                        }

                    }

                }

                break;

            case "ddl_timeend":

                int count_end = 0;
                foreach (GridViewRow row in GvCreateShiftRotate.Rows)
                {
                    Label lbl_emp_code_otday = (Label)row.FindControl("lbl_emp_code_otday");
                    Label lbl_scan_in = (Label)row.FindControl("lbl_scan_in");
                    Label lbl_scan_out = (Label)row.FindControl("lbl_scan_out");

                    Label lbl_parttime_start_time = (Label)row.FindControl("lbl_parttime_start_time");
                    Label lbl_parttime_end_time = (Label)row.FindControl("lbl_parttime_end_time");

                    Label lbl_ot_before = (Label)row.FindControl("lbl_ot_before");
                    DropDownList ddl_timebefore_set = (DropDownList)row.FindControl("ddl_timebefore_set");
                    DropDownList ddl_timebefore_ = (DropDownList)row.FindControl("ddl_timebefore");

                    Label lbl_ot_after = (Label)row.FindControl("lbl_ot_after");
                    DropDownList ddl_timeafter_set = (DropDownList)row.FindControl("ddl_timeafter_set");
                    DropDownList ddl_timeafter_ = (DropDownList)row.FindControl("ddl_timeafter");

                    Label lbl_ot_holiday = (Label)row.FindControl("lbl_ot_holiday");
                    DropDownList ddl_timeholiday_set = (DropDownList)row.FindControl("ddl_timeholiday_set");
                    DropDownList ddl_timeholiday_ = (DropDownList)row.FindControl("ddl_timeholiday");

                    Label lbl_day_off = (Label)row.FindControl("lbl_day_off");
                    Label lbl_date_condition = (Label)row.FindControl("lbl_date_condition");
                    Label lbl_holiday_idx = (Label)row.FindControl("lbl_holiday_idx");

                    Label lbl_time_shiftstart = (Label)row.FindControl("lbl_time_shiftstart");
                    Label lbl_time_shiftend = (Label)row.FindControl("lbl_time_shiftend");

                    Label lbl_parttime_break_start_time = (Label)row.FindControl("lbl_parttime_break_start_time");
                    Label lbl_parttime_break_end_time = (Label)row.FindControl("lbl_parttime_break_end_time");

                    Label lbl_emp_type_idx = (Label)row.FindControl("lbl_emp_type_idx");

                    Label lbl_hour_otbefore_ = (Label)row.FindControl("lbl_hour_otbefore");
                    Label lbl_hour_otafter_ = (Label)row.FindControl("lbl_hour_otafter");
                    Label lbl_hour_otholiday = (Label)row.FindControl("lbl_hour_otholiday");

                    Label lbl_detailholiday = (Label)row.FindControl("lbl_detailholiday");
                    Label lbl_detaildayoff = (Label)row.FindControl("lbl_detaildayoff");

                    CheckBox chk_otday = (CheckBox)row.FindControl("chk_otday");
                    TextBox txt_remark_shift_otholiday = (TextBox)row.FindControl("txt_remark_shift_otholiday");

                    CheckBox chkallCreate = (CheckBox)GvCreateShiftRotate.HeaderRow.Cells[0].FindControl("chkallCreate");

                    DropDownList ddl_timestart = (DropDownList)row.FindControl("ddl_timestart");
                    DropDownList ddl_timeend = (DropDownList)row.FindControl("ddl_timeend");

                    IFormatProvider culture_ = new CultureInfo("en-US", true);


                    //day off employee
                    string day_off = lbl_day_off.Text;
                    string day_condition = lbl_date_condition.Text;
                    string[] set_test = day_off.Split(',');
                    string day_offemployee = "";

                    double value_otbefore;
                    double value_otafter;
                    DateTime d_finish_addhour_after = new DateTime();

                    var d_start = new DateTime();
                    var d_end = new DateTime();
                    var d_finish = new DateTime();
                    var d_workstart = new DateTime();

                    var DateTime_BreakStart = new DateTime();
                    var DateTime_BreakEnd = new DateTime();

                    double value_total_ot;
                    double value_total_otresult;

                    double value_total_ot_holiday;
                    double value_total_ot_before;
                    double value_total_ot_diff;

                    ////DateTime DateTime_BreakStart = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_start.Text, "dd/MM/yyyy HH:mm", culture);
                    ////DateTime DateTime_BreakEnd = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_end.Text, "dd/MM/yyyy HH:mm", culture);

                    d_workstart = DateTime.ParseExact((txtDateStart_create.Text + " " + lbl_parttime_start_time.Text), "dd/MM/yyyy HH:mm", culture_);
                    DateTime_BreakStart = DateTime.ParseExact((txtDateStart_create.Text + " " + lbl_parttime_break_start_time.Text), "dd/MM/yyyy HH:mm", culture_);
                    if (DateTime.ParseExact((lbl_parttime_start_time.Text), "HH:mm", culture_) < DateTime.ParseExact((lbl_parttime_end_time.Text), "HH:mm", culture_))
                    {

                        d_finish = DateTime.ParseExact((txtDateStart_create.Text + " " + lbl_parttime_end_time.Text), "dd/MM/yyyy HH:mm", culture_);
                        DateTime_BreakEnd = DateTime.ParseExact((txtDateStart_create.Text + " " + lbl_parttime_break_end_time.Text), "dd/MM/yyyy HH:mm", culture_);


                    }
                    else
                    {
                        //d_workstart = DateTime.ParseExact((DateTime.ParseExact((txtDateStart_create.Text), "dd/MM/yyyy", culture).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_start_time.Text), "dd/MM/yyyy HH:mm", culture);
                        d_finish = DateTime.ParseExact((DateTime.ParseExact((txtDateStart_create.Text), "dd/MM/yyyy", culture_).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_end_time.Text), "dd/MM/yyyy HH:mm", culture_);
                        DateTime_BreakEnd = DateTime.ParseExact(DateTime.ParseExact((txtDateStart_create.Text), "dd/MM/yyyy", culture_).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_break_end_time.Text, "dd/MM/yyyy HH:mm", culture_);

                    }

                    if (row.RowType == DataControlRowType.DataRow)
                    {

                        if (Array.IndexOf(set_test, day_condition.ToString()) > -1)
                        {
                            day_offemployee = "1"; //yes day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                                                   //litDebug.Text += "1" + "|";
                        }
                        else
                        {
                            day_offemployee = "2"; // no day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                        }
                        //day off employee

                        IFormatProvider culture = new CultureInfo("en-US", true);

                        //litDebug.Text += lbl_break_start.Text;

                        if (ddl_timeend.SelectedValue != "" && ddl_timestart.SelectedValue != "")
                        {


                            DateTime DateTime_ScanIn = DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_ScanOut = DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);


                            double value;
                            double _value_diffbreaktime;
                            double _value_timescan_late;
                            double _value_timescan_late_add;

                            double _value_otafter_holiday;

                            DateTime d_finish_addhour = new DateTime();
                            DateTime d_start_addhour = new DateTime();

                            DateTime d_finish_addhour_holiday = new DateTime();
                            double _value_diff_scanout;
                            /*
                            
                            litDebug1.Text += DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_).ToString("dd/MM/yyyy") + "|";
                            DateTime DateTime_BreakStart = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_finish.Text), "dd/MM/yyyy HH:mm", culture_);

                            */

                            //ot before
                            value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                            lbl_ot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                            //litDebug.Text = lbl_ot_before.Text;
                            getHourToOT(ddl_timebefore_, Convert.ToDouble(lbl_ot_before.Text), _before);

                            //ot after
                            if (DateTime_ScanOut > DateTime_ScanIn)
                            {
                                //ddl_timeafter_.Visible = true;

                                //litDebug.Text = "1";
                                d_finish_addhour_after = d_finish.AddMinutes(30);

                                //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                lbl_ot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                ////litDebug.Text += value_otafter.ToString() + "|" + DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_).ToString("dd/MM/yyyy HH:mm");

                                //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                getHourToOT(ddl_timeafter_, Convert.ToDouble(lbl_ot_after.Text), _after);
                            }
                            else
                            {


                                //litDebug1.Text = DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_).ToString("dd/MM/yyyy HH:mm");
                                ddl_timeafter_.Visible = false;
                                lbl_hour_otafter_.Visible = false;
                            }

                            //litDebug.Text = day_offemployee.ToString();

                            //ot holiday
                            if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1")
                            {
                                //lbl_detailholiday.Visible = true;


                                //litDebug.Text += day_offemployee.ToString() + "|";
                                if (lbl_holiday_idx.Text != "0")
                                {
                                    lbl_detailholiday.Visible = true;
                                    lbl_detaildayoff.Visible = false;
                                }
                                else if (day_offemployee.ToString() == "1")
                                {
                                    lbl_detaildayoff.Visible = true;
                                    lbl_detailholiday.Visible = false;
                                }

                                //if (DateTime_ScanIn < d_workstart || DateTime_ScanIn > d_workstart)
                                if ((DateTime_ScanIn <= d_workstart || DateTime_ScanIn >= d_workstart) && DateTime_ScanOut >= d_finish)
                                {

                                    //_value_diffbreaktime = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;
                                    //_value_diff_scanout = DateTime_ScanOut.Subtract(DateTime_ShiftEnd).TotalMinutes;

                                    //ot before
                                    value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                                    lbl_ot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                    //litDebug.Text = lbl_ot_before.Text;
                                    getHourToOT(ddl_timebefore_, Convert.ToDouble(lbl_ot_before.Text), _before);

                                    //ot after
                                    if (DateTime_ScanOut > DateTime_ScanIn)
                                    {

                                        d_finish_addhour_after = d_finish.AddMinutes(30);

                                        //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                        //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                        value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                        lbl_ot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                                                                           //litDebug.Text += value_otafter.ToString() + "|";

                                        //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                        getHourToOT(ddl_timeafter_, Convert.ToDouble(lbl_ot_after.Text), _after);
                                    }
                                    else
                                    {
                                        ddl_timeafter_.Visible = false;
                                    }

                                    _value_diffbreaktime = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;
                                    _value_diff_scanout = DateTime_ScanOut.Subtract(d_finish).TotalMinutes;


                                    //litDebug1.Text = _value_diff_scanout.ToString() + "|";

                                    if (DateTime_ScanOut >= d_finish && DateTime_ScanIn <= d_workstart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {
                                        //litDebug.Text += "8";

                                        value = Convert.ToDouble((Convert.ToDouble(d_finish.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50 - 1);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);

                                        if (ddl_timeholiday_.SelectedValue == "8")//check ot holiday have 8 hour
                                        {

                                            d_finish_addhour_holiday = d_finish.AddMinutes(30);

                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                            lbl_ot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter_, Convert.ToDouble(lbl_ot_after.Text), _after);


                                        }
                                    }
                                    else if (DateTime_ScanOut >= d_finish && DateTime_ScanIn > d_workstart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {

                                        //litDebug.Text = DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");

                                        _value_timescan_late = Convert.ToDouble((Convert.ToDouble(DateTime_ScanIn.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50);//DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes;
                                        _value_timescan_late_add = Convert.ToDouble(Math.Round(_value_timescan_late * 2, MidpointRounding.AwayFromZero) / 2);// _value_timescan_late.ToString();

                                        //litDebug.Text = _value_timescan_late_add.ToString();

                                        d_start_addhour = d_workstart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                        d_finish_addhour = d_finish.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                        // litDebug1.Text += d_start_addhour.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish_addhour.ToString("dd/MM/yyyy HH:mm");

                                        if (_value_diff_scanout >= 60)
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }
                                        else
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }

                                        //litDebug1.Text += value.ToString();
                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);

                                        if (ddl_timeholiday_.SelectedValue == "8")//check ot holiday have 8 hour
                                        {
                                            //litDebug.Text = d_finish_addhour_holiday.ToString();
                                            //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                            //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                            //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                            d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_).AddMinutes(30);
                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                            lbl_ot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter_, Convert.ToDouble(lbl_ot_after.Text), _after);

                                        }
                                        else
                                        {
                                            getHourToOT(ddl_timeafter_, Convert.ToDouble("0"), _after);
                                        }

                                    }
                                    else if (DateTime_ScanOut < d_finish && DateTime_ScanIn >= DateTime_BreakStart)
                                    {
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                    }
                                    else if (DateTime_ScanOut < d_finish && DateTime_ScanOut >= DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {

                                        //litDebug.Text = "32";
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        //value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                    }
                                    else //if (DateTime_ScanOut > DateTime_BreakEnd && DateTime_ScanOut < DateTime_ShiftEnd)
                                    {
                                        //litDebug.Text += DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|";
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                    }

                                }
                                else if (DateTime_ScanIn < d_workstart && DateTime_ScanOut < d_finish)
                                {

                                    //total ot
                                    value_total_ot = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 60));
                                    value_total_otresult = (Math.Floor(value_total_ot * 2) / 2);
                                    //total ot

                                    if (DateTime_ScanOut > DateTime_ScanIn)
                                    {
                                        //litDebug.Text = DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|" + DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");//DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_).ToString("dd/MM/yyyy") + "|";

                                        if (value_total_otresult >= 9.5) //set -1 breaktime
                                        {

                                            value_total_ot_diff = value_total_otresult - 1;


                                            value_total_ot_holiday = 8;
                                            value_total_ot_before = value_total_ot_diff - value_total_ot_holiday;

                                            //litDebug.Text = value_total_ot_before.ToString();
                                            getHourToOT(ddl_timeholiday_, Convert.ToDouble(value_total_ot_holiday.ToString()), _holiday);
                                            getHourToOT(ddl_timebefore_, Convert.ToDouble(value_total_ot_before.ToString()), _before);

                                        }
                                        else if (value_total_otresult == 9)
                                        {
                                            value_total_ot_diff = value_total_otresult - 1;
                                            value_total_ot_holiday = 8;
                                            getHourToOT(ddl_timeholiday_, Convert.ToDouble(value_total_ot_holiday.ToString()), _holiday);
                                        }
                                        else if (value_total_otresult >= 4 && value_total_otresult <= 5)
                                        {
                                            value_total_ot_before = 4;
                                            getHourToOT(ddl_timebefore_, Convert.ToDouble(value_total_ot_before.ToString()), _before);
                                        }
                                        else if (value_total_otresult > 5 && value_total_otresult <= 8)
                                        {
                                            value_total_ot_diff = value_total_otresult - 1;

                                            value_total_ot_before = value_total_ot_diff;
                                            getHourToOT(ddl_timebefore_, Convert.ToDouble(value_total_ot_before.ToString()), _before);
                                        }
                                    }

                                }
                                else if (DateTime_ScanOut <= DateTime_ScanIn)
                                {

                                    getHourToOT(ddl_timeholiday_, Convert.ToDouble("0"), _holiday);
                                    getHourToOT(ddl_timebefore_, Convert.ToDouble("0"), _before);

                                }

                            }
                            else
                            {
                                lbl_detailholiday.Visible = false;
                                lbl_detaildayoff.Visible = false;
                                ddl_timeholiday_.Visible = false;
                            }


                            //litDebug.Text += value_otbefore.ToString() + "|";



                        }

                    }

                    count_end++;


                    //set ot x1, x1.5, x2, x3
                    //set ot before

                    if (ddl_timebefore_.SelectedValue != "" && (DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_) != DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_)))
                    {
                        //litDebug.Text = ddl_timeend.SelectedItem.ToString();


                        lbl_hour_otbefore_.Visible = true;
                        ddl_timebefore_.Visible = true;
                        if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                        {
                            if (ddl_timeholiday_.SelectedValue == "8")
                            {
                                lbl_hour_otbefore_.Text = "x3";
                            }
                            else
                            {
                                lbl_hour_otbefore_.Text = "x1";
                            }
                            //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                            //{
                            //    lbl_hour_otbefore.Text = "x1";
                            //}
                            //else
                            //{
                            //    lbl_hour_otbefore.Text = "x2";
                            //}
                        }
                        else
                        {
                            lbl_hour_otbefore_.Text = "x1.5";
                        }


                    }
                    else
                    {
                        lbl_hour_otbefore_.Visible = false;
                        ddl_timebefore_.Visible = false;
                    }

                    //set ot after
                    if (ddl_timeafter_.SelectedValue != "" && (DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_) != DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_)))
                    {


                        // litDebug.Text += ddl_timestart.SelectedItem.ToString() + "|" + ddl_timeend.SelectedItem.ToString();

                        lbl_hour_otafter_.Visible = true;
                        ddl_timeafter_.Visible = true;
                        if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                        {
                            if (ddl_timeholiday_.SelectedValue == "8")
                            {
                                lbl_hour_otafter_.Text = "x3";
                            }
                            else
                            {
                                lbl_hour_otafter_.Text = "x1";
                            }
                            //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                            //{
                            //    lbl_hour_otbefore.Text = "x1";
                            //}
                            //else
                            //{
                            //    lbl_hour_otbefore.Text = "x2";
                            //}
                        }
                        else
                        {
                            lbl_hour_otafter_.Text = "x1.5";
                        }



                    }
                    else
                    {
                        lbl_hour_otafter_.Visible = false;
                        ddl_timeafter_.Visible = false;
                    }

                    //set ot holiday
                    if (ddl_timeholiday_.SelectedValue != "" && (DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_) != DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_)))
                    {

                        lbl_hour_otholiday.Visible = true;
                        ddl_timeholiday_.Visible = true;
                        if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                        {
                            if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                            {
                                lbl_hour_otholiday.Text = "x1";
                            }
                            else
                            {
                                lbl_hour_otholiday.Text = "x2";
                            }
                        }

                    }
                    else
                    {
                        lbl_hour_otholiday.Visible = false;
                        ddl_timeholiday_.Visible = false;
                    }


                    //set condition checkbox
                    if (ddl_timebefore_.SelectedValue == "" && ddl_timeafter_.SelectedValue == "" && ddl_timeholiday_.SelectedValue == "")
                    {

                        chk_otday.Visible = false;
                        txt_remark_shift_otholiday.Visible = false;
                        chk_otday.Checked = false;
                    }
                    else
                    {
                        if ((DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_) != DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_))) //&& (ddl_timebefore.SelectedValue != "" && ddl_timeholiday.SelectedValue != "")
                        {
                            //litDebug.Text = "1";
                            chk_otday.Visible = true;
                            txt_remark_shift_otholiday.Visible = true;
                            chk_otday.Checked = true;

                            chkallCreate.Enabled = true;
                            chkallCreate.Checked = true;

                        }
                        else
                        {
                            //litDebug.Text = "2";
                            chk_otday.Visible = false;
                            txt_remark_shift_otholiday.Visible = false;
                            chk_otday.Checked = false;
                        }

                    }

                }

                break;

            case "ddl_timestart_edit":

                int count_edit = 0;
                foreach (GridViewRow row in GvWaitAdminEditDoc.Rows)
                {
                    Label lbl_emp_code_otday = (Label)row.FindControl("lbl_emp_code_otday_edit");
                    //Label lbl_scan_in = (Label)row.FindControl("lbl_scan_in");
                    //Label lbl_scan_out = (Label)row.FindControl("lbl_scan_out");

                    Label lbl_parttime_start_time = (Label)row.FindControl("lbl_parttime_start_time_edit");
                    Label lbl_parttime_end_time = (Label)row.FindControl("lbl_parttime_end_time_edit");

                    Label lbl_ot_before = (Label)row.FindControl("lbl_ot_before_edit");
                    DropDownList ddl_timebefore_ = (DropDownList)row.FindControl("ddl_timebefore_edit");

                    Label lbl_ot_after = (Label)row.FindControl("lbl_ot_after_edit");
                    DropDownList ddl_timeafter_ = (DropDownList)row.FindControl("ddl_timeafter_edit");

                    Label lbl_ot_holiday = (Label)row.FindControl("lbl_ot_holiday_edit");
                    DropDownList ddl_timeholiday_ = (DropDownList)row.FindControl("ddl_timeholiday_edit");

                    Label lbl_day_off = (Label)row.FindControl("lbl_day_off_edit");
                    Label lbl_date_condition = (Label)row.FindControl("lbl_date_condition_edit");
                    Label lbl_holiday_idx = (Label)row.FindControl("lbl_holiday_idx_edit");

                    //Label lbl_time_shiftstart = (Label)row.FindControl("lbl_time_shiftstart");
                    //Label lbl_time_shiftend = (Label)row.FindControl("lbl_time_shiftend");

                    Label lbl_parttime_break_start_time = (Label)row.FindControl("lbl_parttime_break_start_time_edit");
                    Label lbl_parttime_break_end_time = (Label)row.FindControl("lbl_parttime_break_end_time_edit");

                    Label lbl_emp_type_idx = (Label)row.FindControl("lbl_emp_type_idx_edit");

                    Label lbl_hour_otbefore_ = (Label)row.FindControl("lbl_hour_otbefore_edit");
                    Label lbl_hour_otafter_ = (Label)row.FindControl("lbl_hour_otafter_edit");
                    Label lbl_hour_otholiday = (Label)row.FindControl("lbl_hour_otholiday_edit");

                    Label lbl_detailholiday = (Label)row.FindControl("lbl_detailholiday_edit");
                    Label lbl_detaildayoff = (Label)row.FindControl("lbl_detaildayoff_edit");

                    CheckBox chk_edit = (CheckBox)row.FindControl("chk_edit");
                    //TextBox txt_remark_shift_otholiday = (TextBox)row.FindControl("txt_remark_shift_otholiday");

                    CheckBox chkall_edit = (CheckBox)GvWaitAdminEditDoc.HeaderRow.Cells[0].FindControl("chkall_edit");

                    DropDownList ddl_timestart = (DropDownList)row.FindControl("ddl_timestart_edit");
                    DropDownList ddl_timeend = (DropDownList)row.FindControl("ddl_timeend_edit");
                    Label lbl_datetime_otstart_edit = (Label)row.FindControl("lbl_datetime_otstart_edit");

                    IFormatProvider culture_ = new CultureInfo("en-US", true);


                    //day off employee
                    string day_off = lbl_day_off.Text;
                    string day_condition = lbl_date_condition.Text;
                    string[] set_test = day_off.Split(',');
                    string day_offemployee = "";

                    double value_otbefore;
                    double value_otafter;
                    DateTime d_finish_addhour_after = new DateTime();

                    var d_start = new DateTime();
                    var d_end = new DateTime();
                    var d_finish = new DateTime();
                    var d_workstart = new DateTime();

                    var DateTime_BreakStart = new DateTime();
                    var DateTime_BreakEnd = new DateTime();

                    double value_total_ot;
                    double value_total_otresult;

                    double value_total_ot_holiday;
                    double value_total_ot_before;
                    double value_total_ot_diff;

                    ////DateTime DateTime_BreakStart = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_start.Text, "dd/MM/yyyy HH:mm", culture);
                    ////DateTime DateTime_BreakEnd = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_end.Text, "dd/MM/yyyy HH:mm", culture);

                    d_workstart = DateTime.ParseExact((lbl_datetime_otstart_edit.Text + " " + lbl_parttime_start_time.Text), "dd/MM/yyyy HH:mm", culture_);
                    DateTime_BreakStart = DateTime.ParseExact((lbl_datetime_otstart_edit.Text + " " + lbl_parttime_break_start_time.Text), "dd/MM/yyyy HH:mm", culture_);
                    if (DateTime.ParseExact((lbl_parttime_start_time.Text), "HH:mm", culture_) < DateTime.ParseExact((lbl_parttime_end_time.Text), "HH:mm", culture_))
                    {

                        d_finish = DateTime.ParseExact((lbl_datetime_otstart_edit.Text + " " + lbl_parttime_end_time.Text), "dd/MM/yyyy HH:mm", culture_);
                        DateTime_BreakEnd = DateTime.ParseExact((lbl_datetime_otstart_edit.Text + " " + lbl_parttime_break_end_time.Text), "dd/MM/yyyy HH:mm", culture_);


                    }
                    else
                    {
                        //d_workstart = DateTime.ParseExact((DateTime.ParseExact((txtDateStart_create.Text), "dd/MM/yyyy", culture).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_start_time.Text), "dd/MM/yyyy HH:mm", culture);
                        d_finish = DateTime.ParseExact((DateTime.ParseExact((lbl_datetime_otstart_edit.Text), "dd/MM/yyyy", culture_).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_end_time.Text), "dd/MM/yyyy HH:mm", culture_);
                        DateTime_BreakEnd = DateTime.ParseExact(DateTime.ParseExact((lbl_datetime_otstart_edit.Text), "dd/MM/yyyy", culture_).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_break_end_time.Text, "dd/MM/yyyy HH:mm", culture_);

                    }

                    if (row.RowType == DataControlRowType.DataRow)
                    {

                        if (Array.IndexOf(set_test, day_condition.ToString()) > -1)
                        {
                            day_offemployee = "1"; //yes day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                                                   //litDebug.Text += "1" + "|";
                        }
                        else
                        {
                            day_offemployee = "2"; // no day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                        }
                        //day off employee

                        IFormatProvider culture = new CultureInfo("en-US", true);

                        //litDebug.Text += lbl_break_start.Text;

                        if (ddl_timeend.SelectedValue != "" && ddl_timestart.SelectedValue != "")
                        {


                            DateTime DateTime_ScanIn = DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_ScanOut = DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);


                            double value;
                            double _value_diffbreaktime;
                            double _value_timescan_late;
                            double _value_timescan_late_add;

                            double _value_otafter_holiday;

                            DateTime d_finish_addhour = new DateTime();
                            DateTime d_start_addhour = new DateTime();

                            DateTime d_finish_addhour_holiday = new DateTime();
                            double _value_diff_scanout;
                            /*
                            
                            litDebug1.Text += DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_).ToString("dd/MM/yyyy") + "|";
                            DateTime DateTime_BreakStart = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_finish.Text), "dd/MM/yyyy HH:mm", culture_);

                            */

                            //ot before
                            value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                            lbl_ot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                            //litDebug.Text = lbl_ot_before.Text;
                            getHourToOT(ddl_timebefore_, Convert.ToDouble(lbl_ot_before.Text), _before);

                            //ot after
                            if (DateTime_ScanOut > DateTime_ScanIn)
                            {

                                d_finish_addhour_after = d_finish.AddMinutes(30);

                                //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                lbl_ot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                //litDebug.Text += value_otafter.ToString() + "|";

                                //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                getHourToOT(ddl_timeafter_, Convert.ToDouble(lbl_ot_after.Text), _after);
                            }
                            else
                            {
                                ddl_timeafter_.Visible = false;
                            }


                            //ot holiday
                            if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1")
                            {
                                //lbl_detailholiday.Visible = true;


                                //litDebug.Text += day_offemployee.ToString() + "|";
                                if (lbl_holiday_idx.Text != "0")
                                {
                                    lbl_detailholiday.Visible = true;
                                    lbl_detaildayoff.Visible = false;
                                }
                                else if (day_offemployee.ToString() == "1")
                                {
                                    lbl_detaildayoff.Visible = true;
                                    lbl_detailholiday.Visible = false;
                                }

                                if ((DateTime_ScanIn < d_workstart || DateTime_ScanIn >= d_workstart) && DateTime_ScanOut >= d_finish)
                                {

                                    //ot before
                                    value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                                    lbl_ot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                    //litDebug.Text = lbl_ot_before.Text;
                                    getHourToOT(ddl_timebefore_, Convert.ToDouble(lbl_ot_before.Text), _before);

                                    //ot after
                                    if (DateTime_ScanOut > DateTime_ScanIn)
                                    {

                                        d_finish_addhour_after = d_finish.AddMinutes(30);

                                        //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                        //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                        value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                        lbl_ot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                                                                           //litDebug.Text += value_otafter.ToString() + "|";

                                        //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                        getHourToOT(ddl_timeafter_, Convert.ToDouble(lbl_ot_after.Text), _after);
                                    }
                                    else
                                    {
                                        ddl_timeafter_.Visible = false;
                                    }

                                    //_value_diffbreaktime = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;
                                    //_value_diff_scanout = DateTime_ScanOut.Subtract(DateTime_ShiftEnd).TotalMinutes;

                                    _value_diffbreaktime = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;
                                    _value_diff_scanout = DateTime_ScanOut.Subtract(d_finish).TotalMinutes;


                                    //litDebug1.Text = _value_diff_scanout.ToString() + "|";

                                    if (DateTime_ScanOut >= d_finish && DateTime_ScanIn < d_workstart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {
                                        value = Convert.ToDouble((Convert.ToDouble(d_finish.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50 - 1);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);

                                        if (ddl_timeholiday_.SelectedValue == "8")//check ot holiday have 8 hour
                                        {

                                            d_finish_addhour_holiday = d_finish.AddMinutes(30);

                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                            lbl_ot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter_, Convert.ToDouble(lbl_ot_after.Text), _after);


                                        }
                                    }
                                    else if (DateTime_ScanOut >= d_finish && DateTime_ScanIn > d_workstart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {

                                        //litDebug.Text = DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");

                                        _value_timescan_late = Convert.ToDouble((Convert.ToDouble(DateTime_ScanIn.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50);//DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes;
                                        _value_timescan_late_add = Convert.ToDouble(Math.Round(_value_timescan_late * 2, MidpointRounding.AwayFromZero) / 2);// _value_timescan_late.ToString();

                                        //litDebug.Text = _value_timescan_late_add.ToString();

                                        d_start_addhour = d_workstart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                        d_finish_addhour = d_finish.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                        // litDebug1.Text += d_start_addhour.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish_addhour.ToString("dd/MM/yyyy HH:mm");

                                        if (_value_diff_scanout >= 60)
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }
                                        else
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }

                                        //litDebug1.Text += value.ToString();
                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);

                                        if (ddl_timeholiday_.SelectedValue == "8")//check ot holiday have 8 hour
                                        {
                                            //litDebug.Text = d_finish_addhour_holiday.ToString();
                                            //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                            //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                            //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                            d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_).AddMinutes(30);
                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                            lbl_ot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter_, Convert.ToDouble(lbl_ot_after.Text), _after);

                                        }
                                        else
                                        {
                                            getHourToOT(ddl_timeafter_, Convert.ToDouble("0"), _after);
                                        }

                                    }
                                    else if (DateTime_ScanOut < d_finish && DateTime_ScanIn >= DateTime_BreakStart)
                                    {
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                    }
                                    else if (DateTime_ScanOut < d_finish && DateTime_ScanOut >= DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {

                                        //litDebug.Text = "32";
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        //value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                    }
                                    else //if (DateTime_ScanOut > DateTime_BreakEnd && DateTime_ScanOut < DateTime_ShiftEnd)
                                    {
                                        //litDebug.Text += DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|";
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                    }

                                }
                                else if (DateTime_ScanIn < d_workstart && DateTime_ScanOut < d_finish)
                                {

                                    //litDebug1.Text = "have case new";

                                    //total ot
                                    value_total_ot = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 60));
                                    value_total_otresult = (Math.Floor(value_total_ot * 2) / 2);
                                    //total ot

                                    if (value_total_otresult >= 9.5) //set -1 breaktime
                                    {

                                        value_total_ot_diff = value_total_otresult - 1;


                                        value_total_ot_holiday = 8;
                                        value_total_ot_before = value_total_ot_diff - value_total_ot_holiday;

                                        //litDebug.Text = value_total_ot_before.ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(value_total_ot_holiday.ToString()), _holiday);
                                        getHourToOT(ddl_timebefore_, Convert.ToDouble(value_total_ot_before.ToString()), _before);

                                    }
                                    else if (value_total_otresult == 9)
                                    {
                                        value_total_ot_diff = value_total_otresult - 1;
                                        value_total_ot_holiday = 8;
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(value_total_ot_holiday.ToString()), _holiday);
                                    }
                                    else if (value_total_otresult >= 4 && value_total_otresult <= 5)
                                    {
                                        value_total_ot_before = 4;
                                        getHourToOT(ddl_timebefore_, Convert.ToDouble(value_total_ot_before.ToString()), _before);
                                    }
                                    else if (value_total_otresult > 5 && value_total_otresult <= 8)
                                    {
                                        value_total_ot_diff = value_total_otresult - 1;

                                        value_total_ot_before = value_total_ot_diff;
                                        getHourToOT(ddl_timebefore_, Convert.ToDouble(value_total_ot_before.ToString()), _before);
                                    }

                                }

                            }
                            else
                            {
                                lbl_detailholiday.Visible = false;
                                lbl_detaildayoff.Visible = false;
                                ddl_timeholiday_.Visible = false;
                            }


                            //litDebug.Text += value_otbefore.ToString() + "|";
                        }

                    }

                    count_edit++;



                    //set ot x1, x1.5, x2, x3
                    //set ot before
                    if (ddl_timebefore_.SelectedValue != "")
                    {

                        //lbl_hour_otbefore_.Visible = true;
                        //ddl_timebefore_.Visible = true;
                        if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                        {
                            if (ddl_timeholiday_.SelectedValue == "8")
                            {
                                lbl_hour_otbefore_.Text = "x3";
                            }
                            else
                            {
                                lbl_hour_otbefore_.Text = "x1";
                            }
                            //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                            //{
                            //    lbl_hour_otbefore.Text = "x1";
                            //}
                            //else
                            //{
                            //    lbl_hour_otbefore.Text = "x2";
                            //}
                        }
                        else
                        {
                            lbl_hour_otbefore_.Text = "x1.5";
                        }
                    }
                    else
                    {
                        lbl_hour_otbefore_.Visible = false;
                        ddl_timebefore_.Visible = false;
                    }

                    //set ot after
                    if (ddl_timeafter_.SelectedValue != "")
                    {
                        //lbl_hour_otafter_.Visible = true;
                        //ddl_timeafter_.Visible = true;
                        if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                        {
                            if (ddl_timeholiday_.SelectedValue == "8")
                            {
                                lbl_hour_otafter_.Text = "x3";
                            }
                            else
                            {
                                lbl_hour_otafter_.Text = "x1";
                            }
                            //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                            //{
                            //    lbl_hour_otbefore.Text = "x1";
                            //}
                            //else
                            //{
                            //    lbl_hour_otbefore.Text = "x2";
                            //}
                        }
                        else
                        {
                            lbl_hour_otafter_.Text = "x1.5";
                        }
                    }
                    else
                    {
                        lbl_hour_otafter_.Visible = false;
                        ddl_timeafter_.Visible = false;
                    }

                    //set ot holiday
                    if (ddl_timeholiday_.SelectedValue != "")
                    {

                        //lbl_hour_otholiday.Visible = true;
                        //ddl_timeholiday_.Visible = true;
                        if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                        {
                            if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                            {
                                lbl_hour_otholiday.Text = "x1";
                            }
                            else
                            {
                                lbl_hour_otholiday.Text = "x2";
                            }
                        }

                    }
                    else
                    {
                        lbl_hour_otholiday.Visible = false;
                        ddl_timeholiday_.Visible = false;
                    }

                    //////set condition checkbox
                    ////if (ddl_timebefore_.SelectedValue == "" && ddl_timeafter_.SelectedValue == "" && ddl_timeholiday_.SelectedValue == "")
                    ////{
                    ////    chk_edit.Visible = false;
                    ////    //txt_remark_shift_otholiday.Visible = false;
                    ////    chk_edit.Checked = false;
                    ////}
                    ////else
                    ////{
                    ////    if (ddl_timestart.SelectedValue != ddl_timeend.SelectedValue) //&& (ddl_timebefore.SelectedValue != "" && ddl_timeholiday.SelectedValue != "")
                    ////    {
                    ////        chk_edit.Visible = true;
                    ////        //txt_remark_shift_otholiday.Visible = true;
                    ////        chk_edit.Checked = true;

                    ////        chkall_edit.Enabled = true;
                    ////        chkall_edit.Checked = true;

                    ////    }
                    ////    else
                    ////    {
                    ////        chk_edit.Visible = false;
                    ////        //txt_remark_shift_otholiday.Visible = false;
                    ////        chk_edit.Checked = false;
                    ////    }

                    ////}

                }

                break;

            case "ddl_timeend_edit":

                int count_end_edit = 0;
                foreach (GridViewRow row in GvWaitAdminEditDoc.Rows)
                {
                    Label lbl_emp_code_otday = (Label)row.FindControl("lbl_emp_code_otday_edit");
                    //Label lbl_scan_in = (Label)row.FindControl("lbl_scan_in");
                    //Label lbl_scan_out = (Label)row.FindControl("lbl_scan_out");

                    Label lbl_parttime_start_time = (Label)row.FindControl("lbl_parttime_start_time_edit");
                    Label lbl_parttime_end_time = (Label)row.FindControl("lbl_parttime_end_time_edit");

                    Label lbl_ot_before = (Label)row.FindControl("lbl_ot_before_edit");
                    DropDownList ddl_timebefore_ = (DropDownList)row.FindControl("ddl_timebefore_edit");

                    Label lbl_ot_after = (Label)row.FindControl("lbl_ot_after_edit");
                    DropDownList ddl_timeafter_ = (DropDownList)row.FindControl("ddl_timeafter_edit");

                    Label lbl_ot_holiday = (Label)row.FindControl("lbl_ot_holiday_edit");
                    DropDownList ddl_timeholiday_ = (DropDownList)row.FindControl("ddl_timeholiday_edit");

                    Label lbl_day_off = (Label)row.FindControl("lbl_day_off_edit");
                    Label lbl_date_condition = (Label)row.FindControl("lbl_date_condition_edit");
                    Label lbl_holiday_idx = (Label)row.FindControl("lbl_holiday_idx_edit");

                    //Label lbl_time_shiftstart = (Label)row.FindControl("lbl_time_shiftstart");
                    //Label lbl_time_shiftend = (Label)row.FindControl("lbl_time_shiftend");

                    Label lbl_parttime_break_start_time = (Label)row.FindControl("lbl_parttime_break_start_time_edit");
                    Label lbl_parttime_break_end_time = (Label)row.FindControl("lbl_parttime_break_end_time_edit");

                    Label lbl_emp_type_idx = (Label)row.FindControl("lbl_emp_type_idx_edit");

                    Label lbl_hour_otbefore_ = (Label)row.FindControl("lbl_hour_otbefore_edit");
                    Label lbl_hour_otafter_ = (Label)row.FindControl("lbl_hour_otafter_edit");
                    Label lbl_hour_otholiday = (Label)row.FindControl("lbl_hour_otholiday_edit");

                    Label lbl_detailholiday = (Label)row.FindControl("lbl_detailholiday_edit");
                    Label lbl_detaildayoff = (Label)row.FindControl("lbl_detaildayoff_edit");

                    CheckBox chk_edit = (CheckBox)row.FindControl("chk_edit");
                    //TextBox txt_remark_shift_otholiday = (TextBox)row.FindControl("txt_remark_shift_otholiday");

                    CheckBox chkall_edit = (CheckBox)GvWaitAdminEditDoc.HeaderRow.Cells[0].FindControl("chkall_edit");

                    DropDownList ddl_timestart = (DropDownList)row.FindControl("ddl_timestart_edit");
                    DropDownList ddl_timeend = (DropDownList)row.FindControl("ddl_timeend_edit");

                    Label lbl_datetime_otstart_edit = (Label)row.FindControl("lbl_datetime_otstart_edit");

                    IFormatProvider culture_ = new CultureInfo("en-US", true);


                    //day off employee
                    string day_off = lbl_day_off.Text;
                    string day_condition = lbl_date_condition.Text;
                    string[] set_test = day_off.Split(',');
                    string day_offemployee = "";

                    double value_otbefore;
                    double value_otafter;
                    DateTime d_finish_addhour_after = new DateTime();

                    var d_start = new DateTime();
                    var d_end = new DateTime();
                    var d_finish = new DateTime();
                    var d_workstart = new DateTime();

                    var DateTime_BreakStart = new DateTime();
                    var DateTime_BreakEnd = new DateTime();

                    double value_total_ot;
                    double value_total_otresult;

                    double value_total_ot_holiday;
                    double value_total_ot_before;
                    double value_total_ot_diff;

                    ////DateTime DateTime_BreakStart = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_start.Text, "dd/MM/yyyy HH:mm", culture);
                    ////DateTime DateTime_BreakEnd = DateTime.ParseExact(lbl_date_start.Text + " " + lbl_break_end.Text, "dd/MM/yyyy HH:mm", culture);

                    d_workstart = DateTime.ParseExact((lbl_datetime_otstart_edit.Text + " " + lbl_parttime_start_time.Text), "dd/MM/yyyy HH:mm", culture_);
                    DateTime_BreakStart = DateTime.ParseExact((lbl_datetime_otstart_edit.Text + " " + lbl_parttime_break_start_time.Text), "dd/MM/yyyy HH:mm", culture_);
                    if (DateTime.ParseExact((lbl_parttime_start_time.Text), "HH:mm", culture_) < DateTime.ParseExact((lbl_parttime_end_time.Text), "HH:mm", culture_))
                    {

                        d_finish = DateTime.ParseExact((lbl_datetime_otstart_edit.Text + " " + lbl_parttime_end_time.Text), "dd/MM/yyyy HH:mm", culture_);
                        DateTime_BreakEnd = DateTime.ParseExact((lbl_datetime_otstart_edit.Text + " " + lbl_parttime_break_end_time.Text), "dd/MM/yyyy HH:mm", culture_);


                    }
                    else
                    {
                        //d_workstart = DateTime.ParseExact((DateTime.ParseExact((txtDateStart_create.Text), "dd/MM/yyyy", culture).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_start_time.Text), "dd/MM/yyyy HH:mm", culture);
                        d_finish = DateTime.ParseExact((DateTime.ParseExact((lbl_datetime_otstart_edit.Text), "dd/MM/yyyy", culture_).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_end_time.Text), "dd/MM/yyyy HH:mm", culture_);
                        DateTime_BreakEnd = DateTime.ParseExact(DateTime.ParseExact((lbl_datetime_otstart_edit.Text), "dd/MM/yyyy", culture_).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_break_end_time.Text, "dd/MM/yyyy HH:mm", culture_);

                    }

                    if (row.RowType == DataControlRowType.DataRow)
                    {

                        if (Array.IndexOf(set_test, day_condition.ToString()) > -1)
                        {
                            day_offemployee = "1"; //yes day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                                                   //litDebug.Text += "1" + "|";
                        }
                        else
                        {
                            day_offemployee = "2"; // no day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                        }
                        //day off employee

                        IFormatProvider culture = new CultureInfo("en-US", true);

                        //litDebug.Text += lbl_break_start.Text;

                        if (ddl_timeend.SelectedValue != "" && ddl_timestart.SelectedValue != "")
                        {


                            DateTime DateTime_ScanIn = DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);
                            DateTime DateTime_ScanOut = DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_);


                            double value;
                            double _value_diffbreaktime;
                            double _value_timescan_late;
                            double _value_timescan_late_add;

                            double _value_otafter_holiday;

                            DateTime d_finish_addhour = new DateTime();
                            DateTime d_start_addhour = new DateTime();

                            DateTime d_finish_addhour_holiday = new DateTime();
                            double _value_diff_scanout;
                            /*
                            
                            litDebug1.Text += DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_).ToString("dd/MM/yyyy") + "|";
                            DateTime DateTime_BreakStart = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_finish.Text), "dd/MM/yyyy HH:mm", culture_);

                            */

                            //ot before
                            value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                            lbl_ot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                            //litDebug.Text = lbl_ot_before.Text;
                            getHourToOT(ddl_timebefore_, Convert.ToDouble(lbl_ot_before.Text), _before);

                            //ot after
                            if (DateTime_ScanOut > DateTime_ScanIn)
                            {

                                d_finish_addhour_after = d_finish.AddMinutes(30);

                                //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                lbl_ot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                //litDebug.Text += value_otafter.ToString() + "|";

                                //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                getHourToOT(ddl_timeafter_, Convert.ToDouble(lbl_ot_after.Text), _after);
                                lbl_hour_otafter_.Visible = true;
                            }
                            else
                            {
                                ddl_timeafter_.Visible = false;
                                lbl_hour_otafter_.Visible = false;
                            }


                            //ot holiday
                            if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1")
                            {
                                //lbl_detailholiday.Visible = true;


                                //litDebug.Text += day_offemployee.ToString() + "|";
                                if (lbl_holiday_idx.Text != "0")
                                {
                                    lbl_detailholiday.Visible = true;
                                    lbl_detaildayoff.Visible = false;
                                }
                                else if (day_offemployee.ToString() == "1")
                                {
                                    lbl_detaildayoff.Visible = true;
                                    lbl_detailholiday.Visible = false;
                                }

                                if ((DateTime_ScanIn < d_workstart || DateTime_ScanIn > d_workstart) && DateTime_ScanOut >= d_finish)
                                {


                                    //ot before
                                    value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                                    lbl_ot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                    //litDebug.Text = lbl_ot_before.Text;
                                    getHourToOT(ddl_timebefore_, Convert.ToDouble(lbl_ot_before.Text), _before);

                                    //ot after
                                    if (DateTime_ScanOut > DateTime_ScanIn)
                                    {

                                        d_finish_addhour_after = d_finish.AddMinutes(30);

                                        //litDebug.Text += d_finish_addhour_after.ToString() + "|";

                                        //value_otafter = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                        value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                        lbl_ot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                                                                           //litDebug.Text += value_otafter.ToString() + "|";

                                        //litDebug.Text += (Math.Round(value_otafter * 2, MidpointRounding.AwayFromZero) / 2).ToString() + "|";
                                        getHourToOT(ddl_timeafter_, Convert.ToDouble(lbl_ot_after.Text), _after);
                                        lbl_hour_otafter_.Visible = true;
                                    }
                                    else
                                    {
                                        ddl_timeafter_.Visible = false;
                                        lbl_hour_otafter_.Visible = false;
                                    }


                                    //_value_diffbreaktime = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;
                                    //_value_diff_scanout = DateTime_ScanOut.Subtract(DateTime_ShiftEnd).TotalMinutes;

                                    _value_diffbreaktime = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;
                                    _value_diff_scanout = DateTime_ScanOut.Subtract(d_finish).TotalMinutes;


                                    //litDebug1.Text = _value_diff_scanout.ToString() + "|";

                                    if (DateTime_ScanOut >= d_finish && DateTime_ScanIn < d_workstart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {
                                        value = Convert.ToDouble((Convert.ToDouble(d_finish.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50 - 1);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);

                                        if (ddl_timeholiday_.SelectedValue == "8")//check ot holiday have 8 hour
                                        {

                                            d_finish_addhour_holiday = d_finish.AddMinutes(30);

                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                            lbl_ot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter_, Convert.ToDouble(lbl_ot_after.Text), _after);


                                        }
                                    }
                                    else if (DateTime_ScanOut >= d_finish && DateTime_ScanIn > d_workstart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {

                                        //litDebug.Text = DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");

                                        _value_timescan_late = Convert.ToDouble((Convert.ToDouble(DateTime_ScanIn.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50);//DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes;
                                        _value_timescan_late_add = Convert.ToDouble(Math.Round(_value_timescan_late * 2, MidpointRounding.AwayFromZero) / 2);// _value_timescan_late.ToString();

                                        //litDebug.Text = _value_timescan_late_add.ToString();

                                        d_start_addhour = d_workstart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                        d_finish_addhour = d_finish.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                        // litDebug1.Text += d_start_addhour.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish_addhour.ToString("dd/MM/yyyy HH:mm");

                                        if (_value_diff_scanout >= 60)
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }
                                        else
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }

                                        //litDebug1.Text += value.ToString();
                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);

                                        if (ddl_timeholiday_.SelectedValue == "8")//check ot holiday have 8 hour
                                        {
                                            //litDebug.Text = d_finish_addhour_holiday.ToString();
                                            //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                            //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                            //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                            d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture_).AddMinutes(30);
                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                            lbl_ot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter_, Convert.ToDouble(lbl_ot_after.Text), _after);

                                        }
                                        else
                                        {
                                            getHourToOT(ddl_timeafter_, Convert.ToDouble("0"), _after);
                                        }

                                    }
                                    else if (DateTime_ScanOut < d_finish && DateTime_ScanIn >= DateTime_BreakStart)
                                    {
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                    }
                                    else if (DateTime_ScanOut < d_finish && DateTime_ScanOut >= DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {

                                        //litDebug.Text = "32";
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        //value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                    }
                                    else //if (DateTime_ScanOut > DateTime_BreakEnd && DateTime_ScanOut < DateTime_ShiftEnd)
                                    {
                                        //litDebug.Text += DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|";
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                    }

                                }
                                else if (DateTime_ScanIn < d_workstart && DateTime_ScanOut < d_finish)
                                {

                                    //litDebug1.Text = "have case new";

                                    //total ot
                                    value_total_ot = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 60));
                                    value_total_otresult = (Math.Floor(value_total_ot * 2) / 2);
                                    //total ot

                                    if (value_total_otresult >= 9.5) //set -1 breaktime
                                    {

                                        value_total_ot_diff = value_total_otresult - 1;


                                        value_total_ot_holiday = 8;
                                        value_total_ot_before = value_total_ot_diff - value_total_ot_holiday;

                                        //litDebug.Text = value_total_ot_before.ToString();
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(value_total_ot_holiday.ToString()), _holiday);
                                        getHourToOT(ddl_timebefore_, Convert.ToDouble(value_total_ot_before.ToString()), _before);

                                    }
                                    else if (value_total_otresult == 9)
                                    {
                                        value_total_ot_diff = value_total_otresult - 1;
                                        value_total_ot_holiday = 8;
                                        getHourToOT(ddl_timeholiday_, Convert.ToDouble(value_total_ot_holiday.ToString()), _holiday);
                                    }
                                    else if (value_total_otresult >= 4 && value_total_otresult <= 5)
                                    {
                                        value_total_ot_before = 4;
                                        getHourToOT(ddl_timebefore_, Convert.ToDouble(value_total_ot_before.ToString()), _before);
                                    }
                                    else if (value_total_otresult > 5 && value_total_otresult <= 8)
                                    {
                                        value_total_ot_diff = value_total_otresult - 1;

                                        value_total_ot_before = value_total_ot_diff;
                                        getHourToOT(ddl_timebefore_, Convert.ToDouble(value_total_ot_before.ToString()), _before);
                                    }

                                }

                            }
                            else
                            {
                                lbl_detailholiday.Visible = false;
                                lbl_detaildayoff.Visible = false;
                                ddl_timeholiday_.Visible = false;
                            }


                            //litDebug.Text += value_otbefore.ToString() + "|";
                        }

                    }

                    count_end_edit++;


                    //set ot x1, x1.5, x2, x3
                    //set ot before
                    if (ddl_timebefore_.SelectedValue != "")
                    {

                        //lbl_hour_otbefore_.Visible = true;
                        //ddl_timebefore_.Visible = true;
                        if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                        {
                            if (ddl_timeholiday_.SelectedValue == "8")
                            {
                                lbl_hour_otbefore_.Text = "x3";
                            }
                            else
                            {
                                lbl_hour_otbefore_.Text = "x1";
                            }
                            //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                            //{
                            //    lbl_hour_otbefore.Text = "x1";
                            //}
                            //else
                            //{
                            //    lbl_hour_otbefore.Text = "x2";
                            //}
                        }
                        else
                        {
                            lbl_hour_otbefore_.Text = "x1.5";
                        }
                    }
                    else
                    {
                        lbl_hour_otbefore_.Visible = false;
                        ddl_timebefore_.Visible = false;
                    }

                    //set ot after
                    if (ddl_timeafter_.SelectedValue != "")
                    {
                        //lbl_hour_otafter_.Visible = true;
                        //ddl_timeafter_.Visible = true;

                        if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                        {
                            if (ddl_timeholiday_.SelectedValue == "8")
                            {
                                lbl_hour_otafter_.Text = "x3";
                            }
                            else
                            {
                                lbl_hour_otafter_.Text = "x1";
                            }
                            //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                            //{
                            //    lbl_hour_otbefore.Text = "x1";
                            //}
                            //else
                            //{
                            //    lbl_hour_otbefore.Text = "x2";
                            //}
                        }
                        else
                        {
                            lbl_hour_otafter_.Text = "x1.5";
                        }
                    }
                    else
                    {
                        lbl_hour_otafter_.Visible = false;
                        ddl_timeafter_.Visible = false;
                    }

                    //set ot holiday
                    if (ddl_timeholiday_.SelectedValue != "")
                    {

                        //lbl_hour_otholiday.Visible = true;
                        //ddl_timeholiday_.Visible = true;
                        if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                        {
                            if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                            {
                                lbl_hour_otholiday.Text = "x1";
                            }
                            else
                            {
                                lbl_hour_otholiday.Text = "x2";
                            }
                        }

                    }
                    else
                    {
                        lbl_hour_otholiday.Visible = false;
                        ddl_timeholiday_.Visible = false;
                    }


                }

                break;



        }
    }

    protected void TextBoxChanged(object sender, EventArgs e)
    {
        var txtName = (TextBox)sender;

        switch (txtName.ID)
        {

            ////case "txt_timeend_job":

            ////    //linkBtnTrigger(btnSaveDetailMA);

            ////    foreach (GridViewRow row in GvCreateOTMonth.Rows)
            ////    {
            ////        CheckBox chk_date_ot = (CheckBox)row.FindControl("chk_date_ot");
            ////        UpdatePanel Update_Timestart = (UpdatePanel)row.FindControl("Update_Timestart");
            ////        UpdatePanel Update_Timeend = (UpdatePanel)row.FindControl("Update_Timeend");
            ////        TextBox txt_job = (TextBox)row.FindControl("txt_job");
            ////        TextBox txt_timestart_job = (TextBox)row.FindControl("txt_timestart_job");
            ////        TextBox txt_timeend_job = (TextBox)row.FindControl("txt_timeend_job");
            ////        TextBox txt_sum_hour = (TextBox)row.FindControl("txt_sum_hour");
            ////        TextBox txt_head_set = (TextBox)row.FindControl("txt_head_set");
            ////        TextBox txt_remark = (TextBox)row.FindControl("txt_remark");
            ////        decimal total_hour = 0;


            ////        //TimeSpan diff = secondDate - firstDate;
            ////        //double hours = diff.TotalHours;

            ////        litDebug.Text = "3333";

            ////        if (chk_date_ot.Checked)
            ////        {
            ////            if (txt_timestart_job.Text != "" && txt_timeend_job.Text != "")
            ////            {
            ////                //linkBtnTrigger(btnSaveDetailMA);
            ////                //litDebug.Text = Convert.ToInt32(txt_Quantity_txt.Text).ToString();
            ////                total_hour = Convert.ToDecimal(txt_timestart_job.Text) * Convert.ToDecimal(txt_timeend_job.Text);
            ////                //litDebug.Text = Convert.ToDecimal(total_price).ToString();
            ////                txt_sum_hour.Text = Convert.ToDecimal(total_hour).ToString();

            ////            }
            ////            else
            ////            {
            ////                //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรอกราคา/หน่วย ---');", true);

            ////                break;
            ////            }
            ////        }


            ////    }

            ////    break;




        }

    }

    #endregion Changed

    #region gridview
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {
            case "GvDetailOTShiftRotate":
                setGridData(GvDetailOTShiftRotate, ViewState["Vs_DetailShiftRotateToUser"]);

                break;
            case "GvDetailCreateToAdmin":

                setGridData(GvDetailCreateToAdmin, ViewState["Vs_DetailAdminShiftRotate"]);
                break;
            case "GvViewDetailCreateToAdmin":

                setGridData(GvViewDetailCreateToAdmin, ViewState["Vs_ViewDetailAdminShiftRotate"]);
                break;
        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvCreateShiftRotate":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    if (ViewState["Vs_ScanIn_OT"].ToString() != null && ViewState["Vs_ScanOut_OT"].ToString() != null)
                    {

                        Label lbl_emp_code_otday = (Label)e.Row.FindControl("lbl_emp_code_otday");
                        Label lbl_scan_in = (Label)e.Row.FindControl("lbl_scan_in");
                        Label lbl_scan_out = (Label)e.Row.FindControl("lbl_scan_out");

                        Label lbl_parttime_start_time = (Label)e.Row.FindControl("lbl_parttime_start_time");
                        Label lbl_parttime_end_time = (Label)e.Row.FindControl("lbl_parttime_end_time");

                        Label lbl_ot_before = (Label)e.Row.FindControl("lbl_ot_before");
                        DropDownList ddl_timebefore_set = (DropDownList)e.Row.FindControl("ddl_timebefore_set");
                        DropDownList ddl_timebefore = (DropDownList)e.Row.FindControl("ddl_timebefore");

                        Label lbl_ot_after = (Label)e.Row.FindControl("lbl_ot_after");
                        DropDownList ddl_timeafter_set = (DropDownList)e.Row.FindControl("ddl_timeafter_set");
                        DropDownList ddl_timeafter = (DropDownList)e.Row.FindControl("ddl_timeafter");

                        Label lbl_ot_holiday = (Label)e.Row.FindControl("lbl_ot_holiday");
                        DropDownList ddl_timeholiday_set = (DropDownList)e.Row.FindControl("ddl_timeholiday_set");
                        DropDownList ddl_timeholiday = (DropDownList)e.Row.FindControl("ddl_timeholiday");

                        Label lbl_day_off = (Label)e.Row.FindControl("lbl_day_off");
                        Label lbl_date_condition = (Label)e.Row.FindControl("lbl_date_condition");
                        Label lbl_holiday_idx = (Label)e.Row.FindControl("lbl_holiday_idx");

                        Label lbl_time_shiftstart = (Label)e.Row.FindControl("lbl_time_shiftstart");
                        Label lbl_time_shiftend = (Label)e.Row.FindControl("lbl_time_shiftend");

                        Label lbl_parttime_break_start_time = (Label)e.Row.FindControl("lbl_parttime_break_start_time");
                        Label lbl_parttime_break_end_time = (Label)e.Row.FindControl("lbl_parttime_break_end_time");

                        Label lbl_emp_type_idx = (Label)e.Row.FindControl("lbl_emp_type_idx");

                        Label lbl_hour_otbefore = (Label)e.Row.FindControl("lbl_hour_otbefore");
                        Label lbl_hour_otafter = (Label)e.Row.FindControl("lbl_hour_otafter");
                        Label lbl_hour_otholiday = (Label)e.Row.FindControl("lbl_hour_otholiday");

                        Label lbl_detailholiday = (Label)e.Row.FindControl("lbl_detailholiday");
                        Label lbl_detaildayoff = (Label)e.Row.FindControl("lbl_detaildayoff");

                        CheckBox chk_otday = (CheckBox)e.Row.FindControl("chk_otday");
                        TextBox txt_remark_shift_otholiday = (TextBox)e.Row.FindControl("txt_remark_shift_otholiday");

                        CheckBox chkallCreate = (CheckBox)GvCreateShiftRotate.HeaderRow.Cells[0].FindControl("chkallCreate");

                        DropDownList ddl_timestart = (DropDownList)e.Row.FindControl("ddl_timestart");

                        IFormatProvider culture = new CultureInfo("en-US", true);

                        //day off employee
                        string day_off = lbl_day_off.Text;
                        string day_condition = lbl_date_condition.Text;
                        string[] set_test = day_off.Split(',');
                        string day_offemployee = "";

                        double value_otbefore;
                        double value_otafter;
                        DateTime d_finish_addhour_after = new DateTime();

                        var d_start = new DateTime();
                        var d_end = new DateTime();
                        var d_finish = new DateTime();
                        var d_workstart = new DateTime();

                        var DateTime_BreakStart = new DateTime();
                        var DateTime_BreakEnd = new DateTime();

                        double value_total_ot;
                        double value_total_otresult;

                        double value_total_ot_holiday;
                        double value_total_ot_before;
                        double value_total_ot_diff;


                        d_workstart = DateTime.ParseExact((txtDateStart_create.Text + " " + lbl_parttime_start_time.Text), "dd/MM/yyyy HH:mm", culture);
                        DateTime_BreakStart = DateTime.ParseExact((txtDateStart_create.Text + " " + lbl_parttime_break_start_time.Text), "dd/MM/yyyy HH:mm", culture);
                        if (DateTime.ParseExact((lbl_parttime_start_time.Text), "HH:mm", culture) < DateTime.ParseExact((lbl_parttime_end_time.Text), "HH:mm", culture))
                        {

                            d_finish = DateTime.ParseExact((txtDateStart_create.Text + " " + lbl_parttime_end_time.Text), "dd/MM/yyyy HH:mm", culture);
                            DateTime_BreakEnd = DateTime.ParseExact((txtDateStart_create.Text + " " + lbl_parttime_break_end_time.Text), "dd/MM/yyyy HH:mm", culture);


                        }
                        else
                        {
                            //d_workstart = DateTime.ParseExact((DateTime.ParseExact((txtDateStart_create.Text), "dd/MM/yyyy", culture).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_start_time.Text), "dd/MM/yyyy HH:mm", culture);
                            d_finish = DateTime.ParseExact((DateTime.ParseExact((txtDateStart_create.Text), "dd/MM/yyyy", culture).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_end_time.Text), "dd/MM/yyyy HH:mm", culture);

                            //DateTime_BreakStart = DateTime.ParseExact(DateTime.ParseExact((txtDateStart_create.Text), "dd/MM/yyyy", culture).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_break_start_time.Text, "dd/MM/yyyy HH:mm", culture);
                            DateTime_BreakEnd = DateTime.ParseExact(DateTime.ParseExact((txtDateStart_create.Text), "dd/MM/yyyy", culture).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_break_end_time.Text, "dd/MM/yyyy HH:mm", culture);


                            //litDebug.Text += d_workstart.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish.ToString("dd/MM/yyyy HH:mm");

                            //litDebug.Text += (DateTime.ParseExact((txtDateStart_create.Text), "dd/MM/yyyy", culture).AddDays(1)).ToString("dd/MM/yyyy") + "|";
                        }

                        if (Array.IndexOf(set_test, day_condition.ToString()) > -1)
                        {
                            day_offemployee = "1"; //yes day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                                                   //litDebug.Text += "1" + "|";
                        }
                        else
                        {
                            day_offemployee = "0"; // no day off
                                                   //litDebug.Text += day_offemployee.ToString() + "|";
                        }
                        //day off employee


                        //double _value_difftimestart;

                        //_value_difftimestart = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;



                        #region ddl 
                        //ddl datetime_start
                        ovt_shifttime_rotatestart_detail[] _item_timestart = (ovt_shifttime_rotatestart_detail[])ViewState["Vs_ScanIn_OT"];
                        var _linqTimeStart = (from dt in _item_timestart

                                              where
                                              ////dt.date_start != "" && dt.time_start != ""
                                              ////&& dt.date_start == lbl_date_start.Text && (DateTime.ParseExact((dt.time_start), "dd/MM/yyyy HH:mm", culture_) < d_finish)

                                              dt.date_ot != "" && dt.time_scanin != ""
                                              && dt.emp_code == lbl_emp_code_otday.Text
                                              && (dt.date_ot == txtDateStart_create.Text)
                                              && (
                                                    (DateTime.ParseExact((dt.time_scanin), "dd/MM/yyyy HH:mm", culture) <= DateTime.ParseExact((dt.time_shiftstart), "dd/MM/yyyy HH:mm", culture))

                                                    || (

                                                        DateTime.ParseExact((dt.time_scanin), "dd/MM/yyyy HH:mm", culture) >= DateTime.ParseExact((dt.time_shiftstart), "dd/MM/yyyy HH:mm", culture)

                                                        && DateTime.ParseExact((dt.time_scanin), "dd/MM/yyyy HH:mm", culture) <= DateTime.ParseExact((dt.time_shiftend), "dd/MM/yyyy HH:mm", culture) //times
                                                                                                                                                                                                      //&& DateTime.ParseExact((dt.time_scanin), "dd/MM/yyyy HH:mm", culture).Subtract(DateTime.ParseExact((dt.time_shiftstart), "dd/MM/yyyy HH:mm", culture)).TotalMinutes < 180

                                                    )

                                                    || (
                                                            (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1")
                                                            && (DateTime.ParseExact((dt.time_scanin), "dd/MM/yyyy HH:mm", culture) <= DateTime.ParseExact((dt.time_shiftend), "dd/MM/yyyy HH:mm", culture))
                                                            && dt.time_scanin != "" && dt.time_scanin != ""
                                                        )
                                               ////|| (
                                               ////(lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1")
                                               ////&& (DateTime.ParseExact((dt.time_scanin), "dd/MM/yyyy HH:mm", culture) < DateTime.ParseExact((dt.time_shiftend), "dd/MM/yyyy HH:mm", culture))
                                               ////&& dt.time_scanin == txtDateStart_create.Text && dt.time_scanin != "" && dt.time_scanin != ""
                                               ////)
                                               )


                                              //&& (
                                              //   (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1")
                                              //   || ((DateTime.ParseExact((dt.time_start), "dd/MM/yyyy HH:mm", culture_) < d_finish))
                                              // )

                                              select new
                                              {
                                                  dt.time_scanin,
                                                  dt.time_shiftend,
                                                  dt.diff_second_start
                                              }).Distinct().ToList();

                        setDdlData(ddl_timestart, _linqTimeStart.ToList(), "time_scanin", "diff_second_start");


                        //litDebug.Text = day_offemployee.ToString();
                        //ddl datetime_start


                        DateTime d_add = new DateTime();

                        //ddl datetime_end
                        ovt_shifttime_rotateend_detail[] _item_timeend = (ovt_shifttime_rotateend_detail[])ViewState["Vs_ScanOut_OT"];
                        var _linqTimeEnd = (from dt in _item_timeend
                                                //join dt1 in _templist_TimeStart on dt.time_end equals dt1.time_start into ps
                                            where
                                            ////dt.date_end == lbl_date_start.Text 
                                            ////&&

                                            (dt.date_ot != "" && dt.time_scanout != ""
                                            && dt.emp_code == lbl_emp_code_otday.Text
                                            &&
                                                (
                                                    (

                                                        DateTime.ParseExact((dt.time_scanout), "dd/MM/yyyy HH:mm", culture) >= DateTime.ParseExact((dt.time_shiftend), "dd/MM/yyyy HH:mm", culture)
                                                        && dt.emp_code == lbl_emp_code_otday.Text

                                                        && DateTime.ParseExact((lbl_parttime_start_time.Text), "HH:mm", culture) <= DateTime.ParseExact((lbl_parttime_end_time.Text), "HH:mm", culture)

                                                    )

                                                    ||
                                                    (DateTime.ParseExact((dt.time_scanout), "dd/MM/yyyy HH:mm", culture) <= DateTime.ParseExact((dt.time_shiftend), "dd/MM/yyyy HH:mm", culture)
                                                        && DateTime.ParseExact((dt.time_scanout), "dd/MM/yyyy HH:mm", culture) >= DateTime.ParseExact((dt.time_shiftstart), "dd/MM/yyyy HH:mm", culture)
                                                    )

                                                    ||
                                                    (
                                                        DateTime.ParseExact((dt.date_ot), "dd/MM/yyyy", culture) == DateTime.ParseExact((txtDateStart_create.Text), "dd/MM/yyyy", culture).AddDays(1)
                                                        && dt.emp_code == lbl_emp_code_otday.Text && DateTime.ParseExact((lbl_parttime_start_time.Text), "HH:mm", culture) >= DateTime.ParseExact((lbl_parttime_end_time.Text), "HH:mm", culture)

                                                    )

                                                //||
                                                //(DateTime.ParseExact((dt.time_scanout), "dd/MM/yyyy HH:mm", culture) >= d_finish && DateTime.ParseExact((dt.time_scanout), "dd/MM/yyyy HH:mm", culture).Subtract(d_finish).TotalHours <= 8)

                                                )
                                            //|| (DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) >= d_finish && dt.date_end == lbl_date_start.Text)
                                            //&& (DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) >= d_finish)
                                            //|| (
                                            //   (lbl_holiday_idx_check.Text != "0" || day_offemployee.ToString() == "1")
                                            //   && ((DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) >= d_finish && dt.date_end == lbl_date_start.Text)
                                            //        || DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) < d_finish && dt.date_end == lbl_date_start.Text)
                                            //   )

                                            //|| (DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) > dstart_add && DateTime.ParseExact((dt.time_end), "dd/MM/yyyy HH:mm", culture_) < dend_add)

                                            )
                                            select new
                                            {
                                                dt.time_scanout,
                                                dt.diff_second_end
                                            }).Distinct().ToList();


                        setDdlData(ddl_timeend, _linqTimeEnd.ToList(), "time_scanout", "diff_second_end");

                        //ddl datetime_end
                        #endregion ddl

                        //set gv row detail show data
                        if (ddl_timestart.SelectedValue != "" && ddl_timeend.SelectedValue != "")
                        {
                            if ((ddl_timeend.SelectedItem.ToString() != ddl_timestart.SelectedItem.ToString()) || (ddl_timeend.Items.Count.ToString() != ddl_timestart.Items.Count.ToString()))
                            {
                                e.Row.Visible = true;

                                DateTime DateTime_ScanIn = DateTime.ParseExact(ddl_timestart.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture);
                                DateTime DateTime_ScanOut = DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture);

                                double value;
                                double _value_diffbreaktime;
                                decimal _value_timescan_late;
                                double _value_timescan_late_add;

                                double _value_otafter_holiday;

                                DateTime d_finish_addhour = new DateTime();
                                DateTime d_start_addhour = new DateTime();

                                DateTime d_finish_addhour_holiday = new DateTime();
                                double _value_diff_scanout;

                                /*

                                litDebug1.Text += DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_).ToString("dd/MM/yyyy") + "|";
                                DateTime DateTime_BreakStart = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_finish.Text), "dd/MM/yyyy HH:mm", culture_);

                                */

                                //ot before
                                value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                //litDebug.Text += d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString() + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm");

                                lbl_ot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                getHourToOT(ddl_timebefore, Convert.ToDouble(lbl_ot_before.Text), _before);

                                //ot after
                                if (DateTime_ScanOut > DateTime_ScanIn)
                                {

                                    d_finish_addhour_after = d_finish.AddMinutes(30);

                                    value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                    //_value_test = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);

                                    lbl_ot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round((value_otafter * 2)/2, MidpointRounding.AwayFromZero)).ToString();
                                    getHourToOT(ddl_timeafter, Convert.ToDouble(lbl_ot_after.Text), _after);



                                }
                                else
                                {
                                    ddl_timeafter.Visible = false;
                                }


                                //ot holiday
                                if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1")
                                {

                                    //set DH/HH
                                    if (lbl_holiday_idx.Text != "0")
                                    {
                                        lbl_detailholiday.Visible = true;
                                        lbl_detaildayoff.Visible = false;
                                    }
                                    else if (day_offemployee.ToString() == "1")
                                    {
                                        lbl_detaildayoff.Visible = true;
                                        lbl_detailholiday.Visible = false;
                                    }


                                    //check case new
                                    if ((DateTime_ScanIn <= d_workstart || DateTime_ScanIn >= d_workstart) && DateTime_ScanOut >= d_finish)
                                    {

                                        //litDebug.Text = "have case old";
                                        //litDebug.Text += (Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString())/30) * 0.50).ToString()) + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|"  + DateTime_ShiftEnd.ToString("dd/MM/yyyy HH:mm");
                                        ////decimal value = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                                        ////litDebug1.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        //litDebug.Text += _value_diffbreaktime.ToString() + "|";


                                        //ot before
                                        value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                                        //litDebug.Text += d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString() + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm");
                                        lbl_ot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timebefore, Convert.ToDouble(lbl_ot_before.Text), _before);

                                        //ot after
                                        if (DateTime_ScanOut > DateTime_ScanIn)
                                        {

                                            d_finish_addhour_after = d_finish.AddMinutes(30);

                                            value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                            //_value_test = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);

                                            lbl_ot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round((value_otafter * 2)/2, MidpointRounding.AwayFromZero)).ToString();
                                            getHourToOT(ddl_timeafter, Convert.ToDouble(lbl_ot_after.Text), _after);



                                        }
                                        else
                                        {
                                            ddl_timeafter.Visible = false;
                                        }

                                        _value_diffbreaktime = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;
                                        _value_diff_scanout = DateTime_ScanOut.Subtract(d_finish).TotalMinutes;

                                        //litDebug1.Text = _value_diff_scanout.ToString() + "|";

                                        if (DateTime_ScanOut >= d_finish && DateTime_ScanIn <= d_workstart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                        {
                                            //litDebug.Text = "66";

                                            value = Convert.ToDouble((Convert.ToDouble(d_finish.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50 - 1);

                                            lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);

                                            if (ddl_timeholiday.SelectedValue == "8")//check ot holiday have 8 hour
                                            {

                                                d_finish_addhour_holiday = d_finish.AddMinutes(30);

                                                _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                                lbl_ot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                getHourToOT(ddl_timeafter, Convert.ToDouble(lbl_ot_after.Text), _after);


                                            }
                                        }
                                        else if (DateTime_ScanOut >= d_finish && DateTime_ScanIn >= d_workstart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                        {
                                            //litDebug.Text = "22";
                                            //litDebug.Text = DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");

                                            _value_timescan_late = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanIn.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50);//DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes;
                                            _value_timescan_late_add = Convert.ToDouble(Math.Round(_value_timescan_late * 2, MidpointRounding.AwayFromZero) / 2);// _value_timescan_late.ToString();

                                            //litDebug.Text = _value_timescan_late_add.ToString();

                                            d_start_addhour = d_workstart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                            d_finish_addhour = d_finish.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                            // litDebug1.Text += d_start_addhour.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish_addhour.ToString("dd/MM/yyyy HH:mm");

                                            if (_value_diff_scanout >= 60)
                                            {
                                                value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                            }
                                            else
                                            {
                                                value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                            }

                                            //litDebug1.Text += value.ToString();
                                            lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();// (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                            // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);

                                            if (ddl_timeholiday.SelectedValue == "8")//check ot holiday have 8 hour
                                            {
                                                //litDebug.Text = d_finish_addhour_holiday.ToString();
                                                //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                                //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                                //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                                d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture).AddMinutes(30);
                                                _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                                lbl_ot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                                getHourToOT(ddl_timeafter, Convert.ToDouble(lbl_ot_after.Text), _after);

                                            }
                                            else
                                            {
                                                getHourToOT(ddl_timeafter, Convert.ToDouble("0"), _after);
                                            }

                                        }
                                        else if (DateTime_ScanOut < d_finish && DateTime_ScanIn >= DateTime_BreakStart)
                                        {

                                            //litDebug.Text = "33";
                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                            lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                        }
                                        else if (DateTime_ScanOut < d_finish && DateTime_ScanOut >= DateTime_BreakStart && _value_diffbreaktime >= 60)
                                        {

                                            //litDebug.Text = "32";
                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                            //value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                            lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                        }
                                        else //if (DateTime_ScanOut > DateTime_BreakEnd && DateTime_ScanOut < DateTime_ShiftEnd)
                                        {
                                            //litDebug.Text = "77";
                                            //litDebug.Text += DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|";
                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50);

                                            lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                        }

                                    }
                                    else if (DateTime_ScanIn < d_workstart && DateTime_ScanOut < d_finish)
                                    {

                                        //litDebug1.Text = "have case new";

                                        //total ot
                                        value_total_ot = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 60));
                                        value_total_otresult = (Math.Floor(value_total_ot * 2) / 2);
                                        //total ot

                                        if (value_total_otresult >= 9.5) //set -1 breaktime
                                        {

                                            value_total_ot_diff = value_total_otresult - 1;


                                            value_total_ot_holiday = 8;
                                            value_total_ot_before = value_total_ot_diff - value_total_ot_holiday;

                                            //litDebug.Text = value_total_ot_before.ToString();
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(value_total_ot_holiday.ToString()), _holiday);
                                            getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()), _before);

                                        }
                                        else if (value_total_otresult == 9)
                                        {
                                            value_total_ot_diff = value_total_otresult - 1;
                                            value_total_ot_holiday = 8;
                                            getHourToOT(ddl_timeholiday, Convert.ToDouble(value_total_ot_holiday.ToString()), _holiday);
                                        }
                                        else if (value_total_otresult >= 4 && value_total_otresult <= 5)
                                        {
                                            value_total_ot_before = 4;
                                            getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()), _before);
                                        }
                                        else if (value_total_otresult > 5 && value_total_otresult <= 8)
                                        {
                                            value_total_ot_diff = value_total_otresult - 1;

                                            value_total_ot_before = value_total_ot_diff;
                                            getHourToOT(ddl_timebefore, Convert.ToDouble(value_total_ot_before.ToString()), _before);
                                        }

                                    }

                                }
                                else
                                {
                                    //litDebug.Text += DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|";
                                    lbl_detailholiday.Visible = false;
                                    lbl_detaildayoff.Visible = false;
                                    ddl_timeholiday.Visible = false;
                                }


                                //litDebug.Text += value_otbefore.ToString() + "|";
                            }
                            else
                            {

                                //litDebug1.Text += ddl_timeend.SelectedItem.ToString() + "|" + ddl_timestart.SelectedItem.ToString() + "|";
                                e.Row.Visible = false;
                            }
                        }
                        else
                        {
                            //litDebug1.Text += ddl_timeend.SelectedItem.ToString() + "|";
                            e.Row.Visible = false;
                        }


                        //set ot x1, x1.5, x2, x3
                        //set ot before
                        if (ddl_timebefore.SelectedValue != "")
                        {

                            lbl_hour_otbefore.Visible = true;
                            ddl_timebefore.Visible = true;
                            if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                            {
                                if (ddl_timeholiday.SelectedValue == "8")
                                {
                                    lbl_hour_otbefore.Text = "x3";
                                }
                                else
                                {
                                    lbl_hour_otbefore.Text = "x1";
                                }
                                //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                //{
                                //    lbl_hour_otbefore.Text = "x1";
                                //}
                                //else
                                //{
                                //    lbl_hour_otbefore.Text = "x2";
                                //}
                            }
                            else
                            {
                                lbl_hour_otbefore.Text = "x1.5";
                            }
                        }
                        else
                        {
                            lbl_hour_otbefore.Visible = false;
                            ddl_timebefore.Visible = false;
                        }

                        //set ot after
                        if (ddl_timeafter.SelectedValue != "")
                        {

                            lbl_hour_otafter.Visible = true;
                            ddl_timeafter.Visible = true;
                            if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                            {
                                if (ddl_timeholiday.SelectedValue == "8")
                                {
                                    lbl_hour_otafter.Text = "x3";
                                }
                                else
                                {
                                    lbl_hour_otafter.Text = "x1";
                                }
                                //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                //{
                                //    lbl_hour_otbefore.Text = "x1";
                                //}
                                //else
                                //{
                                //    lbl_hour_otbefore.Text = "x2";
                                //}
                            }
                            else
                            {
                                lbl_hour_otafter.Text = "x1.5";
                            }
                        }
                        else
                        {
                            lbl_hour_otafter.Visible = false;
                            ddl_timeafter.Visible = false;
                        }

                        //set ot holiday
                        if (ddl_timeholiday.SelectedValue != "")
                        {

                            lbl_hour_otholiday.Visible = true;
                            ddl_timeholiday.Visible = true;
                            if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                            {
                                if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                                {
                                    lbl_hour_otholiday.Text = "x1";
                                }
                                else
                                {
                                    lbl_hour_otholiday.Text = "x2";
                                }
                            }

                        }
                        else
                        {
                            lbl_hour_otholiday.Visible = false;
                            ddl_timeholiday.Visible = false;
                        }


                        //set condition checkbox
                        if (ddl_timebefore.SelectedValue == "" && ddl_timeafter.SelectedValue == "" && ddl_timeholiday.SelectedValue == "")
                        {
                            chk_otday.Visible = false;
                            txt_remark_shift_otholiday.Visible = false;
                            chk_otday.Checked = false;
                        }
                        else
                        {
                            if (ddl_timestart.SelectedItem.Text != ddl_timeend.SelectedItem.Text) //&& (ddl_timebefore.SelectedValue != "" && ddl_timeholiday.SelectedValue != "")
                            {
                                chk_otday.Visible = true;
                                txt_remark_shift_otholiday.Visible = true;
                                chk_otday.Checked = true;

                                chkallCreate.Enabled = true;
                                chkallCreate.Checked = true;

                            }
                            else
                            {
                                chk_otday.Visible = false;
                                txt_remark_shift_otholiday.Visible = false;
                                chk_otday.Checked = false;
                            }

                        }


                    }

                    //litDebug.Text = GvCreateShiftRotate.Rows.Count.ToString();
                }
                if (e.Row.RowType == DataControlRowType.Footer)
                {


                }

                break;

            case "GvWaitApprove":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label lbl_m0_node_idx_approve = (Label)e.Row.FindControl("lbl_m0_node_idx_approve");
                    Label lbl_m0_actor_idx_approve = (Label)e.Row.FindControl("lbl_m0_actor_idx_approve");

                    DropDownList ddl_timebefore_approve = (DropDownList)e.Row.FindControl("ddl_timebefore_approve");
                    DropDownList ddl_timeafter_approve = (DropDownList)e.Row.FindControl("ddl_timeafter_approve");
                    DropDownList ddl_timeholiday_approve = (DropDownList)e.Row.FindControl("ddl_timeholiday_approve");

                    Label lbl_ot_before_approve_max = (Label)e.Row.FindControl("lbl_ot_before_approve_max");
                    Label lbl_ot_after_max = (Label)e.Row.FindControl("lbl_ot_after_max");
                    Label lbl_ot_holiday_max = (Label)e.Row.FindControl("lbl_ot_holiday_max");

                    Label lbl_emp_code_approve = (Label)e.Row.FindControl("lbl_emp_code_approve");
                    Label lbl_ot_before_approve = (Label)e.Row.FindControl("lbl_ot_before_approve");
                    Label lbl_ot_after_approve = (Label)e.Row.FindControl("lbl_ot_after_approve");
                    Label lbl_ot_holiday_approve = (Label)e.Row.FindControl("lbl_ot_holiday_approve");

                    Label lbl_hour_otbefore_approve = (Label)e.Row.FindControl("lbl_hour_otbefore_approve");
                    Label lbl_hour_otholiday_approve = (Label)e.Row.FindControl("lbl_hour_otholiday_approve");
                    Label lbl_hour_otafter_approve = (Label)e.Row.FindControl("lbl_hour_otafter_approve");

                    Label lbl_parttime_start_time_approve = (Label)e.Row.FindControl("lbl_parttime_start_time_approve");
                    Label lbl_parttime_end_time_approve = (Label)e.Row.FindControl("lbl_parttime_end_time_approve");

                    Label lbl_parttime_break_start_time_approve = (Label)e.Row.FindControl("lbl_parttime_break_start_time_approve");
                    Label lbl_parttime_break_end_time_approve = (Label)e.Row.FindControl("lbl_parttime_break_end_time_approve");

                    TextBox txt_remark_admin_edit = (TextBox)e.Row.FindControl("txt_remark_admin_edit");
                    TextBox txt_comment_head_approve = (TextBox)e.Row.FindControl("txt_comment_head_approve");
                    TextBox txt_comment_hr_check = (TextBox)e.Row.FindControl("txt_comment_hr_check");

                    Label lbl_holiday_name_approve = (Label)e.Row.FindControl("lbl_holiday_name_approve");
                    Label lbl_detaildayoff_approve = (Label)e.Row.FindControl("lbl_detaildayoff_approve");

                    Label lbl_holiday_idx_approve = (Label)e.Row.FindControl("lbl_holiday_idx_approve");
                    Label lbl_day_off_approve = (Label)e.Row.FindControl("lbl_day_off_approve");
                    Label lbl_date_condition_approve = (Label)e.Row.FindControl("lbl_date_condition_approve");

                    Label lbl_datetime_otstart_approve = (Label)e.Row.FindControl("lbl_datetime_otstart_approve");

                    Label lbl_ddl_timestart_approve = (Label)e.Row.FindControl("lbl_ddl_timestart_approve");
                    Label lbl_ddl_timeend_approve = (Label)e.Row.FindControl("lbl_ddl_timeend_approve");

                    DropDownList ddl_timestart_approve = (DropDownList)e.Row.FindControl("ddl_timestart_approve");
                    DropDownList ddl_timeend_approve = (DropDownList)e.Row.FindControl("ddl_timeend_approve");


                    double ot_before_approve;
                    double ot_after_approve;
                    double ot_holiday_approve;

                    ddl_timebefore_approve.Enabled = false;
                    ddl_timeafter_approve.Enabled = false;
                    ddl_timeholiday_approve.Enabled = false;

                    lbl_hour_otbefore_approve.Visible = false;
                    lbl_hour_otholiday_approve.Visible = false;
                    lbl_hour_otafter_approve.Visible = false;

                    txt_remark_admin_edit.Enabled = false;
                    txt_comment_head_approve.Enabled = false;
                    txt_comment_hr_check.Enabled = false;

                    lbl_holiday_name_approve.Visible = false;
                    lbl_detaildayoff_approve.Visible = false;

                    //set time_start/time_end
                    ddl_timestart_approve.Visible = false;
                    ddl_timeend_approve.Visible = false;

                    lbl_ddl_timestart_approve.Visible = false;
                    lbl_ddl_timeend_approve.Visible = false;
                    //set time_start/time_end

                    //day off employee
                    string day_off = lbl_day_off_approve.Text;
                    string day_condition = lbl_date_condition_approve.Text;
                    string[] set_test = day_off.Split(',');
                    string day_offemployee = "";

                    if (Array.IndexOf(set_test, day_condition.ToString()) > -1)
                    {
                        day_offemployee = "1"; //yes day off
                                               //litDebug.Text += day_offemployee.ToString() + "|";
                                               //litDebug.Text += "1" + "|";
                    }
                    else
                    {
                        day_offemployee = "2"; // no day off
                                               //litDebug.Text += day_offemployee.ToString() + "|";
                    }
                    //day off employee


                    IFormatProvider culture = new CultureInfo("en-US", true);
                    double value_otbefore;
                    double value_otafter;
                    DateTime d_finish_addhour_after = new DateTime();

                    var d_start = new DateTime();
                    var d_end = new DateTime();
                    var d_finish = new DateTime();
                    var d_workstart = new DateTime();

                    var DateTime_BreakStart = new DateTime();
                    var DateTime_BreakEnd = new DateTime();



                    //set ddl hour before
                    if (lbl_ot_before_approve.Text != "" && lbl_ot_before_approve.Text != "0")
                    {
                        lbl_hour_otbefore_approve.Visible = true;
                        getHourToOT(ddl_timebefore_approve, Convert.ToDouble(lbl_ot_before_approve.Text), "");

                    }
                    else
                    {
                        getHourToOT(ddl_timebefore_approve, Convert.ToDouble("0"), _before);

                    }

                    //set ddl hour after
                    if (lbl_ot_after_approve.Text != "" && lbl_ot_after_approve.Text != "0")
                    {
                        lbl_hour_otafter_approve.Visible = true;
                        getHourToOT(ddl_timeafter_approve, Convert.ToDouble(lbl_ot_after_approve.Text), _after);

                    }
                    else
                    {
                        //lbl_hour_otafter_approve.Visible = false;
                        getHourToOT(ddl_timeafter_approve, Convert.ToDouble("0"), _after);
                    }

                    //set ddl hour holiday
                    if (lbl_ot_holiday_approve.Text != "" && lbl_ot_holiday_approve.Text != "0")
                    {
                        lbl_hour_otholiday_approve.Visible = true;
                        getHourToOT(ddl_timeholiday_approve, Convert.ToDouble(lbl_ot_holiday_approve.Text), _holiday);

                    }
                    else
                    {
                        getHourToOT(ddl_timeholiday_approve, Convert.ToDouble("0"), _holiday);
                    }

                    if (lbl_holiday_idx_approve.Text != "0" || day_offemployee.ToString() == "1")
                    {
                        if (lbl_holiday_idx_approve.Text != "0")
                        {
                            lbl_holiday_name_approve.Visible = true;
                        }
                        else if (day_offemployee.ToString() == "1")
                        {
                            lbl_detaildayoff_approve.Visible = true;
                        }

                    }
                    else
                    {
                        lbl_holiday_name_approve.Visible = false;
                        lbl_detaildayoff_approve.Visible = false;
                    }

                    switch (int.Parse(lbl_m0_node_idx_approve.Text))
                    {
                        case 11: //admin edit document

                            ddl_timestart_approve.Visible = true;
                            ddl_timeend_approve.Visible = true;

                            ddl_timebefore_approve.Enabled = true;
                            ddl_timeafter_approve.Enabled = true;
                            ddl_timeholiday_approve.Enabled = true;

                            txt_remark_admin_edit.Enabled = true;

                            break;

                        case 12: //head1,2 approve

                            lbl_ddl_timestart_approve.Visible = true;
                            lbl_ddl_timeend_approve.Visible = true;

                            ddl_timebefore_approve.Enabled = true;
                            ddl_timeafter_approve.Enabled = true;
                            ddl_timeholiday_approve.Enabled = true;

                            txt_comment_head_approve.Enabled = true;

                            break;
                        case 13: //hr check document

                            lbl_ddl_timestart_approve.Visible = true;
                            lbl_ddl_timeend_approve.Visible = true;

                            txt_comment_hr_check.Enabled = true;

                            break;
                    }

                }
                break;

            case "GvWaitAdminEditDoc":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {



                    Label lbl_u1_doc_idx_edit = (Label)e.Row.FindControl("lbl_u1_doc_idx_edit");
                    Label lbl_emp_code_otday_edit = (Label)e.Row.FindControl("lbl_emp_code_otday_edit");
                    Label lbl_emp_idx_edit = (Label)e.Row.FindControl("lbl_emp_idx_edit");
                    Label lbl_datetime_otstart_edit = (Label)e.Row.FindControl("lbl_datetime_otstart_edit");

                    Label lbl_parttime_start_time = (Label)e.Row.FindControl("lbl_parttime_start_time_edit");
                    Label lbl_parttime_end_time = (Label)e.Row.FindControl("lbl_parttime_end_time_edit");


                    DropDownList ddl_timebefore_edit = (DropDownList)e.Row.FindControl("ddl_timebefore_edit");
                    DropDownList ddl_timeafter_edit = (DropDownList)e.Row.FindControl("ddl_timeafter_edit");
                    DropDownList ddl_timeholiday_edit = (DropDownList)e.Row.FindControl("ddl_timeholiday_edit");

                    Label lbl_ot_before = (Label)e.Row.FindControl("lbl_ot_before_edit");
                    Label lbl_ot_after = (Label)e.Row.FindControl("lbl_ot_after_edit");
                    Label lbl_ot_holiday = (Label)e.Row.FindControl("lbl_ot_holiday_edit");


                    Label lbl_day_off = (Label)e.Row.FindControl("lbl_day_off_edit");
                    Label lbl_date_condition = (Label)e.Row.FindControl("lbl_date_condition_edit");
                    Label lbl_holiday_idx = (Label)e.Row.FindControl("lbl_holiday_idx_edit");

                    //Label lbl_time_shiftstart = (Label)e.Row.FindControl("lbl_time_shiftstart");
                    //Label lbl_time_shiftend = (Label)e.Row.FindControl("lbl_time_shiftend");

                    Label lbl_parttime_break_start_time = (Label)e.Row.FindControl("lbl_parttime_break_start_time_edit");
                    Label lbl_parttime_break_end_time = (Label)e.Row.FindControl("lbl_parttime_break_end_time_edit");

                    Label lbl_emp_type_idx = (Label)e.Row.FindControl("lbl_emp_type_idx_edit");

                    Label lbl_hour_otbefore = (Label)e.Row.FindControl("lbl_hour_otbefore_edit");
                    Label lbl_hour_otafter = (Label)e.Row.FindControl("lbl_hour_otafter_edit");
                    Label lbl_hour_otholiday = (Label)e.Row.FindControl("lbl_hour_otholiday_edit");

                    Label lbl_detailholiday = (Label)e.Row.FindControl("lbl_detailholiday_edit");
                    Label lbl_detaildayoff = (Label)e.Row.FindControl("lbl_detaildayoff_edit");

                    Label lbl_datetime_start_edit = (Label)e.Row.FindControl("lbl_datetime_start_edit");
                    Label lbl_datetime_end_edit = (Label)e.Row.FindControl("lbl_datetime_end_edit");


                    CheckBox chk_edit = (CheckBox)e.Row.FindControl("chk_edit");
                    TextBox txt_remark_shift_otholiday = (TextBox)e.Row.FindControl("txt_remark_shift_otholiday_edit");

                    CheckBox chkall_edit = (CheckBox)GvWaitAdminEditDoc.HeaderRow.Cells[0].FindControl("chkall_edit");

                    Label lbl_ot_before_valueedit = (Label)e.Row.FindControl("lbl_ot_before_valueedit");
                    Label lbl_ot_holiday_valueedit = (Label)e.Row.FindControl("lbl_ot_holiday_valueedit");
                    Label lbl_ot_after_valueedit = (Label)e.Row.FindControl("lbl_ot_after_valueedit");

                    ////lbl_detailholiday.Visible = false;
                    ////lbl_detaildayoff.Visible = false;


                    IFormatProvider culture = new CultureInfo("en-US", true);

                    //day off employee
                    string day_off = lbl_day_off.Text;
                    string day_condition = lbl_date_condition.Text;
                    string[] set_test = day_off.Split(',');
                    string day_offemployee = "";

                    double value_otbefore;
                    double value_otafter;
                    DateTime d_finish_addhour_after = new DateTime();

                    var d_start = new DateTime();
                    var d_end = new DateTime();
                    var d_finish = new DateTime();
                    var d_workstart = new DateTime();

                    var DateTime_BreakStart = new DateTime();
                    var DateTime_BreakEnd = new DateTime();

                    double value_total_ot;
                    double value_total_otresult;

                    double value_total_ot_holiday;
                    double value_total_ot_before;
                    double value_total_ot_diff;


                    d_workstart = DateTime.ParseExact((lbl_datetime_otstart_edit.Text + " " + lbl_parttime_start_time.Text), "dd/MM/yyyy HH:mm", culture);
                    DateTime_BreakStart = DateTime.ParseExact((lbl_datetime_otstart_edit.Text + " " + lbl_parttime_break_start_time.Text), "dd/MM/yyyy HH:mm", culture);
                    if (DateTime.ParseExact((lbl_parttime_start_time.Text), "HH:mm", culture) < DateTime.ParseExact((lbl_parttime_end_time.Text), "HH:mm", culture))
                    {

                        d_finish = DateTime.ParseExact((lbl_datetime_otstart_edit.Text + " " + lbl_parttime_end_time.Text), "dd/MM/yyyy HH:mm", culture);
                        DateTime_BreakEnd = DateTime.ParseExact((lbl_datetime_otstart_edit.Text + " " + lbl_parttime_break_end_time.Text), "dd/MM/yyyy HH:mm", culture);



                    }
                    else
                    {

                        d_finish = DateTime.ParseExact((DateTime.ParseExact((lbl_datetime_otstart_edit.Text), "dd/MM/yyyy", culture).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_end_time.Text), "dd/MM/yyyy HH:mm", culture);
                        //DateTime_BreakStart = DateTime.ParseExact(DateTime.ParseExact((txtDateStart_create.Text), "dd/MM/yyyy", culture).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_break_start_time.Text, "dd/MM/yyyy HH:mm", culture);
                        DateTime_BreakEnd = DateTime.ParseExact(DateTime.ParseExact((lbl_datetime_otstart_edit.Text), "dd/MM/yyyy", culture).AddDays(1).ToString("dd/MM/yyyy") + " " + lbl_parttime_break_end_time.Text, "dd/MM/yyyy HH:mm", culture);


                        //litDebug.Text += d_workstart.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish.ToString("dd/MM/yyyy HH:mm");

                        //litDebug.Text += (DateTime.ParseExact((txtDateStart_create.Text), "dd/MM/yyyy", culture).AddDays(1)).ToString("dd/MM/yyyy") + "|";
                    }

                    if (Array.IndexOf(set_test, day_condition.ToString()) > -1)
                    {
                        day_offemployee = "1"; //yes day off

                    }
                    else
                    {
                        day_offemployee = "0"; // no day off

                    }
                    //day off employee


                    //litDebug.Text += day_offemployee.ToString() + "|";
                    if (lbl_holiday_idx.Text != "0")
                    {
                        lbl_detailholiday.Visible = true;

                    }
                    else if (day_offemployee.ToString() == "1")
                    {
                        lbl_detaildayoff.Visible = true;

                    }



                    ////////////////

                    ovt_shifttime_rotate_detail[] _item_ddlStartEdit = (ovt_shifttime_rotate_detail[])ViewState["Vs_WaitEditApproveDetailOTShiftRotateALL"];
                    ovt_shifttime_rotate_detail[] _item_ddlEndEdit = (ovt_shifttime_rotate_detail[])ViewState["Vs_WaitEditApproveDetailOTShiftRotateALL"];


                    ////var _linq_ddlStartEdit = (from data in _item_ddlStartEdit
                    ////                          where
                    ////                          data.u1_doc_idx == int.Parse(lbl_u1_doc_idx_edit.Text)

                    ////                          select data
                    ////                      ).ToList();

                    ////var _linq_ddlEndEdit = (from data in _item_ddlEndEdit
                    ////                        where
                    ////                          data.u1_doc_idx == int.Parse(lbl_u1_doc_idx_edit.Text)

                    ////                        select data
                    ////                      ).ToList();




                    ////// ViewState["Vs_DetailOTImportEmployeeDay"] = _linq_FilterStatusIndex;


                    ////setDdlData(ddl_timestart_edit, _linq_ddlStartEdit, "datetime_start", "datetime_start");
                    ////setDdlData(ddl_timeend_edit, _linq_ddlEndEdit, "datetime_end", "datetime_end");


                    ////////////////

                    //DropDownList ddl_timestart_edit = (DropDownList)e.Row.FindControl("ddl_timestart_edit");
                    //DropDownList ddl_timeend_edit = (DropDownList)e.Row.FindControl("ddl_timeend_edit");


                    //ddl start edit to admin/user

                    data_overtime data_search_date_startedit = new data_overtime();
                    ovt_shifttime_rotatestart_detail u0_searchdate_startedit = new ovt_shifttime_rotatestart_detail();
                    data_search_date_startedit.ovt_shifttime_rotatestart_list = new ovt_shifttime_rotatestart_detail[1];

                    u0_searchdate_startedit.cemp_idx = _emp_idx;
                    u0_searchdate_startedit.rpos_idx_admim = int.Parse(ViewState["rpos_permission"].ToString());

                    u0_searchdate_startedit.emp_code = lbl_emp_code_otday_edit.Text;
                    u0_searchdate_startedit.emp_idx = int.Parse(lbl_emp_idx_edit.Text);
                    u0_searchdate_startedit.search_date = lbl_datetime_otstart_edit.Text;

                    ////u0_searchdate_startedit.holiday_check = int.Parse(lbl_holiday_idx.Text);
                    ////u0_searchdate_startedit.day_off_check = int.Parse(day_offemployee.ToString());

                    data_search_date_startedit.ovt_shifttime_rotatestart_list[0] = u0_searchdate_startedit;
                    data_search_date_startedit = callServicePostOvertime(_urlGetDetailRotateAdminStartEdit1, data_search_date_startedit);

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_date_startedit));
                    //ddl start edit to admin/user

                    ViewState["Vs_ScanInEdit_OT"] = data_search_date_startedit.ovt_shifttime_rotatestart_list;
                    ovt_shifttime_rotatestart_detail[] _item_timestartEdit = (ovt_shifttime_rotatestart_detail[])ViewState["Vs_ScanInEdit_OT"];


                    var _linqTimeStartEdit = (from dt in _item_timestartEdit

                                              where
                                              ////dt.date_start != "" && dt.time_start != ""
                                              ////&& dt.date_start == lbl_date_start.Text && (DateTime.ParseExact((dt.time_start), "dd/MM/yyyy HH:mm", culture_) < d_finish)

                                              dt.date_ot != "" && dt.time_scanin != ""
                                                && dt.emp_code == lbl_emp_code_otday_edit.Text
                                                && (dt.date_ot == lbl_datetime_otstart_edit.Text)
                                                && (
                                                    (DateTime.ParseExact((dt.time_scanin), "dd/MM/yyyy HH:mm", culture) <= DateTime.ParseExact((dt.time_shiftstart), "dd/MM/yyyy HH:mm", culture))

                                                    || (

                                                        DateTime.ParseExact((dt.time_scanin), "dd/MM/yyyy HH:mm", culture) >= DateTime.ParseExact((dt.time_shiftstart), "dd/MM/yyyy HH:mm", culture)

                                                        //&& DateTime.ParseExact((dt.time_scanin), "dd/MM/yyyy HH:mm", culture) <= DateTime.ParseExact((dt.time_shiftend), "dd/MM/yyyy HH:mm", culture) //times
                                                        && DateTime.ParseExact((dt.time_scanin), "dd/MM/yyyy HH:mm", culture).Subtract(DateTime.ParseExact((dt.time_shiftstart), "dd/MM/yyyy HH:mm", culture)).TotalMinutes < 180

                                                    )

                                                    || (
                                                            (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1")
                                                            && (DateTime.ParseExact((dt.time_scanin), "dd/MM/yyyy HH:mm", culture) <= DateTime.ParseExact((dt.time_shiftend), "dd/MM/yyyy HH:mm", culture))
                                                            && dt.time_scanin != ""
                                                        )

                                                )

                                              select new
                                              {
                                                  dt.time_scanin,
                                                  dt.time_shiftend,
                                                  dt.diff_second_start
                                              }).Distinct().ToList();


                    setDdlData(ddl_timestart_edit, _linqTimeStartEdit.ToList(), "time_scanin", "time_scanin");
                    //setDdlData(ddl_timestart_edit, data_search_date_startedit.ovt_shifttime_rotatestart_list, "time_scanin", "time_scanin");


                    //ddl end edit to admin/user

                    data_overtime data_search_date_endedit = new data_overtime();
                    ovt_shifttime_rotateend_detail u0_searchdate_endedit = new ovt_shifttime_rotateend_detail();
                    data_search_date_endedit.ovt_shifttime_rotateend_list = new ovt_shifttime_rotateend_detail[1];

                    u0_searchdate_endedit.cemp_idx = _emp_idx;
                    u0_searchdate_endedit.rpos_idx_admim = int.Parse(ViewState["rpos_permission"].ToString());

                    u0_searchdate_endedit.emp_code = lbl_emp_code_otday_edit.Text;
                    u0_searchdate_endedit.emp_idx = int.Parse(lbl_emp_idx_edit.Text);
                    u0_searchdate_endedit.search_date = lbl_datetime_otstart_edit.Text;

                    //u0_searchdate_endedit.holiday_check = int.Parse(lbl_holiday_idx.Text);
                    //u0_searchdate_endedit.day_off_check = int.Parse(day_offemployee.ToString());


                    data_search_date_endedit.ovt_shifttime_rotateend_list[0] = u0_searchdate_endedit;


                    data_search_date_endedit = callServicePostOvertime(_urlGetDetailRotateAdminEndEdit1, data_search_date_endedit);
                    //litDebug1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_date_endedit));


                    ViewState["Vs_ScanOutEdit_OT"] = data_search_date_endedit.ovt_shifttime_rotateend_list;
                    ovt_shifttime_rotateend_detail[] _item_timeendEdit = (ovt_shifttime_rotateend_detail[])ViewState["Vs_ScanOutEdit_OT"];

                    var _linqTimeEndEdit = (from dt in _item_timeendEdit
                                                //join dt1 in _templist_TimeStart on dt.time_end equals dt1.time_start into ps
                                            where
                                            ////dt.date_end == lbl_date_start.Text 
                                            ////&&

                                            (dt.date_ot != "" && dt.time_scanout != ""
                                            && dt.emp_code == lbl_emp_code_otday_edit.Text
                                            &&
                                                (
                                                    (

                                                        DateTime.ParseExact((dt.time_scanout), "dd/MM/yyyy HH:mm", culture) >= DateTime.ParseExact((dt.time_shiftend), "dd/MM/yyyy HH:mm", culture)
                                                        && dt.emp_code == lbl_emp_code_otday_edit.Text

                                                        && DateTime.ParseExact((lbl_parttime_start_time.Text), "HH:mm", culture) <= DateTime.ParseExact((lbl_parttime_end_time.Text), "HH:mm", culture)

                                                    )

                                                    ||
                                                    (DateTime.ParseExact((dt.time_scanout), "dd/MM/yyyy HH:mm", culture) <= DateTime.ParseExact((dt.time_shiftend), "dd/MM/yyyy HH:mm", culture)
                                                        && DateTime.ParseExact((dt.time_scanout), "dd/MM/yyyy HH:mm", culture) >= DateTime.ParseExact((dt.time_shiftstart), "dd/MM/yyyy HH:mm", culture)
                                                    )

                                                    ||
                                                    (
                                                        DateTime.ParseExact((dt.date_ot), "dd/MM/yyyy", culture) == DateTime.ParseExact((lbl_datetime_otstart_edit.Text), "dd/MM/yyyy", culture).AddDays(1)
                                                        && dt.emp_code == lbl_emp_code_otday_edit.Text && DateTime.ParseExact((lbl_parttime_start_time.Text), "HH:mm", culture) >= DateTime.ParseExact((lbl_parttime_end_time.Text), "HH:mm", culture)

                                                    )

                                                )

                                            )
                                            select new
                                            {
                                                dt.time_scanout,
                                                dt.diff_second_end
                                            }).Distinct().ToList();




                    setDdlData(ddl_timeend_edit, _linqTimeEndEdit.ToList(), "time_scanout", "time_scanout");
                    //setDdlData(ddl_timeend_edit, data_search_date_endedit.ovt_shifttime_rotateend_list, "time_scanout", "time_scanout");
                    //////////ddl end edit to admin/user



                    //set gv row detail show data
                    if (ddl_timestart_edit.SelectedValue != "" && ddl_timeend_edit.SelectedValue != "")
                    {

                        ddl_timestart_edit.SelectedValue = lbl_datetime_start_edit.Text;
                        ddl_timeend_edit.SelectedValue = lbl_datetime_end_edit.Text;


                        if ((ddl_timeend_edit.SelectedItem.ToString() != ddl_timestart_edit.SelectedItem.ToString()) || (ddl_timeend_edit.Items.Count.ToString() != ddl_timestart_edit.Items.Count.ToString()))
                        {
                            e.Row.Visible = true;

                            DateTime DateTime_ScanIn = DateTime.ParseExact(ddl_timestart_edit.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture);
                            DateTime DateTime_ScanOut = DateTime.ParseExact(ddl_timeend_edit.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture);

                            double value;
                            double _value_diffbreaktime;
                            decimal _value_timescan_late;
                            double _value_timescan_late_add;

                            double _value_otafter_holiday;

                            DateTime d_finish_addhour = new DateTime();
                            DateTime d_start_addhour = new DateTime();

                            DateTime d_finish_addhour_holiday = new DateTime();
                            double _value_diff_scanout;


                            double _value_test;
                            double _value_test1;
                            double _value_test2;

                            /*

                            litDebug1.Text += DateTime.ParseExact(ddl_timeend.SelectedItem.Text, "dd/MM/yyyy HH:mm", culture_).ToString("dd/MM/yyyy") + "|";
                            DateTime DateTime_BreakStart = DateTime.ParseExact((lbl_date_start.Text + " " + lbl_work_finish.Text), "dd/MM/yyyy HH:mm", culture_);

                            */

                            //ot before
                            value_otbefore = Convert.ToDouble((Convert.ToDouble(d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                            //litDebug.Text += d_workstart.Subtract(DateTime_ScanIn).TotalMinutes.ToString() + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm");

                            lbl_ot_before.Text = (Math.Floor(value_otbefore * 2) / 2).ToString();//(Math.Round(value_otbefore * 2, MidpointRounding.AwayFromZero) / 2).ToString();


                            //litDebug.Text = lbl_ot_before.Text;

                            getHourToOT(ddl_timebefore_edit, Convert.ToDouble(lbl_ot_before.Text), _before);

                            //ot after
                            if (DateTime_ScanOut > DateTime_ScanIn)
                            {

                                d_finish_addhour_after = d_finish.AddMinutes(30);

                                value_otafter = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);
                                //_value_test = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_after).TotalMinutes.ToString()) / 30) * 0.50);

                                lbl_ot_after.Text = (Math.Floor(value_otafter * 2) / 2).ToString();//(Math.Round((value_otafter * 2)/2, MidpointRounding.AwayFromZero)).ToString();
                                getHourToOT(ddl_timeafter_edit, Convert.ToDouble(lbl_ot_after.Text), _after);



                            }
                            else
                            {
                                ddl_timeafter_edit.Visible = false;
                            }


                            //ot holiday
                            if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1")
                            {
                                //litDebug.Text += day_offemployee.ToString() + "|";
                                if (lbl_holiday_idx.Text != "0")
                                {
                                    lbl_detailholiday.Visible = true;
                                    lbl_detaildayoff.Visible = false;
                                }
                                else if (day_offemployee.ToString() == "1")
                                {
                                    lbl_detaildayoff.Visible = true;
                                    lbl_detailholiday.Visible = false;
                                }

                                //check case new
                                if ((DateTime_ScanIn <= d_workstart || DateTime_ScanIn >= d_workstart) && DateTime_ScanOut >= d_finish)
                                {
                                    //litDebug.Text += (Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString())/30) * 0.50).ToString()) + "|" + DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|"  + DateTime_ShiftEnd.ToString("dd/MM/yyyy HH:mm");
                                    ////decimal value = Convert.ToDecimal((Convert.ToDouble(DateTime_ShiftEnd.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);
                                    ////litDebug1.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                    //litDebug.Text += _value_diffbreaktime.ToString() + "|";



                                    _value_diffbreaktime = DateTime_ScanOut.Subtract(DateTime_BreakStart).TotalMinutes;
                                    _value_diff_scanout = DateTime_ScanOut.Subtract(d_finish).TotalMinutes;

                                    //litDebug1.Text = _value_diff_scanout.ToString() + "|";

                                    if (DateTime_ScanOut >= d_finish && DateTime_ScanIn <= d_workstart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {
                                        //litDebug.Text = "66";

                                        value = Convert.ToDouble((Convert.ToDouble(d_finish.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50 - 1);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);

                                        if (ddl_timeholiday_edit.SelectedValue == "8")//check ot holiday have 8 hour
                                        {

                                            d_finish_addhour_holiday = d_finish.AddMinutes(30);

                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);
                                            lbl_ot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter_edit, Convert.ToDouble(lbl_ot_after.Text), _after);


                                        }
                                    }
                                    else if (DateTime_ScanOut >= d_finish && DateTime_ScanIn >= d_workstart && DateTime_ScanOut > DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {
                                        //litDebug.Text = "22";
                                        //litDebug.Text = DateTime_ScanOut.ToString("dd/MM/yyyy HH:mm");

                                        _value_timescan_late = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanIn.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50);//DateTime_ScanIn.Subtract(DateTime_ShiftStart).TotalMinutes;
                                        _value_timescan_late_add = Convert.ToDouble(Math.Round(_value_timescan_late * 2, MidpointRounding.AwayFromZero) / 2);// _value_timescan_late.ToString();

                                        //litDebug.Text = _value_timescan_late_add.ToString();

                                        d_start_addhour = d_workstart.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                        d_finish_addhour = d_finish.AddHours(Convert.ToDouble(_value_timescan_late_add.ToString()));
                                        // litDebug1.Text += d_start_addhour.ToString("dd/MM/yyyy HH:mm") + "|" + d_finish_addhour.ToString("dd/MM/yyyy HH:mm");

                                        if (_value_diff_scanout >= 60)
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(d_finish_addhour.Subtract(d_start_addhour).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }
                                        else
                                        {
                                            value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        }

                                        //litDebug1.Text += value.ToString();
                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();// (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();

                                        // litDebug.Text = (Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);

                                        if (ddl_timeholiday_edit.SelectedValue == "8")//check ot holiday have 8 hour
                                        {
                                            //litDebug.Text = d_finish_addhour_holiday.ToString();
                                            //d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString(), "dd/MM/yyyy HH:mm", culture);
                                            //_value_otafter_holiday = Convert.ToDecimal((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour).TotalMinutes.ToString()) / 30) * 0.50);
                                            //litDebug1.Text = (Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString(); // _value_otafter_holiday.ToString();
                                            d_finish_addhour_holiday = DateTime.ParseExact(d_finish_addhour.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture).AddMinutes(30);
                                            _value_otafter_holiday = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_finish_addhour_holiday).TotalMinutes.ToString()) / 30) * 0.50);

                                            lbl_ot_after.Text = (Math.Floor(_value_otafter_holiday * 2) / 2).ToString();//(Math.Round(_value_otafter_holiday * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                            getHourToOT(ddl_timeafter_edit, Convert.ToDouble(lbl_ot_after.Text), _after);

                                        }
                                        else
                                        {
                                            getHourToOT(ddl_timeafter_edit, Convert.ToDouble("0"), _after);
                                        }

                                    }
                                    else if (DateTime_ScanOut < d_finish && DateTime_ScanIn >= DateTime_BreakStart)
                                    {

                                        //litDebug.Text = "33";
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                    }
                                    else if (DateTime_ScanOut < d_finish && DateTime_ScanOut >= DateTime_BreakStart && _value_diffbreaktime >= 60)
                                    {

                                        //litDebug.Text = "32";
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50 - 1);
                                        //value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 30) * 0.50);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                    }
                                    else //if (DateTime_ScanOut > DateTime_BreakEnd && DateTime_ScanOut < DateTime_ShiftEnd)
                                    {
                                        //litDebug.Text = "77";
                                        //litDebug.Text += DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|";
                                        value = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(d_workstart).TotalMinutes.ToString()) / 30) * 0.50);

                                        lbl_ot_holiday.Text = (Math.Floor(value * 2) / 2).ToString();//(Math.Round(value * 2, MidpointRounding.AwayFromZero) / 2).ToString();
                                        getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(lbl_ot_holiday.Text), _holiday);
                                    }

                                }
                                else if (DateTime_ScanIn < d_workstart && DateTime_ScanOut < d_finish)
                                {

                                    //litDebug1.Text = "have case new";

                                    //total ot
                                    value_total_ot = Convert.ToDouble((Convert.ToDouble(DateTime_ScanOut.Subtract(DateTime_ScanIn).TotalMinutes.ToString()) / 60));
                                    value_total_otresult = (Math.Floor(value_total_ot * 2) / 2);
                                    //total ot

                                    if (value_total_otresult >= 9.5) //set -1 breaktime
                                    {

                                        value_total_ot_diff = value_total_otresult - 1;


                                        value_total_ot_holiday = 8;
                                        value_total_ot_before = value_total_ot_diff - value_total_ot_holiday;

                                        //litDebug.Text = value_total_ot_before.ToString();
                                        getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(value_total_ot_holiday.ToString()), _holiday);
                                        getHourToOT(ddl_timebefore_edit, Convert.ToDouble(value_total_ot_before.ToString()), _before);

                                    }
                                    else if (value_total_otresult == 9)
                                    {
                                        value_total_ot_diff = value_total_otresult - 1;
                                        value_total_ot_holiday = 8;
                                        getHourToOT(ddl_timeholiday_edit, Convert.ToDouble(value_total_ot_holiday.ToString()), _holiday);
                                    }
                                    else if (value_total_otresult >= 4 && value_total_otresult <= 5)
                                    {
                                        value_total_ot_before = 4;
                                        getHourToOT(ddl_timebefore_edit, Convert.ToDouble(value_total_ot_before.ToString()), _before);
                                    }
                                    else if (value_total_otresult > 5 && value_total_otresult <= 8)
                                    {
                                        value_total_ot_diff = value_total_otresult - 1;

                                        value_total_ot_before = value_total_ot_diff;
                                        getHourToOT(ddl_timebefore_edit, Convert.ToDouble(value_total_ot_before.ToString()), _before);
                                    }

                                }
                            }
                            else
                            {
                                //litDebug.Text += DateTime_ScanIn.ToString("dd/MM/yyyy HH:mm") + "|";
                                lbl_detailholiday.Visible = false;
                                lbl_detaildayoff.Visible = false;
                                ddl_timeholiday_edit.Visible = false;
                            }


                            //litDebug.Text += value_otbefore.ToString() + "|";
                        }
                        else
                        {

                            //litDebug1.Text += ddl_timeend.SelectedItem.ToString() + "|" + ddl_timestart.SelectedItem.ToString() + "|";
                            e.Row.Visible = false;
                        }
                    }
                    else
                    {
                        //litDebug1.Text += ddl_timeend.SelectedItem.ToString() + "|";
                        e.Row.Visible = false;
                    }


                    //set ot x1, x1.5, x2, x3
                    //set ot before
                    if (ddl_timebefore_edit.SelectedValue != "")
                    {

                        //litDebug1.Text += "44";

                        lbl_hour_otbefore.Visible = true;
                        ddl_timebefore_edit.Visible = true;

                        ddl_timebefore_edit.SelectedValue = lbl_ot_before_valueedit.Text;

                        if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                        {
                            if (ddl_timeholiday_edit.SelectedValue == "8")
                            {
                                lbl_hour_otbefore.Text = "x3";
                            }
                            else
                            {
                                lbl_hour_otbefore.Text = "x1";
                            }
                            //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                            //{
                            //    lbl_hour_otbefore.Text = "x1";
                            //}
                            //else
                            //{
                            //    lbl_hour_otbefore.Text = "x2";
                            //}
                        }
                        else
                        {
                            lbl_hour_otbefore.Text = "x1.5";
                        }
                    }
                    else
                    {
                        //litDebug1.Text += "22";

                        lbl_hour_otbefore.Visible = false;
                        ddl_timebefore_edit.Visible = false;
                    }

                    //set ot after
                    if (ddl_timeafter_edit.SelectedValue != "")
                    {

                        lbl_hour_otafter.Visible = true;
                        ddl_timeafter_edit.Visible = true;

                        ddl_timeafter_edit.SelectedValue = lbl_ot_after_valueedit.Text;

                        if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                        {
                            if (ddl_timeholiday_edit.SelectedValue == "8")
                            {
                                lbl_hour_otafter.Text = "x3";
                            }
                            else
                            {
                                lbl_hour_otafter.Text = "x1";
                            }
                            //if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                            //{
                            //    lbl_hour_otbefore.Text = "x1";
                            //}
                            //else
                            //{
                            //    lbl_hour_otbefore.Text = "x2";
                            //}
                        }
                        else
                        {
                            lbl_hour_otafter.Text = "x1.5";
                        }
                    }
                    else
                    {
                        lbl_hour_otafter.Visible = false;
                        ddl_timeafter_edit.Visible = false;
                    }

                    //set ot holiday
                    if (ddl_timeholiday_edit.SelectedValue != "")
                    {

                        lbl_hour_otholiday.Visible = true;
                        ddl_timeholiday_edit.Visible = true;
                        ddl_timeholiday_edit.SelectedValue = lbl_ot_holiday_valueedit.Text;

                        if (lbl_holiday_idx.Text != "0" || day_offemployee.ToString() == "1") //holiday or day off
                        {
                            if (lbl_emp_type_idx.Text == "2") //employee month shift type 2
                            {
                                lbl_hour_otholiday.Text = "x1";
                            }
                            else
                            {
                                lbl_hour_otholiday.Text = "x2";
                            }
                        }

                    }
                    else
                    {
                        lbl_hour_otholiday.Visible = false;
                        ddl_timeholiday_edit.Visible = false;
                    }






                }
                break;

            case "GvDetailOTShiftRotate":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    Label lbl_day_off = (Label)e.Row.FindControl("lbl_day_off_detail");
                    Label lbl_date_condition = (Label)e.Row.FindControl("lbl_date_condition_detail");
                    Label lbl_holiday_idx = (Label)e.Row.FindControl("lbl_holiday_idx_detail");

                    Label lbl_detailholiday = (Label)e.Row.FindControl("lbl_detailholiday_detail");
                    Label lbl_detaildayoff = (Label)e.Row.FindControl("lbl_detaildayoff_detail");

                    lbl_detailholiday.Visible = false;
                    lbl_detaildayoff.Visible = false;

                    //day off employee
                    string day_off = lbl_day_off.Text;
                    string day_condition = lbl_date_condition.Text;
                    string[] set_test = day_off.Split(',');
                    string day_offemployee = "";


                    if (Array.IndexOf(set_test, day_condition.ToString()) > -1)
                    {
                        day_offemployee = "1"; //yes day off

                    }
                    else
                    {
                        day_offemployee = "0"; // no day off

                    }


                    //litDebug.Text += day_offemployee.ToString() + "|";
                    if (lbl_holiday_idx.Text != "0")
                    {
                        lbl_detailholiday.Visible = true;

                    }
                    else if (day_offemployee.ToString() == "1")
                    {
                        lbl_detaildayoff.Visible = true;

                    }


                }
                break;

        }
    }

    protected void getHourToOT(DropDownList ddlNane, double _hour, string setcal)
    {
        ddlNane.AppendDataBoundItems = true;
        ddlNane.Items.Clear();



        if (Convert.ToDouble(_hour) >= 0.5)
        {
            ddlNane.Visible = true;
            double _timescan = Convert.ToDouble(_hour);
            for (double i = 0.0; i <= _timescan; i += 0.5)
            {
                ddlNane.Items.Add((i).ToString());
            }

            ddlNane.Items.FindByValue(Convert.ToString(_timescan)).Selected = true;

            if (setcal == "before")
            {
                ddlNane.SelectedValue = "0";
            }


        }
        else
        {
            ddlNane.Visible = false;
            ddlNane.SelectedValue = "0";
        }
    }

    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {
            case "GvViewDetailReport":
                //for (int rowIndex = GvViewDetailReport.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                //{
                //    GridViewRow currentRow = GvViewDetailReport.Rows[rowIndex];
                //    GridViewRow previousRow = GvViewDetailReport.Rows[rowIndex + 1];

                //    if (((Label)currentRow.Cells[0].FindControl("lbl_dept_name_th_reportview")).Text == ((Label)previousRow.Cells[0].FindControl("lbl_dept_name_th_reportview")).Text)
                //    {
                //        if (previousRow.Cells[0].RowSpan < 2)
                //        {
                //            currentRow.Cells[0].RowSpan = 2;

                //        }
                //        else
                //        {
                //            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                //        }
                //        previousRow.Cells[0].Visible = false;
                //    }


                //    if (((Label)currentRow.Cells[1].FindControl("lbl_costcenter_no_reportview")).Text == ((Label)previousRow.Cells[1].FindControl("lbl_costcenter_no_reportview")).Text)
                //    {
                //        if (previousRow.Cells[1].RowSpan < 2)
                //        {
                //            currentRow.Cells[1].RowSpan = 2;

                //        }
                //        else
                //        {
                //            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;

                //        }
                //        previousRow.Cells[1].Visible = false;
                //    }


                //}
                break;
        }
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;

            switch (cmdName)
            {
                case "cmdRemoveCar":
                    //GridView gvAddCarBooking = (GridView)FvHrApprove.FindControl("gvAddCarBooking");
                    //GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    //int rowIndex = rowSelect.RowIndex;
                    //DataSet dsContacts = (DataSet)ViewState["vsCarDetailList"];
                    //dsContacts.Tables["dsCarDetailTable"].Rows[rowIndex].Delete();
                    //dsContacts.AcceptChanges();
                    //setGridData(gvAddCarBooking, dsContacts.Tables["dsCarDetailTable"]);
                    //if (dsContacts.Tables["dsCarDetailTable"].Rows.Count < 1)
                    //{
                    //    gvAddCarBooking.Visible = false;
                    //}
                    break;

            }
        }
    }

    #endregion gridview

    #region Formview Databind
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            FormView FvName = (FormView)sender;

            switch (FvName.ID)
            {
                case "FvWaitApproveDetail":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {

                    }

                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {


                    }
                    break;

            }
        }
    }
    #endregion Formview Databind

    #region reuse

    protected void initPageLoad()
    {

    }

    protected void initPage()
    {

        //litDebug.Text = ViewState["Vs_Setpermission_Admin"].ToString();
        if (ViewState["Vs_Setpermission_Admin"].ToString() == "1") //admin
        {
            setActiveTab("docDetailAdmin", 0, 0, 0, 0, 0, 0, 0);
        }
        else
        {
            setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);
        }

        // setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);

        ////litDebug.Text = ViewState["time_idx_permission"].ToString();
        //if (ViewState["time_idx_permission"].ToString() == "1") //shift คงที่
        //{
        //    setActiveTab("docDetail", 0, 0, 0, 0, 2, 0, 0);
        //}
        //else if (ViewState["time_idx_permission"].ToString() == "2")
        //{

        //    setActiveTab("docDetailAdminOTDay", 0, 0, 0, 0, 2, 0, 0);
        //    //setActiveTab("docDetail", 0, 0, 0, 0, 1, 0, 0);
        //}
        //else
        //{
        //    setActiveTab("docDetail", 0, 0, 0, 0, 2, 0, 0);
        //}

    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setChkData(CheckBoxList chkName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        chkName.Items.Clear();
        // bind items
        chkName.DataSource = obj;
        chkName.DataTextField = _data_text;
        chkName.DataValueField = _data_value;
        chkName.DataBind();
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_overtime callServicePostOvertime(string _cmdUrl, data_overtime _data_overtime)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_overtime);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_overtime = (data_overtime)_funcTool.convertJsonToObject(typeof(data_overtime), _localJson);

        return _data_overtime;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cempidx, int u1idx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));
        setFormData(fvEmpDetail, FormViewMode.ReadOnly, null);

        //set tab detail
        PanelAdminCreateOT.Visible = false;
        PanelDetailOTCreate.Visible = false;

        

        //settab detail admin
        GvDetailCreateToAdmin.Visible = false;
        setGridData(GvDetailCreateToAdmin, null);

        GvViewDetailCreateToAdmin.Visible = false;
        setGridData(GvViewDetailCreateToAdmin, null);

        Panel_ViewDetailAdminShiftRotate.Visible = false;

        //set tab detail user
        setFormData(fvEmpCreateDetail, FormViewMode.ReadOnly, null);
        _PanelSearchDetailUser.Visible = false;

        GvDetailOTShiftRotate.Visible = false;
        setGridData(GvDetailOTShiftRotate, null);

        div_LogViewDetailOTShiftRotate.Visible = false;

        li4.Attributes.Add("class", "");
        li2.Attributes.Add("class", "");
        li5.Attributes.Add("class", "");

        //set tab wait approve
        _PanelWaitApprove.Visible = false;
        _PanelbtnApprove.Visible = false;
        _PanelbtnAdmin_Edit.Visible = false;
        _PanelSearchWaitApprove.Visible = false;

        GvWaitAdminEditDoc.Visible = false;
        setGridData(GvWaitAdminEditDoc, null);

        _Panel_Employee.Visible = false;
        txt_empcodeCreate.Text = String.Empty;


        switch (activeTab)
        {
            case "docDetail":

                if (uidx == 0) //show detail to admin
                {
                    txt_scanin_search.Text = String.Empty;
                    _PanelSearchDetailUser.Visible = true;
                    getStatusSearchDocument(ddlStatus, 0);

                    getDetailShiftRotateUser(GvDetailOTShiftRotate, uidx);
                }
                else
                {
                    getViewDetailShiftRotateUser(fvEmpCreateDetail, u1idx);

                    div_LogViewDetailOTShiftRotate.Visible = true;
                    getLogViewDetailOTShiftRotate(rptLogViewDetailOTShiftRotate, uidx, u1idx);
                }

                setOntop.Focus();
                break;

            case "docCreate":

                setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                PanelAdminCreateOT.Visible = true;

                getOrganizationList(ddlorgCreate);
                getDepartmentList(ddlrdeptCreate, int.Parse(ddlorgCreate.SelectedValue));
                getSectionList(ddlrsecCreate, int.Parse(ddlorgCreate.SelectedValue), int.Parse(ddlrdeptCreate.SelectedValue));
                getPositionList(ddlrposCreate, int.Parse(ddlorgCreate.SelectedValue), int.Parse(ddlrdeptCreate.SelectedValue), int.Parse(ddlrsecCreate.SelectedValue));
                //getEmployeeList(chkempCreate, int.Parse(ddlorgCreate.SelectedValue), int.Parse(ddlrdeptCreate.SelectedValue), int.Parse(ddlrsecCreate.SelectedValue), int.Parse(ddlrposCreate.SelectedValue));

                setOntop.Focus();
                break;
            case "docApprove":

                switch (_chk_tab)
                {
                    case 0:

                        switch (nodeidx)
                        {
                            case 12: //head user 1/2 approve
                                _PanelWaitApprove.Visible = true;
                                li2.Attributes.Add("class", "active");
                                ViewState["Vs_check_tab_noidx"] = nodeidx.ToString();
                                //divheadapprove_otday.Visible = true;

                                getWaitApproveDetailOTDayAdminCreate(GvWaitApprove, nodeidx, 1);

                                if (ViewState["Vs_WaitApproveDetailOTShiftRotate"] != null)
                                {
                                    _PanelbtnApprove.Visible = true;
                                    getbtnDecisionApprove(rptBindbtnApprove, nodeidx, 0);
                                }

                                break;
                            case 13: //hr approve 

                                txt_dateStart_approve.Text = String.Empty;
                                txt_dateEnd_approve.Text = String.Empty;
                                ddlemptypeApprove.ClearSelection();
                                txt_empcodeApprove.Text = String.Empty;


                                _PanelWaitApprove.Visible = true;
                                li5.Attributes.Add("class", "active");

                                ViewState["Vs_check_tab_noidx"] = nodeidx.ToString();
                                //divheadapprove_otday.Visible = true;

                                getWaitApproveDetailOTDayAdminCreate(GvWaitApprove, nodeidx, 2);

                                _PanelSearchWaitApprove.Visible = true;
                                getddl_Affiliation(ddl_Affiliation, 0);
                                getOrganizationList(ddlorgApprove);
                                getDepartmentList(ddlrdeptApprove, int.Parse(ddlorgApprove.SelectedValue));
                                getSectionList(ddlrsecApprove, int.Parse(ddlorgApprove.SelectedValue), int.Parse(ddlrdeptApprove.SelectedValue));
                                getPositionList(ddlrposApprove, int.Parse(ddlorgApprove.SelectedValue), int.Parse(ddlrdeptApprove.SelectedValue), int.Parse(ddlrsecApprove.SelectedValue));


                                if (ViewState["Vs_WaitApproveDetailOTShiftRotate"] != null)
                                {

                                    _PanelbtnApprove.Visible = true;
                                    getbtnDecisionApprove(rptBindbtnApprove, nodeidx, 0);
                                }


                                break;
                            case 11: // admin edit ot 

                                li4.Attributes.Add("class", "active");
                                ViewState["Vs_check_tab_noidx"] = nodeidx.ToString();

                                //getWaitApproveDetailOTDayAdminCreate(GvWaitApprove, nodeidx, 3);
                                getWaitApproveDetailOTDayAdminEdit(GvWaitAdminEditDoc, 0, 0);
                                if (ViewState["Vs_WaitEditApproveDetailOTShiftRotate"] != null)
                                {
                                    _PanelbtnAdmin_Edit.Visible = true;

                                }

                                //divadminapprove_otday.Visible = true;
                                //getWaitApproveDetailOTDayAdminCreate(GvWaitApproveOTDayAdminCrate, nodeidx, 3);


                                //if (ViewState["Vs_WaitApproveDetailOTDayAdminCreate"] != null)
                                //{
                                //    div_btnApproveAdminEditOTDay.Visible = true;

                                //}
                                break;
                        }
                        break;
                }

                setOntop.Focus();
                break;
            case "docDetailAdmin":

                if (uidx == 0) //detail shift rotate to admin
                {

                    data_overtime data_u0docdetail_admin = new data_overtime();
                    ovt_u0doc_rotate_detail u0doc_admin_detail = new ovt_u0doc_rotate_detail();
                    data_u0docdetail_admin.ovt_u0doc_rotate_list = new ovt_u0doc_rotate_detail[1];

                    u0doc_admin_detail.u0_doc_idx = 0;
                    u0doc_admin_detail.cemp_idx = _emp_idx;
                    u0doc_admin_detail.cemp_rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());

                    data_u0docdetail_admin.ovt_u0doc_rotate_list[0] = u0doc_admin_detail;

                    //data_search_month = callServicePostOvertime(_urlGetDateScanOTMont, data_search_month);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_reportmonth));
                    data_u0docdetail_admin = callServicePostOvertime(_urlGetDetailCreateToAdminShiftRotate, data_u0docdetail_admin);


                    ViewState["Vs_DetailAdminShiftRotate"] = data_u0docdetail_admin.ovt_u0doc_rotate_list;

                    GvDetailCreateToAdmin.Visible = true;
                    setGridData(GvDetailCreateToAdmin, ViewState["Vs_DetailAdminShiftRotate"]);

                }
                else
                {
                    GvDetailCreateToAdmin.Visible = false;
                    Panel_ViewDetailAdminShiftRotate.Visible = true;

                    data_overtime data_u0docdetail_viewadmin = new data_overtime();

                    ovt_u1doc_rotate_detail u1doc_admin_viewadmin = new ovt_u1doc_rotate_detail();
                    data_u0docdetail_viewadmin.ovt_u1doc_rotate_list = new ovt_u1doc_rotate_detail[1];

                    ovt_u0doc_rotate_detail u0doc_admin_viewadmin = new ovt_u0doc_rotate_detail();
                    data_u0docdetail_viewadmin.ovt_u0doc_rotate_list = new ovt_u0doc_rotate_detail[1];

                    u0doc_admin_viewadmin.u0_doc_idx = uidx;

                    data_u0docdetail_viewadmin.ovt_u0doc_rotate_list[0] = u0doc_admin_viewadmin;
                    data_u0docdetail_viewadmin.ovt_u1doc_rotate_list[0] = u1doc_admin_viewadmin;

                    //data_search_month = callServicePostOvertime(_urlGetDateScanOTMont, data_search_month);
                    //
                    data_u0docdetail_viewadmin = callServicePostOvertime(_urlGetDetailCreateToAdminShiftRotate, data_u0docdetail_viewadmin);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_u0docdetail_viewadmin));

                    ViewState["Vs_ViewDetailAdminShiftRotate"] = data_u0docdetail_viewadmin.ovt_u1doc_rotate_list;

                    GvViewDetailCreateToAdmin.Visible = true;
                    setGridData(GvViewDetailCreateToAdmin, ViewState["Vs_ViewDetailAdminShiftRotate"]);
                }

                setOntop.Focus();
                break;


        }
    }

    protected void setActiveTab(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cemp_idx, int u1idx)
    {
        setActiveView(activeTab, uidx, nodeidx, staidx, actor_idx, _chk_tab, _cemp_idx, u1idx);
        switch (activeTab)
        {
            case "docDetail":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");

                ////li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");

                break;
            case "docCreate":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                ////li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");

                break;
            case "docApprove":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                ////li2.Attributes.Add("class", "active");
                li3.Attributes.Add("class", "");
                //li4.Attributes.Add("class", "active");

                break;
            case "docDetailAdmin":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                ////li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
                li4.Attributes.Add("class", "");

                break;


        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }

    protected string getStatus(int status)
    {
        if (status == 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Certificate'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='No Certificate'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        //specified ASP.NET server control at run time.
    }

    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;
        switch (rptName.ID)
        {

            case "rptBindbtnApprove":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    //Repeater rptBindbtnApprove = (Repeater)FvHeadUserApprove.FindControl("rptBindbtnApprove");
                    var chk_coler = (Label)e.Item.FindControl("lbcheck_coler_approve");
                    var btnApprove = (LinkButton)e.Item.FindControl("btnApprove");

                    for (int k = 0; k <= rptBindbtnApprove.Items.Count; k++)
                    {
                        btnApprove.CssClass = ConfigureColors(k);
                        //Console.WriteLine(i);
                    }


                }
                break;

        }
    }

    #endregion reuse

    #region ConfigureColors
    protected string ConfigureColors(int _btnidx)
    {
        string returnResult1 = "";
        if (_btnidx == 0)
        {
            //returnResult1 = "btn btn-success";
            returnResult1 = "btn btn-success";
            //return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else if (_btnidx == 1)
        {
            returnResult1 = "btn btn-danger";
        }
        else if (_btnidx == 2)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (_btnidx == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (_btnidx == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-success";
        }

        return returnResult1;
    }

    protected string ConfigureColorsOTDay(int _btnidx)
    {
        string returnResult1 = "";
        if (_btnidx == 0)
        {
            returnResult1 = "btn btn-danger";
        }
        else if (_btnidx == 1)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (_btnidx == 2)
        {
            returnResult1 = "btn btn-success";
        }
        else if (_btnidx == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (_btnidx == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-success";
        }

        return returnResult1;
    }

    protected string ConfigureColorsOTDayHR(int _btnidx)
    {
        string returnResult1 = "";
        if (_btnidx == 0)
        {
            returnResult1 = "btn btn-warning";
        }
        else if (_btnidx == 1)
        {
            returnResult1 = "btn btn-success";
        }
        else if (_btnidx == 2)
        {
            returnResult1 = "btn btn-danger";
        }
        else if (_btnidx == 3)
        {
            returnResult1 = "btn btn-default";
        }
        else if (_btnidx == 4)
        {
            returnResult1 = "btn btn-info";
        }
        else
        {
            returnResult1 = "btn btn-success";
        }

        return returnResult1;
    }
    #endregion ConfigureColors

    #region data excel

    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName)
    {
        IWorkbook workbook;
        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();
        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();
        }
        else
        {
            throw new Exception("This format is not supported");
        }
        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);
        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }

        //set merge cell
        ////int max_row = dt.Rows.Count;


        for (int i = 0; i < dt.Rows.Count; i++)
        {
            IRow row = sheet1.CreateRow(i + 1);

            //set merge cell
            ////var cra = new NPOI.SS.Util.CellRangeAddress(1, max_row, 0, 0);
            ////sheet1.AddMergedRegion(cra);

            //var cra1 = new NPOI.SS.Util.CellRangeAddress(1, max_row, 1, 1);
            //sheet1.AddMergedRegion(cra1);


            for (int j = 0; j < dt.Columns.Count; j++)
            {
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());
            }
        }
        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);
            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));
                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }
    }

    #endregion data excel
}