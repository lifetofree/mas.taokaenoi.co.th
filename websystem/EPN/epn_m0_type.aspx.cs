﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_epn_m0_type : System.Web.UI.Page
{
    #region service
    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();

    // ชื่อกล่อง  ตัวแปร = new ชื่อกล่อง
    data_certificate_doc _dtepn = new data_certificate_doc();
    data_employee _dtEmployee = new data_employee();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";
    int _emp_idx = 0;

    // เรียก service มาใช้ จาก web config
    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlget_Inserttype = _serviceUrl + ConfigurationManager.AppSettings["urlget_InsertCertificate"];
    static string urlget_Selecttype = _serviceUrl + ConfigurationManager.AppSettings["urlget_SelectCertificate"];
    static string urlget_Deletetype = _serviceUrl + ConfigurationManager.AppSettings["urlget_DeleteCertificate"];
    static string urlget_Updatetype = _serviceUrl + ConfigurationManager.AppSettings["urlget_UpdateCertificate"];
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());
        if (!IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            Select_MasterSystem();
        }

    }
    #region Insert
    protected void Insert_MasterSystem()
    {
        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail datainsert = new certificate_doc_detail();

      
        datainsert.type_name = txtnamecer.Text;
        datainsert.type_status = int.Parse(ddlstatus.SelectedValue);
        datainsert.cemp_idx = _emp_idx;
        datainsert.condition = 1;
        
        _dtepn.certificate_doc_list[0] = datainsert;
        
         // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));

        _dtepn = callServicePost(urlget_Inserttype, _dtepn);
        // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        if (_dtepn.ReturnCode == 1)
        {

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('ขออภัยมีข้อมูลนี้อยู่แล้ว !')", true);
        }


    }
    #endregion

    #region select
    protected void Select_MasterSystem()
    {
        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail dataSelect = new certificate_doc_detail();

         dataSelect.condition = 1;
        _dtepn.certificate_doc_list[0] = dataSelect;

       // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(_dtepn));
       // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
       _dtepn = callServicePost(urlget_Selecttype, _dtepn);

      // Literal1.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));

        _funcTool.setGvData(GvMaster, _dtepn.certificate_doc_list);
    }
    #endregion

    #region Delete
    protected void Delete_MasterSystem()
    {

        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail dedelete = new certificate_doc_detail();

        dedelete.condition = 1;
        dedelete.cemp_idx = _emp_idx;
        dedelete.type_idx = int.Parse(ViewState["type_idx"].ToString());

        _dtepn.certificate_doc_list[0] = dedelete;

        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dteye));

        _dtepn = callServicePost(urlget_Deletetype, _dtepn);

        // litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dteye));
        // setGridData(GvMaster, _dteye.Test_store_eye_list);

    }

    #endregion

    #region update
    protected void Update_MasterSystem()
    {

        _dtepn.certificate_doc_list = new certificate_doc_detail[1];
        certificate_doc_detail dataupdate = new certificate_doc_detail();

        dataupdate.type_name = ViewState["type_name"].ToString();
        dataupdate.type_status = 1;
        dataupdate.condition = 1;
        dataupdate.cemp_idx = _emp_idx;
        dataupdate.type_idx = int.Parse(ViewState["type_idx"].ToString());
        _dtepn.certificate_doc_list[0] = dataupdate;
        // litDebug.Text = "Test"+ HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtepn));
        _dtepn = callServicePost(urlget_Updatetype, _dtepn);

    }
    #endregion

    #region SetGrid
    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }
    #endregion

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                litDebug.Text = e.Row.RowState.ToString();
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();
                Select_MasterSystem();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                Select_MasterSystem();
                btnshow.Visible = false;
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                Select_MasterSystem();
                btnshow.Visible = true;
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int type_idx = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var txtname_edit = (TextBox)GvMaster.Rows[e.RowIndex].FindControl("txtname_edit");
                var ddStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");


                GvMaster.EditIndex = -1;

                ViewState["type_idx"] = type_idx;
                ViewState["type_name"] = txtname_edit.Text;
                ViewState["type_status"] = ddStatusUpdate.SelectedValue;
                btnshow.Visible = true;
                Update_MasterSystem();
                Select_MasterSystem();

                break;
        }
    }

    #endregion

    #region BTN
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();
        int type_idx = 0;


      
        switch (cmdName)
        {

            case "btnCmn":
                btnshow.Visible = false;
                Panel_Add.Visible = true;
              
                break;

            case "btnCmnA":
                Insert_MasterSystem();
                Panel_Add.Visible = false;
                btnshow.Visible = true;
                Select_MasterSystem();
                GvMaster.Visible = true;
                break;

            case "CmdDel":
                type_idx = int.Parse(cmdArg);
                ViewState["type_idx"] = type_idx;
                Panel_Add.Visible = false;
                btnshow.Visible = true;
                Delete_MasterSystem();
                Select_MasterSystem();
                break;

        }

    }


    #endregion


    #region CallService

    protected data_certificate_doc callServicePost(string _cmdUrl, data_certificate_doc _dtepn)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtepn);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtepn = (data_certificate_doc)_funcTool.convertJsonToObject(typeof(data_certificate_doc), _localJson);


        return _dtepn;

    }
    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }
    #endregion

}