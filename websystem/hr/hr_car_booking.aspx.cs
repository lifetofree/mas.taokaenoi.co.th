﻿using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Drawing;
using DotNet.Highcharts;
using DotNet.Highcharts.Options;
using DotNet.Highcharts.Helpers;
using NPOI.SS.Util;

public partial class websystem_hr_hr_car_booking : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();
    service_execute serviceexcute = new service_execute();
    data_employee _dataEmployee = new data_employee();
    data_carbooking _data_carbooking = new data_carbooking();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    //-- employee --//

    //-- car booking --//
    static string _urlGetCbkm0DetailTypeCar = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0DetailTypeCar"];
    static string _urlGetCbkm0Place = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0Place"];
    static string _urlGetCbkm0TypeBooking = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0TypeBooking"];
    static string _urlGetCbkm0TypeTravel = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0TypeTravel"];
    static string _urlSetCbkCarbooking = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkCarbooking"];
    static string _urlGetCbkDetailCarbooking = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkDetailCarbooking"];
    static string _urlGetCbkViewDetailCarbooking = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkViewDetailCarbooking"];
    static string _urlGetCbkLogViewDetailCarbooking = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkLogViewDetailCarbooking"];
    static string _urlGetCbkWaitDetailApprove = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkWaitDetailApprove"];
    static string _urlGetCbkCountWaitApprove = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkCountWaitApprove"];
    static string _urlGetCbkDecisionApprove = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkDecisionApprove"];
    static string _urlSetCbkApproveHeadUser = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkApproveHeadUser"];
    static string _urlGetCbkm0Admin = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0Admin"];
    static string _urlGetCbkm0CarUse = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkm0CarUse"];
    static string _urlGetCbkCarBusy = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkCarBusy"];
    static string _urlSetCbkApproveHrAdmin = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkApproveHrAdmin"];
    static string _urlSetCbkCreateExpensesHr = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkCreateExpensesHr"];
    static string _urlGetCbkDetailCarInBooking = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkDetailCarInBooking"];
    static string _urlGetCbkM0Expenses = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkM0Expenses"];
    static string _urlSetCbkAddUseExpensesCarHr = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkAddUseExpensesCarHr"];
    static string _urlGetCbkViewDetailCarUseHr = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkViewDetailCarUseHr"];
    static string _urlGetCbkViewDetailCarUseExpenses = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkViewDetailCarUseExpenses"];
    static string _urlGetSearchCbkDetailIndex = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchCbkDetailIndex"];
    static string _urlGetSearchCbkDetailTypeCalendar = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchCbkDetailTypeCalendar"];
    static string _urlGetSearchCbkDetailWaitApprove = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchCbkDetailWaitApprove"];
    static string _urlSetCbkUserCancel = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkUserCancel"];
    static string _urlSetCbkAddUseExpensesCarOutHr = _serviceUrl + ConfigurationManager.AppSettings["urlSetCbkAddUseExpensesCarOutHr"];
    static string _urlGetCbkStatusDocumentSearch = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkStatusDocumentSearch"];
    static string _urlGetCbkSearchReportTable = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkSearchReportTable"];
    static string _urlGetCbkSearchReportGraph = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkSearchReportGraph"];
    static string _urlGetCbkSearchCarBusyDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkSearchCarBusyDetail"];
    static string _urlGetCbkViewDetailCarUseOutplan = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkViewDetailCarUseOutplan"];
    static string _urlGetCbkChoiceOutplan = _serviceUrl + ConfigurationManager.AppSettings["urlGetCbkChoiceOutplan"];


    //-- car booking --//


    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;

    int decision_hr_addusecar = 6; //hr add use car

    decimal tot_actual = 0;
    decimal tot_actualoutplan = 0;
    int returnResult = 0;
    int condition_ = 0;
    int _set_statusFilter = 0;
    //set rpos hr tab report carbooking
    string set_rpos_idx_hr = "5881,5882,5883,5897,5896";

    //ViewState["vs_ViewWaitApproveRoomBooking"]

    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());

        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];
        //check permission
        //foreach (int item in rdept_qmr)
        //{
        //    if (_dataEmployee.employee_list[0].rdept_idx == item)
        //    {
        //        _flag_qmr = true;
        //        break;
        //    }
        //}
        ViewState["org_permission"] = _dataEmployee.employee_list[0].org_idx;
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["joblevel_permission"] = _dataEmployee.employee_list[0].jobgrade_level;


        Session["DetailCarUseIDXSearch"] = 0;
        Session["DetailtypecarSearch"] = 0;
        Session["ddlm0car_idxSearch"] = 0;


        //Set Permission Tab Report with HR
        string[] setTabReportHR = set_rpos_idx_hr.Split(',');
        for (int i = 0; i < setTabReportHR.Length; i++)
        {
            if (setTabReportHR[i] == ViewState["rpos_permission"].ToString())
            {
                li4.Visible = true;

                break;
            }
            else
            {
                li4.Visible = false;

            }

        }
        //Set Permission Tab Report with HR

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
            initPageLoad();

            //data table
            getCarDetailList();
            getCarDetailOutplanList();
            //data table

            //count wait detail approve
            getCountWaitDetailApprove(0);
            //linkBtnTrigger(btnSaveCarUseHr);


        }
        linkBtnTrigger(lbDetail);
        linkBtnTrigger(lbCreate);


        //linkBtnTrigger(btnInsertCarUseHr);
    }

    #region set/get bind data

    protected void getStatusDocument(DropDownList ddlName, int _status_idx)
    {

        data_carbooking data_m0_status_detail = new data_carbooking();
        cbk_m0_document_status_detail m0_docstatus_detail = new cbk_m0_document_status_detail();
        data_m0_status_detail.cbk_m0_document_status_list = new cbk_m0_document_status_detail[1];

        //m0_docstatus_detail.condition = 1;

        data_m0_status_detail.cbk_m0_document_status_list[0] = m0_docstatus_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_status_detail = callServicePostCarBooking(_urlGetCbkStatusDocumentSearch, data_m0_status_detail);

        setDdlData(ddlName, data_m0_status_detail.cbk_m0_document_status_list, "status_name", "staidx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานะเอกสาร ---", "0"));
        ddlName.SelectedValue = _status_idx.ToString();

    }

    protected void getStatusDocumentReport(DropDownList ddlName, int _status_idx)
    {

        data_carbooking data_m0_status_detail_report = new data_carbooking();
        cbk_m0_document_status_detail m0_docstatus_detail_report = new cbk_m0_document_status_detail();
        data_m0_status_detail_report.cbk_m0_document_status_list = new cbk_m0_document_status_detail[1];

        m0_docstatus_detail_report.condition = 1;

        data_m0_status_detail_report.cbk_m0_document_status_list[0] = m0_docstatus_detail_report;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_status_detail_report = callServicePostCarBooking(_urlGetCbkStatusDocumentSearch, data_m0_status_detail_report);

        setDdlData(ddlName, data_m0_status_detail_report.cbk_m0_document_status_list, "status_name", "staidx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานะเอกสาร ---", "0"));
        ddlName.SelectedValue = _status_idx.ToString();

    }

    protected void getPlace(DropDownList ddlName, int _place_idx)
    {

        data_carbooking data_m0_place_detail = new data_carbooking();
        cbk_m0_place_detail m0_place_detail = new cbk_m0_place_detail();
        data_m0_place_detail.cbk_m0_place_list = new cbk_m0_place_detail[1];

        m0_place_detail.condition = 1;

        data_m0_place_detail.cbk_m0_place_list[0] = m0_place_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_place_detail = callServicePostCarBooking(_urlGetCbkm0Place, data_m0_place_detail);

        setDdlData(ddlName, data_m0_place_detail.cbk_m0_place_list, "place_name", "place_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานที่ ---", "0"));
        ddlName.SelectedValue = _place_idx.ToString();

    }

    protected void getDetailTypeCar(DropDownList ddlName, int _detail_typecar_idx)
    {

        data_carbooking data_m1_typecar_detail = new data_carbooking();
        cbk_m1_typecar_detail m1_typecar_detail = new cbk_m1_typecar_detail();
        data_m1_typecar_detail.cbk_m1_typecar_list = new cbk_m1_typecar_detail[1];

        m1_typecar_detail.condition = 1;

        data_m1_typecar_detail.cbk_m1_typecar_list[0] = m1_typecar_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m1_typecar_detail = callServicePostCarBooking(_urlGetCbkm0DetailTypeCar, data_m1_typecar_detail);

        setDdlData(ddlName, data_m1_typecar_detail.cbk_m1_typecar_list, "detailtype_car_name", "detailtype_car_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกประเภทรถ ---", "0"));

        if (_detail_typecar_idx > 0)
        {
            ddlName.SelectedValue = _detail_typecar_idx.ToString();

        }


    }

    protected void getTypebooking(DropDownList ddlName, int _typebooking_idx)
    {

        data_carbooking data_type_booking_detail = new data_carbooking();
        cbk_type_booking_detail type_booking_detail = new cbk_type_booking_detail();
        data_type_booking_detail.cbk_type_booking_list = new cbk_type_booking_detail[1];

        data_type_booking_detail.cbk_type_booking_list[0] = type_booking_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_type_booking_detail = callServicePostCarBooking(_urlGetCbkm0TypeBooking, data_type_booking_detail);

        setDdlData(ddlName, data_type_booking_detail.cbk_type_booking_list, "type_booking_name", "type_booking_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกประเภทการจอง ---", "0"));
        ddlName.SelectedValue = _typebooking_idx.ToString();
        //if (_detail_typecar_idx > 0)
        //{
        //    ddlName.SelectedValue = _detail_typecar_idx.ToString();

        //}


    }

    protected void getTypetravel(DropDownList ddlName, int _typebooking_idx, int _type_travel_idx)
    {
        UpdatePanel PanelTypeTravel = (UpdatePanel)FvCreateCarBooking.FindControl("PanelTypeTravel");

        data_carbooking data_type_travel_detail = new data_carbooking();
        cbk_type_travel_detail type_travel_detail = new cbk_type_travel_detail();
        data_type_travel_detail.cbk_type_travel_list = new cbk_type_travel_detail[1];
        type_travel_detail.type_booking_idx = _typebooking_idx;

        data_type_travel_detail.cbk_type_travel_list[0] = type_travel_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_type_travel_detail = callServicePostCarBooking(_urlGetCbkm0TypeTravel, data_type_travel_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_type_travel_detail));
        if (data_type_travel_detail.return_code == 0)
        {
            PanelTypeTravel.Visible = true;

            setDdlData(ddlName, data_type_travel_detail.cbk_type_travel_list, "type_travel_name", "type_travel_idx");
            ddlName.Items.Insert(0, new ListItem("--- เลือกประเภทการเดินทาง ---", "0"));
            ddlName.SelectedValue = _type_travel_idx.ToString();
        }
        else
        {
            PanelTypeTravel.Visible = false;
        }



    }

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกองค์กร ---", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        ddlName.Items.Clear();
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));
    }

    protected void getddlYear(DropDownList ddlName)
    {

        ddlName.AppendDataBoundItems = true;
        ddlName.Items.Clear();

        var currentYear = DateTime.Today.Year;
        for (int i = 5; i >= 0; i--)
        {
            ddlName.Items.Add((currentYear - i).ToString());
        }

        ddlName.Items.FindByValue(Convert.ToString(currentYear)).Selected = true;
    }

    protected void getDetailCarBooking()
    {

        data_carbooking data_u0_document_detail = new data_carbooking();
        cbk_u0_document_detail u0_document_detail = new cbk_u0_document_detail();
        data_u0_document_detail.cbk_u0_document_list = new cbk_u0_document_detail[1];
        u0_document_detail.cemp_idx = _emp_idx;
        u0_document_detail.cemp_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0_document_detail.cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        u0_document_detail.cemp_rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
        u0_document_detail.cemp_joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

        data_u0_document_detail.cbk_u0_document_list[0] = u0_document_detail;


        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_detail));
        data_u0_document_detail = callServicePostCarBooking(_urlGetCbkDetailCarbooking, data_u0_document_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_type_travel_detail));

        ViewState["Va_DetailCarbooking"] = data_u0_document_detail.cbk_u0_document_list;

        GvDetail.Visible = true;
        setGridData(GvDetail, ViewState["Va_DetailCarbooking"]);
    }

    protected void getViewDetailCarBooking(int _u0_doc_idx)
    {

        data_carbooking data_u0_document_view = new data_carbooking();
        cbk_u0_document_detail u0_document_view = new cbk_u0_document_detail();
        data_u0_document_view.cbk_u0_document_list = new cbk_u0_document_detail[1];
        u0_document_view.u0_document_idx = _u0_doc_idx;

        data_u0_document_view.cbk_u0_document_list[0] = u0_document_view;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_u0_document_view = callServicePostCarBooking(_urlGetCbkViewDetailCarbooking, data_u0_document_view);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_type_travel_detail));

        ViewState["vs_ViewDetailCarBooking"] = data_u0_document_view.cbk_u0_document_list;
        FvDetailCarBooking.Visible = true;
        setFormData(FvDetailCarBooking, FormViewMode.ReadOnly, ViewState["vs_ViewDetailCarBooking"]);
    }

    protected void getDetailCarInBooking(int _u0_doc_idx)
    {

        data_carbooking data_u0_document_caruse = new data_carbooking();
        cbk_u2_document_detail u0_document_caruse = new cbk_u2_document_detail();
        data_u0_document_caruse.cbk_u2_document_list = new cbk_u2_document_detail[1];
        u0_document_caruse.u0_document_idx = _u0_doc_idx;

        data_u0_document_caruse.cbk_u2_document_list[0] = u0_document_caruse;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_u0_document_caruse = callServicePostCarBooking(_urlGetCbkDetailCarInBooking, data_u0_document_caruse);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_type_travel_detail));

        ViewState["Va_DetailCarInbooking"] = data_u0_document_caruse.cbk_u2_document_list;

        GvDetailCarInBooking.Visible = true;
        setGridData(GvDetailCarInBooking, ViewState["Va_DetailCarInbooking"]);
    }

    protected void getDetailCarInBookingAddHr(int _u0_doc_idx)
    {

        data_carbooking data_u0_document_caruse_hr = new data_carbooking();
        cbk_u2_document_detail u0_document_caruse_hr = new cbk_u2_document_detail();
        data_u0_document_caruse_hr.cbk_u2_document_list = new cbk_u2_document_detail[1];
        u0_document_caruse_hr.u0_document_idx = _u0_doc_idx;

        data_u0_document_caruse_hr.cbk_u2_document_list[0] = u0_document_caruse_hr;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_u0_document_caruse_hr = callServicePostCarBooking(_urlGetCbkDetailCarInBooking, data_u0_document_caruse_hr);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_type_travel_detail));

        ViewState["Va_DetailCarInbookingByHR"] = data_u0_document_caruse_hr.cbk_u2_document_list;

        GvDetailCarUse.Visible = true;
        setGridData(GvDetailCarUse, ViewState["Va_DetailCarInbookingByHR"]);

        //ViewState["Count_OpenValueGvDetailCarUse"] = GvDetailCarUse.Rows.Count.ToString();
        //litDebug1.Text = GvDetailCarUse.Rows.Count.ToString();

        //linkBtnTrigger(btnSaveCarUseHr);


    }

    protected void getViewDetailCarInBookingAddHr(int _u0_doc_idx, int _car_use_idx)
    {

        Update_ShowDetailCarUse.Visible = false;
        Update_ShowDetailCarUseOut.Visible = false;


        data_carbooking data_u0_document_caruse_view = new data_carbooking();
        cbk_u2_document_detail u0_document_caruse_view = new cbk_u2_document_detail();
        data_u0_document_caruse_view.cbk_u2_document_list = new cbk_u2_document_detail[1];
        u0_document_caruse_view.u0_document_idx = _u0_doc_idx;

        data_u0_document_caruse_view.cbk_u2_document_list[0] = u0_document_caruse_view;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_u0_document_caruse_view = callServicePostCarBooking(_urlGetCbkViewDetailCarUseHr, data_u0_document_caruse_view);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_type_travel_detail));

        ViewState["Vs_ViewDetailCarInbookingByHR"] = data_u0_document_caruse_view.cbk_u2_document_list;

        if (_car_use_idx == 1 || _car_use_idx == 0) //car in
        {
            Update_ShowDetailCarUse.Visible = true;
            setGridData(GvShowDetailCarUse, ViewState["Vs_ViewDetailCarInbookingByHR"]);
            
            if ((int.Parse(data_u0_document_caruse_view.cbk_u2_document_list[0].usecar_outplan.ToString())) != 0)
            {
                //litDebug.Text = data_u0_document_caruse_view.cbk_u2_document_list[0].usecar_outplan.ToString();
                getDetailChoiceOutplan(rdouse_outplandetail,int.Parse(data_u0_document_caruse_view.cbk_u2_document_list[0].usecar_outplan.ToString()));
                //litDebug.Text = _u0_doc_idx.ToString();
                ////rdouse_outplandetail.SelectedValue = data_u0_document_caruse_view.cbk_u2_document_list[0].usecar_outplan.ToString();

                getViewDetailCaruseOutplan(_u0_doc_idx);


            }
            else
            {
                //litDebug.Text = "44444";
                getDetailChoiceOutplan(rdouse_outplandetail, 0);
                getViewDetailCaruseOutplan(_u0_doc_idx);
            }
            



        }
        else
        {
            Update_ShowDetailCarUseOut.Visible = true;
            var btnViewCarDetailOut = (HyperLink)Update_ShowDetailCarUseOut.FindControl("btnViewCarDetailOut");

            string filePath_View = ConfigurationManager.AppSettings["path_flie_carbooking"];
            string directoryName_View = "u0_doc" + _u0_doc_idx.ToString() + "-" + "carout";//_u0_doc_idx.ToString();//lbl_place_name_edit.Text.Trim() + lbl_room_name_en_edit.Text + lblm0_room_idx_edit.Text;

            if (Directory.Exists(Server.MapPath(filePath_View + directoryName_View)))
            {
                string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View + directoryName_View));
                List<ListItem> files = new List<ListItem>();
                foreach (string path in filesPath)
                {
                    string getfiles = "";
                    getfiles = Path.GetFileName(path);
                    btnViewCarDetailOut.NavigateUrl = filePath_View + directoryName_View + "/" + getfiles;

                    //HtmlImage imgedit = new HtmlImage();
                    //imgedit.Src = filePath_View + directoryName_View + "/" + getfiles;
                    //imgedit.Height = 100;
                    //btnViewCarDetailOut.Controls.Add(imgedit);

                }
                btnViewCarDetailOut.Visible = true;
            }
            else
            {
                btnViewCarDetailOut.Visible = false;
            }
        }



    }

    protected void getDetailExpenses(int _expenses_idx)
    {

        data_carbooking data_u0_document_expenses = new data_carbooking();
        cbk_m0_expenses_detail u0_document_expenses = new cbk_m0_expenses_detail();
        data_u0_document_expenses.cbk_m0_expenses_list = new cbk_m0_expenses_detail[1];
        u0_document_expenses.condition = 1;

        data_u0_document_expenses.cbk_m0_expenses_list[0] = u0_document_expenses;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_u0_document_expenses = callServicePostCarBooking(_urlGetCbkM0Expenses, data_u0_document_expenses);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_type_travel_detail));

        ViewState["Va_DetailExpensesHR"] = data_u0_document_expenses.cbk_m0_expenses_list;

        GvExpenses.Visible = true;
        setGridData(GvExpenses, ViewState["Va_DetailExpensesHR"]);
    }

    protected void getDetailChoiceOutplan(RadioButtonList rdo,int _usecar_outplan_idx)
    {

        data_carbooking data_choice_outplan = new data_carbooking();
        cbk_m0_usecar_outplan_detail m0_choice_outplan = new cbk_m0_usecar_outplan_detail();
        data_choice_outplan.cbk_m0_usecar_outplan_list = new cbk_m0_usecar_outplan_detail[1];
        

        data_choice_outplan.cbk_m0_usecar_outplan_list[0] = m0_choice_outplan;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_choice_outplan = callServicePostCarBooking(_urlGetCbkChoiceOutplan, data_choice_outplan);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_type_travel_detail));

        rdo.Items.Clear();
        rdo.AppendDataBoundItems = true;
        rdo.DataSource = data_choice_outplan.cbk_m0_usecar_outplan_list;
        rdo.DataTextField = "usecar_outplan_name";
        rdo.DataValueField = "usecar_outplan_idx";
        rdo.DataBind();

        if(_usecar_outplan_idx != 0)
        {
            rdo.SelectedValue = _usecar_outplan_idx.ToString();
        }
        
        //ViewState["data_test_sample"] = chk_TestDetail.SelectedValue;
    }

    protected void getDetailChoiceOutplanTable(DropDownList ddlName, int _usecar_outplan_idx)
    {

        data_carbooking data_choice_outplan = new data_carbooking();
        cbk_m0_usecar_outplan_detail m0_choice_outplan = new cbk_m0_usecar_outplan_detail();
        data_choice_outplan.cbk_m0_usecar_outplan_list = new cbk_m0_usecar_outplan_detail[1];


        data_choice_outplan.cbk_m0_usecar_outplan_list[0] = m0_choice_outplan;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_choice_outplan = callServicePostCarBooking(_urlGetCbkChoiceOutplan, data_choice_outplan);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_type_travel_detail));
        setDdlData(ddlName, data_choice_outplan.cbk_m0_usecar_outplan_list, "usecar_outplan_name", "usecar_outplan_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานะการใช้รถนอกเส้นทาง ---", "0"));

        //ViewState["data_test_sample"] = chk_TestDetail.SelectedValue;
    }

    protected void getViewDetailCaruseOutplan(int _u0_doc_idx_caroutplan)
    {

        data_carbooking data_u0_docoutplan = new data_carbooking();
        cbk_u4_document_detail u0_document_outplan = new cbk_u4_document_detail();
        data_u0_docoutplan.cbk_u4_document_list = new cbk_u4_document_detail[1];
        u0_document_outplan.u0_document_idx = _u0_doc_idx_caroutplan;

        data_u0_docoutplan.cbk_u4_document_list[0] = u0_document_outplan;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));     
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_docoutplan));
        data_u0_docoutplan = callServicePostCarBooking(_urlGetCbkViewDetailCarUseOutplan, data_u0_docoutplan);
        

        ViewState["Va_DetailCaruseOutplan"] = data_u0_docoutplan.cbk_u4_document_list;
        if(data_u0_docoutplan.return_code == 0)
        {
            GvDetailOutplan.Visible = true;
            setGridData(GvDetailOutplan, ViewState["Va_DetailCaruseOutplan"]);
        }
        else
        {
            GvDetailOutplan.Visible = false;
            ViewState["Va_DetailCaruseOutplan"] = null;
            setGridData(GvDetailOutplan, ViewState["Va_DetailCaruseOutplan"]);
        }


        
    }

    protected void getLogDetailCarBooking(int _u0_doc_idx)
    {

        data_carbooking data_u0_roomdetail_log = new data_carbooking();
        cbk_u1_document_detail u0_roomdetail_log = new cbk_u1_document_detail();
        data_u0_roomdetail_log.cbk_u1_document_list = new cbk_u1_document_detail[1];
        u0_roomdetail_log.u0_document_idx = _u0_doc_idx;

        data_u0_roomdetail_log.cbk_u1_document_list[0] = u0_roomdetail_log;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_u0_roomdetail_log = callServicePostCarBooking(_urlGetCbkLogViewDetailCarbooking, data_u0_roomdetail_log);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_type_travel_detail));

        ViewState["vs_LogDetailCarBooking"] = data_u0_roomdetail_log.cbk_u1_document_list;
        setRepeaterData(rptLogCarBooking, ViewState["vs_LogDetailCarBooking"]);
    }

    protected void getCountWaitDetailApprove(int _setvalue)
    {

        data_carbooking data_u0_document_count = new data_carbooking();
        cbk_u0_document_detail u0_document_count = new cbk_u0_document_detail();
        data_u0_document_count.cbk_u0_document_list = new cbk_u0_document_detail[1];

        u0_document_count.cemp_idx = _emp_idx;
        u0_document_count.cemp_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0_document_count.cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        u0_document_count.cemp_rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
        u0_document_count.cemp_joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

        data_u0_document_count.cbk_u0_document_list[0] = u0_document_count;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b)); 
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_approve));

        data_u0_document_count = callServicePostCarBooking(_urlGetCbkCountWaitApprove, data_u0_document_count);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_count));

        if (data_u0_document_count.cbk_u0_document_list[0].count_waitApprove > 0)
        {
            //litDebug.Text = "1";
            ViewState["vs_CountWaitApprove"] = data_u0_document_count.cbk_u0_document_list[0].count_waitApprove;
            lbApprove.Text = "รายการรออนุมัติ <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
            setActiveTab("docApprove", 0, 0, 0, 0, 0, 0);

        }
        else if(_setvalue == 0)
        {
            //litDebug.Text = "2";
            ViewState["vs_CountWaitApprove"] = 0;
            lbApprove.Text = "รายการรออนุมัติ <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
            setActiveTab("docDetailCar", 0, 0, 0, 0, 0, 0);


        }
        else
        {
            //litDebug.Text = "2";
            ViewState["vs_CountWaitApprove"] = 0;
            lbApprove.Text = "รายการรออนุมัติ <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
            setActiveTab("docDetail", 0, 0, 0, 0, 0, 0);
        }

    }

    protected void getViewDetailWaitCarBooking(int _u0_doc_idx)
    {

        data_carbooking data_u0_document_approve = new data_carbooking();
        cbk_u0_document_detail u0_document_approve = new cbk_u0_document_detail();
        data_u0_document_approve.cbk_u0_document_list = new cbk_u0_document_detail[1];
        u0_document_approve.u0_document_idx = _u0_doc_idx;

        data_u0_document_approve.cbk_u0_document_list[0] = u0_document_approve;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_u0_document_approve = callServicePostCarBooking(_urlGetCbkViewDetailCarbooking, data_u0_document_approve);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_type_travel_detail));

        ViewState["vs_ViewDetailWaitCarBooking"] = data_u0_document_approve.cbk_u0_document_list;
        FvDetailWaitApprove.Visible = true;
        setFormData(FvDetailWaitApprove, FormViewMode.ReadOnly, ViewState["vs_ViewDetailWaitCarBooking"]);
    }

    protected void getLogDetailWaitApproveCarBooking(int _u0_doc_idx)
    {

        data_carbooking data_u0_roomdetail_logapprove = new data_carbooking();
        cbk_u1_document_detail u0_roomdetail_logapprove = new cbk_u1_document_detail();
        data_u0_roomdetail_logapprove.cbk_u1_document_list = new cbk_u1_document_detail[1];
        u0_roomdetail_logapprove.u0_document_idx = _u0_doc_idx;

        data_u0_roomdetail_logapprove.cbk_u1_document_list[0] = u0_roomdetail_logapprove;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_u0_roomdetail_logapprove = callServicePostCarBooking(_urlGetCbkLogViewDetailCarbooking, data_u0_roomdetail_logapprove);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_type_travel_detail));

        ViewState["vs_LogDetailWaitApproveCarBooking"] = data_u0_roomdetail_logapprove.cbk_u1_document_list;
        setRepeaterData(rptDetailWaitApprove, ViewState["vs_LogDetailWaitApproveCarBooking"]);
    }

    protected void getDecisionApprove(DropDownList ddlName, int _m0_node_idx, int _decision_idx)
    {

        data_carbooking data_u0_document_node = new data_carbooking();
        cbk_u0_document_detail u0_document_node = new cbk_u0_document_detail();
        data_u0_document_node.cbk_u0_document_list = new cbk_u0_document_detail[1];
        u0_document_node.m0_node_idx = _m0_node_idx;

        data_u0_document_node.cbk_u0_document_list[0] = u0_document_node;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_u0_document_node = callServicePostCarBooking(_urlGetCbkDecisionApprove, data_u0_document_node);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_type_travel_detail));

        setDdlData(ddlName, data_u0_document_node.cbk_u0_document_list, "decision_name", "decision_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกสถานะ ---", "0"));
        ddlName.SelectedValue = _decision_idx.ToString();
    }

    protected void getDetailAdmin(DropDownList ddlName, int _admin_idx)
    {

        data_carbooking data_m0_admin_detail = new data_carbooking();
        cbk_m0_admin_detail m0_admin_detail = new cbk_m0_admin_detail();
        data_m0_admin_detail.cbk_m0_admin_list = new cbk_m0_admin_detail[1];

        data_m0_admin_detail.cbk_m0_admin_list[0] = m0_admin_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_admin_detail = callServicePostCarBooking(_urlGetCbkm0Admin, data_m0_admin_detail);

        setDdlData(ddlName, data_m0_admin_detail.cbk_m0_admin_list, "emp_name_th", "emp_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกผู้ดูแล ---", "0"));
        ddlName.SelectedValue = _admin_idx.ToString();

    }

    protected void getDetailCarUse(DropDownList ddlName, int _m0_car_use_idx)
    {

        data_carbooking data_m0_admin_caruse = new data_carbooking();
        cbk_m0_car_use_detail m0_car_use_detail = new cbk_m0_car_use_detail();
        data_m0_admin_caruse.cbk_m0_car_use_list = new cbk_m0_car_use_detail[1];

        data_m0_admin_caruse.cbk_m0_car_use_list[0] = m0_car_use_detail;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_admin_caruse = callServicePostCarBooking(_urlGetCbkm0CarUse, data_m0_admin_caruse);

        setDdlData(ddlName, data_m0_admin_caruse.cbk_m0_car_use_list, "car_use_name", "car_use_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกประเภทรถที่ใช้ ---", "0"));
        ddlName.SelectedValue = _m0_car_use_idx.ToString();

    }

    //getDetailCarBusy(ddlm0_car_idx, lbl_date_start_approve.Text, lbl_time_start_approve.Text, lbl_date_end_approve.Text, lbl_time_end_approve.Text);
    protected void getDetailCarBusy(DropDownList ddlName, string _date_start, string _time_start, string _date_end, string _time_end, int _m0_car_idx)
    {

        // -- check car busy --//
        data_carbooking data_u0_document_search = new data_carbooking();
        cbk_u0_document_detail u0_document_search = new cbk_u0_document_detail();
        data_u0_document_search.cbk_u0_document_list = new cbk_u0_document_detail[1];

        u0_document_search.date_start = _date_start.ToString() + " " + _time_start.ToString();
        u0_document_search.time_start = _time_start.ToString();
        u0_document_search.date_end = _date_end.ToString() + " " + _time_end.ToString();
        u0_document_search.time_end = _time_end.ToString();
        u0_document_search.cemp_idx = _emp_idx;

        data_u0_document_search.cbk_u0_document_list[0] = u0_document_search;

        data_u0_document_search = callServicePostCarBooking(_urlGetCbkCarBusy, data_u0_document_search);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_search));

        setDdlData(ddlName, data_u0_document_search.cbk_u0_document_list, "car_register", "m0_car_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกรถ ---", "0"));
        ddlName.SelectedValue = _m0_car_idx.ToString();


    }

    protected void getDetailTypeSearchCalendar(DropDownList ddlName, int _ddlDetailtypecarSearch)
    {

        data_carbooking data_m0_searchuser = new data_carbooking();
        cbk_m0_car_detail m0_room_searchuser = new cbk_m0_car_detail();
        data_m0_searchuser.cbk_m0_car_list = new cbk_m0_car_detail[1];

        m0_room_searchuser.detailtype_car_idx = _ddlDetailtypecarSearch;

        data_m0_searchuser.cbk_m0_car_list[0] = m0_room_searchuser;
        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b));        
        data_m0_searchuser = callServicePostCarBooking(_urlGetSearchCbkDetailTypeCalendar, data_m0_searchuser);

        setDdlData(ddlName, data_m0_searchuser.cbk_m0_car_list, "car_register", "m0_car_idx");
        ddlName.Items.Insert(0, new ListItem("--- เลือกทะเบียนรถ ---", "0"));
        //ddlName.SelectedValue = _place_idx.ToString();

    }


    #endregion set/get bind data  

    #region bind graph
    public void getCountBookingReportGraph()
    {

        data_carbooking data_report_countbooking = new data_carbooking();
        cbk_searchreport_car_detail search_report_countbooking = new cbk_searchreport_car_detail();
        data_report_countbooking.cbk_searchreport_car_list = new cbk_searchreport_car_detail[1];

        search_report_countbooking.cemp_idx = _emp_idx;
        search_report_countbooking.DC_YEAR = ddlyear.SelectedValue.ToString();
        search_report_countbooking.DC_Mount = ddlmonth.SelectedValue.ToString();
        search_report_countbooking.staidx = int.Parse(ddlStatusBooking.SelectedValue);
        search_report_countbooking.condition = 0;

        data_report_countbooking.cbk_searchreport_car_list[0] = search_report_countbooking;

        //_local_xml = serviceexcute.actionExec("conn_mas", "data_roombooking", "service_roombooking", data_report_countbooking, 261);
        //data_report_countbooking = (data_roombooking)_funcTool.convertXmlToObject(typeof(data_roombooking), _local_xml);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_countbooking));

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_countbooking));
        data_report_countbooking = callServicePostCarBooking(_urlGetCbkSearchReportGraph, data_report_countbooking);

        ViewState["Vs_ReportCountBookingDept"] = data_report_countbooking.cbk_searchreport_car_list;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_countbooking));

        if (data_report_countbooking.return_code == 0)
        {

            //litDebug.Text = "3333";
            int count = data_report_countbooking.cbk_searchreport_car_list.Length;
            string[] Dept_name = new string[count];
            object[] Count_booking = new object[count];
            int i = 0;
            foreach (var data in data_report_countbooking.cbk_searchreport_car_list)
            {
                Dept_name[i] = data.dept_name_th.ToString();
                Count_booking[i] = data.count_booking.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = Dept_name });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Count of Booking",
                    Data = new Data(Count_booking)
                }
            );
            Update_PanelGraph.Visible = true;
            litReportChart.Text = chart.ToHtmlString();
            // -- -- //

            // -- pie chart --//
            var caseclose = new List<object>();

            foreach (var data in data_report_countbooking.cbk_searchreport_car_list)
            {
                caseclose.Add(new object[] { data.dept_name_th.ToString(), data.count_booking.ToString() });
            }

            Highcharts chart1 = new Highcharts("chart1");
            chart1.SetTitle(new DotNet.Highcharts.Options.Title { Text = "Department / Counts" });
            chart1.InitChart(new DotNet.Highcharts.Options.Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart1.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        //Color = ColorTranslator.FromHtml("#000000"),
                        //ConnectorColor = ColorTranslator.FromHtml("#000000"),
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart1.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart1.SetSeries(new DotNet.Highcharts.Options.Series[]
                 {
                        new DotNet.Highcharts.Options.Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });


            litReportChart1.Text = chart1.ToHtmlString();
            // -- pie chart --//

            //ViewState["Va_DetailCarbooking"] = null;
            setGridData(GvDetailGraph, ViewState["Vs_ReportCountBookingDept"]);
            GvDetailGraph.Columns[2].Visible = true;
            GvDetailGraph.Columns[3].Visible = false;


        }
        else
        {
            Update_PanelGraph.Visible = false;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);

            //litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void getCountCancelBookingReportGraph()
    {

        data_carbooking data_report_countcancelbooking = new data_carbooking();
        cbk_searchreport_car_detail search_report_countcancelbooking = new cbk_searchreport_car_detail();
        data_report_countcancelbooking.cbk_searchreport_car_list = new cbk_searchreport_car_detail[1];

        search_report_countcancelbooking.cemp_idx = _emp_idx;
        search_report_countcancelbooking.DC_YEAR = ddlyear.SelectedValue.ToString();
        search_report_countcancelbooking.DC_Mount = ddlmonth.SelectedValue.ToString();
        search_report_countcancelbooking.staidx = int.Parse(ddlStatusBooking.SelectedValue);
        search_report_countcancelbooking.condition = 1;

        data_report_countcancelbooking.cbk_searchreport_car_list[0] = search_report_countcancelbooking;

        //_local_xml = serviceexcute.actionExec("conn_mas", "data_roombooking", "service_roombooking", data_report_countbooking, 261);
        //data_report_countbooking = (data_roombooking)_funcTool.convertXmlToObject(typeof(data_roombooking), _local_xml);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_countbooking));

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_countcancelbooking));
        data_report_countcancelbooking = callServicePostCarBooking(_urlGetCbkSearchReportGraph, data_report_countcancelbooking);

        ViewState["Vs_ReportCountBookingDept"] = data_report_countcancelbooking.cbk_searchreport_car_list;

        if (data_report_countcancelbooking.return_code == 0)
        {

            //litDebug.Text = "3333";
            int count = data_report_countcancelbooking.cbk_searchreport_car_list.Length;
            string[] Dept_name = new string[count];
            object[] Count_booking = new object[count];
            int i = 0;
            foreach (var data in data_report_countcancelbooking.cbk_searchreport_car_list)
            {
                Dept_name[i] = data.dept_name_th.ToString();
                Count_booking[i] = data.count_booking.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = Dept_name });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Count of Booking",
                    Data = new Data(Count_booking)
                }
            );
            Update_PanelGraph.Visible = true;
            litReportChart.Text = chart.ToHtmlString();
            // -- -- //

            // -- pie chart --//
            var caseclose = new List<object>();

            foreach (var data in data_report_countcancelbooking.cbk_searchreport_car_list)
            {
                caseclose.Add(new object[] { data.dept_name_th.ToString(), data.count_booking.ToString() });
            }

            Highcharts chart1 = new Highcharts("chart1");
            chart1.SetTitle(new DotNet.Highcharts.Options.Title { Text = "Department / Counts" });
            chart1.InitChart(new DotNet.Highcharts.Options.Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart1.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        //Color = ColorTranslator.FromHtml("#000000"),
                        //ConnectorColor = ColorTranslator.FromHtml("#000000"),
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart1.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart1.SetSeries(new DotNet.Highcharts.Options.Series[]
                 {
                        new DotNet.Highcharts.Options.Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });


            litReportChart1.Text = chart1.ToHtmlString();
            // -- pie chart --//

            //ViewState["Va_DetailCarbooking"] = null;
            setGridData(GvDetailGraph, ViewState["Vs_ReportCountBookingDept"]);
            GvDetailGraph.Columns[2].Visible = true;
            GvDetailGraph.Columns[3].Visible = false;


        }
        else
        {
            Update_PanelGraph.Visible = false;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);

            //litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void getCountExpensesBookingReportGraph()
    {

        condition_ = 2;

        data_carbooking data_report_countcancelbooking = new data_carbooking();
        cbk_searchreport_car_detail search_report_countcancelbooking = new cbk_searchreport_car_detail();
        data_report_countcancelbooking.cbk_searchreport_car_list = new cbk_searchreport_car_detail[1];

        search_report_countcancelbooking.cemp_idx = _emp_idx;
        search_report_countcancelbooking.DC_YEAR = ddlyear.SelectedValue.ToString();
        search_report_countcancelbooking.DC_Mount = ddlmonth.SelectedValue.ToString();
        search_report_countcancelbooking.staidx = int.Parse(ddlStatusBooking.SelectedValue);
        search_report_countcancelbooking.condition = 2;

        data_report_countcancelbooking.cbk_searchreport_car_list[0] = search_report_countcancelbooking;

        //_local_xml = serviceexcute.actionExec("conn_mas", "data_roombooking", "service_roombooking", data_report_countbooking, 261);
        //data_report_countbooking = (data_roombooking)_funcTool.convertXmlToObject(typeof(data_roombooking), _local_xml);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_countbooking));

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_countcancelbooking));
        data_report_countcancelbooking = callServicePostCarBooking(_urlGetCbkSearchReportGraph, data_report_countcancelbooking);

        ViewState["Vs_ReportCountBookingDept"] = data_report_countcancelbooking.cbk_searchreport_car_list;

        if (data_report_countcancelbooking.return_code == 0)
        {

            //litDebug.Text = "3333";
            int count = data_report_countcancelbooking.cbk_searchreport_car_list.Length;
            string[] Dept_name = new string[count];
            object[] Countsum_expenses_booking = new object[count];
            int i = 0;
            foreach (var data in data_report_countcancelbooking.cbk_searchreport_car_list)
            {
                Dept_name[i] = data.dept_name_th.ToString();
                Countsum_expenses_booking[i] = data.sum_expenses.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = Dept_name });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Count Expenses of Booking",
                    Data = new Data(Countsum_expenses_booking)
                }
            );
            Update_PanelGraph.Visible = true;
            litReportChart.Text = chart.ToHtmlString();
            // -- -- //

            // -- pie chart --//
            var caseclose = new List<object>();

            foreach (var data in data_report_countcancelbooking.cbk_searchreport_car_list)
            {
                caseclose.Add(new object[] { data.dept_name_th.ToString(), data.sum_expenses.ToString() });
            }

            Highcharts chart1 = new Highcharts("chart1");
            chart1.SetTitle(new DotNet.Highcharts.Options.Title { Text = "Department / Counts" });
            chart1.InitChart(new DotNet.Highcharts.Options.Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart1.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        //Color = ColorTranslator.FromHtml("#000000"),
                        //ConnectorColor = ColorTranslator.FromHtml("#000000"),
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart1.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart1.SetSeries(new DotNet.Highcharts.Options.Series[]
                 {
                        new DotNet.Highcharts.Options.Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });


            litReportChart1.Text = chart1.ToHtmlString();
            // -- pie chart --//

            //ViewState["Va_DetailCarbooking"] = null;
            setGridData(GvDetailGraph, ViewState["Vs_ReportCountBookingDept"]);
            GvDetailGraph.Columns[2].Visible = false;
            GvDetailGraph.Columns[3].Visible = true;

        }
        else
        {
            Update_PanelGraph.Visible = false;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);

            //litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }

    public void getCountNotUseCarBookingReportGraph()
    {

        data_carbooking data_report_countnotusebooking = new data_carbooking();
        cbk_searchreport_car_detail search_report_countnotusebooking = new cbk_searchreport_car_detail();
        data_report_countnotusebooking.cbk_searchreport_car_list = new cbk_searchreport_car_detail[1];

        search_report_countnotusebooking.cemp_idx = _emp_idx;
        search_report_countnotusebooking.DC_YEAR = ddlyear.SelectedValue.ToString();
        search_report_countnotusebooking.DC_Mount = ddlmonth.SelectedValue.ToString();
        search_report_countnotusebooking.staidx = int.Parse(ddlStatusBooking.SelectedValue);
        search_report_countnotusebooking.condition = 3;

        data_report_countnotusebooking.cbk_searchreport_car_list[0] = search_report_countnotusebooking;

        //_local_xml = serviceexcute.actionExec("conn_mas", "data_roombooking", "service_roombooking", data_report_countbooking, 261);
        //data_report_countbooking = (data_roombooking)_funcTool.convertXmlToObject(typeof(data_roombooking), _local_xml);

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_countbooking));

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_countnotusebooking));
        data_report_countnotusebooking = callServicePostCarBooking(_urlGetCbkSearchReportGraph, data_report_countnotusebooking);

        ViewState["Vs_ReportCountBookingDept"] = data_report_countnotusebooking.cbk_searchreport_car_list;

        if (data_report_countnotusebooking.return_code == 0)
        {

            //litDebug.Text = "3333";
            int count = data_report_countnotusebooking.cbk_searchreport_car_list.Length;
            string[] Dept_name = new string[count];
            object[] Count_booking = new object[count];
            int i = 0;
            foreach (var data in data_report_countnotusebooking.cbk_searchreport_car_list)
            {
                Dept_name[i] = data.dept_name_th.ToString();
                Count_booking[i] = data.count_booking.ToString();
                i++;
            }
            Highcharts chart = new Highcharts("chart");
            chart.SetTitle(new DotNet.Highcharts.Options.Title { Text = "" });
            chart.SetYAxis(new[] { new YAxis { Title = new YAxisTitle { Text = "Number of times" } } });
            chart.SetXAxis(new XAxis { Categories = Dept_name });
            chart.SetSeries(
                new DotNet.Highcharts.Options.Series
                {
                    Type = DotNet.Highcharts.Enums.ChartTypes.Column,
                    Name = "Count of Booking",
                    Data = new Data(Count_booking)
                }
            );
            Update_PanelGraph.Visible = true;
            litReportChart.Text = chart.ToHtmlString();
            // -- -- //

            // -- pie chart --//
            var caseclose = new List<object>();

            foreach (var data in data_report_countnotusebooking.cbk_searchreport_car_list)
            {
                caseclose.Add(new object[] { data.dept_name_th.ToString(), data.count_booking.ToString() });
            }

            Highcharts chart1 = new Highcharts("chart1");
            chart1.SetTitle(new DotNet.Highcharts.Options.Title { Text = "Department / Counts" });
            chart1.InitChart(new DotNet.Highcharts.Options.Chart { DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Pie });

            chart1.SetPlotOptions(new PlotOptions
            {
                Pie = new PlotOptionsPie
                {
                    ShowInLegend = true,
                    AllowPointSelect = true,
                    DataLabels = new PlotOptionsPieDataLabels
                    {
                        Enabled = true,
                        // Formatter = "function() { return '" + this. + ' : '" + lv1count + "';}" //  this.percentage.toFixed(2)+' % '; }"
                        //Color = ColorTranslator.FromHtml("#000000"),
                        //ConnectorColor = ColorTranslator.FromHtml("#000000"),
                        Formatter = "function() { return this.point.name +' : '+  this.y.toFixed(0)+' '; }"
                    }
                }
            });

            chart1.SetTooltip(new Tooltip
            {
                Enabled = true,
                Formatter = "function() { return this.point.name +' : '+  this.percentage.toFixed(2)+' % '; }"

            });
            chart1.SetSeries(new DotNet.Highcharts.Options.Series[]
                 {
                        new DotNet.Highcharts.Options.Series()
                        {
                            Data = new Data (caseclose.ToArray())
                        }
                 });


            litReportChart1.Text = chart1.ToHtmlString();
            // -- pie chart --//

            //ViewState["Va_DetailCarbooking"] = null;
            setGridData(GvDetailGraph, ViewState["Vs_ReportCountBookingDept"]);
            GvDetailGraph.Columns[2].Visible = true;
            GvDetailGraph.Columns[3].Visible = false;


        }
        else
        {
            Update_PanelGraph.Visible = false;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);

            //litReportChart.Text = "<p class='bg-danger'>No result.</p>";
        }
    }


    #endregion end graph

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, 0, 0, 0);

    }

    protected void ClickPrint(object sender, EventArgs e)
    {
        //  litDebug.Text = "lals";
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        //linkBtnTrigger(lbDocSaveImport);

        switch (cmdName)
        {

            case "cmdSave":

                DropDownList ddlDetailtypecar = (DropDownList)FvCreateCarBooking.FindControl("ddlDetailtypecar");
                DropDownList ddlType_booking_idx = (DropDownList)FvCreateCarBooking.FindControl("ddlType_booking_idx");
                DropDownList ddlType_travel_idx = (DropDownList)FvCreateCarBooking.FindControl("ddlType_travel_idx");
                DropDownList ddlPlace = (DropDownList)FvCreateCarBooking.FindControl("ddlPlace");
                TextBox txt_PlaceOther = (TextBox)FvCreateCarBooking.FindControl("txt_PlaceOther");
                TextBox txt_distance = (TextBox)FvCreateCarBooking.FindControl("txt_distance");
                TextBox txt_time_length = (TextBox)FvCreateCarBooking.FindControl("txt_time_length");
                TextBox txt_note_booking = (TextBox)FvCreateCarBooking.FindControl("txt_note_booking");
                TextBox txtDateStart_create = (TextBox)FvCreateCarBooking.FindControl("txtDateStart_create");
                TextBox txt_timestart_create = (TextBox)FvCreateCarBooking.FindControl("txt_timestart_create");
                TextBox txtDateEnd_create = (TextBox)FvCreateCarBooking.FindControl("txtDateEnd_create");
                TextBox txt_timeend_create = (TextBox)FvCreateCarBooking.FindControl("txt_timeend_create");
                TextBox txt_objective_booking = (TextBox)FvCreateCarBooking.FindControl("txt_objective_booking");
                TextBox txt_count_travel = (TextBox)FvCreateCarBooking.FindControl("txt_count_travel");
                FileUpload UploadFileCreateCarUser = (FileUpload)FvCreateCarBooking.FindControl("UploadFileCreateCarUser");

                //
                ViewState["va_DateStart_Insert_CheckInsertDate"] = txtDateStart_create.Text + " " + txt_timestart_create.Text;
                //ViewState["va_TimeStart_Insert"] = txt_timestart_create.Text;
                ViewState["va_DateEnd_Insert_CheckInsertDate"] = txtDateEnd_create.Text + " " + txt_timeend_create.Text;
                //


                //DateTime starting = new DateTime();
                DateTime Datestarting = DateTime.ParseExact(txtDateStart_create.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                //DateTime ending = new DateTime();
                DateTime Dateending = DateTime.ParseExact(txtDateEnd_create.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                DateTime tempStartTime = DateTime.ParseExact(txt_timestart_create.Text, "HH:mm", CultureInfo.InvariantCulture);
                DateTime tempEndTime = DateTime.ParseExact(txt_timeend_create.Text, "HH:mm", CultureInfo.InvariantCulture);

                //// all room detail online ////
                // 
                IFormatProvider culture = new CultureInfo("en-US", true);
                DateTime DateToday = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                DateTime dateVal_start = DateTime.ParseExact(ViewState["va_DateStart_Insert_CheckInsertDate"].ToString(), "dd/MM/yyyy HH:mm", culture);
                DateTime dateVal_end = DateTime.ParseExact(ViewState["va_DateEnd_Insert_CheckInsertDate"].ToString(), "dd/MM/yyyy HH:mm", culture);

                var result_time_value = dateVal_start.Subtract(DateToday).TotalMinutes; // result time chrck >= 60 min because cancel in room
                //litDebug.Text = result_time_value.ToString();

                // check time before insert room booking
                if (dateVal_start >= DateToday && dateVal_start < dateVal_end)
                {
                    //booking car before 240 minute 4hr  
                    if(result_time_value > 240)
                    {
                        data_carbooking data_u0_document_detail = new data_carbooking();
                        cbk_u0_document_detail u0_document_detail = new cbk_u0_document_detail();
                        data_u0_document_detail.cbk_u0_document_list = new cbk_u0_document_detail[1];

                        u0_document_detail.detailtype_car_idx = int.Parse(ddlDetailtypecar.SelectedValue);
                        u0_document_detail.type_booking_idx = int.Parse(ddlType_booking_idx.SelectedValue);
                        u0_document_detail.type_travel_idx = int.Parse(ddlType_travel_idx.SelectedValue);
                        u0_document_detail.place_idx = int.Parse(ddlPlace.SelectedValue);
                        u0_document_detail.place_name_other = txt_PlaceOther.Text;
                        u0_document_detail.distance = txt_distance.Text;
                        u0_document_detail.time_length = txt_time_length.Text;
                        u0_document_detail.note_booking = txt_note_booking.Text;
                        u0_document_detail.date_start = txtDateStart_create.Text + " " + txt_timestart_create.Text;
                        u0_document_detail.time_start = txt_timestart_create.Text;
                        u0_document_detail.date_end = txtDateEnd_create.Text + " " + txt_timeend_create.Text;
                        u0_document_detail.time_end = txt_timeend_create.Text;
                        u0_document_detail.objective_booking = txt_objective_booking.Text;
                        u0_document_detail.count_travel = txt_count_travel.Text;
                        u0_document_detail.cemp_idx = _emp_idx;
                        u0_document_detail.cemp_org_idx = int.Parse(ViewState["org_permission"].ToString());
                        u0_document_detail.cemp_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                        u0_document_detail.cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                        u0_document_detail.m0_actor_idx = 1;
                        u0_document_detail.m0_node_idx = 1;
                        u0_document_detail.decision = 0;

                        data_u0_document_detail.cbk_u0_document_list[0] = u0_document_detail;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_detail));
                        //if (UploadFileCreateCarUser != null)
                        //{
                            
                        if (UploadFileCreateCarUser.HasFile)
                        {
                            //litDebug.Text = "have file";
                            //check extention in file before save
                            string extension = Path.GetExtension(UploadFileCreateCarUser.PostedFile.FileName);

                            if (extension.ToLower() == ".jpg" || extension.ToLower() == ".png")
                            {

                                data_u0_document_detail = callServicePostCarBooking(_urlSetCbkCarbooking, data_u0_document_detail);

                                if (data_u0_document_detail.return_code == 0)
                                {
                                    //doc idx create file
                                    var _u0_docidx_create = data_u0_document_detail.cbk_u0_document_list[0].u0_document_idx;

                                    //litDebug.Text = "save ok";
                                    string getPathfile = ConfigurationManager.AppSettings["path_flie_carbooking_create"];
                                    string car_file_namecreate = "u0_doc" + _u0_docidx_create.ToString();//ViewState["vs_createRoom_DetailFile"].ToString();
                                    string fileName_upload1_create = "u0_doc" + _u0_docidx_create.ToString();

                                    string fileName_upload_create = car_file_namecreate + '-' + fileName_upload1_create;
                                    string filePath_upload = Server.MapPath(getPathfile + fileName_upload_create);

                                    if (!Directory.Exists(filePath_upload))
                                    {
                                        Directory.CreateDirectory(filePath_upload);
                                    }
                                    //string extension = Path.GetExtension(UploadFileCreateCarUser.FileName);
                                    UploadFileCreateCarUser.SaveAs(Server.MapPath(getPathfile + fileName_upload_create) + "\\" + fileName_upload1_create + extension);
                                    setActiveTab("docDetail", 0, 0, 0, 0, 0, 0);
                                }
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เฉพาะไฟล์ Excel (นามสกุลไฟล์ .jpg หรือ .png) เท่านั้น');", true);
                                //check_file = 1;
                                break;
                            }


                        }
                        else
                        {
                            //litDebug.Text = "no file";

                            data_u0_document_detail = callServicePostCarBooking(_urlSetCbkCarbooking, data_u0_document_detail);
                            //litDebug.Text = "33333";
                            if (data_u0_document_detail.return_code == 0)
                            {

                                setActiveTab("docDetail", 0, 0, 0, 0, 0, 0);
                            }
                            else
                            {
                                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- มีข้อมูลในระบบแล้ว ---');", true);
                                break;
                            }

                        }
                        
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณาทำรายการจองก่อนวันเดินทาง 4 ชั่วโมง ---');", true);
                        break;
                    }

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ตรวจสอบวันที่และเวลา ก่อนทำการบันทึก ---');", true);
                    break;
                }

                break;
            case "cmdCancel":

                getCountWaitDetailApprove(0);

                break;

            case "cmdSearchCarBusy":

                DropDownList ddlDetailtypecar_search = (DropDownList)FvCreateCarBooking.FindControl("ddlDetailtypecar");
                TextBox txtDateStart_search = (TextBox)FvCreateCarBooking.FindControl("txtDateStart_create");
                TextBox txt_timestart_search = (TextBox)FvCreateCarBooking.FindControl("txt_timestart_create");
                TextBox txtDateEnd_search = (TextBox)FvCreateCarBooking.FindControl("txtDateEnd_create");
                TextBox txt_timeend_search = (TextBox)FvCreateCarBooking.FindControl("txt_timeend_create");
                UpdatePanel Update_DetailCarSearchCreate = (UpdatePanel)FvCreateCarBooking.FindControl("Update_DetailCarSearchCreate");
                UpdatePanel Update_SaveFileCreateCarUser = (UpdatePanel)FvCreateCarBooking.FindControl("Update_SaveFileCreateCarUser");

                data_carbooking data_search_detail = new data_carbooking();
                cbk_u0_document_detail search_detail = new cbk_u0_document_detail();
                data_search_detail.cbk_u0_document_list = new cbk_u0_document_detail[1];

                search_detail.detailtype_car_idx = int.Parse(ddlDetailtypecar_search.SelectedValue);
                search_detail.date_start = txtDateStart_search.Text + " " + txt_timestart_search.Text;
                search_detail.time_start = txt_timestart_search.Text;
                search_detail.date_end = txtDateEnd_search.Text + " " + txt_timeend_search.Text;
                search_detail.time_end = txt_timeend_search.Text;


                data_search_detail.cbk_u0_document_list[0] = search_detail;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_detail));

                if(int.Parse(ddlDetailtypecar_search.SelectedValue) != 0 && txtDateStart_search.Text != "" && txtDateEnd_search.Text != "" && txt_timestart_search.Text != "" && txt_timeend_search.Text != "")
                {
                    data_search_detail = callServicePostCarBooking(_urlGetCbkSearchCarBusyDetail, data_search_detail);


                    Update_DetailCarSearchCreate.Visible = true;
                    Update_SaveFileCreateCarUser.Visible = true;
                    setGridData(GvDetailCarSearchCreate, data_search_detail.cbk_u0_document_list);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- เลือกข้อมูลไม่ครบถ้วน ---');", true);
                    break;
                    //Update_DetailCarSearchCreate.Visible = true;
                    //Update_SaveFileCreateCarUser.Visible = true;
                    //setGridData(GvDetailCarSearchCreate, data_search_detail.cbk_u0_document_list);
                }
                

                break;

            case "cmdViewDetail":

                linkBtnTrigger(btnViewDetail);
                string[] arg2 = new string[5];
                arg2 = e.CommandArgument.ToString().Split(';');
                int _u0_document_idx_view = int.Parse(arg2[0]);
                int _m0_node_idx_view = int.Parse(arg2[1]);
                int _m0_actor_idx_view = int.Parse(arg2[2]);
                int _staidx_view = int.Parse(arg2[3]);
                int _cemp_idx_view = int.Parse(arg2[4]);
                int _car_use_idx_view = int.Parse(arg2[5]);

                ViewState["Vs_cemp_idx_view"] = _cemp_idx_view;

                setActiveTab("docDetail", _u0_document_idx_view, _m0_node_idx_view, _staidx_view, _m0_actor_idx_view, 1, _car_use_idx_view);
                //linkBtnTrigger(btnViewDetail);
                break;

            case "cmdBackToDetail":
                setActiveTab("docDetail", 0, 0, 0, 0, 0, 0);
                break;

            case "cmdViewDetailApprove":

                string[] arg3 = new string[4];
                arg3 = e.CommandArgument.ToString().Split(';');
                int _u0_document_idx_approve = int.Parse(arg3[0]);
                int _m0_node_idx_approve = int.Parse(arg3[1]);
                int _m0_actor_idx_approve = int.Parse(arg3[2]);
                int _staidx_approve = int.Parse(arg3[3]);
                int _cemp_idx_approve = int.Parse(arg3[4]);

                setActiveTab("docApprove", _u0_document_idx_approve, _m0_node_idx_approve, _staidx_approve, _m0_actor_idx_approve, 1, 0);


                break;

            case "cmdBackToWaitDetail":

                setActiveTab("docApprove", 0, 0, 0, 0, 0, 0);
                break;

            case "cmdSaveApprove":

                HiddenField hfu0_document_idx = (HiddenField)FvDetailWaitApprove.FindControl("hfu0_document_idx");
                HiddenField hfM0NodeIDX = (HiddenField)FvDetailWaitApprove.FindControl("hfM0NodeIDX");
                HiddenField hfM0ActoreIDX = (HiddenField)FvDetailWaitApprove.FindControl("hfM0ActoreIDX");
                HiddenField hfM0StatusIDX = (HiddenField)FvDetailWaitApprove.FindControl("hfM0StatusIDX");

                DropDownList ddlApproveStatus = (DropDownList)FvHeadUserApprove.FindControl("ddlApproveStatus");
                TextBox txt_cooment_approve = (TextBox)FvHeadUserApprove.FindControl("txt_cooment_approve");


                data_carbooking data_u0_document_headapprove = new data_carbooking();
                cbk_u0_document_detail u0_document_headapprove = new cbk_u0_document_detail();
                data_u0_document_headapprove.cbk_u0_document_list = new cbk_u0_document_detail[1];

                u0_document_headapprove.u0_document_idx = int.Parse(hfu0_document_idx.Value);
                u0_document_headapprove.m0_node_idx = int.Parse(hfM0NodeIDX.Value);
                u0_document_headapprove.m0_actor_idx = int.Parse(hfM0ActoreIDX.Value);
                u0_document_headapprove.staidx = int.Parse(hfM0StatusIDX.Value);
                u0_document_headapprove.decision_idx = int.Parse(ddlApproveStatus.SelectedValue);
                u0_document_headapprove.comment = txt_cooment_approve.Text;
                u0_document_headapprove.cemp_idx = _emp_idx;

                data_u0_document_headapprove.cbk_u0_document_list[0] = u0_document_headapprove;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_headapprove));

                data_u0_document_headapprove = callServicePostCarBooking(_urlSetCbkApproveHeadUser, data_u0_document_headapprove);

                getCountWaitDetailApprove(1);


                break;

            case "cmdAddCar":
                setCarDetailList();

                break;
            case "cmdAddOutplandistanceTolist":

                setCarDetailOutplanList();

                break;

            case "cmdSaveApproveHr":


                HiddenField hfu0_document_idx_insertcar = (HiddenField)FvDetailWaitApprove.FindControl("hfu0_document_idx");
                HiddenField hfM0NodeIDX_insertcar = (HiddenField)FvDetailWaitApprove.FindControl("hfM0NodeIDX");
                HiddenField hfM0ActoreIDX_idx_insertcar = (HiddenField)FvDetailWaitApprove.FindControl("hfM0ActoreIDX");
                HiddenField hfM0StatusIDX_idx_insertcar = (HiddenField)FvDetailWaitApprove.FindControl("hfM0StatusIDX");
                Label lbl_rsec_idx_approve_empcreate = (Label)FvDetailWaitApprove.FindControl("lbl_rsec_idx_approve");

                DropDownList ddlHrApproveStatus_insertcar = (DropDownList)FvHrApprove.FindControl("ddlHrApproveStatus");
                TextBox txt_cooment_approve_insertcar = (TextBox)FvHrApprove.FindControl("txt_cooment_approve");
                DropDownList ddlcar_use_idx_insertcar = (DropDownList)FvHrApprove.FindControl("ddlcar_use_idx");


                data_carbooking data_u0_document_hrapprove = new data_carbooking();
                cbk_u0_document_detail u0_document_hrapprove = new cbk_u0_document_detail();
                data_u0_document_hrapprove.cbk_u0_document_list = new cbk_u0_document_detail[1];

                u0_document_hrapprove.u0_document_idx = int.Parse(hfu0_document_idx_insertcar.Value);
                u0_document_hrapprove.car_use_idx = int.Parse(ddlcar_use_idx_insertcar.SelectedValue);
                u0_document_hrapprove.m0_node_idx = int.Parse(hfM0NodeIDX_insertcar.Value);
                u0_document_hrapprove.m0_actor_idx = int.Parse(hfM0ActoreIDX_idx_insertcar.Value);
                u0_document_hrapprove.staidx = int.Parse(hfM0StatusIDX_idx_insertcar.Value);
                u0_document_hrapprove.decision_idx = int.Parse(ddlHrApproveStatus_insertcar.SelectedValue);
                u0_document_hrapprove.comment = txt_cooment_approve_insertcar.Text;
                u0_document_hrapprove.cemp_idx = _emp_idx;
                u0_document_hrapprove.rsec_idx = int.Parse(lbl_rsec_idx_approve_empcreate.Text);


                //insert car booking
                //u2 use m0 car
                var _dataset_CarDetailList = (DataSet)ViewState["vsCarDetailList"];

                var _add_CarList = new cbk_u2_document_detail[_dataset_CarDetailList.Tables[0].Rows.Count];
                int row_carlist = 0;

                foreach (DataRow dtrow_Carlist in _dataset_CarDetailList.Tables[0].Rows)
                {
                    _add_CarList[row_carlist] = new cbk_u2_document_detail();

                    _add_CarList[row_carlist].u0_document_idx = int.Parse(hfu0_document_idx_insertcar.Value);
                    _add_CarList[row_carlist].m0_car_idx = int.Parse(dtrow_Carlist["drCarDetailIDX"].ToString());
                    _add_CarList[row_carlist].emp_idx_driver = int.Parse(dtrow_Carlist["drAdminCarIDX"].ToString());
                    _add_CarList[row_carlist].car_use_idx = int.Parse(ddlcar_use_idx_insertcar.SelectedValue);
                    _add_CarList[row_carlist].cemp_idx = _emp_idx;

                    row_carlist++;

                }

                data_u0_document_hrapprove.cbk_u0_document_list[0] = u0_document_hrapprove;
                data_u0_document_hrapprove.cbk_u2_document_list = _add_CarList;

                if (ddlHrApproveStatus.SelectedValue == "3") //hr approve
                {
                    if (ddlcar_use_idx.SelectedValue == "1") //car use in 
                    {

                        //litDebug.Text = row_carlist.ToString();
                        if (row_carlist > 0) //have car insert becase use car in
                        {
                            data_u0_document_hrapprove = callServicePostCarBooking(_urlSetCbkApproveHrAdmin, data_u0_document_hrapprove);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_hrapprove));

                            getCountWaitDetailApprove(1);
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- เพิ่มข้อมูลไม่ครบถ้วน ---');", true);
                            break;
                        }

                    }
                    else//car out
                    {
                        data_u0_document_hrapprove = callServicePostCarBooking(_urlSetCbkApproveHrAdmin, data_u0_document_hrapprove);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_hrapprove));

                        getCountWaitDetailApprove(1);
                    }
                }
                else
                {
                    data_u0_document_hrapprove = callServicePostCarBooking(_urlSetCbkApproveHrAdmin, data_u0_document_hrapprove);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_hrapprove));

                    getCountWaitDetailApprove(1);
                }
                break;

            case "cmdInsertCarUseHr":
                //linkBtnTrigger(btnInsertCarUseHr);

                HiddenField hfu0_document_idx_usecar = (HiddenField)FvDetailCarBooking.FindControl("hfu0_document_idx");
                HiddenField hfM0NodeIDX_usecar = (HiddenField)FvDetailCarBooking.FindControl("hfM0NodeIDX");
                HiddenField hfM0ActoreIDX_usecar = (HiddenField)FvDetailCarBooking.FindControl("hfM0ActoreIDX");
                HiddenField hfM0StatusIDX_usecar = (HiddenField)FvDetailCarBooking.FindControl("hfM0StatusIDX");

                setActiveTab("docDetail", int.Parse(hfu0_document_idx_usecar.Value), int.Parse(hfM0NodeIDX_usecar.Value), int.Parse(hfM0StatusIDX_usecar.Value), int.Parse(hfM0ActoreIDX_usecar.Value), 5, 0);

                break;

            case "cmdBackToHrAddUseCar":

                setActiveTab("docDetail", int.Parse(lbl_u0_document_idx_caruse.Text), int.Parse(lbl_node_idx_caruse.Text), int.Parse(lbl_status_idx_caruse.Text), int.Parse(lbl_actor_idx_caruse.Text), 1, 0);

                break;

            case "cmdBackToCarUseHr":

                setActiveTab("docDetail", 0, 0, 0, 0, 0, 0);

                break;

            case "cmdSaveCarUseHr":

                int check_file = 0;

                RadioButtonList rdouse_outplan = (RadioButtonList)Panel_AddCarUseHr.FindControl("rdouse_outplan");

                data_carbooking data_u0_document_hr_addcar = new data_carbooking();

                cbk_u0_document_detail u0_document_hr_addcar = new cbk_u0_document_detail();
                data_u0_document_hr_addcar.cbk_u0_document_list = new cbk_u0_document_detail[1];

                u0_document_hr_addcar.u0_document_idx = int.Parse(lbl_u0_document_idx_caruse.Text);
                u0_document_hr_addcar.m0_node_idx = int.Parse(lbl_node_idx_caruse.Text);
                u0_document_hr_addcar.staidx = int.Parse(lbl_status_idx_caruse.Text);
                u0_document_hr_addcar.m0_actor_idx = int.Parse(lbl_actor_idx_caruse.Text);
                u0_document_hr_addcar.decision_idx = decision_hr_addusecar;
                u0_document_hr_addcar.cemp_idx = _emp_idx;


                //litDebug.Text = rdouse_outplan.SelectedValue.ToString();
                if (rdouse_outplan.SelectedValue.ToString() != "")
                {
                    u0_document_hr_addcar.usecar_outplan = int.Parse(rdouse_outplan.SelectedValue);
                }
                else
                {
                    u0_document_hr_addcar.usecar_outplan = 0;
                }


                //u0_document_hr_addcar.m0_actor_idx = int.Parse(hfM0ActoreIDX_idx_insertcar.Value); 

                foreach (GridViewRow gvDetailCaruse in GvDetailCarUse.Rows)
                {

                    if (gvDetailCaruse.RowType == DataControlRowType.DataRow)
                    {
                        //linkBtnTrigger(btnSaveCarUseHr);
                        GridView GvExpenses = (GridView)gvDetailCaruse.FindControl("GvExpenses");
                        Label lbl_u2_document_idx_hr_add = (Label)gvDetailCaruse.FindControl("lbl_u2_document_idx_hr_add");
                        Label lbl_m0_car_idx_hr_add = (Label)gvDetailCaruse.FindControl("lbl_m0_car_idx_hr_add");
                        RadioButtonList rdoStatusUse = (RadioButtonList)gvDetailCaruse.FindControl("rdoStatusUse");
                        TextBox txt_mile_start = (TextBox)gvDetailCaruse.FindControl("txt_mile_start");
                        TextBox txt_mile_end = (TextBox)gvDetailCaruse.FindControl("txt_mile_end");
                        TextBox txt_note_caruse = (TextBox)gvDetailCaruse.FindControl("txt_note_caruse");

                        FileUpload UploadFile_UseCar = (FileUpload)gvDetailCaruse.Cells[5].FindControl("UploadFile_UseCar");
                        //UpdatePanel Update_PnFileUpload = gvDetailCaruse.FindControl("Update_PnFileUpload") as UpdatePanel;


                        // u2_document
                        //data_carbooking data_u2_document_hr_addcar = new data_carbooking();
                        data_u0_document_hr_addcar.cbk_u2_document_list = new cbk_u2_document_detail[1];

                        var _u2_document = new cbk_u2_document_detail[GvDetailCarUse.Rows.Count];
                        int count_u2 = 0;

                        _u2_document[count_u2] = new cbk_u2_document_detail();
                        _u2_document[count_u2].u2_document_idx = int.Parse(lbl_u2_document_idx_hr_add.Text);
                        _u2_document[count_u2].emp_admin_idx = _emp_idx;
                        _u2_document[count_u2].car_use_status = int.Parse(rdoStatusUse.SelectedValue);

                        if (txt_mile_start.Text == "")
                        {
                            _u2_document[count_u2].miles_start = 0;
                        }
                        else
                        {
                            _u2_document[count_u2].miles_start = int.Parse(txt_mile_start.Text);
                        }

                        if(txt_mile_end.Text == "")
                        {
                            _u2_document[count_u2].miles_end = 0;
                        }
                        else
                        {
                            _u2_document[count_u2].miles_end = int.Parse(txt_mile_end.Text);
                        }
                        //_u2_document[count_u2].miles_start = int.Parse(txt_mile_start.Text);
                        //_u2_document[count_u2].miles_end = int.Parse(txt_mile_end.Text);
                        _u2_document[count_u2].note_detail_caruse = txt_note_caruse.Text;

                        ////if (int.Parse(rdoStatusUse.SelectedValue) == 2) // not use car
                        ////{
                        ////    _u2_document[count_u2].miles_start = 0;//int.Parse(txt_mile_start.Text);
                        ////    _u2_document[count_u2].miles_end = 0; //int.Parse(txt_mile_end.Text);
                        ////    _u2_document[count_u2].note_detail_caruse = "-";//txt_note_caruse.Text;

                        ////}
                        ////else
                        ////{


                        ////    _u2_document[count_u2].miles_start = int.Parse(txt_mile_start.Text);
                        ////    _u2_document[count_u2].miles_end = int.Parse(txt_mile_end.Text);
                        ////    _u2_document[count_u2].note_detail_caruse = txt_note_caruse.Text;
                        ////}


                        //hasfile save file
                        if (UploadFile_UseCar != null)
                        {

                            if (UploadFile_UseCar.HasFile)
                            {
                                string getPathfile = ConfigurationManager.AppSettings["path_flie_carbooking"];
                                string car_file_name = "u0_doc" + lbl_u0_document_idx_caruse.Text;//ViewState["vs_createRoom_DetailFile"].ToString();
                                string fileName_upload1 = "u2_doc" + lbl_u2_document_idx_hr_add.Text;

                                string fileName_upload = car_file_name + '-' + fileName_upload1;

                                string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                                //litDebug.Text = filePath_upload.ToString();


                                if (!Directory.Exists(filePath_upload))
                                {
                                    Directory.CreateDirectory(filePath_upload);
                                }
                                string extension = Path.GetExtension(UploadFile_UseCar.FileName);

                                //litDebug.Text = extension.ToString();
                                if (extension.ToLower() == ".jpg" || extension.ToLower() == ".png")
                                {
                                    //litDebug.Text = "0000";
                                    UploadFile_UseCar.SaveAs(Server.MapPath(getPathfile + fileName_upload) + "\\" + fileName_upload1 + extension);
                                    check_file = 0;

                                }
                                else
                                {
                                    // _funcTool.showAlert(this, "Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls หรือ .xlsx) เท่านั้น");fffff
                                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เฉพาะไฟล์ Excel (นามสกุลไฟล์ .jpg หรือ .png) เท่านั้น');", true);
                                    check_file = 1;
                                    break;
                                }
                                //UploadFile_UseCar.SaveAs(Server.MapPath(getPathfile + car_file_name) + "\\" + fileName_upload + extension);

                            }
                            else
                            {
                                //litDebug.Text += "not have file";
                            }

                        }



                        // u3_document
                        data_u0_document_hr_addcar.cbk_u3_document_list = new cbk_u3_document_detail[1];
                        var _u3_document = new cbk_u3_document_detail[GvExpenses.Rows.Count];
                        int count_u3 = 0;
                        foreach (GridViewRow GvExpenses_ in GvExpenses.Rows)
                        {
                            Label lblexpenses_idx = GvExpenses_.FindControl("lblexpenses_idx") as Label;
                            TextBox txt_value_caruse = GvExpenses_.FindControl("txt_value_caruse") as TextBox;

                            _u3_document[count_u3] = new cbk_u3_document_detail();
                            _u3_document[count_u3].u2_document_idx = int.Parse(lbl_u2_document_idx_hr_add.Text);
                            _u3_document[count_u3].expenses_idx = int.Parse(lblexpenses_idx.Text);
                            _u3_document[count_u3].cemp_idx = _emp_idx;
                            if(txt_value_caruse.Text == "")
                            {
                                _u3_document[count_u3].count_expenses = 0;
                            }
                            else
                            {
                                _u3_document[count_u3].count_expenses = int.Parse(txt_value_caruse.Text);
                            }
                            
                            //if (int.Parse(rdoStatusUse.SelectedValue) == 2)
                            //{
                            //    _u3_document[count_u3].count_expenses = 0;//int.Parse(txt_value_caruse.Text);
                            //}
                            //else
                            //{
                            //    _u3_document[count_u3].count_expenses = int.Parse(txt_value_caruse.Text);
                            //}


                            count_u3++;
                        }

                        count_u2++;

                        data_u0_document_hr_addcar.cbk_u0_document_list[0] = u0_document_hr_addcar;
                        data_u0_document_hr_addcar.cbk_u2_document_list = _u2_document;
                        data_u0_document_hr_addcar.cbk_u3_document_list = _u3_document;

                        

                    }

                }

                //insert car out plan u4
                var _dataset_CarDetailOutplanList = (DataSet)ViewState["vsCarDetailOutplanList"];

                var _add_CarOutplanList = new cbk_u4_document_detail[_dataset_CarDetailOutplanList.Tables[0].Rows.Count];
                int row_caroutplanlist = 0;

                int numrow = _dataset_CarDetailOutplanList.Tables[0].Rows.Count; //จำนวนแถว

                foreach (DataRow dtrow_Carlist in _dataset_CarDetailOutplanList.Tables[0].Rows)
                {
                    _add_CarOutplanList[row_caroutplanlist] = new cbk_u4_document_detail();

                    _add_CarOutplanList[row_caroutplanlist].u0_document_idx = int.Parse(lbl_u0_document_idx_caruse.Text);
                    _add_CarOutplanList[row_caroutplanlist].org_idx = int.Parse(dtrow_Carlist["drOutplanOrgIDX"].ToString());
                    _add_CarOutplanList[row_caroutplanlist].rdept_idx = int.Parse(dtrow_Carlist["drOutplanRdeptIDX"].ToString());
                    _add_CarOutplanList[row_caroutplanlist].rsec_idx = int.Parse(dtrow_Carlist["drOutplanRsecIDX"].ToString());
                    _add_CarOutplanList[row_caroutplanlist].cemp_idx = _emp_idx;
                    _add_CarOutplanList[row_caroutplanlist].distance_outplan = dtrow_Carlist["drOutplanDistanceText"].ToString();
                    _add_CarOutplanList[row_caroutplanlist].expenses_outplan = dtrow_Carlist["drOutplanExpensesText"].ToString();
                    _add_CarOutplanList[row_caroutplanlist].detail_outplan = dtrow_Carlist["drOutplanDetailText"].ToString();
                 

                    row_caroutplanlist++;

                }

                data_u0_document_hr_addcar.cbk_u4_document_list = _add_CarOutplanList;

                //use out plan insert detail before save usecar
                if (rdouse_outplan.SelectedValue == "2") 
                {
                    if(numrow > 0)
                    {
                        //litDebug.Text = numrow.ToString();
                        //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_hr_addcar));
                        data_u0_document_hr_addcar = callServicePostCarBooking(_urlSetCbkAddUseExpensesCarHr, data_u0_document_hr_addcar);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เพิ่มข้อมูลในส่วนของการเพิ่มเส้นทางรถให้เรียบร้อย ก่อนทำการบันทึก');", true);
                        ////ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert('รายชื่อผู้มาติดต่อครบ {0} คนแล้วไม่สามารถเพิ่มได้อีก');", dsContacts.Tables["dsContactPermTempTable"].Rows.Count.ToString()), true);
                        //check_file_out = 1;
                        break;
                    }
                }
                else
                {
                    //litDebug.Text = "2222";
                    data_u0_document_hr_addcar = callServicePostCarBooking(_urlSetCbkAddUseExpensesCarHr, data_u0_document_hr_addcar);
                }



                //approve hr use car or not use car
                if (check_file == 0)
                {
                    setActiveTab("docDetail", 0, 0, 0, 0, 0, 0);
                }

                break;

            case "cmdSaveCarUseHrOut":

                //int check_file_out = 0;
                FileUpload UploadFile_UseCarOut = (FileUpload)Update_SaveCarUseHrOut.FindControl("UploadFile_UseCarOut");

                data_carbooking data_u0_document_hr_addcarout = new data_carbooking();

                cbk_u0_document_detail u0_document_hr_addcarout = new cbk_u0_document_detail();
                data_u0_document_hr_addcarout.cbk_u0_document_list = new cbk_u0_document_detail[1];

                u0_document_hr_addcarout.u0_document_idx = int.Parse(lbl_u0_document_idx_caruse.Text);
                u0_document_hr_addcarout.m0_node_idx = int.Parse(lbl_node_idx_caruse.Text);
                u0_document_hr_addcarout.staidx = int.Parse(lbl_status_idx_caruse.Text);
                u0_document_hr_addcarout.m0_actor_idx = int.Parse(lbl_actor_idx_caruse.Text);
                u0_document_hr_addcarout.decision_idx = decision_hr_addusecar;
                u0_document_hr_addcarout.cemp_idx = _emp_idx;

                //hasfile save file
                if (UploadFile_UseCarOut != null)
                {

                    if (UploadFile_UseCarOut.HasFile)
                    {
                        string getPathfile = ConfigurationManager.AppSettings["path_flie_carbooking"];
                        string car_file_name = "u0_doc" + lbl_u0_document_idx_caruse.Text + "-" + "carout";//ViewState["vs_createRoom_DetailFile"].ToString();
                        string fileName_upload1 = "u0_doc" + lbl_u0_document_idx_caruse.Text;

                        string fileName_upload = car_file_name;

                        string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

                        //litDebug.Text = filePath_upload.ToString();

                        if (!Directory.Exists(filePath_upload))
                        {
                            Directory.CreateDirectory(filePath_upload);
                        }
                        string extension = Path.GetExtension(UploadFile_UseCarOut.FileName);

                        //litDebug.Text = extension.ToString();
                        if (extension.ToLower() == ".jpg" || extension.ToLower() == ".png")
                        {
                            //litDebug.Text = (getPathfile + fileName_upload) + "\\" + fileName_upload1 + extension;

                            UploadFile_UseCarOut.SaveAs(Server.MapPath(getPathfile + fileName_upload) + "\\" + fileName_upload1 + extension);
                            //check_file_out = 0;

                        }
                        else
                        {
                            //litDebug.Text = "1111";
                            // _funcTool.showAlert(this, "Import ได้เฉพาะไฟล์ Excel (นามสกุลไฟล์ .xls หรือ .xlsx) เท่านั้น");fffff
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('เฉพาะไฟล์ Excel (นามสกุลไฟล์ .jpg หรือ .png) เท่านั้น');", true);
                            //check_file_out = 1;
                            break;
                        }
                        //UploadFile_UseCar.SaveAs(Server.MapPath(getPathfile + car_file_name) + "\\" + fileName_upload + extension);

                    }
                    else
                    {
                        //litDebug.Text += "not have file";
                    }

                }
                data_u0_document_hr_addcarout.cbk_u0_document_list[0] = u0_document_hr_addcarout;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_hr_addcarout));
                data_u0_document_hr_addcarout = callServicePostCarBooking(_urlSetCbkAddUseExpensesCarOutHr, data_u0_document_hr_addcarout);

                setActiveTab("docDetail", 0, 0, 0, 0, 0, 0);

                break;

            case "cmdViewDetailUseCar":

                HiddenField hfu0_document_idx_usecar_ = (HiddenField)FvDetailCarBooking.FindControl("hfu0_document_idx");
                HiddenField hfM0NodeIDX_usecar_ = (HiddenField)FvDetailCarBooking.FindControl("hfM0NodeIDX");
                HiddenField hfM0ActoreIDX_usecar_ = (HiddenField)FvDetailCarBooking.FindControl("hfM0ActoreIDX");
                HiddenField hfM0StatusIDX_usecar_ = (HiddenField)FvDetailCarBooking.FindControl("hfM0StatusIDX");
                Label lbl_car_use_idx_usecar_ = (Label)FvDetailCarBooking.FindControl("lbl_car_use_idx_");

                UpdatePanel_ShowDetailCarUse.Visible = false;
                Panel_ShowDetailCarUse.Visible = true;

                lbl_u0_document_idx_caruse_detail.Text = hfu0_document_idx_usecar_.Value.ToString();
                lbl_node_idx_caruse_detail.Text = hfM0NodeIDX_usecar_.Value.ToString(); //nodeidx.ToString();
                lbl_actor_idx_caruse_detail.Text = hfM0StatusIDX_usecar_.Value.ToString(); //staidx.ToString();
                lbl_status_idx_caruse_detail.Text = hfM0ActoreIDX_usecar_.Value.ToString(); //actor_idx.ToString();

                getViewDetailCarInBookingAddHr(int.Parse(hfu0_document_idx_usecar_.Value), int.Parse(lbl_car_use_idx_usecar_.Text));

                //setActiveTab("docDetail", int.Parse(hfu0_document_idx_usecar_.Value), int.Parse(hfM0NodeIDX_usecar_.Value), int.Parse(hfM0StatusIDX_usecar_.Value), int.Parse(hfM0ActoreIDX_usecar_.Value), int.Parse(hfM0StatusIDX_usecar_.Value));

                break;
            case "cmdHideDatailUseCar":

                HiddenField hfu0_document_idx_usecar_hide = (HiddenField)FvDetailCarBooking.FindControl("hfu0_document_idx");
                HiddenField hfM0NodeIDX_usecar_hide = (HiddenField)FvDetailCarBooking.FindControl("hfM0NodeIDX");
                HiddenField hfM0ActoreIDX_usecar_hide = (HiddenField)FvDetailCarBooking.FindControl("hfM0ActoreIDX");
                HiddenField hfM0StatusIDX_usecar_hide = (HiddenField)FvDetailCarBooking.FindControl("hfM0StatusIDX");

                setActiveTab("docDetail", int.Parse(hfu0_document_idx_usecar_hide.Value), int.Parse(hfM0NodeIDX_usecar_hide.Value), int.Parse(hfM0StatusIDX_usecar_hide.Value), int.Parse(hfM0ActoreIDX_usecar_hide.Value), 1, 0);

                break;

            case "cmdSearchDetailIndex":

                setActiveTab("docDetail", 0, 0, 0, 0, 2, 0); //open tab search

                break;
            case "cmdSearchWaitApprove":
                setActiveTab("docApprove", 0, 0, 0, 0, 2, 0); //open tab search form wait approve

                break;
            case "cmdHideSearchDetailIndex":

                setActiveTab("docDetail", 0, 0, 0, 0, 0, 0); //hide tab search

                break;

            case "cmdSearchIndexDetailCar":

                DropDownList ddlPlaceSearchDetail = (DropDownList)FvSearchDetailIndex.FindControl("ddlPlaceSearchDetail");
                DropDownList ddlDetailtypecarSearchDetail = (DropDownList)FvSearchDetailIndex.FindControl("ddlDetailtypecarSearchDetail");
                DropDownList ddlSearchDate = (DropDownList)FvSearchDetailIndex.FindControl("ddlSearchDate");
                TextBox AddStartdate = (TextBox)FvSearchDetailIndex.FindControl("AddStartdate");
                TextBox AddEndDate = (TextBox)FvSearchDetailIndex.FindControl("AddEndDate");
                DropDownList ddl_StatusDetail = (DropDownList)FvSearchDetailIndex.FindControl("ddl_StatusDetail");
                DropDownList ddlCarUseSearchDetail = (DropDownList)FvSearchDetailIndex.FindControl("ddlCarUseSearchDetail");
                DropDownList ddlm0caridxSearchDetail = (DropDownList)FvSearchDetailIndex.FindControl("ddlm0caridxSearchDetail");


                // --- Check Date Search Between //
                IFormatProvider culture_search = new CultureInfo("en-US", true);
                //DateTime DateToday = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                DateTime dateVal_startserach = DateTime.ParseExact(AddStartdate.Text, "dd/MM/yyyy", culture_search);
                if (AddEndDate.Text != "")
                {
                    DateTime dateVal_endsearch = DateTime.ParseExact(AddEndDate.Text, "dd/MM/yyyy", culture_search);
                    ViewState["Vs_CheckDateSearch"] = (dateVal_endsearch - dateVal_startserach).TotalDays.ToString();

                    //litDebug.Text = ViewState["Vs_CheckDateSearch"].ToString();
                }

                // --- Check Date Search Between //

                data_carbooking data_search_index = new data_carbooking();
                cbk_search_car_detail search_detail_index = new cbk_search_car_detail();
                data_search_index.cbk_search_car_list = new cbk_search_car_detail[1];

                data_search_index.cbk_u0_document_list = new cbk_u0_document_detail[1];

                search_detail_index.place_idx = int.Parse(ddlPlaceSearchDetail.SelectedValue);
                search_detail_index.detailtype_car_idx = int.Parse(ddlDetailtypecarSearchDetail.SelectedValue);
                search_detail_index.IFSearchbetween = int.Parse(ddlSearchDate.SelectedValue);
                search_detail_index.date_start = AddStartdate.Text;
                search_detail_index.date_end = AddEndDate.Text;
                search_detail_index.cemp_idx = _emp_idx;
                search_detail_index.cemp_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                search_detail_index.cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                search_detail_index.cemp_rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
                search_detail_index.cemp_joblevel = int.Parse(ViewState["joblevel_permission"].ToString());
                search_detail_index.staidx = int.Parse(ddl_StatusDetail.SelectedValue);
                search_detail_index.car_use_idx = int.Parse(ddlCarUseSearchDetail.SelectedValue);
                search_detail_index.m0_car_idx = int.Parse(ddlm0caridxSearchDetail.SelectedValue);

                data_search_index.cbk_search_car_list[0] = search_detail_index;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_index)); 

                //data_u0_document_headapprove = callServicePostCarBooking(_urlSetCbkApproveHeadUser, data_u0_document_headapprove);
                if (int.Parse(ddlSearchDate.SelectedValue) == 3 && int.Parse(ViewState["Vs_CheckDateSearch"].ToString()) <= 0) //between date start and date end
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณาตรวจสอบวันที่ก่อนทำการการค้นหา ---');", true);
                    ViewState["Va_DetailCarbooking"] = null;
                    setGridData(GvDetail, ViewState["Va_DetailCarbooking"]);
                    break;
                }
                else
                {
                    data_search_index = callServicePostCarBooking(_urlGetSearchCbkDetailIndex, data_search_index);

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_detail));

                    ViewState["Va_DetailCarbooking"] = data_search_index.cbk_u0_document_list;
                    setGridData(GvDetail, ViewState["Va_DetailCarbooking"]);

                }

                break;
            case "cmdSearchDetailWaitApproveCar":

                DropDownList ddlPlaceSearchDetailWaitApprove = (DropDownList)FvSearchWaitApprove.FindControl("ddlPlaceSearchDetailWaitApprove");
                DropDownList ddlDetailtypecarSearchDetailWaitApprove = (DropDownList)FvSearchWaitApprove.FindControl("ddlDetailtypecarSearchDetailWaitApprove");
                DropDownList ddlSearchDateWaitApprove = (DropDownList)FvSearchWaitApprove.FindControl("ddlSearchDateWaitApprove");
                TextBox AddStartdateWaitApprove = (TextBox)FvSearchWaitApprove.FindControl("AddStartdateWaitApprove");
                TextBox AddEndDateWaitApprove = (TextBox)FvSearchWaitApprove.FindControl("AddEndDateWaitApprove");


                // --- Check Date Search Between //
                IFormatProvider culture_searchwait = new CultureInfo("en-US", true);
                //DateTime DateToday = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", CultureInfo.InvariantCulture);
                DateTime dateVal_startserachwait = DateTime.ParseExact(AddStartdateWaitApprove.Text, "dd/MM/yyyy", culture_searchwait);
                if (AddEndDateWaitApprove.Text != "")
                {
                    DateTime dateVal_endsearchwait = DateTime.ParseExact(AddEndDateWaitApprove.Text, "dd/MM/yyyy", culture_searchwait);
                    ViewState["Vs_CheckDateSearchWaitApprove"] = (dateVal_endsearchwait - dateVal_startserachwait).TotalDays.ToString();

                    //litDebug.Text = ViewState["Vs_CheckDateSearch"].ToString();
                }

                // --- Check Date Search Between //

                data_carbooking data_search_waitapprove = new data_carbooking();
                cbk_search_car_detail search_detail_waitapprove = new cbk_search_car_detail();
                data_search_waitapprove.cbk_search_car_list = new cbk_search_car_detail[1];

                data_search_waitapprove.cbk_u0_document_list = new cbk_u0_document_detail[1];

                search_detail_waitapprove.place_idx = int.Parse(ddlPlaceSearchDetailWaitApprove.SelectedValue);
                search_detail_waitapprove.detailtype_car_idx = int.Parse(ddlDetailtypecarSearchDetailWaitApprove.SelectedValue);
                search_detail_waitapprove.IFSearchbetween = int.Parse(ddlSearchDateWaitApprove.SelectedValue);
                search_detail_waitapprove.date_start = AddStartdateWaitApprove.Text;
                search_detail_waitapprove.date_end = AddEndDateWaitApprove.Text;
                search_detail_waitapprove.cemp_idx = _emp_idx;
                search_detail_waitapprove.cemp_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                search_detail_waitapprove.cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                search_detail_waitapprove.cemp_rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
                search_detail_waitapprove.cemp_joblevel = int.Parse(ViewState["joblevel_permission"].ToString());


                data_search_waitapprove.cbk_search_car_list[0] = search_detail_waitapprove;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_waitapprove));

                //data_u0_document_headapprove = callServicePostCarBooking(_urlSetCbkApproveHeadUser, data_u0_document_headapprove);
                if (int.Parse(ddlSearchDateWaitApprove.SelectedValue) == 3 && int.Parse(ViewState["Vs_CheckDateSearchWaitApprove"].ToString()) <= 0) //between date start and date end
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณาตรวจสอบวันที่ก่อนทำการการค้นหา ---');", true);
                    ViewState["Vs_WaitDetailApprove"] = null;
                    setGridData(GvWaitDetailApprove, ViewState["Vs_WaitDetailApprove"]);
                    break;
                }
                else
                {
                    data_search_waitapprove = callServicePostCarBooking(_urlGetSearchCbkDetailWaitApprove, data_search_waitapprove);

                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_search_detail));

                    ViewState["Vs_WaitDetailApprove"] = data_search_waitapprove.cbk_u0_document_list;
                    setGridData(GvWaitDetailApprove, ViewState["Vs_WaitDetailApprove"]);

                }

                break;
            case "cmdHideSearchDetailWaitApprove":

                setActiveTab("docApprove", 0, 0, 0, 0, 0, 0);

                break;

            case "cmdCancelCarDetail":

                HiddenField hfu0_document_idx_ = (HiddenField)FvDetailCarBooking.FindControl("hfu0_document_idx");


                data_carbooking data_u0_document_usercancel = new data_carbooking();

                cbk_u0_document_detail u0_document_usercancel = new cbk_u0_document_detail();
                data_u0_document_usercancel.cbk_u0_document_list = new cbk_u0_document_detail[1];

                u0_document_usercancel.u0_document_idx = int.Parse(hfu0_document_idx_.Value);
                u0_document_usercancel.m0_node_idx = 4;//int.Parse(lbl_node_idx_caruse.Text);
                u0_document_usercancel.staidx = 6;//int.Parse(lbl_status_idx_caruse.Text);
                u0_document_usercancel.m0_actor_idx = 1;
                u0_document_usercancel.decision_idx = 5;//decision_hr_addusecar;
                u0_document_usercancel.cemp_idx = _emp_idx;

                data_u0_document_usercancel.cbk_u0_document_list[0] = u0_document_usercancel;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_usercancel));

                data_u0_document_usercancel = callServicePostCarBooking(_urlSetCbkUserCancel, data_u0_document_usercancel);


                getCountWaitDetailApprove(0);


                break;
            case "cmdSearchReportCarDetail":

                data_carbooking data_report_carbooking = new data_carbooking();
                cbk_searchreport_car_detail search_report_car = new cbk_searchreport_car_detail();
                data_report_carbooking.cbk_searchreport_car_list = new cbk_searchreport_car_detail[1];

                search_report_car.condition = 0;
                search_report_car.cemp_idx = _emp_idx;
                search_report_car.staidx = int.Parse(ddlStatusCarBooking.SelectedValue);
                search_report_car.IFSearchbetween = int.Parse(ddlSearchDateReport.SelectedValue);
                search_report_car.date_start = txtAddStartdateReport.Text;
                search_report_car.date_end = txtAddEndDateReport.Text;
                search_report_car.org_idx = int.Parse(ddlorgidx.SelectedValue);
                search_report_car.rdept_idx = int.Parse(ddlrdeptidx.SelectedValue);
                search_report_car.place_idx = int.Parse(ddlPlaceReportSearchDetail.SelectedValue);
                search_report_car.car_use_idx = int.Parse(ddlCarUseReportSearchDetail.SelectedValue);
                search_report_car.car_use_status = int.Parse(ddlCarUseStatus.SelectedValue);
                search_report_car.usecar_outplan_idx = int.Parse(ddlStatusOutplan.SelectedValue);

                data_report_carbooking.cbk_searchreport_car_list[0] = search_report_car;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_carbooking));
                if (txtAddEndDateReport.Text != "")
                {

                    IFormatProvider culture_report = new CultureInfo("en-US", true);
                    //DateTime DateToday_Set = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture); //Date Time Today Incheck
                    DateTime DateTime_Start_Check = DateTime.ParseExact(txtAddStartdateReport.Text, "dd/MM/yyyy", culture_report);
                    DateTime DateTime_End_Check = DateTime.ParseExact(txtAddEndDateReport.Text, "dd/MM/yyyy", culture_report);

                    var result_day = DateTime_End_Check.Subtract(DateTime_Start_Check).TotalDays; // result time chrck > 30 min because cancel in room

                    //litDebug.Text = result_day.ToString();
                    if (result_day > 0)
                    {
                        
                        data_report_carbooking = callServicePostCarBooking(_urlGetCbkSearchReportTable, data_report_carbooking);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_carbooking));

                        Update_PanelReportTable.Visible = true;
                        ViewState["Vs_DetailReportCarbookingTable"] = data_report_carbooking.cbk_searchreport_car_list;
                        setGridData(GvDetailTableReport, ViewState["Vs_DetailReportCarbookingTable"]);

                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณาเช็ควันที่ค้นก่อนที่การค้นหา!!! ---');", true);
                        break;
                    }

                }
                else
                {
                    data_report_carbooking = callServicePostCarBooking(_urlGetCbkSearchReportTable, data_report_carbooking);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_roombooking));

                    Update_PanelReportTable.Visible = true;
                    ViewState["Vs_DetailReportCarbookingTable"] = data_report_carbooking.cbk_searchreport_car_list;
                    setGridData(GvDetailTableReport, ViewState["Vs_DetailReportCarbookingTable"]);
                }
                break;
            case "cmdExportReportCarDetail":

                data_carbooking data_report_carbooking_export = new data_carbooking();
                cbk_searchreport_car_detail search_report_car_export = new cbk_searchreport_car_detail();
                data_report_carbooking_export.cbk_searchreport_car_list = new cbk_searchreport_car_detail[1];
                search_report_car_export.condition = 0;
                search_report_car_export.cemp_idx = _emp_idx;
                search_report_car_export.staidx = int.Parse(ddlStatusCarBooking.SelectedValue);
                search_report_car_export.IFSearchbetween = int.Parse(ddlSearchDateReport.SelectedValue);
                search_report_car_export.date_start = txtAddStartdateReport.Text;
                search_report_car_export.date_end = txtAddEndDateReport.Text;
                search_report_car_export.org_idx = int.Parse(ddlorgidx.SelectedValue);
                search_report_car_export.rdept_idx = int.Parse(ddlrdeptidx.SelectedValue);
                search_report_car_export.place_idx = int.Parse(ddlPlaceReportSearchDetail.SelectedValue);
                search_report_car_export.car_use_idx = int.Parse(ddlCarUseReportSearchDetail.SelectedValue);
                search_report_car_export.car_use_status = int.Parse(ddlCarUseStatus.SelectedValue);
                data_report_carbooking_export.cbk_searchreport_car_list[0] = search_report_car_export;


                //check datetime before export data excel
                if (txtAddEndDateReport.Text != "")
                {

                    IFormatProvider culture_report = new CultureInfo("en-US", true);
                    //DateTime DateToday_Set = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture); //Date Time Today Incheck
                    DateTime DateTime_Start_Check = DateTime.ParseExact(txtAddStartdateReport.Text, "dd/MM/yyyy", culture_report);
                    DateTime DateTime_End_Check = DateTime.ParseExact(txtAddEndDateReport.Text, "dd/MM/yyyy", culture_report);

                    var result_day = DateTime_End_Check.Subtract(DateTime_Start_Check).TotalDays; // result time chrck > 30 min because cancel in room

                    //litDebug.Text = result_day.ToString();
                    if (result_day > 0)
                    {
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_roombooking_export));
                        data_report_carbooking_export = callServicePostCarBooking(_urlGetCbkSearchReportTable, data_report_carbooking_export);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_roombooking));

                        //Update_PanelReportTable.Visible = true;
                        ViewState["Vs_DetailReportCarTable_Export"] = data_report_carbooking_export.cbk_searchreport_car_list;
                        //setGridData(GvDetailTableReport, ViewState["Vs_DetailReportCarTable_Export"]);

                        int count_ = 0;

                        if (data_report_carbooking_export.return_code == 0)
                        {
                            DataTable tableCarBooking = new DataTable();
                            tableCarBooking.Columns.Add("ฝ่าย", typeof(String));
                            tableCarBooking.Columns.Add("CostCenter", typeof(String));
                            tableCarBooking.Columns.Add("จำนวน", typeof(String));
                            //tableRoomBooking.Columns.Add("จำนวน", typeof(String));
                            //tableRoomBooking.Columns.Add("หน่วยนับ", typeof(String));

                            foreach (var row_report in data_report_carbooking_export.cbk_searchreport_car_list)
                            {
                                DataRow addMRoomBookingRow = tableCarBooking.NewRow();

                                addMRoomBookingRow[0] = row_report.dept_name_th.ToString();
                                addMRoomBookingRow[1] = row_report.costcenter_no.ToString();
                                addMRoomBookingRow[2] = row_report.count_carbooking.ToString();
                                //addMaterialLogRow[2] = getTypeMaterialStockLog((string)materialLog.type_stock.ToString(), false);
                                //addMaterialLogRow[3] = string.Format("{0:n2}", materialLog.amount_material_log.ToString());
                                //addMaterialLogRow[4] = materialLog.unit_name_log.ToString();

                                tableCarBooking.Rows.InsertAt(addMRoomBookingRow, count_++);
                            }
                            WriteExcelWithNPOI(tableCarBooking, "xls", "report-carbooking", 0);
                            //WriteExcelWithNPOI(tableRoomBooking, "xls", "report-roombooking", "Export To Excel");
                        }
                        else
                        {
                            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                            break;
                        }

                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรุณาเช็ควันที่ค้นก่อนที่การ Export ข้อมูล ---');", true);
                        break;
                    }

                }
                else
                {
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_roombooking_export));
                    data_report_carbooking_export = callServicePostCarBooking(_urlGetCbkSearchReportTable, data_report_carbooking_export);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_roombooking));

                    //Update_PanelReportTable.Visible = true;
                    ViewState["Vs_DetailReportCarTable_Export"] = data_report_carbooking_export.cbk_searchreport_car_list;
                    //setGridData(GvDetailTableReport, ViewState["Vs_DetailReportRoomTable"]);

                    int count_ = 0;

                    if (data_report_carbooking_export.return_code == 0)
                    {
                        DataTable tableCarBooking = new DataTable();
                        tableCarBooking.Columns.Add("ฝ่าย", typeof(String));
                        tableCarBooking.Columns.Add("CostCenter", typeof(String));
                        tableCarBooking.Columns.Add("จำนวน", typeof(String));
                        //tableRoomBooking.Columns.Add("จำนวน", typeof(String));
                        //tableRoomBooking.Columns.Add("หน่วยนับ", typeof(String));

                        foreach (var row_report in data_report_carbooking_export.cbk_searchreport_car_list)
                        {
                            DataRow addMRoomBookingRow = tableCarBooking.NewRow();

                            addMRoomBookingRow[0] = row_report.dept_name_th.ToString();
                            addMRoomBookingRow[1] = row_report.costcenter_no.ToString();
                            addMRoomBookingRow[2] = row_report.count_carbooking.ToString();
                            //addMaterialLogRow[2] = getTypeMaterialStockLog((string)materialLog.type_stock.ToString(), false);
                            //addMaterialLogRow[3] = string.Format("{0:n2}", materialLog.amount_material_log.ToString());
                            //addMaterialLogRow[4] = materialLog.unit_name_log.ToString();

                            tableCarBooking.Rows.InsertAt(addMRoomBookingRow, count_++);
                        }
                        WriteExcelWithNPOI(tableCarBooking, "xls", "report-carbooking", 0);
                        //WriteExcelWithNPOI(tableRoomBooking, "xls", "report-roombooking", "Export To Excel");
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                        break;
                    }
                }

                break;

            case "cmdViewDetailReportTable":

                Update_PanelSearchReport.Visible = false;
                Update_PanelReportTable.Visible = false;

                string[] arg8 = new string[2];
                arg8 = e.CommandArgument.ToString().Split(';');
                int _costcenter_idx_report = int.Parse(arg8[0]);
                int _rdept_idx_report = int.Parse(arg8[1]);
                int _staidx_report = int.Parse(arg8[2]);

                data_carbooking data_report_view = new data_carbooking();
                cbk_searchreport_car_detail search_report_view = new cbk_searchreport_car_detail();
                data_report_view.cbk_searchreport_car_list = new cbk_searchreport_car_detail[1];
                search_report_view.condition = 1;
                search_report_view.cemp_idx = _emp_idx;
                search_report_view.staidx = _staidx_report;
                search_report_view.IFSearchbetween = int.Parse(ddlSearchDateReport.SelectedValue);
                search_report_view.date_start = txtAddStartdateReport.Text;
                search_report_view.date_end = txtAddEndDateReport.Text;
                search_report_view.org_idx = int.Parse(ddlorgidx.SelectedValue);
                search_report_view.rdept_idx = _rdept_idx_report;
                search_report_view.place_idx = int.Parse(ddlPlaceReportSearchDetail.SelectedValue);
                search_report_view.car_use_idx = int.Parse(ddlCarUseReportSearchDetail.SelectedValue);
                search_report_view.car_use_status = int.Parse(ddlCarUseStatus.SelectedValue);
                search_report_view.usecar_outplan_idx = int.Parse(ddlStatusOutplan.SelectedValue);

                data_report_view.cbk_searchreport_car_list[0] = search_report_view;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_report_view));
                data_report_view = callServicePostCarBooking(_urlGetCbkSearchReportTable, data_report_view);

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_report_view));
                Update_PanelDetailReportTable.Visible = true;
                Update_PanelBackToSearch.Visible = true;

                ViewState["Vs_ViewDetailReportCarTable"] = data_report_view.cbk_searchreport_car_list;
                setGridData(GvViewDetailReport, ViewState["Vs_ViewDetailReportCarTable"]);

                mergeCell(GvViewDetailReport);

                setOntop.Focus();

                break;
            case "cmdBackToSearchReport":

                Update_PanelDetailReportTable.Visible = false;
                Update_PanelBackToSearch.Visible = false;

                Update_PanelSearchReport.Visible = true;
                Update_PanelReportTable.Visible = true;


                setGridData(GvDetailTableReport, ViewState["Vs_DetailReportCarbookingTable"]);

                setOntop.Focus();

                break;
            case "cmdExportDetailReportCarDetail":

                //litDebug.Text = "55555";
                int count_detail_ = 0;
                decimal count_detail_export = 0;
                decimal count_detailoutplan_export = 0;
                if (ViewState["Vs_ViewDetailReportCarTable"] != null)
                {
                    DataTable tableCarBooking = new DataTable();


                    tableCarBooking.Columns.Add("ฝ่าย", typeof(String));
                    tableCarBooking.Columns.Add("CostCenter", typeof(String));
                    tableCarBooking.Columns.Add("สถานที่", typeof(String));
                    tableCarBooking.Columns.Add("วันและเวลาที่เริ่มจอง", typeof(String));
                    tableCarBooking.Columns.Add("วันและเวลาที่สิ้นสุด", typeof(String));
                    tableCarBooking.Columns.Add("ค่าใช้จ่าย", typeof(String));
                    tableCarBooking.Columns.Add("ค่าใช้จ่ายนอกเส้นทาง", typeof(String));
                    //tableRoomBooking.Columns.Add("จำนวน", typeof(String));
                    //tableRoomBooking.Columns.Add("หน่วยนับ", typeof(String));

                    cbk_searchreport_car_detail[] _item_listexport_detail = (cbk_searchreport_car_detail[])ViewState["Vs_ViewDetailReportCarTable"];

                    var _linqTest_export = (from data in _item_listexport_detail
                                            select new
                                            {
                                                data.sum_expenses,
                                                data.sum_expenses_decimal,
                                                data.sum_expenses_outplan_decimal

                                            }).ToList();
                    if (_linqTest_export.Count() > 0)
                    {

                        for (int z = 0; z < _linqTest_export.Count(); z++)
                        {
                            count_detail_export += Convert.ToDecimal(_linqTest_export[z].sum_expenses_decimal.ToString());
                            count_detailoutplan_export += Convert.ToDecimal(_linqTest_export[z].sum_expenses_outplan_decimal.ToString());

                        }
                    }

                    foreach (var row_report_detail in _item_listexport_detail)
                    {
                        DataRow addCarBookingRow_Detail = tableCarBooking.NewRow();

                        addCarBookingRow_Detail[0] = row_report_detail.dept_name_th.ToString();
                        addCarBookingRow_Detail[1] = row_report_detail.costcenter_no.ToString();
                        addCarBookingRow_Detail[2] = row_report_detail.place_name.ToString();
                        addCarBookingRow_Detail[3] = "ตั้งแต่วันที่ : " + " " + row_report_detail.date_start.ToString() + " " + "เวลา : " + " " + row_report_detail.time_start.ToString() + "น.";
                        addCarBookingRow_Detail[4] = "ถึงวันที่ : " + " " + row_report_detail.date_end.ToString() + " " + "เวลา : " + " " + row_report_detail.time_end.ToString() + "น.";
                        addCarBookingRow_Detail[5] = String.Format("{0:N2}", Convert.ToDecimal(row_report_detail.sum_expenses_decimal.ToString()));//row_report_detail.sum_expenses_decimal.ToString();
                        addCarBookingRow_Detail[6] = String.Format("{0:N2}", Convert.ToDecimal(row_report_detail.sum_expenses_outplan_decimal.ToString()));//row_report_detail.sum_expenses_decimal.ToString();

                        //tableCarBooking.Rows.Add(addCarBookingRow_Detail);
                        //tableCarBooking.Rows.Add(88888, 80000, 7777, 001);
                        tableCarBooking.Rows.InsertAt(addCarBookingRow_Detail, count_detail_++);


                        //litDebug.Text += count_detail_.ToString();
                    }
                    //set var for bg color
                    tableCarBooking.Rows.Add("", "", "", "", "Total :", String.Format("{0:N2}", count_detail_export), String.Format("{0:N2}", count_detailoutplan_export));
                    //tableCarBooking.Rows.Add("", "", "", "", "ราคารวมนอกเส้นทาง :", String.Format("{0:N2}", count_detailoutplan_export));

                    WriteExcelWithNPOI(tableCarBooking, "xls", "report-carbookingdetail", 1); 

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- ไม่พบข้อมูล ---');", true);
                    break;
                }

                break;
            case "cmdSearchReportGraph":

                switch (ddlTypeGraph.SelectedValue)
                {
                    case "1": //จำนวนการจองรถ/หน่วยงาน
                        getCountBookingReportGraph();
                        break;

                    case "2": //จำนวนการยกเลิก/หน่วยงาน
                        getCountCancelBookingReportGraph();
                        break;
                    case "3": //จำนวนค่าใช้จ่าย/หน่วยงาน
                        getCountExpensesBookingReportGraph();
                        break;
                    case "4": //จำนวนการจองแล้วไม่ได้ใช้/หน่วยงาน
                        getCountNotUseCarBookingReportGraph();
                        break;
                }

                break;

        }

    }
    //endbtn


    #endregion event command

    #region data table
    protected void getCarDetailList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsCarDetailList = new DataSet();
        dsCarDetailList.Tables.Add("dsCarDetailTable");
        dsCarDetailList.Tables["dsCarDetailTable"].Columns.Add("drCarDetailIDX", typeof(String));
        dsCarDetailList.Tables["dsCarDetailTable"].Columns.Add("drCarDetailText", typeof(String));
        dsCarDetailList.Tables["dsCarDetailTable"].Columns.Add("drAdminCarIDX", typeof(String));
        dsCarDetailList.Tables["dsCarDetailTable"].Columns.Add("drAdminCarText", typeof(String));
        //dsFoodMaterialList.Tables["dsFoodMaterialTable"].Columns.Add("drFoodMaterialAmount", typeof(String));
        //dsFoodMaterialList.Tables["dsFoodMaterialTable"].Columns.Add("drFoodMaterialPerUnit", typeof(String));
        ViewState["vsCarDetailList"] = dsCarDetailList;
    }

    protected void getCarDetailOutplanList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsCarDetailOutplanList = new DataSet();
        dsCarDetailOutplanList.Tables.Add("dsCarDetailOutplanTable");
        dsCarDetailOutplanList.Tables["dsCarDetailOutplanTable"].Columns.Add("drOutplanOrgIDX", typeof(String));
        dsCarDetailOutplanList.Tables["dsCarDetailOutplanTable"].Columns.Add("drOutplanOrgText", typeof(String));
        dsCarDetailOutplanList.Tables["dsCarDetailOutplanTable"].Columns.Add("drOutplanRdeptIDX", typeof(String));
        dsCarDetailOutplanList.Tables["dsCarDetailOutplanTable"].Columns.Add("drOutplanRdeptText", typeof(String));
        dsCarDetailOutplanList.Tables["dsCarDetailOutplanTable"].Columns.Add("drOutplanRsecIDX", typeof(String));
        dsCarDetailOutplanList.Tables["dsCarDetailOutplanTable"].Columns.Add("drOutplanRsecText", typeof(String));      
        dsCarDetailOutplanList.Tables["dsCarDetailOutplanTable"].Columns.Add("drOutplanDistanceText", typeof(String));
        dsCarDetailOutplanList.Tables["dsCarDetailOutplanTable"].Columns.Add("drOutplanExpensesText", typeof(String));
        dsCarDetailOutplanList.Tables["dsCarDetailOutplanTable"].Columns.Add("drOutplanDetailText", typeof(String));
        
        ViewState["vsCarDetailOutplanList"] = dsCarDetailOutplanList;
    }

    protected void setCarDetailList()
    {
        if (ViewState["vsCarDetailList"] != null)
        {
            DropDownList ddlm0_car_idx = (DropDownList)FvHrApprove.FindControl("ddlm0_car_idx");
            DropDownList ddl_emp_idx_driver = (DropDownList)FvHrApprove.FindControl("ddl_emp_idx_driver");
            GridView gvAddCarBooking = (GridView)FvHrApprove.FindControl("gvAddCarBooking");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsCarDetailList"];

            foreach (DataRow dr in dsContacts.Tables["dsCarDetailTable"].Rows)
            {
                if (dr["drCarDetailIDX"].ToString() == ddlm0_car_idx.SelectedValue || dr["drAdminCarIDX"].ToString() == ddl_emp_idx_driver.SelectedValue)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลอยู่แล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsCarDetailTable"].NewRow();
            drContacts["drCarDetailIDX"] = ddlm0_car_idx.SelectedValue;
            drContacts["drCarDetailText"] = ddlm0_car_idx.SelectedItem.Text;
            drContacts["drAdminCarIDX"] = ddl_emp_idx_driver.SelectedValue;
            drContacts["drAdminCarText"] = ddl_emp_idx_driver.SelectedItem.Text;

            dsContacts.Tables["dsCarDetailTable"].Rows.Add(drContacts);
            ViewState["vsCarDetailList"] = dsContacts;
            setGridData(gvAddCarBooking, dsContacts.Tables["dsCarDetailTable"]);
            gvAddCarBooking.Visible = true;

            

        }
    }

    protected void setCarDetailOutplanList()
    {
        if (ViewState["vsCarDetailOutplanList"] != null)
        {
            DropDownList ddlorg_idx = (DropDownList)updateUseDistanceOutInsert.FindControl("ddlorg_idx");
            DropDownList ddlrdept_idx = (DropDownList)updateUseDistanceOutInsert.FindControl("ddlrdept_idx");
            DropDownList ddlrsec_idx = (DropDownList)updateUseDistanceOutInsert.FindControl("ddlrsec_idx");
            TextBox txt_Distance = (TextBox)updateUseDistanceOutInsert.FindControl("txt_Distance");
            TextBox txt_expenses_outplan = (TextBox)updateUseDistanceOutInsert.FindControl("txt_expenses_outplan");
            TextBox txt_Detailoutplan = (TextBox)updateUseDistanceOutInsert.FindControl("txt_Detailoutplan");
            GridView gvDetailOutplanList = (GridView)updateUseDistanceOutInsert.FindControl("gvDetailOutplanList");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsCarDetailOutplanList"];

            foreach (DataRow dr in dsContacts.Tables["dsCarDetailOutplanTable"].Rows)
            {
                if (dr["drOutplanRsecIDX"].ToString() == ddlrsec_idx.SelectedValue)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลอยู่แล้ว!!!');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsCarDetailOutplanTable"].NewRow();
            drContacts["drOutplanOrgIDX"] = ddlorg_idx.SelectedValue;
            drContacts["drOutplanOrgText"] = ddlorg_idx.SelectedItem.Text;
            drContacts["drOutplanRdeptIDX"] = ddlrdept_idx.SelectedValue;
            drContacts["drOutplanRdeptText"] = ddlrdept_idx.SelectedItem.Text;
            drContacts["drOutplanRsecIDX"] = ddlrsec_idx.SelectedValue;
            drContacts["drOutplanRsecText"] = ddlrsec_idx.SelectedItem.Text;
            drContacts["drOutplanDistanceText"] = txt_Distance.Text;
            drContacts["drOutplanExpensesText"] = txt_expenses_outplan.Text;
            drContacts["drOutplanDetailText"] = txt_Detailoutplan.Text;

            dsContacts.Tables["dsCarDetailOutplanTable"].Rows.Add(drContacts);
            ViewState["vsCarDetailOutplanList"] = dsContacts;
            setGridData(gvDetailOutplanList, dsContacts.Tables["dsCarDetailOutplanTable"]);
            gvDetailOutplanList.Visible = true;
        }
    }

    protected void CleardataSetCarDetailList()
    {
        var gvAddCarBooking = (GridView)FvHrApprove.FindControl("gvAddCarBooking");
        ViewState["vsCarDetailList"] = null;
        gvAddCarBooking.DataSource = ViewState["vsCarDetailList"];
        gvAddCarBooking.DataBind();

        getCarDetailList();
    }

    protected void CleardataSetCarDetailOutplanList()
    {
        var gvDetailOutplanList = (GridView)updateUseDistanceOutInsert.FindControl("gvDetailOutplanList");
        ViewState["vsCarDetailOutplanList"] = null;
        gvDetailOutplanList.DataSource = ViewState["vsCarDetailOutplanList"];
        gvDetailOutplanList.DataBind();

        getCarDetailOutplanList();
    }
    #endregion data table


    #region selected Changed
    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        switch (ddlName.ID)
        {
            case "ddlType_booking_idx":

                DropDownList ddlType_booking_idx = (DropDownList)FvCreateCarBooking.FindControl("ddlType_booking_idx");
                DropDownList ddlType_travel_idx = (DropDownList)FvCreateCarBooking.FindControl("ddlType_travel_idx");

                getTypetravel(ddlType_travel_idx, int.Parse(ddlType_booking_idx.SelectedValue), 0);

                break;

            case "ddlcar_use_idx":

                CleardataSetCarDetailList();
                if (ddlcar_use_idx.SelectedValue == "1")
                {

                    Label lbl_date_start_approve = (Label)FvDetailWaitApprove.FindControl("lbl_date_start_approve");
                    Label lbl_time_start_approve = (Label)FvDetailWaitApprove.FindControl("lbl_time_start_approve");
                    Label lbl_date_end_approve = (Label)FvDetailWaitApprove.FindControl("lbl_date_end_approve");
                    Label lbl_time_end_approve = (Label)FvDetailWaitApprove.FindControl("lbl_time_end_approve");


                    Panel_SelectCarInBooking.Visible = true;
                    getDetailCarBusy(ddlm0_car_idx, lbl_date_start_approve.Text, lbl_time_start_approve.Text, lbl_date_end_approve.Text, lbl_time_end_approve.Text, 0);
                    getDetailAdmin(ddl_emp_idx_driver, 0);

                }
                else
                {
                    Panel_SelectCarInBooking.Visible = false;
                }

                break;

            case "ddlHrApproveStatus":

                UpdatePanel update_InsertSelectCar = (UpdatePanel)FvHrApprove.FindControl("update_InsertSelectCar");

                if (ddlHrApproveStatus.SelectedValue == "3") //approve hr
                {

                    Label lbl_date_start_approve = (Label)FvDetailWaitApprove.FindControl("lbl_date_start_approve");
                    Label lbl_time_start_approve = (Label)FvDetailWaitApprove.FindControl("lbl_time_start_approve");
                    Label lbl_date_end_approve = (Label)FvDetailWaitApprove.FindControl("lbl_date_end_approve");
                    Label lbl_time_end_approve = (Label)FvDetailWaitApprove.FindControl("lbl_time_end_approve");

                    update_InsertSelectCar.Visible = true;
                    getDetailCarUse(ddlcar_use_idx, 0);
                    getDetailCarBusy(ddlm0_car_idx, lbl_date_start_approve.Text, lbl_time_start_approve.Text, lbl_date_end_approve.Text, lbl_time_end_approve.Text, 0);
                    getDetailAdmin(ddl_emp_idx_driver, 0);
                    //litDebug.Text = "222";

                }
                else
                {
                    //litDebug.Text = "333";
                    update_InsertSelectCar.Visible = false;
                }
                break;

            case "ddlSearchDate":

                if (int.Parse(ddlSearchDate.SelectedValue) == 3)
                {
                    AddEndDate.Enabled = true;
                }
                else
                {
                    AddEndDate.Enabled = false;
                    AddEndDate.Text = string.Empty;
                }

                break;
            case "ddlPlace":
                UpdatePanel Panel_OtherPlace = (UpdatePanel)FvCreateCarBooking.FindControl("Panel_OtherPlace");
                DropDownList ddlPlace = (DropDownList)FvCreateCarBooking.FindControl("ddlPlace");

                if (int.Parse(ddlPlace.SelectedValue) == 5) // another place
                {
                    Panel_OtherPlace.Visible = true;
                }
                else
                {
                    Panel_OtherPlace.Visible = false;
                }

                break;

            case "ddlCarUseNameSearch":

                UpdatePanel Update_SearchTypeCarUse = (UpdatePanel)FvDetailUseCarSearch.FindControl("Update_SearchTypeCarUse");
                DropDownList ddlCarUseNameSearch = (DropDownList)Update_SearchTypeCarUse.FindControl("ddlCarUseNameSearch");

                UpdatePanel Update_SearchUserCar = (UpdatePanel)FvDetailUseCarSearch.FindControl("Update_SearchUserCar");
                DropDownList ddlDetailtypecarSearch_ = (DropDownList)Update_SearchUserCar.FindControl("ddlDetailtypecarSearch");

                if (int.Parse(ddlCarUseNameSearch.SelectedValue) == 1) // another place
                {
                    Update_SearchUserCar.Visible = true;
                    Session["DetailCarUseIDXSearch"] = ddlCarUseNameSearch.SelectedValue;
                    getDetailTypeCar(ddlDetailtypecarSearch_, 0);
                }
                else
                {
                    Session["DetailCarUseIDXSearch"] = ddlCarUseNameSearch.SelectedValue;
                    Update_SearchUserCar.Visible = false;
                }

                break;

            case "ddlCarUseSearchDetail":

                //UpdatePanel Update_SearchTypeCarUse = (UpdatePanel)FvDetailUseCarSearch.FindControl("Update_SearchTypeCarUse");
                DropDownList ddlCarUseSearchDetail = (DropDownList)FvSearchDetailIndex.FindControl("ddlCarUseSearchDetail");
                DropDownList ddlDetailtypecarSearchDetail = (DropDownList)FvSearchDetailIndex.FindControl("ddlDetailtypecarSearchDetail");
                DropDownList ddlm0caridxSearchDetail = (DropDownList)FvSearchDetailIndex.FindControl("ddlm0caridxSearchDetail");

                if (int.Parse(ddlCarUseSearchDetail.SelectedValue) == 1) // another place
                {
                    ddlDetailtypecarSearchDetail.Enabled = true;
                    ddlm0caridxSearchDetail.Enabled = true;
                    getDetailTypeCar(ddlDetailtypecarSearchDetail, 0);
                }
                else
                {
                    ddlDetailtypecarSearchDetail.Enabled = false;
                    ddlm0caridxSearchDetail.Enabled = false;
                    getDetailTypeCar(ddlDetailtypecarSearchDetail, 0);
                }

                break;
            case "ddlDetailtypecarSearchDetail":

                DropDownList ddlDetailtypecarSearchDetail_value = (DropDownList)FvSearchDetailIndex.FindControl("ddlDetailtypecarSearchDetail");
                DropDownList ddlm0caridxSearchDetail_value = (DropDownList)FvSearchDetailIndex.FindControl("ddlm0caridxSearchDetail");

                getDetailTypeSearchCalendar(ddlm0caridxSearchDetail_value, int.Parse(ddlDetailtypecarSearchDetail_value.SelectedValue));


                break;

            case "ddlDetailtypecarSearch":

                DropDownList ddlCarUseNameSearch_ = (DropDownList)FvDetailUseCarSearch.FindControl("ddlCarUseNameSearch");

                DropDownList ddlDetailtypecarSearch = (DropDownList)FvDetailUseCarSearch.FindControl("ddlDetailtypecarSearch");
                DropDownList ddlm0car_idxSearch = (DropDownList)FvDetailUseCarSearch.FindControl("ddlm0car_idxSearch");

                Session["DetailCarUseIDXSearch"] = ddlCarUseNameSearch_.SelectedValue;
                Session["DetailtypecarSearch"] = ddlDetailtypecarSearch.SelectedValue;
                getDetailTypeSearchCalendar(ddlm0car_idxSearch, int.Parse(ddlDetailtypecarSearch.SelectedValue));

                break;
            case "ddlm0car_idxSearch":
                DropDownList ddlCarUseNameSearch_value = (DropDownList)FvDetailUseCarSearch.FindControl("ddlCarUseNameSearch");
                DropDownList ddlDetailtypecarSearch_value = (DropDownList)FvDetailUseCarSearch.FindControl("ddlDetailtypecarSearch");
                DropDownList ddlm0car_idxSearch_value = (DropDownList)FvDetailUseCarSearch.FindControl("ddlm0car_idxSearch");
                //DropDownList ddlPlaceSearch_value = (DropDownList)FvDetailUseRoomSearch.FindControl("ddlPlaceSearch");
                //DropDownList ddlRoomSearch_value = (DropDownList)FvDetailUseRoomSearch.FindControl("ddlRoomSearch");

                if (ddlDetailtypecarSearch_value.SelectedValue != "0")
                {
                    Session["DetailCarUseIDXSearch"] = ddlCarUseNameSearch_value.SelectedValue;
                    Session["DetailtypecarSearch"] = ddlDetailtypecarSearch_value.SelectedValue;
                    Session["ddlm0car_idxSearch"] = ddlm0car_idxSearch_value.SelectedValue;
                    //litDebug.Text = Session["RoomIDXSearch_Room"].ToString();
                }
                //getRoom(ddlRoomSearch, int.Parse(ddlPlaceSearch.SelectedValue));

                break;
            case "ddlTypeReport":

                Update_PnTableReportDetail.Visible = false;
                Update_PnGraphReportDetail.Visible = false;

                Update_PanelReportTable.Visible = false;
                Update_PanelGraph.Visible = false;

                ddlSearchDateReport.ClearSelection();
                ddlTypeGraph.ClearSelection();
                ddlmonth.ClearSelection();

                txtAddEndDateReport.Enabled = false;
                txtAddStartdateReport.Text = string.Empty;
                txtAddEndDateReport.Text = string.Empty;

                ddlrdeptidx.Items.Clear();
                ddlrdeptidx.Items.Insert(0, new ListItem("--- เลือกฝ่าย ---", "0"));

                Div_CarUseStatus.Visible = false;
                ddlStatusOutplan.Enabled = false;
                getDetailChoiceOutplanTable(ddlStatusOutplan, 0);
                switch (ddlTypeReport.SelectedValue)
                {
                    case "1": //ตาราง
                        Update_PnTableReportDetail.Visible = true;
                        getOrganizationList(ddlorgidx);
                        getPlace(ddlPlaceReportSearchDetail, 0);
                        getStatusDocumentReport(ddlStatusCarBooking, 0);
                        getDetailCarUse(ddlCarUseReportSearchDetail, 0);
                        
                        //setActiveTab(string activeTab, int uidx, int u1idx, int staidx, int _chk_tab)
                        //setActiveTab("docReport", 0, 0, 0, int.Parse(ddlTypeReport.SelectedValue));

                        break;
                    case "2": //กราฟ

                        Update_PnGraphReportDetail.Visible = true;
                        getddlYear(ddlyear);

                        //Update_PnTableReportDetail.Visible = false;
                        //setActiveTab("docReport", 0, 0, 0, int.Parse(ddlTypeReport.SelectedValue));
                        break;

                }

                break;
            case "ddlSearchDateReport":
                if (int.Parse(ddlSearchDateReport.SelectedValue) == 3) //between
                {
                    txtAddEndDateReport.Enabled = true;
                }
                else
                {
                    txtAddEndDateReport.Enabled = false;
                    txtAddEndDateReport.Text = string.Empty;
                }
                break;
            case "ddlorgidx":
                getDepartmentList(ddlrdeptidx, int.Parse(ddlorgidx.SelectedValue));
                break;
            case "ddlorg_idx":
                getDepartmentList(ddlrdept_idx, int.Parse(ddlorg_idx.SelectedValue));

                ddlrsec_idx.Items.Clear();
                ddlrsec_idx.Items.Insert(0, new ListItem("-- เลือกแผนก --", "0"));
                break;
            case "ddlrdept_idx":
                //ddlDocType.Focus();
                getSectionList(ddlrsec_idx, int.Parse(ddlorg_idx.SelectedItem.Value), int.Parse(ddlrdept_idx.SelectedItem.Value));
                break;
            case "ddlCarUseReportSearchDetail":

                Div_CarUseStatus.Visible = false;

                if (int.Parse(ddlCarUseReportSearchDetail.SelectedValue) == 1) //car use in
                {
                    Div_CarUseStatus.Visible = true;
                    ddlCarUseStatus.ClearSelection();
                }

                break;

            case "ddlTypeGraph":

                status_report.Visible = false;
                ddlStatusBooking.Visible = false;


                Update_PanelGraph.Visible = false;

                if (ddlTypeGraph.SelectedValue == "1") //Count booking/Dept
                {
                    status_report.Visible = true;
                    ddlStatusBooking.Visible = true;
                    getStatusDocumentReport(ddlStatusBooking, 0);
                }


                break;
            case "ddlStatusFilter":
                //int _set_statusFilter = 0;
                //setGridData(GvDetail, ViewState["Va_DetailCarbooking"]);
                Panel_searchDetailIndex.Visible = false;
                if (ddlStatusFilter.SelectedValue != "0")
                {
                    Panel_searchDetailIndex.Visible = false;
                    //setGridData(GvDetail, ViewState["Va_DetailCarbooking"]);
                    //litDebug.Text = "1";

                    data_carbooking data_u0_document_detail = new data_carbooking();
                    cbk_u0_document_detail u0_document_detail = new cbk_u0_document_detail();
                    data_u0_document_detail.cbk_u0_document_list = new cbk_u0_document_detail[1];
                    u0_document_detail.cemp_idx = _emp_idx;
                    u0_document_detail.cemp_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                    u0_document_detail.cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                    u0_document_detail.cemp_rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
                    u0_document_detail.cemp_joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

                    data_u0_document_detail.cbk_u0_document_list[0] = u0_document_detail;


                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_detail));
                    data_u0_document_detail = callServicePostCarBooking(_urlGetCbkDetailCarbooking, data_u0_document_detail);
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_type_travel_detail));

                    //ViewState["Va_DetailCarbooking"] = data_u0_document_detail.cbk_u0_document_list;



                    cbk_u0_document_detail[] _item_FilterStatus = (cbk_u0_document_detail[])data_u0_document_detail.cbk_u0_document_list;

                    var _linq_FilterStatusIndex = (from data in _item_FilterStatus
                                                   where 
                                                   data.staidx == int.Parse(ddlStatusFilter.SelectedValue)

                                                   select data
                                            ).ToList();

                    _set_statusFilter = int.Parse(ddlStatusFilter.SelectedValue);

                    //

                    ViewState["Va_DetailCarbooking"] = _linq_FilterStatusIndex;
                    setGridData(GvDetail, ViewState["Va_DetailCarbooking"]);
                    //ViewState["Va_DetailCarbooking"] = null;

                }
                else
                {
                    Panel_searchDetailIndex.Visible = true;
                    //ViewState["Va_DetailCarbooking_linq"] = null;
                    getDetailCarBooking();
                    
                    //setGridData(GvDetail, ViewState["Va_DetailCarbooking"]);
                }

                break;
            case "ddlStatusCarBooking":

                getDetailChoiceOutplanTable(ddlStatusOutplan, 0);

                if (ddlStatusCarBooking.SelectedValue == "4")
                {
                    ddlStatusOutplan.Enabled = true;
                }
                else
                {
                    ddlStatusOutplan.Enabled = false;
                }

                break;

        }
    }

    protected void TextBoxChanged(object sender, EventArgs e)
    {
        var txtName = (TextBox)sender;
        
        switch (txtName.ID)
        {

            case "txtDateStart_create":


                //litDebug.Text = txtDateStart_create.Text;
                //DropDownList ddlDetailtypecar = (DropDownList)FvCreateCarBooking.FindControl("ddlDetailtypecar");
                //TextBox txtDateStart_create = (TextBox)FvCreateCarBooking.FindControl("txtDateStart_create");
                //TextBox txt_timestart_create = (TextBox)FvCreateCarBooking.FindControl("txt_timestart_create");
                //TextBox txtDateEnd_create = (TextBox)FvCreateCarBooking.FindControl("txtDateEnd_create");
                //TextBox txt_timeend_create = (TextBox)FvCreateCarBooking.FindControl("txt_timeend_create");

                //if (ddlDetailtypecar.SelectedValue != "0" && txtDateStart_create.Text != "")
                //{
                //    litDebug.Text = "2222";
                //}
                //else
                //{
                //    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรอกราคา/หน่วย ---');", true);
                //    break;

                //}




                //get_detailcar_create();


                //linkBtnTrigger(btnSaveDetailMA);

                ////foreach (GridViewRow row in GvTopicDetailMa.Rows)
                ////{
                ////    CheckBox chk_detailTopic_ma_txt = (CheckBox)row.Cells[0].FindControl("chk_detailTopic_ma");
                ////    TextBox txt_Quantity_txt = (TextBox)row.Cells[2].FindControl("txt_Quantity");
                ////    TextBox txt_price_uit_txt = (TextBox)row.Cells[3].FindControl("txt_price_uit");
                ////    TextBox txt_price_total_txt = (TextBox)row.Cells[4].FindControl("txt_price_total");
                ////    decimal total_price = 0;

                ////    if (chk_detailTopic_ma_txt.Checked)
                ////    {
                ////        if (txt_Quantity_txt.Text != "" && txt_price_uit_txt.Text != "")
                ////        {
                ////            linkBtnTrigger(btnSaveDetailMA);
                ////            //litDebug.Text = Convert.ToInt32(txt_Quantity_txt.Text).ToString();
                ////            total_price = Convert.ToDecimal(txt_Quantity_txt.Text) * Convert.ToDecimal(txt_price_uit_txt.Text);
                ////            //litDebug.Text = Convert.ToDecimal(total_price).ToString();
                ////            txt_price_total_txt.Text = Convert.ToDecimal(total_price).ToString();

                ////        }
                ////        else
                ////        {
                ////            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรอกราคา/หน่วย ---');", true);

                ////            break;
                ////        }
                ////    }


                ////}

                break;


            
        }

    }

    //protected void startDate_TextChanged(object sender, EventArgs e)
    //{
    //    litDebug.Text = "1111";
    //    // Write your cde logic
    //}


    //protected void txtPickupDate_TextChanged(object sender, EventArgs e)
    //{
    //    TextBox txtDateStart_create = (TextBox)FvCreateCarBooking.FindControl("txtDateStart_create");
    //    DateTime pickupDate = DateTime.Parse(txtDateStart_create.Text);
    //    litDebug.Text = "2222";
    //    litDebug.Text = pickupDate.ToString();
    //}

    ////protected void get_detailcar_create()
    ////{
    ////    DropDownList ddlDetailtypecar = (DropDownList)FvCreateCarBooking.FindControl("ddlDetailtypecar");
    ////    TextBox txtDateStart_create = (TextBox)FvCreateCarBooking.FindControl("txtDateStart_create");
    ////    TextBox txt_timestart_create = (TextBox)FvCreateCarBooking.FindControl("txt_timestart_create");
    ////    TextBox txtDateEnd_create = (TextBox)FvCreateCarBooking.FindControl("txtDateEnd_create");
    ////    TextBox txt_timeend_create = (TextBox)FvCreateCarBooking.FindControl("txt_timeend_create");


    ////    litDebug.Text = txtDateStart_create.Text;
    ////    if (ddlDetailtypecar.SelectedValue != "0" && txtDateStart_create.Text != "")
    ////    {
    ////        litDebug.Text = "2222";
    ////    }
    ////    else
    ////    {
    ////        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- กรอกราคา/หน่วย ---');", true);
    ////        return;
           
    ////    }
    ////}

    #endregion selected Changed

    #region gridview
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {

            case "GvDetail":
                setGridData(GvDetail, ViewState["Va_DetailCarbooking"]);
                setOntop.Focus();

                break;

            case "GvWaitDetailApprove":
                setGridData(GvWaitDetailApprove, ViewState["Vs_WaitDetailApprove"]);
                setOntop.Focus();
                break;

            case "GvDetailTableReport":
                setGridData(GvDetailTableReport, ViewState["Vs_DetailReportCarbookingTable"]);
                setOntop.Focus();
                break;

            case "GvViewDetailReport":
                //GvViewDetailReport.PageIndex = e.NewPageIndex;
                setGridData(GvViewDetailReport, ViewState["Vs_ViewDetailReportCarTable"]);

                //Label lbl = (Label)e.Row.FindControl("lit_total");
                //lbl.Text = ViewState["Total_Expenses"].ToString();//tot_actual.ToString();

                GvViewDetailReport.PageIndex = 0;

                mergeCell(GvViewDetailReport);

                setOntop.Focus();
                break;

            case "GvDetailGraph":

                setGridData(GvDetailGraph, ViewState["Vs_ReportCountBookingDept"]);

                break;


        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvDetail":
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    UpdatePanel UpdatePanel_Header = (UpdatePanel)e.Row.FindControl("UpdatePanel_Header");
                    DropDownList ddlStatusFilter = (DropDownList)e.Row.FindControl("ddlStatusFilter");
                    getStatusDocument(ddlStatusFilter, _set_statusFilter);

                    if(ViewState["Vs_SetHeader_Filter"].ToString() == "2")
                    {
                        UpdatePanel_Header.Visible = false;
                    }
                    else
                    {
                        UpdatePanel_Header.Visible = true;
                    }

                }

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                }
                break;
            case "GvDetailCarUse":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    getDetailExpenses(0);

                    GvDetailCarUse.Columns[5].Visible = false;
                }
                break;

            case "GvShowDetailCarUse":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    // selected usr car in radio use car
                    Label lbl_car_use_status_idx = (Label)e.Row.FindControl("lbl_car_use_status_idx");
                    RadioButtonList rdoStatusUseViewDetail = (RadioButtonList)e.Row.FindControl("rdoStatusUseViewDetail");
                    Label lbl_u2_document_idx_viewcaruse = (Label)e.Row.FindControl("lbl_u2_document_idx_viewcaruse");
                    Label lbl_u0_document_idx_viewcaruse = (Label)e.Row.FindControl("lbl_u0_document_idx_viewcaruse");

                    if (lbl_car_use_status_idx.Text != "0")
                    {
                        rdoStatusUseViewDetail.SelectedValue = lbl_car_use_status_idx.Text;
                   
                    }
                    // selected usr car in radio use car //_urlGetCbkViewDetailCarUseExpenses

                    //select in gv expenses
                    data_carbooking data_u0_document_view_expenses = new data_carbooking();
                    cbk_u3_document_detail u0_document_view_expenses = new cbk_u3_document_detail();
                    data_u0_document_view_expenses.cbk_u3_document_list = new cbk_u3_document_detail[1];

                    u0_document_view_expenses.u2_document_idx = int.Parse(lbl_u2_document_idx_viewcaruse.Text);


                    data_u0_document_view_expenses.cbk_u3_document_list[0] = u0_document_view_expenses;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_headapprove));

                    data_u0_document_view_expenses = callServicePostCarBooking(_urlGetCbkViewDetailCarUseExpenses, data_u0_document_view_expenses);


                    ViewState["Vs_TotalExpenses_View"] = data_u0_document_view_expenses.cbk_u3_document_list;
                    setGridData(GvShowDetailExpenses, ViewState["Vs_TotalExpenses_View"]);


                    //view detail directory
                    HyperLink btnViewFileUseCar = (HyperLink)e.Row.FindControl("btnViewFileUseCar");
                    //HiddenField hidFile112 = (HiddenField)e.Row.FindControl("hidFile112");
                    string filePath_Viewusecar = ConfigurationManager.AppSettings["path_flie_carbooking"];
                    string directoryName_ViewUsecar_U0 = "u0_doc" + lbl_u0_document_idx_viewcaruse.Text;
                    string fileName_ViewUsecar_U2 = directoryName_ViewUsecar_U0 + "-" + "u2_doc" + lbl_u2_document_idx_viewcaruse.Text;

                    if (Directory.Exists(Server.MapPath(filePath_Viewusecar + fileName_ViewUsecar_U2)))
                    {

                        string[] filesPath_view = Directory.GetFiles(Server.MapPath(filePath_Viewusecar + fileName_ViewUsecar_U2));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string path in filesPath_view)
                        {
                            string getfiles = "";
                            getfiles = Path.GetFileName(path);

                            btnViewFileUseCar.NavigateUrl = filePath_Viewusecar + fileName_ViewUsecar_U2 + "/" + getfiles;

                            //litDebug.Text = filePath_view + directoryName + "/" + getfiles;
                        }
                        btnViewFileUseCar.Visible = true;
                    }
                    else
                    {
                        btnViewFileUseCar.Visible = false;

                    }


                }
                break;

            case "GvViewDetailReport":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label lbl_sum_expenses_reportview = (Label)e.Row.FindControl("lbl_sum_expenses_reportview");
                    Label lbl_sum_expenses_reportview_value = (Label)e.Row.FindControl("lbl_sum_expenses_reportview_value");

                    Label lbl_sum_expenses_outplan_decimal_reportview = (Label)e.Row.FindControl("lbl_sum_expenses_outplan_decimal_reportview");
                    Label lbl_sum_expenses_outplan_decimal_reportview_value = (Label)e.Row.FindControl("lbl_sum_expenses_outplan_decimal_reportview_value");

                    lbl_sum_expenses_reportview_value.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_sum_expenses_reportview.Text));
                    lbl_sum_expenses_outplan_decimal_reportview_value.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_sum_expenses_outplan_decimal_reportview.Text));


                }

                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    cbk_searchreport_car_detail[] _item_Result = (cbk_searchreport_car_detail[])ViewState["Vs_ViewDetailReportCarTable"];

                    var _linqTest = (from data in _item_Result
                                     select new
                                     {
                                         data.sum_expenses,
                                         data.sum_expenses_decimal,
                                         data.sum_expenses_outplan_decimal


                                     }).ToList();


                    if (_linqTest.Count() > 0)
                    {

                        for (int z = 0; z < _linqTest.Count(); z++)
                        {
                            returnResult += Convert.ToInt32(_linqTest[z].sum_expenses.ToString());
                            tot_actual += Convert.ToDecimal(_linqTest[z].sum_expenses_decimal.ToString());
                            tot_actualoutplan += Convert.ToDecimal(_linqTest[z].sum_expenses_outplan_decimal.ToString());

                        }
                    }

                    Label lbl = (Label)e.Row.FindControl("lit_total");
                    Label lit_totaloutplan = (Label)e.Row.FindControl("lit_totaloutplan");
                    //lbl.Text = returnResult.ToString();//ViewState["Total_Expenses"].ToString();//tot_actual.ToString();

                    lbl.Text = String.Format("{0:N2}", tot_actual); // Convert.ToString(tot_actual);
                    lit_totaloutplan.Text = String.Format("{0:N2}", tot_actualoutplan); // Convert.ToString(tot_actual);
                    //litDebug.Text = String.Format("{0:N2}", tot_actual);

                }

                break;
            case "GvDetailGraph":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    if(condition_ == 2)
                    {
                        Label lbl_sum_expenses_reportgraph = (Label)e.Row.FindControl("lbl_sum_expenses_reportgraph");
                        Label lbl_sum_expenses_reportgraph_value = (Label)e.Row.FindControl("lbl_sum_expenses_reportgraph_value");

                        lbl_sum_expenses_reportgraph_value.Text = String.Format("{0:N2}", Convert.ToDecimal(lbl_sum_expenses_reportgraph.Text));

                        //cbk_searchreport_car_detail[] _item_Result = (cbk_searchreport_car_detail[])ViewState["Vs_ReportCountBookingDept"];

                        //var _linqexpenses_report = (from data in _item_Result
                        //                  select new
                        //                  {
                        //                      data.sum_expenses,
                        //                      data.sum_expenses_decimal


                        //                  }).ToList();

                        //if (_linqexpenses_report.Count() > 0)
                        //{

                        //    for (int z = 0; z < _linqexpenses_report.Count(); z++)
                        //    {
                        //        returnResult += Convert.ToInt32(_linqexpenses_report[z].sum_expenses.ToString());
                        //        tot_actual += Convert.ToDecimal(_linqexpenses_report[z].sum_expenses_decimal.ToString());


                        //    }
                        //}
                        //lbl_sum_expenses_reportgraph.Text = String.Format("{0:N2}", tot_actual);
                    }
                }
                break;
            case "GvDetailCarInBooking":

                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    Label lbl_m0_car_idx_viewdetail = (Label)e.Row.FindControl("lbl_m0_car_idx_viewdetail");

                    //View File In Detail Car
                    var btnViewFileCar = (HyperLink)e.Row.FindControl("btnViewFileCar");
                    //var btnViewFileRoom_ = (Image)e.Row.FindControl("btnViewFileRoom");

                    string filePath_View = ConfigurationManager.AppSettings["path_flie_cardetail"]; 
                    string directoryName = lbl_m0_car_idx_viewdetail.Text;//lbl_place_name_file.Text.Trim() + lbl_room_name_en_view_file.Text + lbl_m0_room_idx_file.Text;

                    if (Directory.Exists(Server.MapPath(filePath_View + directoryName)))
                    {
                        string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View + directoryName));
                        List<ListItem> files = new List<ListItem>();
                        foreach (string path in filesPath)
                        {
                            string getfiles = "";
                            getfiles = Path.GetFileName(path);
                            btnViewFileCar.NavigateUrl = filePath_View + directoryName + "/" + getfiles;
                            //btnViewFileRoom_.ImageUrl = filePath_View + directoryName + "/" + getfiles;

                            HtmlImage img = new HtmlImage();
                            img.Src = filePath_View + directoryName + "/" + getfiles;
                            img.Height = 100;
                            btnViewFileCar.Controls.Add(img);



                        }
                        btnViewFileCar.Visible = true;
                    }
                    else
                    {
                        btnViewFileCar.Visible = false;
                        //btnViewFileRoom_.Text = "-";
                    }
                    //View File In Detail Car
                }
                break;

            case "GvShowDetailExpenses":
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    cbk_u3_document_detail[] _item_view_expenses = (cbk_u3_document_detail[])ViewState["Vs_TotalExpenses_View"];

                    var _linq_expenses = (from data in _item_view_expenses
                                     select new
                                     {
                                         data.count_expenses,
                                         data.count_expenses_decimal


                                     }).ToList();


                    if (_linq_expenses.Count() > 0)
                    {

                        for (int z = 0; z < _linq_expenses.Count(); z++)
                        {
                            returnResult += Convert.ToInt32(_linq_expenses[z].count_expenses.ToString());
                            tot_actual += Convert.ToDecimal(_linq_expenses[z].count_expenses_decimal.ToString());


                        }
                    }

                    Label lit_value_expenses = (Label)e.Row.FindControl("lit_value_expenses");
                    //lbl.Text = returnResult.ToString();//ViewState["Total_Expenses"].ToString();//tot_actual.ToString();

                    lit_value_expenses.Text = String.Format("{0:N2}", tot_actual); // Convert.ToString(tot_actual);
                    //litDebug.Text = String.Format("{0:N2}", tot_actual);

                }
                break;
           
        }
    }

    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {
            case "GvViewDetailReport":
                for (int rowIndex = GvViewDetailReport.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvViewDetailReport.Rows[rowIndex];
                    GridViewRow previousRow = GvViewDetailReport.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[0].FindControl("lbl_dept_name_th_reportview")).Text == ((Label)previousRow.Cells[0].FindControl("lbl_dept_name_th_reportview")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }


                    if (((Label)currentRow.Cells[1].FindControl("lbl_costcenter_no_reportview")).Text == ((Label)previousRow.Cells[1].FindControl("lbl_costcenter_no_reportview")).Text)
                    {
                        if (previousRow.Cells[1].RowSpan < 2)
                        {
                            currentRow.Cells[1].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[1].RowSpan = previousRow.Cells[1].RowSpan + 1;

                        }
                        previousRow.Cells[1].Visible = false;
                    }


                }
                break;
        }
    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;

            switch (cmdName)
            {
                case "cmdRemoveCar":
                    GridView gvAddCarBooking = (GridView)FvHrApprove.FindControl("gvAddCarBooking");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsCarDetailList"];
                    dsContacts.Tables["dsCarDetailTable"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(gvAddCarBooking, dsContacts.Tables["dsCarDetailTable"]);
                    if (dsContacts.Tables["dsCarDetailTable"].Rows.Count < 1)
                    {
                        gvAddCarBooking.Visible = false;
                    }
                    break;
                case "btnRemove":
                    GridView gvDetailOutplanList = (GridView)updateUseDistanceOutInsert.FindControl("gvDetailOutplanList");
                    GridViewRow row_= (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_ = row_.RowIndex;
                    DataSet dsContacts_ = (DataSet)ViewState["vsCarDetailOutplanList"];
                    dsContacts_.Tables["dsCarDetailOutplanTable"].Rows[rowIndex_].Delete();
                    dsContacts_.AcceptChanges();
                    setGridData(gvDetailOutplanList, dsContacts_.Tables["dsCarDetailOutplanTable"]);
                    if (dsContacts_.Tables["dsCarDetailOutplanTable"].Rows.Count < 1)
                    {
                        gvDetailOutplanList.Visible = false;
                    }
                    break;


            }
        }
    }

    #endregion gridview

    #region Directories_File URL
    public void SearchDirectories(string _dir, String target)
    {

        string dirfiles = _dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");

        //litDebug.Text = target.ToString();//files.Length.ToString();
        int i = 0;

        //litDebug.Text = Server.MapPath(_dir);
        if (Directory.Exists(Server.MapPath(_dir)))
        {
            string[] filesPath_view = Directory.GetFiles(Server.MapPath(_dir));
            List<ListItem> files = new List<ListItem>();
            foreach (string path in filesPath_view)
            {
                string getfiles = "";
                getfiles = Path.GetFileName(path);

                i++;

            }
        }
        else
        {

        }

    }


    public static bool UrlExists(string url)
    {
        try
        {
            var request = WebRequest.Create(url) as HttpWebRequest;
            if (request == null) return false;
            request.Method = "HEAD";
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                return response.StatusCode == HttpStatusCode.OK;
            }
        }
        catch (UriFormatException)
        {
            //Invalid Url
            return false;
        }
        catch (WebException)
        {
            //Unable to access url
            return false;
        }
    }

    #endregion

    #region radiobutton
    protected void onSelectedIndexChanged(object sender, EventArgs e)
    {
        var rdName = (RadioButtonList)sender;
        //var chkName = (CheckBoxList)sender;

        switch (rdName.ID)
        {
            case "rdoStatusUse":
                //litDebug.Text = "666";

                //litDebug.Text = chk_selectdetail.ToString();
                setUseCar_chk();
                break;
            case "rdouse_outplan":

                CleardataSetCarDetailOutplanList();
                txt_Distance.Text = String.Empty;
                txt_expenses_outplan.Text = String.Empty;
                txt_Detailoutplan.Text = String.Empty;

                if (rdouse_outplan.SelectedValue == "2")
                {
                    updateUseDistanceOutInsert.Visible = true;
                    getOrganizationList(ddlorg_idx);
                    getDepartmentList(ddlrdept_idx, int.Parse(ddlorg_idx.SelectedValue));
                    getSectionList(ddlrsec_idx, int.Parse(ddlorg_idx.SelectedValue), int.Parse(ddlrdept_idx.SelectedValue));
                }
                else
                {
                    updateUseDistanceOutInsert.Visible = false;
                }

                break;

        }
    }
    #endregion radiobutton

    protected void setUseCar_chk()
    {

        foreach (GridViewRow row in GvDetailCarUse.Rows)
        {
            RadioButtonList rdoStatusUse = (RadioButtonList)row.Cells[1].FindControl("rdoStatusUse");
            Label Check_Count_List = (Label)row.Cells[1].FindControl("Check_Count_List");
            TextBox txt_mile_start = (TextBox)row.Cells[2].FindControl("txt_mile_start");
            TextBox txt_mile_end = (TextBox)row.Cells[2].FindControl("txt_mile_end");

            GridView GvExpenses = (GridView)row.Cells[3].FindControl("GvExpenses");
            //TextBox txt_value_caruse = (TextBox)GvExpenses.FindControl("txt_value_caruse");

            //rbl_responses.ClearSelection();

            //RadioButtonList rdoStatusUse = (RadioButtonList)GvDetailCarUse.FindControl("rdoStatusUse");

            //litDebug1.Text = rdoStatusUse.Items.Count.ToString() + ",";
            int count = 0;
            string count_check = "";
            for (int i = 0; i < rdoStatusUse.Items.Count; i++)
            {
                if (rdoStatusUse.Items[i].Selected == true)
                {
                    string str = rdoStatusUse.Items[i].Text;
                    //MessageBox.Show(str);

                    var itemValue = rdoStatusUse.Items[i].Value;


                    ////if (rdoStatusUse.SelectedValue == "1") //use car
                    ////{

                    ////    row.Cells[2].Enabled = true;
                    ////    row.Cells[3].Enabled = true;
                    ////    row.Cells[4].Enabled = true;
                    ////    row.Cells[5].Enabled = true;
                    ////}
                    ////else
                    ////{
                    ////    row.Cells[2].Enabled = false;
                    ////    row.Cells[3].Enabled = false;
                    ////    row.Cells[4].Enabled = false;
                    ////    row.Cells[5].Enabled = false;

                    ////    txt_mile_start.Text = "";
                    ////    txt_mile_end.Text = "";
                    ////    //txt_value_caruse.Text = "";

                    ////    foreach (GridViewRow row_in in GvExpenses.Rows)
                    ////    {
                    ////        //TextBox txt_mile_end = (TextBox)row.Cells[2].FindControl("txt_mile_end");
                    ////        TextBox txt_value_caruse = (TextBox)row_in.Cells[1].FindControl("txt_value_caruse");
                    ////        txt_value_caruse.Text = "";
                    ////    }
                    ////}
                    //chk_select_un1.Text += itemValue;
                    count++;
                    count_check = count.ToString();
                    GvDetailCarUse.Columns[5].Visible = false;
                }
                else
                {
                    count--;
                    count_check = count.ToString();
                    GvDetailCarUse.Columns[5].Visible = false;


                }
            }

            Check_Count_List.Text = count_check;

            if (Check_Count_List.Text == "0")
            {
                ViewState["Bind_DataShowExpenses"] = Check_Count_List.Text;
                GvDetailCarUse.Columns[5].Visible = true;
            }
        }



    }

    #region Formview Databind
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            FormView FvName = (FormView)sender;

            switch (FvName.ID)
            {
                case "FvWaitApproveDetail":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {

                    }

                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {


                    }
                    break;
                case "FvDetailCarBooking":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {
                        
                        Label lbl_u0_document_idx_view = (Label)FvName.FindControl("lbl_u0_document_idx_view");

                        
                        if (lbl_u0_document_idx_view.Text != "")
                        {
                            var btnViewFileCarDetailCreate = (HyperLink)FvName.FindControl("btnViewFileCarDetailCreate");

                            ////string filePath_View = ConfigurationManager.AppSettings["path_flie_carbooking_create"];
                            //////string directoryName_View = "u0_doc" + lbl_u0_document_idx_view.Text;//lbl_place_name_edit.Text.Trim() + lbl_room_name_en_edit.Text + lblm0_room_idx_edit.Text;

                            ////string directoryName_View = "u0_doc" + lbl_u0_document_idx_view.Text;//ViewState["vs_createRoom_DetailFile"].ToString();
                            ////string directoryName_View1 = "u0_doc" + lbl_u0_document_idx_view.Text;

                            ////string fileName_upload_view = directoryName_View + '-' + directoryName_View1;
                            //////string directoryName_View1 = "u0_doc" + '-' + lbl_u0_document_idx_view.Text;
                            ////litDebug.Text = fileName_upload_view.ToString();


                            string filePath_View = ConfigurationManager.AppSettings["path_flie_carbooking_create"];
                            string directoryName_View = "u0_doc" + lbl_u0_document_idx_view.Text + "-" + "u0_doc" + lbl_u0_document_idx_view.Text;//_u0_doc_idx.ToString();//lbl_place_name_edit.Text.Trim() + lbl_room_name_en_edit.Text + lblm0_room_idx_edit.Text;

                            if (Directory.Exists(Server.MapPath(filePath_View + directoryName_View)))
                            {
                                string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View + directoryName_View));
                                List<ListItem> files = new List<ListItem>();
                                foreach (string path in filesPath)
                                {
                                    string getfiles = "";
                                    getfiles = Path.GetFileName(path);
                                    btnViewFileCarDetailCreate.NavigateUrl = filePath_View + directoryName_View + "/" + getfiles;

                                    //HtmlImage imgedit = new HtmlImage();
                                    //imgedit.Src = filePath_View + directoryName_View + "/" + getfiles;
                                    //imgedit.Height = 100;
                                    //btnViewCarDetailOut.Controls.Add(imgedit);

                                }
                                btnViewFileCarDetailCreate.Visible = true;
                            }
                            else
                            {
                                btnViewFileCarDetailCreate.Visible = false;
                            }

                        }
                        
                    }
                    break;

                case "FvDetailWaitApprove":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {

                        Label lbl_u0_document_idx_approve = (Label)FvName.FindControl("lbl_u0_document_idx_approve");


                        if (lbl_u0_document_idx_approve.Text != "")
                        {
                            var btnViewFileCarDetailWaitapprove = (HyperLink)FvName.FindControl("btnViewFileCarDetailWaitapprove");

                            string filePath_View = ConfigurationManager.AppSettings["path_flie_carbooking_create"];
                            string directoryName_View = "u0_doc" + lbl_u0_document_idx_approve.Text + "-" + "u0_doc" + lbl_u0_document_idx_approve.Text;//_u0_doc_idx.ToString();//lbl_place_name_edit.Text.Trim() + lbl_room_name_en_edit.Text + lblm0_room_idx_edit.Text;

                            if (Directory.Exists(Server.MapPath(filePath_View + directoryName_View)))
                            {
                                string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View + directoryName_View));
                                List<ListItem> files = new List<ListItem>();
                                foreach (string path in filesPath)
                                {
                                    string getfiles = "";
                                    getfiles = Path.GetFileName(path);
                                    btnViewFileCarDetailWaitapprove.NavigateUrl = filePath_View + directoryName_View + "/" + getfiles;

                                    //HtmlImage imgedit = new HtmlImage();
                                    //imgedit.Src = filePath_View + directoryName_View + "/" + getfiles;
                                    //imgedit.Height = 100;
                                    //btnViewCarDetailOut.Controls.Add(imgedit);

                                }
                                btnViewFileCarDetailWaitapprove.Visible = true;
                            }
                            else
                            {
                                btnViewFileCarDetailWaitapprove.Visible = false;
                            }

                        }

                    }
                    break;


            }
        }
    }
    #endregion Formview Databind

    #region reuse

    protected void initPageLoad()
    {

    }

    protected void initPage()
    {
        setActiveTab("docDetailCar", 0, 0, 0, 0, 0, 0);
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_carbooking callServicePostCarBooking(string _cmdUrl, data_carbooking _data_carbooking)
    {
        _localJson = _funcTool.convertObjectToJson(_data_carbooking);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_carbooking = (data_carbooking)_funcTool.convertJsonToObject(typeof(data_carbooking), _localJson);


        return _data_carbooking;
    }

    protected data_roombooking callServicePostRoomBooking(string _cmdUrl, data_roombooking _data_roombooking)
    {
        _localJson = _funcTool.convertObjectToJson(_data_roombooking);

        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        _data_roombooking = (data_roombooking)_funcTool.convertJsonToObject(typeof(data_roombooking), _localJson);


        return _data_roombooking;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _car_use_idx)
    {
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        //set tab car
        setFormData(FvDetailUseCarSearch, FormViewMode.ReadOnly, null);



        //set tab create
        setFormData(fvEmpDetail, FormViewMode.ReadOnly, null);
        setFormData(FvCreateCarBooking, FormViewMode.ReadOnly, null);
        //setFormData(FvExpenses, FormViewMode.ReadOnly, null);

        //tab detail
        GvDetail.Visible = false;
        setGridData(GvDetail, null);
        FvDetailCarBooking.Visible = false;
        //setFormData(FvDetailCarBooking, FormViewMode.ReadOnly, null);

        div_LogViewDetail.Visible = false;
        Update_BackToDetail.Visible = false;
        UpDate_InsertCarUseHR.Visible = false;
        Panel_AddCarUseHr.Visible = false;
        updateUseDistanceOutInsert.Visible = false;
        rdouse_outplan.ClearSelection();

        Update_SaveCarUseHr.Visible = false;
        Update_SaveCarUseHrOut.Visible = false;

        UpdatePanel_ShowDetailCarUse.Visible = false;
        Panel_ShowDetailCarUse.Visible = false;
        Panel_searchDetailIndex.Visible = false;

        setFormData(FvSearchDetailIndex, FormViewMode.ReadOnly, null);
        Update_CancelCarDetail.Visible = false;

        //tab detail

        //setGridData(GvDetailCarInBooking, null);

        //tab approve
        Div_Search_WaitApprove.Visible = false;
        setFormData(FvSearchWaitApprove, FormViewMode.ReadOnly, null);
        GvWaitDetailApprove.Visible = false;
        setGridData(GvWaitDetailApprove, null);
        Update_BackToWaitDetail.Visible = false;

        FvDetailWaitApprove.Visible = false;
        //setFormData(FvDetailWaitApprove, FormViewMode.ReadOnly, null);


        setFormData(FvHeadUserApprove, FormViewMode.ReadOnly, null);
        div_LogViewDetailWaitApprove.Visible = false;
        setFormData(FvHrApprove, FormViewMode.ReadOnly, null);

        //tab report
        Update_PanelSearchReport.Visible = false;
        Update_PnTableReportDetail.Visible = false;
        Update_PnGraphReportDetail.Visible = false;
        Update_PanelGraph.Visible = false;
        ddlTypeReport.ClearSelection();

        Update_PanelReportTable.Visible = false;
        Div_CarUseStatus.Visible = false;
        Update_PanelDetailReportTable.Visible = false;
        Update_PanelBackToSearch.Visible = false;

        switch (activeTab)
        {
            case "docDetailCar":

                switch (_chk_tab)
                {
                    case 0:
                        setFormData(FvDetailUseCarSearch, FormViewMode.Insert, null);
                        DropDownList ddlCarUseNameSearch = (DropDownList)FvDetailUseCarSearch.FindControl("ddlCarUseNameSearch");
                        DropDownList ddlDetailtypecarSearch = (DropDownList)FvDetailUseCarSearch.FindControl("ddlDetailtypecarSearch");

                        getDetailCarUse(ddlCarUseNameSearch, 0);

                        //getDetailTypeCar(ddlDetailtypecarSearch, 0);

                        //getShowDetailRoomSlider();

                        break;

                }
                setOntop.Focus();
                break;
            case "docDetail":
                ViewState["Vs_SetHeader_Filter"] = 0;
                switch (_chk_tab)
                {
                    case 0:

                        Panel_searchDetailIndex.Visible = true;

                        GvDetail.Visible = true;
                        getDetailCarBooking();
                        setOntop.Focus();

                        break;
                    case 2: // show tab search

                        setFormData(FvSearchDetailIndex, FormViewMode.Insert, null);

                        DropDownList ddlPlaceSearchDetail = (DropDownList)FvSearchDetailIndex.FindControl("ddlPlaceSearchDetail");
                        DropDownList ddlCarUseSearchDetail = (DropDownList)FvSearchDetailIndex.FindControl("ddlCarUseSearchDetail");
                        DropDownList ddlDetailtypecarSearchDetail = (DropDownList)FvSearchDetailIndex.FindControl("ddlDetailtypecarSearchDetail");
                        DropDownList ddl_StatusDetail = (DropDownList)FvSearchDetailIndex.FindControl("ddl_StatusDetail");
                        //DropDownList ddlType_booking_idx = (DropDownList)FvCreateCarBooking.FindControl("ddlType_booking_idx");
                        //DropDownList ddlType_booking_idx = (DropDownList)FvCreateCarBooking.FindControl("ddlType_booking_idx");

                        getPlace(ddlPlaceSearchDetail, 0);
                        getDetailCarUse(ddlCarUseSearchDetail, 0);
                        getDetailTypeCar(ddlDetailtypecarSearchDetail, 0);

                        getStatusDocument(ddl_StatusDetail, 0);

                        GvDetail.Visible = true;

                        ViewState["Vs_SetHeader_Filter"] = _chk_tab;

                        getDetailCarBooking();
                        setOntop.Focus();

                        break;

                    case 1:
                        Update_BackToDetail.Visible = true;
                        getViewDetailCarBooking(uidx);
                        div_LogViewDetail.Visible = true;

                        getLogDetailCarBooking(uidx);

                        Label lbl_cemp_idx_view = (Label)FvDetailCarBooking.FindControl("lbl_cemp_idx_view");
                        Label lbl_date_time_chekbutton_view = (Label)FvDetailCarBooking.FindControl("lbl_date_time_chekbutton_view");
                        Label lbl_car_use_idx_view = (Label)FvDetailCarBooking.FindControl("lbl_car_use_idx_view");
                        Label lbl_car_use_idx_ = (Label)FvDetailCarBooking.FindControl("lbl_car_use_idx_");

                        IFormatProvider culture = new CultureInfo("en-US", true);
                        DateTime DateToday_Set = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy HH:mm"), "dd/MM/yyyy HH:mm", culture); //Date Time Today Incheck
                        DateTime DateTime_DBSet = DateTime.ParseExact(lbl_date_time_chekbutton_view.Text, "dd/MM/yyyy HH:mm", culture);


                        var result_time = DateTime_DBSet.Subtract(DateToday_Set).TotalMinutes; // result time chrck > 30 min because cancel in room

                        //litDebug.Text = result_time.ToString();

                        UpdatePanel Update_DetailCarInBooking = (UpdatePanel)FvDetailCarBooking.FindControl("Update_DetailCarInBooking");
                        Update_DetailCarInBooking.Visible = false;

                        switch (nodeidx)
                        {
                            case 2:
                            case 3:

                                data_carbooking data_u0_document_expenses = new data_carbooking();
                                cbk_u0_document_detail u0_document_expenses = new cbk_u0_document_detail();
                                data_u0_document_expenses.cbk_u0_document_list = new cbk_u0_document_detail[1];

                                u0_document_expenses.cemp_idx = _emp_idx;
                                u0_document_expenses.u0_document_idx = uidx;
                                u0_document_expenses.m0_node_idx = nodeidx;
                                u0_document_expenses.staidx = staidx;
                                u0_document_expenses.cemp_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                                u0_document_expenses.cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                                u0_document_expenses.cemp_rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
                                u0_document_expenses.cemp_joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

                                data_u0_document_expenses.cbk_u0_document_list[0] = u0_document_expenses;

                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_approve));
                                data_u0_document_expenses = callServicePostCarBooking(_urlSetCbkCreateExpensesHr, data_u0_document_expenses);

                                //Session["emp_idx"].ToString()
                                //use car in 30 minute
                                if ((_emp_idx == int.Parse(lbl_cemp_idx_view.Text)) && lbl_car_use_idx_view.Text == "0")
                                {
                                    //litDebug.Text = "2";
                                    //litDebug.Text = result_time.ToString();
                                    Update_CancelCarDetail.Visible = true;
                                }


                                break;
                            case 5: //node 5 hr save use carbooking

                                //litDebug1.Text = DateToday_Set.ToString();
                                //litDebug.Text = result_time.ToString();
                                //ViewState["Vs_cemp_idx_view"] = _cemp_idx_view;

                                //linkBtnTrigger(btnViewDetail);

                                if (lbl_car_use_idx_.Text == "1") // car use in
                                {
                                    Update_DetailCarInBooking.Visible = true;
                                    getDetailCarInBooking(uidx);
                                }

                                //linkBtnTrigger(btnInsertCarUseHr);
                                //linkBtnTrigger(btnViewDetail);

                                //Check Permission
                                lbl_u0_document_idx_caruse.Text = uidx.ToString();
                                lbl_node_idx_caruse.Text = nodeidx.ToString();
                                lbl_status_idx_caruse.Text = staidx.ToString();
                                lbl_actor_idx_caruse.Text = actor_idx.ToString();


                                data_carbooking data_u0_document_expenses_ = new data_carbooking();
                                cbk_u0_document_detail u0_document_expenses_ = new cbk_u0_document_detail();
                                data_u0_document_expenses_.cbk_u0_document_list = new cbk_u0_document_detail[1];

                                u0_document_expenses_.cemp_idx = _emp_idx;
                                u0_document_expenses_.u0_document_idx = uidx;
                                u0_document_expenses_.m0_node_idx = nodeidx;
                                u0_document_expenses_.staidx = staidx;
                                u0_document_expenses_.cemp_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                                u0_document_expenses_.cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                                u0_document_expenses_.cemp_rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
                                u0_document_expenses_.cemp_joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

                                data_u0_document_expenses_.cbk_u0_document_list[0] = u0_document_expenses_;

                                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_approve));
                                data_u0_document_expenses_ = callServicePostCarBooking(_urlSetCbkCreateExpensesHr, data_u0_document_expenses_);

                                //Session["emp_idx"].ToString()
                                //use car in 60 minute
                                if ((_emp_idx == int.Parse(lbl_cemp_idx_view.Text)) && ((DateToday_Set <= DateTime_DBSet) && (result_time > 60)) && lbl_car_use_idx_view.Text == "1")
                                {
                                    //litDebug.Text = "1";
                                    Update_CancelCarDetail.Visible = true;
                                }
                                //use car out 2 day
                                else if ((_emp_idx == int.Parse(lbl_cemp_idx_view.Text)) && ((DateToday_Set <= DateTime_DBSet) && (result_time > 2880)) && lbl_car_use_idx_view.Text == "2")
                                {
                                    //litDebug.Text = "2";
                                    //litDebug.Text = result_time.ToString();
                                    Update_CancelCarDetail.Visible = true;
                                }
                                else if ((_emp_idx == int.Parse(lbl_cemp_idx_view.Text)) && lbl_car_use_idx_view.Text == "0")
                                {
                                    //litDebug.Text = "2";
                                    //litDebug.Text = result_time.ToString();
                                    Update_CancelCarDetail.Visible = true;
                                }
                                else //hr create expenses
                                {

                                    //Check Permission hr create expenses
                                    
                                    if (((result_time < 0) && lbl_car_use_idx_view.Text == "1"))//car use in || ((result_time < 2880) && lbl_car_use_idx_view.Text == "2")) //
                                    {
                                        //litDebug.Text = "1";
                                        if (data_u0_document_expenses_.return_code == 0)
                                        {
                                            Panel_AddCarUseHr.Visible = true;
                                            Update_SaveCarUseHr.Visible = true;
                                            getDetailCarInBookingAddHr(uidx);
                                            getDetailChoiceOutplan(rdouse_outplan, 0);


                                            linkBtnTrigger(btnSaveCarUseHr);
                                        }
                                    }
                                    else if (((result_time < 0) && lbl_car_use_idx_view.Text == "2")) //car use out
                                    {
                                        //litDebug.Text = "2";
                                        if (data_u0_document_expenses_.return_code == 0)
                                        {
                                            Panel_AddCarUseHr.Visible = true;
                                            Update_SaveCarUseHrOut.Visible = true;
                                            
                                        }
                                    }



                                }

                                break;
                            case 9:

                                switch (staidx)
                                {
                                    case 4:

                                        UpdatePanel_ShowDetailCarUse.Visible = true;

                                        break;

                                }
                                break;
                        }

                        setOntop.Focus();
                        break;

                    case 5: //node 5 Insert Car In Use
                        setOntop.Focus();

                        break;
                }

                break;
            case "docCreate":

                switch (_chk_tab)
                {
                    case 0:
                        setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                        setFormData(FvCreateCarBooking, FormViewMode.Insert, null);

                        DropDownList ddlDetailtypecar = (DropDownList)FvCreateCarBooking.FindControl("ddlDetailtypecar");
                        DropDownList ddlPlace = (DropDownList)FvCreateCarBooking.FindControl("ddlPlace");
                        DropDownList ddlType_booking_idx = (DropDownList)FvCreateCarBooking.FindControl("ddlType_booking_idx");

                        getDetailTypeCar(ddlDetailtypecar, 0);
                        getPlace(ddlPlace, 0);
                        getTypebooking(ddlType_booking_idx, 0);

                        break;
                }

                break;
            case "docApprove":

                switch (_chk_tab)
                {
                    case 0:

                        setFormData(FvSearchWaitApprove, FormViewMode.Insert, null);
                        DropDownList ddlPlaceSearchDetailWaitApprove = (DropDownList)FvSearchWaitApprove.FindControl("ddlPlaceSearchDetailWaitApprove");
                        DropDownList ddlDetailtypecarSearchDetailWaitApprove = (DropDownList)FvSearchWaitApprove.FindControl("ddlDetailtypecarSearchDetailWaitApprove");
                        //DropDownList ddlType_booking_idx = (DropDownList)FvCreateCarBooking.FindControl("ddlType_booking_idx");

                        getPlace(ddlPlaceSearchDetailWaitApprove, 0);
                        getDetailTypeCar(ddlDetailtypecarSearchDetailWaitApprove, 0);


                        //Check Permission
                        data_carbooking data_u0_document_approve = new data_carbooking();
                        cbk_u0_document_detail u0_document_approve = new cbk_u0_document_detail();
                        data_u0_document_approve.cbk_u0_document_list = new cbk_u0_document_detail[1];

                        u0_document_approve.cemp_idx = _emp_idx;
                        u0_document_approve.cemp_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                        u0_document_approve.cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                        u0_document_approve.cemp_rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
                        u0_document_approve.cemp_joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

                        data_u0_document_approve.cbk_u0_document_list[0] = u0_document_approve;
                        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b)); 
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_approve));
                        data_u0_document_approve = callServicePostCarBooking(_urlGetCbkWaitDetailApprove, data_u0_document_approve);


                        ViewState["Vs_WaitDetailApprove"] = data_u0_document_approve.cbk_u0_document_list;

                        ////Div_Search_WaitApprove.Visible = true;
                        GvWaitDetailApprove.Visible = true;
                        setGridData(GvWaitDetailApprove, ViewState["Vs_WaitDetailApprove"]);
                        //getCountWaitDetailApprove();
                        setOntop.Focus();
                        break;
                    case 2: // Show Frome Search Detail Wait Approve

                        //setFormData(FvSearchWaitApprove, FormViewMode.Insert, null);
                        //DropDownList ddlPlaceSearchDetailWaitApprove = (DropDownList)FvSearchWaitApprove.FindControl("ddlPlaceSearchDetailWaitApprove");
                        //DropDownList ddlDetailtypecarSearchDetailWaitApprove = (DropDownList)FvSearchWaitApprove.FindControl("ddlDetailtypecarSearchDetailWaitApprove");
                        ////DropDownList ddlType_booking_idx = (DropDownList)FvCreateCarBooking.FindControl("ddlType_booking_idx");

                        //getPlace(ddlPlaceSearchDetailWaitApprove, 0);
                        //getDetailTypeCar(ddlDetailtypecarSearchDetailWaitApprove, 0);

                        //Check Permission
                        data_carbooking data_u0_document_searchapprove = new data_carbooking();
                        cbk_u0_document_detail u0_document_searchapprove = new cbk_u0_document_detail();
                        data_u0_document_searchapprove.cbk_u0_document_list = new cbk_u0_document_detail[1];

                        u0_document_searchapprove.cemp_idx = _emp_idx;
                        u0_document_searchapprove.cemp_rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                        u0_document_searchapprove.cemp_rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                        u0_document_searchapprove.cemp_rpos_idx = int.Parse(ViewState["rpos_permission"].ToString());
                        u0_document_searchapprove.cemp_joblevel = int.Parse(ViewState["joblevel_permission"].ToString());

                        data_u0_document_searchapprove.cbk_u0_document_list[0] = u0_document_searchapprove;
                        //test_place.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(Place_b)); 
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_approve));
                        data_u0_document_searchapprove = callServicePostCarBooking(_urlGetCbkWaitDetailApprove, data_u0_document_searchapprove);


                        ViewState["Vs_WaitDetailApprove"] = data_u0_document_searchapprove.cbk_u0_document_list;

                        //Div_Search_WaitApprove.Visible = true;
                        GvWaitDetailApprove.Visible = true;
                        setGridData(GvWaitDetailApprove, ViewState["Vs_WaitDetailApprove"]);

                        setOntop.Focus();
                        break;

                    case 1:

                        Update_BackToWaitDetail.Visible = true;
                        GvWaitDetailApprove.Visible = false;
                        getViewDetailWaitCarBooking(uidx);

                        switch (nodeidx)
                        {
                            case 2: //head user approve
                                setFormData(FvHeadUserApprove, FormViewMode.Insert, null);
                                div_LogViewDetailWaitApprove.Visible = true;
                                getLogDetailWaitApproveCarBooking(uidx);


                                DropDownList ddlApproveStatus = (DropDownList)FvHeadUserApprove.FindControl("ddlApproveStatus");
                                getDecisionApprove(ddlApproveStatus, nodeidx, 0);


                                setOntop.Focus();
                                break;
                            case 3:// Hr Admin approve

                                setFormData(FvHrApprove, FormViewMode.Insert, null);
                                div_LogViewDetailWaitApprove.Visible = true;
                                getLogDetailWaitApproveCarBooking(uidx);

                                //clear data set
                                CleardataSetCarDetailList();

                                DropDownList ddlHrApproveStatus = (DropDownList)FvHrApprove.FindControl("ddlHrApproveStatus");
                                DropDownList ddl_emp_idx_driver = (DropDownList)FvHrApprove.FindControl("ddl_emp_idx_driver");
                                DropDownList ddlcar_use_idx = (DropDownList)FvHrApprove.FindControl("ddlcar_use_idx");
                                DropDownList ddlm0_car_idx = (DropDownList)FvHrApprove.FindControl("ddlm0_car_idx");

                                Label lbl_date_start_approve = (Label)FvDetailWaitApprove.FindControl("lbl_date_start_approve");
                                Label lbl_time_start_approve = (Label)FvDetailWaitApprove.FindControl("lbl_time_start_approve");
                                Label lbl_date_end_approve = (Label)FvDetailWaitApprove.FindControl("lbl_date_end_approve");
                                Label lbl_time_end_approve = (Label)FvDetailWaitApprove.FindControl("lbl_time_end_approve");

                                getDecisionApprove(ddlHrApproveStatus, nodeidx, 0);
                                getDetailAdmin(ddl_emp_idx_driver, 0);
                                getDetailCarUse(ddlcar_use_idx, 0);

                                //check car busy
                                getDetailCarBusy(ddlm0_car_idx, lbl_date_start_approve.Text, lbl_time_start_approve.Text, lbl_date_end_approve.Text, lbl_time_end_approve.Text, 0);


                                setOntop.Focus();
                                break;
                        }


                        break;

                }

                break;

            case "docReport":

                Update_PanelSearchReport.Visible = true;

                break;

        }
    }


    protected void setActiveTab(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _car_use_idx)
    {
        setActiveView(activeTab, uidx, nodeidx, staidx, actor_idx, _chk_tab, _car_use_idx);
        switch (activeTab)
        {
            case "docDetailCar":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");

                break;
            case "docDetail":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");

                break;
            case "docCreate":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "");

                break;
            case "docApprove":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
                li4.Attributes.Add("class", "");

                break;

            case "docReport":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                li4.Attributes.Add("class", "active");

                break;

        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }

    protected string getStatus(int status)
    {
        if (status == 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Certificate'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='No Certificate'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        //specified ASP.NET server control at run time.
    }

    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;
        switch (rptName.ID)
        {
            case "rp_place":

                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {

                    //var chk_coler = (Label)e.Item.FindControl("lbcheck_coler");
                    //var btnPlaceLab = (LinkButton)e.Item.FindControl("btnPlaceLab");

                    //for (int k = 0; k <= rp_place.Items.Count; k++)
                    //{
                    //    btnPlaceLab.CssClass = ConfigureColors(k);
                    //    //Console.WriteLine(i);
                    //}


                }

                break;
        }
    }

    #endregion reuse

    #region data excel
    protected void WriteExcelWithNPOI(DataTable dt, String extension, String fileName, int _set_value)
    {
        IWorkbook workbook;
        

        if (extension == "xlsx")
        {
            workbook = new XSSFWorkbook();



        }
        else if (extension == "xls")
        {
            workbook = new HSSFWorkbook();


        }
        else
        {
            throw new Exception("This format is not supported");
        }

        ISheet sheet1 = workbook.CreateSheet("Sheet 1");
        IRow row1 = sheet1.CreateRow(0);

        ICellStyle testeStyle = workbook.CreateCellStyle();
        testeStyle.BorderBottom = NPOI.SS.UserModel.BorderStyle.Medium;
        testeStyle.FillForegroundColor = IndexedColors.BrightGreen.Index;
        testeStyle.FillPattern = FillPattern.SolidForeground;

        //Create a Title row
        //var titleFont = workbook.CreateFont();
        //titleFont.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
        //titleFont.FontHeightInPoints = 11;
        //titleFont.Underline = NPOI.SS.UserModel.FontUnderlineType.Single;

        //var titleStyle = workbook.CreateCellStyle();
        //titleStyle.SetFont(titleFont);

        //HSSFCellStyle Style = workbook.CreateCellStyle() as HSSFCellStyle;

        for (int j = 0; j < dt.Columns.Count; j++)
        {
            ICell cell = row1.CreateCell(j);
            String columnName = dt.Columns[j].ToString();
            cell.SetCellValue(columnName);
        }

        //set merge cell
        int max_row = dt.Rows.Count - 1;

        for (int i = 0; i < dt.Rows.Count; i++)
        {
            

            IRow row = sheet1.CreateRow(i + 1);

            //set merge cell
            var cra = new NPOI.SS.Util.CellRangeAddress(1, max_row, 0, 0);
            sheet1.AddMergedRegion(cra);

            var cra1 = new NPOI.SS.Util.CellRangeAddress(1, max_row, 1, 1);
            sheet1.AddMergedRegion(cra1);

            for (int j = 0; j < dt.Columns.Count; j++)
            {

                
                ICell cell = row.CreateCell(j);
                String columnName = dt.Columns[j].ToString();
                cell.SetCellValue(dt.Rows[i][columnName].ToString());

                //Style.Style.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
                //Style.VerticalAlignment = VerticalAlignment.Center;
                //Style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                //set font
                //cell.CellStyle = titleStyle;

                //set export color total expenses green
                if (_set_value == 1) 
                {
                    //if ((i + 1) == dt.Rows.Count && (dt.Rows[i][columnName].ToString() == "Total :" || ((i + 1) == dt.Rows.Count && (j + 1) == dt.Columns.Count)))
                    if ((i + 1) == dt.Rows.Count && (dt.Rows[i][columnName].ToString() == "Total :") 
                        || ((i + 1) == dt.Rows.Count && (j + 1) == dt.Columns.Count) 
                        || ((i + 1) == dt.Rows.Count && ( j == 5))
                        )
                    {
                        cell.CellStyle = testeStyle;
                        //cell.CellStyle.WrapText = true;
                    }


                }
               
            }

        }


        
        

        using (var exportData = new MemoryStream())
        {
            Response.Clear();
            workbook.Write(exportData);

            if (extension == "xlsx")
            {
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xlsx"));

                Response.BinaryWrite(exportData.ToArray());
            }
            else if (extension == "xls")
            {
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", fileName + ".xls"));
                Response.BinaryWrite(exportData.GetBuffer());
            }
            Response.End();
        }


    }
    #endregion data excel

}