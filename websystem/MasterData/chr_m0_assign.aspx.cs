﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_chr_m0_assign : System.Web.UI.Page
{
    #region Connect

    service_execute serviceexcute = new service_execute();
    function_tool _funcTool = new function_tool();
    data_employee _dataEmployee = new data_employee();
    data_chr _dtchr = new data_chr();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string urlInsert_Master_Assign = _serviceUrl + ConfigurationManager.AppSettings["urlInsert_Master_Assign"];
    static string urlSelect_Master_Assign = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_Assign"];
    static string urlUpdate_Master_Assign = _serviceUrl + ConfigurationManager.AppSettings["urlUpdate_Master_Assign"];
    static string urlDelete_Master_Assign = _serviceUrl + ConfigurationManager.AppSettings["urlDelete_Master_Assign"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];
    static string urlSelect_System = _serviceUrl + ConfigurationManager.AppSettings["urlSelect_Master_SystemList"];



    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            MvMaster.SetActiveView(ViewIndex);
            SelectMasterList();

        }
        linkBtnTrigger(btnshow);
        ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
    }
    #region Select

    protected void getOrganizationList(DropDownList ddlName)
    {
        _dataEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dataEmployee.organization_list[0] = _orgList;

        _dataEmployee = callServicePostEmployee(_urlGetOrganizationList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร...", "0"));
    }

    protected void getDepartmentList(DropDownList ddlName, int _org_idx)
    {
        _dataEmployee.department_list = new department_details[1];
        department_details _deptList = new department_details();
        _deptList.org_idx = _org_idx;
        _dataEmployee.department_list[0] = _deptList;

        _dataEmployee = callServicePostEmployee(_urlGetDepartmentList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.department_list, "dept_name_th", "rdept_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกฝ่าย...", "0"));
    }

    protected void getSectionList(DropDownList ddlName, int _org_idx, int _rdept_idx)
    {
        _dataEmployee.section_list = new section_details[1];
        section_details _secList = new section_details();
        _secList.org_idx = _org_idx;
        _secList.rdept_idx = _rdept_idx;
        _dataEmployee.section_list[0] = _secList;

        _dataEmployee = callServicePostEmployee(_urlGetSectionList, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.section_list, "sec_name_th", "rsec_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกแผนก...", "0"));
    }

    protected void getEmpList(DropDownList ddlName, int _org_idx, int _rdept_idx, int _rsec_idx)
    {
        _dataEmployee.employee_list = new employee_detail[1];
        employee_detail _empList = new employee_detail();
        _empList.org_idx = _org_idx;
        _empList.rdept_idx = _rdept_idx;
        _empList.rsec_idx = _rsec_idx;
        _empList.emp_status = 1;
        _dataEmployee.employee_list[0] = _empList;


        _dataEmployee = callServicePostEmployee(urlGetAll, _dataEmployee);
        setDdlData(ddlName, _dataEmployee.employee_list, "emp_name_th", "emp_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกรายชื่อ...", "0"));
    }

    protected void getSystemList(DropDownList ddlName)
    {
        _dtchr = new data_chr();
        _dtchr.BoxMaster_SystemList = new MasterSystemList[1];
        MasterSystemList dataselect = new MasterSystemList();

        _dtchr.BoxMaster_SystemList[0] = dataselect;

        _dtchr = callServicePostCHR(urlSelect_System, _dtchr);
        // txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtchr));
        setDdlData(ddlName, _dtchr.BoxMaster_SystemList, "System_name", "Sysidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกระบบ...", "0"));

    }

    protected void SelectMasterList()
    {

        _dtchr.BoxMaster_AssignList = new MasterAssignList[1];
        MasterAssignList dataselect = new MasterAssignList();

        _dtchr.BoxMaster_AssignList[0] = dataselect;

        _dtchr = callServicePostCHR(urlSelect_Master_Assign, _dtchr);
        setGridData(GvMaster, _dtchr.BoxMaster_AssignList);

    }

    protected void Insert_Master()
    {
        FileUpload UploadFileRoom = (FileUpload)Panel_Add.FindControl("UploadFileRoom");

        _dtchr.BoxMaster_AssignList = new MasterAssignList[1];
        MasterAssignList datainsert = new MasterAssignList();

        datainsert.OrgIDX = int.Parse(ddlorgidx.SelectedValue);
        datainsert.RDeptIDX = int.Parse(ddlrdeptidx.SelectedValue);
        datainsert.Sysidx = int.Parse(ddlsystem.SelectedValue);
        datainsert.RSecIDX = int.Parse(ddlsecidx.SelectedValue);
        datainsert.EmpIDX = int.Parse(ddlassign.SelectedValue);
        datainsert.Assign_Status = int.Parse(ddStatusadd.SelectedValue);
        datainsert.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());

        _dtchr.BoxMaster_AssignList[0] = datainsert;

        _dtchr = callServicePostCHR(urlInsert_Master_Assign, _dtchr);

        ViewState["ReturnCode"] = _dtchr.ReturnCode;

        if (UploadFileRoom.HasFile)
        {
            string getPathfile = ConfigurationManager.AppSettings["path_file_chr_assign"];
            string fileName_upload = ViewState["ReturnCode"].ToString();
            string filePath_upload = Server.MapPath(getPathfile + fileName_upload);

            if (!Directory.Exists(filePath_upload))
            {
                Directory.CreateDirectory(filePath_upload);
            }
            string extension = Path.GetExtension(UploadFileRoom.FileName);

            UploadFileRoom.SaveAs(Server.MapPath(getPathfile + ViewState["ReturnCode"].ToString()) + "\\" + fileName_upload + extension);
            //litdebug.Text = room_name;
        }
    }

    protected void Delete_MasterSystem()
    {

        _dtchr.BoxMaster_AssignList = new MasterAssignList[1];
        MasterAssignList dataupdate = new MasterAssignList();

        dataupdate.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        dataupdate.AssignIDX = int.Parse(ViewState["AssignIDX"].ToString());
        _dtchr.BoxMaster_AssignList[0] = dataupdate;

        _dtchr = callServicePostCHR(urlDelete_Master_Assign, _dtchr);

    }

    protected void Update_Master_List()
    {
        _dtchr.BoxMaster_AssignList = new MasterAssignList[1];
        MasterAssignList dataupdate = new MasterAssignList();

        dataupdate.CEmpIDX = int.Parse(ViewState["EmpIDX"].ToString());
        dataupdate.AssignIDX = int.Parse(ViewState["AssignIDX"].ToString());
        dataupdate.OrgIDX = int.Parse(ViewState["OrgIDX"].ToString());
        dataupdate.RDeptIDX = int.Parse(ViewState["RDeptIDX"].ToString());
        dataupdate.RSecIDX = int.Parse(ViewState["ddlrsecidx_edit"].ToString());
        dataupdate.EmpIDX = int.Parse(ViewState["ddlempidx_edit"].ToString());
        dataupdate.Sysidx = int.Parse(ViewState["Sysidx"].ToString());
        dataupdate.Assign_Status = int.Parse(ViewState["ddStatusUpdate"].ToString());


        _dtchr.BoxMaster_AssignList[0] = dataupdate;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtchr));
        _dtchr = callServicePostCHR(urlUpdate_Master_Assign, _dtchr);

    }
    #endregion

    #region Directories_File URL


    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }



    public void SearchDirectories(DirectoryInfo dir, String target)
    {

        string dirfiles = dir.ToString();

        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        DataRow dr = dt.NewRow();
        DataColumn dc = new DataColumn("FileName", typeof(string));
        dc = new DataColumn("Download", typeof(string));
        dt.Columns.Add("FileName");
        dt.Columns.Add("Download");

        FileInfo[] files = dir.GetFiles();
        int i = 0;
        foreach (FileInfo file in files)
        {
            if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
            {
                string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
                dt.Rows.Add(file.Name);
                dt.Rows[i][1] = f[0];
                i++;
            }
        }

        GridView GvMaster = (GridView)ViewIndex.FindControl("GvMaster");
        // Panel gvFileLo1 = (Panel)FormViewDetails_CMP.FindControl("gvFileLo1");

        if (dt.Rows.Count > 0)
        {

            ds.Tables.Add(dt);
            //gvFileLo1.Visible = true;
            GvMaster.DataSource = ds.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            GvMaster.DataBind();
            ds.Dispose();
        }
        else
        {

            GvMaster.DataSource = null;
            GvMaster.DataBind();

        }

    }

    #endregion

    #region Trigger 
    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }
    #endregion Trigger

    #region reuse
    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //  convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_chr callServicePostCHR(string _cmdUrl, data_chr _dtchr)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtchr);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtchr = (data_chr)_funcTool.convertJsonToObject(typeof(data_chr), _localJson);

        return _dtchr;
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void SetDefaultAdd()
    {
        getOrganizationList(ddlorgidx);
        getDepartmentList(ddlrdeptidx, int.Parse(ddlorgidx.SelectedValue));
        getSectionList(ddlsecidx, int.Parse(ddlorgidx.SelectedValue), int.Parse(ddlrdeptidx.SelectedValue));
        getEmpList(ddlassign, int.Parse(ddlorgidx.SelectedValue), int.Parse(ddlrdeptidx.SelectedValue), int.Parse(ddlsecidx.SelectedValue));
        getSystemList(ddlsystem);
        ddlrdeptidx.SelectedValue = "0";
    }

    #endregion reuse

    #region GridView
    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvMaster.EditIndex == e.Row.RowIndex)
                    {
                        var ddlsysidx_edit = (DropDownList)e.Row.FindControl("ddlsysidx_edit");
                        var lbl_SysIDX_edit = (Label)e.Row.FindControl("lbl_SysIDX_edit");

                        var ddlorgidx_edit = (DropDownList)e.Row.FindControl("ddlorgidx_edit");
                        var lbl_OrgIDX_edit = (Label)e.Row.FindControl("lbl_OrgIDX_edit");

                        var ddlrdeptidx_edit = (DropDownList)e.Row.FindControl("ddlrdeptidx_edit");
                        var lbl_RDeptIDX_edit = (Label)e.Row.FindControl("lbl_RDeptIDX_edit");

                        var ddlrsecidx_edit = (DropDownList)e.Row.FindControl("ddlrsecidx_edit");
                        var lbl_RSecIDX_edit = (Label)e.Row.FindControl("lbl_RSecIDX_edit");

                        var ddlempidx_edit = (DropDownList)e.Row.FindControl("ddlempidx_edit");
                        var lbl_EmpIDX_edit = (Label)e.Row.FindControl("lbl_EmpIDX_edit");




                        getSystemList(ddlsysidx_edit);
                        ddlsysidx_edit.SelectedValue = lbl_SysIDX_edit.Text;

                        getOrganizationList(ddlorgidx_edit);
                        ddlorgidx_edit.SelectedValue = lbl_OrgIDX_edit.Text;

                        getDepartmentList(ddlrdeptidx_edit, int.Parse(ddlorgidx_edit.SelectedValue));
                        ddlrdeptidx_edit.SelectedValue = lbl_RDeptIDX_edit.Text;

                        getSectionList(ddlrsecidx_edit, int.Parse(ddlorgidx_edit.SelectedValue), int.Parse(ddlrdeptidx_edit.SelectedValue));
                        ddlrsecidx_edit.SelectedValue = lbl_RSecIDX_edit.Text;

                        getEmpList(ddlempidx_edit, int.Parse(ddlorgidx_edit.SelectedValue), int.Parse(ddlrdeptidx_edit.SelectedValue), int.Parse(ddlrsecidx_edit.SelectedValue));
                        ddlempidx_edit.SelectedValue = lbl_EmpIDX_edit.Text;


                    }
                    else if (GvMaster.EditIndex != e.Row.RowIndex)
                    {
                        var lblstidx = (Label)e.Row.FindControl("lblstidx");
                        HyperLink btnDL11 = (HyperLink)e.Row.Cells[7].FindControl("btnDL11");  //Cells[3] เลือกเซลล์ว่าช่องที่ต้องการแสดงอยู่คอลัมน์ไหน
                        HiddenField hidFile11 = (HiddenField)e.Row.Cells[7].FindControl("hidFile11");


                        string filePath_View_edit = ConfigurationManager.AppSettings["path_file_chr_assign"];
                        string directoryName_View_edit = lblstidx.Text;//lbl_place_name_edit.Text.Trim() + lbl_room_name_en_edit.Text + lblm0_room_idx_edit.Text;

                        if (Directory.Exists(Server.MapPath(filePath_View_edit + directoryName_View_edit)))
                        {
                            string[] filesPath = Directory.GetFiles(Server.MapPath(filePath_View_edit + directoryName_View_edit));
                            List<ListItem> files = new List<ListItem>();
                            foreach (string path in filesPath)
                            {
                                string getfiles = "";
                                getfiles = Path.GetFileName(path);
                                btnDL11.NavigateUrl = filePath_View_edit + directoryName_View_edit + "/" + getfiles;
                            }
                            btnDL11.Visible = true;
                        }
                        else
                        {
                            btnDL11.Visible = false;
                        }
                    }
                }




                break;


        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.PageIndex = e.NewPageIndex;
                GvMaster.DataBind();

                SelectMasterList();

                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                GvMaster.EditIndex = e.NewEditIndex;
                SelectMasterList();
                btnshow.Visible = false;
                break;

        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {

            case "GvMaster":
                GvMaster.EditIndex = -1;
                SelectMasterList();
                btnshow.Visible = true;
                break;

        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvMaster":

                int AssignIDX = Convert.ToInt32(GvMaster.DataKeys[e.RowIndex].Values[0].ToString());
                var ddlsysidx_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlsysidx_edit");
                var ddlorgidx_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlorgidx_edit");
                var ddlrdeptidx_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlrdeptidx_edit");
                var ddlrsecidx_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlrsecidx_edit");
                var ddlempidx_edit = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddlempidx_edit");
                var ddStatusUpdate = (DropDownList)GvMaster.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvMaster.EditIndex = -1;

                ViewState["AssignIDX"] = AssignIDX;
                ViewState["Sysidx"] = ddlsysidx_edit.SelectedValue;
                ViewState["OrgIDX"] = ddlorgidx_edit.SelectedValue;
                ViewState["RDeptIDX"] = ddlrdeptidx_edit.SelectedValue;
                ViewState["ddlrsecidx_edit"] = ddlrsecidx_edit.SelectedValue;
                ViewState["ddlempidx_edit"] = ddlempidx_edit.SelectedValue;
                ViewState["ddStatusUpdate"] = ddStatusUpdate.SelectedValue;

                Update_Master_List();
                SelectMasterList();

                break;
        }
    }

    #endregion

    #endregion

    #region ddlSelectedIndexChanged

    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        var ddName = (DropDownList)sender;
     
        switch (ddName.ID)
        {
            case "ddlorgidx":
                getDepartmentList(ddlrdeptidx, int.Parse(ddlorgidx.SelectedValue));
                break;
            case "ddlrdeptidx":
                getSectionList(ddlsecidx, int.Parse(ddlorgidx.SelectedValue), int.Parse(ddlrdeptidx.SelectedValue));
                break;
            case "ddlsecidx":
                getEmpList(ddlassign, int.Parse(ddlorgidx.SelectedValue), int.Parse(ddlrdeptidx.SelectedValue), int.Parse(ddlsecidx.SelectedValue));
                break;

            case "ddlorgidx_edit":
                var ddName1 = (DropDownList)sender;
                var row1 = (GridViewRow)ddName1.NamingContainer;

                var ddlorgidx_edit = (DropDownList)row1.FindControl("ddlorgidx_edit");
                var ddlrdeptidx_edit = (DropDownList)row1.FindControl("ddlrdeptidx_edit");

                getDepartmentList(ddlrdeptidx_edit, int.Parse(ddlorgidx_edit.SelectedValue));
                break;

            case "ddlrdeptidx_edit":

                var ddName2 = (DropDownList)sender;
                var row2 = (GridViewRow)ddName2.NamingContainer;

                var ddlorgidx_edit1 = (DropDownList)row2.FindControl("ddlorgidx_edit");
                var ddlrdeptidx_edit1 = (DropDownList)row2.FindControl("ddlrdeptidx_edit");
                var ddlrsecidx_edit1 = (DropDownList)row2.FindControl("ddlrsecidx_edit");

                getSectionList(ddlrsecidx_edit1, int.Parse(ddlorgidx_edit1.SelectedValue), int.Parse(ddlrdeptidx_edit1.SelectedValue));
                break;


            case "ddlrsecidx_edit":

                var ddName3 = (DropDownList)sender;
                var row3 = (GridViewRow)ddName3.NamingContainer;

                var ddlorgidx_edit3 = (DropDownList)row3.FindControl("ddlorgidx_edit");
                var ddlrdeptidx_edit3 = (DropDownList)row3.FindControl("ddlrdeptidx_edit");
                var ddlrsecidx_edit3 = (DropDownList)row3.FindControl("ddlrsecidx_edit");
                var ddlempidx_edit = (DropDownList)row3.FindControl("ddlempidx_edit");

                getEmpList(ddlempidx_edit, int.Parse(ddlorgidx_edit3.SelectedValue), int.Parse(ddlrdeptidx_edit3.SelectedValue), int.Parse(ddlrsecidx_edit3.SelectedValue));
                break;

        }

    }

    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {

            case "CmdAdd":
                btnshow.Visible = false;
                Panel_Add.Visible = true;
                SetDefaultAdd();
                break;

            case "btnCancel":
                btnshow.Visible = true;
                Panel_Add.Visible = false;
                break;

            case "btnAdd":
                Insert_Master();
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                //Panel_Add.Visible = false;
                //btnshow.Visible = true;
                //SelectMasterList();
                break;

            case "CmdDel":
                int AssignIDX = int.Parse(cmdArg);
                ViewState["AssignIDX"] = AssignIDX;
                Delete_MasterSystem();
                SelectMasterList();

                break;
        }



    }
    #endregion
}