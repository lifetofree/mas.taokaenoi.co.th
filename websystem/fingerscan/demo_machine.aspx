<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage_demo.master" AutoEventWireup="true" CodeFile="demo_machine.aspx.cs" Inherits="websystem_fingerscan_demo_machine" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" Runat="Server">
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <div class="row">
        <!--multiview-->
        <asp:MultiView ID="mvSystem" runat="server">
            <asp:View ID="viewCreate" runat="server">
                <div class="col-md-12">
                    <div class="panel panel-info" ID="div_heading" runat="server">
                        <div class="panel-heading">
                            <h3 class="panel-title"><asp:Literal ID="litHeadingTitle" runat="server" Text="เพิ่มข้อมูล"></asp:Literal></h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">ประเภท master data
                                            <span class="text-danger">*</span> :</label>
                                        <div class="col-md-8 control-label textleft">
                                            <asp:RadioButtonList ID="rblMasterType" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblSelectedIndexChanged" AutoPostBack="True">
                                                <asp:ListItem Text="&nbsp;เครื่อง&nbsp;" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="&nbsp;สถานที่ตั้ง&nbsp;" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="&nbsp;ตำแหน่งที่ตั้ง&nbsp;" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="&nbsp;Zone&nbsp;" Value="4"></asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                </div>
                                <asp:FormView ID="fvDetail" runat="server" Width="100%" DefaultMode="ReadOnly">
                                    <InsertItemTemplate>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">IP / Host name
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbMachineIp" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Zone
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlZone" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">สถานที่ตั้ง
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlMachinePlace" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">ตำแหน่งที่ตั้ง
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlMachineLoc" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">ผู้ดูแลเครื่อง
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlMachineOrg" runat="server" CssClass="form-control">
                                                    <asp:ListItem>--- องค์กร ---</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"><span class="text-danger">*</span></label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlMachineDept" runat="server" CssClass="form-control">
                                                    <asp:ListItem>--- ฝ่าย ---</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"><span class="text-danger">*</span></label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlMachineSec" runat="server" CssClass="form-control">
                                                    <asp:ListItem>--- แผนก ---</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">บันทึกเวลาเข้างาน
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-8 control-label textleft">
                                                <asp:RadioButtonList ID="rblAttendanceStatus" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="&nbsp;บันทึก&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;ไม่บันทึก&nbsp;" Value="0"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">สถานะ
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-8 control-label textleft">
                                                <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-8">
                                                <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0">
                                                    <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                    <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                                <asp:LinkButton ID="lbReset" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument="0">
                                                    <i class="fas fa-sync-alt" aria-hidden="true"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                            </div>
                                        </div>
                                    </InsertItemTemplate>
                                    <EditItemTemplate>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">IP / Host name
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbMachineIp" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">Zone
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlZone" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">สถานที่ตั้ง
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlMachinePlace" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">ตำแหน่งที่ตั้ง
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlMachineLoc" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">ผู้ดูแลเครื่อง
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlMachineOrg" runat="server" CssClass="form-control">
                                                    <asp:ListItem>--- องค์กร ---</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"><span class="text-danger">*</span></label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlMachineDept" runat="server" CssClass="form-control">
                                                    <asp:ListItem>--- ฝ่าย ---</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"><span class="text-danger">*</span></label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlMachineSec" runat="server" CssClass="form-control">
                                                    <asp:ListItem>--- แผนก ---</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">บันทึกเวลาเข้างาน
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-8 control-label textleft">
                                                <asp:RadioButtonList ID="rblAttendanceStatus" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="&nbsp;บันทึก&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;ไม่บันทึก&nbsp;" Value="0"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">สถานะ
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-8 control-label textleft">
                                                <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-8">
                                                <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0">
                                                    <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                    <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                            </div>
                                        </div>
                                    </EditItemTemplate>
                                </asp:FormView>

                                <asp:FormView ID="fvDetail2" runat="server" Width="100%" DefaultMode="ReadOnly">
                                    <InsertItemTemplate>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">สถานที่ตั้ง
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbPlace" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">สถานะ
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-8 control-label textleft">
                                                <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-8">
                                                <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0">
                                                    <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                    <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                                <asp:LinkButton ID="lbReset" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument="0">
                                                    <i class="fas fa-sync-alt" aria-hidden="true"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                            </div>
                                        </div>
                                    </InsertItemTemplate>
                                    <EditItemTemplate>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">สถานที่ตั้ง
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbPlace" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">สถานะ
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-8 control-label textleft">
                                                <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-8">
                                                <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0">
                                                    <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                    <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                            </div>
                                        </div>
                                    </EditItemTemplate>
                                </asp:FormView>

                                <asp:FormView ID="fvDetail3" runat="server" Width="100%" DefaultMode="ReadOnly">
                                    <InsertItemTemplate>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">ตำแหน่งที่ตั้ง <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbLoc" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">สถานะ
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-8 control-label textleft">
                                                <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-8">
                                                <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0">
                                                    <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                    <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                                <asp:LinkButton ID="lbReset" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument="0">
                                                    <i class="fas fa-sync-alt" aria-hidden="true"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                            </div>
                                        </div>
                                    </InsertItemTemplate>
                                    <EditItemTemplate>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">ตำแหน่งที่ตั้ง
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbLoc" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">สถานะ
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-8 control-label textleft">
                                                <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-8">
                                                <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0">
                                                    <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                    <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                            </div>
                                        </div>
                                    </EditItemTemplate>
                                </asp:FormView>

                                <asp:FormView ID="fvDetail4" runat="server" Width="100%" DefaultMode="ReadOnly">
                                    <InsertItemTemplate>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">ชื่อ Zone <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbZoneName" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <!-- <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-4">
                                                <hr>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">สถานที่ตั้ง <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlMachinePlace" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">ตำแหน่งที่ตั้ง <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlMachineLocation" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-4">
                                                <hr>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div> -->
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">สถานะ
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-8 control-label textleft">
                                                <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-8">
                                                <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0">
                                                    <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                    <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                                <asp:LinkButton ID="lbReset" runat="server" CssClass="btn btn-info" OnCommand="btnCommand" CommandName="cmdReset" CommandArgument="0">
                                                    <i class="fas fa-sync-alt" aria-hidden="true"></i>&nbsp;เคลียร์ค่า</asp:LinkButton>
                                            </div>
                                        </div>
                                    </InsertItemTemplate>
                                    <EditItemTemplate>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">ชื่อ Zone
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="tbZoneName" runat="server" CssClass="form-control"></asp:TextBox>
                                            </div>
                                            <label class="col-md-4 control-label"></label>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label">สถานะ
                                                <span class="text-danger">*</span> :</label>
                                            <div class="col-md-8 control-label textleft">
                                                <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Text="&nbsp;แสดง&nbsp;" Value="1" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="&nbsp;ไม่แสดง&nbsp;" Value="0"></asp:ListItem>
                                                </asp:RadioButtonList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 control-label"></label>
                                            <div class="col-md-8">
                                                <asp:LinkButton ID="lbSave" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0">
                                                    <i class="fas fa-save" aria-hidden="true"></i>&nbsp;บันทึก</asp:LinkButton>
                                                <asp:LinkButton ID="lbCancel" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0">
                                                    <i class="fas fa-times" aria-hidden="true"></i>&nbsp;ยกเลิก</asp:LinkButton>
                                            </div>
                                        </div>
                                    </EditItemTemplate>
                                </asp:FormView>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:View>
            <asp:View ID="viewList" runat="server">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <div class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-md-4 control-label">ประเภท master data
                                    <span class="text-danger">*</span> :</label>
                                <div class="col-md-8 control-label textleft">
                                    <asp:RadioButtonList ID="rblMasterType2" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rblSelectedIndexChanged" AutoPostBack="True">
                                        <asp:ListItem Text="&nbsp;เครื่อง&nbsp;" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="&nbsp;สถานที่ตั้ง&nbsp;" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="&nbsp;ตำแหน่งที่ตั้ง&nbsp;" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="&nbsp;Zone&nbsp;" Value="4"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <asp:LinkButton ID="lbCreate" runat="server" CssClass="btn btn-success" OnCommand="btnCommand" CommandName="cmdCreate" CommandArgument="0"><i class="fas fa-plus" aria-hidden="true"></i>&nbsp;เพิ่มข้อมูล</asp:LinkButton>
                    </div>
                    <div class="col-md-4">
                        <div class="input-group">
                            <asp:TextBox ID="tbItemSearch" runat="server" CssClass="form-control" placeholder="ค้นหา"></asp:TextBox>
                            <asp:LinkButton ID="lbItemSearch" runat="server" CssClass="input-group-addon"><i class="fas fa-search"></i></asp:LinkButton>
                            <asp:LinkButton ID="lbItemReset" runat="server" CssClass="input-group-addon"><i class="fas fa-sync-alt"></i></asp:LinkButton>
                        </div>
                    </div>

                    <table id="table1" runat="server" class="table table-striped table-bordered table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse:collapse;">
                        <tbody>
                            <tr class="info" style="font-size:Small;height:40px;">
                                <th scope="col">#</th>
                                <th scope="col">IP / Host name</th>
                                <th scope="col">Zone</th>
                                <th scope="col">สถานที่ตั้ง</th>
                                <th scope="col">ผู้ดูแลเครื่อง</th>
                                <th scope="col">บันทึกเวลาเข้างาน</th>
                                <th scope="col">สถานะ</th>
                                <th scope="col">การจัดการ</th>
                            </tr>
                            <tr style="font-size:Small;">
                                <td>1</td>
                                <td>192.168.3.1</td>
                                <td>NPW-A</td>
                                <td>NPW(นพวงศ์) - หน้าประตูทางเข้า</td>
                                <td>ฝ่ายบริการงานทรัพยากรบุคคล - สำนักงานเมืองทองธานี</td>
                                <td>
                                    <i class="fas fa-check text-success"></i>
                                </td>
                                <td>ใช้งาน</td>
                                <td>
                                    <asp:LinkButton ID="lbEdit" runat="server" CssClass="btn btn-warning" OnCommand="btnCommand" CommandName="cmdEdit" CommandArgument="1"><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</asp:LinkButton>
                                    <a onclick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" id="ContentMain_gvList_lbDelete_0" class="btn btn-danger" href="#"><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</a>
                                </td>
                            </tr>
                            <tr style="font-size:Small;">
                                <td>2</td>
                                <td>192.168.3.2</td>
                                <td>NPW-A</td>
                                <td>NPW(นพวงศ์) - หน้าประตูทางเข้า</td>
                                <td>ฝ่ายบริการงานทรัพยากรบุคคล - สำนักงานเมืองทองธานี</td>
                                <td>
                                    <i class="fas fa-check text-success"></i>
                                </td>
                                <td>ใช้งาน</td>
                                <td>
                                    <a id="ContentMain_gvList_lbEdit_0" class="btn btn-warning" href="#"><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</a>
                                    <a onclick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" id="ContentMain_gvList_lbDelete_0" class="btn btn-danger" href="#"><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</a>
                                </td>
                            </tr>
                            <tr style="font-size:Small;">
                                <td>3</td>
                                <td>192.168.3.3</td>
                                <td>NPW-A</td>
                                <td>NPW(นพวงศ์) - หน้าประตูทางเข้า</td>
                                <td>ฝ่ายบริการงานทรัพยากรบุคคล - สำนักงานเมืองทองธานี</td>
                                <td>
                                    <i class="fas fa-check text-success"></i>
                                </td>
                                <td>ไม่ใช้งาน</td>
                                <td>
                                    <a id="ContentMain_gvList_lbEdit_0" class="btn btn-warning" href="#"><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</a>
                                    <a onclick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" id="ContentMain_gvList_lbDelete_0" class="btn btn-danger" href="#"><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</a>
                                </td>
                            </tr>
                            <tr style="font-size:Small;">
                                <td>4</td>
                                <td>192.168.3.4</td>
                                <td>NPW-B</td>
                                <td>NPW(นพวงศ์) - ทางเข้าอาคารผลิต</td>
                                <td>Safety</td>
                                <td>
                                    <i class="fas fa-times text-danger"></i>
                                </td>
                                <td>ใช้งาน</td>
                                <td>
                                    <a id="ContentMain_gvList_lbEdit_0" class="btn btn-warning" href="#"><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</a>
                                    <a onclick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" id="ContentMain_gvList_lbDelete_0" class="btn btn-danger" href="#"><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</a>
                                </td>
                            </tr>
                            <tr style="font-size:Small;">
                                <td>5</td>
                                <td>192.168.3.5</td>
                                <td>NPW-B</td>
                                <td>NPW(นพวงศ์) - ทางเข้าอาคารผลิต</td>
                                <td>Safety</td>
                                <td>
                                    <i class="fas fa-times text-danger"></i>
                                </td>
                                <td>ใช้งาน</td>
                                <td>
                                    <a id="ContentMain_gvList_lbEdit_0" class="btn btn-warning" href="#"><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</a>
                                    <a onclick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" id="ContentMain_gvList_lbDelete_0" class="btn btn-danger" href="#"><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</a>
                                </td>
                            </tr>
                            <tr style="font-size:Small;">
                                <td>6</td>
                                <td>tknl017.dyndns.org</td>
                                <td>-</td>
                                <td>สาขา - สาขาตลาดน้ำ 4 ภาค เฟส 1</td>
                                <td>สาขาตลาดน้ำ 4 ภาค เฟส 1</td>
                                <td>
                                    <i class="fas fa-check text-success"></i>
                                </td>
                                <td>ใช้งาน</td>
                                <td>
                                    <a id="ContentMain_gvList_lbEdit_0" class="btn btn-warning" href="#"><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</a>
                                    <a onclick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" id="ContentMain_gvList_lbDelete_0" class="btn btn-danger" href="#"><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</a>
                                </td>
                            </tr>
                            <tr style="font-size:Small;">
                                <td>7</td>
                                <td>tknl018.dyndns.org</td>
                                <td>-</td>
                                <td>สาขา - สาขา Terminal21</td>
                                <td>สาขา Terminal21</td>
                                <td>
                                    <i class="fas fa-check text-success"></i>
                                </td>
                                <td>ไม่ใช้งาน</td>
                                <td>
                                    <a id="ContentMain_gvList_lbEdit_0" class="btn btn-warning" href="#"><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</a>
                                    <a onclick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" id="ContentMain_gvList_lbDelete_0" class="btn btn-danger" href="#"><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <table id="table2" runat="server" class="table table-striped table-bordered table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse:collapse;">
                        <tbody>
                            <tr class="info" style="font-size:Small;height:40px;">
                                <th scope="col">#</th>
                                <th scope="col">สถานที่ตั้ง</th>
                                <th scope="col">สถานะ</th>
                                <th scope="col">การจัดการ</th>
                            </tr>
                            <tr style="font-size:Small;">
                                <td>1</td>
                                <td>NPW(นพวงศ์)</td>
                                <td>ใช้งาน</td>
                                <td>
                                    <asp:LinkButton ID="lbEdit2" runat="server" CssClass="btn btn-warning" OnCommand="btnCommand" CommandName="cmdEdit" CommandArgument="2"><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</asp:LinkButton>
                                    <a onclick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" id="ContentMain_gvList_lbDelete_0" class="btn btn-danger" href="#"><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</a>
                                </td>
                            </tr>
                            <tr style="font-size:Small;">
                                <td>2</td>
                                <td>RJN(โรจนะ)</td>
                                <td>ใช้งาน</td>
                                <td>
                                    <a id="ContentMain_gvList_lbEdit_0" class="btn btn-warning" href="#"><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</a>
                                    <a onclick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" id="ContentMain_gvList_lbDelete_0" class="btn btn-danger" href="#"><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</a>
                                </td>
                            </tr>
                            <tr style="font-size:Small;">
                                <td>3</td>
                                <td>สาขา</td>
                                <td>ใช้งาน</td>
                                <td>
                                    <a id="ContentMain_gvList_lbEdit_0" class="btn btn-warning" href="#"><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</a>
                                    <a onclick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" id="ContentMain_gvList_lbDelete_0" class="btn btn-danger" href="#"><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <table id="table3" runat="server" class="table table-striped table-bordered table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse:collapse;">
                        <tbody>
                            <tr class="info" style="font-size:Small;height:40px;">
                                <th scope="col">#</th>
                                <th scope="col">ตำแหน่งที่ตั้ง</th>
                                <th scope="col">สถานะ</th>
                                <th scope="col">การจัดการ</th>
                            </tr>
                            <tr style="font-size:Small;">
                                <td>1</td>
                                <td>หน้าประตูทางเข้า</td>
                                <td>ใช้งาน</td>
                                <td>
                                    <asp:LinkButton ID="lbEdit3" runat="server" CssClass="btn btn-warning" OnCommand="btnCommand" CommandName="cmdEdit" CommandArgument="3"><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</asp:LinkButton>
                                    <a onclick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" id="ContentMain_gvList_lbDelete_0" class="btn btn-danger" href="#"><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</a>
                                </td>
                            </tr>
                            <tr style="font-size:Small;">
                                <td>2</td>
                                <td>ทางเข้าอาคารผลิต</td>
                                <td>ใช้งาน</td>
                                <td>
                                    <a id="ContentMain_gvList_lbEdit_0" class="btn btn-warning" href="#"><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</a>
                                    <a onclick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" id="ContentMain_gvList_lbDelete_0" class="btn btn-danger" href="#"><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</a>
                                </td>
                            </tr>
                            <tr style="font-size:Small;">
                                <td>3</td>
                                <td>สาขาตลาดน้ำ 4 ภาค เฟส 1</td>
                                <td>ใช้งาน</td>
                                <td>
                                    <a id="ContentMain_gvList_lbEdit_0" class="btn btn-warning" href="#"><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</a>
                                    <a onclick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" id="ContentMain_gvList_lbDelete_0" class="btn btn-danger" href="#"><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <table id="table4" runat="server" class="table table-striped table-bordered table-responsive" cellspacing="0" rules="all" border="1" style="border-collapse:collapse;">
                        <tbody>
                            <tr class="info" style="font-size:Small;height:40px;">
                                <th scope="col">#</th>
                                <th scope="col">Zone</th>
                                <th scope="col">สถานะ</th>
                                <th scope="col">การจัดการ</th>
                            </tr><!-- <th scope="col">สถานที่ตั้ง</th> -->
                            <tr style="font-size:Small;">
                                <td>1</td>
                                <td>NPW-A</td>
                                <td>ใช้งาน</td>
                                <td>
                                    <asp:LinkButton ID="lbEdit4" runat="server" CssClass="btn btn-warning" OnCommand="btnCommand" CommandName="cmdEdit" CommandArgument="3"><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</asp:LinkButton>
                                    <a onclick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" id="ContentMain_gvList_lbDelete_0" class="btn btn-danger" href="#"><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</a>
                                </td>
                            </tr><!-- <td>NPW(นพวงศ์) - หน้าประตูทางเข้า<br>NPW(นพวงศ์) - ทางเข้าอาคารผลิต</td> -->
                            <tr style="font-size:Small;">
                                <td>2</td>
                                <td>NPW-B</td>
                                <td>ใช้งาน</td>
                                <td>
                                    <a id="ContentMain_gvList_lbEdit_0" class="btn btn-warning" href="#"><i class="far fa-edit" aria-hidden="true"></i>&nbsp;แก้ไข</a>
                                    <a onclick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?');" id="ContentMain_gvList_lbDelete_0" class="btn btn-danger" href="#"><i class="far fa-trash-alt" aria-hidden="true"></i>&nbsp;ลบ</a>
                                </td>
                            </tr><!-- <td>NPW(นพวงศ์) - ประตูด้านหลัง</td> -->
                        </tbody>
                    </table>
                </div>
            </asp:View>
        </asp:MultiView>
    </div>
</asp:Content>