﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_laws_law_trademark_recordsheet : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();
    service_execute serviceexcute = new service_execute();
    data_employee _dataEmployee = new data_employee();
    data_law _data_law = new data_law();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];
    //-- employee --//

    //-- trademarks --//
    static string _urlGetLawContryDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawContryDetail"];
    static string _urlGetLawM0DocType = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0DocType"];
    static string _urlGetLawM0SubDocType = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0SubDocType"];
    static string _urlGetLawM0JobType = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0JobType"];

    static string _urlSetLawCreateDocument = _serviceUrl + ConfigurationManager.AppSettings["urlSetLawCreateDocument"];
    static string _urlGetLawDetailDocument = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawDetailDocument"];
    static string _urlGetLawLogDetailDocument = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawLogDetailDocument"];
    static string _urlGetLawWaitApproveDetailDoc = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawWaitApproveDetailDoc"];
    static string _urlGetLawStatusApprove = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawStatusApprove"];
    static string _urlSetLawApproveDocument = _serviceUrl + ConfigurationManager.AppSettings["urlSetLawApproveDocument"];
    static string _urlGetSelectLawInDocument = _serviceUrl + ConfigurationManager.AppSettings["urlGetSelectLawInDocument"];
    static string _urlGetViewDetailLawOfficer = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewDetailLawOfficer"];
    static string _urlGetLawRecordSheet = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawRecordSheet"];
    static string _urlGetCountLawWaitApprove = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountLawWaitApprove"];
    static string _urlSetLawUpdateDocument = _serviceUrl + ConfigurationManager.AppSettings["urlSetLawUpdateDocument"];
    static string _urlGetLawM0Owner = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0Owner"];
    static string _urlGetLawM0Incorporated = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0Incorporated"];
    static string _urlGetLawM0Trademark = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0Trademark"];
    static string _urlGetLawM0RecordStatus = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0RecordStatus"];
    static string _urlGetLawM0LocalLanguage = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0LocalLanguage"];
    static string _urlGetLawM0Actions = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0Actions"];
    static string _urlGetLawM0TypeOfRegistration = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0TypeOfRegistration"];
    static string _urlGetLawM0TypeOfWork = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0TypeOfWork"];

    static string _urlGetLawViewRecordSheet = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawViewRecordSheet"];
    static string _urlGetLawM0ActionkDetailList = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0ActionkDetailList"];

    static string _urlSetLawRecordSheet = _serviceUrl + ConfigurationManager.AppSettings["urlSetLawRecordSheet"];
    static string _urlSetLawRecordSheetPublication = _serviceUrl + ConfigurationManager.AppSettings["urlSetLawRecordSheetPublication"];
    static string _urlSetLawRecordSheetDeadline = _serviceUrl + ConfigurationManager.AppSettings["urlSetLawRecordSheetDeadline"];
    static string _urlGetLawViewDeadlineRecordSheet = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawViewDeadlineRecordSheet"];
    static string _urlGetDDLTrademarksProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetDDLTrademarksProfile"];

    static string _urlSetUpdateLawRenewal = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateLawRenewal"];
    static string _urlSetUpdateLawAssignment = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateLawAssignment"];
    static string _urlSetUpdateLawChangeofname = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateLawChangeofname"];
    static string _urlSetUpdateLawPriorities = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateLawPriorities"];
    static string _urlSetUpdateLawClient = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateLawClient"];
    static string _urlSetUpdateLawAgent = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateLawAgent"];
    static string _urlSetUpdateLawGoods = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateLawGoods"];
    static string _urlSetUpdateLawDeadline = _serviceUrl + ConfigurationManager.AppSettings["urlSetUpdateLawDeadline"];


    //path file
    static string _path_file_law_usercreate = ConfigurationManager.AppSettings["path_file_law_usercreate"];
    static string _path_file_law_lawmemo = ConfigurationManager.AppSettings["path_file_law_lawmemo"];
    static string _path_file_law_usermemo = ConfigurationManager.AppSettings["path_file_law_usermemo"];

    static string _path_file_law_trademarks_profile = ConfigurationManager.AppSettings["path_file_law_trademarks_profile"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;


    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());

        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

        ViewState["org_permission"] = _dataEmployee.employee_list[0].org_idx;
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["joblevel_permission"] = _dataEmployee.employee_list[0].jobgrade_level;
        ViewState["emp_type_permission"] = _dataEmployee.employee_list[0].emp_type_idx;

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
            initPageLoad();

            linkBtnTrigger(btnSaveTrademarkRecordSheet);
            linkBtnTrigger(btnUpdateTrademarkRecordSheet);

            ////linkBtnTrigger(lbCreate);
            ////linkBtnTrigger(lbWaitApprove);

            ////getCountWaitApproveDocument();

            //data table
            getPrioritiesList();
            getClientList();
            getAgentList();
            getGoodsList();
            getRenewalList();
            getDeadlineList();
            getAssignmentList();
            getChangeofNameList();

        }
    }

    #region set/get bind data

    protected void getDetailRecordSheet(GridView gvName, int _u1doc_idx)
    {
        data_law data_u1_document_record = new data_law();
        tcm_u1_document_detail u1_document_record = new tcm_u1_document_detail();
        data_u1_document_record.tcm_u1_document_list = new tcm_u1_document_detail[1];

        u1_document_record.u1_doc_idx = _u1doc_idx;

        data_u1_document_record.tcm_u1_document_list[0] = u1_document_record;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));
        data_u1_document_record = callServicePostLaw(_urlGetLawRecordSheet, data_u1_document_record);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u3_document_viewdetail));
        if (data_u1_document_record.return_code == 0)
        {

            ViewState["Vs_GvDetailRecordSheet"] = data_u1_document_record.tcm_u1_document_list;
            setGridData(gvName, ViewState["Vs_GvDetailRecordSheet"]);
        }
        else
        {
            ViewState["Vs_GvDetailRecordSheet"] = null;
            setGridData(gvName, ViewState["Vs_GvDetailRecordSheet"]);
        }

    }

    protected void getViewDetailRecordSheet(int _u1doc_idx, int _type_idx, int _chek_viewtab)
    {
        data_law data_u1_document_viewrecord = new data_law();
        tcm_u1_document_detail u1_document_viewrecord = new tcm_u1_document_detail();
        data_u1_document_viewrecord.tcm_u1_document_list = new tcm_u1_document_detail[1];

        u1_document_viewrecord.u1_doc_idx = _u1doc_idx;
        u1_document_viewrecord.type_idx = _type_idx;

        data_u1_document_viewrecord.tcm_u1_document_list[0] = u1_document_viewrecord;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));
        data_u1_document_viewrecord = callServicePostLaw(_urlGetLawRecordSheet, data_u1_document_viewrecord);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u3_document_viewdetail));

        if (data_u1_document_viewrecord.return_code == 0)
        {

            ViewState["vs_detailrecordsheet"] = data_u1_document_viewrecord.tcm_u1_document_list;

            ////if (_chek_viewtab == 0) // view record //
            ////{
            ////    setFormData(fvName, FormViewMode.ReadOnly, ViewState["vs_detailrecordsheet"]);
            ////}
            ////else // edit/save record //
            ////{
            ////    setFormData(fvName, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);
            ////}


            //////check Country in Document
            ////CheckBoxList chkCountryview = (CheckBoxList)fvName.FindControl("chkCountryview");
            ////getCountry(chkCountryview, 0);
            ////string _valuecountry = data_u0_doc_viewdetail.tcm_u0_document_list[0].country_idx_checkdetail.ToString();
            ////string[] _country_check = _valuecountry.Split(',');

            ////foreach (ListItem item in chkCountryview.Items)
            ////{
            ////    foreach (string _i in _country_check)
            ////    {

            ////        if (item.Value == _i)
            ////        {
            ////            item.Selected = true;
            ////        }
            ////    }
            ////}
        }

    }

    protected void getViewDetailLawOfficer(GridView gvName, int _u0doc_idx)
    {
        data_law data_u3_document_viewdetail = new data_law();
        tcm_u3_document_detail u3_document_viewdetail = new tcm_u3_document_detail();
        data_u3_document_viewdetail.tcm_u3_document_list = new tcm_u3_document_detail[1];
        u3_document_viewdetail.u0_doc_idx = _u0doc_idx;
        data_u3_document_viewdetail.tcm_u3_document_list[0] = u3_document_viewdetail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));
        data_u3_document_viewdetail = callServicePostLaw(_urlGetViewDetailLawOfficer, data_u3_document_viewdetail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u3_document_viewdetail));

        setGridData(gvName, data_u3_document_viewdetail.tcm_u3_document_list);

    }

    protected void getLawOfficer(CheckBoxList chkName, int _emp_idx)
    {

        data_law data_u3_document_detail = new data_law();
        tcm_u3_document_detail u3_document_detail = new tcm_u3_document_detail();
        data_u3_document_detail.tcm_u3_document_list = new tcm_u3_document_detail[1];

        data_u3_document_detail.tcm_u3_document_list[0] = u3_document_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_u3_document_detail = callServicePostLaw(_urlGetSelectLawInDocument, data_u3_document_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setChkData(chkName, data_u3_document_detail.tcm_u3_document_list, "emp_name_en", "emp_idx");
        //chkName.Items.Insert(0, new ListItem("--- Select ---", "0"));

    }

    protected void getCountry(CheckBoxList chkName, int _country_idx)
    {

        data_law data_m0_country_detail = new data_law();
        tcm_m0_country_detail m0_country_detail = new tcm_m0_country_detail();
        data_m0_country_detail.tcm_m0_country_list = new tcm_m0_country_detail[1];

        data_m0_country_detail.tcm_m0_country_list[0] = m0_country_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_m0_country_detail = callServicePostLaw(_urlGetLawContryDetail, data_m0_country_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setChkData(chkName, data_m0_country_detail.tcm_m0_country_list, "country_name_en", "country_idx");
        //chkName.Items.Insert(0, new ListItem("--- Select ---", "0"));

    }

    protected void getDoctypeDetail(DropDownList ddlName, int _doctype_idx)
    {

        data_law data_m0_type_detail = new data_law();
        tcm_m0_type_detail m0_type_detail = new tcm_m0_type_detail();
        data_m0_type_detail.tcm_m0_type_list = new tcm_m0_type_detail[1];
        m0_type_detail.condition = 1;
        data_m0_type_detail.tcm_m0_type_list[0] = m0_type_detail;

        data_m0_type_detail = callServicePostLaw(_urlGetLawM0DocType, data_m0_type_detail);
        setDdlData(ddlName, data_m0_type_detail.tcm_m0_type_list, "type_name_en", "type_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Document Type ---", "0"));
        ddlName.SelectedValue = _doctype_idx.ToString();
        ////setGridData(GvDetail, data_m0_type_detail.tcm_m0_type_list);

    }

    protected void getSubDoctypeDetail(DropDownList ddlName, int _doctype_idx)
    {

        data_law data_m0_subtype_detail = new data_law();
        tcm_m0_subtype_detail m0_subtype_detail = new tcm_m0_subtype_detail();
        data_m0_subtype_detail.tcm_m0_subtype_list = new tcm_m0_subtype_detail[1];
        m0_subtype_detail.condition = 2;
        m0_subtype_detail.type_idx = _doctype_idx;
        data_m0_subtype_detail.tcm_m0_subtype_list[0] = m0_subtype_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_subtype_detail));
        data_m0_subtype_detail = callServicePostLaw(_urlGetLawM0SubDocType, data_m0_subtype_detail);
        setDdlData(ddlName, data_m0_subtype_detail.tcm_m0_subtype_list, "subtype_name_en", "subtype_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Sub Document Type ---", "0"));


    }

    protected void getJobType(DropDownList ddlName, int _jobtype_idx)
    {

        data_law data_m0_jobtype_detail = new data_law();
        tcm_m0_jobtype_detail m0_jobtype_detail = new tcm_m0_jobtype_detail();
        data_m0_jobtype_detail.tcm_m0_jobtype_list = new tcm_m0_jobtype_detail[1];

        data_m0_jobtype_detail.tcm_m0_jobtype_list[0] = m0_jobtype_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_m0_jobtype_detail = callServicePostLaw(_urlGetLawM0JobType, data_m0_jobtype_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_jobtype_detail.tcm_m0_jobtype_list, "jobtype_name_en", "jobtype_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Job Type ---", "0"));
        //chkName.Items.Insert(0, new ListItem("--- Select ---", "0"));

    }

    protected void getDetailDocumentTrademark(GridView gvName, int _u0doc_idx)
    {
        data_law data_u0_document_detail = new data_law();
        tcm_u0_document_detail u0_document_detail = new tcm_u0_document_detail();
        data_u0_document_detail.tcm_u0_document_list = new tcm_u0_document_detail[1];
        u0_document_detail.u0_doc_idx = _u0doc_idx;

        data_u0_document_detail.tcm_u0_document_list[0] = u0_document_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_u0_document_detail = callServicePostLaw(_urlGetLawDetailDocument, data_u0_document_detail);
        setGridData(gvName, data_u0_document_detail.tcm_u0_document_list);


    }

    protected void getViewDetailDocumentTrademark(FormView fvName, int _u0doc_idx)
    {
        data_law data_u0_doc_viewdetail = new data_law();
        tcm_u0_document_detail u0_document_viewdetail = new tcm_u0_document_detail();
        data_u0_doc_viewdetail.tcm_u0_document_list = new tcm_u0_document_detail[1];
        u0_document_viewdetail.u0_doc_idx = _u0doc_idx;

        data_u0_doc_viewdetail.tcm_u0_document_list[0] = u0_document_viewdetail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_u0_doc_viewdetail = callServicePostLaw(_urlGetLawDetailDocument, data_u0_doc_viewdetail);
        //setGridData(gvName, data_u0_document_detail.tcm_u0_document_list);
        if (data_u0_doc_viewdetail.return_code == 0)
        {
            setFormData(fvName, FormViewMode.ReadOnly, data_u0_doc_viewdetail.tcm_u0_document_list);

            //check Country in Document
            CheckBoxList chkCountryview = (CheckBoxList)fvName.FindControl("chkCountryview");
            getCountry(chkCountryview, 0);
            string _valuecountry = data_u0_doc_viewdetail.tcm_u0_document_list[0].country_idx_checkdetail.ToString();
            string[] _country_check = _valuecountry.Split(',');

            foreach (ListItem item in chkCountryview.Items)
            {
                foreach (string _i in _country_check)
                {

                    if (item.Value == _i)
                    {
                        item.Selected = true;
                    }
                }
            }
        }

    }

    protected void getViewDetailEditDocumentTrademark(FormView fvName, int _u0doc_idx)
    {
        data_law data_u0_doc_viewdetail = new data_law();
        tcm_u0_document_detail u0_document_viewdetail = new tcm_u0_document_detail();
        data_u0_doc_viewdetail.tcm_u0_document_list = new tcm_u0_document_detail[1];
        u0_document_viewdetail.u0_doc_idx = _u0doc_idx;

        data_u0_doc_viewdetail.tcm_u0_document_list[0] = u0_document_viewdetail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_u0_doc_viewdetail = callServicePostLaw(_urlGetLawDetailDocument, data_u0_doc_viewdetail);
        //setGridData(gvName, data_u0_document_detail.tcm_u0_document_list);
        if (data_u0_doc_viewdetail.return_code == 0)
        {
            setFormData(fvName, FormViewMode.Edit, data_u0_doc_viewdetail.tcm_u0_document_list);

            //check Country in Document
            CheckBoxList chkCountryview = (CheckBoxList)fvName.FindControl("chkCountry");
            getCountry(chkCountryview, 0);
            string _valuecountry = data_u0_doc_viewdetail.tcm_u0_document_list[0].country_idx_checkdetail.ToString();
            string[] _country_check = _valuecountry.Split(',');

            foreach (ListItem item in chkCountryview.Items)
            {
                foreach (string _i in _country_check)
                {

                    if (item.Value == _i)
                    {
                        item.Selected = true;
                    }
                }
            }
        }

    }

    protected void getLogDetailDocument(Repeater rpName, int _u0doc_idx, int _u1doc_idx)
    {
        data_law data_u0_doc_log = new data_law();
        tcm_u2_document_detail u0_document_log = new tcm_u2_document_detail();
        data_u0_doc_log.tcm_u2_document_list = new tcm_u2_document_detail[1];
        u0_document_log.u0_doc_idx = _u0doc_idx;
        u0_document_log.u1_doc_idx = _u1doc_idx;

        data_u0_doc_log.tcm_u2_document_list[0] = u0_document_log;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_u0_doc_log = callServicePostLaw(_urlGetLawLogDetailDocument, data_u0_doc_log);

        setRepeaterData(rpName, data_u0_doc_log.tcm_u2_document_list);
    }

    protected void getDetailWaitApproveDocument(GridView gvName, int _u0doc_idx)
    {
        data_law data_u0_document_wait = new data_law();
        tcm_u0_document_detail u0_document_wait = new tcm_u0_document_detail();
        data_u0_document_wait.tcm_u0_document_list = new tcm_u0_document_detail[1];
        u0_document_wait.u0_doc_idx = _u0doc_idx;
        u0_document_wait.cemp_idx = _emp_idx;
        u0_document_wait.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0_document_wait.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        u0_document_wait.jobgrade_level = int.Parse(ViewState["joblevel_permission"].ToString());

        data_u0_document_wait.tcm_u0_document_list[0] = u0_document_wait;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));
        data_u0_document_wait = callServicePostLaw(_urlGetLawWaitApproveDetailDoc, data_u0_document_wait);
        setGridData(gvName, data_u0_document_wait.tcm_u0_document_list);


    }

    protected void getDecisionApprove(DropDownList ddlName, int _noidx)
    {

        data_law data_m0_decision_detail = new data_law();
        tcm_m0_decision_detail m0_decision_detail = new tcm_m0_decision_detail();
        data_m0_decision_detail.tcm_m0_decision_list = new tcm_m0_decision_detail[1];

        m0_decision_detail.noidx = _noidx;

        data_m0_decision_detail.tcm_m0_decision_list[0] = m0_decision_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_m0_decision_detail = callServicePostLaw(_urlGetLawStatusApprove, data_m0_decision_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_decision_detail.tcm_m0_decision_list, "decision_name", "decision_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Status ---", "0"));


    }

    protected void getDecisionAttachFileApprove(FormView fvName, int _noidx)
    {

        data_law data_m0_decision_detail = new data_law();
        tcm_m0_decision_detail m0_decision_detail = new tcm_m0_decision_detail();
        data_m0_decision_detail.tcm_m0_decision_list = new tcm_m0_decision_detail[1];

        m0_decision_detail.noidx = _noidx;

        data_m0_decision_detail.tcm_m0_decision_list[0] = m0_decision_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_m0_decision_detail = callServicePostLaw(_urlGetLawStatusApprove, data_m0_decision_detail);
        Label lbldecision_idx = (Label)fvName.FindControl("lbldecision_idx");

        lbldecision_idx.Text = data_m0_decision_detail.tcm_m0_decision_list[0].decision_idx.ToString();

    }

    protected void getOwner(DropDownList ddlName, int _owner_idx)
    {

        data_law data_m0_owner_detail = new data_law();
        tcm_m0_owner_detail m0_owner_detail = new tcm_m0_owner_detail();
        data_m0_owner_detail.tcm_m0_owner_list = new tcm_m0_owner_detail[1];

        //m0_owner_detail.owner_idx = _owner_idx;

        data_m0_owner_detail.tcm_m0_owner_list[0] = m0_owner_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_m0_owner_detail = callServicePostLaw(_urlGetLawM0Owner, data_m0_owner_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_owner_detail.tcm_m0_owner_list, "owner_name_en", "owner_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Owner ---", "0"));
        ddlName.SelectedValue = _owner_idx.ToString();
        if (_owner_idx != 0)
        {
            TextBox tbowner_address_en = (TextBox)fvRegistrationDetail.FindControl("tbowner_address_en");
            tbowner_address_en.Text = data_m0_owner_detail.tcm_m0_owner_list[0].owner_address_en.ToString();

        }
        else
        {
            TextBox tbowner_address_en = (TextBox)fvRegistrationDetail.FindControl("tbowner_address_en");
            tbowner_address_en.Text = string.Empty;
        }

    }

    protected void getIncorporated(DropDownList ddlName, int _Incorporated_in_idx)
    {

        data_law data_m0_Incorporated_detail = new data_law();
        tcm_m0_Incorporated_detail m0_Incorporated_detail = new tcm_m0_Incorporated_detail();
        data_m0_Incorporated_detail.tcm_m0_Incorporated_list = new tcm_m0_Incorporated_detail[1];

        m0_Incorporated_detail.Incorporated_in_idx = _Incorporated_in_idx;

        data_m0_Incorporated_detail.tcm_m0_Incorporated_list[0] = m0_Incorporated_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_m0_Incorporated_detail = callServicePostLaw(_urlGetLawM0Incorporated, data_m0_Incorporated_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_Incorporated_detail.tcm_m0_Incorporated_list, "Incorporated_in_en", "Incorporated_in_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Incorporated In ---", "0"));
        ddlName.SelectedValue = _Incorporated_in_idx.ToString();

    }

    protected void getTrademarkStatus(DropDownList ddlName, int _trademark_status_idx, int _type_idx, string _form_setdetail)
    {

        data_law data_m0_trademark_status = new data_law();
        tcm_m0_trademark_status_detail m0_trademark_status_detail = new tcm_m0_trademark_status_detail();
        data_m0_trademark_status.tcm_m0_trademark_status_list = new tcm_m0_trademark_status_detail[1];

        m0_trademark_status_detail.condition = 0;
        m0_trademark_status_detail.trademark_status_idx = _trademark_status_idx;
        m0_trademark_status_detail.type_idx = _type_idx;
        m0_trademark_status_detail.formset_detail = _form_setdetail;

        data_m0_trademark_status.tcm_m0_trademark_status_list[0] = m0_trademark_status_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_trademark_status));
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_trademark_status));
        data_m0_trademark_status = callServicePostLaw(_urlGetLawM0Trademark, data_m0_trademark_status);


        setDdlData(ddlName, data_m0_trademark_status.tcm_m0_trademark_status_list, "trademark_status_en", "trademark_status_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Status ---", "0"));
        ddlName.SelectedValue = _trademark_status_idx.ToString();

    }


    protected void getTrademarkSubStatus(DropDownList ddlName, int _trademark_status_idx, int _type_idx, string _form_setdetail)
    {

        data_law data_m0_trademark_substatus = new data_law();
        tcm_m0_trademark_status_detail m0_trademark_substatus_detail = new tcm_m0_trademark_status_detail();
        data_m0_trademark_substatus.tcm_m0_trademark_status_list = new tcm_m0_trademark_status_detail[1];

        m0_trademark_substatus_detail.condition = 1;
        m0_trademark_substatus_detail.trademark_status_idx = _trademark_status_idx;
        m0_trademark_substatus_detail.type_idx = _type_idx;
        m0_trademark_substatus_detail.formset_detail = _form_setdetail;

        data_m0_trademark_substatus.tcm_m0_trademark_status_list[0] = m0_trademark_substatus_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_trademark_status));

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_trademark_substatus));
        data_m0_trademark_substatus = callServicePostLaw(_urlGetLawM0Trademark, data_m0_trademark_substatus);
        

        setDdlData(ddlName, data_m0_trademark_substatus.tcm_m0_trademark_status_list, "trademark_substatus_en", "trademark_substatus_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Sub Status ---", "0"));
        

        //ddlName.SelectedValue = _trademark_status_idx.ToString();

    }

    protected void getM0ActionDetailList(CheckBoxList chkName, int _action_detail_idx)
    {

        data_law data_m0_action_detail = new data_law();
        tcm_m0_actionsrecord_detail m0_action_detail = new tcm_m0_actionsrecord_detail();
        data_m0_action_detail.tcm_m0_actions_detail_list = new tcm_m0_actionsrecord_detail[1];

        data_m0_action_detail.tcm_m0_actions_detail_list[0] = m0_action_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_trademark_status));
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_trademark_substatus));

        data_m0_action_detail = callServicePostLaw(_urlGetLawM0ActionkDetailList, data_m0_action_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_action_detail));

        setChkData(chkName, data_m0_action_detail.tcm_m0_actions_detail_list, "action_detail_name", "action_detail_idx");
        //setDdlData(ddlName, data_m0_trademark_substatus.tcm_m0_trademark_status_list, "trademark_substatus_en", "trademark_substatus_idx");
        //ddlName.Items.Insert(0, new ListItem("--- Select Sub Status ---", "0"));
        //ddlName.SelectedValue = _trademark_status_idx.ToString();

    }

    protected void getRecordSheetStatus(DropDownList ddlName, int _status_idx, string setstatus_name)
    {

        data_law data_m0_recordsheet_status = new data_law();
        tcm_m0_recordsheet_status_detail m0_recordsheet_status_detail = new tcm_m0_recordsheet_status_detail();
        data_m0_recordsheet_status.tcm_m0_recordsheet_status_list = new tcm_m0_recordsheet_status_detail[1];

        m0_recordsheet_status_detail.status_idx = _status_idx;
        m0_recordsheet_status_detail.status_name = setstatus_name.ToString();

        data_m0_recordsheet_status.tcm_m0_recordsheet_status_list[0] = m0_recordsheet_status_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_m0_recordsheet_status = callServicePostLaw(_urlGetLawM0RecordStatus, data_m0_recordsheet_status);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_recordsheet_status.tcm_m0_recordsheet_status_list, "status_name_en", "status_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Status ---", "0"));
        ddlName.SelectedValue = _status_idx.ToString();

    }

    protected void getLocalLanguage(DropDownList ddlName, int _language_idx)
    {

        data_law data_m0_local_language = new data_law();
        tcm_m0_local_language_detail m0_local_language_detail = new tcm_m0_local_language_detail();
        data_m0_local_language.tcm_m0_local_language_list = new tcm_m0_local_language_detail[1];

        m0_local_language_detail.language_idx = _language_idx;

        data_m0_local_language.tcm_m0_local_language_list[0] = m0_local_language_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_m0_local_language = callServicePostLaw(_urlGetLawM0LocalLanguage, data_m0_local_language);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_local_language.tcm_m0_local_language_list, "language_en", "language_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Local Language ---", "0"));
        ddlName.SelectedValue = _language_idx.ToString();

    }

    protected void getRecordsheetCountry(DropDownList ddlName, int _country_idx)
    {

        data_law data_m0_country = new data_law();
        tcm_m0_country_detail m0_country_detail = new tcm_m0_country_detail();
        data_m0_country.tcm_m0_country_list = new tcm_m0_country_detail[1];

        m0_country_detail.country_idx = _country_idx;

        data_m0_country.tcm_m0_country_list[0] = m0_country_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_m0_country = callServicePostLaw(_urlGetLawContryDetail, data_m0_country);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_country.tcm_m0_country_list, "country_name_en", "country_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Country ---", "0"));
        ddlName.SelectedValue = _country_idx.ToString();

    }

    protected void getLawM0Actions(GridView gvName, int _action_idx)
    {
        data_law data_m0_actions_detail = new data_law();
        tcm_m0_actions_detail m0_actions_detail = new tcm_m0_actions_detail();
        data_m0_actions_detail.tcm_m0_actions_list = new tcm_m0_actions_detail[1];
        m0_actions_detail.action_idx = _action_idx;


        data_m0_actions_detail.tcm_m0_actions_list[0] = m0_actions_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_m0_actions_detail = callServicePostLaw(_urlGetLawM0Actions, data_m0_actions_detail);
        setGridData(gvName, data_m0_actions_detail.tcm_m0_actions_list);


    }

    protected void getTypeOfRegistrtion(DropDownList ddlName, int _typeofregistration_idx)
    {

        data_law data_m0_typeofregistration_detail = new data_law();
        tcm_m0_typeofregistration_detail m0_typeofregistration_detail = new tcm_m0_typeofregistration_detail();
        data_m0_typeofregistration_detail.tcm_m0_typeofregistration_list = new tcm_m0_typeofregistration_detail[1];

        m0_typeofregistration_detail.typeofregistration_idx = _typeofregistration_idx;

        data_m0_typeofregistration_detail.tcm_m0_typeofregistration_list[0] = m0_typeofregistration_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_m0_typeofregistration_detail = callServicePostLaw(_urlGetLawM0TypeOfRegistration, data_m0_typeofregistration_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_typeofregistration_detail.tcm_m0_typeofregistration_list, "typeofregistration_en", "typeofregistration_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Type Of Registrtion ---", "0"));
        ddlName.SelectedValue = _typeofregistration_idx.ToString();

    }

    protected void getRecordSheetTypeOfwork(RadioButtonList rdoName, int _typeofwork_idx)
    {

        data_law data_m0_typeofwork_detail = new data_law();
        tcm_m0_typeofwork_detail m0_typeofwork_detail = new tcm_m0_typeofwork_detail();
        data_m0_typeofwork_detail.tcm_m0_typeofwork_list = new tcm_m0_typeofwork_detail[1];

        m0_typeofwork_detail.typeofwork_idx = 0;

        data_m0_typeofwork_detail.tcm_m0_typeofwork_list[0] = m0_typeofwork_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_m0_typeofwork_detail = callServicePostLaw(_urlGetLawM0TypeOfWork, data_m0_typeofwork_detail);


        setRdoData(rdoName, data_m0_typeofwork_detail.tcm_m0_typeofwork_list, "typeofwork_en", "typeofwork_idx");

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

    }

    protected void getViewRecordsheet(int _u1idx, int _type_idx)
    {

        data_law data_u1_document_viewrecord = new data_law();
        tcm_u1_document_detail u1_document_viewrecord = new tcm_u1_document_detail();
        data_u1_document_viewrecord.tcm_u1_document_list = new tcm_u1_document_detail[1];

        u1_document_viewrecord.u1_doc_idx = _u1idx;
        u1_document_viewrecord.type_idx = _type_idx;

        data_u1_document_viewrecord.tcm_u1_document_list[0] = u1_document_viewrecord;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1_document_viewrecord));
        data_u1_document_viewrecord = callServicePostLaw(_urlGetLawViewRecordSheet, data_u1_document_viewrecord);
        if (data_u1_document_viewrecord.return_code == 0)
        {

            ViewState["vs_detailrecordsheet"] = data_u1_document_viewrecord.tcm_u1_document_list;
            ViewState["vs_renewal_recordsheet"] = data_u1_document_viewrecord.tcm_record_renewal_list;
            ViewState["vs_publication_recordsheet"] = data_u1_document_viewrecord.tcm_record_publication_list;

            ViewState["vs_assignment_recordsheet"] = data_u1_document_viewrecord.tcm_record_assignment_list;
            ViewState["vs_changeofname_recordsheet"] = data_u1_document_viewrecord.tcm_record_changeofname_list;

            ViewState["vs_dependent_recordsheet"] = data_u1_document_viewrecord.tcm_record_dependent_list;
            ViewState["vs_priorities_recordsheet"] = data_u1_document_viewrecord.tcm_record_priorities_list;
            ViewState["vs_client_recordsheet"] = data_u1_document_viewrecord.tcm_record_client_list;
            ViewState["vs_agent_recordsheet"] = data_u1_document_viewrecord.tcm_record_agent_list;
            ViewState["vs_good_recordsheet"] = data_u1_document_viewrecord.tcm_record_goods_list;
            ViewState["vs_additional_recordsheet"] = data_u1_document_viewrecord.tcm_record_additional_list;
            ViewState["vs_trademarks_recordsheet"] = data_u1_document_viewrecord.tcm_record_trademarkprofile_list;

            ViewState["vs_typeofwork_recordsheet"] = data_u1_document_viewrecord.tcm_record_typeofwork_list;
            ViewState["vs_description_recordsheet"] = data_u1_document_viewrecord.tcm_record_description_list;
            ViewState["vs_claims_recordsheet"] = data_u1_document_viewrecord.tcm_record_claims_list;
            ViewState["vs_summary_recordsheet"] = data_u1_document_viewrecord.tcm_record_summary_list;
            ViewState["vs_backgroud_recordsheet"] = data_u1_document_viewrecord.tcm_record_backgroud_list;
            ViewState["vs_abstract_recordsheet"] = data_u1_document_viewrecord.tcm_record_abstract_list;
            //////litDebug1.Text = data_u1_document_viewrecord.tcm_record_agent_list.Count().ToString();


        }
    }

    protected void getViewRecordsheetPublication(int _u1idx, int _publication_idx)
    {

        data_law data_m0_deadline_detail = new data_law();
        tcm_record_deadline_detail m0_deadline_detail = new tcm_record_deadline_detail();
        data_m0_deadline_detail.tcm_record_deadline_list = new tcm_record_deadline_detail[1];

        m0_deadline_detail.u1_doc_idx = _u1idx;
        m0_deadline_detail.publication_idx = _publication_idx;

        data_m0_deadline_detail.tcm_record_deadline_list[0] = m0_deadline_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_deadline_detail));
        data_m0_deadline_detail = callServicePostLaw(_urlGetLawViewDeadlineRecordSheet, data_m0_deadline_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_deadline_detail));

        ViewState["vs_ActioneDetail"] = data_m0_deadline_detail.tcm_record_actions_list;
        ViewState["vs_DeadlineDetail"] = data_m0_deadline_detail.tcm_record_deadline_list;
        //setGridData(gvDetailDeadline, ViewState["vs_DeadlineDetail"]);
    }

    protected void getddlTrademarkprofile(DropDownList ddlName, int _trademark_profile_idx, int _condition)
    {

        data_law data_m0_trademarkprofile = new data_law();
        tcm_record_trademarkprofile_detail m0_trademarkprofile_detail = new tcm_record_trademarkprofile_detail();
        data_m0_trademarkprofile.tcm_record_trademarkprofile_list = new tcm_record_trademarkprofile_detail[1];

        m0_trademarkprofile_detail.trademark_profile_idx = _trademark_profile_idx;
        m0_trademarkprofile_detail.condition = _condition;

        data_m0_trademarkprofile.tcm_record_trademarkprofile_list[0] = m0_trademarkprofile_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_trademarkprofile));
        data_m0_trademarkprofile = callServicePostLaw(_urlGetDDLTrademarksProfile, data_m0_trademarkprofile);
        

        setDdlData(ddlName, data_m0_trademarkprofile.tcm_record_trademarkprofile_list, "Name", "trademark_profile_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Trademark Profile ---", "0"));
        ddlName.SelectedValue = _trademark_profile_idx.ToString();

        ////if (_condition == 1)
        ////{
        ////   // ViewState["Vs_TrademarkProfile"] = data_m0_trademarkprofile.tcm_record_trademarkprofile_list;

        ////    TextBox tbName_trademark = (TextBox)fvTademarkProfile.FindControl("tbName_trademark");
        ////    if (data_m0_trademarkprofile.return_code == 0)
        ////    {

        ////        tbName_trademark.Text = data_m0_trademarkprofile.tcm_record_trademarkprofile_list[0].Name.ToString();
        ////    }
        ////    else
        ////    {
        ////        tbName_trademark.Text = "";
        ////        ddlName.ClearSelection();
        ////        //getddlTrademarkprofile(ddlName, 0, 0);
        ////    }

        ////    //setFormData(fvTademarkProfile, FormViewMode.Insert, ViewState["Vs_TrademarkProfile"]);
        ////}


    }

    #endregion set/get bind data


    #region Formview Databind
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            FormView FvName = (FormView)sender;

            switch (FvName.ID)
            {
               
                case "fvRegistrationDetail":
                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {
                       
                    }
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {

                        CleardataSetRenewalList();

                        TextBox tbowner_idx = (TextBox)FvName.FindControl("tbowner_idx");
                        DropDownList ddlowner_idx = (DropDownList)FvName.FindControl("ddlowner_idx");

                        TextBox tbincorporated_in_idx = (TextBox)FvName.FindControl("tbincorporated_in_idx");
                        DropDownList ddlincorporated_in_idx = (DropDownList)FvName.FindControl("ddlincorporated_in_idx");
                        DropDownList ddlTrademarkStatus = (DropDownList)FvName.FindControl("ddlTrademarkStatus");
                        DropDownList ddlTrademarkSubStatus = (DropDownList)FvName.FindControl("ddlTrademarkSubStatus");
                        DropDownList ddlTypeOfRegistrtion = (DropDownList)FvName.FindControl("ddlTypeOfRegistrtion");

                        DropDownList ddlRenewalStatus = (DropDownList)FvName.FindControl("ddlRenewalStatus");
                        DropDownList ddlRenewalSubStatus = (DropDownList)FvName.FindControl("ddlRenewalSubStatus");

                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");
                        HiddenField hfACIDX = (HiddenField)FvName.FindControl("hfACIDX");
                        HiddenField hfNOIDX = (HiddenField)FvName.FindControl("hfNOIDX");
                        HiddenField hfSTAIDX = (HiddenField)FvName.FindControl("hfSTAIDX");
                        HiddenField hftype_idx = (HiddenField)FvName.FindControl("hftype_idx");

                        TextBox tbdocrequest_code_detail = (TextBox)FvName.FindControl("tbdocrequest_code_detail");
                        TextBox tbcreate_date_u0_detail = (TextBox)FvName.FindControl("tbcreate_date_u0_detail");
                        TextBox tbtype_name_en = (TextBox)FvName.FindControl("tbtype_name_en");
                        TextBox tbsubtype_name_en = (TextBox)FvName.FindControl("tbsubtype_name_en");
                        TextBox tbcountry_name_en = (TextBox)FvName.FindControl("tbcountry_name_en");
                        TextBox tbemp_name_en_head = (TextBox)FvName.FindControl("tbemp_name_en_head");
                        TextBox tbemp_name_en_mgemp = (TextBox)FvName.FindControl("tbemp_name_en_mgemp");


                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();
                        hfACIDX.Value = _linq_detailrecord_sheet[0].acidx.ToString();
                        hfNOIDX.Value = _linq_detailrecord_sheet[0].noidx.ToString();
                        hfSTAIDX.Value = _linq_detailrecord_sheet[0].staidx.ToString();
                        hftype_idx.Value = _linq_detailrecord_sheet[0].type_idx.ToString();

                        tbdocrequest_code_detail.Text = _linq_detailrecord_sheet[0].docrequest_code.ToString();
                        tbcreate_date_u0_detail.Text = _linq_detailrecord_sheet[0].create_date_u0.ToString();
                        tbtype_name_en.Text = _linq_detailrecord_sheet[0].type_name_en.ToString();
                        tbsubtype_name_en.Text = _linq_detailrecord_sheet[0].subtype_name_en.ToString();
                        tbcountry_name_en.Text = _linq_detailrecord_sheet[0].country_name_en.ToString();
                        tbemp_name_en_head.Text = _linq_detailrecord_sheet[0].emp_name_en_head.ToString();
                        tbemp_name_en_mgemp.Text = _linq_detailrecord_sheet[0].emp_name_en_mgemp.ToString();


                        ViewState["vs_detailrecordsheet_type_idx"] = _linq_detailrecord_sheet[0].type_idx.ToString();
                        //bind detail to insert record sheet


                        getOwner(ddlowner_idx, 0);
                        getIncorporated(ddlincorporated_in_idx, 0);
                        getTrademarkStatus(ddlTrademarkStatus, 0, int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvRegistrationDetail");
                        getTrademarkSubStatus(ddlTrademarkSubStatus, int.Parse(ddlTrademarkStatus.SelectedValue), int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvRegistrationDetail");

                        getTrademarkStatus(ddlRenewalStatus, 0, int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "Renewal");
                        getTrademarkSubStatus(ddlRenewalSubStatus, int.Parse(ddlRenewalStatus.SelectedValue), int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "Renewal");

                        getTypeOfRegistrtion(ddlTypeOfRegistrtion, 0);

                    }
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {

                        ////TextBox tbowner_idx = (TextBox)FvName.FindControl("tbowner_idx");
                        ////DropDownList ddlowner_idx = (DropDownList)FvName.FindControl("ddlowner_idx");

                        ////TextBox tbincorporated_in_idx = (TextBox)FvName.FindControl("tbincorporated_in_idx");
                        ////DropDownList ddlincorporated_in_idx = (DropDownList)FvName.FindControl("ddlincorporated_in_idx");
                        ////DropDownList ddlTrademarkStatus = (DropDownList)FvName.FindControl("ddlTrademarkStatus");
                        ////DropDownList ddlTrademarkSubStatus = (DropDownList)FvName.FindControl("ddlTrademarkSubStatus");
                        ////DropDownList ddlTypeOfRegistrtion = (DropDownList)FvName.FindControl("ddlTypeOfRegistrtion");

                        ////HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        ////HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");
                        ////HiddenField hfACIDX = (HiddenField)FvName.FindControl("hfACIDX");
                        ////HiddenField hfNOIDX = (HiddenField)FvName.FindControl("hfNOIDX");
                        ////HiddenField hfSTAIDX = (HiddenField)FvName.FindControl("hfSTAIDX");

                        ////TextBox tbdocrequest_code_detail = (TextBox)FvName.FindControl("tbdocrequest_code_detail");
                        ////TextBox tbcreate_date_u0_detail = (TextBox)FvName.FindControl("tbcreate_date_u0_detail");
                        ////TextBox tbtype_name_en = (TextBox)FvName.FindControl("tbtype_name_en");
                        ////TextBox tbsubtype_name_en = (TextBox)FvName.FindControl("tbsubtype_name_en");
                        ////TextBox tbcountry_name_en = (TextBox)FvName.FindControl("tbcountry_name_en");
                        ////TextBox tbemp_name_en_head = (TextBox)FvName.FindControl("tbemp_name_en_head");
                        ////TextBox tbemp_name_en_mgemp = (TextBox)FvName.FindControl("tbemp_name_en_mgemp");


                        ////tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        ////var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                        ////                                select dt).ToList();

                        //////bind detail to insert record sheet

                        ////hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        ////hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();
                        ////hfACIDX.Value = _linq_detailrecord_sheet[0].acidx.ToString();
                        ////hfNOIDX.Value = _linq_detailrecord_sheet[0].noidx.ToString();
                        ////hfSTAIDX.Value = _linq_detailrecord_sheet[0].staidx.ToString();

                        ////tbdocrequest_code_detail.Text = _linq_detailrecord_sheet[0].docrequest_code.ToString();
                        ////tbcreate_date_u0_detail.Text = _linq_detailrecord_sheet[0].create_date_u0.ToString();
                        ////tbtype_name_en.Text = _linq_detailrecord_sheet[0].type_name_en.ToString();
                        ////tbsubtype_name_en.Text = _linq_detailrecord_sheet[0].subtype_name_en.ToString();
                        ////tbcountry_name_en.Text = _linq_detailrecord_sheet[0].country_name_en.ToString();
                        ////tbemp_name_en_head.Text = _linq_detailrecord_sheet[0].emp_name_en_head.ToString();
                        ////tbemp_name_en_mgemp.Text = _linq_detailrecord_sheet[0].emp_name_en_mgemp.ToString();
                        //////bind detail to insert record sheet


                        ////getOwner(ddlowner_idx, 0);
                        ////getIncorporated(ddlincorporated_in_idx, 0);
                        ////getTrademarkStatus(ddlTrademarkStatus, 0);
                        ////getTrademarkStatus(ddlTrademarkSubStatus, 0);
                        ////getTypeOfRegistrtion(ddlTypeOfRegistrtion, 0);

                    }
                    break;
                case "fvPublicationDetail":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");


                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();


                    }
                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {

                        TextBox txt_u1_doc_idx = (TextBox)FvName.FindControl("txt_u1_doc_idx");
                        TextBox txt_publication_idx = (TextBox)FvName.FindControl("txt_publication_idx");
                        GridView gvDetailDeadline = (GridView)FvName.FindControl("gvDetailDeadline");

                        data_law data_m0_deadline_detail = new data_law();
                        tcm_record_deadline_detail m0_deadline_detail = new tcm_record_deadline_detail();
                        data_m0_deadline_detail.tcm_record_deadline_list = new tcm_record_deadline_detail[1];

                        m0_deadline_detail.u1_doc_idx = int.Parse(txt_u1_doc_idx.Text);
                        m0_deadline_detail.publication_idx = int.Parse(txt_publication_idx.Text);

                        data_m0_deadline_detail.tcm_record_deadline_list[0] = m0_deadline_detail;
                        
                        data_m0_deadline_detail = callServicePostLaw(_urlGetLawViewDeadlineRecordSheet, data_m0_deadline_detail);
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_deadline_detail));

                        ViewState["vs_ActioneDetail"] = data_m0_deadline_detail.tcm_record_actions_list;
                        ViewState["vs_DeadlineDetail"] = data_m0_deadline_detail.tcm_record_deadline_list;
                        setGridData(gvDetailDeadline, ViewState["vs_DeadlineDetail"]);


                    }
                    break;
                case "fvDependentDetail":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");

                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();
                    }
                    break;
                case "fvPrioritiesDetail":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        CleardataSetPrioritiesList();

                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");

                        DropDownList ddlStatus_priority = (DropDownList)FvName.FindControl("ddlStatus_priority");
                        DropDownList ddlCountry_priority = (DropDownList)FvName.FindControl("ddlCountry_priority");

                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();

                        getRecordSheetStatus(ddlStatus_priority, 0, "priority");
                        getRecordsheetCountry(ddlCountry_priority, 0);
                    }
                    break;
                case "fvClientDetail":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        CleardataSetClientList();

                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");
                        DropDownList ddlClientStatus = (DropDownList)FvName.FindControl("ddlClientStatus");

                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();

                        getRecordSheetStatus(ddlClientStatus, 0, "");
                    }
                    break;
                case "fvAgentDetail":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        CleardataSetAgentList();

                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");

                        DropDownList ddlAgentStatus = (DropDownList)FvName.FindControl("ddlAgentStatus");

                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();

                        getRecordSheetStatus(ddlAgentStatus, 0, "agent");
                    }
                    break;
                case "fvGoodsDetail":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        CleardataSetGoodsList();

                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");

                        DropDownList ddlLocal_language = (DropDownList)FvName.FindControl("ddlLocal_language");

                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();

                        getLocalLanguage(ddlLocal_language, 0);
                    }
                    break;
                case "fvAdditionalDetail":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");

                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();
                    }
                    break;
                case "fvTademarkProfile":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");

                        DropDownList ddlLocal_language_trademark = (DropDownList)FvName.FindControl("ddlLocal_language_trademark");
                        DropDownList ddlProfileOwnerLanguage = (DropDownList)FvName.FindControl("ddlProfileOwnerLanguage");
                        DropDownList ddlProfileClientLanguage = (DropDownList)FvName.FindControl("ddlProfileClientLanguage");

                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();

                        getLocalLanguage(ddlLocal_language_trademark, 0);
                        getLocalLanguage(ddlProfileOwnerLanguage, 0);
                        getLocalLanguage(ddlProfileClientLanguage, 0);
                    }
                    break;
                case "fvAssignment":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        ////CleardataSetPrioritiesList();

                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");

                        DropDownList ddlStatusAssignment = (DropDownList)FvName.FindControl("ddlStatusAssignment");
                        

                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();
                        
                       // getRecordSheetStatus(ddlStatusAssignment, 0);
                        
                    }
                    break;
                case "fvTypeofwork":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        RadioButtonList rdoTypeofwork = (RadioButtonList)FvName.FindControl("rdoTypeofwork");
                        
                        getRecordSheetTypeOfwork(rdoTypeofwork, 0);
                    }
                        
                    break;

            }
        }
    }
    #endregion Formview Databind

    #region SelectedIndexChanged
    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        switch (ddlName.ID)
        {

            case "ddlowner_idx":
                TextBox tbowner_address_en = (TextBox)fvRegistrationDetail.FindControl("tbowner_address_en");

                data_law data_m0_owner_detail = new data_law();
                tcm_m0_owner_detail m0_owner_detail = new tcm_m0_owner_detail();
                data_m0_owner_detail.tcm_m0_owner_list = new tcm_m0_owner_detail[1];

                m0_owner_detail.owner_idx = int.Parse(ddlName.SelectedValue);

                data_m0_owner_detail.tcm_m0_owner_list[0] = m0_owner_detail;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

                //
                data_m0_owner_detail = callServicePostLaw(_urlGetLawM0Owner, data_m0_owner_detail);

                if (int.Parse(ddlName.SelectedValue) != 0)
                {
                    tbowner_address_en.Text = data_m0_owner_detail.tcm_m0_owner_list[0].owner_address_en.ToString();
                }
                else
                {
                    tbowner_address_en.Text = string.Empty;
                }

                break;
            case "ddlTrademarkStatus":

                DropDownList ddlTrademarkStatus = (DropDownList)fvRegistrationDetail.FindControl("ddlTrademarkStatus");
                DropDownList ddlTrademarkSubStatus = (DropDownList)fvRegistrationDetail.FindControl("ddlTrademarkSubStatus");

                getTrademarkSubStatus(ddlTrademarkSubStatus, int.Parse(ddlTrademarkStatus.SelectedValue), int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvRegistrationDetail");
                
                break;
            case "ddlRenewalStatus":

                DropDownList ddlRenewalStatus = (DropDownList)fvRegistrationDetail.FindControl("ddlRenewalStatus");
                DropDownList ddlRenewalSubStatus = (DropDownList)fvRegistrationDetail.FindControl("ddlRenewalSubStatus");

                getTrademarkSubStatus(ddlRenewalSubStatus, int.Parse(ddlRenewalStatus.SelectedValue), int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "Renewal");

                break;
            case "ddlRenewalStatus_edit":

                GridView gvDetailRenewal = (GridView)fvRegistrationDetail.FindControl("gvDetailRenewal");

                foreach (GridViewRow row in gvDetailRenewal.Rows)
                {

                    if (row.RowState.ToString().Contains("Edit"))
                    {

                        DropDownList ddlRenewalStatus_edit = (DropDownList)row.FindControl("ddlRenewalStatus_edit");
                        DropDownList ddlRenewalSubStatus_edit = (DropDownList)row.FindControl("ddlRenewalSubStatus_edit");
                        getTrademarkSubStatus(ddlRenewalSubStatus_edit, int.Parse(ddlRenewalStatus_edit.SelectedValue), int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "Renewal");

                    }
                }

                break;
            case "ddlStatusAssignment":


                DropDownList ddlStatusAssignment = (DropDownList)fvAssignment.FindControl("ddlStatusAssignment");
                DropDownList ddlSubStatusAssignment = (DropDownList)fvAssignment.FindControl("ddlSubStatusAssignment");

                getTrademarkSubStatus(ddlSubStatusAssignment, int.Parse(ddlStatusAssignment.SelectedValue), int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvAssignment");

                break;
            case "ddlStatusAssignment_edit":

                GridView gvDetailAssignment = (GridView)fvAssignment.FindControl("gvDetailAssignment");

                foreach (GridViewRow row in gvDetailAssignment.Rows)
                {

                    if (row.RowState.ToString().Contains("Edit"))
                    {

                        DropDownList ddlStatusAssignment_edit = (DropDownList)row.FindControl("ddlStatusAssignment_edit");
                        DropDownList ddlSubStatusAssignment_edit = (DropDownList)row.FindControl("ddlSubStatusAssignment_edit");
                        getTrademarkSubStatus(ddlSubStatusAssignment_edit, int.Parse(ddlStatusAssignment_edit.SelectedValue), int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvAssignment");

                    }
                }


                break;
            case "ddlStatusChangeofName":

                DropDownList ddlStatusChangeofName = (DropDownList)fvChangeofName.FindControl("ddlStatusChangeofName");
                DropDownList ddlSubStatusChangeofName = (DropDownList)fvChangeofName.FindControl("ddlSubStatusChangeofName");

                getTrademarkSubStatus(ddlSubStatusChangeofName, int.Parse(ddlStatusChangeofName.SelectedValue), int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvChangeofName");

                break;
            case "ddlStatusChangeofName_edit":
                GridView gvDetailChangeofName = (GridView)fvChangeofName.FindControl("gvDetailChangeofName");
                foreach (GridViewRow row in gvDetailChangeofName.Rows)
                {

                    if (row.RowState.ToString().Contains("Edit"))
                    {
                        DropDownList ddlStatusChangeofName_edit = (DropDownList)row.FindControl("ddlStatusChangeofName_edit");
                        DropDownList ddlSubStatusChangeofName_edit = (DropDownList)row.FindControl("ddlSubStatusChangeofName_edit");

                        getTrademarkSubStatus(ddlSubStatusChangeofName_edit, int.Parse(ddlStatusChangeofName_edit.SelectedValue), int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvChangeofName");
                    }
                }
                    
                break;
            case "ddlSearchTrademark":

                DropDownList ddlSearchTrademark = (DropDownList)fvTademarkProfile.FindControl("ddlSearchTrademark");
                TextBox tbName_trademark = (TextBox)fvTademarkProfile.FindControl("tbName_trademark");
                DropDownList ddlLocal_language_trademark = (DropDownList)fvTademarkProfile.FindControl("ddlLocal_language_trademark");

                TextBox tbProfileOwner = (TextBox)fvTademarkProfile.FindControl("tbProfileOwner");
                DropDownList ddlProfileOwnerLanguage = (DropDownList)fvTademarkProfile.FindControl("ddlProfileOwnerLanguage");

                TextBox tbProfileClient = (TextBox)fvTademarkProfile.FindControl("tbProfileClient");
                DropDownList ddlProfileClientLanguage = (DropDownList)fvTademarkProfile.FindControl("ddlProfileClientLanguage");

                TextBox tbTranslation = (TextBox)fvTademarkProfile.FindControl("tbTranslation");
                TextBox tbTransliteration = (TextBox)fvTademarkProfile.FindControl("tbTransliteration");


                data_law data_m0_trademarkprofile = new data_law();
                tcm_record_trademarkprofile_detail m0_trademarkprofile_detail = new tcm_record_trademarkprofile_detail();
                data_m0_trademarkprofile.tcm_record_trademarkprofile_list = new tcm_record_trademarkprofile_detail[1];

                m0_trademarkprofile_detail.trademark_profile_idx = int.Parse(ddlSearchTrademark.SelectedValue);
                m0_trademarkprofile_detail.condition = 1;

                data_m0_trademarkprofile.tcm_record_trademarkprofile_list[0] = m0_trademarkprofile_detail;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_trademarkprofile));
                data_m0_trademarkprofile = callServicePostLaw(_urlGetDDLTrademarksProfile, data_m0_trademarkprofile);
                if(data_m0_trademarkprofile.return_code == 0)
                {
                    tbName_trademark.Text = data_m0_trademarkprofile.tcm_record_trademarkprofile_list[0].Name.ToString();
                    ddlLocal_language_trademark.SelectedValue = data_m0_trademarkprofile.tcm_record_trademarkprofile_list[0].doc_language_idx.ToString();

                    tbProfileOwner.Text = data_m0_trademarkprofile.tcm_record_trademarkprofile_list[0].Profile_Owner.ToString();
                    ddlProfileOwnerLanguage.SelectedValue = data_m0_trademarkprofile.tcm_record_trademarkprofile_list[0].owner_language_idx.ToString();

                    tbProfileClient.Text = data_m0_trademarkprofile.tcm_record_trademarkprofile_list[0].Profile_Client.ToString();
                    ddlProfileClientLanguage.SelectedValue = data_m0_trademarkprofile.tcm_record_trademarkprofile_list[0].client_language_idx.ToString();

                    tbTranslation.Text = data_m0_trademarkprofile.tcm_record_trademarkprofile_list[0].Translation.ToString();
                    tbTransliteration.Text = data_m0_trademarkprofile.tcm_record_trademarkprofile_list[0].Transliteration.ToString();
                }
                else
                {
                    tbName_trademark.Text = string.Empty;
                    ddlLocal_language_trademark.ClearSelection();

                    tbProfileOwner.Text = string.Empty;
                    ddlProfileOwnerLanguage.ClearSelection();

                    tbProfileClient.Text = string.Empty;
                    ddlProfileClientLanguage.ClearSelection();

                    tbTranslation.Text = string.Empty;
                    tbTransliteration.Text = string.Empty;

                }

                //getddlTrademarkprofile(ddlSearchTrademark, int.Parse(ddlSearchTrademark.SelectedValue), 1);
                break;

        }
    }

    protected void rdoSelectedIndexChanged(object sender, EventArgs e)
    {
        RadioButtonList rdoName = (RadioButtonList)sender;
        switch (rdoName.ID)
        {
            case "rdoStepAssignment":

                RadioButtonList rdoStepAssignment = (RadioButtonList)fvAssignment.FindControl("rdoStepAssignment");
                TextBox txt_step_remark = (TextBox)fvAssignment.FindControl("txt_step_remark");
                if (rdoStepAssignment.SelectedValue == "2")
                {
                    txt_step_remark.Visible = true;
                }
                else
                {
                    txt_step_remark.Visible = false;
                }

                break;
        }

    }

    protected void chkSelectedIndexChanged(object sender, EventArgs e)
    {
        CheckBox chkName = (CheckBox)sender;
        switch (chkName.ID)
        {
            case "chkAddRenewalDate":
                
                UpdatePanel Panel_AddRenewalDate = (UpdatePanel)fvRegistrationDetail.FindControl("Panel_AddRenewalDate");
                
                TextBox tbtype_idx_viewdetail = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");
                DropDownList ddlRenewalStatus = (DropDownList)Panel_AddRenewalDate.FindControl("ddlRenewalStatus");
                DropDownList ddlRenewalSubStatus = (DropDownList)Panel_AddRenewalDate.FindControl("ddlRenewalSubStatus");

                RadioButtonList rdoRenewal = (RadioButtonList)Panel_AddRenewalDate.FindControl("rdoRenewal");
                TextBox tb_Next_Renewal_Due = (TextBox)fvRegistrationDetail.FindControl("tb_Next_Renewal_Due");

                ViewState["vs_detailrecordsheet_type_idx"] = tbtype_idx_viewdetail.Text;
                //int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString())

                Panel_AddRenewalDate.Visible = false;
                rdoRenewal.ClearSelection();
                tb_Next_Renewal_Due.Text = string.Empty;
                if (chkName.Checked == true)
                {
                    
                    Panel_AddRenewalDate.Visible = true;
                    getTrademarkStatus(ddlRenewalStatus, 0, int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "Renewal");
                    getTrademarkSubStatus(ddlRenewalSubStatus, int.Parse(ddlRenewalStatus.SelectedValue), int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "Renewal");
                    

                }

                break;
            case "chkAddDeadline":

                UpdatePanel Panel_AddDeadline = (UpdatePanel)fvPublicationDetail.FindControl("Panel_AddDeadline");
                CheckBoxList chkActionList = (CheckBoxList)Panel_AddDeadline.FindControl("chkActionList");

                Panel_AddDeadline.Visible = false;
                if (chkName.Checked == true)
                {

                    Panel_AddDeadline.Visible = true;
                    getM0ActionDetailList(chkActionList, 0);

                }
                break;
            case "chkAddAssignment":
                TextBox tbtype_idx_viewdetail_ = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                UpdatePanel Panel_AddAssignment = (UpdatePanel)fvAssignment.FindControl("Panel_AddAssignment");
                DropDownList ddlStatusAssignment = (DropDownList)Panel_AddAssignment.FindControl("ddlStatusAssignment");
                DropDownList ddlSubStatusAssignment = (DropDownList)Panel_AddAssignment.FindControl("ddlSubStatusAssignment");

                //CheckBoxList chkAddAssignment = (CheckBoxList)Panel_AddAssignment.FindControl("chkAddAssignment");
                ViewState["vs_detailrecordsheet_type_idx"] = tbtype_idx_viewdetail_.Text;
                Panel_AddAssignment.Visible = false;
                if (chkName.Checked == true)
                {
                    Panel_AddAssignment.Visible = true;
                    getTrademarkStatus(ddlStatusAssignment, 0, int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvAssignment");
                    getTrademarkSubStatus(ddlSubStatusAssignment, int.Parse(ddlStatusAssignment.SelectedValue), int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvAssignment");
                }
                break;
            case "chkAddChangeofName":
                TextBox tbtype_idx_viewdetail_AddChangeofName = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                UpdatePanel Panel_AddChangeofName = (UpdatePanel)fvChangeofName.FindControl("Panel_AddChangeofName");
                DropDownList ddlStatusChangeofName = (DropDownList)Panel_AddChangeofName.FindControl("ddlStatusChangeofName");
                DropDownList ddlSubStatusChangeofName = (DropDownList)Panel_AddChangeofName.FindControl("ddlSubStatusChangeofName");

                ViewState["vs_detailrecordsheet_type_idx"] = tbtype_idx_viewdetail_AddChangeofName.Text;
                Panel_AddChangeofName.Visible = false;
                if (chkName.Checked == true)
                {
                    Panel_AddChangeofName.Visible = true;
                    getTrademarkStatus(ddlStatusChangeofName, 0, int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvChangeofName");
                    getTrademarkSubStatus(ddlSubStatusChangeofName, int.Parse(ddlStatusChangeofName.SelectedValue), int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvChangeofName");
                }
                break;
            case "chkAddPriorities":
                TextBox tbtype_idx_viewdetail_Priorities = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                UpdatePanel Panel_AddPriorities = (UpdatePanel)fvPrioritiesDetail.FindControl("Panel_AddPriorities");
                DropDownList ddlCountry_priority = (DropDownList)Panel_AddPriorities.FindControl("ddlCountry_priority");
                DropDownList ddlStatus_priority = (DropDownList)Panel_AddPriorities.FindControl("ddlStatus_priority");

                ViewState["vs_detailrecordsheet_type_idx"] = tbtype_idx_viewdetail_Priorities.Text;
                Panel_AddPriorities.Visible = false;
                if (chkName.Checked == true)
                {
                    Panel_AddPriorities.Visible = true;
                    getRecordsheetCountry(ddlCountry_priority, 0);
                    getRecordSheetStatus(ddlStatus_priority, 0, "priority");
                }
                break;
            case "chkAddClient":
               
                UpdatePanel Panel_AddClient = (UpdatePanel)fvClientDetail.FindControl("Panel_AddClient");
                DropDownList ddlClientStatus = (DropDownList)Panel_AddClient.FindControl("ddlClientStatus");

                Panel_AddClient.Visible = false;
                if (chkName.Checked == true)
                {

                    Panel_AddClient.Visible = true;
                    getRecordSheetStatus(ddlClientStatus, 0, "");

                }
                break;
            case "chkAddAgent":

                UpdatePanel Panel_AddAgent = (UpdatePanel)fvAgentDetail.FindControl("Panel_AddAgent");
                DropDownList ddlAgentStatus = (DropDownList)Panel_AddAgent.FindControl("ddlAgentStatus");

                Panel_AddAgent.Visible = false;
                if (chkName.Checked == true)
                {

                    Panel_AddAgent.Visible = true;
                    getRecordSheetStatus(ddlAgentStatus, 0, "agent");

                }
                break;
            case "chkAddGood":

                UpdatePanel Panel_AddGood = (UpdatePanel)fvGoodsDetail.FindControl("Panel_AddGood");
                DropDownList ddlLocal_language = (DropDownList)Panel_AddGood.FindControl("ddlLocal_language");

                Panel_AddGood.Visible = false;
                if (chkName.Checked == true)
                {

                    Panel_AddGood.Visible = true;
                    getLocalLanguage(ddlLocal_language, 0);
                    //getRecordSheetStatus(ddlAgentStatus, 0);

                }
                break;
            case "chkTrademark":

                UpdatePanel Panel_SearchTrademark = (UpdatePanel)fvTademarkProfile.FindControl("Panel_SearchTrademark");
                DropDownList ddlSearchTrademark = (DropDownList)Panel_SearchTrademark.FindControl("ddlSearchTrademark");

                Panel_SearchTrademark.Visible = false;
                if (chkName.Checked == true)
                {

                    Panel_SearchTrademark.Visible = true;
                    getddlTrademarkprofile(ddlSearchTrademark, 0, 0);

                }
                break;
        }

    }


    #endregion SelectedIndexChanged

    #region gridview
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {
            case "gvWaitApprove":

                //getDetailWaitApproveDocument(gvWaitApprove, 0);
                break;
            case "gvRecordSheet":
                setGridData(gvRecordSheet, ViewState["Vs_GvDetailRecordSheet"]);
                break;
          

        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
           
            case "gvFileDocViewDetail":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.FindControl("btnDL11");
                    HiddenField hidFile = (HiddenField)e.Row.FindControl("hidFile");
                    string LinkHost = string.Format("http://{0}", Request.Url.Host);

                    //btnDL11.NavigateUrl = LinkHost11_permwait + MapURL(hidFile.Value);    
                    string path = HttpContext.Current.Request.Url.AbsolutePath;
                    string[] result_path = path.Split(new string[] { "/" }, StringSplitOptions.None);

                    if (LinkHost.ToString() == "http://localhost")
                    {
                        //litDebug1.Text = LinkHost11_permwait;
                        btnDL11.NavigateUrl = LinkHost + "/" + result_path[1].ToString() + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }
                    else
                    {
                        btnDL11.NavigateUrl = LinkHost + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }


                }
                break;
            case "gvFileMemoLawOfficer":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.FindControl("btnDL11");
                    HiddenField hidFile = (HiddenField)e.Row.FindControl("hidFile");
                    string LinkHost = string.Format("http://{0}", Request.Url.Host);

                    //btnDL11.NavigateUrl = LinkHost11_permwait + MapURL(hidFile.Value);    
                    string path = HttpContext.Current.Request.Url.AbsolutePath;
                    string[] result_path = path.Split(new string[] { "/" }, StringSplitOptions.None);

                    if (LinkHost.ToString() == "http://localhost")
                    {
                        //litDebug1.Text = LinkHost11_permwait;
                        btnDL11.NavigateUrl = LinkHost + "/" + result_path[1].ToString() + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }
                    else
                    {
                        btnDL11.NavigateUrl = LinkHost + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }


                }
                break;
            case "gvFileMemoUser":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.FindControl("btnDL11");
                    HiddenField hidFile = (HiddenField)e.Row.FindControl("hidFile");
                    string LinkHost = string.Format("http://{0}", Request.Url.Host);

                    //btnDL11.NavigateUrl = LinkHost11_permwait + MapURL(hidFile.Value);    
                    string path = HttpContext.Current.Request.Url.AbsolutePath;
                    string[] result_path = path.Split(new string[] { "/" }, StringSplitOptions.None);

                    if (LinkHost.ToString() == "http://localhost")
                    {
                        //litDebug1.Text = LinkHost11_permwait;
                        btnDL11.NavigateUrl = LinkHost + "/" + result_path[1].ToString() + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }
                    else
                    {
                        btnDL11.NavigateUrl = LinkHost + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }
                }
                break;
            case "gvRecordSheet":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                    linkBtnTrigger(btnSaveTrademarkRecordSheet);
                    linkBtnTrigger(btnUpdateTrademarkRecordSheet);

                    Label lblnoidx_wait = (Label)e.Row.FindControl("lblnoidx_wait");
                    Label lblstaidx_wait = (Label)e.Row.FindControl("lblstaidx_wait");
                    LinkButton btnEditRecordSheet = (LinkButton)e.Row.FindControl("btnEditRecordSheet");
                    LinkButton btnSaveRecordSheet = (LinkButton)e.Row.FindControl("btnSaveRecordSheet");
                    LinkButton btnViewRecordSheet = (LinkButton)e.Row.FindControl("btnViewRecordSheet");

                    btnEditRecordSheet.Visible = false;
                    btnSaveRecordSheet.Visible = false;
                    btnViewRecordSheet.Visible = false;

                    switch (int.Parse(lblstaidx_wait.Text))
                    {
                        case 11: //save recordsheet
                            btnSaveRecordSheet.Visible = true;

                            break;
                        case 12:
                        case 18:
                            //btnEditRecordSheet.Visible = true;
                            btnViewRecordSheet.Visible = true;
                            break;
                    }
                }
                break;

            case "gvDeadlineInsert":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    GridView gvActionsDetail = (GridView)e.Row.FindControl("gvActionsDetail");
                    getLawM0Actions(gvActionsDetail, 0);

                }
                break;
            case "gvDetailDeadline":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    //GridView gvDetailDeadline = (GridView)fvPublicationDetail.FindControl("gvDetailDeadline");
                    //Label lbldeadline_idx = (Label)e.Row.FindControl("lbldeadline_idx");
                    Label lbl_condition = (Label)e.Row.FindControl("lbl_condition");
                    Label lbldeadline_idx = (Label)e.Row.FindControl("lbldeadline_idx");
                    GridView gvActionsDetail = (GridView)e.Row.FindControl("gvActionsDetail");

                    //litDebug.Text += lbldeadline_idx.Text;
                    // ViewState["vs_DeadlineDetail"]
                    tcm_record_actions_detail[] _templist_recordsheet_deadline = (tcm_record_actions_detail[])ViewState["vs_ActioneDetail"];

                    var _linq_detailrecord_deadline = (from dt in _templist_recordsheet_deadline
                                                       where dt.deadline_idx == int.Parse(lbldeadline_idx.Text)
                                                       select dt
                                                       ).ToList();


                    
                    setGridData(gvActionsDetail, _linq_detailrecord_deadline.ToList());
                 
                    


                }

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox tbtype_idx_viewdetail = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                    RadioButtonList rdoDeadline_edit = (RadioButtonList)e.Row.FindControl("rdoDeadline_edit");
                    Label lbl_status = (Label)e.Row.FindControl("lbl_status");

                    CheckBoxList chkActionList_edit = (CheckBoxList)e.Row.FindControl("chkActionList_edit");
                    Label lbl_action_detail_idx = (Label)e.Row.FindControl("lbl_action_detail_idx");

                    Label lbl_deadline_idx = (Label)e.Row.FindControl("lbl_deadline_idx");
                    GridView gvActionsDetailEdit = (GridView)e.Row.FindControl("gvActionsDetailEdit");

                    ViewState["vs_detailrecordsheet_type_idx"] = tbtype_idx_viewdetail.Text;

                    //getLocalLanguage(ddlLocal_language_edit, 0);
                    rdoDeadline_edit.SelectedValue = lbl_status.Text;

                    getM0ActionDetailList(chkActionList_edit, 0);


                    //litDebug.Text = lbl_action_detail_idx.Text;
                    string[] setTestChkOnly = lbl_action_detail_idx.Text.Split(',');
                    foreach (ListItem item in chkActionList_edit.Items)
                    {
                        foreach (string _i in setTestChkOnly)
                        {
                            if (item.Value == _i)
                            {
                                item.Selected = true;


                            }

                        }
                    }

                    //////
                    tcm_record_actions_detail[] _templist_recordsheet_deadline = (tcm_record_actions_detail[])ViewState["vs_ActioneDetail"];

                    var _linq_detailrecord_deadline = (from dt in _templist_recordsheet_deadline
                                                       where dt.deadline_idx == int.Parse(lbl_deadline_idx.Text)
                                                       select dt
                                                       ).ToList();


                    setGridData(gvActionsDetailEdit, _linq_detailrecord_deadline.ToList());



                }
                break;
            case "gvDetailRenewal":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox tbtype_idx_viewdetail = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                    DropDownList ddlRenewalStatus_edit = (DropDownList)e.Row.FindControl("ddlRenewalStatus_edit");
                    DropDownList ddlRenewalSubStatus_edit = (DropDownList)e.Row.FindControl("ddlRenewalSubStatus_edit");
                    RadioButtonList rdoRenewal_edit = (RadioButtonList)e.Row.FindControl("rdoRenewal_edit");

                    Label lbl_Selected_value = (Label)e.Row.FindControl("lbl_Selected_value");
                    Label lbl_Renewal_Status_edit = (Label)e.Row.FindControl("lbl_Renewal_Status_edit");
                    Label lbl_Renewal_SubStatus_edit = (Label)e.Row.FindControl("lbl_Renewal_SubStatus_edit");

                    ViewState["vs_detailrecordsheet_type_idx"] = tbtype_idx_viewdetail.Text;

                    getTrademarkStatus(ddlRenewalStatus_edit, 0, int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "Renewal");
                    ddlRenewalStatus_edit.SelectedValue = lbl_Renewal_Status_edit.Text;

                    getTrademarkSubStatus(ddlRenewalSubStatus_edit, int.Parse(ddlRenewalStatus_edit.SelectedValue), int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "Renewal");
                    ddlRenewalSubStatus_edit.SelectedValue = lbl_Renewal_SubStatus_edit.Text;

                    rdoRenewal_edit.SelectedValue = lbl_Selected_value.Text;


                }

                break;
            case "gvDetailAssignment":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox tbtype_idx_viewdetail = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                    DropDownList ddlStatusAssignment_edit = (DropDownList)e.Row.FindControl("ddlStatusAssignment_edit");
                    DropDownList ddlSubStatusAssignment_edit = (DropDownList)e.Row.FindControl("ddlSubStatusAssignment_edit");
                    RadioButtonList rdoStepAssignment_edit = (RadioButtonList)e.Row.FindControl("rdoStepAssignment_edit");
                    RadioButtonList rdoAssignment_edit = (RadioButtonList)e.Row.FindControl("rdoAssignment_edit");

                    Label lbl_step_idx = (Label)e.Row.FindControl("lbl_step_idx");
                    Label lbl_selected_idx = (Label)e.Row.FindControl("lbl_selected_idx");
                    Label lbl_staus = (Label)e.Row.FindControl("lbl_staus");
                    Label lbl_substatus = (Label)e.Row.FindControl("lbl_substatus");

                    ViewState["vs_detailrecordsheet_type_idx"] = tbtype_idx_viewdetail.Text;

                    getTrademarkStatus(ddlStatusAssignment_edit, 0, int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvAssignment");
                    ddlStatusAssignment_edit.SelectedValue = lbl_staus.Text;

                    getTrademarkSubStatus(ddlSubStatusAssignment_edit, int.Parse(ddlStatusAssignment_edit.SelectedValue), int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvAssignment");
                    ddlSubStatusAssignment_edit.SelectedValue = lbl_substatus.Text;

                    rdoStepAssignment_edit.SelectedValue = lbl_step_idx.Text;
                    rdoAssignment_edit.SelectedValue = lbl_selected_idx.Text;
                    
                    
                }

                break;
            case "gvDetailChangeofName":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox tbtype_idx_viewdetail = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                    RadioButtonList rdoChangeofName_edit = (RadioButtonList)e.Row.FindControl("rdoChangeofName_edit");
                    DropDownList ddlStatusChangeofName_edit = (DropDownList)e.Row.FindControl("ddlStatusChangeofName_edit");
                    DropDownList ddlSubStatusChangeofName_edit = (DropDownList)e.Row.FindControl("ddlSubStatusChangeofName_edit");

                    Label lbl_selected_idx = (Label)e.Row.FindControl("lbl_selected_idx");
                    Label lbl_status = (Label)e.Row.FindControl("lbl_status");
                    Label lbl_substatus = (Label)e.Row.FindControl("lbl_substatus");

                    ViewState["vs_detailrecordsheet_type_idx"] = tbtype_idx_viewdetail.Text;

                    rdoChangeofName_edit.SelectedValue = lbl_selected_idx.Text;

                    getTrademarkStatus(ddlStatusChangeofName_edit, 0, int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvChangeofName");
                    ddlStatusChangeofName_edit.SelectedValue = lbl_status.Text;

                    getTrademarkSubStatus(ddlSubStatusChangeofName_edit, int.Parse(ddlStatusChangeofName_edit.SelectedValue), int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvChangeofName");
                    ddlSubStatusChangeofName_edit.SelectedValue = lbl_substatus.Text;

                }

                break;
            case "gvDetailPriorities":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox tbtype_idx_viewdetail = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                    
                    DropDownList ddlCountry_priority_edit = (DropDownList)e.Row.FindControl("ddlCountry_priority_edit");
                    DropDownList ddlStatus_priority_edit = (DropDownList)e.Row.FindControl("ddlStatus_priority_edit");

                    
                    Label lbl_country_idx = (Label)e.Row.FindControl("lbl_country_idx");
                    Label lbl_status_idx = (Label)e.Row.FindControl("lbl_status_idx");

                    ViewState["vs_detailrecordsheet_type_idx"] = tbtype_idx_viewdetail.Text;

                   
                    getRecordsheetCountry(ddlCountry_priority_edit, 0);
                    ddlCountry_priority_edit.SelectedValue = lbl_country_idx.Text;
                    getRecordSheetStatus(ddlStatus_priority_edit, 0, "priority");
                    ddlStatus_priority_edit.SelectedValue = lbl_status_idx.Text;
                }

                break;
            case "gvDetailClient":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox tbtype_idx_viewdetail = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                    DropDownList ddlClientStatus_edit = (DropDownList)e.Row.FindControl("ddlClientStatus_edit");

                    Label lbl_status_idx = (Label)e.Row.FindControl("lbl_status_idx");

                    ViewState["vs_detailrecordsheet_type_idx"] = tbtype_idx_viewdetail.Text;

                    getRecordSheetStatus(ddlClientStatus_edit, 0, "");
                    ddlClientStatus_edit.SelectedValue = lbl_status_idx.Text;
                }

                break;
            case "gvDetailAgent":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox tbtype_idx_viewdetail = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                    DropDownList ddlAgentStatus_edit = (DropDownList)e.Row.FindControl("ddlAgentStatus_edit");
                    Label lbl_status_idx = (Label)e.Row.FindControl("lbl_status_idx");

                    ViewState["vs_detailrecordsheet_type_idx"] = tbtype_idx_viewdetail.Text;

                    getRecordSheetStatus(ddlAgentStatus_edit, 0, "agent");
                    ddlAgentStatus_edit.SelectedValue = lbl_status_idx.Text;
                }

                break;
            case "gvDetailGood":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox tbtype_idx_viewdetail = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                    DropDownList ddlLocal_language_edit = (DropDownList)e.Row.FindControl("ddlLocal_language_edit");
                    Label lbl_language_idx = (Label)e.Row.FindControl("lbl_language_idx");

                    ViewState["vs_detailrecordsheet_type_idx"] = tbtype_idx_viewdetail.Text;

                    getLocalLanguage(ddlLocal_language_edit, 0);
                    ddlLocal_language_edit.SelectedValue = lbl_language_idx.Text;
                }

                break;
            case "gvFileTrademarkDetail":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.FindControl("btnDL11");
                    HiddenField hidFile = (HiddenField)e.Row.FindControl("hidFile");
                    string LinkHost = string.Format("http://{0}", Request.Url.Host);

                    //btnDL11.NavigateUrl = LinkHost11_permwait + MapURL(hidFile.Value);    
                    string path = HttpContext.Current.Request.Url.AbsolutePath;
                    string[] result_path = path.Split(new string[] { "/" }, StringSplitOptions.None);

                    if (LinkHost.ToString() == "http://localhost")
                    {
                        //litDebug1.Text = LinkHost11_permwait;
                        btnDL11.NavigateUrl = LinkHost + "/" + result_path[1].ToString() + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }
                    else
                    {
                        btnDL11.NavigateUrl = LinkHost + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }


                }
                break;

        }

    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "cmdRemovePriorities":
                    GridView gvPrioritiesList = (GridView)fvPrioritiesDetail.FindControl("gvPrioritiesList");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsPrioritiesList"];
                    dsContacts.Tables["dsPrioritiesListTable"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(gvPrioritiesList, dsContacts.Tables["dsPrioritiesListTable"]);
                    if (dsContacts.Tables["dsPrioritiesListTable"].Rows.Count < 1)
                    {
                        gvPrioritiesList.Visible = false;
                    }
                    break;
                case "cmdRemoveClient":
                    GridView gvClientList = (GridView)fvClientDetail.FindControl("gvClientList");
                    GridViewRow rowselect_client = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_client = rowselect_client.RowIndex;
                    DataSet dsContacts_client = (DataSet)ViewState["vsClientList"];
                    dsContacts_client.Tables["dsClientListTable"].Rows[rowIndex_client].Delete();
                    dsContacts_client.AcceptChanges();
                    setGridData(gvClientList, dsContacts_client.Tables["dsClientListTable"]);
                    if (dsContacts_client.Tables["dsClientListTable"].Rows.Count < 1)
                    {
                        gvClientList.Visible = false;
                    }
                    break;
                case "cmdRemoveAgent":
                    GridView gvAgentList = (GridView)fvAgentDetail.FindControl("gvAgentList");
                    GridViewRow rowselect_Agent = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_Agent = rowselect_Agent.RowIndex;
                    DataSet dsContacts_Agent = (DataSet)ViewState["vsAgentList"];
                    dsContacts_Agent.Tables["dsAgentListTable"].Rows[rowIndex_Agent].Delete();
                    dsContacts_Agent.AcceptChanges();
                    setGridData(gvAgentList, dsContacts_Agent.Tables["dsAgentListTable"]);
                    if (dsContacts_Agent.Tables["dsAgentListTable"].Rows.Count < 1)
                    {
                        gvAgentList.Visible = false;
                    }
                    break;
                case "cmdRemoveGoods":
                    GridView gvGoodsList = (GridView)fvGoodsDetail.FindControl("gvGoodsList");
                    GridViewRow rowselect_Goods = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_Goods = rowselect_Goods.RowIndex;
                    DataSet dsContacts_Goods = (DataSet)ViewState["vsGoodsList"];
                    dsContacts_Goods.Tables["dsGoodsListTable"].Rows[rowIndex_Goods].Delete();
                    dsContacts_Goods.AcceptChanges();
                    setGridData(gvGoodsList, dsContacts_Goods.Tables["dsGoodsListTable"]);
                    if (dsContacts_Goods.Tables["dsGoodsListTable"].Rows.Count < 1)
                    {
                        gvGoodsList.Visible = false;
                    }
                    break;
                case "cmdRemoveRenewal":
                    GridView gvRenewalList = (GridView)fvRegistrationDetail.FindControl("gvRenewalList");
                    GridViewRow rowselect_Renewal = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_Renewal = rowselect_Renewal.RowIndex;
                    DataSet dsContacts_Renewal = (DataSet)ViewState["vsRenewalList"];
                    dsContacts_Renewal.Tables["dsRenewalListTable"].Rows[rowIndex_Renewal].Delete();
                    dsContacts_Renewal.AcceptChanges();
                    setGridData(gvRenewalList, dsContacts_Renewal.Tables["dsRenewalListTable"]);
                    if (dsContacts_Renewal.Tables["dsRenewalListTable"].Rows.Count < 1)
                    {
                        gvRenewalList.Visible = false;
                    }
                    break;
                case "cmdRemoveDeadline":

                    //litDebug.Text = "1";

                    GridView gvDeadlineInsert = (GridView)fvPublicationDetail.FindControl("gvDeadlineInsert");
                    GridViewRow rowselect_Deadline = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_Deadline = rowselect_Deadline.RowIndex;
                    DataSet dsContacts_Deadline = (DataSet)ViewState["vsDeadlineList"];
                    dsContacts_Deadline.Tables["dsDeadlineListTable"].Rows[rowIndex_Deadline].Delete();
                    dsContacts_Deadline.AcceptChanges();
                    setGridData(gvDeadlineInsert, dsContacts_Deadline.Tables["dsDeadlineListTable"]);
                    if (dsContacts_Deadline.Tables["dsDeadlineListTable"].Rows.Count < 1)
                    {
                        gvDeadlineInsert.Visible = false;
                    }
                    break;
                case "cmdRemoveAssignment":

                    GridView gvAssignmentList = (GridView)fvAssignment.FindControl("gvAssignmentList");
                    GridViewRow rowselect_Assignment = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_Assignment = rowselect_Assignment.RowIndex;
                    DataSet dsContacts_Assignment = (DataSet)ViewState["vsAssignmentList"];
                    dsContacts_Assignment.Tables["dsAssignmentListTable"].Rows[rowIndex_Assignment].Delete();
                    dsContacts_Assignment.AcceptChanges();
                    setGridData(gvAssignmentList, dsContacts_Assignment.Tables["dsAssignmentListTable"]);
                    if (dsContacts_Assignment.Tables["dsAssignmentListTable"].Rows.Count < 1)
                    {
                        gvAssignmentList.Visible = false;
                    }
                    break;
                case "cmdRemoveChangeofName":

                    GridView gvChangeofNameList = (GridView)fvChangeofName.FindControl("gvChangeofNameList");
                    GridViewRow rowselect_ChangeofName = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_ChangeofName = rowselect_ChangeofName.RowIndex;
                    DataSet dsContacts_ChangeofName = (DataSet)ViewState["vsChangeofNameList"];
                    dsContacts_ChangeofName.Tables["dsChangeofNameListTable"].Rows[rowIndex_ChangeofName].Delete();
                    dsContacts_ChangeofName.AcceptChanges();
                    setGridData(gvChangeofNameList, dsContacts_ChangeofName.Tables["dsChangeofNameListTable"]);
                    if (dsContacts_ChangeofName.Tables["dsChangeofNameListTable"].Rows.Count < 1)
                    {
                        gvChangeofNameList.Visible = false;
                    }
                    break;

            }
        }
    }

    protected void gvRowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvDetailRenewal":
             
                GridView gvDetailRenewal = (GridView)fvRegistrationDetail.FindControl("gvDetailRenewal");

                gvDetailRenewal.EditIndex = e.NewEditIndex;
                setGridData(gvDetailRenewal, ViewState["vs_renewal_recordsheet"]);

                break;
            case "gvDetailAssignment":
                
                GridView gvDetailAssignment = (GridView)fvAssignment.FindControl("gvDetailAssignment");

                gvDetailAssignment.EditIndex = e.NewEditIndex;
                setGridData(gvDetailAssignment, ViewState["vs_assignment_recordsheet"]);
                break;
            case "gvDetailChangeofName":
                
                GridView gvDetailChangeofName = (GridView)fvChangeofName.FindControl("gvDetailChangeofName");

                gvDetailChangeofName.EditIndex = e.NewEditIndex;
                setGridData(gvDetailChangeofName, ViewState["vs_changeofname_recordsheet"]);

                
                break;
            case "gvDetailPriorities":
                
                GridView gvDetailPriorities = (GridView)fvPrioritiesDetail.FindControl("gvDetailPriorities");

                gvDetailPriorities.EditIndex = e.NewEditIndex;
                setGridData(gvDetailPriorities, ViewState["vs_priorities_recordsheet"]);


                break;
            case "gvDetailClient":

                GridView gvDetailClient = (GridView)fvClientDetail.FindControl("gvDetailClient");

                gvDetailClient.EditIndex = e.NewEditIndex;
                setGridData(gvDetailClient, ViewState["vs_client_recordsheet"]);
               

                break;
            case "gvDetailAgent":

                GridView gvDetailAgent = (GridView)fvAgentDetail.FindControl("gvDetailAgent");

                gvDetailAgent.EditIndex = e.NewEditIndex;
                setGridData(gvDetailAgent, ViewState["vs_agent_recordsheet"]);


                break;
            case "gvDetailGood":

                GridView gvDetailGood = (GridView)fvGoodsDetail.FindControl("gvDetailGood");

                gvDetailGood.EditIndex = e.NewEditIndex;
                setGridData(gvDetailGood, ViewState["vs_good_recordsheet"]);

                
                break;
            case "gvDetailDeadline":

                GridView gvDetailDeadline = (GridView)fvPublicationDetail.FindControl("gvDetailDeadline");

                gvDetailDeadline.EditIndex = e.NewEditIndex;
                setGridData(gvDetailDeadline, ViewState["vs_DeadlineDetail"]);


                break;
           

        }
    }

    protected void gvRowUpdating(object sender, GridViewUpdateEventArgs e)
    {

        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;

        HiddenField hfU1IDX = (HiddenField)fvRegistrationDetail.FindControl("hfU1IDX");
        TextBox tbtype_idx_viewdetail = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

        switch (GvName.ID)
        {
            case "gvDetailRenewal":

                GridView gvDetailRenewal = (GridView)fvRegistrationDetail.FindControl("gvDetailRenewal");
                Label lblrenewal_idx = (Label)gvDetailRenewal.Rows[e.RowIndex].FindControl("lblrenewal_idx");
                TextBox tb_Next_Renewal_Due_edit = (TextBox)gvDetailRenewal.Rows[e.RowIndex].FindControl("tb_Next_Renewal_Due_edit");
                RadioButtonList rdoRenewal_edit = (RadioButtonList)gvDetailRenewal.Rows[e.RowIndex].FindControl("rdoRenewal_edit");
                DropDownList ddlRenewalStatus_edit = (DropDownList)gvDetailRenewal.Rows[e.RowIndex].FindControl("ddlRenewalStatus_edit");
                DropDownList ddlRenewalSubStatus_edit = (DropDownList)gvDetailRenewal.Rows[e.RowIndex].FindControl("ddlRenewalSubStatus_edit");

                gvDetailRenewal.EditIndex = -1;

                data_law data_renewal_detail_edit = new data_law();
                tcm_record_renewal_detail m0_renewal_detail_edit = new tcm_record_renewal_detail();
                data_renewal_detail_edit.tcm_record_renewal_list = new tcm_record_renewal_detail[1];

                m0_renewal_detail_edit.renewal_idx = int.Parse(lblrenewal_idx.Text);
                m0_renewal_detail_edit.NextRenewalDue_date = tb_Next_Renewal_Due_edit.Text;
                m0_renewal_detail_edit.cemp_idx = _emp_idx;
                m0_renewal_detail_edit.Selected_value = int.Parse(rdoRenewal_edit.SelectedValue);
                m0_renewal_detail_edit.Selected_name = rdoRenewal_edit.SelectedItem.Text;
                m0_renewal_detail_edit.Renewal_Status = int.Parse(ddlRenewalStatus_edit.SelectedValue);
                m0_renewal_detail_edit.Renewal_SubStatus = int.Parse(ddlRenewalSubStatus_edit.SelectedValue);

                data_renewal_detail_edit.tcm_record_renewal_list[0] = m0_renewal_detail_edit;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_renewal_detail_edit));
                data_renewal_detail_edit = callServicePostLaw(_urlSetUpdateLawRenewal, data_renewal_detail_edit);

                getViewRecordsheet(int.Parse(hfU1IDX.Value), int.Parse(tbtype_idx_viewdetail.Text));

                setGridData(gvDetailRenewal, ViewState["vs_renewal_recordsheet"]);

                break;
            case "gvDetailAssignment":

                GridView gvDetailAssignment = (GridView)fvAssignment.FindControl("gvDetailAssignment");

                Label lbl_assignment_idx = (Label)gvDetailAssignment.Rows[e.RowIndex].FindControl("lbl_assignment_idx");
                RadioButtonList rdoStepAssignment_edit = (RadioButtonList)gvDetailAssignment.Rows[e.RowIndex].FindControl("rdoStepAssignment_edit");
                RadioButtonList rdoAssignment_edit = (RadioButtonList)gvDetailAssignment.Rows[e.RowIndex].FindControl("rdoAssignment_edit");
                DropDownList ddlStatusAssignment_edit = (DropDownList)gvDetailAssignment.Rows[e.RowIndex].FindControl("ddlStatusAssignment_edit");
                DropDownList ddlSubStatusAssignment_edit = (DropDownList)gvDetailAssignment.Rows[e.RowIndex].FindControl("ddlSubStatusAssignment_edit");

                gvDetailAssignment.EditIndex = -1;

                data_law data_assignment_detail_edit = new data_law();
                tcm_record_assignment_detail m0_assignment_edit = new tcm_record_assignment_detail();
                data_assignment_detail_edit.tcm_record_assignment_list = new tcm_record_assignment_detail[1];

                m0_assignment_edit.cemp_idx = _emp_idx;
                m0_assignment_edit.assignment_idx = int.Parse(lbl_assignment_idx.Text);
                m0_assignment_edit.step_idx = int.Parse(rdoStepAssignment_edit.SelectedValue);
                m0_assignment_edit.step_name = rdoStepAssignment_edit.SelectedItem.Text;
                m0_assignment_edit.selected_idx = int.Parse(rdoAssignment_edit.SelectedValue);
                m0_assignment_edit.selected_name = rdoAssignment_edit.SelectedItem.Text;
                m0_assignment_edit.status = int.Parse(ddlStatusAssignment_edit.SelectedValue);
                m0_assignment_edit.substatus = int.Parse(ddlSubStatusAssignment_edit.SelectedValue);

                data_assignment_detail_edit.tcm_record_assignment_list[0] = m0_assignment_edit;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_assignment_detail_edit));
                data_assignment_detail_edit = callServicePostLaw(_urlSetUpdateLawAssignment, data_assignment_detail_edit);

                getViewRecordsheet(int.Parse(hfU1IDX.Value), int.Parse(tbtype_idx_viewdetail.Text));

                setGridData(gvDetailAssignment, ViewState["vs_assignment_recordsheet"]);
                

                break;
            case "gvDetailChangeofName":

                GridView gvDetailChangeofName = (GridView)fvChangeofName.FindControl("gvDetailChangeofName");

                Label lblchangeofname_idx = (Label)gvDetailChangeofName.Rows[e.RowIndex].FindControl("lblchangeofname_idx");
                RadioButtonList rdoChangeofName_edit = (RadioButtonList)gvDetailChangeofName.Rows[e.RowIndex].FindControl("rdoChangeofName_edit");
               
                DropDownList ddlStatusChangeofName_edit = (DropDownList)gvDetailChangeofName.Rows[e.RowIndex].FindControl("ddlStatusChangeofName_edit");
                DropDownList ddlSubStatusChangeofName_edit = (DropDownList)gvDetailChangeofName.Rows[e.RowIndex].FindControl("ddlSubStatusChangeofName_edit");

                gvDetailChangeofName.EditIndex = -1;

                data_law data_changeofname_detail_edit = new data_law();
                tcm_record_changeofname_detail m0_changeofname_edit = new tcm_record_changeofname_detail();
                data_changeofname_detail_edit.tcm_record_changeofname_list = new tcm_record_changeofname_detail[1];

                m0_changeofname_edit.cemp_idx = _emp_idx;
                m0_changeofname_edit.changeofname_idx = int.Parse(lblchangeofname_idx.Text);
               
                m0_changeofname_edit.selected_idx = int.Parse(rdoChangeofName_edit.SelectedValue);
                m0_changeofname_edit.selected_name = rdoChangeofName_edit.SelectedItem.Text;
                m0_changeofname_edit.status = int.Parse(ddlStatusChangeofName_edit.SelectedValue);
                m0_changeofname_edit.substatus = int.Parse(ddlSubStatusChangeofName_edit.SelectedValue);

                data_changeofname_detail_edit.tcm_record_changeofname_list[0] = m0_changeofname_edit;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_changeofname_detail_edit));
                data_changeofname_detail_edit = callServicePostLaw(_urlSetUpdateLawChangeofname, data_changeofname_detail_edit);

                getViewRecordsheet(int.Parse(hfU1IDX.Value), int.Parse(tbtype_idx_viewdetail.Text));

                setGridData(gvDetailChangeofName, ViewState["vs_changeofname_recordsheet"]);


                break;
            case "gvDetailPriorities":

                GridView gvDetailPriorities = (GridView)fvPrioritiesDetail.FindControl("gvDetailPriorities");

                Label lbltypeofpriority_idx = (Label)gvDetailPriorities.Rows[e.RowIndex].FindControl("lbltypeofpriority_idx");
                TextBox tbType_of_priority_edit = (TextBox)gvDetailPriorities.Rows[e.RowIndex].FindControl("tbType_of_priority_edit");
                TextBox tb_Priority_Date_edit = (TextBox)gvDetailPriorities.Rows[e.RowIndex].FindControl("tb_Priority_Date_edit");
                TextBox tb_Priority_No_edit = (TextBox)gvDetailPriorities.Rows[e.RowIndex].FindControl("tb_Priority_No_edit");

                DropDownList ddlCountry_priority_edit = (DropDownList)gvDetailPriorities.Rows[e.RowIndex].FindControl("ddlCountry_priority_edit");
                DropDownList ddlStatus_priority_edit = (DropDownList)gvDetailPriorities.Rows[e.RowIndex].FindControl("ddlStatus_priority_edit");

                gvDetailPriorities.EditIndex = -1;

                data_law data_priorities_detail_edit = new data_law();
                tcm_record_priorities_detail m0_priorities_edit = new tcm_record_priorities_detail();
                data_priorities_detail_edit.tcm_record_priorities_list = new tcm_record_priorities_detail[1];

                m0_priorities_edit.cemp_idx = _emp_idx;
                m0_priorities_edit.typeofpriority_idx = int.Parse(lbltypeofpriority_idx.Text);
                m0_priorities_edit.Type_of_priority = tbType_of_priority_edit.Text;
                m0_priorities_edit.Priority_Date = tb_Priority_Date_edit.Text;
                m0_priorities_edit.Priority_No = tb_Priority_No_edit.Text;
                m0_priorities_edit.country_idx = int.Parse(ddlCountry_priority_edit.SelectedValue);
                m0_priorities_edit.status_idx = int.Parse(ddlStatus_priority_edit.SelectedValue);

                data_priorities_detail_edit.tcm_record_priorities_list[0] = m0_priorities_edit;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_changeofname_detail_edit));
                data_priorities_detail_edit = callServicePostLaw(_urlSetUpdateLawPriorities, data_priorities_detail_edit);

                getViewRecordsheet(int.Parse(hfU1IDX.Value), int.Parse(tbtype_idx_viewdetail.Text));

                setGridData(gvDetailPriorities, ViewState["vs_priorities_recordsheet"]);

                
                break;
            case "gvDetailClient":

                GridView gvDetailClient = (GridView)fvClientDetail.FindControl("gvDetailClient");

                Label lblclient_idx = (Label)gvDetailClient.Rows[e.RowIndex].FindControl("lblclient_idx");
                TextBox tbClientDetail_edit = (TextBox)gvDetailClient.Rows[e.RowIndex].FindControl("tbClientDetail_edit");
                TextBox tbClientContact_edit = (TextBox)gvDetailClient.Rows[e.RowIndex].FindControl("tbClientContact_edit");
                TextBox tbClientReference_edit = (TextBox)gvDetailClient.Rows[e.RowIndex].FindControl("tbClientReference_edit");

                DropDownList ddlClientStatus_edit = (DropDownList)gvDetailClient.Rows[e.RowIndex].FindControl("ddlClientStatus_edit");

                gvDetailClient.EditIndex = -1;

                data_law data_client_detail_edit = new data_law();
                tcm_record_client_detail m0_client_edit = new tcm_record_client_detail();
                data_client_detail_edit.tcm_record_client_list = new tcm_record_client_detail[1];

                m0_client_edit.cemp_idx = _emp_idx;
                m0_client_edit.client_idx = int.Parse(lblclient_idx.Text);
                m0_client_edit.Client_Detail = tbClientDetail_edit.Text;
                m0_client_edit.Client_Contact = tbClientContact_edit.Text;
                m0_client_edit.Client_Reference = tbClientReference_edit.Text;
                
                m0_client_edit.status_idx = int.Parse(ddlClientStatus_edit.SelectedValue);

                data_client_detail_edit.tcm_record_client_list[0] = m0_client_edit;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_changeofname_detail_edit));
                data_client_detail_edit = callServicePostLaw(_urlSetUpdateLawClient, data_client_detail_edit);

                getViewRecordsheet(int.Parse(hfU1IDX.Value), int.Parse(tbtype_idx_viewdetail.Text));

                setGridData(gvDetailClient, ViewState["vs_client_recordsheet"]);

                
                break;
            case "gvDetailAgent":

                GridView gvDetailAgent = (GridView)fvAgentDetail.FindControl("gvDetailAgent");

                Label lblagent_idx = (Label)gvDetailAgent.Rows[e.RowIndex].FindControl("lblagent_idx");
                TextBox tbAgentDetail_edit = (TextBox)gvDetailAgent.Rows[e.RowIndex].FindControl("tbAgentDetail_edit");
                TextBox tbAgentContact_edit = (TextBox)gvDetailAgent.Rows[e.RowIndex].FindControl("tbAgentContact_edit");
                TextBox tbAgentReference_edit = (TextBox)gvDetailAgent.Rows[e.RowIndex].FindControl("tbAgentReference_edit");

                DropDownList ddlAgentStatus_edit = (DropDownList)gvDetailAgent.Rows[e.RowIndex].FindControl("ddlAgentStatus_edit");

                gvDetailAgent.EditIndex = -1;

                data_law data_agent_detail_edit = new data_law();
                tcm_record_agent_detail m0_agent_edit = new tcm_record_agent_detail();
                data_agent_detail_edit.tcm_record_agent_list = new tcm_record_agent_detail[1];

                m0_agent_edit.cemp_idx = _emp_idx;
                m0_agent_edit.agent_idx = int.Parse(lblagent_idx.Text);
                m0_agent_edit.Agent_Detail = tbAgentDetail_edit.Text;
                m0_agent_edit.Agent_Contact = tbAgentContact_edit.Text;
                m0_agent_edit.Agent_Reference = tbAgentReference_edit.Text;

                m0_agent_edit.status_idx = int.Parse(ddlAgentStatus_edit.SelectedValue);

                data_agent_detail_edit.tcm_record_agent_list[0] = m0_agent_edit;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_changeofname_detail_edit));
                data_agent_detail_edit = callServicePostLaw(_urlSetUpdateLawAgent, data_agent_detail_edit);

                getViewRecordsheet(int.Parse(hfU1IDX.Value), int.Parse(tbtype_idx_viewdetail.Text));

                setGridData(gvDetailAgent, ViewState["vs_agent_recordsheet"]);

                
                break;
            case "gvDetailGood":

                GridView gvDetailGood = (GridView)fvGoodsDetail.FindControl("gvDetailGood");

                Label lblgoods_idx = (Label)gvDetailGood.Rows[e.RowIndex].FindControl("lblgoods_idx");
                TextBox tbGoods_IntClass_edit = (TextBox)gvDetailGood.Rows[e.RowIndex].FindControl("tbGoods_IntClass_edit");
                TextBox tbGoodsDetail_edit = (TextBox)gvDetailGood.Rows[e.RowIndex].FindControl("tbGoodsDetail_edit");
               
                DropDownList ddlLocal_language_edit = (DropDownList)gvDetailGood.Rows[e.RowIndex].FindControl("ddlLocal_language_edit");

                gvDetailGood.EditIndex = -1;

                data_law data_goods_detail_edit = new data_law();
                tcm_record_goods_detail m0_goods_edit = new tcm_record_goods_detail();
                data_goods_detail_edit.tcm_record_goods_list = new tcm_record_goods_detail[1];

                m0_goods_edit.cemp_idx = _emp_idx;
                m0_goods_edit.goods_idx = int.Parse(lblgoods_idx.Text);
                m0_goods_edit.IntClass = tbGoods_IntClass_edit.Text;
                m0_goods_edit.Goods_Detail = tbGoodsDetail_edit.Text;
                m0_goods_edit.language_idx = int.Parse(ddlLocal_language_edit.SelectedValue);

                data_goods_detail_edit.tcm_record_goods_list[0] = m0_goods_edit;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_changeofname_detail_edit));
                data_goods_detail_edit = callServicePostLaw(_urlSetUpdateLawGoods, data_goods_detail_edit);

                getViewRecordsheet(int.Parse(hfU1IDX.Value), int.Parse(tbtype_idx_viewdetail.Text));

                setGridData(gvDetailGood, ViewState["vs_good_recordsheet"]);
                
                break;
            case "gvDetailDeadline":

                TextBox txt_publication_idx = (TextBox)fvPublicationDetail.FindControl("txt_publication_idx");
                GridView gvDetailDeadline = (GridView)fvPublicationDetail.FindControl("gvDetailDeadline");
                Label lbl_condition = (Label)gvDetailDeadline.FindControl("lbl_condition");

                Label lbl_u1_doc_idx = (Label)gvDetailDeadline.Rows[e.RowIndex].FindControl("lbl_u1_doc_idx");
                Label lbl_deadline_idx = (Label)gvDetailDeadline.Rows[e.RowIndex].FindControl("lbl_deadline_idx");
                

                TextBox tb_Office_Action_Date_edit = (TextBox)gvDetailDeadline.Rows[e.RowIndex].FindControl("tb_Office_Action_Date_edit");
                RadioButtonList rdoDeadline_edit = (RadioButtonList)gvDetailDeadline.Rows[e.RowIndex].FindControl("rdoDeadline_edit");
                CheckBoxList chkActionList_edit = (CheckBoxList)gvDetailDeadline.Rows[e.RowIndex].FindControl("chkActionList_edit");

                GridView gvActionsDetailEdit = (GridView)gvDetailDeadline.Rows[e.RowIndex].FindControl("gvActionsDetailEdit");

                gvDetailDeadline.EditIndex = -1;

                //check box selected
                int count_edit = 0;
                string Action_detail = "";
                string Action_detail_idx = "";

                List<String> AddoingList_edit = new List<string>();
                foreach (ListItem chkList_edit in chkActionList_edit.Items)
                {
                    if (chkList_edit.Selected)
                    {
                        
                        Action_detail += chkList_edit.Text + ',' + "<br>";
                        Action_detail_idx += chkList_edit.Value + ',';

                        count_edit = count_edit + 1;
                        //litDebug.Text = test_detail;
                        //litDebug1.Text = test_detail_idx;
                        count_edit++;
                    }
                }

               // litDebug.Text = Action_detail_idx.ToString();
                data_law data_deadline_detail_edit = new data_law();
                tcm_record_deadline_detail m0_deadline_edit = new tcm_record_deadline_detail();
                data_deadline_detail_edit.tcm_record_deadline_list = new tcm_record_deadline_detail[1];

                m0_deadline_edit.cemp_idx = _emp_idx;
                m0_deadline_edit.deadline_idx = int.Parse(lbl_deadline_idx.Text);
                m0_deadline_edit.deadline_date = tb_Office_Action_Date_edit.Text;
                m0_deadline_edit.action_detail_idx_value = Action_detail_idx.ToString();
                m0_deadline_edit.deadline_staus = int.Parse(rdoDeadline_edit.SelectedValue);
                m0_deadline_edit.deadline_stausname = rdoDeadline_edit.SelectedItem.Text;

                //update gv actionlist detail edit
                var _record_actions_detail_edit = new tcm_record_actions_detail[gvActionsDetailEdit.Rows.Count];
                //data_u1insert_recordsheet.tcm_record_actions_list = new tcm_record_actions_detail[1];
                int sumcheck_record = 0;
                int count_record = 0;
                foreach (GridViewRow gvrow_edit in gvActionsDetailEdit.Rows)
                {

                    Label lbl_actiondetail_idx_edit = (Label)gvrow_edit.FindControl("lbl_actiondetail_idx_edit");
                    TextBox tbActionsDetail_edit = (TextBox)gvrow_edit.FindControl("tbActionsDetail_edit");
                    TextBox tbOwnerDetail_edit = (TextBox)gvrow_edit.FindControl("tbOwnerDetail_edit");
                    Label lbl_action_idx_edit = (Label)gvrow_edit.FindControl("lbl_action_idx_edit");

                    _record_actions_detail_edit[count_record] = new tcm_record_actions_detail();
                    _record_actions_detail_edit[count_record].cemp_idx = _emp_idx;
                    _record_actions_detail_edit[count_record].actiondetail_idx = int.Parse(lbl_actiondetail_idx_edit.Text);
                    _record_actions_detail_edit[count_record].Actions_Detail = tbActionsDetail_edit.Text;
                    _record_actions_detail_edit[count_record].Owner_Detail = tbOwnerDetail_edit.Text;

                    sumcheck_record = sumcheck_record + 1;

                    count_record++;

                }

                data_deadline_detail_edit.tcm_record_deadline_list[0] = m0_deadline_edit;
                data_deadline_detail_edit.tcm_record_actions_list = _record_actions_detail_edit;

                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_deadline_detail_edit));

                data_deadline_detail_edit = callServicePostLaw(_urlSetUpdateLawDeadline, data_deadline_detail_edit);

                //getViewRecordsheet(int.Parse(hfU1IDX.Value), int.Parse(tbtype_idx_viewdetail.Text));
                getViewRecordsheetPublication(int.Parse(hfU1IDX.Value), int.Parse(txt_publication_idx.Text));

                //ViewState["vs_ActioneDetail"] = data_deadline_detail_edit.tcm_record_actions_list;
                ////lbl_condition.Text = "1";
                setGridData(gvDetailDeadline, ViewState["vs_DeadlineDetail"]);

                break;

        }
    }

    protected void gvRowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;
        //Label5.Text = GvName.ID;
        switch (GvName.ID)
        {
            case "gvDetailRenewal":
                GridView gvDetailRenewal = (GridView)fvRegistrationDetail.FindControl("gvDetailRenewal");
                gvDetailRenewal.EditIndex = -1;
                setGridData(gvDetailRenewal, ViewState["vs_renewal_recordsheet"]);

               
                break;
            case "gvDetailAssignment":
                GridView gvDetailAssignment = (GridView)fvAssignment.FindControl("gvDetailAssignment");
                gvDetailAssignment.EditIndex = -1;
                setGridData(gvDetailAssignment, ViewState["vs_assignment_recordsheet"]);


                break;
            case "gvDetailChangeofName":
                GridView gvDetailChangeofName = (GridView)fvChangeofName.FindControl("gvDetailChangeofName");
                gvDetailChangeofName.EditIndex = -1;
                setGridData(gvDetailChangeofName, ViewState["vs_changeofname_recordsheet"]);

                break;
            case "gvDetailPriorities":

                GridView gvDetailPriorities = (GridView)fvPrioritiesDetail.FindControl("gvDetailPriorities");
                gvDetailPriorities.EditIndex = -1;
                setGridData(gvDetailPriorities, ViewState["vs_priorities_recordsheet"]);

                break;
            case "gvDetailClient":

                GridView gvDetailClient = (GridView)fvClientDetail.FindControl("gvDetailClient");
                gvDetailClient.EditIndex = -1;
                setGridData(gvDetailClient, ViewState["vs_client_recordsheet"]);

                break;
            case "gvDetailAgent":

                GridView gvDetailAgent = (GridView)fvAgentDetail.FindControl("gvDetailAgent");

                gvDetailAgent.EditIndex = -1;
                setGridData(gvDetailAgent, ViewState["vs_agent_recordsheet"]);

                break;
            case "gvDetailGood":

                GridView gvDetailGood = (GridView)fvGoodsDetail.FindControl("gvDetailGood");

                gvDetailGood.EditIndex = -1;
                setGridData(gvDetailGood, ViewState["vs_good_recordsheet"]);


                break;
            case "gvDetailDeadline":

                GridView gvDetailDeadline = (GridView)fvPublicationDetail.FindControl("gvDetailDeadline");

                gvDetailDeadline.EditIndex = -1;
                setGridData(gvDetailDeadline, ViewState["vs_DeadlineDetail"]);


                break;


        }
    }


    #endregion gridview

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, 0, 0, 0, 0, 0);

    }

    protected void ClickPrint(object sender, EventArgs e)
    {
        //  litDebug.Text = "lals";
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        //linkBtnTrigger(lbDocSaveImport);

        switch (cmdName)
        {

            case "cmdBackToDetail":
                setActiveTab("docRecordSheet", 0, 0, 0, 0, 0, 0, 0, 0);
                break;

            case "cmdViewRecordSheet":

                linkBtnTrigger(btnUpdateTrademarkRecordSheet);

                string[] _viewrecordsheet = new string[6];
                _viewrecordsheet = e.CommandArgument.ToString().Split(';');
                int _u0_doc_idx_viewrecord = int.Parse(_viewrecordsheet[0]);
                int _u1_doc_idx_viewrecord = int.Parse(_viewrecordsheet[1]);
                int _noidx_viewrecord = int.Parse(_viewrecordsheet[2]);
                int _acidx_viewrecord = int.Parse(_viewrecordsheet[3]);
                int _staidx_viewrecord = int.Parse(_viewrecordsheet[4]);
                int _cemp_idx_viewrecord = int.Parse(_viewrecordsheet[5]);
                int _type_idx_viewrecord = int.Parse(_viewrecordsheet[6]);

                setActiveTab("docRecordSheet", _u0_doc_idx_viewrecord, _noidx_viewrecord, _staidx_viewrecord, _acidx_viewrecord, 0, _cemp_idx_viewrecord, _u1_doc_idx_viewrecord, _type_idx_viewrecord);
                break;

            case "cmdEditRecordSheet":
                string[] _editrecordsheet = new string[6];
                _editrecordsheet = e.CommandArgument.ToString().Split(';');
                int _u0_doc_idx_editrecord = int.Parse(_editrecordsheet[0]);
                int _u1_doc_idx_editrecord = int.Parse(_editrecordsheet[1]);
                int _noidx_editrecord = int.Parse(_editrecordsheet[2]);
                int _acidx_editrecord = int.Parse(_editrecordsheet[3]);
                int _staidx_editrecord = int.Parse(_editrecordsheet[4]);
                int _cemp_idx_editrecord = int.Parse(_editrecordsheet[5]);
                int _type_idx_editrecord = int.Parse(_editrecordsheet[6]);

                setActiveTab("docRecordSheet", _u0_doc_idx_editrecord, _noidx_editrecord, _staidx_editrecord, _acidx_editrecord, 1, _cemp_idx_editrecord, _u1_doc_idx_editrecord, _type_idx_editrecord);
                break;

            case "cmdSaveRecordSheet":
                linkBtnTrigger(btnSaveTrademarkRecordSheet);
                string[] _saverecordsheet = new string[6];
                _saverecordsheet = e.CommandArgument.ToString().Split(';');
                int _u0_doc_idx_saverecord = int.Parse(_saverecordsheet[0]);
                int _u1_doc_idx_saverecord = int.Parse(_saverecordsheet[1]);
                int _noidx_saverecord = int.Parse(_saverecordsheet[2]);
                int _acidx_saverecord = int.Parse(_saverecordsheet[3]);
                int _staidx_saverecord = int.Parse(_saverecordsheet[4]);
                int _cemp_idx_saverecord = int.Parse(_saverecordsheet[5]);
                int _type_idx_saverecord = int.Parse(_saverecordsheet[6]);

                setActiveTab("docRecordSheet", _u0_doc_idx_saverecord, _noidx_saverecord, _staidx_saverecord, _acidx_saverecord, 2, _cemp_idx_saverecord, _u1_doc_idx_saverecord, _type_idx_saverecord);
                break;

            case "cmdAddPrioritiesList":
                setPrioritiesList();

                break;
            case "cmdAddClientList":
                setClientList();

                break;
            case "cmdAddAgentList":
                setAgentList();

                break;
            case "cmdAddGoodsList":
                setGoodsList();

                break;
            case "cmdAddRenewalList":
                setRenewalList();

                break;
            case "cmdAddDeadlineList":
                setDeadlineList();

                break;
            case "cmdAddAssignmentList":
                setAssignmentList();

                break;
            case "cmdAddChangeofNameList":
                setChangeofNameList();

                break;
            case "cmdSaveTrademarkRecordSheet":

                linkBtnTrigger(btnSaveTrademarkRecordSheet);

                //fvRegistrationDetail
                HiddenField hfU0IDX_insertrecord = (HiddenField)fvRegistrationDetail.FindControl("hfU0IDX");
                HiddenField hfU1IDX_insertrecord = (HiddenField)fvRegistrationDetail.FindControl("hfU1IDX");
                HiddenField hfACIDX_insertrecord = (HiddenField)fvRegistrationDetail.FindControl("hfACIDX");
                HiddenField hfNOIDX_insertrecord = (HiddenField)fvRegistrationDetail.FindControl("hfNOIDX");
                HiddenField hfSTAIDX_insertrecord = (HiddenField)fvRegistrationDetail.FindControl("hfSTAIDX");
                HiddenField hftype_idx_insertrecord = (HiddenField)fvRegistrationDetail.FindControl("hftype_idx");

                DropDownList ddlowner_idx = (DropDownList)fvRegistrationDetail.FindControl("ddlowner_idx");
                DropDownList ddlincorporated_in_idx = (DropDownList)fvRegistrationDetail.FindControl("ddlincorporated_in_idx");
                TextBox txt_application_no = (TextBox)fvRegistrationDetail.FindControl("txt_application_no");
                TextBox txt_application_date = (TextBox)fvRegistrationDetail.FindControl("txt_application_date");
                TextBox txt_registration_no = (TextBox)fvRegistrationDetail.FindControl("txt_registration_no");
                TextBox txt_registration_date = (TextBox)fvRegistrationDetail.FindControl("txt_registration_date");
                DropDownList ddlTrademarkStatus = (DropDownList)fvRegistrationDetail.FindControl("ddlTrademarkStatus");
                DropDownList ddlTrademarkSubStatus = (DropDownList)fvRegistrationDetail.FindControl("ddlTrademarkSubStatus");
                TextBox tb_File_Reference = (TextBox)fvRegistrationDetail.FindControl("tb_File_Reference");
                TextBox tb_Record_Reference = (TextBox)fvRegistrationDetail.FindControl("tb_Record_Reference");
                TextBox tb_Next_Renewal_Due = (TextBox)fvRegistrationDetail.FindControl("tb_Next_Renewal_Due");
                DropDownList ddlTypeOfRegistrtion = (DropDownList)fvRegistrationDetail.FindControl("ddlTypeOfRegistrtion");
                TextBox tbtype_idx_viewdetail = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");
                //fvRegistrationDetail

                //fvPublicationDetail
                TextBox tbPublication_Date = (TextBox)fvPublicationDetail.FindControl("tbPublication_Date");
                TextBox tbGrant_date = (TextBox)fvPublicationDetail.FindControl("tbGrant_date");
                TextBox tbJournal_Volume = (TextBox)fvPublicationDetail.FindControl("tbJournal_Volume");
                TextBox tb_Pub_Date = (TextBox)fvPublicationDetail.FindControl("tb_Pub_Date");
                TextBox tb_Registry_Reference = (TextBox)fvPublicationDetail.FindControl("tb_Registry_Reference");
                TextBox tbFinal_Office_Date = (TextBox)fvPublicationDetail.FindControl("tbFinal_Office_Date");
                TextBox tb_Office_Action_Date = (TextBox)fvPublicationDetail.FindControl("tb_Office_Action_Date");
                TextBox tb_Date_Declaration_Filed = (TextBox)fvPublicationDetail.FindControl("tb_Date_Declaration_Filed");
                //fvPublicationDetail

                //fvDependentDetail
                TextBox tbdependent_registration = (TextBox)fvDependentDetail.FindControl("tbdependent_registration");

                //fvAdditionalDetail
                TextBox tbAdditionalDetail = (TextBox)fvAdditionalDetail.FindControl("tbAdditionalDetail");

                //fvTademarkProfile
                TextBox tbName_trademark = (TextBox)fvTademarkProfile.FindControl("tbName_trademark");
                DropDownList ddlLocal_language_trademark = (DropDownList)fvTademarkProfile.FindControl("ddlLocal_language_trademark");
                TextBox tbProfileOwner = (TextBox)fvTademarkProfile.FindControl("tbProfileOwner");
                DropDownList ddlProfileOwnerLanguage = (DropDownList)fvTademarkProfile.FindControl("ddlProfileOwnerLanguage");
                TextBox tbProfileClient = (TextBox)fvTademarkProfile.FindControl("tbProfileClient");
                DropDownList ddlProfileClientLanguage = (DropDownList)fvTademarkProfile.FindControl("ddlProfileClientLanguage");
                TextBox tbTranslation = (TextBox)fvTademarkProfile.FindControl("tbTranslation");
                TextBox tbTransliteration = (TextBox)fvTademarkProfile.FindControl("tbTransliteration");

                //fvBackground
                TextBox tbBackgroundDetail = (TextBox)fvBackground.FindControl("tbBackgroundDetail");

                //fvSummary
                TextBox tbSummaryDetail = (TextBox)fvSummary.FindControl("tbSummaryDetail");

                //fvClaims
                TextBox tbClaimsDetail = (TextBox)fvClaims.FindControl("tbClaimsDetail");

                //fvAbstract
                TextBox tbAbstractDetail = (TextBox)fvAbstract.FindControl("tbAbstractDetail");

                //fvDescription
                TextBox tbDescriptionDetail = (TextBox)fvDescription.FindControl("tbDescriptionDetail");

                //fvTypeofwork
                RadioButtonList rdoTypeofwork = (RadioButtonList)fvTypeofwork.FindControl("rdoTypeofwork");

                int decision_recordsheet = 16; //save recordsheet

                data_law data_u1insert_recordsheet = new data_law();

                //insert record sheet fvRegistrationDetail
                tcm_u1_document_detail u1insert_recordsheet = new tcm_u1_document_detail();
                data_u1insert_recordsheet.tcm_u1_document_list = new tcm_u1_document_detail[1];

                u1insert_recordsheet.cemp_idx = _emp_idx;
                u1insert_recordsheet.u0_doc_idx = int.Parse(hfU0IDX_insertrecord.Value);
                u1insert_recordsheet.u1_doc_idx = int.Parse(hfU1IDX_insertrecord.Value);
                u1insert_recordsheet.acidx = int.Parse(hfACIDX_insertrecord.Value);
                u1insert_recordsheet.noidx = int.Parse(hfNOIDX_insertrecord.Value);
                u1insert_recordsheet.decision = decision_recordsheet;
                u1insert_recordsheet.comment = "-";
                u1insert_recordsheet.owner_idx = int.Parse(ddlowner_idx.SelectedValue);
                u1insert_recordsheet.incorporated_in_idx = int.Parse(ddlincorporated_in_idx.SelectedValue);
                u1insert_recordsheet.application_no = txt_application_no.Text;
                u1insert_recordsheet.application_date = txt_application_date.Text;
                u1insert_recordsheet.registration_no = txt_registration_no.Text;
                u1insert_recordsheet.registration_date = txt_registration_date.Text;
                u1insert_recordsheet.tradmark_status = int.Parse(ddlTrademarkStatus.SelectedValue);
                u1insert_recordsheet.tradmark_substatus = int.Parse(ddlTrademarkSubStatus.SelectedValue);
                u1insert_recordsheet.file_reference = tb_File_Reference.Text;
                u1insert_recordsheet.recode_referense = tb_Record_Reference.Text;
                u1insert_recordsheet.type_of_registration_idx = int.Parse(ddlTypeOfRegistrtion.SelectedValue);
                u1insert_recordsheet.next_renewal_due = tb_Next_Renewal_Due.Text;
                //insert u0 document fvRegistrationDetail

                
                //insert next renewal due
                
                var _datasetRenewal = (DataSet)ViewState["vsRenewalList"];
                var _addRenewal = new tcm_record_renewal_detail[_datasetRenewal.Tables[0].Rows.Count];
                int _Renewal = 0;
                if (_datasetRenewal.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dtrowRenewal in _datasetRenewal.Tables[0].Rows)
                    {
                        _addRenewal[_Renewal] = new tcm_record_renewal_detail();
                        _addRenewal[_Renewal].cemp_idx = _emp_idx;
                        _addRenewal[_Renewal].NextRenewalDue_date = dtrowRenewal["drNextRenewalDueText"].ToString();
                        _addRenewal[_Renewal].Selected_value = int.Parse(dtrowRenewal["drSelectedIdx"].ToString());
                        _addRenewal[_Renewal].Selected_name = dtrowRenewal["drSelectedText"].ToString();
                        _addRenewal[_Renewal].Renewal_Status = int.Parse(dtrowRenewal["drRenewalStatusIdx"].ToString());
                        _addRenewal[_Renewal].Renewal_SubStatus = int.Parse(dtrowRenewal["drRenewalSubStatusIdx"].ToString());

                        _Renewal++;

                    }
                }
                
                //insert next renewal due

                //insert fvAssignment
                var _datasetAssignment = (DataSet)ViewState["vsAssignmentList"];
                var _addAssignment = new tcm_record_assignment_detail[_datasetAssignment.Tables[0].Rows.Count];
                int _Assignment = 0;
                if (_datasetAssignment.Tables[0].Rows.Count > 0)
                {
                    

                    foreach (DataRow dtrowAssignment in _datasetAssignment.Tables[0].Rows)
                    {
                        _addAssignment[_Assignment] = new tcm_record_assignment_detail();
                        _addAssignment[_Assignment].cemp_idx = _emp_idx;
                        _addAssignment[_Assignment].step_idx = int.Parse(dtrowAssignment["drStepAssignmentIdx"].ToString());
                        _addAssignment[_Assignment].step_name = dtrowAssignment["drStepAssignmentText"].ToString();
                        _addAssignment[_Assignment].selected_idx = int.Parse(dtrowAssignment["drSelectedAssignmentIdx"].ToString());
                        _addAssignment[_Assignment].selected_name = dtrowAssignment["drSelectedAssignmentText"].ToString();
                        _addAssignment[_Assignment].status = int.Parse(dtrowAssignment["drStatusAssignmentIdx"].ToString());
                        _addAssignment[_Assignment].substatus = int.Parse(dtrowAssignment["drSubStatusAssignmentIdx"].ToString());

                        _Assignment++;

                    }
                }
                
                //insert fvAssignment

                //insert fvChangeofName
                var _datasetChangeofName = (DataSet)ViewState["vsChangeofNameList"];
                var _addChangeofName = new tcm_record_changeofname_detail[_datasetChangeofName.Tables[0].Rows.Count];
                int _ChangeofName = 0;

                if (_datasetChangeofName.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dtrowChangeofName in _datasetChangeofName.Tables[0].Rows)
                    {
                        _addChangeofName[_ChangeofName] = new tcm_record_changeofname_detail();
                        _addChangeofName[_ChangeofName].cemp_idx = _emp_idx;
                        _addChangeofName[_ChangeofName].selected_idx = int.Parse(dtrowChangeofName["drSelectedChangeofNameIdx"].ToString());
                        _addChangeofName[_ChangeofName].selected_name = dtrowChangeofName["drSelectedChangeofNameText"].ToString();
                        _addChangeofName[_ChangeofName].status = int.Parse(dtrowChangeofName["drStatusChangeofNameIdx"].ToString());
                        _addChangeofName[_ChangeofName].substatus = int.Parse(dtrowChangeofName["drSubStatusChangeofNameIdx"].ToString());

                        _ChangeofName++;

                    }
                }
                //insert fvChangeofName

                //insert fvPrioritiesDetail
                var _datasetPriorities = (DataSet)ViewState["vsPrioritiesList"];
                var _addPriorities = new tcm_record_priorities_detail[_datasetPriorities.Tables[0].Rows.Count];
                int _Priorities = 0;
                if (_datasetPriorities.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dtrowPriorities in _datasetPriorities.Tables[0].Rows)
                    {
                        _addPriorities[_Priorities] = new tcm_record_priorities_detail();
                        _addPriorities[_Priorities].cemp_idx = _emp_idx;
                        _addPriorities[_Priorities].Type_of_priority = dtrowPriorities["drTypeofpriorityText"].ToString();
                        _addPriorities[_Priorities].Priority_Date = dtrowPriorities["drPriorityDateText"].ToString();
                        _addPriorities[_Priorities].Priority_No = dtrowPriorities["drPriorityNoText"].ToString();
                        _addPriorities[_Priorities].country_idx = int.Parse(dtrowPriorities["drCountrypriorityIdx"].ToString());
                        _addPriorities[_Priorities].status_idx = int.Parse(dtrowPriorities["drStatuspriorityIdx"].ToString());

                        _Priorities++;

                    }
                }
               
                //insert fvPrioritiesDetail

                //insert fvClientDetail
                var _datasetClient = (DataSet)ViewState["vsClientList"];
                var _addClient = new tcm_record_client_detail[_datasetClient.Tables[0].Rows.Count];
                int _Client = 0;
                if (_datasetClient.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dtrowClient in _datasetClient.Tables[0].Rows)
                    {
                        _addClient[_Client] = new tcm_record_client_detail();
                        _addClient[_Client].cemp_idx = _emp_idx;
                        _addClient[_Client].Client_Detail = dtrowClient["drClientDetailText"].ToString();
                        _addClient[_Client].Client_Contact = dtrowClient["drClientContactText"].ToString();
                        _addClient[_Client].Client_Reference = dtrowClient["drClientReferenceText"].ToString();
                        _addClient[_Client].status_idx = int.Parse(dtrowClient["drClientStatusIdx"].ToString());

                        _Client++;

                    }
                }
               
                //insert fvClientDetail

                //insert fvAgentDetail
                var _datasetAgent = (DataSet)ViewState["vsAgentList"];
                var _addAgent = new tcm_record_agent_detail[_datasetAgent.Tables[0].Rows.Count];
                int _Agent = 0;
                if (_datasetAgent.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dtrowagent in _datasetAgent.Tables[0].Rows)
                    {
                        _addAgent[_Agent] = new tcm_record_agent_detail();
                        _addAgent[_Agent].cemp_idx = _emp_idx;
                        _addAgent[_Agent].Agent_Detail = dtrowagent["drAgentDetailText"].ToString();
                        _addAgent[_Agent].Agent_Contact = dtrowagent["drAgentContactText"].ToString();
                        _addAgent[_Agent].Agent_Reference = dtrowagent["drAgentReferenceText"].ToString();
                        _addAgent[_Agent].status_idx = int.Parse(dtrowagent["drAgentStatusIdx"].ToString());

                        _Agent++;

                    }
                }
                
                //insert fvAgentDetail

                //insert fvGoodsDetail
                var _datasetGoods = (DataSet)ViewState["vsGoodsList"];
                var _addGoods = new tcm_record_goods_detail[_datasetGoods.Tables[0].Rows.Count];
                int _Goods = 0;
                if (_datasetGoods.Tables[0].Rows.Count > 0)
                {
                    

                    foreach (DataRow dtrowaGoods in _datasetGoods.Tables[0].Rows)
                    {
                        _addGoods[_Goods] = new tcm_record_goods_detail();
                        _addGoods[_Goods].cemp_idx = _emp_idx;
                        _addGoods[_Goods].IntClass = dtrowaGoods["drGoodsIntClassText"].ToString();
                        _addGoods[_Goods].Goods_Detail = dtrowaGoods["drGoodsDetailText"].ToString();
                        _addGoods[_Goods].language_idx = int.Parse(dtrowaGoods["drLocallanguageIdx"].ToString());


                        _Goods++;

                    }
                }

                //insert fvGoodsDetail

                //insert fvDependentDetail
                tcm_record_dependent_detail record_dependent_insert = new tcm_record_dependent_detail();
                data_u1insert_recordsheet.tcm_record_dependent_list = new tcm_record_dependent_detail[1];

                //insert fvAdditionalDetail
                tcm_record_additional_detail record_additional_insert = new tcm_record_additional_detail();
                data_u1insert_recordsheet.tcm_record_additional_list = new tcm_record_additional_detail[1];

                //insert fvBackground
                tcm_record_backgroud_detail record_backgroud_insert = new tcm_record_backgroud_detail();
                data_u1insert_recordsheet.tcm_record_backgroud_list = new tcm_record_backgroud_detail[1];

                //insert fvSummary
                tcm_record_summary_detail record_summary_insert = new tcm_record_summary_detail();
                data_u1insert_recordsheet.tcm_record_summary_list = new tcm_record_summary_detail[1];

                //insert fvClaims
                tcm_record_claims_detail record_claims_insert = new tcm_record_claims_detail();
                data_u1insert_recordsheet.tcm_record_claims_list = new tcm_record_claims_detail[1];

                //insert fvAbstract
                tcm_record_abstract_detail record_abstract_insert = new tcm_record_abstract_detail();
                data_u1insert_recordsheet.tcm_record_abstract_list = new tcm_record_abstract_detail[1];

                //insert fvDescription
                tcm_record_description_detail record_description_insert = new tcm_record_description_detail();
                data_u1insert_recordsheet.tcm_record_description_list = new tcm_record_description_detail[1];

                //insert fvTypeofwork
                tcm_record_typeofwork_detail record_typeofwork_insert = new tcm_record_typeofwork_detail();
                data_u1insert_recordsheet.tcm_record_typeofwork_list = new tcm_record_typeofwork_detail[1];


                switch (hftype_idx_insertrecord.Value)
                {
                    case "1": //mark
                         //insert fvDependentDetail
                        if (tbdependent_registration.Text != null)
                        {
                            record_dependent_insert.cemp_idx = _emp_idx;
                            record_dependent_insert.Dependent_Registration = tbdependent_registration.Text;
                        }

                        //insert fvAdditionalDetail
                        if (tbAdditionalDetail.Text != null)
                        {
                            record_additional_insert.cemp_idx = _emp_idx;
                            record_additional_insert.Additional_Details = tbAdditionalDetail.Text;
                        }
                        break;
                    case "7": // Industrial Design
                        //insert fvDependentDetail
                        if (tbdependent_registration.Text != null)
                        {
                            record_dependent_insert.cemp_idx = _emp_idx;
                            record_dependent_insert.Dependent_Registration = tbdependent_registration.Text;
                        }

                        //insert fvAdditionalDetail
                        if (tbAdditionalDetail.Text != null)
                        {
                            record_additional_insert.cemp_idx = _emp_idx;
                            record_additional_insert.Additional_Details = tbAdditionalDetail.Text;
                        }

                        //insert fvBackground
                        if (tbBackgroundDetail.Text != null)
                        {
                            record_backgroud_insert.cemp_idx = _emp_idx;
                            record_backgroud_insert.background_detail = tbBackgroundDetail.Text;
                        }

                        //insert fvSummary
                        if (tbSummaryDetail.Text != null)
                        {
                            record_summary_insert.cemp_idx = _emp_idx;
                            record_summary_insert.summary_detail = tbSummaryDetail.Text;
                        }

                        //insert fvClaims
                        if (tbClaimsDetail.Text != null)
                        {
                            record_claims_insert.cemp_idx = _emp_idx;
                            record_claims_insert.claims_detail = tbClaimsDetail.Text;
                        }

                        //insert fvDescription
                        if (tbDescriptionDetail.Text != null)
                        {
                            record_description_insert.cemp_idx = _emp_idx;
                            record_description_insert.description_detail = tbDescriptionDetail.Text;
                        }

                        break;
                    case "6": //Invention/Process

                        //insert fvDependentDetail
                        if (tbdependent_registration.Text != null)
                        {
                            record_dependent_insert.cemp_idx = _emp_idx;
                            record_dependent_insert.Dependent_Registration = tbdependent_registration.Text;
                        }

                        //insert fvAdditionalDetail
                        if (tbAdditionalDetail.Text != null)
                        {
                            record_additional_insert.cemp_idx = _emp_idx;
                            record_additional_insert.Additional_Details = tbAdditionalDetail.Text;
                        }

                        //insert fvBackground
                        if (tbBackgroundDetail.Text != null)
                        {
                            record_backgroud_insert.cemp_idx = _emp_idx;
                            record_backgroud_insert.background_detail = tbBackgroundDetail.Text;
                        }

                        //insert fvSummary
                        if (tbSummaryDetail.Text != null)
                        {
                            record_summary_insert.cemp_idx = _emp_idx;
                            record_summary_insert.summary_detail = tbSummaryDetail.Text;
                        }

                        //insert fvClaims
                        if (tbClaimsDetail.Text != null)
                        {
                            record_claims_insert.cemp_idx = _emp_idx;
                            record_claims_insert.claims_detail = tbClaimsDetail.Text;
                        }

                        break;
                    case "8": //copy right

                        //insert fvAdditionalDetail
                        if (tbAdditionalDetail.Text != null)
                        {
                            record_additional_insert.cemp_idx = _emp_idx;
                            record_additional_insert.Additional_Details = tbAdditionalDetail.Text;
                        }

                        //insert fvAbstract
                        if (tbAbstractDetail.Text != null)
                        {
                            record_abstract_insert.cemp_idx = _emp_idx;
                            record_abstract_insert.abstract_detail = tbAbstractDetail.Text;
                        }

                        //insert fvTypeofwork
                        record_typeofwork_insert.cemp_idx = _emp_idx;
                        record_typeofwork_insert.typeofwork_idx = int.Parse(rdoTypeofwork.SelectedValue);
                        

                        break;
                }

                //insert fvTademarkProfile
                tcm_record_trademarkprofile_detail record_trademarkprofile_insert = new tcm_record_trademarkprofile_detail();
                data_u1insert_recordsheet.tcm_record_trademarkprofile_list = new tcm_record_trademarkprofile_detail[1];

                record_trademarkprofile_insert.cemp_idx = _emp_idx;
                record_trademarkprofile_insert.Name = tbName_trademark.Text;
                record_trademarkprofile_insert.doc_language_idx = int.Parse(ddlLocal_language_trademark.SelectedValue);
                record_trademarkprofile_insert.Profile_Owner = tbProfileOwner.Text;
                record_trademarkprofile_insert.owner_language_idx = int.Parse(ddlProfileOwnerLanguage.SelectedValue);
                record_trademarkprofile_insert.Profile_Client = tbProfileClient.Text;
                record_trademarkprofile_insert.client_language_idx = int.Parse(ddlProfileClientLanguage.SelectedValue);
                record_trademarkprofile_insert.Translation = tbTranslation.Text;
                record_trademarkprofile_insert.Transliteration = tbTransliteration.Text;
                record_trademarkprofile_insert.Transliteration = tbTransliteration.Text;
                //insert fvTademarkProfile

                
                data_u1insert_recordsheet.tcm_record_renewal_list = _addRenewal;
                data_u1insert_recordsheet.tcm_record_assignment_list = _addAssignment;
                data_u1insert_recordsheet.tcm_record_changeofname_list = _addChangeofName;

                data_u1insert_recordsheet.tcm_u1_document_list[0] = u1insert_recordsheet;
                data_u1insert_recordsheet.tcm_record_dependent_list[0] = record_dependent_insert;
                data_u1insert_recordsheet.tcm_record_priorities_list = _addPriorities;
                data_u1insert_recordsheet.tcm_record_client_list = _addClient;
                data_u1insert_recordsheet.tcm_record_agent_list = _addAgent;
                data_u1insert_recordsheet.tcm_record_goods_list = _addGoods;
                data_u1insert_recordsheet.tcm_record_additional_list[0] = record_additional_insert;

                data_u1insert_recordsheet.tcm_record_backgroud_list[0] = record_backgroud_insert;
                data_u1insert_recordsheet.tcm_record_summary_list[0] = record_summary_insert;
                data_u1insert_recordsheet.tcm_record_claims_list[0] = record_claims_insert;
                data_u1insert_recordsheet.tcm_record_description_list[0] = record_description_insert;
                data_u1insert_recordsheet.tcm_record_typeofwork_list[0] = record_typeofwork_insert;

                data_u1insert_recordsheet.tcm_record_trademarkprofile_list[0] = record_trademarkprofile_insert;

                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1insert_recordsheet));
                data_u1insert_recordsheet = callServicePostLaw(_urlSetLawRecordSheet, data_u1insert_recordsheet);

                ViewState["vs_trademark_idx"] = data_u1insert_recordsheet.return_code.ToString();

                FileUpload UploadFileTrademark = (FileUpload)fvTademarkProfile.FindControl("UploadFileTrademark");

                if (UploadFileTrademark.HasFile)
                {
                    //litDebug.Text = "1";

                    string filepath_trademark = Server.MapPath(_path_file_law_trademarks_profile);
                    HttpFileCollection uploadedFiles_trademark = Request.Files;

                    string filePath2 = "trademark" + "-" + ViewState["vs_trademark_idx"].ToString();//data_u0doc_insert.tcm_u0_document_list[0].docrequest_code.ToString();
                    string filePath1 = Server.MapPath(_path_file_law_trademarks_profile + filePath2);

                    for (int i = 0; i < uploadedFiles_trademark.Count; i++)
                    {
                        HttpPostedFile userpost_createfiletrademark = uploadedFiles_trademark[i];

                        try
                        {
                            if (userpost_createfiletrademark.ContentLength > 0)
                            {

                                string _filepathExtension = Path.GetExtension(userpost_createfiletrademark.FileName);

                                //litDebug1.Text += filepath_usercreate.ToString() + filePath2.ToString();

                                if (!Directory.Exists(filePath1))
                                {
                                    Directory.CreateDirectory(filePath1);
                                }

                                userpost_createfiletrademark.SaveAs(filepath_trademark + filePath2 + "\\" + ViewState["vs_trademark_idx"].ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());

                            }
                        }
                        catch (Exception Ex)
                        {
                            litDebug.Text += "Error: <br>" + Ex.Message;
                        }
                    }

                    //setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0, 0);
                }
                else
                {
                    //litDebug.Text = "2";
                    ////ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Please Attach File ---');", true);
                    ////break;
                }


                // --- insert u1 ------  //


                //insert record sheet fvPublicationDetail
                if (tbPublication_Date.Text != "")
                {
                    data_law data_publication_recordsheet = new data_law();
                    tcm_record_publication_detail record_publication_insert = new tcm_record_publication_detail();
                    data_publication_recordsheet.tcm_record_publication_list = new tcm_record_publication_detail[1];
                    record_publication_insert.u1_doc_idx = int.Parse(hfU1IDX_insertrecord.Value);
                    record_publication_insert.publication_idx = 0;
                    record_publication_insert.cemp_idx = _emp_idx;
                    record_publication_insert.Publication_Date = tbPublication_Date.Text;
                    record_publication_insert.NoticeofAllowance_Date = tbGrant_date.Text;
                    record_publication_insert.Journal_Volume = tbJournal_Volume.Text;
                    record_publication_insert.PubRegDate_Date = tb_Pub_Date.Text;
                    record_publication_insert.Registry_Reference = tb_Registry_Reference.Text;
                    record_publication_insert.FinalOfficeAction_Date = tbFinal_Office_Date.Text;
                    record_publication_insert.OfficeAction_Date = tb_Office_Action_Date.Text;
                    record_publication_insert.Date_DeclarationFiled = tb_Date_Declaration_Filed.Text;

                    data_publication_recordsheet.tcm_record_publication_list[0] = record_publication_insert;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_publication_recordsheet));
                    data_publication_recordsheet = callServicePostLaw(_urlSetLawRecordSheetPublication, data_publication_recordsheet);

                    ViewState["vs_publication_idx"] = data_publication_recordsheet.return_code.ToString();
                    GridView gvDeadlineInsert = (GridView)fvPublicationDetail.FindControl("gvDeadlineInsert");

                    data_law data_deadline_recordsheet = new data_law();
                    var _record_deadline_detail = new tcm_record_deadline_detail[gvDeadlineInsert.Rows.Count];
                    int i = 0;
                    //var _record_actions_detail = new tcm_record_actions_detail[0];
                    foreach (GridViewRow gvr_gvDeadline in gvDeadlineInsert.Rows)
                    {
                        GridView gvActionsDetail = (GridView)gvr_gvDeadline.FindControl("gvActionsDetail");
                        Label lbl_drOfficeActionDateText = (Label)gvr_gvDeadline.FindControl("lbl_drOfficeActionDateText");
                        Label lbl_drSelectedDeadlineIdx = (Label)gvr_gvDeadline.FindControl("lbl_drSelectedDeadlineIdx");
                        Label lbl_drSelectedDeadlineText = (Label)gvr_gvDeadline.FindControl("lbl_drSelectedDeadlineText");

                        Label lbl_drSelectedActionListDeadlineText = (Label)gvr_gvDeadline.FindControl("lbl_drSelectedActionListDeadlineText");
                        Label lbl_drSelectedActionListDeadlineIdx = (Label)gvr_gvDeadline.FindControl("lbl_drSelectedActionListDeadlineIdx");

                        _record_deadline_detail[i] = new tcm_record_deadline_detail();
                        _record_deadline_detail[i].cemp_idx = _emp_idx;
                        _record_deadline_detail[i].publication_idx = int.Parse(ViewState["vs_publication_idx"].ToString());
                        _record_deadline_detail[i].deadline_date = lbl_drOfficeActionDateText.Text;
                        _record_deadline_detail[i].deadline_staus = int.Parse(lbl_drSelectedDeadlineIdx.Text);
                        _record_deadline_detail[i].deadline_stausname = lbl_drSelectedDeadlineText.Text;
                        _record_deadline_detail[i].action_detail_idx_value = lbl_drSelectedActionListDeadlineIdx.Text;


                        //int i = 0;
                        //var _u2doc_node3 = new qa_lab_u2_qalab[rp_test_sample.Items.Count];
                        //int count_u2doc = 0;

                        var _record_actions_detail = new tcm_record_actions_detail[gvActionsDetail.Rows.Count];
                        //data_u1insert_recordsheet.tcm_record_actions_list = new tcm_record_actions_detail[1];
                        int sumcheck_record = 0;
                        int count_record = 0;
                        foreach (GridViewRow gvrow in gvActionsDetail.Rows)
                        {


                            TextBox tbActionsDetail = (TextBox)gvrow.FindControl("tbActionsDetail");
                            TextBox tbOwnerDetail = (TextBox)gvrow.FindControl("tbOwnerDetail");
                            Label lblaction_idx = (Label)gvrow.FindControl("lblaction_idx");

                            _record_actions_detail[count_record] = new tcm_record_actions_detail();
                            _record_actions_detail[count_record].cemp_idx = _emp_idx;
                            _record_actions_detail[count_record].deadline_date = lbl_drOfficeActionDateText.Text;
                            _record_actions_detail[count_record].Actions_Detail = tbActionsDetail.Text;
                            _record_actions_detail[count_record].Owner_Detail = tbOwnerDetail.Text;
                            _record_actions_detail[count_record].action_idx = int.Parse(lblaction_idx.Text);

                            sumcheck_record = sumcheck_record + 1;

                            count_record++;

                        }

                        //data_publication_recordsheet.tcm_record_actions_list = _record_actions_detail;

                        //data_u1insert_recordsheet.tcm_record_actions_list = _record_actions_detail;
                        //i++;
                        //
                        data_deadline_recordsheet.tcm_record_deadline_list = _record_deadline_detail;
                        data_deadline_recordsheet.tcm_record_actions_list = _record_actions_detail;

                        //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_deadline_recordsheet));
                        data_deadline_recordsheet = callServicePostLaw(_urlSetLawRecordSheetDeadline, data_deadline_recordsheet);
                    }
                    //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1insert_recordsheet));
                    //insert action detail
                }
                //insert record sheet fvPublicationDetail

                // ---------  //

                setActiveTab("docRecordSheet", 0, 0, 0, 0, 0, 0, 0, 0);

                break;

            case "cmdUpdateTrademarkRecordSheet":

                linkBtnTrigger(btnUpdateTrademarkRecordSheet);

                //fvRegistrationDetail
                HiddenField hfU0IDX_insertrecord_edit = (HiddenField)fvRegistrationDetail.FindControl("hfU0IDX");
                HiddenField hfU1IDX_insertrecord_edit = (HiddenField)fvRegistrationDetail.FindControl("hfU1IDX");
                HiddenField hfACIDX_insertrecord_edit = (HiddenField)fvRegistrationDetail.FindControl("hfACIDX");
                HiddenField hfNOIDX_insertrecord_edit = (HiddenField)fvRegistrationDetail.FindControl("hfNOIDX");
                HiddenField hfSTAIDX_insertrecord_edit = (HiddenField)fvRegistrationDetail.FindControl("hfSTAIDX");
                HiddenField hftype_idx_insertrecord_edit = (HiddenField)fvRegistrationDetail.FindControl("hftype_idx");

                DropDownList ddlowner_idx_edit = (DropDownList)fvRegistrationDetail.FindControl("ddlowner_idx");
                DropDownList ddlincorporated_in_idx_edit = (DropDownList)fvRegistrationDetail.FindControl("ddlincorporated_in_idx");
                TextBox txt_application_no_edit = (TextBox)fvRegistrationDetail.FindControl("txt_application_no");
                TextBox txt_application_date_edit = (TextBox)fvRegistrationDetail.FindControl("txt_application_date");
                TextBox txt_registration_no_edit = (TextBox)fvRegistrationDetail.FindControl("txt_registration_no");
                TextBox txt_registration_date_edit = (TextBox)fvRegistrationDetail.FindControl("txt_registration_date");
                DropDownList ddlTrademarkStatus_edit = (DropDownList)fvRegistrationDetail.FindControl("ddl_tradmark_status");
                DropDownList ddlTrademarkSubStatus_edit = (DropDownList)fvRegistrationDetail.FindControl("ddl_tradmark_substatus");
                TextBox tb_File_Reference_edit = (TextBox)fvRegistrationDetail.FindControl("txt_file_reference");
                TextBox tb_Record_Reference_edit = (TextBox)fvRegistrationDetail.FindControl("txt_recode_referense");
                //TextBox tb_Next_Renewal_Due_edit = (TextBox)fvRegistrationDetail.FindControl("txt_recode_referense");
                DropDownList ddlTypeOfRegistrtion_edit = (DropDownList)fvRegistrationDetail.FindControl("ddlTypeOfRegistrtion");
                TextBox tbtype_idx_viewdetail_edit = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");
                //fvRegistrationDetail

                //fvPublicationDetail
                TextBox tbPublication_Date_edit = (TextBox)fvPublicationDetail.FindControl("tbPublication_Date");
                TextBox tbGrant_date_edit = (TextBox)fvPublicationDetail.FindControl("tbGrant_date");
                TextBox tbJournal_Volume_edit = (TextBox)fvPublicationDetail.FindControl("tbJournal_Volume");
                TextBox tb_Pub_Date_edit = (TextBox)fvPublicationDetail.FindControl("tb_Pub_Date");
                TextBox tb_Registry_Reference_edit = (TextBox)fvPublicationDetail.FindControl("tb_Registry_Reference");
                TextBox tbFinal_Office_Date_edit = (TextBox)fvPublicationDetail.FindControl("tbFinal_Office_Date");
                TextBox tb_Office_Action_Date_edit = (TextBox)fvPublicationDetail.FindControl("tb_Office_Action_Date");
                TextBox tb_Date_Declaration_Filed_edit = (TextBox)fvPublicationDetail.FindControl("tb_Date_DeclarationFiled");
                TextBox txt_publication_idx_edit = (TextBox)fvPublicationDetail.FindControl("txt_publication_idx");
                //fvPublicationDetail

                //fvDependentDetail
                TextBox tbdependent_registration_edit = (TextBox)fvDependentDetail.FindControl("tbdependent_registration");
                Label lbl_dependent_idx_edit = (Label)fvDependentDetail.FindControl("lbl_dependent_idx_edit");

                //fvAdditionalDetail
                TextBox tbAdditionalDetail_edit = (TextBox)fvAdditionalDetail.FindControl("tbAdditionalDetail");
                Label lbl_additional_idx_edit = (Label)fvAdditionalDetail.FindControl("lbl_additional_idx_edit");

                //fvTademarkProfile
                Label lbl_trademark_profile_idx_edit = (Label)fvTademarkProfile.FindControl("lbl_trademark_profile_idx_edit");
                TextBox tbName_trademark_edit = (TextBox)fvTademarkProfile.FindControl("tbName_trademark");
                DropDownList ddlLocal_language_trademark_edit = (DropDownList)fvTademarkProfile.FindControl("ddlLocal_language_trademark");
                TextBox tbProfileOwner_edit = (TextBox)fvTademarkProfile.FindControl("tbProfileOwner");
                DropDownList ddlProfileOwnerLanguage_edit = (DropDownList)fvTademarkProfile.FindControl("ddlProfileOwnerLanguage");
                TextBox tbProfileClient_edit = (TextBox)fvTademarkProfile.FindControl("tbProfileClient");
                DropDownList ddlProfileClientLanguage_edit = (DropDownList)fvTademarkProfile.FindControl("ddlProfileClientLanguage");
                TextBox tbTranslation_edit = (TextBox)fvTademarkProfile.FindControl("tbTranslation");
                TextBox tbTransliteration_edit = (TextBox)fvTademarkProfile.FindControl("tbTransliteration");

                //fvBackground
                TextBox tbBackgroundDetail_edit = (TextBox)fvBackground.FindControl("tbBackgroundDetail");
                Label lbl_background_idx_edit = (Label)fvBackground.FindControl("lbl_background_idx_edit");

                //fvSummary
                TextBox tbSummaryDetail_edit = (TextBox)fvSummary.FindControl("tbSummaryDetail");
                Label lbl_summary_idx_edit = (Label)fvSummary.FindControl("lbl_summary_idx_edit");

                //fvClaims
                TextBox tbClaimsDetail_edit = (TextBox)fvClaims.FindControl("tbClaimsDetail");
                Label lbl_claims_idx_edit = (Label)fvClaims.FindControl("lbl_claims_idx_edit");

                //fvAbstract
                TextBox tbAbstractDetail_edit = (TextBox)fvAbstract.FindControl("tbAbstractDetail");
                Label lbl_abstract_idx_edit = (Label)fvAbstract.FindControl("lbl_abstract_idx_edit");

                //fvDescription
                TextBox tbDescriptionDetail_edit = (TextBox)fvDescription.FindControl("tbDescriptionDetail");
                Label lbl_description_idx_edit = (Label)fvDescription.FindControl("lbl_description_idx_edit");

                //fvTypeofwork
                RadioButtonList rdoTypeofwork_edit = (RadioButtonList)fvTypeofwork.FindControl("rdoTypeofwork");
                Label lbl_typework_idx_edit = (Label)fvTypeofwork.FindControl("lbl_typework_idx_edit");

                int decision_recordsheet_edit = 20; //edit recordsheet
                int noidx_recordsheet_edit = 14; //edit recordsheet

                data_law data_u1insert_recordsheet_edit = new data_law();

                //insert record sheet fvRegistrationDetail
                tcm_u1_document_detail u1insert_recordsheet_edit = new tcm_u1_document_detail();
                data_u1insert_recordsheet_edit.tcm_u1_document_list = new tcm_u1_document_detail[1];

                u1insert_recordsheet_edit.cemp_idx = _emp_idx;
                u1insert_recordsheet_edit.u0_doc_idx = int.Parse(hfU0IDX_insertrecord_edit.Value);
                u1insert_recordsheet_edit.u1_doc_idx = int.Parse(hfU1IDX_insertrecord_edit.Value);
                u1insert_recordsheet_edit.condition = 1;
                u1insert_recordsheet_edit.acidx = int.Parse(hfACIDX_insertrecord_edit.Value);
                u1insert_recordsheet_edit.noidx = noidx_recordsheet_edit;
                u1insert_recordsheet_edit.decision = decision_recordsheet_edit;
                u1insert_recordsheet_edit.comment = "-";
                u1insert_recordsheet_edit.owner_idx = int.Parse(ddlowner_idx_edit.SelectedValue);
                u1insert_recordsheet_edit.incorporated_in_idx = int.Parse(ddlincorporated_in_idx_edit.SelectedValue);
                u1insert_recordsheet_edit.application_no = txt_application_no_edit.Text;
                u1insert_recordsheet_edit.application_date = txt_application_date_edit.Text;
                u1insert_recordsheet_edit.registration_no = txt_registration_no_edit.Text;
                u1insert_recordsheet_edit.registration_date = txt_registration_date_edit.Text;
                u1insert_recordsheet_edit.tradmark_status = int.Parse(ddlTrademarkStatus_edit.SelectedValue);
                u1insert_recordsheet_edit.tradmark_substatus = int.Parse(ddlTrademarkSubStatus_edit.SelectedValue);
                u1insert_recordsheet_edit.file_reference = tb_File_Reference_edit.Text;
                u1insert_recordsheet_edit.recode_referense = tb_Record_Reference_edit.Text;
                u1insert_recordsheet_edit.type_of_registration_idx = int.Parse(ddlTypeOfRegistrtion_edit.SelectedValue);
                //u1insert_recordsheet_edit.next_renewal_due = tb_Next_Renewal_Due_edit.Text;
                //insert u0 document fvRegistrationDetail

                //insert next renewal due

                var _datasetRenewal_edit = (DataSet)ViewState["vsRenewalList"];
                var _addRenewal_edit = new tcm_record_renewal_detail[_datasetRenewal_edit.Tables[0].Rows.Count];
                int _Renewal_edit = 0;
                if (_datasetRenewal_edit.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dtrowRenewal in _datasetRenewal_edit.Tables[0].Rows)
                    {
                        _addRenewal_edit[_Renewal_edit] = new tcm_record_renewal_detail();
                        _addRenewal_edit[_Renewal_edit].cemp_idx = _emp_idx;
                        _addRenewal_edit[_Renewal_edit].NextRenewalDue_date = dtrowRenewal["drNextRenewalDueText"].ToString();
                        _addRenewal_edit[_Renewal_edit].Selected_value = int.Parse(dtrowRenewal["drSelectedIdx"].ToString());
                        _addRenewal_edit[_Renewal_edit].Selected_name = dtrowRenewal["drSelectedText"].ToString();
                        _addRenewal_edit[_Renewal_edit].Renewal_Status = int.Parse(dtrowRenewal["drRenewalStatusIdx"].ToString());
                        _addRenewal_edit[_Renewal_edit].Renewal_SubStatus = int.Parse(dtrowRenewal["drRenewalSubStatusIdx"].ToString());

                        _Renewal_edit++;

                    }
                }

                //insert next renewal due

                //insert fvAssignment
                var _datasetAssignment_edit = (DataSet)ViewState["vsAssignmentList"];
                var _addAssignment_edit = new tcm_record_assignment_detail[_datasetAssignment_edit.Tables[0].Rows.Count];
                int _Assignment_edit = 0;
                if (_datasetAssignment_edit.Tables[0].Rows.Count > 0)
                {


                    foreach (DataRow dtrowAssignment_edit in _datasetAssignment_edit.Tables[0].Rows)
                    {
                        _addAssignment_edit[_Assignment_edit] = new tcm_record_assignment_detail();
                        _addAssignment_edit[_Assignment_edit].cemp_idx = _emp_idx;
                        _addAssignment_edit[_Assignment_edit].step_idx = int.Parse(dtrowAssignment_edit["drStepAssignmentIdx"].ToString());
                        _addAssignment_edit[_Assignment_edit].step_name = dtrowAssignment_edit["drStepAssignmentText"].ToString();
                        _addAssignment_edit[_Assignment_edit].selected_idx = int.Parse(dtrowAssignment_edit["drSelectedAssignmentIdx"].ToString());
                        _addAssignment_edit[_Assignment_edit].selected_name = dtrowAssignment_edit["drSelectedAssignmentText"].ToString();
                        _addAssignment_edit[_Assignment_edit].status = int.Parse(dtrowAssignment_edit["drStatusAssignmentIdx"].ToString());
                        _addAssignment_edit[_Assignment_edit].substatus = int.Parse(dtrowAssignment_edit["drSubStatusAssignmentIdx"].ToString());

                        _Assignment_edit++;

                    }
                }

                //insert fvAssignment

                //insert fvChangeofName
                var _datasetChangeofName_edit = (DataSet)ViewState["vsChangeofNameList"];
                var _addChangeofName_edit = new tcm_record_changeofname_detail[_datasetChangeofName_edit.Tables[0].Rows.Count];
                int _ChangeofName_edit = 0;

                if (_datasetChangeofName_edit.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dtrowChangeofName_edit in _datasetChangeofName_edit.Tables[0].Rows)
                    {
                        _addChangeofName_edit[_ChangeofName_edit] = new tcm_record_changeofname_detail();
                        _addChangeofName_edit[_ChangeofName_edit].cemp_idx = _emp_idx;
                        _addChangeofName_edit[_ChangeofName_edit].selected_idx = int.Parse(dtrowChangeofName_edit["drSelectedChangeofNameIdx"].ToString());
                        _addChangeofName_edit[_ChangeofName_edit].selected_name = dtrowChangeofName_edit["drSelectedChangeofNameText"].ToString();
                        _addChangeofName_edit[_ChangeofName_edit].status = int.Parse(dtrowChangeofName_edit["drStatusChangeofNameIdx"].ToString());
                        _addChangeofName_edit[_ChangeofName_edit].substatus = int.Parse(dtrowChangeofName_edit["drSubStatusChangeofNameIdx"].ToString());

                        _ChangeofName_edit++;

                    }
                }
                //insert fvChangeofName

                //insert fvPrioritiesDetail
                var _datasetPriorities_edit = (DataSet)ViewState["vsPrioritiesList"];
                var _addPriorities_edit = new tcm_record_priorities_detail[_datasetPriorities_edit.Tables[0].Rows.Count];
                int _Priorities_edit = 0;
                if (_datasetPriorities_edit.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dtrowPriorities_edit in _datasetPriorities_edit.Tables[0].Rows)
                    {
                        _addPriorities_edit[_Priorities_edit] = new tcm_record_priorities_detail();
                        _addPriorities_edit[_Priorities_edit].cemp_idx = _emp_idx;
                        _addPriorities_edit[_Priorities_edit].Type_of_priority = dtrowPriorities_edit["drTypeofpriorityText"].ToString();
                        _addPriorities_edit[_Priorities_edit].Priority_Date = dtrowPriorities_edit["drPriorityDateText"].ToString();
                        _addPriorities_edit[_Priorities_edit].Priority_No = dtrowPriorities_edit["drPriorityNoText"].ToString();
                        _addPriorities_edit[_Priorities_edit].country_idx = int.Parse(dtrowPriorities_edit["drCountrypriorityIdx"].ToString());
                        _addPriorities_edit[_Priorities_edit].status_idx = int.Parse(dtrowPriorities_edit["drStatuspriorityIdx"].ToString());

                        _Priorities_edit++;

                    }
                }

                //insert fvPrioritiesDetail

                //insert fvClientDetail
                var _datasetClient_edit = (DataSet)ViewState["vsClientList"];
                var _addClient_edit = new tcm_record_client_detail[_datasetClient_edit.Tables[0].Rows.Count];
                int _Client_edit = 0;
                if (_datasetClient_edit.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dtrowClient_edit in _datasetClient_edit.Tables[0].Rows)
                    {
                        _addClient_edit[_Client_edit] = new tcm_record_client_detail();
                        _addClient_edit[_Client_edit].cemp_idx = _emp_idx;
                        _addClient_edit[_Client_edit].Client_Detail = dtrowClient_edit["drClientDetailText"].ToString();
                        _addClient_edit[_Client_edit].Client_Contact = dtrowClient_edit["drClientContactText"].ToString();
                        _addClient_edit[_Client_edit].Client_Reference = dtrowClient_edit["drClientReferenceText"].ToString();
                        _addClient_edit[_Client_edit].status_idx = int.Parse(dtrowClient_edit["drClientStatusIdx"].ToString());

                        _Client_edit++;

                    }
                }

                //insert fvClientDetail

                //insert fvAgentDetail
                var _datasetAgent_edit = (DataSet)ViewState["vsAgentList"];
                var _addAgent_edit = new tcm_record_agent_detail[_datasetAgent_edit.Tables[0].Rows.Count];
                int _Agent_edit = 0;
                if (_datasetAgent_edit.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dtrowagent_edit in _datasetAgent_edit.Tables[0].Rows)
                    {
                        _addAgent_edit[_Agent_edit] = new tcm_record_agent_detail();
                        _addAgent_edit[_Agent_edit].cemp_idx = _emp_idx;
                        _addAgent_edit[_Agent_edit].Agent_Detail = dtrowagent_edit["drAgentDetailText"].ToString();
                        _addAgent_edit[_Agent_edit].Agent_Contact = dtrowagent_edit["drAgentContactText"].ToString();
                        _addAgent_edit[_Agent_edit].Agent_Reference = dtrowagent_edit["drAgentReferenceText"].ToString();
                        _addAgent_edit[_Agent_edit].status_idx = int.Parse(dtrowagent_edit["drAgentStatusIdx"].ToString());

                        _Agent_edit++;

                    }
                }

                //insert fvAgentDetail

                //insert fvGoodsDetail
                var _datasetGoods_edit = (DataSet)ViewState["vsGoodsList"];
                var _addGoods_edit = new tcm_record_goods_detail[_datasetGoods_edit.Tables[0].Rows.Count];
                int _Goods_edit = 0;
                if (_datasetGoods_edit.Tables[0].Rows.Count > 0)
                {


                    foreach (DataRow dtrowaGoods_edit in _datasetGoods_edit.Tables[0].Rows)
                    {
                        _addGoods_edit[_Goods_edit] = new tcm_record_goods_detail();
                        _addGoods_edit[_Goods_edit].cemp_idx = _emp_idx;
                        _addGoods_edit[_Goods_edit].IntClass = dtrowaGoods_edit["drGoodsIntClassText"].ToString();
                        _addGoods_edit[_Goods_edit].Goods_Detail = dtrowaGoods_edit["drGoodsDetailText"].ToString();
                        _addGoods_edit[_Goods_edit].language_idx = int.Parse(dtrowaGoods_edit["drLocallanguageIdx"].ToString());


                        _Goods_edit++;

                    }
                }

                //insert fvGoodsDetail

                //insert fvDependentDetail
                tcm_record_dependent_detail record_dependent_insert_edit = new tcm_record_dependent_detail();
                data_u1insert_recordsheet_edit.tcm_record_dependent_list = new tcm_record_dependent_detail[1];

                //insert fvAdditionalDetail
                tcm_record_additional_detail record_additional_insert_edit = new tcm_record_additional_detail();
                data_u1insert_recordsheet_edit.tcm_record_additional_list = new tcm_record_additional_detail[1];

                //insert fvBackground
                tcm_record_backgroud_detail record_backgroud_insert_edit = new tcm_record_backgroud_detail();
                data_u1insert_recordsheet_edit.tcm_record_backgroud_list = new tcm_record_backgroud_detail[1];

                //insert fvSummary
                tcm_record_summary_detail record_summary_insert_edit = new tcm_record_summary_detail();
                data_u1insert_recordsheet_edit.tcm_record_summary_list = new tcm_record_summary_detail[1];

                //insert fvClaims
                tcm_record_claims_detail record_claims_insert_edit = new tcm_record_claims_detail();
                data_u1insert_recordsheet_edit.tcm_record_claims_list = new tcm_record_claims_detail[1];

                //insert fvAbstract
                tcm_record_abstract_detail record_abstract_insert_edit = new tcm_record_abstract_detail();
                data_u1insert_recordsheet_edit.tcm_record_abstract_list = new tcm_record_abstract_detail[1];

                //insert fvDescription
                tcm_record_description_detail record_description_insert_edit = new tcm_record_description_detail();
                data_u1insert_recordsheet_edit.tcm_record_description_list = new tcm_record_description_detail[1];

                //insert fvTypeofwork
                tcm_record_typeofwork_detail record_typeofwork_insert_edit = new tcm_record_typeofwork_detail();
                data_u1insert_recordsheet_edit.tcm_record_typeofwork_list = new tcm_record_typeofwork_detail[1];


                switch (hftype_idx_insertrecord_edit.Value)
                {
                    case "1": //mark
                              //insert fvDependentDetail
                        if (tbdependent_registration_edit.Text != null)
                        {
                            record_dependent_insert_edit.cemp_idx = _emp_idx;
                            record_dependent_insert_edit.dependent_idx = int.Parse(lbl_dependent_idx_edit.Text);
                            record_dependent_insert_edit.Dependent_Registration = tbdependent_registration_edit.Text;
                        }

                        //insert fvAdditionalDetail
                        if (tbAdditionalDetail_edit.Text != null)
                        {
                            record_additional_insert_edit.cemp_idx = _emp_idx;
                            record_additional_insert_edit.additional_idx = int.Parse(lbl_additional_idx_edit.Text);
                            record_additional_insert_edit.Additional_Details = tbAdditionalDetail_edit.Text;
                        }
                        break;
                    case "7": // Industrial Design
                        //insert fvDependentDetail
                        if (tbdependent_registration_edit.Text != null)
                        {
                            record_dependent_insert_edit.cemp_idx = _emp_idx;
                            record_dependent_insert_edit.dependent_idx = int.Parse(lbl_dependent_idx_edit.Text);
                            record_dependent_insert_edit.Dependent_Registration = tbdependent_registration_edit.Text;
                        }

                        //insert fvAdditionalDetail
                        if (tbAdditionalDetail_edit.Text != null)
                        {
                            record_additional_insert_edit.cemp_idx = _emp_idx;
                            record_additional_insert_edit.additional_idx = int.Parse(lbl_additional_idx_edit.Text);
                            record_additional_insert_edit.Additional_Details = tbAdditionalDetail_edit.Text;
                        }

                        //insert fvBackground
                        if (tbBackgroundDetail_edit.Text != null)
                        {
                            record_backgroud_insert_edit.cemp_idx = _emp_idx;
                            record_backgroud_insert_edit.background_idx = int.Parse(lbl_background_idx_edit.Text); 
                            record_backgroud_insert_edit.background_detail = tbBackgroundDetail_edit.Text;
                        }

                        //insert fvSummary
                        if (tbSummaryDetail_edit.Text != null)
                        {
                            record_summary_insert_edit.cemp_idx = _emp_idx;
                            record_summary_insert_edit.summary_idx = int.Parse(lbl_summary_idx_edit.Text);
                            record_summary_insert_edit.summary_detail = tbSummaryDetail_edit.Text;
                        }

                        //insert fvClaims
                        if (tbClaimsDetail_edit.Text != null)
                        {
                            record_claims_insert_edit.cemp_idx = _emp_idx;
                            record_claims_insert_edit.claims_idx = int.Parse(lbl_claims_idx_edit.Text);
                            record_claims_insert_edit.claims_detail = tbClaimsDetail_edit.Text;
                        }

                        //insert fvDescription
                        if (tbDescriptionDetail_edit.Text != null)
                        {
                            record_description_insert_edit.cemp_idx = _emp_idx;
                            record_description_insert_edit.description_idx = int.Parse(lbl_description_idx_edit.Text);
                            record_description_insert_edit.description_detail = tbDescriptionDetail_edit.Text;
                        }

                        break;
                    case "6": //Invention/Process

                        //insert fvDependentDetail
                        if (tbdependent_registration_edit.Text != null)
                        {
                            record_dependent_insert_edit.cemp_idx = _emp_idx;
                            record_dependent_insert_edit.dependent_idx = int.Parse(lbl_dependent_idx_edit.Text);
                            record_dependent_insert_edit.Dependent_Registration = tbdependent_registration_edit.Text;
                        }

                        //insert fvAdditionalDetail
                        if (tbAdditionalDetail_edit.Text != null)
                        {
                            record_additional_insert_edit.cemp_idx = _emp_idx;
                            record_additional_insert_edit.additional_idx = int.Parse(lbl_additional_idx_edit.Text);
                            record_additional_insert_edit.Additional_Details = tbAdditionalDetail_edit.Text;
                        }

                        //insert fvBackground
                        if (tbBackgroundDetail_edit.Text != null)
                        {
                            record_backgroud_insert_edit.cemp_idx = _emp_idx;
                            record_backgroud_insert_edit.background_idx = int.Parse(lbl_background_idx_edit.Text);
                            record_backgroud_insert_edit.background_detail = tbBackgroundDetail_edit.Text;
                        }

                        //insert fvSummary
                        if (tbSummaryDetail_edit.Text != null)
                        {
                            record_summary_insert_edit.cemp_idx = _emp_idx;
                            record_summary_insert_edit.summary_idx = int.Parse(lbl_summary_idx_edit.Text);
                            record_summary_insert_edit.summary_detail = tbSummaryDetail_edit.Text;
                        }

                        //insert fvClaims
                        if (tbClaimsDetail_edit.Text != null)
                        {
                            record_claims_insert_edit.cemp_idx = _emp_idx;
                            record_claims_insert_edit.claims_idx = int.Parse(lbl_claims_idx_edit.Text);
                            record_claims_insert_edit.claims_detail = tbClaimsDetail_edit.Text;
                        }

                        break;
                    case "8": //copy right

                        //insert fvAdditionalDetail
                        if (tbAdditionalDetail_edit.Text != null)
                        {
                            record_additional_insert_edit.cemp_idx = _emp_idx;
                            record_additional_insert_edit.additional_idx = int.Parse(lbl_additional_idx_edit.Text);
                            record_additional_insert_edit.Additional_Details = tbAdditionalDetail_edit.Text;
                        }

                        //insert fvAbstract
                        if (tbAbstractDetail_edit.Text != null)
                        {
                            record_abstract_insert_edit.cemp_idx = _emp_idx;
                            record_abstract_insert_edit.abstract_idx = int.Parse(lbl_abstract_idx_edit.Text);
                            record_abstract_insert_edit.abstract_detail = tbAbstractDetail_edit.Text;
                        }

                        //insert fvTypeofwork
                        record_typeofwork_insert_edit.cemp_idx = _emp_idx;
                        record_typeofwork_insert_edit.typework_idx = int.Parse(lbl_typework_idx_edit.Text);
                        record_typeofwork_insert_edit.typeofwork_idx = int.Parse(rdoTypeofwork_edit.SelectedValue);


                        break;
                }

                //insert fvTademarkProfile
                tcm_record_trademarkprofile_detail record_trademarkprofile_insert_edit = new tcm_record_trademarkprofile_detail();
                data_u1insert_recordsheet_edit.tcm_record_trademarkprofile_list = new tcm_record_trademarkprofile_detail[1];

                record_trademarkprofile_insert_edit.cemp_idx = _emp_idx;
                record_trademarkprofile_insert_edit.trademark_profile_idx = int.Parse(lbl_trademark_profile_idx_edit.Text);
                record_trademarkprofile_insert_edit.Name = tbName_trademark_edit.Text;
                record_trademarkprofile_insert_edit.doc_language_idx = int.Parse(ddlLocal_language_trademark_edit.SelectedValue);
                record_trademarkprofile_insert_edit.Profile_Owner = tbProfileOwner_edit.Text;
                record_trademarkprofile_insert_edit.owner_language_idx = int.Parse(ddlProfileOwnerLanguage_edit.SelectedValue);
                record_trademarkprofile_insert_edit.Profile_Client = tbProfileClient_edit.Text;
                record_trademarkprofile_insert_edit.client_language_idx = int.Parse(ddlProfileClientLanguage_edit.SelectedValue);
                record_trademarkprofile_insert_edit.Translation = tbTranslation_edit.Text;
                record_trademarkprofile_insert_edit.Transliteration = tbTransliteration_edit.Text;
                record_trademarkprofile_insert_edit.Transliteration = tbTransliteration_edit.Text;
                //insert fvTademarkProfile

                data_u1insert_recordsheet_edit.tcm_record_renewal_list = _addRenewal_edit;
                data_u1insert_recordsheet_edit.tcm_record_assignment_list = _addAssignment_edit;
                data_u1insert_recordsheet_edit.tcm_record_changeofname_list = _addChangeofName_edit;

                data_u1insert_recordsheet_edit.tcm_u1_document_list[0] = u1insert_recordsheet_edit;
                data_u1insert_recordsheet_edit.tcm_record_dependent_list[0] = record_dependent_insert_edit;
                data_u1insert_recordsheet_edit.tcm_record_priorities_list = _addPriorities_edit;
                data_u1insert_recordsheet_edit.tcm_record_client_list = _addClient_edit;
                data_u1insert_recordsheet_edit.tcm_record_agent_list = _addAgent_edit;
                data_u1insert_recordsheet_edit.tcm_record_goods_list = _addGoods_edit;
                data_u1insert_recordsheet_edit.tcm_record_additional_list[0] = record_additional_insert_edit;

                data_u1insert_recordsheet_edit.tcm_record_backgroud_list[0] = record_backgroud_insert_edit;
                data_u1insert_recordsheet_edit.tcm_record_summary_list[0] = record_summary_insert_edit;
                data_u1insert_recordsheet_edit.tcm_record_claims_list[0] = record_claims_insert_edit;
                data_u1insert_recordsheet_edit.tcm_record_description_list[0] = record_description_insert_edit;
                data_u1insert_recordsheet_edit.tcm_record_typeofwork_list[0] = record_typeofwork_insert_edit;

                data_u1insert_recordsheet_edit.tcm_record_trademarkprofile_list[0] = record_trademarkprofile_insert_edit;

                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1insert_recordsheet_edit));
                data_u1insert_recordsheet_edit = callServicePostLaw(_urlSetLawRecordSheet, data_u1insert_recordsheet_edit);

                // --- insert u1 ------  //

                //ViewState["vs_trademark_idx"] = data_u1insert_recordsheet.return_code.ToString();

                FileUpload UploadFileTrademark_Edit = (FileUpload)fvTademarkProfile.FindControl("UploadFileTrademark_Edit");

                if (UploadFileTrademark_Edit.HasFile)
                {
                    //litDebug.Text = "1";

                    string filepath_trademark = Server.MapPath(_path_file_law_trademarks_profile);
                    HttpFileCollection uploadedFiles_edittrademark = Request.Files;
                    string filePath2 = "trademark" + "-" + lbl_trademark_profile_idx_edit.Text;
                    string filePath1 = Server.MapPath(_path_file_law_trademarks_profile + filePath2);

                    //check file in directory in edit file memo
                    if (Directory.Exists(filePath1))
                    {
                        string[] filePaths = Directory.GetFiles(filePath1);
                        foreach (string filePath in filePaths)
                            File.Delete(filePath);
                    }

                    for (int i = 0; i < uploadedFiles_edittrademark.Count; i++)
                    {
                        HttpPostedFile userpost_edittrademark = uploadedFiles_edittrademark[i];

                        try
                        {
                            if (userpost_edittrademark.ContentLength > 0)
                            {
                                //litDebug.Text = "1";
                                //litDebug.Text += "MA620001";

                                string _filepathExtension = Path.GetExtension(userpost_edittrademark.FileName);
                                if (!Directory.Exists(filePath1))
                                {
                                    Directory.CreateDirectory(filePath1);
                                    //litDebug.Text += "create file" + (i + 1) + "|" + "<br>";
                                }
                                else
                                {

                                }
                                ////litDebug.Text += "File Content Type: " + userpost_createfile.ContentType + "<br>";// data_insert_otmonth.ovt_u0_document_list[0].u0_doc_idx.ToString()
                                ////litDebug.Text += "File Size: " + userpost_createfile.ContentLength + "kb<br>";
                                ////litDebug.Text += "File Name: " + userpost_createfile.FileName + "<br>";
                                ////litDebug.Text += (filepath_usercreate + filePath2 + "\\" + "MA620001" + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());

                                userpost_edittrademark.SaveAs(filepath_trademark + filePath2 + "\\" + lbl_trademark_profile_idx_edit.Text + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                //////litDebug.Text += (filepath + filePath12 + "\\" + "1209" + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                //////userpost_createfile.SaveAs(filePath1 + "\\" + filePath2 + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                //userpost_createfile.SaveAs(filepath_usercreate + filePath2 + "\\" + filePath2.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                //////litDebug.Text += "Location where saved: " + filepath + "\\" + Path.GetFileName(userPostedFile.FileName) + "<p>";


                            }
                        }
                        catch (Exception Ex)
                        {
                            //litDebug.Text += "Error: <br>" + Ex.Message;
                        }
                    }

                }
                else
                {
                    //litDebug.Text = "2";
                }

                //insert record sheet fvPublicationDetail
                if (tbPublication_Date_edit.Text != "")
                {
                    data_law data_publication_recordsheet = new data_law();
                    tcm_record_publication_detail record_publication_insert = new tcm_record_publication_detail();
                    data_publication_recordsheet.tcm_record_publication_list = new tcm_record_publication_detail[1];
                    record_publication_insert.u1_doc_idx = int.Parse(hfU1IDX_insertrecord_edit.Value);
                    record_publication_insert.publication_idx = int.Parse(txt_publication_idx_edit.Text);
                    record_publication_insert.cemp_idx = _emp_idx;
                    record_publication_insert.Publication_Date = tbPublication_Date_edit.Text;
                    record_publication_insert.NoticeofAllowance_Date = tbGrant_date_edit.Text;
                    record_publication_insert.Journal_Volume = tbJournal_Volume_edit.Text;
                    record_publication_insert.PubRegDate_Date = tb_Pub_Date_edit.Text;
                    record_publication_insert.Registry_Reference = tb_Registry_Reference_edit.Text;
                    record_publication_insert.FinalOfficeAction_Date = tbFinal_Office_Date_edit.Text;
                    record_publication_insert.OfficeAction_Date = tb_Office_Action_Date_edit.Text;
                    record_publication_insert.Date_DeclarationFiled = tb_Date_Declaration_Filed_edit.Text;

                    data_publication_recordsheet.tcm_record_publication_list[0] = record_publication_insert;
                    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_publication_recordsheet));
                    data_publication_recordsheet = callServicePostLaw(_urlSetLawRecordSheetPublication, data_publication_recordsheet);

                    ViewState["vs_publication_idx"] = int.Parse(txt_publication_idx_edit.Text);
                    GridView gvDeadlineInsert = (GridView)fvPublicationDetail.FindControl("gvDeadlineInsert");

                    if(gvDeadlineInsert.Rows.Count > 0) //check have data set deadline
                    {
                        data_law data_deadline_recordsheet = new data_law();
                        var _record_deadline_detail = new tcm_record_deadline_detail[gvDeadlineInsert.Rows.Count];
                        int i = 0;
                        //var _record_actions_detail = new tcm_record_actions_detail[0];
                        foreach (GridViewRow gvr_gvDeadline in gvDeadlineInsert.Rows)
                        {
                            GridView gvActionsDetail = (GridView)gvr_gvDeadline.FindControl("gvActionsDetail");
                            Label lbl_drOfficeActionDateText = (Label)gvr_gvDeadline.FindControl("lbl_drOfficeActionDateText");
                            Label lbl_drSelectedDeadlineIdx = (Label)gvr_gvDeadline.FindControl("lbl_drSelectedDeadlineIdx");
                            Label lbl_drSelectedDeadlineText = (Label)gvr_gvDeadline.FindControl("lbl_drSelectedDeadlineText");

                            Label lbl_drSelectedActionListDeadlineText = (Label)gvr_gvDeadline.FindControl("lbl_drSelectedActionListDeadlineText");
                            Label lbl_drSelectedActionListDeadlineIdx = (Label)gvr_gvDeadline.FindControl("lbl_drSelectedActionListDeadlineIdx");

                            _record_deadline_detail[i] = new tcm_record_deadline_detail();
                            _record_deadline_detail[i].cemp_idx = _emp_idx;
                            _record_deadline_detail[i].publication_idx = int.Parse(ViewState["vs_publication_idx"].ToString());
                            _record_deadline_detail[i].deadline_date = lbl_drOfficeActionDateText.Text;
                            _record_deadline_detail[i].deadline_staus = int.Parse(lbl_drSelectedDeadlineIdx.Text);
                            _record_deadline_detail[i].deadline_stausname = lbl_drSelectedDeadlineText.Text;
                            _record_deadline_detail[i].action_detail_idx_value = lbl_drSelectedActionListDeadlineIdx.Text;


                            //int i = 0;
                            //var _u2doc_node3 = new qa_lab_u2_qalab[rp_test_sample.Items.Count];
                            //int count_u2doc = 0;

                            var _record_actions_detail = new tcm_record_actions_detail[gvActionsDetail.Rows.Count];
                            //data_u1insert_recordsheet.tcm_record_actions_list = new tcm_record_actions_detail[1];
                            int sumcheck_record = 0;
                            int count_record = 0;
                            foreach (GridViewRow gvrow in gvActionsDetail.Rows)
                            {


                                TextBox tbActionsDetail = (TextBox)gvrow.FindControl("tbActionsDetail");
                                TextBox tbOwnerDetail = (TextBox)gvrow.FindControl("tbOwnerDetail");
                                Label lblaction_idx = (Label)gvrow.FindControl("lblaction_idx");

                                _record_actions_detail[count_record] = new tcm_record_actions_detail();
                                _record_actions_detail[count_record].cemp_idx = _emp_idx;
                                _record_actions_detail[count_record].deadline_date = lbl_drOfficeActionDateText.Text;
                                _record_actions_detail[count_record].Actions_Detail = tbActionsDetail.Text;
                                _record_actions_detail[count_record].Owner_Detail = tbOwnerDetail.Text;
                                _record_actions_detail[count_record].action_idx = int.Parse(lblaction_idx.Text);

                                sumcheck_record = sumcheck_record + 1;

                                count_record++;

                            }

                            //data_publication_recordsheet.tcm_record_actions_list = _record_actions_detail;

                            //data_u1insert_recordsheet.tcm_record_actions_list = _record_actions_detail;
                            //i++;
                            //
                            data_deadline_recordsheet.tcm_record_deadline_list = _record_deadline_detail;
                            data_deadline_recordsheet.tcm_record_actions_list = _record_actions_detail;

                            //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_deadline_recordsheet));
                            data_deadline_recordsheet = callServicePostLaw(_urlSetLawRecordSheetDeadline, data_deadline_recordsheet);
                        }
                        //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1insert_recordsheet));
                        //insert action detail
                    }

                }
                //insert record sheet fvPublicationDetail

                ////// ---------  //

               setActiveTab("docRecordSheet", 0, 0, 0, 0, 0, 0, 0, 0);

                break;

            case "CmdDeleteRenewal":
                int renewal_idx_del = int.Parse(cmdArg);


                HiddenField hfU1IDX = (HiddenField)fvRegistrationDetail.FindControl("hfU1IDX");
                TextBox tbtype_idx_viewdetail_del = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                GridView gvDetailRenewal = (GridView)fvRegistrationDetail.FindControl("gvDetailRenewal");
                

                data_law data_renewal_detail_del = new data_law();
                tcm_record_renewal_detail m0_renewal_detail_del = new tcm_record_renewal_detail();
                data_renewal_detail_del.tcm_record_renewal_list = new tcm_record_renewal_detail[1];

                m0_renewal_detail_del.renewal_idx = renewal_idx_del;
                m0_renewal_detail_del.cemp_idx = _emp_idx;
                m0_renewal_detail_del.condition = 1;
                m0_renewal_detail_del.status = 9;

                data_renewal_detail_del.tcm_record_renewal_list[0] = m0_renewal_detail_del;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_renewal_detail_del));
                data_renewal_detail_del = callServicePostLaw(_urlSetUpdateLawRenewal, data_renewal_detail_del);

                getViewRecordsheet(int.Parse(hfU1IDX.Value), int.Parse(tbtype_idx_viewdetail_del.Text));

                tcm_record_renewal_detail[] _templist_renewal_detail_del = (tcm_record_renewal_detail[])ViewState["vs_renewal_recordsheet"];
                var _linq_renewal_detail_del = (from dt in _templist_renewal_detail_del
                                     
                                      select dt).ToList();


                //setGridData(gvDetailRenewal, ViewState["vs_renewal_recordsheet"]);
                //renewal date detail

                if (_linq_renewal_detail_del[0].u1_doc_idx == 0)
                {
                    //litDebug1.Text = "1";
                    ViewState["vs_renewal_recordsheet"] = null;
                    setGridData(gvDetailRenewal, ViewState["vs_renewal_recordsheet"]);
                }
                else
                {
                    //litDebug1.Text = "2";
                    //ViewState["vs_renewal_recordsheet"] = data_u1_document_viewrecord.tcm_record_agent_list;
                    setGridData(gvDetailRenewal, ViewState["vs_renewal_recordsheet"]);
                }

                break;
            case "CmdDeleteAssignment":
                int assignment_idx_del = int.Parse(cmdArg);


                HiddenField hfU1IDX_assignment_del = (HiddenField)fvRegistrationDetail.FindControl("hfU1IDX");
                TextBox tbtype_idx_assignment_del = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                GridView gvDetailAssignment = (GridView)fvAssignment.FindControl("gvDetailAssignment");

                data_law data_assignment_detail_del = new data_law();
                tcm_record_assignment_detail m0_assignment_del = new tcm_record_assignment_detail();
                data_assignment_detail_del.tcm_record_assignment_list = new tcm_record_assignment_detail[1];

                m0_assignment_del.cemp_idx = _emp_idx;
                m0_assignment_del.assignment_idx = assignment_idx_del;
                m0_assignment_del.staidx = 9;
                m0_assignment_del.condition = 1;

                data_assignment_detail_del.tcm_record_assignment_list[0] = m0_assignment_del;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_assignment_detail_edit));
                data_assignment_detail_del = callServicePostLaw(_urlSetUpdateLawAssignment, data_assignment_detail_del);

                getViewRecordsheet(int.Parse(hfU1IDX_assignment_del.Value), int.Parse(tbtype_idx_assignment_del.Text));

                tcm_record_assignment_detail[] _templist_assignment_detail_del = (tcm_record_assignment_detail[])ViewState["vs_assignment_recordsheet"];
                var _linq_assignment_detail_del = (from dt in _templist_assignment_detail_del

                                                select dt).ToList();


                if (_linq_assignment_detail_del[0].u1_doc_idx == 0)
                {
                    //litDebug1.Text = "1";
                    ViewState["vs_assignment_recordsheet"] = null;
                    setGridData(gvDetailAssignment, ViewState["vs_assignment_recordsheet"]);
                }
                else
                {
                    //litDebug1.Text = "2";
                    //ViewState["vs_renewal_recordsheet"] = data_u1_document_viewrecord.tcm_record_agent_list;
                    setGridData(gvDetailAssignment, ViewState["vs_assignment_recordsheet"]);
                }

                break;
            case "CmdDeleteChangeofname":
                int changeofname_idx_del = int.Parse(cmdArg);


                HiddenField hfU1IDX_changeofname_del = (HiddenField)fvRegistrationDetail.FindControl("hfU1IDX");
                TextBox tbtype_idx_changeofname_del = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                GridView gvDetailChangeofName = (GridView)fvChangeofName.FindControl("gvDetailChangeofName");

                data_law data_changeofname_detail_del = new data_law();
                tcm_record_changeofname_detail m0_changeofname_del = new tcm_record_changeofname_detail();
                data_changeofname_detail_del.tcm_record_changeofname_list = new tcm_record_changeofname_detail[1];

                m0_changeofname_del.changeofname_idx = changeofname_idx_del;
                m0_changeofname_del.cemp_idx = _emp_idx;
                m0_changeofname_del.staidx = 9;
                m0_changeofname_del.condition = 1;

                data_changeofname_detail_del.tcm_record_changeofname_list[0] = m0_changeofname_del;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_changeofname_detail_edit));
                data_changeofname_detail_del = callServicePostLaw(_urlSetUpdateLawChangeofname, data_changeofname_detail_del);

                getViewRecordsheet(int.Parse(hfU1IDX_changeofname_del.Value), int.Parse(tbtype_idx_changeofname_del.Text));

                //setGridData(gvDetailChangeofName, ViewState["vs_changeofname_recordsheet"]);

                tcm_record_changeofname_detail[] _templist_changeofname_detail_del = (tcm_record_changeofname_detail[])ViewState["vs_changeofname_recordsheet"];
                var _linq_changeofname_detail_del = (from dt in _templist_changeofname_detail_del

                                                     select dt).ToList();


                if (_linq_changeofname_detail_del[0].u1_doc_idx == 0)
                {
                    //litDebug1.Text = "1";
                    ViewState["vs_changeofname_recordsheet"] = null;
                    setGridData(gvDetailChangeofName, ViewState["vs_changeofname_recordsheet"]);
                }
                else
                {
                    //litDebug1.Text = "2";
                    //ViewState["vs_renewal_recordsheet"] = data_u1_document_viewrecord.tcm_record_agent_list;
                    setGridData(gvDetailChangeofName, ViewState["vs_changeofname_recordsheet"]);
                }

                break;
            case "CmdDeleteTypeofpriority":
                int priority_idx_del = int.Parse(cmdArg);


                HiddenField hfU1IDX_priority_del = (HiddenField)fvRegistrationDetail.FindControl("hfU1IDX");
                TextBox tbtype_idx_priority_del = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                GridView gvDetailPriorities = (GridView)fvPrioritiesDetail.FindControl("gvDetailPriorities");

                data_law data_priorities_detail_edit = new data_law();
                tcm_record_priorities_detail m0_priorities_edit = new tcm_record_priorities_detail();
                data_priorities_detail_edit.tcm_record_priorities_list = new tcm_record_priorities_detail[1];

                m0_priorities_edit.cemp_idx = _emp_idx;
                m0_priorities_edit.typeofpriority_idx = priority_idx_del;
                m0_priorities_edit.status = 9;
                m0_priorities_edit.condition = 1;

                data_priorities_detail_edit.tcm_record_priorities_list[0] = m0_priorities_edit;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_changeofname_detail_edit));
                data_priorities_detail_edit = callServicePostLaw(_urlSetUpdateLawPriorities, data_priorities_detail_edit);

                getViewRecordsheet(int.Parse(hfU1IDX_priority_del.Value), int.Parse(tbtype_idx_priority_del.Text));

                //setGridData(gvDetailChangeofName, ViewState["vs_changeofname_recordsheet"]);

                tcm_record_priorities_detail[] _templist_priority_detail_del = (tcm_record_priorities_detail[])ViewState["vs_priorities_recordsheet"];
                var _linq_priority_detail_del = (from dt in _templist_priority_detail_del

                                                 select dt).ToList();


                if (_linq_priority_detail_del[0].u1_doc_idx == 0)
                {
                    //litDebug1.Text = "1";
                    ViewState["vs_priorities_recordsheet"] = null;
                    setGridData(gvDetailPriorities, ViewState["vs_priorities_recordsheet"]);
                }
                else
                {
                    //litDebug1.Text = "2";
                    //ViewState["vs_renewal_recordsheet"] = data_u1_document_viewrecord.tcm_record_agent_list;
                    setGridData(gvDetailPriorities, ViewState["vs_priorities_recordsheet"]);
                }

                break;
            case "CmdDeleteClient":
                int Client_idx_del = int.Parse(cmdArg);


                HiddenField hfU1IDX_Client_del = (HiddenField)fvRegistrationDetail.FindControl("hfU1IDX");
                TextBox tbtype_idx_Client_del = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                GridView gvDetailClient = (GridView)fvClientDetail.FindControl("gvDetailClient");

                data_law data_client_detail_edit = new data_law();
                tcm_record_client_detail m0_client_edit = new tcm_record_client_detail();
                data_client_detail_edit.tcm_record_client_list = new tcm_record_client_detail[1];

                m0_client_edit.cemp_idx = _emp_idx;
                m0_client_edit.client_idx = Client_idx_del;
                m0_client_edit.status = 9;
                m0_client_edit.condition = 1;

                data_client_detail_edit.tcm_record_client_list[0] = m0_client_edit;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_changeofname_detail_edit));
                data_client_detail_edit = callServicePostLaw(_urlSetUpdateLawClient, data_client_detail_edit);

                getViewRecordsheet(int.Parse(hfU1IDX_Client_del.Value), int.Parse(tbtype_idx_Client_del.Text));

                //setGridData(gvDetailChangeofName, ViewState["vs_changeofname_recordsheet"]);

                tcm_record_client_detail[] _templist_client_detail_del = (tcm_record_client_detail[])ViewState["vs_client_recordsheet"];
                var _linq_client_detail_del = (from dt in _templist_client_detail_del

                                               select dt).ToList();


                if (_linq_client_detail_del[0].u1_doc_idx == 0)
                {
                    //litDebug1.Text = "1";
                    ViewState["vs_client_recordsheet"] = null;
                    setGridData(gvDetailClient, ViewState["vs_client_recordsheet"]);
                }
                else
                {
                    //litDebug1.Text = "2";
                    //ViewState["vs_renewal_recordsheet"] = data_u1_document_viewrecord.tcm_record_agent_list;
                    setGridData(gvDetailClient, ViewState["vs_client_recordsheet"]);
                }

                break;
            case "CmdDeleteAgent":
                int Agent_idx_del = int.Parse(cmdArg);


                HiddenField hfU1IDX_Agent_del = (HiddenField)fvRegistrationDetail.FindControl("hfU1IDX");
                TextBox tbtype_idx_Agent_del = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                GridView gvDetailAgent = (GridView)fvAgentDetail.FindControl("gvDetailAgent");

                data_law data_agent_detail_del = new data_law();
                tcm_record_agent_detail m0_agent_del = new tcm_record_agent_detail();
                data_agent_detail_del.tcm_record_agent_list = new tcm_record_agent_detail[1];

                m0_agent_del.cemp_idx = _emp_idx;
                m0_agent_del.agent_idx = Agent_idx_del;
                m0_agent_del.status = 9;
                m0_agent_del.condition = 1;

                data_agent_detail_del.tcm_record_agent_list[0] = m0_agent_del;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_changeofname_detail_edit));
                data_agent_detail_del = callServicePostLaw(_urlSetUpdateLawAgent, data_agent_detail_del);

                getViewRecordsheet(int.Parse(hfU1IDX_Agent_del.Value), int.Parse(tbtype_idx_Agent_del.Text));

                //setGridData(gvDetailChangeofName, ViewState["vs_changeofname_recordsheet"]);

                tcm_record_agent_detail[] _templist_agent_detail_del = (tcm_record_agent_detail[])ViewState["vs_agent_recordsheet"];
                var _linq_agent_detail_del = (from dt in _templist_agent_detail_del

                                               select dt).ToList();


                if (_linq_agent_detail_del[0].u1_doc_idx == 0)
                {
                    //litDebug1.Text = "1";
                    ViewState["vs_agent_recordsheet"] = null;
                    setGridData(gvDetailAgent, ViewState["vs_agent_recordsheet"]);
                }
                else
                {
                    //litDebug1.Text = "2";
                    //ViewState["vs_renewal_recordsheet"] = data_u1_document_viewrecord.tcm_record_agent_list;
                    setGridData(gvDetailAgent, ViewState["vs_agent_recordsheet"]);
                }

                break;
            case "CmdDeleteGoods":
                int Goods_idx_del = int.Parse(cmdArg);


                HiddenField hfU1IDX_Goods_del = (HiddenField)fvRegistrationDetail.FindControl("hfU1IDX");
                TextBox tbtype_idx_Goods_del = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                GridView gvDetailGood = (GridView)fvGoodsDetail.FindControl("gvDetailGood");

                data_law data_goods_detail_del = new data_law();
                tcm_record_goods_detail m0_goods_del = new tcm_record_goods_detail();
                data_goods_detail_del.tcm_record_goods_list = new tcm_record_goods_detail[1];

                m0_goods_del.cemp_idx = _emp_idx;
                m0_goods_del.goods_idx = Goods_idx_del;
                m0_goods_del.status = 9;
                m0_goods_del.condition = 1;

                data_goods_detail_del.tcm_record_goods_list[0] = m0_goods_del;

                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_changeofname_detail_edit));
                data_goods_detail_del = callServicePostLaw(_urlSetUpdateLawGoods, data_goods_detail_del);

                getViewRecordsheet(int.Parse(hfU1IDX_Goods_del.Value), int.Parse(tbtype_idx_Goods_del.Text));

                //setGridData(gvDetailChangeofName, ViewState["vs_changeofname_recordsheet"]);

                tcm_record_goods_detail[] _templist_goods_detail_del = (tcm_record_goods_detail[])ViewState["vs_good_recordsheet"];
                var _linq_goods_detail_del = (from dt in _templist_goods_detail_del

                                              select dt).ToList();


                if (_linq_goods_detail_del[0].u1_doc_idx == 0)
                {
                    //litDebug1.Text = "1";
                    ViewState["vs_good_recordsheet"] = null;
                    setGridData(gvDetailGood, ViewState["vs_good_recordsheet"]);
                }
                else
                {
                    //litDebug1.Text = "2";
                    //ViewState["vs_renewal_recordsheet"] = data_u1_document_viewrecord.tcm_record_agent_list;
                    setGridData(gvDetailGood, ViewState["vs_good_recordsheet"]);
                }

                break;
            case "CmdDeletedeadline":
                int deadline_idx_del = int.Parse(cmdArg);

                TextBox txt_publication_idx = (TextBox)fvPublicationDetail.FindControl("txt_publication_idx");
                HiddenField hfU1IDX_Deadline_del = (HiddenField)fvRegistrationDetail.FindControl("hfU1IDX");
                TextBox tbtype_idx_Deadline_del = (TextBox)fvRegistrationDetail.FindControl("tbtype_idx_viewdetail");

                GridView gvDetailDeadline = (GridView)fvPublicationDetail.FindControl("gvDetailDeadline");
                TextBox txt_publication_idx_del = (TextBox)fvPublicationDetail.FindControl("txt_publication_idx");

                data_law data_deadline_detail_del = new data_law();
                tcm_record_deadline_detail m0_deadline_del = new tcm_record_deadline_detail();
                data_deadline_detail_del.tcm_record_deadline_list = new tcm_record_deadline_detail[1];

                m0_deadline_del.cemp_idx = _emp_idx;
                m0_deadline_del.deadline_idx = deadline_idx_del;
                m0_deadline_del.status = 9;
                m0_deadline_del.condition = 1;

                data_deadline_detail_del.tcm_record_deadline_list[0] = m0_deadline_del;


                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_changeofname_detail_edit));
                data_deadline_detail_del = callServicePostLaw(_urlSetUpdateLawDeadline, data_deadline_detail_del);

                getViewRecordsheetPublication(int.Parse(hfU1IDX_Deadline_del.Value), int.Parse(txt_publication_idx_del.Text));

                ////getViewRecordsheet(int.Parse(hfU1IDX_Goods_del.Value), int.Parse(tbtype_idx_Goods_del.Text));

                //////setGridData(gvDetailChangeofName, ViewState["vs_changeofname_recordsheet"]);

                tcm_record_deadline_detail[] _templist_deadline_detail_del = (tcm_record_deadline_detail[])ViewState["vs_DeadlineDetail"];
                var _linq_deadline_detail_del = (from dt in _templist_deadline_detail_del

                                              select dt).ToList();


                if (_linq_deadline_detail_del[0].deadline_idx == 0)
                {
                    //litDebug1.Text = "1";
                    ViewState["vs_DeadlineDetail"] = null;
                    setGridData(gvDetailDeadline, ViewState["vs_DeadlineDetail"]);
                }
                else
                {
                    //litDebug1.Text = "2";
                    //ViewState["vs_renewal_recordsheet"] = data_u1_document_viewrecord.tcm_record_agent_list;
                    setGridData(gvDetailDeadline, ViewState["vs_DeadlineDetail"]);
                }



                break;

        }

    }
    //endbtn

    #endregion event command

    #region set data table
    //piorities
    protected void getPrioritiesList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsPrioritiesList = new DataSet();
        dsPrioritiesList.Tables.Add("dsPrioritiesListTable");

        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drTypeofpriorityText", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drPriorityDateText", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drPriorityNoText", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drCountrypriorityIdx", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drCountrypriorityText", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drStatuspriorityIdx", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drStatuspriorityText", typeof(String));

        ViewState["vsPrioritiesList"] = dsPrioritiesList;
    }

    protected void setPrioritiesList()
    {
        if (ViewState["vsPrioritiesList"] != null)
        {
            TextBox tbType_of_priority = (TextBox)fvPrioritiesDetail.FindControl("tbType_of_priority");
            TextBox tb_Priority_Date = (TextBox)fvPrioritiesDetail.FindControl("tb_Priority_Date");
            TextBox tb_Priority_No = (TextBox)fvPrioritiesDetail.FindControl("tb_Priority_No");
            DropDownList ddlCountry_priority = (DropDownList)fvPrioritiesDetail.FindControl("ddlCountry_priority");
            DropDownList ddlStatus_priority = (DropDownList)fvPrioritiesDetail.FindControl("ddlStatus_priority");
            GridView gvPrioritiesList = (GridView)fvPrioritiesDetail.FindControl("gvPrioritiesList");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsPrioritiesList"];

            foreach (DataRow dr in dsContacts.Tables["dsPrioritiesListTable"].Rows)
            {
                if (dr["drTypeofpriorityText"].ToString() == tbType_of_priority.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Check Data Again ---');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsPrioritiesListTable"].NewRow();
            drContacts["drTypeofpriorityText"] = tbType_of_priority.Text;
            drContacts["drPriorityDateText"] = tb_Priority_Date.Text;
            drContacts["drPriorityNoText"] = tb_Priority_No.Text;
            drContacts["drCountrypriorityIdx"] = ddlCountry_priority.SelectedValue;
            drContacts["drCountrypriorityText"] = ddlCountry_priority.SelectedItem.Text;
            drContacts["drStatuspriorityIdx"] = ddlStatus_priority.SelectedValue;
            drContacts["drStatuspriorityText"] = ddlStatus_priority.SelectedItem.Text;

            dsContacts.Tables["dsPrioritiesListTable"].Rows.Add(drContacts);
            ViewState["vsPrioritiesList"] = dsContacts;


            setGridData(gvPrioritiesList, dsContacts.Tables["dsPrioritiesListTable"]);
            gvPrioritiesList.Visible = true;
        }
    }

    protected void CleardataSetPrioritiesList()
    {
        var gvPrioritiesList = (GridView)fvPrioritiesDetail.FindControl("gvPrioritiesList");
        ViewState["vsPrioritiesList"] = null;
        gvPrioritiesList.DataSource = ViewState["vsPrioritiesList"];
        gvPrioritiesList.DataBind();
        getPrioritiesList();
    }

    //client
    protected void getClientList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsClientList = new DataSet();
        dsClientList.Tables.Add("dsClientListTable");

        dsClientList.Tables["dsClientListTable"].Columns.Add("drClientDetailText", typeof(String));
        dsClientList.Tables["dsClientListTable"].Columns.Add("drClientContactText", typeof(String));
        dsClientList.Tables["dsClientListTable"].Columns.Add("drClientReferenceText", typeof(String));
        dsClientList.Tables["dsClientListTable"].Columns.Add("drClientStatusIdx", typeof(String));
        dsClientList.Tables["dsClientListTable"].Columns.Add("drClientStatusText", typeof(String));

        ViewState["vsClientList"] = dsClientList;
    }

    protected void setClientList()
    {
        if (ViewState["vsClientList"] != null)
        {
            TextBox tbClientDetail = (TextBox)fvClientDetail.FindControl("tbClientDetail");
            TextBox tbClientContact = (TextBox)fvClientDetail.FindControl("tbClientContact");
            TextBox tbClientReference = (TextBox)fvClientDetail.FindControl("tbClientReference");
            DropDownList ddlClientStatus = (DropDownList)fvClientDetail.FindControl("ddlClientStatus");
            GridView gvClientList = (GridView)fvClientDetail.FindControl("gvClientList");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsClientList"];

            foreach (DataRow dr in dsContacts.Tables["dsClientListTable"].Rows)
            {
                if (dr["drClientDetailText"].ToString() == tbClientDetail.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Check Data Again ---');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsClientListTable"].NewRow();
            drContacts["drClientDetailText"] = tbClientDetail.Text;
            drContacts["drClientContactText"] = tbClientContact.Text;
            drContacts["drClientReferenceText"] = tbClientReference.Text;
            drContacts["drClientStatusIdx"] = ddlClientStatus.SelectedValue;
            drContacts["drClientStatusText"] = ddlClientStatus.SelectedItem.Text;

            dsContacts.Tables["dsClientListTable"].Rows.Add(drContacts);
            ViewState["vsClientList"] = dsContacts;


            setGridData(gvClientList, dsContacts.Tables["dsClientListTable"]);
            gvClientList.Visible = true;
        }
    }

    protected void CleardataSetClientList()
    {
        var gvClientList = (GridView)fvClientDetail.FindControl("gvClientList");
        ViewState["vsClientList"] = null;
        gvClientList.DataSource = ViewState["vsClientList"];
        gvClientList.DataBind();
        getClientList();
    }

    //agent
    protected void getAgentList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsAgentList = new DataSet();
        dsAgentList.Tables.Add("dsAgentListTable");

        dsAgentList.Tables["dsAgentListTable"].Columns.Add("drAgentDetailText", typeof(String));
        dsAgentList.Tables["dsAgentListTable"].Columns.Add("drAgentContactText", typeof(String));
        dsAgentList.Tables["dsAgentListTable"].Columns.Add("drAgentReferenceText", typeof(String));
        dsAgentList.Tables["dsAgentListTable"].Columns.Add("drAgentStatusIdx", typeof(String));
        dsAgentList.Tables["dsAgentListTable"].Columns.Add("drAgentStatusText", typeof(String));

        ViewState["vsAgentList"] = dsAgentList;
    }

    protected void setAgentList()
    {
        if (ViewState["vsAgentList"] != null)
        {
            TextBox tbAgentDetail = (TextBox)fvAgentDetail.FindControl("tbAgentDetail");
            TextBox tbAgentContact = (TextBox)fvAgentDetail.FindControl("tbAgentContact");
            TextBox tbAgentReference = (TextBox)fvAgentDetail.FindControl("tbAgentReference");
            DropDownList ddlAgentStatus = (DropDownList)fvAgentDetail.FindControl("ddlAgentStatus");
            GridView gvAgentList = (GridView)fvAgentDetail.FindControl("gvAgentList");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsAgentList"];

            foreach (DataRow dr in dsContacts.Tables["dsAgentListTable"].Rows)
            {
                if (dr["drAgentDetailText"].ToString() == tbAgentDetail.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Check Data Again ---');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsAgentListTable"].NewRow();
            drContacts["drAgentDetailText"] = tbAgentDetail.Text;
            drContacts["drAgentContactText"] = tbAgentContact.Text;
            drContacts["drAgentReferenceText"] = tbAgentReference.Text;
            drContacts["drAgentStatusIdx"] = ddlAgentStatus.SelectedValue;
            drContacts["drAgentStatusText"] = ddlAgentStatus.SelectedItem.Text;

            dsContacts.Tables["dsAgentListTable"].Rows.Add(drContacts);
            ViewState["vsAgentList"] = dsContacts;


            setGridData(gvAgentList, dsContacts.Tables["dsAgentListTable"]);
            gvAgentList.Visible = true;
        }
    }

    protected void CleardataSetAgentList()
    {
        var gvAgentList = (GridView)fvAgentDetail.FindControl("gvAgentList");
        ViewState["vsAgentList"] = null;
        gvAgentList.DataSource = ViewState["vsAgentList"];
        gvAgentList.DataBind();
        getAgentList();
    }

    //good
    protected void getGoodsList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsGoodsList = new DataSet();
        dsGoodsList.Tables.Add("dsGoodsListTable");

        dsGoodsList.Tables["dsGoodsListTable"].Columns.Add("drGoodsIntClassText", typeof(String));
        dsGoodsList.Tables["dsGoodsListTable"].Columns.Add("drGoodsDetailText", typeof(String));
        dsGoodsList.Tables["dsGoodsListTable"].Columns.Add("drLocallanguageIdx", typeof(String));
        dsGoodsList.Tables["dsGoodsListTable"].Columns.Add("drLocallanguageText", typeof(String));


        ViewState["vsGoodsList"] = dsGoodsList;
    }

    protected void setGoodsList()
    {
        if (ViewState["vsGoodsList"] != null)
        {
            TextBox tbGoods_IntClass = (TextBox)fvGoodsDetail.FindControl("tbGoods_IntClass");
            TextBox tbGoodsDetail = (TextBox)fvGoodsDetail.FindControl("tbGoodsDetail");
            DropDownList ddlLocal_language = (DropDownList)fvGoodsDetail.FindControl("ddlLocal_language");
            GridView gvGoodsList = (GridView)fvGoodsDetail.FindControl("gvGoodsList");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsGoodsList"];

            foreach (DataRow dr in dsContacts.Tables["dsGoodsListTable"].Rows)
            {
                if (dr["drGoodsIntClassText"].ToString() == tbGoods_IntClass.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Check Data Again ---');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsGoodsListTable"].NewRow();
            drContacts["drGoodsIntClassText"] = tbGoods_IntClass.Text;
            drContacts["drGoodsDetailText"] = tbGoodsDetail.Text;
            drContacts["drLocallanguageIdx"] = ddlLocal_language.SelectedValue;
            drContacts["drLocallanguageText"] = ddlLocal_language.SelectedItem.Text;

            dsContacts.Tables["dsGoodsListTable"].Rows.Add(drContacts);
            ViewState["vsGoodsList"] = dsContacts;


            setGridData(gvGoodsList, dsContacts.Tables["dsGoodsListTable"]);
            gvGoodsList.Visible = true;
        }
    }


    protected void CleardataSetGoodsList()
    {
        var gvGoodsList = (GridView)fvGoodsDetail.FindControl("gvGoodsList");
        ViewState["vsGoodsList"] = null;
        gvGoodsList.DataSource = ViewState["vsGoodsList"];
        gvGoodsList.DataBind();
        getGoodsList();
    }


    //renewal date
    protected void getRenewalList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsRenewalList = new DataSet();
        dsRenewalList.Tables.Add("dsRenewalListTable");

        dsRenewalList.Tables["dsRenewalListTable"].Columns.Add("drNextRenewalDueText", typeof(String));
        dsRenewalList.Tables["dsRenewalListTable"].Columns.Add("drSelectedIdx", typeof(String));
        dsRenewalList.Tables["dsRenewalListTable"].Columns.Add("drSelectedText", typeof(String));
        dsRenewalList.Tables["dsRenewalListTable"].Columns.Add("drRenewalStatusIdx", typeof(String));
        dsRenewalList.Tables["dsRenewalListTable"].Columns.Add("drRenewalStatusText", typeof(String));
        dsRenewalList.Tables["dsRenewalListTable"].Columns.Add("drRenewalSubStatusIdx", typeof(String));
        dsRenewalList.Tables["dsRenewalListTable"].Columns.Add("drRenewalSubStatusText", typeof(String));


        ViewState["vsRenewalList"] = dsRenewalList;
    }

    protected void setRenewalList()
    {
        if (ViewState["vsRenewalList"] != null)
        {
            TextBox tb_Next_Renewal_Due = (TextBox)fvRegistrationDetail.FindControl("tb_Next_Renewal_Due");
            RadioButtonList rdoRenewal = (RadioButtonList)fvRegistrationDetail.FindControl("rdoRenewal");
            DropDownList ddlRenewalStatus = (DropDownList)fvRegistrationDetail.FindControl("ddlRenewalStatus");
            DropDownList ddlRenewalSubStatus = (DropDownList)fvRegistrationDetail.FindControl("ddlRenewalSubStatus");
            GridView gvRenewalList = (GridView)fvRegistrationDetail.FindControl("gvRenewalList");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsRenewalList"];

            foreach (DataRow dr in dsContacts.Tables["dsRenewalListTable"].Rows)
            {
                if (dr["drNextRenewalDueText"].ToString() == tb_Next_Renewal_Due.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Check Data Again ---');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsRenewalListTable"].NewRow();
            drContacts["drNextRenewalDueText"] = tb_Next_Renewal_Due.Text;
            drContacts["drSelectedIdx"] = rdoRenewal.SelectedValue;
            drContacts["drSelectedText"] = rdoRenewal.SelectedItem.Text;
            drContacts["drRenewalStatusIdx"] = ddlRenewalStatus.SelectedValue;
            drContacts["drRenewalStatusText"] = ddlRenewalStatus.SelectedItem.Text;
            drContacts["drRenewalSubStatusIdx"] = ddlRenewalSubStatus.SelectedValue;
            drContacts["drRenewalSubStatusText"] = ddlRenewalSubStatus.SelectedItem.Text;

            dsContacts.Tables["dsRenewalListTable"].Rows.Add(drContacts);
            ViewState["vsRenewalList"] = dsContacts;


            setGridData(gvRenewalList, dsContacts.Tables["dsRenewalListTable"]);
            gvRenewalList.Visible = true;
        }
    }

    protected void CleardataSetRenewalList()
    {
        var gvRenewalList = (GridView)fvRegistrationDetail.FindControl("gvRenewalList");
        ViewState["vsRenewalList"] = null;
        gvRenewalList.DataSource = ViewState["vsRenewalList"];
        gvRenewalList.DataBind();
        getRenewalList();
    }

    //deadeline date 

    protected void getDeadlineList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dstDeadlineList = new DataSet();
        dstDeadlineList.Tables.Add("dsDeadlineListTable");

        dstDeadlineList.Tables["dsDeadlineListTable"].Columns.Add("drOfficeActionDateText", typeof(String));
        dstDeadlineList.Tables["dsDeadlineListTable"].Columns.Add("drSelectedDeadlineIdx", typeof(String));
        dstDeadlineList.Tables["dsDeadlineListTable"].Columns.Add("drSelectedDeadlineText", typeof(String));
        dstDeadlineList.Tables["dsDeadlineListTable"].Columns.Add("drSelectedActionListDeadlineText", typeof(String));
        dstDeadlineList.Tables["dsDeadlineListTable"].Columns.Add("drSelectedActionListDeadlineIdx", typeof(String));
        //dsRenewalList.Tables["dsRenewalListTable"].Columns.Add("drRenewalStatusText", typeof(String));
        //dsRenewalList.Tables["dsRenewalListTable"].Columns.Add("drRenewalSubStatusIdx", typeof(String));
        //dsRenewalList.Tables["dsRenewalListTable"].Columns.Add("drRenewalSubStatusText", typeof(String));
        //drContacts["drSelectedActionListDeadlineIdx"] = Action_detail_idx.ToString();

        ViewState["vsDeadlineList"] = dstDeadlineList;
    }

    protected void setDeadlineList()
    {
        if (ViewState["vsDeadlineList"] != null)
        {
            TextBox tb_Office_Action_Date = (TextBox)fvPublicationDetail.FindControl("tb_Office_Action_Date");
            RadioButtonList rdoDeadline = (RadioButtonList)fvPublicationDetail.FindControl("rdoDeadline");
            CheckBoxList chkActionList = (CheckBoxList)fvPublicationDetail.FindControl("chkActionList");
            //DropDownList ddlRenewalStatus = (DropDownList)fvPublicationDetail.FindControl("ddlRenewalStatus");
            //DropDownList ddlRenewalSubStatus = (DropDownList)fvPublicationDetail.FindControl("ddlRenewalSubStatus");
            GridView gvDeadlineInsert = (GridView)fvPublicationDetail.FindControl("gvDeadlineInsert");
            

            var m0_actionsrecord_create = new tcm_m0_actionsrecord_detail[chkActionList.Items.Count];
            int count_create = 0;
            string Action_detail = "";
            string Action_detail_idx = "";

            List<String> AddoingList_create = new List<string>();
            foreach (ListItem chkList_create in chkActionList.Items)
            {
                if (chkList_create.Selected)
                {
                    m0_actionsrecord_create[count_create] = new tcm_m0_actionsrecord_detail();
                    m0_actionsrecord_create[count_create].action_detail_idx = int.Parse(chkList_create.Value);

                    Action_detail += chkList_create.Text + ',' + "<br>";
                    Action_detail_idx += chkList_create.Value + ',';

                    count_create = count_create + 1;
                    //litDebug.Text = test_detail;
                    //litDebug1.Text = test_detail_idx;
                    count_create++;
                }
            }

            if(count_create >= 1)
            {
                CultureInfo culture = new CultureInfo("en-US");
                Thread.CurrentThread.CurrentCulture = culture;

                DataSet dsContacts = (DataSet)ViewState["vsDeadlineList"];

                foreach (DataRow dr in dsContacts.Tables["dsDeadlineListTable"].Rows)
                {
                    if (dr["drOfficeActionDateText"].ToString() == tb_Office_Action_Date.Text)
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Check Data Again ---');", true);
                        return;
                    }
                }

                DataRow drContacts = dsContacts.Tables["dsDeadlineListTable"].NewRow();
                drContacts["drOfficeActionDateText"] = tb_Office_Action_Date.Text;
                drContacts["drSelectedDeadlineIdx"] = rdoDeadline.SelectedValue;
                drContacts["drSelectedDeadlineText"] = rdoDeadline.SelectedItem.Text;

                drContacts["drSelectedActionListDeadlineText"] = Action_detail.ToString();
                drContacts["drSelectedActionListDeadlineIdx"] = Action_detail_idx.ToString();
                //drContacts["drRenewalStatusIdx"] = ddlRenewalStatus.SelectedValue;
                //drContacts["drRenewalStatusText"] = ddlRenewalStatus.SelectedItem.Text;
                //drContacts["drRenewalSubStatusIdx"] = ddlRenewalSubStatus.SelectedValue;
                //drContacts["drRenewalSubStatusText"] = ddlRenewalSubStatus.SelectedItem.Text;

                dsContacts.Tables["dsDeadlineListTable"].Rows.Add(drContacts);
                ViewState["vsDeadlineList"] = dsContacts;


                setGridData(gvDeadlineInsert, ViewState["vsDeadlineList"]);
                gvDeadlineInsert.Visible = true;
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Please Select ActionList ---');", true);
                return;
            }

           
        }
    }

    protected void CleardataSetDeadlineList()
    {
        var gvDeadlineInsert = (GridView)fvPublicationDetail.FindControl("gvDeadlineInsert");
        ViewState["vsDeadlineList"] = null;
        gvDeadlineInsert.DataSource = ViewState["vsDeadlineList"];
        gvDeadlineInsert.DataBind();
        getDeadlineList();
    }

    //Assignment
    protected void setAssignmentList()
    {
        if (ViewState["vsAssignmentList"] != null)
        {
           
            RadioButtonList rdoStepAssignment = (RadioButtonList)fvAssignment.FindControl("rdoStepAssignment");
            TextBox txt_step_remark = (TextBox)fvAssignment.FindControl("txt_step_remark");
            RadioButtonList rdoAssignment = (RadioButtonList)fvAssignment.FindControl("rdoAssignment");
            DropDownList ddlStatusAssignment = (DropDownList)fvAssignment.FindControl("ddlStatusAssignment");
            DropDownList ddlSubStatusAssignment = (DropDownList)fvAssignment.FindControl("ddlSubStatusAssignment");
            GridView gvAssignmentList = (GridView)fvAssignment.FindControl("gvAssignmentList");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsAssignmentList"];

            foreach (DataRow dr in dsContacts.Tables["dsAssignmentListTable"].Rows)
            {
                if (dr["drStepAssignmentIdx"].ToString() == rdoStepAssignment.SelectedValue && dr["drSelectedAssignmentIdx"].ToString() == rdoAssignment.SelectedValue)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Check Data Again ---');", true);
                    return;
                }
            }

            DataRow drContacts = dsContacts.Tables["dsAssignmentListTable"].NewRow();
            //drContacts["drNextRenewalDueText"] = tb_Next_Renewal_Due.Text;
            drContacts["drStepAssignmentIdx"] = rdoStepAssignment.SelectedValue;
            drContacts["drStepAssignmentText"] = rdoStepAssignment.SelectedItem.Text;
            drContacts["drStepRemarkText"] = txt_step_remark.Text;

            drContacts["drSelectedAssignmentIdx"] = rdoAssignment.SelectedValue;
            drContacts["drSelectedAssignmentText"] = rdoAssignment.SelectedItem.Text;

            drContacts["drStatusAssignmentIdx"] = ddlStatusAssignment.SelectedValue;
            drContacts["drStatusAssignmentText"] = ddlStatusAssignment.SelectedItem.Text;

            drContacts["drSubStatusAssignmentIdx"] = ddlSubStatusAssignment.SelectedValue;
            drContacts["drSubStatusAssignmentText"] = ddlSubStatusAssignment.SelectedItem.Text;

            dsContacts.Tables["dsAssignmentListTable"].Rows.Add(drContacts);
            ViewState["vsAssignmentList"] = dsContacts;


            setGridData(gvAssignmentList, dsContacts.Tables["dsAssignmentListTable"]);
            gvAssignmentList.Visible = true;
        }
    }

    protected void getAssignmentList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dstAssignmentList = new DataSet();
        dstAssignmentList.Tables.Add("dsAssignmentListTable");

        dstAssignmentList.Tables["dsAssignmentListTable"].Columns.Add("drStepAssignmentIdx", typeof(String));
        dstAssignmentList.Tables["dsAssignmentListTable"].Columns.Add("drStepAssignmentText", typeof(String));
        dstAssignmentList.Tables["dsAssignmentListTable"].Columns.Add("drStepRemarkText", typeof(String));
        dstAssignmentList.Tables["dsAssignmentListTable"].Columns.Add("drSelectedAssignmentIdx", typeof(String));
        dstAssignmentList.Tables["dsAssignmentListTable"].Columns.Add("drSelectedAssignmentText", typeof(String));
        dstAssignmentList.Tables["dsAssignmentListTable"].Columns.Add("drStatusAssignmentIdx", typeof(String));
        dstAssignmentList.Tables["dsAssignmentListTable"].Columns.Add("drStatusAssignmentText", typeof(String));
        dstAssignmentList.Tables["dsAssignmentListTable"].Columns.Add("drSubStatusAssignmentIdx", typeof(String));
        dstAssignmentList.Tables["dsAssignmentListTable"].Columns.Add("drSubStatusAssignmentText", typeof(String));
        

        ViewState["vsAssignmentList"] = dstAssignmentList;
    }

    protected void CleardataSetAssignmentList()
    {
        var gvAssignmentList = (GridView)fvAssignment.FindControl("gvAssignmentList");
        ViewState["vsAssignmentList"] = null;
        gvAssignmentList.DataSource = ViewState["vsAssignmentList"];
        gvAssignmentList.DataBind();
        getAssignmentList();
    }

    //change name address
    protected void setChangeofNameList()
    {
        if (ViewState["vsChangeofNameList"] != null)
        {

            RadioButtonList rdoChangeofName = (RadioButtonList)fvChangeofName.FindControl("rdoChangeofName");
        
            DropDownList ddlStatusChangeofName = (DropDownList)fvChangeofName.FindControl("ddlStatusChangeofName");
            DropDownList ddlSubStatusChangeofName = (DropDownList)fvChangeofName.FindControl("ddlSubStatusChangeofName");
            GridView gvChangeofNameList = (GridView)fvChangeofName.FindControl("gvChangeofNameList");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsChangeofNameList"];

            foreach (DataRow dr in dsContacts.Tables["dsChangeofNameListTable"].Rows)
            {
                if (dr["drSelectedChangeofNameIdx"].ToString() == rdoChangeofName.SelectedValue)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Check Data Again ---');", true);
                    return;
                }
            }

            DataRow drContacts = dsContacts.Tables["dsChangeofNameListTable"].NewRow();
            //drContacts["drNextRenewalDueText"] = tb_Next_Renewal_Due.Text;
            drContacts["drSelectedChangeofNameIdx"] = rdoChangeofName.SelectedValue;
            drContacts["drSelectedChangeofNameText"] = rdoChangeofName.SelectedItem.Text;

            drContacts["drStatusChangeofNameIdx"] = ddlStatusChangeofName.SelectedValue;
            drContacts["drStatusChangeofNameText"] = ddlStatusChangeofName.SelectedItem.Text;

            drContacts["drSubStatusChangeofNameIdx"] = ddlSubStatusChangeofName.SelectedValue;
            drContacts["drSubStatusChangeofNameText"] = ddlSubStatusChangeofName.SelectedItem.Text;

            dsContacts.Tables["dsChangeofNameListTable"].Rows.Add(drContacts);
            ViewState["vsChangeofNameList"] = dsContacts;


            setGridData(gvChangeofNameList, dsContacts.Tables["dsChangeofNameListTable"]);
            gvChangeofNameList.Visible = true;
        }
    }

    protected void getChangeofNameList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dstChangeofNameList = new DataSet();
        dstChangeofNameList.Tables.Add("dsChangeofNameListTable");

        dstChangeofNameList.Tables["dsChangeofNameListTable"].Columns.Add("drSelectedChangeofNameIdx", typeof(String));
        dstChangeofNameList.Tables["dsChangeofNameListTable"].Columns.Add("drSelectedChangeofNameText", typeof(String));
        dstChangeofNameList.Tables["dsChangeofNameListTable"].Columns.Add("drStatusChangeofNameIdx", typeof(String));
        dstChangeofNameList.Tables["dsChangeofNameListTable"].Columns.Add("drStatusChangeofNameText", typeof(String));
        dstChangeofNameList.Tables["dsChangeofNameListTable"].Columns.Add("drSubStatusChangeofNameIdx", typeof(String));
        dstChangeofNameList.Tables["dsChangeofNameListTable"].Columns.Add("drSubStatusChangeofNameText", typeof(String));

        ViewState["vsChangeofNameList"] = dstChangeofNameList;
    }

    protected void CleardataSetChangeofNameList()
    {
       
        var gvChangeofNameList = (GridView)fvChangeofName.FindControl("gvChangeofNameList");
        ViewState["vsChangeofNameList"] = null;
        gvChangeofNameList.DataSource = ViewState["vsChangeofNameList"];
        gvChangeofNameList.DataBind();
        getChangeofNameList();
    }

    #endregion set data table

    #region pathfile
    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    protected void SelectPathFile(string _docrequest_code, string path, GridView gvName)
    {
        try
        {
            string filePathView = Server.MapPath(path + _docrequest_code);

            //litDebug.Text = filePathView.ToString();
            DirectoryInfo myDirLotus = new DirectoryInfo(filePathView);
            SearchDirectories(myDirLotus, _docrequest_code, gvName);
            gvName.Visible = true;

        }
        catch (Exception ex)
        {
            //gvName.Visible = false;
            //txt.Text = ex.ToString();
        }
    }

    public void SearchDirectories(DirectoryInfo dir, String target, GridView gvName)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");


        FileInfo[] files = dir.GetFiles();
        int i = 0;
        foreach (FileInfo file in files)
        {
            //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
            //{
            string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
            dt1.Rows.Add(file.Name);
            dt1.Rows[i][1] = f[0];
            i++;
            //}
        }

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            gvName.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvName.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvName.DataSource = null;
            gvName.DataBind();

        }


    }

    #endregion path file

    #region reuse

    protected void initPageLoad()
    {

    }

    protected void initPage()
    {

        setActiveTab("docRecordSheet", 0, 0, 0, 0, 0, 0, 0, 0);

    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setChkData(CheckBoxList chkName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        chkName.Items.Clear();
        // bind items
        chkName.DataSource = obj;
        chkName.DataTextField = _data_text;
        chkName.DataValueField = _data_value;
        chkName.DataBind();
    }

    protected void setRdoData(RadioButtonList rdoName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        rdoName.Items.Clear();
        // bind items
        rdoName.DataSource = obj;
        rdoName.DataTextField = _data_text;
        rdoName.DataValueField = _data_value;
        rdoName.DataBind();
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_law callServicePostLaw(string _cmdUrl, data_law _data_law)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_law);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_law = (data_law)_funcTool.convertJsonToObject(typeof(data_law), _localJson);

        return _data_law;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cempidx, int u1idx, int _type_idx)
    {
        //set tab
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        //tab create
        //setFormData(fvEmpDetail, FormViewMode.ReadOnly, null);
        //setFormData(FvCreate, FormViewMode.ReadOnly, null);

        //tab record sheet
        gvRecordSheet.Visible = false;
        setGridData(gvRecordSheet, null);
        setFormData(FvSearchRecordSheet, FormViewMode.ReadOnly, null);

        setFormData(fvRegistrationDetail, FormViewMode.ReadOnly, null);
        ////Panel_RecordSheet.Visible = false;
        setFormData(fvPublicationDetail, FormViewMode.ReadOnly, null);
        setFormData(fvDependentDetail, FormViewMode.ReadOnly, null);
        setFormData(fvPrioritiesDetail, FormViewMode.ReadOnly, null);
        setFormData(fvClientDetail, FormViewMode.ReadOnly, null);
        setFormData(fvAgentDetail, FormViewMode.ReadOnly, null);
        setFormData(fvGoodsDetail, FormViewMode.ReadOnly, null);
        setFormData(fvAdditionalDetail, FormViewMode.ReadOnly, null);
        setFormData(fvTademarkProfile, FormViewMode.ReadOnly, null);
        setFormData(fvBackground, FormViewMode.ReadOnly, null);
        setFormData(fvSummary, FormViewMode.ReadOnly, null);
        setFormData(fvClaims, FormViewMode.ReadOnly, null);
        setFormData(fvAbstract, FormViewMode.ReadOnly, null);
        setFormData(fvAssignment, FormViewMode.ReadOnly, null);
        setFormData(fvChangeofName, FormViewMode.ReadOnly, null);
        setFormData(fvTypeofwork, FormViewMode.ReadOnly, null);
        setFormData(fvDescription, FormViewMode.ReadOnly, null);

        div_LogRecordSheet.Visible = false;
        Panel_SaveRecordSheet.Visible = false;
        Panel_UpdateRecordSheet.Visible = false;

        gvActions.Visible = false;
        setGridData(gvActions, null);
        Panel_HeaderRecordSheet.Visible = false;

        //btnSaveRecordSheet.Visible = false;
        //btnViewRecordSheet.Visible = false;
        Update_BackToDetail.Visible = false;


        switch (activeTab)
        {

            case "docRecordSheet": //record sheet

                if (u1idx == 0)
                {
                    gvRecordSheet.Visible = true;
                    getDetailRecordSheet(gvRecordSheet, 0);
                    setFormData(FvSearchRecordSheet, FormViewMode.Insert, null);

                }
                else if (u1idx > 0)
                {
                    Update_BackToDetail.Visible = true;
                    Panel_HeaderRecordSheet.Visible = true;

                    switch (_chk_tab)
                    {
                        case 0: // view or edit record sheet

                            div_LogRecordSheet.Visible = true;
                            getLogDetailDocument(rptLogRecordSheet, uidx, u1idx);

                            Panel_UpdateRecordSheet.Visible = true;

                            data_law data_u1_document_viewrecord = new data_law();
                            tcm_u1_document_detail u1_document_viewrecord = new tcm_u1_document_detail();
                            data_u1_document_viewrecord.tcm_u1_document_list = new tcm_u1_document_detail[1];

                            u1_document_viewrecord.u1_doc_idx = u1idx;
                            u1_document_viewrecord.type_idx = _type_idx;

                            data_u1_document_viewrecord.tcm_u1_document_list[0] = u1_document_viewrecord;
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1_document_viewrecord));
                            data_u1_document_viewrecord = callServicePostLaw(_urlGetLawViewRecordSheet, data_u1_document_viewrecord);

                            if (data_u1_document_viewrecord.return_code == 0)
                            {

                                ViewState["vs_detailrecordsheet"] = data_u1_document_viewrecord.tcm_u1_document_list;
                                ViewState["vs_renewal_recordsheet"] = data_u1_document_viewrecord.tcm_record_renewal_list;
                                ViewState["vs_publication_recordsheet"] = data_u1_document_viewrecord.tcm_record_publication_list;

                                ViewState["vs_assignment_recordsheet"] = data_u1_document_viewrecord.tcm_record_assignment_list;
                                ViewState["vs_changeofname_recordsheet"] = data_u1_document_viewrecord.tcm_record_changeofname_list;

                                ViewState["vs_dependent_recordsheet"] = data_u1_document_viewrecord.tcm_record_dependent_list;
                                ViewState["vs_priorities_recordsheet"] = data_u1_document_viewrecord.tcm_record_priorities_list;
                                ViewState["vs_client_recordsheet"] = data_u1_document_viewrecord.tcm_record_client_list;
                                ViewState["vs_agent_recordsheet"] = data_u1_document_viewrecord.tcm_record_agent_list;
                                ViewState["vs_good_recordsheet"] = data_u1_document_viewrecord.tcm_record_goods_list;
                                ViewState["vs_additional_recordsheet"] = data_u1_document_viewrecord.tcm_record_additional_list;
                                ViewState["vs_trademarks_recordsheet"] = data_u1_document_viewrecord.tcm_record_trademarkprofile_list;

                                ViewState["vs_typeofwork_recordsheet"] = data_u1_document_viewrecord.tcm_record_typeofwork_list;
                                ViewState["vs_description_recordsheet"] = data_u1_document_viewrecord.tcm_record_description_list;
                                ViewState["vs_claims_recordsheet"] = data_u1_document_viewrecord.tcm_record_claims_list;
                                ViewState["vs_summary_recordsheet"] = data_u1_document_viewrecord.tcm_record_summary_list;
                                ViewState["vs_backgroud_recordsheet"] = data_u1_document_viewrecord.tcm_record_backgroud_list;
                                ViewState["vs_abstract_recordsheet"] = data_u1_document_viewrecord.tcm_record_abstract_list;
                                //////litDebug1.Text = data_u1_document_viewrecord.tcm_record_agent_list.Count().ToString();

                                string _value_formview = data_u1_document_viewrecord.tcm_u1_document_list[0].formview_set.ToString();
                                string[] _set_formview = _value_formview.Split(',');

                                foreach (string _i in _set_formview)
                                {
                                    //check form view in record sheet
                                    if (_i.ToString() == "fvRegistrationDetail")
                                    {
                                        //litDebug.Text += _i.ToString() + "|";
                                        setFormData(fvRegistrationDetail, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);

                                        TextBox tbowner_idx = (TextBox)fvRegistrationDetail.FindControl("tbowner_idx");
                                        DropDownList ddlowner_idx = (DropDownList)fvRegistrationDetail.FindControl("ddlowner_idx");

                                        TextBox tbincorporated_in_idx = (TextBox)fvRegistrationDetail.FindControl("tbincorporated_in_idx");
                                        DropDownList ddlincorporated_in_idx = (DropDownList)fvRegistrationDetail.FindControl("ddlincorporated_in_idx");

                                        TextBox txt_tradmark_status = (TextBox)fvRegistrationDetail.FindControl("txt_tradmark_status");
                                        DropDownList ddl_tradmark_status = (DropDownList)fvRegistrationDetail.FindControl("ddl_tradmark_status");
                                        TextBox txt_tradmark_substatus = (TextBox)fvRegistrationDetail.FindControl("txt_tradmark_substatus");
                                        DropDownList ddl_tradmark_substatus = (DropDownList)fvRegistrationDetail.FindControl("ddl_tradmark_substatus");

                                        TextBox txt_type_of_registration_idx = (TextBox)fvRegistrationDetail.FindControl("txt_type_of_registration_idx");
                                        DropDownList ddlTypeOfRegistrtion = (DropDownList)fvRegistrationDetail.FindControl("ddlTypeOfRegistrtion");

                                        getOwner(ddlowner_idx, int.Parse(tbowner_idx.Text));
                                        getIncorporated(ddlincorporated_in_idx, int.Parse(tbincorporated_in_idx.Text));

                                        getTrademarkStatus(ddl_tradmark_status, int.Parse(txt_tradmark_status.Text), _type_idx, "fvRegistrationDetail");
                                        getTrademarkSubStatus(ddl_tradmark_substatus, int.Parse(txt_tradmark_status.Text), _type_idx, "fvRegistrationDetail");
                                        ddl_tradmark_substatus.SelectedValue = txt_tradmark_substatus.Text;

                                        getTypeOfRegistrtion(ddlTypeOfRegistrtion, 0);
                                        ddlTypeOfRegistrtion.SelectedValue = txt_type_of_registration_idx.Text;

                                        //renewal date detail
                                        GridView gvDetailRenewal = (GridView)fvRegistrationDetail.FindControl("gvDetailRenewal");
                                        if (data_u1_document_viewrecord.tcm_record_renewal_list[0].u1_doc_idx == 0)
                                        {
                                            //litDebug1.Text = "1";
                                            ViewState["vs_renewal_recordsheet"] = null;
                                            setGridData(gvDetailRenewal, ViewState["vs_renewal_recordsheet"]);
                                        }
                                        else
                                        {
                                            //litDebug1.Text = "2";
                                            //ViewState["vs_renewal_recordsheet"] = data_u1_document_viewrecord.tcm_record_agent_list;
                                            setGridData(gvDetailRenewal, ViewState["vs_renewal_recordsheet"]);
                                        }
                                    }

                                    if (_i.ToString() == "fvPublicationDetail")
                                    {
                                        setFormData(fvPublicationDetail, FormViewMode.Edit, ViewState["vs_publication_recordsheet"]);
                                    }

                                    if (_i.ToString() == "fvAssignment")
                                    {
                                        setFormData(fvAssignment, FormViewMode.Edit, ViewState["vs_assignment_recordsheet"]);
                                        GridView gvDetailAssignment = (GridView)fvAssignment.FindControl("gvDetailAssignment");
                                        ////setGridData(gvDetailAssignment, ViewState["vs_assignment_recordsheet"]);
                                        if (data_u1_document_viewrecord.tcm_record_assignment_list[0].u1_doc_idx == 0)
                                        {
                                            //litDebug1.Text = "1";
                                            ViewState["vs_assignment_recordsheet"] = null;

                                            setGridData(gvDetailAssignment, ViewState["vs_assignment_recordsheet"]);
                                        }
                                        else
                                        {
                                            //litDebug1.Text = "2";
                                            //ViewState["vs_renewal_recordsheet"] = data_u1_document_viewrecord.tcm_record_agent_list;
                                            setGridData(gvDetailAssignment, ViewState["vs_assignment_recordsheet"]);
                                        }

                                        
                                        
                                    }

                                    if (_i.ToString() == "fvChangeofName")
                                    {
                                        setFormData(fvChangeofName, FormViewMode.Edit, ViewState["vs_changeofname_recordsheet"]);
                                        GridView gvDetailChangeofName = (GridView)fvChangeofName.FindControl("gvDetailChangeofName");
                                        //setGridData(gvDetailChangeofName, ViewState["vs_changeofname_recordsheet"]);

                                        if (data_u1_document_viewrecord.tcm_record_changeofname_list[0].u1_doc_idx == 0)
                                        {
                                            //litDebug1.Text = "1";
                                            ViewState["vs_changeofname_recordsheet"] = null;

                                            setGridData(gvDetailChangeofName, ViewState["vs_changeofname_recordsheet"]);
                                        }
                                        else
                                        {
                                            //litDebug1.Text = "2";
                                            //ViewState["vs_renewal_recordsheet"] = data_u1_document_viewrecord.tcm_record_agent_list;
                                            setGridData(gvDetailChangeofName, ViewState["vs_changeofname_recordsheet"]);
                                        }
                                    }

                                    if (_i.ToString() == "fvDependentDetail")
                                    {
                                        setFormData(fvDependentDetail, FormViewMode.Edit, ViewState["vs_dependent_recordsheet"]);
                                    }

                                    if (_i.ToString() == "fvPrioritiesDetail")
                                    {
                                        setFormData(fvPrioritiesDetail, FormViewMode.Edit, ViewState["vs_priorities_recordsheet"]);
                                        GridView gvDetailPriorities = (GridView)fvPrioritiesDetail.FindControl("gvDetailPriorities");
                                        //setGridData(gvDetailPriorities, ViewState["vs_priorities_recordsheet"]);
                                        if (data_u1_document_viewrecord.tcm_record_priorities_list[0].u1_doc_idx == 0)
                                        {
                                            //litDebug1.Text = "1";
                                            ViewState["vs_priorities_recordsheet"] = null;

                                            setGridData(gvDetailPriorities, ViewState["vs_priorities_recordsheet"]);
                                        }
                                        else
                                        {
                                            //litDebug1.Text = "2";
                                            //ViewState["vs_renewal_recordsheet"] = data_u1_document_viewrecord.tcm_record_agent_list;
                                            setGridData(gvDetailPriorities, ViewState["vs_priorities_recordsheet"]);
                                        }
                                    }

                                    if (_i.ToString() == "fvClientDetail")
                                    {
                                        setFormData(fvClientDetail, FormViewMode.Edit, ViewState["vs_client_recordsheet"]);
                                        GridView gvDetailClient = (GridView)fvClientDetail.FindControl("gvDetailClient");

                                        //setGridData(gvDetailClient, ViewState["vs_client_recordsheet"]);
                                        if (data_u1_document_viewrecord.tcm_record_client_list[0].u1_doc_idx == 0)
                                        {
                                            //litDebug1.Text = "1";
                                            ViewState["vs_client_recordsheet"] = null;
                                            setGridData(gvDetailClient, ViewState["vs_client_recordsheet"]);
                                        }
                                        else
                                        {
                                            //litDebug1.Text = "2";
                                            
                                            setGridData(gvDetailClient, ViewState["vs_client_recordsheet"]);
                                        }
                                    }

                                    if (_i.ToString() == "fvAgentDetail")
                                    {
                                        setFormData(fvAgentDetail, FormViewMode.Edit, ViewState["vs_agent_recordsheet"]);
                                        GridView gvDetailAgent = (GridView)fvAgentDetail.FindControl("gvDetailAgent");
                                        if (data_u1_document_viewrecord.tcm_record_agent_list[0].u1_doc_idx == 0)
                                        {
                                            //litDebug1.Text = "1";
                                            ViewState["vs_agent_recordsheet"] = null;
                                            setGridData(gvDetailAgent, ViewState["vs_agent_recordsheet"]);
                                        }
                                        else
                                        {
                                            //litDebug1.Text = "2";
                                            //ViewState["vs_agent_recordsheet"] = data_u1_document_viewrecord.tcm_record_agent_list;
                                            setGridData(gvDetailAgent, ViewState["vs_agent_recordsheet"]);
                                        }

                                    }

                                    if (_i.ToString() == "fvGoodsDetail")
                                    {
                                        setFormData(fvGoodsDetail, FormViewMode.Edit, ViewState["vs_good_recordsheet"]);
                                        GridView gvDetailGood = (GridView)fvGoodsDetail.FindControl("gvDetailGood");
                                        //setGridData(gvDetailGood, ViewState["vs_good_recordsheet"]);
                                        if (data_u1_document_viewrecord.tcm_record_goods_list[0].u1_doc_idx == 0)
                                        {
                                            //litDebug1.Text = "1";
                                            ViewState["vs_good_recordsheet"] = null;
                                            setGridData(gvDetailGood, ViewState["vs_good_recordsheet"]);
                                        }
                                        else
                                        {
                                            //litDebug1.Text = "2";
                                            //ViewState["vs_agent_recordsheet"] = data_u1_document_viewrecord.tcm_record_agent_list;
                                            setGridData(gvDetailGood, ViewState["vs_good_recordsheet"]);
                                        }

                                    }

                                    if (_i.ToString() == "fvAdditionalDetail")
                                    {
                                        setFormData(fvAdditionalDetail, FormViewMode.Edit, ViewState["vs_additional_recordsheet"]);

                                    }

                                    if (_i.ToString() == "fvTademarkProfile")
                                    {
                                        setFormData(fvTademarkProfile, FormViewMode.Edit, ViewState["vs_trademarks_recordsheet"]);
                                        Label lbl_trademark_profile_idx_edit = (Label)fvTademarkProfile.FindControl("lbl_trademark_profile_idx_edit");

                                        DropDownList ddlLocal_language_trademark = (DropDownList)fvTademarkProfile.FindControl("ddlLocal_language_trademark");
                                        Label lbl_doc_language_idx = (Label)fvTademarkProfile.FindControl("lbl_doc_language_idx");

                                        DropDownList ddlProfileOwnerLanguage = (DropDownList)fvTademarkProfile.FindControl("ddlProfileOwnerLanguage");
                                        Label lbl_owner_language_idx = (Label)fvTademarkProfile.FindControl("lbl_owner_language_idx");

                                        DropDownList ddlProfileClientLanguage = (DropDownList)fvTademarkProfile.FindControl("ddlProfileClientLanguage");
                                        Label lbl_client_language_idx = (Label)fvTademarkProfile.FindControl("lbl_client_language_idx");
                                        GridView gvFileTrademarkDetail = (GridView)fvTademarkProfile.FindControl("gvFileTrademarkDetail");

                                        getLocalLanguage(ddlLocal_language_trademark, 0);
                                        ddlLocal_language_trademark.SelectedValue = lbl_doc_language_idx.Text;

                                        getLocalLanguage(ddlProfileOwnerLanguage, 0);
                                        ddlProfileOwnerLanguage.SelectedValue = lbl_owner_language_idx.Text;

                                        getLocalLanguage(ddlProfileClientLanguage, 0);
                                        ddlProfileClientLanguage.SelectedValue = lbl_client_language_idx.Text;

                                        //select path file
                                        string getPath_file_law_trademark = ConfigurationSettings.AppSettings["path_file_law_trademarks_profile"];
                                        //string docrequest_code = "doc-" + tbdocrequest_code_viewdetail.Text;
                                        string trademark_code = "trademark" + "-" + lbl_trademark_profile_idx_edit.Text;//data_u0doc_insert.tcm_u0_document_list[0].docrequest_code.ToString();

                                        //litDebug.Text = trademark_code.ToString();
                                        SelectPathFile(trademark_code.ToString(), getPath_file_law_trademark, gvFileTrademarkDetail);

                                    }

                                    if (_i.ToString() == "fvTypeofwork")
                                    {
                                        setFormData(fvTypeofwork, FormViewMode.Edit, ViewState["vs_typeofwork_recordsheet"]);
                                        Label lbl_typeofwork_idx = (Label)fvTypeofwork.FindControl("lbl_typeofwork_idx");
                                        RadioButtonList rdoTypeofwork = (RadioButtonList)fvTypeofwork.FindControl("rdoTypeofwork");

                                        getRecordSheetTypeOfwork(rdoTypeofwork, 0);
                                        rdoTypeofwork.SelectedValue = lbl_typeofwork_idx.Text;

                                    }

                                    if (_i.ToString() == "fvDescription")
                                    {
                                        setFormData(fvDescription, FormViewMode.Edit, ViewState["vs_description_recordsheet"]);

                                    }

                                    if (_i.ToString() == "fvClaims")
                                    {
                                        setFormData(fvClaims, FormViewMode.Edit, ViewState["vs_claims_recordsheet"]);

                                    }

                                    if (_i.ToString() == "fvSummary")
                                    {
                                        setFormData(fvSummary, FormViewMode.Edit, ViewState["vs_summary_recordsheet"]);
                                    }

                                    if (_i.ToString() == "fvBackground")
                                    {
                                        setFormData(fvBackground, FormViewMode.Edit, ViewState["vs_backgroud_recordsheet"]);
                                    }

                                    if (_i.ToString() == "fvAbstract")
                                    {
                                        setFormData(fvAbstract, FormViewMode.Edit, ViewState["vs_abstract_recordsheet"]);
                                    }

                                    /////////

                                    //check form view in record sheet

                                }


                            }

                            break;

                        case 1: // edit record sheet
                            ////Panel_RecordSheet.Visible = true;

                            data_law data_u1_document_editrecord = new data_law();
                            tcm_u1_document_detail u1_document_editrecord = new tcm_u1_document_detail();
                            data_u1_document_editrecord.tcm_u1_document_list = new tcm_u1_document_detail[1];

                            u1_document_editrecord.u1_doc_idx = u1idx;
                            u1_document_editrecord.type_idx = _type_idx;

                            data_u1_document_editrecord.tcm_u1_document_list[0] = u1_document_editrecord;
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));
                            data_u1_document_editrecord = callServicePostLaw(_urlGetLawRecordSheet, data_u1_document_editrecord);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u3_document_viewdetail));

                            if (data_u1_document_editrecord.return_code == 0)
                            {

                                ViewState["vs_detailrecordsheet"] = data_u1_document_editrecord.tcm_u1_document_list;

                                string _value_formview = data_u1_document_editrecord.tcm_u1_document_list[0].formview_set.ToString();
                                string[] _set_formview = _value_formview.Split(',');

                                foreach (string _i in _set_formview)
                                {
                                    //check form view in record sheet
                                    if (_i.ToString() == "fvRegistrationDetail")
                                    {
                                        //litDebug.Text += _i.ToString() + "|";
                                        setFormData(fvRegistrationDetail, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);

                                        TextBox tbowner_idx = (TextBox)fvRegistrationDetail.FindControl("tbowner_idx");
                                        DropDownList ddlowner_idx = (DropDownList)fvRegistrationDetail.FindControl("ddlowner_idx");

                                        TextBox tbincorporated_in_idx = (TextBox)fvRegistrationDetail.FindControl("tbincorporated_in_idx");
                                        DropDownList ddlincorporated_in_idx = (DropDownList)fvRegistrationDetail.FindControl("ddlincorporated_in_idx");


                                        getOwner(ddlowner_idx, int.Parse(tbowner_idx.Text));
                                        getIncorporated(ddlincorporated_in_idx, int.Parse(tbincorporated_in_idx.Text));
                                    }

                                    if (_i.ToString() == "fvPublicationDetail")
                                    {
                                        setFormData(fvPublicationDetail, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvDependentDetail")
                                    {
                                        setFormData(fvDependentDetail, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvPrioritiesDetail")
                                    {
                                        setFormData(fvPrioritiesDetail, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvClientDetail")
                                    {
                                        setFormData(fvClientDetail, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvAgentDetail")
                                    {
                                        setFormData(fvAgentDetail, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvGoodsDetail")
                                    {
                                        setFormData(fvGoodsDetail, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvAdditionalDetail")
                                    {
                                        setFormData(fvAdditionalDetail, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);
                                    }
                                    //check form view in record sheet

                                }

                            }

                            break;
                        case 2: // save insert record sheet
                            ////Panel_RecordSheet.Visible = true;
                            
                            div_LogRecordSheet.Visible = true;
                            getLogDetailDocument(rptLogRecordSheet, uidx, u1idx);
                            Panel_SaveRecordSheet.Visible = true;


                            data_law data_u1_document_insertrecord = new data_law();
                            tcm_u1_document_detail u1_document_insertrecord = new tcm_u1_document_detail();
                            data_u1_document_insertrecord.tcm_u1_document_list = new tcm_u1_document_detail[1];

                            u1_document_insertrecord.u1_doc_idx = u1idx;
                            u1_document_insertrecord.type_idx = _type_idx;

                            data_u1_document_insertrecord.tcm_u1_document_list[0] = u1_document_insertrecord;
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1_document_insertrecord));
                            data_u1_document_insertrecord = callServicePostLaw(_urlGetLawRecordSheet, data_u1_document_insertrecord);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u3_document_viewdetail));

                            if (data_u1_document_insertrecord.return_code == 0)
                            {

                                ViewState["vs_insertrecordsheet"] = data_u1_document_insertrecord.tcm_u1_document_list;

                                string _value_formview = data_u1_document_insertrecord.tcm_u1_document_list[0].formview_set.ToString();
                                string[] _set_formview = _value_formview.Split(',');

                                foreach (string _i in _set_formview)
                                {
                                    //check form view in record sheet
                                    if (_i.ToString() == "fvRegistrationDetail")
                                    {
                                        //litDebug.Text += _i.ToString() + "|";
                                        setFormData(fvRegistrationDetail, FormViewMode.Insert, ViewState["vs_insertrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvPublicationDetail")
                                    {
                                        setFormData(fvPublicationDetail, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                        CheckBoxList chkActionList = (CheckBoxList)fvPublicationDetail.FindControl("chkActionList");

                                        ////GridView gvActionsDetail = (GridView)fvPublicationDetail.FindControl("gvActionsDetail");
                                        ////getLawM0Actions(gvActionsDetail, 0);

                                        getM0ActionDetailList(chkActionList, 0);

                                    }

                                    if (_i.ToString() == "fvDependentDetail")
                                    {
                                        setFormData(fvDependentDetail, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvPrioritiesDetail")
                                    {
                                        setFormData(fvPrioritiesDetail, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvClientDetail")
                                    {
                                        setFormData(fvClientDetail, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvAgentDetail")
                                    {
                                        setFormData(fvAgentDetail, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvGoodsDetail")
                                    {
                                        setFormData(fvGoodsDetail, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvAdditionalDetail")
                                    {
                                        setFormData(fvAdditionalDetail, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "gvActions")
                                    {
                                        gvActions.Visible = true;
                                        getLawM0Actions(gvActions, 0);
                                    }

                                    if (_i.ToString() == "fvTademarkProfile")
                                    {
                                        setFormData(fvTademarkProfile, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvBackground")
                                    {
                                        setFormData(fvBackground, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvSummary")
                                    {
                                        setFormData(fvSummary, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvClaims")
                                    {
                                        setFormData(fvClaims, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvAbstract")
                                    {
                                        setFormData(fvAbstract, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvAssignment")
                                    {
                                        setFormData(fvAssignment, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);

                                        DropDownList ddlStatusAssignment = (DropDownList)fvAssignment.FindControl("ddlStatusAssignment");
                                        DropDownList ddlSubStatusAssignment = (DropDownList)fvAssignment.FindControl("ddlSubStatusAssignment");

                                        getTrademarkStatus(ddlStatusAssignment, 0, int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvAssignment");
                                        getTrademarkSubStatus(ddlSubStatusAssignment, int.Parse(ddlStatusAssignment.SelectedValue), int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvAssignment");

                                    }

                                    if (_i.ToString() == "fvChangeofName")
                                    {
                                        setFormData(fvChangeofName, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);

                                        DropDownList ddlStatusChangeofName = (DropDownList)fvChangeofName.FindControl("ddlStatusChangeofName");
                                        DropDownList ddlSubStatusChangeofName = (DropDownList)fvChangeofName.FindControl("ddlSubStatusChangeofName");

                                        getTrademarkStatus(ddlStatusChangeofName, 0, int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvChangeofName");
                                        getTrademarkSubStatus(ddlSubStatusChangeofName, int.Parse(ddlStatusChangeofName.SelectedValue), int.Parse(ViewState["vs_detailrecordsheet_type_idx"].ToString()), "fvChangeofName");

                                    }

                                    if (_i.ToString() == "fvTypeofwork")
                                    {
                                        setFormData(fvTypeofwork, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvDescription")
                                    {
                                        setFormData(fvDescription, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    //check form view in record sheet

                                }

                            }

                            break;
                    }
                }

                setOntop.Focus();
                break;


        }
    }

    protected void setActiveTab(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cemp_idx, int u1idx, int _type_idx)
    {
        setActiveView(activeTab, uidx, nodeidx, staidx, actor_idx, _chk_tab, _cemp_idx, u1idx, _type_idx);
        switch (activeTab)
        {
            case "docRecordSheet":
                li0.Attributes.Add("class", "active");
                //li1.Attributes.Add("class", "");
                //li2.Attributes.Add("class", "");
                //li3.Attributes.Add("class", "");
                break;
            //case "docCreate":
            //    li0.Attributes.Add("class", "");
            //    li1.Attributes.Add("class", "active");
            //    li2.Attributes.Add("class", "");
            //    li3.Attributes.Add("class", "");
            //    break;
            //case "docWaitApprove":
            //    li0.Attributes.Add("class", "");
            //    li1.Attributes.Add("class", "");
            //    li2.Attributes.Add("class", "active");
            //    li3.Attributes.Add("class", "");
            //    break;
            //case "docRegistration":
            //    li0.Attributes.Add("class", "");
            //    li1.Attributes.Add("class", "");
            //    li2.Attributes.Add("class", "");
            //    li3.Attributes.Add("class", "active");
            //    break;
        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }

    protected string getStatus(int status)
    {
        if (status == 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Certificate'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='No Certificate'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        //specified ASP.NET server control at run time.
    }

    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;
        switch (rptName.ID)
        {

            case "":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {



                }
                break;

        }
    }

    protected void gridViewTrigger(GridView linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    #endregion reuse

}