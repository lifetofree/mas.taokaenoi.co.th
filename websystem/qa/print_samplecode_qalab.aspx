﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="print_samplecode_qalab.aspx.cs" Inherits="websystem_qa_print_samplecode_qalab" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link href="./../Content/bootstrap.css" runat="server" rel="stylesheet" />
    <link href="./../Content/custom.css" runat="server" rel="stylesheet" />

    <style type="text/css" media="print,screen">
        @page {
            size: A4 landscape;
            margin: 25px;
        }

        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
            }
        }

        .formPrint {
            margin: 10pt; /*from 5 to 10*/
            padding: 10pt; /*add*/
            width: 842pt;
        }
    </style>


    <title></title>
</head>
<body onload="window.print()">
    <div class="formPrint">

        <%--<img src='<%=ResolveUrl("~/uploadfiles/qa_lab/lab_form/lab_form.jpg")%>' width="100%" /> src='<%=ResolveUrl("~/Scripts/menu-scripts/jquery.metisMenu.js")%>'    --%>

        <table class="table table-striped table-bordered table-hover table-responsive" cellspacing="0" width="100%" rules="all" border="1" style="border-collapse: collapse;">
            <tbody>
                <tr class="info" style="font-size: Small; height: 40px;">
                    <th scope="col" style="width: 15%;">Sample Code</th>
                    <th scope="col" style="width: 15%;">Test Item</th>
                    <th scope="col" style="width: 15%;">QR Code</th>
           
                </tr>
                <tr style="font-size: Small;">
                    <td>7G07001</td>
                    <td>Total Plate Count(CFU/g),Moistue(%MC)</td>
                    <td><img src='<%=ResolveUrl("~/uploadfiles/Qr_Code/agwg.jpg")%>' alt="" style="font-size: Small;width:50px;"></td>
                   
                   
                </tr>
                <tr style="font-size: Small;">
                    <td>7G07002</td>
                    <td>Total Plate Count(CFU/g),Moistue(%MC)</td>
                    <td><img src='<%=ResolveUrl("~/uploadfiles/Qr_Code/agwg.jpg")%>' alt="" style="font-size: Small;width:50px;"></td>
                               
                </tr>
               
            </tbody>
        </table>

    </div>
</body>
</html>

