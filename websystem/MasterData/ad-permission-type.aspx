﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="ad-permission-type.aspx.cs" Inherits="websystem_adonline_permission_type" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
   <asp:Literal ID="litDebug" runat="server" />
   <!-- Start Gridview & Update Form -->
   <div id="divIndex" runat="server">
      <div class="col-md-12">
         <asp:LinkButton ID="btnToInsert" CssClass="btn btn-success pull-right f-s-14" runat="server" CommandName="btnToInsert" OnCommand="btnCommand" Text="สร้าง" />
         <div class="form-inline">
            <div class="form-group">
               <label for="keywordSearch">Keyword :</label>
               <asp:TextBox ID="keywordSearch" runat="server" CssClass="form-control" placeholder="ชื่อรูปแบบสิทธิ์..." />
            </div>
            <div class="form-group">
               <label for="keywordSearch">สถานะ :</label>
               <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                  <asp:ListItem Value="99">All</asp:ListItem>
                  <asp:ListItem Value="1">Online</asp:ListItem>
                  <asp:ListItem Value="0">Offline</asp:ListItem>
               </asp:DropDownList>
            </div>
            <div class="form-group">
               <asp:LinkButton ID="LinkButton1" runat="server" OnCommand="btnCommand" CommandName="btnSearch" CssClass="btn btn-primary"><i class="fa fa-search"></i></asp:LinkButton>
            </div>
         </div>
         <asp:GridView ID="gvPermType"
            runat="server"
            AutoGenerateColumns="false"
            DataKeyNames="m0_perm_type_idx"
            CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
            HeaderStyle-CssClass="info"
            AllowPaging="true"
            PageSize="10"
            OnRowEditing="Master_RowEditing"
            OnRowUpdating="Master_RowUpdating"
            OnRowCancelingEdit="Master_RowCancelingEdit"
            OnPageIndexChanging="Master_PageIndexChanging"
            OnRowDataBound="Master_RowDataBound">
            <PagerStyle CssClass="pageCustom" />
            <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
            <EmptyDataTemplate>
               <div class="text-center">No result</div>
            </EmptyDataTemplate>
            <Columns>
               <asp:TemplateField HeaderText="#" HeaderStyle-CssClass="text-center"
                  HeaderStyle-Font-Size="Small" ItemStyle-CssClass="text-center" HeaderStyle-Width="10%">
                  <ItemTemplate>
                     <%# (Container.DataItemIndex + 1) %>
                  </ItemTemplate>
                  <EditItemTemplate>
                     <asp:TextBox ID="m0_perm_type_idx" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("m0_perm_type_idx")%>' />
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="f-s-12">ชื่อรูปแบบสิทธิ์</label>
                           <asp:TextBox ID="txtPermTypeNameUpdate" runat="server" CssClass="form-control" Text='<%# Eval("perm_type_name") %>' />
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label class="f-s-12">สถานะ</label>
                           <asp:DropDownList ID="ddlPermTypeStatusUpdate" AutoPostBack="false" runat="server"
                              CssClass="form-control" SelectedValue='<%# Eval("perm_type_status") %>'>
                              <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                              <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                           </asp:DropDownList>
                        </div>
                     </div>
                     <div class="col-md-3">
                        <div class="panel panel-info">
                           <div class="panel-heading">การอนุญาตสิทธิ์</div>
                           <div class="panel-body">
                              <asp:TextBox ID="txtPermissionCategoryUpdate" runat="server" Text='<%# Eval("m0_perm_cate_idx_ref") %>' Visible="false" />
                              <asp:CheckBoxList ID="chkPermissionCategoryUpdate" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical"></asp:CheckBoxList>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-3">
                        <div class="panel panel-info">
                           <div class="panel-heading">การอนุญาตผู้ขอใช้สิทธิ์</div>
                           <div class="panel-body">
                              <asp:TextBox ID="txtEmployeeLifeTimeTypeUpdate" runat="server" Text='<%# Eval("m0_emp_type_idx_ref") %>' Visible="false" />
                              <asp:CheckBoxList ID="chkEmployeeLifeTimeTypeUpdate" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical"></asp:CheckBoxList>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-12">
                        <div class="pull-left">
                           <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                              ValidationGroup="savePermTypeUpdate" CommandName="Update"
                              OnClientClick="return confirm('คุณมั่นใจหรือไม่ว่าต้องการบันทึกการเปลี่ยนแปลง ?')">
                              บันทึกการเปลี่ยนแปลง
                           </asp:LinkButton>
                           <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" CommandName="Cancel">
                              ยกเลิก
                           </asp:LinkButton>
                        </div>
                     </div>
                  </EditItemTemplate>
               </asp:TemplateField>
               <asp:TemplateField HeaderText="ชื่อรูปแบบสิทธิ์" ItemStyle-HorizontalAlign="Left"
                  HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="40%">
                  <ItemTemplate>
                     <small>
                        <asp:Label ID="permissionTypeName" runat="server" Text='<%# Eval("perm_type_name") %>' />
                     </small>
                  </ItemTemplate>
                  <EditItemTemplate />
                  <FooterTemplate />
               </asp:TemplateField>
               <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                  HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="30%">
                  <ItemTemplate>
                     <small>
                        <asp:Label ID="permissionTypeStatus" runat="server" Text='<%# getStatus((int)Eval("perm_type_status")) %>' />
                     </small>
                  </ItemTemplate>
                  <EditItemTemplate />
                  <FooterTemplate />
               </asp:TemplateField>
               <asp:TemplateField ItemStyle-HorizontalAlign="center"
                  HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                  <ItemTemplate>
                     <asp:LinkButton ID="Edit" CssClass="btn btn-warning btn-sm" runat="server" CommandName="Edit"
                        data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></asp:LinkButton>
                     <asp:LinkButton ID="delete" CssClass="btn btn-danger btn-sm" runat="server" data-toggle="tooltip"
                        title="Remove" CommandName="btnBan" OnCommand="btnCommand"
                        CommandArgument='<%# Eval("m0_perm_type_idx") %>'
                        OnClientClick="return confirm('Do you want to delete this item?')">
                           <i class="fa fa-trash"></i>
                     </asp:LinkButton>
                  </ItemTemplate>
                  <EditItemTemplate />
                  <FooterTemplate />
               </asp:TemplateField>
            </Columns>
         </asp:GridView>
      </div>
   </div>
   <!-- End Gridview & Update Form -->

   <!-- Start Insert Form -->
   <div id="divInsert" runat="server">
      <div class="row">
         <div class="col-md-12">
            <div class="form-group">
               <asp:LinkButton CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                  CommandName="btnCancel" OnCommand="btnCommand"><i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ</asp:LinkButton>
            </div>
         </div>
         <div class="col-md-12">
            <div class="panel panel-info">
               <div class="panel-heading">สร้างรูปแบบสิทธิ์</div>
               <div class="panel-body">
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="pull-left m-r-10">ชื่อรูปแบบสิทธิ์</label> 
                        <asp:RequiredFieldValidator ID="requiredPermTypeName"
                           ValidationGroup="savePermType" runat="server"
                           Display="Dynamic"
                           SetFocusOnError="true"
                           ControlToValidate="txtPermTypeName"
                           Font-Size="12px" ForeColor="Red"
                           ErrorMessage="กรุณากรอกชื่อรูปแบบสิทธิ์" />
                        <asp:CustomValidator ID="customPermTypeName"
                           ValidationGroup="savePermType" runat="server"
                           Display="Dynamic"
                           SetFocusOnError="true"
                           ControlToValidate="txtPermTypeName"
                           OnServerValidate="checkExistsPermTypeName"
                           Font-Size="12px" ForeColor="Red"
                           ErrorMessage="ชื่อรูปแบบสิทธิ์นี้ มีในระบบแล้ว" />
                        <asp:TextBox ID="txtPermTypeName" runat="server" CssClass="form-control"
                           placeholder="ชื่อรูปแบบสิทธิ์..." />
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label>สถานะ</label>
                        <asp:DropDownList ID="ddlPermTypeStatus" runat="server" CssClass="form-control">
                           <asp:ListItem Value="1" Text="Online" />
                           <asp:ListItem Value="0" Text="Offline" />
                        </asp:DropDownList>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="panel panel-info">
                        <div class="panel-heading">การอนุญาตสิทธิ์</div>
                        <div class="panel-body">
                           <asp:CheckBoxList ID="chkPermissionCategory" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical"></asp:CheckBoxList>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3">
                     <div class="panel panel-info">
                        <div class="panel-heading">การอนุญาตผู้ขอใช้สิทธิ์</div>
                        <div class="panel-body">
                           <asp:CheckBoxList ID="chkEmployeeLifeTimeType" runat="server" CssClass="radio-list-inline-emps" RepeatDirection="Vertical"></asp:CheckBoxList>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group">
                        <asp:LinkButton ID="btnInsert" CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="savePermType" />
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- End Insert Form -->
</asp:Content>
