<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="admin_conference.aspx.cs" Inherits="websystem_conference_admin_conference" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="hlSetTotop" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <!--tab menu-->
    <div id="divMenu" runat="server" class="col-md-12">
        <nav class="navbar navbar-default" id="divBtnHead" runat="server" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-bar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="sub-navbar">
                        <a class="navbar-brand" href="#"><b>Menu</b></a>
                    </div>
                </div>

                <!--Collect the nav links, forms, and other content for toggling-->
                <div class="collapse navbar-collapse" id="menu-bar">
                    <ul class="nav navbar-nav" id="uiNav" runat="server">
                        <li id="li0" runat="server">
                            <asp:LinkButton ID="lbList" runat="server" CommandName="navList" OnCommand="navCommand">
                                รายชื่อ</asp:LinkButton>
                        </li>
                    </ul>
                </div>
                <!--Collect the nav links, forms, and other content for toggling-->
            </div>
        </nav>
    </div>
    <!--tab menu-->
    <asp:MultiView ID="mvSystem" runat="server">
        <asp:View ID="viewList" runat="server">
            <div class="row form-group" style="padding-bottom: 10px;">
                <div class="col-md-8">
                    <asp:LinkButton ID="lbExport" runat="server" CssClass="btn btn-sm btn-success"
                        OnCommand="btnCommand" CommandName="cmdExport" CommandArgument="0"><i
                            class="far fa-file-excel"></i>&nbsp;Export</asp:LinkButton>
                </div>
                <div class="col-md-4">
                    <div class="input-group">
                        <asp:TextBox ID="tbItemSearch" runat="server" CssClass="form-control" placeholder="ค้นหาโดยชื่อ"
                            MaxLength="100">
                        </asp:TextBox>
                        <asp:LinkButton ID="lbItemSearch" runat="server" CssClass="input-group-addon"
                            OnCommand="btnCommand" CommandName="cmdItemSearch"><i class="fas fa-search"></i>
                        </asp:LinkButton>
                        <asp:LinkButton ID="lbItemReset" runat="server" CssClass="input-group-addon"
                            OnCommand="btnCommand" CommandName="cmdItemReset"><i class="fas fa-sync-alt"></i>
                        </asp:LinkButton>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <asp:GridView ID="gvUserList" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-responsive">
                    <HeaderStyle CssClass="info" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <ItemTemplate>
                                <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Username">
                            <ItemTemplate>
                                <asp:Label ID="lblUserName" runat="server" Text='<%# Eval("user_name") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Firstname">
                            <ItemTemplate>
                                <asp:Label ID="lblFirstname" runat="server" Text='<%# Eval("user_firstname") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Lastname">
                            <ItemTemplate>
                                <asp:Label ID="lblLastName" runat="server" Text='<%# Eval("user_lastname") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="lblFlag" runat="server"
                                    Text='<%# ((int)Eval("user_flag") == 0) ? "-" : "Lock" %>'
                                    CssClass='<%# ((int)Eval("user_flag") == 0) ? "text-success" : "text-danger" %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbDownloadData" runat="server" CssClass="btn btn-sm btn-warning"
                                    OnCommand="btnCommand" CommandName="cmdUnlock"
                                    CommandArgument='<%# Eval("u0_idx") %>'
                                    Visible='<%# ((int)Eval("user_flag") == 0) ? false : true %>'>Unlock
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <asp:GridView ID="gvRegisterList" runat="server" AutoGenerateColumns="False"
                    CssClass="table table-striped table-bordered table-responsive">
                    <HeaderStyle CssClass="info" Font-Size="Small" />
                    <RowStyle Font-Size="Small" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">ไม่พบข้อมูล</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="#">
                            <ItemTemplate>
                                <asp:Label ID="lblNo" runat="server" Text='<%# (Container.DataItemIndex +1) %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name-Surname">
                            <ItemTemplate>
                                <asp:Label ID="lblFirstname2" runat="server" Text='<%# Eval("user_firstname") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="Surname">
                            <ItemTemplate>
                                <asp:Label ID="lbLastname2" runat="server" Text='<%# Eval("user_lastname") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="E-mail">
                            <ItemTemplate>
                                <asp:Label ID="lbEmail2" runat="server" Text='<%# Eval("user_mail") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Company / Fund name">
                            <ItemTemplate>
                                <asp:Label ID="lbCompany2" runat="server" Text='<%# Eval("user_company") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="WebEx display name">
                            <ItemTemplate>
                                <asp:Label ID="lbUserWebEx2" runat="server" Text='<%# Eval("user_name") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="lblFlag2" runat="server"
                                    Text='<%# ((int)Eval("user_flag") == 0) ? "-" : "Join" %>'
                                    CssClass='<%# ((int)Eval("user_flag") == 1) ? "text-success" : "text-danger" %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="lbJoinMeeting" runat="server" CssClass="btn btn-sm btn-success"
                                    OnCommand="btnCommand" CommandName="cmdJoin" CommandArgument='<%# Eval("u0_idx") %>'
                                    Visible='<%# ((int)Eval("user_flag") == 1) ? false : true %>'>Join
                                </asp:LinkButton>
                                <asp:LinkButton ID="lbUnjoinMeeting" runat="server" CssClass="btn btn-sm btn-warning"
                                    OnCommand="btnCommand" CommandName="cmdUnjoin" CommandArgument='<%# Eval("u0_idx") %>'
                                    Visible='<%# ((int)Eval("user_flag") == 0) ? false : true %>'>Unjoin
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </asp:View>
    </asp:MultiView>
</asp:Content>