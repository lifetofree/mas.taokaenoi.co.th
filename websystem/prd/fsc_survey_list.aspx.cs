using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

public partial class websystem_prd_fsc_survey_list : System.Web.UI.Page
{
    #region initial function/data
    function_tool _funcTool = new function_tool();
    data_employee _data_employee = new data_employee();
    data_fsc _data_fsc = new data_fsc();

    int _emp_idx = 0;
    int _rpos_idx = 0;
    int _temp_idx = 0;
    int _temp_sum = 0;
    int _temp_value = 0;
    string _temp_data = "";
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _local_json = String.Empty;

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlFscGetRandomList = _serviceUrl + ConfigurationManager.AppSettings["urlFscGetRandomList"];
    static string _urlFscSetRandomList = _serviceUrl + ConfigurationManager.AppSettings["urlFscSetRandomList"];
    static string _urlFscSetRandomSurveyStatus = _serviceUrl + ConfigurationManager.AppSettings["urlFscSetRandomSurveyStatus"];
    #endregion initial function/data

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
        }

        //set background
        HtmlGenericControl body_master =  (HtmlGenericControl)Master.FindControl("body_master");
        body_master.Attributes.Add("style", "background-image: url('" + ResolveUrl("~/masterpage/images/prd/fsc_bg.png") + "'); background-repeat: no-repeat; background-size: cover; background-attachment: fixed;");
        HtmlGenericControl div_container =  (HtmlGenericControl)Master.FindControl("div_container");
        div_container.Attributes.Add("style", "background-color: rgba(255, 255, 255, 0.8)");
    }

    #region event command
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        switch (cmdName)
        {
            case "cmdItemSearch":
                defaultData();
                fsc_random_detail[] _tempData = (fsc_random_detail[])ViewState["EmployeeList"];
                var _linqSearch = from d in _tempData
                                  where (d.emp_code.Contains(tbItemSearch.Text))
                                  select d;
                _funcTool.setGvData(gvRandomList, _linqSearch.ToList());
                gvRandomList.PageIndex = 0;
                break;
            case "cmdItemReset":
                defaultData();
                tbItemSearch.Text = String.Empty;
                gvRandomList.PageIndex = 0;
                break;
        }
    }

    protected void btnCommandLink(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        fsc_random_detail _updateData = new fsc_random_detail();
        _updateData.emp_idx = _funcTool.convertToInt(cmdArg);

        _data_fsc.fsc_random_list = new fsc_random_detail[1];
        _data_fsc.fsc_random_list[0] = _updateData;

        _data_fsc = callServicePostFsc(_urlFscSetRandomSurveyStatus, _data_fsc);

        if (_data_fsc.return_code == 0)
        {
            _data_fsc = new data_fsc();
            _data_fsc = callServicePostFsc(_urlFscGetRandomList, _data_fsc);

            ViewState["EmployeeList"] = _data_fsc.fsc_random_list;
            _funcTool.setGvData(gvRandomList, ViewState["EmployeeList"]);
            hlSetTotop.Focus();

            switch (cmdName)
            {
                case "cmdLinkTh":
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('https://docs.google.com/forms/d/e/1FAIpQLSf_pwJfVT5tT0V_6m-jQdNe8WxcNSNVQcOgFzihb5_O7KtmJg/viewform','_blank')", true);
                    break;
                case "cmdLinkBm":
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('https://docs.google.com/forms/d/e/1FAIpQLSex85JJagfkouCc_lKod48Mt75Iorw12rkcBWD389Cpl6OlLA/viewform','_blank')", true);
                    break;
            }
        }
    }
    #endregion event command

    #region paging
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {
            case "gvRandomList":
                if (ViewState["EmployeeList"] != null)
                {
                    _funcTool.setGvData(gvRandomList, ViewState["EmployeeList"]);
                }
                else
                {
                    defaultData();
                }
                break;
        }
    }
    #endregion paging

    #region reuse
    protected void initPage()
    {
        clearSession();
        clearViewState();
        defaultData();
    }

    protected void clearSession()
    {
        // Session["checkSubmit"] = null;
    }

    protected void clearViewState()
    {
        ViewState["EmployeeList"] = null;
        // ViewState["VisitorSelected"] = null;
        // ViewState["ReportList"] = null;
    }

    protected void defaultData()
    {
        if (ViewState["EmployeeList"] == null)
        {
            _data_fsc = new data_fsc();
            _data_fsc = callServicePostFsc(_urlFscGetRandomList, _data_fsc);

            ViewState["EmployeeList"] = _data_fsc.fsc_random_list;
        }

        _funcTool.setGvData(gvRandomList, ViewState["EmployeeList"]);
        hlSetTotop.Focus();
    }

    protected data_fsc callServicePostFsc(string _cmdUrl, data_fsc _data_fsc)
    {
        // convert to json
        _local_json = _funcTool.convertObjectToJson(_data_fsc);
        //litDebug.Text = _local_json;

        // call services
        _local_json = _funcTool.callServicePost(_cmdUrl, _local_json);

        // convert json to object
        _data_fsc = (data_fsc)_funcTool.convertJsonToObject(typeof(data_fsc), _local_json);

        return _data_fsc;
    }
    #endregion reuse
}