﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_cims_m0_location.aspx.cs" Inherits="websystem_MasterData_qa_cims_m0_location" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">
    <asp:HyperLink ID="setFocus" runat="server"></asp:HyperLink>
    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:TextBox ID="txt_Location_name" runat="server" CssClass="form-control" placeholder="ชื่อ ..." Visible="false" Enabled="true" />
    <asp:MultiView ID="mvMultiview" runat="server" ActiveViewIndex="0">
        <asp:View ID="ViewInsertLocation" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnAddLocation" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่ม Location" data-toggle="tooltip" title="เพิ่ม Location" runat="server"
                    CommandName="cmdAddLocation" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่ม Location</asp:LinkButton>
            </div>

            <asp:FormView ID="fvInsertLocation" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">เพิ่ม Location</h4>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:Label ID="lbl_form_name" runat="server" Text="ชื่อ Location" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txt_Locationname" runat="server" CssClass="form-control" placeholder="กรอกชื่อ Location ..." Enabled="true" />
                                        <asp:RequiredFieldValidator ID="Retxt_Locationname" runat="server"
                                            ControlToValidate="txt_Locationname" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกชื่อ Location" ValidationGroup="Save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttxtformname" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxt_Locationname" Width="220" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="lbl_form_name_status" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddl_status_location" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Value="1">Online</asp:ListItem>
                                            <asp:ListItem Value="0">Offline</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-3">
                                        <asp:LinkButton ID="btnSaveLocation" ValidationGroup="Save" CssClass="btn btn-success" Text="Save" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="cmdSave" OnCommand="btnCommand"></asp:LinkButton>
                                        <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvLocation" runat="server" Visible="true"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound"
                AutoPostBack="false">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center;"><b>ไม่มีข้อมูล Location</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <%--<div style="text-align: center;">--%>
                                <asp:Label ID="lbl_m0_location_idx" runat="server" Visible="false" Text='<%# Eval("m0_location_idx") %>' />
                                <%# (Container.DataItemIndex +1) %>
                                <%--</div>--%>
                            </small>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_m0_location_idx_edit" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("m0_location_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbl_location_nameedit" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อ Location" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_location_name_edit" runat="server" CssClass="form-control " Text='<%# Eval("location_name") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Requiredtxt_location_name_edit" runat="server"
                                                ControlToValidate="txt_location_name_edit" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อ Location" ValidationGroup="Saveedit" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valitxt_update_formcal_name" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_location_name_edit" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lblocation_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddllocation_status" Text='<%# Eval("location_status") %>'
                                                CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editinvname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ชื่อ Location" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="50%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:Label ID="lbllocation_name" runat="server" Visible="true" Text='<%# Eval("location_name") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="20%">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbl_location_status" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("location_status") %>'></asp:Label>

                                <asp:Label ID="lbllocation_statusOnline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="Online"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span>
                                </asp:Label>
                                <asp:Label ID="lbllocation_statusOffline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span>
                                </asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Width="20%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:LinkButton ID="btnView" CssClass="text-read" runat="server" CommandName="cmdView"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("m0_location_idx") + "," + Eval("location_name") %>' title="view">
                                        <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i></asp:LinkButton>
                            <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelete" CssClass="text-trash" runat="server" CommandName="cmdDelete"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                CommandArgument='<%#Eval("m0_location_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

            </asp:GridView>

        </asp:View>

        <asp:View ID="ViewInsertZone" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnBack" CssClass="btn btn-danger" Visible="true" data-original-title="ย้อนกลับ" data-toggle="tooltip" title="ย้อนกลับ" runat="server"
                    CommandName="cmdBack" OnCommand="btnCommand">&nbsp;ย้อนกลับ</asp:LinkButton>
            </div>

            <div class="form-group">
                <asp:LinkButton ID="btnAddZone" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่ม Zone" data-toggle="tooltip" title="เพิ่ม Zone" runat="server"
                    CommandName="cmdAddZone" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่ม Zone</asp:LinkButton>
            </div>

            <div class="alert alert-info" id="divLocationDetails" runat="server" visible="true">
                <asp:Label ID="lbl_show_namezone" runat="server"></asp:Label>
            </div>

            <asp:FormView ID="fvInsertZone" runat="server" DefaultMode="insert" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <asp:Label ID="lbl_showname_zone_insert" Text="เพิ่ม Zone เครื่องมือ" runat="server"></asp:Label>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:Label ID="lbnameZone" runat="server" Text="ชื่อ Location" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txt_Location_view" runat="server" CssClass="form-control" Enabled="false" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="Label1" runat="server" Text="ชื่อ Zone" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txt_Zone" runat="server" CssClass="form-control" Enabled="true" />
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="lbl_status_zone" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddl_status_zone" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Value="1">Online</asp:ListItem>
                                            <asp:ListItem Value="0">Offline</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-3">
                                        <asp:LinkButton ID="btnSaveZone" ValidationGroup="SaveZone" CssClass="btn btn-success" Text="Save" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="cmdSaveZone" OnCommand="btnCommand"></asp:LinkButton>
                                        <asp:LinkButton ID="btnCancelZone" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancelZone" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvZone" runat="server" Visible="true"
                AutoGenerateColumns="false"
                CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                HeaderStyle-CssClass="success"
                AllowPaging="true"
                PageSize="10"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound"
                AutoPostBack="false">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                <EmptyDataTemplate>
                    <div style="text-align: center;"><b>ไม่มีข้อมูล Zone</b> </div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <%--<div style="text-align: center;">--%>
                            <small>
                                <asp:Label ID="lbl_m1_location_idx" runat="server" Visible="false" Text='<%# Eval("m1_location_idx") %>' />
                                <%# (Container.DataItemIndex +1) %>
                            </small>
                            <%--</div>--%>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="txt_m1_location_idx" Visible="false" runat="server" CssClass="form-control " Text='<%# Eval("m1_location_idx") %>'></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbformname" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อ Location" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_m0_location_name_update" Enabled="false" runat="server" CssClass="form-control"></asp:TextBox>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lbupdate_m0_formcal_table_idx" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อ Zone" />
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txt_update_location_zone" runat="server" CssClass="form-control " Text='<%# Eval("location_zone") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="Requiredtxt_update_location_zone" runat="server"
                                                ControlToValidate="txt_update_location_zone" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อ Zone" ValidationGroup="update_formcal_table_name" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Valtxt_update_formcal_table_name" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requiredtxt_update_location_zone" Width="220" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>

                                    <div class="form-group">
                                        <asp:Label ID="lblupdates_zone_status" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlupdatezone_status" Text='<%# Eval("zone_status") %>'
                                                CssClass="form-control" runat="server">
                                                <asp:ListItem Value="1">Online</asp:ListItem>
                                                <asp:ListItem Value="0">Offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-3"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editinvname" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อ Zone" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Width="20%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:Label ID="lbl_location_zone" runat="server" Visible="true" Text='<%# Eval("location_zone") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center" HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbzone_status" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("zone_status") %>'></asp:Label>
                                <asp:Label ID="lbzone_statusOnline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="Online"
                                    CssClass="col-sm-12">
                                                <div style="text-align: center; color:green;">
                                                    <span><b>Online</b></span></div>
                                </asp:Label>
                                <asp:Label ID="lbzone_statusOffline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="Offline" CssClass="col-sm-12">
                                                <div style="text-align: center; color:red;">
                                                <span><b>Offline</b></span></div>
                                </asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Width="20%" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <asp:LinkButton ID="btn_edit_table" CssClass="text-edit" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndeletetable" CssClass="text-trash" runat="server" CommandName="cmdDeleteZone"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                CommandArgument='<%#Eval("m1_location_idx") %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

            </asp:GridView>

        </asp:View>

    </asp:MultiView>

</asp:Content>
