﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="nwd_r0_position.aspx.cs" Inherits="websystem_MasterData_nwd_r0_position" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    <asp:MultiView ID="MvMaster" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewIndex" runat="server">
            <div class="col-md-12">
                <asp:LinkButton ID="btnToInsert" CssClass="btn btn-primary" runat="server" CommandName="btnToInsert" OnCommand="btnCommand"><i class="fa fa-plus-square"></i> สร้างตำแหน่งวางอุปกรณ์ </asp:LinkButton>


                <asp:GridView ID="GvMaster"
                    runat="server"
                    AutoGenerateColumns="false"
                    DataKeyNames="REIDX"
                    CssClass="table table-striped table-bordered table-responsive col-md-12 m-t-10"
                    HeaderStyle-CssClass="info"
                    AllowPaging="true"
                    PageSize="10"
                    OnRowEditing="Master_RowEditing"
                    OnRowUpdating="Master_RowUpdating"
                    OnRowCancelingEdit="Master_RowCancelingEdit"
                    OnPageIndexChanging="Master_PageIndexChanging"
                    OnRowDataBound="Master_RowDataBound">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="1" FirstPageText="หน้าแรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center">No result</div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            HeaderStyle-Font-Size="Small" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <small>
                                    <%# (Container.DataItemIndex +1) %>
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="REIDX" runat="server" CssClass="form-control"
                                    Visible="False" Text='<%# Eval("REIDX")%>' />


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">สถานที่ตั้งอุปกรณ์</label>
                                            <asp:TextBox ID="lblFLIDX" CssClass="form-control" runat="server" Text='<%# Bind("place_name") %>' Enabled="false" />
                                            <%-- <asp:DropDownList ID="ddlLocUpdate" AutoPostBack="true" runat="server"
                                                CssClass="form-control">
                                            </asp:DropDownList>--%>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">อาคารที่ตั้งอุปกรณ์</label>
                                            <asp:TextBox ID="Label1" CssClass="form-control" runat="server" Text='<%# Bind("room_name") %>' Enabled="false" />
                                            <%-- <asp:DropDownList ID="ddlLocUpdate" AutoPostBack="true" runat="server"
                                                CssClass="form-control">
                                            </asp:DropDownList>--%>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">ชั้นที่ตั้งอุปกรณ์</label>
                                            <asp:TextBox ID="lblCHIDX" CssClass="form-control" runat="server" Text='<%# Bind("Floor_name") %>' Enabled="false" />
                                           <%-- <asp:DropDownList ID="ddlChamberUpdate" AutoPostBack="true" runat="server"
                                                CssClass="form-control">
                                            </asp:DropDownList>--%>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">อุปกรณ์</label>
                                            <asp:Label ID="lblu0idx" runat="server" Text='<%# Bind("u0idx") %>' Visible="false" />
                                            <asp:DropDownList ID="ddlRegisUpdate" AutoPostBack="true" runat="server"
                                                CssClass="form-control">
                                            </asp:DropDownList>
                                        </small>
                                    </div>
                                </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">row ที่กำหนด</label>
                                            <asp:TextBox ID="txtrowupdate" CssClass="form-control" runat="server" Text='<%# Bind("row_") %>' />

                                        </small>
                                    </div>
                                </div>

                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">cell ที่กำหนด</label>
                                            <asp:TextBox ID="txtcellupdate" CssClass="form-control" runat="server" Text='<%# Bind("cell_") %>' />

                                        </small>
                                    </div>
                                </div>



                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">ชั้นวางตำแหน่งอุปกรณ์</label>
                                            <asp:TextBox ID="txtnoregis_update" CssClass="form-control" runat="server" Text='<%# Bind("no_regis") %>' />

                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <small>
                                            <label class="pull-left">สถานะ</label>
                                            <asp:DropDownList ID="ddlStatusUpdate" AutoPostBack="false" runat="server"
                                                CssClass="form-control" SelectedValue='<%# Eval("RE_status") %>'>
                                                <asp:ListItem Value="1" Text="Online"></asp:ListItem>
                                                <asp:ListItem Value="0" Text="Offline"></asp:ListItem>
                                            </asp:DropDownList>
                                        </small>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="pull-left">
                                        <asp:LinkButton ID="lbCmdUpdate" CssClass="btn btn-success" runat="server"
                                            ValidationGroup="saveTypeNameUpdate" CommandName="Update"
                                            OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')">
                                         บันทึก
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server"
                                            CommandName="Cancel">ยกเลิก</asp:LinkButton>
                                    </div>
                                </div>
                            </EditItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="สถานที่ตั้ง" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Locname" runat="server" Text='<%# Eval("place_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="อาคารที่ตั้ง" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="room_name" runat="server" Text='<%# Eval("room_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชั้นที่ตั้ง" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Floor_name" runat="server" Text='<%# Eval("Floor_name") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="อุปกรณ์ที่ตั้ง" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Regisname" runat="server" Text='<%# Eval("register_number") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขอุปกรณ์ที่ตั้ง" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="no_regis" runat="server" Text='<%# Eval("no_regis") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="row ที่กำหนด" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="row_" runat="server" Text='<%# Eval("row_") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="cell ที่กำหนด" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="cell_" runat="server" Text='<%# Eval("cell_") %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="File Document" ItemStyle-HorizontalAlign="Left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>
                                <asp:HiddenField runat="server" ID="hidFile11" Value='<%#  Eval("img_name") %>' />

                            </ItemTemplate>

                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="Center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <asp:Label ID="Statuscategory" runat="server" Text='<%# getStatus((int)Eval("RE_status")) %>' />
                                </small>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Manage" ItemStyle-HorizontalAlign="center"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                    data-toggle="tooltip" title="แก้ไข"><i class="glyphicon glyphicon-pencil"></i></asp:LinkButton>
                                <asp:LinkButton ID="delete" CssClass="text-trash" runat="server" data-toggle="tooltip"
                                    title="ลบ" CommandName="btnDelete" OnCommand="btnCommand"
                                    CommandArgument='<%# Eval("REIDX") %>'
                                    OnClientClick="return confirm('คุณค้องการลบข้อมูลนี้ใช่หรือไม่')">
                                   <i class="glyphicon glyphicon-trash"></i>
                                </asp:LinkButton>
                            </ItemTemplate>
                            <EditItemTemplate />
                            <FooterTemplate />
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>
        </asp:View>


        <asp:View ID="ViewInsert" runat="server">
            <div class="row col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <asp:LinkButton CssClass="btn btn-danger" data-toggle="tooltip" title="Back" runat="server"
                                CommandName="btnCancel" OnCommand="btnCommand">ย้อนกลับ</asp:LinkButton>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label>ชั้นที่ตั้งอุปกรณ์</label>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>

                                    <asp:DropDownList ID="ddlimage" runat="server" OnSelectedIndexChanged="ddlSelectedIndexChanged" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="requiredddltype"
                                        ValidationGroup="save" runat="server"
                                        Display="Dynamic"
                                        SetFocusOnError="true"
                                        ControlToValidate="ddlimage"
                                        Font-Size="1em" ForeColor="Red"
                                        ErrorMessage="กรุณาเลือกรูปภาพ" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>

                    <%-- <div class="col-md-6">
                        <div class="form-group">
                            <label>ห้องที่ตั้งอุปกรณ์</label>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>

                                    <asp:DropDownList ID="ddlchamber" runat="server" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save" runat="server" Display="None"
                                        ControlToValidate="ddlchamber" Font-Size="11"
                                        ErrorMessage="กรุณาเลือกห้องที่ตั้งอุปกรณ์"
                                        ValidationExpression="กรุณาเลือกห้องที่ตั้งอุปกรณ์" InitialValue="0" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>--%>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>อุปกรณ์ที่ตั้ง</label>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>

                                    <asp:DropDownList ID="ddlregis" runat="server" CssClass="form-control"></asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Save" runat="server" Display="None"
                                        ControlToValidate="ddlregis" Font-Size="11"
                                        ErrorMessage="กรุณาเลือกอุปกรณ์ที่ตั้ง"
                                        ValidationExpression="กรุณาเลือกอุปกรณ์ที่ตั้ง" InitialValue="0" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator1" Width="160" />

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>



                    <div class="col-md-6">
                        <div class="form-group">
                            <label>row ที่กำหนด</label>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtrow" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RqRetxtprice22" ValidationGroup="Save" runat="server" Display="None"
                                        ControlToValidate="txtrow" Font-Size="11"
                                        ErrorMessage="กรุณากรอกจำนวน" />
                                    <asp:RegularExpressionValidator ID="Retxtprice22" runat="server" ValidationGroup="Save" Display="None"
                                        ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                        ControlToValidate="txtrow"
                                        ValidationExpression="^[0-9]{1,10}$" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RqRetxtprice22" Width="160" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender223" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtprice22" Width="160" />


                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>cell ที่กำหนด</label>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtcell" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Save" runat="server" Display="None"
                                        ControlToValidate="txtcell" Font-Size="11"
                                        ErrorMessage="กรุณากรอกจำนวน" />
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationGroup="Save" Display="None"
                                        ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                        ControlToValidate="txtcell"
                                        ValidationExpression="^[0-9]{1,10}$" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender3" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidator2" Width="160" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressionValidator1" Width="160" />




                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <label>เลขอุปกรณ์</label>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtaddnoregis" runat="server" CssClass="form-control"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidators8" ValidationGroup="save" runat="server" Display="None" ControlToValidate="txtaddnoregis" Font-Size="11"
                                        ErrorMessage="กรุณากรอกชั้นวางตำแหน่งอุปกรณ์"
                                        ValidationExpression="กรุณากรอกชั้นวางตำแหน่งอุปกรณ์"
                                        SetFocusOnError="true" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender7" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredFieldValidators8" Width="160" />
                                    <asp:RegularExpressionValidator ID="RegularExpressiosndValidator1" runat="server"
                                        ValidationGroup="Save" Display="None"
                                        ErrorMessage="คุณกรอกข้อมูลเกิน 1000 ตัวอักษร" Font-Size="11"
                                        ControlToValidate="txtaddnoregis"
                                        ValidationExpression="^[\s\S]{0,1000}$"
                                        SetFocusOnError="true" />

                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender12" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularExpressiosndValidator1" Width="160" />

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>สถานะ</label>
                            <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control">
                                <asp:ListItem Value="1" Text="Online" />
                                <asp:ListItem Value="0" Text="Offline" />
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <asp:Button CssClass="btn btn-success" runat="server" CommandName="btnInsert" OnClientClick="return confirm('คุณต้องการเพิ่มรายการนี้ใช่หรือไม่ ?')" OnCommand="btnCommand" Text="บันทึก" ValidationGroup="Save" />
                        </div>
                    </div>

                    <div class="col-lg-12" runat="server" id="div_position" visible="false">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="glyphicon glyphicon-list-alt"></i><strong>&nbsp; แผนผังภาพข้อมูล</strong></h3>
                            </div>
                            <%--  <div class="panel-body">--%>
                            <%-- <div class="form-horizontal" role="form">--%>

                            <div id="div_gridview" runat="server" style="z-index: 1; position: absolute; width: 100%">
                                <asp:GridView ID="GvCount" runat="server" ShowHeader="true"
                                    AutoGenerateColumns="true"
                                    CssClass="table-bordered table-responsive col-lg-12"
                                    HeaderStyle-CssClass="primary"
                                    HeaderStyle-Height="40px"
                                    GridLines="None"
                                    OnRowDataBound="Master_RowDataBound">

                                    <PagerStyle CssClass="pageCustom" />
                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="First" LastPageText="Last" />

                                    <EmptyDataTemplate>
                                    </EmptyDataTemplate>
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <%# (Container.DataItemIndex +1) %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>

                            <div id="div_image" runat="server" style="z-index: unset; width: 100%;">
                                <asp:Image ID="img" runat="server" Width="100%" />

                            </div>

                            <%-- </div>
                   </div>--%>
                        </div>
                    </div>
                </div>
            </div>
        </asp:View>

    </asp:MultiView>
</asp:Content>

