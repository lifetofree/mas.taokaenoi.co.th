﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="hr_blacklist.aspx.cs"
    Inherits="websystem_hr_hr_blacklist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <script>
        $(function () {
            $('.from-date-datepicker').datetimepicker({
                format: 'DD/MM/YYYY'
            });
        });
        //});

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.from-date-datepicker').datetimepicker({
                    format: 'DD/MM/YYYY'
                });
            });
        });

    </script>

    <script>
        $(function () {
            $('.UploadFileblacklist').MultiFile({

            });

        });
        $(function () {
            $('.UploadFileblacklist_edit').MultiFile({

            });

        });

        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            $(function () {
                $('.UploadFileblacklist').MultiFile({

                });

            });

            $(function () {
                $('.UploadFileblacklist_edit').MultiFile({

                });

            });
        });
    </script>

    <asp:Literal ID="litdebug" runat="server"></asp:Literal>
    <asp:MultiView ID="MvSystem" runat="server" ActiveViewIndex="0">
        <asp:View ID="viewindex" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnAdd" CssClass="btn btn-info" Visible="true" data-original-title="เพิ่ม" data-toggle="tooltip" title="เพิ่ม" runat="server"
                    CommandName="cmdAdd" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i> Add Blacklist</asp:LinkButton>
            </div>
            <asp:FormView ID="FvInsert" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">ข้อมูล Blacklist</h3>
                        </div>
                        <div class="panel-body">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="lidcarden" runat="server" Text="ID Card" /><br />
                                                    <small><b>
                                                        <asp:Label ID="lidcardth" runat="server" Text="เลขบัตรประชาชน" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="Idcard" runat="server" CssClass="form-control" placeholder="เลขบัตรประชาชน..." Enabled="true" MaxLength="13" ValidationGroup="Insertblacklist" />
                                                <%--<asp:RequiredFieldValidator ID="Requilidator1" ValidationGroup="Insertblacklist" runat="server" Display="None"
                                                    ControlToValidate="Idcard" Font-Size="11" ErrorMessage="กรุณากรอกเลขบัตรประชาชน" SetFocusOnError="true" />--%>
                                                <%--<ajaxToolkit:ValidatorCalloutExtender ID="Valr1" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requilidator1" Width="160" />--%>
                                                <asp:RegularExpressionValidator ID="Regur1" runat="server" ValidationGroup="Insertblacklist" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                    ControlToValidate="Idcard"
                                                    ValidationExpression="^[0-9]{1,15}$" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validader4" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regur1" Width="160" />
                                            </div>
                                            <%--<div class="col-md-1">
                                                <asp:Label ID="chk1" runat="server" ForeColor="Red" Text="***" />
                                            </div>--%>
                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="lPassposten" runat="server" Text="Passpost" /><br />
                                                    <small><b>
                                                        <asp:Label ID="lPasspostth" runat="server" Text="พาสปอร์ต" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-3">
                                                <asp:TextBox ID="Passpost" runat="server" CssClass="form-control" placeholder="พาสปอร์ต..." Enabled="true" MaxLength="9" ValidationGroup="Insertblacklist" />
                                            </div>
                                            <div class="col-md-1"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="lnameen" runat="server" Text="Name(Thai)" /><br />
                                                    <small><b>
                                                        <asp:Label ID="lnametn" runat="server" Text="ชื่อ(ไทย)" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-1">
                                                <asp:DropDownList ID="ddl_prefix_th" runat="server" CssClass="form-control" AutoPostBack="true" Width="105" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Value="1">นาย</asp:ListItem>
                                                    <asp:ListItem Value="2">นางสาว</asp:ListItem>
                                                    <asp:ListItem Value="3">นาง</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txt_name_th" runat="server" CssClass="form-control" placeholder="ชื่อ..." Enabled="true" ValidationGroup="Insertblacklist" />
                                                <asp:RequiredFieldValidator ID="C_NameTH_add" runat="server" ControlToValidate="txt_name_th" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="Insertblacklist" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_NameTH_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_NameTH_add" Width="160" />
                                                <asp:RegularExpressionValidator ID="R_NamTH_add" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_name_th" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="Insertblacklist" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_NameTH2_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NamTH_add" Width="160" />
                                            </div>
                                            <div class="col-md-1"></div>
                                            <label class="col-md-1 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="llastnameen" runat="server" Text="Lastname" /><br />
                                                    <small><b>
                                                        <asp:Label ID="llastnameth" runat="server" Text="นามสกุล" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txt_surname_th" runat="server" CssClass="form-control" placeholder="นามสกุล..." Enabled="true" ValidationGroup="Insertblacklist" />
                                                <asp:RequiredFieldValidator ID="C_LastnameTH_add" runat="server" ControlToValidate="txt_surname_th" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="Insertblacklist" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameTH_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_LastnameTH_add" Width="160" />
                                                <asp:RegularExpressionValidator ID="R_LastNameTH_add" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_surname_th" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="Insertblacklist" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_LastNameTH2_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_LastNameTH_add" Width="160" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="lnameen1" runat="server" Text="Name(English)" /><br />
                                                    <small><b>
                                                        <asp:Label ID="lnameth1" runat="server" Text="ชื่อ(อังกฤษ)" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-1">
                                                <asp:DropDownList ID="ddl_prefix_en" runat="server" CssClass="form-control" AutoPostBack="true" Width="105" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Value="1">Mr.</asp:ListItem>
                                                    <asp:ListItem Value="2">Miss</asp:ListItem>
                                                    <asp:ListItem Value="3">Mrs.</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:TextBox ID="FirstnameEN" runat="server" CssClass="form-control" placeholder="Firstname..." Enabled="true" ValidationGroup="Insertblacklist" />
                                                <asp:RequiredFieldValidator ID="C_NameEN_add" runat="server" ControlToValidate="FirstnameEN" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล"
                                                    ValidationGroup="Insertblacklist" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_NameEN_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_NameEN_add" Width="160" />
                                                <asp:RegularExpressionValidator ID="R_NameEN_add" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาอังกฤษเท่านั้น" SetFocusOnError="true" Display="None"
                                                    ControlToValidate="FirstnameEN" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="Insertblacklist" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_NameEN2_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NameEN_add" Width="160" />
                                            </div>
                                            <div class="col-md-1"></div>
                                            <label class="col-md-1 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="llastnameen1" runat="server" Text="Lastname" /><br />
                                                    <small><b>
                                                        <asp:Label ID="llastnameth1" runat="server" Text="นามสกุล" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-3">
                                                <asp:TextBox ID="LastnameEN" runat="server" CssClass="form-control" placeholder="Lastname..." Enabled="true" ValidationGroup="Insertblacklist" />
                                                <asp:RequiredFieldValidator ID="C_LastnameEN_add" runat="server" ControlToValidate="LastnameEN" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="Insertblacklist" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameEN_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_LastnameEN_add" Width="160" />
                                                <asp:RegularExpressionValidator ID="R_LastNameEN_add" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาอังกฤษเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="LastnameEN" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="Insertblacklist" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameEN2_add" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_LastNameEN_add" Width="160" />
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="LNaten" runat="server" Text="Nationality" /><br />
                                                    <small><b>
                                                        <asp:Label ID="LNatth" runat="server" Text="สัญชาติ" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-4">
                                                <asp:DropDownList ID="ddlnation" AutoPostBack="true" CssClass="form-control" runat="server">
                                                    <asp:ListItem Value="0">เลือกสัญชาติ....</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>

                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Lraceen" runat="server" Text="Race" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Lraceth" runat="server" Text="เชื้อชาติ" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-3">
                                                <asp:DropDownList ID="ddlrace" runat="server" CssClass="form-control">
                                                    <asp:ListItem Value="0">เลือกเชื้อชาติ....</asp:ListItem>
                                                    <asp:ListItem Value="1">ไทย</asp:ListItem>
                                                    <asp:ListItem Value="2">ลาว</asp:ListItem>
                                                    <asp:ListItem Value="3">พม่า</asp:ListItem>
                                                    <asp:ListItem Value="4">กัมพูชา</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="LDescriptionen" runat="server" Text="Description" /><br />
                                                    <small><b>
                                                        <asp:Label ID="LDescriptionth" runat="server" Text="รายละเอียด" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txtnote" CssClass="form-control" runat="server" placeholder="รายละเอียด ..." autocomplete="off" MaxLength="250" TextMode="MultiLine" Rows="3" ValidationGroup="AddReference1"></asp:TextBox>
                                            </div>
                                            <label class="col-md-6 control-label">
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label1" runat="server" Text="Blacklist" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Label2" runat="server" Text="ข้อมูล Blacklist" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-10">
                                                <%--  <div id="grdCharges" runat="server" style="width: 875px; overflow: auto; height: 318px;">--%>
                                                <asp:GridView ID="Gvblacklist" runat="server" Visible="true"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                                                    HeaderStyle-CssClass="success"
                                                    AllowPaging="true"
                                                    Width="150%"
                                                    AutoPostBack="false">
                                                    <PagerStyle CssClass="pageCustom" />
                                                    <%--  <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />--%>

                                                    <%-- <div style="width: 100%; height: 400px; overflow: scroll"></div>--%>
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                            ItemStyle-HorizontalAlign="left" HeaderStyle-Width="1%" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <div style="text-align: center; padding-top: 5px;">
                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                    </div>
                                                                </small>

                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="center"
                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="1%" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <%-- <asp:UpdatePanel ID="Updatecheckbox" runat="server">
                                                                  <ContentTemplate>--%>
                                                                <small>

                                                                    <asp:CheckBox ID="chk_full" runat="server" AutoPostBack="true" OnCheckedChanged="RowIndexChangedBlacklist" />
                                                                    <%--<center><asp:CheckBox ID="chk_full" runat="server" OnCheckedChanged="RowIndexChangedBlacklist"/></center>--%>
                                                                   
                                                                </small>
                                                                <%--</ContentTemplate>
                                                                    <Triggers>
                                                                        <asp:ASyncPostBackTrigger ControlID="chk_full"  />
                                                                    </Triggers>
                                                                </asp:UpdatePanel>--%>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ข้อมูล Blacklist" ItemStyle-HorizontalAlign="left"
                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <div style="padding-top: 5px;">
                                                                        <asp:Label ID="idxU1" runat="server" Text='<%# Eval("Blacklist_idx") %>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="lbl_test" runat="server" Text='<%# Eval("Blacklist_detail") %>'></asp:Label>
                                                                    </div>
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="left"
                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <div style="padding-top: 5px;">
                                                                        <asp:TextBox ID="blacklist_note" PlaceHolder="หมายเหตุ ..." runat="server" CssClass="form-control" Enabled="true" Visible="false" />
                                                                    </div>
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                <%-- </div>--%>
                                            </div>
                                        </div>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-group">

                                                    <asp:Label ID="Labelf12" class="col-sm-2 control-label" runat="server" Text="Upload File " />
                                                    <div class="col-sm-7">
                                                        <asp:FileUpload ID="UploadFileblacklist" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="control-label UploadFileblacklist multi max-5" accept="jpg|pdf|png" />
                                                        <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpg|pdf|png </font></p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label"></label>
                                                    <div class="col-sm-4">

                                                        <asp:LinkButton ID="btnSave" ValidationGroup="Insertblacklist" CssClass="btn btn-success" Text="บันทึก" data-toggle="tooltip"
                                                            title="บันทึก" runat="server" CommandName="CmdSave" OnCommand="btnCommand" OnClientClick="return confirm('ยืนยันการบันทึก')"></asp:LinkButton>
                                                        <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" Text="ยกเลิก" OnCommand="btnCommand" CommandName="CmdCancel" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                                    </div>
                                                    <label class="col-md-6 control-label"></label>
                                                </div>

                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnSave" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                </InsertItemTemplate>
            </asp:FormView>


            <asp:UpdatePanel ID="_PanelSearchBlacklist" runat="server" UpdateMode="Conditional">
                <ContentTemplate>

                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">ค้นหา Blacklist </h3>
                        </div>

                        <div class="panel-body">

                            <div class="col-md-10 col-md-offset-1">

                                <div class="col-md-12">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>เลขบัตรประชาชน</label>
                                            <asp:TextBox ID="txt_idcard_search" MaxLength="13" placeholder="เลขบัตรประชาชน" ValidationGroup="searchblacklist" runat="server" CssClass="form-control"> </asp:TextBox>

                                            <asp:RegularExpressionValidator ID="Regur11" runat="server" ValidationGroup="searchblacklist" Display="None"
                                                ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                ControlToValidate="txt_idcard_search"
                                                ValidationExpression="^[0-9]{1,15}$" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="Validader4111" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regur11" PopupPosition="BottomLeft" Width="160" />


                                          
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>พาสปอร์ต</label>
                                            <asp:TextBox ID="txt_passport_search" MaxLength="9" placeholder="พาสปอร์ต" runat="server" CssClass="form-control"> </asp:TextBox>


                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <asp:LinkButton ID="btnsearch" ValidationGroup="searchblacklist" CssClass="btn btn-success" Text="ค้นหา" data-toggle="tooltip"
                                                title="ค้นหา" runat="server" CommandName="Cmdsearch" OnCommand="btnCommand"></asp:LinkButton>

                                            <asp:LinkButton ID="btnclear" CssClass="btn btn-default" Text="ล้างค่า" data-toggle="tooltip"
                                                title="ล้างค่า" runat="server" CommandName="Cmdclear" OnCommand="btnCommand"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>

                            </div>


                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>

            <div id="Gvblacklistu0_scroll" style="overflow-x: auto; width: 100%" runat="server">
                <asp:GridView ID="Gvblacklistu0" runat="server" Visible="true"
                    AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                    HeaderStyle-CssClass="success"
                    AllowPaging="true"
                    PageSize="10"
                    OnPageIndexChanging="U0_PageIndexChanging"
                    AutoPostBack="false">
                    <PagerStyle CssClass="pageCustom" />
                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                    <EmptyDataTemplate>
                        <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                    </EmptyDataTemplate>
                    <Columns>
                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                            ItemStyle-HorizontalAlign="left" HeaderStyle-Width="1%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style="text-align: center; padding-top: 5px;">
                                        <%# (Container.DataItemIndex + 1) %>
                                    </div>
                                </small>

                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="เลขบัตรประชาชน" ItemStyle-HorizontalAlign="left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style="padding-top: 5px;">
                                        <asp:Label ID="lbl_Idcard" runat="server"
                                            Text='<%# Eval("Idcard") %>'></asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="พาสปอร์ต" ItemStyle-HorizontalAlign="left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style="padding-top: 5px;">
                                        <asp:Label ID="lbl_Passpost" runat="server"
                                            Text='<%# Eval("Passpost") %>'></asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ชื่อ - นามสกุล (ไทย)" ItemStyle-HorizontalAlign="left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style="padding-top: 5px;">
                                        <asp:Label ID="lbl_PrefixIDXTH" runat="server"
                                            Text='<%# Eval("PrefixnameTH") %>'></asp:Label>
                                        <asp:Label ID="lbl_Firstname" runat="server"
                                            Text='<%# Eval("FirstnameTH") %>'></asp:Label>
                                        <asp:Label ID="lbl_LastnameTH" runat="server"
                                            Text='<%# Eval("LastnameTH") %>'></asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:TemplateField HeaderText="ชื่อ - นามสกุล (อังกฤษ)" ItemStyle-HorizontalAlign="left"
                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                            <ItemTemplate>
                                <small>
                                    <div style="padding-top: 5px;">
                                        <asp:Label ID="lbl_PrefixIDXEN" runat="server"
                                            Text='<%# Eval("PrefixnameEN") %>'></asp:Label>
                                        <asp:Label ID="lbl_FirstnameEN" runat="server"
                                            Text='<%# Eval("FirstnameEN") %>'></asp:Label>
                                        <asp:Label ID="lbl_LastnameEN" runat="server"
                                            Text='<%# Eval("LastnameEN") %>'></asp:Label>
                                    </div>
                                </small>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center" ItemStyle-CssClass="text-center" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:UpdatePanel ID="UpdatePnFileUploadEdit" runat="server">
                                    <ContentTemplate>
                                        <asp:LinkButton ID="btnViewblacklistDetail" CssClass="btn btn-info btn-xs" target="" runat="server" CommandName="cmdViewblacklistDetail" OnCommand="btnCommand"
                                            CommandArgument='<%# Eval("U0_IDX")%>'
                                            data-toggle="tooltip" title="ดูข้อมูล"><i class="fa fa-file"></i></asp:LinkButton>

                                        <asp:LinkButton ID="btnDeleteblacklistDetail" CssClass="btn btn-danger btn-xs" target="" runat="server" CommandName="cmdDeleteblacklistDetail" OnCommand="btnCommand"
                                            CommandArgument='<%# Eval("U0_IDX")%>' OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                            data-toggle="tooltip" title="ลบข้อมูล"><i class="fa fa-trash"></i></asp:LinkButton>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>


            <asp:FormView ID="FvDetailBlacklist" runat="server" Width="100%">

                <ItemTemplate>
                    <asp:UpdatePanel ID="UpdatePnFileUploadEdit_blacklist" runat="server">
                        <ContentTemplate>
                            <asp:LinkButton ID="btnBackToDetailIndexblacklist" runat="server" CssClass="btn btn-danger" OnCommand="btnCommand"
                                CommandName="cmdBackToDetailIndexblacklist"><i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ</asp:LinkButton>
                            <div class="form-group pull-right">
                                <asp:LinkButton ID="btnEditDetailBlacklist" CssClass="btn btn-info" runat="server" CommandName="cmdEditDetailBlacklist" OnCommand="btnCommand" CommandArgument='<%# Eval("U0_idx")%>' Text="แก้ไขข้อมูล Blacklist" />
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnEditDetailBlacklist" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <%--<label>&nbsp;</label>--%>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="panel panel-info">
                            <div class="panel-heading f-bold">รายละเอียด Blacklist</div>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="col-md-2 col-md-offset-1">
                                                    <asp:Label ID="lblIdcard" runat="server" CssClass="f-s-13 f-bold" Text="เลขบัตรประชาชน :" />
                                                </div>
                                                <div class="col-md-7 m-b-5">
                                                    <asp:Label ID="lbl_Idcard_detail" runat="server" Text='<%# Eval("Idcard") %>' CssClass="f-s-13" />
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="col-md-2 col-md-offset-1">
                                                    <asp:Label ID="lblPasspost" runat="server" CssClass="f-s-13 f-bold" Text="พาสปอร์ต :" />
                                                </div>
                                                <div class="col-md-7 m-b-5">
                                                    <asp:Label ID="lbl_Passpost_detail" runat="server" Text='<%# Eval("Passpost") %>' CssClass="f-s-13" />
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="col-md-2 col-md-offset-1">
                                                    <asp:Label ID="lbl_nameTH" runat="server" CssClass="f-s-13 f-bold" Text="ชื่อ-นามสกุล(ไทย) :" />
                                                </div>
                                                <div class="col-md-7 m-b-5">
                                                    <asp:Label ID="lbl_PrefixIDXTH" runat="server" Text='<%# Eval("PrefixnameTH") %>'></asp:Label>
                                                    <asp:Label ID="lbl_FirstnameTH" runat="server" Text='<%# Eval("FirstnameTH") %>'></asp:Label>
                                                    <asp:Label ID="lbl_LastnameTH" runat="server" Text='<%# Eval("LastnameTH") %>'></asp:Label>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="col-md-2 col-md-offset-1">
                                                    <asp:Label ID="lbl_nameEN" runat="server" CssClass="f-s-13 f-bold" Text="ชื่อ-นามสกุล(อังกฤษ) :" />
                                                </div>
                                                <div class="col-md-7 m-b-5">
                                                    <asp:Label ID="lbl_PrefixIDXEN" runat="server" Text='<%# Eval("PrefixnameEN") %>'></asp:Label>
                                                    <asp:Label ID="lbl_FirstnameEN" runat="server" Text='<%# Eval("FirstnameEN") %>'></asp:Label>
                                                    <asp:Label ID="lbl_LastnameEN" runat="server" Text='<%# Eval("LastnameEN") %>'></asp:Label>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="col-md-2 col-md-offset-1">
                                                    <asp:Label ID="lbl_nat" runat="server" CssClass="f-s-13 f-bold" Text="สัญชาติ :" />
                                                </div>
                                                <div class="col-md-7 m-b-5">
                                                    <asp:Label ID="lbl_nat_detail" runat="server" Text='<%# Eval("NatName") %>'></asp:Label>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="col-md-2 col-md-offset-1">
                                                    <asp:Label ID="lbl_Race" runat="server" CssClass="f-s-13 f-bold" Text="เชื้อชาติ :" />
                                                </div>
                                                <div class="col-md-7 m-b-5">
                                                    <asp:Label ID="lbl_Race_detail" runat="server" Text='<%# Eval("RaceName") %>'></asp:Label>
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="col-md-2 col-md-offset-1">
                                                    <asp:Label ID="lbl_note" runat="server" CssClass="f-s-13 f-bold" Text="รายละเอียด :" />
                                                </div>
                                                <div class="col-md-7 m-b-5">
                                                    <asp:Label ID="lbl_note_detail" runat="server" Text='<%# Eval("U0_note") %>'></asp:Label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="col-md-2 col-md-offset-1">
                                                    <asp:Label ID="lbl_backlist" runat="server" CssClass="f-s-13 f-bold" Text="ข้อมูลBlacklist :" />
                                                </div>
                                                <div class="col-md-7 m-b-5">
                                                    <asp:GridView ID="GvDetailblacklist" runat="server" Visible="true"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover"
                                                        HeaderStyle-CssClass="success"
                                                        HeaderStyle-Height="40px"
                                                        AllowPaging="true"
                                                        PageSize="10"
                                                        Width="800px"
                                                        HorizontalAlign="Center"
                                                        AutoPostBack="false">
                                                        <PagerStyle CssClass="pageCustom" />
                                                        <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                                                        <EmptyDataTemplate>
                                                            <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                                                        </EmptyDataTemplate>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                                ItemStyle-HorizontalAlign="left" HeaderStyle-Width="1%" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <small>
                                                                        <div style="text-align: center; padding-top: 5px;">
                                                                            <%# (Container.DataItemIndex + 1) %>
                                                                        </div>
                                                                    </small>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="ข้อมูล Blacklist" ItemStyle-HorizontalAlign="left"
                                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <small>
                                                                        <div style="padding-top: 5px;">
                                                                            <asp:Label ID="lbl_Blacklist_detail" runat="server" Text='<%# Eval("Blacklist_detail") %>'></asp:Label>
                                                                        </div>
                                                                    </small>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="left"
                                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="20%" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <small>
                                                                        <div style="padding-top: 5px;">
                                                                            <asp:Label ID="lbl_u1_note" runat="server" Text='<%# Eval("u1_note") %>'></asp:Label>
                                                                        </div>
                                                                    </small>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="col-md-2 col-md-offset-1">
                                                    <asp:Label ID="lbl_fileblacklist" CssClass="f-s-13 f-bold" runat="server" Text="รายละเอียดไฟล์ :" />
                                                </div>
                                                <div class="col-md-7 m-b-5">
                                                    <asp:GridView ID="gvFileblacklist" Visible="true" runat="server"
                                                        AutoGenerateColumns="false"
                                                        CssClass="table table-striped table-bordered table-hover"
                                                        HeaderStyle-CssClass="success"
                                                        OnRowDataBound="Master_RowDataBound"
                                                        BorderStyle="None"
                                                        Width="800px"
                                                        CellSpacing="2"
                                                        Font-Size="Small">
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                                ItemStyle-HorizontalAlign="left" HeaderStyle-Width="1%" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <small>
                                                                        <div style="text-align: center; padding-top: 5px;">
                                                                            <%# (Container.DataItemIndex + 1) %>
                                                                        </div>
                                                                    </small>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="ไฟล์เอกสาร" ItemStyle-HorizontalAlign="left"
                                                                HeaderStyle-CssClass="text-center" HeaderStyle-Width="35%" HeaderStyle-Font-Size="Small">
                                                                <ItemTemplate>
                                                                    <small>
                                                                        <div class="col-lg-10">
                                                                            <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />

                                                                        </div>
                                                                        <div class="col-lg-2">
                                                                            <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>

                                                                            <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                                        </div>
                                                                    </small>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                </ItemTemplate>

                <EditItemTemplate>
                    <div class="form-group">
                        <asp:LinkButton runat="server" CssClass="btn btn-danger" OnCommand="btnCommand" CommandName="CmdCancel_edit" CommandArgument='<%# Eval("U0_idx") %>' Text="ยกเลิก"><i class="fa fa-angle-left fa-lg"></i> ย้อนกลับ</asp:LinkButton>
                    </div>
                    <%--<label>&nbsp;</label>--%>
                    <div class="clearfix"></div>
                    <div class="panel panel-info">
                        <div class="panel-heading f-bold">แก้ไขข้อมูล Blacklist</div>
                        <div class="panel-body">
                            <div class="panel-heading">
                                <div class="form-horizontal" role="form">
                                    <div class="panel-heading">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="lidcarden_edit" runat="server" Text="ID Card" /><br />
                                                    <small><b>
                                                        <asp:Label ID="lidcardth_edit" runat="server" Text="เลขบัตรประชาชน" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-4">
                                                <asp:Label ID="lbl_u0_blacklist_idx_edit" runat="server" Visible="false" Text='<%# Eval("U0_idx") %>' CssClass="f-s-13" />
                                                <asp:TextBox ID="txt_Idcard_edit" runat="server" CssClass="form-control" Text='<%# Eval("Idcard") %>' MaxLength="13" Enabled="true" ValidationGroup="SaveEdit" />
                                                <%--<asp:RequiredFieldValidator ID="Requilidator1_edit" ValidationGroup="SaveEdit" runat="server" Display="None"
                                                    ControlToValidate="txt_Idcard_edit" Font-Size="11" ErrorMessage="กรุณากรอกเลขบัตรประชาชน" SetFocusOnError="true" />--%>
                                                <%--<ajaxToolkit:ValidatorCalloutExtender ID="Valr1_edit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Requilidator1_edit" Width="160" />--%>
                                                <asp:RegularExpressionValidator ID="Regur1_edit" runat="server" ValidationGroup="SaveEdit" Display="None"
                                                    ErrorMessage="กรุณากรอกเฉพาะข้อมูลตัวเลข" Font-Size="11"
                                                    ControlToValidate="txt_Idcard_edit"
                                                    ValidationExpression="^[0-9]{1,15}$" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="Validader4_edit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="Regur1_edit" Width="160" />

                                            </div>
                                            <%--<div class="col-md-1">
                                                <asp:Label ID="chk1" runat="server" ForeColor="Red" Text="***" />
                                            </div>--%>

                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="lPassposten_edit" runat="server" Text="Passpost" /><br />
                                                    <small><b>
                                                        <asp:Label ID="lPasspostth_edit" runat="server" Text="พาสปอร์ต" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txt_Passpost_edit" runat="server" MaxLength="9" CssClass="form-control" Text='<%# Eval("Passpost") %>' Enabled="true" />
                                            </div>
                                            <div class="col-md-1"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="lnameen_edit" runat="server" Text="Name(Thai)" /><br />
                                                    <small><b>
                                                        <asp:Label ID="lnametn_edit" runat="server" Text="ชื่อ(ไทย)" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-1">
                                                <asp:TextBox ID="txt_Prefixth_edit" runat="server" CssClass="form-control" Text='<%# Eval("PrefixIDXTH") %>' Visible="false" />
                                                <asp:DropDownList ID="ddl_prefixth_edit" runat="server" AutoPostBack="true" CssClass="form-control" Width="105" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Value="1">นาย</asp:ListItem>
                                                    <asp:ListItem Value="2">นางสาว</asp:ListItem>
                                                    <asp:ListItem Value="3">นาง</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txt_nameth_edit" runat="server" CssClass="form-control" Text='<%# Eval("FirstnameTH") %>' Enabled="true" ValidationGroup="SaveEdit" />
                                                <asp:RequiredFieldValidator ID="C_NameTH__edit" runat="server" ControlToValidate="txt_nameth_edit" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_NameTH__edit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_NameTH__edit" Width="160" />
                                                <asp:RegularExpressionValidator ID="R_NamTH_edit" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_nameth_edit" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_NameTH2_edit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NamTH_edit" Width="160" />
                                            </div>
                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="llastnameen_edit" runat="server" Text="Lastname" /><br />
                                                    <small><b>
                                                        <asp:Label ID="llastnameth_edit" runat="server" Text="นามสกุล" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txt_surnameth_edit" runat="server" CssClass="form-control" Text='<%# Eval("LastnameTH") %>' Enabled="true" ValidationGroup="SaveEdit" />
                                                <asp:RequiredFieldValidator ID="C_LastnameTH_edit" runat="server" ControlToValidate="txt_surnameth_edit" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameTH_edit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_LastnameTH_edit" Width="160" />
                                                <asp:RegularExpressionValidator ID="R_LastNameTH_edit" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาไทยเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_surnameth_edit" ValidationExpression="^[ก-๙'.\s]{1,250}$" ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_LastNameTH2_edit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_LastNameTH_edit" Width="160" />
                                            </div>
                                            <div class="col-md-1"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="lnameen_edit1" runat="server" Text="Name(English)" /><br />
                                                    <small><b>
                                                        <asp:Label ID="lnameth_edit1" runat="server" Text="ชื่อ(อังกฤษ)" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-1">
                                                <asp:TextBox ID="txt_prefixen_edit" runat="server" CssClass="form-control" Text='<%# Eval("PrefixIDXEN") %>' Visible="false" />
                                                <asp:DropDownList ID="ddl_prefixen_edit" runat="server" CssClass="form-control" Width="105" AutoPostBack="true" OnSelectedIndexChanged="ddlSelectedIndexChanged">
                                                    <asp:ListItem Value="1">Mr.</asp:ListItem>
                                                    <asp:ListItem Value="2">Miss</asp:ListItem>
                                                    <asp:ListItem Value="3">Mrs.</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txt_FirstnameEN_edit" runat="server" CssClass="form-control" Text='<%# Eval("FirstnameEN") %>' Enabled="true" ValidationGroup="SaveEdit" />
                                                <asp:RequiredFieldValidator ID="C_NameEN_edit" runat="server" ControlToValidate="txt_FirstnameEN_edit" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล"
                                                    ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_NameEN_edit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_NameEN_edit" Width="160" />
                                                <asp:RegularExpressionValidator ID="R_NameEN_edit" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาอังกฤษเท่านั้น" SetFocusOnError="true" Display="None"
                                                    ControlToValidate="txt_FirstnameEN_edit" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_NameEN2_edit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_NameEN_edit" Width="160" />
                                            </div>
                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="llastnameen_edit1" runat="server" Text="Lastname" /><br />
                                                    <small><b>
                                                        <asp:Label ID="llastnameth_edit1" runat="server" Text="นามสกุล" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txt_LastnameEN_edit" runat="server" CssClass="form-control" Text='<%# Eval("LastnameEN") %>' Enabled="true" ValidationGroup="SaveEdit" />
                                                <asp:RequiredFieldValidator ID="C_LastnameEN_edit" runat="server" ControlToValidate="txt_LastnameEN_edit" Display="None" SetFocusOnError="true" ErrorMessage="*กรอกข้อมูล" ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameEN_edit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="C_LastnameEN_edit" Width="160" />
                                                <asp:RegularExpressionValidator ID="R_LastNameEN_edit" runat="server" ErrorMessage="*เฉพาะตัวอักษรภาษาอังกฤษเท่านั้น" SetFocusOnError="true" Display="None" ControlToValidate="txt_LastnameEN_edit" ValidationExpression="^[a-zA-Z'.\s]{1,250}$" ValidationGroup="SaveEdit" />
                                                <ajaxToolkit:ValidatorCalloutExtender ID="V_LastnameEN2_edit" runat="Server" HighlightCssClass="validatorCalloutHighlight" TargetControlID="R_LastNameEN_edit" Width="160" />

                                            </div>
                                            <div class="col-md-1"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="LNaten_edit" runat="server" Text="Nationality" /><br />
                                                    <small><b>
                                                        <asp:Label ID="LNatth_edit" runat="server" Text="สัญชาติ" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-4">
                                                <asp:Label ID="txt_nat_edit" runat="server" Text='<%# Bind("NatIDX") %>' Visible="false" />
                                                <asp:DropDownList ID="ddlnation_edit" runat="server" AutoPostBack="true" Enabled="true" CssClass="form-control">
                                                    <asp:ListItem Value="0">เลือกสัญชาติ....</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Lraceen_edit" runat="server" Text="Race" /><br />
                                                    <small><b>
                                                        <asp:Label ID="Lraceth_edit" runat="server" Text="เชื้อชาติ" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-3">
                                                <asp:TextBox ID="txt_race_edit" runat="server" CssClass="form-control" Text='<%# Eval("RaceIDX") %>' Visible="false" />
                                                <asp:DropDownList ID="ddl_race_edit" runat="server" EnableViewState="true" CssClass="form-control">
                                                    <asp:ListItem Value="1">ไทย</asp:ListItem>
                                                    <asp:ListItem Value="2">ลาว</asp:ListItem>
                                                    <asp:ListItem Value="3">พม่า</asp:ListItem>
                                                    <asp:ListItem Value="4">กัมพูชา</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="LDescriptionen_edit" runat="server" Text="Description" /><br />
                                                    <small><b>
                                                        <asp:Label ID="LDescriptionth_edit" runat="server" Text="รายละเอียด" /></b></small>
                                                </h2>
                                            </label>
                                            <div class="col-md-4">
                                                <asp:TextBox ID="txt_note_edit" CssClass="form-control" runat="server" Text='<%# Eval("U0_note") %>' Enabled="true" autocomplete="off" MaxLength="250" TextMode="MultiLine" Rows="3" ValidationGroup="AddReference1">
                                                </asp:TextBox>
                                            </div>
                                            <label class="col-md-6 control-label">
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label3" runat="server" Text="ข้อมูล Blacklist" /><br />
                                                    <%--  <small><b>
                                                        <asp:Label ID="Label4" runat="server" Text="ชื่อ(ไทย)" /></b></small>--%>
                                                </h2>
                                            </label>
                                            <div class="col-md-10">
                                                <%--<div id="grdCharges" runat="server" style="width: 875px; overflow: auto; height: 318px;">--%>
                                                <asp:GridView ID="Gvblacklist_edit" runat="server" Visible="true"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                                                    HeaderStyle-CssClass="success"
                                                    AllowPaging="true"
                                                    Width="150%"
                                                    AutoPostBack="false">
                                                    <PagerStyle CssClass="pageCustom" />
                                                    <PagerSettings Mode="NumericFirstLast" PageButtonCount="4" FirstPageText="หน้าเเรก" LastPageText="หน้าสุดท้าย" />
                                                    <EmptyDataTemplate>
                                                        <div style="text-align: center; color: red"><b>-- ไม่พบข้อมูล --</b> </div>
                                                    </EmptyDataTemplate>
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                            ItemStyle-HorizontalAlign="left" HeaderStyle-Width="1%" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <div style="text-align: center; padding-top: 5px;">
                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                    </div>
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="left"
                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="1%" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <div style="padding-top: 5px;">
                                                                        <asp:Label ID="lbl_idxu1_edit" runat="server" Text='<%# Eval("U1_IDX1") %>' Visible="false"></asp:Label>
                                                                        <center><asp:CheckBox ID="chk_full_edit" runat="server" AutoPostBack="true" Checked='<%# Convert.ToBoolean(Eval("U1_BlacklistIDX")) %>' OnCheckedChanged="RowIndexChangedBlacklist_edit"/></center>
                                                                        <%--<center><asp:CheckBox ID="chk_full_edit" runat="server" Checked='<%# Convert.ToBoolean(Eval("U1_BlacklistIDX")) %>' OnCheckedChanged="RowIndexChangedBlacklist_edit"/></center>--%>
                                                                    </div>
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="ข้อมูล Blacklist" ItemStyle-HorizontalAlign="left"
                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <div style="padding-top: 5px;">
                                                                        <asp:Label ID="idxU1_edit" runat="server" Text='<%# Eval("M0_blacklistidx") %>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="lbl_Blacklistdetail_edit" runat="server" Text='<%# Eval("M0_Blacklistdetail") %>'></asp:Label>
                                                                    </div>
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="หมายเหตุ" ItemStyle-HorizontalAlign="left"
                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="15%" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <div style="padding-top: 5px;">
                                                                        <asp:TextBox ID="blacklist_note_edit" runat="server" Text='<%# Eval("U1_note") %>' CssClass="form-control" Enabled="true" />
                                                                    </div>
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                                <%-- </div>--%>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                <h2 class="panel-title">
                                                    <asp:Label ID="Label5" runat="server" Text="รายละเอียดไฟล์" /><br />
                                                    <%--<small><b>
                                                        <asp:Label ID="Label6" runat="server" Text="ชื่อ(ไทย)" /></b></small>--%>
                                                </h2>
                                            </label>
                                            <div class="col-md-4">
                                                <asp:GridView ID="gvFileblacklist_edit" Visible="true" runat="server"
                                                    AutoGenerateColumns="false"
                                                    CssClass="table table-striped table-bordered table-responsive footable col-md-12"
                                                    HeaderStyle-CssClass="success"
                                                    OnRowDataBound="Master_RowDataBound"
                                                    BorderStyle="None"
                                                    CellSpacing="2"
                                                    Width="870px"
                                                    Font-Size="Small"
                                                    AutoPostBack="false">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                                                            ItemStyle-HorizontalAlign="left" HeaderStyle-Width="1%" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <div style="text-align: center; padding-top: 5px;">
                                                                        <%# (Container.DataItemIndex + 1) %>
                                                                    </div>
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ไฟล์เอกสาร" ItemStyle-HorizontalAlign="left"
                                                            HeaderStyle-CssClass="text-center" HeaderStyle-Width="30%" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <small>
                                                                    <div class="col-lg-10">
                                                                        <asp:Literal ID="ltFileName11" runat="server" Text='<%# Eval("FileName") %>' />
                                                                    </div>

                                                                    <div class="col-lg-2">
                                                                        <asp:HyperLink runat="server" ID="btnDL11" CssClass="btn btn-default" data-original-title="Download" Target="_blank"><i class="fa fa-download"></i></asp:HyperLink>
                                                                        <asp:HiddenField runat="server" ID="hidFile11" Value='<%# Eval("Download") %>' />
                                                                    </div>
                                                                </small>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="จัดการข้อมูล" HeaderStyle-Width="1%" HeaderStyle-CssClass="text-center"
                                                            ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                                                            <ItemTemplate>
                                                                <div style="padding-top: 5px">
                                                                    <%-- <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                                                        data-toggle="tooltip" title="edit"> <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>--%>
                                                                    <asp:LinkButton ID="btndeleteupdatefile" CssClass="text-trash" runat="server" CommandName="Delete_updatefile"
                                                                        data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('คุณต้องการลบรายการนี้ใช่หรือไม่ ?')"
                                                                        CommandArgument='<%#Eval("FileName") %>' title="delete">
                                                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>

                                        <asp:UpdatePanel ID="Updateblacklistedit" runat="server" UpdateMode="Conditional">
                                            <ContentTemplate>
                                                <div class="form-group">

                                                    <asp:Label ID="Labelf122" class="col-sm-2 control-label" runat="server" Text="Upload File " />
                                                    <div class="col-sm-7">
                                                        <asp:FileUpload ID="UploadFileblacklist_edit" ViewStateMode="Enabled" AutoPostBack="true" Font-Size="small" ClientIDMode="Static" runat="server" CssClass="control-label UploadFileblacklist multi max-5" accept="jpg|pdf|png" />
                                                        <p class="help-block"><font color="red">**แนบเอกสารเพิ่มเติม เฉพาะนามสกุล jpg|pdf|png </font></p>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <label class="col-md-2 control-label"></label>
                                                    <div class="col-sm-4">
                                                        <asp:LinkButton ID="btnSaveUpdate" CssClass="btn btn-success" runat="server" OnCommand="btnCommand"
                                                            CommandName="cmdSaveEdit" CommandArgument='<%# Eval("U0_idx") %>' Text="บันทึกการเปลี่ยนแปลง" ValidationGroup="SaveEdit"
                                                            OnClientClick="return confirm('ยืนยันการบันทึก')" />
                                                        <%--<asp:LinkButton CssClass="btn btn-danger" runat="server" OnCommand="btnCommand"
                                                                CommandName="CmdCancel_edit" CommandArgument='<%# Eval("U0_idx") %>' Text="ยกเลิก" />--%>
                                                    </div>
                                                    <label class="col-md-6 control-label">
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnSaveUpdate" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </EditItemTemplate>

            </asp:FormView>

            <div class="panel panel-info" id="div_Logblacklist" runat="server">
                <div class="panel-heading">ประวัติการดำเนินการ</div>
                <table class="table table-striped f-s-12 table-empshift-responsive">
                    <asp:Repeater ID="rptLogblacklist" runat="server">
                        <HeaderTemplate>
                            <tr>
                                <th>วัน / เวลาที่สร้าง</th>
                                <th>วัน / เวลาที่แก้ไข</th>
                                <th>ผู้ดำเนินการ</th>
                                <%--<th>สถานะ</th>--%>
                            </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td data-th="วัน / เวลาที่สร้าง"><%# Eval("create_date") %> <%# Eval("time_create") %></td>
                                <td data-th="วัน / เวลาที่แก้ไข"><%# Eval("update_date") %> <%# Eval("time_update") %></td>
                                <td data-th="ผู้ดำเนินการ"><%# Eval("emp_name_th") %> </td>
                                <%--<td data-th="สถานะ"><%# Eval("log_status") %></td>--%>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                </table>
                <div class="m-t-10 m-b-10"></div>
            </div>

        </asp:View>
    </asp:MultiView>
</asp:Content>

