﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_cims_m0_form_signature.aspx.cs" Inherits="websystem_MasterData_qa_cims_m0_form_signature" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <asp:Literal ID="litDebug" runat="server"></asp:Literal>

    <asp:MultiView ID="mvMultiview" runat="server" ActiveViewIndex="0">

        <asp:View ID="ViewInsert" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnAddSignature" CssClass="btn btn-primary" Visible="true" data-original-title="เพิ่มลายเซ็นต์" data-toggle="tooltip" title="เพิ่มลายเซ็นต์" runat="server"
                    CommandName="cmdAddSignature" OnCommand="btnCommand"><i class="fa fa-plus-square" aria-hidden="true"></i>&nbsp;เพิ่มลายเซ็นต์</asp:LinkButton>
            </div>

            <asp:FormView ID="fvInsertSignature" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h4 class="panel-title">เพิ่มลายเซ็นต์</h4>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">
                                    <asp:Label ID="lbl_formcalculate" runat="server" Text="เลือกฟอร์ม" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddl_formcalculate" runat="server" CssClass="form-control" Enabled="true" />
                                        <%--<asp:RequiredFieldValidator ID="Retxtformname" runat="server"
                                            ControlToValidate="txt_form_name" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกชื่อฟอร์มกรอกผล" ValidationGroup="Save" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCallouttxtformname" runat="Server"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="Retxtformname" Width="220" />--%>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="form-group">
                                    <asp:Label ID="lbl_signature_name" runat="server" Text="ชื่อเรียก" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtsignature_name" runat="server" CssClass="form-control" placeholder="กรุณากรอกชื่อเรียก"></asp:TextBox>
                                        <%--       <asp:RegularExpressionValidator ID="RegularExpressionValidator_require" runat="server" ValidationGroup="Saveinsert" Display="Dynamic" ErrorMessage="*คุณกรอกข้อมูลเกิน 1000 ตัวอักษร"
                                            Font-Size="8" ForeColor="Red" ControlToValidate="require" ValidationExpression="^[\s\S]{0,1000}$" SetFocusOnError="true" />
                                        <asp:RequiredFieldValidator ID="requiredreq" ValidationGroup="Saveinsert" runat="server" SetFocusOnError="true"
                                            ControlToValidate="require" Font-Size="8" ForeColor="Red" Display="Dynamic" ErrorMessage="*กรุณากรอกข้อมูลให้ครบ" />--%>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="lbl_position_signature" runat="server" Text="ตำแหน่ง" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtposition_signature" runat="server" CssClass="form-control" placeholder="กรุณากรอกตำแหน่ง"></asp:TextBox>
                                        <%--       <asp:RegularExpressionValidator ID="RegularExpressionValidator_require" runat="server" ValidationGroup="Saveinsert" Display="Dynamic" ErrorMessage="*คุณกรอกข้อมูลเกิน 1000 ตัวอักษร"
                                            Font-Size="8" ForeColor="Red" ControlToValidate="require" ValidationExpression="^[\s\S]{0,1000}$" SetFocusOnError="true" />
                                        <asp:RequiredFieldValidator ID="requiredreq" ValidationGroup="Saveinsert" runat="server" SetFocusOnError="true"
                                            ControlToValidate="require" Font-Size="8" ForeColor="Red" Display="Dynamic" ErrorMessage="*กรุณากรอกข้อมูลให้ครบ" />--%>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="lbldatetime_signature" runat="server" Text="วันที่" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtdatetime_signature" runat="server" CssClass="form-control" placeholder="กรุณากรอกวันที่"></asp:TextBox>
                                        <%--       <asp:RegularExpressionValidator ID="RegularExpressionValidator_require" runat="server" ValidationGroup="Saveinsert" Display="Dynamic" ErrorMessage="*คุณกรอกข้อมูลเกิน 1000 ตัวอักษร"
                                            Font-Size="8" ForeColor="Red" ControlToValidate="require" ValidationExpression="^[\s\S]{0,1000}$" SetFocusOnError="true" />
                                        <asp:RequiredFieldValidator ID="requiredreq" ValidationGroup="Saveinsert" runat="server" SetFocusOnError="true"
                                            ControlToValidate="require" Font-Size="8" ForeColor="Red" Display="Dynamic" ErrorMessage="*กรุณากรอกข้อมูลให้ครบ" />--%>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="lblselect_detail" runat="server" Text="ตำแหน่งแสดงที่" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:TextBox ID="txtselect_detail" runat="server" CssClass="form-control" placeholder="กรุณากรอกตำแหน่งแสดงที่"></asp:TextBox>
                                        <%--       <asp:RegularExpressionValidator ID="RegularExpressionValidator_require" runat="server" ValidationGroup="Saveinsert" Display="Dynamic" ErrorMessage="*คุณกรอกข้อมูลเกิน 1000 ตัวอักษร"
                                            Font-Size="8" ForeColor="Red" ControlToValidate="require" ValidationExpression="^[\s\S]{0,1000}$" SetFocusOnError="true" />
                                        <asp:RequiredFieldValidator ID="requiredreq" ValidationGroup="Saveinsert" runat="server" SetFocusOnError="true"
                                            ControlToValidate="require" Font-Size="8" ForeColor="Red" Display="Dynamic" ErrorMessage="*กรุณากรอกข้อมูลให้ครบ" />--%>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>

                                <div class="form-group">
                                    <asp:Label ID="lbl_formcal_status" runat="server" Text="สถานะ" CssClass="col-sm-3 control-label"></asp:Label>
                                    <div class="col-sm-6">
                                        <asp:DropDownList ID="ddlformcal_status" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Value="1">Online</asp:ListItem>
                                            <asp:ListItem Value="0">Offline</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-3"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-2 col-sm-offset-3">
                                        <asp:LinkButton ID="btnSaveSignatureName" ValidationGroup="Save" CssClass="btn btn-success" Text="Save" data-toggle="tooltip" title="บันทึก" runat="server" CommandName="cmdSave" OnCommand="btnCommand"></asp:LinkButton>
                                        <asp:LinkButton ID="btnCancel" CssClass="btn btn-danger" runat="server" Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" data-toggle="tooltip" title="ยกเลิก"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>


        </asp:View>

    </asp:MultiView>

</asp:Content>

