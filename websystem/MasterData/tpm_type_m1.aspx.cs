﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_MasterData_tpm_type_m1 : System.Web.UI.Page
{

    #region Connect

    function_tool _funcTool = new function_tool();
    data_tpm_form _dtpmform = new data_tpm_form();
    data_employee _dtEmployee = new data_employee();

    private string BoxXML;
    string localXml = String.Empty;
    string localString = String.Empty;
    string _local_xml = "";
    string _localJson = "";

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];

    static string _urlInsertMaster = _serviceUrl + ConfigurationManager.AppSettings["urlInsertMaster_FormResult"];
    static string _urlSelectMaster = _serviceUrl + ConfigurationManager.AppSettings["urlSelectMaster_FormResult"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];


    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ViewState["EmpIDX"] = int.Parse(Session["emp_idx"].ToString());
            select_empIdx_present();
            lblheadbanner.Text = "แบบฟอร์มประเมินประจำปี";
            ViewState["m0_type_idx"] = "0";
            ViewState["check_position"] = "0";
            ViewState["m2_typeidx_insert"] = null;
            ViewState["check_position_insert"] = "0";
            ViewState["check_m2idx_insert"] = "0";
            SetDefaultpage(1);
        }

    }
    #endregion

    #region Select
    protected void select_empIdx_present()
    {

        _dtEmployee = callServiceEmpProfile(_urlGetMyProfile + ViewState["EmpIDX"].ToString());

        ViewState["rdept_name"] = _dtEmployee.employee_list[0].dept_name_th;
        ViewState["rdept_idx"] = _dtEmployee.employee_list[0].rdept_idx;
        ViewState["FullName"] = _dtEmployee.employee_list[0].emp_name_th;
        ViewState["Org_name"] = _dtEmployee.employee_list[0].org_name_th;
        ViewState["Org_idx"] = _dtEmployee.employee_list[0].org_idx;

        ViewState["EmpCode"] = _dtEmployee.employee_list[0].emp_code;
        ViewState["Positname"] = _dtEmployee.employee_list[0].pos_name_th;
        ViewState["Pos_idx"] = _dtEmployee.employee_list[0].rpos_idx;
        ViewState["Email"] = _dtEmployee.employee_list[0].emp_email;
        ViewState["Tel"] = _dtEmployee.employee_list[0].emp_mobile_no;
        ViewState["Secname"] = _dtEmployee.employee_list[0].sec_name_th;
        ViewState["Sec_idx"] = _dtEmployee.employee_list[0].rsec_idx;
        ViewState["CostIDX"] = _dtEmployee.employee_list[0].costcenter_idx;

    }

    protected void select_mastertopic(GridView gvName)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 1;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        setGridData(gvName, _dtpmform.Boxtpmm0_DocFormDetail);

    }

    protected void select_m1type(DropDownList ddlName)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 2;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        setDdlData(ddlName, _dtpmform.Boxtpmm0_DocFormDetail, "m1_type_name", "m1_typeidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกหัวข้อหลัก...", "0"));

    }

    protected void select_m1type_forsubtopic(DropDownList ddlName)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 18;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        setDdlData(ddlName, _dtpmform.Boxtpmm0_DocFormDetail, "m1_type_name", "m1_typeidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกหัวข้อหลัก...", "0"));

    }

    protected void select_mastersubtopic(GridView gvName)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 3;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        setGridData(gvName, _dtpmform.Boxtpmm0_DocFormDetail);

    }

    protected void select_m2type(CheckBoxList chkName, int m1typeidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 4;
        dataselect.m1_typeidx = m1typeidx;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        setChkData(chkName, _dtpmform.Boxtpmm0_DocFormDetail, "m2_type_name", "m2_typeidx");

    }

    protected void getOrganizationList(DropDownList ddlName)
    {

        _dtEmployee.organization_list = new organization_details[1];
        organization_details _orgList = new organization_details();
        _orgList.org_idx = 0;
        _dtEmployee.organization_list[0] = _orgList;

        _dtEmployee = callServicePostEmp(_urlGetOrganizationList, _dtEmployee);
        setDdlData(ddlName, _dtEmployee.organization_list, "org_name_th", "org_idx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกองค์กร...", "0"));
    }

    protected void select_positiongroup(CheckBoxList chkName)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 5;
        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        setChkData(chkName, _dtpmform.Boxtpmm0_DocFormDetail, "pos_name", "posidx");
        //setDdlData(ddlName, _dtpmform.Boxtpmm0_DocFormDetail, "pos_name", "posidx");
        //ddlName.Items.Insert(0, new ListItem("กรุณาเลือกตำแหน่ง...", "0"));

    }

    protected void select_masterformresult(GridView gvName)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 6;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        setGridData(gvName, _dtpmform.Boxtpmm0_DocFormDetail);

    }

    protected void select_masterformdetail(GridView gvName, int u0_typeidx_detail)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 7;
        dataselect.u0_typeidx = u0_typeidx_detail;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        setGridData(gvName, _dtpmform.Boxtpmm0_DocFormDetail);
    }

    protected void select_m2typidx_check(DropDownList ddlName, int m1typidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 4;
        dataselect.m1_typeidx = m1typidx;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
        //  txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);
        setDdlData(ddlName, _dtpmform.Boxtpmm0_DocFormDetail, "m2_type_name", "m2_typeidx");
        ddlName.Items.Insert(0, new ListItem("กรุณาเลือกหัวข้อย่อย...", "0"));

    }

    protected void select_positiongroupwhere_orgidx(int orgidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 14;
        dataselect.org_idx = orgidx;
        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);

        if (_dtpmform.Boxtpmm0_DocFormDetail[0].posidx_comma != null && _dtpmform.Boxtpmm0_DocFormDetail[0].posidx_comma.ToString() != "")
        {
            ViewState["check_position"] = _dtpmform.Boxtpmm0_DocFormDetail[0].posidx_comma.ToString();
        }

    }

    #endregion

    #region Insert && Update
    protected void Insert_Topic()
    {
        int i = 0;
        _dtpmform = new data_tpm_form();


        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail datainsert = new tpmu0_DocFormDetail();

        datainsert.condition = 1;
        datainsert.cemp_idx = int.Parse(ViewState["EmpIDX"].ToString());

        _dtpmform.Boxtpmu0_DocFormDetail[0] = datainsert;

        var ds_master_insert = (DataSet)ViewState["vsAddListTable_Topic"];
        var _mastertopic = new tpmm0_DocFormDetail[ds_master_insert.Tables[0].Rows.Count];


        foreach (DataRow dtrow in ds_master_insert.Tables[0].Rows)
        {
            _mastertopic[i] = new tpmm0_DocFormDetail();

            _mastertopic[i].m0_typeidx = int.Parse(dtrow["m0_typeidx"].ToString());
            _mastertopic[i].m1_type_name = dtrow["m1_type_name"].ToString();
            _mastertopic[i].status = int.Parse(dtrow["status"].ToString());


            i++;
            _dtpmform.Boxtpmm0_DocFormDetail = _mastertopic;
        }

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));
        _dtpmform = callServicePostTPMForm(_urlInsertMaster, _dtpmform);

    }

    protected void Update_Topic(int condition, int cempidx, int status, int m1typeidx, string m1_typename)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = condition;
        dataselect.cemp_idx = cempidx;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;

        _dtpmform.Boxtpmm0_DocFormDetail = new tpmm0_DocFormDetail[1];
        tpmm0_DocFormDetail dataupdate = new tpmm0_DocFormDetail();

        dataupdate.m1_typeidx = m1typeidx;
        dataupdate.m1_type_name = m1_typename;
        dataupdate.status = status;

        _dtpmform.Boxtpmm0_DocFormDetail[0] = dataupdate;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlInsertMaster, _dtpmform);
    }

    protected void Insert_SubTopic()
    {
        int i = 0;
        _dtpmform = new data_tpm_form();


        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail datainsert = new tpmu0_DocFormDetail();

        datainsert.condition = 3;
        datainsert.cemp_idx = int.Parse(ViewState["EmpIDX"].ToString());

        _dtpmform.Boxtpmu0_DocFormDetail[0] = datainsert;

        var ds_master_insert = (DataSet)ViewState["vsAddListTable_SubTopic"];
        var _mastertopic = new tpmm0_DocFormDetail[ds_master_insert.Tables[0].Rows.Count];


        foreach (DataRow dtrow in ds_master_insert.Tables[0].Rows)
        {
            _mastertopic[i] = new tpmm0_DocFormDetail();

            _mastertopic[i].m1_typeidx = int.Parse(dtrow["m1_typeidx"].ToString());
            _mastertopic[i].m2_type_name = dtrow["m2_type_name"].ToString();
            _mastertopic[i].status = int.Parse(dtrow["status_sub"].ToString());

            i++;
            _dtpmform.Boxtpmm0_DocFormDetail = _mastertopic;
        }

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));
        _dtpmform = callServicePostTPMForm(_urlInsertMaster, _dtpmform);

    }

    protected void Update_SubTopic(int condition, int cempidx, int status, int m1typeidx, int m2typeidx, string m2_typename)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = condition;
        dataselect.cemp_idx = cempidx;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;

        _dtpmform.Boxtpmm0_DocFormDetail = new tpmm0_DocFormDetail[1];
        tpmm0_DocFormDetail dataupdate = new tpmm0_DocFormDetail();

        dataupdate.m1_typeidx = m1typeidx;
        dataupdate.m2_typeidx = m2typeidx;
        dataupdate.m2_type_name = m2_typename;
        dataupdate.status = status;

        _dtpmform.Boxtpmm0_DocFormDetail[0] = dataupdate;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlInsertMaster, _dtpmform);
    }

    protected void Insert_FormResult()
    {
        int i = 0;
        _dtpmform = new data_tpm_form();

        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail datainsert = new tpmu0_DocFormDetail();

        datainsert.condition = 5;
        datainsert.cemp_idx = int.Parse(ViewState["EmpIDX"].ToString());

        _dtpmform.Boxtpmu0_DocFormDetail[0] = datainsert;

        var ds_master_insert = (DataSet)ViewState["vsAddListTable_FormResult"];
        var _mastertopic = new tpmm0_DocFormDetail[ds_master_insert.Tables[0].Rows.Count];


        foreach (DataRow dtrow in ds_master_insert.Tables[0].Rows)
        {
            _mastertopic[i] = new tpmm0_DocFormDetail();

            _mastertopic[i].orgidx = int.Parse(dtrow["orgidx"].ToString());


            string posidx_comma = dtrow["posidx_comma"].ToString().Replace("<br>", "");

            _mastertopic[i].posidx_comma = posidx_comma;
            _mastertopic[i].form_name = dtrow["form_name"].ToString();

            //_mastertopic[i].posidx = int.Parse(dtrow["posidx"].ToString());

            _mastertopic[i].m1_typeidx = int.Parse(dtrow["m1_typeidx"].ToString());
            //_mastertopic[i].m2_typeidx_comma = dtrow["m2_typeidx_comma"].ToString();

            _mastertopic[i].status = int.Parse(dtrow["status"].ToString());

            string m2typidx = dtrow["m2_typeidx_comma"].ToString().Replace("<br>", "");
            _mastertopic[i].m2_typeidx_comma = m2typidx;

            i++;
            _dtpmform.Boxtpmm0_DocFormDetail = _mastertopic;
        }

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));
        _dtpmform = callServicePostTPMForm(_urlInsertMaster, _dtpmform);


    }

    protected void Update_FormResult(int u0idx, string form_name, int status, int cempidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 9;
        dataselect.cemp_idx = cempidx;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;

        _dtpmform.Boxtpmm0_DocFormDetail = new tpmm0_DocFormDetail[1];
        tpmm0_DocFormDetail dataupdate = new tpmm0_DocFormDetail();

        dataupdate.form_name = form_name;
        dataupdate.u0_typeidx = u0idx;
        dataupdate.status = status;

        _dtpmform.Boxtpmm0_DocFormDetail[0] = dataupdate;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlInsertMaster, _dtpmform);

        if (_dtpmform.ReturnCode.ToString() == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกการแก้ไขเสร็จสมบูรณ์');", true);

        }

    }

    protected void DeleteForm(int cempidx, int status, int u0_typeidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.cemp_idx = cempidx;
        dataselect.condition = 6;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;

        _dtpmform.Boxtpmm0_DocFormDetail = new tpmm0_DocFormDetail[1];
        tpmm0_DocFormDetail dataupdate = new tpmm0_DocFormDetail();

        dataupdate.u0_typeidx = u0_typeidx;
        dataupdate.status = status;

        _dtpmform.Boxtpmm0_DocFormDetail[0] = dataupdate;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlInsertMaster, _dtpmform);
    }

    protected void DeleteFormDetail(int cempidx, int status, int r0_typeidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.cemp_idx = cempidx;
        dataselect.condition = 7;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;

        _dtpmform.Boxtpmm0_DocFormDetail = new tpmm0_DocFormDetail[1];
        tpmm0_DocFormDetail dataupdate = new tpmm0_DocFormDetail();

        dataupdate.r0_typeidx = r0_typeidx;
        dataupdate.status = status;

        _dtpmform.Boxtpmm0_DocFormDetail[0] = dataupdate;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlInsertMaster, _dtpmform);
    }

    protected void Update_FormDetail(int r0idx, int u0idx, int m1idx, int m2idx, int status, int cempidx)
    {
        _dtpmform = new data_tpm_form();
        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

        dataselect.condition = 8;
        dataselect.cemp_idx = cempidx;

        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;

        _dtpmform.Boxtpmm0_DocFormDetail = new tpmm0_DocFormDetail[1];
        tpmm0_DocFormDetail dataupdate = new tpmm0_DocFormDetail();

        dataupdate.m1_typeidx = m1idx;
        dataupdate.m2_typeidx = m2idx;
        dataupdate.u0_typeidx = u0idx;
        dataupdate.r0_typeidx = r0idx;
        dataupdate.status = status;

        _dtpmform.Boxtpmm0_DocFormDetail[0] = dataupdate;

        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

        _dtpmform = callServicePostTPMForm(_urlInsertMaster, _dtpmform);

        ViewState["rtcode_updateform"] = _dtpmform.ReturnCode.ToString();

        if (_dtpmform.ReturnCode.ToString() == "1")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกการแก้ไขเสร็จสมบูรณ์');", true);

        }

    }

    #endregion

    #region CallService

    protected data_employee callServiceEmpProfile(string _cmdUrl)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        //text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServiceGet(_cmdUrl);
        // text.Text = _localJson;

        ////// convert json to object
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dtEmployee;
    }
    protected data_employee callServicePostEmp(string _cmdUrl, data_employee _dtEmployee)
    {
        _localJson = _funcTool.convertObjectToJson(_dtEmployee);
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        _dtEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dtEmployee;
    }
    protected data_tpm_form callServicePostTPMForm(string _cmdUrl, data_tpm_form _dtpmform)
    {
        //// convert to json
        _localJson = _funcTool.convertObjectToJson(_dtpmform);
        // text.Text =  _cmdUrl + _localJson;

        //// call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        // text.Text = _localJson;

        ////// convert json to object
        _dtpmform = (data_tpm_form)_funcTool.convertJsonToObject(typeof(data_tpm_form), _localJson);


        return _dtpmform;
    }
    #endregion

    #region reuse
    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setChkData(CheckBoxList chkName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        chkName.Items.Clear();
        // bind items
        chkName.DataSource = obj;
        chkName.DataTextField = _data_text;
        chkName.DataValueField = _data_value;
        chkName.DataBind();
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormViewData(FormView fvName, Object obj)
    {
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected string getStatus(int status)
    {
        if (status == 1)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Online'><i class='fa fa-check-circle'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='Offline'><i class='fa fa-times-circle'></i></span>";
        }
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger1 = new PostBackTrigger();
        trigger1.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger1);
    }
    #endregion

    #region FormView

    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        var FvName = (FormView)sender;

        switch (FvName.ID)
        {

            case "FvDetailUser":
                FormView FvDetailUser = (FormView)ViewInsert.FindControl("FvDetailUser");

                if (FvDetailUser.CurrentMode == FormViewMode.Insert)
                {
                    var txtempcode = ((TextBox)FvDetailUser.FindControl("txtempcode"));
                    var txtrequesname = ((TextBox)FvDetailUser.FindControl("txtrequesname"));
                    var txtrequesdept = ((TextBox)FvDetailUser.FindControl("txtrequesdept"));
                    var txtsec = ((TextBox)FvDetailUser.FindControl("txtsec"));
                    var txtpos = ((TextBox)FvDetailUser.FindControl("txtpos"));
                    var txtemail = ((TextBox)FvDetailUser.FindControl("txtemail"));
                    var txttel = ((TextBox)FvDetailUser.FindControl("txttel"));
                    var txtorg = ((TextBox)FvDetailUser.FindControl("txtorg"));

                    txtempcode.Text = ViewState["EmpCode"].ToString();
                    txtrequesname.Text = ViewState["FullName"].ToString();
                    txtorg.Text = ViewState["Org_name"].ToString();
                    txtrequesdept.Text = ViewState["rdept_name"].ToString();
                    txtsec.Text = ViewState["Secname"].ToString();
                    txtpos.Text = ViewState["Positname"].ToString();
                    txttel.Text = ViewState["Tel"].ToString();
                    txtemail.Text = ViewState["Email"].ToString();
                }
                break;

            case "fvadd_topic":
                FormView fvadd_topic = (FormView)ViewInsert.FindControl("fvadd_topic");

                if (fvadd_topic.CurrentMode == FormViewMode.Insert)
                {
                    var div_save = ((Control)fvadd_topic.FindControl("div_save"));

                    div_save.Visible = false;

                }
                break;
            case "fvadd_subtopic":
                FormView fvadd_subtopic = (FormView)ViewInsert.FindControl("fvadd_subtopic");

                if (fvadd_subtopic.CurrentMode == FormViewMode.Insert)
                {
                    var ddltopic = ((DropDownList)fvadd_subtopic.FindControl("ddltopic"));
                    var div_save_sub = ((Control)fvadd_subtopic.FindControl("div_save_sub"));

                    select_m1type_forsubtopic(ddltopic);
                    div_save_sub.Visible = false;

                }
                break;

            case "fvadd_formresult":
                FormView fvadd_formresult = (FormView)ViewInsert.FindControl("fvadd_formresult");

                if (fvadd_formresult.CurrentMode == FormViewMode.Insert)
                {
                    var ddlorgidx = ((DropDownList)fvadd_formresult.FindControl("ddlorgidx"));
                    var chkposidx = ((CheckBoxList)fvadd_formresult.FindControl("chkposidx"));
                    var ddltopic_form = ((DropDownList)fvadd_formresult.FindControl("ddltopic_form"));
                    var div_save_form = ((Control)fvadd_formresult.FindControl("div_save_form"));


                    select_m1type(ddltopic_form);
                    getOrganizationList(ddlorgidx);
                    select_positiongroup(chkposidx);
                    div_save_form.Visible = false;
                }
                break;



        }
    }
    #endregion

    #region Gridview

    #region RowDatabound
    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvForm_Result":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    if (GvForm_Result.EditIndex != e.Row.RowIndex)
                    {

                        Label lblstatusidx = (Label)e.Row.FindControl("lblstatusidx");
                        LinkButton btndelete = (LinkButton)e.Row.FindControl("btndelete");
                        LinkButton btnadd = (LinkButton)e.Row.FindControl("btnadd");

                        if (int.Parse(lblstatusidx.Text) == 1)
                        {
                            btndelete.Visible = false;
                            btnadd.Visible = false;
                        }
                        else
                        {
                            btndelete.Visible = true;
                            btnadd.Visible = true;
                        }

                    }
                    if (GvForm_Result.EditIndex == e.Row.RowIndex)
                    {
                        var txtorgidx = (TextBox)e.Row.FindControl("txtorgidx");
                        var ddlorgidx_edit = (DropDownList)e.Row.FindControl("ddlorgidx_edit");
                        var txtposidx_comma = (TextBox)e.Row.FindControl("txtposidx_comma");
                        var chkposidx_edit = (CheckBoxList)e.Row.FindControl("chkposidx_edit");


                        getOrganizationList(ddlorgidx_edit);
                        ddlorgidx_edit.SelectedValue = txtorgidx.Text;

                        select_positiongroup(chkposidx_edit);


                        string[] ToId = txtposidx_comma.Text.Split(',');

                        foreach (ListItem li in chkposidx_edit.Items)
                        {
                            foreach (string To_check in ToId)
                            {

                                if (li.Value == To_check)
                                {
                                    li.Selected = true;

                                    break;
                                }
                                else
                                {
                                    li.Selected = false;
                                }
                            }
                        }
                    }


                }
                break;
            case "GvTopic":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }


                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvTopic.EditIndex != e.Row.RowIndex)
                    {
                        var lblm1_typeidx = (Label)e.Row.FindControl("lblm1_typeidx");
                        var Delete = (LinkButton)e.Row.FindControl("Delete");

                        _dtpmform = new data_tpm_form();
                        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
                        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

                        dataselect.condition = 15;
                        dataselect.m1_typeidx = int.Parse(lblm1_typeidx.Text);

                        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
                        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

                        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);

                        string m1_typeidx = _dtpmform.ReturnCode.ToString();

                        if (m1_typeidx != "0")
                        {
                            Delete.Visible = false;
                        }
                        else
                        {
                            Delete.Visible = true;
                        }


                    }
                }
                break;

            case "GvSubTopic":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    if (GvSubTopic.EditIndex != e.Row.RowIndex)
                    {
                        var lblm2_typeidx = (Label)e.Row.FindControl("lblm2_typeidx");
                        var Delete = (LinkButton)e.Row.FindControl("Delete");

                        _dtpmform = new data_tpm_form();
                        _dtpmform.Boxtpmu0_DocFormDetail = new tpmu0_DocFormDetail[1];
                        tpmu0_DocFormDetail dataselect = new tpmu0_DocFormDetail();

                        dataselect.condition = 16;
                        dataselect.m2_typeidx = int.Parse(lblm2_typeidx.Text);

                        _dtpmform.Boxtpmu0_DocFormDetail[0] = dataselect;
                        //txt.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_dtpmform));

                        _dtpmform = callServicePostTPMForm(_urlSelectMaster, _dtpmform);

                        string m2_typeidx = _dtpmform.ReturnCode.ToString();

                        if (m2_typeidx != "0")
                        {
                            Delete.Visible = false;
                        }
                        else
                        {
                            Delete.Visible = true;
                        }


                    }

                    else if (GvSubTopic.EditIndex == e.Row.RowIndex)
                    {
                        var txtm1_typeidx = (TextBox)e.Row.FindControl("txtm1_typeidx");
                        var ddltopic_edit = (DropDownList)e.Row.FindControl("ddltopic_edit");

                        select_m1type_forsubtopic(ddltopic_edit);
                        ddltopic_edit.SelectedValue = txtm1_typeidx.Text;
                    }

                }
                break;

            case "GvForm_Detail":
                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (GvForm_Detail.EditIndex != e.Row.RowIndex)
                    {
                        if (ViewState["status_form"].ToString() == "1")
                        {
                            GvForm_Detail.Columns[3].Visible = false;
                        }
                        else
                        {
                            GvForm_Detail.Columns[3].Visible = true;
                        }
                    }

                    if (GvForm_Detail.EditIndex == e.Row.RowIndex)
                    {
                        var txtm1_typeidx_edit = (TextBox)e.Row.FindControl("txtm1_typeidx_edit");
                        var ddltopic_edit = (DropDownList)e.Row.FindControl("ddltopic_edit");
                        var txtm2_typeidx_edit = (TextBox)e.Row.FindControl("txtm2_typeidx_edit");
                        var ddlsubtopic_edit = (DropDownList)e.Row.FindControl("ddlsubtopic_edit");

                        select_m1type(ddltopic_edit);
                        ddltopic_edit.SelectedValue = txtm1_typeidx_edit.Text;

                        select_m2typidx_check(ddlsubtopic_edit, int.Parse(txtm1_typeidx_edit.Text));
                        ddlsubtopic_edit.SelectedValue = txtm2_typeidx_edit.Text;

                    }

                }
                break;

        }

    }

    #endregion

    #region Paging

    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvTopic":

                GvTopic.PageIndex = e.NewPageIndex;
                SetLiActive(1);
                //select_mastertopic(GvTopic);
                //_divMenuLiToViewFormResult.Attributes.Remove("class");
                //_divMenuLiToViewTopic.Attributes.Add("class", "active");
                //_divMenuLiToViewSubTopic.Attributes.Remove("class");

                break;


            case "GvSubTopic":
                GvSubTopic.PageIndex = e.NewPageIndex;
                SetLiActive(2);
                //select_mastersubtopic(GvSubTopic);
                //mergeCell(GvSubTopic);

                //_divMenuLiToViewFormResult.Attributes.Remove("class");
                //_divMenuLiToViewTopic.Attributes.Remove("class");
                //_divMenuLiToViewSubTopic.Attributes.Add("class", "active");
                break;


        }
    }

    #endregion

    #region GvRowEditing

    protected void Master_RowEditing(object sender, GridViewEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvForm_Result":

                GvForm_Result.EditIndex = e.NewEditIndex;
                select_masterformresult(GvForm_Result);
                break;

            case "GvTopic":

                GvTopic.EditIndex = e.NewEditIndex;
                ////select_mastertopic(GvTopic);
                /*select_mastertopic*/
                SetLiActive(1);


                break;

            case "GvSubTopic":

                GvSubTopic.EditIndex = e.NewEditIndex;
                select_mastersubtopic(GvSubTopic);
                _divMenuLiToViewFormResult.Attributes.Remove("class");
                _divMenuLiToViewTopic.Attributes.Remove("class");
                _divMenuLiToViewSubTopic.Attributes.Add("class", "active");

                break;

            case "GvForm_Detail":
                GvForm_Detail.EditIndex = e.NewEditIndex;
                select_masterformdetail(GvForm_Detail, int.Parse(ViewState["u0_typeidx"].ToString()));

                break;
        }
    }

    #endregion

    #region GvRowCancelingEdit

    protected void Master_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvForm_Result":
                GvForm_Result.EditIndex = -1;
                select_masterformresult(GvForm_Result);
                mergeCell(GvForm_Result);

                break;
            case "GvTopic":
                GvTopic.EditIndex = -1;
                SetLiActive(1);
                //select_mastertopic(GvTopic);
                break;

            case "GvSubTopic":
                GvSubTopic.EditIndex = -1;
                SetLiActive(2);
                //select_mastersubtopic(GvSubTopic);
                //mergeCell(GvSubTopic);

                break;

            case "GvForm_Detail":
                GvForm_Detail.EditIndex = -1;
                select_masterformdetail(GvForm_Detail, int.Parse(ViewState["u0_typeidx"].ToString()));
                mergeCell(GvForm_Detail);

                break;
        }
    }

    #endregion

    #region GvRowUpdating

    protected void Master_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "GvTopic":

                int m1_typeidx = Convert.ToInt32(GvTopic.DataKeys[e.RowIndex].Values[0].ToString());
                var txtname_edit = (TextBox)GvTopic.Rows[e.RowIndex].FindControl("txtname_edit");
                var StatusUpdate = (DropDownList)GvTopic.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvTopic.EditIndex = -1;

                Update_Topic(2, int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(StatusUpdate.SelectedValue), m1_typeidx, txtname_edit.Text);
                //select_mastertopic(GvTopic);
                SetLiActive(1);

                break;

            case "GvSubTopic":
                int m2_typeidx = Convert.ToInt32(GvSubTopic.DataKeys[e.RowIndex].Values[0].ToString());
                var txtname_edit_sub = (TextBox)GvSubTopic.Rows[e.RowIndex].FindControl("txtname_edit");
                var ddltopic_edit = (DropDownList)GvSubTopic.Rows[e.RowIndex].FindControl("ddltopic_edit");
                var StatusUpdate_sub = (DropDownList)GvSubTopic.Rows[e.RowIndex].FindControl("ddStatusUpdate");

                GvSubTopic.EditIndex = -1;

                Update_SubTopic(4, int.Parse(ViewState["EmpIDX"].ToString()), int.Parse(StatusUpdate_sub.SelectedValue), int.Parse(ddltopic_edit.SelectedValue), m2_typeidx, txtname_edit_sub.Text);
                SetLiActive(2);
                //select_mastersubtopic(GvSubTopic);
                //mergeCell(GvSubTopic);
                break;

            case "GvForm_Detail":
                int r0_typeidx = Convert.ToInt32(GvForm_Detail.DataKeys[e.RowIndex].Values[0].ToString());
                var ddltopic_edit_r0 = (DropDownList)GvForm_Detail.Rows[e.RowIndex].FindControl("ddltopic_edit");
                var ddlsubtopic_edit = (DropDownList)GvForm_Detail.Rows[e.RowIndex].FindControl("ddlsubtopic_edit");
                var StatusUpdate_r0 = (DropDownList)GvForm_Detail.Rows[e.RowIndex].FindControl("ddStatusUpdate");


                Update_FormDetail(r0_typeidx, int.Parse(ViewState["u0_typeidx"].ToString()), int.Parse(ddltopic_edit_r0.SelectedValue), int.Parse(ddlsubtopic_edit.SelectedValue),
                    int.Parse(StatusUpdate_r0.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()));


                if (ViewState["rtcode_updateform"].ToString() == "0")
                {
                    GvForm_Detail.EditIndex = -1;
                    select_masterformdetail(GvForm_Detail, int.Parse(ViewState["u0_typeidx"].ToString()));
                    mergeCell(GvForm_Detail);
                }


                break;

            case "GvForm_Result":
                int u0_typeidx = Convert.ToInt32(GvForm_Result.DataKeys[e.RowIndex].Values[0].ToString());
                var txtnform_name = (TextBox)GvForm_Result.Rows[e.RowIndex].FindControl("txtnform_name");

                var ddStatusUpdate = (DropDownList)GvForm_Result.Rows[e.RowIndex].FindControl("ddStatusUpdate");
                GvForm_Result.EditIndex = -1;
                Update_FormResult(u0_typeidx, txtnform_name.Text, int.Parse(ddStatusUpdate.SelectedValue), int.Parse(ViewState["EmpIDX"].ToString()));
                ViewState["check_position"] = "0";

                select_masterformresult(GvForm_Result);
                mergeCell(GvForm_Result);
                break;
        }
    }

    #endregion

    #region onRowCommand

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "btndeletetopic":
                    GridView GvTopicAdd = (GridView)fvadd_topic.FindControl("GvTopicAdd");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    Control div_save = (Control)fvadd_topic.FindControl("div_save");

                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsAddListTable_Topic"];
                    dsContacts.Tables["dsAddListTable_Topic"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(GvTopicAdd, dsContacts.Tables["dsAddListTable_Topic"]);

                    if (dsContacts.Tables["dsAddListTable_Topic"].Rows.Count < 1)
                    {
                        GvTopicAdd.Visible = false;
                        div_save.Visible = false;

                    }

                    break;

                case "btndeletesubtopic":
                    GridView GvSubTopicAdd = (GridView)fvadd_subtopic.FindControl("GvSubTopicAdd");
                    GridViewRow rowSelect_sub = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    Control div_save_sub = (Control)fvadd_subtopic.FindControl("div_save_sub");

                    int rowIndex_sub = rowSelect_sub.RowIndex;
                    DataSet dsContacts_sub = (DataSet)ViewState["vsAddListTable_SubTopic"];
                    dsContacts_sub.Tables["dsAddListTable_SubTopic"].Rows[rowIndex_sub].Delete();
                    dsContacts_sub.AcceptChanges();
                    setGridData(GvSubTopicAdd, dsContacts_sub.Tables["dsAddListTable_SubTopic"]);

                    if (dsContacts_sub.Tables["dsAddListTable_SubTopic"].Rows.Count < 1)
                    {
                        GvSubTopicAdd.Visible = false;
                        div_save_sub.Visible = false;

                    }
                    break;

                case "btndeleteformresult":
                    GridView GvFormResultAdd = (GridView)fvadd_formresult.FindControl("GvFormResultAdd");
                    GridViewRow rowSelect_form = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                    Control div_save_form = (Control)fvadd_formresult.FindControl("div_save_form");

                    int rowIndex_form = rowSelect_form.RowIndex;
                    DataSet dsContacts_form = (DataSet)ViewState["vsAddListTable_FormResult"];
                    dsContacts_form.Tables["dsAddListTable_FormResult"].Rows[rowIndex_form].Delete();
                    dsContacts_form.AcceptChanges();
                    setGridData(GvFormResultAdd, dsContacts_form.Tables["dsAddListTable_FormResult"]);

                    if (dsContacts_form.Tables["dsAddListTable_FormResult"].Rows.Count < 1)
                    {
                        GvFormResultAdd.Visible = false;
                        div_save_form.Visible = false;

                    }

                    mergeCell(GvFormResultAdd);
                    break;

            }
        }
    }
    #endregion

    #region mergeCell
    protected void mergeCell(GridView GvName)
    {
        switch (GvName.ID)
        {
            case "GvSubTopicAdd":

                GridView GvSubTopicAdd = (GridView)fvadd_subtopic.FindControl("GvSubTopicAdd");

                for (int rowIndex = GvSubTopicAdd.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvSubTopicAdd.Rows[rowIndex];
                    GridViewRow previousRow = GvSubTopicAdd.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[0].FindControl("lbm1_typeidx")).Text == ((Label)previousRow.Cells[0].FindControl("lbm1_typeidx")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }

                }
                break;

            case "GvSubTopic":
                GridView GvSubTopic = (GridView)ViewIndex.FindControl("GvSubTopic");

                for (int rowIndex = GvSubTopic.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvSubTopic.Rows[rowIndex];
                    GridViewRow previousRow = GvSubTopic.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[0].FindControl("lblm1_type_name")).Text == ((Label)previousRow.Cells[0].FindControl("lblm1_type_name")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }

                }
                break;

            case "GvFormResultAdd":
                GridView GvFormResultAdd = (GridView)fvadd_formresult.FindControl("GvFormResultAdd");

                for (int rowIndex = GvFormResultAdd.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvFormResultAdd.Rows[rowIndex];
                    GridViewRow previousRow = GvFormResultAdd.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[0].FindControl("lblorgidx")).Text == ((Label)previousRow.Cells[0].FindControl("lblorgidx")).Text &&
                       ((Label)currentRow.Cells[0].FindControl("lblposidx")).Text == ((Label)previousRow.Cells[0].FindControl("lblposidx")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }

                }
                break;

            case "GvForm_Result":
                GridView GvForm_Result = (GridView)ViewIndex.FindControl("GvForm_Result");

                for (int rowIndex = GvForm_Result.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvForm_Result.Rows[rowIndex];
                    GridViewRow previousRow = GvForm_Result.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[0].FindControl("lblOrgNameTH")).Text == ((Label)previousRow.Cells[0].FindControl("lblOrgNameTH")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }

                }
                break;

            case "GvForm_Detail":
                GridView GvForm_Detail = (GridView)ViewIndex.FindControl("GvForm_Detail");

                for (int rowIndex = GvForm_Detail.Rows.Count - 2; rowIndex >= 0; rowIndex--)
                {
                    GridViewRow currentRow = GvForm_Detail.Rows[rowIndex];
                    GridViewRow previousRow = GvForm_Detail.Rows[rowIndex + 1];

                    if (((Label)currentRow.Cells[0].FindControl("lblm1_type_name")).Text == ((Label)previousRow.Cells[0].FindControl("lblm1_type_name")).Text)
                    {
                        if (previousRow.Cells[0].RowSpan < 2)
                        {
                            currentRow.Cells[0].RowSpan = 2;

                        }
                        else
                        {
                            currentRow.Cells[0].RowSpan = previousRow.Cells[0].RowSpan + 1;

                        }
                        previousRow.Cells[0].Visible = false;
                    }

                }
                break;
        }
    }
    #endregion

    #endregion

    #region SelectedIndexChanged
    protected void SelectedIndexChanged(object sender, EventArgs e)
    {
        if (sender is DropDownList)
        {
            DropDownList ddlName = (DropDownList)sender;

            var div_chk2type = ((Control)fvadd_formresult.FindControl("div_chk2type"));
            var ddltopic_form = ((DropDownList)fvadd_formresult.FindControl("ddltopic_form"));
            var chkm2type = ((CheckBoxList)fvadd_formresult.FindControl("chkm2type"));
            var ddlorgidx = ((DropDownList)fvadd_formresult.FindControl("ddlorgidx"));
            var chkposidx = ((CheckBoxList)fvadd_formresult.FindControl("chkposidx"));
            var txtform_name = ((TextBox)fvadd_formresult.FindControl("txtform_name"));
            string poscomma = String.Empty;


            switch (ddlName.ID)
            {
                case "ddlorgidx":
                    chkposidx.ClearSelection();
                    chkposidx.Enabled = true;
                    if (ddlorgidx.SelectedValue != "0")
                    {
                        txtform_name.Enabled = true;
                        ddltopic_form.Enabled = true;

                        select_positiongroupwhere_orgidx(int.Parse(ddlorgidx.SelectedValue));

                        if (ViewState["check_position"].ToString() != String.Empty && ViewState["check_position"].ToString() != null)
                        {
                            poscomma = ViewState["check_position"].ToString();
                            string[] ToId = poscomma.Split(',');

                            foreach (ListItem li in chkposidx.Items)
                            {
                                foreach (string To_check in ToId)
                                {

                                    if (li.Value == To_check)
                                    {

                                        li.Enabled = false;
                                        break;
                                    }
                                    else
                                    {
                                        li.Enabled = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            chkposidx.ClearSelection();
                            chkposidx.Enabled = true;
                        }
                    }
                    else
                    {
                        txtform_name.Enabled = false;
                        chkposidx.Enabled = false;
                        ddltopic_form.Enabled = false;


                    }

                    ViewState["check_position"] = "0";
                    break;

                case "ddltopic_form":

                    if (ddltopic_form.SelectedValue != "0")
                    {
                        select_m2type(chkm2type, int.Parse(ddltopic_form.SelectedValue));
                        div_chk2type.Visible = true;
                    }
                    else
                    {
                        div_chk2type.Visible = false;
                        chkm2type.ClearSelection();

                    }
                    break;

                case "ddltopic_edit":
                    var ddltopic_edit = (DropDownList)sender;
                    var rowGrid_formupdate = (GridViewRow)ddltopic_edit.NamingContainer;

                    DropDownList ddlsubtopic_edit = (DropDownList)rowGrid_formupdate.FindControl("ddlsubtopic_edit");

                    select_m2typidx_check(ddlsubtopic_edit, int.Parse(ddltopic_edit.SelectedValue));
                    break;


            }
        }
    }
    #endregion

    #region SetDefault
    protected void SetDefaultpage(int page)
    {

        switch (page)
        {

            case 1:
                _divMenuLiToViewIndex.Attributes.Add("class", "active");
                _divMenuLiToViewAdd.Attributes.Remove("class");
                MvMaster.SetActiveView(ViewIndex);
                _divMenuLiToViewFormResult.Attributes.Add("class", "active");
                _divMenuLiToViewTopic.Attributes.Remove("class");
                _divMenuLiToViewSubTopic.Attributes.Remove("class");

                div_formresult.Visible = true;
                div_topic.Visible = false;
                div_subtopic.Visible = false;
                select_masterformresult(GvForm_Result);
                mergeCell(GvForm_Result);
                div_formdetail.Visible = false;
                //txt.Text = ViewState["check_position"].ToString();
                break;

            case 2:

                _divMenuLiToViewIndex.Attributes.Remove("class");
                _divMenuLiToViewAdd.Attributes.Add("class", "active");
                MvMaster.SetActiveView(ViewInsert);

                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataBind();

                divadd_topic.Visible = false;
                divadd_subtopic.Visible = false;
                divadd_formresult.Visible = false;

                imgtopic.ImageUrl = "~/images/tpm-formresult/btntopic.png";
                imgsubtopic.ImageUrl = "~/images/tpm-formresult/btnsubtopic.png";
                imgformresult.ImageUrl = "~/images/tpm-formresult/btnform.png";
                break;

        }
    }

    protected void SetLiActive(int system)
    {
        switch (system)
        {

            case 1:
                _divMenuLiToViewFormResult.Attributes.Remove("class");
                _divMenuLiToViewTopic.Attributes.Add("class", "active");
                _divMenuLiToViewSubTopic.Attributes.Remove("class");


                div_topic.Visible = true;
                div_formresult.Visible = false;
                div_formdetail.Visible = false;
                div_subtopic.Visible = false;

                select_mastertopic(GvTopic);
                break;

            case 2:
                _divMenuLiToViewFormResult.Attributes.Remove("class");
                _divMenuLiToViewTopic.Attributes.Remove("class");
                _divMenuLiToViewSubTopic.Attributes.Add("class", "active");

                div_topic.Visible = false;
                div_formresult.Visible = false;
                div_formdetail.Visible = false;
                div_subtopic.Visible = true;

                select_mastersubtopic(GvSubTopic);
                mergeCell(GvSubTopic);
                break;

            case 3:
                _divMenuLiToViewFormResult.Attributes.Add("class", "active");
                _divMenuLiToViewTopic.Attributes.Remove("class");
                _divMenuLiToViewSubTopic.Attributes.Remove("class");


                div_topic.Visible = false;
                div_formresult.Visible = true;
                div_formdetail.Visible = false;
                div_subtopic.Visible = false;

                select_masterformresult(GvForm_Result);
                mergeCell(GvForm_Result);
                break;

        }
    }

    #endregion

    #region SetDefault Form

    #region Master Topic
    protected void SetViewState_FormTopic()
    {

        DataSet dsTopicList = new DataSet();
        dsTopicList.Tables.Add("dsAddListTable_Topic");
        dsTopicList.Tables["dsAddListTable_Topic"].Columns.Add("m0_typeidx", typeof(int));
        dsTopicList.Tables["dsAddListTable_Topic"].Columns.Add("m1_type_name", typeof(String));
        dsTopicList.Tables["dsAddListTable_Topic"].Columns.Add("status_name", typeof(String));
        dsTopicList.Tables["dsAddListTable_Topic"].Columns.Add("status", typeof(int));

        ViewState["vsAddListTable_Topic"] = dsTopicList;

    }

    protected void CleardataSetFormList(GridView GvName)
    {
        ViewState["vsAddListTable_Topic"] = null;
        GvName.DataSource = ViewState["vsAddListTable_Topic"];
        GvName.DataBind();
        SetViewState_FormTopic();
    }

    protected void setAddList_FormTopic(TextBox txttopic, DropDownList ddltype, DropDownList ddlstatus, GridView gvName)
    {

        if (ViewState["vsAddListTable_Topic"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsAddListTable_Topic"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable_Topic"].Rows)
            {

                if (dr["m1_type_name"].ToString() == txttopic.Text && int.Parse(dr["m0_typeidx"].ToString()) == 2)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }
            }

            DataRow drContacts = dsContacts.Tables["dsAddListTable_Topic"].NewRow();



            drContacts["m1_type_name"] = txttopic.Text;
            drContacts["m0_typeidx"] = int.Parse(ddltype.SelectedValue);
            drContacts["status_name"] = ddlstatus.SelectedItem.Text;
            drContacts["status"] = int.Parse(ddlstatus.SelectedValue);

            dsContacts.Tables["dsAddListTable_Topic"].Rows.Add(drContacts);
            ViewState["vsAddListTable_Topic"] = dsContacts;
            setGridData(gvName, dsContacts.Tables["dsAddListTable_Topic"]);
            gvName.Visible = true;

        }

    }

    #endregion

    #region Master SubTopic
    protected void SetViewState_FormSubTopic()
    {

        DataSet dsSubTopicList = new DataSet();
        dsSubTopicList.Tables.Add("dsAddListTable_SubTopic");
        dsSubTopicList.Tables["dsAddListTable_SubTopic"].Columns.Add("m1_typeidx", typeof(int));
        dsSubTopicList.Tables["dsAddListTable_SubTopic"].Columns.Add("m1_type_name", typeof(String));
        dsSubTopicList.Tables["dsAddListTable_SubTopic"].Columns.Add("m2_type_name", typeof(String));
        dsSubTopicList.Tables["dsAddListTable_SubTopic"].Columns.Add("status_name", typeof(String));
        dsSubTopicList.Tables["dsAddListTable_SubTopic"].Columns.Add("status_sub", typeof(int));

        ViewState["vsAddListTable_SubTopic"] = dsSubTopicList;

    }

    protected void CleardataSetSubFormList(GridView GvName)
    {
        ViewState["vsAddListTable_SubTopic"] = null;
        GvName.DataSource = ViewState["vsAddListTable_SubTopic"];
        GvName.DataBind();
        SetViewState_FormSubTopic();
    }

    protected void setAddList_FormSubTopic(DropDownList ddltopic, TextBox m2_type_name, DropDownList ddlstatus, GridView gvName)
    {

        if (ViewState["vsAddListTable_SubTopic"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsAddListTable_SubTopic"];

            foreach (DataRow dr in dsContacts.Tables["dsAddListTable_SubTopic"].Rows)
            {

                if (dr["m2_type_name"].ToString() == m2_type_name.Text && int.Parse(dr["m1_typeidx"].ToString()) == int.Parse(ddltopic.SelectedValue))
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลนี้แล้ว!!!');", true);
                    return;
                }
            }

            DataRow drContacts = dsContacts.Tables["dsAddListTable_SubTopic"].NewRow();



            drContacts["m1_type_name"] = ddltopic.SelectedItem.Text;
            drContacts["m1_typeidx"] = int.Parse(ddltopic.SelectedValue);
            drContacts["m2_type_name"] = m2_type_name.Text;
            drContacts["status_name"] = ddlstatus.SelectedItem.Text;
            drContacts["status_sub"] = int.Parse(ddlstatus.SelectedValue);

            dsContacts.Tables["dsAddListTable_SubTopic"].Rows.Add(drContacts);
            ViewState["vsAddListTable_SubTopic"] = dsContacts;
            setGridData(gvName, dsContacts.Tables["dsAddListTable_SubTopic"]);
            gvName.Visible = true;

        }

    }

    #endregion

    #region FormResult
    protected void SetViewState_FormResult()
    {

        DataSet dsFormResultList = new DataSet();
        dsFormResultList.Tables.Add("dsAddListTable_FormResult");
        dsFormResultList.Tables["dsAddListTable_FormResult"].Columns.Add("orgidx", typeof(int));
        dsFormResultList.Tables["dsAddListTable_FormResult"].Columns.Add("orgname", typeof(String));

        dsFormResultList.Tables["dsAddListTable_FormResult"].Columns.Add("posidx_comma", typeof(String));
        dsFormResultList.Tables["dsAddListTable_FormResult"].Columns.Add("posname", typeof(String));
        dsFormResultList.Tables["dsAddListTable_FormResult"].Columns.Add("form_name", typeof(String));

        dsFormResultList.Tables["dsAddListTable_FormResult"].Columns.Add("m1_typeidx", typeof(int));
        dsFormResultList.Tables["dsAddListTable_FormResult"].Columns.Add("m1_type_name", typeof(String));
        dsFormResultList.Tables["dsAddListTable_FormResult"].Columns.Add("m2_typeidx_comma", typeof(String));
        dsFormResultList.Tables["dsAddListTable_FormResult"].Columns.Add("m2_type_name", typeof(String));

        dsFormResultList.Tables["dsAddListTable_FormResult"].Columns.Add("status_name", typeof(String));
        dsFormResultList.Tables["dsAddListTable_FormResult"].Columns.Add("status", typeof(int));

        ViewState["vsAddListTable_FormResult"] = dsFormResultList;

    }

    protected void CleardataSetFormResultList(GridView GvName)
    {
        ViewState["vsAddListTable_FormResult"] = null;
        GvName.DataSource = ViewState["vsAddListTable_FormResult"];
        GvName.DataBind();
        SetViewState_FormResult();
    }

    protected void setAddList_FormResult(GridView gvName)
    {
        DropDownList ddlorgidx = (DropDownList)fvadd_formresult.FindControl("ddlorgidx");
        //DropDownList ddlposidx = (DropDownList)fvadd_formresult.FindControl("ddlposidx");
        DropDownList ddltopic_form = (DropDownList)fvadd_formresult.FindControl("ddltopic_form");
        CheckBoxList chkm2type = (CheckBoxList)fvadd_formresult.FindControl("chkm2type");
        DropDownList ddlstatus = (DropDownList)fvadd_formresult.FindControl("ddlstatus");
        CheckBoxList chkposidx = (CheckBoxList)fvadd_formresult.FindControl("chkposidx");
        TextBox txtform_name = (TextBox)fvadd_formresult.FindControl("txtform_name");
        //int[] m2count;

        if (ViewState["vsAddListTable_FormResult"] != null)
        {
            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsAddListTable_FormResult"];


            foreach (ListItem item in chkposidx.Items)
            {
                if (item.Selected)
                {
                    ViewState["check_position_insert"] = "1";
                }
            }

            foreach (ListItem item in chkm2type.Items)
            {
                if (item.Selected)
                {
                    ViewState["check_m2idx_insert"] = "1";
                }
            }


            if (ViewState["check_position_insert"].ToString() == "0" && ViewState["check_m2idx_insert"].ToString() == "0")
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกตำแหน่งงาน และหัวข้อย่อย!!!');", true);
                return;
            }
            else if (ViewState["check_position_insert"].ToString() == "0" && ViewState["check_m2idx_insert"].ToString() == "1")
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกตำแหน่งงาน!!!');", true);
                return;
            }
            else if (ViewState["check_position_insert"].ToString() == "1" && ViewState["check_m2idx_insert"].ToString() == "0")
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาเลือกหัวข้อย่อย!!!');", true);
                return;
            }
            else
            {
                foreach (DataRow dr in dsContacts.Tables["dsAddListTable_FormResult"].Rows)
                {

                    if (int.Parse(dr["m1_typeidx"].ToString()) == int.Parse(ddltopic_form.SelectedValue))
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('มีข้อมูลหัวข้อหลักนี้แล้ว!!!');", true);
                        return;
                    }
                }

                DataRow drContacts = dsContacts.Tables["dsAddListTable_FormResult"].NewRow();

                drContacts["orgidx"] = int.Parse(ddlorgidx.SelectedValue);
                drContacts["orgname"] = ddlorgidx.SelectedItem.Text;
                drContacts["form_name"] = txtform_name.Text;

                //drContacts["posidx_comma"] = int.Parse(ddlposidx.SelectedValue);
                //drContacts["posname"] = ddlposidx.SelectedItem.Text;

                drContacts["m1_typeidx"] = int.Parse(ddltopic_form.SelectedValue);
                drContacts["m1_type_name"] = ddltopic_form.SelectedItem.Text;

                drContacts["status_name"] = ddlstatus.SelectedItem.Text;
                drContacts["status"] = int.Parse(ddlstatus.SelectedValue);

                foreach (ListItem item in chkposidx.Items)
                {
                    if (item.Selected)
                    {

                        drContacts["posidx_comma"] += item.Value + "," + "<br>";
                        drContacts["posname"] += item.ToString() + "," + "<br>";
                    }

                }

                foreach (ListItem item in chkm2type.Items)
                {
                    if (item.Selected)
                    {

                        drContacts["m2_typeidx_comma"] += item.Value + "," + "<br>";
                        drContacts["m2_type_name"] += item.ToString() + "," + "<br>";

                    }
                }


                //foreach (ListItem item in chkm2type.Items)
                //{
                //    if (item.Selected)
                //    {
                //        ViewState["m2_typeidx_insert"] += item.Value + ",";

                //        string[] ToId = ViewState["m2_typeidx_insert"].ToString().Split(',');

                //        foreach (string To_check in ToId)
                //        {
                //            if (item.Value == To_check)
                //            {
                //                //li.Selected = true;
                //                break;
                //            }
                //            else
                //            {
                //                drContacts["m2_typeidx_comma"] += item.Value + "," + "<br>";
                //                drContacts["m2_type_name"] += item.ToString() + "," + "<br>";
                //            }
                //        }
                //    }
                //}

                dsContacts.Tables["dsAddListTable_FormResult"].Rows.Add(drContacts);
                ViewState["vsAddListTable_FormResult"] = dsContacts;
                setGridData(gvName, dsContacts.Tables["dsAddListTable_FormResult"]);
                gvName.Visible = true;
            }
        }

    }



    #endregion


    #endregion

    #region BTN

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        GridView GvTopicAdd = (GridView)fvadd_topic.FindControl("GvTopicAdd");
        TextBox txttopic = (TextBox)fvadd_topic.FindControl("txttopic");
        DropDownList ddltype = (DropDownList)fvadd_topic.FindControl("ddltype");
        DropDownList ddlstatus = (DropDownList)fvadd_topic.FindControl("ddlstatus");
        Control div_save = (Control)fvadd_topic.FindControl("div_save");
        DropDownList ddltopic = (DropDownList)fvadd_subtopic.FindControl("ddltopic");
        TextBox txtsubtopic = (TextBox)fvadd_subtopic.FindControl("txtsubtopic");
        GridView GvSubTopicAdd = (GridView)fvadd_subtopic.FindControl("GvSubTopicAdd");
        Control div_save_sub = (Control)fvadd_subtopic.FindControl("div_save_sub");
        DropDownList ddlstatus_sub = (DropDownList)fvadd_subtopic.FindControl("ddlstatus_sub");

        DropDownList ddlorgidx = (DropDownList)fvadd_formresult.FindControl("ddlorgidx");
        CheckBoxList chkposidx = (CheckBoxList)fvadd_formresult.FindControl("chkposidx");
        DropDownList ddltopic_form = (DropDownList)fvadd_formresult.FindControl("ddltopic_form");
        CheckBoxList chkm2type = (CheckBoxList)fvadd_formresult.FindControl("chkm2type");
        GridView GvFormResultAdd = (GridView)fvadd_formresult.FindControl("GvFormResultAdd");
        Control div_save_form = (Control)fvadd_formresult.FindControl("div_save_form");
        Control div_chk2type = (Control)fvadd_formresult.FindControl("div_chk2type");
        TextBox txtform_name = (TextBox)fvadd_formresult.FindControl("txtform_name");


        int type_add = int.Parse(ViewState["m0_type_idx"].ToString());

        switch (cmdName)
        {
            case "_divMenuBtnToDivIndex":
            case "BtnBack":
                SetDefaultpage(1);
                break;

            case "_divMenuBtnToDivAdd":
                SetDefaultpage(2);

                break;
            case "btnsystem":
                string cmdArg_sys = e.CommandArgument.ToString();

                switch (cmdArg_sys)
                {
                    case "1":
                        ViewState["m0_type_idx"] = "1";
                        imgtopic.ImageUrl = "~/images/tpm-formresult/btntopic_active.png";
                        imgsubtopic.ImageUrl = "~/images/tpm-formresult/btnsubtopic.png";
                        imgformresult.ImageUrl = "~/images/tpm-formresult/btnform.png";
                        divadd_topic.Visible = true;
                        fvadd_topic.ChangeMode(FormViewMode.Insert);
                        fvadd_topic.DataBind();
                        divadd_subtopic.Visible = false;
                        divadd_formresult.Visible = false;
                        txttopic.Text = String.Empty;

                        CleardataSetFormList(GvTopicAdd);
                        GvTopicAdd.Visible = false;
                        break;

                    case "2":
                        ViewState["m0_type_idx"] = "2";
                        imgtopic.ImageUrl = "~/images/tpm-formresult/btntopic.png";
                        imgsubtopic.ImageUrl = "~/images/tpm-formresult/btnsubtopic_active.png";
                        imgformresult.ImageUrl = "~/images/tpm-formresult/btnform.png";

                        divadd_topic.Visible = false;
                        divadd_subtopic.Visible = true;
                        divadd_formresult.Visible = false;

                        fvadd_subtopic.ChangeMode(FormViewMode.Insert);
                        fvadd_subtopic.DataBind();

                        ddltopic.SelectedValue = "0";
                        txtsubtopic.Text = String.Empty;

                        CleardataSetSubFormList(GvSubTopicAdd);
                        GvSubTopicAdd.Visible = false;
                        break;
                    case "3":
                        ViewState["m0_type_idx"] = "3";
                        imgtopic.ImageUrl = "~/images/tpm-formresult/btntopic.png";
                        imgsubtopic.ImageUrl = "~/images/tpm-formresult/btnsubtopic.png";
                        imgformresult.ImageUrl = "~/images/tpm-formresult/btnform_active.png";

                        divadd_topic.Visible = false;
                        divadd_subtopic.Visible = false;
                        divadd_formresult.Visible = true;
                        fvadd_formresult.ChangeMode(FormViewMode.Insert);
                        fvadd_formresult.DataBind();
                        ddlorgidx.SelectedValue = "0";
                        chkposidx.ClearSelection();
                        txtform_name.Text = String.Empty;
                        //ddlposidx.SelectedValue = "0";
                        ddltopic_form.SelectedValue = "0";
                        chkm2type.ClearSelection();
                        div_chk2type.Visible = false;
                        CleardataSetFormResultList(GvFormResultAdd);
                        GvFormResultAdd.Visible = false;
                        break;

                }


                break;

            case "CmdInsert_Dataset":

                switch (type_add)
                {
                    case 1:
                        setAddList_FormTopic(txttopic, ddltype, ddlstatus, GvTopicAdd);
                        GvTopicAdd.Visible = true;
                        div_save.Visible = true;
                        txttopic.Text = String.Empty;
                        break;
                    case 2:
                        setAddList_FormSubTopic(ddltopic, txtsubtopic, ddlstatus_sub, GvSubTopicAdd);
                        GvSubTopicAdd.Visible = true;
                        mergeCell(GvSubTopicAdd);
                        ddltopic.SelectedValue = "0";
                        txtsubtopic.Text = String.Empty;
                        div_save_sub.Visible = true;

                        break;
                    case 3:
                        setAddList_FormResult(GvFormResultAdd);
                        mergeCell(GvFormResultAdd);

                        if (ViewState["check_position_insert"].ToString() == "1" && ViewState["check_m2idx_insert"].ToString() == "1")
                        {
                            ddltopic_form.SelectedValue = "0";
                            ddlorgidx.Enabled = false;
                            txtform_name.Enabled = false;
                            chkposidx.Enabled = false;
                            chkm2type.ClearSelection();
                            div_chk2type.Visible = false;
                            div_save_form.Visible = true;
                        }
                        break;
                }

                break;

            case "CmdInsert":
                switch (type_add)
                {
                    case 1:
                        Insert_Topic();
                        SetDefaultpage(1);
                        SetLiActive(1);

                        break;
                    case 2:
                        Insert_SubTopic();
                        SetDefaultpage(1);
                        SetLiActive(2);
                        break;
                    case 3:
                        Insert_FormResult();
                        SetDefaultpage(1);
                        SetLiActive(3);
                        break;
                }

                break;

            case "cmddata_formresult":

                SetLiActive(3);
                lblheadbanner.Text = "แบบฟอร์มประเมินประจำปี";

                break;

            case "cmddata_topic":

                SetLiActive(1);
                lblheadbanner.Text = "หัวข้อประเมินหลัก";

                break;

            case "cmddata_subtopic":
                SetLiActive(2);
                lblheadbanner.Text = "หัวข้อประเมินย่อย";

                break;

            case "CmdDelTopic":
                string[] arg_topic = new string[2];
                arg_topic = e.CommandArgument.ToString().Split(';');

                int m1_typeidx = int.Parse(arg_topic[0].ToString());
                string m1_typename = arg_topic[1].ToString();

                Update_Topic(2, int.Parse(ViewState["EmpIDX"].ToString()), 9, m1_typeidx, m1_typename);
                SetLiActive(1);
                break;

            case "CmdDelSubTopic":
                string[] arg_subtopic = new string[3];
                arg_subtopic = e.CommandArgument.ToString().Split(';');

                int m1_typeidx_sub = int.Parse(arg_subtopic[0].ToString());
                int m2_typeidx = int.Parse(arg_subtopic[1].ToString());
                string m2_typename = arg_subtopic[2].ToString();


                Update_SubTopic(4, int.Parse(ViewState["EmpIDX"].ToString()), 9, m1_typeidx_sub, m2_typeidx, m2_typename);
                SetLiActive(2);
                break;

            case "CmdDelete_form":
                string[] arg_form = new string[3];
                arg_form = e.CommandArgument.ToString().Split(';');
                int u0_typeidx = int.Parse(arg_form[0].ToString());


                DeleteForm(int.Parse(ViewState["EmpIDX"].ToString()), 9, u0_typeidx);
                select_masterformresult(GvForm_Result);
                mergeCell(GvForm_Result);
                break;

            case "CmdDetail":
                string[] arg_formresult = new string[2];
                arg_formresult = e.CommandArgument.ToString().Split(';');
                int u0_typeidx_result = int.Parse(arg_formresult[0].ToString());

                ViewState["u0_typeidx"] = u0_typeidx_result;
                ViewState["status_form"] = int.Parse(arg_formresult[1].ToString());

                div_formdetail.Visible = true;
                div_formresult.Visible = false;

                select_masterformdetail(GvForm_Detail, u0_typeidx_result);
                mergeCell(GvForm_Detail);

                break;

            case "CmdDelete_formdetail":
                string[] arg_formdetail = new string[3];
                arg_formdetail = e.CommandArgument.ToString().Split(';');
                int r0_typeidx = int.Parse(arg_formdetail[0].ToString());


                DeleteFormDetail(int.Parse(ViewState["EmpIDX"].ToString()), 9, r0_typeidx);
                select_masterformdetail(GvForm_Detail, int.Parse(ViewState["u0_typeidx"].ToString()));
                mergeCell(GvForm_Detail);
                //SetLiActive(3);
                break;

            case "CmdAdd_form":
                string[] arg_form_add = new string[3];
                arg_form_add = e.CommandArgument.ToString().Split(';');

                string orgidx = arg_form_add[0].ToString();
                string posidx_comma = arg_form_add[1].ToString();
                string form_name = arg_form_add[2].ToString();

                SetDefaultpage(2);
                ViewState["m0_type_idx"] = "3";
                imgtopic.ImageUrl = "~/images/tpm-formresult/btntopic.png";
                imgsubtopic.ImageUrl = "~/images/tpm-formresult/btnsubtopic.png";
                imgformresult.ImageUrl = "~/images/tpm-formresult/btnform_active.png";

                divadd_topic.Visible = false;
                divadd_subtopic.Visible = false;
                divadd_formresult.Visible = true;
                //fvadd_formresult.ChangeMode(FormViewMode.Insert);
                //fvadd_formresult.DataBind();
                ddlorgidx.SelectedValue = orgidx;
                ddlorgidx.Enabled = false;
                chkposidx.Enabled = false;
                string[] ToId = posidx_comma.Split(',');

                foreach (ListItem li in chkposidx.Items)
                {
                    foreach (string To_check in ToId)
                    {

                        if (li.Value == To_check)
                        {
                            li.Selected = true;

                            break;
                        }
                        else
                        {
                            li.Selected = false;
                        }
                    }
                }

                txtform_name.Text =  form_name;
                txtform_name.Enabled = false;
                ddltopic_form.Enabled = true;
                ddltopic_form.SelectedValue = "0";
                chkm2type.ClearSelection();
                div_chk2type.Visible = false;
                CleardataSetFormResultList(GvFormResultAdd);
                GvFormResultAdd.Visible = false;
                div_save_form.Visible = false;
                break;
       
        }
    }
    #endregion
}