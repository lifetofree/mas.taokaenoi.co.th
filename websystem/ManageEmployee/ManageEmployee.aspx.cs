﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


using System.Data;
using System.IO;
using System.Configuration;
using System.Collections;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Globalization;
using System.Threading;
using System.Text;



public partial class websystem_ManageEmployee_ManageEmployee : System.Web.UI.Page
{

    #region initial 
    function_tool _funcTool = new function_tool();

    service_execute _service_exec = new service_execute();

    data_employee _data_employee = new data_employee();
    dataODSP_Relation dataODSPReration = new dataODSP_Relation();
    data_Region _data_Region = new data_Region();
    data_presonal data_presonal = new data_presonal();

    static string _hrsBenefits = ConfigurationManager.AppSettings["hrsBenefits"];
    string Conn_manage_employee = "conn_centralized";
    //int _actionType = 0;
    string _local_xml = "";
    int EDUIDX;
    string rtCode = "";
    string msAlert = "";
    string _localJson = "";

    #endregion

    #region page Lode
    protected void Page_Load(object sender, EventArgs e)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        TextBox txtEmpin = fvAddDataEmployee.FindControl("txtEmpin") as TextBox;

        if (!this.IsPostBack)
        {
            SetDefultData();
           // permissions();
            MvMaster.SetActiveView(ViewIndex);
            linkBtnTrigger(BoxButtonAddEmployee);
        }
        else
        {
            linkBtnTrigger(btn_save_file1);
            linkBtnTrigger(btn_save_pic);

        }
        //Select_tabel_posision();
        //Select_tabel_Experience();
        //Select_tabel_Benefits();
        used_java_Scripts();

    }

    #endregion

    #region  permissions
    protected void permissions()
    {

        ViewState["emp_idx"] = int.Parse(Session["emp_idx"].ToString());

        LinkButton BtnadjustPay = (LinkButton)viewEdit_dataEmployee.FindControl("BtnadjustPay");
        TextBox txttype = (TextBox)fvEdit_dataEmployee.FindControl("txttype");
        ViewState["type_employee"] = txttype;

        if (int.Parse(ViewState["emp_idx"].ToString()) == 127
            || int.Parse(ViewState["emp_idx"].ToString()) == 126
            || int.Parse(ViewState["emp_idx"].ToString()) == 129
            || int.Parse(ViewState["emp_idx"].ToString()) == 130
            || int.Parse(ViewState["emp_idx"].ToString()) == 128
            || int.Parse(ViewState["emp_idx"].ToString()) == 132
            && ViewState["type_employee"] == "1")
        {
            BtnadjustPay.Visible = true;
        }
        else
        {
            BtnadjustPay.Visible = false;
        }
        if (int.Parse(ViewState["emp_idx"].ToString()) == 127
             || int.Parse(ViewState["emp_idx"].ToString()) == 126
             || int.Parse(ViewState["emp_idx"].ToString()) == 138
             || int.Parse(ViewState["emp_idx"].ToString()) == 134
             || int.Parse(ViewState["emp_idx"].ToString()) == 133
             || int.Parse(ViewState["emp_idx"].ToString()) == 3760
             && ViewState["type_employee"] == "2")
        {
            BtnadjustPay.Visible = true;
        }
        else
        {
            BtnadjustPay.Visible = false;
        }

        //}
        //เงินนจัดการพนักงานต่างด้าว
        //else if (int.Parse(ViewState["type_employee"].ToString()) == 2)
        //{
        //    if (int.Parse(ViewState["emp_idx"].ToString()) == )
        //    {
        //        BtnadjustPay.Visible = true;
        //    }

        //}

        //if (int.Parse(ViewState["emp_idx"].ToString()) == 3742)
        //{

        //    BtnadjustPay.Visible = true;
        //}
        //else
        //{
        //    BtnadjustPay.Visible = false;
        //}
    }

    #endregion

    //#region getEncrypt and getDecrypt
    //protected void getEncrypt_and_getDecrypt()
    //{
    //    string _temp = _funcTool.getEncryptRC4("stringtest", _hrsBenefits);
    //    Response.Write(_temp);
    //    Response.Write("<br />");
    //    Response.Write(_funcTool.getDecryptRC4(_temp, _hrsBenefits));

    //}
    //#endregion

    #region SetDefultData
    protected void SetDefultData()
    {
        lbMenu0.BackColor = System.Drawing.Color.LightGray;
        SelectData_Employee();
        Organization_search(0, "0");
        Department_search(0, "0");
        BoxRemovePosisionn.Visible = false;
        BoxEdit_Sarary.Visible = false;
        BoxPenaltyEmployee.Visible = false;
        //select_empIdx_present();
    }
    #endregion

    #region Class InsertData

    protected void insert_sarary_new()
    {
        var txtSarary_new = (TextBox)fvEdit_Sarary.FindControl("txtSarary_new");
        var txtSarary_old = (TextBox)fvEdit_Sarary.FindControl("txtSarary_old");
        var txt_date_approve_sarary = (TextBox)fvEdit_Sarary.FindControl("txt_date_approve_sarary");
        var ddl_motive = (DropDownList)fvEdit_Sarary.FindControl("ddl_motive");
        var cbrecipients = (CheckBoxList)fvEdit_Sarary.FindControl("cbrecipients");
        var LoEmpCode = (Label)fvEdit_dataEmployee.FindControl("LoEmpCode");

        var dataset_benefits = (DataSet)ViewState["Create_DataBenefits"];

        if (ViewState["Create_DataBenefits"].ToString() == "0")

        {

        }

        else if (ViewState["Create_DataBenefits"].ToString() != "0")

        {
            var Add_benefits = new BoxBenefitsList[dataset_benefits.Tables[0].Rows.Count];
            int p = 0;

            foreach (DataRow data_row in dataset_benefits.Tables[0].Rows)
            {

                Add_benefits[p] = new BoxBenefitsList();
                Add_benefits[p].benefit_idx = int.Parse(data_row["BenefitsID"].ToString());
                Add_benefits[p].emp_idx_benefit = int.Parse(LoEmpCode.Text);
                p++;
            }
            data_employee data_benefits = new data_employee();
            data_benefits.BoxBenefits = Add_benefits;
            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", data_benefits, 112);
        }


        data_employee sarary_new_ = new data_employee();
        BoxSararyList add_sarary_new_ = new BoxSararyList();
        sarary_new_.BoxSarary = new BoxSararyList[1];
        add_sarary_new_.emp_sarary_idx = int.Parse(LoEmpCode.Text);
        add_sarary_new_.motive_of_pay = (ddl_motive.SelectedItem.ToString());
        add_sarary_new_.sarary_new = txtSarary_new.Text;
        add_sarary_new_.sarary_old = txtSarary_old.Text;
        add_sarary_new_.date_approve_sarary = txt_date_approve_sarary.Text;

        sarary_new_.BoxSarary[0] = add_sarary_new_;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", sarary_new_, 110);
        sarary_new_ = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        ViewState["returnCode_addSararyNew"] = sarary_new_.return_value;

        if (sarary_new_.return_value == "01")
        {
            Response.Redirect("~/alertsuccess");
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('!! ผิดพลาด บันทึกข้อมูลไม่สำเร็จ');", true);

        }

    }

    protected void insert_sarary()
    {
        var txtSarary_new = (TextBox)fvEdit_Sarary.FindControl("txtSarary_new");
        var txtSarary_old = (TextBox)fvEdit_Sarary.FindControl("txtSarary_old");
        var txt_date_approve_sarary = (TextBox)fvEdit_Sarary.FindControl("txt_date_approve_sarary");
        var ddl_motive = (DropDownList)fvEdit_Sarary.FindControl("ddl_motive");
        var cbrecipients = (CheckBoxList)fvEdit_Sarary.FindControl("cbrecipients");
        var LoEmpCode = (Label)fvEdit_dataEmployee.FindControl("LoEmpCode");
        var txt_status_employee = (TextBox)fvEdit_dataEmployee.FindControl("txt_status_employee");

        var dataset_benefits = (DataSet)ViewState["Create_DataBenefits"];

        if (ViewState["Create_DataBenefits"].ToString() == "0")

        {


        }

        else if (ViewState["Create_DataBenefits"].ToString() != "0")

        {
            data_employee data_benefits = new data_employee();
            BoxSararyList sararylist = new BoxSararyList();
            data_benefits.BoxSarary = new BoxSararyList[1];

            sararylist.emp_sarary_idx = int.Parse(LoEmpCode.Text);

            var Add_benefits = new BoxBenefitsList[dataset_benefits.Tables[0].Rows.Count];
            int p = 0;

            foreach (DataRow data_row in dataset_benefits.Tables[0].Rows)
            {

                Add_benefits[p] = new BoxBenefitsList();
                Add_benefits[p].benefit_idx = int.Parse(data_row["BenefitsID"].ToString());
                Add_benefits[p].emp_idx_benefit = int.Parse(LoEmpCode.Text);
                p++;
            }

            data_benefits.BoxBenefits = Add_benefits;
            data_benefits.BoxSarary[0] = sararylist;
            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", data_benefits, 112);
        }


        data_employee sarary_new = new data_employee();
        BoxSararyList add_sarary_new = new BoxSararyList();
        sarary_new.BoxSarary = new BoxSararyList[1];

        add_sarary_new.emp_sarary_idx = int.Parse(LoEmpCode.Text);
        add_sarary_new.motive_of_pay = (ddl_motive.SelectedItem.ToString());
        add_sarary_new.sarary_new = txtSarary_new.Text;
        add_sarary_new.sarary_old = txtSarary_old.Text;
        add_sarary_new.date_approve_sarary = txt_date_approve_sarary.Text;
        //add_sarary_new.Benefits_new_ = add_idBef;

        sarary_new.BoxSarary[0] = add_sarary_new;
        // check_XML.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(sarary_new));
        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", sarary_new, 103);
        sarary_new = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        ViewState["returnCode_addSararyNew_"] = sarary_new.return_value;

        if (sarary_new.return_value == "01")
        {
            Response.Redirect("~/alertsuccess");
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('!! ผิดพลาด บันทึกข้อมูลไม่สำเร็จ');", true);
        }
    }

    ////////////////////// บันทึกการลงโทษพนักงาน ////////////////////////////////
    protected void insert_penalty()
    {
        // penalty_history();
        var txt_number_penalty = (TextBox)viewEdit_dataEmployee.FindControl("txt_number_penalty");
        var txt_details_penalty = (TextBox)viewEdit_dataEmployee.FindControl("txt_details_penalty");
        var ddl_type_penalty = (DropDownList)viewEdit_dataEmployee.FindControl("ddl_type_penalty");
        var ddl_next_penalty = (DropDownList)viewEdit_dataEmployee.FindControl("ddl_next_penalty");
        var name_of_warning = (TextBox)viewEdit_dataEmployee.FindControl("name_of_warning");
        var LoEmpCode = (Label)fvEdit_dataEmployee.FindControl("LoEmpCode");

        data_employee penalty__ = new data_employee();
        penalty_details add_penalty__ = new penalty_details();
        penalty__.penalty_list = new penalty_details[1];

        add_penalty__.emp_idx_penalty = int.Parse(LoEmpCode.Text);
        add_penalty__.details__ = txt_details_penalty.Text;
        add_penalty__.name__ = name_of_warning.Text;
        add_penalty__.penalty_number = txt_number_penalty.Text;
        add_penalty__.next_penalty = ddl_next_penalty.SelectedItem.Text;
        add_penalty__.date_delinquent = txt_date_delinquent.Text;
        add_penalty__.date_of_warning = txt_date_of_warning.Text;

        if (ddl_type_penalty.SelectedValue == "3")
        {
            txt_quantity_day.Enabled = true;
            txt_start_date_break.Enabled = true;
            txt_end_date_break.Enabled = true;

            add_penalty__.penalty_was = ddl_type_penalty.SelectedItem.Text;
            add_penalty__.penalty_type = int.Parse(ddl_type_penalty.SelectedValue);
            add_penalty__.qty_day = txt_quantity_day.Text;
            add_penalty__.start_date_break = txt_start_date_break.Text;
            add_penalty__.end_date_break = txt_end_date_break.Text;

        }
        else
        {
            add_penalty__.penalty_type = int.Parse(ddl_type_penalty.SelectedValue);
            add_penalty__.penalty_was = ddl_type_penalty.SelectedItem.Text;
        }

        penalty__.penalty_list[0] = add_penalty__;
        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", penalty__, 104);
        penalty__ = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        ViewState["return_doccode"] = penalty__.return_code;
        ViewState["returnCode_penalty"] = penalty__.return_value;


        HttpFileCollection file_penalty = Request.Files;

        for (int ii = 0; ii < file_penalty.Count; ii++)
        {
            HttpPostedFile uploade_penalty = file_penalty[ii];
            if (uploade_penalty.ContentLength > 1)
            {
                string getPath = ConfigurationSettings.AppSettings["upload_penalty_file"];

                string RECode_document = ViewState["return_doccode"].ToString();
                string fileName_upload = RECode_document;
                string filePath_upload = Server.MapPath(getPath + RECode_document);
                if (!Directory.Exists(filePath_upload))
                {
                    Directory.CreateDirectory(filePath_upload);
                }
                string extension = Path.GetExtension(uploade_penalty.FileName);

                uploade_penalty.SaveAs(Server.MapPath(getPath + RECode_document) + "\\" + fileName_upload + extension);
            }
        }

        if (penalty__.return_value == "01")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลสำเร็จ');", true);

        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('!! ผิดพลาด บันทึกข้อมูลไม่สำเร็จ');", true);
        }
        penalty_history();
        //var HyperLink1 = (HyperLink)history_penalty.Items[0].FindControl("HyperLink1");
        //HyperLink1.Focus();
        //history_penalty.Focus();

    }

    protected void insert_position_form_edit()
    {
        DropDownList ddl_add_org_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_org_idx");
        DropDownList ddl_add_rdept_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_rdept_idx");
        DropDownList ddl_add_rsec_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_rsec_idx");
        DropDownList ddl_add_rpos_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_rpos_idx");
        DropDownList ddl_add_rpos_status = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_rpos_status");

        data_employee position_edit_ = new data_employee();
        employee_detail add_position_from_edit = new employee_detail();
        position_edit_.employee_list = new employee_detail[1];
        add_position_from_edit.rpos_idx = int.Parse(ddl_add_rpos_idx.SelectedValue);
        add_position_from_edit.status_position = int.Parse(ddl_add_rpos_status.SelectedValue);
        add_position_from_edit.emp_editor = (int)Session["emp_idx"];
        add_position_from_edit.emp_idx = (int)ViewState["emp_idx"];

        position_edit_.employee_list[0] = add_position_from_edit;
        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", position_edit_, 107);
        position_edit_ = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        ViewState["returnCode_position_editadd"] = position_edit_.return_value;

        if (int.Parse(ddl_add_rpos_status.SelectedValue) == 1)
        {
            ViewState["rsec_idx"] = int.Parse(ddl_add_rpos_idx.SelectedValue);
        }
        if (position_edit_.return_value == "01")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลสำเร็จ');", true);
            // Response.Redirect("~/alertsuccess");
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('!! ผิดพลาด บันทึกข้อมูลไม่สำเร็จ');", true);
        }

    }

    protected void InsertData_Employee()
    {
        #region ประกาศตัวแปร
        var BoxEmployeeEN = (Panel)fvAddDataEmployee.FindControl("BoxEmployeeEN");
        //ประกาศตัวแปรข้อมูลพนักงานเบื้องตัน
        var txtEmpCodeOld = (TextBox)fvAddDataEmployee.FindControl("txtEmpCodeOld");
        var txtEmpin = (TextBox)fvAddDataEmployee.FindControl("txtEmpin");
        var ddEmpType = (DropDownList)fvAddDataEmployee.FindControl("ddEmpType");
        var ddOrganiztion = (DropDownList)fvAddDataEmployee.FindControl("ddOrganiztion");
        var ddDepartment = (DropDownList)fvAddDataEmployee.FindControl("ddDepartment");
        var ddCostCenter = (DropDownList)fvAddDataEmployee.FindControl("ddCostCenter");
        var ddDivision = (DropDownList)fvAddDataEmployee.FindControl("ddDivision");
        var ddPosition = (DropDownList)fvAddDataEmployee.FindControl("ddPosition");
        var ddlLavel = (DropDownList)fvAddDataEmployee.FindControl("ddlLavel");
        var ddPrefixTH = (DropDownList)fvAddDataEmployee.FindControl("ddPrefixTH");
        var txtNameTH = (TextBox)fvAddDataEmployee.FindControl("txtNameTH");
        var txtLastnameTH = (TextBox)fvAddDataEmployee.FindControl("txtLastnameTH");
        var ddPrefixEN = (DropDownList)fvAddDataEmployee.FindControl("ddPrefixEN");
        var txtNameEN = (TextBox)fvAddDataEmployee.FindControl("txtNameEN");
        var txtLastnameEN = (TextBox)fvAddDataEmployee.FindControl("txtLastnameEN");
        var txtNickNameTH = (TextBox)fvAddDataEmployee.FindControl("txtNickNameTH");
        var txtNickNameEN = (TextBox)fvAddDataEmployee.FindControl("txtNickNameEN");
        var ddlistSex = (DropDownList)fvAddDataEmployee.FindControl("ddlistSex");
        var txtBirthday = (TextBox)fvAddDataEmployee.FindControl("txtBirthday");
        var ddlNationality = (DropDownList)fvAddDataEmployee.FindControl("ddlNationality");
        var ddRace = (DropDownList)fvAddDataEmployee.FindControl("ddRace");
        var ddReligion = (DropDownList)fvAddDataEmployee.FindControl("ddReligion");
        var ddlApprover_Leave1 = (DropDownList)fvAddDataEmployee.FindControl("ddlApprover_Leave1");
        var ddlApprover_Leave2 = (DropDownList)fvAddDataEmployee.FindControl("ddlApprover_Leave2");
        var txtSarary = (TextBox)fvAddDataEmployee.FindControl("txtSarary");

        //ประกาศตัวแปรที่อยู่ของพนักงานและข้อมูลการติดต่อ

        var txtPresentAddress = (TextBox)fvAddDataEmployee.FindControl("txtPresentAddress");
        var ddCountry = (DropDownList)fvAddDataEmployee.FindControl("ddCountry");
        var ddProvince = (DropDownList)fvAddDataEmployee.FindControl("ddProvince");
        var ddAmphoe = (DropDownList)fvAddDataEmployee.FindControl("ddAmphoe");
        var ddDistrict = (DropDownList)fvAddDataEmployee.FindControl("ddDistrict");
        var txtMobilePhone = (TextBox)fvAddDataEmployee.FindControl("txtMobilePhone");
        var txtHomePhone = (TextBox)fvAddDataEmployee.FindControl("txtHomePhone");
        var txtZipCode = (TextBox)fvAddDataEmployee.FindControl("txtZipCode");
        var txtEmail = (TextBox)fvAddDataEmployee.FindControl("txtEmail");
        var ddMilitary = (DropDownList)fvAddDataEmployee.FindControl("ddMilitary");
        var txtIDCard = (TextBox)fvAddDataEmployee.FindControl("txtIDCard");
        var txtIIssuedAtIDCard = (TextBox)fvAddDataEmployee.FindControl("txtIIssuedAtIDCard");
        var txtDateExpIDCard = (TextBox)fvAddDataEmployee.FindControl("txtDateExpIDCard");
        var txtPermanentaddress = (TextBox)fvAddDataEmployee.FindControl("txtPermanentaddress");
        var ddCountry2 = (DropDownList)fvAddDataEmployee.FindControl("ddCountry2");
        var ddProvince2 = (DropDownList)fvAddDataEmployee.FindControl("ddProvince2");
        var ddAmphoe2 = (DropDownList)fvAddDataEmployee.FindControl("ddAmphoe2");
        var ddDistrict2 = (DropDownList)fvAddDataEmployee.FindControl("ddDistrict2");
        var txtZipcode2 = (TextBox)fvAddDataEmployee.FindControl("txtZipcode2");
        var txtHomePhone2 = (TextBox)fvAddDataEmployee.FindControl("txtHomePhone2");

        //ประกาศตัวแปรข้อมูลประกันสังคมสุขภาพ

        var ddHospital = (DropDownList)fvAddDataEmployee.FindControl("ddHospital");
        var txtSocial = (TextBox)fvAddDataEmployee.FindControl("txtSocial");
        var txtSocialDateExp = (TextBox)fvAddDataEmployee.FindControl("txtSocialDateExp");
        var txtHeight = (TextBox)fvAddDataEmployee.FindControl("txtHeight");
        var txtWeight = (TextBox)fvAddDataEmployee.FindControl("txtWeight");
        var ddBloodgroup = (DropDownList)fvAddDataEmployee.FindControl("ddBloodgroup");
        var ddlBloodgroup = (DropDownList)fvAddDataEmployee.FindControl("ddlBloodgroup");
        var txtScar = (TextBox)fvAddDataEmployee.FindControl("txtScar");
        var rbtnSelect = (RadioButton)fvAddDataEmployee.FindControl("rbtnSelect");

        //ประกาศตัวแปรข้อมูลพนักงานไทย

        //ประกาศตัวแปรข้อมูลพนักงานต่างชาติ
        var txtPermanentaddressEN = (TextBox)fvAddDataEmployee.FindControl("txtPermanentaddressEN");
        var txtPhoneEN = (TextBox)fvAddDataEmployee.FindControl("txtPhoneEN");
        var ddlCountryEN = (DropDownList)fvAddDataEmployee.FindControl("ddlCountryEN");
        var txt_perExpire_date = (TextBox)fvAddDataEmployee.FindControl("txt_perExpire_date");
        var txt_perIssued_date = (TextBox)fvAddDataEmployee.FindControl("txt_perIssued_date");
        var txt_perIssued_at = (TextBox)fvAddDataEmployee.FindControl("txt_perIssued_at");
        var txt_perIssued_No = (TextBox)fvAddDataEmployee.FindControl("txt_perIssued_No");
        var txt_ExpDate = (TextBox)fvAddDataEmployee.FindControl("txt_ExpDate");
        var txt_Issued_Date = (TextBox)fvAddDataEmployee.FindControl("txt_Issued_Date");
        var txt_PassportIssued = (TextBox)fvAddDataEmployee.FindControl("txt_PassportIssued");
        var txt_Passport_Issued_No = (TextBox)fvAddDataEmployee.FindControl("txt_Passport_Issued_No");

        //ประกาศตัวแปรข้อมูลการศึกษา
        var txtmajor = (TextBox)fvAddDataEmployee.FindControl("txtmajor");
        var txtUniversity = (TextBox)fvAddDataEmployee.FindControl("txtUniversity");
        var ddlFaculty = (DropDownList)fvAddDataEmployee.FindControl("ddlFaculty");
        var ddlEducation = (DropDownList)fvAddDataEmployee.FindControl("ddlEducation");
        var txtgraduation_year = (TextBox)fvAddDataEmployee.FindControl("txtgraduation_year");
        var txtGPA = (TextBox)fvAddDataEmployee.FindControl("txtGPA");

        //กำหนดสถานะประสบการณ์ทำงาน
        var chk_nothave_Experience = (CheckBox)fvAddDataEmployee.FindControl("chk_nothave_Experience");
        var chk_have_Experience = (CheckBox)fvAddDataEmployee.FindControl("chk_have_Experience");
        var txtOrganiztion_Old = (TextBox)fvAddDataEmployee.FindControl("txtOrganiztion_Old");
        var txtPositionOld = (TextBox)fvAddDataEmployee.FindControl("txtPositionOld");
        var txtWorkInOld = (TextBox)fvAddDataEmployee.FindControl("txtWorkInOld");
        var txtresign_Old = (TextBox)fvAddDataEmployee.FindControl("txtresign_Old");
        var txtMotive = (TextBox)fvAddDataEmployee.FindControl("txtMotive");
        var txtSalary_Old = (TextBox)fvAddDataEmployee.FindControl("txtSalary_Old");
        var cbrecipients = (CheckBoxList)fvAddDataEmployee.FindControl("cbrecipients");

        //บุคคลอ้างอิง
        var txt_fullname_reference = (TextBox)fvAddDataEmployee.FindControl("txt_fullname_reference");
        var txt_relationship = (TextBox)fvAddDataEmployee.FindControl("txt_relationship");
        var txt_address_reference = (TextBox)fvAddDataEmployee.FindControl("txt_address_reference");
        var txt_mobile_number = (TextBox)fvAddDataEmployee.FindControl("txt_mobile_number");

        var chk_sameAddress_present = (CheckBox)fvAddDataEmployee.FindControl("chk_sameAddress_present");
        var chk_notSameAddress_present = (CheckBox)fvAddDataEmployee.FindControl("chk_notSameAddress_present");

        #endregion

        employee_detail dataEmployee = new employee_detail();
        EmployeeTHlist EmployeeTH = new EmployeeTHlist();
        EmployeeFRlist EmployeeEN = new EmployeeFRlist();
        DetailEducation Education = new DetailEducation();
        EmployeeHealthlist Healthlist = new EmployeeHealthlist();
        BoxPosisionList AddPosision = new BoxPosisionList();
        Reference_persons_list reference_persons = new Reference_persons_list();

        _data_employee.BoxPosision = new BoxPosisionList[1];
        _data_employee.BoxEmployeeHealthlist = new EmployeeHealthlist[1];
        _data_employee.EducationList = new DetailEducation[1];
        _data_employee.BoxEmployeeFRlist = new EmployeeFRlist[1];
        _data_employee.employee_list = new employee_detail[1];
        _data_employee.BoxEmployeeTHlist = new EmployeeTHlist[1];
        _data_employee.Reference_persons = new Reference_persons_list[1];

        dataEmployee.org_idx = 0;
        //บังคับเลือก
        dataEmployee.emp_type_idx = int.Parse(ddEmpType.SelectedValue);
        dataEmployee.emp_start_date = txtEmpin.Text;
        dataEmployee.nat_idx = int.Parse(ddlNationality.SelectedValue);

        RadioButton check_Posision = (RadioButton)fvAddDataEmployee.FindControl("rbtnSelect");

        //ตำแหน่ง

        if (int.Parse(ddOrganiztion.SelectedValue) != 0)

        { dataEmployee.org_idx = int.Parse(ddOrganiztion.SelectedValue); }

        else
        { dataEmployee.org_idx = 0; }

        if (int.Parse(ddDepartment.SelectedValue) != 0)

        { dataEmployee.rdept_idx = int.Parse(ddDepartment.SelectedValue); }

        else

        { dataEmployee.rdept_idx = 0; }

        if (int.Parse(ddDivision.SelectedValue) != 0)
        { dataEmployee.rsec_idx = int.Parse(ddDivision.SelectedValue); }

        else

        { dataEmployee.rsec_idx = 0; }

        dataEmployee.rpos_idx = int.Parse(ddPosition.SelectedValue);
        ViewState["RPosIDX"] = int.Parse(ddPosition.SelectedValue);

        if (int.Parse(ddCostCenter.SelectedItem.Value) != 0)

        { dataEmployee.costcenter_idx = int.Parse(ddCostCenter.SelectedItem.Value); }

        else

        { dataEmployee.costcenter_idx = 0; }

        if (int.Parse(ddRace.SelectedValue) != 0)

        { dataEmployee.race_idx = int.Parse(ddRace.SelectedValue); }

        else

        { dataEmployee.race_idx = 0; }

        if (int.Parse(ddReligion.SelectedValue) != 0)

        { dataEmployee.rel_idx = int.Parse(ddReligion.SelectedValue); }

        else

        { dataEmployee.rel_idx = 0; }

        if (int.Parse(ddPrefixTH.SelectedValue) != 0)

        { dataEmployee.prefix_idx = int.Parse(ddPrefixTH.SelectedValue); }

        else

        { dataEmployee.prefix_idx = 0; }

        dataEmployee.emp_firstname_th = txtNameTH.Text;
        dataEmployee.emp_lastname_th = txtLastnameTH.Text;

        if (txtNickNameTH.Text != null && txtNickNameTH.Text != "")

        { dataEmployee.emp_nickname_th = txtNickNameTH.Text; }

        else

        { dataEmployee.emp_nickname_th = "-"; }

        if (txtNameEN.Text != null && txtNameEN.Text != "")

        { dataEmployee.emp_firstname_en = txtNameEN.Text; }

        else
        { dataEmployee.emp_firstname_en = "-"; }

        if (txtLastnameEN.Text != null && txtLastnameEN.Text != "")

        { dataEmployee.emp_lastname_en = txtLastnameEN.Text; }

        else

        { dataEmployee.emp_lastname_en = "-"; }

        if (txtNickNameEN.Text != null && txtNickNameEN.Text != "")

        { dataEmployee.emp_nickname_en = txtNickNameEN.Text; }

        else

        { dataEmployee.emp_nickname_en = "-"; }

        if (ddlistSex.SelectedValue != "0")

        { dataEmployee.sex_idx = int.Parse(ddlistSex.SelectedValue); }

        else

        { dataEmployee.sex_idx = 0; }

        if (txtBirthday.Text != null && txtBirthday.Text != "")

        { dataEmployee.emp_birthday = txtBirthday.Text; }

        else

        { dataEmployee.emp_birthday = "01-01-1900"; }

        if (txtPresentAddress.Text != null && txtPresentAddress.Text != "")

        { dataEmployee.emp_address = txtPresentAddress.Text; }

        else

        { dataEmployee.emp_address = "-"; }

        if (int.Parse(ddCountry.SelectedValue) != 0)

        {
            dataEmployee.country_idx = int.Parse(ddCountry.SelectedValue);
            ViewState["country"] = int.Parse(ddCountry.SelectedValue);
        }

        else

        { dataEmployee.country_idx = 0; }

        if (int.Parse(ddProvince.SelectedValue) != 0)

        {
            dataEmployee.prov_idx = int.Parse(ddProvince.SelectedValue);
            ViewState["province"] = int.Parse(ddProvince.SelectedValue);
        }

        else

        { dataEmployee.prov_idx = 0; }

        if (int.Parse(ddAmphoe.SelectedValue) != 0)

        {
            dataEmployee.amp_idx = int.Parse(ddAmphoe.SelectedValue);
            ViewState["amphoe"] = int.Parse(ddAmphoe.SelectedValue);
        }

        else

        { dataEmployee.amp_idx = 0; }

        if (int.Parse(ddDistrict.SelectedValue) != 0)

        {
            dataEmployee.dist_idx = int.Parse(ddDistrict.SelectedValue);
            ViewState["district"] = int.Parse(ddDistrict.SelectedValue);
        }

        else

        { dataEmployee.dist_idx = 0; }

        if (txtZipCode.Text != null && txtZipCode.Text != "")

        { dataEmployee.post_code = txtZipCode.Text; }

        else

        { dataEmployee.post_code = "-"; }

        if (txtMobilePhone.Text != null && txtMobilePhone.Text != "")

        { dataEmployee.emp_mobile_no = txtMobilePhone.Text; }

        else

        { dataEmployee.emp_mobile_no = ""; }

        if (txtHomePhone.Text != null && txtHomePhone.Text != "")

        { dataEmployee.emp_phone_no = txtHomePhone.Text; }

        else

        { dataEmployee.emp_phone_no = ""; }

        if (txtEmail.Text != null && txtEmail.Text != "")

        { dataEmployee.emp_email = txtEmail.Text; }

        else

        { dataEmployee.emp_email = "-"; }

        if (int.Parse(ddCostCenter.SelectedValue) != 0)

        { dataEmployee.costcenter_idx = int.Parse(ddCostCenter.SelectedValue); }

        else

        { dataEmployee.costcenter_idx = 0; }

        if (int.Parse(ddlApprover_Leave1.SelectedValue) != 0)

        {
            dataEmployee.emp_idx_approve1 = int.Parse(ddlApprover_Leave1.SelectedValue);
        }

        else

        { }

        if (int.Parse(ddlApprover_Leave2.SelectedValue) != 0)

        {
            dataEmployee.emp_idx_approve2 = int.Parse(ddlApprover_Leave2.SelectedValue);
        }

        else

        { }

        if (txtSarary.Text != null && txtSarary.Text != "")

        {
            dataEmployee.sarary = txtSarary.Text;
        }

        else

        { dataEmployee.sarary = "None"; }


        #region Healthlist
        //เพิ่มสุขภาพ
        if (txtScar.Text != null && txtScar.Text != "")

        { Healthlist.Scar = txtScar.Text; }

        else

        { Healthlist.Scar = "-"; }

        if (txtHeight.Text != null && txtHeight.Text != "")

        { Healthlist.Height = int.Parse(txtHeight.Text); }

        else

        { Healthlist.Height = 0; }

        if (txtWeight.Text != null && txtWeight.Text != "")

        { Healthlist.Weight = int.Parse(txtWeight.Text); }

        else

        { Healthlist.Weight = 0; }

        if (ddlBloodgroup.SelectedValue != "0")

        { Healthlist.BTypeIDX = int.Parse(ddlBloodgroup.SelectedValue); }

        else

        { Healthlist.BTypeIDX = 0; }

        _data_employee.BoxEmployeeHealthlist[0] = Healthlist;

        #endregion

        #region reference_persons
        //บุคคลอ้างอิง
        if (txt_mobile_number.Text != null && txt_mobile_number.Text != "")

        { reference_persons.mobile_number = txt_mobile_number.Text; }

        else

        { reference_persons.mobile_number = ""; }

        if (txt_fullname_reference.Text != null && txt_fullname_reference.Text != "")

        { reference_persons.fullname_reference = txt_fullname_reference.Text; }

        else

        { reference_persons.fullname_reference = "-"; }

        if (txt_relationship.Text != null && txt_relationship.Text != "")

        { reference_persons.relationship = txt_relationship.Text; }

        else

        { reference_persons.relationship = "-"; }

        if (txt_address_reference.Text != null && txt_address_reference.Text != "")

        { reference_persons.address_reference = txt_address_reference.Text; }

        else

        { reference_persons.address_reference = "-"; }

        #endregion

        #region Education

        //เพิ่มการศึกษา
        if (txtmajor.Text != null && txtmajor.Text != "")

        { Education.Major = txtmajor.Text; }

        else

        { Education.Major = "-"; }

        if (txtUniversity.Text != null && txtUniversity.Text != "")

        { Education.University = txtUniversity.Text; }

        else

        { Education.University = "-"; }

        if (ddlFaculty.SelectedValue != "0")

        { Education.Faculty = int.Parse(ddlFaculty.SelectedValue); }

        else

        { Education.Faculty = 0; }

        if (txtGPA.Text != null && txtGPA.Text != "")

        { Education.GPA = txtGPA.Text; }

        else

        { Education.GPA = "-"; }

        if (txtgraduation_year.Text != null && txtgraduation_year.Text != "")

        { Education.academic_year = txtgraduation_year.Text; }

        else
        { Education.academic_year = "-"; }

        if (ddlEducation.SelectedValue != "0")

        { Education.Eduidx = int.Parse(ddlEducation.SelectedValue); }

        else

        { Education.Eduidx = 0; }



        if (chk_nothave_Experience.Checked)
        {
            Education.exper_idx = 0;
        }

        else if (chk_have_Experience.Checked)
        {
            ViewState["Have_Experience"] = 1;
            Education.exper_idx = int.Parse(ViewState["Have_Experience"].ToString());
        }
        #endregion

        #region if Nationality = 177
        else if (int.Parse(ddlNationality.SelectedValue) == 177)
        {

            //เพิ่มข้อมูลพนักงานไทย

            if (ddMilitary.SelectedValue != "0")

            { EmployeeTH.MilIDX = int.Parse(ddMilitary.SelectedValue); }

            else

            { EmployeeTH.MilIDX = 0; }

            if (txtIDCard.Text != null && txtIDCard.Text != "")

            { EmployeeTH.IdentityCard = txtIDCard.Text; }

            else

            { EmployeeTH.IdentityCard = "-"; }

            if (txtIIssuedAtIDCard.Text != null && txtIIssuedAtIDCard.Text != "")

            { EmployeeTH.IssuedAt = txtIIssuedAtIDCard.Text; }

            else

            { EmployeeTH.IssuedAt = "None"; }

            if (txtDateExpIDCard.Text != null && txtDateExpIDCard.Text != "")

            { EmployeeTH.IDateExpired = txtDateExpIDCard.Text; }

            else

            { EmployeeTH.IDateExpired = "01-01-1900"; }

            CheckBox chkAddress = (CheckBox)fvAddDataEmployee.FindControl("chkAddress");
            CheckBox chkAddress1 = (CheckBox)fvAddDataEmployee.FindControl("chkAddress1");



            //if (ddProvince2.SelectedItem.Value != "0")

            //{ EmployeeTH.BProvIDX = int.Parse(ddProvince2.SelectedItem.Value); }

            //else

            //{ EmployeeTH.BProvIDX = 0; }

            //if (txtPermanentaddress.Text != null && txtPermanentaddress.Text != "")

            //{ EmployeeTH.EmpAddress = txtPermanentaddress.Text; }

            //else

            //{ EmployeeTH.EmpAddress = "-"; }

            //if (ddDistrict2.SelectedItem.Value != "0")

            //{ EmployeeTH.DistIDX = int.Parse(ddDistrict2.SelectedItem.Value); }

            //else

            //{ EmployeeTH.DistIDX = 0; }

            //if (ddAmphoe2.SelectedItem.Value != "0")

            //{ EmployeeTH.AmpIDX = int.Parse(ddAmphoe2.SelectedItem.Value); }

            //else

            //{ EmployeeTH.AmpIDX = 0; }

            //if (ddProvince2.SelectedItem.Value != "0")

            //{ EmployeeTH.ProvIDX = int.Parse(ddProvince2.SelectedItem.Value); }

            //else

            //{ EmployeeTH.ProvIDX = 0; }

            //if (txtZipcode2.Text != null && txtZipcode2.Text != "")

            //{ EmployeeTH.PostCode = txtZipcode2.Text; }

            //else

            //{ EmployeeTH.PostCode = "-"; }

            //if (ddCountry2.SelectedItem.Value != "0")

            //{ EmployeeTH.CountryIDX = int.Parse(ddCountry2.SelectedItem.Value); }

            //else

            //{ EmployeeTH.CountryIDX = 0; }

            //if (txtHomePhone2.Text != null && txtHomePhone2.Text != "")

            //{ EmployeeTH.PhoneNo = txtHomePhone2.Text; }

            //else

            //{ EmployeeTH.PhoneNo = "-"; }

            //}

            if (ddHospital.SelectedValue != "0")

            { EmployeeTH.SocHosIDX = int.Parse(ddHospital.SelectedValue); }

            else

            { EmployeeTH.SocHosIDX = 0; }

            if (txtSocial.Text != null && txtSocial.Text != "")

            { EmployeeTH.SocNo = txtSocial.Text; }

            else

            { EmployeeTH.SocNo = "-"; }

            if (txtSocialDateExp.Text != null && txtSocialDateExp.Text != "")

            { EmployeeTH.SocDateExpired = txtSocialDateExp.Text; }

            else

            { EmployeeTH.SocDateExpired = "-"; }

            EmployeeTH.SocStatus = 1;
        }
        #endregion

        #region if Nationality != 177
        else if (int.Parse(ddlNationality.SelectedValue) != 177)
        {
            //เพิ่มข้อมูลพนักงานต่างชาติ

            if (txtPermanentaddressEN.Text != null && txtPermanentaddressEN.Text != "")

            { EmployeeEN.EmpAddress = txtPermanentaddressEN.Text; }

            else

            { EmployeeEN.EmpAddress = "-"; }

            if (ddlCountryEN.SelectedValue != "0")

            { EmployeeEN.CountryIDX = int.Parse(ddlCountryEN.SelectedValue); }

            else

            { EmployeeEN.CountryIDX = 0; }

            if (txtPhoneEN.Text != null && txtPhoneEN.Text != "")

            { EmployeeEN.PhoneNo = txtPhoneEN.Text; }

            else

            { EmployeeEN.PhoneNo = "-"; }

            if (txt_Passport_Issued_No.Text != null && txt_Passport_Issued_No.Text != "")

            { EmployeeEN.VisaIDX = (txt_Passport_Issued_No.Text); }  //เลขที่หนังสือเดินทาง 

            else

            { EmployeeEN.VisaIDX = "-"; }

            if (txt_Issued_Date.Text != null && txt_Issued_Date.Text != "")

            { EmployeeEN.TrDateIssued = (txt_Issued_Date.Text); }   //วันที่ออกหนังสือเดินทาง

            else

            { EmployeeEN.TrDateIssued = "01-01-1900"; }

            if (txt_ExpDate.Text != null && txt_ExpDate.Text != "")

            { EmployeeEN.TrExpired = (txt_ExpDate.Text); } //วันหมดอายุหนังสือเดินทาง

            else

            { EmployeeEN.TrExpired = "01-01-1900"; }

            if (txt_perIssued_No.Text != null && txt_perIssued_No.Text != "")

            { EmployeeEN.WPermitNo = (txt_perIssued_No.Text); }  //เลขที่ใบอนุญาตทำงาน

            else

            { EmployeeEN.WPermitNo = "-"; }

            if (txt_perIssued_date.Text != null && txt_perIssued_date.Text != "")

            { EmployeeEN.WDateIssued = (txt_perIssued_date.Text); } //วันที่ออกใบอนุญาตทำงาน

            else

            { EmployeeEN.WDateIssued = "01-01-1900"; }

            if (txt_PassportIssued.Text != null && txt_PassportIssued.Text != "")

            { EmployeeEN.Passport_Issued = (txt_PassportIssued.Text); }  //สถานที่ออกหนังสือเดินทาง

            else

            { EmployeeEN.Passport_Issued = "-"; }

            if (txt_perIssued_at.Text != null && txt_perIssued_at.Text != "")

            { EmployeeEN.WPermit_Issued = (txt_perIssued_at.Text); }  //สถานที่ออกใบอนุญาตทำงาน

            else

            { EmployeeEN.WPermit_Issued = "-"; }

            if (txt_perExpire_date.Text != null && txt_perExpire_date.Text != "")

            { EmployeeEN.WDateExpired = (txt_perExpire_date.Text); }  //วันหมดอายุใบอนุญาตทำงาน

            else

            { EmployeeEN.WDateExpired = "01-01-1900"; }


            if (ddHospital.SelectedValue != "0")

            { EmployeeTH.SocHosIDX = int.Parse(ddHospital.SelectedValue); }

            else

            { EmployeeTH.SocHosIDX = 0; }

            if (txtSocial.Text != null && txtSocial.Text != "")

            { EmployeeTH.SocNo = txtSocial.Text; }

            else

            { EmployeeTH.SocNo = "-"; }

            if (txtSocialDateExp.Text != null && txtSocialDateExp.Text != "")

            { EmployeeTH.SocDateExpired = txtSocialDateExp.Text; }

            else

            { EmployeeTH.SocDateExpired = "-"; }

            EmployeeTH.SocStatus = 1;
        }
        #endregion

        if (chk_sameAddress_present.Checked)
        {
            EmployeeTH.EmpAddress = txtPresentAddress.Text;
            EmployeeTH.DistIDX = int.Parse(ddDistrict.SelectedValue);
            EmployeeTH.AmpIDX = int.Parse(ddAmphoe.SelectedValue);
            EmployeeTH.ProvIDX = int.Parse(ddProvince.SelectedValue);
            EmployeeTH.PostCode = txtZipCode.Text;
            EmployeeTH.CountryIDX = int.Parse(ddCountry.SelectedValue);
            EmployeeTH.PhoneNo = txtHomePhone.Text;
        }
        else if (chk_notSameAddress_present.Checked)
        {
            EmployeeTH.BProvIDX = int.Parse(ddProvince2.SelectedItem.Value);
            EmployeeTH.EmpAddress = txtPermanentaddress.Text;
            EmployeeTH.DistIDX = int.Parse(ddDistrict2.SelectedItem.Value);
            EmployeeTH.AmpIDX = int.Parse(ddAmphoe2.SelectedItem.Value);
            EmployeeTH.ProvIDX = int.Parse(ddProvince2.SelectedItem.Value);
            EmployeeTH.PostCode = txtZipcode2.Text;
            EmployeeTH.CountryIDX = int.Parse(ddCountry2.SelectedItem.Value);
            EmployeeTH.PhoneNo = txtHomePhone2.Text;
        }



        _data_employee.employee_list[0] = dataEmployee;
        _data_employee.BoxEmployeeTHlist[0] = EmployeeTH;

        _data_employee.BoxEmployeeFRlist[0] = EmployeeEN;
        _data_employee.EducationList[0] = Education;
        _data_employee.Reference_persons[0] = reference_persons;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee, 100);
        _data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        ViewState["return_emp_idx"] = _data_employee.return_code;
        ViewState["return_alert_msg"] = _data_employee.return_value;

        // insert_shiftTime_employee();

        GridView gvAddPosision = (GridView)fvAddDataEmployee.FindControl("gvAddPosision");

        if (ViewState["Create_DataPosision"].ToString() == "0")

        {
            data_employee dataposition = new data_employee();
            employee_detail add_position__ = new employee_detail();
            dataposition.employee_list = new employee_detail[1];

            if (int.Parse(ddPosition.SelectedValue) != 0)

            { add_position__.rpos_idx = int.Parse(ddPosition.SelectedValue); }

            else

            { add_position__.rpos_idx = 0; }

            add_position__.emp_idx = int.Parse(ViewState["return_emp_idx"].ToString());

            dataposition.employee_list[0] = add_position__;
            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", dataposition, 106);

        }
        else if (ViewState["Create_DataPosision"] != null)
        {

            var dsBuyequipment = (DataSet)ViewState["Create_DataPosision"];

            var AddPur = new BoxPosisionList[dsBuyequipment.Tables[0].Rows.Count];
            int i = 0;

            foreach (DataRow dr in dsBuyequipment.Tables[0].Rows)
            {
                AddPur[i] = new BoxPosisionList();
                AddPur[i].RPosIDX = int.Parse(dr["Position"].ToString());
                AddPur[i].EmpIDX_Pos = int.Parse(ViewState["return_emp_idx"].ToString());
                i++;
            }

            if (dsBuyequipment.Tables[0].Rows.Count == 0)
            {
                data_employee dataposition = new data_employee();
                employee_detail add_position__ = new employee_detail();
                dataposition.employee_list = new employee_detail[1];
                add_position__.emp_idx = int.Parse(ViewState["return_emp_idx"].ToString());
                add_position__.rpos_idx = int.Parse(ddPosition.SelectedValue);

                dataposition.employee_list[0] = add_position__;
                _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", dataposition, 106);
            }
            else
            {
                data_employee Box_Add_Position = new data_employee();
                Box_Add_Position.BoxPosision = AddPur;
                _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", Box_Add_Position, 101);
            }

            if (ViewState["checkPosision"] != null)
            {
                data_employee posision_main = new data_employee();
                employee_detail add_posision_main = new employee_detail();
                posision_main.employee_list = new employee_detail[1];
                add_posision_main.rpos_idx = int.Parse(ViewState["Position"].ToString());
                add_posision_main.emp_idx = int.Parse(ViewState["return_emp_idx"].ToString());

                posision_main.employee_list[0] = add_posision_main;
                _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", posision_main, 302);
            }
        }

        var formview_shiftTime = (FormView)fvAddDataEmployee.FindControl("formview_shiftTime");
        var checkstaticShift = (CheckBox)formview_shiftTime.FindControl("checkstaticShift");
        var checkDynamicShift = (CheckBox)formview_shiftTime.FindControl("checkDynamicShift");
        var ddlist_ShiftTime = (DropDownList)formview_shiftTime.FindControl("ddlist_ShiftTime");

        data_employee data_empShiftTime = new data_employee();
        ShiftTime insert_ShiftTime = new ShiftTime();
        data_empShiftTime.ShiftTime_details = new ShiftTime[1];


        if (checkstaticShift.Checked)
        {
            //กะคงที่(กำหนด Status)
            ViewState["checkShiftTime"] = 1;

        }
        else
        {
            ViewState["checkShiftTime"] = 0;

        }
        if (checkDynamicShift.Checked)
        {
            //กะหมุนเวียน(กำหนด Status)
            ViewState["checkShiftTime"] = 2;
        }

        else {

            ViewState["checkShiftTime"] = 0;

        }
      
    
        insert_ShiftTime.empShiftTime_idx = int.Parse(ViewState["return_emp_idx"].ToString());
        insert_ShiftTime.idxTypeShiftTime = int.Parse(ViewState["checkShiftTime"].ToString());
        insert_ShiftTime.shiftTime_idx = int.Parse(ddlist_ShiftTime.SelectedValue);

        data_empShiftTime.ShiftTime_details[0] = insert_ShiftTime;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", data_empShiftTime, 113);

        //  insert_shiftTime_employee();
        var ExperienceList = (DataSet)ViewState["Create_table_Experience"];

        var add_Experience = new boxExperienceList[ExperienceList.Tables[0].Rows.Count];
        int j = 0;

        foreach (DataRow drExperience in ExperienceList.Tables[0].Rows)
        {
            add_Experience[j] = new boxExperienceList();
            add_Experience[j].name_company = (drExperience["Name_Organiztion_Old"].ToString());
            add_Experience[j].position_old = (drExperience["Position_Old"].ToString());
            add_Experience[j].motive = (drExperience["Motive"].ToString());
            add_Experience[j].salary_old = (drExperience["Salary_Old"].ToString());
            add_Experience[j].job_brief_details = (drExperience["Job_Brief"].ToString());
            add_Experience[j].emp_idx_exper = int.Parse(ViewState["return_emp_idx"].ToString());
            add_Experience[j].workIn_old = (drExperience["date_Work_In_Old"].ToString());
            add_Experience[j].resign_old = (drExperience["date_resign_Old"].ToString());

            j++;
        }

        if (ExperienceList.Tables[0].Rows.Count == 0)
        {

        }
        else
        {
            data_presonal boxExperience_add = new data_presonal();
            boxExperience_add.boxExperience = add_Experience;
         //   _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_presonal", "Manage_Employee", boxExperience_add, 111);
        }

        select_empIdx_last_insert();
        string Path = ConfigurationSettings.AppSettings["upload_profile_picture"];

        if (Directory.Exists(Server.MapPath(Path + ViewState["emp_idx_last"].ToString())))
        {

            HttpFileCollection hfcit3 = Request.Files;
            for (int bb = 0; bb < 1; bb++)
            {

                string fileName2 = ViewState["emp_idx_last"].ToString();
                string fileName22 = fileName2;

                string oldFilePath = ConfigurationSettings.AppSettings["upload_profile_picture"] + ViewState["emp_idx_last"].ToString();



                string fileName3 = ViewState["emp_code_new"].ToString();
                string fileName33 = fileName3;

                string newFilePath = ConfigurationSettings.AppSettings["upload_profile_picture"] + ViewState["emp_code_new"].ToString();


                if (oldFilePath != newFilePath)
                {
                    Directory.CreateDirectory(Server.MapPath(newFilePath));
                    File.Move(Server.MapPath(oldFilePath + "/" + fileName22 + ".jpg"), Server.MapPath(newFilePath + "/" + fileName33 + ".jpg"));
                    Directory.Delete(Server.MapPath(oldFilePath));
                }
            }
        }
        else
        { }

        if (ViewState["return_alert_msg"].ToString() == "01")
        {
            Response.Redirect("~/alertsuccess");
        }
        if (ViewState["return_alert_msg"].ToString() == "02")
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('!! ผิดพลาด บันทึกข้อมูลไม่สำเร็จ');", true);
        }
    }

    #endregion

    #region insert shift Time
    protected void insert_shiftTime_employee()

    {
        var formview_shiftTime = (FormView)fvAddDataEmployee.FindControl("formview_shiftTime");
        var checkstaticShift = (CheckBox)formview_shiftTime.FindControl("checkstaticShift");
        var checkDynamicShift = (CheckBox)formview_shiftTime.FindControl("checkDynamicShift");
        var ddlist_ShiftTime = (DropDownList)formview_shiftTime.FindControl("ddlist_ShiftTime");

        data_employee data_empShiftTime = new data_employee();
        ShiftTime insert_ShiftTime = new ShiftTime();
        data_empShiftTime.ShiftTime_details = new ShiftTime[1];

        if (checkstaticShift.Checked)
        {
            //กะคงที่(กำหนด Status)
            ViewState["checkShiftTime"] = 1;

        }
        else if (checkDynamicShift.Checked)
        {
            //กะหมุนเวียน(กำหนด Status)
            ViewState["checkShiftTime"] = 2;
        }

        insert_ShiftTime.empShiftTime_idx = (int)ViewState["return_emp_idx"];
        insert_ShiftTime.idxTypeShiftTime = int.Parse(ViewState["checkShiftTime"].ToString());
        insert_ShiftTime.shiftTime_idx = int.Parse(ddlist_ShiftTime.SelectedValue);

        data_empShiftTime.ShiftTime_details[0] = insert_ShiftTime;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", data_empShiftTime, 113);




    }
    #endregion

    #region Class SelectData 

    protected void SelectEmp()
    {
        data_employee emp = new data_employee();
        select_emp_list __select_emp = new select_emp_list();
        emp.select_emp = new select_emp_list[1];

        emp.select_emp[0] = __select_emp;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", emp, 228);
        emp = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

    }

    protected void select_empIdx_present()
    {
        employee_detail DataEmployee = new employee_detail();
        _data_employee.employee_list = new employee_detail[1];

        _data_employee.employee_list[0] = DataEmployee;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee, 233);
        _data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        ViewState["emp_idx_last"] = _data_employee.employee_list[0].emp_idx_last;
        ViewState["emp_code"] = _data_employee.employee_list[0].emp_code;

    }

    protected void select_empIdx_last_insert()
    {
        employee_detail DataEmployee = new employee_detail();
        _data_employee.employee_list = new employee_detail[1];

        _data_employee.employee_list[0] = DataEmployee;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee, 233);
        _data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        ViewState["emp_code_new"] = _data_employee.employee_list[0].emp_code;

    }

    protected void select_empIdx_part()
    {
        Repeater Repeater_picture = (Repeater)ViewAddDataEmployee.FindControl("Repeater_picture");
        employee_detail DataEmployee = new employee_detail();
        _data_employee.employee_list = new employee_detail[1];

        _data_employee.employee_list[0] = DataEmployee;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee, 233);
        _data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        Repeater_picture.DataSource = _data_employee.employee_list;
        Repeater_picture.DataBind();

    }

    protected void select_listemp_setshiftTime()
    {
        ShiftTime dataEmpShiftTime = new ShiftTime();
        _data_employee.ShiftTime_details = new ShiftTime[1];

        _data_employee.ShiftTime_details[0] = dataEmpShiftTime;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee, 247);
        _data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);



        setGridData(gvListNameShiftTime, _data_employee.ShiftTime_details);


    }

    protected void SelectData_Employee()
    {

        employee_detail DataEmployee = new employee_detail();
        _data_employee.employee_list = new employee_detail[1];

        _data_employee.employee_list[0] = DataEmployee;

        // _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee, 211);
        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee, 211);
        _data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        setGridData(gvEmployee, _data_employee.employee_list);

    }
    #endregion

    #region ClassUpdate 

    protected void delete_position()
    {


    }

    protected void Position_change()
    {
        Label LoEmpCode = (Label)fvEdit_dataEmployee.FindControl("LoEmpCode");
        ViewState["idx"] = LoEmpCode.Text;

        if (ViewState["checkPosision_remove"] != null)
        {
            data_employee data_emp = new data_employee();
            position_changeList Position_change = new position_changeList();
            data_emp.position_change = new position_changeList[1];
            Position_change.effecttive_date = AddStartdatea.Text;
            Position_change.id_EODSPIDX = int.Parse(ViewState["id"].ToString());
            Position_change.id_emps = int.Parse(ViewState["idx"].ToString());
            Position_change.rpos_id_new = int.Parse(ddlchangPosision.SelectedValue);
            Position_change.rpos_id_old = int.Parse(ViewState["Position_move"].ToString());
            Position_change.status_EStaOut = int.Parse(ViewState["status_Position_move"].ToString());
            data_emp.position_change[0] = Position_change;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", data_emp, 305);
            data_emp = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

            if (data_emp.return_code == "0")
            {

                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลสำเร็จ');", true);

            }
            else

            { ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลไม่สำเร็จ');", true); }
        }
        else
        {

        }

    }

    #endregion

    #region update Shift Time
    protected void update_shiftTime_employee()
    {
        var editformview_shiftTime = (FormView)fvEdit_dataEmployee.FindControl("editformview_shiftTime");
        var ddlist_edit_ShiftTime = (DropDownList)editformview_shiftTime.FindControl("ddlist_edit_ShiftTime");
        var Information_alertHaveShiftTime = (Repeater)editformview_shiftTime.FindControl("Information_alertHaveShiftTime");
        var checkstaticShift_edit = (RadioButton)editformview_shiftTime.FindControl("checkstaticShift_edit");
        var checkDynamicShift_edit = (RadioButton)editformview_shiftTime.FindControl("checkDynamicShift_edit");
        var txtidxTypeShiftTimer = (TextBox)Information_alertHaveShiftTime.Items[0].FindControl("txtidxTypeShiftTime");
        var txtshiftTime_idx = (TextBox)Information_alertHaveShiftTime.Items[0].FindControl("txtshiftTime_idx");

        ViewState["editcheckShiftTime"] = (txtidxTypeShiftTimer.Text);
        ViewState["shiftTime_value"] = (txtshiftTime_idx.Text);


        data_employee informationShift = new data_employee();
        ShiftTime editInformationShiftTime = new ShiftTime();
        informationShift.ShiftTime_details = new ShiftTime[1];

        if (checkstaticShift_edit.Checked)
        {
            //กะคงที่(กำหนด Status)
            ViewState["editcheckShiftTime"] = 1;

            editInformationShiftTime.empShiftTime_idx = (int)ViewState["emp_idx"];
            editInformationShiftTime.idxTypeShiftTime = int.Parse(ViewState["editcheckShiftTime"].ToString());
            editInformationShiftTime.shiftTime_idx = int.Parse(ddlist_edit_ShiftTime.SelectedValue);


        }
        else if (checkDynamicShift_edit.Checked)
        {
            //กะหมุนเวียน(กำหนด Status)
            ViewState["editcheckShiftTime"] = 2;

            editInformationShiftTime.empShiftTime_idx = (int)ViewState["emp_idx"];
            editInformationShiftTime.idxTypeShiftTime = int.Parse(ViewState["editcheckShiftTime"].ToString());
            editInformationShiftTime.shiftTime_idx = int.Parse(ddlist_edit_ShiftTime.SelectedValue);

        }
        else if (checkstaticShift_edit.Checked == false && checkDynamicShift_edit.Checked == false)

        {
            //  ViewState["editcheckShiftTime"] = int.Parse(txtidxTypeShiftTime.Text);
            editInformationShiftTime.empShiftTime_idx = (int)ViewState["emp_idx"];
            editInformationShiftTime.idxTypeShiftTime = int.Parse(ViewState["editcheckShiftTime"].ToString());
            editInformationShiftTime.shiftTime_idx = int.Parse(ViewState["shiftTime_value"].ToString());

        }


        //editInformationShiftTime.empShiftTime_idx = (int)ViewState["emp_idx"];
        //editInformationShiftTime.idxTypeShiftTime = int.Parse(ViewState["editcheckShiftTime"].ToString());
        //editInformationShiftTime.shiftTime_idx = int.Parse(ddlist_edit_ShiftTime.SelectedValue);

        informationShift.ShiftTime_details[0] = editInformationShiftTime;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", informationShift, 307);

    }
    #endregion

    #region class edit data employee
    protected void update_dataEmployee()
    {
        #region ประกาศตัวแปร

        var txtEmpCode_new_Edit = (TextBox)fvEdit_dataEmployee.FindControl("txtEmpCode_new_Edit");
        var txtNameTH_Edit = (TextBox)fvEdit_dataEmployee.FindControl("txtNameTH_Edit");
        var txtNameEN_Edit = (TextBox)fvEdit_dataEmployee.FindControl("txtNameEN_Edit");
        var txtLastnameTH_Edit = (TextBox)fvEdit_dataEmployee.FindControl("txtLastnameTH_Edit");
        var txtLastnameEN_Edit = (TextBox)fvEdit_dataEmployee.FindControl("txtLastnameEN_Edit");
        var txtNickNameEN_Edit = (TextBox)fvEdit_dataEmployee.FindControl("txtNickNameEN_Edit");
        var txtNickNameTH_Edit = (TextBox)fvEdit_dataEmployee.FindControl("txtNickNameTH_Edit");
        var txtMobilePhone_Edit = (TextBox)fvEdit_dataEmployee.FindControl("txtMobilePhone_Edit");
        var txtHomePhone_Edit = (TextBox)fvEdit_dataEmployee.FindControl("txtHomePhone_Edit");
        var txtEmail_Edit = (TextBox)fvEdit_dataEmployee.FindControl("txtEmail_Edit");
        var txtPresentAddress_Edit = (TextBox)fvEdit_dataEmployee.FindControl("txtPresentAddress_Edit");
        var txtZipCode_Edit = (TextBox)fvEdit_dataEmployee.FindControl("txtZipCode_Edit");
        var txtEmpin_Edit = (TextBox)fvEdit_dataEmployee.FindControl("txtEmpin_Edit");
        var txtBirthday_Edit = (TextBox)fvEdit_dataEmployee.FindControl("txtBirthday_Edit");
        var LoEmpCode = (Label)fvEdit_dataEmployee.FindControl("LoEmpCode");
        ViewState["empid"] = LoEmpCode.Text;

        var ddEmpType_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddEmpType_Edit");
        var ddlApprover_Leave1_edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddlApprover_Leave1_edit");
        var ddlApprover_Leave2_edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddlApprover_Leave2_edit");
        var ddPrefixTH_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddPrefixTH_Edit");
        var ddPrefixEN_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddPrefixEN_Edit");
        var ddlistSex_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddlistSex_Edit");
        var ddNationality_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddNationality_Edit");
        var ddRace_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddRace_Edit");
        var ddReligion_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddReligion_Edit");
        var ddDistrict_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddDistrict_Edit");
        var ddAmphoe_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddAmphoe_Edit");
        var ddProvince_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddProvince_Edit");
        var ddCountry_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddCountry_Edit");

        //สถานะพนักงาน
        var chk_probation = (CheckBox)fvEdit_dataEmployee.FindControl("chk_probation");
        var chk_pass_probation = (CheckBox)fvEdit_dataEmployee.FindControl("chk_pass_probation");
        var chk_termination = (CheckBox)fvEdit_dataEmployee.FindControl("chk_termination");
        var txt_status_employee = (TextBox)fvEdit_dataEmployee.FindControl("txt_status_employee");

        //พนักงานไทย
        var ddMilitary_Edit = (DropDownList)fvEdit_dataEmployeeTHai.FindControl("ddMilitary_Edit");
        var ddCountry_Permanent_Edit = (DropDownList)fvEdit_dataEmployeeTHai.FindControl("ddCountry_Permanent_Edit");
        var ddProvince_Permanent_Edit = (DropDownList)fvEdit_dataEmployeeTHai.FindControl("ddProvince_Permanent_Edit");
        var ddAmphoe_Permanent_Edit = (DropDownList)fvEdit_dataEmployeeTHai.FindControl("ddAmphoe_Permanent_Edit");
        var ddDistrict_Permanent_Edit = (DropDownList)fvEdit_dataEmployeeTHai.FindControl("ddDistrict_Permanent_Edit");
        var ddHospital_Edit = (DropDownList)fvEdit_dataEmployeeTHai.FindControl("ddHospital_Edit");

        var txtIDCard_Edit = (TextBox)fvEdit_dataEmployeeTHai.FindControl("txtIDCard_Edit");
        var txtIIssuedAtIDCard_Edit = (TextBox)fvEdit_dataEmployeeTHai.FindControl("txtIIssuedAtIDCard_Edit");
        var txtPermanentaddresst_Edit = (TextBox)fvEdit_dataEmployeeTHai.FindControl("txtPermanentaddresst_Edit");
        var txtZipcode_Permanent_Edit = (TextBox)fvEdit_dataEmployeeTHai.FindControl("txtZipcode_Permanent_Edit");
        var txtHomePhone_Permanent_Edit = (TextBox)fvEdit_dataEmployeeTHai.FindControl("txtHomePhone_Permanent_Edit");
        var txtSocial_Edit = (TextBox)fvEdit_dataEmployeeTHai.FindControl("txtSocial_Edit");

        //สุขภาพ
        var txtHeight_Edit = (TextBox)fv_edit_Health.FindControl("txtHeight_Edit");
        var txtWeight_Edit = (TextBox)fv_edit_Health.FindControl("txtWeight_Edit");
        var txtScar_Edit = (TextBox)fv_edit_Health.FindControl("txtScar_Edit");

        var ddBloodgroup_Edit = (DropDownList)fv_edit_Health.FindControl("ddBloodgroup_Edit");

        //ข้อมูลการศึกษา
        var ddlEducation_Edit = (DropDownList)fv_edit_Education.FindControl("ddlEducation_Edit");
        var ddlFaculty_Edit = (DropDownList)fv_edit_Education.FindControl("ddlFaculty_Edit");

        var txt_edit_graduation = (TextBox)fv_edit_Education.FindControl("txt_edit_graduation");
        var txt_edit_GPA = (TextBox)fv_edit_Education.FindControl("txt_edit_GPA");
        var txt_major_edit = (TextBox)fv_edit_Education.FindControl("txt_major_edit");
        var txt_University_Edit = (TextBox)fv_edit_Education.FindControl("txt_University_Edit");

        //ข้อมูลบุคคลอ้างอิง
        var txt_fullname_reference_edit = (TextBox)fv_edit_reference_persons.FindControl("txt_fullname_reference_edit");
        var txt_relationship_edit = (TextBox)fv_edit_reference_persons.FindControl("txt_relationship_edit");
        var txt_address_reference_edit = (TextBox)fv_edit_reference_persons.FindControl("txt_address_reference_edit");
        var txt_mobile_number_edit = (TextBox)fv_edit_reference_persons.FindControl("txt_mobile_number_edit");

        //ข้อมูลพนักงานต่างชาติ
        var txtPermanentaddressEN_edit = (TextBox)fv_edit_employee_Foreing.FindControl("txtPermanentaddressEN_edit");
        var ddlCountryEN_edit = (DropDownList)fv_edit_employee_Foreing.FindControl("ddlCountryEN_edit");
        var txtPhoneEN_edit = (TextBox)fv_edit_employee_Foreing.FindControl("txtPhoneEN_edit");
        var txt_Passport_Issued_No_edit = (TextBox)fv_edit_employee_Foreing.FindControl("txt_Passport_Issued_No_edit");
        var txt_PassportIssued_edit = (TextBox)fv_edit_employee_Foreing.FindControl("txt_PassportIssued_edit");
        var txt_Issued_Date_edit = (TextBox)fv_edit_employee_Foreing.FindControl("txt_Issued_Date_edit");
        var txt_ExpDate_edit = (TextBox)fv_edit_employee_Foreing.FindControl("txt_ExpDate_edit");
        var txt_perIssued_No_edit = (TextBox)fv_edit_employee_Foreing.FindControl("txt_perIssued_No_edit");
        var txt_perIssued_at_edit = (TextBox)fv_edit_employee_Foreing.FindControl("txt_perIssued_at_edit");
        var txt_perIssued_date_edit = (TextBox)fv_edit_employee_Foreing.FindControl("txt_perIssued_date_edit");
        var txt_perExpire_date_edit = (TextBox)fv_edit_employee_Foreing.FindControl("txt_perExpire_date_edit");

        #endregion

        #region edit data employee

        //ข้อมูลเบื้องต้นพนักงาน
        employee_detail edit_dataEmployee = new employee_detail();
        _data_employee.employee_list = new employee_detail[1];
        edit_dataEmployee.emp_idx = (int)ViewState["emp_idx"];
        edit_dataEmployee.emp_start_date = txtEmpin_Edit.Text;
        edit_dataEmployee.emp_code = txtEmpCode_new_Edit.Text;
        edit_dataEmployee.emp_firstname_th = txtNameTH_Edit.Text;
        edit_dataEmployee.emp_firstname_en = txtNameEN_Edit.Text;
        edit_dataEmployee.emp_lastname_th = txtLastnameTH_Edit.Text;
        edit_dataEmployee.emp_lastname_en = txtLastnameEN_Edit.Text;
        edit_dataEmployee.emp_nickname_en = txtNickNameEN_Edit.Text;
        edit_dataEmployee.emp_nickname_th = txtNickNameTH_Edit.Text;
        edit_dataEmployee.emp_address = txtPresentAddress_Edit.Text;
        edit_dataEmployee.emp_mobile_no = txtMobilePhone_Edit.Text;
        edit_dataEmployee.emp_phone_no = txtHomePhone_Edit.Text;
        edit_dataEmployee.post_code = txtZipCode_Edit.Text;
        edit_dataEmployee.emp_email = txtEmail_Edit.Text;
        edit_dataEmployee.emp_birthday = txtBirthday_Edit.Text;


        edit_dataEmployee.emp_type_idx = int.Parse(ddEmpType_Edit.SelectedValue);
        edit_dataEmployee.emp_idx_approve1 = int.Parse(ddlApprover_Leave1_edit.SelectedValue);
        edit_dataEmployee.emp_idx_approve2 = int.Parse(ddlApprover_Leave2_edit.SelectedValue);
        edit_dataEmployee.prefix_idx = int.Parse(ddPrefixTH_Edit.SelectedValue);
        edit_dataEmployee.prefix_idx = int.Parse(ddPrefixEN_Edit.SelectedValue);
        edit_dataEmployee.sex_idx = int.Parse(ddlistSex_Edit.SelectedValue);
        edit_dataEmployee.nat_idx = int.Parse(ddNationality_Edit.SelectedValue);
        edit_dataEmployee.race_idx = int.Parse(ddRace_Edit.SelectedValue);
        edit_dataEmployee.rel_idx = int.Parse(ddReligion_Edit.SelectedValue);
        edit_dataEmployee.country_idx = int.Parse(ddCountry_Edit.SelectedValue);
        edit_dataEmployee.prov_idx = int.Parse(ddProvince_Edit.SelectedValue);
        edit_dataEmployee.amp_idx = int.Parse(ddAmphoe_Edit.SelectedValue);
        edit_dataEmployee.dist_idx = int.Parse(ddDistrict_Edit.SelectedValue);

        if (chk_pass_probation.Checked)
        {
            var box_probation_date = (Panel)fvEdit_dataEmployee.FindControl("box_probation_date");
            var txt_probation_edit = (TextBox)box_probation_date.FindControl("txt_probation_edit");

            edit_dataEmployee.emp_status = 1;
            edit_dataEmployee.emp_probation_date = txt_probation_edit.Text;

        }
        else if (chk_probation.Checked)
        {
            edit_dataEmployee.emp_status = 2;
        }
        else if (chk_termination.Checked)
        {
            var box_add_end_date = (Panel)fvEdit_dataEmployee.FindControl("box_add_end_date");
            var txt_emp_end_date_add = (TextBox)box_add_end_date.FindControl("txt_emp_end_date_add");

            edit_dataEmployee.emp_status = 9;
            edit_dataEmployee.emp_probation_date = txt_emp_end_date_add.Text;
        }

        //ข้อมูลพนักงานไทย
        EmployeeTHlist edit_employee_thai = new EmployeeTHlist();
        _data_employee.BoxEmployeeTHlist = new EmployeeTHlist[1];

        if (int.Parse(ddNationality_Edit.SelectedValue) == 177)
        {
            edit_employee_thai.emp_idx_th = (int)ViewState["emp_idx"];
            edit_employee_thai.MilIDX = int.Parse(ddMilitary_Edit.SelectedValue);
            edit_employee_thai.CountryIDX = int.Parse(ddCountry_Permanent_Edit.SelectedValue);
            edit_employee_thai.ProvIDX = int.Parse(ddProvince_Permanent_Edit.SelectedValue);
            edit_employee_thai.AmpIDX = int.Parse(ddAmphoe_Permanent_Edit.SelectedValue);
            edit_employee_thai.DistIDX = int.Parse(ddDistrict_Permanent_Edit.SelectedValue);

            edit_employee_thai.IdentityCard = txtIDCard_Edit.Text;
            edit_employee_thai.IssuedAt = txtIIssuedAtIDCard_Edit.Text;
            edit_employee_thai.EmpAddress = txtPermanentaddresst_Edit.Text;
            edit_employee_thai.PostCode = txtZipcode_Permanent_Edit.Text;
            edit_employee_thai.PhoneNo = txtHomePhone_Permanent_Edit.Text;

            /*-------------ประกันสังคม---------------------*/
            edit_employee_thai.SocNo = txtSocial_Edit.Text;
            edit_employee_thai.SocHosIDX = int.Parse(ddHospital_Edit.SelectedValue);
        }
        //ข้อมูลพนักงานต่างชาติ

        else if (int.Parse(ddNationality_Edit.SelectedValue) != 177)
        {
            _data_employee.BoxEmployeeFRlist = new EmployeeFRlist[1];
            EmployeeFRlist edit_employee_Foreing = new EmployeeFRlist();

            edit_employee_Foreing.EmpIDX = (int)ViewState["emp_idx"];
            edit_employee_Foreing.EmpAddress = txtPermanentaddressEN_edit.Text;
            edit_employee_Foreing.CountryIDX = int.Parse(ddlCountryEN_edit.SelectedValue);
            edit_employee_Foreing.PhoneNo = txtPhoneEN_edit.Text;
            edit_employee_Foreing.TrDateIssued = txt_Issued_Date_edit.Text;
            edit_employee_Foreing.VisaIDX = txt_PassportIssued_edit.Text;
            edit_employee_Foreing.Passport_Issued = txt_Passport_Issued_No_edit.Text;
            edit_employee_Foreing.TrExpired = txt_ExpDate_edit.Text;
            edit_employee_Foreing.WPermitNo = txt_perIssued_No_edit.Text;
            edit_employee_Foreing.WPermit_Issued = txt_perIssued_at_edit.Text;
            edit_employee_Foreing.WDateIssued = txt_perIssued_date_edit.Text;
            edit_employee_Foreing.WDateExpired = txt_perExpire_date_edit.Text;

            _data_employee.BoxEmployeeFRlist[0] = edit_employee_Foreing;
        }

        //สุขภาพ
        EmployeeHealthlist edit_employee_health = new EmployeeHealthlist();
        _data_employee.BoxEmployeeHealthlist = new EmployeeHealthlist[1];

        edit_employee_health.EmpIDX = (int)ViewState["emp_idx"];
        edit_employee_health.Height = int.Parse(txtHeight_Edit.Text);
        edit_employee_health.Weight = int.Parse(txtWeight_Edit.Text);
        edit_employee_health.Scar = txtScar_Edit.Text;
        edit_employee_health.BTypeIDX = int.Parse(ddBloodgroup_Edit.SelectedValue);

        /*-----------------ข้อมูลการศึกษา-----------------*/
        if (int.Parse(ViewState["Value"].ToString()) == 0)
        {
            var box_insert_education = (Panel)viewEdit_dataEmployee.FindControl("box_insert_education");
            var ddlEducation_insert = (DropDownList)box_insert_education.FindControl("ddlEducation_insert");
            var ddlFaculty_insert = (DropDownList)box_insert_education.FindControl("ddlFaculty_insert");
            var txt_insert_graduation = (TextBox)box_insert_education.FindControl("txt_insert_graduation");
            var txt_insert_GPA = (TextBox)box_insert_education.FindControl("txt_insert_GPA");
            var txt_major_insert = (TextBox)box_insert_education.FindControl("txt_major_insert");
            var txt_University_insert = (TextBox)box_insert_education.FindControl("txt_major_insert");

            data_employee data_education = new data_employee();
            DetailEducation add_eduction = new DetailEducation();
            data_education.EducationList = new DetailEducation[1];

            add_eduction.emp_idx_edu = int.Parse(ViewState["empid"].ToString());
            add_eduction.Major = txt_major_insert.Text;
            add_eduction.University = txt_University_insert.Text;
            add_eduction.Faculty = int.Parse(ddlFaculty_insert.SelectedValue);
            add_eduction.GPA = txt_insert_GPA.Text;
            add_eduction.academic_year = txt_insert_graduation.Text;
            add_eduction.Eduidx = int.Parse(ddlEducation_insert.SelectedValue);

            data_education.EducationList[0] = add_eduction;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", data_education, 108);

        }
        else if (int.Parse(ViewState["Value"].ToString()) != 0)
        {

            DetailEducation edit_employee_eduction = new DetailEducation();
            _data_employee.EducationList = new DetailEducation[1];

            edit_employee_eduction.emp_idx_edu = (int)ViewState["emp_idx"];
            edit_employee_eduction.Edu_name = ddlEducation_Edit.SelectedValue;
            edit_employee_eduction.academic_year = txt_edit_graduation.Text;
            edit_employee_eduction.GPA = txt_edit_GPA.Text;
            edit_employee_eduction.Major = txt_major_edit.Text;
            edit_employee_eduction.University = txt_University_Edit.Text;
            edit_employee_eduction.Faculty = int.Parse(ddlFaculty_Edit.SelectedValue);
            _data_employee.EducationList[0] = edit_employee_eduction;
        }

        /*-------------บุคคลอ้างอิง---------------------*/
        Reference_persons_list edit_reference_persons = new Reference_persons_list();
        _data_employee.Reference_persons = new Reference_persons_list[1];

        if (int.Parse(ViewState["uuuuu"].ToString()) == 0)
        {
            var txt_mobile_number_ = (TextBox)box_insert_reference_persons.FindControl("txt_mobile_number_");
            var txt_address_reference_ = (TextBox)box_insert_reference_persons.FindControl("txt_address_reference_");
            var txt_relationship_ = (TextBox)box_insert_reference_persons.FindControl("txt_relationship_");
            var txt_fullname_reference_ = (TextBox)box_insert_reference_persons.FindControl("txt_fullname_reference_");

            data_employee data_reference_persons = new data_employee();
            Reference_persons_list add_reference_persons = new Reference_persons_list();
            data_reference_persons.Reference_persons = new Reference_persons_list[1];

            if (txt_mobile_number_.Text == "" && txt_mobile_number_.Text == null)
            { add_reference_persons.mobile_number = "-"; }
            else
            { add_reference_persons.mobile_number = txt_mobile_number_.Text; }

            if (txt_address_reference_.Text == "" && txt_address_reference_.Text == null)
            { add_reference_persons.address_reference = "-"; }
            else
            { add_reference_persons.address_reference = txt_address_reference_.Text; }

            if (txt_relationship_.Text == "" && txt_relationship_.Text == null)
            { add_reference_persons.relationship = "-"; }
            else
            { add_reference_persons.relationship = txt_relationship_.Text; }

            if (txt_fullname_reference_.Text == "" && txt_fullname_reference_.Text == null)
            { add_reference_persons.fullname_reference = "-"; }
            else
            { add_reference_persons.fullname_reference = txt_fullname_reference_.Text; }

            add_reference_persons.emp_idx_reference = (int)ViewState["emp_idx"];

            data_reference_persons.Reference_persons[0] = add_reference_persons;
            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", data_reference_persons, 109);
        }
        else
        {

            edit_reference_persons.emp_idx_reference = (int)ViewState["emp_idx"];
            edit_reference_persons.mobile_number = txt_mobile_number_edit.Text;
            edit_reference_persons.address_reference = txt_address_reference_edit.Text;
            edit_reference_persons.relationship = txt_relationship_edit.Text;
            edit_reference_persons.fullname_reference = txt_fullname_reference_edit.Text;

        }
        _data_employee.employee_list[0] = edit_dataEmployee;
        _data_employee.BoxEmployeeTHlist[0] = edit_employee_thai;
        _data_employee.BoxEmployeeHealthlist[0] = edit_employee_health;
        _data_employee.Reference_persons[0] = edit_reference_persons;

        //check_XML.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(_data_employee));

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee, 301);
        _data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        ViewState["msg_update"] = _data_employee.return_value;

        if (_data_employee.return_value == "01")
        {
            var editformview_shiftTime = (FormView)fvEdit_dataEmployee.FindControl("editformview_shiftTime");
            var ddlist_edit_ShiftTime = (DropDownList)editformview_shiftTime.FindControl("ddlist_edit_ShiftTime");
            var checkstaticShift_edit = (RadioButton)editformview_shiftTime.FindControl("checkstaticShift_edit");
            var checkDynamicShift_edit = (RadioButton)editformview_shiftTime.FindControl("checkDynamicShift_edit");

            if (ViewState["returnCodeShiftTime"].ToString() == "0")
            {

                data_employee data_empShiftTime = new data_employee();
                ShiftTime insert_or_edit_ShiftTime = new ShiftTime();
                data_empShiftTime.ShiftTime_details = new ShiftTime[1];

                if (checkstaticShift_edit.Checked)
                {
                    //กะคงที่(กำหนด Status)
                    ViewState["checkShiftTime_edit"] = 1;

                }
                else if (checkDynamicShift_edit.Checked)
                {
                    //กะหมุนเวียน(กำหนด Status)
                    ViewState["checkShiftTime_edit"] = 2;
                }
                else if (checkstaticShift_edit.Checked == false && checkDynamicShift_edit.Checked == false)

                {
                    ViewState["checkShiftTime_edit"] = 0;
                }

                insert_or_edit_ShiftTime.empShiftTime_idx = (int)ViewState["emp_idx"];
                insert_or_edit_ShiftTime.idxTypeShiftTime = int.Parse(ViewState["checkShiftTime_edit"].ToString());
                insert_or_edit_ShiftTime.shiftTime_idx = int.Parse(ddlist_edit_ShiftTime.SelectedValue);

                data_empShiftTime.ShiftTime_details[0] = insert_or_edit_ShiftTime;

                _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", data_empShiftTime, 113);


                //insert_shiftTime_employee();
            }
            else if (ViewState["returnCodeShiftTime"].ToString() == "1")
            {
                update_shiftTime_employee();
            }
            //else if (checkDynamicShift_edit.Checked == false && checkstaticShift_edit.Checked == false)
            //{
            //    var idxTypeShiftTime = (TextBox)fvEdit_dataEmployee.FindControl("idxTypeShiftTime");
            //    var txtshiftTime_idx = (TextBox)fvEdit_dataEmployee.FindControl("txtshiftTime_idx");

            //    data_employee informationShift = new data_employee();
            //    ShiftTime editInformationShiftTime = new ShiftTime();
            //    informationShift.ShiftTime_details = new ShiftTime[1];

            //    editInformationShiftTime.empShiftTime_idx = (int)ViewState["emp_idx"];
            //    editInformationShiftTime.idxTypeShiftTime = int.Parse(idxTypeShiftTime.Text);//int.Parse(ViewState["editcheckShiftTime"].ToString());
            //    editInformationShiftTime.shiftTime_idx = int.Parse(txtshiftTime_idx.Text);

            //    informationShift.ShiftTime_details[0] = editInformationShiftTime;

            //    _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", informationShift, 307);

            //}

            ddlist_edit_ShiftTime.SelectedValue = "0";

            checkDynamicShift_edit.Checked = false;
            checkstaticShift_edit.Checked = false;

            Response.Redirect("~/alertsuccess");
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('!! แก้ไขข้อมูลไม่สำเร็จ');", true);
        }

        #endregion
    }
    #endregion

    #region ViewEmployeeEdit//select_dataEdit

    #region select ข้อมูลพนักงานออกมาแสดงเพื่อที่ะทำการแก้ไข (เฉพาะ ID นั้นๆ)
    protected void fv_Show_Edit_employee()
    {
        data_employee edit_employee_data = new data_employee();
        employee_detail Editdataemployee = new employee_detail();
        edit_employee_data.employee_list = new employee_detail[1];

        Editdataemployee.emp_idx = (int)ViewState["emp_idx"];
        edit_employee_data.employee_list[0] = Editdataemployee;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", edit_employee_data, 212);
        edit_employee_data = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        fvEdit_dataEmployee.ChangeMode(FormViewMode.Edit);
        fvEdit_dataEmployee.DataSource = edit_employee_data.employee_list;
        fvEdit_dataEmployee.DataBind();
    }

    protected void show_pic()
    {
        data_employee edit_employee_data = new data_employee();
        employee_detail Editdataemployee = new employee_detail();
        edit_employee_data.employee_list = new employee_detail[1];

        Editdataemployee.emp_idx = (int)ViewState["emp_idx"];
        edit_employee_data.employee_list[0] = Editdataemployee;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", edit_employee_data, 212);
        edit_employee_data = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        gvpicture.DataSource = edit_employee_data.employee_list;
        gvpicture.DataBind();

    }


    protected void gv_show_edit_position()
    {
        var gv_edit_position = (GridView)fvEdit_dataEmployee.FindControl("gv_edit_position");
        var showalert = (Panel)fvEdit_dataEmployee.FindControl("showalert");
        dataODSP_Relation _position_show = new dataODSP_Relation();
        Relation_ODSP edit_position = new Relation_ODSP();
        _position_show.detailRelation_ODSP = new Relation_ODSP[1];

        edit_position.emp_idx_position = (int)ViewState["emp_idx"];
        _position_show.detailRelation_ODSP[0] = edit_position;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", _position_show, 204);
        _position_show = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);
        ViewState["returnCode_"] = _position_show.ReturnCode;

        if (_position_show.ReturnCode == "0")

        {
            gv_edit_position.Visible = false;
            showalert.Visible = true;
            ViewState["value_positionEmty"] = 0;
        }

        else if ((_position_show.ReturnCode != "0"))
        {
            gv_edit_position.DataSource = _position_show.detailRelation_ODSP;
            gv_edit_position.DataBind();
            gv_edit_position.Visible = true;
            showalert.Visible = false;
            ViewState["value_positionEmty"] = 1;
        }

    }

    protected void select_informationEmpShift()
    {
        Repeater Information_empShift = (Repeater)VieweditSetShiftTime.FindControl("Information_empShift");

        data_employee edit_employee_data = new data_employee();
        employee_detail Editdataemployee = new employee_detail();
        edit_employee_data.employee_list = new employee_detail[1];

        Editdataemployee.emp_idx = (int)ViewState["empSetShiftTimeIDX"];
        edit_employee_data.employee_list[0] = Editdataemployee;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", edit_employee_data, 244);
        edit_employee_data = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        Information_empShift.DataSource = edit_employee_data.employee_list;
        Information_empShift.DataBind();
    }


    protected void select_history_ShiftTime()
    {
        var editformview_shiftTime = (FormView)fvEdit_dataEmployee.FindControl("editformview_shiftTime");
        Repeater Information_alertHaveShiftTime = (Repeater)editformview_shiftTime.FindControl("Information_alertHaveShiftTime");
        Label alertNoneShiftTime = (Label)editformview_shiftTime.FindControl("alertNoneShiftTime");
        Label alertHaveShiftTime = (Label)Information_alertHaveShiftTime.FindControl("alertHaveShiftTime");
        Label alertHaveShiftTimeDynamic = (Label)Information_alertHaveShiftTime.FindControl("alertHaveShiftTimeDynamic");
        TextBox txtidxTypeShiftTime = (TextBox)Information_alertHaveShiftTime.FindControl("txtidxTypeShiftTime");



        data_employee information_ShiftTime = new data_employee();
        ShiftTime history_ShiftTime = new ShiftTime();
        information_ShiftTime.ShiftTime_details = new ShiftTime[1];

        history_ShiftTime.empShiftTime_idx = (int)ViewState["emp_idx"];
        information_ShiftTime.ShiftTime_details[0] = history_ShiftTime;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", information_ShiftTime, 246);
        information_ShiftTime = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
         ViewState["returnCodeShiftTime"] = information_ShiftTime.return_idx;

        //   check_XML.Text = information_ShiftTime.return_idx;

        if (ViewState["returnCodeShiftTime"].ToString() == "0")
        {
            alertNoneShiftTime.Visible = true;
            Information_alertHaveShiftTime.Visible = false;

        }
        else 
        {


            Information_alertHaveShiftTime.DataSource = information_ShiftTime.ShiftTime_details;
            Information_alertHaveShiftTime.DataBind();
            Information_alertHaveShiftTime.Visible = true;
            alertNoneShiftTime.Visible = false;


        }

    }


    protected void gv_show_present_position()
    {

        var gv_edit_position = (GridView)fvEdit_dataEmployee.FindControl("gv_edit_position");
        var showalert = (Panel)fvEdit_dataEmployee.FindControl("showalert");
        dataODSP_Relation _position_show_present = new dataODSP_Relation();
        Relation_ODSP edit_position = new Relation_ODSP();
        _position_show_present.detailRelation_ODSP = new Relation_ODSP[1];

        edit_position.emp_idx_position = (int)ViewState["emp_idx"];
        _position_show_present.detailRelation_ODSP[0] = edit_position;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", _position_show_present, 204);
        _position_show_present = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);
        ViewState["returnCode_present_position"] = _position_show_present.ReturnCode;

        if (_position_show_present.ReturnCode == "0")

        {
            BoxRemovePosisionn.Visible = false;
            BoxEdit_Sarary.Visible = false;
            BoxPenaltyEmployee.Visible = false;
            showalert_position.Visible = true;

        }

        else if ((_position_show_present.ReturnCode == "1"))

        {
            gv_present_position.DataSource = _position_show_present.detailRelation_ODSP;
            gv_present_position.DataBind();
            showalert_position.Visible = false;

            BoxRemovePosisionn.Visible = true;
            BoxEdit_Sarary.Visible = false;
            BoxPenaltyEmployee.Visible = false;
            ddlchang_Organization(0, "0");
            ddlchangDepartment_action(1, "0");
            ddlchangSection_action(1, "0");
            ddlchangPosision_action(1, "0");
            show_history_transfer_position();
        }

    }

    protected void gv_show_edit_experience()
    {
        data_employee _experienc_show_ = new data_employee();
        experiencelist edit_experienc = new experiencelist();
        _experienc_show_.box_experience_ = new experiencelist[1];

        edit_experienc.emp_idx_ = (int)ViewState["emp_idx"];
        _experienc_show_.box_experience_[0] = edit_experienc;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _experienc_show_, 231);
        _experienc_show_ = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        ViewState["returnCode_experienc"] = _experienc_show_.return_code;

        if (_experienc_show_.return_code == "0")
        {
            gv_edit_Experience.DataSource = null;
            gv_edit_Experience.DataBind();
        }
        else
        {
            gv_edit_Experience.DataSource = _experienc_show_.box_experience_;
            gv_edit_Experience.DataBind();
        }
    }

    protected void show_history_transfer_position()
    {
        Repeater history_transfer_position = (Repeater)viewEdit_dataEmployee.FindControl("history_transfer_position");
        data_employee history_transfer_data = new data_employee();
        position_changeList transfer_position = new position_changeList();
        history_transfer_data.position_change = new position_changeList[1];

        transfer_position.id_emps = (int)ViewState["emp_idx"];
        history_transfer_data.position_change[0] = transfer_position;
        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", history_transfer_data, 234);
        history_transfer_data = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        history_transfer_position.DataSource = history_transfer_data.position_change;
        history_transfer_position.DataBind();
    }

    protected void fv_Show_Edit_Health()
    {
        data_employee Health = new data_employee();
        EmployeeHealthlist edit_Health = new EmployeeHealthlist();
        Health.BoxEmployeeHealthlist = new EmployeeHealthlist[1];

        edit_Health.EmpIDX = (int)ViewState["emp_idx"];
        Health.BoxEmployeeHealthlist[0] = edit_Health;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", Health, 224);
        Health = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        var returnCode_health = Health.return_code;

        if (Health.return_code == "0")

        {
            fv_edit_Health.ChangeMode(FormViewMode.Insert);
            fv_edit_Health.DataSource = Health.BoxEmployeeHealthlist;
            fv_edit_Health.DataBind();
        }
        else if (Health.return_code == "1")
        {

            fv_edit_Health.ChangeMode(FormViewMode.Edit);
            fv_edit_Health.DataSource = Health.BoxEmployeeHealthlist;
            fv_edit_Health.DataBind();

        }

    }

    protected void fv_Show_Edit_Reference_persons()
    {
        data_employee reference_persons = new data_employee();
        Reference_persons_list edit_reference_persons = new Reference_persons_list();
        reference_persons.Reference_persons = new Reference_persons_list[1];

        edit_reference_persons.emp_idx_reference = (int)ViewState["emp_idx"];
        reference_persons.Reference_persons[0] = edit_reference_persons;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", reference_persons, 230);
        reference_persons = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        ViewState["uuuuu"] = reference_persons.return_code;

        if ((reference_persons.return_code == "0"))
        {
            box_insert_reference_persons.Visible = true;
        }
        else if (reference_persons.return_code != "0")
        {
            fv_edit_reference_persons.ChangeMode(FormViewMode.Edit);
            fv_edit_reference_persons.DataSource = reference_persons.Reference_persons;
            fv_edit_reference_persons.DataBind();
        }
    }

    protected void fv_Show_Edit_Education()
    {

        data_employee Education = new data_employee();
        DetailEducation edit_Education = new DetailEducation();
        Education.EducationList = new DetailEducation[1];

        edit_Education.emp_idx_edu = (int)ViewState["emp_idx"];
        Education.EducationList[0] = edit_Education;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", Education, 225);
        Education = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        ViewState["Value"] = Education.return_code;
        if (Education.return_code == "0")
        {
            box_insert_education.Visible = true;
            ddlFaculty_insert_();
            ddlEducation_insert_();
        }
        else if (Education.return_code != "0")
        {
            fv_edit_Education.DataSource = Education.EducationList;
            fv_edit_Education.DataBind();
            Faculty_edit(0, "0");
            ddlEducation_edit__(0, "0");
        }

    }

    protected void fv_Show_Edit_employeeTHai()
    {

        data_employee edit_emp_th_data = new data_employee();
        EmployeeTHlist Edit_dataemployeeTH = new EmployeeTHlist();
        edit_emp_th_data.BoxEmployeeTHlist = new EmployeeTHlist[1];

        Edit_dataemployeeTH.emp_idx_th = int.Parse(ViewState["emp_idx"].ToString());
        edit_emp_th_data.BoxEmployeeTHlist[0] = Edit_dataemployeeTH;
        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", edit_emp_th_data, 218);
        edit_emp_th_data = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        ViewState["return_empTH"] = edit_emp_th_data.return_code;

        data_employee edit_emp_FR_data = new data_employee();
        EmployeeFRlist Edit_dataemployeeFR = new EmployeeFRlist();
        edit_emp_FR_data.BoxEmployeeFRlist = new EmployeeFRlist[1];

        Edit_dataemployeeFR.EmpIDX = int.Parse(ViewState["emp_idx"].ToString());
        edit_emp_FR_data.BoxEmployeeFRlist[0] = Edit_dataemployeeFR;
        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", edit_emp_FR_data, 235);
        edit_emp_FR_data = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        ViewState["return_empER"] = edit_emp_FR_data.return_code;

        if (edit_emp_th_data.return_code == "1" && edit_emp_FR_data.return_code == "0")
        {
            fvEdit_dataEmployeeTHai.ChangeMode(FormViewMode.Edit);
            fvEdit_dataEmployeeTHai.DataSource = edit_emp_th_data.BoxEmployeeTHlist;
            fvEdit_dataEmployeeTHai.DataBind();
            ddCountry_Permanent_Edit(0, "0");
            ddlProvince_Permanent_Edit(0, "0");

            var txt_District_Permanent_Edit = (TextBox)fvEdit_dataEmployeeTHai.FindControl("txt_District_Permanent_Edit");
            var txt_Amphoe_Permanent_Edit = (TextBox)fvEdit_dataEmployeeTHai.FindControl("txt_Amphoe_Permanent_Edit");

            if (txt_Amphoe_Permanent_Edit.Text != "0")
            { ddAmphoe_Permanent_Edit(0, "0"); }
            else if (txt_Amphoe_Permanent_Edit.Text == "0")
            { ddAmphoe_Permanent_Edit(1, "0"); }
            if (txt_District_Permanent_Edit.Text != "0")
            { ddDistrict_Permanent_Edit(0, "0"); }
            else if (txt_District_Permanent_Edit.Text == "0")
            { ddDistrict_Permanent_Edit(1, "0"); }

            ddlMilitary_Edit(0, "0");
            ddlHotpital_edit(0, "0");
        }
        else if (edit_emp_FR_data.return_code == "1" && edit_emp_th_data.return_code == "0")
        {
            fv_edit_employee_Foreing.ChangeMode(FormViewMode.Edit);
            fv_edit_employee_Foreing.DataSource = edit_emp_FR_data.BoxEmployeeFRlist;
            fv_edit_employee_Foreing.DataBind();
            ddlCountry_FR_Edit(0, "0");
        }

        else if (edit_emp_FR_data.return_code == "1" && edit_emp_th_data.return_code == "1")
        {
            fv_edit_employee_Foreing.ChangeMode(FormViewMode.Edit);
            fv_edit_employee_Foreing.DataSource = edit_emp_FR_data.BoxEmployeeFRlist;
            fv_edit_employee_Foreing.DataBind();
            ddlCountry_FR_Edit(0, "0");
        }
    }

    protected void Show_Edit_Sarary()
    {

        data_employee Sarary_ = new data_employee();
        BoxSararyList Edit_Sarary = new BoxSararyList();
        Sarary_.BoxSarary = new BoxSararyList[1];

        Edit_Sarary.emp_sarary_idx = int.Parse(ViewState["emp_idx"].ToString());
        Sarary_.BoxSarary[0] = Edit_Sarary;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", Sarary_, 221);
        Sarary_ = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
        ViewState["return_sarary"] = Sarary_.return_code;

        if (Sarary_.return_code == "0")
        {
            fvEdit_Sarary.ChangeMode(FormViewMode.Insert);
            fvEdit_Sarary.DataSource = Sarary_.BoxSarary;
            fvEdit_Sarary.DataBind();
            ViewState["insert_value"] = 0;

        }
        else if (Sarary_.return_code == "1")
        {

            fvEdit_Sarary.ChangeMode(FormViewMode.Edit);
            fvEdit_Sarary.DataSource = Sarary_.BoxSarary;
            fvEdit_Sarary.DataBind();

        }
    }

    protected void select_rsec_idx()
    {
        data_employee RsecIDX = new data_employee();
        employee_detail select_rsec = new employee_detail();
        RsecIDX.employee_list = new employee_detail[1];

        select_rsec.emp_idx = int.Parse(ViewState["emp_idx"].ToString());

        RsecIDX.employee_list[0] = select_rsec;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", RsecIDX, 240);
        RsecIDX = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
    }

    protected void History_Sarary()
    {
        var sarary_history = (Repeater)fvEdit_Sarary.FindControl("sarary_history");
        data_employee Sarary_ = new data_employee();
        BoxSararyList Edit_Sarary = new BoxSararyList();
        Sarary_.BoxSarary = new BoxSararyList[1];

        Edit_Sarary.emp_sarary_idx = int.Parse(ViewState["emp_idx"].ToString());
        Sarary_.BoxSarary[0] = Edit_Sarary;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", Sarary_, 223);
        Sarary_ = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        sarary_history.DataSource = Sarary_.BoxSarary;
        sarary_history.DataBind();
    }

    #endregion

    #endregion

    #region รวม Dropdownlist

    #region ddlHotpital
    protected void ddlHotpital()

    {
        DropDownList ddHospital = (DropDownList)fvAddDataEmployee.FindControl("ddHospital");
        ddHospital.AppendDataBoundItems = true;
        ddHospital.Items.Add(new ListItem("เลือกโรงพยาบาล", "00"));

        data_presonal _data_pre = new data_presonal();
        DetailsHospital Hotpital = new DetailsHospital();
        _data_pre.BoxHospitalList = new DetailsHospital[1];

        _data_pre.BoxHospitalList[0] = Hotpital;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_presonal", "Manage_Employee", _data_pre, 217);
        _data_pre = (data_presonal)_funcTool.convertXmlToObject(typeof(data_presonal), _local_xml);

        ddHospital.DataSource = _data_pre.BoxHospitalList;
        ddHospital.DataTextField = "Name";
        ddHospital.DataValueField = "HosIDX";
        ddHospital.DataBind();

    }

    protected void ddlHotpital_edit(int Type, string SelectValue)
    {
        // var fvEdit_dataEmployeeTHai = (FormView)fvEdit_dataEmployee.FindControl("fvEdit_dataEmployeeTHai");
        var lb_edit_hospital = (TextBox)fvEdit_dataEmployeeTHai.FindControl("lb_edit_hospital");
        DropDownList ddHospital_Edit = (DropDownList)fvEdit_dataEmployeeTHai.FindControl("ddHospital_Edit");

        if (Type == 0)
        {

            data_presonal _data_pre = new data_presonal();

            DetailsHospital Hotpital = new DetailsHospital();
            _data_pre.BoxHospitalList = new DetailsHospital[1];

            _data_pre.BoxHospitalList[0] = Hotpital;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_presonal", "Manage_Employee", _data_pre, 217);
            _data_pre = (data_presonal)_funcTool.convertXmlToObject(typeof(data_presonal), _local_xml);

            if (_data_pre.BoxHospitalList == null)
            {

                ddHospital_Edit.DataSource = _data_pre.BoxHospitalList;
                ddHospital_Edit.AppendDataBoundItems = true;
                ddHospital_Edit.Items.Add(new ListItem("เลือกโรงพยาบาล", "00"));
                ddHospital_Edit.DataTextField = "Name";
                ddHospital_Edit.DataValueField = "HosIDX";
                ddHospital_Edit.DataBind();
            }
            else
            {

                ddHospital_Edit.DataSource = _data_pre.BoxHospitalList;
                ddHospital_Edit.AppendDataBoundItems = true;
                ddHospital_Edit.Items.Add(new ListItem("เลือกโรงพยาบาล", "00"));
                ddHospital_Edit.DataTextField = "Name";
                ddHospital_Edit.DataValueField = "HosIDX";
                ddHospital_Edit.DataBind();
                ddHospital_Edit.SelectedValue = lb_edit_hospital.Text;


            }


        }
        else if (Type == 1)
        {
            ddHospital_Edit.DataBind();
            ddHospital_Edit.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
        }
    }

    #endregion

    #region selectJobgrade
    protected void selectJobgrade()
    {

        DropDownList ddlLavel = (DropDownList)fvAddDataEmployee.FindControl("ddlLavel");
        ddlLavel.AppendDataBoundItems = true;
        ddlLavel.Items.Add(new ListItem("เลือกระดับพนักงาน", "00"));

        employee_detail Jobgrade = new employee_detail();
        _data_employee.employee_list = new employee_detail[1];

        _data_employee.employee_list[0] = Jobgrade;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee, 213);
        _data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        ddlLavel.DataSource = _data_employee.employee_list;
        ddlLavel.DataTextField = "jobgrade_level";
        ddlLavel.DataValueField = "jobgrade_idx";
        ddlLavel.DataBind();
    }

    #endregion

    protected void penalty_history()
    {
        GridView gridviewHistory = (GridView)viewEdit_dataEmployee.FindControl("gridviewHistory");
        Label LoEmpCode = (Label)fvEdit_dataEmployee.FindControl("LoEmpCode");
        data_employee history_penalty_ = new data_employee();
        penalty_details Select_penalty_history = new penalty_details();
        history_penalty_.penalty_list = new penalty_details[1];
        Select_penalty_history.emp_idx_penalty = int.Parse(LoEmpCode.Text);//

        history_penalty_.penalty_list[0] = Select_penalty_history;
        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", history_penalty_, 226);
        history_penalty_ = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        gridviewHistory.DataSource = history_penalty_.penalty_list;
        gridviewHistory.DataBind();
    }


    #region ddRaceAction DataBind
    protected string ddRaceAction(int Type, string SelectValue)
    {

        var ddRace = (DropDownList)fvAddDataEmployee.FindControl("ddRace");

        ddRace.Items.Clear();

        if (Type == 0)
        {
            employee_detail Race = new employee_detail();
            _data_employee.employee_list = new employee_detail[1];

            _data_employee.employee_list[0] = Race;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee, 215);
            _data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

            if (_data_employee.employee_list == null)
            {

                ddRace.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {

                ddRace.DataSource = _data_employee.employee_list;
                ddRace.DataTextField = "race_name";
                ddRace.DataValueField = "race_idx";
                ddRace.DataBind();
                ddRace.Items.Insert(0, new ListItem("เลือกเชื้อชาติ", "0"));
                ddRace.SelectedValue = SelectValue;
            }



        }
        else if (Type == 1)
        {
            ddRace.DataBind();
            ddRace.Items.Insert(0, new ListItem("เลือกเชื้อชาติ", "0"));
        }


        return ddRace.SelectedItem.Value;
    }
    #endregion

    #region ddNationalityAction DataBind
    protected string ddNationalityAction(int Type, string SelectValue)
    {

        var ddlNationality = (DropDownList)fvAddDataEmployee.FindControl("ddlNationality");

        ddlNationality.Items.Clear();

        if (Type == 0)
        {

            employee_detail Nationality = new employee_detail();
            _data_employee.employee_list = new employee_detail[1];

            _data_employee.employee_list[0] = Nationality;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee, 214);
            _data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);


            if (_data_employee.employee_list == null)
            {

                ddlNationality.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {
                ddlNationality.DataSource = _data_employee.employee_list;
                ddlNationality.DataTextField = "nat_name";
                ddlNationality.DataValueField = "nat_idx";
                ddlNationality.DataBind();
                ddlNationality.Items.Insert(0, new ListItem("เลือกสัญชาติ", "0"));
                ddlNationality.SelectedValue = SelectValue;
            }



        }
        else if (Type == 1)
        {
            ddlNationality.DataBind();
            ddlNationality.Items.Insert(0, new ListItem("เลือกสัญชาติ", "0"));
        }


        return ddlNationality.SelectedItem.Value;
    }
    #endregion


    #region ddlApprover_Leave2 DataBind
    protected void ddlApprover_Leave()
    {
        DropDownList ddDivision = (DropDownList)fvAddDataEmployee.FindControl("ddDivision");
        DropDownList ddDepartment = (DropDownList)fvAddDataEmployee.FindControl("ddDepartment");
        DropDownList ddPosition = (DropDownList)fvAddDataEmployee.FindControl("ddPosition");
        DropDownList ddlApprover_Leave1 = (DropDownList)fvAddDataEmployee.FindControl("ddlApprover_Leave1");
        DropDownList ddlApprover_Leave2 = (DropDownList)fvAddDataEmployee.FindControl("ddlApprover_Leave2");

        ddlApprover_Leave1.Items.Clear();
        ddlApprover_Leave2.Items.Clear();


        ddlApprover_Leave2.AppendDataBoundItems = true;
        ddlApprover_Leave2.Items.Add(new ListItem("เลือกผู้อนุมัติใบลา", "0"));

        ddlApprover_Leave1.AppendDataBoundItems = true;
        ddlApprover_Leave1.Items.Add(new ListItem("เลือกผู้อนุมัติใบลา", "0"));


        data_employee Approve = new data_employee();
        employee_detail relation = new employee_detail();
        Approve.employee_list = new employee_detail[1];
        relation.rdept_idx = int.Parse(ddDepartment.SelectedValue);
        relation.rsec_idx = int.Parse(ddDivision.SelectedValue);
        relation.rpos_idx = int.Parse(ddPosition.SelectedValue);

        Approve.employee_list[0] = relation;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", Approve, 220);
        Approve = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        ddlApprover_Leave2.DataSource = Approve.employee_list;
        ddlApprover_Leave2.DataTextField = "emp_fullname_th";
        ddlApprover_Leave2.DataValueField = "emp_idx";
        ddlApprover_Leave2.DataBind();

        ddlApprover_Leave1.DataSource = Approve.employee_list;
        ddlApprover_Leave1.DataTextField = "emp_fullname_th";
        ddlApprover_Leave1.DataValueField = "emp_idx";
        ddlApprover_Leave1.DataBind();
    }



    #endregion

    #region ddlApprover_Leave2_edit DataBind
    protected void ddlApprover_Leave_edit()
    {

        TextBox txt_rsec_edit_appropve1 = (TextBox)fvEdit_dataEmployee.FindControl("txt_rsec_edit_appropve1");
        TextBox txt_rsec_edit_appropve2 = (TextBox)fvEdit_dataEmployee.FindControl("txt_rsec_edit_appropve2");
        DropDownList ddlApprover_Leave1_edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddlApprover_Leave1_edit");
        DropDownList ddlApprover_Leave2_edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddlApprover_Leave2_edit");
        DropDownList ddl_add_rpos_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_rpos_idx");
        DropDownList ddl_add_rsec_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_rsec_idx");
        DropDownList ddl_add_rdept_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_rdept_idx");

        ddlApprover_Leave1_edit.Items.Clear();
        ddlApprover_Leave2_edit.Items.Clear();


        ddlApprover_Leave2_edit.AppendDataBoundItems = true;
        ddlApprover_Leave2_edit.Items.Add(new ListItem("เลือกผู้อนุมัติใบลา", "0"));

        ddlApprover_Leave1_edit.AppendDataBoundItems = true;
        ddlApprover_Leave1_edit.Items.Add(new ListItem("เลือกผู้อนุมัติใบลา", "0"));


        data_employee Approve = new data_employee();
        employee_detail relation = new employee_detail();
        Approve.employee_list = new employee_detail[1];

        if (ViewState["value_positionEmty"].ToString() == "0")
        {
            relation.rsec_idx = int.Parse(ddl_add_rsec_idx.SelectedValue);
            relation.rdept_idx = int.Parse(ddl_add_rdept_idx.SelectedValue);
            relation.rpos_idx = int.Parse(ddl_add_rpos_idx.SelectedValue);

        }
        else if (ViewState["value_positionEmty"].ToString() == "1")
        {
            relation.rsec_idx = int.Parse(ViewState["rsec_idx"].ToString());
            relation.rdept_idx = int.Parse(ViewState["rdept_idx"].ToString());
            relation.rpos_idx = int.Parse(ViewState["rpos_idxx"].ToString());
        }



        Approve.employee_list[0] = relation;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", Approve, 220);
        Approve = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        ddlApprover_Leave2_edit.DataSource = Approve.employee_list;
        ddlApprover_Leave2_edit.DataTextField = "emp_fullname_th";
        ddlApprover_Leave2_edit.DataValueField = "emp_idx";
        ddlApprover_Leave2_edit.DataBind();
        ddlApprover_Leave2_edit.SelectedValue = txt_rsec_edit_appropve2.Text;

        ddlApprover_Leave1_edit.DataSource = Approve.employee_list;
        ddlApprover_Leave1_edit.DataTextField = "emp_fullname_th";
        ddlApprover_Leave1_edit.DataValueField = "emp_idx";
        ddlApprover_Leave1_edit.DataBind();
        ddlApprover_Leave1_edit.SelectedValue = txt_rsec_edit_appropve1.Text;
    }

    #endregion

    #region ddMilitaryAction DataBind
    protected void ddlMilitary()
    {

        DropDownList ddMilitary = (DropDownList)fvAddDataEmployee.FindControl("ddMilitary");
        ddMilitary.AppendDataBoundItems = true;
        ddMilitary.Items.Add(new ListItem("เลือกสถานะทางทหาร", "00"));
        data_presonal _data_pre = new data_presonal();

        DetailsMilitary Military = new DetailsMilitary();
        _data_pre.MilitaryList = new DetailsMilitary[1];

        _data_pre.MilitaryList[0] = Military;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_presonal", "Manage_Employee", _data_pre, 209);
        _data_pre = (data_presonal)_funcTool.convertXmlToObject(typeof(data_presonal), _local_xml);

        //localXml = _functionWeb.ConvertObjectToXml(dataPersonal);
        //TextBox6.Text = localXml;

        ddMilitary.DataSource = _data_pre.MilitaryList;
        ddMilitary.DataTextField = "MilName";
        ddMilitary.DataValueField = "MilIDX";
        ddMilitary.DataBind();
    }


    protected void ddlMilitary_edit_()
    {
        // var fvEdit_dataEmployeeTHai = (FormView)viewEdit_dataEmployee.FindControl("fvEdit_dataEmployeeTHai");
        DropDownList ddMilitary_Edit = (DropDownList)fvEdit_dataEmployeeTHai.FindControl("ddMilitary_Edit");
        TextBox txt_MilIDX_edit = (TextBox)fvEdit_dataEmployeeTHai.FindControl("txt_MilIDX_edit");
        ddMilitary_Edit.AppendDataBoundItems = true;
        ddMilitary_Edit.Items.Add(new ListItem("เลือกสถานะทางทหาร", "0"));
        data_presonal _data_pre = new data_presonal();

        DetailsMilitary Military = new DetailsMilitary();
        _data_pre.MilitaryList = new DetailsMilitary[1];

        _data_pre.MilitaryList[0] = Military;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_presonal", "Manage_Employee", _data_pre, 209);
        _data_pre = (data_presonal)_funcTool.convertXmlToObject(typeof(data_presonal), _local_xml);

        //localXml = _functionWeb.ConvertObjectToXml(dataPersonal);
        //TextBox6.Text = localXml;

        ddMilitary_Edit.DataSource = _data_pre.MilitaryList;
        ddMilitary_Edit.DataTextField = "MilName";
        ddMilitary_Edit.DataValueField = "MilIDX";
        ddMilitary_Edit.DataBind();
        ddMilitary_Edit.SelectedValue = txt_MilIDX_edit.Text;

    }


    #endregion

    #region ddCountryENAction DataBind
    protected string ddCountryENAction(int Type, string SelectValue)
    {

        var ddlCountryEN = (DropDownList)fvAddDataEmployee.FindControl("ddlCountryEN");

        ddlCountryEN.Items.Clear();

        if (Type == 0)
        {
            data_Region Region = new data_Region();
            DetailCountry Country = new DetailCountry();
            Region.CountryList = new DetailCountry[1];

            Region.CountryList[0] = Country;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", Region, 204);
            Region = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);

            if (Region.CountryList == null)
            {

                ddlCountryEN.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {
                ddlCountryEN.DataSource = Region.CountryList;
                ddlCountryEN.DataTextField = "CountryName";
                ddlCountryEN.DataValueField = "CountryIDX";
                ddlCountryEN.DataBind();
                ddlCountryEN.Items.Insert(0, new ListItem("เลือกประเทศ", "0"));
                ddlCountryEN.SelectedValue = SelectValue;

            }


        }
        else if (Type == 1)
        {
            ddlCountryEN.DataBind();
            ddlCountryEN.Items.Insert(0, new ListItem("เลือกประเทศ", "0"));
        }


        return ddlCountryEN.SelectedItem.Value;
    }
    #endregion

    #region ddCountryAction DataBind
    protected string ddCountryAction(int Type, string SelectValue)
    {

        var ddCountry = (DropDownList)fvAddDataEmployee.FindControl("ddCountry");

        ddCountry.Items.Clear();

        if (Type == 0)
        {

            DetailCountry Country = new DetailCountry();
            _data_Region.CountryList = new DetailCountry[1];

            _data_Region.CountryList[0] = Country;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", _data_Region, 204);
            _data_Region = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);

            if (_data_Region.CountryList == null)
            {

                ddCountry.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {
                ddCountry.DataSource = _data_Region.CountryList;
                ddCountry.DataTextField = "CountryName";
                ddCountry.DataValueField = "CountryIDX";
                ddCountry.DataBind();
                ddCountry.Items.Insert(0, new ListItem("เลือกประเทศ", "0"));
                ddCountry.SelectedValue = SelectValue;

            }


        }
        else if (Type == 1)
        {
            ddCountry.DataBind();
            ddCountry.Items.Insert(0, new ListItem("เลือกประเทศ", "0"));
        }


        return ddCountry.SelectedItem.Value;
    }

    protected string ddCountry_Permanent_Edit(int Type, string SelectValue)
    {
        FormView fvEdit_dataEmployeeTHai = (FormView)viewEdit_dataEmployee.FindControl("fvEdit_dataEmployeeTHai");
        var ddCountry_Permanent_Edit = (DropDownList)fvEdit_dataEmployeeTHai.FindControl("ddCountry_Permanent_Edit");
        TextBox txt_Country_Permanent = (TextBox)fvEdit_dataEmployeeTHai.FindControl("txt_Country_Permanent");

        ddCountry_Permanent_Edit.Items.Clear();

        if (Type == 0)
        {
            data_Region Region = new data_Region();
            DetailCountry Country_edit = new DetailCountry();
            Region.CountryList = new DetailCountry[1];

            Region.CountryList[0] = Country_edit;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", Region, 204);
            Region = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);

            if (Region.CountryList == null)
            {

                ddCountry_Permanent_Edit.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {
                ddCountry_Permanent_Edit.DataSource = Region.CountryList;
                ddCountry_Permanent_Edit.DataTextField = "CountryName";
                ddCountry_Permanent_Edit.DataValueField = "CountryIDX";
                ddCountry_Permanent_Edit.DataBind();
                ddCountry_Permanent_Edit.Items.Insert(0, new ListItem("เลือกประเทศ", "0"));
                ddCountry_Permanent_Edit.SelectedValue = txt_Country_Permanent.Text;//SelectValue;

            }


        }
        else if (Type == 1)
        {
            ddCountry_Permanent_Edit.DataBind();
            ddCountry_Permanent_Edit.Items.Insert(0, new ListItem("เลือกประเทศ", "0"));
        }


        return ddCountry_Permanent_Edit.SelectedItem.Value;
    }

    #endregion

    #region ddDistrictAction Edit
    protected string ddDistrict_Edit(int Type, string SelectValue)
    {

        var ddAmphoe_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddAmphoe_Edit");
        var ddDistrict_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddDistrict_Edit");

        ddDistrict_Edit.Items.Clear();

        if (Type == 0)
        {
            data_Region region = new data_Region();
            DetailDist District = new DetailDist();
            region.DistList = new DetailDist[1];

            District.AmpIDX = int.Parse(ddAmphoe_Edit.SelectedValue);

            region.DistList[0] = District;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", region, 207);
            region = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);

            if (region.DistList == null)
            {

                ddDistrict_Edit.Items.Insert(0, new ListItem("เลือกตำบล", "0"));
            }
            else
            {
                ddDistrict_Edit.DataSource = region.DistList;
                ddDistrict_Edit.DataTextField = "DistName";
                ddDistrict_Edit.DataValueField = "DistIDX";
                ddDistrict_Edit.DataBind();
                ddDistrict_Edit.Items.Insert(0, new ListItem("เลือกตำบล", "0"));
                ddDistrict_Edit.SelectedValue = SelectValue;
            }


        }
        else if (Type == 1)
        {

            ddDistrict_Edit.Items.Insert(0, new ListItem("เลือกตำบล", "0"));
            ddDistrict_Edit.SelectedValue = SelectValue;
        }


        return ddDistrict_Edit.SelectedItem.Value;
    }


    protected string ddDistrict_Permanent_Edit(int Type, string SelectValue)
    {

        //  var fvEdit_dataEmployeeTHai = (FormView)fvEdit_dataEmployee.FindControl("fvEdit_dataEmployeeTHai");
        var ddDistrict_Permanent_Edit = (DropDownList)fvEdit_dataEmployeeTHai.FindControl("ddDistrict_Permanent_Edit");
        var ddAmphoe_Permanent_Edit = (DropDownList)fvEdit_dataEmployeeTHai.FindControl("ddAmphoe_Permanent_Edit");
        var txt_District_Permanent_Edit = (TextBox)fvEdit_dataEmployeeTHai.FindControl("txt_District_Permanent_Edit");

        ddDistrict_Permanent_Edit.Items.Clear();

        if (Type == 0)
        {
            data_Region region__ = new data_Region();
            DetailDist District = new DetailDist();
            region__.DistList = new DetailDist[1];

            District.AmpIDX = int.Parse(ddAmphoe_Permanent_Edit.SelectedValue);

            region__.DistList[0] = District;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", region__, 207);
            region__ = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);

            if (region__.DistList == null)
            {

                ddDistrict_Permanent_Edit.Items.Insert(0, new ListItem("เลือกตำบล", "0"));
            }
            else
            {
                ddDistrict_Permanent_Edit.DataSource = region__.DistList;
                ddDistrict_Permanent_Edit.DataTextField = "DistName";
                ddDistrict_Permanent_Edit.DataValueField = "DistIDX";
                ddDistrict_Permanent_Edit.DataBind();
                ddDistrict_Permanent_Edit.Items.Insert(0, new ListItem("เลือกตำบล", "0"));
                ddDistrict_Permanent_Edit.SelectedValue = txt_District_Permanent_Edit.Text;
            }


        }
        else if (Type == 1)
        {

            ddDistrict_Permanent_Edit.Items.Insert(0, new ListItem("เลือกตำบล", "0"));
            ddDistrict_Permanent_Edit.SelectedValue = SelectValue;
        }


        return ddDistrict_Permanent_Edit.SelectedItem.Value;
    }




    #endregion

    #region ddProvinceAction DataBind
    protected string ddProvinceAction(int Type, string SelectValue)
    {

        var ddProvince = (DropDownList)fvAddDataEmployee.FindControl("ddProvince");

        ddProvince.Items.Clear();

        if (Type == 0)
        {
            DetailProv Province = new DetailProv();
            _data_Region.ProvList = new DetailProv[1];

            _data_Region.ProvList[0] = Province;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", _data_Region, 205);
            _data_Region = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);

            if (_data_Region.ProvList == null)
            {

                ddProvince.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {
                ddProvince.DataSource = _data_Region.ProvList;
                ddProvince.DataTextField = "ProvName";
                ddProvince.DataValueField = "ProvIDX";
                ddProvince.DataBind();
                ddProvince.Items.Insert(0, new ListItem("เลือกจังหวัด", "0"));
                ddProvince.SelectedValue = SelectValue;

            }


        }
        else if (Type == 1)
        {
            ddProvince.DataBind();
            ddProvince.Items.Insert(0, new ListItem("เลือกจังหวัด", "0"));
        }


        return ddProvince.SelectedItem.Value;
    }
    #endregion

    #region ddAmphoeAction DataBind
    protected string ddAmphoeAction(int Type, string SelectValue)
    {

        var ddProvince = (DropDownList)fvAddDataEmployee.FindControl("ddProvince");
        var ddAmphoe = (DropDownList)fvAddDataEmployee.FindControl("ddAmphoe");

        ddAmphoe.Items.Clear();

        if (Type == 0)
        {
            DetailAmp Amphoe = new DetailAmp();
            _data_Region.AmpList = new DetailAmp[1];

            Amphoe.ProvIDX = int.Parse(ddProvince.SelectedValue);

            _data_Region.AmpList[0] = Amphoe;

            //_local_xml = _funcTool.convertObjectToXml(data_Region);
            //TextBox1.Text = _local_xml;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", _data_Region, 206);
            _data_Region = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);


            if (_data_Region.AmpList == null)
            {

                ddAmphoe.Items.Insert(0, new ListItem("เลือกอำเภอ", "0"));
            }
            else
            {

                ddAmphoe.DataSource = _data_Region.AmpList;
                ddAmphoe.DataTextField = "AmpName";
                ddAmphoe.DataValueField = "AmpIDX";
                ddAmphoe.DataBind();
                ddAmphoe.Items.Insert(0, new ListItem("เลือกอำเภอ", "0"));
                ddAmphoe.SelectedValue = SelectValue;

            }


        }
        else if (Type == 1)
        {

            ddAmphoe.DataBind();
            ddAmphoe.Items.Insert(0, new ListItem("เลือกอำเภอ", "0"));
        }


        return ddAmphoe.SelectedItem.Value;
    }
    #endregion

    #region ddDistrictAction DataBind
    protected string ddDistrictAction(int Type, string SelectValue)
    {

        var ddAmphoe = (DropDownList)fvAddDataEmployee.FindControl("ddAmphoe");
        var ddDistrict = (DropDownList)fvAddDataEmployee.FindControl("ddDistrict");

        ddDistrict.Items.Clear();

        if (Type == 0)
        {
            DetailDist District = new DetailDist();
            _data_Region.DistList = new DetailDist[1];

            District.AmpIDX = int.Parse(ddAmphoe.SelectedValue);

            _data_Region.DistList[0] = District;

            //dataRegion = serviceRegion.DistAction(0, "", "", Int32.Parse(ddAmphoe.SelectedValue), 0, 0, 0, 24);


            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", _data_Region, 207);
            _data_Region = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);

            if (_data_Region.DistList == null)
            {

                ddDistrict.Items.Insert(0, new ListItem("เลือกตำบล", "0"));
            }
            else
            {
                ddDistrict.DataSource = _data_Region.DistList;
                ddDistrict.DataTextField = "DistName";
                ddDistrict.DataValueField = "DistIDX";
                ddDistrict.DataBind();
                ddDistrict.Items.Insert(0, new ListItem("เลือกตำบล", "0"));
                ddDistrict.SelectedValue = SelectValue;
            }


        }
        else if (Type == 1)
        {

            ddDistrict.Items.Insert(0, new ListItem("เลือกตำบล", "0"));
            ddDistrict.SelectedValue = SelectValue;
        }


        return ddDistrict.SelectedItem.Value;
    }
    #endregion

    #region ddCountry2Action DataBind
    protected string ddCountry2Action(int Type, string SelectValue)
    {

        var ddCountry2 = (DropDownList)fvAddDataEmployee.FindControl("ddCountry2");

        ddCountry2.Items.Clear();

        if (Type == 0)
        {
            data_Region region_ = new data_Region();
            DetailCountry Country2 = new DetailCountry();
            region_.CountryList = new DetailCountry[1];

            region_.CountryList[0] = Country2;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", region_, 204);
            region_ = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);

            if (region_.CountryList == null)
            {

                ddCountry2.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {
                ddCountry2.DataSource = region_.CountryList;
                ddCountry2.DataTextField = "CountryName";
                ddCountry2.DataValueField = "CountryIDX";
                ddCountry2.DataBind();
                ddCountry2.Items.Insert(0, new ListItem("เลือกประเทศ", "0"));
                ddCountry2.SelectedValue = SelectValue;

            }


        }
        else if (Type == 1)
        {
            ddCountry2.DataBind();
            ddCountry2.Items.Insert(0, new ListItem("เลือกประเทศ", "0"));
        }


        return ddCountry2.SelectedItem.Value;
    }
    #endregion

    #region ddDistrict2Action DataBind
    protected string ddDistrict2Action(int Type, string SelectValue)
    {

        var ddAmphoe2 = (DropDownList)fvAddDataEmployee.FindControl("ddAmphoe2");
        var ddDistrict2 = (DropDownList)fvAddDataEmployee.FindControl("ddDistrict2");

        ddDistrict2.Items.Clear();

        if (Type == 0)
        {
            DetailDist District = new DetailDist();
            _data_Region.DistList = new DetailDist[1];

            District.AmpIDX = int.Parse(ddAmphoe2.SelectedValue);

            _data_Region.DistList[0] = District;

            //dataRegion = serviceRegion.DistAction(0, "", "", Int32.Parse(ddAmphoe.SelectedValue), 0, 0, 0, 24);


            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", _data_Region, 207);
            _data_Region = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);

            if (_data_Region.DistList == null)
            {

                ddDistrict2.Items.Insert(0, new ListItem("เลือกตำบล", "0"));
            }
            else
            {
                ddDistrict2.DataSource = _data_Region.DistList;
                ddDistrict2.DataTextField = "DistName";
                ddDistrict2.DataValueField = "DistIDX";
                ddDistrict2.DataBind();
                ddDistrict2.Items.Insert(0, new ListItem("เลือกตำบล", "0"));
                ddDistrict2.SelectedValue = SelectValue;
            }


        }
        else if (Type == 1)
        {

            ddDistrict2.Items.Insert(0, new ListItem("เลือกตำบล", "0"));
            ddDistrict2.SelectedValue = SelectValue;
        }


        return ddDistrict2.SelectedItem.Value;
    }
    #endregion

    #region ddAmphoe2Action DataBind
    protected string ddAmphoe2Action(int Type, string SelectValue)
    {

        var ddAmphoe2 = (DropDownList)fvAddDataEmployee.FindControl("ddAmphoe2");
        var ddProvince2 = (DropDownList)fvAddDataEmployee.FindControl("ddProvince2");

        ddAmphoe2.Items.Clear();

        if (Type == 0)
        {

            DetailAmp Amphoe = new DetailAmp();
            _data_Region.AmpList = new DetailAmp[1];

            Amphoe.ProvIDX = int.Parse(ddProvince2.SelectedValue);

            _data_Region.AmpList[0] = Amphoe;

            //_local_xml = _funcTool.convertObjectToXml(data_Region);
            //TextBox1.Text = _local_xml;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", _data_Region, 206);
            _data_Region = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);

            if (_data_Region.AmpList == null)
            {

                ddAmphoe2.Items.Insert(0, new ListItem("เลือกอำเภอ", "0"));
            }
            else
            {

                ddAmphoe2.DataSource = _data_Region.AmpList;
                ddAmphoe2.DataTextField = "AmpName";
                ddAmphoe2.DataValueField = "AmpIDX";
                ddAmphoe2.DataBind();
                ddAmphoe2.Items.Insert(0, new ListItem("เลือกอำเภอ", "0"));
                ddAmphoe2.SelectedValue = SelectValue;

            }


        }
        else if (Type == 1)
        {

            ddAmphoe2.DataBind();
            ddAmphoe2.Items.Insert(0, new ListItem("เลือกอำเภอ", "0"));
        }


        return ddAmphoe2.SelectedItem.Value;
    }
    #endregion

    #region ddProvince2Action DataBind
    protected string ddProvince2Action(int Type, string SelectValue)
    {

        var ddProvince2 = (DropDownList)fvAddDataEmployee.FindControl("ddProvince2");

        ddProvince2.Items.Clear();

        if (Type == 0)
        {
            data_Region Region = new data_Region();
            DetailProv Province = new DetailProv();
            Region.ProvList = new DetailProv[1];

            Region.ProvList[0] = Province;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", Region, 205);
            Region = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);

            if (Region.ProvList == null)
            {

                ddProvince2.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {
                ddProvince2.DataSource = Region.ProvList;
                ddProvince2.DataTextField = "ProvName";
                ddProvince2.DataValueField = "ProvIDX";
                ddProvince2.DataBind();
                ddProvince2.Items.Insert(0, new ListItem("เลือกจังหวัด", "0"));
                ddProvince2.SelectedValue = SelectValue;

            }


        }
        else if (Type == 1)
        {
            ddProvince2.DataBind();
            ddProvince2.Items.Insert(0, new ListItem("เลือกจังหวัด", "0"));
        }


        return ddProvince2.SelectedItem.Value;
    }
    #endregion

    #region ddReligionAction 
    protected string ddReligionAction(int Type, string SelectValue)
    {

        var ddReligion = (DropDownList)fvAddDataEmployee.FindControl("ddReligion");

        ddReligion.Items.Clear();

        if (Type == 0)
        {

            DetailReligion Religion = new DetailReligion();
            _data_Region.ReligionList = new DetailReligion[1];

            _data_Region.ReligionList[0] = Religion;

            //_local_xml = _funcTool.convertObjectToXml(data_Region);
            //TextBox1.Text = _local_xml;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", _data_Region, 203);
            _data_Region = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);

            if (_data_Region.ReligionList == null)
            {

                ddReligion.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {
                ddReligion.DataSource = _data_Region.ReligionList;
                ddReligion.DataTextField = "RelNameTH";
                ddReligion.DataValueField = "RelIDX";
                ddReligion.DataBind();
                ddReligion.Items.Insert(0, new ListItem("เลือกศาสนา", "0"));
                ddReligion.SelectedValue = SelectValue;
            }


        }
        else if (Type == 1)
        {
            ddReligion.DataBind();
            ddReligion.Items.Insert(0, new ListItem("เลือกศาสนา", "0"));
        }


        return ddReligion.SelectedItem.Value;
    }
    #endregion

    #region ddlSelect_dddCostCenter 
    protected string ddlSelect_dddCostCenter(int Type, string SelectValue)
    {

        var ddCostCenter = (DropDownList)fvAddDataEmployee.FindControl("ddCostCenter");

        ddCostCenter.Items.Clear();

        if (Type == 0)
        {
            CostCenter CostCenter = new CostCenter();
            _data_employee.CostCenterDetail = new CostCenter[1];

            _data_employee.CostCenterDetail[0] = CostCenter;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee, 208);
            _data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

            if (_data_employee.CostCenterDetail == null)
            {
                ddCostCenter.Items.Insert(0, new ListItem("เลือก CostCenter ", "0"));
            }
            else
            {

                ddCostCenter.DataSource = _data_employee.CostCenterDetail;
                ddCostCenter.DataTextField = "CostNo";
                ddCostCenter.DataValueField = "CostIDX";
                ddCostCenter.DataBind();
                ddCostCenter.Items.Insert(0, new ListItem("เลือก CostCenter ", "0"));
                ddCostCenter.SelectedValue = SelectValue;

            }

        }
        else if (Type == 1)
        {
            ddCostCenter.DataBind();
            ddCostCenter.Items.Insert(0, new ListItem("เลือก CostCenter ", "0"));
        }


        return ddCostCenter.SelectedItem.Value;
    }
    #endregion

    #region ddlSelect_ddPrefixENAction
    protected void ddPrefixENAction1(int Type, string SelectValue)
    {
        var ddPrefixEN = (DropDownList)fvAddDataEmployee.FindControl("ddPrefixEN");

        ddPrefixEN.Items.Clear();

        if (Type == 0)
        {

            DetailPrefixName ddlPrefixTH = new DetailPrefixName();
            data_presonal.PrefixNameList = new DetailPrefixName[1];

            data_presonal.PrefixNameList[0] = ddlPrefixTH;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_presonal", "Manage_Employee", data_presonal, 201);
            data_presonal = (data_presonal)_funcTool.convertXmlToObject(typeof(data_presonal), _local_xml);
            ddPrefixEN.DataSource = data_presonal.PrefixNameList;
            ddPrefixEN.DataTextField = "PrefixNameEN";
            ddPrefixEN.DataValueField = "PrefixIDX";
            ddPrefixEN.DataBind();

        }
        else if (Type == 1)
        {
            ddPrefixEN.DataBind();
            ddPrefixEN.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
        }

    }
    #endregion

    #region ddlSelect_ddPrefixTH
    protected void ddPrefixTHAction(int Type, string SelectValue)
    {
        var ddPrefixTH = (DropDownList)fvAddDataEmployee.FindControl("ddPrefixTH");
        ddPrefixTH.AppendDataBoundItems = true;
        ddPrefixTH.Items.Clear();

        if (Type == 0)
        {
            DetailPrefixName ddlPrefixTH = new DetailPrefixName();
            data_presonal.PrefixNameList = new DetailPrefixName[1];

            data_presonal.PrefixNameList[0] = ddlPrefixTH;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_presonal", "Manage_Employee", data_presonal, 201);
            data_presonal = (data_presonal)_funcTool.convertXmlToObject(typeof(data_presonal), _local_xml);
            ddPrefixTH.DataSource = data_presonal.PrefixNameList;
            ddPrefixTH.DataTextField = "PrefixNameTH";
            ddPrefixTH.DataValueField = "PrefixIDX";
            ddPrefixTH.DataBind();



        }
        else if (Type == 1)
        {
            ddPrefixTH.DataBind();
            ddPrefixTH.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
        }


        //return ddPrefixTH.SelectedItem.Value;
    }
    #endregion

    #region ddlEducation

    protected void Select_ddlEducation()
    {
        DropDownList ddlEducation = (DropDownList)fvAddDataEmployee.FindControl("ddlEducation");
        ddlEducation.AppendDataBoundItems = true;
        ddlEducation.Items.Add(new ListItem("เลือกระดับการศึกษา", "00"));
        data_employee _data_pre = new data_employee();
        DetailEducation Education = new DetailEducation();
        _data_pre.EducationList = new DetailEducation[1];

        _data_pre.EducationList[0] = Education;


        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_presonal", "Manage_Employee", _data_pre, 210);
        _data_pre = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        ddlEducation.DataSource = _data_pre.EducationList;
        ddlEducation.DataTextField = "Edu_name";
        ddlEducation.DataValueField = "Eduidx";
        ddlEducation.DataBind();

    }


    protected void ddlEducation_insert_()
    {
        // var box_insert_education = (Panel)fvEdit_dataEmployee.FindControl("box_insert_education");
        DropDownList ddlEducation_insert1 = (DropDownList)box_insert_education.FindControl("ddlEducation_insert");
        ddlEducation_insert1.AppendDataBoundItems = true;
        ddlEducation_insert1.Items.Add(new ListItem("เลือกระดับการศึกษา", "00"));
        data_employee _data_pre = new data_employee();
        DetailEducation Education = new DetailEducation();
        _data_pre.EducationList = new DetailEducation[1];

        _data_pre.EducationList[0] = Education;


        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_presonal", "Manage_Employee", _data_pre, 210);
        _data_pre = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        ddlEducation_insert1.DataSource = _data_pre.EducationList;
        ddlEducation_insert1.DataTextField = "Edu_name";
        ddlEducation_insert1.DataValueField = "Eduidx";
        ddlEducation_insert1.DataBind();

    }

    #endregion

    #region ddlSelect_Faculty
    protected void ddlSelect_Faculty()
    {

        DropDownList ddlFaculty = (DropDownList)fvAddDataEmployee.FindControl("ddlFaculty");
        ddlFaculty.AppendDataBoundItems = true;
        ddlFaculty.Items.Add(new ListItem("เลือกคณะ", "00"));
        data_presonal _data_pre = new data_presonal();
        FacultyList Faculty = new FacultyList();
        _data_pre.boxFaculty = new FacultyList[1];
        _data_pre.boxFaculty[0] = Faculty;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_presonal", "Manage_Employee", _data_pre, 200);
        _data_pre = (data_presonal)_funcTool.convertXmlToObject(typeof(data_presonal), _local_xml);

        //_local_xml = _funcTool.convertObjectToXml(data_presonal);
        //TextBox1.Text = _local_xml;

        ddlFaculty.DataSource = _data_pre.boxFaculty;
        ddlFaculty.DataTextField = "Faculty_Name";
        ddlFaculty.DataValueField = "FacIDX";
        ddlFaculty.DataBind();

    }


    protected void ddlFaculty_insert_()
    {
        // var box_insert_education = (Panel)fvEdit_dataEmployee.FindControl("box_insert_education");
        DropDownList ddlFaculty_insert = (DropDownList)box_insert_education.FindControl("ddlFaculty_insert");
        ddlFaculty_insert.AppendDataBoundItems = true;
        ddlFaculty_insert.Items.Add(new ListItem("เลือกคณะ", "00"));
        data_presonal _data_pre = new data_presonal();
        FacultyList Faculty = new FacultyList();
        _data_pre.boxFaculty = new FacultyList[1];
        _data_pre.boxFaculty[0] = Faculty;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_presonal", "Manage_Employee", _data_pre, 200);
        _data_pre = (data_presonal)_funcTool.convertXmlToObject(typeof(data_presonal), _local_xml);

        //_local_xml = _funcTool.convertObjectToXml(data_presonal);
        //TextBox1.Text = _local_xml;

        ddlFaculty_insert.DataSource = _data_pre.boxFaculty;
        ddlFaculty_insert.DataTextField = "Faculty_Name";
        ddlFaculty_insert.DataValueField = "FacIDX";
        ddlFaculty_insert.DataBind();

    }
    protected void ddl_ShiftTime()
    {
        var formview_shiftTime = (FormView)fvAddDataEmployee.FindControl("formview_shiftTime");
        var ddOrganiztion = (DropDownList)fvAddDataEmployee.FindControl("ddOrganiztion");
        DropDownList ddlist_ShiftTime = (DropDownList)formview_shiftTime.FindControl("ddlist_ShiftTime");
        // var hiddenOrg = (TextBox)Information_empShift.Items[0].FindControl("hiddenOrg");
        ddlist_ShiftTime.Items.Clear();
        ddlist_ShiftTime.AppendDataBoundItems = true;
        ddlist_ShiftTime.Items.Add(new ListItem("กรุณาเลือกตารางกะงาน", "0"));
        data_employee dataShiftTime = new data_employee();
        ShiftTime listShiftime = new ShiftTime();
        dataShiftTime.ShiftTime_details = new ShiftTime[1];

        // ViewState["orgIDX"] = (hiddenOrg.Text);//1;
        // int.Parse(ddOrganiztion.SelectedValue);
        listShiftime.org_idx_shiftTime = int.Parse(ddOrganiztion.SelectedValue); /*int.Parse(ViewState["orgIDX"].ToString());*/

        dataShiftTime.ShiftTime_details[0] = listShiftime;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", dataShiftTime, 245);
        dataShiftTime = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        ddlist_ShiftTime.DataSource = dataShiftTime.ShiftTime_details;
        ddlist_ShiftTime.DataTextField = "type_shiftTime";
        ddlist_ShiftTime.DataValueField = "shiftTime_idx";
        ddlist_ShiftTime.DataBind();

    }

    protected void ddl_edit_ShiftTime()
    {
        var editformview_shiftTime = (FormView)fvEdit_dataEmployee.FindControl("editformview_shiftTime");
        var ddlist_edit_ShiftTime = (DropDownList)editformview_shiftTime.FindControl("ddlist_edit_ShiftTime");
        // TextBox txt_org_idx_update = (TextBox)fvEdit_dataEmployee.FindControl("txt_org_idx_update");
        // var hiddenOrg = (TextBox)Information_empShift.Items[0].FindControl("hiddenOrg");
        ddlist_edit_ShiftTime.Items.Clear();
        ddlist_edit_ShiftTime.AppendDataBoundItems = true;
        ddlist_edit_ShiftTime.Items.Add(new ListItem("กรุณาเลือกตารางกะงาน", "0"));
        data_employee dataShiftTime = new data_employee();
        ShiftTime listShiftime = new ShiftTime();
        dataShiftTime.ShiftTime_details = new ShiftTime[1];

        // ViewState["orgIDX"] = (txt_org_idx_update.Text);//1;
        // int.Parse(ddOrganiztion.SelectedValue);
        listShiftime.org_idx_shiftTime = int.Parse(ViewState["orgIDX"].ToString());

        dataShiftTime.ShiftTime_details[0] = listShiftime;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", dataShiftTime, 245);
        dataShiftTime = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        ddlist_edit_ShiftTime.DataSource = dataShiftTime.ShiftTime_details;
        ddlist_edit_ShiftTime.DataTextField = "type_shiftTime";
        ddlist_edit_ShiftTime.DataValueField = "shiftTime_idx";
        ddlist_edit_ShiftTime.DataBind();

    }



    protected void Faculty_edit(int Type, string SelectValue)
    {
        DropDownList ddlFaculty_Edit = (DropDownList)fv_edit_Education.FindControl("ddlFaculty_Edit");
        TextBox txt_Faculty_edit = (TextBox)fv_edit_Education.FindControl("txt_Faculty_edit");

        if (Type == 0)
        {

            ddlFaculty_Edit.AppendDataBoundItems = true;
            ddlFaculty_Edit.Items.Add(new ListItem("เลือกคณะ", "00"));
            data_presonal _data_pre = new data_presonal();
            FacultyList Faculty = new FacultyList();
            _data_pre.boxFaculty = new FacultyList[1];
            _data_pre.boxFaculty[0] = Faculty;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_presonal", "Manage_Employee", _data_pre, 200);
            _data_pre = (data_presonal)_funcTool.convertXmlToObject(typeof(data_presonal), _local_xml);

            if (_data_pre.boxFaculty == null)
            {
                ddlFaculty_Edit.DataSource = _data_pre.boxFaculty;
                ddlFaculty_Edit.DataTextField = "Faculty_Name";
                ddlFaculty_Edit.DataValueField = "FacIDX";
                ddlFaculty_Edit.DataBind();

            }
            else
            {
                ddlFaculty_Edit.DataSource = _data_pre.boxFaculty;
                ddlFaculty_Edit.DataTextField = "Faculty_Name";
                ddlFaculty_Edit.DataValueField = "FacIDX";
                ddlFaculty_Edit.DataBind();
                ddlFaculty_Edit.SelectedValue = txt_Faculty_edit.Text;
            }
        }
        else if (Type == 1)
        {
            ddlFaculty_Edit.DataBind();
            ddlFaculty_Edit.Items.Insert(0, new ListItem("เลือกคณะ", "00"));
        }
    }
    #endregion

    #region ddlSelect_ddSexAction
    protected void ddlSelect_ddSexAction()
    {

        DropDownList ddlistSex = (DropDownList)fvAddDataEmployee.FindControl("ddlistSex");
        ddlistSex.AppendDataBoundItems = true;
        ddlistSex.Items.Add(new ListItem("เลือกเพศ", "00"));
        data_presonal _data_pre = new data_presonal();
        DetailPrefixName ddlSex = new DetailPrefixName();
        _data_pre.PrefixNameList = new DetailPrefixName[1];
        _data_pre.PrefixNameList[0] = ddlSex;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_presonal", "Manage_Employee", _data_pre, 201);
        _data_pre = (data_presonal)_funcTool.convertXmlToObject(typeof(data_presonal), _local_xml);

        //_local_xml = _funcTool.convertObjectToXml(data_presonal);
        //TextBox1.Text = _local_xml;

        ddlistSex.DataSource = _data_pre.PrefixNameList;
        ddlistSex.DataTextField = "SexNameTH";
        ddlistSex.DataValueField = "SexIDX";
        ddlistSex.DataBind();

    }
    #endregion

    protected void ddlEducation_edit__(int Type, string SelectValue)
    {

        //FormView fvEdit_dataEmployee = (FormView)viewEdit_dataEmployee.FindControl("fvEdit_dataEmployee");
        //FormView fv_edit_Education = (FormView)fvEdit_dataEmployee.FindControl("fv_edit_Education");
        //   fv_edit_Education.ChangeMode(FormViewMode.Edit);

        DropDownList ddlEducation_Edit = (DropDownList)fv_edit_Education.FindControl("ddlEducation_Edit");
        TextBox txt_Education_edit = (TextBox)fv_edit_Education.FindControl("txt_Education_edit");


        if (Type == 0)
        {


            data_employee Education_data_edit = new data_employee();
            DetailEducation Education_editt = new DetailEducation();
            Education_data_edit.EducationList = new DetailEducation[1];

            Education_data_edit.EducationList[0] = Education_editt;


            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", Education_data_edit, 210);
            Education_data_edit = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

            ddlEducation_Edit.DataSource = Education_data_edit.EducationList;
            ddlEducation_Edit.AppendDataBoundItems = true;
            ddlEducation_Edit.Items.Add(new ListItem("เลือกระดับการศึกษา", "00"));
            ddlEducation_Edit.DataTextField = "Edu_name";
            ddlEducation_Edit.DataValueField = "Eduidx";
            ddlEducation_Edit.DataBind();
            ddlEducation_Edit.SelectedValue = txt_Education_edit.Text;
            //lEducation_Edit.SelectedValue = SelectValue;

        }
        else if (Type == 1)
        {
            ddlEducation_Edit.DataBind();
            ddlEducation_Edit.Items.Insert(0, new ListItem("เลือกระดับการศึกษา", "00"));
        }

    }


    #region ddl_add_org_idx
    protected void ddl_org_action(int Type, string SelectValue)
    {
        var ddl_add_org_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_org_idx");

        ddl_add_org_idx.Items.Clear();

        if (Type == 0)
        {
            dataODSP_Relation data_osdp1 = new dataODSP_Relation();
            Relation_ODSP relation_1 = new Relation_ODSP();
            data_osdp1.detailRelation_ODSP = new Relation_ODSP[1];
            relation_1.OrgIDX = 0;
            data_osdp1.detailRelation_ODSP[0] = relation_1;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", data_osdp1, 200);
            data_osdp1 = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

            if (data_osdp1.detailRelation_ODSP == null)
            {
                ddl_add_org_idx.Items.Insert(0, new ListItem("เลือกบริษัท", "00"));
            }
            else
            {

                ddl_add_org_idx.DataSource = data_osdp1.detailRelation_ODSP;
                ddl_add_org_idx.DataTextField = "OrgNameTH";
                ddl_add_org_idx.DataValueField = "OrgIDX";
                ddl_add_org_idx.DataBind();
                ddl_add_org_idx.Items.Insert(0, new ListItem("เลือกบริษัท", "00"));
                ddl_add_org_idx.SelectedValue = SelectValue;

            }

        }
        else if (Type == 1)
        {
            ddl_add_org_idx.DataBind();
            ddl_add_org_idx.Items.Insert(0, new ListItem("เลือกบริษัท", "00"));
        }


    }
    #endregion

    #region ddlchangOrganization
    protected void ddlchang_Organization(int Type, string SelectValue)
    {
        //var Panel2 = (Panel)BoxRemovePosisionn.FindControl("Panel2");
        DropDownList ddlchangOrganization = (DropDownList)BoxRemovePosisionn.FindControl("ddlchangOrganization");

        ddlchangOrganization.Items.Clear();

        if (Type == 0)
        {

            dataODSP_Relation _move_position1 = new dataODSP_Relation();
            Relation_ODSP relation_move_position1 = new Relation_ODSP();
            _move_position1.detailRelation_ODSP = new Relation_ODSP[1];
            relation_move_position1.OrgIDX = 0;
            _move_position1.detailRelation_ODSP[0] = relation_move_position1;



            _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", _move_position1, 200);
            _move_position1 = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

            if (_move_position1.detailRelation_ODSP == null)
            {
                ddlchangOrganization.Items.Insert(0, new ListItem("เลือกบริษัท", "0"));
            }
            else
            {

                ddlchangOrganization.DataSource = _move_position1.detailRelation_ODSP;
                ddlchangOrganization.DataTextField = "OrgNameTH";
                ddlchangOrganization.DataValueField = "OrgIDX";
                ddlchangOrganization.DataBind();
                ddlchangOrganization.Items.Insert(0, new ListItem("เลือกบริษัท", "0"));
                //ddlchangOrganization.SelectedValue = SelectValue;

            }

        }
        else if (Type == 1)
        {
            ddlchangOrganization.DataBind();
            ddlchangOrganization.Items.Insert(0, new ListItem("เลือกบริษัท", "00"));
        }


    }
    #endregion

    #region ddlSelect_ddOrganiztion
    protected void ddlSelect_ddOrganiztion(int Type, string SelectValue)
    {
        var ddOrganiztion = (DropDownList)fvAddDataEmployee.FindControl("ddOrganiztion");

        ddOrganiztion.Items.Clear();

        if (Type == 0)
        {

            Relation_ODSP relation = new Relation_ODSP();
            dataODSPReration.detailRelation_ODSP = new Relation_ODSP[1];
            relation.OrgIDX = 0;
            dataODSPReration.detailRelation_ODSP[0] = relation;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Reration", "ODSP_Reletion", dataODSPReration, 200);
            dataODSPReration = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

            if (dataODSPReration.detailRelation_ODSP == null)
            {
                ddOrganiztion.Items.Insert(0, new ListItem("เลือกบริษัท", "00"));
            }
            else
            {

                ddOrganiztion.DataSource = dataODSPReration.detailRelation_ODSP;
                ddOrganiztion.DataTextField = "OrgNameTH";
                ddOrganiztion.DataValueField = "OrgIDX";
                ddOrganiztion.DataBind();
                ddOrganiztion.Items.Insert(0, new ListItem("เลือกบริษัท", "00"));
                ddOrganiztion.SelectedValue = SelectValue;

            }

        }
        else if (Type == 1)
        {
            ddOrganiztion.DataBind();
            ddOrganiztion.Items.Insert(0, new ListItem("เลือกบริษัท", "00"));
        }


    }
    #endregion

    #region ddl_Select_Organization_search
    protected void Organization_search(int Type, string SelectValue)
    {


        var ddl_Select_Organization_search = (DropDownList)ViewIndex.FindControl("ddl_Select_Organization_search");

        ddl_Select_Organization_search.Items.Clear();

        if (Type == 0)
        {

            Relation_ODSP relation = new Relation_ODSP();
            dataODSPReration.detailRelation_ODSP = new Relation_ODSP[1];
            relation.OrgIDX = 0;
            dataODSPReration.detailRelation_ODSP[0] = relation;


            _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", dataODSPReration, 200);
            dataODSPReration = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);


            if (dataODSPReration.detailRelation_ODSP == null)
            {
                ddl_Select_Organization_search.Items.Insert(0, new ListItem("เลือกบริษัท", "00"));
            }
            else
            {


                ddl_Select_Organization_search.DataSource = dataODSPReration.detailRelation_ODSP;
                ddl_Select_Organization_search.DataTextField = "OrgNameTH";
                ddl_Select_Organization_search.DataValueField = "OrgIDX";
                ddl_Select_Organization_search.DataBind();
                ddl_Select_Organization_search.Items.Insert(0, new ListItem("เลือกบริษัท", "00"));
                ddl_Select_Organization_search.SelectedValue = SelectValue;

            }

        }
        else if (Type == 1)
        {
            ddl_Select_Organization_search.DataBind();
            ddl_Select_Organization_search.Items.Insert(0, new ListItem("เลือกบริษัท", "00"));
        }


    }


    #endregion

    #region ddl_Select_Department_search
    protected void Department_search(int Type, string SelectValue)
    {


        var ddl_Select_Organization_search = (DropDownList)ViewIndex.FindControl("ddl_Select_Organization_search");
        var ddl_Select_Department_search = (DropDownList)ViewIndex.FindControl("ddl_Select_Department_search");

        ddl_Select_Department_search.Items.Clear();

        if (Type == 0)
        {
            dataODSP_Relation ODSP_Relation = new dataODSP_Relation();
            var relation = new Relation_ODSP[1];
            relation[0] = new Relation_ODSP();
            relation[0].OrgIDX = Int32.Parse(ddl_Select_Organization_search.SelectedValue);
            ODSP_Relation.detailRelation_ODSP = relation;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", ODSP_Relation, 200);
            ODSP_Relation = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

            if (ODSP_Relation.detailRelation_ODSP == null)
            {
                ddl_Select_Department_search.Items.Insert(0, new ListItem("เลือกฝ่าย", "0"));
            }
            else
            {

                ddl_Select_Department_search.DataSource = ODSP_Relation.detailRelation_ODSP;
                ddl_Select_Department_search.DataTextField = "DeptNameTH";
                ddl_Select_Department_search.DataValueField = "RDeptIDX";
                ddl_Select_Department_search.DataBind();
                ddl_Select_Department_search.Items.Insert(0, new ListItem("เลือกฝ่าย", "0"));
                ddl_Select_Department_search.SelectedValue = SelectValue;

            }

        }
        else if (Type == 1)
        {
            ddl_Select_Department_search.DataBind();
            ddl_Select_Department_search.Items.Insert(0, new ListItem("เลือกฝ่าย", "0"));
        }


    }


    #endregion

    #region ddl_add_rdept_idx
    protected void ddl_rdept_action(int Type, string SelectValue)
    {

        var ddl_add_org_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_org_idx");
        var ddl_add_rdept_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_rdept_idx");

        ddl_add_rdept_idx.Items.Clear();

        if (Type == 0)
        {
            dataODSP_Relation ODSP_Relation__ = new dataODSP_Relation();
            var relation__ = new Relation_ODSP[1];
            relation__[0] = new Relation_ODSP();
            relation__[0].OrgIDX = Int32.Parse(ddl_add_org_idx.SelectedValue);
            ODSP_Relation__.detailRelation_ODSP = relation__;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", ODSP_Relation__, 200);
            ODSP_Relation__ = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

            if (ODSP_Relation__.detailRelation_ODSP == null)
            {
                ddl_add_rdept_idx.Items.Insert(0, new ListItem("เลือกฝ่าย", "0"));
            }
            else
            {

                ddl_add_rdept_idx.DataSource = ODSP_Relation__.detailRelation_ODSP;
                ddl_add_rdept_idx.DataTextField = "DeptNameTH";
                ddl_add_rdept_idx.DataValueField = "RDeptIDX";
                ddl_add_rdept_idx.DataBind();
                ddl_add_rdept_idx.Items.Insert(0, new ListItem("เลือกฝ่าย", "0"));
                ddl_add_rdept_idx.SelectedValue = SelectValue;

            }

        }
        else if (Type == 1)
        {
            ddl_add_rdept_idx.DataBind();
            ddl_add_rdept_idx.Items.Insert(0, new ListItem("เลือกฝ่าย", "0"));
        }
    }
    #endregion

    #region ddlchangDepartment_action
    protected void ddlchangDepartment_action(int Type, string SelectValue)
    {

        var ddlchangOrganization = (DropDownList)BoxRemovePosisionn.FindControl("ddlchangOrganization");
        var ddlchangDepartment = (DropDownList)BoxRemovePosisionn.FindControl("ddlchangDepartment");

        ddlchangDepartment.Items.Clear();

        if (Type == 0)
        {
            dataODSP_Relation ODSP_Relation__move = new dataODSP_Relation();
            var relation__move = new Relation_ODSP[1];
            relation__move[0] = new Relation_ODSP();
            relation__move[0].OrgIDX = Int32.Parse(ddlchangOrganization.SelectedValue);
            ODSP_Relation__move.detailRelation_ODSP = relation__move;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", ODSP_Relation__move, 200);
            ODSP_Relation__move = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

            if (ODSP_Relation__move.detailRelation_ODSP == null)
            {
                ddlchangDepartment.Items.Insert(0, new ListItem("เลือกฝ่าย", "0"));
            }
            else
            {

                ddlchangDepartment.DataSource = ODSP_Relation__move.detailRelation_ODSP;
                ddlchangDepartment.DataTextField = "DeptNameTH";
                ddlchangDepartment.DataValueField = "RDeptIDX";
                ddlchangDepartment.DataBind();
                ddlchangDepartment.Items.Insert(0, new ListItem("เลือกฝ่าย", "0"));
                ddlchangDepartment.SelectedValue = SelectValue;

            }

        }
        else if (Type == 1)
        {
            ddlchangDepartment.DataBind();
            ddlchangDepartment.Items.Insert(0, new ListItem("เลือกฝ่าย", "0"));
        }
    }
    #endregion

    #region ddlSelect_ddDepartment
    protected void ddlSelect_ddDepartment(int Type, string SelectValue)
    {

        var ddOrganiztion = (DropDownList)fvAddDataEmployee.FindControl("ddOrganiztion");
        var ddDepartment = (DropDownList)fvAddDataEmployee.FindControl("ddDepartment");

        ddDepartment.Items.Clear();

        if (Type == 0)
        {
            dataODSP_Relation ODSP_Relation = new dataODSP_Relation();
            var relation = new Relation_ODSP[1];
            relation[0] = new Relation_ODSP();
            relation[0].OrgIDX = Int32.Parse(ddOrganiztion.SelectedValue);
            ODSP_Relation.detailRelation_ODSP = relation;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Reration", "ODSP_Reletion", ODSP_Relation, 200);
            ODSP_Relation = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

            if (ODSP_Relation.detailRelation_ODSP == null)
            {
                ddDepartment.Items.Insert(0, new ListItem("เลือกฝ่าย", "0"));
            }
            else
            {

                ddDepartment.DataSource = ODSP_Relation.detailRelation_ODSP;
                ddDepartment.DataTextField = "DeptNameTH";
                ddDepartment.DataValueField = "RDeptIDX";
                ddDepartment.DataBind();
                ddDepartment.Items.Insert(0, new ListItem("เลือกฝ่าย", "0"));
                ddDepartment.SelectedValue = SelectValue;

            }

        }
        else if (Type == 1)
        {
            ddDepartment.DataBind();
            ddDepartment.Items.Insert(0, new ListItem("เลือกฝ่าย", "0"));
        }


        //return ddDepartment.SelectedItem.Value;
    }

    #endregion

    #region ddlchangPosision_action
    protected string ddlchangPosision_action(int Type, string SelectValue)
    {

        var ddlchangOrganization = (DropDownList)BoxRemovePosisionn.FindControl("ddlchangOrganization");
        var ddlchangDepartment = (DropDownList)BoxRemovePosisionn.FindControl("ddlchangDepartment");
        var ddlchangSection = (DropDownList)BoxRemovePosisionn.FindControl("ddlchangSection");
        var ddlchangPosision = (DropDownList)BoxRemovePosisionn.FindControl("ddlchangPosision");
        //var txtPresentAddress = (TextBox)fvAddDataEmployee.FindControl("txtPresentAddress");

        ddlchangPosision.Items.Clear();

        if (Type == 0)
        {
            dataODSP_Relation ODSP_Relation_Position = new dataODSP_Relation();
            var relation = new Relation_ODSP[1];
            relation[0] = new Relation_ODSP();
            relation[0].OrgIDX = Int32.Parse(ddlchangOrganization.SelectedValue);
            relation[0].RDeptIDX = Int32.Parse(ddlchangDepartment.SelectedValue);
            relation[0].RSecIDX = Int32.Parse(ddlchangSection.SelectedValue);
            ODSP_Relation_Position.detailRelation_ODSP = relation;


            _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", ODSP_Relation_Position, 200);
            ODSP_Relation_Position = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

            if (ODSP_Relation_Position.detailRelation_ODSP == null)
            {

                ddlchangPosision.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {
                ddlchangPosision.DataSource = ODSP_Relation_Position.detailRelation_ODSP;
                ddlchangPosision.DataTextField = "PosNameTH";
                ddlchangPosision.DataValueField = "RPosIDX";
                ddlchangPosision.DataBind();
                ddlchangPosision.Items.Insert(0, new ListItem("เลือกตำแหน่ง", "0"));
                ddlchangPosision.SelectedValue = SelectValue;

            }

        }
        else if (Type == 1)
        {
            ddlchangPosision.DataBind();
            ddlchangPosision.Items.Insert(0, new ListItem("เลือกตำแหน่ง", "0"));
        }


        return ddlchangPosision.SelectedItem.Value;
    }
    #endregion

    #region ddl_add_rpos_idx DataBind
    protected string ddl_rpos_action(int Type, string SelectValue)
    {

        var ddl_add_rdept_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_rdept_idx");
        var ddl_add_rsec_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_rsec_idx");
        var ddl_add_rpos_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_rpos_idx");
        var ddl_add_org_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_org_idx");
        //var txtPresentAddress = (TextBox)fvAddDataEmployee.FindControl("txtPresentAddress");

        ddl_add_rpos_idx.Items.Clear();

        if (Type == 0)
        {
            dataODSP_Relation ODSP_Relation_Position = new dataODSP_Relation();
            var relation = new Relation_ODSP[1];
            relation[0] = new Relation_ODSP();
            relation[0].OrgIDX = Int32.Parse(ddl_add_org_idx.SelectedValue);
            relation[0].RDeptIDX = Int32.Parse(ddl_add_rdept_idx.SelectedValue);
            relation[0].RSecIDX = Int32.Parse(ddl_add_rsec_idx.SelectedValue);
            ODSP_Relation_Position.detailRelation_ODSP = relation;


            _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", ODSP_Relation_Position, 200);
            ODSP_Relation_Position = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

            if (ODSP_Relation_Position.detailRelation_ODSP == null)
            {

                ddl_add_rpos_idx.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {
                ddl_add_rpos_idx.DataSource = ODSP_Relation_Position.detailRelation_ODSP;
                ddl_add_rpos_idx.DataTextField = "PosNameTH";
                ddl_add_rpos_idx.DataValueField = "RPosIDX";
                ddl_add_rpos_idx.DataBind();
                ddl_add_rpos_idx.Items.Insert(0, new ListItem("เลือกตำแหน่ง", "0"));
                ddl_add_rpos_idx.SelectedValue = SelectValue;

            }

        }
        else if (Type == 1)
        {
            ddl_add_rpos_idx.DataBind();
            ddl_add_rpos_idx.Items.Insert(0, new ListItem("เลือกตำแหน่ง", "0"));
        }


        return ddl_add_rpos_idx.SelectedItem.Value;
    }
    #endregion

    #region ddlchangSection_action
    protected string ddlchangSection_action(int Type, string SelectValue)
    {

        var ddlchangOrganization = (DropDownList)BoxRemovePosisionn.FindControl("ddlchangOrganization");
        var ddlchangDepartment = (DropDownList)BoxRemovePosisionn.FindControl("ddlchangDepartment");
        var ddlchangSection = (DropDownList)BoxRemovePosisionn.FindControl("ddlchangSection");

        ddlchangSection.Items.Clear();

        if (Type == 0)
        {
            dataODSP_Relation data_odsp2 = new dataODSP_Relation();
            var relation_box2 = new Relation_ODSP[1];
            relation_box2[0] = new Relation_ODSP();
            relation_box2[0].OrgIDX = Int32.Parse(ddlchangOrganization.SelectedValue);
            relation_box2[0].RDeptIDX = Int32.Parse(ddlchangDepartment.SelectedValue);
            data_odsp2.detailRelation_ODSP = relation_box2;


            _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", data_odsp2, 200);
            data_odsp2 = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

            if (data_odsp2.detailRelation_ODSP == null)
            {

                ddlchangSection.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {

                ddlchangSection.DataSource = data_odsp2.detailRelation_ODSP;
                ddlchangSection.DataTextField = "SecNameTH";
                ddlchangSection.DataValueField = "RSecIDX";
                ddlchangSection.DataBind();
                ddlchangSection.Items.Insert(0, new ListItem("เลือกแผนก", "0"));
                //ddlchangSection.SelectedValue = SelectValue;

            }

        }
        else if (Type == 1)
        {
            ddlchangSection.DataBind();
            ddlchangSection.Items.Insert(0, new ListItem("เลือกแผนก", "0"));
        }


        return ddlchangSection.SelectedItem.Value;
    }
    #endregion

    #region ddPositionAction DataBind
    protected string ddPositionAction(int Type, string SelectValue)
    {

        var ddDepartment = (DropDownList)fvAddDataEmployee.FindControl("ddDepartment");
        var ddDivision = (DropDownList)fvAddDataEmployee.FindControl("ddDivision");
        var ddPosition = (DropDownList)fvAddDataEmployee.FindControl("ddPosition");
        var ddOrganiztion = (DropDownList)fvAddDataEmployee.FindControl("ddOrganiztion");
        //var txtPresentAddress = (TextBox)fvAddDataEmployee.FindControl("txtPresentAddress");

        ddPosition.Items.Clear();

        if (Type == 0)
        {
            dataODSP_Relation ODSP_Relation_Position = new dataODSP_Relation();
            var relation = new Relation_ODSP[1];
            relation[0] = new Relation_ODSP();
            relation[0].OrgIDX = Int32.Parse(ddOrganiztion.SelectedValue);
            relation[0].RDeptIDX = Int32.Parse(ddDepartment.SelectedValue);
            relation[0].RSecIDX = Int32.Parse(ddDivision.SelectedValue);
            ODSP_Relation_Position.detailRelation_ODSP = relation;


            _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", ODSP_Relation_Position, 200);
            ODSP_Relation_Position = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

            if (ODSP_Relation_Position.detailRelation_ODSP == null)
            {

                ddPosition.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {
                ddPosition.DataSource = ODSP_Relation_Position.detailRelation_ODSP;
                ddPosition.DataTextField = "PosNameTH";
                ddPosition.DataValueField = "RPosIDX";
                ddPosition.DataBind();
                ddPosition.Items.Insert(0, new ListItem("เลือกตำแหน่ง", "0"));

            }

        }
        else if (Type == 1)
        {
            ddPosition.DataBind();
            ddPosition.Items.Insert(0, new ListItem("เลือกตำแหน่ง", "0"));
        }


        return ddPosition.SelectedItem.Value;
    }
    #endregion

    #region ddBloodgroupAction DataBind
    protected void ddlBloodgroupList()
    {


        DropDownList ddlBloodgroup = (DropDownList)fvAddDataEmployee.FindControl("ddlBloodgroup");
        ddlBloodgroup.AppendDataBoundItems = true;
        ddlBloodgroup.Items.Add(new ListItem("เลือกกรุ๊ปเลือด", "0"));

        data_presonal _data_pre = new data_presonal();
        DetailBloodGroup Bloodgroup = new DetailBloodGroup();
        _data_pre.BloodGroupList = new DetailBloodGroup[1];
        _data_pre.BloodGroupList[0] = Bloodgroup;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_pre, 216);
        _data_pre = (data_presonal)_funcTool.convertXmlToObject(typeof(data_presonal), _local_xml);


        ddlBloodgroup.DataSource = _data_pre.BloodGroupList;
        ddlBloodgroup.DataTextField = "BGName";
        ddlBloodgroup.DataValueField = "BGIDX";
        ddlBloodgroup.DataBind();
        ddlBloodgroup.Items.Add(new ListItem("ไม่ทราบกรุ๊ปเลือด", "5"));

    }

    protected void ddlBloodgroup_edit(int Type, string SelectValue)
    {

        //var fv_edit_Health = (FormView)fvEdit_dataEmployee.FindControl("fv_edit_Health");
        DropDownList ddBloodgroup_Edit = (DropDownList)fv_edit_Health.FindControl("ddBloodgroup_Edit");
        var txt_Bloodgroup_edit = (TextBox)fv_edit_Health.FindControl("txt_Bloodgroup_edit");
        if (Type == 0)
        {

            data_presonal _data_pre = new data_presonal();
            DetailBloodGroup Bloodgroup = new DetailBloodGroup();
            _data_pre.BloodGroupList = new DetailBloodGroup[1];
            _data_pre.BloodGroupList[0] = Bloodgroup;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_pre, 216);
            _data_pre = (data_presonal)_funcTool.convertXmlToObject(typeof(data_presonal), _local_xml);

            if (_data_pre.BloodGroupList == null)
            {

                ddBloodgroup_Edit.AppendDataBoundItems = true;
                ddBloodgroup_Edit.Items.Add(new ListItem("เลือกกรุ๊ปเลือด", "0"));
                ddBloodgroup_Edit.DataSource = _data_pre.BloodGroupList;
                ddBloodgroup_Edit.DataTextField = "BGName";
                ddBloodgroup_Edit.DataValueField = "BGIDX";
                ddBloodgroup_Edit.DataBind();
                ddBloodgroup_Edit.Items.Add(new ListItem("ไม่ทราบกรุ๊ปเลือด", "5"));
            }
            else
            {

                ddBloodgroup_Edit.AppendDataBoundItems = true;
                ddBloodgroup_Edit.Items.Add(new ListItem("เลือกกรุ๊ปเลือด", "0"));
                ddBloodgroup_Edit.DataSource = _data_pre.BloodGroupList;
                ddBloodgroup_Edit.DataTextField = "BGName";
                ddBloodgroup_Edit.DataValueField = "BGIDX";
                ddBloodgroup_Edit.DataBind();
                ddBloodgroup_Edit.Items.Add(new ListItem("ไม่ทราบกรุ๊ปเลือด", "5"));
                ddBloodgroup_Edit.SelectedValue = txt_Bloodgroup_edit.Text;

            }
        }
        else if (Type == 1)
        {
            ddBloodgroup_Edit.DataBind();
            ddBloodgroup_Edit.Items.Insert(0, new ListItem("เลือกกรุ๊ปเลือด", "0"));
            ddBloodgroup_Edit.Items.Add(new ListItem("ไม่ทราบกรุ๊ปเลือด", "5"));
        }

    }
    //}
    #endregion

    #region ddl_add_rsec_idx DataBind
    protected string ddl_rsec_action(int Type, string SelectValue)
    {

        var ddl_add_org_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_org_idx");
        var ddl_add_rdept_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_rdept_idx");
        var ddl_add_rsec_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_rsec_idx");

        ddl_add_rsec_idx.Items.Clear();

        if (Type == 0)
        {
            dataODSP_Relation data_odsp2 = new dataODSP_Relation();
            var relation_box2 = new Relation_ODSP[1];
            relation_box2[0] = new Relation_ODSP();
            relation_box2[0].OrgIDX = Int32.Parse(ddl_add_org_idx.SelectedValue);
            relation_box2[0].RDeptIDX = Int32.Parse(ddl_add_rdept_idx.SelectedValue);
            data_odsp2.detailRelation_ODSP = relation_box2;


            _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", data_odsp2, 200);
            data_odsp2 = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

            if (data_odsp2.detailRelation_ODSP == null)
            {

                ddl_add_rsec_idx.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {

                ddl_add_rsec_idx.DataSource = data_odsp2.detailRelation_ODSP;
                ddl_add_rsec_idx.DataTextField = "SecNameTH";
                ddl_add_rsec_idx.DataValueField = "RSecIDX";
                ddl_add_rsec_idx.DataBind();
                ddl_add_rsec_idx.Items.Insert(0, new ListItem("เลือกแผนก", "0"));
                ddl_add_rsec_idx.SelectedValue = SelectValue;

            }

        }
        else if (Type == 1)
        {
            ddl_add_rsec_idx.DataBind();
            ddl_add_rsec_idx.Items.Insert(0, new ListItem("เลือกแผนก", "0"));
        }


        return ddl_add_rsec_idx.SelectedItem.Value;
    }
    #endregion

    #region ddDivisionAction DataBind
    protected string ddDivisionAction(int Type, string SelectValue)
    {

        var ddOrganiztion = (DropDownList)fvAddDataEmployee.FindControl("ddOrganiztion");
        var ddDepartment = (DropDownList)fvAddDataEmployee.FindControl("ddDepartment");
        var ddDivision = (DropDownList)fvAddDataEmployee.FindControl("ddDivision");

        ddDivision.Items.Clear();

        if (Type == 0)
        {
            dataODSP_Relation ODSP_Relation = new dataODSP_Relation();
            var relation = new Relation_ODSP[1];
            relation[0] = new Relation_ODSP();
            relation[0].OrgIDX = Int32.Parse(ddOrganiztion.SelectedValue);
            relation[0].RDeptIDX = Int32.Parse(ddDepartment.SelectedValue);
            ODSP_Relation.detailRelation_ODSP = relation;


            _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Reration", "ODSP_Reletion", ODSP_Relation, 200);
            ODSP_Relation = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

            if (ODSP_Relation.detailRelation_ODSP == null)
            {

                ddDivision.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {

                ddDivision.DataSource = ODSP_Relation.detailRelation_ODSP;
                ddDivision.DataTextField = "SecNameTH";
                ddDivision.DataValueField = "RSecIDX";
                ddDivision.DataBind();
                ddDivision.Items.Insert(0, new ListItem("เลือกแผนก", "0"));
                ddDivision.SelectedValue = SelectValue;

            }

        }
        else if (Type == 1)
        {
            ddDivision.DataBind();
            ddDivision.Items.Insert(0, new ListItem("เลือกแผนก", "0"));
        }


        return ddDivision.SelectedItem.Value;
    }
    #endregion

    #endregion รวม Dropdownlist

    #region Gridview

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    #region row_editing

    protected void row_editing(object sender, GridViewEditEventArgs e)
    {
        var name_gv_editing = (GridView)sender;
        GridView gv_edit_position = (GridView)fvEdit_dataEmployee.FindControl("gv_edit_position");
        GridView gv_edit_Experience = (GridView)viewEdit_dataEmployee.FindControl("gv_edit_Experience");

        switch (name_gv_editing.ID)
        {
            case "gv_edit_position":

                gv_edit_position.EditIndex = e.NewEditIndex;
                gv_show_edit_position();
                break;

            case "gv_edit_Experience":

                gv_edit_Experience.EditIndex = e.NewEditIndex;
                gv_show_edit_experience();
                break;
        }

    }

    #endregion

    #region row_updating
    protected void row_updating(object sender, GridViewUpdateEventArgs e)
    {
        var name_gv_row_updating = (GridView)sender;
        GridView gv_edit_position = (GridView)fvEdit_dataEmployee.FindControl("gv_edit_position");
        GridView gv_edit_Experience = (GridView)viewEdit_dataEmployee.FindControl("gv_edit_Experience");

        switch (name_gv_row_updating.ID)
        {
            case "gv_edit_position":

                int txtposition_idx = Convert.ToInt32(gv_edit_position.DataKeys[e.RowIndex].Values[0].ToString());
                var name_organization_update = (DropDownList)gv_edit_position.Rows[e.RowIndex].FindControl("ddl_edit_org_idx");
                var name_department_update = (DropDownList)gv_edit_position.Rows[e.RowIndex].FindControl("ddl_edit_department");
                var name_section_update = (DropDownList)gv_edit_position.Rows[e.RowIndex].FindControl("ddl_edit_section");
                var name_position_update = (DropDownList)gv_edit_position.Rows[e.RowIndex].FindControl("ddl_edit_position");
                var status_position_update = (DropDownList)gv_edit_position.Rows[e.RowIndex].FindControl("ddl_edit_status_position");

                gv_edit_position.EditIndex = -1;

                data_employee position_data = new data_employee();
                employee_detail edit_position = new employee_detail();
                position_data.employee_list = new employee_detail[1];

                edit_position.idx_position = txtposition_idx;
                edit_position.emp_idx = int.Parse(ViewState["emp_idx"].ToString());
                edit_position.org_idx = int.Parse(name_organization_update.SelectedValue);
                edit_position.rdept_idx = int.Parse(name_department_update.SelectedValue);
                edit_position.rsec_idx = int.Parse(name_section_update.SelectedValue);
                edit_position.rpos_idx = int.Parse(name_position_update.SelectedValue);
                edit_position.status_position = int.Parse(status_position_update.SelectedValue);
                edit_position.emp_editor = (int)Session["emp_idx"];

                position_data.employee_list[0] = edit_position;

                _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", position_data, 303);
                position_data = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
                ViewState["returnCode_positionEdit"] = position_data.return_value;

                if (position_data.return_value == "01")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลสำเร็จ');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('!! ผิดพลาด บันทึกข้อมูลไม่สำเร็จ');", true);
                }
                gv_show_edit_position();

                break;

            case "gv_edit_Experience":

                int id_table_exper = Convert.ToInt32(gv_edit_Experience.DataKeys[e.RowIndex].Values[0].ToString());
                var txt_name_company_edit = (TextBox)gv_edit_Experience.Rows[e.RowIndex].FindControl("txt_name_company_edit");
                var txt_position_old_edit = (TextBox)gv_edit_Experience.Rows[e.RowIndex].FindControl("txt_position_old_edit");
                var txt_job_brief_details_edit = (TextBox)gv_edit_Experience.Rows[e.RowIndex].FindControl("txt_job_brief_details_edit");
                var txt_edit_salary_old_company = (TextBox)gv_edit_Experience.Rows[e.RowIndex].FindControl("txt_edit_salary_old_company");
                var txt_workIn_old_edit = (TextBox)gv_edit_Experience.Rows[e.RowIndex].FindControl("txt_workIn_old_edit");
                var txt_motive_edit = (TextBox)gv_edit_Experience.Rows[e.RowIndex].FindControl("txt_motive_edit");
                var txt_resign_old_edit = (TextBox)gv_edit_Experience.Rows[e.RowIndex].FindControl("txt_resign_old_edit");

                gv_edit_position.EditIndex = -1;

                data_employee experience_data = new data_employee();
                experiencelist edit_experience = new experiencelist();
                experience_data.box_experience_ = new experiencelist[1];

                edit_experience.exper_idx_ = id_table_exper;
                edit_experience.emp_idx_ = int.Parse(ViewState["emp_idx"].ToString());
                edit_experience.job__details = txt_job_brief_details_edit.Text;
                edit_experience.name_company = txt_name_company_edit.Text;
                edit_experience.position_old_company = txt_position_old_edit.Text;
                edit_experience.motive_resign = txt_motive_edit.Text;
                edit_experience.salary_old_company = txt_edit_salary_old_company.Text;
                edit_experience.workIn_old_company = txt_workIn_old_edit.Text;
                edit_experience.resign_old_company = txt_resign_old_edit.Text;

                experience_data.box_experience_[0] = edit_experience;
                _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", experience_data, 304);
                experience_data = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
                ViewState["returnCode_experienceEdit"] = experience_data.return_value;

                if (experience_data.return_value == "01")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('บันทึกข้อมูลสำเร็จ');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('!! ผิดพลาด บันทึกข้อมูลไม่สำเร็จ');", true);
                }

                gv_show_edit_experience();

                break;
        }
    }

    #endregion

    #region row_canceling_edit

    protected void row_canceling_edit(object sender, GridViewCancelEditEventArgs e)
    {
        var name_row_canceling_edit = (GridView)sender;
        GridView gv_edit_position = (GridView)fvEdit_dataEmployee.FindControl("gv_edit_position");
        GridView gv_edit_Experience = (GridView)viewEdit_dataEmployee.FindControl("gv_edit_Experience");

        switch (name_row_canceling_edit.ID)
        {
            case "gv_edit_position":
                gv_edit_position.EditIndex = -1;
                gv_show_edit_position();

                break;

            case "gv_edit_Experience":
                gv_edit_Experience.EditIndex = -1;
                gv_show_edit_experience();

                break;
        }

    }

    #endregion

    #region row_DataBound
    protected void row_DataBound(object sender, GridViewRowEventArgs e)
    {
        var name_row_DataBound = (GridView)sender;

        switch (name_row_DataBound.ID)
        {
            case "gv_edit_position":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";

                    TextBox txt_org_idx_update = (TextBox)e.Row.FindControl("txt_org_idx_update");
                    TextBox txt_department_edit = (TextBox)e.Row.FindControl("txt_department_edit");
                    TextBox txt_section_edit = (TextBox)e.Row.FindControl("txt_section_edit");
                    TextBox txt_position_edit = (TextBox)e.Row.FindControl("txt_position_edit");

                    DropDownList ddl_edit_org_idx = (DropDownList)e.Row.FindControl("ddl_edit_org_idx");
                    DropDownList ddl_edit_department = (DropDownList)e.Row.FindControl("ddl_edit_department");
                    DropDownList ddl_edit_section = (DropDownList)e.Row.FindControl("ddl_edit_section");
                    DropDownList ddl_edit_position = (DropDownList)e.Row.FindControl("ddl_edit_position");

                    dataODSP_Relation ODSP_Relation1 = new dataODSP_Relation();
                    Relation_ODSP relation = new Relation_ODSP();
                    ODSP_Relation1.detailRelation_ODSP = new Relation_ODSP[1];

                    relation.OrgIDX = 0;
                    ODSP_Relation1.detailRelation_ODSP[0] = relation;

                    _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", ODSP_Relation1, 200);
                    ODSP_Relation1 = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

                    ddl_edit_org_idx.Items.Insert(0, new ListItem("เลือกบริษัท", "00"));
                    ddl_edit_org_idx.DataSource = ODSP_Relation1.detailRelation_ODSP;
                    ddl_edit_org_idx.DataTextField = "OrgNameTH";
                    ddl_edit_org_idx.DataValueField = "OrgIDX";
                    ddl_edit_org_idx.DataBind();
                    ddl_edit_org_idx.SelectedValue = txt_org_idx_update.Text;


                    dataODSP_Relation ODSP_Relation = new dataODSP_Relation();
                    var relation1 = new Relation_ODSP[1];
                    relation1[0] = new Relation_ODSP();
                    relation1[0].OrgIDX = Int32.Parse(ddl_edit_org_idx.SelectedValue);
                    ODSP_Relation.detailRelation_ODSP = relation1;

                    _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", ODSP_Relation, 200);
                    ODSP_Relation = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

                    ddl_edit_department.DataSource = ODSP_Relation.detailRelation_ODSP;
                    ddl_edit_department.DataTextField = "DeptNameTH";
                    ddl_edit_department.DataValueField = "RDeptIDX";
                    ddl_edit_department.DataBind();
                    ddl_edit_department.Items.Insert(0, new ListItem("เลือกฝ่าย", "0"));
                    ddl_edit_department.SelectedValue = txt_department_edit.Text;


                    dataODSP_Relation ODSP_Relation2 = new dataODSP_Relation();
                    var relation2 = new Relation_ODSP[1];
                    relation2[0] = new Relation_ODSP();
                    ddl_edit_section.Items.Clear();

                    relation2[0].OrgIDX = Int32.Parse(ddl_edit_org_idx.SelectedValue);
                    relation2[0].RDeptIDX = Int32.Parse(ddl_edit_department.SelectedValue);
                    ODSP_Relation2.detailRelation_ODSP = relation2;

                    _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", ODSP_Relation2, 200);
                    ODSP_Relation2 = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

                    ddl_edit_section.DataSource = ODSP_Relation2.detailRelation_ODSP;
                    ddl_edit_section.DataTextField = "SecNameTH";
                    ddl_edit_section.DataValueField = "RSecIDX";
                    ddl_edit_section.DataBind();
                    ddl_edit_section.Items.Insert(0, new ListItem("เลือกแผนก", "0"));
                    ddl_edit_section.SelectedValue = txt_section_edit.Text;

                    dataODSP_Relation ODSP_Relation3 = new dataODSP_Relation();
                    var relation3 = new Relation_ODSP[1];
                    relation3[0] = new Relation_ODSP();

                    ddl_edit_position.Items.Clear();

                    relation3[0].OrgIDX = Int32.Parse(ddl_edit_org_idx.SelectedValue);
                    relation3[0].RDeptIDX = Int32.Parse(ddl_edit_department.SelectedValue);
                    relation3[0].RSecIDX = Int32.Parse(ddl_edit_section.SelectedValue);
                    ODSP_Relation3.detailRelation_ODSP = relation3;


                    _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", ODSP_Relation3, 200);
                    ODSP_Relation3 = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);


                    ddl_edit_position.DataSource = ODSP_Relation3.detailRelation_ODSP;
                    ddl_edit_position.DataTextField = "PosNameTH";
                    ddl_edit_position.DataValueField = "RPosIDX";
                    ddl_edit_position.DataBind();
                    ddl_edit_position.Items.Insert(0, new ListItem("เลือกตำแหน่ง", "0"));
                    ddl_edit_position.SelectedValue = txt_position_edit.Text;

                }

                break;

            case "gv_edit_Experience":

                if (e.Row.RowState.ToString().Contains("Edit"))
                {
                    GridView editGrid = sender as GridView;
                    int colSpan = editGrid.Columns.Count;
                    for (int i = 1; i < colSpan; i++)
                    {
                        e.Row.Cells[i].Visible = false;
                        e.Row.Cells[i].Controls.Clear();
                    }

                    e.Row.Cells[0].Attributes["ColSpan"] = (colSpan).ToString();
                    e.Row.Cells[0].CssClass = "";
                }

                break;
        }
    }

    #endregion

    #region GvPageIndexChanging


    protected void Master_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        var gv_Name = (GridView)sender;

        switch (gv_Name.ID)
        {
            case "gvEmployee":

                gvEmployee.PageIndex = e.NewPageIndex;
                gvEmployee.DataBind();
                SelectData_Employee();

                if (ddl_Select_Organization_search.SelectedValue == "0"
                    && ddl_Select_Department_search.SelectedValue == "0"
                    && txt_search.Text == "")
                {
                    SelectData_Employee();
                }
                else
                {
                    data_employee Search_data_employee = new data_employee();
                    employee_detail searching = new employee_detail();
                    Search_data_employee.employee_list = new employee_detail[1];

                    searching.org_idx = int.Parse(ddl_Select_Organization_search.SelectedValue);
                    searching.rdept_idx = int.Parse(ddl_Select_Department_search.SelectedValue);
                    searching.Search = txt_search.Text;

                    Search_data_employee.employee_list[0] = searching;

                    _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", Search_data_employee, 222);
                    Search_data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

                    gvEmployee.DataSource = Search_data_employee.employee_list;
                    gvEmployee.DataBind();

                }

                break;

            case "gvListNameShiftTime":
                GridView gvListNameShiftTime = (GridView)ViewSetShiftTime.FindControl("gvListNameShiftTime");
                gvListNameShiftTime.PageIndex = e.NewPageIndex;
                gvListNameShiftTime.DataBind();
                select_listemp_setshiftTime();

                break;

            case "gvAddPosision":
                GridView gvAddPosision = (GridView)fvAddDataEmployee.FindControl("gvAddPosision");
                gvAddPosision.PageIndex = e.NewPageIndex;
                gvAddPosision.DataBind();


                break;

            case "gvExperience":
                GridView gvExperience = (GridView)fvAddDataEmployee.FindControl("gvExperience");
                gvExperience.PageIndex = e.NewPageIndex;
                gvExperience.DataBind();

                break;

            case "gv_edit_position":
                GridView gv_edit_position = (GridView)fvEdit_dataEmployee.FindControl("gv_edit_position");
                gv_edit_position.PageIndex = e.NewPageIndex;
                gv_edit_position.DataBind();
                gv_show_edit_position();
                break;

            case "gv_edit_Experience":

                //   GridView gv_edit_Experience = (GridView)viewEdit_dataEmployee.FindControl("gv_edit_Experience");
                gv_edit_Experience.PageIndex = e.NewPageIndex;
                gv_edit_Experience.DataBind();
                gv_show_edit_experience();
                break;
        }
    }

    #endregion

    #region GvRowDataBound

    protected void Master_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        var Gv_Name = (GridView)sender;


        switch (Gv_Name.ID)
        {
            case "gvEmployee":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //LinkButton lbView = (LinkButton)e.Row.Cells[4].FindControl("lbView");

                    //Label label_statusemp = (Label)e.Row.Cells[3].FindControl("label_statusemp");
                    //Label status_offline = (Label)e.Row.Cells[3].FindControl("status_offline");
                    //Label status_online = (Label)e.Row.Cells[3].FindControl("status_online");
                    //ViewState["status_employee"] = label_statusemp.Text;

                    //if (ViewState["status_employee"].ToString() == "9")
                    //{

                    //    status_offline.Visible = true;

                    //}
                    //else if (ViewState["status_employee"].ToString() != "9")

                    //{
                    //    status_online.Visible = true;

                    //}

                    ////องค์กร
                    //Label LabelshiftOrgtIDX = (Label)e.Row.Cells[2].FindControl("LabelshiftOrgtIDX");
                    //Label lborgNameShiftTime = (Label)e.Row.Cells[2].FindControl("lborgNameShiftTime");
                    //Label labeltextOrgshiftdefault = (Label)e.Row.Cells[2].FindControl("labeltextOrgshiftdefault");

                    ////ฝ่าย
                    //Label lbdeptNameShiftTime = (Label)e.Row.Cells[2].FindControl("lbdeptNameShiftTime");
                    //Label labeltextDeptshiftdefault = (Label)e.Row.Cells[2].FindControl("labeltextDeptshiftdefault");
                    //Label LabelshiftDeptIDX = (Label)e.Row.Cells[2].FindControl("LabelshiftDeptIDX");

                    ////แผนก
                    //Label lbsecNameShiftTime = (Label)e.Row.Cells[2].FindControl("lbsecNameShiftTime");
                    //Label labeltextSecshiftdefault = (Label)e.Row.Cells[2].FindControl("labeltextSecshiftdefault");
                    //Label LabelshiftSecIDX = (Label)e.Row.Cells[2].FindControl("LabelshiftSecIDX");

                    ////ตำแหน่ง
                    //Label lbPossitionNameShiftTime = (Label)e.Row.Cells[2].FindControl("lbPossitionNameShiftTime");
                    //Label labeltextPosShiftdefault = (Label)e.Row.Cells[2].FindControl("labeltextPosShiftdefault");
                    //Label LabelshiftPosIDX = (Label)e.Row.Cells[2].FindControl("LabelshiftPosIDX");

                    ////ข้อมูลกะ
                    //Label typeShiftTime = (Label)e.Row.Cells[3].FindControl("typeShiftTime");
                    //Label haveShiftTime = (Label)e.Row.Cells[3].FindControl("haveShiftTime");
                    //Label NoneShiftTime = (Label)e.Row.Cells[3].FindControl("NoneShiftTime");
                    //// Label LabelshiftPosIDX = (Label)e.Row.Cells[2].FindControl("LabelshiftPosIDX");


                  //  ViewState["orgShiftTimeIDX"] = LabelshiftOrgtIDX.Text;
                  //  ViewState["deptShiftTimeIDX"] = LabelshiftDeptIDX.Text;
                  //  ViewState["SecShiftTimeIDX"] = LabelshiftSecIDX.Text;
                  //  ViewState["PosShiftTimeIDX"] = LabelshiftPosIDX.Text;
                  ////  ViewState["checkValue_TypeshiftTime"] = typeShiftTime.Text;

                  //  if ((int.Parse(ViewState["orgShiftTimeIDX"].ToString())) == 0 || ViewState["orgShiftTimeIDX"].ToString() == null)
                  //  {
                  //      labeltextOrgshiftdefault.Visible = true;
                  //      lborgNameShiftTime.Visible = false;

                  //  }
                  //  else
                  //  {
                  //      labeltextOrgshiftdefault.Visible = false;
                  //      lborgNameShiftTime.Visible = true;

                  //  }
                  //  if ((int.Parse(ViewState["deptShiftTimeIDX"].ToString())) == 0 || ViewState["deptShiftTimeIDX"].ToString() == null)
                  //  {
                  //      labeltextDeptshiftdefault.Visible = true;
                  //      lbdeptNameShiftTime.Visible = false;
                  //  }
                  //  else
                  //  {
                  //      labeltextDeptshiftdefault.Visible = false;
                  //      lbdeptNameShiftTime.Visible = true;
                  //  }
                  //  if ((int.Parse(ViewState["SecShiftTimeIDX"].ToString())) == 0 || ViewState["SecShiftTimeIDX"].ToString() == null)
                  //  {
                  //      labeltextSecshiftdefault.Visible = true;
                  //      lbsecNameShiftTime.Visible = false;
                  //  }
                  //  else
                  //  {
                  //      labeltextSecshiftdefault.Visible = false;
                  //      lbsecNameShiftTime.Visible = true;
                  //  }
                  //  if ((int.Parse(ViewState["PosShiftTimeIDX"].ToString())) == 0 || ViewState["PosShiftTimeIDX"].ToString() == null)
                  //  {
                  //      labeltextPosShiftdefault.Visible = true;
                  //      lbPossitionNameShiftTime.Visible = false;
                  //  }
                  //  else
                  //  {
                  //      labeltextPosShiftdefault.Visible = false;
                  //      lbPossitionNameShiftTime.Visible = true;
                  //  }
                  //  //if (int.Parse(ViewState["checkValue_TypeshiftTime"].ToString()) == 0)
                  //  //{

                  //  //    NoneShiftTime.Visible = true;
                  //  //    haveShiftTime.Visible = false;

                  //  //}
                  //  //else
                  //  //{
                  //  //    NoneShiftTime.Visible = false;
                  //  //    haveShiftTime.Visible = true;
                  //  //}

                }

                break;


            case "gvListNameShiftTime":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //องค์กร
                    Label LabelshiftOrgtIDX = (Label)e.Row.Cells[2].FindControl("LabelshiftOrgtIDX");
                    Label lborgNameShiftTime = (Label)e.Row.Cells[2].FindControl("lborgNameShiftTime");
                    Label labeltextOrgshiftdefault = (Label)e.Row.Cells[2].FindControl("labeltextOrgshiftdefault");

                    //ฝ่าย
                    Label lbdeptNameShiftTime = (Label)e.Row.Cells[2].FindControl("lbdeptNameShiftTime");
                    Label labeltextDeptshiftdefault = (Label)e.Row.Cells[2].FindControl("labeltextDeptshiftdefault");
                    Label LabelshiftDeptIDX = (Label)e.Row.Cells[2].FindControl("LabelshiftDeptIDX");

                    //แผนก
                    Label lbsecNameShiftTime = (Label)e.Row.Cells[2].FindControl("lbsecNameShiftTime");
                    Label labeltextSecshiftdefault = (Label)e.Row.Cells[2].FindControl("labeltextSecshiftdefault");
                    Label LabelshiftSecIDX = (Label)e.Row.Cells[2].FindControl("LabelshiftSecIDX");

                    //ตำแหน่ง
                    Label lbPossitionNameShiftTime = (Label)e.Row.Cells[2].FindControl("lbPossitionNameShiftTime");
                    Label labeltextPosShiftdefault = (Label)e.Row.Cells[2].FindControl("labeltextPosShiftdefault");
                    Label LabelshiftPosIDX = (Label)e.Row.Cells[2].FindControl("LabelshiftPosIDX");

                    //ข้อมูลกะ
                    Label typeShiftTime = (Label)e.Row.Cells[3].FindControl("typeShiftTime");
                    Label haveShiftTime = (Label)e.Row.Cells[3].FindControl("haveShiftTime");
                    Label NoneShiftTime = (Label)e.Row.Cells[3].FindControl("NoneShiftTime");
                    // Label LabelshiftPosIDX = (Label)e.Row.Cells[2].FindControl("LabelshiftPosIDX");


                    ViewState["orgShiftTimeIDX"] = LabelshiftOrgtIDX.Text;
                    ViewState["deptShiftTimeIDX"] = LabelshiftDeptIDX.Text;
                    ViewState["SecShiftTimeIDX"] = LabelshiftSecIDX.Text;
                    ViewState["PosShiftTimeIDX"] = LabelshiftPosIDX.Text;
                    ViewState["checkValue_TypeshiftTime"] = typeShiftTime.Text;

                    if ((int.Parse(ViewState["orgShiftTimeIDX"].ToString())) == 0 || ViewState["orgShiftTimeIDX"].ToString() == null)
                    {
                        labeltextOrgshiftdefault.Visible = true;
                        lborgNameShiftTime.Visible = false;

                    }
                    else
                    {
                        labeltextOrgshiftdefault.Visible = false;
                        lborgNameShiftTime.Visible = true;

                    }
                    if ((int.Parse(ViewState["deptShiftTimeIDX"].ToString())) == 0 || ViewState["deptShiftTimeIDX"].ToString() == null)
                    {
                        labeltextDeptshiftdefault.Visible = true;
                        lbdeptNameShiftTime.Visible = false;
                    }
                    else
                    {
                        labeltextDeptshiftdefault.Visible = false;
                        lbdeptNameShiftTime.Visible = true;
                    }
                    if ((int.Parse(ViewState["SecShiftTimeIDX"].ToString())) == 0 || ViewState["SecShiftTimeIDX"].ToString() == null)
                    {
                        labeltextSecshiftdefault.Visible = true;
                        lbsecNameShiftTime.Visible = false;
                    }
                    else
                    {
                        labeltextSecshiftdefault.Visible = false;
                        lbsecNameShiftTime.Visible = true;
                    }
                    if ((int.Parse(ViewState["PosShiftTimeIDX"].ToString())) == 0 || ViewState["PosShiftTimeIDX"].ToString() == null)
                    {
                        labeltextPosShiftdefault.Visible = true;
                        lbPossitionNameShiftTime.Visible = false;
                    }
                    else
                    {
                        labeltextPosShiftdefault.Visible = false;
                        lbPossitionNameShiftTime.Visible = true;
                    }
                    if (int.Parse(ViewState["checkValue_TypeshiftTime"].ToString()) == 0)
                    {

                        NoneShiftTime.Visible = true;
                        haveShiftTime.Visible = false;

                    }
                    else
                    {
                        NoneShiftTime.Visible = false;
                        haveShiftTime.Visible = true;
                    }


                }
                break;

            case "gvAddPosision":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {


                }

                break;
            case "gvExperience":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                }

                break;

            case "gvpicture":

                if (e.Row.RowType == DataControlRowType.DataRow)

                {
                    Image ImagProfile = (Image)e.Row.Cells[0].FindControl("ImagProfile");
                    Panel ImagProfile_defaultedit = (Panel)e.Row.Cells[0].FindControl("ImagProfile_defaultedit");
                    Repeater Repeater_pictureProfile = (Repeater)e.Row.Cells[0].FindControl("Repeater_pictureProfile");
                    TextBox txtemp_code = (TextBox)e.Row.Cells[0].FindControl("txtemp_code");

                    ViewState["emp_code_path_picture"] = txtemp_code.Text;

                    string getPath_picture = ConfigurationSettings.AppSettings["upload_profile_picture"];

                    if (Directory.Exists(Server.MapPath(getPath_picture + ViewState["emp_code_path_picture"].ToString())))
                    {

                        data_employee edit_employee_data = new data_employee();
                        employee_detail Editdataemployee = new employee_detail();
                        edit_employee_data.employee_list = new employee_detail[1];

                        Editdataemployee.emp_idx = (int)ViewState["emp_idx"];
                        edit_employee_data.employee_list[0] = Editdataemployee;

                        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", edit_employee_data, 212);
                        edit_employee_data = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

                        Repeater_pictureProfile.DataSource = edit_employee_data.employee_list;
                        Repeater_pictureProfile.DataBind();

                        ImagProfile_defaultedit.Visible = false;
                        // check_XML.Text = "เข้ามีภาพ";

                    }
                    else
                    {
                        Repeater_pictureProfile.Visible = false;
                        ImagProfile_defaultedit.Visible = true;
                        // check_XML.Text = "เข้าไม่มีภาพ";
                    }


                }
                break;

            case "gridviewHistory":


                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    GridView gvHistory = (GridView)fvEdit_dataEmployee.FindControl("gvHistory");
                    HyperLink fileDocument = (HyperLink)e.Row.Cells[7].FindControl("fileDocument");  //Cells[3] เลือกเซลล์ว่าช่องที่ต้องการแสดงอยู่คอลัมน์ไหน
                    TextBox valueDocumentCode = (TextBox)e.Row.Cells[7].FindControl("valueDocumentCode");
                    ViewState["CodeDocument"] = valueDocumentCode.Text;

                    //e.Row.Cells[1].Attributes.Add("colspan", "3");


                    string getPathFileDocument = ConfigurationSettings.AppSettings["upload_penalty_file"];

                    if (ViewState["CodeDocument"].ToString() != "" && ViewState["CodeDocument"].ToString() != null)
                    {
                        if (Directory.Exists(Server.MapPath(getPathFileDocument + ViewState["CodeDocument"].ToString())))
                        {
                            fileDocument.Visible = true;
                            fileDocument.NavigateUrl = getPathFileDocument + ViewState["CodeDocument"].ToString() + "/" + ViewState["CodeDocument"].ToString() + ".pdf";//LinkHost11 + MapURL(hidFile11.Value);
                        }
                        else
                        {
                            fileDocument.Visible = false;
                        }

                    }
                    else if (ViewState["CodeDocument"].ToString() == "" && ViewState["CodeDocument"].ToString() == null)
                    {
                        fileDocument.Visible = false;
                    }
                }
                break;
        }

    }

    #endregion

    protected void Gv_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "gvAddPosision":
                GridView gvAddPosision = (GridView)fvAddDataEmployee.FindControl("gvAddPosision");
                var DeletePosisionRow = (DataSet)ViewState["Create_DataPosision"];
                var drDriving = DeletePosisionRow.Tables[0].Rows;

                drDriving.RemoveAt(e.RowIndex);

                ViewState["Create_DataPosision"] = DeletePosisionRow;
                gvAddPosision.EditIndex = -1;
                gvAddPosision.DataSource = ViewState["Create_DataPosision"];
                gvAddPosision.DataBind();

                break;

            case "gvExperience":
                GridView gvExperience = (GridView)fvAddDataEmployee.FindControl("gvExperience");
                var Delete_Experience = (DataSet)ViewState["Create_table_Experience"];
                var drExperience = Delete_Experience.Tables[0].Rows;

                drExperience.RemoveAt(e.RowIndex);

                ViewState["Create_table_Experience"] = Delete_Experience;
                gvExperience.EditIndex = -1;
                gvExperience.DataSource = ViewState["Create_table_Experience"];
                gvExperience.DataBind();

                break;
        }


    }


    #endregion

    #region RadioSelected_insert_IndexChanged
    protected void RadioSelected_insert_IndexChanged(object sender, EventArgs e)
    {
        var RadioCheck_insert = (RadioButton)sender;
        var formview_shiftTime = (FormView)fvAddDataEmployee.FindControl("formview_shiftTime");
        var checkstaticShift = (RadioButton)formview_shiftTime.FindControl("checkstaticShift");
        var checkDynamicShift = (RadioButton)formview_shiftTime.FindControl("checkDynamicShift");
        var ddlist_ShiftTime = (DropDownList)formview_shiftTime.FindControl("ddlist_ShiftTime");


        switch (RadioCheck_insert.ID)
        {
            case "checkstaticShift":

                if (checkstaticShift.Checked)
                {
                    ddlist_ShiftTime.Enabled = true;
                    checkDynamicShift.Checked = false;
                }
                else
                {
                    ddlist_ShiftTime.Enabled = false;
                    checkstaticShift.Checked = false;
                }

                break;

            case "checkDynamicShift":

                if (checkDynamicShift.Checked)
                {
                    ddlist_ShiftTime.Enabled = false;
                    checkstaticShift.Checked = false;
                }
                else
                {
                    ddlist_ShiftTime.Enabled = false;
                    checkstaticShift.Checked = false;
                }

                break;
        }




    }
    #endregion

    #region RadioSelectedIndexChanged
    protected void RadioSelectedIndexChanged(object sender, EventArgs e)
    {
        //edit
        var RadioCheck = (RadioButton)sender;
        var editformview_shiftTime = (FormView)fvEdit_dataEmployee.FindControl("editformview_shiftTime");
        var checkstaticShift_edit = (RadioButton)editformview_shiftTime.FindControl("checkstaticShift_edit");
        var checkDynamicShift_edit = (RadioButton)editformview_shiftTime.FindControl("checkDynamicShift_edit");
        var ddlist_edit_ShiftTime = (DropDownList)editformview_shiftTime.FindControl("ddlist_edit_ShiftTime");

        switch (RadioCheck.ID)
        {

            //edit

            case "checkstaticShift_edit":
                if (checkstaticShift_edit.Checked)
                {
                    ddlist_edit_ShiftTime.Enabled = true;
                    checkDynamicShift_edit.Checked = false;

                }
                else
                {

                    ddlist_edit_ShiftTime.Enabled = false;
                    checkDynamicShift_edit.Checked = false;
                }

                break;

            case "checkDynamicShift_edit":
                if (checkDynamicShift_edit.Checked)
                {
                    ddlist_edit_ShiftTime.Enabled = false;
                    checkstaticShift_edit.Checked = false;
                }
                else
                {
                    ddlist_edit_ShiftTime.Enabled = false;
                    checkstaticShift_edit.Checked = false;
                }

                break;
        }
    }

    #endregion

    #region checkBox

    protected void chkSelectedIndexChanged(object sender, EventArgs e)
    {
        var check_box_ = (CheckBox)sender;
        CheckBox chk_have_Experience = (CheckBox)fvAddDataEmployee.FindControl("chk_have_Experience");
        CheckBox chk_nothave_Experience = (CheckBox)fvAddDataEmployee.FindControl("chk_nothave_Experience");
        CheckBox chk_probation = (CheckBox)fvEdit_dataEmployee.FindControl("chk_probation");
        CheckBox chk_pass_probation = (CheckBox)fvEdit_dataEmployee.FindControl("chk_pass_probation");
        CheckBox chk_termination = (CheckBox)fvEdit_dataEmployee.FindControl("chk_termination");
        Panel BoxAddExperience = (Panel)fvAddDataEmployee.FindControl("BoxAddExperience");
        LinkButton BtnSave = (LinkButton)fvAddDataEmployee.FindControl("BtnSave");
        LinkButton BtnCancel = (LinkButton)fvAddDataEmployee.FindControl("BtnCancel");

        var formview_shiftTime = (FormView)fvAddDataEmployee.FindControl("formview_shiftTime");
        var checkstaticShift = (CheckBox)formview_shiftTime.FindControl("checkstaticShift");
        var checkDynamicShift = (CheckBox)formview_shiftTime.FindControl("checkDynamicShift");
        var ddlist_ShiftTime = (DropDownList)formview_shiftTime.FindControl("ddlist_ShiftTime");


        switch (check_box_.ID)
        {
            case "chk_have_Experience":


                if (chk_have_Experience.Checked)
                {
                    BoxAddExperience.Visible = true;
                    chk_nothave_Experience.Checked = false;
                    BoxAddExperience.Focus();
                    BtnSave.Visible = false;
                    BtnCancel.Visible = false;
                }
                else
                {
                    BoxAddExperience.Visible = false;
                    chk_nothave_Experience.Checked = true;
                    BtnSave.Visible = true;
                    BtnCancel.Visible = true;
                }

                break;

            case "chk_nothave_Experience":

                if (chk_nothave_Experience.Checked)
                {
                    BoxAddExperience.Visible = false;
                    chk_have_Experience.Checked = false;
                    BtnSave.Visible = true;
                    BtnCancel.Visible = true;
                }
                else
                {
                    BoxAddExperience.Visible = true;
                    chk_have_Experience.Checked = true;
                    BtnSave.Visible = false;
                    BtnCancel.Visible = false;
                }

                break;

            case "chk_probation":

                if (chk_probation.Checked)
                {
                    chk_probation.Checked = true;
                    chk_pass_probation.Checked = false;
                    chk_termination.Checked = false;
                    var box_probation_date = (Panel)fvEdit_dataEmployee.FindControl("box_probation_date");
                    box_probation_date.Visible = false;

                    var box_probation_edit = (Panel)fvEdit_dataEmployee.FindControl("box_probation_edit");
                    box_probation_edit.Visible = false;

                }

                break;

            case "chk_pass_probation":

                if (chk_pass_probation.Checked)
                {
                    chk_termination.Checked = false;
                    chk_probation.Checked = false;
                    chk_pass_probation.Checked = true;
                    var box_probation_edit = (Panel)fvEdit_dataEmployee.FindControl("box_probation_edit");
                    box_probation_edit.Visible = true;
                }

                break;

            case "chk_termination":

                if (chk_termination.Checked)
                {
                    var box_probation_date = (Panel)fvEdit_dataEmployee.FindControl("box_probation_date");
                    var box_probation_edit = (Panel)fvEdit_dataEmployee.FindControl("box_probation_edit");
                    var box_add_end_date = (Panel)fvEdit_dataEmployee.FindControl("box_add_end_date");
                    chk_termination.Checked = true;
                    chk_probation.Checked = false;
                    chk_pass_probation.Checked = false;

                    box_probation_date.Visible = true;
                    box_probation_edit.Visible = false;
                    box_add_end_date.Visible = true;
                }

                break;
        }
    }
    #endregion

    #region FormView_DataBound
    protected void FormView_DataBound(object sender, EventArgs e)
    {

        var fv_name = (FormView)sender;
        var fvEdit_dataEmployeeTHai = (FormView)fvEdit_dataEmployee.FindControl("fvEdit_dataEmployeeTHai");
        //var FormView1 = (FormView)view_uploadpic.FindControl("FormView1");
        switch (fv_name.ID)
        {
            case "fvAddDataEmployee":
                if (fvAddDataEmployee.CurrentMode == FormViewMode.Insert)
                {
                    LinkButton Insert = (LinkButton)fvAddDataEmployee.FindControl("BtnSave");
                    ScriptManager.GetCurrent(Page).RegisterPostBackControl(Insert);
                }

                break;
        }




        //Similarily you can put register the Insert LinkButton as well.
    }
    #endregion

    #region ChkCheckedChanged
    protected void chkCheckedChanged(object sender, EventArgs e)
    {
        var txtPresentAddress = (TextBox)fvAddDataEmployee.FindControl("txtPresentAddress");
        var txtZipCode = (TextBox)fvAddDataEmployee.FindControl("txtZipCode");
        var txtHomePhone = (TextBox)fvAddDataEmployee.FindControl("txtHomePhone");

        var ddProvince = (DropDownList)fvAddDataEmployee.FindControl("ddProvince");
        var ddAmphoe = (DropDownList)fvAddDataEmployee.FindControl("ddAmphoe");
        var ddDistrict = (DropDownList)fvAddDataEmployee.FindControl("ddDistrict");
        var ddCountry = (DropDownList)fvAddDataEmployee.FindControl("ddCountry");

        // ตัวแปร DATA EmployeeThai
        var chk_sameAddress_present = (CheckBox)fvAddDataEmployee.FindControl("chk_sameAddress_present");
        var chk_notSameAddress_present = (CheckBox)fvAddDataEmployee.FindControl("chk_notSameAddress_present");

        var txtPermanentaddress = (TextBox)fvAddDataEmployee.FindControl("txtPermanentaddress");
        var txtZipcode2 = (TextBox)fvAddDataEmployee.FindControl("txtZipcode2");
        var txtHomePhone2 = (TextBox)fvAddDataEmployee.FindControl("txtHomePhone2");

        var ddProvince2 = (DropDownList)fvAddDataEmployee.FindControl("ddProvince2");
        var ddAmphoe2 = (DropDownList)fvAddDataEmployee.FindControl("ddAmphoe2");
        var ddDistrict2 = (DropDownList)fvAddDataEmployee.FindControl("ddDistrict2");
        var ddCountry2 = (DropDownList)fvAddDataEmployee.FindControl("ddCountry2");
        var BoxAddAddress = (Panel)fvAddDataEmployee.FindControl("BoxAddAddress");

        var chkName = (CheckBox)sender;

        EmployeeTHlist EmployeeTH = new EmployeeTHlist();
        _data_employee.BoxEmployeeTHlist = new EmployeeTHlist[1];

        switch (chkName.ID)
        {
            case "chk_sameAddress_present":

                if (chk_sameAddress_present.Checked)
                {
                    BoxAddAddress.Visible = false;
                    chk_sameAddress_present.Checked = true;
                    chk_notSameAddress_present.Checked = false;
                    txtPermanentaddress.Text = txtPresentAddress.Text;
                    txtZipcode2.Text = txtZipCode.Text;
                    txtHomePhone2.Text = txtHomePhone.Text;

                }
                break;

            case "chk_notSameAddress_present":
                if (chk_notSameAddress_present.Checked)
                {
                    chk_sameAddress_present.Checked = false;
                    BoxAddAddress.Visible = true;
                }

                break;
        }
    }

    #endregion

    #region TxtTextChanged
    protected void TxtTextChanged(object sender, EventArgs e)
    {

        var chk_sameAddress_present = (CheckBox)fvAddDataEmployee.FindControl("chk_sameAddress_present");

        var txtPermanentaddress = (TextBox)fvAddDataEmployee.FindControl("txtPermanentaddress");
        var txtZipcode2 = (TextBox)fvAddDataEmployee.FindControl("txtZipcode2");
        var txtHomePhone2 = (TextBox)fvAddDataEmployee.FindControl("txtHomePhone2");

        var txtPresentAddress = (TextBox)fvAddDataEmployee.FindControl("txtPresentAddress");
        var txtZipCode = (TextBox)fvAddDataEmployee.FindControl("txtZipCode");
        var txtHomePhone = (TextBox)fvAddDataEmployee.FindControl("txtHomePhone");

        var txtName = (TextBox)sender;

        switch (txtName.ID)
        {
            case "txtPresentAddress":

                if (chk_sameAddress_present.Checked)
                {
                    txtPermanentaddress.Text = txtPresentAddress.Text;
                }

                break;

            case "txtZipCode":

                if (chk_sameAddress_present.Checked)
                {
                    txtZipcode2.Text = txtZipCode.Text;
                }

                break;

            case "txtHomePhone":

                if (chk_sameAddress_present.Checked)
                {
                    txtHomePhone2.Text = txtHomePhone.Text;
                }

                break;
        }

    }
    #endregion

    #region select ddl_Edit 

    #region ddMilitary_Editing

    protected string ddlMilitary_Edit(int Type, string SelectValue)
    {
        var ddMilitary_Edit = (DropDownList)fvEdit_dataEmployeeTHai.FindControl("ddMilitary_Edit");
        var txt_MilIDX_edit = (TextBox)fvEdit_dataEmployeeTHai.FindControl("txt_MilIDX_edit");

        ddMilitary_Edit.Items.Clear();

        if (Type == 0)
        {
            data_presonal presonal = new data_presonal();
            DetailsMilitary Military = new DetailsMilitary();
            presonal.MilitaryList = new DetailsMilitary[1];
            presonal.MilitaryList[0] = Military;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_presonal", "Manage_Employee", presonal, 209);
            presonal = (data_presonal)_funcTool.convertXmlToObject(typeof(data_presonal), _local_xml);

            if (presonal.MilitaryList == null)
            {

                ddMilitary_Edit.DataSource = presonal.MilitaryList;
                ddMilitary_Edit.Items.Insert(0, new ListItem("เลือกสถานะทางการทหาร", "0"));
                ddMilitary_Edit.DataTextField = "MilName";
                ddMilitary_Edit.DataValueField = "MilIDX";
                ddMilitary_Edit.DataBind();
            }
            else
            {

                ddMilitary_Edit.DataSource = presonal.MilitaryList;
                ddMilitary_Edit.Items.Insert(0, new ListItem("เลือกสถานะทางการทหาร", "0"));
                ddMilitary_Edit.DataTextField = "MilName";
                ddMilitary_Edit.DataValueField = "MilIDX";
                ddMilitary_Edit.DataBind();
                ddMilitary_Edit.SelectedValue = txt_MilIDX_edit.Text;

            }
        }
        else if (Type == 1)
        {
            ddMilitary_Edit.DataBind();
            ddMilitary_Edit.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
        }
        return ddMilitary_Edit.SelectedItem.Value;
    }

    #endregion

    #region ddlCountry_Edit
    protected string ddlCountry_Edit(int Type, string SelectValue)
    {

        var ddCountry_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddCountry_Edit");

        ddCountry_Edit.Items.Clear();

        if (Type == 0)
        {

            DetailCountry Country = new DetailCountry();
            _data_Region.CountryList = new DetailCountry[1];

            _data_Region.CountryList[0] = Country;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", _data_Region, 204);
            _data_Region = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);

            if (_data_Region.CountryList == null)
            {

                ddCountry_Edit.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {
                ddCountry_Edit.DataSource = _data_Region.CountryList;
                ddCountry_Edit.DataTextField = "CountryName";
                ddCountry_Edit.DataValueField = "CountryIDX";
                ddCountry_Edit.DataBind();
                ddCountry_Edit.Items.Insert(0, new ListItem("เลือกประเทศ", "0"));
                ddCountry_Edit.SelectedValue = SelectValue;

            }


        }
        else if (Type == 1)
        {
            ddCountry_Edit.DataBind();
            ddCountry_Edit.Items.Insert(0, new ListItem("เลือกประเทศ", "0"));
        }


        return ddCountry_Edit.SelectedItem.Value;
    }
    #endregion

    #region ddlCountry_FR_Edit
    protected string ddlCountry_FR_Edit(int Type, string SelectValue)
    {
        var txtEdit_FR_Country = (TextBox)fv_edit_employee_Foreing.FindControl("txtEdit_FR_Country");
        var ddlCountryEN_edit = (DropDownList)fv_edit_employee_Foreing.FindControl("ddlCountryEN_edit");

        if (Type == 0)
        {
            data_Region data_country_ = new data_Region();
            DetailCountry Country__ = new DetailCountry();
            data_country_.CountryList = new DetailCountry[1];

            data_country_.CountryList[0] = Country__;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", data_country_, 204);
            data_country_ = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);

            if (data_country_.CountryList == null)
            {

                ddlCountryEN_edit.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {
                ddlCountryEN_edit.DataSource = data_country_.CountryList;
                ddlCountryEN_edit.DataTextField = "CountryName";
                ddlCountryEN_edit.DataValueField = "CountryIDX";
                ddlCountryEN_edit.DataBind();
                ddlCountryEN_edit.Items.Insert(0, new ListItem("เลือกประเทศ", "0"));
                ddlCountryEN_edit.SelectedValue = txtEdit_FR_Country.Text;

            }


        }
        else if (Type == 1)
        {
            ddlCountryEN_edit.DataBind();
            ddlCountryEN_edit.Items.Insert(0, new ListItem("เลือกประเทศ", "0"));
        }


        return ddlCountryEN_edit.SelectedItem.Value;
    }
    #endregion

    #region ddlReligion_Edit 
    protected string ddlReligion_Edit(int Type, string SelectValue)
    {

        var ddReligion_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddReligion_Edit");

        ddReligion_Edit.Items.Clear();

        if (Type == 0)
        {

            DetailReligion Religion = new DetailReligion();
            _data_Region.ReligionList = new DetailReligion[1];

            _data_Region.ReligionList[0] = Religion;

            //_local_xml = _funcTool.convertObjectToXml(data_Region);
            //TextBox1.Text = _local_xml;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", _data_Region, 203);
            _data_Region = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);

            if (_data_Region.ReligionList == null)
            {

                ddReligion_Edit.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {
                ddReligion_Edit.DataSource = _data_Region.ReligionList;
                ddReligion_Edit.DataTextField = "RelNameTH";
                ddReligion_Edit.DataValueField = "RelIDX";
                ddReligion_Edit.DataBind();
                ddReligion_Edit.Items.Insert(0, new ListItem("เลือกศาสนา", "0"));
                ddReligion_Edit.SelectedValue = SelectValue;
            }


        }
        else if (Type == 1)
        {
            ddReligion_Edit.DataBind();
            ddReligion_Edit.Items.Insert(0, new ListItem("เลือกศาสนา", "0"));
        }


        return ddReligion_Edit.SelectedItem.Value;
    }
    #endregion

    #region ddPrefixTHEAction Edit
    protected string ddPrefixTHEAction(int Type, string SelectValue)
    {

        var ddPrefixTH_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddPrefixTH_Edit");

        ddPrefixTH_Edit.Items.Clear();

        if (Type == 0)
        {

            DetailPrefixName ddlPrefixTH = new DetailPrefixName();
            data_presonal.PrefixNameList = new DetailPrefixName[1];

            data_presonal.PrefixNameList[0] = ddlPrefixTH;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_presonal", "Manage_Employee", data_presonal, 201);
            data_presonal = (data_presonal)_funcTool.convertXmlToObject(typeof(data_presonal), _local_xml);
            ddPrefixTH_Edit.DataSource = data_presonal.PrefixNameList;
            ddPrefixTH_Edit.DataTextField = "PrefixNameTH";
            ddPrefixTH_Edit.DataValueField = "PrefixIDX";
            ddPrefixTH_Edit.DataBind();
            ddPrefixTH_Edit.SelectedValue = SelectValue;

        }
        else if (Type == 1)
        {
            ddPrefixTH_Edit.DataBind();
            ddPrefixTH_Edit.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
        }


        return ddPrefixTH_Edit.SelectedItem.Value;
    }
    #endregion

    #region ddRace_Edit
    protected string ddlRace_Edit(int Type, string SelectValue)
    {

        var ddRace_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddRace_Edit");

        ddRace_Edit.Items.Clear();

        if (Type == 0)
        {
            employee_detail Race = new employee_detail();
            _data_employee.employee_list = new employee_detail[1];

            _data_employee.employee_list[0] = Race;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee, 215);
            _data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

            if (_data_employee.employee_list == null)
            {

                ddRace_Edit.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {

                ddRace_Edit.DataSource = _data_employee.employee_list;
                ddRace_Edit.DataTextField = "race_name";
                ddRace_Edit.DataValueField = "race_idx";
                ddRace_Edit.DataBind();
                ddRace_Edit.Items.Insert(0, new ListItem("เลือกเชื้อชาติ", "0"));
                ddRace_Edit.SelectedValue = SelectValue;
            }



        }
        else if (Type == 1)
        {
            ddRace_Edit.DataBind();
            ddRace_Edit.Items.Insert(0, new ListItem("เลือกเชื้อชาติ", "0"));
        }


        return ddRace_Edit.SelectedItem.Value;
    }
    #endregion

    #region ddlPrefixEN_Edit
    protected void ddlPrefixEN_Edit(int Type, string SelectValue)
    {
        var ddPrefixEN_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddPrefixEN_Edit");

        ddPrefixEN_Edit.Items.Clear();

        if (Type == 0)
        {

            DetailPrefixName ddlPrefixTH = new DetailPrefixName();
            data_presonal.PrefixNameList = new DetailPrefixName[1];

            data_presonal.PrefixNameList[0] = ddlPrefixTH;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_presonal", "Manage_Employee", data_presonal, 201);
            data_presonal = (data_presonal)_funcTool.convertXmlToObject(typeof(data_presonal), _local_xml);
            ddPrefixEN_Edit.DataSource = data_presonal.PrefixNameList;
            ddPrefixEN_Edit.DataTextField = "PrefixNameEN";
            ddPrefixEN_Edit.DataValueField = "PrefixIDX";
            ddPrefixEN_Edit.DataBind();

        }
        else if (Type == 1)
        {
            ddPrefixEN_Edit.DataBind();
            ddPrefixEN_Edit.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
        }

    }
    #endregion

    #region ddNationality_Edit 
    protected string ddlNationality_Edit(int Type, string SelectValue)
    {

        var ddNationality_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddNationality_Edit");

        ddNationality_Edit.Items.Clear();

        if (Type == 0)
        {

            employee_detail Nationality = new employee_detail();
            _data_employee.employee_list = new employee_detail[1];

            _data_employee.employee_list[0] = Nationality;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee, 214);
            _data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);


            if (_data_employee.employee_list == null)
            {

                ddNationality_Edit.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {

                //check_XML.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_Region));
                ddNationality_Edit.DataSource = _data_employee.employee_list;
                ddNationality_Edit.DataTextField = "nat_name";
                ddNationality_Edit.DataValueField = "nat_idx";
                ddNationality_Edit.DataBind();
                ddNationality_Edit.Items.Insert(0, new ListItem("เลือกสัญชาติ", "0"));
                ddNationality_Edit.SelectedValue = SelectValue;
            }



        }
        else if (Type == 1)
        {
            ddNationality_Edit.DataBind();
            ddNationality_Edit.Items.Insert(0, new ListItem("เลือกสัญชาติ", "0"));
        }


        return ddNationality_Edit.SelectedItem.Value;
    }
    #endregion

    #region ddlProvince_Edit
    protected string ddlProvince_Edit(int Type, string SelectValue)
    {

        var ddProvince_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddProvince_Edit");

        ddProvince_Edit.Items.Clear();

        if (Type == 0)
        {
            DetailProv Province = new DetailProv();
            _data_Region.ProvList = new DetailProv[1];

            _data_Region.ProvList[0] = Province;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", _data_Region, 205);
            _data_Region = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);

            if (_data_Region.ProvList == null)
            {

                ddProvince_Edit.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {
                ddProvince_Edit.DataSource = _data_Region.ProvList;
                ddProvince_Edit.DataTextField = "ProvName";
                ddProvince_Edit.DataValueField = "ProvIDX";
                ddProvince_Edit.DataBind();
                ddProvince_Edit.Items.Insert(0, new ListItem("เลือกจังหวัด", "0"));
                ddProvince_Edit.SelectedValue = SelectValue;

            }


        }
        else if (Type == 1)
        {
            ddProvince_Edit.DataBind();
            ddProvince_Edit.Items.Insert(0, new ListItem("เลือกจังหวัด", "0"));
        }


        return ddProvince_Edit.SelectedItem.Value;
    }

    protected string ddlProvince_Permanent_Edit(int Type, string SelectValue)
    {
        //FormView fvEdit_dataEmployeeTHai = (FormView).FindControl("fvEdit_dataEmployeeTHai");
        TextBox txt_Province_Permanent = (TextBox)fvEdit_dataEmployeeTHai.FindControl("txt_Province_Permanent");
        DropDownList ddProvince_Permanent_Edit = (DropDownList)fvEdit_dataEmployeeTHai.FindControl("ddProvince_Permanent_Edit");

        ddProvince_Permanent_Edit.Items.Clear();

        if (Type == 0)
        {
            data_Region Region__ = new data_Region();
            DetailProv Province_edit = new DetailProv();
            Region__.ProvList = new DetailProv[1];

            Region__.ProvList[0] = Province_edit;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", Region__, 205);
            Region__ = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);

            if (Region__.ProvList == null)
            {

                ddProvince_Permanent_Edit.Items.Insert(0, new ListItem("ไม่มีข้อมูล", "0"));
            }
            else
            {
                ddProvince_Permanent_Edit.DataSource = Region__.ProvList;
                ddProvince_Permanent_Edit.DataTextField = "ProvName";
                ddProvince_Permanent_Edit.DataValueField = "ProvIDX";
                ddProvince_Permanent_Edit.DataBind();
                ddProvince_Permanent_Edit.Items.Insert(0, new ListItem("เลือกจังหวัด", "0"));
                ddProvince_Permanent_Edit.SelectedValue = txt_Province_Permanent.Text;

            }


        }
        else if (Type == 1)
        {
            ddProvince_Permanent_Edit.DataBind();
            ddProvince_Permanent_Edit.Items.Insert(0, new ListItem("เลือกจังหวัด", "0"));
        }


        return ddProvince_Permanent_Edit.SelectedItem.Value;
    }



    #endregion

    #region ddlAmphoe_Edit DataBind
    protected string ddlAmphoe_Edit(int Type, string SelectValue)
    {

        var ddProvince_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddProvince_Edit");
        var ddAmphoe_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddAmphoe_Edit");

        ddAmphoe_Edit.Items.Clear();

        if (Type == 0)
        {
            data_Region Region = new data_Region();
            DetailAmp Amphoe = new DetailAmp();
            Region.AmpList = new DetailAmp[1];

            Amphoe.ProvIDX = int.Parse(ddProvince_Edit.SelectedValue);

            Region.AmpList[0] = Amphoe;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", Region, 206);
            Region = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);


            if (Region.AmpList == null)
            {

                ddAmphoe_Edit.Items.Insert(0, new ListItem("เลือกอำเภอ", "0"));
            }
            else
            {

                ddAmphoe_Edit.DataSource = Region.AmpList;
                ddAmphoe_Edit.DataTextField = "AmpName";
                ddAmphoe_Edit.DataValueField = "AmpIDX";
                ddAmphoe_Edit.DataBind();
                ddAmphoe_Edit.Items.Insert(0, new ListItem("เลือกอำเภอ", "0"));
                ddAmphoe_Edit.SelectedValue = SelectValue;

            }


        }
        else if (Type == 1)
        {

            ddAmphoe_Edit.DataBind();
            ddAmphoe_Edit.Items.Insert(0, new ListItem("เลือกอำเภอ", "0"));
        }


        return ddAmphoe_Edit.SelectedItem.Value;
    }



    protected string ddAmphoe_Permanent_Edit(int Type, string SelectValue)
    {

        //FormView fvEdit_dataEmployeeTHai = (FormView)fvEdit_dataEmployee.FindControl("fvEdit_dataEmployeeTHai");
        var ddProvince_Permanent_Edit = (DropDownList)fvEdit_dataEmployeeTHai.FindControl("ddProvince_Permanent_Edit");
        var ddAmphoe_Permanent_Edit = (DropDownList)fvEdit_dataEmployeeTHai.FindControl("ddAmphoe_Permanent_Edit");
        TextBox txt_Amphoe_Permanent_Edit = (TextBox)fvEdit_dataEmployeeTHai.FindControl("txt_Amphoe_Permanent_Edit");

        ddAmphoe_Permanent_Edit.Items.Clear();

        if (Type == 0)
        {
            data_Region Region = new data_Region();
            DetailAmp Amphoe = new DetailAmp();
            Region.AmpList = new DetailAmp[1];

            Amphoe.ProvIDX = int.Parse(ddProvince_Permanent_Edit.SelectedValue);

            Region.AmpList[0] = Amphoe;

            _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_Region", "Manage_Employee", Region, 206);
            Region = (data_Region)_funcTool.convertXmlToObject(typeof(data_Region), _local_xml);


            if (Region.AmpList == null)
            {

                ddAmphoe_Permanent_Edit.Items.Insert(0, new ListItem("เลือกอำเภอ", "0"));
            }
            else
            {

                ddAmphoe_Permanent_Edit.DataSource = Region.AmpList;
                ddAmphoe_Permanent_Edit.DataTextField = "AmpName";
                ddAmphoe_Permanent_Edit.DataValueField = "AmpIDX";
                ddAmphoe_Permanent_Edit.DataBind();
                ddAmphoe_Permanent_Edit.Items.Insert(0, new ListItem("เลือกอำเภอ", "0"));
                ddAmphoe_Permanent_Edit.SelectedValue = txt_Amphoe_Permanent_Edit.Text;

            }


        }
        else if (Type == 1)
        {

            ddAmphoe_Permanent_Edit.DataBind();
            ddAmphoe_Permanent_Edit.Items.Insert(0, new ListItem("เลือกอำเภอ", "0"));
        }


        return ddAmphoe_Permanent_Edit.SelectedItem.Value;
    }
    #endregion

    #endregion

    #region CheckboxList

    protected void Select_CheckboxList_Benefits_OnShow()
    {
        var cbrecipients = (CheckBoxList)fvEdit_Sarary.FindControl("cbrecipients");
        var txt_id_benafits = (TextBox)fvEdit_Sarary.FindControl("txt_id_benafits");
        data_employee data_employee_ = new data_employee();
        BoxBenefitsList Benefits_Onshow = new BoxBenefitsList();
        data_employee_.BoxBenefits = new BoxBenefitsList[1];

        data_employee_.BoxBenefits[0] = Benefits_Onshow;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", data_employee_, 219);
        data_employee_ = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

        cbrecipients.DataSource = data_employee_.BoxBenefits;
        cbrecipients.DataTextField = "name_benefit";
        cbrecipients.DataValueField = "benefit_idx";
        cbrecipients.DataBind();
        var i = 0;

        //รูปแบบ     string[] arrays = { "1","2","3" };


        string list_benefits = ViewState["returnValue_benefits"].ToString();
        string[] arrays = list_benefits.Split(new string[] { "," }, StringSplitOptions.None);

        foreach (ListItem item in cbrecipients.Items)
        {
            if (Array.IndexOf(arrays, cbrecipients.Items[i].Value) > -1)
            {
                cbrecipients.Items[i].Selected = true;
            }
            i++;
        }

    }

    protected void Select_listbef()
    {
        var cbrecipients = (CheckBoxList)fvEdit_Sarary.FindControl("cbrecipients");
        var LoEmpCode = (Label)fvEdit_dataEmployee.FindControl("LoEmpCode");
        var Repeater1 = (Repeater)fvEdit_Sarary.FindControl("Repeater1");
        var txt_id_benafits = (TextBox)Repeater1.FindControl("txt_id_benafits");

        data_employee data_employee_ = new data_employee();
        BoxBenefitsList Benefits_Onshow = new BoxBenefitsList();
        BoxSararyList box_sarary = new BoxSararyList();
        data_employee_.BoxSarary = new BoxSararyList[1];
        data_employee_.BoxBenefits = new BoxBenefitsList[1];

        box_sarary.emp_sarary_idx = int.Parse(LoEmpCode.Text);
        data_employee_.BoxSarary[0] = box_sarary;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", data_employee_, 242);
        data_employee_ = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);


        ViewState["returnValue_benefits"] = data_employee_.return_code;

    }

    #endregion

    #region Select_databefore_Sent_email

    protected void Select_databefore_Sent_email()
    {

        data_employee data_employee_ = new data_employee();
        employee_detail databefore_Sent_email = new employee_detail();
        data_employee_.employee_list = new employee_detail[1];

        // box_sarary.emp_sarary_idx = int.Parse(LoEmpCode.Text);
        data_employee_.employee_list[0] = databefore_Sent_email;

        _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", data_employee_, 243);
        data_employee_ = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);


        // ViewState["returnValue_benefits"] = data_employee_.return_code;

    }

    #endregion

    #region rbtnSelect_CheckedChanged
    protected void rbtnSelect_CheckedChanged(object sender, EventArgs e)

    {
        GridView gvAddPosision = (GridView)fvAddDataEmployee.FindControl("gvAddPosision");
        ViewState["Position"] = int.Parse(((DropDownList)fvAddDataEmployee.FindControl("ddPosition")).SelectedValue);
        ArrayList posision = new ArrayList();
        ArrayList posision1 = new ArrayList();
        bool SelectRow;

        foreach (GridViewRow oldrow in gvAddPosision.Rows)
        {
            var alert_selectPosition = (Panel)(oldrow.FindControl("alert_selectPosition"));
            SelectRow = ((RadioButton)oldrow.FindControl("rbtnSelect")).Checked = false;
            alert_selectPosition.Visible = false;
        }
        RadioButton rb = (RadioButton)sender;
        GridViewRow row = (GridViewRow)rb.NamingContainer;
        SelectRow = ((RadioButton)row.FindControl("rbtnSelect")).Checked = true;
        if (SelectRow == true)
        {

            ViewState["checkPosision"] = null;
            ViewState["Position"] = ((Label)(row.FindControl("Position_idx"))).Text;
            var alert_selectPosition = (Panel)(row.FindControl("alert_selectPosition"));
            alert_selectPosition.Visible = false;
            posision.Add(ViewState["Position"]);
        }
        ViewState["checkPosision"] = posision;
    }
    #endregion

    #region radio_select_CheckedChanged
    protected void radio_MovePosition_CheckedChanged(object sender, EventArgs e)

    {
        GridView gv_present_position = (GridView)viewEdit_dataEmployee.FindControl("gv_present_position");
        ArrayList posision_select_move = new ArrayList();
        ArrayList posision_move = new ArrayList();
        bool SelectRow_position;

        foreach (GridViewRow oldrow_move in gv_present_position.Rows)
        {
            SelectRow_position = ((RadioButton)oldrow_move.FindControl("radio_select")).Checked = false;
            //  check_XML.Text = "Bad";
        }

        RadioButton RadioButton_remove = (RadioButton)sender;
        GridViewRow rowGrid = (GridViewRow)RadioButton_remove.NamingContainer;
        SelectRow_position = ((RadioButton)rowGrid.FindControl("radio_select")).Checked = true;

        if (SelectRow_position == true)
        {

            // check_XML.Text = "Good";
            ViewState["checkPosision_remove"] = null;
            ViewState["Position_move"] = ((Label)(rowGrid.FindControl("lb_id_position"))).Text;
            ViewState["status_Position_move"] = ((Label)(rowGrid.FindControl("lb_statuss"))).Text;
            ViewState["id"] = ((Label)(rowGrid.FindControl("id_"))).Text;
            posision_select_move.Add(ViewState["Position_move"]);
            posision_select_move.Add(ViewState["status_Position_move"]);
            posision_select_move.Add(ViewState["id"]);
        }

        ViewState["checkPosision_remove"] = posision_select_move;
        //check_XML.Text = (ViewState["checkPosision_remove"].ToString());

    }
    #endregion

    #region ClickReset
    protected void ClickReset(object sender, EventArgs e)

    {
        GridView gv_present_position = (GridView)viewEdit_dataEmployee.FindControl("gv_present_position");
        bool ResetRow_position;
        foreach (GridViewRow oldrow_move in gv_present_position.Rows)
        {
            ResetRow_position = ((RadioButton)oldrow_move.FindControl("radio_select")).Checked = false;

        }
    }
    #endregion

    #region DdSelectedIndexChanged
    protected void DdSelectedIndexChanged(object sender, EventArgs e)
    {

        var ddlNationality = (DropDownList)fvAddDataEmployee.FindControl("ddlNationality");
        var divMilitary = (Panel)fvAddDataEmployee.FindControl("divMilitary");
        var ddSex = (DropDownList)fvAddDataEmployee.FindControl("ddSex");

        var chk_sameAddress_present = (CheckBox)fvAddDataEmployee.FindControl("chk_sameAddress_present");
        var ddDistrict2 = (DropDownList)fvAddDataEmployee.FindControl("ddDistrict2");
        var ddAmphoe2 = (DropDownList)fvAddDataEmployee.FindControl("ddAmphoe2");
        var ddProvince2 = (DropDownList)fvAddDataEmployee.FindControl("ddProvince2");
        var ddCountry2 = (DropDownList)fvAddDataEmployee.FindControl("ddCountry2");
        var ddHospital = (DropDownList)fvAddDataEmployee.FindControl("ddHospital");
        var txtSocialDateExp = (TextBox)fvAddDataEmployee.FindControl("txtSocialDateExp");

        var ddDistrict = (DropDownList)fvAddDataEmployee.FindControl("ddDistrict");
        var ddAmphoe = (DropDownList)fvAddDataEmployee.FindControl("ddAmphoe");
        var ddProvince = (DropDownList)fvAddDataEmployee.FindControl("ddProvince");
        var ddCountry = (DropDownList)fvAddDataEmployee.FindControl("ddCountry");

        var ddl_add_org_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_org_idx");
        var ddl_add_rdept_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_rdept_idx");
        var ddl_add_rsec_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_rsec_idx");
        var ddl_add_rpos_idx = (DropDownList)fvEdit_dataEmployee.FindControl("ddl_add_rpos_idx");

        var BoxEmployeeEN = (Panel)fvAddDataEmployee.FindControl("BoxEmployeeEN");
        var BoxEmployeeTH = (Panel)fvAddDataEmployee.FindControl("BoxEmployeeTH");
        var BoxApprovers = (Panel)fvAddDataEmployee.FindControl("BoxApprovers");

        var ddlchangOrganization = (DropDownList)BoxRemovePosisionn.FindControl("ddlchangOrganization");
        var ddlchangDepartment = (DropDownList)BoxRemovePosisionn.FindControl("ddlchangDepartment");
        var ddlchangSection = (DropDownList)BoxRemovePosisionn.FindControl("ddlchangSection");

        var ddlistSex = (DropDownList)fvAddDataEmployee.FindControl("ddlistSex");
        var Military = (Panel)fvAddDataEmployee.FindControl("Military");

        var edit_Military = (Panel)fvEdit_dataEmployeeTHai.FindControl("edit_Military");
        var ddlistSex_Edit = (DropDownList)fvEdit_dataEmployee.FindControl("ddlistSex_Edit");
        var txtidSex = (TextBox)fvEdit_dataEmployee.FindControl("txt_MilIDX_edit");

        var ddName = (DropDownList)sender;
        switch (ddName.ID)
        {
            case "ddOrganiztion":
                ddlSelect_ddDepartment(0, "0");
                ddPositionAction(1, "0");
                ddl_ShiftTime();
                break;
            case "ddDepartment":
                ddDivisionAction(0, "0");
                ddPositionAction(1, "0");
                break;
            case "ddDivision":
                ddPositionAction(0, "0");
                BoxApprovers.Visible = true;
                break;
            case "ddPosition":
                ddlApprover_Leave();
                break;
            case "ddAmphoe":
                ddDistrictAction(0, "0");
                break;
            case "ddAmphoe2":
                ddDistrict2Action(0, "0");
                break;
            case "ddProvince":
                ddAmphoeAction(0, "0");
                ddDistrictAction(1, "0");

                break;
            case "ddProvince2":
                ddAmphoe2Action(0, "0");
                ddDistrict2Action(1, "0");
                break;
            case "ddCountry":

                break;
            case "ddDistrict":

                break;
            case "ddlNationality":

                if (ddlNationality.SelectedValue == "177")
                {
                    BoxEmployeeTH.Visible = true;
                }
                else
                {
                    BoxEmployeeEN.Visible = true;
                    BoxEmployeeTH.Visible = false;
                }
                if (ddlNationality.SelectedValue != "177")
                {
                    BoxEmployeeTH.Visible = false;
                }
                else
                {
                    BoxEmployeeEN.Visible = false;
                    BoxEmployeeTH.Visible = true;
                }

                break;

            case "ddl_Select_Organization_search":
                Department_search(0, "0");
                break;

            case "ddlistSex":
                if (ddlistSex.SelectedValue == "2")
                {
                    Military.Visible = false;

                }
                else
                {
                    Military.Visible = true;
                }

                break;

            #region dropdownList Edit

            case "ddlistSex_Edit":

                if (ddlistSex_Edit.SelectedItem.Value == "2")
                {
                    edit_Military.Visible = false;

                }
                else if (ddlistSex_Edit.SelectedItem.Value != "2")
                {
                    edit_Military.Visible = true;
                }
                break;

            case "ddProvince_Edit":
                ddlAmphoe_Edit(0, "0");
                break;

            case "ddAmphoe_Edit":
                ddDistrict_Edit(0, "0");
                break;

            case "ddProvince_Permanent_Edit":
                ddAmphoe_Permanent_Edit(0, "0");
                break;

            case "ddAmphoe_Permanent_Edit":
                ddDistrict_Permanent_Edit(0, "0");
                break;

            #endregion

            case "ddl_add_org_idx":
                ddl_rdept_action(0, "0");
                ddl_rpos_action(1, "0");

                break;

            case "ddl_add_rdept_idx":
                ddl_rsec_action(0, "0");
                ddl_rpos_action(1, "0");

                break;

            case "ddl_add_rsec_idx":
                ddl_rpos_action(0, "0");
                break;

            case "ddl_add_rpos_idx":
                ddlApprover_Leave_edit();
                break;

            case "ddlchangOrganization":
                ddlchangDepartment_action(0, "0");
                ddlchangPosision_action(1, "0");
                break;

            case "ddlchangDepartment":
                ddlchangSection_action(0, "0");
                ddlchangPosision_action(1, "0");
                break;

            case "ddlchangSection":
                ddlchangPosision_action(0, "0");
                break;
            case "ddl_type_penalty":
                if (ddl_type_penalty.SelectedValue == "3")
                {
                    txt_quantity_day.Enabled = true;
                    txt_start_date_break.Enabled = true;
                    txt_end_date_break.Enabled = true;
                }
                else
                {
                    txt_quantity_day.Enabled = false;
                    txt_start_date_break.Enabled = false;
                    txt_end_date_break.Enabled = false;
                    txt_quantity_day.Text = null;
                    txt_start_date_break.Text = null;
                    txt_end_date_break.Text = null;
                }
                break;

            case "ddl_edit_org_idx":
                var ddl_org = (DropDownList)sender;
                var row_org = (GridViewRow)ddl_org.NamingContainer;

                DropDownList ddl_edit_org_idx = (DropDownList)row_org.FindControl("ddl_edit_org_idx");
                DropDownList ddl_edit_department = (DropDownList)row_org.FindControl("ddl_edit_department");
                DropDownList ddl_edit_section = (DropDownList)row_org.FindControl("ddl_edit_section");
                DropDownList ddl_edit_position = (DropDownList)row_org.FindControl("ddl_edit_position");

                if (ddl_edit_org_idx.SelectedValue == "0")

                {
                    ddl_edit_org_idx.AppendDataBoundItems = true;
                    ddl_edit_org_idx.Items.Clear();
                    ddl_edit_org_idx.Items.Insert(0, new ListItem("เลือกองค์กร", "0"));

                    dataODSP_Relation ODSP_Relation1 = new dataODSP_Relation();
                    Relation_ODSP relation = new Relation_ODSP();
                    ODSP_Relation1.detailRelation_ODSP = new Relation_ODSP[1];

                    ODSP_Relation1.detailRelation_ODSP[0] = relation;

                    _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", ODSP_Relation1, 200);
                    ODSP_Relation1 = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

                    ddl_edit_org_idx.Items.Insert(0, new ListItem("เลือกบริษัท", "0"));
                    ddl_edit_org_idx.DataSource = ODSP_Relation1.detailRelation_ODSP;
                    ddl_edit_org_idx.DataTextField = "OrgNameTH";
                    ddl_edit_org_idx.DataValueField = "OrgIDX";
                    ddl_edit_org_idx.DataBind();
                    ddl_edit_org_idx.SelectedValue = "0";

                    ddl_edit_department.Items.Clear();
                    ddl_edit_department.Items.Insert(0, new ListItem("เลือกฝ่าย", "0"));
                    ddl_edit_department.SelectedValue = "0";

                    ddl_edit_section.Items.Clear();
                    ddl_edit_section.Items.Insert(0, new ListItem("เลือกแผนก", "0"));
                    ddl_edit_section.SelectedValue = "0";

                    ddl_edit_position.Items.Clear();
                    ddl_edit_position.Items.Insert(0, new ListItem("เลือกตำแหน่ง", "0"));
                    ddl_edit_position.SelectedValue = "0";

                }

                else
                {
                    ddl_edit_department.AppendDataBoundItems = true;
                    ddl_edit_department.Items.Clear();
                    ddl_edit_department.Items.Insert(0, new ListItem("เลือกฝ่าย", "0"));

                    dataODSP_Relation ODSP_Relation1 = new dataODSP_Relation();
                    Relation_ODSP relation = new Relation_ODSP();
                    ODSP_Relation1.detailRelation_ODSP = new Relation_ODSP[1];

                    relation.OrgIDX = int.Parse(ddl_edit_org_idx.SelectedValue);
                    ODSP_Relation1.detailRelation_ODSP[0] = relation;

                    _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", ODSP_Relation1, 200);
                    ODSP_Relation1 = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

                    ddl_edit_department.DataSource = ODSP_Relation1.detailRelation_ODSP;
                    ddl_edit_department.DataTextField = "DeptNameTH";
                    ddl_edit_department.DataValueField = "RDeptIDX";
                    ddl_edit_department.DataBind();

                    ddl_edit_section.Items.Clear();
                    ddl_edit_section.Items.Insert(0, new ListItem("เลือกแผนก", "0"));
                    ddl_edit_section.SelectedValue = "0";

                    ddl_edit_position.Items.Clear();
                    ddl_edit_position.Items.Insert(0, new ListItem("เลือกตำแหน่ง", "0"));
                    ddl_edit_position.SelectedValue = "0";

                }


                break;

            case "ddl_edit_department":

                var ddl_department = (DropDownList)sender;
                var row_department = (GridViewRow)ddl_department.NamingContainer;

                DropDownList ddl_edit_org_idx1 = (DropDownList)row_department.FindControl("ddl_edit_org_idx");
                DropDownList ddl_edit_department1 = (DropDownList)row_department.FindControl("ddl_edit_department");
                DropDownList ddl_edit_section1 = (DropDownList)row_department.FindControl("ddl_edit_section");
                DropDownList ddl_edit_position1 = (DropDownList)row_department.FindControl("ddl_edit_position");

                if (ddl_edit_department1.SelectedValue == "0")

                {

                    ddl_edit_department1.AppendDataBoundItems = true;
                    ddl_edit_department1.Items.Clear();
                    ddl_edit_department1.Items.Insert(0, new ListItem("เลือกฝ่าย", "0"));

                    dataODSP_Relation ODSP_Relation = new dataODSP_Relation();
                    var relation1 = new Relation_ODSP[1];
                    relation1[0] = new Relation_ODSP();
                    relation1[0].OrgIDX = Int32.Parse(ddl_edit_org_idx1.SelectedValue);
                    relation1[0].RDeptIDX = Int32.Parse(ddl_edit_department1.SelectedValue);

                    ODSP_Relation.detailRelation_ODSP = relation1;

                    _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", ODSP_Relation, 200);
                    ODSP_Relation = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

                    ddl_edit_department1.DataSource = ODSP_Relation.detailRelation_ODSP;
                    ddl_edit_department1.DataTextField = "DeptNameTH";
                    ddl_edit_department1.DataValueField = "RDeptIDX";
                    ddl_edit_department1.DataBind();
                    ddl_edit_department1.Items.Insert(0, new ListItem("เลือกฝ่าย", "0"));
                    ddl_edit_department1.SelectedValue = "0";

                    ddl_edit_section1.Items.Clear();
                    ddl_edit_section1.Items.Insert(0, new ListItem("เลือกแผนก", "0"));
                    ddl_edit_section1.SelectedValue = "0";

                    ddl_edit_position1.Items.Clear();
                    ddl_edit_position1.Items.Insert(0, new ListItem("เลือกตำแหน่ง", "0"));
                    ddl_edit_position1.SelectedValue = "0";

                }

                else

                {
                    ddl_edit_section1.AppendDataBoundItems = true;
                    ddl_edit_section1.Items.Clear();
                    ddl_edit_section1.Items.Insert(0, new ListItem("เลือกแผนก", "0"));

                    dataODSP_Relation ODSP_Relation2 = new dataODSP_Relation();
                    var relation2 = new Relation_ODSP[1];
                    relation2[0] = new Relation_ODSP();


                    relation2[0].OrgIDX = Int32.Parse(ddl_edit_org_idx1.SelectedValue);
                    relation2[0].RDeptIDX = Int32.Parse(ddl_edit_department1.SelectedValue);
                    ODSP_Relation2.detailRelation_ODSP = relation2;

                    _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", ODSP_Relation2, 200);
                    ODSP_Relation2 = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

                    ddl_edit_section1.DataSource = ODSP_Relation2.detailRelation_ODSP;
                    ddl_edit_section1.DataTextField = "SecNameTH";
                    ddl_edit_section1.DataValueField = "RSecIDX";
                    ddl_edit_section1.DataBind();

                    ddl_edit_position1.Items.Clear();
                    ddl_edit_position1.Items.Insert(0, new ListItem("เลือกตำแหน่ง", "0"));
                    ddl_edit_position1.SelectedValue = "0";

                }


                break;

            case "ddl_edit_section":

                var ddl_section = (DropDownList)sender;
                var row_section = (GridViewRow)ddl_section.NamingContainer;

                DropDownList ddl_edit_org_idx2 = (DropDownList)row_section.FindControl("ddl_edit_org_idx");
                DropDownList ddl_edit_department2 = (DropDownList)row_section.FindControl("ddl_edit_department");
                DropDownList ddl_edit_section2 = (DropDownList)row_section.FindControl("ddl_edit_section");
                DropDownList ddl_edit_position2 = (DropDownList)row_section.FindControl("ddl_edit_position");

                if (ddl_edit_section2.SelectedValue == "0")

                {
                    ddl_edit_section2.AppendDataBoundItems = true;
                    ddl_edit_section2.Items.Clear();
                    ddl_edit_section2.Items.Insert(0, new ListItem("เลือกแผนก", "0"));

                    dataODSP_Relation ODSP_Relation3 = new dataODSP_Relation();
                    var relation3 = new Relation_ODSP[1];
                    relation3[0] = new Relation_ODSP();

                    relation3[0].OrgIDX = Int32.Parse(ddl_edit_org_idx2.SelectedValue);
                    relation3[0].RDeptIDX = Int32.Parse(ddl_edit_department2.SelectedValue);
                    relation3[0].RSecIDX = Int32.Parse(ddl_edit_section2.SelectedValue);
                    ODSP_Relation3.detailRelation_ODSP = relation3;


                    _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", ODSP_Relation3, 200);
                    ODSP_Relation3 = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);

                    ddl_edit_section2.DataSource = ODSP_Relation3.detailRelation_ODSP;
                    ddl_edit_section2.DataTextField = "SecNameTH";
                    ddl_edit_section2.DataValueField = "RSecIDX";
                    ddl_edit_section2.DataBind();


                    ddl_edit_position2.Items.Clear();
                    ddl_edit_position2.Items.Insert(0, new ListItem("เลือกตำแหน่ง", "0"));
                    ddl_edit_position2.SelectedValue = "0";
                }
                else
                {
                    ddl_edit_position2.AppendDataBoundItems = true;
                    ddl_edit_position2.Items.Clear();
                    ddl_edit_position2.Items.Insert(0, new ListItem("เลือกตำแหน่ง", "0"));

                    dataODSP_Relation ODSP_Relation3 = new dataODSP_Relation();
                    var relation3 = new Relation_ODSP[1];
                    relation3[0] = new Relation_ODSP();

                    relation3[0].OrgIDX = Int32.Parse(ddl_edit_org_idx2.SelectedValue);
                    relation3[0].RDeptIDX = Int32.Parse(ddl_edit_department2.SelectedValue);
                    relation3[0].RSecIDX = Int32.Parse(ddl_edit_section2.SelectedValue);
                    ODSP_Relation3.detailRelation_ODSP = relation3;

                    _local_xml = _service_exec.actionExec(Conn_manage_employee, "dataODSP_Relation", "ODSP_Reletion", ODSP_Relation3, 200);
                    ODSP_Relation3 = (dataODSP_Relation)_funcTool.convertXmlToObject(typeof(dataODSP_Relation), _local_xml);


                    ddl_edit_position2.DataSource = ODSP_Relation3.detailRelation_ODSP;
                    ddl_edit_position2.DataTextField = "PosNameTH";
                    ddl_edit_position2.DataValueField = "RPosIDX";
                    ddl_edit_position2.DataBind();

                }

                break;
        }
    }
    #endregion

    #region table Position

    protected void Select_tabel_posision()
    {
        var data_Simulate = new DataSet();
        data_Simulate.Tables.Add("SimulateData_Posiision");

        data_Simulate.Tables[0].Columns.Add("Organiztion", typeof(int));
        data_Simulate.Tables[0].Columns.Add("Position", typeof(int));
        data_Simulate.Tables[0].Columns.Add("Division", typeof(int));
        data_Simulate.Tables[0].Columns.Add("Department", typeof(int));
        data_Simulate.Tables[0].Columns.Add("Organiztion_Name", typeof(String));
        data_Simulate.Tables[0].Columns.Add("Position_Name", typeof(String));
        data_Simulate.Tables[0].Columns.Add("Division_Name", typeof(String));
        data_Simulate.Tables[0].Columns.Add("Department_Name", typeof(String));

        if (ViewState["Create_DataPosision"] == null)
        {
            ViewState["Create_DataPosision"] = data_Simulate;
        }
    }

    #endregion

    #region table Benefits

    protected void Select_tabel_Benefits()
    {

        var data_Benefits_sum = new DataSet();
        data_Benefits_sum.Tables.Add("SimulateData_Benefits");

        data_Benefits_sum.Tables[0].Columns.Add("BenefitsID", typeof(int));
        data_Benefits_sum.Tables[0].Columns.Add("Benefits_list", typeof(String));


        if (ViewState["Create_DataBenefits"] == null)
        {
            ViewState["Create_DataBenefits"] = data_Benefits_sum;
        }
    }

    #endregion

    #region table Experience Work

    protected void Select_tabel_Experience()
    {
        var data_Experience = new DataSet();
        data_Experience.Tables.Add("SimulateData_Experience");

        data_Experience.Tables[0].Columns.Add("Name_Organiztion_Old", typeof(String));
        data_Experience.Tables[0].Columns.Add("Position_Old", typeof(String));
        data_Experience.Tables[0].Columns.Add("date_Work_In_Old", typeof(String));
        data_Experience.Tables[0].Columns.Add("date_resign_Old", typeof(String));
        data_Experience.Tables[0].Columns.Add("Motive", typeof(String));
        data_Experience.Tables[0].Columns.Add("Salary_Old", typeof(String));
        data_Experience.Tables[0].Columns.Add("Job_Brief", typeof(String));

        if (ViewState["Create_table_Experience"] == null)
        {
            ViewState["Create_table_Experience"] = data_Experience;
        }
    }

    #endregion

    #region btnCommand

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        employee_detail DataEmployee = new employee_detail();
        _data_employee.employee_list = new employee_detail[1];
        var fvEdit_dataEmployee = (FormView)viewEdit_dataEmployee.FindControl("fvEdit_dataEmployee");

        switch (cmdName)
        {
            case "BtnAddEmployee":

                fvAddDataEmployee.DataBind();

                #region รวมคลาสที่เรียก DropdownList
                ddPrefixTHAction(0, "0");
                ddPrefixENAction1(0, "0");
                ddlSelect_ddOrganiztion(0, "0");
                ddlSelect_ddDepartment(1, "0");
                ddDivisionAction(1, "0");
                ddPositionAction(1, "0");
                ddlApprover_Leave();
                ddReligionAction(0, "0");
                ddCountryAction(0, "218");
                ddProvinceAction(0, "0");
                ddAmphoeAction(1, "0");
                ddDistrictAction(1, "0");
                ddAmphoe2Action(1, "0");
                ddDistrict2Action(1, "0");
                ddlSelect_dddCostCenter(0, "0");
                ddNationalityAction(0, "177");
                ddCountryENAction(0, "218");
                ddCountry2Action(0, "218");
                ddProvince2Action(0, "0");
                ddRaceAction(0, "0");
                ddlMilitary();
                //Select_ddlEducation();
                //ddlSelect_Faculty();
                ddlBloodgroupList();
                ddlHotpital();
                //ddl_ShiftTime();
                #endregion

                MvMaster.SetActiveView(ViewAddDataEmployee);

                break;

            case "btnViewSelected":
                lbMenu0.BackColor = System.Drawing.Color.LightGray;
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btnViewSelected4":
                lbMenu0.BackColor = System.Drawing.Color.Transparent;
                break;
            case "btnaddPosison":
                Select_tabel_posision();
                var ddOrganiztion = (DropDownList)fvAddDataEmployee.FindControl("ddOrganiztion");
                var ddDepartment = (DropDownList)fvAddDataEmployee.FindControl("ddDepartment");
                var ddDivision = (DropDownList)fvAddDataEmployee.FindControl("ddDivision");
                var ddPosition = (DropDownList)fvAddDataEmployee.FindControl("ddPosition");
                var redioPosision = (RadioButton)fvAddDataEmployee.FindControl("redioPosision");
                var BoxApprovers = (Panel)fvAddDataEmployee.FindControl("BoxApprovers");

                var data_Simulate_Posision = (DataSet)ViewState["Create_DataPosision"];
                var Simulate = data_Simulate_Posision.Tables[0].NewRow();

                Simulate["Organiztion"] = int.Parse(ddOrganiztion.SelectedValue);
                Simulate["Position"] = int.Parse(ddPosition.SelectedValue);
                Simulate["Division"] = int.Parse(ddDivision.SelectedValue);
                Simulate["Department"] = int.Parse(ddDepartment.SelectedValue);

                Simulate["Organiztion_Name"] = ddOrganiztion.SelectedItem.Text;
                Simulate["Position_Name"] = ddPosition.SelectedItem.Text;
                Simulate["Division_Name"] = ddDivision.SelectedItem.Text;
                Simulate["Department_Name"] = ddDepartment.SelectedItem.Text;

                GridView gvAddPosision = (GridView)fvAddDataEmployee.FindControl("gvAddPosision");

                data_Simulate_Posision.Tables[0].Rows.Add(Simulate);
                ViewState["Create_DataPosision"] = data_Simulate_Posision;

                gvAddPosision.DataSource = data_Simulate_Posision.Tables[0];
                gvAddPosision.DataBind();
                gvAddPosision.Visible = true;
                BoxApprovers.Visible = true;

                break;

            case "btnaddExperience": // ****เพิ่มข้อมูลเป็นเทเบิลจำลอง ของผู้ที่มีประสบการณ์ทำงาน******
                Select_tabel_Experience();

                FormView fvAddDataEmployee_ = (FormView)ViewAddDataEmployee.FindControl("fvAddDataEmployee");

                var txtOrganiztion_Old = (TextBox)fvAddDataEmployee_.FindControl("txtOrganiztion_Old");
                var txtPositionOld = (TextBox)fvAddDataEmployee_.FindControl("txtPositionOld");
                var txtWorkInOld = (TextBox)fvAddDataEmployee_.FindControl("txtWorkInOld");
                var txtresign_Old = (TextBox)fvAddDataEmployee_.FindControl("txtresign_Old");
                var txtMotive = (TextBox)fvAddDataEmployee_.FindControl("txtMotive");
                var txtSalary_Old = (TextBox)fvAddDataEmployee_.FindControl("txtSalary_Old");
                var txt_Job_Brief = (TextBox)fvAddDataEmployee_.FindControl("txt_Job_Brief");
                LinkButton BtnCancel = (LinkButton)fvAddDataEmployee_.FindControl("BtnCancel");
                LinkButton BtnSave = (LinkButton)fvAddDataEmployee_.FindControl("BtnSave");

                var data_Simulate_Experience = (DataSet)ViewState["Create_table_Experience"];
                var Sum_Data_Experience = data_Simulate_Experience.Tables[0].NewRow();

                Sum_Data_Experience["Name_Organiztion_Old"] = txtOrganiztion_Old.Text;
                Sum_Data_Experience["Position_Old"] = txtPositionOld.Text;
                Sum_Data_Experience["Job_Brief"] = txt_Job_Brief.Text;
                Sum_Data_Experience["date_Work_In_Old"] = txtWorkInOld.Text;
                Sum_Data_Experience["date_resign_Old"] = txtresign_Old.Text;

                Sum_Data_Experience["Motive"] = txtMotive.Text;
                Sum_Data_Experience["Salary_Old"] = txtSalary_Old.Text;

                GridView gvExperience = (GridView)fvAddDataEmployee_.FindControl("gvExperience");
                data_Simulate_Experience.Tables[0].Rows.Add(Sum_Data_Experience);
                ViewState["Create_table_Experience"] = data_Simulate_Experience;

                gvExperience.DataSource = data_Simulate_Experience.Tables[0];
                gvExperience.DataBind();
                gvExperience.Visible = true;

                BtnSave.Visible = true;
                BtnCancel.Visible = true;


                txtOrganiztion_Old.Text = String.Empty;
                txtPositionOld.Text = String.Empty;
                txt_Job_Brief.Text = String.Empty;
                txtWorkInOld.Text = String.Empty;
                txtresign_Old.Text = String.Empty;
                txtMotive.Text = String.Empty;
                txtSalary_Old.Text = String.Empty;

                break;
            case "BtnSaveDataEmploy":
                Select_tabel_posision();
                InsertData_Employee();
                // insert_shiftTime_employee();
                break;

            case "BtnCancel":
                MvMaster.SetActiveView(viewEdit_dataEmployee);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "cancel__":
                Panel boxedit_addposition__ = (Panel)fvEdit_dataEmployee.FindControl("boxedit_addposition");
                boxedit_addposition__.Visible = false;
                break;
            case "cmdedit":

                string[] arg1 = new string[5];
                arg1 = e.CommandArgument.ToString().Split(';');
                int emp_idx = int.Parse(arg1[0]);
                string rsec = arg1[1];
                string rpos_idxx = arg1[2];
                string rdept_idxx = arg1[3];
                string org_idxx = arg1[4];

                fvEdit_dataEmployee.DataBind();
                fvEdit_dataEmployee.ChangeMode(FormViewMode.Edit);
                fv_edit_reference_persons.ChangeMode(FormViewMode.Edit);
                fv_edit_employee_Foreing.ChangeMode(FormViewMode.Edit);
                MvMaster.SetActiveView(viewEdit_dataEmployee);
                ViewState["emp_idx"] = emp_idx;
                ViewState["rsec_idx"] = rsec;
                ViewState["rpos_idxx"] = rpos_idxx;
                ViewState["rdept_idx"] = rdept_idxx;
                ViewState["orgIDX"] = org_idxx;


                fv_Show_Edit_employee();
                fv_Show_Edit_employeeTHai();
                show_pic();
                fv_Show_Edit_Health();
                //fv_Show_Edit_Education();
                //gv_show_edit_experience();
                gv_show_edit_position();
                ddl_org_action(0, "0");
                ddl_rdept_action(1, "0");
                ddl_rpos_action(1, "0");
                ddl_rsec_action(1, "0");
                ddlApprover_Leave_edit();
                select_history_ShiftTime();
                ddl_edit_ShiftTime();
                fv_Show_Edit_Reference_persons();

                linkBtnTrigger(btn_save_pic);
                linkBtnTrigger(BtnRemovePosision);


                var fv_edit_Health = (FormView)viewEdit_dataEmployee.FindControl("fv_edit_Health");
                var fvEdit_dataEmployeeTHai = (FormView)viewEdit_dataEmployee.FindControl("fvEdit_dataEmployeeTHai");
                var txtPrefixTHE = (TextBox)fvEdit_dataEmployee.FindControl("txtPrefixTHE");
                var txtEmpCodeOld_Edit = (TextBox)fvEdit_dataEmployee.FindControl("txtEmpCodeOld_Edit");
                var txtPrefixEN = (TextBox)fvEdit_dataEmployee.FindControl("txtPrefixEN");
                var txtEdit_Nationality = (TextBox)fvEdit_dataEmployee.FindControl("txtEdit_Nationality");
                var txtEdit_Race = (TextBox)fvEdit_dataEmployee.FindControl("txtEdit_Race");
                var txtEdit_Religion = (TextBox)fvEdit_dataEmployee.FindControl("txtEdit_Religion");
                var txtEdit_Country = (TextBox)fvEdit_dataEmployee.FindControl("txtEdit_Country");
                var txtEdit_Province = (TextBox)fvEdit_dataEmployee.FindControl("txtEdit_Province");
                var txtEdit_Amphoe = (TextBox)fvEdit_dataEmployee.FindControl("txtEdit_Amphoe");
                var txtEdit_District = (TextBox)fvEdit_dataEmployee.FindControl("txtEdit_District");


                ddlNationality_Edit(0, txtEdit_Nationality.Text);
                ddPrefixTHEAction(0, txtPrefixTHE.Text);
                ddlPrefixEN_Edit(0, txtPrefixEN.Text);
                ddlRace_Edit(0, txtEdit_Race.Text);
                ddlReligion_Edit(0, txtEdit_Religion.Text);
                ddlCountry_Edit(0, txtEdit_Country.Text);
                ddlProvince_Edit(0, txtEdit_Province.Text);

                if (txtEdit_Amphoe.Text != "0")
                { ddlAmphoe_Edit(0, txtEdit_Amphoe.Text); }
                else if (txtEdit_Amphoe.Text == "0")
                { ddlAmphoe_Edit(1, "0"); }
                if (txtEdit_District.Text != "0")
                { ddDistrict_Edit(0, txtEdit_District.Text); }
                else if (txtEdit_District.Text == "0")
                { ddDistrict_Edit(1, "0"); }


                var txtidSex = (TextBox)fvEdit_dataEmployee.FindControl("txtidSex");
                var edit_Military = (Panel)fvEdit_dataEmployeeTHai.FindControl("edit_Military");

                if (txtidSex.Text == "2")
                { edit_Military.Visible = false; }
                else if (txtidSex.Text != "2")
                { edit_Military.Visible = true; }

                ddlBloodgroup_edit(0, "0");
                penalty_history();

                //สถานะพนักงาน
                var chk_probation = (CheckBox)fvEdit_dataEmployee.FindControl("chk_probation");
                var chk_pass_probation = (CheckBox)fvEdit_dataEmployee.FindControl("chk_pass_probation");
                var chk_termination = (CheckBox)fvEdit_dataEmployee.FindControl("chk_termination");
                var txt_status_employee = (TextBox)fvEdit_dataEmployee.FindControl("txt_status_employee");
                var txtdate_probation_edit = (TextBox)fvEdit_dataEmployee.FindControl("txtdate_probation_edit");
                var box_probation_date = (Panel)fvEdit_dataEmployee.FindControl("box_probation_date");
                var box_probation_edit = (Panel)fvEdit_dataEmployee.FindControl("box_probation_edit");
                var box_emp_end_date = (Panel)fvEdit_dataEmployee.FindControl("box_emp_end_date");
                var txtdate_probation_show = (TextBox)box_probation_date.FindControl("txtdate_probation_show");
                var doing = (Label)fvEdit_dataEmployee.FindControl("doing");
                

                if (txt_status_employee.Text == "1")
                {
                    chk_pass_probation.Checked = true;
                    box_probation_date.Visible = true;
                    chk_probation.Visible = false;
                    doing.Visible = false;
                    box_emp_end_date.Visible = false;

                }
                else if (txt_status_employee.Text == "2")
                {
                    chk_probation.Checked = true;
                    doing.Visible = true;
                    box_probation_edit.Visible = false;
                    box_emp_end_date.Visible = false;
                }
                else if (txt_status_employee.Text == "9")
                {
                    chk_termination.Checked = true;
                    box_emp_end_date.Visible = true;
                    box_probation_date.Visible = true;
                    box_probation_edit.Visible = false;
                    doing.Visible = false;
                    chk_probation.Visible = false;
                    chk_pass_probation.Enabled = false;
                    chk_termination.Enabled = false;
                }


                break;

            #region ปุ่มรีเซ็ต password
            case "cmdreset_pass":
                //string IP = HttpContext.Current.Request.Params["HTTP_CLIENT_IP"] ?? HttpContext.Current.Request.UserHostAddress;
                //  var ip_address = Request.ServerVariables["REMOTE_ADDR"];
                string remoteIP = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                // Response.Write(remoteIP);
                ViewState["emp_idx_resetpass"] = int.Parse(cmdArg);

                employee_detail list_employee = new employee_detail();
                _data_employee.employee_list = new employee_detail[1];

                list_employee.type_reset = 1;
                list_employee.emp_idx = (int)ViewState["emp_idx_resetpass"];
                list_employee.emp_editor = (int)Session["emp_idx"];
                //list_employee.ip_address = ip_address;

                _data_employee.employee_list[0] = list_employee;
                //check_XML.Text = HttpUtility.HtmlEncode(remoteIP);
                // check_XML.Text = remoteIP;

                _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee, 300);
                _data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
                ViewState["returnCode_resetpass"] = _data_employee.return_value;

                if (_data_employee.return_value == "01")
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('รีเซ็ตรหัสผ่านสำเร็จ');", true);
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('!!! รีเซ็ตรหัสผ่านไม่สำเร็จ');", true);
                }
                break;
            #endregion


            case "cmddelete":
                ViewState["emp_idx"] = int.Parse(cmdArg);
                DataEmployee.type_reset = 1;
                DataEmployee.emp_idx = (int)ViewState["emp_idx"];
                _data_employee.employee_list[0] = DataEmployee;

                _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee, 900);
                _data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
                Page.Response.Redirect(Page.Request.Url.ToString(), true);

                break;

            case "btnSaveWarning":
                penalty_history();
                insert_penalty();

                ddl_type_penalty.SelectedValue = "0";
                ddl_type_penalty.SelectedValue = "0";
                ddl_next_penalty.SelectedValue = "0";
                txt_start_date_break.Text = String.Empty;
                txt_end_date_break.Text = String.Empty;
                ddl_type_penalty.SelectedValue = "0";
                ddl_type_penalty.SelectedValue = "0";
                txt_date_of_warning.Text = String.Empty;
                txt_number_penalty.Text = String.Empty;
                txt_date_delinquent.Text = String.Empty;
                txt_details_penalty.Text = String.Empty;
                name_of_warning.Text = String.Empty;

                break;
            case "BtnadjustPay":
                BoxEdit_Sarary.Visible = true;
                BoxPenaltyEmployee.Visible = false;
                BoxRemovePosisionn.Visible = false;
                showalert_position.Visible = false;
                Show_Edit_Sarary();
                Select_listbef();
                Select_CheckboxList_Benefits_OnShow();
                History_Sarary();
                break;

            case "BtnRemovePosision":
                gv_show_present_position();
                MvMaster.SetActiveView(viewEdit_dataEmployee);

                break;
            case "btnPenaltyEmployee":
                BoxPenaltyEmployee.Visible = true;
                BoxRemovePosisionn.Visible = false;
                BoxEdit_Sarary.Visible = false;
                showalert_position.Visible = false;
                break;
            case "btnBack":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;
            case "BtnSaveEdit":
                update_dataEmployee();

                TextBox txtEmpCode_new_Edit1 = (TextBox)fvEdit_dataEmployee.FindControl("txtEmpCode_new_Edit");
                ViewState["code_employee"] = txtEmpCode_new_Edit1.Text;
                // Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btn_search_data":

                if (ddl_Select_Department_search.SelectedValue == "0" && txt_search.Text == "")
                {
                    SelectData_Employee();
                }
                else
                {
                    data_employee Search_data_employee = new data_employee();
                    employee_detail searching = new employee_detail();
                    Search_data_employee.employee_list = new employee_detail[1];

                    searching.org_idx = int.Parse(ddl_Select_Organization_search.SelectedValue);
                    searching.rdept_idx = int.Parse(ddl_Select_Department_search.SelectedValue);
                    searching.Search = txt_search.Text;

                    Search_data_employee.employee_list[0] = searching;

                    _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", Search_data_employee, 222);
                    Search_data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

                    gvEmployee.DataSource = Search_data_employee.employee_list;
                    gvEmployee.DataBind();

                }

                break;

            case "reset":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "btn_save_sarary":
                Select_tabel_Benefits();
                GridView gvBenefitsList = (GridView)fvEdit_Sarary.FindControl("gvBenefitsList");
                var cbrecipients = ((CheckBoxList)fvEdit_Sarary.FindControl("cbrecipients"));
                foreach (ListItem cbrecipient in cbrecipients.Items)
                {
                    if (cbrecipient.Selected)
                    {

                        var ds_listBef = (DataSet)ViewState["Create_DataBenefits"];
                        var dr_listBef = ds_listBef.Tables[0].NewRow();

                        dr_listBef["BenefitsID"] = cbrecipient.Value;
                        dr_listBef["Benefits_list"] = cbrecipient;

                        ds_listBef.Tables[0].Rows.Add(dr_listBef);
                        ViewState["Create_DataBenefits"] = ds_listBef;

                        gvBenefitsList.DataSource = ds_listBef.Tables[0];
                        gvBenefitsList.DataBind();

                    }
                }

                var txtSarary_old = (TextBox)fvEdit_Sarary.FindControl("txtSarary_old");

                if (txtSarary_old.Text == null || txtSarary_old.Text == "" || ViewState["insert_value"] == "0")
                {
                    insert_sarary_new();
                    History_Sarary();

                }
                else if (txtSarary_old.Text != null || txtSarary_old.Text != "")
                {
                    insert_sarary();
                    History_Sarary();
                }

                ViewState["return_sarary"] = null;
                break;

            case "show_position_edit":

                Panel boxedit_addposition = (Panel)fvEdit_dataEmployee.FindControl("boxedit_addposition");
                boxedit_addposition.Visible = true;
                break;

            case "save_position_edit":
                insert_position_form_edit();
                gv_show_edit_position();
                break;

            case "btnsave_positionnew":

                Position_change();
                show_history_transfer_position();
                MvMaster.SetActiveView(viewEdit_dataEmployee);
                break;

            case "savefile_picture":

                HttpFileCollection upload_picture_profile1 = Request.Files;
                for (int j = 0; j < upload_picture_profile1.Count; j++)
                {
                    HttpPostedFile hpfLoadfile = upload_picture_profile1[j];
                    if (hpfLoadfile.ContentLength > 1)
                    {
                        string getPath_upload = ConfigurationSettings.AppSettings["upload_profile_picture"];
                        string RECode_uploadfile = ViewState["emp_idx_last"].ToString();
                        string fileName_upload = RECode_uploadfile;
                        string filePath_upload = Server.MapPath(getPath_upload + RECode_uploadfile);
                        if (!Directory.Exists(filePath_upload))
                        {
                            Directory.CreateDirectory(filePath_upload);
                        }
                        string extension = Path.GetExtension(hpfLoadfile.FileName);

                        hpfLoadfile.SaveAs(Server.MapPath(getPath_upload + RECode_uploadfile) + "\\" + fileName_upload + extension);

                        ImagProfile_default.Visible = false;
                        select_empIdx_part();
                    }
                }
                break;

            case "save_picture_edit":

                TextBox txtEmpCode_new_Edit = (TextBox)fvEdit_dataEmployee.FindControl("txtEmpCode_new_Edit");
                ViewState["code_employee"] = txtEmpCode_new_Edit.Text;
                HttpFileCollection upload_profile_pic = Request.Files;
                for (int j = 0; j < upload_profile_pic.Count; j++)
                {
                    HttpPostedFile hpfLoadfile_edit = upload_profile_pic[j];
                    if (hpfLoadfile_edit.ContentLength > 1)
                    {
                        string getPath_upload_edit = ConfigurationSettings.AppSettings["upload_profile_picture"];
                        string edit_uploadfile = ViewState["code_employee"].ToString();
                        string fileName_upload_edit = edit_uploadfile;
                        string filePath_upload_edit = Server.MapPath(getPath_upload_edit + edit_uploadfile);

                        if (!Directory.Exists(filePath_upload_edit))
                        {
                            Directory.CreateDirectory(filePath_upload_edit);
                        }
                        string extension_edit = Path.GetExtension(hpfLoadfile_edit.FileName);
                        hpfLoadfile_edit.SaveAs(Server.MapPath(getPath_upload_edit + edit_uploadfile) + "\\" + fileName_upload_edit + extension_edit);

                        show_pic();
                    }

                }
                show_pic();
                MvMaster.SetActiveView(viewEdit_dataEmployee);
                break;

            case "delete_position":

                ViewState["EODSPIDX"] = int.Parse(cmdArg);
                DataEmployee.idx_position = (int)ViewState["EODSPIDX"];
                _data_employee.employee_list[0] = DataEmployee;

                _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee, 901);
                _data_employee = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

                gv_show_edit_position();

                break;

            case "add_Experience":
                boxaddExperience.Visible = true;

                break;

            case "btnaddExperience_edit":

                data_employee experience_data1 = new data_employee();
                experiencelist edit_experience1 = new experiencelist();
                experience_data1.box_experience_ = new experiencelist[1];

                edit_experience1.emp_idx_ = int.Parse(ViewState["emp_idx"].ToString());
                edit_experience1.job__details = txt_Job_Brief_edit.Text;
                edit_experience1.name_company = txtOrganiztion_Old_edit.Text;
                edit_experience1.position_old_company = txtPositionOld_edit.Text;
                edit_experience1.motive_resign = txtMotive_edit.Text;
                edit_experience1.salary_old_company = txtSalary_Old_edit.Text;
                edit_experience1.workIn_old_company = txtWorkInOld_edit.Text;
                edit_experience1.resign_old_company = txtresign_Old_edit.Text;

                experience_data1.box_experience_[0] = edit_experience1;
                _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", experience_data1, 306);
                experience_data1 = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
                var returnCode_Experience = experience_data1.return_code;
                gv_show_edit_experience();

                txt_Job_Brief_edit.Text = String.Empty;
                txtOrganiztion_Old_edit.Text = String.Empty;
                txtPositionOld_edit.Text = String.Empty;
                txtMotive_edit.Text = String.Empty;
                txtSalary_Old_edit.Text = String.Empty;
                txtWorkInOld_edit.Text = String.Empty;
                txtresign_Old_edit.Text = String.Empty;


                break;

            case "delete_exper":

                data_employee _data_employee1 = new data_employee();
                experiencelist edit_experience11 = new experiencelist();
                _data_employee1.box_experience_ = new experiencelist[1];

                ViewState["exper_idx_"] = int.Parse(cmdArg);
                edit_experience11.exper_idx_ = (int)ViewState["exper_idx_"];
                _data_employee1.box_experience_[0] = edit_experience11;

                _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", _data_employee1, 902);
                _data_employee1 = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);
                gv_show_edit_experience();
                break;

            case "tabsetshifttime":

                lbMenu0.BackColor = System.Drawing.Color.Transparent;
                select_listemp_setshiftTime();
                ViewState["empSetShiftTimeIDX"] = "";
                MvMaster.SetActiveView(ViewSetShiftTime);
                break;

            case "editshiftTime":
                ViewState["empSetShiftTimeIDX"] = int.Parse(cmdArg);
                select_informationEmpShift();
                ddl_ShiftTime();
                setFocus.Focus();
                select_history_ShiftTime();
                MvMaster.SetActiveView(VieweditSetShiftTime);

                break;

            case "resetShifttime":

                var Editformview_shiftTime = (FormView)fvEdit_dataEmployee.FindControl("editformview_shiftTime");
                var checkstaticShift_Edit = (RadioButton)Editformview_shiftTime.FindControl("checkstaticShift_edit");
                var checkDynamicShift_Edit = (RadioButton)Editformview_shiftTime.FindControl("checkDynamicShift_edit");
                var ddlist_edit_ShiftTime = (DropDownList)Editformview_shiftTime.FindControl("ddlist_edit_ShiftTime");
                var resetShifttime = (LinkButton)Editformview_shiftTime.FindControl("resetShifttime");

                checkstaticShift_Edit.Checked = false;
                checkDynamicShift_Edit.Checked = false;
                ddlist_edit_ShiftTime.SelectedValue = "0";

                break;

            case "search_shiftTime":
                if (ddl_type_shift.SelectedValue == "0" && txtsearching_shiftTime.Text == "")
                {
                    select_listemp_setshiftTime();
                }
                else
                {
                    data_employee Search_data_shiftTime = new data_employee();
                    ShiftTime searching_shiftTime = new ShiftTime();
                    Search_data_shiftTime.ShiftTime_details = new ShiftTime[1];

                    searching_shiftTime.idxTypeShiftTime = int.Parse(ddl_type_shift.SelectedValue);
                    searching_shiftTime.search_shiftTime = txtsearching_shiftTime.Text;

                    Search_data_shiftTime.ShiftTime_details[0] = searching_shiftTime;
                    _local_xml = _service_exec.actionExec(Conn_manage_employee, "data_employee", "Manage_Employee", Search_data_shiftTime, 248);
                    Search_data_shiftTime = (data_employee)_funcTool.convertXmlToObject(typeof(data_employee), _local_xml);

                    gvListNameShiftTime.DataSource = Search_data_shiftTime.ShiftTime_details;
                    gvListNameShiftTime.DataBind();


                    ddl_type_shift.SelectedValue = "0";
                    txtsearching_shiftTime.Text = String.Empty;
                }


                break;

            case "tabreportExcel":
                lbMenu0.BackColor = System.Drawing.Color.Transparent;
                break;

            case "resetSelect":



                break;
        }
    }
    #endregion

    #region  JS Scripts
    protected void used_java_Scripts()
    {
        string jsemp_start = String.Empty;
        string js_date_picker = String.Empty;
        string jsemp_end = String.Empty;
        string js_conn = "";

        jsemp_start +=

     "var prm = Sys.WebForms.PageRequestManager.getInstance();" +
     "prm.add_endRequest(function() {";
        js_date_picker +=
        "$(function () {" +
            "$('.from-date-datepicker').datetimepicker({" +
              "format: 'DD/MM/YYYY'" +
            "});" +

         "});";


        jsemp_end +=
        "});";

        js_conn = jsemp_start + js_date_picker + jsemp_end;
        Page.ClientScript.RegisterStartupScript(this.GetType(), "", js_conn, true);
    }


    #endregion

    #region  preview picture

    protected string retrunpat(int x)
    {
        string getPath = ConfigurationSettings.AppSettings["upload_profile_picture"];
        string pat = getPath + x.ToString() + "/" + x.ToString() + ".jpg";
        return pat;

    }

    protected string retrunpat_edit(string z)
    {
        string getPath = ConfigurationSettings.AppSettings["upload_profile_picture"];
        string path = getPath + z.ToString() + "/" + z.ToString() + ".jpg";
        return path;

        //if(pat == )
    }

    #endregion

    #region reuse
    protected void btnTrigger(Button btnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger = new PostBackTrigger();
        trigger.ControlID = btnID.UniqueID;
        updatePanel.Triggers.Add(trigger);
    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger trigger = new PostBackTrigger();
        trigger.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(trigger);
    }

    #endregion reuse


}