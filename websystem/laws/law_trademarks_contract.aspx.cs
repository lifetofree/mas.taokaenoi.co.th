﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_laws_law_trademarks_contract : System.Web.UI.Page
{

    #region initial function/data
    function_tool _funcTool = new function_tool();
    service_execute serviceexcute = new service_execute();
    data_employee _dataEmployee = new data_employee();
    data_law _data_law = new data_law();

    static string _serviceUrl = ConfigurationManager.AppSettings["serviceUrl"];

    //-- employee --//
    static string _urlGetMyProfile = _serviceUrl + ConfigurationManager.AppSettings["urlGetMyProfile"];
    static string _urlGetOrganizationList = _serviceUrl + ConfigurationManager.AppSettings["urlGetOrganizationList"];
    static string _urlGetDepartmentList = _serviceUrl + ConfigurationManager.AppSettings["urlGetDepartmentList"];
    static string _urlGetSectionList = _serviceUrl + ConfigurationManager.AppSettings["urlGetSectionList"];
    static string urlGetAll = _serviceUrl + ConfigurationManager.AppSettings["urlGetAll"];
    //-- employee --//

    //-- trademarks --//
    static string _urlGetLawContryDetail = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawContryDetail"];
    static string _urlGetLawM0DocType = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0DocType"];
    static string _urlGetLawM0SubDocType = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0SubDocType"];
    static string _urlGetLawM0JobType = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0JobType"];

    static string _urlSetLawCreateDocument = _serviceUrl + ConfigurationManager.AppSettings["urlSetLawCreateDocument"];
    static string _urlGetLawDetailDocument = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawDetailDocument"];
    static string _urlGetLawLogDetailDocument = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawLogDetailDocument"];
    static string _urlGetLawWaitApproveDetailDoc = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawWaitApproveDetailDoc"];
    static string _urlGetLawStatusApprove = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawStatusApprove"];
    static string _urlSetLawApproveDocument = _serviceUrl + ConfigurationManager.AppSettings["urlSetLawApproveDocument"];
    static string _urlGetSelectLawInDocument = _serviceUrl + ConfigurationManager.AppSettings["urlGetSelectLawInDocument"];
    static string _urlGetViewDetailLawOfficer = _serviceUrl + ConfigurationManager.AppSettings["urlGetViewDetailLawOfficer"];
    static string _urlGetLawRecordSheet = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawRecordSheet"];
    static string _urlGetCountLawWaitApprove = _serviceUrl + ConfigurationManager.AppSettings["urlGetCountLawWaitApprove"];
    static string _urlSetLawUpdateDocument = _serviceUrl + ConfigurationManager.AppSettings["urlSetLawUpdateDocument"];
    static string _urlGetLawM0Owner = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0Owner"];
    static string _urlGetLawM0Incorporated = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0Incorporated"];
    static string _urlGetLawM0Trademark = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0Trademark"];
    static string _urlGetLawM0RecordStatus = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0RecordStatus"];
    static string _urlGetLawM0LocalLanguage = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0LocalLanguage"];
    static string _urlGetLawM0Actions = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0Actions"];
    static string _urlGetLawM0TypeOfRegistration = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0TypeOfRegistration"];
    static string _urlGetLawM0TypeOfWork = _serviceUrl + ConfigurationManager.AppSettings["urlGetLawM0TypeOfWork"];
    static string _urlGetSearchDetailContract = _serviceUrl + ConfigurationManager.AppSettings["urlGetSearchDetailContract"];


    //path file
    static string _path_file_law_usercreate = ConfigurationManager.AppSettings["path_file_law_usercreate"];
    static string _path_file_law_lawmemo = ConfigurationManager.AppSettings["path_file_law_lawmemo"];
    static string _path_file_law_usermemo = ConfigurationManager.AppSettings["path_file_law_usermemo"];

    int _emp_idx = 0;
    int _default_int = 0;
    string _local_xml = String.Empty;
    string _localJson = String.Empty;


    #endregion initial function/data

    protected void Page_Init(object sender, EventArgs e)
    {
        _emp_idx = int.Parse(Session["emp_idx"].ToString());

        getEmployeeProfile(_emp_idx);

        _dataEmployee = (data_employee)ViewState["vsEmpProfile"];

        ViewState["org_permission"] = _dataEmployee.employee_list[0].org_idx;
        ViewState["rsec_permission"] = _dataEmployee.employee_list[0].rsec_idx;
        ViewState["rdept_permission"] = _dataEmployee.employee_list[0].rdept_idx;
        ViewState["rpos_permission"] = _dataEmployee.employee_list[0].rpos_idx;
        ViewState["joblevel_permission"] = _dataEmployee.employee_list[0].jobgrade_level;
        ViewState["emp_type_permission"] = _dataEmployee.employee_list[0].emp_type_idx;

    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            initPage();
            initPageLoad();
            linkBtnTrigger(lbCreate);
            linkBtnTrigger(lbWaitApprove);

            getCountWaitApproveDocument();

            //data table
            getPrioritiesList();
            getClientList();
            getAgentList();
            getGoodsList();

        }
    }

    #region set/get bind data

    ////protected void getViewDetailRecordSheet(FormView fvName, int _u1doc_idx, int _type_idx, int _chek_viewtab)
    ////{

    ////    data_law data_u1_doc_viewdetail = new data_law();
    ////    tcm_u1_document_detail u1_document_viewdetail = new tcm_u1_document_detail();
    ////    data_u1_doc_viewdetail.tcm_u1_document_list = new tcm_u1_document_detail[1];
    ////    u1_document_viewdetail.u1_doc_idx = _u1doc_idx;
    ////    u1_document_viewdetail.type_idx = _type_idx;

    ////    data_u1_doc_viewdetail.tcm_u1_document_list[0] = u1_document_viewdetail;

    ////    data_u1_doc_viewdetail = callServicePostLaw(_urlGetLawViewRecordSheet, data_u1_doc_viewdetail);
    ////    //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1_doc_viewdetail));
    ////    //////setGridData(gvName, data_u0_document_detail.tcm_u0_document_list);

    ////    if (data_u1_doc_viewdetail.return_code == 0)
    ////    {

    ////        if (_chek_viewtab == 0) // view record //
    ////        {
    ////            setFormData(fvName, FormViewMode.ReadOnly, data_u1_doc_viewdetail.tcm_u1_document_list);
    ////        }
    ////        else // edit/save record //
    ////        {
    ////            setFormData(fvName, FormViewMode.Edit, data_u1_doc_viewdetail.tcm_u1_document_list);
    ////        }


    ////        //////check Country in Document
    ////        ////CheckBoxList chkCountryview = (CheckBoxList)fvName.FindControl("chkCountryview");
    ////        ////getCountry(chkCountryview, 0);
    ////        ////string _valuecountry = data_u0_doc_viewdetail.tcm_u0_document_list[0].country_idx_checkdetail.ToString();
    ////        ////string[] _country_check = _valuecountry.Split(',');

    ////        ////foreach (ListItem item in chkCountryview.Items)
    ////        ////{
    ////        ////    foreach (string _i in _country_check)
    ////        ////    {

    ////        ////        if (item.Value == _i)
    ////        ////        {
    ////        ////            item.Selected = true;
    ////        ////        }
    ////        ////    }
    ////        ////}
    ////    }

    ////}

    protected void getCountWaitApproveDocument()
    {
        data_law data_u0_document_countwait = new data_law();
        tcm_u0_document_detail u0_document_countwait = new tcm_u0_document_detail();
        data_u0_document_countwait.tcm_u0_document_list = new tcm_u0_document_detail[1];

        u0_document_countwait.u0_doc_idx = 0;
        u0_document_countwait.cemp_idx = _emp_idx;
        u0_document_countwait.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0_document_countwait.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        u0_document_countwait.jobgrade_level = int.Parse(ViewState["joblevel_permission"].ToString());

        data_u0_document_countwait.tcm_u0_document_list[0] = u0_document_countwait;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_countwait));
        data_u0_document_countwait = callServicePostLaw(_urlGetCountLawWaitApprove, data_u0_document_countwait);
        //
        // setGridData(gvName, data_u0_document_wait.tcm_u0_document_list);

        if (data_u0_document_countwait.tcm_u0_document_list[0].count_waitApprove > 0)
        {
            ViewState["vs_CountWaitApprove"] = data_u0_document_countwait.tcm_u0_document_list[0].count_waitApprove; // count all wait approve
            lbWaitApprove.Text = " Waiting Approve <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
            setActiveTab("docWaitApprove", 0, 0, 0, 0, 0, 0, 0, 0);

        }
        else
        {
            ViewState["vs_CountWaitApprove"] = 0; // count all wait approve
            lbWaitApprove.Text = " Waiting Approve <span class='badge progress-bar-danger' >" + Convert.ToString(ViewState["vs_CountWaitApprove"]) + "</span>";
            setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0, 0);

        }
        

    }

    protected void getDetailRecordSheet(GridView gvName, int _u1doc_idx)
    {
        data_law data_u1_document_record = new data_law();
        tcm_u1_document_detail u1_document_record = new tcm_u1_document_detail();
        data_u1_document_record.tcm_u1_document_list = new tcm_u1_document_detail[1];

        u1_document_record.u1_doc_idx = _u1doc_idx;

        data_u1_document_record.tcm_u1_document_list[0] = u1_document_record;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));
        data_u1_document_record = callServicePostLaw(_urlGetLawRecordSheet, data_u1_document_record);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u3_document_viewdetail));
        if(data_u1_document_record.return_code == 0)
        {


            ViewState["Vs_GvDetailRecordSheet"] = data_u1_document_record.tcm_u1_document_list;
            setGridData(gvName, ViewState["Vs_GvDetailRecordSheet"]);
        }
        else
        {
            setGridData(gvName, null);
        }
            
    }

    protected void getViewDetailRecordSheet(int _u1doc_idx, int _type_idx, int _chek_viewtab)
    {
        data_law data_u1_document_viewrecord = new data_law();
        tcm_u1_document_detail u1_document_viewrecord = new tcm_u1_document_detail();
        data_u1_document_viewrecord.tcm_u1_document_list = new tcm_u1_document_detail[1];

        u1_document_viewrecord.u1_doc_idx = _u1doc_idx;
        u1_document_viewrecord.type_idx = _type_idx;

        data_u1_document_viewrecord.tcm_u1_document_list[0] = u1_document_viewrecord;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));
        data_u1_document_viewrecord = callServicePostLaw(_urlGetLawRecordSheet, data_u1_document_viewrecord);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u3_document_viewdetail));

        if (data_u1_document_viewrecord.return_code == 0)
        {
           
            ViewState["vs_detailrecordsheet"] = data_u1_document_viewrecord.tcm_u1_document_list;

            ////if (_chek_viewtab == 0) // view record //
            ////{
            ////    setFormData(fvName, FormViewMode.ReadOnly, ViewState["vs_detailrecordsheet"]);
            ////}
            ////else // edit/save record //
            ////{
            ////    setFormData(fvName, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);
            ////}


            //////check Country in Document
            ////CheckBoxList chkCountryview = (CheckBoxList)fvName.FindControl("chkCountryview");
            ////getCountry(chkCountryview, 0);
            ////string _valuecountry = data_u0_doc_viewdetail.tcm_u0_document_list[0].country_idx_checkdetail.ToString();
            ////string[] _country_check = _valuecountry.Split(',');

            ////foreach (ListItem item in chkCountryview.Items)
            ////{
            ////    foreach (string _i in _country_check)
            ////    {

            ////        if (item.Value == _i)
            ////        {
            ////            item.Selected = true;
            ////        }
            ////    }
            ////}
        }

    }

    protected void getViewDetailLawOfficer(GridView gvName, int _u0doc_idx)
    {
        data_law data_u3_document_viewdetail = new data_law();
        tcm_u3_document_detail u3_document_viewdetail = new tcm_u3_document_detail();
        data_u3_document_viewdetail.tcm_u3_document_list = new tcm_u3_document_detail[1];
        u3_document_viewdetail.u0_doc_idx = _u0doc_idx;
        data_u3_document_viewdetail.tcm_u3_document_list[0] = u3_document_viewdetail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));
        data_u3_document_viewdetail = callServicePostLaw(_urlGetViewDetailLawOfficer, data_u3_document_viewdetail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u3_document_viewdetail));

        setGridData(gvName, data_u3_document_viewdetail.tcm_u3_document_list);

    }

    protected void getLawOfficer(CheckBoxList chkName, int _emp_idx)
    {

        data_law data_u3_document_detail = new data_law();
        tcm_u3_document_detail u3_document_detail = new tcm_u3_document_detail();
        data_u3_document_detail.tcm_u3_document_list = new tcm_u3_document_detail[1];

        data_u3_document_detail.tcm_u3_document_list[0] = u3_document_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_u3_document_detail = callServicePostLaw(_urlGetSelectLawInDocument, data_u3_document_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setChkData(chkName, data_u3_document_detail.tcm_u3_document_list, "emp_name_en", "emp_idx");
        //chkName.Items.Insert(0, new ListItem("--- Select ---", "0"));

    }

    protected void getCountry(CheckBoxList chkName, int _country_idx)
    {

        data_law data_m0_country_detail = new data_law();
        tcm_m0_country_detail m0_country_detail = new tcm_m0_country_detail();
        data_m0_country_detail.tcm_m0_country_list = new tcm_m0_country_detail[1];

        data_m0_country_detail.tcm_m0_country_list[0] = m0_country_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_m0_country_detail = callServicePostLaw(_urlGetLawContryDetail, data_m0_country_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setChkData(chkName, data_m0_country_detail.tcm_m0_country_list, "country_name_en", "country_idx");
        //chkName.Items.Insert(0, new ListItem("--- Select ---", "0"));

    }

    protected void getDoctypeDetail(DropDownList ddlName, int _doctype_idx)
    {

        data_law data_m0_type_detail = new data_law();
        tcm_m0_type_detail m0_type_detail = new tcm_m0_type_detail();
        data_m0_type_detail.tcm_m0_type_list = new tcm_m0_type_detail[1];
        m0_type_detail.condition = 1;
        data_m0_type_detail.tcm_m0_type_list[0] = m0_type_detail;

        data_m0_type_detail = callServicePostLaw(_urlGetLawM0DocType, data_m0_type_detail);
        setDdlData(ddlName, data_m0_type_detail.tcm_m0_type_list, "type_name_en", "type_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Document Type ---", "0"));
        ddlName.SelectedValue = _doctype_idx.ToString();
        ////setGridData(GvDetail, data_m0_type_detail.tcm_m0_type_list);

    }

    protected void getSubDoctypeDetail(DropDownList ddlName, int _doctype_idx)
    {

        data_law data_m0_subtype_detail = new data_law();
        tcm_m0_subtype_detail m0_subtype_detail = new tcm_m0_subtype_detail();
        data_m0_subtype_detail.tcm_m0_subtype_list = new tcm_m0_subtype_detail[1];
        m0_subtype_detail.condition = 2;
        m0_subtype_detail.type_idx = _doctype_idx;
        data_m0_subtype_detail.tcm_m0_subtype_list[0] = m0_subtype_detail;

        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_subtype_detail));
        data_m0_subtype_detail = callServicePostLaw(_urlGetLawM0SubDocType, data_m0_subtype_detail);
        setDdlData(ddlName, data_m0_subtype_detail.tcm_m0_subtype_list, "subtype_name_en", "subtype_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Sub Document Type ---", "0"));


    }

    protected void getJobType(DropDownList ddlName ,int _jobtype_idx)
    {

        data_law data_m0_jobtype_detail = new data_law();
        tcm_m0_jobtype_detail m0_jobtype_detail = new tcm_m0_jobtype_detail();
        data_m0_jobtype_detail.tcm_m0_jobtype_list = new tcm_m0_jobtype_detail[1];

        data_m0_jobtype_detail.tcm_m0_jobtype_list[0] = m0_jobtype_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_m0_jobtype_detail = callServicePostLaw(_urlGetLawM0JobType, data_m0_jobtype_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_jobtype_detail.tcm_m0_jobtype_list, "jobtype_name_en", "jobtype_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Job Type ---", "0"));
        //chkName.Items.Insert(0, new ListItem("--- Select ---", "0"));

    }

    protected void getDetailDocumentTrademark(GridView gvName, int _u0doc_idx)
    {
        data_law data_u0_document_detail = new data_law();
        tcm_u0_document_detail u0_document_detail = new tcm_u0_document_detail();
        data_u0_document_detail.tcm_u0_document_list = new tcm_u0_document_detail[1];
        u0_document_detail.u0_doc_idx = _u0doc_idx;
        u0_document_detail.cemp_idx = _emp_idx;
        u0_document_detail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0_document_detail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        u0_document_detail.jobgrade_level = int.Parse(ViewState["joblevel_permission"].ToString());

        data_u0_document_detail.tcm_u0_document_list[0] = u0_document_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_u0_document_detail = callServicePostLaw(_urlGetLawDetailDocument, data_u0_document_detail);
        ViewState["Vs_DetailContract_ALL"] = data_u0_document_detail.tcm_u0_document_list;
        ViewState["Vs_DetailContract"] = data_u0_document_detail.tcm_u0_document_list;
        setGridData(gvName, ViewState["Vs_DetailContract"]);


    }

    protected void getViewDetailDocumentTrademark(FormView fvName, int _u0doc_idx)
    {
        data_law data_u0_doc_viewdetail = new data_law();
        tcm_u0_document_detail u0_document_viewdetail = new tcm_u0_document_detail();
        data_u0_doc_viewdetail.tcm_u0_document_list = new tcm_u0_document_detail[1];
        u0_document_viewdetail.u0_doc_idx = _u0doc_idx;

        data_u0_doc_viewdetail.tcm_u0_document_list[0] = u0_document_viewdetail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_u0_doc_viewdetail = callServicePostLaw(_urlGetLawDetailDocument, data_u0_doc_viewdetail);
        //setGridData(gvName, data_u0_document_detail.tcm_u0_document_list);
        if (data_u0_doc_viewdetail.return_code == 0)
        {
            setFormData(fvName, FormViewMode.ReadOnly, data_u0_doc_viewdetail.tcm_u0_document_list);

            //check Country in Document
            CheckBoxList chkCountryview = (CheckBoxList)fvName.FindControl("chkCountryview");
            getCountry(chkCountryview, 0);
            string _valuecountry = data_u0_doc_viewdetail.tcm_u0_document_list[0].country_idx_checkdetail.ToString();
            string[] _country_check = _valuecountry.Split(',');

            foreach (ListItem item in chkCountryview.Items)
            {
                foreach (string _i in _country_check)
                {

                    if(item.Value == _i)
                    {
                        item.Selected = true;
                    }
                }
            }
        }

    }

    protected void getViewDetailEditDocumentTrademark(FormView fvName, int _u0doc_idx)
    {
        data_law data_u0_doc_viewdetail = new data_law();
        tcm_u0_document_detail u0_document_viewdetail = new tcm_u0_document_detail();
        data_u0_doc_viewdetail.tcm_u0_document_list = new tcm_u0_document_detail[1];
        u0_document_viewdetail.u0_doc_idx = _u0doc_idx;

        data_u0_doc_viewdetail.tcm_u0_document_list[0] = u0_document_viewdetail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_u0_doc_viewdetail = callServicePostLaw(_urlGetLawDetailDocument, data_u0_doc_viewdetail);
        //setGridData(gvName, data_u0_document_detail.tcm_u0_document_list);
        if (data_u0_doc_viewdetail.return_code == 0)
        {
            setFormData(fvName, FormViewMode.Edit, data_u0_doc_viewdetail.tcm_u0_document_list);

            //check Country in Document
            CheckBoxList chkCountryview = (CheckBoxList)fvName.FindControl("chkCountry");
            getCountry(chkCountryview, 0);
            string _valuecountry = data_u0_doc_viewdetail.tcm_u0_document_list[0].country_idx_checkdetail.ToString();
            string[] _country_check = _valuecountry.Split(',');

            foreach (ListItem item in chkCountryview.Items)
            {
                foreach (string _i in _country_check)
                {

                    if (item.Value == _i)
                    {
                        item.Selected = true;
                    }
                }
            }
        }

    }

    protected void getLogDetailDocument(Repeater rpName, int _u0doc_idx, int _u1doc_idx)
    {
        data_law data_u0_doc_log = new data_law();
        tcm_u2_document_detail u0_document_log = new tcm_u2_document_detail();
        data_u0_doc_log.tcm_u2_document_list = new tcm_u2_document_detail[1];
        u0_document_log.u0_doc_idx = _u0doc_idx;
        u0_document_log.u1_doc_idx = _u1doc_idx;

        data_u0_doc_log.tcm_u2_document_list[0] = u0_document_log;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_u0_doc_log = callServicePostLaw(_urlGetLawLogDetailDocument, data_u0_doc_log);

        setRepeaterData(rpName, data_u0_doc_log.tcm_u2_document_list);
    }

    protected void getDetailWaitApproveDocument(GridView gvName, int _u0doc_idx)
    {
        data_law data_u0_document_wait = new data_law();
        tcm_u0_document_detail u0_document_wait = new tcm_u0_document_detail();
        data_u0_document_wait.tcm_u0_document_list = new tcm_u0_document_detail[1];
        u0_document_wait.u0_doc_idx = _u0doc_idx;
        u0_document_wait.cemp_idx = _emp_idx;
        u0_document_wait.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
        u0_document_wait.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
        u0_document_wait.jobgrade_level = int.Parse(ViewState["joblevel_permission"].ToString());

        data_u0_document_wait.tcm_u0_document_list[0] = u0_document_wait;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0_document_wait));
        data_u0_document_wait = callServicePostLaw(_urlGetLawWaitApproveDetailDoc, data_u0_document_wait);
        setGridData(gvName, data_u0_document_wait.tcm_u0_document_list);


    }

    protected void getDecisionApprove(DropDownList ddlName, int _noidx)
    {

        data_law data_m0_decision_detail = new data_law();
        tcm_m0_decision_detail m0_decision_detail = new tcm_m0_decision_detail();
        data_m0_decision_detail.tcm_m0_decision_list = new tcm_m0_decision_detail[1];

        m0_decision_detail.noidx = _noidx;

        data_m0_decision_detail.tcm_m0_decision_list[0] = m0_decision_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_m0_decision_detail = callServicePostLaw(_urlGetLawStatusApprove, data_m0_decision_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_decision_detail.tcm_m0_decision_list, "decision_name", "decision_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Status ---", "0"));
       

    }

    protected void getDecisionAttachFileApprove(FormView fvName, int _noidx)
    {

        data_law data_m0_decision_detail = new data_law();
        tcm_m0_decision_detail m0_decision_detail = new tcm_m0_decision_detail();
        data_m0_decision_detail.tcm_m0_decision_list = new tcm_m0_decision_detail[1];

        m0_decision_detail.noidx = _noidx;

        data_m0_decision_detail.tcm_m0_decision_list[0] = m0_decision_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_m0_decision_detail = callServicePostLaw(_urlGetLawStatusApprove, data_m0_decision_detail);
        Label lbldecision_idx = (Label)fvName.FindControl("lbldecision_idx");

        lbldecision_idx.Text = data_m0_decision_detail.tcm_m0_decision_list[0].decision_idx.ToString();

    }

    protected void getOwner(DropDownList ddlName, int _owner_idx)
    {

        data_law data_m0_owner_detail = new data_law();
        tcm_m0_owner_detail m0_owner_detail = new tcm_m0_owner_detail();
        data_m0_owner_detail.tcm_m0_owner_list = new tcm_m0_owner_detail[1];

        m0_owner_detail.owner_idx = _owner_idx;

        data_m0_owner_detail.tcm_m0_owner_list[0] = m0_owner_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_m0_owner_detail = callServicePostLaw(_urlGetLawM0Owner, data_m0_owner_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_owner_detail.tcm_m0_owner_list, "owner_name_en", "owner_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Owner ---", "0"));
        ddlName.SelectedValue = _owner_idx.ToString();
        if (_owner_idx != 0)
        {
            TextBox tbowner_address_en = (TextBox)fvRegistrationDetail.FindControl("tbowner_address_en");
            tbowner_address_en.Text = data_m0_owner_detail.tcm_m0_owner_list[0].owner_address_en.ToString();

        }
        else
        {
            TextBox tbowner_address_en = (TextBox)fvRegistrationDetail.FindControl("tbowner_address_en");
            tbowner_address_en.Text = string.Empty;
        }

    }

    protected void getIncorporated(DropDownList ddlName, int _Incorporated_in_idx)
    {

        data_law data_m0_Incorporated_detail = new data_law();
        tcm_m0_Incorporated_detail m0_Incorporated_detail = new tcm_m0_Incorporated_detail();
        data_m0_Incorporated_detail.tcm_m0_Incorporated_list = new tcm_m0_Incorporated_detail[1];

        m0_Incorporated_detail.Incorporated_in_idx = _Incorporated_in_idx;

        data_m0_Incorporated_detail.tcm_m0_Incorporated_list[0] = m0_Incorporated_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_m0_Incorporated_detail = callServicePostLaw(_urlGetLawM0Incorporated, data_m0_Incorporated_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_Incorporated_detail.tcm_m0_Incorporated_list, "Incorporated_in_en", "Incorporated_in_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Incorporated In ---", "0"));
        ddlName.SelectedValue = _Incorporated_in_idx.ToString();

    }

    protected void getTrademarkStatus(DropDownList ddlName, int _trademark_status_idx)
    {

        data_law data_m0_trademark_status = new data_law();
        tcm_m0_trademark_status_detail m0_trademark_status_detail = new tcm_m0_trademark_status_detail();
        data_m0_trademark_status.tcm_m0_trademark_status_list = new tcm_m0_trademark_status_detail[1];

        m0_trademark_status_detail.trademark_status_idx = _trademark_status_idx;

        data_m0_trademark_status.tcm_m0_trademark_status_list[0] = m0_trademark_status_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_m0_trademark_status = callServicePostLaw(_urlGetLawM0Trademark, data_m0_trademark_status);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_trademark_status.tcm_m0_trademark_status_list, "trademark_status_en", "trademark_status_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Status ---", "0"));
        ddlName.SelectedValue = _trademark_status_idx.ToString();

    }

    protected void getRecordSheetStatus(DropDownList ddlName, int _status_idx)
    {

        data_law data_m0_recordsheet_status = new data_law();
        tcm_m0_recordsheet_status_detail m0_recordsheet_status_detail = new tcm_m0_recordsheet_status_detail();
        data_m0_recordsheet_status.tcm_m0_recordsheet_status_list = new tcm_m0_recordsheet_status_detail[1];

        m0_recordsheet_status_detail.status_idx = _status_idx;

        data_m0_recordsheet_status.tcm_m0_recordsheet_status_list[0] = m0_recordsheet_status_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_m0_recordsheet_status = callServicePostLaw(_urlGetLawM0RecordStatus, data_m0_recordsheet_status);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_recordsheet_status.tcm_m0_recordsheet_status_list, "status_name_en", "status_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Status ---", "0"));
        ddlName.SelectedValue = _status_idx.ToString();

    }

    protected void getLocalLanguage(DropDownList ddlName, int _language_idx)
    {

        data_law data_m0_local_language = new data_law();
        tcm_m0_local_language_detail m0_local_language_detail = new tcm_m0_local_language_detail();
        data_m0_local_language.tcm_m0_local_language_list = new tcm_m0_local_language_detail[1];

        m0_local_language_detail.language_idx = _language_idx;

        data_m0_local_language.tcm_m0_local_language_list[0] = m0_local_language_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_m0_local_language = callServicePostLaw(_urlGetLawM0LocalLanguage, data_m0_local_language);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_local_language.tcm_m0_local_language_list, "language_en", "language_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Local Language ---", "0"));
        ddlName.SelectedValue = _language_idx.ToString();

    }

    protected void getRecordsheetCountry(DropDownList ddlName, int _country_idx)
    {

        data_law data_m0_country = new data_law();
        tcm_m0_country_detail m0_country_detail = new tcm_m0_country_detail();
        data_m0_country.tcm_m0_country_list = new tcm_m0_country_detail[1];

        m0_country_detail.country_idx = _country_idx;

        data_m0_country.tcm_m0_country_list[0] = m0_country_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_m0_country = callServicePostLaw(_urlGetLawContryDetail, data_m0_country);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_country.tcm_m0_country_list, "country_name_en", "country_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Country ---", "0"));
        ddlName.SelectedValue = _country_idx.ToString();

    }

    protected void getLawM0Actions(GridView gvName, int _action_idx)
    {
        data_law data_m0_actions_detail = new data_law();
        tcm_m0_actions_detail m0_actions_detail = new tcm_m0_actions_detail();
        data_m0_actions_detail.tcm_m0_actions_list = new tcm_m0_actions_detail[1];
        m0_actions_detail.action_idx = _action_idx;
       

        data_m0_actions_detail.tcm_m0_actions_list[0] = m0_actions_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        //
        data_m0_actions_detail = callServicePostLaw(_urlGetLawM0Actions, data_m0_actions_detail);
        setGridData(gvName, data_m0_actions_detail.tcm_m0_actions_list);


    }

    protected void getTypeOfRegistrtion(DropDownList ddlName, int _typeofregistration_idx)
    {

        data_law data_m0_typeofregistration_detail = new data_law();
        tcm_m0_typeofregistration_detail m0_typeofregistration_detail = new tcm_m0_typeofregistration_detail();
        data_m0_typeofregistration_detail.tcm_m0_typeofregistration_list = new tcm_m0_typeofregistration_detail[1];

        m0_typeofregistration_detail.typeofregistration_idx = _typeofregistration_idx;

        data_m0_typeofregistration_detail.tcm_m0_typeofregistration_list[0] = m0_typeofregistration_detail;
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

        data_m0_typeofregistration_detail = callServicePostLaw(_urlGetLawM0TypeOfRegistration, data_m0_typeofregistration_detail);
        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_m0_country_detail));

        setDdlData(ddlName, data_m0_typeofregistration_detail.tcm_m0_typeofregistration_list, "typeofregistration_en", "typeofregistration_idx");
        ddlName.Items.Insert(0, new ListItem("--- Select Type Of Registrtion ---", "0"));
        ddlName.SelectedValue = _typeofregistration_idx.ToString();

    }

    #endregion set/get bind data

    #region Formview Databind
    protected void FvDetail_DataBound(object sender, EventArgs e)
    {
        if (sender is FormView)
        {
            FormView FvName = (FormView)sender;

            switch (FvName.ID)
            {
                case "FvCreate":
                    if (FvName.CurrentMode == FormViewMode.ReadOnly)
                    {

                    }

                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {


                    }

                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        CheckBoxList chkCountry = (CheckBoxList)FvName.FindControl("chkCountry");
                        DropDownList ddlType = (DropDownList)FvName.FindControl("ddlType");
                        DropDownList ddlSubType = (DropDownList)FvName.FindControl("ddlSubType");
                        DropDownList ddlJobtype = (DropDownList)FvName.FindControl("ddlJobtype");

                        getCountry(chkCountry, 0);
                        getDoctypeDetail(ddlType, 0);
                        getSubDoctypeDetail(ddlSubType, int.Parse(ddlType.SelectedValue));
                        getJobType(ddlJobtype, 0);



                    }
                    break;
                case "fvWaitApproveDocumentDetail":
                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {
                       

                    }
                    break;
                case "fvRegistrationDetail":
                    if (FvName.CurrentMode == FormViewMode.Edit)
                    {
                        
                    }
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {

                        TextBox tbowner_idx = (TextBox)FvName.FindControl("tbowner_idx");
                        DropDownList ddlowner_idx = (DropDownList)FvName.FindControl("ddlowner_idx");

                        TextBox tbincorporated_in_idx = (TextBox)FvName.FindControl("tbincorporated_in_idx");
                        DropDownList ddlincorporated_in_idx = (DropDownList)FvName.FindControl("ddlincorporated_in_idx");
                        DropDownList ddlTrademarkStatus = (DropDownList)FvName.FindControl("ddlTrademarkStatus");
                        DropDownList ddlTrademarkSubStatus = (DropDownList)FvName.FindControl("ddlTrademarkSubStatus");
                        DropDownList ddlTypeOfRegistrtion = (DropDownList)FvName.FindControl("ddlTypeOfRegistrtion");

                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");
                        HiddenField hfACIDX = (HiddenField)FvName.FindControl("hfACIDX");
                        HiddenField hfNOIDX = (HiddenField)FvName.FindControl("hfNOIDX");
                        HiddenField hfSTAIDX = (HiddenField)FvName.FindControl("hfSTAIDX");

                        TextBox tbdocrequest_code_detail = (TextBox)FvName.FindControl("tbdocrequest_code_detail");
                        TextBox tbcreate_date_u0_detail = (TextBox)FvName.FindControl("tbcreate_date_u0_detail");
                        TextBox tbtype_name_en = (TextBox)FvName.FindControl("tbtype_name_en");
                        TextBox tbsubtype_name_en = (TextBox)FvName.FindControl("tbsubtype_name_en");
                        TextBox tbcountry_name_en = (TextBox)FvName.FindControl("tbcountry_name_en");
                        TextBox tbemp_name_en_head = (TextBox)FvName.FindControl("tbemp_name_en_head");
                        TextBox tbemp_name_en_mgemp = (TextBox)FvName.FindControl("tbemp_name_en_mgemp");


                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                              select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();
                        hfACIDX.Value = _linq_detailrecord_sheet[0].acidx.ToString();
                        hfNOIDX.Value = _linq_detailrecord_sheet[0].noidx.ToString();
                        hfSTAIDX.Value = _linq_detailrecord_sheet[0].staidx.ToString();

                        tbdocrequest_code_detail.Text = _linq_detailrecord_sheet[0].docrequest_code.ToString();
                        tbcreate_date_u0_detail.Text = _linq_detailrecord_sheet[0].create_date_u0.ToString();
                        tbtype_name_en.Text = _linq_detailrecord_sheet[0].type_name_en.ToString();
                        tbsubtype_name_en.Text = _linq_detailrecord_sheet[0].subtype_name_en.ToString();
                        tbcountry_name_en.Text = _linq_detailrecord_sheet[0].country_name_en.ToString();
                        tbemp_name_en_head.Text = _linq_detailrecord_sheet[0].emp_name_en_head.ToString();
                        tbemp_name_en_mgemp.Text = _linq_detailrecord_sheet[0].emp_name_en_mgemp.ToString();
                        //bind detail to insert record sheet


                        getOwner(ddlowner_idx, 0);
                        getIncorporated(ddlincorporated_in_idx, 0);
                        getTrademarkStatus(ddlTrademarkStatus, 0);
                        getTrademarkStatus(ddlTrademarkSubStatus, 0);
                        getTypeOfRegistrtion(ddlTypeOfRegistrtion, 0);

                    }
                    break;
                case "fvPublicationDetail":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");
                       

                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();

                       
                    }
                    break;
                case "fvDependentDetail":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");

                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();
                    }
                    break;
                case "fvPrioritiesDetail":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        CleardataSetPrioritiesList();

                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");

                        DropDownList ddlStatus_priority = (DropDownList)FvName.FindControl("ddlStatus_priority");
                        DropDownList ddlCountry_priority = (DropDownList)FvName.FindControl("ddlCountry_priority");

                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();

                        getRecordSheetStatus(ddlStatus_priority, 0);
                        getRecordsheetCountry(ddlCountry_priority, 0);
                    }
                    break;
                case "fvClientDetail":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        CleardataSetClientList();

                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");
                        DropDownList ddlClientStatus = (DropDownList)FvName.FindControl("ddlClientStatus");

                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();

                        getRecordSheetStatus(ddlClientStatus, 0);
                    }
                    break;
                case "fvAgentDetail":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        CleardataSetAgentList();

                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");

                        DropDownList ddlAgentStatus = (DropDownList)FvName.FindControl("ddlAgentStatus");

                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();

                        getRecordSheetStatus(ddlAgentStatus, 0);
                    }
                    break;
                case "fvGoodsDetail":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        CleardataSetGoodsList();

                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");

                        DropDownList ddlLocal_language = (DropDownList)FvName.FindControl("ddlLocal_language");

                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();

                        getLocalLanguage(ddlLocal_language, 0);
                    }
                    break;
                case "fvAdditionalDetail":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");

                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();
                    }
                    break;
                case "fvTademarkProfile":
                    if (FvName.CurrentMode == FormViewMode.Insert)
                    {
                        HiddenField hfU0IDX = (HiddenField)FvName.FindControl("hfU0IDX");
                        HiddenField hfU1IDX = (HiddenField)FvName.FindControl("hfU1IDX");

                        DropDownList ddlLocal_language_trademark = (DropDownList)FvName.FindControl("ddlLocal_language_trademark");
                        DropDownList ddlProfileOwnerLanguage = (DropDownList)FvName.FindControl("ddlProfileOwnerLanguage");
                        DropDownList ddlProfileClientLanguage = (DropDownList)FvName.FindControl("ddlProfileClientLanguage");

                        tcm_u1_document_detail[] _templist_insert_recordsheet = (tcm_u1_document_detail[])ViewState["vs_insertrecordsheet"];

                        var _linq_detailrecord_sheet = (from dt in _templist_insert_recordsheet
                                                        select dt).ToList();

                        //bind detail to insert record sheet

                        hfU0IDX.Value = _linq_detailrecord_sheet[0].u0_doc_idx.ToString();
                        hfU1IDX.Value = _linq_detailrecord_sheet[0].u1_doc_idx.ToString();

                        getLocalLanguage(ddlLocal_language_trademark, 0);
                        getLocalLanguage(ddlProfileOwnerLanguage, 0);
                        getLocalLanguage(ddlProfileClientLanguage, 0);
                    }
                    break;

            }
        }
    }
    #endregion Formview Databind

    #region SelectedIndexChanged
    protected void ddlSelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList ddlName = (DropDownList)sender;

        switch (ddlName.ID)
        {

            case "ddlowner_idx":
                TextBox tbowner_address_en = (TextBox)fvRegistrationDetail.FindControl("tbowner_address_en");

                data_law data_m0_owner_detail = new data_law();
                tcm_m0_owner_detail m0_owner_detail = new tcm_m0_owner_detail();
                data_m0_owner_detail.tcm_m0_owner_list = new tcm_m0_owner_detail[1];

                m0_owner_detail.owner_idx = int.Parse(ddlName.SelectedValue);

                data_m0_owner_detail.tcm_m0_owner_list[0] = m0_owner_detail;
                //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

                //
                data_m0_owner_detail = callServicePostLaw(_urlGetLawM0Owner, data_m0_owner_detail);

                if(int.Parse(ddlName.SelectedValue) != 0 )
                {
                    tbowner_address_en.Text = data_m0_owner_detail.tcm_m0_owner_list[0].owner_address_en.ToString();
                }
                else
                {
                    tbowner_address_en.Text = string.Empty;
                }

                break;

            case "ddlType":

                DropDownList ddlType = (DropDownList)FvCreate.FindControl("ddlType");
                DropDownList ddlSubType = (DropDownList)FvCreate.FindControl("ddlSubType");

                getSubDoctypeDetail(ddlSubType, int.Parse(ddlType.SelectedValue));

                break;
            case "ddlTypeSearch":

                DropDownList ddlTypeSearch = (DropDownList)fvSearchDetail.FindControl("ddlTypeSearch");
                DropDownList ddlSubTypeSearch = (DropDownList)fvSearchDetail.FindControl("ddlSubTypeSearch");

                getSubDoctypeDetail(ddlSubTypeSearch, int.Parse(ddlTypeSearch.SelectedValue));

                break;
            case "ddlMgLawApprove":

                DropDownList ddlMgLawApprove = (DropDownList)fvMgLaw.FindControl("ddlMgLawApprove");
                UpdatePanel _Panel_selectofficer_law = (UpdatePanel)fvMgLaw.FindControl("_Panel_selectofficer_law");
                if (ddlMgLawApprove.SelectedValue == "4") //select officer law to document
                {
                    _Panel_selectofficer_law.Visible = true;
                    getLawOfficer(chkLawOfficer, 0);
                }
                else
                {
                    _Panel_selectofficer_law.Visible = false;
                    chkLawOfficer.ClearSelection();
                }

                break;
            case "ddlLawApprove":

                ////DropDownList ddlLawApprove = (DropDownList)fvLaw.FindControl("ddlLawApprove");
                ////UpdatePanel _Panel_File_memo = (UpdatePanel)fvLaw.FindControl("_Panel_File_memo");
               
                ////if (ddlLawApprove.SelectedValue == "6") //select register and law officer attach file memo
                ////{
                ////    div_lawfilememo.Visible = true;
                ////}
                ////else
                ////{
                ////    div_lawfilememo.Visible = false;
                ////}

                break;

        }
    }
    #endregion SelectedIndexChanged

    #region gridview
    protected void gvPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView gvName = (GridView)sender;
        gvName.PageIndex = e.NewPageIndex;

        switch (gvName.ID)
        {
            case "gvWaitApprove":
                
                getDetailWaitApproveDocument(gvWaitApprove, 0);
                break;
            case "gvDetailList":

                setGridData(gvDetailList, ViewState["Vs_DetailContract"]);
               
                break;
            case "gvRecordSheet":
                setGridData(gvRecordSheet, ViewState["Vs_GvDetailRecordSheet"]);
                break;

        }
    }

    protected void gvRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var GvName = (GridView)sender;

        switch (GvName.ID)
        {
            case "":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {

                }
                break;
            case "gvFileDocViewDetail":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.FindControl("btnDL11");
                    HiddenField hidFile = (HiddenField)e.Row.FindControl("hidFile");
                    string LinkHost = string.Format("http://{0}", Request.Url.Host);

                    //btnDL11.NavigateUrl = LinkHost11_permwait + MapURL(hidFile.Value);    
                    string path = HttpContext.Current.Request.Url.AbsolutePath;
                    string[] result_path = path.Split(new string[] { "/" }, StringSplitOptions.None);

                    if (LinkHost.ToString() == "http://localhost")
                    {
                        //litDebug1.Text = LinkHost11_permwait;
                        btnDL11.NavigateUrl = LinkHost + "/" + result_path[1].ToString() + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }
                    else
                    {
                        btnDL11.NavigateUrl = LinkHost + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }


                }
                break;
            case "gvFileMemoLawOfficer":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.FindControl("btnDL11");
                    HiddenField hidFile = (HiddenField)e.Row.FindControl("hidFile");
                    string LinkHost = string.Format("http://{0}", Request.Url.Host);

                    //btnDL11.NavigateUrl = LinkHost11_permwait + MapURL(hidFile.Value);    
                    string path = HttpContext.Current.Request.Url.AbsolutePath;
                    string[] result_path = path.Split(new string[] { "/" }, StringSplitOptions.None);

                    if (LinkHost.ToString() == "http://localhost")
                    {
                        //litDebug1.Text = LinkHost11_permwait;
                        btnDL11.NavigateUrl = LinkHost + "/" + result_path[1].ToString() + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }
                    else
                    {
                        btnDL11.NavigateUrl = LinkHost + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }


                }
                break;
            case "gvFileMemoUser":
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink btnDL11 = (HyperLink)e.Row.FindControl("btnDL11");
                    HiddenField hidFile = (HiddenField)e.Row.FindControl("hidFile");
                    string LinkHost = string.Format("http://{0}", Request.Url.Host);

                    //btnDL11.NavigateUrl = LinkHost11_permwait + MapURL(hidFile.Value);    
                    string path = HttpContext.Current.Request.Url.AbsolutePath;
                    string[] result_path = path.Split(new string[] { "/" }, StringSplitOptions.None);

                    if (LinkHost.ToString() == "http://localhost")
                    {
                        //litDebug1.Text = LinkHost11_permwait;
                        btnDL11.NavigateUrl = LinkHost + "/" + result_path[1].ToString() + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }
                    else
                    {
                        btnDL11.NavigateUrl = LinkHost + MapURL(hidFile.Value);//path + MapURL(hidFile11_permwait.Value);
                    }
                }
                break;
            case "gvRecordSheet":
                if (e.Row.RowType == DataControlRowType.DataRow && e.Row.RowState == DataControlRowState.Normal || e.Row.RowState == DataControlRowState.Alternate)
                {
                    Label lblnoidx_wait = (Label)e.Row.FindControl("lblnoidx_wait");
                    LinkButton btnEditRecordSheet = (LinkButton)e.Row.FindControl("btnEditRecordSheet");
                    LinkButton btnSaveRecordSheet = (LinkButton)e.Row.FindControl("btnSaveRecordSheet");
                    LinkButton btnViewRecordSheet = (LinkButton)e.Row.FindControl("btnViewRecordSheet");

                    btnEditRecordSheet.Visible = false;
                    btnSaveRecordSheet.Visible = false;
                    btnViewRecordSheet.Visible = false;

                    switch (int.Parse(lblnoidx_wait.Text))
                    {
                        case 9:
                            btnSaveRecordSheet.Visible = true;
                            
                            break;
                    }
                }
                break;

        }

    }

    protected void onRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (sender is GridView)
        {
            string cmdName = e.CommandName;
            switch (cmdName)
            {
                case "cmdRemovePriorities":
                    GridView gvPrioritiesList = (GridView)fvPrioritiesDetail.FindControl("gvPrioritiesList");
                    GridViewRow rowSelect = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex = rowSelect.RowIndex;
                    DataSet dsContacts = (DataSet)ViewState["vsPrioritiesList"];
                    dsContacts.Tables["dsPrioritiesListTable"].Rows[rowIndex].Delete();
                    dsContacts.AcceptChanges();
                    setGridData(gvPrioritiesList, dsContacts.Tables["dsPrioritiesListTable"]);
                    if (dsContacts.Tables["dsPrioritiesListTable"].Rows.Count < 1)
                    {
                        gvPrioritiesList.Visible = false;
                    }
                    break;
                case "cmdRemoveClient":
                    GridView gvClientList = (GridView)fvClientDetail.FindControl("gvClientList");
                    GridViewRow rowselect_client= (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_client = rowselect_client.RowIndex;
                    DataSet dsContacts_client = (DataSet)ViewState["vsClientList"];
                    dsContacts_client.Tables["dsClientListTable"].Rows[rowIndex_client].Delete();
                    dsContacts_client.AcceptChanges();
                    setGridData(gvClientList, dsContacts_client.Tables["dsClientListTable"]);
                    if (dsContacts_client.Tables["dsClientListTable"].Rows.Count < 1)
                    {
                        gvClientList.Visible = false;
                    }
                    break;
                case "cmdRemoveAgent":
                    GridView gvAgentList = (GridView)fvAgentDetail.FindControl("gvAgentList");
                    GridViewRow rowselect_Agent = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_Agent = rowselect_Agent.RowIndex;
                    DataSet dsContacts_Agent = (DataSet)ViewState["vsAgentList"];
                    dsContacts_Agent.Tables["dsAgentListTable"].Rows[rowIndex_Agent].Delete();
                    dsContacts_Agent.AcceptChanges();
                    setGridData(gvAgentList, dsContacts_Agent.Tables["dsAgentListTable"]);
                    if (dsContacts_Agent.Tables["dsAgentListTable"].Rows.Count < 1)
                    {
                        gvAgentList.Visible = false;
                    }
                    break;
                case "cmdRemoveGoods":
                    GridView gvGoodsList = (GridView)fvGoodsDetail.FindControl("gvGoodsList");
                    GridViewRow rowselect_Goods = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    int rowIndex_Goods = rowselect_Goods.RowIndex;
                    DataSet dsContacts_Goods = (DataSet)ViewState["vsGoodsList"];
                    dsContacts_Goods.Tables["dsGoodsListTable"].Rows[rowIndex_Goods].Delete();
                    dsContacts_Goods.AcceptChanges();
                    setGridData(gvGoodsList, dsContacts_Goods.Tables["dsGoodsListTable"]);
                    if (dsContacts_Goods.Tables["dsGoodsListTable"].Rows.Count < 1)
                    {
                        gvGoodsList.Visible = false;
                    }
                    break;

            }
        }
    }


    #endregion gridview

    #region event command
    protected void navCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();

        setActiveTab(cmdArg, 0, 0, 0, 0, 0, 0, 0, 0);

    }

    protected void ClickPrint(object sender, EventArgs e)
    {
        //  litDebug.Text = "lals";
    }

    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName.ToString();
        string cmdArg = e.CommandArgument.ToString();
        //linkBtnTrigger(lbDocSaveImport);

        switch (cmdName)
        {

            case "cmdSave":

                DropDownList ddlType = (DropDownList)FvCreate.FindControl("ddlType");
                DropDownList ddlSubType = (DropDownList)FvCreate.FindControl("ddlSubType");
                DropDownList ddlJobtype = (DropDownList)FvCreate.FindControl("ddlJobtype");
                TextBox txt_topic_document = (TextBox)FvCreate.FindControl("txt_topic_document");
                TextBox txt_detail_document = (TextBox)FvCreate.FindControl("txt_detail_document");
                CheckBoxList chkCountry = (CheckBoxList)FvCreate.FindControl("chkCountry");

                FileUpload UploadFile = (FileUpload)FvCreate.FindControl("UploadFile");

                data_law data_u0doc_insert = new data_law();

                //insert u0 document
                tcm_u0_document_detail u0doc_insert = new tcm_u0_document_detail();
                data_u0doc_insert.tcm_u0_document_list = new tcm_u0_document_detail[1];

                u0doc_insert.cemp_idx = _emp_idx;
                u0doc_insert.type_idx = int.Parse(ddlType.SelectedValue);
                u0doc_insert.subtype_idx = int.Parse(ddlSubType.SelectedValue);
                u0doc_insert.jobtype_idx = int.Parse(ddlJobtype.SelectedValue);
                u0doc_insert.topic_name = txt_topic_document.Text;
                u0doc_insert.doc_detail = txt_detail_document.Text;
                u0doc_insert.acidx = 1;
                u0doc_insert.noidx = 1;
                u0doc_insert.decision = 0;

                data_u0doc_insert.tcm_u0_document_list[0] = u0doc_insert;
                //insert u0 document

                //insert u1 document
                var _u1_doc_insert = new tcm_u1_document_detail[chkCountry.Items.Count];
                int sumcheck = 0;
                int count = 0;

                List<String> Chk_list = new List<string>();
                foreach (ListItem chkcountry_list in chkCountry.Items)
                {
                    if (chkcountry_list.Selected)
                    {
                        _u1_doc_insert[count] = new tcm_u1_document_detail();
                        _u1_doc_insert[count].country_idx = int.Parse(chkcountry_list.Value);

                        sumcheck = sumcheck + 1;

                        count++;
                    }
                }
                //insert u1 document

                //check country in document
                if(sumcheck == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", string.Format("alert('--- Please Selected Country ---');"), true);
                    break;
                }
                else
                {
                    data_u0doc_insert.tcm_u0_document_list[0] = u0doc_insert;
                    data_u0doc_insert.tcm_u1_document_list = _u1_doc_insert;

                    if (UploadFile.HasFile)
                    {
                        //litDebug.Text = "1";
                        //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0doc_insert));
                        data_u0doc_insert = callServicePostLaw(_urlSetLawCreateDocument, data_u0doc_insert);


                        string filepath_usercreate = Server.MapPath(_path_file_law_usercreate);
                        HttpFileCollection uploadedFiles_usercreate = Request.Files;

                        string filePath2 = "doc" + "-" + data_u0doc_insert.tcm_u0_document_list[0].docrequest_code.ToString();
                        string filePath1 = Server.MapPath(_path_file_law_usercreate + filePath2);

                        for (int i = 0; i < uploadedFiles_usercreate.Count; i++)
                        {
                            HttpPostedFile userpost_createfile = uploadedFiles_usercreate[i];

                            try
                            {
                                if (userpost_createfile.ContentLength > 0)
                                {

                                    //litDebug.Text += "MA620001";


                                    string _filepathExtension = Path.GetExtension(userpost_createfile.FileName);

                                    ////litDebug1.Text += filepath_usercreate.ToString() + filePath2.ToString();

                                    if (!Directory.Exists(filePath1))
                                    {
                                        Directory.CreateDirectory(filePath1);
                                    }

                                    userpost_createfile.SaveAs(filepath_usercreate + filePath2 + "\\" + data_u0doc_insert.tcm_u0_document_list[0].docrequest_code.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());

                                }
                            }
                            catch (Exception Ex)
                            {
                                //litDebug.Text += "Error: <br>" + Ex.Message;
                            }
                        }

                        setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0, 0);
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Please Attach File ---');", true);
                        break;
                    }

                }

                break;
            case "cmdSaveEditDocument":

                HiddenField hfU0IDX_update = (HiddenField)fvWaitApproveDocumentDetail.FindControl("hfU0IDX");
                HiddenField hfACIDX_update = (HiddenField)fvWaitApproveDocumentDetail.FindControl("hfACIDX");
                HiddenField hfNOIDX_update = (HiddenField)fvWaitApproveDocumentDetail.FindControl("hfNOIDX");
                HiddenField hfSTAIDX_update = (HiddenField)fvWaitApproveDocumentDetail.FindControl("hfSTAIDX");

                Label lbldecision_idx_update = (Label)fvWaitApproveDocumentDetail.FindControl("lbldecision_idx");
                TextBox tbdocrequest_code_update = (TextBox)fvWaitApproveDocumentDetail.FindControl("tbdocrequest_code_viewdetail");
                TextBox txt_topic_document_update = (TextBox)fvWaitApproveDocumentDetail.FindControl("txt_topic_document");
                TextBox txt_detail_document_update = (TextBox)fvWaitApproveDocumentDetail.FindControl("txt_detail_document");
                UpdatePanel Panel_FvEditDocument = (UpdatePanel)fvWaitApproveDocumentDetail.FindControl("Panel_FvEditDocument");
                FileUpload UploadFileEditDocument = (FileUpload)Panel_FvEditDocument.FindControl("UploadFileEditDocument");
                

                data_law data_u0doc_update = new data_law();

                //insert u0 document
                tcm_u0_document_detail u0doc_update = new tcm_u0_document_detail();
                data_u0doc_update.tcm_u0_document_list = new tcm_u0_document_detail[1];

                u0doc_update.cemp_idx = _emp_idx;
                u0doc_update.u0_doc_idx = int.Parse(hfU0IDX_update.Value);
                u0doc_update.acidx = int.Parse(hfACIDX_update.Value);
                u0doc_update.noidx = int.Parse(hfNOIDX_update.Value);
                u0doc_update.decision = int.Parse(lbldecision_idx_update.Text);
                u0doc_update.comment = "-";
                u0doc_update.topic_name = txt_topic_document_update.Text;
                u0doc_update.doc_detail = txt_detail_document_update.Text;

                data_u0doc_update.tcm_u0_document_list[0] = u0doc_update;
                //insert u0 document

                //litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u0doc_update));
                data_u0doc_update = callServicePostLaw(_urlSetLawUpdateDocument, data_u0doc_update);

                if (UploadFileEditDocument.HasFile)
                {
                    //litDebug.Text = "1";

                    string filepath_usermemo = Server.MapPath(_path_file_law_usercreate);
                    HttpFileCollection uploadedFiles_usermemo = Request.Files;
                    string filePath2 = "doc" + "-" + tbdocrequest_code_update.Text;
                    string filePath1 = Server.MapPath(_path_file_law_usercreate + filePath2);

                    //check file in directory in edit file memo
                    if (Directory.Exists(filePath1))
                    {
                        string[] filePaths = Directory.GetFiles(filePath1);
                        foreach (string filePath in filePaths)
                        File.Delete(filePath);
                    }


                    for (int i = 0; i < uploadedFiles_usermemo.Count; i++)
                    {
                        HttpPostedFile usermemopost_createfile = uploadedFiles_usermemo[i];

                        try
                        {
                            if (usermemopost_createfile.ContentLength > 0)
                            {
                                //litDebug.Text = "1";
                                //litDebug.Text += "MA620001";

                                string _filepathExtension = Path.GetExtension(usermemopost_createfile.FileName);
                                if (!Directory.Exists(filePath1))
                                {
                                    Directory.CreateDirectory(filePath1);
                                    //litDebug.Text += "create file" + (i + 1) + "|" + "<br>";
                                }
                                else
                                {

                                }
                                ////litDebug.Text += "File Content Type: " + userpost_createfile.ContentType + "<br>";// data_insert_otmonth.ovt_u0_document_list[0].u0_doc_idx.ToString()
                                ////litDebug.Text += "File Size: " + userpost_createfile.ContentLength + "kb<br>";
                                ////litDebug.Text += "File Name: " + userpost_createfile.FileName + "<br>";
                                ////litDebug.Text += (filepath_usercreate + filePath2 + "\\" + "MA620001" + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());

                                usermemopost_createfile.SaveAs(filepath_usermemo + filePath2 + "\\" + tbdocrequest_code_update.Text + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                //////litDebug.Text += (filepath + filePath12 + "\\" + "1209" + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                //////userpost_createfile.SaveAs(filePath1 + "\\" + filePath2 + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                //userpost_createfile.SaveAs(filepath_usercreate + filePath2 + "\\" + filePath2.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                //////litDebug.Text += "Location where saved: " + filepath + "\\" + Path.GetFileName(userPostedFile.FileName) + "<p>";


                            }
                        }
                        catch (Exception Ex)
                        {
                            //litDebug.Text += "Error: <br>" + Ex.Message;
                        }
                    }

                }
                else
                {
                    //litDebug.Text = "2";
                }

                getCountWaitApproveDocument();
                setOntop.Focus();

                break;

            case "cmdViewDetail":
                string[] _viewdetail = new string[4];
                _viewdetail = e.CommandArgument.ToString().Split(';');
                int _u0_doc_idx_view = int.Parse(_viewdetail[0]);
                int _noidx_view = int.Parse(_viewdetail[1]);
                int _acidx_view = int.Parse(_viewdetail[2]);
                int _staidx_view = int.Parse(_viewdetail[3]);
                int _cemp_idx_view = int.Parse(_viewdetail[4]);
 
                setActiveTab("docDetail", _u0_doc_idx_view, _noidx_view, _staidx_view, _acidx_view, 0, _cemp_idx_view, 0, 0);
                break;
            case "cmdBackDocument":
                setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0, 0);
                break;
            case "cmdBackDocWaitApprove":
                setActiveTab("docWaitApprove", 0, 0, 0, 0, 0, 0, 0, 0);
                break;
            case "cmdViewWaitDetail":

                linkBtnTrigger(btnViewWaitDetail);
                string[] _waitapprove = new string[4];
                _waitapprove = e.CommandArgument.ToString().Split(';');
                int _u0_doc_idx_waitapprove = int.Parse(_waitapprove[0]);
                int _noidx_waitapprove = int.Parse(_waitapprove[1]);
                int _acidx_waitapprove = int.Parse(_waitapprove[2]);
                int _staidx_waitapprove = int.Parse(_waitapprove[3]);
                int _cemp_idx_waitapprove = int.Parse(_waitapprove[4]);

                setActiveTab("docWaitApprove", _u0_doc_idx_waitapprove, _noidx_waitapprove, _staidx_waitapprove, _acidx_waitapprove, 0, _cemp_idx_waitapprove, 0 , 0);
                break;
            case "cmdDocSaveApprove":
                HiddenField hfU0IDX = (HiddenField)fvWaitApproveDocumentDetail.FindControl("hfU0IDX");
                HiddenField hfACIDX = (HiddenField)fvWaitApproveDocumentDetail.FindControl("hfACIDX");
                HiddenField hfNOIDX = (HiddenField)fvWaitApproveDocumentDetail.FindControl("hfNOIDX");
                HiddenField hfSTAIDX = (HiddenField)fvWaitApproveDocumentDetail.FindControl("hfSTAIDX");

                switch (hfNOIDX.Value)
                {
                    case "2": //director user approve
                        DropDownList ddlDirectorUserApprove = (DropDownList)fvDirectorUser.FindControl("ddlDirectorUserApprove");
                        TextBox txt_comment_DirectorUserApprove = (TextBox)fvDirectorUser.FindControl("txt_comment_DirectorUserApprove");

                        data_law data_doc_approve = new data_law();

                        //insert u0 document
                        tcm_u0_document_detail u0doc_approve = new tcm_u0_document_detail();
                        data_doc_approve.tcm_u0_document_list = new tcm_u0_document_detail[1];

                        u0doc_approve.cemp_idx = _emp_idx;
                        u0doc_approve.emphead_idx = _emp_idx;
                        u0doc_approve.u0_doc_idx = int.Parse(hfU0IDX.Value);
                        u0doc_approve.acidx = int.Parse(hfACIDX.Value);
                        u0doc_approve.noidx = int.Parse(hfNOIDX.Value);
                        u0doc_approve.decision = int.Parse(ddlDirectorUserApprove.SelectedValue);
                        u0doc_approve.comment = txt_comment_DirectorUserApprove.Text;

                        data_doc_approve.tcm_u0_document_list[0] = u0doc_approve;

                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_doc_approve));
                        data_doc_approve = callServicePostLaw(_urlSetLawApproveDocument, data_doc_approve);
                        //setActiveTab("docWaitApprove", 0, 0, 0, 0, 0, 0, 0);


                        break;
                    case "3": //mg law receive document and select personal

                        DropDownList ddlMgLawApprove = (DropDownList)fvMgLaw.FindControl("ddlMgLawApprove");
                        TextBox txt_comment_MgLawApprove = (TextBox)fvMgLaw.FindControl("txt_comment_MgLawApprove");
                        CheckBoxList chkLawOfficer = (CheckBoxList)fvMgLaw.FindControl("chkLawOfficer");

                        data_law data_doc_mglawapprove = new data_law();

                        //insert u0 document
                        tcm_u0_document_detail u0doc_mglawapprove = new tcm_u0_document_detail();
                        data_doc_mglawapprove.tcm_u0_document_list = new tcm_u0_document_detail[1];

                        u0doc_mglawapprove.cemp_idx = _emp_idx;
                        u0doc_mglawapprove.empmg_idx = _emp_idx;
                        u0doc_mglawapprove.u0_doc_idx = int.Parse(hfU0IDX.Value);
                        u0doc_mglawapprove.acidx = int.Parse(hfACIDX.Value);
                        u0doc_mglawapprove.noidx = int.Parse(hfNOIDX.Value);
                        u0doc_mglawapprove.decision = int.Parse(ddlMgLawApprove.SelectedValue);
                        u0doc_mglawapprove.comment = txt_comment_MgLawApprove.Text;

                        //insert u3 document
                        var _u3_doc_insert = new tcm_u3_document_detail[chkLawOfficer.Items.Count];
                        int sumcheck_u3 = 0;
                        int count_u3 = 0;

                        List<String> Chk_u3list = new List<string>();
                        foreach (ListItem chklaw_list in chkLawOfficer.Items)
                        {
                            if (chklaw_list.Selected)
                            {
                                _u3_doc_insert[count_u3] = new tcm_u3_document_detail();
                                _u3_doc_insert[count_u3].cemp_idx = _emp_idx;
                                _u3_doc_insert[count_u3].emp_idx = int.Parse(chklaw_list.Value);

                                sumcheck_u3 = sumcheck_u3 + 1;

                                count_u3++;
                            }
                        }
                        //insert u3 document
                        data_doc_mglawapprove.tcm_u0_document_list[0] = u0doc_mglawapprove;
                        data_doc_mglawapprove.tcm_u3_document_list = _u3_doc_insert;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_doc_mglawapprove));
                        data_doc_mglawapprove = callServicePostLaw(_urlSetLawApproveDocument, data_doc_mglawapprove);
                        //setActiveTab("docWaitApprove", 0, 0, 0, 0, 0, 0, 0);

                        break;
                    case "4": //law officer approve register and memo

                        DropDownList ddlLawApprove = (DropDownList)fvLaw.FindControl("ddlLawApprove");
                        TextBox txt_comment_LawApprove = (TextBox)fvLaw.FindControl("txt_comment_LawApprove");
                        
                        UpdatePanel _Panel_File_memo = (UpdatePanel)fvLaw.FindControl("_Panel_File_memo");
                        FileUpload UploadFileMemo = (FileUpload)_Panel_File_memo.FindControl("UploadFileMemo");

                        TextBox tbdocrequest_code_viewdetail = (TextBox)fvWaitApproveDocumentDetail.FindControl("tbdocrequest_code_viewdetail");

                        data_law data_lawofficer_approve = new data_law();

                        //insert u0 document
                        tcm_u0_document_detail u0doc_lawofficer_approve = new tcm_u0_document_detail();
                        data_lawofficer_approve.tcm_u0_document_list = new tcm_u0_document_detail[1];

                        u0doc_lawofficer_approve.cemp_idx = _emp_idx;
                        u0doc_lawofficer_approve.u0_doc_idx = int.Parse(hfU0IDX.Value);
                        u0doc_lawofficer_approve.acidx = int.Parse(hfACIDX.Value);
                        u0doc_lawofficer_approve.noidx = int.Parse(hfNOIDX.Value);
                        u0doc_lawofficer_approve.decision = int.Parse(ddlLawApprove.SelectedValue);
                        u0doc_lawofficer_approve.comment = txt_comment_LawApprove.Text;

                        data_lawofficer_approve.tcm_u0_document_list[0] = u0doc_lawofficer_approve;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_lawofficer_approve));
                        data_doc_mglawapprove = callServicePostLaw(_urlSetLawApproveDocument, data_lawofficer_approve);

                        if (UploadFileMemo.HasFile)
                        {
                            //litDebug.Text = "1";
                            string filepath_lawmemo = Server.MapPath(_path_file_law_lawmemo);
                            HttpFileCollection uploadedFiles_usercreate = Request.Files;

                            for (int i = 0; i < uploadedFiles_usercreate.Count; i++)
                            {
                                HttpPostedFile userpost_createfile = uploadedFiles_usercreate[i];

                                try
                                {
                                    if (userpost_createfile.ContentLength > 0)
                                    {

                                        //litDebug.Text += "MA620001";
                                        string filePath2 = "lawmemo" + "-" + tbdocrequest_code_viewdetail.Text;
                                        string filePath1 = Server.MapPath(_path_file_law_lawmemo + filePath2);

                                        string _filepathExtension = Path.GetExtension(userpost_createfile.FileName);

                                        ////litDebug1.Text += filepath_usercreate.ToString() + filePath2.ToString();

                                        if (!Directory.Exists(filePath1))
                                        {
                                            Directory.CreateDirectory(filePath1);
                                        }

                                        ////litDebug.Text += "<u>File #" + (i + 1) + "</u><br>";
                                        ////litDebug.Text += "File Content Type: " + userpost_createfile.ContentType + "<br>";// data_insert_otmonth.ovt_u0_document_list[0].u0_doc_idx.ToString()
                                        ////litDebug.Text += "File Size: " + userpost_createfile.ContentLength + "kb<br>";
                                        ////litDebug.Text += "File Name: " + userpost_createfile.FileName + "<br>";
                                        ////litDebug.Text += (filepath_usercreate + filePath2 + "\\" + "MA620001" + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());

                                        userpost_createfile.SaveAs(filepath_lawmemo + filePath2 + "\\" + tbdocrequest_code_viewdetail.Text + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());

                                        //////litDebug.Text += (filepath + filePath12 + "\\" + "1209" + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                        //////userpost_createfile.SaveAs(filePath1 + "\\" + filePath2 + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                        //userpost_createfile.SaveAs(filepath_usercreate + filePath2 + "\\" + filePath2.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                        //////litDebug.Text += "Location where saved: " + filepath + "\\" + Path.GetFileName(userPostedFile.FileName) + "<p>";


                                    }
                                }
                                catch (Exception Ex)
                                {
                                    //litDebug.Text += "Error: <br>" + Ex.Message;
                                }
                            }

                            ////setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0);
                        }
                      
                        
                        ////else
                        ////{
                        ////    litDebug.Text = "2";
                        ////    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Please Attach File ---');", true);
                        ////    break;
                        ////}

                        ////getCountWaitApproveDocument();
                        //setActiveTab("docWaitApprove", 0, 0, 0, 0, 0, 0, 0);

                        break;
                    case "5": //user attach file memo 

                        Label lbldecision_idx = (Label)fvUserCreateFileMemo.FindControl("lbldecision_idx");
                        TextBox txt_comment_UserCreateFileMemo = (TextBox)fvUserCreateFileMemo.FindControl("txt_comment_UserCreateFileMemo");

                        UpdatePanel Panel_UserCreateFileMemo = (UpdatePanel)fvUserCreateFileMemo.FindControl("Panel_UserCreateFileMemo");
                        FileUpload UploadUserCreateFileMemo = (FileUpload)Panel_UserCreateFileMemo.FindControl("UploadUserCreateFileMemo");
                        TextBox tbdocrequest_code_waitapprove = (TextBox)fvWaitApproveDocumentDetail.FindControl("tbdocrequest_code_viewdetail");

                        data_law data_usercreate_filememo = new data_law();

                        //insert u0 document
                        tcm_u0_document_detail u0doc_usercreate_filememo = new tcm_u0_document_detail();
                        data_usercreate_filememo.tcm_u0_document_list = new tcm_u0_document_detail[1];

                        u0doc_usercreate_filememo.cemp_idx = _emp_idx;
                        u0doc_usercreate_filememo.u0_doc_idx = int.Parse(hfU0IDX.Value);
                        u0doc_usercreate_filememo.acidx = int.Parse(hfACIDX.Value);
                        u0doc_usercreate_filememo.noidx = int.Parse(hfNOIDX.Value);
                        u0doc_usercreate_filememo.decision = int.Parse(lbldecision_idx.Text);
                        u0doc_usercreate_filememo.comment = txt_comment_UserCreateFileMemo.Text;

                        data_usercreate_filememo.tcm_u0_document_list[0] = u0doc_usercreate_filememo;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_usercreate_filememo));

                        if (UploadUserCreateFileMemo.HasFile)
                        {
                            //litDebug.Text = "1";
                            data_usercreate_filememo = callServicePostLaw(_urlSetLawApproveDocument, data_usercreate_filememo);
                            string filepath_usermemo = Server.MapPath(_path_file_law_usermemo);
                            HttpFileCollection uploadedFiles_usermemo = Request.Files;
                            string filePath2 = "usermemo" + "-" + tbdocrequest_code_waitapprove.Text;
                            string filePath1 = Server.MapPath(_path_file_law_usermemo + filePath2);

                            //check file in directory in edit file memo
                            if (Directory.Exists(filePath1))
                            {
                                string[] filePaths = Directory.GetFiles(filePath1);
                                foreach (string filePath in filePaths)
                                    File.Delete(filePath);
                            }


                            for (int i = 0; i < uploadedFiles_usermemo.Count; i++)
                            {
                                HttpPostedFile usermemopost_createfile = uploadedFiles_usermemo[i];

                                try
                                {
                                    if (usermemopost_createfile.ContentLength > 0)
                                    {
                                        //litDebug.Text = "1";
                                        //litDebug.Text += "MA620001";

                                        string _filepathExtension = Path.GetExtension(usermemopost_createfile.FileName);
                                        if (!Directory.Exists(filePath1))
                                        {
                                            Directory.CreateDirectory(filePath1);
                                            //litDebug.Text += "create file" + (i + 1) + "|" + "<br>";
                                        }
                                        else
                                        {

                                        }
                                        ////litDebug.Text += "File Content Type: " + userpost_createfile.ContentType + "<br>";// data_insert_otmonth.ovt_u0_document_list[0].u0_doc_idx.ToString()
                                        ////litDebug.Text += "File Size: " + userpost_createfile.ContentLength + "kb<br>";
                                        ////litDebug.Text += "File Name: " + userpost_createfile.FileName + "<br>";
                                        ////litDebug.Text += (filepath_usercreate + filePath2 + "\\" + "MA620001" + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());

                                        usermemopost_createfile.SaveAs(filepath_usermemo + filePath2 + "\\" + tbdocrequest_code_waitapprove.Text + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                        //////litDebug.Text += (filepath + filePath12 + "\\" + "1209" + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                        //////userpost_createfile.SaveAs(filePath1 + "\\" + filePath2 + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                        //userpost_createfile.SaveAs(filepath_usercreate + filePath2 + "\\" + filePath2.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                        //////litDebug.Text += "Location where saved: " + filepath + "\\" + Path.GetFileName(userPostedFile.FileName) + "<p>";


                                    }
                                }
                                catch (Exception Ex)
                                {
                                    //litDebug.Text += "Error: <br>" + Ex.Message;
                                }
                            }

                        }
                        else
                        {
                            //litDebug.Text = "2";
                        }

                        //setActiveTab("docWaitApprove", 0, 0, 0, 0, 0, 0, 0);

                        break;
                    case "6": //director user check file memo
                        DropDownList ddlDirectorUserCheckMemo = (DropDownList)fvDirectorUserCheckMemo.FindControl("ddlDirectorUserCheckMemo");
                        TextBox txt_comment_DirectorUserCheckMemo = (TextBox)fvDirectorUserCheckMemo.FindControl("txt_comment_DirectorUserCheckMemo");

                        data_law data_directoruser_checkmemo = new data_law();

                        //insert u0 document
                        tcm_u0_document_detail u0doc_directoruser_checkmemo = new tcm_u0_document_detail();
                        data_directoruser_checkmemo.tcm_u0_document_list = new tcm_u0_document_detail[1];

                        u0doc_directoruser_checkmemo.cemp_idx = _emp_idx;
                        u0doc_directoruser_checkmemo.u0_doc_idx = int.Parse(hfU0IDX.Value);
                        u0doc_directoruser_checkmemo.acidx = int.Parse(hfACIDX.Value);
                        u0doc_directoruser_checkmemo.noidx = int.Parse(hfNOIDX.Value);
                        u0doc_directoruser_checkmemo.decision = int.Parse(ddlDirectorUserCheckMemo.SelectedValue);
                        u0doc_directoruser_checkmemo.comment = txt_comment_DirectorUserCheckMemo.Text;

                        data_directoruser_checkmemo.tcm_u0_document_list[0] = u0doc_directoruser_checkmemo;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_directoruser_checkmemo));
                        data_directoruser_checkmemo = callServicePostLaw(_urlSetLawApproveDocument, data_directoruser_checkmemo);
                        //setActiveTab("docWaitApprove", 0, 0, 0, 0, 0, 0, 0);


                        break;
                    case "7": //law officer check file memo
                        DropDownList ddlLawOfficerCheckMemo = (DropDownList)fvLawOfficerCheckMemo.FindControl("ddlLawOfficerCheckMemo");
                        TextBox txt_comment_LawOfficerCheckMemo = (TextBox)fvLawOfficerCheckMemo.FindControl("txt_comment_LawOfficerCheckMemo");

                        data_law data_lawofficer_checkmemo = new data_law();

                        //insert u0 document
                        tcm_u0_document_detail u0doc_lawofficer_checkmemo = new tcm_u0_document_detail();
                        data_lawofficer_checkmemo.tcm_u0_document_list = new tcm_u0_document_detail[1];

                        u0doc_lawofficer_checkmemo.cemp_idx = _emp_idx;
                        u0doc_lawofficer_checkmemo.u0_doc_idx = int.Parse(hfU0IDX.Value);
                        u0doc_lawofficer_checkmemo.acidx = int.Parse(hfACIDX.Value);
                        u0doc_lawofficer_checkmemo.noidx = int.Parse(hfNOIDX.Value);
                        u0doc_lawofficer_checkmemo.decision = int.Parse(ddlLawOfficerCheckMemo.SelectedValue);
                        u0doc_lawofficer_checkmemo.comment = txt_comment_LawOfficerCheckMemo.Text;

                        data_lawofficer_checkmemo.tcm_u0_document_list[0] = u0doc_lawofficer_checkmemo;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_lawofficer_checkmemo));
                        data_lawofficer_checkmemo = callServicePostLaw(_urlSetLawApproveDocument, data_lawofficer_checkmemo);
                        //setActiveTab("docWaitApprove", 0, 0, 0, 0, 0, 0, 0);


                        break;
                    case "8":  //mg law check memo to user edit memo or register trademarks
                    case "12": //mg law check memo to law officer edit memo or register trademarks

                        DropDownList ddlMgLawApproveMemo = (DropDownList)fvMgLawMemo.FindControl("ddlMgLawApproveMemo");
                        TextBox txt_comment_MgLawApproveMemo = (TextBox)fvMgLawMemo.FindControl("txt_comment_MgLawApproveMemo");

                        data_law data_mglawmemo_approve = new data_law();

                        //insert u0 document
                        tcm_u0_document_detail u0doc_mglawmemo_approve = new tcm_u0_document_detail();
                        data_mglawmemo_approve.tcm_u0_document_list = new tcm_u0_document_detail[1];

                        u0doc_mglawmemo_approve.cemp_idx = _emp_idx;
                        u0doc_mglawmemo_approve.u0_doc_idx = int.Parse(hfU0IDX.Value);
                        u0doc_mglawmemo_approve.acidx = int.Parse(hfACIDX.Value);
                        u0doc_mglawmemo_approve.noidx = int.Parse(hfNOIDX.Value);
                        u0doc_mglawmemo_approve.decision = int.Parse(ddlMgLawApproveMemo.SelectedValue);
                        u0doc_mglawmemo_approve.comment = txt_comment_MgLawApproveMemo.Text;

                        data_mglawmemo_approve.tcm_u0_document_list[0] = u0doc_mglawmemo_approve;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_mglawmemo_approve));
                        data_mglawmemo_approve = callServicePostLaw(_urlSetLawApproveDocument, data_mglawmemo_approve);
                        

                        break;
                    case "13": //law officer edit file memo 

                        Label lbldecision_idx_ = (Label)fvLawOfficerEditFileMemo.FindControl("lbldecision_idx");
                        TextBox txt_comment_LawOfficerEditFileMemo = (TextBox)fvLawOfficerEditFileMemo.FindControl("txt_comment_LawOfficerEditFileMemo");
                        UpdatePanel Panel_LawOfficerEditFileMemo = (UpdatePanel)fvLawOfficerEditFileMemo.FindControl("Panel_LawOfficerEditFileMemo");
                        FileUpload UploadLawOfficerEditFileMemo = (FileUpload)Panel_LawOfficerEditFileMemo.FindControl("UploadLawOfficerEditFileMemo");

                        TextBox tbdocrequest_code_waitapprove_ = (TextBox)fvWaitApproveDocumentDetail.FindControl("tbdocrequest_code_viewdetail");

                        data_law data_lawofficer_editfilememo = new data_law();

                        //insert u0 document
                        tcm_u0_document_detail u0doc_lawofficer_editfilememo = new tcm_u0_document_detail();
                        data_lawofficer_editfilememo.tcm_u0_document_list = new tcm_u0_document_detail[1];

                        u0doc_lawofficer_editfilememo.cemp_idx = _emp_idx;
                        u0doc_lawofficer_editfilememo.u0_doc_idx = int.Parse(hfU0IDX.Value);
                        u0doc_lawofficer_editfilememo.acidx = int.Parse(hfACIDX.Value);
                        u0doc_lawofficer_editfilememo.noidx = int.Parse(hfNOIDX.Value);
                        u0doc_lawofficer_editfilememo.decision = int.Parse(lbldecision_idx_.Text);
                        u0doc_lawofficer_editfilememo.comment = txt_comment_LawOfficerEditFileMemo.Text;

                        data_lawofficer_editfilememo.tcm_u0_document_list[0] = u0doc_lawofficer_editfilememo;
                        //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_lawofficer_approve));


                        if (UploadLawOfficerEditFileMemo.HasFile)
                        {
                            //litDebug.Text = "1";
                            data_lawofficer_editfilememo = callServicePostLaw(_urlSetLawApproveDocument, data_lawofficer_editfilememo);
                            string filepath_lawmemo = Server.MapPath(_path_file_law_lawmemo);
                            HttpFileCollection uploadedFiles_lawmemo = Request.Files;
                            string filePath2 = "lawmemo" + "-" + tbdocrequest_code_waitapprove_.Text;
                            string filePath1 = Server.MapPath(_path_file_law_lawmemo + filePath2);

                            //check file in directory in edit file memo
                            if (Directory.Exists(filePath1))
                            {
                                string[] filePaths = Directory.GetFiles(filePath1);
                                foreach (string filePath in filePaths)
                                    File.Delete(filePath);
                            }

                            for (int i = 0; i < uploadedFiles_lawmemo.Count; i++)
                            {
                                HttpPostedFile lawmemopost_editfile = uploadedFiles_lawmemo[i];

                                try
                                {
                                    if (lawmemopost_editfile.ContentLength > 0)
                                    {
                                        //litDebug.Text = "1";
                                        //litDebug.Text += "MA620001";

                                        string _filepathExtension = Path.GetExtension(lawmemopost_editfile.FileName);
                                        if (!Directory.Exists(filePath1))
                                        {
                                            Directory.CreateDirectory(filePath1);
                                            //litDebug.Text += "create file" + (i + 1) + "|" + "<br>";
                                        }
                                        else
                                        {

                                        }
                                        ////litDebug.Text += "File Content Type: " + userpost_createfile.ContentType + "<br>";// data_insert_otmonth.ovt_u0_document_list[0].u0_doc_idx.ToString()
                                        ////litDebug.Text += "File Size: " + userpost_createfile.ContentLength + "kb<br>";
                                        ////litDebug.Text += "File Name: " + userpost_createfile.FileName + "<br>";
                                        ////litDebug.Text += (filepath_usercreate + filePath2 + "\\" + "MA620001" + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());

                                        lawmemopost_editfile.SaveAs(filepath_lawmemo + filePath2 + "\\" + tbdocrequest_code_waitapprove_.Text + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                        //////litDebug.Text += (filepath + filePath12 + "\\" + "1209" + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                        //////userpost_createfile.SaveAs(filePath1 + "\\" + filePath2 + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                        //userpost_createfile.SaveAs(filepath_usercreate + filePath2 + "\\" + filePath2.ToString() + Path.GetFileName((i).ToString()) + _filepathExtension.ToLower());
                                        //////litDebug.Text += "Location where saved: " + filepath + "\\" + Path.GetFileName(userPostedFile.FileName) + "<p>";


                                    }
                                }
                                catch (Exception Ex)
                                {
                                    //litDebug.Text += "Error: <br>" + Ex.Message;
                                }
                            }

                        }
                        else
                        {
                            //litDebug.Text = "2";
                        }

                        //setActiveTab("docWaitApprove", 0, 0, 0, 0, 0, 0, 0);

                        break;

                }

                //setActiveTab("docWaitApprove", 0, 0, 0, 0, 0, 0, 0);
                getCountWaitApproveDocument();
                setOntop.Focus();
                break;
            case "cmdViewRecordSheet":
                string[] _viewrecordsheet = new string[6];
                _viewrecordsheet = e.CommandArgument.ToString().Split(';');
                int _u0_doc_idx_viewrecord = int.Parse(_viewrecordsheet[0]);
                int _u1_doc_idx_viewrecord = int.Parse(_viewrecordsheet[1]);
                int _noidx_viewrecord = int.Parse(_viewrecordsheet[2]);
                int _acidx_viewrecord = int.Parse(_viewrecordsheet[3]);
                int _staidx_viewrecord = int.Parse(_viewrecordsheet[4]);
                int _cemp_idx_viewrecord = int.Parse(_viewrecordsheet[5]);
                int _type_idx_viewrecord = int.Parse(_viewrecordsheet[6]);

                setActiveTab("docRegistration", _u0_doc_idx_viewrecord, _noidx_viewrecord, _staidx_viewrecord, _acidx_viewrecord, 0, _cemp_idx_viewrecord, _u1_doc_idx_viewrecord, _type_idx_viewrecord);
                break;

            case "cmdEditRecordSheet":
                string[] _editrecordsheet = new string[6];
                _editrecordsheet = e.CommandArgument.ToString().Split(';');
                int _u0_doc_idx_editrecord = int.Parse(_editrecordsheet[0]);
                int _u1_doc_idx_editrecord = int.Parse(_editrecordsheet[1]);
                int _noidx_editrecord = int.Parse(_editrecordsheet[2]);
                int _acidx_editrecord = int.Parse(_editrecordsheet[3]);
                int _staidx_editrecord = int.Parse(_editrecordsheet[4]);
                int _cemp_idx_editrecord = int.Parse(_editrecordsheet[5]);
                int _type_idx_editrecord = int.Parse(_editrecordsheet[6]);

                setActiveTab("docRegistration", _u0_doc_idx_editrecord, _noidx_editrecord, _staidx_editrecord, _acidx_editrecord, 1, _cemp_idx_editrecord, _u1_doc_idx_editrecord, _type_idx_editrecord);
                break;

            case "cmdSaveRecordSheet":
                string[] _saverecordsheet = new string[6];
                _saverecordsheet = e.CommandArgument.ToString().Split(';');
                int _u0_doc_idx_saverecord = int.Parse(_saverecordsheet[0]);
                int _u1_doc_idx_saverecord = int.Parse(_saverecordsheet[1]);
                int _noidx_saverecord = int.Parse(_saverecordsheet[2]);
                int _acidx_saverecord = int.Parse(_saverecordsheet[3]);
                int _staidx_saverecord = int.Parse(_saverecordsheet[4]);
                int _cemp_idx_saverecord = int.Parse(_saverecordsheet[5]);
                int _type_idx_saverecord = int.Parse(_saverecordsheet[6]);

                setActiveTab("docRegistration", _u0_doc_idx_saverecord, _noidx_saverecord, _staidx_saverecord, _acidx_saverecord, 2, _cemp_idx_saverecord, _u1_doc_idx_saverecord, _type_idx_saverecord);
                break;

            case "cmdAddPrioritiesList":
                setPrioritiesList();

                break;
            case "cmdAddClientList":
                setClientList();

                break;
            case "cmdAddAgentList":
                setAgentList();

                break;
            case "cmdAddGoodsList":
                setGoodsList();

                break;
            case "cmdDocCancel":
                setActiveTab("docWaitApprove", 0, 0, 0, 0, 0, 0, 0, 0);
                break;
            case "cmdSaveTrademarkRecordSheet":

                //fvRegistrationDetail
                HiddenField hfU0IDX_insertrecord = (HiddenField)fvRegistrationDetail.FindControl("hfU0IDX");
                HiddenField hfU1IDX_insertrecord = (HiddenField)fvRegistrationDetail.FindControl("hfU1IDX");
                HiddenField hfACIDX_insertrecord = (HiddenField)fvRegistrationDetail.FindControl("hfACIDX");
                HiddenField hfNOIDX_insertrecord = (HiddenField)fvRegistrationDetail.FindControl("hfNOIDX");
                HiddenField hfSTAIDX_insertrecord = (HiddenField)fvRegistrationDetail.FindControl("hfSTAIDX");

                DropDownList ddlowner_idx = (DropDownList)fvRegistrationDetail.FindControl("ddlowner_idx");
                DropDownList ddlincorporated_in_idx = (DropDownList)fvRegistrationDetail.FindControl("ddlincorporated_in_idx");
                TextBox txt_application_no = (TextBox)fvRegistrationDetail.FindControl("txt_application_no");
                TextBox txt_application_date = (TextBox)fvRegistrationDetail.FindControl("txt_application_date");
                TextBox txt_registration_no = (TextBox)fvRegistrationDetail.FindControl("txt_registration_no");
                TextBox txt_registration_date = (TextBox)fvRegistrationDetail.FindControl("txt_registration_date");
                DropDownList ddlTrademarkStatus = (DropDownList)fvRegistrationDetail.FindControl("ddlTrademarkStatus");
                DropDownList ddlTrademarkSubStatus = (DropDownList)fvRegistrationDetail.FindControl("ddlTrademarkSubStatus");
                TextBox tb_File_Reference = (TextBox)fvRegistrationDetail.FindControl("tb_File_Reference");
                TextBox tb_Record_Reference = (TextBox)fvRegistrationDetail.FindControl("tb_Record_Reference");
                TextBox tb_Next_Renewal_Due = (TextBox)fvRegistrationDetail.FindControl("tb_Next_Renewal_Due");
                DropDownList ddlTypeOfRegistrtion = (DropDownList)fvRegistrationDetail.FindControl("ddlTypeOfRegistrtion");
                //fvRegistrationDetail

                //fvPublicationDetail
                TextBox tbPublication_Date = (TextBox)fvPublicationDetail.FindControl("tbPublication_Date");
                TextBox tbGrant_date = (TextBox)fvPublicationDetail.FindControl("tbGrant_date");
                TextBox tbJournal_Volume = (TextBox)fvPublicationDetail.FindControl("tbJournal_Volume");
                TextBox tb_Pub_Date = (TextBox)fvPublicationDetail.FindControl("tb_Pub_Date");
                TextBox tb_Registry_Reference = (TextBox)fvPublicationDetail.FindControl("tb_Registry_Reference");
                TextBox tbFinal_Office_Date = (TextBox)fvPublicationDetail.FindControl("tbFinal_Office_Date");
                TextBox tb_Office_Action_Date = (TextBox)fvPublicationDetail.FindControl("tb_Office_Action_Date");
                TextBox tb_Date_Declaration_Filed = (TextBox)fvPublicationDetail.FindControl("tb_Date_Declaration_Filed");
                //fvPublicationDetail

                //fvDependentDetail
                TextBox tbdependent_registration = (TextBox)fvDependentDetail.FindControl("tbdependent_registration");

                //fvAdditionalDetail
                TextBox tbAdditionalDetail = (TextBox)fvAdditionalDetail.FindControl("tbAdditionalDetail");

                //fvTademarkProfile
                TextBox tbName_trademark = (TextBox)fvTademarkProfile.FindControl("tbName_trademark");
                DropDownList ddlLocal_language_trademark = (DropDownList)fvTademarkProfile.FindControl("ddlLocal_language_trademark");
                TextBox tbProfileOwner = (TextBox)fvTademarkProfile.FindControl("tbProfileOwner");
                DropDownList ddlProfileOwnerLanguage = (DropDownList)fvTademarkProfile.FindControl("ddlProfileOwnerLanguage");
                TextBox tbProfileClient = (TextBox)fvTademarkProfile.FindControl("tbProfileClient");
                DropDownList ddlProfileClientLanguage = (DropDownList)fvTademarkProfile.FindControl("ddlProfileClientLanguage");
                TextBox tbTranslation = (TextBox)fvTademarkProfile.FindControl("tbTranslation");
                TextBox tbTransliteration = (TextBox)fvTademarkProfile.FindControl("tbTransliteration");

                int decision_recordsheet = 16; //save recordsheet

                data_law data_u1insert_recordsheet = new data_law();

                //insert record sheet fvRegistrationDetail
                tcm_u1_document_detail u1insert_recordsheet = new tcm_u1_document_detail();
                data_u1insert_recordsheet.tcm_u1_document_list = new tcm_u1_document_detail[1];

                u1insert_recordsheet.cemp_idx = _emp_idx;
                u1insert_recordsheet.u0_doc_idx = int.Parse(hfU0IDX_insertrecord.Value);
                u1insert_recordsheet.u1_doc_idx = int.Parse(hfU1IDX_insertrecord.Value);
                u1insert_recordsheet.acidx = int.Parse(hfACIDX_insertrecord.Value);
                u1insert_recordsheet.noidx = int.Parse(hfNOIDX_insertrecord.Value);
                u1insert_recordsheet.decision = decision_recordsheet;
                u1insert_recordsheet.comment = "-";
                u1insert_recordsheet.owner_idx = int.Parse(ddlowner_idx.SelectedValue);
                u1insert_recordsheet.incorporated_in_idx = int.Parse(ddlincorporated_in_idx.SelectedValue);
                u1insert_recordsheet.application_no = txt_application_no.Text;
                u1insert_recordsheet.application_date = txt_application_date.Text;
                u1insert_recordsheet.registration_no = txt_registration_no.Text;
                u1insert_recordsheet.registration_date = txt_registration_date.Text;
                u1insert_recordsheet.tradmark_status = int.Parse(ddlTrademarkStatus.SelectedValue);
                u1insert_recordsheet.tradmark_substatus = int.Parse(ddlTrademarkSubStatus.SelectedValue);
                u1insert_recordsheet.file_reference = tb_File_Reference.Text;
                u1insert_recordsheet.recode_referense = tb_Record_Reference.Text;
                u1insert_recordsheet.next_renewal_due = tb_Next_Renewal_Due.Text;
                u1insert_recordsheet.type_of_registration_idx = int.Parse(ddlTypeOfRegistrtion.SelectedValue);

                
                //insert u0 document fvRegistrationDetail
                

                //insert record sheet fvPublicationDetail
                tcm_record_publication_detail record_publication_insert = new tcm_record_publication_detail();
                data_u1insert_recordsheet.tcm_record_publication_list = new tcm_record_publication_detail[1];

                record_publication_insert.cemp_idx = _emp_idx;
                record_publication_insert.Publication_Date = tbPublication_Date.Text;
                record_publication_insert.NoticeofAllowance_Date = tbGrant_date.Text;
                record_publication_insert.Journal_Volume = tbJournal_Volume.Text;
                record_publication_insert.PubRegDate_Date = tb_Pub_Date.Text;
                record_publication_insert.Registry_Reference = tb_Registry_Reference.Text;
                record_publication_insert.FinalOfficeAction_Date = tbFinal_Office_Date.Text;
                record_publication_insert.OfficeAction_Date = tb_Office_Action_Date.Text;
                record_publication_insert.Date_DeclarationFiled = tb_Date_Declaration_Filed.Text;

                

                //insert fvDependentDetail
                tcm_record_dependent_detail record_dependent_insert = new tcm_record_dependent_detail();
                data_u1insert_recordsheet.tcm_record_dependent_list = new tcm_record_dependent_detail[1];

                record_dependent_insert.cemp_idx = _emp_idx;
                record_dependent_insert.Dependent_Registration = tbdependent_registration.Text;

                

                //insert fvPrioritiesDetail
                var _datasetPriorities = (DataSet)ViewState["vsPrioritiesList"];
                var _addPriorities = new tcm_record_priorities_detail[_datasetPriorities.Tables[0].Rows.Count];
                int _Priorities = 0;

                foreach (DataRow dtrowPriorities in _datasetPriorities.Tables[0].Rows)
                {
                    _addPriorities[_Priorities] = new tcm_record_priorities_detail();
                    _addPriorities[_Priorities].cemp_idx = _emp_idx;
                    _addPriorities[_Priorities].Type_of_priority = dtrowPriorities["drTypeofpriorityText"].ToString();
                    _addPriorities[_Priorities].Priority_Date = dtrowPriorities["drPriorityDateText"].ToString();
                    _addPriorities[_Priorities].Priority_No = dtrowPriorities["drPriorityNoText"].ToString();
                    _addPriorities[_Priorities].country_idx = int.Parse(dtrowPriorities["drCountrypriorityIdx"].ToString());
                    _addPriorities[_Priorities].status_idx = int.Parse(dtrowPriorities["drStatuspriorityIdx"].ToString());

                    _Priorities++;

                }
                //insert fvPrioritiesDetail

                //insert fvClientDetail
                var _datasetClient = (DataSet)ViewState["vsClientList"];
                var _addClient = new tcm_record_client_detail[_datasetClient.Tables[0].Rows.Count];
                int _Client = 0;

                foreach (DataRow dtrowClient in _datasetClient.Tables[0].Rows)
                {
                    _addClient[_Client] = new tcm_record_client_detail();
                    _addClient[_Client].cemp_idx = _emp_idx;
                    _addClient[_Client].Client_Detail = dtrowClient["drClientDetailText"].ToString();
                    _addClient[_Client].Client_Contact = dtrowClient["drClientContactText"].ToString();
                    _addClient[_Client].Client_Reference = dtrowClient["drClientReferenceText"].ToString();
                    _addClient[_Client].status_idx = int.Parse(dtrowClient["drClientStatusIdx"].ToString());

                    _Client++;

                }
                //insert fvClientDetail

                //insert fvAgentDetail
                var _datasetAgent = (DataSet)ViewState["vsAgentList"];
                var _addAgent = new tcm_record_agent_detail[_datasetAgent.Tables[0].Rows.Count];
                int _Agent = 0;

                foreach (DataRow dtrowagent in _datasetAgent.Tables[0].Rows)
                {
                    _addAgent[_Agent] = new tcm_record_agent_detail();
                    _addAgent[_Agent].cemp_idx = _emp_idx;
                    _addAgent[_Agent].Agent_Detail = dtrowagent["drAgentDetailText"].ToString();
                    _addAgent[_Agent].Agent_Contact = dtrowagent["drAgentContactText"].ToString();
                    _addAgent[_Agent].Agent_Reference = dtrowagent["drAgentReferenceText"].ToString();
                    _addAgent[_Agent].status_idx = int.Parse(dtrowagent["drAgentStatusIdx"].ToString());

                    _Agent++;

                }
                //insert fvAgentDetail

                //insert fvGoodsDetail
                var _datasetGoods = (DataSet)ViewState["vsGoodsList"];
                var _addGoods = new tcm_record_goods_detail[_datasetGoods.Tables[0].Rows.Count];
                int _Goods = 0;

                foreach (DataRow dtrowaGoods in _datasetGoods.Tables[0].Rows)
                {
                    _addGoods[_Goods] = new tcm_record_goods_detail();
                    _addGoods[_Goods].cemp_idx = _emp_idx;
                    _addGoods[_Goods].IntClass = dtrowaGoods["drGoodsIntClassText"].ToString();
                    _addGoods[_Goods].Goods_Detail = dtrowaGoods["drGoodsDetailText"].ToString();
                    _addGoods[_Goods].language_idx = int.Parse(dtrowaGoods["drLocallanguageIdx"].ToString());
                    

                    _Goods++;

                }
                //insert fvGoodsDetail

                //insert fvAdditionalDetail
                tcm_record_additional_detail record_additional_insert = new tcm_record_additional_detail();
                data_u1insert_recordsheet.tcm_record_additional_list = new tcm_record_additional_detail[1];

                record_additional_insert.cemp_idx = _emp_idx;
                record_additional_insert.Additional_Details = tbAdditionalDetail.Text;

                
                //insert fvAdditionalDetail

                //insert fvTademarkProfile
                tcm_record_trademarkprofile_detail record_trademarkprofile_insert = new tcm_record_trademarkprofile_detail();
                data_u1insert_recordsheet.tcm_record_trademarkprofile_list = new tcm_record_trademarkprofile_detail[1];

                record_trademarkprofile_insert.cemp_idx = _emp_idx;
                record_trademarkprofile_insert.Name = tbName_trademark.Text;
                record_trademarkprofile_insert.doc_language_idx = int.Parse(ddlLocal_language_trademark.SelectedValue);
                record_trademarkprofile_insert.Profile_Owner = tbProfileOwner.Text;
                record_trademarkprofile_insert.owner_language_idx = int.Parse(ddlProfileOwnerLanguage.SelectedValue);
                record_trademarkprofile_insert.Profile_Client = tbProfileClient.Text;
                record_trademarkprofile_insert.client_language_idx = int.Parse(ddlProfileClientLanguage.SelectedValue);
                record_trademarkprofile_insert.Translation = tbTranslation.Text;
                record_trademarkprofile_insert.Transliteration = tbTransliteration.Text;

                //insert fvTademarkProfile


                //insert actions
                var _record_actions_detail = new tcm_record_actions_detail[gvActions.Rows.Count];
                int sumcheck_record = 0;
                int count_record = 0;

                foreach (GridViewRow gvrow in gvActions.Rows)
                {

                    data_u1insert_recordsheet.tcm_record_actions_list = new tcm_record_actions_detail[1];

                    TextBox tbActionsDetail = (TextBox)gvrow.FindControl("tbActionsDetail");
                    TextBox tbOwnerDetail = (TextBox)gvrow.FindControl("tbOwnerDetail");
                    Label lblaction_idx = (Label)gvrow.FindControl("lblaction_idx");

                    _record_actions_detail[count_record] = new tcm_record_actions_detail();
                    _record_actions_detail[count_record].cemp_idx = _emp_idx;
                    _record_actions_detail[count_record].Actions_Detail = tbActionsDetail.Text;
                    _record_actions_detail[count_record].Owner_Detail = tbOwnerDetail.Text;
                    _record_actions_detail[count_record].action_idx = int.Parse(lblaction_idx.Text);

                    sumcheck_record = sumcheck_record + 1;
                    count_record++;

                }
                //insert actions


                data_u1insert_recordsheet.tcm_u1_document_list[0] = u1insert_recordsheet;
                data_u1insert_recordsheet.tcm_record_publication_list[0] = record_publication_insert;
                data_u1insert_recordsheet.tcm_record_dependent_list[0] = record_dependent_insert;
                data_u1insert_recordsheet.tcm_record_priorities_list = _addPriorities;
                data_u1insert_recordsheet.tcm_record_client_list = _addClient;
                data_u1insert_recordsheet.tcm_record_agent_list = _addAgent;
                data_u1insert_recordsheet.tcm_record_goods_list = _addGoods;
                data_u1insert_recordsheet.tcm_record_additional_list[0] = record_additional_insert;
                data_u1insert_recordsheet.tcm_record_trademarkprofile_list[0] = record_trademarkprofile_insert;
                data_u1insert_recordsheet.tcm_record_actions_list = _record_actions_detail;

                ////litDebug.Text += HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u1insert_recordsheet));
                ////data_u1insert_recordsheet = callServicePostLaw(_urlSetLawUpdateDocument, data_u0doc_update);


                break;
            case "cmdSearchDetail":

                DropDownList ddlTypeSearch = (DropDownList)fvSearchDetail.FindControl("ddlTypeSearch");
                DropDownList ddlSubTypeSearch = (DropDownList)fvSearchDetail.FindControl("ddlSubTypeSearch");
                TextBox txt_documentno_Search = (TextBox)fvSearchDetail.FindControl("txt_documentno_Search");

                ////data_law data_u0_document_searchdetail = new data_law();
                ////tcm_u0_document_detail u0_document_searchdetail = new tcm_u0_document_detail();
                ////data_u0_document_searchdetail.tcm_u0_document_list = new tcm_u0_document_detail[1];

                ////u0_document_searchdetail.type_idx = int.Parse(ddlTypeSearch.SelectedValue);
                ////u0_document_searchdetail.subtype_idx = int.Parse(ddlSubTypeSearch.SelectedValue);
                ////u0_document_searchdetail.cemp_idx = _emp_idx;
                ////u0_document_searchdetail.rdept_idx = int.Parse(ViewState["rdept_permission"].ToString());
                ////u0_document_searchdetail.rsec_idx = int.Parse(ViewState["rsec_permission"].ToString());
                ////u0_document_searchdetail.jobgrade_level = int.Parse(ViewState["joblevel_permission"].ToString());

                ////data_u0_document_searchdetail.tcm_u0_document_list[0] = u0_document_searchdetail;
                //////litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));

                //////
                ////data_u0_document_searchdetail = callServicePostLaw(_urlGetSearchDetailContract, data_u0_document_searchdetail);

                ////ViewState["Vs_DetailContract"] = data_u0_document_searchdetail.tcm_u0_document_list;


                ///
                if (ViewState["Vs_DetailContract_ALL"] != null)
                {
                    tcm_u0_document_detail[] _templist_DocumentDetail = (tcm_u0_document_detail[])ViewState["Vs_DetailContract_ALL"];

                    var _linqDocumentDetail = (from dt in _templist_DocumentDetail

                                               where

                                               (int.Parse(ddlTypeSearch.SelectedValue) == 0 || dt.type_idx == int.Parse(ddlTypeSearch.SelectedValue))
                                               && (int.Parse(ddlSubTypeSearch.SelectedValue) == 0 || dt.subtype_idx == int.Parse(ddlSubTypeSearch.SelectedValue))
                                               && (txt_documentno_Search.Text == "" || dt.docrequest_code.Contains(txt_documentno_Search.Text)) //where Like use Contains

                                               select dt

                                          ).Distinct().ToList();


                    ViewState["Vs_DetailContract"] = _linqDocumentDetail.ToList();

                    setGridData(gvDetailList, ViewState["Vs_DetailContract"]);
                }
                else
                {
                    setGridData(gvDetailList, ViewState["Vs_DetailContract"]);
                }
               



                //ViewState["Vs_DetailContract_ALL"] = data_u0_document_detail.tcm_u0_document_list;

                break;
            case "cmdResetDetail":
                setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0, 0);

                break;

        }

    }
    //endbtn

    #endregion event command

    #region set data table
    //piorities
    protected void getPrioritiesList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsPrioritiesList = new DataSet();
        dsPrioritiesList.Tables.Add("dsPrioritiesListTable");

        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drTypeofpriorityText", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drPriorityDateText", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drPriorityNoText", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drCountrypriorityIdx", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drCountrypriorityText", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drStatuspriorityIdx", typeof(String));
        dsPrioritiesList.Tables["dsPrioritiesListTable"].Columns.Add("drStatuspriorityText", typeof(String));
        
        ViewState["vsPrioritiesList"] = dsPrioritiesList;
    }

    protected void setPrioritiesList()
    {
        if (ViewState["vsPrioritiesList"] != null)
        {
            TextBox tbType_of_priority = (TextBox)fvPrioritiesDetail.FindControl("tbType_of_priority");
            TextBox tb_Priority_Date = (TextBox)fvPrioritiesDetail.FindControl("tb_Priority_Date");
            TextBox tb_Priority_No = (TextBox)fvPrioritiesDetail.FindControl("tb_Priority_No");
            DropDownList ddlCountry_priority = (DropDownList)fvPrioritiesDetail.FindControl("ddlCountry_priority");
            DropDownList ddlStatus_priority = (DropDownList)fvPrioritiesDetail.FindControl("ddlStatus_priority");
            GridView gvPrioritiesList = (GridView)fvPrioritiesDetail.FindControl("gvPrioritiesList");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsPrioritiesList"];

            foreach (DataRow dr in dsContacts.Tables["dsPrioritiesListTable"].Rows)
            {
                if (dr["drTypeofpriorityText"].ToString() == tbType_of_priority.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Check Data Again ---');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsPrioritiesListTable"].NewRow();
            drContacts["drTypeofpriorityText"] = tbType_of_priority.Text;
            drContacts["drPriorityDateText"] = tb_Priority_Date.Text;
            drContacts["drPriorityNoText"] = tb_Priority_No.Text;
            drContacts["drCountrypriorityIdx"] = ddlCountry_priority.SelectedValue;
            drContacts["drCountrypriorityText"] = ddlCountry_priority.SelectedItem.Text;
            drContacts["drStatuspriorityIdx"] = ddlStatus_priority.SelectedValue;
            drContacts["drStatuspriorityText"] = ddlStatus_priority.SelectedItem.Text;

            dsContacts.Tables["dsPrioritiesListTable"].Rows.Add(drContacts);
            ViewState["vsPrioritiesList"] = dsContacts;


            setGridData(gvPrioritiesList, dsContacts.Tables["dsPrioritiesListTable"]);
            gvPrioritiesList.Visible = true;
        }
    }

    protected void CleardataSetPrioritiesList()
    {
        var gvPrioritiesList = (GridView)fvPrioritiesDetail.FindControl("gvPrioritiesList");
        ViewState["vsPrioritiesList"] = null;
        gvPrioritiesList.DataSource = ViewState["vsPrioritiesList"];
        gvPrioritiesList.DataBind();
        getPrioritiesList();
    }

    //client
    protected void getClientList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsClientList = new DataSet();
        dsClientList.Tables.Add("dsClientListTable");

        dsClientList.Tables["dsClientListTable"].Columns.Add("drClientDetailText", typeof(String));
        dsClientList.Tables["dsClientListTable"].Columns.Add("drClientContactText", typeof(String));
        dsClientList.Tables["dsClientListTable"].Columns.Add("drClientReferenceText", typeof(String));
        dsClientList.Tables["dsClientListTable"].Columns.Add("drClientStatusIdx", typeof(String));
        dsClientList.Tables["dsClientListTable"].Columns.Add("drClientStatusText", typeof(String));

        ViewState["vsClientList"] = dsClientList;
    }

    protected void setClientList()
    {
        if (ViewState["vsClientList"] != null)
        {
            TextBox tbClientDetail = (TextBox)fvClientDetail.FindControl("tbClientDetail");
            TextBox tbClientContact = (TextBox)fvClientDetail.FindControl("tbClientContact");
            TextBox tbClientReference = (TextBox)fvClientDetail.FindControl("tbClientReference");
            DropDownList ddlClientStatus = (DropDownList)fvClientDetail.FindControl("ddlClientStatus");
            GridView gvClientList = (GridView)fvClientDetail.FindControl("gvClientList");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsClientList"];

            foreach (DataRow dr in dsContacts.Tables["dsClientListTable"].Rows)
            {
                if (dr["drClientDetailText"].ToString() == tbClientDetail.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Check Data Again ---');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsClientListTable"].NewRow();
            drContacts["drClientDetailText"] = tbClientDetail.Text;
            drContacts["drClientContactText"] = tbClientContact.Text;
            drContacts["drClientReferenceText"] = tbClientReference.Text;
            drContacts["drClientStatusIdx"] = ddlClientStatus.SelectedValue;
            drContacts["drClientStatusText"] = ddlClientStatus.SelectedItem.Text;

            dsContacts.Tables["dsClientListTable"].Rows.Add(drContacts);
            ViewState["vsClientList"] = dsContacts;


            setGridData(gvClientList, dsContacts.Tables["dsClientListTable"]);
            gvClientList.Visible = true;
        }
    }

    protected void CleardataSetClientList()
    {
        var gvClientList = (GridView)fvClientDetail.FindControl("gvClientList");
        ViewState["vsClientList"] = null;
        gvClientList.DataSource = ViewState["vsClientList"];
        gvClientList.DataBind();
        getClientList();
    }

    //agent
    protected void getAgentList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsAgentList = new DataSet();
        dsAgentList.Tables.Add("dsAgentListTable");

        dsAgentList.Tables["dsAgentListTable"].Columns.Add("drAgentDetailText", typeof(String));
        dsAgentList.Tables["dsAgentListTable"].Columns.Add("drAgentContactText", typeof(String));
        dsAgentList.Tables["dsAgentListTable"].Columns.Add("drAgentReferenceText", typeof(String));
        dsAgentList.Tables["dsAgentListTable"].Columns.Add("drAgentStatusIdx", typeof(String));
        dsAgentList.Tables["dsAgentListTable"].Columns.Add("drAgentStatusText", typeof(String));

        ViewState["vsAgentList"] = dsAgentList;
    }

    protected void setAgentList()
    {
        if (ViewState["vsAgentList"] != null)
        {
            TextBox tbAgentDetail = (TextBox)fvAgentDetail.FindControl("tbAgentDetail");
            TextBox tbAgentContact = (TextBox)fvAgentDetail.FindControl("tbAgentContact");
            TextBox tbAgentReference = (TextBox)fvAgentDetail.FindControl("tbAgentReference");
            DropDownList ddlAgentStatus = (DropDownList)fvAgentDetail.FindControl("ddlAgentStatus");
            GridView gvAgentList = (GridView)fvAgentDetail.FindControl("gvAgentList");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsAgentList"];

            foreach (DataRow dr in dsContacts.Tables["dsAgentListTable"].Rows)
            {
                if (dr["drAgentDetailText"].ToString() == tbAgentDetail.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Check Data Again ---');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsAgentListTable"].NewRow();
            drContacts["drAgentDetailText"] = tbAgentDetail.Text;
            drContacts["drAgentContactText"] = tbAgentContact.Text;
            drContacts["drAgentReferenceText"] = tbAgentReference.Text;
            drContacts["drAgentStatusIdx"] = ddlAgentStatus.SelectedValue;
            drContacts["drAgentStatusText"] = ddlAgentStatus.SelectedItem.Text;

            dsContacts.Tables["dsAgentListTable"].Rows.Add(drContacts);
            ViewState["vsAgentList"] = dsContacts;


            setGridData(gvAgentList, dsContacts.Tables["dsAgentListTable"]);
            gvAgentList.Visible = true;
        }
    }

    protected void CleardataSetAgentList()
    {
        var gvAgentList = (GridView)fvAgentDetail.FindControl("gvAgentList");
        ViewState["vsAgentList"] = null;
        gvAgentList.DataSource = ViewState["vsAgentList"];
        gvAgentList.DataBind();
        getAgentList();
    }

    //good
    protected void getGoodsList()
    {
        //ViewState["vsFoodMaterialList"] = null;
        DataSet dsGoodsList = new DataSet();
        dsGoodsList.Tables.Add("dsGoodsListTable");

        dsGoodsList.Tables["dsGoodsListTable"].Columns.Add("drGoodsIntClassText", typeof(String));
        dsGoodsList.Tables["dsGoodsListTable"].Columns.Add("drGoodsDetailText", typeof(String));
        dsGoodsList.Tables["dsGoodsListTable"].Columns.Add("drLocallanguageIdx", typeof(String));
        dsGoodsList.Tables["dsGoodsListTable"].Columns.Add("drLocallanguageText", typeof(String));
        

        ViewState["vsGoodsList"] = dsGoodsList;
    }

    protected void setGoodsList()
    {
        if (ViewState["vsGoodsList"] != null)
        {
            TextBox tbGoods_IntClass = (TextBox)fvGoodsDetail.FindControl("tbGoods_IntClass");
            TextBox tbGoodsDetail = (TextBox)fvGoodsDetail.FindControl("tbGoodsDetail");
            DropDownList ddlLocal_language = (DropDownList)fvGoodsDetail.FindControl("ddlLocal_language");
            GridView gvGoodsList = (GridView)fvGoodsDetail.FindControl("gvGoodsList");

            CultureInfo culture = new CultureInfo("en-US");
            Thread.CurrentThread.CurrentCulture = culture;

            DataSet dsContacts = (DataSet)ViewState["vsGoodsList"];

            foreach (DataRow dr in dsContacts.Tables["dsGoodsListTable"].Rows)
            {
                if (dr["drGoodsIntClassText"].ToString() == tbGoods_IntClass.Text)
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('--- Check Data Again ---');", true);
                    return;
                }
            }
            DataRow drContacts = dsContacts.Tables["dsGoodsListTable"].NewRow();
            drContacts["drGoodsIntClassText"] = tbGoods_IntClass.Text;
            drContacts["drGoodsDetailText"] = tbGoodsDetail.Text;
            drContacts["drLocallanguageIdx"] = ddlLocal_language.SelectedValue;
            drContacts["drLocallanguageText"] = ddlLocal_language.SelectedItem.Text;

            dsContacts.Tables["dsGoodsListTable"].Rows.Add(drContacts);
            ViewState["vsGoodsList"] = dsContacts;


            setGridData(gvGoodsList, dsContacts.Tables["dsGoodsListTable"]);
            gvGoodsList.Visible = true;
        }
    }

    protected void CleardataSetGoodsList()
    {
        var gvGoodsList = (GridView)fvGoodsDetail.FindControl("gvGoodsList");
        ViewState["vsGoodsList"] = null;
        gvGoodsList.DataSource = ViewState["vsGoodsList"];
        gvGoodsList.DataBind();
        getGoodsList();
    }


    #endregion set data table

    #region pathfile
    protected string MapURL(string path)
    {

        string appPath = Server.MapPath("~/").ToLower();
        return string.Format("/{0}", path.ToLower().Replace(appPath, "").Replace(@"\", "/"));
    }

    protected void SelectPathFile(string _docrequest_code, string path, GridView gvName)
    {
        try
        {
            string filePathView = Server.MapPath(path + _docrequest_code);
            DirectoryInfo myDirLotus = new DirectoryInfo(filePathView);
            SearchDirectories(myDirLotus, _docrequest_code, gvName);
            gvName.Visible = true;

        }
        catch (Exception ex)
        {
            //gvName.Visible = false;
            //txt.Text = ex.ToString();
        }
    }

    public void SearchDirectories(DirectoryInfo dir, String target, GridView gvName)
    {

        string dirfiles = dir.ToString();

        DataSet ds1 = new DataSet();
        DataTable dt1 = new DataTable();
        DataRow dr1 = dt1.NewRow();
        DataColumn dc1 = new DataColumn("FileName", typeof(string));
        dc1 = new DataColumn("Download", typeof(string));
        dt1.Columns.Add("FileName");
        dt1.Columns.Add("Download");


        FileInfo[] files = dir.GetFiles();
        int i = 0;
        foreach (FileInfo file in files)
        {
            //if (file.Name.ToLower().IndexOf(target.ToLower()) > -1)
            //{
            string[] f = Directory.GetFiles(dirfiles, file.Name, SearchOption.AllDirectories);
            dt1.Rows.Add(file.Name);
            dt1.Rows[i][1] = f[0];
            i++;
            //}
        }

        if (dt1.Rows.Count > 0)
        {

            ds1.Tables.Add(dt1);
            gvName.DataSource = ds1.Tables[0]; //กรณี ที่กำหนดให้ดึง DataTable ใน DataSet 
            gvName.DataBind();
            ds1.Dispose();
        }
        else
        {

            gvName.DataSource = null;
            gvName.DataBind();

        }


    }

    #endregion path file

    #region reuse

    protected void initPageLoad()
    {

    }

    protected void initPage()
    {

        setActiveTab("docDetail", 0, 0, 0, 0, 0, 0, 0, 0);

    }

    protected void linkBtnTrigger(LinkButton linkBtnID)
    {
        UpdatePanel updatePanel = Page.Master.FindControl("upMain") as UpdatePanel;
        UpdatePanelControlTrigger triggerLinkBtn = new PostBackTrigger();
        triggerLinkBtn.ControlID = linkBtnID.UniqueID;
        updatePanel.Triggers.Add(triggerLinkBtn);
    }

    protected void setDir(string dirName)
    {
        if (!Directory.Exists(Server.MapPath(dirName)))
        {
            Directory.CreateDirectory(Server.MapPath(dirName));
        }
    }

    protected void setGridData(GridView gvName, Object obj)
    {
        gvName.DataSource = obj;
        gvName.DataBind();
    }

    protected void setFormData(FormView fvName, FormViewMode fvMode, Object obj)
    {
        fvName.ChangeMode(fvMode);
        fvName.DataSource = obj;
        fvName.DataBind();
    }

    protected void setRepeaterData(Repeater rptName, Object obj)
    {
        rptName.DataSource = obj;
        rptName.DataBind();
    }

    protected void setDdlData(DropDownList ddlName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        ddlName.Items.Clear();
        // bind items
        ddlName.DataSource = obj;
        ddlName.DataTextField = _data_text;
        ddlName.DataValueField = _data_value;
        ddlName.DataBind();
    }

    protected void setChkData(CheckBoxList chkName, Object obj, string _data_text, string _data_value)
    {
        // clear data
        chkName.Items.Clear();
        // bind items
        chkName.DataSource = obj;
        chkName.DataTextField = _data_text;
        chkName.DataValueField = _data_value;
        chkName.DataBind();
    }

    protected data_employee callServiceEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //fss.Text = _localJson.ToString();

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);
        //fsa.Text = _cmdUrl + _localJson;

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);


        return _dataEmployee;
    }

    protected data_employee callServiceGetEmployee(string _cmdUrl, int _emp_idx)
    {
        // call services
        _localJson = _funcTool.callServiceGet(_cmdUrl + _emp_idx);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_employee callServicePostEmployee(string _cmdUrl, data_employee _dataEmployee)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_dataEmployee);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _dataEmployee = (data_employee)_funcTool.convertJsonToObject(typeof(data_employee), _localJson);

        return _dataEmployee;
    }

    protected data_law callServicePostLaw(string _cmdUrl, data_law _data_law)
    {
        // convert to json
        _localJson = _funcTool.convertObjectToJson(_data_law);
        //litDebug.Text = _localJson;

        // call services
        _localJson = _funcTool.callServicePost(_cmdUrl, _localJson);

        // convert json to object
        _data_law = (data_law)_funcTool.convertJsonToObject(typeof(data_law), _localJson);

        return _data_law;
    }

    protected void getEmployeeProfile(int _emp_idx)
    {
        _dataEmployee = callServiceGetEmployee(_urlGetMyProfile, _emp_idx);
        ViewState["vsEmpProfile"] = _dataEmployee;
    }

    protected void setActiveView(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cempidx, int u1idx, int _type_idx)
    {
        //set tab
        mvSystem.SetActiveView((View)mvSystem.FindControl(activeTab));

        //tab create
        setFormData(fvEmpDetail, FormViewMode.ReadOnly, null);
        setFormData(FvCreate, FormViewMode.ReadOnly, null);

        //tab detail
        gvDetailList.Visible = false;
        setGridData(gvDetailList, null);
        setFormData(fvEmpViewDetail, FormViewMode.ReadOnly, null);
        setFormData(fvDocumentDetail, FormViewMode.ReadOnly, null);
        setFormData(fvSearchDetail, FormViewMode.ReadOnly, null);
        div_LogViewDetail.Visible = false;

        //tab wait approve
        gvWaitApprove.Visible = false;
        setGridData(gvWaitApprove, null);
        setFormData(fvEmpViewDetailApprove, FormViewMode.ReadOnly, null);
        setFormData(fvWaitApproveDocumentDetail, FormViewMode.ReadOnly, null);

        //setGridData(gvDetailLawOfficer, null);

        div_LogViewDetail_WaitApprove.Visible = false;

        setFormData(fvDirectorUser, FormViewMode.ReadOnly, null);
        setFormData(fvMgLaw, FormViewMode.ReadOnly, null);
        setFormData(fvLaw, FormViewMode.ReadOnly, null);
        setFormData(fvMgLawMemo, FormViewMode.ReadOnly, null);
        setFormData(fvUserCreateFileMemo, FormViewMode.ReadOnly, null);

        setFormData(fvDirectorUserCheckMemo, FormViewMode.ReadOnly, null);
        setFormData(fvLawOfficerCheckMemo, FormViewMode.ReadOnly, null);
        setFormData(fvLawOfficerEditFileMemo, FormViewMode.ReadOnly, null);
        

        //tab record sheet
        gvRecordSheet.Visible = false;
        setGridData(gvRecordSheet, null);

        setFormData(fvRegistrationDetail, FormViewMode.ReadOnly, null);
        ////Panel_RecordSheet.Visible = false;
        setFormData(fvPublicationDetail, FormViewMode.ReadOnly, null);
        setFormData(fvDependentDetail, FormViewMode.ReadOnly, null);
        setFormData(fvPrioritiesDetail, FormViewMode.ReadOnly, null);
        setFormData(fvClientDetail, FormViewMode.ReadOnly, null);
        setFormData(fvAgentDetail, FormViewMode.ReadOnly, null);
        setFormData(fvGoodsDetail, FormViewMode.ReadOnly, null);
        setFormData(fvAdditionalDetail, FormViewMode.ReadOnly, null);
        setFormData(fvTademarkProfile, FormViewMode.ReadOnly, null);
        setFormData(fvBackground, FormViewMode.ReadOnly, null);
        setFormData(fvSummary, FormViewMode.ReadOnly, null);
        setFormData(fvClaims, FormViewMode.ReadOnly, null);
        setFormData(fvAbstract, FormViewMode.ReadOnly, null);


        div_LogRecordSheet.Visible = false;
        Panel_SaveRecordSheet.Visible = false;

        gvActions.Visible = false;
        setGridData(gvActions, null);
        Panel_HeaderRecordSheet.Visible = false;

        upMain_HeadDetail.Visible = false;

        //btnSaveRecordSheet.Visible = false;
        //btnViewRecordSheet.Visible = false;


        switch (activeTab)
        {
            case "docDetail":
                if (uidx == 0) //detail in index
                {
                    //upMain_HeadDetail.Visible = true;
                    //setPanelTitle("doc-detail");
                    setFormData(fvSearchDetail, FormViewMode.Insert, null);
                    DropDownList ddlTypeSearch = (DropDownList)fvSearchDetail.FindControl("ddlTypeSearch");
                    DropDownList ddlSubTypeSearch = (DropDownList)fvSearchDetail.FindControl("ddlSubTypeSearch");

                    getDoctypeDetail(ddlTypeSearch, 0);
                    getSubDoctypeDetail(ddlSubTypeSearch, int.Parse(ddlTypeSearch.SelectedValue));

                    gvDetailList.Visible = true;
                    getDetailDocumentTrademark(gvDetailList, 0);

                }
                else if (uidx > 0) //view detail document
                {

                    setFormData(fvEmpViewDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                    getViewDetailDocumentTrademark(fvDocumentDetail, uidx);

                    TextBox tbdocrequest_code_viewdetail = (TextBox)fvDocumentDetail.FindControl("tbdocrequest_code_viewdetail");

                    string getPath_file_law_usercreate = ConfigurationSettings.AppSettings["path_file_law_usercreate"];
                    string getPath_file_law_lawmemo = ConfigurationSettings.AppSettings["path_file_law_lawmemo"];
                    string getPath_file_law_usermemo = ConfigurationSettings.AppSettings["path_file_law_usermemo"];

                    string docrequest_code = "doc-" + tbdocrequest_code_viewdetail.Text;
                    string docrequest_code_lawmemo = "lawmemo-" + tbdocrequest_code_viewdetail.Text;
                    string docrequest_code_usermemo = "usermemo-" + tbdocrequest_code_viewdetail.Text;

                    GridView gvViewDetailLawOfficer = (GridView)fvDocumentDetail.FindControl("gvViewDetailLawOfficer");
                    GridView gvFileDocViewDetail = (GridView)fvDocumentDetail.FindControl("gvFileDocViewDetail");
                    GridView gvFileMemoLawOfficer = (GridView)fvDocumentDetail.FindControl("gvFileMemoLawOfficer");
                    GridView gvFileMemoUser = (GridView)fvDocumentDetail.FindControl("gvFileMemoUser");

                    gvViewDetailLawOfficer.Visible = false;
                    setGridData(gvViewDetailLawOfficer, null);

                    //select path show file
                    SelectPathFile(docrequest_code.ToString(), getPath_file_law_usercreate, gvFileDocViewDetail);
                    SelectPathFile(docrequest_code_lawmemo.ToString(), getPath_file_law_lawmemo, gvFileMemoLawOfficer);
                    SelectPathFile(docrequest_code_usermemo.ToString(), getPath_file_law_usermemo, gvFileMemoUser);
                    //select path show file

                    div_LogViewDetail.Visible = true;
                    getLogDetailDocument(rptLogViewDetail, uidx, 0);

                    switch (nodeidx)
                    {
                        case 2:
                        case 3:
                        case 4:
                        case 5:
                        case 6:
                        case 7:
                        case 8:
                        case 9:
                        case 12:
                            gvViewDetailLawOfficer.Visible = true;
                            getViewDetailLawOfficer(gvViewDetailLawOfficer, uidx);
                            break;

                    }
                }

                setOntop.Focus();
                break;
            case "docCreate":

                setFormData(fvEmpDetail, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);
                setFormData(FvCreate, FormViewMode.Insert, null);

                setOntop.Focus();

                break;
            case "docWaitApprove":
                
                if (uidx == 0) //detail wait document approve
                {
                    gvWaitApprove.Visible = true;
                    getDetailWaitApproveDocument(gvWaitApprove, 0);
                }
                else if (uidx > 0)//view detail wait document approve
                {

                    setFormData(fvEmpViewDetailApprove, FormViewMode.ReadOnly, ((data_employee)ViewState["vsEmpProfile"]).employee_list);

                    if(nodeidx == 1)
                    {
                        getViewDetailEditDocumentTrademark(fvWaitApproveDocumentDetail, uidx);
                        GridView gvFileDocViewDetail = (GridView)fvWaitApproveDocumentDetail.FindControl("gvFileDocViewDetail");
                        TextBox tbdocrequest_code_viewdetail = (TextBox)fvWaitApproveDocumentDetail.FindControl("tbdocrequest_code_viewdetail");
                        //select path file
                        string getPath_file_law_usercreate = ConfigurationSettings.AppSettings["path_file_law_usercreate"];
                        string docrequest_code = "doc-" + tbdocrequest_code_viewdetail.Text;
                        SelectPathFile(docrequest_code.ToString(), getPath_file_law_usercreate, gvFileDocViewDetail);
                    }
                    else
                    {
                        getViewDetailDocumentTrademark(fvWaitApproveDocumentDetail, uidx);

                        TextBox tbdocrequest_code_viewdetail = (TextBox)fvWaitApproveDocumentDetail.FindControl("tbdocrequest_code_viewdetail");
                        GridView gvDetailLawOfficer = (GridView)fvWaitApproveDocumentDetail.FindControl("gvDetailLawOfficer");

                        GridView gvFileDocViewDetail = (GridView)fvWaitApproveDocumentDetail.FindControl("gvFileDocViewDetail");
                        GridView gvFileMemoLawOfficer = (GridView)fvWaitApproveDocumentDetail.FindControl("gvFileMemoLawOfficer");
                        GridView gvFileMemoUser = (GridView)fvWaitApproveDocumentDetail.FindControl("gvFileMemoUser");

                        //select path file
                        string getPath_file_law_usercreate = ConfigurationSettings.AppSettings["path_file_law_usercreate"];
                        string getPath_file_law_lawmemo = ConfigurationSettings.AppSettings["path_file_law_lawmemo"];
                        string getPath_file_law_usermemo = ConfigurationSettings.AppSettings["path_file_law_usermemo"];

                        string docrequest_code = "doc-" + tbdocrequest_code_viewdetail.Text;
                        string docrequest_code_lawmemo = "lawmemo-" + tbdocrequest_code_viewdetail.Text;
                        string docrequest_code_usermemo = "usermemo-" + tbdocrequest_code_viewdetail.Text;

                        SelectPathFile(docrequest_code.ToString(), getPath_file_law_usercreate, gvFileDocViewDetail);
                        SelectPathFile(docrequest_code_lawmemo.ToString(), getPath_file_law_lawmemo, gvFileMemoLawOfficer);
                        SelectPathFile(docrequest_code_usermemo.ToString(), getPath_file_law_usermemo, gvFileMemoUser);
                        //select path file

                        gvDetailLawOfficer.Visible = false;
                        setGridData(gvDetailLawOfficer, null);

                        gvDetailLawOfficer.Visible = true;
                        getViewDetailLawOfficer(gvDetailLawOfficer, uidx);

                    }

                    div_LogViewDetail_WaitApprove.Visible = true;
                    getLogDetailDocument(rptLogWaitApprove, uidx, 0);
                    switch (nodeidx)
                    {
                        case 1:
                            getDecisionAttachFileApprove(fvWaitApproveDocumentDetail, nodeidx);
                            //setFormData(fvEditDocument, FormViewMode.Edit, ViewState["Value_DocumentEdit"]);
                            
                            break;

                        case 2: //director user approve
                            
                            setFormData(fvDirectorUser, FormViewMode.Insert, null);
                            DropDownList ddlDirectorUserApprove = (DropDownList)fvDirectorUser.FindControl("ddlDirectorUserApprove");
                            getDecisionApprove(ddlDirectorUserApprove, nodeidx);

                            break;
                        case 3: //mg approve receive and select law officer document
                            setFormData(fvMgLaw, FormViewMode.Insert, null);
                            DropDownList ddlMgLawApprove = (DropDownList)fvMgLaw.FindControl("ddlMgLawApprove");
                            getDecisionApprove(ddlMgLawApprove, nodeidx);
                            break;
                        case 4: // law officer check document and selected file memo

                            setFormData(fvLaw, FormViewMode.Insert, null);
                            DropDownList ddlLawApprove = (DropDownList)fvLaw.FindControl("ddlLawApprove");
                            getDecisionApprove(ddlLawApprove, nodeidx);

                            ////gvDetailLawOfficer.Visible = true;
                            ////getViewDetailLawOfficer(gvDetailLawOfficer, uidx);

                            break;
                        case 5: //user attach file memo

                            setFormData(fvUserCreateFileMemo, FormViewMode.Insert, null);
                           
                            getDecisionAttachFileApprove(fvUserCreateFileMemo, nodeidx);


                            ////gvDetailLawOfficer.Visible = true;
                            ////getViewDetailLawOfficer(gvDetailLawOfficer, uidx);
                            break;
                        case 6: //director user check memo
                            setFormData(fvDirectorUserCheckMemo, FormViewMode.Insert, null);
                            DropDownList ddlDirectorUserCheckMemo = (DropDownList)fvDirectorUserCheckMemo.FindControl("ddlDirectorUserCheckMemo");
                            getDecisionApprove(ddlDirectorUserCheckMemo, nodeidx);

                            ////gvDetailLawOfficer.Visible = true;
                            ////getViewDetailLawOfficer(gvDetailLawOfficer, uidx);
                            break;
                        case 7: //law officer check file memo
                            setFormData(fvLawOfficerCheckMemo, FormViewMode.Insert, null);
                            DropDownList ddlLawOfficerCheckMemo = (DropDownList)fvLawOfficerCheckMemo.FindControl("ddlLawOfficerCheckMemo");
                            getDecisionApprove(ddlLawOfficerCheckMemo, nodeidx);

                            ////gvDetailLawOfficer.Visible = true;
                            ////getViewDetailLawOfficer(gvDetailLawOfficer, uidx);
                            break;
                        case 8:
                        case 12: //mg law check memo form law officer
                            setFormData(fvMgLawMemo, FormViewMode.Insert, null);
                            DropDownList ddlMgLawApproveMemo = (DropDownList)fvMgLawMemo.FindControl("ddlMgLawApproveMemo");
                            getDecisionApprove(ddlMgLawApproveMemo, nodeidx);

                            ////gvDetailLawOfficer.Visible = true;
                            ////getViewDetailLawOfficer(gvDetailLawOfficer, uidx);
                            break;
                        case 13: //law officer edit file memo

                            setFormData(fvLawOfficerEditFileMemo, FormViewMode.Insert, null);

                            getDecisionAttachFileApprove(fvLawOfficerEditFileMemo, nodeidx);


                            break;
                    }
                }

                setOntop.Focus();
                break;
            case "docRegistration": //record sheet

                if(u1idx == 0)
                {
                    gvRecordSheet.Visible = true;
                    getDetailRecordSheet(gvRecordSheet, 0);

                }
                else if(u1idx > 0)
                {
                    switch (_chk_tab)
                    {
                        case 0: // view record sheet
                            ////Panel_RecordSheet.Visible = true;
                            
                            break;

                        case 1: // edit record sheet
                            ////Panel_RecordSheet.Visible = true;

                            data_law data_u1_document_viewrecord = new data_law();
                            tcm_u1_document_detail u1_document_viewrecord = new tcm_u1_document_detail();
                            data_u1_document_viewrecord.tcm_u1_document_list = new tcm_u1_document_detail[1];

                            u1_document_viewrecord.u1_doc_idx = u1idx;
                            u1_document_viewrecord.type_idx = _type_idx;

                            data_u1_document_viewrecord.tcm_u1_document_list[0] = u1_document_viewrecord;
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));
                            data_u1_document_viewrecord = callServicePostLaw(_urlGetLawRecordSheet, data_u1_document_viewrecord);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u3_document_viewdetail));

                            if (data_u1_document_viewrecord.return_code == 0)
                            {

                                ViewState["vs_detailrecordsheet"] = data_u1_document_viewrecord.tcm_u1_document_list;

                                string _value_formview = data_u1_document_viewrecord.tcm_u1_document_list[0].formview_set.ToString();
                                string[] _set_formview = _value_formview.Split(',');

                                foreach (string _i in _set_formview)
                                {
                                    //check form view in record sheet
                                    if (_i.ToString() == "fvRegistrationDetail")
                                    {
                                        //litDebug.Text += _i.ToString() + "|";
                                        setFormData(fvRegistrationDetail, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);

                                        TextBox tbowner_idx = (TextBox)fvRegistrationDetail.FindControl("tbowner_idx");
                                        DropDownList ddlowner_idx = (DropDownList)fvRegistrationDetail.FindControl("ddlowner_idx");

                                        TextBox tbincorporated_in_idx = (TextBox)fvRegistrationDetail.FindControl("tbincorporated_in_idx");
                                        DropDownList ddlincorporated_in_idx = (DropDownList)fvRegistrationDetail.FindControl("ddlincorporated_in_idx");


                                        getOwner(ddlowner_idx, int.Parse(tbowner_idx.Text));
                                        getIncorporated(ddlincorporated_in_idx, int.Parse(tbincorporated_in_idx.Text));
                                    }

                                    if (_i.ToString() == "fvPublicationDetail")
                                    {
                                        setFormData(fvPublicationDetail, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvDependentDetail")
                                    {
                                        setFormData(fvDependentDetail, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvPrioritiesDetail")
                                    {
                                        setFormData(fvPrioritiesDetail, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvClientDetail")
                                    {
                                        setFormData(fvClientDetail, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvAgentDetail")
                                    {
                                        setFormData(fvAgentDetail, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvGoodsDetail")
                                    {
                                        setFormData(fvGoodsDetail, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvAdditionalDetail")
                                    {
                                        setFormData(fvAdditionalDetail, FormViewMode.Edit, ViewState["vs_detailrecordsheet"]);
                                    }
                                    //check form view in record sheet

                                }

                            }

                            break;
                        case 2: // save insert record sheet
                            ////Panel_RecordSheet.Visible = true;
                            Panel_HeaderRecordSheet.Visible = true;
                            div_LogRecordSheet.Visible = true;
                            getLogDetailDocument(rptLogRecordSheet, uidx, u1idx);
                            Panel_SaveRecordSheet.Visible = true;

                            

                            data_law data_u1_document_insertrecord = new data_law();
                            tcm_u1_document_detail u1_document_insertrecord = new tcm_u1_document_detail();
                            data_u1_document_insertrecord.tcm_u1_document_list = new tcm_u1_document_detail[1];

                            u1_document_insertrecord.u1_doc_idx = u1idx;
                            u1_document_insertrecord.type_idx = _type_idx;

                            data_u1_document_insertrecord.tcm_u1_document_list[0] = u1_document_insertrecord;
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToJson(data_m0type_detail));
                            data_u1_document_insertrecord = callServicePostLaw(_urlGetLawRecordSheet, data_u1_document_insertrecord);
                            //litDebug.Text = HttpUtility.HtmlEncode(_funcTool.convertObjectToXml(data_u3_document_viewdetail));

                            if (data_u1_document_insertrecord.return_code == 0)
                            {

                                ViewState["vs_insertrecordsheet"] = data_u1_document_insertrecord.tcm_u1_document_list;

                                string _value_formview = data_u1_document_insertrecord.tcm_u1_document_list[0].formview_set.ToString();
                                string[] _set_formview = _value_formview.Split(',');

                                foreach (string _i in _set_formview)
                                {
                                    //check form view in record sheet
                                    if (_i.ToString() == "fvRegistrationDetail")
                                    {
                                        //litDebug.Text += _i.ToString() + "|";
                                        setFormData(fvRegistrationDetail, FormViewMode.Insert, ViewState["vs_insertrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvPublicationDetail")
                                    {
                                        setFormData(fvPublicationDetail, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvDependentDetail")
                                    {
                                        setFormData(fvDependentDetail, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvPrioritiesDetail")
                                    {
                                        setFormData(fvPrioritiesDetail, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvClientDetail")
                                    {
                                        setFormData(fvClientDetail, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvAgentDetail")
                                    {
                                        setFormData(fvAgentDetail, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvGoodsDetail")
                                    {
                                        setFormData(fvGoodsDetail, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvAdditionalDetail")
                                    {
                                        setFormData(fvAdditionalDetail, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvTademarkProfile")
                                    {
                                        setFormData(fvTademarkProfile, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "gvActions")
                                    {
                                        gvActions.Visible = true;
                                        getLawM0Actions(gvActions, 0);
                                    }

                                    if (_i.ToString() == "fvTademarkProfile")
                                    {
                                        setFormData(fvTademarkProfile, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvBackground")
                                    {
                                        setFormData(fvBackground, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvSummary")
                                    {
                                        setFormData(fvSummary, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvClaims")
                                    {
                                        setFormData(fvClaims, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    if (_i.ToString() == "fvAbstract")
                                    {
                                        setFormData(fvAbstract, FormViewMode.Insert, ViewState["vs_detailrecordsheet"]);
                                    }

                                    //check form view in record sheet



                                }

                            }

                            break;
                    }
                }

                setOntop.Focus();
                break;


        }
    }

    protected void setActiveTab(string activeTab, int uidx, int nodeidx, int staidx, int actor_idx, int _chk_tab, int _cemp_idx, int u1idx, int _type_idx)
    {
        setActiveView(activeTab, uidx, nodeidx, staidx, actor_idx, _chk_tab, _cemp_idx, u1idx, _type_idx);
        switch (activeTab)
        {
            case "docDetail":
                li0.Attributes.Add("class", "active");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                break;
            case "docCreate":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "active");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "");
                break;
            case "docWaitApprove":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "active");
                li3.Attributes.Add("class", "");
                break;
            case "docRegistration":
                li0.Attributes.Add("class", "");
                li1.Attributes.Add("class", "");
                li2.Attributes.Add("class", "");
                li3.Attributes.Add("class", "active");
                break;
        }
    }

    protected string getOnlyDate(string _dateIn)
    {
        return _funcTool.getOnlyDate(_dateIn);
    }

    protected string getStatus(int status)
    {
        if (status == 0)
        {

            return "<span class='statusmaster-online' data-toggle='tooltip' title='Certificate'><i class='glyphicon glyphicon-ok'></i></span>";
        }
        else
        {
            return "<span class='statusmaster-offline' data-toggle='tooltip' title='No Certificate'><i class='glyphicon glyphicon-remove'></i></span>";
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        // Confirms that an HtmlForm control is rendered for the
        //specified ASP.NET server control at run time.
    }

    protected void rptOnRowDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        var rptName = (Repeater)Sender;
        switch (rptName.ID)
        {

            case "":
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    


                }
                break;

        }
    }

    #endregion reuse

    #region set panel title
    protected void setPanelTitle(string _image_detail)
    {
        string path = Request.Url.AbsolutePath;
        string module = path.Substring(path.LastIndexOf('/') + 1).ToLower();
        string moduleName;
        string path_file;

        //test Title Program
        string module_Value = path.Substring(path.IndexOf('/') + 1).ToLower();

        //path_file = "~/masterpage/images/holiday-header/" + "room-booking" + ".png";

        //litDebug.Text = DateToday.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);//DateToday.ToString();

        switch(_image_detail)
        {
            case "doc-detail":
                path_file = "~/images/law-trademarks/" + "doc-detail" + ".png";
                //litDebug1.Text = path_file.ToString();
                moduleName = "<img src='" + ResolveUrl(path_file) + "' class='img-fluid' height='100px' width='100%' />";
                //moduleName = "<img src='" + ResolveUrl(path_file) + "' class='img-fluid' height='150px' width='100%' />";
                litPanelTitleHeadDetail.Text = moduleName;
                break;
        }
       

    }
    #endregion set panel title
}