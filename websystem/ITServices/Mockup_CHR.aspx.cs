﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class websystem_ITServices_Mockup_CHR : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(IsPostBack != true)
        {
            MvMaster.SetActiveView(ViewIndex);
            // Set_Defult_Index();


            
        }
       
    }

    
    protected void btnCommand(object sender, CommandEventArgs e)
    {
        string cmdName = e.CommandName;
        string cmdArg = e.CommandArgument.ToString();

        Control div_personal = (Control)FvDetailUser.FindControl("div_personal");
        Control div_p1 = (Control)FvDetailUser.FindControl("div_p1");

        LinkButton BoxButtonSearchShow = (LinkButton)FvDetailUser.FindControl("BoxButtonSearchShow");
        LinkButton BoxButtonSearchHide = (LinkButton)FvDetailUser.FindControl("BoxButtonSearchHide");
        
        switch (cmdName)
        {

            case "btnhide":

                MvMaster.SetActiveView(ViewDetail);
                FvDetailUser.ChangeMode(FormViewMode.Insert);
                FvDetailUser.DataSource = null;
                FvDetailUser.DataBind();

           
                break;

            case "btnback":
                Page.Response.Redirect(Page.Request.Url.ToString(), true);
                break;

            case "BtnHideSETBoxAllSearchShow":

              //  div_personal.Visible = true;
                BoxButtonSearchShow.Visible = false;
                BoxButtonSearchHide.Visible = true;
                div_p1.Visible = true;

                break;

            case "BtnHideSETBoxAllSearchHide":
              //  div_personal.Visible = false;
                BoxButtonSearchShow.Visible = true;
                BoxButtonSearchHide.Visible = false;
                div_p1.Visible = false;

                break;
        }
    }
}

   