﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masterpage/masterpage.master" AutoEventWireup="true" CodeFile="qa_lab_test_detail.aspx.cs" Inherits="websystem_MasterData_qa_lab_test_detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentMain" runat="Server">

    <div class="col-sm-12">
        <asp:HyperLink ID="SETFOCUS" runat="server"></asp:HyperLink>
        <asp:Literal ID="litDebug" runat="server"></asp:Literal>
    </div>

    <!--multiview-->
    <asp:MultiView ID="mvMaster" runat="server">
        <asp:View ID="pageGenaral" runat="server">
            <div class="form-group">
                <asp:LinkButton ID="btnAddTestDetail" CssClass="btn btn-primary" runat="server" data-original-title="เพิ่มประเภทการตรวจวิเคราะห์" data-toggle="tooltip"
                    OnCommand="btnCommand" CommandName="cmdAddTestDetail" CommandArgument="0" title="เพิ่มประเภทการตรวจวิเคราะห์"><i class="fa fa-plus-square" aria-hidden="true"></i> เพิ่มประเภทการตรวจวิเคราะห์</asp:LinkButton>
            </div>
            <!--formview insert-->
            <asp:FormView ID="fvformInsert" runat="server" DefaultMode="ReadOnly" Width="100%">
                <InsertItemTemplate>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">&nbsp;เพิ่มประเภทการตรวจวิเคราะห์</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal" role="form">
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ชื่อประเภทการตรวจ</label>
                                    <div class="col-sm-4">

                                        <asp:TextBox ID="txtTestDetailName" runat="server" CssClass="form-control" placeholder="กรอกประเภทการตรวจวิเคราะห์..." />                                     
                                        <asp:RequiredFieldValidator ID="RequiredtxtTestDetailName"  runat="server"
                                                ControlToValidate="txtTestDetailName" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อประเภทการตรวจ" ValidationGroup="save_Insert" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortxtTestDetailName5555" runat="Server"  PopupPosition="Right"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtxtTestDetailName" Width="250" />
                                    </div>

                                    <label class="col-sm-1 control-label">สถานะ</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlTestDetailStatus" runat="server" CssClass="form-control" Enabled="true">
                                            <asp:ListItem Text=" Online " Value="1"></asp:ListItem>
                                            <asp:ListItem Text=" Offline " Value="0"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">

                                    <label class="col-sm-2 control-label">ประเภทการปฏิบัติการ</label>
                                    <div class="col-sm-4">
                                        <asp:DropDownList ID="ddlLabType" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                                    </div>
                                    <asp:RequiredFieldValidator ID="RequiredddlLabType" runat="server" InitialValue="0"
                                        ControlToValidate="ddlLabType" Display="None" SetFocusOnError="true"
                                        ErrorMessage="*กรุณาเลือกประเภทการปฏิบัติการ" ValidationGroup="save_Insert" />
                                    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorddlLabType" runat="Server" PopupPosition="BottomRight"
                                        HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredddlLabType" Width="250" />

                                    <label class="col-sm-1 control-label">ระยะเวลา</label>
                                    <div class="col-sm-3">
                                        <asp:TextBox ID="txtActionTime" runat="server" CssClass="form-control" placeholder="กรอกระยะเวลาในการดำเนินการ.." Enabled="true" />
                                         <asp:RequiredFieldValidator ID="RequiredtxtActionTime" runat="server"
                                            ControlToValidate="txtActionTime" Display="None" SetFocusOnError="true"
                                            ErrorMessage="*กรุณากรอกระยะเวลาในการดำเนินการ" ValidationGroup="save_Insert" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortxtActionTime" runat="Server" PopupPosition="BottomRight"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtxtActionTime" Width="280" />

                                        <asp:RegularExpressionValidator ID="RegulartxtActionTime"
                                            runat="server" ErrorMessage="*กรอกเฉพาะตัวเลขเท่านั้น" SetFocusOnError="true"
                                            Display="None" ControlToValidate="txtActionTime" ValidationExpression="^[1-9'\s]{1,10}$"
                                            ValidationGroup="save_Insert" />
                                        <ajaxToolkit:ValidatorCalloutExtender ID="ValtxtActionTime" runat="Server" PopupPosition="BottomRight"
                                            HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegulartxtActionTime" Width="280" />
                                    </div>
                                    <label class="col-sm-1 control-label">วัน</label>
                                    <div class="col-sm-1"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <asp:LinkButton ID="lnkbtnSave" CssClass="btn btn-success" ValidationGroup="save_Insert" runat="server" data-original-title="Save" data-toggle="tooltip"
                                            Text="Save" OnCommand="btnCommand" CommandName="cmdSave" CommandArgument="0"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                            Text="Cancel" OnCommand="btnCommand" CommandName="cmdCancel" CommandArgument="0"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </InsertItemTemplate>
            </asp:FormView>

            <asp:GridView ID="gvMaster"
                runat="server"
                AutoGenerateColumns="false"
                DataKeyNames="test_detail_idx"
                CssClass="table table-striped table-bordered table-hover table-responsive col-lg-12"
                HeaderStyle-CssClass="success"
                HeaderStyle-Height="30px"
                AllowPaging="true"
                PageSize="10"
                OnPageIndexChanging="gvPageIndexChanging"
                OnRowEditing="gvRowEditing"
                OnRowUpdating="gvRowUpdating"
                OnRowCancelingEdit="gvRowCancelingEdit"
                OnRowDataBound="gvRowDataBound">
                <PagerStyle CssClass="pageCustom" />
                <PagerSettings Mode="NumericFirstLast" PageButtonCount="4"
                    FirstPageText="First" LastPageText="Last" />
                <EmptyDataTemplate>
                    <div style="text-align: center">Data Cannot Be Found</div>
                </EmptyDataTemplate>
                <Columns>
                    <asp:TemplateField HeaderText="ลำดับ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="left" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                              <div style="text-align: center;">
                            <asp:Label ID="lbtestDetailidx" runat="server" CssClass=" font_text text_center" Visible="false" Text='<%# Eval("test_detail_idx")%>'></asp:Label>
                            <%# (Container.DataItemIndex +1) %>
                                 </div>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <div class="panel-body">
                                <div class="form-horizontal" role="form">

                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <asp:TextBox ID="_updateTestDetailIDX" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("test_detail_idx")%>' />

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label1" CssClass="col-sm-3 control-label" runat="server" Text="ชื่อประเภทการตรวจวิเคราะห์" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="tbupdateTestDeatil" runat="server" CssClass="form-control" Text='<%# Eval("test_detail_name")%>' />
                                            <asp:RequiredFieldValidator ID="RequiredtxtTestEditDetailName" runat="server"
                                                ControlToValidate="tbupdateTestDeatil" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกชื่อประเภทการตรวจ" ValidationGroup="Editgroup" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatortxtTestEditDetailName" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredtxtTestEditDetailName" Width="250" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:TextBox ID="txttestTypeIDX" runat="server" CssClass="form-control" Visible="False" Text='<%# Eval("test_type_idx")%>' />
                                        <asp:Label ID="lblupdateTestDetail" CssClass="col-sm-3 control-label" runat="server" Text="ประเภทการปฏิบัติการ" />
                                        <div class="col-sm-7">

                                            <asp:DropDownList ID="ddlupdateTestDeatil"
                                                CssClass="form-control fa-align-left" runat="server">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredEditddlLabType" runat="server" InitialValue="0"
                                                ControlToValidate="ddlupdateTestDeatil" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณาเลือกประเภทการปฏิบัติการ" ValidationGroup="Editgroup" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorEditddlLabType" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredEditddlLabType" Width="250" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="Label2" CssClass="col-sm-3 control-label" runat="server" Text="ระยะเวลาในการดำเนินการ" />
                                        <div class="col-sm-7">
                                            <asp:TextBox ID="txtupdateActionTime" runat="server" CssClass="form-control" Text='<%# Eval("test_detail_time")%>' />

                                            <asp:RequiredFieldValidator ID="RequiredEdittxtActionTime" runat="server"
                                                ControlToValidate="txtupdateActionTime" Display="None" SetFocusOnError="true"
                                                ErrorMessage="*กรุณากรอกระยะเวลาในการดำเนินการ" ValidationGroup="Editgroup" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorEdittxtActionTime" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RequiredEdittxtActionTime" Width="280" />
                                            <asp:RegularExpressionValidator ID="RegularEdittxtActionTime"
                                                runat="server" ErrorMessage="*กรอกเฉพาะตัวเลขเท่านั้น" SetFocusOnError="true"
                                                Display="None" ControlToValidate="txtupdateActionTime" ValidationExpression="^[1-9'\s]{1,10}$"
                                                ValidationGroup="Editgroup" />
                                            <ajaxToolkit:ValidatorCalloutExtender ID="ValEdittxtActionTime" runat="Server"
                                                HighlightCssClass="validatorCalloutHighlight" TargetControlID="RegularEdittxtActionTime" Width="280" />
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <asp:Label ID="lbupdateTestDetailStatus" CssClass="col-sm-3 control-label" runat="server" Text="สถานะ" />
                                        <div class="col-sm-7">
                                            <asp:DropDownList ID="ddlupdate_test_status" Text='<%# Eval("test_detail_status") %>'
                                                CssClass="form-control fa-align-left" runat="server">
                                                <asp:ListItem Value="1">online</asp:ListItem>
                                                <asp:ListItem Value="0">offline</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-2"></div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-3"></div>
                                        <div class="col-sm-9">
                                            <asp:LinkButton ID="btnupdate" CssClass="btn btn-success" ValidationGroup="Editgroup" runat="server" data-original-title="Save" data-toggle="tooltip"
                                                Text="Save" CommandName="Update" OnClientClick="return confirm('คุณต้องการแก้ไขรายการนี้ใช่หรือไม่ ?')"></asp:LinkButton>
                                            <asp:LinkButton ID="lbCmdCancel" CssClass="btn btn-danger" runat="server" data-original-title="Cancel" data-toggle="tooltip"
                                                Text="Cancel" CommandName="Cancel"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อประเภทการตรวจวิเคราะห์" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblTestName" runat="server" Text='<%# Eval("test_detail_name")%>'></asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ประเภทการปฏิบัติการ" ItemStyle-HorizontalAlign="Left"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lbltesttypename" runat="server" Text='<%# Eval("test_type_name_th")%>'></asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ระยะเวลาในการดำเนินการ" ItemStyle-HorizontalAlign="center"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>
                                <asp:Label ID="lblDetailTime" runat="server" Text='<%# Eval("test_detail_time") + " "+ "วัน" %>'> </asp:Label>
                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="สถานะ" ItemStyle-HorizontalAlign="center"
                        HeaderStyle-CssClass="text-center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>
                            <small>

                                <asp:Label ID="lbstatus" Visible="false" runat="server"
                                    CssClass="col-sm-12" Text='<%# Eval("test_detail_status") %>'></asp:Label>
                                <asp:Label ID="status_offline" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="offline"
                                    CssClass="col-sm-12">
                                                <div style="color: red;">
                                                    <span><b>offline</b></span>
                                </asp:Label>
                                <asp:Label ID="status_online" runat="server" Visible="false"
                                    data-toggle="tooltip" data-placement="top" title="online" CssClass="col-sm-12">
                                                <div style="color: green;">
                                                <span><b>online</b></span>
                                </asp:Label>

                            </small>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="การจัดการ" HeaderStyle-CssClass="text-center"
                        ItemStyle-HorizontalAlign="center" HeaderStyle-Font-Size="Small">
                        <ItemTemplate>

                            <%--   <asp:LinkButton ID="btnviewset" CssClass="text-read small" runat="server" CommandName="cmdViewSet"
                                data-toggle="tooltip" OnCommand="btnCommand"
                                CommandArgument='<%#Eval("test_detail_idx") + "," + "0" %>' title="view">--%>
                            <%--        <i class="glyphicon glyphicon-list-alt" aria-hidden="true"></i></asp:LinkButton>--%>
                            <asp:LinkButton ID="Edit" CssClass="text-edit" runat="server" CommandName="Edit"
                                data-toggle="tooltip" OnCommand="btnCommand" title="edit">
                                        <i class="glyphicon glyphicon-edit"></i></asp:LinkButton>
                            <asp:LinkButton ID="btndelettypeNews" CssClass="text-trash" runat="server" CommandName="cmdDelete"
                                data-toggle="tooltip" OnCommand="btnCommand" OnClientClick="return confirm('Do you want delete this item?')"
                                CommandArgument='<%#Eval("test_detail_idx") +"," + "0" %>' title="delete">
                                        <i class="glyphicon glyphicon-trash"></i></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
           
        </asp:View>

       


    </asp:MultiView>



</asp:Content>

